/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.TransferSeatBcCount;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airreservation.api.dto.AutoCancellationSchDTO;
import com.isa.thinair.airreservation.api.dto.EffectedFlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.FlightReservationSummaryDTO;
import com.isa.thinair.airreservation.api.dto.FlightSegmentReservationDTO;
import com.isa.thinair.airreservation.api.dto.OpenReturnOndTO;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentTransferDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PnrPaxFareSegWisePaxTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotification;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.model.ReservationSegmentTransfer;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airschedules.api.model.OperationType;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * ReservationSegmentDAOImpl is the business DAO hibernate implmentation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationSegmentDAO"
 */
public class ReservationSegmentDAOImpl extends PlatformHibernateDaoSupport implements ReservationSegmentDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationSegmentDAOImpl.class);

	/**
	 * Returns reservation segment
	 * 
	 * @param pnrSegId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@Override
	public ReservationSegment getReservationSegment(int pnrSegId) throws CommonsDataAccessException {
		log.debug("Inside getReservationSegment");

		// Returns reservation segment
		ReservationSegment reservationSegment = (ReservationSegment) get(ReservationSegment.class, new Integer(pnrSegId));

		if (reservationSegment == null) {
			throw new CommonsDataAccessException("airreservations.arg.segNoLongerExist");
		}

		log.debug("Exit getReservationSegment");
		return reservationSegment;
	}

	/**
	 * Saves reservation segment
	 * 
	 * @param segments
	 */
	@Override
	public void saveReservationSegment(ReservationSegment reservationSegment) {
		log.debug("Inside saveReservationSegment");

		// Saves reservation segment
		hibernateSaveOrUpdate(reservationSegment);

		log.debug("Exit saveReservationSegment");
	}

	/**
	 * Saves reservation segments
	 * 
	 * @param segments
	 */
	@Override
	public void saveReservationSegments(Collection<ReservationSegment> segments) {
		log.debug("Inside saveReservationSegments");

		// Saves reservation segments
		hibernateSaveOrUpdateAll(segments);

		log.debug("Exit saveReservationSegments");
	}

	/**
	 * Returns Reservation Segments
	 * 
	 * @param pnr
	 * @return
	 */
	@Override
	public Collection<ReservationSegment> getPnrSegments(String pnr) {
		String hql = "SELECT rs FROM ReservationSegment AS rs " + "WHERE rs.reservation.pnr = ? ";

		Object[] params = { pnr };
		return find(hql, params, ReservationSegment.class);
	}

	/**
	 * Returns Reservations
	 * 
	 * @param flightSegId
	 * @return
	 */
	@Override
	public Collection<Reservation> getPnrsForSegment(int flightSegId) {
		String hql = "SELECT r FROM Reservation r, ReservationSegment rs "
				+ "WHERE r.pnr = rs.reservation.pnr AND rs.flightSegId = ? and rs.status = 'CNF' ";

		Object[] params = { new Integer(flightSegId) };
		return find(hql, params, Reservation.class);
	}

	/**
	 * Return reservation segment all information
	 * 
	 * @param pnr
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationSegmentDTO> getPnrSegmentsView(String pnr) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT RSEG.PNR PNR                                   ,"
				+ "RSEG.PNR_SEG_ID PNR_SEG_ID                               ,"
				+ "RSEG.EXT_REF EXT_REF                                     ,"
				+ "RSEG.SEGMENT_SEQ SEG_SEQ                                 ,"
				+ "RSEG.RETURN_FLAG RFLAG                                   ,"
				+ "RSEG.RETURN_GROUP_ID RGROUP_ID                           ,"
				+ "RSEG.STATUS STATUS                                       ,"
				+ "RSEG.CS_FLIGHT_NUMBER                                    ,"
				+ "RSEG.CS_BOOKING_CLASS                                    ,"
				+ "RSEG.SUB_STATUS SUB_STATUS                               ,"
				+ "RSEG.OND_GROUP_ID GROUP_ID                               ,"
				+ "RSEG.ALERT_FLAG ALERT_FLAG                               ,"
				+ "FSEG.SEGMENT_CODE SCODE                                  ,"
				+ "FSEG.STATUS FLT_SEG_STATUS                               ,"
				+ "FSEG.EST_TIME_DEPARTURE_LOCAL DTIME                      ,"
				+ "FSEG.EST_TIME_ARRIVAL_LOCAL ATIME                        ,"
				+ "FSEG.EST_TIME_DEPARTURE_ZULU ZULU_DTIME                  ,"
				+ "FSEG.EST_TIME_ARRIVAL_ZULU ZULU_ATIME                    ,"
				+ "FSEG.FLT_SEG_ID F_SEG_ID                                 ,"
				+ "FSEG.BAGGAGE_ALLOWANCE_REMARKS BG_ALL_RMKS               ,"
				+ "F.FLIGHT_NUMBER FNUMBER                                  ,"
				+ "F.FLIGHT_ID F_ID                                         ,"
				+ "F.FLIGHT_TYPE F_TYPE                                     ,"
				+ "F.OPERATION_TYPE_ID                                      ,"
				+ "F.STATUS FLT_STATUS                                      ,"
				+ "F.CS_OC_CARRIER_CODE CS_OC_CARRIER_CODE                  ,"
				+ "F.CS_OC_FLIGHT_NUMBER CS_OC_FLIGHT_NUMBER                ,"
				+ "T.SHOWN_IN_ITINERARY SHOWN_IN_ITINERARY                  ,"
				+ "RSEG.OPEN_RT_CONFIRM_BEFORE                              ,"
				+ "RSEG.RETURN_GROUP_ID RETURN_OND_GROUP_ID                 ,"
				+ "RSEG.SUB_STATION_SHORTNAME                               ,"
				+ "RSEG.MKTING_CARRIER_CODE                                 ,"
				+ "RSEG.MKTING_STATION_CODE                                 ,"
				+ "RSEG.MKTING_AGENT_CODE                                   ,"
				+ "RSEG.STATUS_MOD_DATE                                     ,"
				+ "RSEG.STATUS_MOD_CHANNEL_CODE                             ,"
				+ "RSEG.GROUND_SEG_ID                                       ,"
				+ "RSEG.LAST_FQ_DATE                                        ,"
				+ "RSEG.TICKET_VALID_TILL                                   ,"
				+ "RSEG.JOURNEY_SEQ                                 		,"
				+ "RSEG.BAGGAGE_OND_GROUP_ID                                ,"
				+ "DEPTERMINAL.TERMINAL_SHORTNAME AS DEPARTURE_TERMINAL_NAME,"
				+ "ARRTERMINAL.TERMINAL_SHORTNAME AS ARRIVAL_TERMINAL_NAME,AM.MODEL_NUMBER,AM.ITIN_DESCRIPTION,F.REMARKS,"
				+ "RSEG.AUTO_CANCELLATION_ID, RSEG.MODIFIED_FROM, RSEG.BUNDLED_FARE_PERIOD_ID ";

		sql += "FROM T_PNR_SEGMENT RSEG, " + "T_FLIGHT_SEGMENT FSEG " + "LEFT OUTER JOIN T_AIRPORT_TERMINAL ARRTERMINAL "
				+ "ON (FSEG.ARRIVAL_TERMINAL_ID = ARRTERMINAL.AIRPORT_TERMINAL_ID) "
				+ "LEFT OUTER JOIN T_AIRPORT_TERMINAL DEPTERMINAL "
				+ "ON (FSEG.DEPARTURE_TERMINAL_ID = DEPTERMINAL.AIRPORT_TERMINAL_ID), " + "T_FLIGHT F ,T_AIRCRAFT_MODEL AM,";

		sql += "T_OPERATION_TYPE T " + "WHERE RSEG.FLT_SEG_ID = FSEG.FLT_SEG_ID " + "AND FSEG.FLIGHT_ID      = F.FLIGHT_ID "
				+ "AND F.OPERATION_TYPE_ID = T.OPERATION_TYPE_ID AND F.MODEL_NUMBER = AM.MODEL_NUMBER ";

		sql += "AND RSEG.PNR            = ? " + "ORDER BY RSEG.OND_GROUP_ID, " + "FSEG.EST_TIME_DEPARTURE_LOCAL ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameter            : " + pnr);
		log.debug("############################################");

		Collection<ReservationSegmentDTO> segments = (Collection<ReservationSegmentDTO>) jt.query(sql, new Object[] { pnr },
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						ReservationSegmentDTO reservationSegmentDTO;
						Collection<ReservationSegmentDTO> segments = new ArrayList<ReservationSegmentDTO>();

						if (rs != null) {
							while (rs.next()) {
								reservationSegmentDTO = new ReservationSegmentDTO();

								reservationSegmentDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));
								reservationSegmentDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID")));
								reservationSegmentDTO.setSegmentSeq(BeanUtils.parseInteger(rs.getInt("SEG_SEQ")));
								reservationSegmentDTO.setReturnFlag(BeanUtils.nullHandler(rs.getString("RFLAG")));
								reservationSegmentDTO.setReturnGroupId(BeanUtils.parseInteger(rs.getInt("RGROUP_ID")));
								reservationSegmentDTO.setStatus(BeanUtils.nullHandler(rs.getString("STATUS")));
								reservationSegmentDTO.setSubStatus(BeanUtils.nullHandler(rs.getString("SUB_STATUS")));
								reservationSegmentDTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("SCODE")));
								reservationSegmentDTO.setDepartureDate(rs.getTimestamp("DTIME"));
								reservationSegmentDTO.setArrivalDate(rs.getTimestamp("ATIME"));
								reservationSegmentDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("FNUMBER")));
								reservationSegmentDTO.setCodeShareFlightNo(rs.getString("CS_FLIGHT_NUMBER"));
								reservationSegmentDTO.setCodeShareBc(rs.getString("CS_BOOKING_CLASS"));
								reservationSegmentDTO.setCsOcCarrierCode(rs.getString("CS_OC_CARRIER_CODE"));
								reservationSegmentDTO.setCsOcFlightNumber(rs.getString("CS_OC_FLIGHT_NUMBER"));
								reservationSegmentDTO.setFlightSegStatus(BeanUtils.nullHandler(rs.getString("FLT_SEG_STATUS")));
								reservationSegmentDTO.setFlightStatus(BeanUtils.nullHandler(rs.getString("FLT_STATUS")));
								reservationSegmentDTO.setOperationTypeID(BeanUtils.parseInteger(rs.getInt("OPERATION_TYPE_ID")));
								reservationSegmentDTO.setFareGroupId(BeanUtils.parseInteger(rs.getInt("GROUP_ID")));
								reservationSegmentDTO.setFlightSegId(BeanUtils.parseInteger(rs.getInt("F_SEG_ID")));
								reservationSegmentDTO.setFlightSegBagAllowance(BeanUtils.parseInteger(rs.getInt("BG_ALL_RMKS")));
								reservationSegmentDTO.setFlightId(BeanUtils.parseInteger(rs.getInt("F_ID")));
								reservationSegmentDTO.setAlertFlag(rs.getInt("ALERT_FLAG"));
								reservationSegmentDTO.setZuluArrivalDate(rs.getTimestamp("ZULU_ATIME"));
								reservationSegmentDTO.setZuluDepartureDate(rs.getTimestamp("ZULU_DTIME"));
								reservationSegmentDTO.setShownInItinerary(OperationType.VISIBLE_IN_ITINERARY
										.equals(BeanUtils.nullHandler(rs.getString("SHOWN_IN_ITINERARY"))) ? true : false);
								reservationSegmentDTO.setOpenRtConfirmBeforeZulu(rs.getTimestamp("OPEN_RT_CONFIRM_BEFORE"));
								reservationSegmentDTO
										.setReturnOndGroupId(BeanUtils.parseInteger(rs.getInt("RETURN_OND_GROUP_ID")));
								reservationSegmentDTO.setSubStationShortName(rs.getString("SUB_STATION_SHORTNAME"));
								reservationSegmentDTO.setGroundStationPnrSegmentID(rs.getBigDecimal("GROUND_SEG_ID") == null
										? null
										: rs.getBigDecimal("GROUND_SEG_ID").intValue());
								reservationSegmentDTO.setExternalReference(rs.getString("EXT_REF"));
								reservationSegmentDTO.setDepartureTerminalName(rs.getString("DEPARTURE_TERMINAL_NAME"));
								reservationSegmentDTO.setArrivalTerminalName(rs.getString("ARRIVAL_TERMINAL_NAME"));
								reservationSegmentDTO.setMarketingAgentCode(rs.getString("MKTING_AGENT_CODE"));
								reservationSegmentDTO.setMarketingCarrierCode(rs.getString("MKTING_CARRIER_CODE"));
								reservationSegmentDTO.setMarketingStationCode(rs.getString("MKTING_STATION_CODE"));
								reservationSegmentDTO.setStatusModifiedChannelCode(
										BeanUtils.parseInteger(rs.getInt("STATUS_MOD_CHANNEL_CODE")));
								reservationSegmentDTO.setStatusModifiedDate(rs.getTimestamp("STATUS_MOD_DATE"));
								reservationSegmentDTO.setFlightType(rs.getString("F_TYPE"));

								reservationSegmentDTO.setFlightModelNumber(rs.getString("MODEL_NUMBER"));
								reservationSegmentDTO.setFlightModelDescription(rs.getString("ITIN_DESCRIPTION"));
								reservationSegmentDTO.setLastFareQuoteDateZulu(rs.getTimestamp("LAST_FQ_DATE"));
								reservationSegmentDTO
										.setBaggageOndGroupId(BeanUtils.nullHandler(rs.getString("BAGGAGE_OND_GROUP_ID")));

								reservationSegmentDTO.setTicketValidTill(rs.getTimestamp("TICKET_VALID_TILL"));

								String durationTime = FlightUtil.getDuration(rs.getTimestamp("ZULU_DTIME"),
										rs.getTimestamp("ZULU_ATIME"));
								reservationSegmentDTO.setFlightDuration(durationTime);
								reservationSegmentDTO.setRemarks(rs.getString("REMARKS"));
								reservationSegmentDTO.setJourneySeq(rs.getInt("JOURNEY_SEQ"));
								if (rs.getInt("AUTO_CANCELLATION_ID") != 0) {
									reservationSegmentDTO.setAlertAutoCancellation(true);
								}
								reservationSegmentDTO.setModifiedFrom(rs.getString("MODIFIED_FROM"));
								reservationSegmentDTO.setBundledFarePeriodId(
										rs.getString("BUNDLED_FARE_PERIOD_ID") == null ? null : rs.getInt("BUNDLED_FARE_PERIOD_ID"));
								segments.add(reservationSegmentDTO);
							}
						}

						return segments;
					}
				});

		return segments;
	}

	/**
	 * method to get Bc with the availability
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, TransferSeatBcCount> getBCCodesWithAvailability(int pnrSegId, boolean considerCancelledPax) {
		final boolean considerCancelledPaxLocal = considerCancelledPax;
		String sql = "SELECT " + "pas.PNR_PAX_ID, pas.STATUS , pas.PAX_TYPE_CODE, paxFareSeg.BOOKING_CODE " + "FROM "
				+ "T_PNR_PASSENGER pas, " + "T_PNR_PAX_FARE  paxFare, " + "T_PNR_PAX_FARE_SEGMENT paxFareSeg " + "WHERE "
				+ "pas.PNR_PAX_ID = paxFare.PNR_PAX_ID and " + "paxFare.PPF_ID = paxFareSeg.PPF_ID and "
				+ "paxFareSeg.PNR_SEG_ID = " + pnrSegId;

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		HashMap<String, TransferSeatBcCount> bcAvailMap = (HashMap<String, TransferSeatBcCount>) jt.query(sql,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						HashMap<String, TransferSeatBcCount> bcAvailMap = new HashMap<String, TransferSeatBcCount>();

						if (rs != null) {
							while (rs.next()) {
								// count only adult seat count
								// count the number of infants.
								String bookingCode = rs.getString("BOOKING_CODE");
								String paxType = rs.getString("PAX_TYPE_CODE");

								if (bookingCode != null) {
									TransferSeatBcCount trSeatCount = bcAvailMap.get(bookingCode);
									if (trSeatCount == null) {
										trSeatCount = new TransferSeatBcCount();
									}

									if (paxType.equals(ReservationInternalConstants.PassengerType.ADULT)
											|| paxType.equals(ReservationInternalConstants.PassengerType.CHILD)) {

										String status = BeanUtils.nullHandler(rs.getString("STATUS"));

										if (status.equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
											trSeatCount.setSoldCount(trSeatCount.getSoldCount() + 1);
										} else if (status.equals(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD)) {
											trSeatCount.setOnHoldCount(trSeatCount.getOnHoldCount() + 1);
										} else if (considerCancelledPaxLocal
												&& status.equals(ReservationInternalConstants.ReservationPaxStatus.CANCEL)) {
											trSeatCount.setSoldCount(trSeatCount.getSoldCount() + 1);
										}

										bcAvailMap.put(bookingCode, trSeatCount);
									}
								}
							}
						}

						return bcAvailMap;
					}
				});

		return bcAvailMap;
	}

	/**
	 * Update Reservation Alerts FIXME UPDATING SCHEDULES WITH PNR PAX SEMGENT EXCEEDING 1000 FAILS NEED TO CHANGE THE
	 * SQL OPERATION TO UPDATE CHUNCK BY CHUNCK
	 * 
	 * @param alertFlag
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 */
	@Override
	public void updateReservationAlerts(int alertFlag, Collection<Integer> flightSegmentIDs, Collection<Integer> pnrSegIDs,
			String status) {

		if ((flightSegmentIDs != null && flightSegmentIDs.size() == 0 || flightSegmentIDs == null)
				&& (pnrSegIDs != null && pnrSegIDs.size() == 0 || pnrSegIDs == null)
				&& (flightSegmentIDs != null || pnrSegIDs != null)) {
			return;
		}

		String hqlUpdate = "";

		if (status == null) {
			hqlUpdate = " update ReservationSegment set alertFlag = " + alertFlag;

			boolean entryFound = false;

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				entryFound = true;
				hqlUpdate += " where flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				if (entryFound) {
					hqlUpdate += " and pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				} else {
					hqlUpdate += " where pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				}
			}
		} else {
			hqlUpdate = " update ReservationSegment set alertFlag = " + alertFlag + " where status = '" + status + "' ";

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				hqlUpdate += " and flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				hqlUpdate += " and pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
			}
		}

		getSession().createQuery(hqlUpdate).executeUpdate();
	}

	/**
	 * Update ReservationSegment Seat Re-protect Status.
	 * 
	 * @param alertFlag
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 */
	@Override
	public void updateReservationSegmentSeatReprotectStatus(int seatReprotectStatus, Collection<Integer> flightSegmentIDs,
			Collection<Integer> pnrSegIDs, String status) {

		if ((flightSegmentIDs != null && flightSegmentIDs.size() == 0 || flightSegmentIDs == null)
				&& (pnrSegIDs != null && pnrSegIDs.size() == 0 || pnrSegIDs == null)
				&& (flightSegmentIDs != null || pnrSegIDs != null)) {
			return;
		}

		String hqlUpdate = "";

		if (status == null) {
			hqlUpdate = " update ReservationSegment set seatReprotectStatus = " + seatReprotectStatus;

			boolean entryFound = false;

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				entryFound = true;
				hqlUpdate += " where flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				if (entryFound) {
					hqlUpdate += " and pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				} else {
					hqlUpdate += " where pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				}
			}
		} else {
			hqlUpdate = " update ReservationSegment set seatReprotectStatus = " + seatReprotectStatus + " where status = '"
					+ status + "' ";

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				hqlUpdate += " and flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				hqlUpdate += " and pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
			}
		}

		getSession().createQuery(hqlUpdate).executeUpdate();
	}

	/**
	 * Update ReservationSegment Meal Re-protect Status
	 * 
	 * @param alertFlag
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 */
	@Override
	public void updateReservationSegmentMealReprotectStatus(int mealReprotectStatus, Collection<Integer> flightSegmentIDs,
			Collection<Integer> pnrSegIDs, String status) {

		if ((flightSegmentIDs != null && flightSegmentIDs.size() == 0 || flightSegmentIDs == null)
				&& (pnrSegIDs != null && pnrSegIDs.size() == 0 || pnrSegIDs == null)
				&& (flightSegmentIDs != null || pnrSegIDs != null)) {
			return;
		}

		String hqlUpdate = "";

		if (status == null) {
			hqlUpdate = " update ReservationSegment set mealReprotectStatus = " + mealReprotectStatus;

			boolean entryFound = false;

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				entryFound = true;
				hqlUpdate += " where flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				if (entryFound) {
					hqlUpdate += " and pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				} else {
					hqlUpdate += " where pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				}
			}
		} else {
			hqlUpdate = " update ReservationSegment set mealReprotectStatus = " + mealReprotectStatus + " where status = '"
					+ status + "' ";

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				hqlUpdate += " and flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				hqlUpdate += " and pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
			}
		}

		getSession().createQuery(hqlUpdate).executeUpdate();
	}

	/**
	 * Return reservation segments
	 * 
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 * @param status
	 * @param loadReservation
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationSegment> getReservationSegments(Collection<Integer> flightSegmentIDs,
			Collection<Integer> pnrSegIDs, String status, boolean loadReservation, boolean loadFares) {

		if ((flightSegmentIDs != null && flightSegmentIDs.size() == 0 || flightSegmentIDs == null)
				&& (pnrSegIDs != null && pnrSegIDs.size() == 0 || pnrSegIDs == null)
				&& (flightSegmentIDs != null || pnrSegIDs != null)) {
			return new ArrayList<ReservationSegment>();
		}

		String hqlSelect = "";

		if (status == null) {
			hqlSelect = "select rs from ReservationSegment rs ";

			boolean entryFound = false;

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				entryFound = true;
				hqlSelect += " where rs.flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				if (entryFound) {
					hqlSelect += " and rs.pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				} else {
					hqlSelect += " where rs.pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
				}
			}
		} else {
			hqlSelect = "select rs from ReservationSegment rs " + " where rs.status = '" + status + "' ";

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				hqlSelect += " and rs.flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}

			if (pnrSegIDs != null && pnrSegIDs.size() > 0) {
				hqlSelect += " and rs.pnrSegId in (" + Util.buildIntegerInClauseContent(pnrSegIDs) + ") ";
			}
		}

		Collection<ReservationSegment> colReservationSegment = find(hqlSelect, ReservationSegment.class);
		if (loadReservation) {
			for (ReservationSegment reservationSegment : colReservationSegment) {
				Reservation reservation = reservationSegment.getReservation();
				// Lazly loading in order to load contact information at a late stage
				reservation.getContactInfo();

				if (loadFares) {
					Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
					ReservationPax reservationPax;
					while (itReservationPax.hasNext()) {
						reservationPax = itReservationPax.next();
						reservationPax.getPnrPaxFares().isEmpty();
					}
				}
			}
		}
		return colReservationSegment;
	}

	/**
	 * Returns the no.of confirmed and onhold pnrs for each segment of a flight
	 * 
	 * @param flightIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<FlightReservationSummaryDTO> getFlightReservationsSummary(Collection<Integer> flightIds) {
		String sql = " SELECT flgSeg.flight_id, flgSeg.flt_seg_id, "
				+ " flgSeg.segment_code, res.status, COUNT(DISTINCT resSeg.pnr) AS pnrs "
				+ " FROM T_RESERVATION res, T_FLIGHT_SEGMENT flgSeg, T_PNR_SEGMENT resSeg " + " WHERE flgSeg.flight_id IN ("
				+ Util.buildIntegerInClauseContent(flightIds) + ") " + " AND flgSeg.flt_seg_id = resSeg.flt_seg_id "
				+ " AND resSeg.pnr = res.pnr " + " AND resSeg.status IN ('"
				+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "') " + " AND res.status IN ('"
				+ ReservationInternalConstants.ReservationStatus.CONFIRMED + "', '"
				+ ReservationInternalConstants.ReservationStatus.ON_HOLD + "') "
				+ " GROUP BY flgSeg.flight_id, flgSeg.flt_seg_id, flgSeg.segment_code, res.status "
				+ " ORDER BY flgSeg.flight_id, flgSeg.flt_seg_id ";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		log.debug("sql for getFlightReservationsSummary() [ " + sql + " ]");

		List<FlightReservationSummaryDTO> flightRes = (List<FlightReservationSummaryDTO>) jt.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightReservationSummaryDTO> flightRes = new ArrayList<FlightReservationSummaryDTO>();
				HashMap<Integer, HashMap<String, FlightSegmentReservationDTO>> flightResMap = new HashMap<Integer, HashMap<String, FlightSegmentReservationDTO>>();
				HashMap<String, FlightSegmentReservationDTO> segments = new HashMap<String, FlightSegmentReservationDTO>();
				FlightSegmentReservationDTO flgSegResDTO = new FlightSegmentReservationDTO();
				Integer flightId;
				String segmentCode;
				String status;
				int pnrs;
				if (rs != null) {
					while (rs.next()) {
						flightId = new Integer(rs.getInt("flight_id"));
						segmentCode = rs.getString("segment_code");
						status = BeanUtils.nullHandler(rs.getString("status"));
						pnrs = rs.getInt("pnrs");
						if (flightId != null && segmentCode != null) {
							segments = flightResMap.get(flightId);
							if (segments == null) {
								segments = new HashMap<String, FlightSegmentReservationDTO>();
							}
							flgSegResDTO = segments.get(segmentCode);
							if (flgSegResDTO == null) {
								flgSegResDTO = new FlightSegmentReservationDTO();
								flgSegResDTO.setSegmentCode(segmentCode);
							}
							if (status.trim().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
								flgSegResDTO.setConfirmedPnrs(pnrs);
							} else if (status.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
								flgSegResDTO.setOnholdPnrs(pnrs);
							}
							segments.put(segmentCode, flgSegResDTO);
							flightResMap.put(flightId, segments);
						}
					}
				}
				Iterator<Integer> resultIt = flightResMap.keySet().iterator();
				while (resultIt.hasNext()) {
					flightId = resultIt.next();
					if (flightId != null) {
						segments = flightResMap.get(flightId);
						if (segments != null) {
							Iterator<String> segIt = segments.keySet().iterator();
							List<FlightSegmentReservationDTO> listSegments = new ArrayList<FlightSegmentReservationDTO>();
							while (segIt.hasNext()) {
								segmentCode = segIt.next();
								if (segmentCode != null) {
									flgSegResDTO = segments.get(segmentCode);
									listSegments.add(flgSegResDTO);
								}
							}
							if (!listSegments.isEmpty()) {
								FlightReservationSummaryDTO flgResSummDTO = new FlightReservationSummaryDTO();
								flgResSummDTO.setFlightId(flightId);
								flgResSummDTO.setSegmentReservations(listSegments);
								flightRes.add(flgResSummDTO);
							}
						}
					}
				}
				return flightRes;
			}
		});
		return flightRes;
	}

	/**
	 * Return the flight segment
	 * 
	 * @param flightSegmentId
	 * @return
	 */
	@Override
	public FlightSegmentDTO getFlightSegment(int flightSegmentId) {
		String sql = "SELECT "
				+ "fseg.flt_seg_id, fseg.flight_id, fseg.segment_code, flt.flight_number, fseg.est_time_departure_local,  fseg.EST_TIME_ARRIVAL_ZULU, fseg.EST_TIME_ARRIVAL_LOCAL,"
				+ "flt.cs_oc_carrier_code, flt.cs_oc_flight_number FROM T_FLIGHT_SEGMENT fseg, T_Flight flt where fseg.flight_id = flt.flight_id and "
				+ " fseg.flt_seg_id =" + flightSegmentId;

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();

				if (rs != null) {
					while (rs.next()) {

						int flightSegId = rs.getInt("FLT_SEG_ID");
						int flightId = rs.getInt("FLIGHT_ID");
						String segmentCode = rs.getString("SEGMENT_CODE");

						flightSegmentDTO.setSegmentId(new Integer(flightSegId));
						flightSegmentDTO.setFlightId(new Integer(flightId));
						flightSegmentDTO.setSegmentCode(segmentCode);
						flightSegmentDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
						flightSegmentDTO.setDepartureDateTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
						flightSegmentDTO.setArrivalDateTimeZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
						flightSegmentDTO.setArrivalDateTime(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
						flightSegmentDTO.setCsOcCarrierCode(rs.getString("cs_oc_carrier_code"));
						flightSegmentDTO.setCsOcFlightNumber(rs.getString("cs_oc_flight_number"));
					}
				}
				return flightSegmentDTO;
			}
		});
		return flightSegmentDTO;
	}

	/**
	 * Return confirmed reservation ond(s)
	 * 
	 * @param colFlightSegIds
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<Integer, EffectedFlightSegmentDTO>> getConfirmedPnrOnds(Collection<Integer> colFlightSegIds)
			throws CommonsDataAccessException {
		log.debug("Inside getConfirmedPnrOnds");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		if (colFlightSegIds == null || colFlightSegIds.size() == 0) {
			throw new CommonsDataAccessException("airreservations.arg.cannotFindFlightSegments");
		}

		// Let's get the flt_seg_ids query build up
		Iterator<Integer> itColFlightSegIds = colFlightSegIds.iterator();
		StringBuffer composedSql = new StringBuffer();
		int i = 1;
		while (itColFlightSegIds.hasNext()) {
			itColFlightSegIds.next();

			if (i == 1) {
				composedSql.append(" WHERE FLT_SEG_ID = ? ");
			} else {
				composedSql.append(" OR FLT_SEG_ID = ? ");
			}

			i++;
		}

		String sql = " SELECT rSeg.PNR, rSeg.PNR_SEG_ID R_PSID, rSeg.OND_GROUP_ID, rSeg.SEGMENT_SEQ, fSeg.FLT_SEG_ID, fSeg.PNR_SEG_ID A_PSID "
				+ " FROM T_PNR_SEGMENT rSeg, (SELECT PNR, OND_GROUP_ID, FLT_SEG_ID, PNR_SEG_ID FROM T_PNR_SEGMENT "
				+ composedSql.toString() + " ) fSeg " + " WHERE rSeg.PNR = fSeg.PNR AND rSeg.OND_GROUP_ID = fSeg.OND_GROUP_ID "
				+ " AND rSeg.STATUS in ('" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "','"
				+ ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING + "')"
				+ " ORDER BY rSeg.PNR, rSeg.OND_GROUP_ID, rSeg.SEGMENT_SEQ ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL parameter            : " + colFlightSegIds);
		log.debug("############################################");

		Map<String, Map<Integer, EffectedFlightSegmentDTO>> mapPnrOnds = (Map<String, Map<Integer, EffectedFlightSegmentDTO>>) jt
				.query(sql, colFlightSegIds.toArray(), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Map<Integer, EffectedFlightSegmentDTO>> mapPnrOnds = new HashMap<String, Map<Integer, EffectedFlightSegmentDTO>>();
						Map<Integer, EffectedFlightSegmentDTO> mapGrpSegIds;

						String pnr;
						Integer ondGroupId;
						Integer pnrSegId;
						Integer actualPnrSegId;
						Integer actualFltSegId;
						EffectedFlightSegmentDTO effectedFlightSegmentDTO;

						if (rs != null) {
							while (rs.next()) {
								pnr = BeanUtils.nullHandler(rs.getString("PNR"));
								ondGroupId = BeanUtils.parseInteger(rs.getInt("OND_GROUP_ID"));
								pnrSegId = BeanUtils.parseInteger(rs.getInt("R_PSID"));
								actualPnrSegId = BeanUtils.parseInteger(rs.getInt("A_PSID"));
								actualFltSegId = BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID"));

								// If Pnr exist
								if (mapPnrOnds.containsKey(pnr)) {
									mapGrpSegIds = mapPnrOnds.get(pnr);

									if (mapGrpSegIds.containsKey(ondGroupId)) {
										effectedFlightSegmentDTO = mapGrpSegIds.get(ondGroupId);

										effectedFlightSegmentDTO.addAllPnrSegmentId(pnrSegId);
										effectedFlightSegmentDTO.addActualPnrSegmentId(actualPnrSegId);
									} else {
										effectedFlightSegmentDTO = new EffectedFlightSegmentDTO();

										effectedFlightSegmentDTO.addAllPnrSegmentId(pnrSegId);
										effectedFlightSegmentDTO.addActualPnrSegmentId(actualPnrSegId);
										effectedFlightSegmentDTO.setFlightSegmentId(actualFltSegId);

										mapGrpSegIds.put(ondGroupId, effectedFlightSegmentDTO);
									}
								} else {
									mapGrpSegIds = new HashMap<Integer, EffectedFlightSegmentDTO>();

									effectedFlightSegmentDTO = new EffectedFlightSegmentDTO();

									effectedFlightSegmentDTO.addAllPnrSegmentId(pnrSegId);
									effectedFlightSegmentDTO.addActualPnrSegmentId(actualPnrSegId);
									effectedFlightSegmentDTO.setFlightSegmentId(actualFltSegId);

									mapGrpSegIds.put(ondGroupId, effectedFlightSegmentDTO);
									mapPnrOnds.put(pnr, mapGrpSegIds);
								}
							}
						}

						return mapPnrOnds;
					}
				});

		log.debug("Exit getConfirmedPnrOnds");
		return mapPnrOnds;
	}

	/**
	 * Returns pnr segments for reconcilation
	 * 
	 * @param flightSegId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<FlightReconcileDTO> getPnrSegmentsForReconcilation(Collection<Integer> flightSegId, String pnr,
			boolean excludePrintExchangedStatus) throws CommonsDataAccessException {
		if (flightSegId == null) {
			throw new CommonsDataAccessException("airreservations.arg.emptyRecFlightInfo");
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT rSeg.PNR PNR, rSeg.PNR_SEG_ID PNR_SEG_ID, fSeg.FLT_SEG_ID FLT_SEG_ID, "
				+ " fSeg.FLIGHT_ID, fSeg.SEGMENT_CODE, fSeg.EST_TIME_DEPARTURE_LOCAL ,fSeg.EST_TIME_DEPARTURE_ZULU , flt.flight_type FROM T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg ,T_FLIGHT flt "
				+ (excludePrintExchangedStatus ? " ,t_pnr_pax_fare_segment ppfs, t_pnr_pax_fare_seg_e_ticket ppfset " : " ")
				+ " WHERE fSeg.FLT_SEG_ID IN (" + Util.buildIntegerInClauseContent(flightSegId)
				+ " )  AND rSeg.OPEN_RT_CONFIRM_BEFORE IS NULL AND rSeg.FLT_SEG_ID (+)= fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID (+)= flt.FLIGHT_ID AND rSeg.STATUS <> 'CNX' "
				+ (excludePrintExchangedStatus
						? " AND rseg.pnr_seg_id = ppfs.pnr_seg_id AND  ppfs.ppfs_id = ppfset.ppfs_id " + "AND ppfset.status <> '"
								+ EticketStatus.PRINT_EXCHANGED.code() + "' "
						: "")
				+ (pnr != null ? " AND rseg.pnr = '" + pnr + "'" : " ") + " ORDER BY rSeg.PNR ";

		Collection<FlightReconcileDTO> colFlightReconcileDTO = (Collection<FlightReconcileDTO>) jt.query(sql, new Object[] {},
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						FlightReconcileDTO flightReconcileDTO;
						Collection<FlightReconcileDTO> colFlightReconcileDTO = new ArrayList<FlightReconcileDTO>();

						if (rs != null) {
							while (rs.next()) {
								flightReconcileDTO = new FlightReconcileDTO();

								flightReconcileDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));
								flightReconcileDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID")));
								flightReconcileDTO.setFlightSegId(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
								flightReconcileDTO.setFlightId(BeanUtils.parseInteger(rs.getInt("FLIGHT_ID")));
								flightReconcileDTO.setSegementCode(BeanUtils.nullHandler(rs.getString("SEGMENT_CODE")));
								flightReconcileDTO.setDepartureDateTimeLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
								flightReconcileDTO.setFlightType(BeanUtils.nullHandler(rs.getString("FLIGHT_TYPE")));
								flightReconcileDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));

								colFlightReconcileDTO.add(flightReconcileDTO);
							}
						}

						return colFlightReconcileDTO;
					}
				});

		return colFlightReconcileDTO;
	}

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer getFlightSegmentId(String flightNumber, Date departureDate, String fromSegmentCode, String toSegmentCode)
			throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		dateFormat.format(departureDate);
		String strFlgSegmentSql = " SELECT s.FLT_SEG_ID FLT_SEG_ID, s.SEGMENT_CODE SEGMENT_CODE "
				+ " FROM T_FLIGHT_SEGMENT s, T_FLIGHT f WHERE s.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND f.FLIGHT_NUMBER = ? AND trunc(s.EST_TIME_DEPARTURE_LOCAL) = '" + dateFormat.format(departureDate) + "'"
				+ " AND UPPER(s.SEGMENT_CODE) LIKE ? " + " AND f.STATUS = '" + FlightStatusEnum.ACTIVE.getCode() + "' "
				+ " AND s.SEGMENT_VALID_FLAG = 'Y' ORDER BY s.SEGMENT_CODE ";

		String parsedSegmentCode;

		if (BeanUtils.nullHandler(toSegmentCode).equals("")) {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/???", null);
		} else {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/" + toSegmentCode, null);
		}

		log.debug("############################################");
		log.debug(" SQL to excute                         : " + strFlgSegmentSql);
		log.debug(" SQL parameter FlightNumber            : " + flightNumber);
		log.debug(" SQL parameter DepartureDate           : " + departureDate);
		log.debug(" SQL parameter DepartureAirport        : " + fromSegmentCode);
		log.debug(" SQL parameter ArrivalAirport          : " + toSegmentCode);
		log.debug(" SQL parameter ParsedDepartureAirport  : " + parsedSegmentCode);
		log.debug("############################################");

		Collection<Integer> colFlgSegIds = (Collection<Integer>) jt.query(strFlgSegmentSql,
				new Object[] { flightNumber, parsedSegmentCode }, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<Integer> colFlgSegIds = new ArrayList<Integer>();

						if (rs != null) {
							while (rs.next()) {
								colFlgSegIds.add(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
							}
						}

						return colFlgSegIds;
					}
				});

		Integer flightSegId = BeanUtils.getFirstElement(colFlgSegIds);

		if (flightSegId == null) {
			throw new CommonsDataAccessException("airreservations.arg.emptyRecFlightInfo");
		} else {
			return flightSegId;
		}
	}

	public Integer getFlightSegmentIdForSegmentTransfer(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		dateFormat.format(departureDate);
		String strFlgSegmentSql = " SELECT s.FLT_SEG_ID FLT_SEG_ID, s.SEGMENT_CODE SEGMENT_CODE "
				+ " FROM T_FLIGHT_SEGMENT s, T_FLIGHT f WHERE s.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND f.FLIGHT_NUMBER = ? AND trunc(s.EST_TIME_DEPARTURE_LOCAL) = '" + dateFormat.format(departureDate) + "'"
				+ " AND UPPER(s.SEGMENT_CODE) LIKE ? " + " AND f.STATUS = '" + FlightStatusEnum.ACTIVE.getCode() + "' "
				+ " AND s.SEGMENT_VALID_FLAG = 'Y' ORDER BY s.SEGMENT_CODE ";

		String parsedSegmentCode;

		if (BeanUtils.nullHandler(toSegmentCode).equals("")) {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/???", null);
		} else {
			parsedSegmentCode = BeanUtils.wildCardParser(fromSegmentCode + "*/" + toSegmentCode, null);
		}

		log.debug("############################################");
		log.debug(" SQL to excute                         : " + strFlgSegmentSql);
		log.debug(" SQL parameter FlightNumber            : " + flightNumber);
		log.debug(" SQL parameter DepartureDate           : " + departureDate);
		log.debug(" SQL parameter DepartureAirport        : " + fromSegmentCode);
		log.debug(" SQL parameter ArrivalAirport          : " + toSegmentCode);
		log.debug(" SQL parameter ParsedDepartureAirport  : " + parsedSegmentCode);
		log.debug("############################################");

		Collection<Integer> colFlgSegIds = (Collection<Integer>) jt.query(strFlgSegmentSql,
				new Object[] { flightNumber, parsedSegmentCode }, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<Integer> colFlgSegIds = new ArrayList<Integer>();

						if (rs != null) {
							while (rs.next()) {
								colFlgSegIds.add(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
							}
						}

						return colFlgSegIds;
					}
				});

		Integer flightSegId = BeanUtils.getFirstElement(colFlgSegIds);

		if (flightSegId == null) {
			throw new CommonsDataAccessException("airreservations.arg.emptyRecFlightInfo");
		} else {
			return flightSegId;
		}
	}

	/**
	 * Returns the correct flight segment ids
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@Override
	public Collection<Integer> getFlightSegmentIds(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		dateFormat.format(departureDate);
		String strFlgSegmentSql = " SELECT s.FLT_SEG_ID FLT_SEG_ID, s.SEGMENT_CODE SEGMENT_CODE "
				+ " FROM T_FLIGHT_SEGMENT s, T_FLIGHT f WHERE s.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND f.FLIGHT_NUMBER = ? AND trunc(s.EST_TIME_DEPARTURE_LOCAL) = '" + dateFormat.format(departureDate) + "'"
				+ " AND UPPER(s.SEGMENT_CODE) LIKE ? " + " AND f.STATUS = '" + FlightStatusEnum.ACTIVE.getCode() + "' "
				+ " AND s.SEGMENT_VALID_FLAG = 'Y' ORDER BY s.SEGMENT_CODE ";

		String parsedSegmentCode;

		if (BeanUtils.nullHandler(toSegmentCode).equals("")) {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/???", null);
		} else {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/" + toSegmentCode, null);
		}

		log.debug("############################################");
		log.debug(" SQL to excute                         : " + strFlgSegmentSql);
		log.debug(" SQL parameter FlightNumber            : " + flightNumber);
		log.debug(" SQL parameter DepartureDate           : " + departureDate);
		log.debug(" SQL parameter DepartureAirport        : " + fromSegmentCode);
		log.debug(" SQL parameter ArrivalAirport          : " + toSegmentCode);
		log.debug(" SQL parameter ParsedDepartureAirport  : " + parsedSegmentCode);
		log.debug("############################################");

		@SuppressWarnings("unchecked")
		Collection<Integer> colFlgSegIds = (Collection<Integer>) jt.query(strFlgSegmentSql,
				new Object[] { flightNumber, parsedSegmentCode }, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<Integer> colFlgSegIds = new ArrayList<Integer>();

						if (rs != null) {
							while (rs.next()) {
								colFlgSegIds.add(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
							}
						}

						return colFlgSegIds;
					}
				});

		// Integer flightSegId = (Integer) BeanUtils.getFirstElement(colFlgSegIds);

		if (colFlgSegIds == null) {
			throw new CommonsDataAccessException("airreservations.arg.emptyRecFlightInfo");
		} else {
			return colFlgSegIds;
		}
	}

	@Override
	public FlightReconcileDTO getFlightSegmentReconcileDTO(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws CommonsDataAccessException {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String formatedDate = dateFormat.format(departureDate);

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT f.FLIGHT_ID, ");
		sb.append("   fs.FLT_SEG_ID, ");
		sb.append("   fs.SEGMENT_CODE, ");
		sb.append("   fs.EST_TIME_DEPARTURE_LOCAL, ");
		sb.append("   fs.EST_TIME_DEPARTURE_ZULU, ");
		sb.append("   fs.EST_TIME_ARRIVAL_LOCAL, ");
		sb.append("   fs.EST_TIME_ARRIVAL_ZULU, ");
		sb.append("   f.FLIGHT_TYPE, ");
		sb.append("   f.FLIGHT_NUMBER ");
		sb.append(" FROM T_FLIGHT_SEGMENT fs, ");
		sb.append("   T_FLIGHT f ");
		sb.append(" WHERE fs.FLIGHT_ID                     = f.FLIGHT_ID ");
		sb.append(" AND f.FLIGHT_NUMBER                    = ? ");
		sb.append(" AND TRUNC(fs.EST_TIME_DEPARTURE_LOCAL) = '").append(formatedDate).append("' ");
		sb.append(" AND UPPER(fs.SEGMENT_CODE) LIKE '").append(fromSegmentCode).append("%").append(toSegmentCode).append("' ");
		sb.append(" AND f.STATUS   = '").append(FlightStatusEnum.ACTIVE.getCode()).append("' ");
		sb.append(" AND fs.SEGMENT_VALID_FLAG = 'Y' ");
		sb.append(" ORDER BY fs.SEGMENT_CODE ");

		@SuppressWarnings("unchecked")
		List<FlightReconcileDTO> flightReconcileDTOS = (List<FlightReconcileDTO>) jt.query(sb.toString(),
				new Object[] { flightNumber }, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<FlightReconcileDTO> flightReconcileDTOS = new ArrayList<FlightReconcileDTO>();

						if (rs != null) {
							while (rs.next()) {
								FlightReconcileDTO flightReconcileDTO = new FlightReconcileDTO();
								flightReconcileDTO.setFlightId(BeanUtils.parseInteger(rs.getInt("FLIGHT_ID")));
								flightReconcileDTO.setFlightSegId(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
								flightReconcileDTO.setSegementCode(BeanUtils.nullHandler(rs.getString("SEGMENT_CODE")));
								flightReconcileDTO.setDepartureDateTimeLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
								flightReconcileDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
								flightReconcileDTO.setArrivalDateTimeLocal(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
								flightReconcileDTO.setArrivalDateTimeZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
								flightReconcileDTO.setFlightType(BeanUtils.nullHandler(rs.getString("FLIGHT_TYPE")));
								flightReconcileDTO.setFlightNumber(BeanUtils.nullHandler(rs.getString("FLIGHT_NUMBER")));

								flightReconcileDTOS.add(flightReconcileDTO);
							}
						}

						return flightReconcileDTOS;
					}
				});

		if (flightReconcileDTOS == null || flightReconcileDTOS.isEmpty()) {
			throw new CommonsDataAccessException("airreservations.arg.emptyRecFlightInfo");
		} else if (flightReconcileDTOS.size() > 1) {
			throw new CommonsDataAccessException("airreservations.arg.twoActiveFlightsFound");
		}

		return flightReconcileDTOS.get(0);
	}

	/**
	 * Return pax fare segments for reconcilation
	 * 
	 * @param pnrSegIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<PnrPaxFareSegWisePaxTO> getReconcilePaxFareSegments(Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT DISTINCT seg.PPFS_ID, fare.PNR_PAX_ID, seg.BOOKING_CODE, pax.PAX_TYPE_CODE FROM T_PNR_PAX_FARE_SEGMENT seg, T_PNR_PAX_FARE fare, T_PNR_SEGMENT rSeg, T_PNR_PASSENGER pax "
				+ " WHERE seg.PNR_SEG_ID IN (" + Util.buildIntegerInClauseContent(pnrSegIds) + ") " + " AND seg.PAX_STATUS = '"
				+ ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED + "' " + " AND rSeg.STATUS = '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' "
				+ " AND seg.PPF_ID = fare.PPF_ID AND rSeg.PNR_SEG_ID = seg.PNR_SEG_ID AND pax.PNR_PAX_ID = fare.PNR_PAX_ID ";

		Collection<PnrPaxFareSegWisePaxTO> colPnrPaxFareSegWisePaxTO = (Collection<PnrPaxFareSegWisePaxTO>) jt.query(sql,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<PnrPaxFareSegWisePaxTO> colPnrPaxFareSegWisePaxTO = new ArrayList<PnrPaxFareSegWisePaxTO>();

						if (rs != null) {
							while (rs.next()) {
								PnrPaxFareSegWisePaxTO pnrPaxFareSegWisePaxTO = new PnrPaxFareSegWisePaxTO();
								pnrPaxFareSegWisePaxTO.setBookingCode(BeanUtils.nullHandler(rs.getString("BOOKING_CODE")));
								pnrPaxFareSegWisePaxTO.setPaxType(BeanUtils.nullHandler(rs.getString("PAX_TYPE_CODE")));
								pnrPaxFareSegWisePaxTO.setPpfId(rs.getInt("PPFS_ID"));
								pnrPaxFareSegWisePaxTO.setPnrPaxId(rs.getInt("PNR_PAX_ID"));

								colPnrPaxFareSegWisePaxTO.add(pnrPaxFareSegWisePaxTO);
							}
						}

						return colPnrPaxFareSegWisePaxTO;
					}
				});

		return colPnrPaxFareSegWisePaxTO;
	}

	/**
	 * Update passenger fare segments
	 * 
	 * @param colPpfsId
	 * @param status
	 */
	@Override
	public void updatePaxFareSegments(Collection<Integer> colPpfsId, String status) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " UPDATE T_PNR_PAX_FARE_SEGMENT SET PAX_STATUS = '" + status
				+ "' , VERSION = VERSION + 1 WHERE PPFS_ID IN ( select ppfs.ppfs_id from t_pnr_pax_fare_segment ppfs, t_pnr_pax_fare_seg_e_ticket ppfset where ppfset.ppfs_id = ppfs.ppfs_id and ppfset.status <> '"
				+ EticketStatus.PRINT_EXCHANGED.code() + "' and ppfs.ppfs_id IN (" + Util.buildIntegerInClauseContent(colPpfsId)
				+ ")) ";

		jt.execute(sql);
	}

	/**
	 * Return reservation segment information
	 * 
	 * @param colPnrSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationSegmentDTO> getSegmentInformation(Collection<Integer> colPnrSegmentIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String strCondition = "";
		if (colPnrSegmentIds != null && colPnrSegmentIds.size() != 0) {
			strCondition = " AND rSeg.PNR_SEG_ID in (" + Util.buildIntegerInClauseContent(colPnrSegmentIds) + ") ";
		}

		String sql = " SELECT rSeg.PNR_SEG_ID P_SEG_ID, rSeg.EXT_REF EXT_REF, rSeg.CS_FLIGHT_NUMBER, rSeg.CS_BOOKING_CLASS, fSeg.SEGMENT_CODE S_CODE,rSeg.pnr, "
				+ " fSeg.EST_TIME_DEPARTURE_LOCAL DTIME, fSeg.EST_TIME_ARRIVAL_LOCAL ATIME, "
				+ " fSeg.EST_TIME_DEPARTURE_ZULU ZULU_DTIME, fSeg.EST_TIME_ARRIVAL_ZULU ZULU_ATIME, "
				+ " fSeg.FLT_SEG_ID F_SEG_ID, f.FLIGHT_NUMBER F_NUMBER, f.FLIGHT_ID F_ID "
				+ " , rSeg.SUB_STATION_SHORTNAME,rSeg.GROUND_SEG_ID, rSeg.LAST_FQ_DATE, rSeg.TICKET_VALID_TILL "
				+ " FROM T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg, T_FLIGHT f "
				+ " WHERE rSeg.FLT_SEG_ID = fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID = f.FLIGHT_ID " + strCondition
				+ " ORDER BY rSeg.SEGMENT_SEQ ";

		log.debug("############################################");
		log.debug(" SQL to excute                  : " + sql);
		log.debug(" SQL parameters				   : " + colPnrSegmentIds);
		log.debug("############################################");

		Collection<ReservationSegmentDTO> segments = (Collection<ReservationSegmentDTO>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ReservationSegmentDTO reservationSegmentDTO;
				Collection<ReservationSegmentDTO> segments = new ArrayList<ReservationSegmentDTO>();

				if (rs != null) {
					while (rs.next()) {
						reservationSegmentDTO = new ReservationSegmentDTO();

						reservationSegmentDTO.setPnr(rs.getString("PNR"));
						reservationSegmentDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("P_SEG_ID")));
						reservationSegmentDTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("S_CODE")));
						reservationSegmentDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("F_NUMBER")));
						reservationSegmentDTO.setCodeShareFlightNo(rs.getString("CS_FLIGHT_NUMBER"));
						reservationSegmentDTO.setCodeShareBc(rs.getString("CS_BOOKING_CLASS"));
						reservationSegmentDTO.setDepartureDate(rs.getTimestamp("DTIME"));
						reservationSegmentDTO.setZuluDepartureDate(rs.getTimestamp("ZULU_DTIME"));
						reservationSegmentDTO.setArrivalDate(rs.getTimestamp("ATIME"));
						reservationSegmentDTO.setZuluArrivalDate(rs.getTimestamp("ZULU_ATIME"));
						reservationSegmentDTO.setFlightSegId(BeanUtils.parseInteger(rs.getInt("F_SEG_ID")));
						reservationSegmentDTO.setFlightId(BeanUtils.parseInteger(rs.getInt("F_ID")));
						reservationSegmentDTO
								.setSubStationShortName(BeanUtils.nullHandler(rs.getString("SUB_STATION_SHORTNAME")));
						reservationSegmentDTO.setGroundStationPnrSegmentID(
								rs.getBigDecimal("GROUND_SEG_ID") == null ? null : rs.getBigDecimal("GROUND_SEG_ID").intValue());
						reservationSegmentDTO.setExternalReference(rs.getString("EXT_REF"));
						reservationSegmentDTO.setLastFareQuoteDateZulu(rs.getTimestamp("LAST_FQ_DATE"));
						reservationSegmentDTO.setTicketValidTill(rs.getTimestamp("TICKET_VALID_TILL"));
						segments.add(reservationSegmentDTO);
					}
				}

				return segments;
			}
		});

		return segments;
	}

	/**
	 * Return reservation segment information
	 * 
	 * @param colFlightLegends
	 * @param executionDate
	 * @param startOverMins
	 * @param cutOverMins
	 * @param colSegStates
	 * @param colStates
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Collection<ReservationSegmentDTO>> getSegmentInformation(Collection<String> colFlightLegends,
			Date executionDate, int startOverMins, int cutOverMins, Collection<String> colSegStates,
			Collection<String> colStates) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT rSeg.PNR PNR, rSeg.PNR_SEG_ID P_SEG_ID, rSeg.EXT_REF EXT_REF, fSeg.SEGMENT_CODE S_CODE, "
				+ " rSeg.SEGMENT_SEQ SEG_SEQ, rSeg.RETURN_FLAG RFLAG, rSeg.STATUS STATUS,rSeg.SUB_STATUS SUB_STATUS, rSeg.OND_GROUP_ID GROUP_ID, "
				+ " rSeg.ALERT_FLAG ALERT_FLAG, fSeg.EST_TIME_DEPARTURE_LOCAL DTIME, fSeg.EST_TIME_ARRIVAL_LOCAL ATIME, "
				+ " fSeg.EST_TIME_DEPARTURE_ZULU ZULU_DTIME, fSeg.EST_TIME_ARRIVAL_ZULU ZULU_ATIME, rSeg.CS_FLIGHT_NUMBER, rSeg.CS_BOOKING_CLASS, "
				+ " fSeg.FLT_SEG_ID F_SEG_ID, f.FLIGHT_NUMBER F_NUMBER, f.FLIGHT_ID F_ID, st.TRANSFER_PNR "
				+ " , rSeg.SUB_STATION_SHORTNAME, rSeg.GROUND_SEG_ID, rSeg.LAST_FQ_DATE, rSeg.TICKET_VALID_TILL "
				+ " FROM T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg, T_FLIGHT f, T_PNR_SEGMENT_TRANSFER st "
				+ " WHERE rSeg.FLT_SEG_ID = fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND rSeg.PNR_SEG_ID = st.PNR_SEG_ID(+) " + " AND (st.STATUS IS NULL OR st.STATUS IN ("
				+ Util.buildStringInClauseContent(colStates) + ")) " + " AND ? BETWEEN fSeg.EST_TIME_DEPARTURE_ZULU - ("
				+ startOverMins + "/1440) " + " AND fSeg.EST_TIME_DEPARTURE_ZULU - (" + cutOverMins + "/1440) "
				+ " AND rSeg.STATUS IN (" + Util.buildStringInClauseContent(colSegStates) + " ) "
				+ " AND SUBSTR(f.FLIGHT_NUMBER, 1, 2) IN (" + Util.buildStringInClauseContent(colFlightLegends) + " ) "
				+ " ORDER BY rSeg.PNR, rSeg.SEGMENT_SEQ, rSeg.PNR_SEG_ID ";

		log.debug("############################################");
		log.debug(" SQL to execute  : " + sql);
		log.debug(" SQL parameters	: Flight Legends " + colFlightLegends);
		log.debug(" SQL parameters	: Execution Date " + executionDate);
		log.debug(" SQL parameters	: Segment States " + colSegStates);
		log.debug(" SQL parameters	: States " + colStates);
		log.debug("############################################");

		Collection<Date> values = new ArrayList<Date>();
		values.add(BeanUtils.getTimestamp(executionDate));

		Map<String, Collection<ReservationSegmentDTO>> pnrAndsegments = (Map<String, Collection<ReservationSegmentDTO>>) jt
				.query(sql, values.toArray(), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<ReservationSegmentDTO>> pnrAndsegments = new HashMap<String, Collection<ReservationSegmentDTO>>();
						ReservationSegmentDTO reservationSegmentDTO;
						Collection<ReservationSegmentDTO> colReservationSegmentDTO;
						String pnr;

						if (rs != null) {
							while (rs.next()) {
								pnr = BeanUtils.nullHandler(rs.getString("PNR"));

								reservationSegmentDTO = new ReservationSegmentDTO();

								reservationSegmentDTO.setPnr(pnr);
								reservationSegmentDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("P_SEG_ID")));
								reservationSegmentDTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("S_CODE")));
								reservationSegmentDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("F_NUMBER")));
								reservationSegmentDTO.setCodeShareFlightNo(rs.getString("CS_FLIGHT_NUMBER"));
								reservationSegmentDTO.setCodeShareBc(rs.getString("CS_BOOKING_CLASS"));
								reservationSegmentDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("F_NUMBER")));
								reservationSegmentDTO.setDepartureDate(rs.getTimestamp("DTIME"));
								reservationSegmentDTO.setZuluDepartureDate(rs.getTimestamp("ZULU_DTIME"));
								reservationSegmentDTO.setArrivalDate(rs.getTimestamp("ATIME"));
								reservationSegmentDTO.setZuluArrivalDate(rs.getTimestamp("ZULU_ATIME"));
								reservationSegmentDTO.setFlightSegId(BeanUtils.parseInteger(rs.getInt("F_SEG_ID")));
								reservationSegmentDTO.setFlightId(BeanUtils.parseInteger(rs.getInt("F_ID")));
								reservationSegmentDTO.setSegmentSeq(BeanUtils.parseInteger(rs.getInt("SEG_SEQ")));
								reservationSegmentDTO.setReturnFlag(BeanUtils.nullHandler(rs.getString("RFLAG")));
								reservationSegmentDTO.setStatus(BeanUtils.nullHandler(rs.getString("STATUS")));
								reservationSegmentDTO.setSubStatus(BeanUtils.nullHandler(rs.getString("SUB_STATUS")));
								reservationSegmentDTO.setFareGroupId(BeanUtils.parseInteger(rs.getInt("GROUP_ID")));
								reservationSegmentDTO.setAlertFlag(rs.getInt("ALERT_FLAG"));
								reservationSegmentDTO
										.setSubStationShortName(BeanUtils.nullHandler(rs.getString("SUB_STATION_SHORTNAME")));
								reservationSegmentDTO.setGroundStationPnrSegmentID(rs.getBigDecimal("GROUND_SEG_ID") == null
										? null
										: rs.getBigDecimal("GROUND_SEG_ID").intValue());
								reservationSegmentDTO.setExternalReference(rs.getString("EXT_REF"));
								reservationSegmentDTO.setLastFareQuoteDateZulu(rs.getTimestamp("LAST_FQ_DATE"));
								reservationSegmentDTO.setTicketValidTill(rs.getTimestamp("TICKET_VALID_TILL"));
								if (pnrAndsegments.containsKey(pnr)) {
									colReservationSegmentDTO = pnrAndsegments.get(pnr);
									colReservationSegmentDTO.add(reservationSegmentDTO);
								} else {
									colReservationSegmentDTO = new ArrayList<ReservationSegmentDTO>();
									colReservationSegmentDTO.add(reservationSegmentDTO);
									pnrAndsegments.put(pnr, colReservationSegmentDTO);
								}

							}
						}

						return pnrAndsegments;
					}
				});

		return pnrAndsegments;
	}

	/**
	 * Returns the reservation segments to Transfer
	 * 
	 * @param flightSegId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Reservation> getReservationSegmentsForTransfer(int flightSegId, boolean isInterline,
			Collection<String> colPnr) {
		String cond = "null";
		if (isInterline) {
			cond = "not null";
		}
		String hql = "SELECT r FROM Reservation r, ReservationSegment rs "
				+ " WHERE r.pnr = rs.reservation.pnr AND rs.flightSegId = ? " + " AND r.status NOT IN ('"
				+ ReservationInternalConstants.ReservationStatus.CANCEL + "') " + " AND rs.status = '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' AND r.originatorPnr is " + cond;

		if (colPnr != null && colPnr.size() > 0) {
			hql += " AND " + Util.getReplaceStringForIN("r.pnr", colPnr);

		}

		Object[] params = { new Integer(flightSegId) };
		Collection<Reservation> reservationList = find(hql, params, Reservation.class);

		// Loading the Passenger Fare
		if (reservationList != null) {
			Iterator<Reservation> itReservations = reservationList.iterator();
			while (itReservations.hasNext()) {
				Reservation res = itReservations.next();
				Iterator<ReservationPax> itReservationPax = res.getPassengers().iterator();
				ReservationPax reservationPax;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}
		}
		return reservationList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Reservation> getReservationsForTransfer(int flightSegId, boolean isInterline, String logicalCabinClass,
			Collection<String> colPnr) {
		String sql = "SELECT r.pnr FROM t_pnr_segment ps , t_pnr_pax_fare_segment ppfs, t_reservation r , t_booking_class bc "
				+ "WHERE ps.pnr_seg_id = ppfs.pnr_seg_id AND r.pnr = ps.pnr AND ps.flt_seg_id = " + flightSegId
				+ " AND bc.booking_code = ppfs.booking_code AND r.status <> '"
				+ ReservationInternalConstants.ReservationStatus.CANCEL + "' AND ps.status = '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
				+ "' AND ps.open_rt_confirm_before IS NULL AND r.originator_pnr is " + (isInterline ? "not" : "")
				+ " null AND bc.logical_cabin_class_code='" + logicalCabinClass + "' AND bc.bc_type <> '"
				+ ReservationInternalConstants.ReservationStatus.STAND_BY + "'";

		if (colPnr != null && colPnr.size() > 0) {
			sql += " AND r.pnr in (" + Util.buildStringInClauseContent(colPnr) + ") ";
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Collection<String> pnrs = (Collection<String>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				Collection<String> pnrs = new ArrayList<String>();
				while (resultSet.next()) {
					pnrs.add(resultSet.getString("PNR"));
				}
				return pnrs;
			}
		});
		return getReservationsForTransfer(pnrs);
	}

	public Collection<Reservation> getReservationsForTransfer(Collection<String> pnrs) {
		String hql = "SELECT r FROM Reservation r where " + Util.getReplaceStringForIN("r.pnr", pnrs);
		Collection<Reservation> reservations = find(hql, Reservation.class);
		// Loading the Passenger Fare
		if (reservations != null) {
			for (Reservation reservation : reservations) {
				reservation.getPassengers().isEmpty();
				for (ReservationPax reservationPax : reservation.getPassengers()) {
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}
		}
		return reservations;
	}

	/**
	 * Returns segment codes for flight segment ids
	 * 
	 * @param colFlgSegIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, String> getSegmentCodes(Collection<Integer> colFlgSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = "SELECT FLT_SEG_ID, SEGMENT_CODE FROM T_FLIGHT_SEGMENT " + " WHERE FLT_SEG_ID IN ("
				+ Util.buildIntegerInClauseContent(colFlgSegIds) + ")";
		Map<Integer, String> mapFlgSegIdAndSegCode = (Map<Integer, String>) jt.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> mapFlgSegIdAndSegCode = new HashMap<Integer, String>();
				if (rs != null) {
					while (rs.next()) {
						mapFlgSegIdAndSegCode.put(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")),
								BeanUtils.nullHandler(rs.getString("SEGMENT_CODE")));
					}
				}
				return mapFlgSegIdAndSegCode;
			}
		});
		return mapFlgSegIdAndSegCode;
	}

	/**
	 * Saves reservation segment transfer
	 * 
	 * @param colReservationSegmentTransfer
	 */
	@Override
	public void saveReservationSegmentsTransfer(Collection<ReservationSegmentTransfer> colReservationSegmentTransfer) {
		log.debug("Inside saveReservationSegmentsTransfer");

		// Saves reservation segments transfer
		hibernateSaveOrUpdateAll(colReservationSegmentTransfer);

		log.debug("Exit saveReservationSegmentsTransfer");
	}

	/**
	 * Returns the reservation segment transfer object by the pnr segment id
	 * 
	 * @param pnrSegId
	 * @return
	 */
	@Override
	public ReservationSegmentTransfer getReservationSegmentTransferByPnrSegId(int pnrSegId) throws CommonsDataAccessException {
		log.debug("Inside getReservationSegmentTransferByPnrSegId");

		String hql = "SELECT rst FROM ReservationSegmentTransfer AS rst " + "WHERE rst.pnrSegId = ? ";

		Object[] params = { pnrSegId };
		Collection<ReservationSegmentTransfer> colReservationSegmentTransfer = find(hql, params,
				ReservationSegmentTransfer.class);
		ReservationSegmentTransfer reservationSegmentTransfer = null;

		if (colReservationSegmentTransfer != null && colReservationSegmentTransfer.size() > 0) {
			if (colReservationSegmentTransfer.size() == 1) {
				reservationSegmentTransfer = BeanUtils.getFirstElement(colReservationSegmentTransfer);
			} else {
				throw new CommonsDataAccessException("airreservations.arg.duplicatePnrSegIdsForPnrSegTransfer");
			}
		}

		log.debug("Exit getReservationSegmentTransferByPnrSegId");
		return reservationSegmentTransfer;
	}

	/**
	 * Returns the transfer segments
	 * 
	 * @param colPnrSegIds
	 * @return
	 */
	@Override
	public Map<Integer, ReservationSegmentTransfer> getReservationTransferSegments(Collection<Integer> colPnrSegIds) {
		log.debug("Inside getReservationTransferSegments");

		String hql = "SELECT rst FROM ReservationSegmentTransfer AS rst " + "WHERE rst.pnrSegId IN ("
				+ Util.buildIntegerInClauseContent(colPnrSegIds) + ") ";

		Collection<ReservationSegmentTransfer> colReservationSegmentTransfer = find(hql, ReservationSegmentTransfer.class);
		ReservationSegmentTransfer reservationSegmentTransfer;
		Map<Integer, ReservationSegmentTransfer> pnrSegAndExtSegments = new HashMap<Integer, ReservationSegmentTransfer>();

		for (ReservationSegmentTransfer reservationSegmentTransfer2 : colReservationSegmentTransfer) {
			reservationSegmentTransfer = reservationSegmentTransfer2;
			pnrSegAndExtSegments.put(reservationSegmentTransfer.getPnrSegId(), reservationSegmentTransfer);
		}

		log.debug("Exit getReservationTransferSegments");
		return pnrSegAndExtSegments;
	}

	/**
	 * Returns the reservtion transfer segments
	 * 
	 * @param states
	 * @param executionDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationSegmentTransferDTO> getReservationTransferSegments(Collection<String> states,
			Date executionDate) {
		log.debug("Inside getReservationTransferSegments");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT st.CARRIER_CODE, st.PROCESS_DESCRIPTION, st.STATUS, st.EXECUTED_DATE, rSeg.PNR PNR, "
				+ " rSeg.PNR_SEG_ID P_SEG_ID, fSeg.SEGMENT_CODE S_CODE, rSeg.SEGMENT_SEQ SEG_SEQ, rSeg.RETURN_FLAG RFLAG, "
				+ " rSeg.STATUS STATUS, rSeg.OND_GROUP_ID GROUP_ID, rSeg.ALERT_FLAG ALERT_FLAG, fSeg.EST_TIME_DEPARTURE_LOCAL DTIME, "
				+ " fSeg.EST_TIME_ARRIVAL_LOCAL ATIME, fSeg.EST_TIME_DEPARTURE_ZULU ZULU_DTIME, fSeg.EST_TIME_ARRIVAL_ZULU ZULU_ATIME,"
				+ " fSeg.FLT_SEG_ID F_SEG_ID, f.FLIGHT_NUMBER F_NUMBER, f.FLIGHT_ID F_ID, st.TRANSFER_PNR "
				+ " FROM T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg, T_FLIGHT f, T_PNR_SEGMENT_TRANSFER st "
				+ " WHERE rSeg.FLT_SEG_ID = fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND rSeg.PNR_SEG_ID = st.PNR_SEG_ID AND st.status IN (" + Util.buildStringInClauseContent(states) + ") "
				+ " AND st.executed_date <= ?  ORDER BY st.CARRIER_CODE, st.STATUS, st.EXECUTED_DATE ";

		Collection<Date> values = new ArrayList<Date>();
		values.add(BeanUtils.getTimestamp(executionDate));

		log.debug("############################################");
		log.debug(" SQL to execute  : " + sql);
		log.debug(" SQL parameters	: Execution Date " + executionDate);
		log.debug(" SQL parameters	: States " + states);
		log.debug("############################################");

		Collection<ReservationSegmentTransferDTO> segments = (Collection<ReservationSegmentTransferDTO>) jt.query(sql,
				values.toArray(), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<ReservationSegmentTransferDTO> colReservationSegmentTransferDTO = new ArrayList<ReservationSegmentTransferDTO>();
						ReservationSegmentTransferDTO reservationSegmentTransferDTO;

						if (rs != null) {
							while (rs.next()) {

								reservationSegmentTransferDTO = new ReservationSegmentTransferDTO();

								reservationSegmentTransferDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));
								reservationSegmentTransferDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("P_SEG_ID")));
								reservationSegmentTransferDTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("S_CODE")));

								reservationSegmentTransferDTO.setDepartureDate(rs.getTimestamp("DTIME"));
								reservationSegmentTransferDTO.setZuluDepartureDate(rs.getTimestamp("ZULU_DTIME"));
								reservationSegmentTransferDTO.setArrivalDate(rs.getTimestamp("ATIME"));
								reservationSegmentTransferDTO.setZuluArrivalDate(rs.getTimestamp("ZULU_ATIME"));
								reservationSegmentTransferDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("F_NUMBER")));
								reservationSegmentTransferDTO.setFareGroupId(BeanUtils.parseInteger(rs.getInt("GROUP_ID")));
								reservationSegmentTransferDTO.setSegmentSeq(BeanUtils.parseInteger(rs.getInt("SEG_SEQ")));
								reservationSegmentTransferDTO.setCarrierCode(BeanUtils.nullHandler(rs.getString("CARRIER_CODE")));
								reservationSegmentTransferDTO.setTransferStatus(BeanUtils.nullHandler(rs.getString("STATUS")));
								reservationSegmentTransferDTO.setExecutedDate(rs.getTimestamp("EXECUTED_DATE"));
								reservationSegmentTransferDTO
										.setProcessDesc(BeanUtils.nullHandler(rs.getString("PROCESS_DESCRIPTION")));

								colReservationSegmentTransferDTO.add(reservationSegmentTransferDTO);
							}
						}

						return colReservationSegmentTransferDTO;
					}
				});

		log.debug("Exit getReservationTransferSegments");
		return segments;
	}

	/**
	 * Returns open return segments
	 * 
	 * @param executionDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<OpenReturnOndTO> getOpenReturnSegments(Date executionDate) {
		log.debug("Inside getOpenReturnSegments");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT DISTINCT a.PNR, a.OND_GROUP_ID FROM T_PNR_SEGMENT a, T_PNR_PAX_FARE_SEGMENT b, T_BOOKING_CLASS c, "
				+ " T_FLIGHT_SEGMENT d WHERE a.PNR_SEG_ID = b.PNR_SEG_ID and a.FLT_SEG_ID = d.FLT_SEG_ID "
				+ " AND b.BOOKING_CODE = c.BOOKING_CODE " + " AND a.STATUS = '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' AND a.RETURN_FLAG = '"
				+ ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE + "' AND c.BC_TYPE = '"
				+ BookingClass.BookingClassType.OPEN_RETURN
				+ "' AND (a.OPEN_RT_CONFIRM_BEFORE IS NULL OR a.OPEN_RT_CONFIRM_BEFORE <= ?) ORDER BY a.PNR ";

		Collection<Date> values = new ArrayList<Date>();
		values.add(BeanUtils.getTimestamp(executionDate));

		log.debug("############################################");
		log.debug(" SQL to execute  : " + sql);
		log.debug(" SQL parameters	: Execution Date " + executionDate);
		log.debug("############################################");

		Collection<OpenReturnOndTO> colOpenReturnOndTOs = (Collection<OpenReturnOndTO>) jt.query(sql, values.toArray(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<OpenReturnOndTO> colOpenReturnOndTOs = new HashSet<OpenReturnOndTO>();
						OpenReturnOndTO openReturnOndTO;

						if (rs != null) {
							while (rs.next()) {
								openReturnOndTO = new OpenReturnOndTO();
								openReturnOndTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR")));
								openReturnOndTO.setOndGroupId(rs.getInt("OND_GROUP_ID"));

								colOpenReturnOndTOs.add(openReturnOndTO);
							}
						}

						return colOpenReturnOndTOs;
					}
				});

		log.debug("Exit getOpenReturnSegments");
		return colOpenReturnOndTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Collection<ReservationExternalSegmentTO>> getExternalSegmentInformation(Collection<String> pnrs) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT EXTSEG.EXT_PNR_SEG_ID, " + "  EXTSEG.PNR, " + "  EXTSEG.SEGMENT_SEQ, " + "  EXTFLT.FLIGHT_NUMBER, "
				+ "  EXTFLT.SEGMENT_CODE, " + "  EXTFLT.EST_TIME_DEPARTURE_LOCAL, " + "  EXTFLT.EST_TIME_ARRIVAL_LOCAL, "
				+ "  EXTSEG.CABIN_CLASS_CODE, " + "  EXTSEG.LOGICAL_CABIN_CLASS_CODE, " + "  EXTSEG.STATUS, "
				+ "  EXTSEG.VERSION " + "FROM T_EXT_PNR_SEGMENT EXTSEG , " + "  T_EXT_FLIGHT_SEGMENT EXTFLT "
				+ "WHERE EXTSEG.EXT_FLT_SEG_ID = EXTFLT.EXT_FLT_SEG_ID " + "AND " + Util.getReplaceStringForIN("EXTSEG.PNR", pnrs)
				+ " " + "ORDER BY EXTSEG.PNR, " + "  EXTFLT.EST_TIME_DEPARTURE_LOCAL ";

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" SQL to execute  : " + sql);
			log.debug(" SQL parameters	: PNR(s) " + pnrs);
			log.debug("############################################");
		}

		Map<String, Collection<ReservationExternalSegmentTO>> pnrAndsegments = (Map<String, Collection<ReservationExternalSegmentTO>>) jt
				.query(sql, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, Collection<ReservationExternalSegmentTO>> pnrAndsegments = new HashMap<String, Collection<ReservationExternalSegmentTO>>();
						ReservationExternalSegmentTO reservationExternalSegmentTO;
						Collection<ReservationExternalSegmentTO> colReservationExternalSegmentTO;
						String pnr;

						if (rs != null) {
							while (rs.next()) {
								pnr = BeanUtils.nullHandler(rs.getString("PNR"));

								reservationExternalSegmentTO = new ReservationExternalSegmentTO();

								reservationExternalSegmentTO.setPnr(pnr);
								reservationExternalSegmentTO.setPnrExtSegId(BeanUtils.parseInteger(rs.getInt("EXT_PNR_SEG_ID")));
								reservationExternalSegmentTO.setSegmentSeq(BeanUtils.parseInteger(rs.getInt("SEGMENT_SEQ")));
								reservationExternalSegmentTO.setFlightNo(BeanUtils.nullHandler(rs.getString("FLIGHT_NUMBER")));
								reservationExternalSegmentTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("SEGMENT_CODE")));

								reservationExternalSegmentTO.setDepartureDate(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
								reservationExternalSegmentTO.setArrivalDate(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
								reservationExternalSegmentTO
										.setCabinClassCode(BeanUtils.nullHandler(rs.getString("CABIN_CLASS_CODE")));
								reservationExternalSegmentTO
										.setLogicalCCCode(BeanUtils.nullHandler(rs.getString("LOGICAL_CABIN_CLASS_CODE")));
								reservationExternalSegmentTO.setStatus(BeanUtils.nullHandler(rs.getString("STATUS")));

								if (pnrAndsegments.containsKey(pnr)) {
									colReservationExternalSegmentTO = pnrAndsegments.get(pnr);
									colReservationExternalSegmentTO.add(reservationExternalSegmentTO);
								} else {
									colReservationExternalSegmentTO = new ArrayList<ReservationExternalSegmentTO>();
									colReservationExternalSegmentTO.add(reservationExternalSegmentTO);
									pnrAndsegments.put(pnr, colReservationExternalSegmentTO);
								}
							}
						}

						return pnrAndsegments;
					}
				});

		return pnrAndsegments;
	}

	@Override
	public List<ExternalPnrSegment> getExternalSegmentInformationByPnr(String pnr) {
		String hql = "SELECT rst FROM ExternalPnrSegment AS rst WHERE rst.reservation.pnr = ?";

		List<ExternalPnrSegment> externalPnrSegments = find(hql, pnr, ExternalPnrSegment.class);
		if (externalPnrSegments != null && !externalPnrSegments.isEmpty()) {
			for (ExternalPnrSegment externalPnrSegment : externalPnrSegments) {
				ExternalFlightSegment externalFlightSegment = getExternalFlightSegment(externalPnrSegment.getExternalFltSegId());
				externalPnrSegment.setExternalFlightSegment(externalFlightSegment);
			}
		}
		return externalPnrSegments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExternalFlightSegment getExternalFlightSegment(String carrierCode, Integer extFlightSegId) {

		if (log.isDebugEnabled()) {
			log.debug("Inside getExternalFlightSegment");
		}

		String hql = "select es from ExternalFlightSegment as es where es.externalCarrierCode = ? and es.externalFlightSegRef = ? ";

		Object[] params = { carrierCode, extFlightSegId };
		Collection<ExternalFlightSegment> colReservationExternalSegment = find(hql, params, ExternalFlightSegment.class);
		ExternalFlightSegment reservationFlightSegment = null;

		if (colReservationExternalSegment != null && colReservationExternalSegment.size() > 0) {
			if (colReservationExternalSegment.size() == 1) {
				reservationFlightSegment = BeanUtils.getFirstElement(colReservationExternalSegment);
				return reservationFlightSegment;
			} else {
				throw new CommonsDataAccessException("airreservations.arg.reservationFlightSegment.invalide");
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Exit getExternalFlightSegment");
		}

		return null;
	}

	@Override
	public ExternalFlightSegment getExternalFlightSegment(Integer externalFlightSegId) {

		log.debug("Inside getExternalFlightSegment");

		// Returns reservation segment
		ExternalFlightSegment reservationExternalSegment = (ExternalFlightSegment) get(ExternalFlightSegment.class,
				externalFlightSegId);

		if (reservationExternalSegment == null) {
			throw new CommonsDataAccessException("airreservations.arg.externalFlightSegId.does.not.exist");
		}

		log.debug("Exit getExternalFlightSegment");
		return reservationExternalSegment;

	}

	@Override
	public ExternalFlightSegment saveExternalFlightSegment(ExternalFlightSegment externalFlightSegment) {
		log.debug("Inside saveExternalFlightSegment");
		// Saves externalFlightSegment
		hibernateSaveOrUpdate(externalFlightSegment);
		log.debug("Exit saveExternalFlightSegment");
		return externalFlightSegment;
	}

	@Override
	public void updatePaxFareSegmentsForCheckin(Collection<Integer> colPpfsId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String flownSql = " UPDATE T_PNR_PAX_FARE_SEGMENT SET PAX_STATUS = '"
				+ ReservationInternalConstants.PaxFareSegmentTypes.FLOWN + "' , VERSION = VERSION + 1 WHERE PPFS_ID IN ("
				+ Util.buildIntegerInClauseContent(colPpfsId) + ") and CHECKIN_STATUS = '"
				+ ReservationInternalConstants.PassengerCheckinStatus.CHECKED_IN + "'";
		jt.execute(flownSql);

		String noshowSql = " UPDATE T_PNR_PAX_FARE_SEGMENT SET PAX_STATUS = '"
				+ ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE + "' , VERSION = VERSION + 1 WHERE PPFS_ID IN ("
				+ Util.buildIntegerInClauseContent(colPpfsId) + ") and CHECKIN_STATUS = '"
				+ ReservationInternalConstants.PassengerCheckinStatus.TO_BE_CHECKED_IN + "'";
		jt.execute(noshowSql);
	}

	@Override
	public void updateStatusReservationSegmentNotifyEvent(ReservationSegmentNotificationEvent notificationEvent) {
		hibernateSaveOrUpdate(notificationEvent);
	}

	/**
	 * Returns Segment Eticket Information for lcc unique ids and transaction IDs
	 * 
	 * @param lccUniqueIDs
	 * @param txnIDs
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ResSegmentEticketInfoDTO> getEticketInfo(List<String> lccUniqueIDs, List<Integer> txnIDs) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM ((SELECT ");
		sb.append("  UNIQUE pp.pnr ,");
		sb.append("  pp.pnr_pax_id ,");
		sb.append("  pp.pax_sequence ,");
		sb.append("  fs.segment_code ,");
		sb.append("  ps.segment_seq seg_sequence ,");
		sb.append("  fs.est_time_departure_zulu ,");
		sb.append("  ps.status AS segment_status ,");
		sb.append("  ps.pnr_seg_id ,");
		sb.append("  pet.e_ticket_number ,");
		sb.append("  tnx.lcc_unique_txn_id ,");
		sb.append("  f.flight_number ,");
		sb.append("  tnx.txn_id");
		sb.append(" FROM");
		sb.append("  t_pnr_passenger pp ,");
		sb.append("  t_pnr_pax_fare ppf ,");
		sb.append("  t_pnr_pax_fare_segment ppfs ,");
		sb.append("  t_pnr_pax_ond_charges ppoc ,");
		sb.append("  t_pnr_pax_ond_payments ppop ,");
		sb.append("  t_pnr_segment ps ,");
		sb.append("  t_flight_segment fs ,");
		sb.append("  t_pnr_pax_fare_seg_e_ticket ppfst ,");
		sb.append("  t_pax_e_ticket pet ,");
		sb.append("  t_pax_transaction tnx ,");
		sb.append("  t_flight f");
		if (lccUniqueIDs != null && txnIDs == null) {
			sb.append(" WHERE " + Util.getReplaceStringForIN("tnx.lcc_unique_txn_id", lccUniqueIDs));
		} else if (lccUniqueIDs == null && txnIDs != null) {
			sb.append(" WHERE " + Util.getReplaceStringForIN("tnx.txn_id", txnIDs));
		} else if (lccUniqueIDs != null && txnIDs != null) {
			sb.append(" WHERE " + Util.getReplaceStringForIN("tnx.lcc_unique_txn_id", lccUniqueIDs) + " AND "
					+ Util.getReplaceStringForIN("tnx.txn_id", txnIDs));
		}
		sb.append(" AND tnx.pnr_pax_id        = pp.pnr_pax_id");
		sb.append(" AND tnx.SEG_MAPPED = 'N'");
		sb.append(" AND pp.pnr_pax_id         = ppf.pnr_pax_id");
		sb.append(" AND ppf.ppf_id            = ppoc.ppf_id");
		sb.append(" AND ppoc.pft_id           = ppop.pft_id");
		sb.append(" AND ppf.ppf_id            = ppfs.ppf_id");
		sb.append(" AND ppfs.pnr_seg_id       = ps.pnr_seg_id");
		sb.append(" AND ps.flt_seg_id         = fs.flt_seg_id");
		sb.append(" AND f.flight_id           = fs.flight_id");
		sb.append(" AND ppfs.ppfs_id          = ppfst.ppfs_id");
		sb.append(" AND ppfst.pax_e_ticket_id = pet.pax_e_ticket_id");
		// sb.append(" AND ppop.nominal_code = 36");
		sb.append(" AND ppfst.pnr_pax_fare_seg_e_ticket_id IN");
		sb.append("  (");
		sb.append("    SELECT");
		sb.append("      MIN(ppfst.pnr_pax_fare_seg_e_ticket_id)");
		sb.append("    FROM");
		sb.append("      t_pnr_pax_fare_seg_e_ticket ppfst");
		sb.append("    WHERE");
		sb.append("      ppfst.ppfs_id =ppfs.ppfs_id");
		sb.append("    GROUP BY");
		sb.append("      ppfst.ppfs_id");
		sb.append("  )");
//		sb.append(" ORDER BY");
//		sb.append("  pp.pnr ,");
//		sb.append("  pp.pnr_pax_id ");
		sb.append(" ) UNION ( ");
		sb.append(" SELECT");
		sb.append("  UNIQUE pp.pnr ,");
		sb.append("  pp.pnr_pax_id ,");
		sb.append("  pp.pax_sequence ,");
		sb.append("  fs.segment_code ,");
		sb.append("  ps.segment_seq seg_sequence ,");
		sb.append("  fs.est_time_departure_zulu ,");
		sb.append("  ps.status AS segment_status ,");
		sb.append("  ps.pnr_seg_id ,");
		sb.append("  pet.e_ticket_number ,");
		sb.append("  tnx.lcc_unique_txn_id ,");
		sb.append("  f.flight_number ,");
		sb.append("  tnx.txn_id");
		sb.append(" FROM");
		sb.append("  t_pnr_passenger pp ,");
		sb.append("  t_pnr_pax_fare ppf ,");
		sb.append("  t_pnr_pax_fare_segment ppfs ,");
		sb.append("  t_pnr_pax_ond_charges ppoc ,");
		sb.append("  t_pnr_pax_ond_payments ppop ,");
		sb.append("  t_pnr_segment ps ,");
		sb.append("  t_flight_segment fs ,");
		sb.append("  t_pnr_pax_fare_seg_e_ticket ppfst ,");
		sb.append("  t_pax_e_ticket pet ,");
		sb.append("  t_pax_transaction tnx ,");
		sb.append("  t_flight f,");
		sb.append("  T_PAX_TRNX_SEGMENT pts");
		if (lccUniqueIDs != null && txnIDs == null) {
			sb.append(" WHERE " + Util.getReplaceStringForIN("tnx.lcc_unique_txn_id", lccUniqueIDs));
		} else if (lccUniqueIDs == null && txnIDs != null) {
			sb.append(" WHERE " + Util.getReplaceStringForIN("tnx.txn_id", txnIDs));
		} else if (lccUniqueIDs != null && txnIDs != null) {
			sb.append(" WHERE " + Util.getReplaceStringForIN("tnx.lcc_unique_txn_id", lccUniqueIDs) + " AND "
					+ Util.getReplaceStringForIN("tnx.txn_id", txnIDs));
		}
		sb.append(" AND tnx.pnr_pax_id        = pp.pnr_pax_id");
		sb.append(" AND tnx.SEG_MAPPED        = 'Y'");
		sb.append(" AND pp.pnr_pax_id         = ppf.pnr_pax_id");
		sb.append(" AND ppf.ppf_id            = ppoc.ppf_id");
		sb.append(" AND ppoc.pft_id           = ppop.pft_id");
		sb.append(" AND ppf.ppf_id            = ppfs.ppf_id");
		sb.append(" AND ppfs.pnr_seg_id       = ps.pnr_seg_id");
		sb.append(" AND ppfs.PNR_SEG_ID       = pts.PNR_SEG_ID");
		sb.append(" AND pts.TXN_ID            = tnx.TXN_ID");
		sb.append(" AND pts.flt_seg_id        = fs.flt_seg_id");
		sb.append(" AND f.flight_id           = fs.flight_id");
		sb.append(" AND ppfs.ppfs_id          = ppfst.ppfs_id");
		sb.append(" AND ppfst.pax_e_ticket_id = pet.pax_e_ticket_id");
		// sb.append(" AND ppop.nominal_code = 36");
		sb.append(" AND ppfst.pnr_pax_fare_seg_e_ticket_id IN");
		sb.append("  (");
		sb.append("    SELECT");
		sb.append("      MIN(ppfst.pnr_pax_fare_seg_e_ticket_id)");
		sb.append("    FROM");
		sb.append("      t_pnr_pax_fare_seg_e_ticket ppfst");
		sb.append("    WHERE");
		sb.append("      ppfst.ppfs_id =ppfs.ppfs_id");
		sb.append("    GROUP BY");
		sb.append("      ppfst.ppfs_id");
		sb.append("  ))");
		sb.append(" ) ORDER BY");
		sb.append("  pnr ,");
		sb.append("  pnr_pax_id ");

		String sql = sb.toString();
		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" SQL to execute  : " + sql);
			log.debug("############################################");
		}

		List<ResSegmentEticketInfoDTO> eticketInfoDTOs = (List<ResSegmentEticketInfoDTO>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ResSegmentEticketInfoDTO> results = new ArrayList<ResSegmentEticketInfoDTO>();

				if (rs != null) {
					while (rs.next()) {
						ResSegmentEticketInfoDTO eticketInfoDTO = new ResSegmentEticketInfoDTO();

						eticketInfoDTO.setDepartureDateZulu(rs.getTimestamp("est_time_departure_zulu"));
						eticketInfoDTO.seteTicketNumber(rs.getLong("e_ticket_number"));
						eticketInfoDTO.setFlightNumber(rs.getString("flight_number"));
						eticketInfoDTO.setLccUniqueID(rs.getString("lcc_unique_txn_id"));
						eticketInfoDTO.setPaxSequence(rs.getInt("pax_sequence"));
						eticketInfoDTO.setPnr(rs.getString("pnr"));
						eticketInfoDTO.setSegmentCode(rs.getString("segment_code"));
						eticketInfoDTO.setSegmentSequence(rs.getInt("seg_sequence"));
						eticketInfoDTO.setSegmentStatus(rs.getString("segment_status"));
						eticketInfoDTO.setTransactionID(rs.getLong("txn_id"));

						results.add(eticketInfoDTO);
					}
				}

				return results;
			}
		});

		return eticketInfoDTOs;
	}

	/**
	 * Returns the correct flight segment id with respective matching segment code
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @param segmentCode
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Integer getFlightSegmentIdBySegmentCode(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode, String segmentCode) throws CommonsDataAccessException {
		final String segCode = segmentCode;

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

		String strFlgSegmentSql = " SELECT s.FLT_SEG_ID FLT_SEG_ID, s.SEGMENT_CODE SEGMENT_CODE "
				+ " FROM T_FLIGHT_SEGMENT s, T_FLIGHT f WHERE s.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND f.FLIGHT_NUMBER = ? AND s.EST_TIME_DEPARTURE_LOCAL = TO_DATE('" + dateFormat.format(departureDate)
				+ " ', 'DD-MON-YYYY HH24:MI')" + " AND UPPER(s.SEGMENT_CODE) LIKE ? " + " AND f.STATUS = '"
				+ FlightStatusEnum.ACTIVE.getCode() + "' " + " AND s.SEGMENT_VALID_FLAG = 'Y' ORDER BY s.SEGMENT_CODE ";

		String parsedSegmentCode;

		if (BeanUtils.nullHandler(toSegmentCode).equals("")) {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/???", null);
		} else {
			parsedSegmentCode = BeanUtils.wildCardParser("*" + fromSegmentCode + "*/" + toSegmentCode, null);
		}

		Collection<Integer> colFlgSegIds = (Collection<Integer>) jt.query(strFlgSegmentSql,
				new Object[] { flightNumber, parsedSegmentCode }, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<Integer> colFlgSegIds = new ArrayList<Integer>();

						if (rs != null) {
							while (rs.next()) {
								if (segCode.equals(rs.getString("SEGMENT_CODE"))) {
									colFlgSegIds.add(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
								}
							}
						}

						return colFlgSegIds;
					}
				});

		Integer flightSegId = BeanUtils.getFirstElement(colFlgSegIds);

		if (flightSegId == null) {
			throw new CommonsDataAccessException("airreservations.arg.emptyRecFlightInfo");
		} else {
			return flightSegId;
		}
	}

	@Override
	public Collection<Integer> getFlightSegmentIds(String pnr, boolean isCNFOnly) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PS.FLT_SEG_ID FLT_SEG_ID FROM  T_RESERVATION R, T_PNR_SEGMENT PS ");
		sql.append("WHERE R.PNR = '" + pnr + "' AND PS.PNR = R.PNR ");

		if (isCNFOnly) {
			sql.append("AND PS.STATUS='" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "'");
		}

		@SuppressWarnings("unchecked")
		Collection<Integer> colFlgSegIds = (Collection<Integer>) jt.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colFlgSegIds = new ArrayList<Integer>();

				if (rs != null) {
					while (rs.next()) {
						colFlgSegIds.add(BeanUtils.parseInteger(rs.getInt("FLT_SEG_ID")));
					}
				}

				return colFlgSegIds;
			}
		});

		return colFlgSegIds;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AutoCancellationSchDTO> getExpiredSegments(Collection<Integer> cancellationIds, String marketingCarrier) {

		boolean isDry = AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier) ? false : true;

		StringBuilder query = new StringBuilder();
		query.append(
				"SELECT r.pnr, ps.pnr_seg_id, ps.ond_group_id, r.version, ps.auto_cancellation_id FROM t_reservation r, t_pnr_segment ps WHERE ");
		query.append("r.pnr=ps.pnr AND r.status = ? AND ps.status= ? AND r.DUMMY_BOOKING = ? AND ");
		query.append(Util.getReplaceStringForIN("ps.auto_cancellation_id", cancellationIds));
		query.append(" AND R.ORIGIN_CARRIER_CODE = ? AND R.ORIGIN_CHANNEL_CODE ");
		if (isDry) {
			query.append("= ?");
		} else {
			query.append("<> ?");
		}

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), marketingCarrier,
				ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (List<AutoCancellationSchDTO>) jt.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AutoCancellationSchDTO> autoCnxDtos = new ArrayList<AutoCancellationSchDTO>();
				if (rs != null) {
					Map<ReservationLiteDTO, Map<Integer, List<Integer>>> pnrWiseOndGroupWiseSegments = new HashMap<ReservationLiteDTO, Map<Integer, List<Integer>>>();
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						long version = rs.getLong("version");
						int ondGroupId = rs.getInt("ond_group_id");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");
						ReservationLiteDTO resDTO = new ReservationLiteDTO(pnr, version, autoCancellationId);

						if (!pnrWiseOndGroupWiseSegments.containsKey(resDTO)) {
							pnrWiseOndGroupWiseSegments.put(resDTO, new HashMap<Integer, List<Integer>>());
						}

						Map<Integer, List<Integer>> ondGroupWiseSegs = pnrWiseOndGroupWiseSegments.get(resDTO);
						if (!ondGroupWiseSegs.containsKey(ondGroupId)) {
							ondGroupWiseSegs.put(ondGroupId, new ArrayList<Integer>());
						}

						ondGroupWiseSegs.get(ondGroupId).add(rs.getInt("pnr_seg_id"));
					}

					for (Entry<ReservationLiteDTO, Map<Integer, List<Integer>>> resSegEntry : pnrWiseOndGroupWiseSegments
							.entrySet()) {
						ReservationLiteDTO resDTO = resSegEntry.getKey();
						AutoCancellationSchDTO autoCancellationSchDTO = new AutoCancellationSchDTO();
						autoCancellationSchDTO.setPnr(resDTO.getPnr());
						autoCancellationSchDTO.setVersion(resDTO.getVersion());
						autoCancellationSchDTO.setAutoCancellationId(resDTO.getAutoCancellationId());
						autoCancellationSchDTO.setOndGroudWiseCancellingSegIds(resSegEntry.getValue());
						autoCnxDtos.add(autoCancellationSchDTO);
					}
				}

				return autoCnxDtos;
			}
		});
	}

	@Override
	public void updateBaggageOndGrpIds(Map<Integer, String> pnrSegIdWiseOndBaggageGrpId) {
		for (Entry<Integer, String> entry : pnrSegIdWiseOndBaggageGrpId.entrySet()) {
			String hqlUpdate = " UPDATE ReservationSegment SET baggageOndGroupId = " + entry.getValue();
			hqlUpdate += " WHERE pnrSegId = " + entry.getKey();

			getSession().createQuery(hqlUpdate).executeUpdate();
		}
	}

	public void saveReservationSegmentNotifications(List<ReservationSegmentNotification> notifications) {
		hibernateSaveOrUpdateAll(notifications);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccSegmentsInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, ps.auto_cancellation_id FROM t_reservation r, t_pnr_segment ps WHERE r.pnr=ps.pnr ");
		query.append("AND r.status = ? AND ps.status= ? AND r.ORIGIN_CHANNEL_CODE = ? AND r.ORIGIN_CARRIER_CODE = ? ");
		query.append("AND r.DUMMY_BOOKING = ? AND ps.auto_cancellation_id IN ("
				+ BeanUtils.constructINStringForInts(cancellationIds) + ") ");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED, ReservationInternalConstants.SalesChannel.LCC,
				marketingCarrier, String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) jt.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> map = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						map.put(pnr, autoCnxId);
					}
				}

				return map;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ReservationPaxDetailsDTO> getExistingModificationChargesDue(List<Integer> pnrSegIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT p.pnr_pax_id, p.pax_type_code, p.adult_id, SUM(ppf.total_mod) mod_charge ");
		query.append("FROM t_pnr_passenger p, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs ");
		query.append("WHERE p.pnr_pax_id= ppf.pnr_pax_id and ppfs.ppf_id= ppf.ppf_id and ppfs.pnr_seg_id in ("
				+ BeanUtils.constructINStringForInts(pnrSegIds) + ") GROUP BY p.pnr_pax_id, p.pax_type_code, p.adult_id");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (List<ReservationPaxDetailsDTO>) jt.query(query.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ReservationPaxDetailsDTO> paxList = new ArrayList<ReservationPaxDetailsDTO>();
				if (rs != null) {
					while (rs.next()) {
						ReservationPaxDetailsDTO resPax = new ReservationPaxDetailsDTO();
						resPax.setPnrPaxId(rs.getInt("pnr_pax_id"));
						resPax.setPaxType(rs.getString("pax_type_code"));
						resPax.setTotalChargeAmount(rs.getBigDecimal("mod_charge"));
						String strAdultId = BeanUtils.nullHandler(rs.getString("adult_id"));
						resPax.setAdultId(("".equals(strAdultId)) ? null : Integer.valueOf(strAdultId));
						paxList.add(resPax);
					}
				}
				return paxList;
			}

		});
	}

	@SuppressWarnings("unchecked")
	public List<PNRGOVSegmentDTO> getReservationSegmentsForPNR(String pnr, String carrierCode) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT carrier_code, ");
		query.append("flight_number, ");
		query.append("segment_code, ");
		query.append("departure_time_local, ");
		query.append("arrival_time_local, ");
		query.append("pnr_seg_id, ");
		query.append("ground_seg_id ");
		query.append(" FROM ");
		query.append("(SELECT ");
		query.append("'" + carrierCode + "' as carrier_code, ");
		query.append("f.flight_number             AS flight_number,");
		query.append("fs.segment_code             AS segment_code ,");
		query.append("fs.est_time_departure_local AS departure_time_local ,");
		query.append("fs.est_time_arrival_local   AS arrival_time_local, ");
		query.append("ps.pnr_seg_id               AS pnr_seg_id, ");
		query.append("ps.ground_seg_id            AS ground_seg_id ");
		query.append("FROM t_pnr_segment ps, t_flight_segment fs, t_flight f ");
		query.append("WHERE ps.flt_seg_id = fs.flt_seg_id ");
		query.append("AND ps.status       = 'CNF' ");
		query.append("AND f.flight_id     = fs.flight_id ");
		query.append("AND ps.pnr          = '" + pnr + "'");
		query.append(" UNION ");
		query.append("SELECT efs.external_carrier_code AS carries_code,");
		query.append("efs.flight_number              AS flight_number,");
		query.append("efs.segment_code               AS segment_code,");
		query.append("efs.est_time_departure_local   AS departure_time_local,");
		query.append("efs.est_time_arrival_local     AS arrival_time_local, ");
		query.append("eps.ext_pnr_seg_id             AS pnr_seg_id, ");
		query.append("null                           AS ground_seg_id ");
		query.append("FROM t_ext_pnr_segment eps, t_ext_flight_segment efs ");
		query.append("WHERE eps.ext_flt_seg_id = efs.ext_flt_seg_id ");
		query.append("AND eps.status           = 'CNF' ");
		query.append("AND eps.pnr              = '" + pnr + "')");
		query.append(" ORDER BY departure_time_local");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (List<PNRGOVSegmentDTO>) jt.query(query.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PNRGOVSegmentDTO> segmentDtoList = new ArrayList<PNRGOVSegmentDTO>();
				if (rs != null) {
					while (rs.next()) {
						PNRGOVSegmentDTO segmentDto = new PNRGOVSegmentDTO();
						segmentDto.setOperatingAirlineCode(rs.getString("carrier_code"));
						segmentDto.setSegmentCode(rs.getString("segment_code"));
						segmentDto.setDepartureDate(rs.getTimestamp("departure_time_local"));
						segmentDto.setArrivalDate(rs.getTimestamp("arrival_time_local"));
						segmentDto.setFlightNumber(rs.getString("flight_number"));
						segmentDto.setPnrSegId(rs.getInt("pnr_seg_id"));
						segmentDto.setGroundSegId(rs.getString("ground_seg_id"));
						segmentDtoList.add(segmentDto);
					}
				}
				return segmentDtoList;
			}

		});

	}

	@Override
	public String getParentPnr(Integer parentPPID) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PP.PNR FROM T_PFS_PARSED PP WHERE PP.PP_ID = " + parentPPID);

		@SuppressWarnings("unchecked")
		Collection<String> colPnrs = (Collection<String>) jt.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> pnrs = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {
						pnrs.add(rs.getString("PNR"));
					}
				}

				return pnrs;
			}
		});

		return BeanUtils.getFirstElement(colPnrs);
	}

	public List<Integer> getAffectedPnrs(Collection<Integer> flightSegmentIDs, String status) {

		if ((flightSegmentIDs != null && flightSegmentIDs.size() == 0 || flightSegmentIDs == null)) {
			return new ArrayList<Integer>();
		}

		String hqlSelect = "";

		if (status == null) {
			hqlSelect = "select rs.reservation.pnr from ReservationSegment rs ";

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				hqlSelect += " where rs.flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}
		} else {
			hqlSelect = "select rs.reservation.pnr from ReservationSegment rs " + " where rs.status = '" + status + "' ";

			if (flightSegmentIDs != null && flightSegmentIDs.size() > 0) {
				hqlSelect += " and rs.flightSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIDs) + ") ";
			}
		}

		List<Integer> colPnrSegments = find(hqlSelect, Integer.class);
		return colPnrSegments;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getSegmentBundledFarePeriodIds(String pnr) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT DISTINCT fs.segment_code, ps.bundled_fare_period_id ");
		queryBuilder.append("FROM t_pnr_segment ps, t_flight_segment fs ");
		queryBuilder.append("WHERE ps.flt_seg_id=fs.flt_seg_id AND ps.pnr=?");

		Object[] args = new Object[] { pnr };

		return (Map<String, Integer>) jt.query(queryBuilder.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> map = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String segmentCode = BeanUtils.nullHandler(rs.getString("segment_code"));
						String bundledFarePeriodStr = rs.getString("bundled_fare_period_id");
						Integer bundledFarePeriodId = bundledFarePeriodStr != null ? Integer.parseInt(bundledFarePeriodStr) : null;
						if ((!map.containsKey(segmentCode)) || (map.get(segmentCode) == null && bundledFarePeriodId != null)) {
							map.put(segmentCode, bundledFarePeriodId);
						}
					}
				}

				return map;
			}
		});
	}

	public Collection<Integer> getConnectedGroundSegmentPPFSID(Collection<Integer> colPpfsId) throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT PPFS_ID FROM T_PNR_PAX_FARE_SEGMENT WHERE PNR_SEG_ID IN (SELECT PS.GROUND_SEG_ID ");
		sql.append(" FROM T_PNR_SEGMENT PS, T_PNR_PAX_FARE_SEGMENT PPFS WHERE PS.PNR_SEG_ID = PPFS.PNR_SEG_ID ");
		sql.append(" AND PS.STATUS='" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' ");
		sql.append(" AND " + Util.getReplaceStringForIN("PPFS.PPFS_ID", colPpfsId));
		sql.append(" AND PS.GROUND_SEG_ID IS NOT NULL)");

		@SuppressWarnings("unchecked")
		Collection<Integer> colGroundSegPpfsId = (Collection<Integer>) jt.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colGroundSegPpfsIds = new ArrayList<Integer>();

				if (rs != null) {
					while (rs.next()) {
						colGroundSegPpfsIds.add(BeanUtils.parseInteger(rs.getInt("PPFS_ID")));
					}
				}

				return colGroundSegPpfsIds;
			}
		});

		return colGroundSegPpfsId;
	}

	@Override
	public Integer getReservationSegmentId(String pnr, String departureAirport, String arrivalAirport)
			throws CommonsDataAccessException {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT PS.PNR_SEG_ID ");
		sb.append("  FROM T_PNR_SEGMENT ps, ");
		sb.append("   T_FLIGHT_SEGMENT fs ");
		sb.append(" WHERE ps.pnr     ='").append(pnr).append("'");
		sb.append(" AND PS.STATUS = 'CNF' ");
		sb.append(" AND PS.FLT_SEG_ID=FS.FLT_SEG_ID ");
		sb.append(" AND FS.SEGMENT_CODE LIKE '").append(departureAirport).append("%").append(arrivalAirport).append("'");

		@SuppressWarnings("unchecked")
		Collection<Integer> pnrSegIds = (Collection<Integer>) jt.query(sb.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> pnrSegIds = new ArrayList<Integer>();

				if (rs != null) {
					while (rs.next()) {
						pnrSegIds.add(BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID")));
					}
				}

				return pnrSegIds;
			}
		});

		if (pnrSegIds == null || pnrSegIds.isEmpty()) {
			throw new CommonsDataAccessException("airreservation.pfs.cannot.locate.parent");
		}

		return BeanUtils.getFirstElement(pnrSegIds);

	}
	
	public Reservation loadReservationByPnrPaxId(Integer pnrPaxId) throws CommonsDataAccessException {
		String hql = "SELECT rp FROM ReservationPax AS rp JOIN FETCH rp.reservation JOIN FETCH rp.pnrPaxFares " + "WHERE rp.pnrPaxId = ? ";

		Object[] params = { pnrPaxId };
		Collection<ReservationPax> passengers = find(hql, params, ReservationPax.class);
		ReservationPax pax = BeanUtils.getFirstElement(passengers);
		return pax.getReservation();
	}
	
}