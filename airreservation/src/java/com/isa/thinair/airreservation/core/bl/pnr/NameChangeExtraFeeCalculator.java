package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameChangeExtChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class NameChangeExtraFeeCalculator extends BaseExtraFeeCalculator implements IExtraFeeCalculator {

	private Collection<OndFareDTO> colOndFareDTOs;
	private Map<String, String> requoteSegmentMap;
	private Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges;
	private Map<Integer, BigDecimal> paxWiseNameChangeChargeTotal = new HashMap<Integer, BigDecimal>();

	public NameChangeExtraFeeCalculator(Reservation reservation, EXTERNAL_CHARGES externalCharge,
			Collection<OndFareDTO> colOndFareDTOs, Map<String, String> requoteSegmentMap,
			Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges) {
		this.reservation = reservation;
		this.externalCharge = externalCharge;
		this.colOndFareDTOs = colOndFareDTOs;
		this.requoteSegmentMap = requoteSegmentMap;
		this.reprotectedExternalCharges = reprotectedExternalCharges;
	}

	@Override
	public void calculate() throws ModuleException {
		processNameChangeCharges();
		if (isExtraFeeApplicable(colOndFareDTOs, requoteSegmentMap)) {
			if (paxWiseNameChangeChargeTotal != null && !paxWiseNameChangeChargeTotal.isEmpty()) {
				for (Entry<Integer, BigDecimal> paxEntry : paxWiseNameChangeChargeTotal.entrySet()) {
					Integer pnrPaxId = paxEntry.getKey();
					BigDecimal paxNameChangeChargeTotal = paxEntry.getValue();

					if (AccelAeroCalculator.isGreaterThan(paxNameChangeChargeTotal,
							AccelAeroCalculator.getDefaultBigDecimalZero())) {
						BigDecimal taxAmount = AccelAeroCalculator.multiplyDefaultScale(paxNameChangeChargeTotal, getRatio());
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) getExternalChgDTO();
						serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) serviceTaxExtChgDTO.clone();
						serviceTaxExtChgDTO.setAmount(taxAmount);
						serviceTaxExtChgDTO.setFlightRefNumber(taxApplicableFlightRefNumber);
						addPaxFee(pnrPaxId, serviceTaxExtChgDTO);
					}
				}

			}

		}

	}

	private void processNameChangeCharges() {
		if (reprotectedExternalCharges != null && !reprotectedExternalCharges.isEmpty()) {
			for (Entry<Integer, Collection<ExternalChgDTO>> extChgEntry : reprotectedExternalCharges.entrySet()) {
				Integer pnrPaxId = extChgEntry.getKey();

				if (!paxWiseNameChangeChargeTotal.containsKey(pnrPaxId)) {
					paxWiseNameChangeChargeTotal.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
				}

				BigDecimal currentTotal = paxWiseNameChangeChargeTotal.get(pnrPaxId);

				Collection<ExternalChgDTO> paxExChgList = extChgEntry.getValue();
				if (paxExChgList != null && !paxExChgList.isEmpty()) {
					for (ExternalChgDTO externalChgDTO : paxExChgList) {
						BigDecimal NameChangeCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (externalChgDTO instanceof NameChangeExtChgDTO) {
							NameChangeExtChgDTO nameChange = (NameChangeExtChgDTO) externalChgDTO;
							NameChangeCharge = nameChange.getAmount();
						}
						currentTotal = AccelAeroCalculator.add(currentTotal, NameChangeCharge);
					}

					paxWiseNameChangeChargeTotal.put(pnrPaxId, currentTotal);
				}

			}
		}
	}

}
