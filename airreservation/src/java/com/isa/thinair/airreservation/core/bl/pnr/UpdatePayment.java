/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.PaymentGatewayBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.util.PaymentUtil;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelSegmentDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for updating payments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="updatePayment"
 */
public class UpdatePayment extends DefaultBaseCommand {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(UpdatePayment.class);

	/** Holds the travel agent delegate */
	private final TravelAgentFinanceBD travelAgentFinanceBD;

	private final PaymentBrokerBD paymentBrokerBD;

	/**
	 * Construct UpdatePayment
	 * 
	 */
	public UpdatePayment() {
		travelAgentFinanceBD = ReservationModuleUtils.getTravelAgentFinanceBD();
		paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();
	}

	/**
	 * Execute method of the UpdatePayment command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection colPaymentInfo = (Collection) this.getParameter(CommandParamNames.PAYMENT_INFO);
		Collection colTnxIds = (Collection) this.getParameter(CommandParamNames.TEMPORY_PAYMENT_IDS);
		ReservationContactInfo reservationContactInfo = (ReservationContactInfo) this
				.getParameter(CommandParamNames.RESERVATION_CONTACT_INFO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Boolean triggerExternalPayTxUpdate = (Boolean) this.getParameter(CommandParamNames.TRIGGER_EXTERNAL_PAY_TX_UPDATE);
		ExternalPaymentTnx externalPaymentInfo = (ExternalPaymentTnx) this.getParameter(CommandParamNames.EXTERNAL_PAY_TX_INFO);
		TrackInfoDTO trackInfo = (TrackInfoDTO) this.getParameter(CommandParamNames.RESERVATIN_TRACINFO);
		String reservationType = (String) this.getParameter(CommandParamNames.RESERVATION_TYPE);
		boolean isGoshowProcess = ((Boolean) this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS)).booleanValue();
		Boolean enableECFraudCheck = (Boolean) this.getParameter(CommandParamNames.CC_FRAUD_ENABLE);
		Boolean isCapturePayment = (Boolean) this.getParameter(CommandParamNames.IS_CAPTURE_PAYMENT);
		Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = new HashMap<Integer, Collection<TnxCreditPayment>>();
		Map<Integer, IPayment> passengerPaymentMap = (Map<Integer, IPayment>) this
				.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);

		if (passengerPaymentMap == null) {
			passengerPaymentMap = new HashMap<Integer, IPayment>();
		}

		this.validateParams(pnr, colPaymentInfo, reservationContactInfo, colTnxIds, credentialsDTO);

		LCCClientPnrModesDTO pnrModesDto = new LCCClientPnrModesDTO();
		pnrModesDto.setPnr(pnr);
		pnrModesDto.setLoadFares(true);
		pnrModesDto.setLoadSegView(true);
		pnrModesDto.setLoadSegViewFareCategoryTypes(true);
		Reservation reservation = null;

		// This has to be skipped in checkin/pfs processing flow as pnr is not saved at this stage.
		if (!isGoshowProcess) {
			try {
				reservation = ReservationProxy.getReservation(pnrModesDto);
			} catch (CommonsDataAccessException commonsDataAccessException) {
				// FIXME need to refactor this later. introducing this fix to make GOSHO flow work
				if (commonsDataAccessException.getExceptionCode().equals("airreservations.arg.noLongerExist")) {
					log.debug("#### Reservation not found. Im cropping this exception because of GOSHO flow");
				}
			}
		}

		this.validateBSPParams(pnr, colPaymentInfo, reservation);

		Collection colCardPaymentInfo = new ArrayList();
		Iterator itColPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfo;
		Integer agentSaleTxId = null;

		Collection<Integer> colAgentSaleTxId = new ArrayList<Integer>();
		String agentCode = null;
		boolean hasOnAccPayments = false;
		boolean hasCreditPayments = false;
		Integer paymentGatewayID = null;
		List<CardDetailConfigDTO> cardDetailConfigData = new ArrayList<CardDetailConfigDTO>();
		Collection<OfflinePaymentInfo> colOfflinePaymentInfo = new ArrayList<OfflinePaymentInfo>();
		
		if (isCapturePayment) {
			while (itColPaymentInfo.hasNext()) {
				paymentInfo = (PaymentInfo) itColPaymentInfo.next();

				if (paymentInfo.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					// Card Payment
					if (paymentInfo instanceof CardPaymentInfo) {
						cardPaymentInfo = (CardPaymentInfo) paymentInfo;

						// Collect the CardPaymentInfo objects for compress
						PaymentGatewayBO.cardPaymentInfoCompressor(cardPaymentInfo, colCardPaymentInfo);
					}
					// Passenger Credit
					else if (paymentInfo instanceof PaxCreditInfo) {
						// and marketing carrier pax credit for a normal own airline booking. For this case we may
						// getting initially
						// captured paxCreditpayments. We are merging them because those need to be recorded in pax
						// transactions.
						PaxCreditInfo paxCreditInfo = (PaxCreditInfo) paymentInfo;
						paxCreditInfo.getPaxCredit().setCreditUtilizingPnr(pnr);
						boolean enableTnxGranularity = ReservationModuleUtils.getReservationBD()
								.isReservationTnxGranularityEnable(paxCreditInfo.getPaxCredit().getPnr());
						DefaultServiceResponse response = (DefaultServiceResponse) ReservationModuleUtils.getCreditAccountBD()
								.carryForwardCredit(paxCreditInfo.getPaxCredit(), credentialsDTO, enableTnxGranularity);
						combineCreditPayments(paxCreditPayments,
								(Map<Integer, Collection<TnxCreditPayment>>) response
										.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS));
						// itColPaymentInfo.remove();
						// Charith : check we need to remove this
						hasCreditPayments = true;
					}
					// Agent Credit
					else if (paymentInfo instanceof AgentCreditInfo) {
						AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
						if (AppSysParamsUtil.isAeroMartPayEnabled() && trackInfo != null && trackInfo.getDirectBillId() != null) {
							agentCreditInfo.getPaymentReferenceTO().setPaymentRef(trackInfo.getDirectBillId());
						}
						// Calling travel agents to reduce necessary payment amounts
						AgentTransaction agentTransaction = travelAgentFinanceBD.recordSale(agentCreditInfo.getAgentCode(),
								agentCreditInfo.getTotalAmount(), pnr, pnr, agentCreditInfo.getPayCurrencyDTO(),
								agentCreditInfo.getPaymentReferenceTO(), null, null);
						agentSaleTxId = agentTransaction.getTnxId();
						colAgentSaleTxId.add(agentSaleTxId);
						agentCode = agentCreditInfo.getAgentCode();
						hasOnAccPayments = true;
					}
					// Cash Payment
					else if (paymentInfo instanceof CashPaymentInfo) {
						// Do nothing since it's just a payment
					} else if (paymentInfo instanceof OfflinePaymentInfo) {
						OfflinePaymentInfo offlinePaymentInfo = (OfflinePaymentInfo) paymentInfo;
						PaymentGatewayBO.getUniqueOfflinePaymentObject(offlinePaymentInfo, colOfflinePaymentInfo);
					}
				}
			} // end while
		}

		if (hasOnAccPayments) {
			PnrTransactionInvoice.createInvoices(pnr, agentCode, colAgentSaleTxId, null);
		}
		if (hasCreditPayments) {
			for (Integer pnrPaxId : passengerPaymentMap.keySet()) {
				PaymentAssembler paymentAssembler = (PaymentAssembler) passengerPaymentMap.get(pnrPaxId);
				boolean creditPaymentFound = false;
				Integer paxSequence = null;
				for (Iterator<PaymentInfo> iterator = paymentAssembler.getPayments().iterator(); iterator.hasNext();) {
					PaymentInfo paxPayInfo = iterator.next();
					if (paxPayInfo instanceof PaxCreditInfo
							&& paxPayInfo.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
						PaxCreditInfo paxCreditInfo = (PaxCreditInfo) paxPayInfo;
						creditPaymentFound = true;
						paxSequence = paxCreditInfo.getPaxCredit().getPaxSequence();
						paymentAssembler.setTotalPayAmount(AccelAeroCalculator.add(paymentAssembler.getTotalPayAmount(),
								paxCreditInfo.getTotalAmount().negate()));
						iterator.remove();
					}
				}
				if (creditPaymentFound) {
					for (TnxCreditPayment tnxCreditPayment : paxCreditPayments.get(paxSequence)) {
						passengerPaymentMap.get(pnrPaxId).addCreditPayment(tnxCreditPayment.getAmount(),
								tnxCreditPayment.getPnrPaxId(), tnxCreditPayment.getPaymentCarrier(),
								tnxCreditPayment.getPayCurrencyDTO(), tnxCreditPayment.getLccUniqueTnxId(), paxSequence,
								tnxCreditPayment.getExpiryDate(), tnxCreditPayment.getPnr(), tnxCreditPayment.getPaxCreditId(),
								null, null);
					}
				}
			}
		}

		if (reservation != null) {
			ReservationBO.processCCFraudCheck(pnr, null, colTnxIds, credentialsDTO, reservation, trackInfo, colCardPaymentInfo,
					enableECFraudCheck, reservationType);
			// Update Segments Ancillary Status
			ReservationCoreUtils.updateSegmentsAncillaryStatus(reservation);
			// Process card payments
			if (log.isDebugEnabled())
				log.debug("UpdatePayment Command:Updating Ancillary Status in T_PNR_SEGMENT (ALL/PARTIAL/NONE)");
			// Card Details configuration data
			paymentGatewayID = getPaymentGatewayID(colCardPaymentInfo);
			if (paymentGatewayID != null) {
				cardDetailConfigData = getPaymentGatewayConfig(paymentGatewayID);
			}
			this.processCardPayments(colCardPaymentInfo, colPaymentInfo, colTnxIds, credentialsDTO, cardDetailConfigData,
					createTravelDTO(reservation));
			this.processOfflinePayments(reservation, credentialsDTO, colOfflinePaymentInfo);
		}

		// Update external transaction details
		if (triggerExternalPayTxUpdate != null && triggerExternalPayTxUpdate.booleanValue()) {
			externalPaymentInfo.setInternalPayId(agentSaleTxId);
			processExternalPayTx(externalPaymentInfo, credentialsDTO);
		}

		if (!isGoshowProcess) {
		ReservationCoreUtils.updateGrpBookingPaymentStatus(reservation, passengerPaymentMap);
		}
		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.PAYMENT_CARD_CONFIG_DATA, cardDetailConfigData);

		log.debug("Exit execute");
		return response;
	}

	public static TravelDTO createTravelDTO(Reservation reservation) throws ModuleException {

		TravelDTO travel = new TravelDTO();
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

		if (reservation != null) {
			ReservationContactInfo contactInfo = reservation.getContactInfo();

			if (contactInfo != null) {
				if (contactInfo != null) {
					travel.setEmailAddress(contactInfo.getEmail());
					travel.setResidenceCountryCode(contactInfo.getCountryCode());
					String phone = contactInfo.getMobileNo();
					if (phone == null || phone.trim().isEmpty()) {
						if (contactInfo.getPhoneNo() != null && !contactInfo.getPhoneNo().isEmpty()) {
							phone = contactInfo.getPhoneNo();
						}
					}
					travel.setContactNumber(phone);
					travel.setContactName(BeanUtils.nullHandler(contactInfo.getFirstName()) + " "
							+ BeanUtils.nullHandler(contactInfo.getLastName()));
				}
			}

			Collection<ReservationSegmentDTO> reservationSegmentDTOs = reservation.getSegmentsView();
			if (reservationSegmentDTOs != null) {
				List<TravelSegmentDTO> segmentDTOs = new ArrayList<TravelSegmentDTO>();
				TravelSegmentDTO segmentDTO = null;
				String depSegmentCode = null;
				String arrivalSegmentCode = null;
				Date systeDate = Calendar.getInstance().getTime();
				Set<ReservationSegmentDTO> sortedTreeSet = new TreeSet<ReservationSegmentDTO>(reservationSegmentDTOs);
				for (ReservationSegmentDTO flightSegmentDTO : sortedTreeSet) {
					if (systeDate.compareTo(flightSegmentDTO.getZuluDepartureDate()) < 0) {
						segmentDTO = new TravelSegmentDTO();
						segmentDTO.setFlightNumber(Util.extractFlightNumber(flightSegmentDTO.getFlightNo()));
						segmentDTO.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightSegmentDTO.getFlightNo()));
						segmentDTO.setFlightDate(flightSegmentDTO.getDepartureDate());
						depSegmentCode = flightSegmentDTO.getOrigin();
						if (depSegmentCode == null || depSegmentCode.trim().isEmpty())
							depSegmentCode = Util.getDepartureAirportCode(flightSegmentDTO.getSegmentCode());
						segmentDTO.setDepartureAirportCode(depSegmentCode);
						segmentDTO.setDepartureAirportName((String) globalConfig.getAirportInfo(depSegmentCode, true)[0]);
						arrivalSegmentCode = flightSegmentDTO.getDestination();
						if (arrivalSegmentCode == null || arrivalSegmentCode.trim().isEmpty())
							arrivalSegmentCode = Util.getAirvalAirportCode(flightSegmentDTO.getSegmentCode());
						segmentDTO.setArrivalAirportCode(arrivalSegmentCode);
						segmentDTO.setArrivalAirportName((String) globalConfig.getAirportInfo(arrivalSegmentCode, true)[0]);
						segmentDTO.setNoOfStopvers(flightSegmentDTO.getSegmentCode().split("/").length - 2);
						segmentDTO.setClassOfService(flightSegmentDTO.getCabinClassCode());
						segmentDTOs.add(segmentDTO);
					}
				}
				travel.setTravelSegments(segmentDTOs);
			}
		}

		return travel;
	}

	/**
	 * Get Payment gateway ID
	 * 
	 * @param colCardPaymentInfo
	 * @return
	 */
	private Integer getPaymentGatewayID(Collection<CardPaymentInfo> colCardPaymentInfo) {
		Integer paymentGatewayID = null;
		if (colCardPaymentInfo != null && !colCardPaymentInfo.isEmpty()) {
			for (CardPaymentInfo cardPaymentInfo : colCardPaymentInfo) {
				if (cardPaymentInfo.getIpgIdentificationParamsDTO() != null) {
					paymentGatewayID = cardPaymentInfo.getIpgIdentificationParamsDTO().getIpgId();
					if (paymentGatewayID != null) {
						break;
					}
				}
			}
		}
		return paymentGatewayID;
	}

	/**
	 * Returns the payment gateway configuration. If the configuration is not found an error is logged however an empty
	 * configuration list will be provided without interrupting the execution flow by throwing an exception.
	 * 
	 * @param paymentGatewayID
	 *            Payment Gateway ID.
	 * @return The Payment Gateway ID, or an empty ArrayList if the payment gateway configuration is not found.
	 */
	private List<CardDetailConfigDTO> getPaymentGatewayConfig(Integer paymentGatewayID) {
		try {
			return paymentBrokerBD.getPaymentGatewayCardConfigData(paymentGatewayID.toString());
		} catch (ModuleException e) {
			log.error("Payment Gateway Configurations not found", e);
			return new ArrayList<CardDetailConfigDTO>();
		}
	}

	/**
	 * Process card payments
	 * 
	 * @param colCardPaymentInfo
	 * @param colPaymentInfo
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void processCardPayments(Collection<CardPaymentInfo> colCardPaymentInfo, Collection<PaymentInfo> colPaymentInfo,
			Collection<Integer> colTnxIds, CredentialsDTO credentialsDTO, List<CardDetailConfigDTO> cardDetailConfigData,
			TravelDTO travelData) throws ModuleException {
		// If any credit card payment exist
		if (colCardPaymentInfo.size() > 0) {
			// Capture the card payment
			Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
			CardPaymentInfo cardPaymentInfo;
			while (itColCardPaymentInfo.hasNext()) {
				cardPaymentInfo = (CardPaymentInfo) itColCardPaymentInfo.next();

				// Send Payment request and capture the response
				Object[] responses = PaymentGatewayBO.creditPayment(cardPaymentInfo, colTnxIds, credentialsDTO,
						cardDetailConfigData, travelData);
				int paymentBrokerRefNo = Integer.parseInt((String) responses[0]);
				String operationType = (String) responses[1];
				String authorizationId = (String) responses[2];

				// Update the references
				PaymentGatewayBO.updatePaymentBrokerReferences(cardPaymentInfo, colPaymentInfo, paymentBrokerRefNo,
						operationType, authorizationId);
			}
		}
	}

	/**
	 * Process External Payment Transaction
	 * 
	 * @param externalPaymentTnx
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void processExternalPayTx(ExternalPaymentTnx externalPaymentTnx, CredentialsDTO credentialsDTO)
			throws ModuleException {
		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setBalanceQueryKey(externalPaymentTnx.getBalanceQueryKey());
		criteriaDTO.setPnr(externalPaymentTnx.getPnr());
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.INITIATED);

		// Transactions with status 'Aborted' are allowed to manually reconcile any number of times
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.ABORTED);
		// Recon failed transaction are allowed reconcile again
		if (!ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED.equals(externalPaymentTnx.getReconcilationStatus())) {
			criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS);
		}

		Map<String, PNRExtTransactionsTO> pnrTnxMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getExtPayTransactions(criteriaDTO);

		PNRExtTransactionsTO pnrPayTnxs = null;
		if (pnrTnxMap != null) {
			pnrPayTnxs = (PNRExtTransactionsTO) pnrTnxMap.get(externalPaymentTnx.getPnr());
			if (pnrPayTnxs == null || pnrPayTnxs.getExtPayTransactions() == null
					|| pnrPayTnxs.getExtPayTransactions().size() == 0) {
				throw new ModuleException("airreservations.recordExtPay.initRecNotFound");
			}
		}

		ExternalPaymentTnx currentExternalPaymentTnx = (ExternalPaymentTnx) pnrPayTnxs.getExtPayTransactions().iterator().next();

		currentExternalPaymentTnx.setExternalPayId(externalPaymentTnx.getExternalPayId());
		currentExternalPaymentTnx.setStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);
		currentExternalPaymentTnx.setInternalTnxEndTimestamp(new Date());
		currentExternalPaymentTnx.setExternalTnxEndTimestamp(externalPaymentTnx.getExternalTnxEndTimestamp());
		currentExternalPaymentTnx.setExternalPayStatus(externalPaymentTnx.getExternalPayStatus());
		currentExternalPaymentTnx.setInternalPayId(externalPaymentTnx.getInternalPayId());// Agent transaction id
		currentExternalPaymentTnx.setAmount(externalPaymentTnx.getAmount());// for CDM, value is rounded up to nearest
																			// 10
		currentExternalPaymentTnx.setPaxCount(externalPaymentTnx.getPaxCount());
		if (currentExternalPaymentTnx.getUserId() == null) {// do not override the user, if already set
			currentExternalPaymentTnx.setUserId(externalPaymentTnx.getUserId() != null ? externalPaymentTnx.getUserId()
					: credentialsDTO.getUserId());
		}
		if (externalPaymentTnx.getRemarks() != null) {
			currentExternalPaymentTnx.setRemarks((currentExternalPaymentTnx.getRemarks() != null ? currentExternalPaymentTnx
					.getRemarks() + "|" : "")
					+ externalPaymentTnx.getRemarks());
		}
		if (externalPaymentTnx.getReconcilationStatus() != null) {
			currentExternalPaymentTnx.setReconcilationStatus(externalPaymentTnx.getReconcilationStatus());
		}
		if (externalPaymentTnx.getReconciledTimestamp() != null) {
			currentExternalPaymentTnx.setReconciledTimestamp(externalPaymentTnx.getReconciledTimestamp());
		}
		if (externalPaymentTnx.getReconciliationSource() != null) {
			currentExternalPaymentTnx.setReconciliationSource(externalPaymentTnx.getReconciliationSource());
		}

		// TODO set additional parameters
		ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveOrUpdateExternalPayTxInfo(currentExternalPaymentTnx);
	}

	private void combineCreditPayments(Map<Integer, Collection<TnxCreditPayment>> existingPaxCreditPayments,
			Map<Integer, Collection<TnxCreditPayment>> newPaxCreditPayments) {
		for (Integer paxSequence : newPaxCreditPayments.keySet()) {
			if (existingPaxCreditPayments.get(paxSequence) == null)
				existingPaxCreditPayments.put(paxSequence, newPaxCreditPayments.get(paxSequence));
			else
				existingPaxCreditPayments.get(paxSequence).addAll(newPaxCreditPayments.get(paxSequence));
		}
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param colPaymentInfo
	 * @param reservationContactInfo
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void validateParams(String pnr, Collection colPaymentInfo, ReservationContactInfo reservationContactInfo,
			Collection colTnxIds, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || colPaymentInfo == null || reservationContactInfo == null || colTnxIds == null
				|| credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	/**
	 * Validate BSP Params
	 * 
	 * @param pnr
	 * @param colPaymentInfo
	 * @param reservation
	 * @throws ModuleException
	 */
	private void validateBSPParams(String pnr, Collection colPaymentInfo, Reservation reservation) throws ModuleException {
		log.debug("Inside validateBSPParams");

		// validate payment
		if (isInvalidPayment(colPaymentInfo)) {
			throw new ModuleException("airreservations.arg.invalid.bsp.payment");
		}

		if (ReservationApiUtils.isBSPpayment(colPaymentInfo)
				&& !AppSysParamsUtil.isBspPaymentsAcceptedForOCAlterReservation()) {
			if (reservation != null && reservation.getSegments() != null) {
				if (reservation.isAnyCancelSegmentsExists()) {
					throw new ModuleException("airreservations.arg.bsp.payment.for.cancel.segments.not.allowed");
				}
			}
		}

		log.debug("Exit validateBSPParams");
	}

	/*
	 * @param colPaymentInfo
	 * 
	 * @return
	 */
	private boolean isInvalidPayment(Collection<PaymentInfo> colPaymentInfo) {

		Iterator<PaymentInfo> iteratorPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		boolean anyotherPaymentFound = false;
		boolean bspPaymentFound = false;
		while (iteratorPaymentInfo.hasNext()) {
			paymentInfo = (PaymentInfo) iteratorPaymentInfo.next();
			if (paymentInfo instanceof AgentCreditInfo) {
				AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
				if (agentCreditInfo.getPaymentReferenceTO() != null
						&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType() != null
						&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
					bspPaymentFound = true;
				} else {
					anyotherPaymentFound = true;
				}
			} else {
				anyotherPaymentFound = true;
			}
		}
		return (anyotherPaymentFound && bspPaymentFound && !AppSysParamsUtil
				.isBspPaymentsAcceptedForOCAlterReservation());
	}

	private void processOfflinePayments(Reservation reservation, CredentialsDTO credentialsDTO,
			Collection<OfflinePaymentInfo> colOfflinePaymentInfo) throws ModuleException {
		if (colOfflinePaymentInfo.size() > 0) {
			Iterator<OfflinePaymentInfo> itColOfflinePaymentInfo = colOfflinePaymentInfo.iterator();
			OfflinePaymentInfo offlinePaymentInfo;
			while (itColOfflinePaymentInfo.hasNext()) {
				offlinePaymentInfo = itColOfflinePaymentInfo.next();
				Integer temporyPaymentId = ReservationModuleUtils.getReservationBD().recordTemporyPaymentEntryForOffline(
						reservation.getContactInfo(), offlinePaymentInfo, credentialsDTO, false, true, reservation.getPnr());
				
				AppIndicatorEnum appIndicatorEnum = credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null ? credentialsDTO.getTrackInfoDTO().getAppIndicator() : null;
				PaymentUtil.cancelPreviousOfflineTransaction(reservation.getPnr(), appIndicatorEnum, true);


				PaymentGatewayBO.getRequestData(offlinePaymentInfo, temporyPaymentId, reservation.getContactInfo());
			}
		}
	}
}
