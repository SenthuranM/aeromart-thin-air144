package com.isa.thinair.airreservation.core.bl.segment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ExternalSegmentUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for change dummy reservation external segments
 * 
 * @author malaka
 * @isa.module.command name="changeDummyResExternalSegment"
 */
public class ChangeDummyResExternalSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ChangeDummyResExternalSegment.class);

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside ChangeDummyResExternalSegment.execute");

		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Collection<ExternalSegmentTO> colExternalSegmentTO = (Collection<ExternalSegmentTO>) this
				.getParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS);

		this.validateParams(reservation, colExternalSegmentTO);

		if (colExternalSegmentTO != null && colExternalSegmentTO.size() > 0) {
			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap = ExternalSegmentUtil
					.getConfirmedReservationExternalSegmentInAMap(reservation.getExternalReservationSegment());

			Map<String, Collection<ExternalPnrSegment>> serverAllSegmentMap = ExternalSegmentUtil
					.getAllReservationExternalSegmentInAMap(reservation.getExternalReservationSegment());

			String addedSegmentInfo = BeanUtils.nullHandler(ExternalSegmentUtil.addExternalSegments(
					colExternalSegmentTO, reservation, serverAllSegmentMap));
						
			String modifySegmentInfo = BeanUtils.nullHandler(ExternalSegmentUtil.modifyExternalSegments(
					removeAlreadyCancelledExternalSegments(serverAllSegmentMap,colExternalSegmentTO) , serverSegmentMap));

			if (addedSegmentInfo.length() > 0 || modifySegmentInfo.length() > 0) {
				ReservationProxy.saveReservation(reservation);
			}
		}

		log.debug("Exit ChangeDummyResExternalSegment.execute");
		
		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.PNR, reservation.getPnr());
		response.addResponceParam(CommandParamNames.VERSION, Long.valueOf((reservation.getVersion())));

		return response;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection<ExternalSegmentTO> removeAlreadyCancelledExternalSegments(
			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap,
			Collection<ExternalSegmentTO> colExternalSegmentTO) {

		Map<String, Collection<ExternalSegmentTO>> segmentMap = new HashMap<String, Collection<ExternalSegmentTO>>();
		Collection<ExternalSegmentTO> filteredSegments = new ArrayList<ExternalSegmentTO>();

		for (ExternalSegmentTO externalSegmentTO : colExternalSegmentTO) {
			String uniqueSegmentKeyWithOutStatus = ReservationApiUtils.getUniqueSegmentKey(
					externalSegmentTO.getFlightNo(), externalSegmentTO.getSegmentCode(),
					externalSegmentTO.getDepartureDate(), externalSegmentTO.getCabinClassCode(), null);

			Collection<ExternalSegmentTO> colReservationExternalSegmentTO = segmentMap
					.get(uniqueSegmentKeyWithOutStatus);

			if (colReservationExternalSegmentTO == null) {
				colReservationExternalSegmentTO = new ArrayList<ExternalSegmentTO>();
				colReservationExternalSegmentTO.add(externalSegmentTO);
				segmentMap.put(uniqueSegmentKeyWithOutStatus, colReservationExternalSegmentTO);
			} else {
				colReservationExternalSegmentTO.add(externalSegmentTO);
			}

		}

		for (String key : segmentMap.keySet()) {
			Collection<ExternalSegmentTO> newExternalSegements = segmentMap.get(key);
			Collection<ExternalPnrSegment> existingExternalSegements = serverSegmentMap.get(key);

			if (existingExternalSegements != null && (newExternalSegements.size() >= existingExternalSegements.size())) {
				List newExternalSegementsList;
				if (newExternalSegements instanceof List) {
					newExternalSegementsList = (List) newExternalSegements;
				} else {
					newExternalSegementsList = new ArrayList(newExternalSegements);
				}
				Collections.sort(newExternalSegementsList);

				List existingExternalSegementsList;
				if (existingExternalSegements instanceof List) {
					existingExternalSegementsList = (List) existingExternalSegements;
				} else {
					existingExternalSegementsList = new ArrayList(existingExternalSegements);
				}
				Collections.sort(existingExternalSegementsList);
				Iterator<ExternalSegmentTO> newSegIterator = newExternalSegementsList.iterator();

				for (ExternalPnrSegment existingExternalSegement : existingExternalSegements) {

					if (ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL
							.equals(existingExternalSegement.getStatus())) {
						if (newSegIterator.hasNext()) {
							ExternalSegmentTO newSegment = newSegIterator.next();
							if (existingExternalSegement.getStatus().equals(newSegment.getStatus())
									&& (BeanUtils.nullHandler(existingExternalSegement.getSubStatus()).equals(BeanUtils
											.nullHandler(newSegment.getSubStatus())))) {
								newSegIterator.remove();
							}

						}
					}
				}
				if(!newExternalSegementsList.isEmpty()) {
					filteredSegments.addAll(newExternalSegementsList);
				}	
			} else {
				filteredSegments.addAll(newExternalSegements);
			}

		}

		return filteredSegments;

	}

	private void validateParams(Reservation reservation, Collection<ExternalSegmentTO> colExternalSegmentTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (reservation == null || colExternalSegmentTO == null || colExternalSegmentTO.size() == 0) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

}
