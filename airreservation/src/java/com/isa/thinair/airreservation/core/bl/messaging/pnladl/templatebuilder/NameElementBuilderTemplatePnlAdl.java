package com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.NameElementBuilderTemplate;

public class NameElementBuilderTemplatePnlAdl extends NameElementBuilderTemplate {
	
	public boolean isMorePaxToProceed() {
		return true;
	}

}
