package com.isa.thinair.airreservation.core.bl.flexi;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;

public class FlexiRetainDTO {
	private Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap = new HashMap<Integer, Collection<ReservationPaxOndFlexibilityDTO>>();
	private Map<Integer, Map<Collection<Integer>, Collection<ReservationPaxOndFlexibilityDTO>>> pnrPaxAndSegwiseFlexibilities = new HashMap<Integer, Map<Collection<Integer>, Collection<ReservationPaxOndFlexibilityDTO>>>();

	public void addRetainFlexi(Collection<Integer> pnrSegIds, Integer pnrPaxId,
			Collection<ReservationPaxOndFlexibilityDTO> paxOndFlexibilities) {
		updatedFlexibilitiesMap.put(pnrPaxId, paxOndFlexibilities);
		if (!pnrPaxAndSegwiseFlexibilities.containsKey(pnrPaxId)) {
			pnrPaxAndSegwiseFlexibilities.put(pnrPaxId,
					new HashMap<Collection<Integer>, Collection<ReservationPaxOndFlexibilityDTO>>());
		}
		pnrPaxAndSegwiseFlexibilities.get(pnrPaxId).put(pnrSegIds, paxOndFlexibilities);
	}

	public Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> getPaxWiseUpdatedFlexiMap() {
		return updatedFlexibilitiesMap;
	}

	public boolean hasRetainableFlexiForPax(Integer pnrPaxId) {
		return pnrPaxAndSegwiseFlexibilities.containsKey(pnrPaxId);
	}

	public Map<Collection<Integer>, Collection<ReservationPaxOndFlexibilityDTO>> getPnrSegWiseFlexibilityForPax(Integer pnrPaxId) {
		return pnrPaxAndSegwiseFlexibilities.get(pnrPaxId);
	}

	public boolean hasRetainableFlexi() {
		return !updatedFlexibilitiesMap.isEmpty();
	}
}
