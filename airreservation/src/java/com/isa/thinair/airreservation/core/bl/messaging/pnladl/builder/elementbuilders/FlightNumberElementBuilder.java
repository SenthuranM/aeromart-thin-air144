/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.DestinationFareContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.MessageHeaderElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class FlightNumberElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private String flightNumber;
	private Date departureDate;
	private String departureAirportCode;
	private String partNumber;

	private MessageHeaderElementContext heContext;
	private DestinationFareContext pEContext;

	private BaseRuleExecutor<RulesDataContext> flightNumberRuleExecutor = new HeaderElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		RulesDataContext rulesContext = null;
		
		initContextData(context);
		flightElementTemplate();
		response = validateSubElement(currentElement);
		ammendMessageDataAccordingTo(response);
	}

	private void initContextData(ElementContext context) {
		heContext = (MessageHeaderElementContext) context;
		pEContext = (DestinationFareContext) context;
		currentLine = heContext.getCurrentMessageLine();
		messageLine = heContext.getMessageString();
		flightNumber = heContext.getFlightNumber();
		departureDate = heContext.getDepartureDate();
		departureAirportCode = heContext.getDepartureAirportCode();
		partNumber = getPartNumber(heContext.getPartNumber());
	}

	private String getPartNumber(Integer partNumber) {
		String partNumberToString = "";
		if (partNumber != null) {
			partNumberToString = partNumber.toString();
		}
		return partNumberToString;
	}

	private String formatDepartureDate(Date date) {
		String formattedDate = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMM");
		formattedDate = dateFormat.format(date).toUpperCase();
		return formattedDate;
	}

	private void flightElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		elementTemplate.setLength(0);
		elementTemplate.append(flightNumber);
		elementTemplate.append(forwardSlash());
		elementTemplate.append(formatDepartureDate(departureDate));
		elementTemplate.append(space());
		elementTemplate.append(departureAirportCode);
		elementTemplate.append(space());
		elementTemplate.append(partText());
		elementTemplate.append(partNumber);
		currentElement = elementTemplate.toString();

	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			executeConcatenationElementBuilder(heContext);
			ammendToBaseLine(currentElement, currentLine, messageLine);
			executeNext();
		}
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = flightNumberRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext(){
		if(nextElementBuilder != null){
			nextElementBuilder.buildElement(pEContext);
		}
	}
	
}
