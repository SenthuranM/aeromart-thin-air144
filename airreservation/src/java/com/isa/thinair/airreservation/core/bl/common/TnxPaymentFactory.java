/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.TnxAgentPayment;
import com.isa.thinair.airreservation.api.model.TnxCardPayment;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.TnxLoyaltyPayment;
import com.isa.thinair.airreservation.api.model.TnxPayment;
import com.isa.thinair.airreservation.api.model.TnxVoucherPayment;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;

/**
 * Transaction Payment Factory
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class TnxPaymentFactory {

	/**
	 * Return cash payment instance
	 * 
	 * @param amount
	 * @param payCurrencyDTO
	 * @param paymentReferanceTO
	 * @param paymentCarrier
	 *            payment carrier which did the payment
	 * @param lccUniqueTnxId
	 *            lcc Unique Tnx id linking pax tnx and ext pax tnx
	 * @param paymentTnxId
	 * @return
	 */
	protected static ITnxPayment getCashPaymentInstance(BigDecimal amount, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferanceTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {
		TnxPayment payment = new TnxPayment();
		payment.setPaymentType(PaymentType.CASH.getTypeValue());
		payment.setAmount(amount);
		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentReferenceTO(paymentReferanceTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setLccUniqueTnxId(lccUniqueTnxId);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}

	/**
	 * Return card payment instance
	 * 
	 * @param amount
	 * @param paymentType
	 * @param cardNumber
	 * @param cardName
	 * @param address
	 * @param cardExpiryDate
	 * @param secCode
	 * @param pnr
	 * @param paymentBrokerRefNumber
	 * @param payCurrencyDTO
	 * @param paymentReferanceTO
	 * @param paymentCarrier
	 *            payment carrier which did the payment
	 * @param lccUniqueTnxId
	 *            lcc Unique Tnx id linking pax tnx and ext pax tnx
	 * @param paymentTnxId
	 * @return
	 */
	protected static ITnxPayment getCardPaymentInstance(BigDecimal amount, int paymentType, String cardNumber, String cardName,
			String address, String cardExpiryDate, String secCode, String pnr, int paymentBrokerRefNumber,
			PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferanceTO, String paymentCarrier, String lccUniqueTnxId,
			String authorizationId, Integer paymentTnxId) {

		TnxCardPayment cardPayment = new TnxCardPayment();
		cardPayment.setPaymentType(paymentType);
		cardPayment.setAmount(amount);
		cardPayment.setCardNumber(cardNumber);
		cardPayment.setCardName(cardName);
		cardPayment.setAddress(address);
		cardPayment.setCardExpiryDate(cardExpiryDate);
		cardPayment.setSecCode(secCode);
		cardPayment.setPnr(pnr);
		cardPayment.setPaymentBrokerRefNumber(paymentBrokerRefNumber);

		cardPayment.setPayCurrencyDTO(payCurrencyDTO);
		cardPayment.setPaymentReferenceTO(paymentReferanceTO);

		cardPayment.setPaymentCarrier(paymentCarrier);
		cardPayment.setLccUniqueTnxId(lccUniqueTnxId);
		cardPayment.setAuthorizationId(authorizationId);
		cardPayment.setPaymentTnxId(paymentTnxId);
		return cardPayment;
	}

	/**
	 * Return agent payment instance
	 * 
	 * @param amount
	 * @param agentCode
	 * @param payCurrencyDTO
	 * @param paymentReferenceTO
	 * @param paymentCarrier
	 *            payment carrier which did the payment
	 * @param lccUniqueTnxId
	 *            lcc Unique Tnx id linking pax tnx and ext pax tnx
	 * @param paymentTnxId
	 * @return
	 */
	protected static ITnxPayment getOnAccountPaymentInstance(BigDecimal amount, String agentCode, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferenceTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {

		if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
				&& paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
			return getBSPPaymentInstance(amount, agentCode, payCurrencyDTO, paymentReferenceTO, paymentCarrier, lccUniqueTnxId,
					paymentTnxId);
		} else {
			return getOnAccPaymentInstance(amount, agentCode, payCurrencyDTO, paymentReferenceTO, paymentCarrier, lccUniqueTnxId,
					paymentTnxId);
		}

	}

	public static ITnxPayment getOnAccPaymentInstance(BigDecimal amount, String agentCode, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferenceTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {
		TnxAgentPayment payment = new TnxAgentPayment();

		payment.setPaymentType(PaymentType.ON_ACCOUNT.getTypeValue());
		payment.setAgentCode(agentCode);
		payment.setAmount(amount);
		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentReferenceTO(paymentReferenceTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setLccUniqueTnxId(lccUniqueTnxId);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}

	public static ITnxPayment getBSPPaymentInstance(BigDecimal amount, String agentCode, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferenceTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {
		TnxAgentPayment payment = new TnxAgentPayment();

		payment.setPaymentType(PaymentType.BSP.getTypeValue());
		payment.setAgentCode(agentCode);
		payment.setAmount(amount);
		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentReferenceTO(paymentReferenceTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setLccUniqueTnxId(lccUniqueTnxId);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}

	/**
	 * Return credit payment instance
	 * 
	 * @param pnrPaxId
	 * @param pnr
	 * @param agentCode
	 * @param amount
	 * @param payCurrencyDTO
	 * @param paymentCarrier
	 *            payment carrier which did the payment
	 * @param lccUniqueTnxId
	 *            lcc Unique Tnx id linking pax tnx and ext pax tnx
	 * @param paymentTnxId
	 * @return
	 */
	public static ITnxPayment getCreditPaymentInstance(String pnrPaxId, String pnr, String agentCode, BigDecimal amount,
			PayCurrencyDTO payCurrencyDTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {
		TnxCreditPayment payment = new TnxCreditPayment();
		payment.setPnrPaxId(pnrPaxId);
		payment.setPnr(pnr);
		payment.setAmount(amount);
		payment.setPaymentType(PaymentType.CREDIT.getTypeValue());
		payment.setAgentCode(agentCode);

		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setLccUniqueTnxId(lccUniqueTnxId);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}

	/**
	 * Return loyalty payment instance
	 * 
	 * @param loyaltyMemberAccountId
	 * @param rewardIDs
	 * @param amount
	 * @param payCurrencyDTO
	 * @param paymentCarrier
	 * @param lccUniqueTnxId
	 * @param paymentTnxId
	 * @return
	 */
	protected static ITnxPayment getLMSPaymentInstance(String loyaltyMemberAccountId, String[] rewardIDs, BigDecimal amount,
			PayCurrencyDTO payCurrencyDTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {
		TnxLoyaltyPayment payment = new TnxLoyaltyPayment();
		payment.setPaymentType(PaymentType.LOYALTY_PAYMENT.getTypeValue());
		payment.setLoyaltyMemberAccountId(loyaltyMemberAccountId);
		payment.setRewardIDs(rewardIDs);
		payment.setAmount(amount);
		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setLccUniqueTnxId(lccUniqueTnxId);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}
	
	protected static ITnxPayment getOfflinePaymentInstance(BigDecimal amount, PayCurrencyDTO payCurrencyDTO,
			String paymentCarrier, Integer paymentTnxId) {
		TnxLoyaltyPayment payment = new TnxLoyaltyPayment();
		payment.setPaymentType(PaymentType.OFFLINE.getTypeValue());
		payment.setAmount(amount);
		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}

	protected static ITnxPayment getVoucherPaymentInstance(VoucherDTO voucherDTO, BigDecimal amount,
			PayCurrencyDTO payCurrencyDTO, String paymentCarrier, String lccUniqueTnxId, Integer paymentTnxId) {
		TnxVoucherPayment payment = new TnxVoucherPayment();
		payment.setPaymentType(PaymentType.VOUCHER.getTypeValue());
		payment.setVoucherDTO(voucherDTO);
		payment.setAmount(amount);
		payment.setPayCurrencyDTO(payCurrencyDTO);
		payment.setPaymentCarrier(paymentCarrier);
		payment.setLccUniqueTnxId(lccUniqueTnxId);
		payment.setPaymentTnxId(paymentTnxId);
		return payment;
	}
}
