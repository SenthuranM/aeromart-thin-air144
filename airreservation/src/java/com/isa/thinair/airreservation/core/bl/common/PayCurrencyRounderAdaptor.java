package com.isa.thinair.airreservation.core.bl.common;

import java.util.Collection;

import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 * @since 8:53 PM 1/13/2009
 */
class PayCurrencyRounderAdaptor {

	protected static void round(PaymentAssembler assembler) throws ModuleException {
		Collection<PaymentAssembler> payments = PayCurrencyRounderAdaptorUtils.getTotalPaymentAssemblers(assembler);
		PayCurrencyRounderAdaptorUtils.convertAndRoundToPayCurr(payments);
	}

	protected static void round(PassengerAssembler assembler) throws ModuleException {
		Collection<PaymentAssembler> payments = PayCurrencyRounderAdaptorUtils.getTotalPaymentAssemblers(assembler);
		PayCurrencyRounderAdaptorUtils.convertAndRoundToPayCurr(payments);
	}

	protected static void round(SegmentAssembler assembler) throws ModuleException {
		Collection<PaymentAssembler> payments = PayCurrencyRounderAdaptorUtils.getTotalPaymentAssemblers(assembler);
		PayCurrencyRounderAdaptorUtils.convertAndRoundToPayCurr(payments);
	}

	protected static void round(Collection<IPayment> iPayments) throws ModuleException {
		Collection<PaymentAssembler> payments = PayCurrencyRounderAdaptorUtils.getTotalPaymentAssemblers(iPayments);
		PayCurrencyRounderAdaptorUtils.convertAndRoundToPayCurr(payments);
	}

	protected static void round(ReservationAssembler assembler) throws ModuleException {
		Collection<PaymentAssembler> payments = PayCurrencyRounderAdaptorUtils.getTotalPaymentAssemblers(assembler);
		PayCurrencyRounderAdaptorUtils.convertAndRoundToPayCurr(payments);
	}

	protected static void roundForRefund(PassengerAssembler assembler) throws ModuleException {
		Collection<PaymentAssembler> payments = PayCurrencyRounderAdaptorUtils.getTotalPaymentAssemblers(assembler);
		PayCurrencyRounderAdaptorUtils.updatePayCurrReferences(payments);
	}
}
