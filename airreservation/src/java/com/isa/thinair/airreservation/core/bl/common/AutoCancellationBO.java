package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Auto Cancellation Business Methods
 * 
 * @author rumesh
 * 
 */
public class AutoCancellationBO {

	public static void saveAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo, String pnr,
			CredentialsDTO credentialsDTO) throws ModuleException {
		if (autoCancellationInfo != null) {
			saveOrUpdateAutoCancellationInfo(autoCancellationInfo);
			doAudit(autoCancellationInfo, pnr, credentialsDTO);
		}
	}

	public static void removeAutoCancellationFlag(Reservation reservation) {
		if (AppSysParamsUtil.isAutoCancellationEnabled()) {
			// Remove segment level auto cnx flag if any
			boolean segUpdated = false;
			for (ReservationSegment reservationSegment : reservation.getSegments()) {

				if (reservationSegment.getAutoCancellationId() != null) {
					segUpdated = true;
					reservationSegment.setAutoCancellationId(null);
				}
			}
			if (segUpdated) {
				ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO.saveReservationSegments(reservation.getSegments());
			}

			boolean paxUpdated = false;
			Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
			// Remove infant auto cnx flag if any
			for (ReservationPax reservationPax : reservation.getPassengers()) {

				if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())
						&& reservationPax.getAutoCancellationId() != null) {
					paxUpdated = true;
					reservationPax.setAutoCancellationId(null);
				} else {
					pnrPaxIds.add(reservationPax.getPnrPaxId());
					if (AppSysParamsUtil.isAirportServicesEnabled() || AppSysParamsUtil.isInFlightServicesEnabled()) {
						// Remove SSR(in-flight/airport service) auto cnx flag if any
						if (reservationPax.getPnrPaxFares() != null) {
							for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
								if (reservationPaxFare != null && reservationPaxFare.getPaxFareSegments() != null) {
									for (ReservationPaxFareSegment resPaxFareSeg : reservationPaxFare.getPaxFareSegments()) {
										if (resPaxFareSeg != null && resPaxFareSeg.getReservationPaxSegmentSSRs() != null) {
											for (ReservationPaxSegmentSSR resPaxSegSSR : resPaxFareSeg
													.getReservationPaxSegmentSSRs()) {
												if (resPaxSegSSR.getAutoCancellationId() != null) {
													paxUpdated = true;
													resPaxSegSSR.setAutoCancellationId(null);
												}
											}
										}
									}
								}
							}
						}
					}

				}
			}

			if (paxUpdated) {
				ReservationDAOUtils.DAOInstance.PASSENGER_DAO.savePassengers(reservation.getPassengers());
			}

			// Remove Seat auto cnx flag if any
			if (AppSysParamsUtil.isShowSeatMap()) {
				SeatMapDAO seatMapDAO = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO;
				Collection<PassengerSeating> paxSeats = seatMapDAO.getFlightSeats(pnrPaxIds);
				if (paxSeats != null && !paxSeats.isEmpty()) {
					boolean seatUpdated = false;
					for (PassengerSeating passengerSeating : paxSeats) {
						if (passengerSeating.getAutoCancellationId() != null) {
							seatUpdated = true;
							passengerSeating.setAutoCancellationId(null);
						}
					}
					if (seatUpdated) {
						seatMapDAO.saveOrUpdate(paxSeats);
					}
				}
			}

			// Remove meal auto cnx flag if any
			if (AppSysParamsUtil.isShowMeals()) {
				MealDAO mealDAO = ReservationDAOUtils.DAOInstance.MEAL_DAO;
				Collection<PassengerMeal> paxMeals = mealDAO.getFlightMealsForPnrPax(pnrPaxIds);
				if (paxMeals != null && !paxMeals.isEmpty()) {
					boolean mealUpdated = false;
					for (PassengerMeal passengerMeal : paxMeals) {
						if (passengerMeal.getAutoCancellationId() != null) {
							mealUpdated = true;
							passengerMeal.setAutoCancellationId(null);
						}
					}
					if (mealUpdated) {
						mealDAO.saveOrUpdate(paxMeals);
					}
				}
			}

			// Remove baggage auto cnx flag if any
			if (AppSysParamsUtil.isShowBaggages()) {
				BaggageDAO baggageDAO = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO;
				Collection<PassengerBaggage> paxBaggages = baggageDAO.getFlightBaggagesForPnrPax(pnrPaxIds);
				if (paxBaggages != null && !paxBaggages.isEmpty()) {
					boolean bgUpdated = false;
					for (PassengerBaggage passengerBaggage : paxBaggages) {
						if (passengerBaggage.getAutoCancellationId() != null) {
							bgUpdated = true;
							passengerBaggage.setAutoCancellationId(null);
						}
					}
					if (bgUpdated) {
						baggageDAO.saveOrUpdate(paxBaggages);
					}
				}
			}

			// Remove insurance auto cnx flag if any
			if (AppSysParamsUtil.isShowTravelInsurance()) {
				if (reservation.getReservationInsurance() != null && !reservation.getReservationInsurance().isEmpty()){
						
					for (ReservationInsurance reservationInsurance : reservation.getReservationInsurance()) {
						if(reservationInsurance.getAutoCancellationId() != null) {
							ReservationDAO resDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
							reservationInsurance.setAutoCancellationId(null);
							resDAO.saveReservationInsurance(reservationInsurance);
						}
					}
					
				}
			}
		}
	}

	/**
	 * Returns array consists of existing modification charges for pax types,
	 * 
	 * 0 --> Adult modification charge 1 --> Child modification charge 2 --> Infant modification charge
	 * 
	 * @param pnrSegs
	 * @return
	 */
	public static BigDecimal[] calculateExistingModificationCharges(Collection<Integer> pnrSegs) {

		Collection<ReservationSegment> colResSegs = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
				.getReservationSegments(null, pnrSegs, null, true, true);

		ReservationSegment modifiedFromResSeg = null;
		for (ReservationSegment resSeg : colResSegs) {
			if (resSeg.getModifiedFrom() != null) {
				modifiedFromResSeg = getModifiedFromReservationSegment(resSeg.getReservation(), resSeg.getModifiedFrom());
				break;
			}
		}

		if (modifiedFromResSeg != null) {
			Map<String, BigDecimal> totModificationCharges = new HashMap<String, BigDecimal>();
			totModificationCharges.put(ReservationInternalConstants.PassengerType.ADULT,
					AccelAeroCalculator.getDefaultBigDecimalZero());
			totModificationCharges.put(ReservationInternalConstants.PassengerType.CHILD,
					AccelAeroCalculator.getDefaultBigDecimalZero());
			totModificationCharges.put(ReservationInternalConstants.PassengerType.PARENT,
					AccelAeroCalculator.getDefaultBigDecimalZero());

			Reservation res = modifiedFromResSeg.getReservation();
			ReservationPax adult = getPax(res.getPassengers(), ReservationInternalConstants.PassengerType.ADULT);
			ReservationPax parent = getPax(res.getPassengers(), ReservationInternalConstants.PassengerType.PARENT);
			ReservationPax child = getPax(res.getPassengers(), ReservationInternalConstants.PassengerType.CHILD);

			calcModificationCharges(modifiedFromResSeg, adult, child, parent, totModificationCharges);

			BigDecimal adultModCharge = totModificationCharges.get(ReservationInternalConstants.PassengerType.ADULT);
			BigDecimal parentModCharge = totModificationCharges.get(ReservationInternalConstants.PassengerType.PARENT);
			BigDecimal childModCharge = totModificationCharges.get(ReservationInternalConstants.PassengerType.CHILD);
			BigDecimal infantModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (parentModCharge.doubleValue() != 0 && adultModCharge.doubleValue() != 0) {
				infantModCharge = AccelAeroCalculator.subtract(parentModCharge, adultModCharge);
			} else if (parentModCharge.doubleValue() != 0 && adultModCharge.doubleValue() == 0) {
				adultModCharge = parentModCharge;
			}

			return new BigDecimal[] { adultModCharge, childModCharge, infantModCharge };
		}

		return null;
	}

	public static Map<String, Integer> getMarketingCarrierExpiredInfo(String marketingCarrier, Collection<Integer> autoCnxIds) {
		Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();

		// Get Auto Expiring PNR due to segment expire
		Map<String, Integer> segmentAutoCnxPNRs = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
				.getExpiredLccSegmentsInfo(marketingCarrier, autoCnxIds);

		// Get Auto Expiring PNR due to infant expire
		Map<String, Integer> infantAutoCnxPNRs = ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getExpiredLccInfantsInfo(
				marketingCarrier, autoCnxIds);

		// Get Auto Expiring PNRs due to ancillary expire
		Map<String, Integer> seatAutoCnxPNRs = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO.getExpiredLccSeatsInfo(
				marketingCarrier, autoCnxIds);
		Map<String, Integer> mealAutoCnxPNRs = ReservationDAOUtils.DAOInstance.MEAL_DAO.getExpiredLccMealsInfo(marketingCarrier,
				autoCnxIds);
		Map<String, Integer> baggageAutoCnxPNRs = ReservationDAOUtils.DAOInstance.BAGGAGE_DAO.getExpiredLccBaggagesInfo(
				marketingCarrier, autoCnxIds);
		Map<String, Integer> ssrAutoCnxPNRs = ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getExpiredLccSsrInfo(
				marketingCarrier, autoCnxIds);
		Map<String, Integer> insAutoCnxPNRs = ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getExpiredLccInsuranceInfo(
				marketingCarrier, autoCnxIds);

		if (segmentAutoCnxPNRs != null && !segmentAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(segmentAutoCnxPNRs);
		}
		if (infantAutoCnxPNRs != null && !infantAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(infantAutoCnxPNRs);
		}
		if (seatAutoCnxPNRs != null && !seatAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(seatAutoCnxPNRs);
		}
		if (mealAutoCnxPNRs != null && !mealAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(mealAutoCnxPNRs);
		}
		if (baggageAutoCnxPNRs != null && !baggageAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(baggageAutoCnxPNRs);
		}
		if (ssrAutoCnxPNRs != null && !ssrAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(ssrAutoCnxPNRs);
		}
		if (insAutoCnxPNRs != null && !insAutoCnxPNRs.isEmpty()) {
			autoCnxPNRs.putAll(insAutoCnxPNRs);
		}

		return autoCnxPNRs;
	}

	public static Map<String, BigDecimal> getExistingModificationChargeDue(List<Integer> pnrSegIds) {
		List<ReservationPaxDetailsDTO> paxList = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
				.getExistingModificationChargesDue(pnrSegIds);
		Map<String, BigDecimal> paxWiseModCharge = null;
		if (paxList != null && !paxList.isEmpty()) {
			paxWiseModCharge = new HashMap<String, BigDecimal>();

			Set<Integer> parentIds = new HashSet<Integer>();
			for (ReservationPaxDetailsDTO resPax : paxList) {
				if (PassengerType.INFANT.equals(resPax.getPaxType())) {
					parentIds.add(resPax.getAdultId());
				}
			}

			BigDecimal adultCharge = getModificationCharge(PassengerType.ADULT, paxList, parentIds);
			BigDecimal childCharge = getModificationCharge(PassengerType.CHILD, paxList, parentIds);
			BigDecimal parentCharge = getModificationCharge(PassengerType.PARENT, paxList, parentIds);
			BigDecimal infantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (parentCharge.doubleValue() != 0 && adultCharge.doubleValue() != 0) {
				infantCharge = AccelAeroCalculator.subtract(parentCharge, adultCharge);
			} else if (parentCharge.doubleValue() != 0 && adultCharge.doubleValue() == 0) {
				adultCharge = parentCharge;
			}

			paxWiseModCharge.put(PassengerType.ADULT, adultCharge);
			paxWiseModCharge.put(PassengerType.CHILD, childCharge);
			paxWiseModCharge.put(PassengerType.INFANT, infantCharge);
		}

		return paxWiseModCharge;
	}

	public static BigDecimal calculateBalanceToPayAfterAutoCancellation(Map<String, Collection<Integer>> groupWiseSegIds,
			List<Integer> autoCnxInfants, Map<String, CustomChargesTO> groupWiseCustomCharges, String pnr, Long version)
			throws ModuleException {

		BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		List<Integer> allCnxPnrSegIds = null;

		// Calculate balance to pay for segment cancellation.
		if (groupWiseSegIds != null && !groupWiseSegIds.isEmpty()) {
			allCnxPnrSegIds = getCancellingPnrSegIds(groupWiseSegIds.values());
			Map<Integer, BigDecimal> paxAvailBalanceMap = new HashMap<Integer, BigDecimal>();
			Map<Integer, BigDecimal> paxRefundableTotalMap = new HashMap<Integer, BigDecimal>();
			Map<Integer, BigDecimal> paxCancellationTotalMap = new HashMap<Integer, BigDecimal>();

			for (Entry<String, Collection<Integer>> groupEntry : groupWiseSegIds.entrySet()) {
				String groupKey = groupEntry.getKey();
				Collection<Integer> pnrSegIds = groupEntry.getValue();
				CustomChargesTO groupCustomChargesTO = (groupWiseCustomCharges != null)
						? groupWiseCustomCharges.get(groupKey)
						: null;
				if (groupCustomChargesTO == null) {
					BigDecimal[] existingModCharges = AutoCancellationBO.calculateExistingModificationCharges(pnrSegIds);
					if (existingModCharges != null) {
						groupCustomChargesTO = new CustomChargesTO();
						groupCustomChargesTO.setCompareWithExistingModificationCharges(true);
						groupCustomChargesTO.setExistingModificationChargeAdult(existingModCharges[0]);
						groupCustomChargesTO.setExistingModificationChargeChild(existingModCharges[1]);
						groupCustomChargesTO.setExistingModificationChargeInfant(existingModCharges[2]);
					}
				}

				Collection<PnrChargesDTO> pnrPaxCharges = ReservationModuleUtils.getSegmentBD().getOndChargesForCancellation(pnr,
						pnrSegIds, true, version, groupCustomChargesTO, false, false);

				if (pnrPaxCharges != null && !pnrPaxCharges.isEmpty()) {
					for (PnrChargesDTO pnrChargesDTO : pnrPaxCharges) {
						Integer paxId = pnrChargesDTO.getPnrPaxId();
						if (!PaxTypeTO.INFANT.equals(pnrChargesDTO.getPaxType())) {

							if (!paxAvailBalanceMap.containsKey(paxId)) {
								paxAvailBalanceMap.put(paxId, pnrChargesDTO.getAvailableBalance());
							}

							if (!paxRefundableTotalMap.containsKey(paxId)) {
								paxRefundableTotalMap.put(paxId, AccelAeroCalculator.getDefaultBigDecimalZero());
							}

							BigDecimal currRefundableTot = paxRefundableTotalMap.get(paxId);
							BigDecimal paxRefundableAmount = AccelAeroCalculator.add(pnrChargesDTO.getRefundableAmounts());
							paxRefundableTotalMap.put(pnrChargesDTO.getPnrPaxId(),
									AccelAeroCalculator.add(currRefundableTot, paxRefundableAmount));

							if (!paxCancellationTotalMap.containsKey(paxId)) {
								paxCancellationTotalMap.put(paxId, AccelAeroCalculator.getDefaultBigDecimalZero());
							}

							paxCancellationTotalMap.put(
									paxId,
									AccelAeroCalculator.add(paxCancellationTotalMap.get(paxId),
											pnrChargesDTO.getActualCancellationAmount()));
						}
					}

				}
			}

			for (Entry<Integer, BigDecimal> availBalEntry : paxAvailBalanceMap.entrySet()) {
				Integer paxId = availBalEntry.getKey();
				BigDecimal availBalance = availBalEntry.getValue();

				BigDecimal paxRefundableTot = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (paxRefundableTotalMap.containsKey(paxId)) {
					paxRefundableTot = paxRefundableTotalMap.get(paxId);
				}

				BigDecimal paxCnxTot = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (paxCancellationTotalMap.containsKey(paxId)) {
					paxCnxTot = paxCancellationTotalMap.get(paxId);
				}

				BigDecimal creditAmount = AccelAeroCalculator.subtract(availBalance, paxRefundableTot);

				balanceToPay = AccelAeroCalculator.add(balanceToPay, paxCnxTot, creditAmount);
			}

		}

		// Calculate balance to pay for infant removal
		if (autoCnxInfants != null && !autoCnxInfants.isEmpty()) {
			Collection<PnrChargesDTO> pnrPaxCharges = ReservationModuleUtils.getSegmentBD().getPnrCharges(pnr, true, version,
					null, false, false, allCnxPnrSegIds);
			if (pnrPaxCharges != null && !pnrPaxCharges.isEmpty()) {
				for (Integer pnrPaxId : autoCnxInfants) {
					PnrChargesDTO infantChargeDTO = ReservationApiUtils.getInfantChargesDTO(pnrPaxCharges, pnrPaxId);
					BigDecimal nonRefunablesTotal = ReservationApiUtils.getNonRefundableTotal(infantChargeDTO
							.getNonRefundableChargeMetaAmounts());

					BigDecimal creditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (creditAmount.compareTo(infantChargeDTO.getAvailableBalance()) == 1) {
						creditAmount = infantChargeDTO.getAvailableBalance();
					}

					balanceToPay = AccelAeroCalculator.add(balanceToPay, nonRefunablesTotal, creditAmount);
				}
			}
		}

		return balanceToPay;
	}

	private static List<Integer> getCancellingPnrSegIds(Collection<Collection<Integer>> colGrpWiseSegIds) {
		List<Integer> allCnxPnrSegIds = null;

		if (colGrpWiseSegIds != null && !colGrpWiseSegIds.isEmpty()) {
			allCnxPnrSegIds = new ArrayList<Integer>();
			for (Collection<Integer> grpSegIds : colGrpWiseSegIds) {
				if (grpSegIds != null && !grpSegIds.isEmpty()) {
					allCnxPnrSegIds.addAll(grpSegIds);
				}
			}
		}

		return allCnxPnrSegIds;
	}

	private static ReservationSegment getModifiedFromReservationSegment(Reservation reservation, int pnrSegId) {

		for (ReservationSegment reservationSegment : reservation.getSegments()) {
			if (reservationSegment.getPnrSegId() == pnrSegId) {
				return reservationSegment;
			}
		}

		return null;
	}

	private static ReservationPax getPax(Collection<ReservationPax> colPax, String paxType) {

		for (ReservationPax reservationPax : colPax) {
			if (paxType.equals(ReservationInternalConstants.PassengerType.PARENT)
					&& ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())
					&& reservationPax.hasInfant()) {
				return reservationPax;
			} else if (paxType.equals(reservationPax.getPaxType())) {
				return reservationPax;
			}
		}

		return null;
	}

	private static void calcModificationCharges(ReservationSegment resSeg, ReservationPax adult, ReservationPax child,
			ReservationPax parent, Map<String, BigDecimal> totModificationCharges) {

		int pnrSegId = resSeg.getPnrSegId();

		if (adult != null) {
			BigDecimal totModCharge = totModificationCharges.get(ReservationInternalConstants.PassengerType.ADULT);
			totModCharge = calculatePaxModCharge(pnrSegId, totModCharge, adult.getPnrPaxFares());
			totModificationCharges.put(ReservationInternalConstants.PassengerType.ADULT, totModCharge);
		}

		if (child != null) {
			BigDecimal totModCharge = totModificationCharges.get(ReservationInternalConstants.PassengerType.CHILD);
			totModCharge = calculatePaxModCharge(pnrSegId, totModCharge, child.getPnrPaxFares());
			totModificationCharges.put(ReservationInternalConstants.PassengerType.CHILD, totModCharge);
		}

		if (parent != null) {
			BigDecimal totModCharge = totModificationCharges.get(ReservationInternalConstants.PassengerType.PARENT);
			totModCharge = calculatePaxModCharge(pnrSegId, totModCharge, parent.getPnrPaxFares());
			totModificationCharges.put(ReservationInternalConstants.PassengerType.PARENT, totModCharge);
		}

		if (resSeg.getModifiedFrom() != null) {
			ReservationSegment modifiedFromSeg = getModifiedFromReservationSegment(resSeg.getReservation(),
					resSeg.getModifiedFrom());
			calcModificationCharges(modifiedFromSeg, adult, child, parent, totModificationCharges);
		} else {
			return;
		}
	}

	private static BigDecimal calculatePaxModCharge(int pnrSegId, BigDecimal totModCharge, Set<ReservationPaxFare> pnrPaxFares) {
		if (pnrPaxFares != null && !pnrPaxFares.isEmpty()) {
			for (ReservationPaxFare reservationPaxFare : pnrPaxFares) {
				Collection<ReservationPaxFareSegment> colResPaxSeg = reservationPaxFare.getPaxFareSegments();
				if (colResPaxSeg != null && !colResPaxSeg.isEmpty()) {
					for (ReservationPaxFareSegment resPaxSeg : colResPaxSeg) {
						if (resPaxSeg.getPnrSegId() == pnrSegId) {
							BigDecimal modChg = reservationPaxFare.getTotalModificationCharge();
							if (modChg != null) {
								return AccelAeroCalculator.add(totModCharge, modChg);
							}
						}
					}
				}
			}
		}

		return totModCharge;
	}

	private static void doAudit(AutoCancellationInfo autoCancellationInfo, String pnr, CredentialsDTO credentialsDTO)
			throws ModuleException {
		// Do audit
		ReservationAudit reservationAudit = createAutoCancellationAudit(autoCancellationInfo, pnr, credentialsDTO);
		ReservationAudit.createReservationAudit(reservationAudit, credentialsDTO, false);
		// Saves auto cancellation audit
		ReservationModuleUtils.getAuditorBD().audit(reservationAudit, reservationAudit.getContentMap());
	}

	private static ReservationAudit createAutoCancellationAudit(AutoCancellationInfo autoCancellationInfo, String pnr,
			CredentialsDTO credentialsDTO) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.AUTO_CANCELLATION.getCode());

		// Setting auto cancellation information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutoCancellationEntry.AUTO_CNX_ID, autoCancellationInfo
				.getAutoCancellationId().toString());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutoCancellationEntry.EXPIRE_ON,
				BeanUtils.parseDateFormat(autoCancellationInfo.getExpireOn(), "E, dd MMM yyyy HH:mm:ss"));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutoCancellationEntry.CNX_TYPE,
				autoCancellationInfo.getCancellationType());

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutoCancellationEntry.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutoCancellationEntry.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutoCancellationEntry.REMOTE_USER,
				credentialsDTO.getDefaultCarrierCode() + "/" + "[" + credentialsDTO.getAgentCode() + "]" + "/" + "["
						+ credentialsDTO.getUserId() + "]");

		return reservationAudit;
	}

	private static void saveOrUpdateAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		reservationDAO.saveAutoCancellationInfo(autoCancellationInfo);
	}

	private static BigDecimal
			getModificationCharge(String paxType, List<ReservationPaxDetailsDTO> paxList, Set<Integer> parentIds) {
		BigDecimal modCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxDetailsDTO resPax : paxList) {
			if (PassengerType.ADULT.equals(paxType) && PassengerType.ADULT.equals(resPax.getPaxType())
					&& (!parentIds.contains(resPax.getPnrPaxId()))) {
				modCharge = resPax.getTotalChargeAmount();
				break;
			} else if (PassengerType.CHILD.equals(paxType) && PassengerType.CHILD.equals(resPax.getPaxType())) {
				modCharge = resPax.getTotalChargeAmount();
				break;
			} else if (PassengerType.PARENT.equals(paxType) && PassengerType.ADULT.equals(resPax.getPaxType())
					&& parentIds.contains(resPax.getPnrPaxId())) {
				modCharge = resPax.getTotalChargeAmount();
				break;
			}
		}

		return modCharge;
	}
}
