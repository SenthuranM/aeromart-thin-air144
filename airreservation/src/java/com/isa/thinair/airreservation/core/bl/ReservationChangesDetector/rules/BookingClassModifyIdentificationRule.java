package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BookingClassModifyIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public BookingClassModifyIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {
		RuleResponseDTO response = getResponseDTO();
		List<Integer> addSegs = new ArrayList<Integer>();
		List<Integer> delSegs = new ArrayList<Integer>();

		for (ReservationSegment updatedSeg : dataContext.getUpdatedReservation().getSegments()) {
			if (!isCNXSegment(updatedSeg)) {
				for (ReservationSegment existSeg : dataContext.getExisitingReservation().getSegments()) {
					if (updatedSeg.getFlightSegId().equals(existSeg.getFlightSegId()) && !isCNXSegment(existSeg)) {

						if (updatedSeg.getBookingCode() != null && updatedSeg.equals(existSeg.getBookingCode())) {

							addSegs.add(updatedSeg.getPnrSegId());
							delSegs.add(existSeg.getPnrSegId());
							break;
						}

					}
				}
			}

		}
		updatePaxWiseAffectedSegmentListToAllGivenPax(response, addSegs, delSegs, getUncancelledPaxIds(dataContext.getUpdatedReservation()));
		return response;
	}

}
