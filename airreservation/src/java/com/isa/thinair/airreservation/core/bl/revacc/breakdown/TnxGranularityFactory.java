package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.PerPaxPriceBreakdown;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationCreditTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.LoyaltyProduct;
import com.isa.thinair.promotion.api.model.lms.FlownFile.PRODUCT_CODE;

/**
 * @author Nilindra Fernando
 * 
 * @since August 5, 2010
 */
public class TnxGranularityFactory {

	/**
	 * record tnx breakdown for payments
	 * 
	 * @param creditTnxBF
	 * @param payCurrencyDTO
	 * @param reservationPaxPaymentMetaTO
	 * @param transactionSeq
	 * @return
	 * @throws ModuleException
	 */
	private static ReservationPaxTnxBreakdown saveReservationPaxTnxBreakdownForPayment(ReservationTnx creditTnxBF,
			PayCurrencyDTO payCurrencyDTO, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			boolean enableTransactionGranularity, Integer transactionSeq) throws ModuleException {

		Integer pnrPaxId = Integer.parseInt(creditTnxBF.getPnrPaxId());
		PerPaxPriceBreakdown perPaxPriceBreakdown;

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = composeOndPayment(pnrPaxId, creditTnxBF,
					reservationPaxPaymentMetaTO, transactionSeq);

			if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
			}

			perPaxPriceBreakdown = TnxGranularityUtils.composePerPaxPriceBreakdown(colReservationPaxOndPayment);
			perPaxPriceBreakdown.setTotalBasePayAmount(reservationPaxPaymentMetaTO.getPerPaxBaseTotalPayAmount());
		} else {
			perPaxPriceBreakdown = TnxGranularityUtils.composePerPaxPriceBreakdown(reservationPaxPaymentMetaTO);
		}

		return TnxBreakDownFactory.saveReservationPaxTnxBreakdownForPayment(creditTnxBF, payCurrencyDTO, perPaxPriceBreakdown);
	}

	/**
	 * save tnx breakdown for refund
	 * 
	 * @param creditTnxBF
	 * @param payCurrencyDTO
	 * @param prefferedChargeCodes
	 *            Order the refund should deduct charges. (TAX,SUR etc.). Will only use the types mentioned to refund.
	 *            If passed amount is 100.00 and charge codes are TAX and SUR if total of SUR, TAX is less than 100.00
	 *            it should fail.
	 * @param pnrPaxOndChgIds
	 *            PaxOndChgID list related to the segment that charges should be refunded from.
	 * @param isManualRefund
	 * @param creditUtilizedMapByTnx
	 *            hold the consumed credit amount by transaction
	 * @throws ModuleException
	 *             If the passed prefferedChargeCodes contain a charge type not defined in the database.
	 */
	public static void saveReservationPaxTnxBreakdownForRefund(ReservationTnx creditTnxBF, PayCurrencyDTO payCurrencyDTO,
			boolean enableTransactionGranularity, Collection<String> prefferedChargeCodes, Collection<Long> pnrPaxOndChgIds,
			Set<Long> refundedPaxOndChargeIds, Boolean isManualRefund, Map<Long, Map<String, BigDecimal>> creditUtilizedMapByTnx)
			throws ModuleException {

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<String> colChargeGroupCodes = TnxGranularityRules.getRefundChargeGroupFulFillmentOrder();

			// Override the order. Replacing not overriding as per Nilindra's instructions
			if (prefferedChargeCodes != null && !prefferedChargeCodes.isEmpty()) {
				if (!colChargeGroupCodes.containsAll(prefferedChargeCodes)) {
					// A chargetype that's not defined in the db parameter.
					throw new ModuleException("airreservations.charges.granularity.wrongchargetype");
				}

				colChargeGroupCodes = prefferedChargeCodes;
			}

			Collection<Long> colPaymentTnxIds = new ArrayList<Long>();
			colPaymentTnxIds.add(Long.valueOf(creditTnxBF.getOriginalPaymentTnxID()));

			boolean isVoidReservation = false;

			if (!StringUtil.isNullOrEmpty(creditTnxBF.getPnrPaxId())) {
				isVoidReservation = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
						.isVoidReservationByPnrPaxIdOrPayTnxId(creditTnxBF.getPnrPaxId(), "P");
			}

			Map<Long, Collection<ReservationPaxOndPayment>> remainingPayments = getRefundEligiblePaymentMetaInfo(colPaymentTnxIds,
					colChargeGroupCodes, pnrPaxOndChgIds, isVoidReservation, true);

			/*
			 * when refund amount is greater than selected payment available credit, system will start refund the
			 * remaining from other payment available credit
			 */
			BigDecimal selTnxAvailCreditAmount = TnxGranularityUtils.getTotalAvailableCredit(remainingPayments);

			BigDecimal requiredCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal remainingAmount = creditTnxBF.getAmount();
			if (creditTnxBF.getAmount().doubleValue() > selTnxAvailCreditAmount.doubleValue()) {
				requiredCredit = AccelAeroCalculator.subtract(creditTnxBF.getAmount(), selTnxAvailCreditAmount);
				remainingAmount = selTnxAvailCreditAmount;
			}

			Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

			for (Collection<ReservationPaxOndPayment> colReservationPaxOndPayment : remainingPayments.values()) {
				if (remainingAmount.doubleValue() > 0) {
					for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {

						if (remainingAmount.doubleValue() > 0) {
							BigDecimal amount = reservationPaxOndPayment.getAmount();
							Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
							Long originalPaxOndChgId = reservationPaxOndPayment.getPnrPaxOndChgId();
							reservationPaxOndPayment = reservationPaxOndPayment.clone();
							reservationPaxOndPayment.setRemarks(null);
							reservationPaxOndPayment.setNominalCode(creditTnxBF.getNominalCode());
							reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
							reservationPaxOndPayment.setPaxOndRefundId(creditTnxBF.getTnxId());
							if (remainingAmount.doubleValue() > amount.doubleValue()) {
								reservationPaxOndPayment.setAmount(amount.negate());
								remainingAmount = AccelAeroCalculator.subtract(remainingAmount, amount);
							} else {
								reservationPaxOndPayment.setAmount(remainingAmount.negate());
								remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							}
							refundedPaxOndChargeIds.add(originalPaxOndChgId);
							composedReservationPaxOndPayment.add(reservationPaxOndPayment);
						} else {
							break;
						}
					}
				} else {
					break;
				}
			}

			// consuming from other payment credits
			consumePaxCreditFromOtherPayments(requiredCredit, colPaymentTnxIds, colChargeGroupCodes, creditTnxBF.getTnxId(),
					creditTnxBF.getNominalCode(), refundedPaxOndChargeIds, composedReservationPaxOndPayment, isVoidReservation);

			/*
			 * validate whether all credits of the selected payment has been refunded.
			 */
			if (isManualRefund != null && isManualRefund
					&& (composedReservationPaxOndPayment == null || composedReservationPaxOndPayment.size() == 0)) {
				throw new ModuleException("airreservations.revac.refund.no.payments");
			}

			// total credit consumed
			if (composedReservationPaxOndPayment != null && composedReservationPaxOndPayment.size() > 0) {

				if (creditUtilizedMapByTnx == null) {
					creditUtilizedMapByTnx = new HashMap<Long, Map<String, BigDecimal>>();
				}

				TnxGranularityUtils.extractPaymentCreditConsumedSummaryMap(creditUtilizedMapByTnx,
						composedReservationPaxOndPayment);

				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.saveReservationPaxOndPayment(composedReservationPaxOndPayment);
			}

			PerPaxPriceBreakdown perPaxPriceBreakdown = TnxGranularityUtils
					.composePerPaxPriceBreakdown(composedReservationPaxOndPayment);
			perPaxPriceBreakdown.setTotalBasePayAmount(creditTnxBF.getAmount());

			TnxBreakDownFactory.saveReservationPaxTnxBreakdownForRefund(creditTnxBF, payCurrencyDTO, perPaxPriceBreakdown);

		} else {
			TnxBreakDownFactory.saveReservationPaxTnxBreakdownForRefund(creditTnxBF, payCurrencyDTO);
		}
	}

	/**
	 * get PayCurrencyAmount By Tnx Breakdown
	 * 
	 * @param creditTnxBF
	 * @param payCurrencyDTO
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getPayCurrencyAmountByTnxBreakdown(Integer originalPaymentTnxId, BigDecimal remainingAmount,
			PayCurrencyDTO payCurrencyDTO) throws ModuleException {
		
		BigDecimal totalPayCurrencyAmount = BigDecimal.ZERO;

		Collection<String> colChargeGroupCodes = TnxGranularityRules.getRefundChargeGroupFulFillmentOrder();

		Collection<Long> colPaymentTnxIds = new ArrayList<Long>();
		colPaymentTnxIds.add(Long.valueOf(originalPaymentTnxId));

		boolean isVoidReservation = false;

		if (originalPaymentTnxId != null) {
			isVoidReservation = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
					.isVoidReservationByPnrPaxIdOrPayTnxId(originalPaymentTnxId.toString(), "T");
		}

		Map<Long, Collection<ReservationPaxOndPayment>> remainingPayments = getRefundEligiblePaymentMetaInfo(colPaymentTnxIds,
				colChargeGroupCodes, null, isVoidReservation, true);

		BigDecimal selTnxAvailCreditAmount = TnxGranularityUtils.getTotalAvailableCredit(remainingPayments);
		BigDecimal requiredCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (remainingAmount.doubleValue() > selTnxAvailCreditAmount.doubleValue()) {
			requiredCredit = AccelAeroCalculator.subtract(remainingAmount, selTnxAvailCreditAmount);
			remainingAmount = selTnxAvailCreditAmount;
		}

		Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		for (Collection<ReservationPaxOndPayment> colReservationPaxOndPayment : remainingPayments.values()) {
			if (remainingAmount.doubleValue() > 0) {
				for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {

					if (remainingAmount.doubleValue() > 0) {
						BigDecimal amount = reservationPaxOndPayment.getAmount();
						Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
						reservationPaxOndPayment = reservationPaxOndPayment.clone();
						reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
						if (remainingAmount.doubleValue() > amount.doubleValue()) {
							reservationPaxOndPayment.setAmount(amount.negate());
							remainingAmount = AccelAeroCalculator.subtract(remainingAmount, amount);
						} else {
							reservationPaxOndPayment.setAmount(remainingAmount.negate());
							remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						}
						composedReservationPaxOndPayment.add(reservationPaxOndPayment);
					} else {
						break;
					}
				}
			} else {
				break;
			}
		}

		// consuming from other payment credits
		consumePaxCreditFromOtherPayments(requiredCredit, colPaymentTnxIds, colChargeGroupCodes, null, null, null,
				composedReservationPaxOndPayment, isVoidReservation);

		Collection<Long> pnrPaxOndChgIds = new ArrayList<Long>();
		for (ReservationPaxOndPayment reservationPaxOndPayment : composedReservationPaxOndPayment) {
			pnrPaxOndChgIds.add(reservationPaxOndPayment.getPnrPaxOndChgId());
		}

		Map<Long, ReservationPaxOndCharge> reservationPaxOndCharges = new HashMap<Long, ReservationPaxOndCharge>();
		if (pnrPaxOndChgIds.size() > 0) {
			reservationPaxOndCharges = getRefundEligibleChargeMetaInfo(pnrPaxOndChgIds);
		}
		Date fareApplicableExRateTimestamp = getFareApplicableExRateTimestamp(reservationPaxOndCharges);

		ReservationPaxOndCharge reservationPaxOndCharge;
		CurrencyExchangeRate currencyExchangeRate;
		ReservationTnx reservationTnx;
		Map<Long, ArrayList<ReservationPaxOndPayment>> exchangerateReservationOndPaymentMap = new HashMap<Long, ArrayList<ReservationPaxOndPayment>>();
		Map<Long, CurrencyExchangeRate> idExchangeRateMap = new HashMap<Long, CurrencyExchangeRate>();
		for (ReservationPaxOndPayment reservationPaxOndPayment : composedReservationPaxOndPayment) {
			
			ArrayList<ReservationPaxOndPayment> exchangerateResOndPaymentList = new ArrayList<ReservationPaxOndPayment>();
			
			reservationPaxOndCharge = reservationPaxOndCharges.get(reservationPaxOndPayment.getPnrPaxOndChgId());
			Date applicableExRateTimestamp = reservationPaxOndCharge.getZuluChargeDate();
			if (reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.CNX)
					&& fareApplicableExRateTimestamp != null) {
				applicableExRateTimestamp = fareApplicableExRateTimestamp;
			} else if (reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.PEN)
					|| reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.ADJ)) {
				reservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
						.getTransaction(reservationPaxOndPayment.getPaymentTnxId());
				if (reservationTnx != null && reservationTnx.getDateTime() != null) {
					applicableExRateTimestamp = reservationTnx.getDateTime();
				}
			}
			currencyExchangeRate = new ExchangeRateProxy(applicableExRateTimestamp)
					.getCurrencyExchangeRate(payCurrencyDTO.getPayCurrencyCode());

			idExchangeRateMap.put(currencyExchangeRate.getExchangeRateId(), currencyExchangeRate);
			
			if(exchangerateReservationOndPaymentMap.containsKey(currencyExchangeRate.getExchangeRateId())){
				exchangerateReservationOndPaymentMap.get(currencyExchangeRate.getExchangeRateId()).add(reservationPaxOndPayment);
			}else{
				exchangerateResOndPaymentList.add(reservationPaxOndPayment);
				exchangerateReservationOndPaymentMap.put(currencyExchangeRate.getExchangeRateId(), exchangerateResOndPaymentList);
			}
		}
		
		if (payCurrencyDTO != null) {
			ReservationTotalPayCurrencyGenarator payCurrencygenarator = new ReservationTotalPayCurrencyGenarator(payCurrencyDTO);
			totalPayCurrencyAmount = payCurrencygenarator.getTotalPayByCurrency(exchangerateReservationOndPaymentMap,
					idExchangeRateMap);

		}
		
		if (payCurrencyDTO != null && payCurrencyDTO.getBoundaryValue() != null
				&& payCurrencyDTO.getBoundaryValue().doubleValue() != 0 && payCurrencyDTO.getBreakPointValue() != null) {
			totalPayCurrencyAmount = AccelAeroRounderPolicy.getRoundedValue(totalPayCurrencyAmount,
					payCurrencyDTO.getBoundaryValue(), payCurrencyDTO.getBreakPointValue());
		}

		totalPayCurrencyAmount = AccelAeroCalculator.scaleDefaultRoundingDown(totalPayCurrencyAmount);
		return totalPayCurrencyAmount;
	}

	private static Date getFareApplicableExRateTimestamp(Map<Long, ReservationPaxOndCharge> reservationPaxOndCharges) {
		for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxOndCharges.values()) {
			if (reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
				return reservationPaxOndCharge.getZuluChargeDate();
			}
		}
		return null;
	}

	/**
	 * record the tnx breakdown for credit CF TODO remove this method.
	 * 
	 * @param pnrPaxId
	 * @param carryForwardAmount
	 * @param reservationTnxNominalCode
	 * @throws ModuleException
	 */
	public static void saveReservationPaxTnxBreakdownForCarryForwardCredit(String pnrPaxId, BigDecimal carryForwardAmount,
			ReservationTnxNominalCode reservationTnxNominalCode, boolean enableTransactionGranularity) throws ModuleException {
		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<String> colChargeGroupCodes = TnxGranularityRules.getCreditCarryForwardChargeGroupFulFillmentOrder();
			Collection<ReservationCredit> colReservationCredit = TnxGranularityRules.getCreditsCarryForwardPolicies(pnrPaxId);

			BigDecimal remainingAmount = carryForwardAmount;

			Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

			for (ReservationCredit reservationCredit : colReservationCredit) {
				Collection<ReservationCredit> tmpReservationCredit = new ArrayList<ReservationCredit>();
				tmpReservationCredit.add(reservationCredit);
				Collection<Long> colPaymentTnxIds = TnxGranularityUtils.getPaymentIds(tmpReservationCredit);
				Map<Long, Collection<ReservationPaxOndPayment>> remainingPayments = getRemainingPaymentMetaInfo(colPaymentTnxIds,
						colChargeGroupCodes);
				if (remainingAmount.doubleValue() > 0) {
					if (reservationCredit.getBalance().doubleValue() > 0) {
						Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = remainingPayments
								.get(reservationCredit.getTnxId());

						if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {

							for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {

								if (remainingAmount.doubleValue() > 0) {

									BigDecimal amount = reservationPaxOndPayment.getAmount();

									// Get the minus charge adjusted breakdown
									if (reservationPaxOndPayment.getAmount()
											.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) < 0) {
										amount = reservationPaxOndPayment.getAmount().negate();
									}

									Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
									reservationPaxOndPayment = reservationPaxOndPayment.clone();
									reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
									reservationPaxOndPayment.setRemarks(null);
									reservationPaxOndPayment.setNominalCode(reservationTnxNominalCode.getCode());
									if (remainingAmount.doubleValue() > amount.doubleValue()) {
										reservationPaxOndPayment.setAmount(amount.negate());
										remainingAmount = AccelAeroCalculator.subtract(remainingAmount, amount);
									} else {
										reservationPaxOndPayment.setAmount(remainingAmount.negate());
										remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
									}
									composedReservationPaxOndPayment.add(reservationPaxOndPayment);
								} else {
									break;
								}
							}
						}
					}
				} else {
					break;
				}
			}

			if (composedReservationPaxOndPayment != null && composedReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.saveReservationPaxOndPayment(composedReservationPaxOndPayment);
			}
		}

	}

	/**
	 * Record tnx breakdown for refundable. Used in cancellation ,modification and transfer infant flows.
	 * 
	 * @param pnrPaxId
	 * @param nominalCode
	 * @param reservationPaxPaymentMetaTO
	 * @param enableTransactionGranularity
	 * @param transactionSeq
	 * @throws ModuleException
	 */
	public static void saveReservationPaxTnxBreakdownForRefundables(String pnrPaxId, int nominalCode,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, boolean enableTransactionGranularity, Integer transactionSeq)
			throws ModuleException {

		Collection<ReservationPaxOndPayment> colRefundableMetaReservationPaxOndPayment = null;

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

			Map<Long, Collection<ReservationPaxOndPayment>> mapPaxOndPayments = reservationPaxPaymentMetaTO
					.getPaxWiseOndChargePayments();

			for (Entry<Long, ReservationPaxOndCharge> entry : reservationPaxPaymentMetaTO.getMapPerPaxWiseOndRefundableCharges()
					.entrySet()) {
				Long ondChargeId = entry.getKey();
				ReservationPaxOndCharge reservationPaxOndCharge = entry.getValue();

				BigDecimal actualAmount = reservationPaxOndCharge.getEffectiveAmount();

				Collection<ReservationPaxOndPayment> colNewReservationPaxOndPayment = TnxGranularityUtils
						.getRespectiveReservationPaxOndPayment(mapPaxOndPayments.get(ondChargeId), actualAmount);

				for (ReservationPaxOndPayment reservationPaxOndPayment : colNewReservationPaxOndPayment) {
					reservationPaxOndPayment.setPnrPaxId(pnrPaxId);
					reservationPaxOndPayment.setPnrPaxOndChgId(Long.valueOf(reservationPaxOndCharge.getPnrPaxOndChgId()));
					reservationPaxOndPayment.setChargeGroupCode(reservationPaxOndCharge.getChargeGroupCode());
					reservationPaxOndPayment.setNominalCode(nominalCode);
					reservationPaxOndPayment.setRemarks(null);
					reservationPaxOndPayment.setTransactionSeq(transactionSeq);
				}

				colReservationPaxOndPayment.addAll(colNewReservationPaxOndPayment);
			}

			if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
			}

			colRefundableMetaReservationPaxOndPayment = TnxGranularityUtils.getReservationPaxOndPayments(mapPaxOndPayments);
		}

		setChargeCodesInReservationPaxOndPayments(colRefundableMetaReservationPaxOndPayment);

		Command balanceCreditCmd = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
		balanceCreditCmd.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		balanceCreditCmd.setParameter(CommandParamNames.REFUNDABLE_CHARGES, colRefundableMetaReservationPaxOndPayment);

		balanceCreditCmd.execute();
	}

	/**
	 * Rec the tnx breakdown for new chargers utilizing only the pax credit.
	 * 
	 * @param pnrPaxId
	 * @param operationTnxNominalCode
	 * @param reservationPaxPaymentMetaTO
	 * @param transactionSeq
	 * @throws ModuleException
	 */
	public static ReservationPaxTnxBreakdown saveReservationPaxTnxBrkForCreditsAndPaymentsComponsatingNewCharges(String pnrPaxId,
			ReservationTnxNominalCode operationTnxNominalCode, ReservationTnx creditTnxBF, PayCurrencyDTO payCurrencyDTO,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, boolean enableTransactionGranularity, Integer transactionSeq)
			throws ModuleException {

		saveReservationPaxTnxBrkForCreditsComponsatingNewCharges(pnrPaxId, operationTnxNominalCode, reservationPaxPaymentMetaTO,
				enableTransactionGranularity, transactionSeq);
		return saveReservationPaxTnxBreakdownForPayment(creditTnxBF, payCurrencyDTO, reservationPaxPaymentMetaTO,
				enableTransactionGranularity, transactionSeq);
	}

	/**
	 * Rec the tnx breakdown for new chargers utilizing only the pax credit.
	 * 
	 * @param pnrPaxId
	 * @param operationTnxNominalCode
	 * @param reservationPaxPaymentMetaTO
	 * @param transactionSeq
	 * @throws ModuleException
	 */
	public static void saveReservationPaxTnxBrkForCreditsComponsatingNewCharges(String pnrPaxId,
			ReservationTnxNominalCode operationTnxNominalCode, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			boolean enableTransactionGranularity, Integer transactionSeq) throws ModuleException {

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

			Collection<ReservationCredit> colReservationCredit = TnxGranularityRules.getCreditsUtilizationPolicies(pnrPaxId);

			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges = reservationPaxPaymentMetaTO
					.getColReservationPaxChargeMetaTOForNewCharges();

			setChargeCodeInPaxChargeMetaTO(colReservationPaxChargeMetaTOForNewCharges);

			// Fill charges using pax credit
			colReservationPaxOndPayment.addAll(fillInNewChargesFromAvailableCredits(pnrPaxId, operationTnxNominalCode,
					colReservationPaxChargeMetaTOForNewCharges, colReservationCredit, transactionSeq));

			if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
			}
		}

		Command balanceCreditCmd = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
		balanceCreditCmd.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);

		balanceCreditCmd.execute();
	}

	public static void saveReservationPaxTnxBrkForLMSPayments(String pnrPaxId, ReservationTnxNominalCode operationTnxNominalCode,
			ReservationTnx creditTnxBF, PayCurrencyDTO payCurrencyDTO, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			boolean enableTransactionGranularity, Integer transactionSeq, Map<String, BigDecimal> lmsProductRedeemedAmount)
			throws ModuleException {

		PerPaxPriceBreakdown perPaxPriceBreakdown;

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			setChargeCodeInPaxChargeMetaTO(reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges());
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = composeOndPaymentForLMS(pnrPaxId, creditTnxBF,
					reservationPaxPaymentMetaTO, transactionSeq, lmsProductRedeemedAmount);

			if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
			}

			Command balanceCreditCmd = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
			balanceCreditCmd.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);

			balanceCreditCmd.execute();

			perPaxPriceBreakdown = TnxGranularityUtils.composePerPaxPriceBreakdown(colReservationPaxOndPayment);
			perPaxPriceBreakdown.setTotalBasePayAmount(reservationPaxPaymentMetaTO.getPerPaxBaseTotalPayAmount());
		} else {
			perPaxPriceBreakdown = TnxGranularityUtils.composePerPaxPriceBreakdown(reservationPaxPaymentMetaTO);
		}

		TnxBreakDownFactory.saveReservationPaxTnxBreakdownForPayment(creditTnxBF, payCurrencyDTO, perPaxPriceBreakdown);
	}

	/**
	 * save tnx breakdown for adjustment
	 * 
	 * @param pnrPaxId
	 * @param operationTnx
	 * @param reservationPaxPaymentMetaTO
	 * @param transactionSeq
	 * @throws ModuleException
	 */
	public static void saveReservationPaxTnxBreakdownForAdjustments(String pnrPaxId, ReservationTnx operationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, boolean enableTransactionGranularity, Integer transactionSeq)
			throws ModuleException {

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

			// Start processing for minus adjustments
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO = reservationPaxPaymentMetaTO
					.getColPerPaxWiseOndNewAdjustments(true);

			if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
				for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colReservationPaxChargeMetaTO) {
					ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
					reservationPaxOndPayment.setPnrPaxId(pnrPaxId.toString());
					reservationPaxOndPayment.setAmount(reservationPaxChargeMetaTO.getAmount());
					reservationPaxOndPayment.setPaymentTnxId(new Long(operationTnx.getTnxId()));
					reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO.getPnrPaxOndChgId()));
					reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
					reservationPaxOndPayment.setNominalCode(ReservationTnxNominalCode.CREDIT_ADJUST.getCode());
					reservationPaxOndPayment.setRemarks(reservationPaxChargeMetaTO.getRemarks());
					reservationPaxOndPayment.setTransactionSeq(transactionSeq);
					colReservationPaxOndPayment.add(reservationPaxOndPayment);
				}
			}

			// Start processing for plus adjustments + new charges
			colReservationPaxChargeMetaTO = reservationPaxPaymentMetaTO.getColPerPaxWiseOndNewAdjustments(false);
			colReservationPaxChargeMetaTO.addAll(reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges());

			if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
				Collection<ReservationCredit> colReservationCredit = TnxGranularityRules.getCreditsUtilizationPolicies(pnrPaxId);

				setChargeCodeInPaxChargeMetaTO(colReservationPaxChargeMetaTO);

				// Fill in for the cancellation
				colReservationPaxOndPayment
						.addAll(fillInNewChargesFromAvailableCredits(pnrPaxId, ReservationTnxNominalCode.CREDIT_ADJUST,
								colReservationPaxChargeMetaTO, colReservationCredit, transactionSeq));
			}

			if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
			}
		}

		// Adjust credit
		Command balanceCreditCmd = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
		balanceCreditCmd.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);

		balanceCreditCmd.execute();
	}

	private static Map<Long, Collection<ReservationPaxOndPayment>> getRemainingPaymentMetaInfo(Collection<Long> colPaymentTnxIds,
			Collection<String> colChargeGroupCodes) throws ModuleException {
		Map<Long, Collection<ReservationPaxOndPayment>> mapPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getReservationPaxOndPayments(colPaymentTnxIds);

		Map<Long, Collection<ReservationPaxOndPayment>> remainingAmounts = new HashMap<Long, Collection<ReservationPaxOndPayment>>();

		for (Entry<Long, Collection<ReservationPaxOndPayment>> entry : mapPayments.entrySet()) {
			Long paymentTnxId = entry.getKey();
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = entry.getValue();

			Collection<ReservationPaxOndPayment> colPayments = TnxGranularityUtils
					.getPaymentMetaInfo(colReservationPaxOndPayment);
			Collection<ReservationPaxOndPayment> colRefunds = TnxGranularityUtils.getRefundMetaInfo(colReservationPaxOndPayment);

			remainingAmounts.put(paymentTnxId, searchRemaininingPayments(colPayments, colRefunds, colChargeGroupCodes, null));
		}

		return remainingAmounts;
	}

	/**
	 * This will give refund eligible ReservationPaxOndPayment for a given set of payments When linking refund to
	 * payments we need to consider ADJ,MOD,CNX as well. Not only payment NC
	 * 
	 * @param colPaymentTnxIds
	 * @param colChargeGroupCodes
	 * @param pnrPaxOndChgIds
	 *            PaxOndChgID list related to the segment that charges should be refunded from.
	 * @param isVoidReservation
	 * @param isParseCredit
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Long, Collection<ReservationPaxOndPayment>> getRefundEligiblePaymentMetaInfo(
			Collection<Long> colPaymentTnxIds, Collection<String> colChargeGroupCodes, Collection<Long> pnrPaxOndChgIds,
			boolean isVoidReservation, boolean isParseCredit) throws ModuleException {
		Map<Long, Collection<ReservationPaxOndPayment>> mapPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getReservationPaxOndPayments(colPaymentTnxIds);

		Map<Long, Collection<ReservationPaxOndPayment>> remainingAmounts = new HashMap<Long, Collection<ReservationPaxOndPayment>>();

		for (Entry<Long, Collection<ReservationPaxOndPayment>> entry : mapPayments.entrySet()) {
			Long paymentTnxId = entry.getKey();
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = entry.getValue();

			List<Integer> paymentNominalCodes = new ArrayList<Integer>();
			paymentNominalCodes.addAll(ReservationTnxNominalCode.getPaymentTypeNominalCodes());
			paymentNominalCodes.addAll(ReservationTnxNominalCode.getAdjModCnxNominalCodes());

			Collection<ReservationPaxOndPayment> colPayments = TnxGranularityUtils
					.getFilteredMetaInfo(colReservationPaxOndPayment, paymentNominalCodes);
			Collection<ReservationPaxOndPayment> colRefunds = TnxGranularityUtils.getFilteredMetaInfo(colReservationPaxOndPayment,
					ReservationTnxNominalCode.getRefundTypeNominalCodes());

			remainingAmounts.put(paymentTnxId,
					searchRemaininingPayments(colPayments, colRefunds, colChargeGroupCodes, pnrPaxOndChgIds));
		}

		transformValidCreditToPayment(remainingAmounts, isVoidReservation);

		if (isParseCredit) {
			parseAvailableCreditAsPayment(remainingAmounts, colPaymentTnxIds);
		}
		return remainingAmounts;
	}

	private static void parseAvailableCreditAsPayment(Map<Long, Collection<ReservationPaxOndPayment>> remainingAmounts,
			Collection<Long> colPaymentTnxIds) {

		for (Long paymentTnxId : colPaymentTnxIds) {

			Long otherPaymentTnxId = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getCreditReInstatedTnxIds(paymentTnxId);

			if (otherPaymentTnxId != null) {

				Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
						.getCreditExpiredPaxOndPayments(otherPaymentTnxId);

				if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
					remainingAmounts.put(paymentTnxId, colReservationPaxOndPayment);
				}

			}
			transformValidCreditToPayment(remainingAmounts, true);

		}

	}

	private static void transformValidCreditToPayment(Map<Long, Collection<ReservationPaxOndPayment>> remainingAmounts,
			boolean isVoidReservation) {
		for (Long paymentTnxId : remainingAmounts.keySet()) {
			// get payment credit
			Collection<ReservationPaxPaymentCredit> resPaxPayCreditColl = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO
					.getReservationPaxPaymentCredit(paymentTnxId);

			Collection<ReservationPaxOndPayment> paymentColl = remainingAmounts.get(paymentTnxId);

			if (paymentColl != null && paymentColl.size() > 0) {
				Iterator<ReservationPaxOndPayment> paymentItr = paymentColl.iterator();
				while (paymentItr.hasNext()) {
					ReservationPaxOndPayment resPaxPayment = paymentItr.next();
					if (!retreiveEffectiveCreditAmount(resPaxPayment, resPaxPayCreditColl, isVoidReservation)) {
						paymentItr.remove();
					}

				}
			}

		}

	}

	private static boolean retreiveEffectiveCreditAmount(ReservationPaxOndPayment resPaxPayment,
			Collection<ReservationPaxPaymentCredit> resPaxPayCreditColl, boolean isVoidReservation) {

		boolean isCreditFound = false;
		BigDecimal totalCreditAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal actualPaymentAmount = resPaxPayment.getAmount();
		if (resPaxPayment != null && resPaxPayCreditColl != null && resPaxPayCreditColl.size() > 0) {

			/**
			 * always allow only if charge is paid for a refundable charge only
			 */
			boolean isRefundableCharge = true;
			if (!isVoidReservation) {
				isRefundableCharge = ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO
						.isRefundableCharge(resPaxPayment.getPnrPaxOndChgId());
			}

			if (isRefundableCharge) {

				Collection<ReservationPaxPaymentCredit> selectedPaymentCredits = TnxGranularityUtils
						.getApplicablePaymentCreditByChargeGroup(resPaxPayment.getChargeGroupCode(), resPaxPayCreditColl, false);

				if (selectedPaymentCredits != null && selectedPaymentCredits.size() > 0) {
					for (ReservationPaxPaymentCredit resPaxPayCredit : selectedPaymentCredits) {

						// if (resPaxPayCreditColl != null && resPaxPayCreditColl.size() > 0) {
						//
						// for (ReservationPaxPaymentCredit resPaxPayCredit : resPaxPayCreditColl) {

						BigDecimal currentCredit = resPaxPayCredit.getAmount();
						BigDecimal effectiveAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

						if (currentCredit.doubleValue() > 0 && actualPaymentAmount.doubleValue() > 0) {

							if (currentCredit.doubleValue() > actualPaymentAmount.doubleValue()) {
								effectiveAmount = actualPaymentAmount;
							} else {
								effectiveAmount = currentCredit;
							}

							if (effectiveAmount.doubleValue() > 0) {
								totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, effectiveAmount);

								resPaxPayCredit.setAmount(AccelAeroCalculator.subtract(currentCredit, effectiveAmount));

								actualPaymentAmount = AccelAeroCalculator.subtract(actualPaymentAmount, effectiveAmount);

								isCreditFound = true;
							}

						}

						// allow -adjustment to be refunded when it generates credit
						Collection<Integer> otherPossibleCreditNCColl = new ArrayList<Integer>();
						otherPossibleCreditNCColl.addAll(ReservationTnxNominalCode.getAdjModCnxNominalCodes());
						// when credit re-instated from expired credit
						otherPossibleCreditNCColl.add(ReservationTnxNominalCode.CREDIT_ACQUIRE.getCode());

						if (!isCreditFound && otherPossibleCreditNCColl.contains(resPaxPayment.getNominalCode())
								&& currentCredit.doubleValue() > 0 && actualPaymentAmount.doubleValue() < 0) {
							BigDecimal amount = actualPaymentAmount.negate();
							if (currentCredit.doubleValue() > amount.doubleValue()) {
								effectiveAmount = amount;
							} else {
								effectiveAmount = currentCredit;
							}

							if (effectiveAmount.doubleValue() > 0) {
								totalCreditAmount = AccelAeroCalculator.add(totalCreditAmount, effectiveAmount);

								resPaxPayCredit.setAmount(AccelAeroCalculator.subtract(currentCredit, effectiveAmount));

								actualPaymentAmount = AccelAeroCalculator.subtract(amount, effectiveAmount);

								isCreditFound = true;
								
								if (resPaxPayment.getChargeGroupCode() == null
										&& ReservationTnxNominalCode.CREDIT_ACQUIRE.getCode() == resPaxPayment.getNominalCode()) {
									resPaxPayment.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.ADJ);
								}
							}

						}

					}

				}

			}

		}

		resPaxPayment.setAmount(totalCreditAmount);

		return isCreditFound;
	}

	private static Map<Long, ReservationPaxOndCharge> getRefundEligibleChargeMetaInfo(Collection<Long> pnrPaxOndChgIds)
			throws ModuleException {
		return ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO.getReservationPaxOndCharges(pnrPaxOndChgIds);
	}

	private static Collection<ReservationPaxOndPayment> searchRemaininingPayments(
			Collection<ReservationPaxOndPayment> colPayments, Collection<ReservationPaxOndPayment> colRefunds,
			Collection<String> colChargeGroupCodes, Collection<Long> pnrPaxOndChgIds) {

		Collection<ReservationPaxOndPayment> remainingPayments = new ArrayList<ReservationPaxOndPayment>();

		for (String chargeGroupCode : colChargeGroupCodes) {

			Collection<ReservationPaxOndPayment> selectedPayments = TnxGranularityUtils.getApplicableChargeTypes(chargeGroupCode,
					colPayments);

			if (selectedPayments.size() > 0) {

				Collection<ReservationPaxOndPayment> selectedRefunds = TnxGranularityUtils
						.getApplicableChargeTypes(chargeGroupCode, colRefunds);

				if (selectedRefunds.size() == 0) {
					remainingPayments.addAll(selectedPayments);
				} else {

					Map<Long, BigDecimal> mapPayments = TnxGranularityUtils.getOndChargeWiseAmounts(selectedPayments);
					Map<Long, BigDecimal> mapRefunds = TnxGranularityUtils.getOndChargeWiseAmounts(selectedRefunds);

					for (Entry<Long, BigDecimal> entry : mapPayments.entrySet()) {

						if (mapRefunds.containsKey(entry.getKey())) {
							BigDecimal paymentAmount = entry.getValue().abs();
							BigDecimal refundAmount = mapRefunds.get(entry.getKey()).abs();
							BigDecimal difference = AccelAeroCalculator.subtract(paymentAmount, refundAmount);
							if (difference.doubleValue() > 0) {
								ReservationPaxOndPayment reservationPaxOndPayment = BeanUtils.getFirstElement(selectedPayments);
								Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
								reservationPaxOndPayment = reservationPaxOndPayment.clone();
								reservationPaxOndPayment.setAmount(difference);
								reservationPaxOndPayment.setPnrPaxOndChgId(entry.getKey());
								reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);
								remainingPayments.add(reservationPaxOndPayment);
							}
						} else {
							Collection<ReservationPaxOndPayment> ondPaymentsForCharge = TnxGranularityUtils
									.getOndPaymentsForCharge(colPayments, entry.getKey());
							remainingPayments.addAll(ondPaymentsForCharge);
						}
					}
				}
			}
		}

		/*
		 * Filter the payments so only the relevant payments for the passed pnrPaxOndChgIds list is returned. If the
		 * list is not null or not empty.
		 */
		if (pnrPaxOndChgIds != null && !pnrPaxOndChgIds.isEmpty()) {
			List<ReservationPaxOndPayment> temp = new ArrayList<ReservationPaxOndPayment>();

			for (ReservationPaxOndPayment payment : remainingPayments) {
				if (pnrPaxOndChgIds.contains(payment.getPnrPaxOndChgId())) {
					temp.add(payment);
				}
			}
			remainingPayments = temp;
		}
		return remainingPayments;
	}

	private static Collection<ReservationPaxOndPayment> composeOndPayment(Integer pnrPaxId, ReservationTnx reservationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, Integer transactionSeq) throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();
		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO = new TnxGranularityFullFillmentDeriverBO(
				reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges())
						.execute(reservationTnx.getAmount().abs(), pnrPaxId);

		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {

			for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colReservationPaxChargeMetaTO) {
				ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
				reservationPaxOndPayment.setPnrPaxId(pnrPaxId.toString());
				reservationPaxOndPayment.setAmount(reservationPaxChargeMetaTO.getAmount());
				reservationPaxOndPayment.setPaymentTnxId(new Long(reservationTnx.getTnxId()));
				reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO.getPnrPaxOndChgId()));
				reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
				reservationPaxOndPayment.setNominalCode(reservationTnx.getNominalCode());
				reservationPaxOndPayment.setRemarks(reservationPaxChargeMetaTO.getRemarks());
				reservationPaxOndPayment.setTransactionSeq(transactionSeq);
				colReservationPaxOndPayment.add(reservationPaxOndPayment);
			}
		}

		return colReservationPaxOndPayment;
	}

	private static Collection<ReservationPaxOndPayment> composeOndPaymentForLMS(String pnrPaxId, ReservationTnx reservationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, Integer transactionSeq,
			Map<String, BigDecimal> lmsProductRedeemedAmount) throws ModuleException {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		Collection<ReservationCredit> colReservationCredit = TnxGranularityRules.getCreditsUtilizationPolicies(pnrPaxId);

		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges = reservationPaxPaymentMetaTO
				.getColReservationPaxChargeMetaTOForNewCharges();

		setChargeCodeInPaxChargeMetaTO(colReservationPaxChargeMetaTOForNewCharges);

		// Fill charges using pax credit
		colReservationPaxOndPayment.addAll(fillInNewChargesFromAvailableCredits(pnrPaxId, ReservationTnxNominalCode.CREDIT_BF,
				colReservationPaxChargeMetaTOForNewCharges, colReservationCredit, transactionSeq));

		List<LoyaltyProduct> loyaltyProducts = ReservationModuleUtils.getLoyaltyManagementBD().getLoyaltyProducts();
		Map<String, Set<String>> productWiseChargeCodes = new HashMap<String, Set<String>>();
		for (LoyaltyProduct loyaltyProduct : loyaltyProducts) {
			productWiseChargeCodes.put(loyaltyProduct.getProductId(), loyaltyProduct.getChargeCodes());
		}

		// Initially record other product payments
		for (String lmsProductCode : lmsProductRedeemedAmount.keySet()) {
			BigDecimal redeemAmount = lmsProductRedeemedAmount.get(lmsProductCode);
			Set<String> productChargeCodes = productWiseChargeCodes.get(lmsProductCode);
			if (productChargeCodes != null && productChargeCodes.size() > 0) {
				for (String chargeCode : productChargeCodes) {
					if (redeemAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						redeemAmount = preparePaymentForMatchigCharge(chargeCode,
								reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges(),
								colReservationPaxOndPayment, Integer.parseInt(pnrPaxId), redeemAmount, reservationTnx,
								transactionSeq);
					}
				}
			}
		}

		// Finally record FLIGHT product payments
		if (lmsProductRedeemedAmount.get(ReservationInternalConstants.LOYALTY_PRODUCTS.FLIGHT.toString()) != null) {
			BigDecimal redeemAmount = lmsProductRedeemedAmount
					.get(ReservationInternalConstants.LOYALTY_PRODUCTS.FLIGHT.toString());
			if (redeemAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				preparePaymentForCommonProduct(reservationPaxPaymentMetaTO.getColReservationPaxChargeMetaTOForNewCharges(),
						colReservationPaxOndPayment, Integer.parseInt(pnrPaxId), redeemAmount, reservationTnx, transactionSeq,
						productWiseChargeCodes);
			}
		}

		return colReservationPaxOndPayment;
	}

	private static BigDecimal preparePaymentForMatchigCharge(String chargeCode,
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment, Integer pnrPaxId, BigDecimal redeemAmount,
			ReservationTnx reservationTnx, Integer transactionSeq) {

		Iterator<ReservationPaxChargeMetaTO> paxChargeIte = colReservationPaxChargeMetaTOForNewCharges.iterator();
		while (paxChargeIte.hasNext()) {
			ReservationPaxChargeMetaTO reservationPaxChargeMetaTO = paxChargeIte.next();
			if (reservationPaxChargeMetaTO.getChargeCode() != null
					&& reservationPaxChargeMetaTO.getChargeCode().equals(chargeCode)) {
				if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) == 0) {
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							reservationPaxChargeMetaTO.getAmount(), reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					paxChargeIte.remove();
					break;
				} else if (AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(), redeemAmount).abs()
						.compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0) {
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							reservationPaxChargeMetaTO.getAmount(), reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					paxChargeIte.remove();
					break;
				} else if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) < 0) {
					BigDecimal remainingCharge = AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(),
							redeemAmount);
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							redeemAmount, reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					reservationPaxChargeMetaTO.setAmount(remainingCharge);
					break;
				} else if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) > 0) {
					BigDecimal remainingRedeemAmount = AccelAeroCalculator.subtract(redeemAmount,
							reservationPaxChargeMetaTO.getAmount());
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							reservationPaxChargeMetaTO.getAmount(), reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					redeemAmount = remainingRedeemAmount;
					paxChargeIte.remove();
				}
			}
		}

		return redeemAmount;

	}

	private static void preparePaymentForCommonProduct(
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment, Integer pnrPaxId, BigDecimal redeemAmount,
			ReservationTnx reservationTnx, Integer transactionSeq, Map<String, Set<String>> productWiseChargeCodes) {

		// First record fare payments
		Iterator<ReservationPaxChargeMetaTO> paxChargeIte = colReservationPaxChargeMetaTOForNewCharges.iterator();
		while (paxChargeIte.hasNext()) {
			ReservationPaxChargeMetaTO reservationPaxChargeMetaTO = paxChargeIte.next();
			if (reservationPaxChargeMetaTO.getChargeCode() == null
					&& reservationPaxChargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
				if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) == 0) {
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							reservationPaxChargeMetaTO.getAmount(), reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					paxChargeIte.remove();
					redeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					break;
				} else if (AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(), redeemAmount).abs()
						.compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0) {
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							reservationPaxChargeMetaTO.getAmount(), reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					paxChargeIte.remove();
					redeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					break;
				} else if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) < 0) {
					BigDecimal remainingCharge = AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(),
							redeemAmount);
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							redeemAmount, reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					reservationPaxChargeMetaTO.setAmount(remainingCharge);
					redeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					break;
				} else if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) > 0) {
					BigDecimal remainingRedeemAmount = AccelAeroCalculator.subtract(redeemAmount,
							reservationPaxChargeMetaTO.getAmount());
					addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(), pnrPaxId.toString(),
							reservationPaxChargeMetaTO.getAmount(), reservationPaxChargeMetaTO.getChargeGroupCode(),
							reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
					redeemAmount = remainingRedeemAmount;
					paxChargeIte.remove();
				}
			}
		}

		// Record other surcharges
		ArrayList<ReservationPaxChargeMetaTO> paxChargeMetaTOs = new ArrayList<ReservationPaxChargeMetaTO>(
				colReservationPaxChargeMetaTOForNewCharges);

		Comparator<ReservationPaxChargeMetaTO> chargeCodeComparator = new Comparator<ReservationPaxChargeMetaTO>() {
			public int compare(ReservationPaxChargeMetaTO c1, ReservationPaxChargeMetaTO c2) {
				if (c1.getChargeCode() != null && c2.getChargeCode() != null) {
					return c1.getChargeCode().compareTo(c2.getChargeCode());
				} else if (c1.getChargeCode() == null && c2.getChargeCode() == null) {
					return 0;
				} else {
					return 1;
				}

			}
		};

		Collections.sort(paxChargeMetaTOs, chargeCodeComparator);

		if (redeemAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			Iterator<ReservationPaxChargeMetaTO> paxChargeIteSUR = paxChargeMetaTOs.iterator();
			Map<String, String> externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
			while (paxChargeIteSUR.hasNext()) {
				ReservationPaxChargeMetaTO reservationPaxChargeMetaTO = paxChargeIteSUR.next();
				if (reservationPaxChargeMetaTO.getChargeCode() != null
						&& isValidOtherSurcharge(reservationPaxChargeMetaTO.getChargeCode(), externalChargesMap,
								productWiseChargeCodes)
						&& reservationPaxChargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.SUR)
						|| reservationPaxChargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.INF)) {
					if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) == 0) {
						addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(),
								pnrPaxId.toString(), reservationPaxChargeMetaTO.getAmount(),
								reservationPaxChargeMetaTO.getChargeGroupCode(), reservationPaxChargeMetaTO.getPnrPaxOndChgId(),
								reservationTnx.getTnxId(), transactionSeq);
						consumeFromOriginalCharges(colReservationPaxChargeMetaTOForNewCharges, reservationPaxChargeMetaTO,
								reservationPaxChargeMetaTO.getAmount());
						paxChargeIteSUR.remove();
						break;
					} else if (AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(), redeemAmount).abs()
							.compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0) {
						addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(),
								pnrPaxId.toString(), reservationPaxChargeMetaTO.getAmount(),
								reservationPaxChargeMetaTO.getChargeGroupCode(), reservationPaxChargeMetaTO.getPnrPaxOndChgId(),
								reservationTnx.getTnxId(), transactionSeq);
						consumeFromOriginalCharges(colReservationPaxChargeMetaTOForNewCharges, reservationPaxChargeMetaTO,
								reservationPaxChargeMetaTO.getAmount());
						paxChargeIteSUR.remove();
						break;
					} else if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) < 0) {
						BigDecimal remainingCharge = AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(),
								redeemAmount);
						addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(),
								pnrPaxId.toString(), redeemAmount, reservationPaxChargeMetaTO.getChargeGroupCode(),
								reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationTnx.getTnxId(), transactionSeq);
						consumeFromOriginalCharges(colReservationPaxChargeMetaTOForNewCharges, reservationPaxChargeMetaTO,
								redeemAmount);
						break;
					} else if (redeemAmount.compareTo(reservationPaxChargeMetaTO.getAmount()) > 0) {
						BigDecimal remainingRedeemAmount = AccelAeroCalculator.subtract(redeemAmount,
								reservationPaxChargeMetaTO.getAmount());
						addReservationPaxOndPayment(colReservationPaxOndPayment, reservationTnx.getNominalCode(),
								pnrPaxId.toString(), reservationPaxChargeMetaTO.getAmount(),
								reservationPaxChargeMetaTO.getChargeGroupCode(), reservationPaxChargeMetaTO.getPnrPaxOndChgId(),
								reservationTnx.getTnxId(), transactionSeq);
						redeemAmount = remainingRedeemAmount;
						consumeFromOriginalCharges(colReservationPaxChargeMetaTOForNewCharges, reservationPaxChargeMetaTO,
								reservationPaxChargeMetaTO.getAmount());
						paxChargeIteSUR.remove();
					}
				}
			}
		}

	}

	private static boolean isValidOtherSurcharge(String chargeCode, Map<String, String> externalChargesMap,
			Map<String, Set<String>> productWiseChargeCodes) {
		if (chargeCode.equals(externalChargesMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD.toString()))) {
			return false;
		} else if (chargeCode
				.equals(externalChargesMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.FLEXI_CHARGES.toString()))) {
			return false;
		} else if (chargeCode.equals(externalChargesMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE.toString()))) {
			return false;
		} else if (chargeCode
				.equals(externalChargesMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.INSURANCE.toString()))) {
			return false;
		} else if (chargeCode.equals(externalChargesMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.MEAL.toString()))) {
			return false;
		} else if (chargeCode
				.equals(externalChargesMap.get(ReservationInternalConstants.EXTERNAL_CHARGES.AIRPORT_SERVICE.toString()))) {
			return false;
		} else if (productWiseChargeCodes.get(PRODUCT_CODE.BUNDLE_FARE.toString()).contains(chargeCode)) {
			return false;
		}
		return true;
	}

	private static void consumeFromOriginalCharges(
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			ReservationPaxChargeMetaTO reservationPaxChargeMetaTO, BigDecimal consumedChargeAmount) {
		Iterator<ReservationPaxChargeMetaTO> newChargesIte = colReservationPaxChargeMetaTOForNewCharges.iterator();
		while (newChargesIte.hasNext()) {
			ReservationPaxChargeMetaTO chargeMetaTO = newChargesIte.next();
			if ((reservationPaxChargeMetaTO.getChargeRateId() != null && chargeMetaTO.getChargeRateId() != null
					&& chargeMetaTO.getChargeRateId().equals(reservationPaxChargeMetaTO.getChargeRateId()))
					|| (reservationPaxChargeMetaTO.getChargeRateId() == null
							&& chargeMetaTO.getChargeGroupCode().equals(reservationPaxChargeMetaTO.getChargeGroupCode()))) {
				if (consumedChargeAmount.equals(chargeMetaTO.getAmount())) {
					newChargesIte.remove();
				} else {
					BigDecimal remainingChg = AccelAeroCalculator.subtract(chargeMetaTO.getAmount(), consumedChargeAmount);
					chargeMetaTO.setAmount(remainingChg);
				}
				break;
			}
		}
	}

	private static void consumeNewChargesFromExactChargeCodes(String pnrPaxId,
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			Collection<ReservationCredit> colReservationCredit, Collection<ReservationPaxOndPayment> colReservationPaxOndPayment,
			Integer transactionSeq) {

		if (colReservationPaxChargeMetaTOForNewCharges != null && colReservationPaxChargeMetaTOForNewCharges.size() > 0
				&& colReservationCredit != null && colReservationCredit.size() > 0) {

			Iterator<ReservationPaxChargeMetaTO> paxChargeMetaTOIte = colReservationPaxChargeMetaTOForNewCharges.iterator();
			while (paxChargeMetaTOIte.hasNext()) {
				ReservationPaxChargeMetaTO reservationPaxChargeMetaTO = paxChargeMetaTOIte.next();
				boolean chargeFullyConsumed = false;
				if (reservationPaxChargeMetaTO.getChargeCode() != null) { // consume charges
					Iterator<ReservationCredit> reservationCreditIte = colReservationCredit.iterator();
					chargeFullyConsumed = false;
					CRED_L: while (reservationCreditIte.hasNext()) {
						ReservationCredit reservationCredit = reservationCreditIte.next();
						if (reservationCredit.getBalance().doubleValue() > 0) {
							Iterator<ReservationPaxPaymentCredit> paymentCreditIte = reservationCredit.getPayments().iterator();

							PAY_L: while (paymentCreditIte.hasNext()) {
								ReservationPaxPaymentCredit reservationPaxPaymentCredit = paymentCreditIte.next();
								if (reservationCredit.getBalance().doubleValue() > 0
										&& reservationPaxPaymentCredit.getAmount().doubleValue() > 0) {

									if (reservationPaxChargeMetaTO.getChargeCode()
											.equals(reservationPaxPaymentCredit.getChargeCode())) {

										if (reservationPaxChargeMetaTO.getAmount()
												.compareTo(reservationPaxPaymentCredit.getAmount()) <= 0) {
											addReservationPaxOndPayment(colReservationPaxOndPayment,
													ReservationTnxNominalCode.CREDIT_PAYOFF.getCode(), pnrPaxId,
													reservationPaxChargeMetaTO.getAmount(),
													reservationPaxChargeMetaTO.getChargeGroupCode(),
													reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationCredit.getTnxId(),
													transactionSeq);

											if (reservationPaxChargeMetaTO.getAmount()
													.compareTo(reservationPaxPaymentCredit.getAmount()) < 0) {
												BigDecimal remainingCredit = AccelAeroCalculator.subtract(
														reservationPaxPaymentCredit.getAmount(),
														reservationPaxChargeMetaTO.getAmount());
												reservationPaxPaymentCredit.setAmount(remainingCredit);
											} else {
												paymentCreditIte.remove();
											}
											reservationCredit.setBalance(AccelAeroCalculator.subtract(
													reservationCredit.getBalance(), reservationPaxChargeMetaTO.getAmount()));
											chargeFullyConsumed = true;
											break PAY_L;
										} else if (reservationPaxChargeMetaTO.getAmount()
												.compareTo(reservationPaxPaymentCredit.getAmount()) > 0) {

											addReservationPaxOndPayment(colReservationPaxOndPayment,
													ReservationTnxNominalCode.CREDIT_PAYOFF.getCode(), pnrPaxId,
													reservationPaxPaymentCredit.getAmount(),
													reservationPaxChargeMetaTO.getChargeGroupCode(),
													reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationCredit.getTnxId(),
													transactionSeq);

											BigDecimal remainingCharge = AccelAeroCalculator.subtract(
													reservationPaxChargeMetaTO.getAmount(),
													reservationPaxPaymentCredit.getAmount());
											reservationPaxChargeMetaTO.setAmount(remainingCharge);
											reservationCredit.setBalance(AccelAeroCalculator.subtract(
													reservationCredit.getBalance(), reservationPaxPaymentCredit.getAmount()));
											// FIXME : some cases paymentCreditIte.remove() doesn't remove the item from
											// iterator as expected there for better update the value as zero
											reservationPaxPaymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											paymentCreditIte.remove();
										}
									}

								}
							}

							if (chargeFullyConsumed) {
								paxChargeMetaTOIte.remove();
								break CRED_L;
							}
						}

					}
				} else {
					// Consume Fares
					Iterator<ReservationCredit> reservationCreditIte = colReservationCredit.iterator();
					CRED_L: while (reservationCreditIte.hasNext()) {
						ReservationCredit reservationCredit = reservationCreditIte.next();
						if (reservationCredit.getBalance().doubleValue() > 0) {
							Iterator<ReservationPaxPaymentCredit> paymentCreditIte = reservationCredit.getPayments().iterator();
							chargeFullyConsumed = false;

							PAY_L: while (paymentCreditIte.hasNext()) {
								ReservationPaxPaymentCredit reservationPaxPaymentCredit = paymentCreditIte.next();
								if (reservationCredit.getBalance().doubleValue() > 0
										&& reservationPaxPaymentCredit.getAmount().doubleValue() > 0) {

									if (reservationPaxChargeMetaTO.getChargeGroupCode()
											.equals(reservationPaxPaymentCredit.getChargeGroupCode())) {

										if (reservationPaxChargeMetaTO.getAmount()
												.compareTo(reservationPaxPaymentCredit.getAmount()) <= 0) {

											addReservationPaxOndPayment(colReservationPaxOndPayment,
													ReservationTnxNominalCode.CREDIT_PAYOFF.getCode(), pnrPaxId,
													reservationPaxChargeMetaTO.getAmount(),
													reservationPaxChargeMetaTO.getChargeGroupCode(),
													reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationCredit.getTnxId(),
													transactionSeq);

											if (reservationPaxChargeMetaTO.getAmount()
													.compareTo(reservationPaxPaymentCredit.getAmount()) < 0) {
												BigDecimal remainingCredit = AccelAeroCalculator.subtract(
														reservationPaxPaymentCredit.getAmount(),
														reservationPaxChargeMetaTO.getAmount());
												reservationPaxPaymentCredit.setAmount(remainingCredit);

											} else {

												paymentCreditIte.remove();
											}
											reservationCredit.setBalance(AccelAeroCalculator.subtract(
													reservationCredit.getBalance(), reservationPaxChargeMetaTO.getAmount()));
											chargeFullyConsumed = true;
											break PAY_L;
										} else if (reservationPaxChargeMetaTO.getAmount()
												.compareTo(reservationPaxPaymentCredit.getAmount()) > 0) {

											addReservationPaxOndPayment(colReservationPaxOndPayment,
													ReservationTnxNominalCode.CREDIT_PAYOFF.getCode(), pnrPaxId,
													reservationPaxPaymentCredit.getAmount(),
													reservationPaxChargeMetaTO.getChargeGroupCode(),
													reservationPaxChargeMetaTO.getPnrPaxOndChgId(), reservationCredit.getTnxId(),
													transactionSeq);

											BigDecimal remainingCharge = AccelAeroCalculator.subtract(
													reservationPaxChargeMetaTO.getAmount(),
													reservationPaxPaymentCredit.getAmount());
											reservationPaxChargeMetaTO.setAmount(remainingCharge);
											reservationCredit.setBalance(AccelAeroCalculator.subtract(
													reservationCredit.getBalance(), reservationPaxPaymentCredit.getAmount()));

											// FIXME : some cases paymentCreditIte.remove() doesn't remove the item from
											// iterator as expected there for better update the value as zero
											reservationPaxPaymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											paymentCreditIte.remove();
										}
									}

								}
							}

							if (chargeFullyConsumed) {
								paxChargeMetaTOIte.remove();
								break CRED_L;
							}
						}

					}
				}

			}
		}
	}

	private static void addReservationPaxOndPayment(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment,
			int nominalCode, String pnrPaxId, BigDecimal amount, String chargeGroupCode, long paxOndChgId, long txnId,
			Integer transactionSeq) {
		ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
		reservationPaxOndPayment.setPnrPaxId(pnrPaxId);
		reservationPaxOndPayment.setAmount(amount);
		reservationPaxOndPayment.setPaymentTnxId(txnId);
		reservationPaxOndPayment.setPnrPaxOndChgId(new Long(paxOndChgId));
		reservationPaxOndPayment.setChargeGroupCode(chargeGroupCode);
		reservationPaxOndPayment.setNominalCode(nominalCode);
		reservationPaxOndPayment.setRemarks(null);
		reservationPaxOndPayment.setTransactionSeq(transactionSeq);
		colReservationPaxOndPayment.add(reservationPaxOndPayment);
	}

	private static Collection<ReservationPaxOndPayment> fillInNewChargesFromAvailableCredits(String pnrPaxId,
			ReservationTnxNominalCode reservationTnxNominalCode,
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			Collection<ReservationCredit> colReservationCredit, Integer transactionSeq) {

		Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

		consumeNewChargesFromExactChargeCodes(pnrPaxId, colReservationPaxChargeMetaTOForNewCharges, colReservationCredit,
				colReservationPaxOndPayment, transactionSeq);

		if (colReservationPaxChargeMetaTOForNewCharges != null && colReservationPaxChargeMetaTOForNewCharges.size() > 0) {

			Collection<String> colChargeGroupOrder = TnxGranularityRules.getPaymentChargeGroupFulFillmentOrder();

			ReservationCreditTO maxCreditAmountTO = TnxGranularityFullFillmentDeriverBO
					.adjustAvailableReservationCredit(colReservationPaxChargeMetaTOForNewCharges, colReservationCredit);

			for (String chargeGroupCode : colChargeGroupOrder) {
				Collection<ReservationPaxChargeMetaTO> colSelectedReservationPaxChargeMetaTO = TnxGranularityUtils
						.getChargeMetaTOByChargeGroupCode(chargeGroupCode, colReservationPaxChargeMetaTOForNewCharges);

				if (colSelectedReservationPaxChargeMetaTO != null && colSelectedReservationPaxChargeMetaTO.size() > 0) {

					ArrayList<ReservationPaxChargeMetaTO> paxChargeMetaTOs = new ArrayList<ReservationPaxChargeMetaTO>(
							colSelectedReservationPaxChargeMetaTO);

					Comparator<ReservationPaxChargeMetaTO> chargeCodeComparator = new Comparator<ReservationPaxChargeMetaTO>() {
						public int compare(ReservationPaxChargeMetaTO c1, ReservationPaxChargeMetaTO c2) {
							if (c1.getChargeCode() != null && c2.getChargeCode() != null) {
								return c1.getChargeCode().compareTo(c2.getChargeCode());
							} else if (c1.getChargeCode() == null && c2.getChargeCode() != null) {
								return 1;
							} else if (c1.getChargeCode() != null && c2.getChargeCode() == null) {
								return -1;
							} else {
								return 0;
							}

						}
					};

					Collections.sort(paxChargeMetaTOs, chargeCodeComparator);

					Collection<ReservationPaxChargeMetaTO> colAdjustedCharges = new ArrayList<ReservationPaxChargeMetaTO>();
					Collection<ReservationPaxChargeMetaTO> colRemovingCharges = new ArrayList<ReservationPaxChargeMetaTO>();

					for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : paxChargeMetaTOs) {

						BigDecimal chargeAmount = reservationPaxChargeMetaTO.getAmount();

						chargeAmount = TnxGranularityUtils.getAvailableMaxCredit(maxCreditAmountTO, chargeGroupCode,
								chargeAmount);
						BigDecimal availableCreditBalance = TnxGranularityUtils.getMaxChargeGroupCredit(maxCreditAmountTO,
								chargeGroupCode);
						availableCreditBalance = AccelAeroCalculator.add(chargeAmount, availableCreditBalance);

						Collection<ReservationCredit> appliedReservationCredit = null;

						appliedReservationCredit = getValidReservationCreditForCharge(chargeAmount, colReservationCredit,
								availableCreditBalance, chargeGroupCode);

						if (appliedReservationCredit != null && appliedReservationCredit.size() > 0) {
							reservationPaxChargeMetaTO.setAmount(reservationPaxChargeMetaTO.getAmount());

							Collection<ReservationPaxChargeMetaTO> adjustmentsForTheCharge = getAdjustedCharges(
									reservationPaxChargeMetaTO, appliedReservationCredit);

							if (adjustmentsForTheCharge.size() > 0) {
								colAdjustedCharges.addAll(adjustmentsForTheCharge);
							}

							colRemovingCharges.add(reservationPaxChargeMetaTO);
							for (ReservationCredit reservationCredit : appliedReservationCredit) {
								ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
								reservationPaxOndPayment.setPnrPaxId(pnrPaxId);
								reservationPaxOndPayment.setAmount(reservationCredit.getBalance());
								reservationPaxOndPayment.setPaymentTnxId(reservationCredit.getTnxId());
								reservationPaxOndPayment
										.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO.getPnrPaxOndChgId()));
								reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
								reservationPaxOndPayment.setNominalCode(reservationTnxNominalCode.getCode());
								reservationPaxOndPayment.setRemarks(null);
								reservationPaxOndPayment.setTransactionSeq(transactionSeq);
								colReservationPaxOndPayment.add(reservationPaxOndPayment);
							}
						}

					}

					if (colAdjustedCharges.size() > 0) {
						colReservationPaxChargeMetaTOForNewCharges.addAll(colAdjustedCharges);
					}

					if (colRemovingCharges.size() > 0) {
						colReservationPaxChargeMetaTOForNewCharges.removeAll(colRemovingCharges);
					}
				}
			}
		}

		return colReservationPaxOndPayment;
	}

	/**
	 * IF the total pax credit is not enough to pay for the charge then the remaining charge should be payed by external
	 * payments This will create a charge for the remaining balance to pay
	 * 
	 * @param reservationPaxChargeMetaTO
	 * @param appliedReservationCredit
	 * @return
	 */
	private static Collection<ReservationPaxChargeMetaTO> getAdjustedCharges(
			ReservationPaxChargeMetaTO reservationPaxChargeMetaTO, Collection<ReservationCredit> appliedReservationCredit) {

		BigDecimal totalBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (ReservationCredit reservationCredit : appliedReservationCredit) {
			totalBalance = AccelAeroCalculator.add(totalBalance, reservationCredit.getBalance());
		}

		Collection<ReservationPaxChargeMetaTO> colAdjustedCharges = new ArrayList<ReservationPaxChargeMetaTO>();

		if (totalBalance.doubleValue() != reservationPaxChargeMetaTO.getAmount().doubleValue()) {
			BigDecimal difference = AccelAeroCalculator.subtract(reservationPaxChargeMetaTO.getAmount(), totalBalance);
			reservationPaxChargeMetaTO = reservationPaxChargeMetaTO.clone();
			reservationPaxChargeMetaTO.setAmount(difference);
			colAdjustedCharges.add(reservationPaxChargeMetaTO);
		}

		return colAdjustedCharges;
	}

	/**
	 * Get the selected pax credits consumed to pay for new charges This will also modify the remaining pax credits to
	 * reflect the utilization to the charge
	 * 
	 * @param amount
	 * @param colReservationCredit
	 * @param groupBalance
	 * @param chargeGroupCode
	 * @return
	 */
	private static Collection<ReservationCredit> getValidReservationCreditForCharge(BigDecimal amount,
			Collection<ReservationCredit> colReservationCredit, BigDecimal groupBalance, String chargeGroupCode) {
		BigDecimal remainingAmount = AccelAeroCalculator.add(AccelAeroCalculator.getDefaultBigDecimalZero(), amount);

		Collection<ReservationCredit> selectedCredits = new ArrayList<ReservationCredit>();
		Collection<ReservationCredit> removingCredits = new ArrayList<ReservationCredit>();
		BigDecimal remainingCreditToConsume = AccelAeroCalculator.add(AccelAeroCalculator.getDefaultBigDecimalZero(), amount);
		for (ReservationCredit reservationCredit : colReservationCredit) {

			// returns applicable amount from current ReservationCredit record
			remainingAmount = TnxGranularityUtils.getEffectiveCredit(remainingCreditToConsume, reservationCredit,
					chargeGroupCode);

			remainingCreditToConsume = AccelAeroCalculator.subtract(remainingCreditToConsume, remainingAmount);

			if (remainingAmount.doubleValue() > 0 && reservationCredit.getBalance().doubleValue() > 0
					&& groupBalance.doubleValue() > 0) {
				// if (reservationCredit.getBalance().doubleValue() <= remainingAmount.doubleValue()) {
				if (groupBalance.doubleValue() <= remainingAmount.doubleValue()
						&& reservationCredit.getBalance().doubleValue() >= remainingAmount.doubleValue()) {

					ReservationCredit clonedReservationCredit = reservationCredit.clone();
					clonedReservationCredit.setBalance(remainingAmount);
					selectedCredits.add(clonedReservationCredit);

					BigDecimal availableCredit = AccelAeroCalculator.subtract(reservationCredit.getBalance(), remainingAmount);
					reservationCredit.setBalance(availableCredit);

					TnxGranularityFullFillmentDeriverBO.wipeOfCreditPayments(reservationCredit, clonedReservationCredit,
							remainingAmount, chargeGroupCode);

					if (reservationCredit.getBalance().doubleValue() == 0) {
						removingCredits.add(reservationCredit);
					}

					remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				} else if (groupBalance.doubleValue() >= remainingAmount.doubleValue()
						&& reservationCredit.getBalance().doubleValue() <= remainingAmount.doubleValue()) {

					BigDecimal effectiveAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

					remainingAmount = AccelAeroCalculator.subtract(remainingAmount, reservationCredit.getBalance());

					BigDecimal availableCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
					effectiveAmount = reservationCredit.getBalance();

					ReservationCredit clonedReservationCredit = reservationCredit.clone();
					clonedReservationCredit.setBalance(effectiveAmount);
					selectedCredits.add(clonedReservationCredit);

					reservationCredit.setBalance(availableCredit);

					TnxGranularityFullFillmentDeriverBO.wipeOfCreditPayments(reservationCredit, clonedReservationCredit,
							effectiveAmount, chargeGroupCode);

					if (reservationCredit.getBalance().doubleValue() == 0) {
						removingCredits.add(reservationCredit);
					}

				} else {

					BigDecimal remainingBalance = AccelAeroCalculator.subtract(reservationCredit.getBalance(), remainingAmount);
					reservationCredit.setBalance(remainingBalance);
					ReservationCredit clonedReservationCredit = reservationCredit.clone();
					clonedReservationCredit.setBalance(remainingAmount);

					TnxGranularityFullFillmentDeriverBO.wipeOfCreditPayments(reservationCredit, clonedReservationCredit,
							remainingAmount, chargeGroupCode);

					remainingAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					selectedCredits.add(clonedReservationCredit);
					// break;
				}
			}
		}

		if (removingCredits.size() > 0) {
			colReservationCredit.removeAll(removingCredits);
		}

		return selectedCredits;
	}

	private static BigDecimal convertAmount(BigDecimal totalPayAmount, PayCurrencyDTO payCurrencyDTO,
			CurrencyExchangeRate currencyExchangeRate) {
		if (AppSysParamsUtil.getBaseCurrency().equals(payCurrencyDTO.getPayCurrencyCode())) {
			return totalPayAmount;
		} else {
			return AccelAeroCalculator.multiply(totalPayAmount, currencyExchangeRate.getMultiplyingExchangeRate());
		}
	}

	private static void consumePaxCreditFromOtherPayments(BigDecimal requiredCredit, Collection<Long> colPaymentTnxIds,
			Collection<String> colChargeGroupCodes, Integer tnxId, Integer nominalCode, Set<Long> refundedPaxOndChargeIds,
			Collection<ReservationPaxOndPayment> composedReservationPaxOndPayment, boolean isVoidReservation)
			throws ModuleException {

		if (requiredCredit != null && requiredCredit.doubleValue() > 0 && colPaymentTnxIds != null
				&& colPaymentTnxIds.size() > 0) {

			Collection<Long> colOtherPaymentTnxIds = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getPaxCreditTransactionIds(colPaymentTnxIds);

			if (colOtherPaymentTnxIds != null && colOtherPaymentTnxIds.size() > 0) {
				Map<Long, Collection<ReservationPaxOndPayment>> otherPayments = getRefundEligiblePaymentMetaInfo(
						colOtherPaymentTnxIds, colChargeGroupCodes, null, isVoidReservation, true);

				for (Long tnxIds : otherPayments.keySet()) {

					Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = null;
					if (tnxIds != null) {
						colReservationPaxOndPayment = otherPayments.get(tnxIds);
					}

					if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0
							&& requiredCredit.doubleValue() > 0) {
						for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {

							if (requiredCredit.doubleValue() > 0) {
								BigDecimal amount = reservationPaxOndPayment.getAmount();
								Long originalPaxOndPaymentId = reservationPaxOndPayment.getPaxOndPaymentId();
								Long originalPaxOndChgId = reservationPaxOndPayment.getPnrPaxOndChgId();
								reservationPaxOndPayment = reservationPaxOndPayment.clone();
								reservationPaxOndPayment.setRemarks(null);
								reservationPaxOndPayment.setOriginalPaxOndPaymentId(originalPaxOndPaymentId);

								if (tnxId != null) {
									reservationPaxOndPayment.setPaxOndRefundId(tnxId);
								}

								if (nominalCode != null) {
									reservationPaxOndPayment.setNominalCode(nominalCode);
								}

								if (requiredCredit.doubleValue() > amount.doubleValue()) {
									reservationPaxOndPayment.setAmount(amount.negate());
									requiredCredit = AccelAeroCalculator.subtract(requiredCredit, amount);
								} else {
									reservationPaxOndPayment.setAmount(requiredCredit.negate());
									requiredCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
								}

								composedReservationPaxOndPayment.add(reservationPaxOndPayment);

								if (refundedPaxOndChargeIds != null) {
									refundedPaxOndChargeIds.add(originalPaxOndChgId);
								}
							} else {
								break;
							}
						}
					}

				}
			}

		}
	}

	private static void
			setChargeCodesInReservationPaxOndPayments(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment) {
		if (colReservationPaxOndPayment != null && !colReservationPaxOndPayment.isEmpty()) {
			Collection<Integer> ppocIds = new ArrayList<Integer>();
			for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
				ppocIds.add(reservationPaxOndPayment.getPnrPaxOndChgId().intValue());
			}

			Map<Long, String> ppocChargeCodeMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getChargeCodesForPaxOndPayments(ppocIds);
			if (ppocChargeCodeMap != null && ppocChargeCodeMap.size() > 0) {
				for (ReservationPaxOndPayment reservationPaxOndPayment : colReservationPaxOndPayment) {
					String chargeCode = ppocChargeCodeMap.get(reservationPaxOndPayment.getPnrPaxOndChgId());
					if (chargeCode != null) {
						reservationPaxOndPayment.setChargeCode(chargeCode);
					}
				}
			}
		}
	}

	private static void
			setChargeCodeInPaxChargeMetaTO(Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges) {
		if (colReservationPaxChargeMetaTOForNewCharges != null && !colReservationPaxChargeMetaTOForNewCharges.isEmpty()) {
			Collection<Integer> ppocIds = new ArrayList<Integer>();
			for (ReservationPaxChargeMetaTO reservationPaxOndCharges : colReservationPaxChargeMetaTOForNewCharges) {
				ppocIds.add(reservationPaxOndCharges.getPnrPaxOndChgId().intValue());
			}

			Map<Long, String> ppocChargeCodeMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
					.getChargeCodesForPaxOndPayments(ppocIds);
			if (ppocChargeCodeMap != null && ppocChargeCodeMap.size() > 0) {
				for (ReservationPaxChargeMetaTO reservationPaxOndCharges : colReservationPaxChargeMetaTOForNewCharges) {
					String chargeCode = ppocChargeCodeMap.get(reservationPaxOndCharges.getPnrPaxOndChgId().longValue());
					if (chargeCode != null) {
						reservationPaxOndCharges.setChargeCode(chargeCode);
					}
				}
			}
		}
	}

	public static void saveReservationPaxTnxBreakdownForChargesReversal(String pnrPaxId, ReservationTnx operationTnx,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, boolean enableTransactionGranularity, Integer transactionSeq)
			throws ModuleException {

		if (AppSysParamsUtil.isEnableTransactionGranularity() && enableTransactionGranularity) {
			Collection<ReservationPaxOndPayment> colReservationPaxOndPayment = new ArrayList<ReservationPaxOndPayment>();

			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO = reservationPaxPaymentMetaTO
					.getColReservationPaxChargeMetaTOForNewCharges();

			if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
				for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colReservationPaxChargeMetaTO) {
					ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
					reservationPaxOndPayment.setPnrPaxId(pnrPaxId.toString());
					reservationPaxOndPayment.setAmount(reservationPaxChargeMetaTO.getAmount());
					reservationPaxOndPayment.setPaymentTnxId(new Long(operationTnx.getTnxId()));
					reservationPaxOndPayment.setPnrPaxOndChgId(new Long(reservationPaxChargeMetaTO.getPnrPaxOndChgId()));
					reservationPaxOndPayment.setChargeGroupCode(reservationPaxChargeMetaTO.getChargeGroupCode());
					reservationPaxOndPayment.setNominalCode(ReservationTnxNominalCode.CHARGE_REVERSAL.getCode());
					reservationPaxOndPayment.setRemarks(reservationPaxChargeMetaTO.getRemarks());
					reservationPaxOndPayment.setTransactionSeq(transactionSeq);
					colReservationPaxOndPayment.add(reservationPaxOndPayment);
				}
			}

			if (colReservationPaxOndPayment != null && colReservationPaxOndPayment.size() > 0) {
				ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveReservationPaxOndPayment(colReservationPaxOndPayment);
			}
		}

		// Adjust credit
		Command balanceCreditCmd = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
		balanceCreditCmd.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);

		balanceCreditCmd.execute();
	}

}
