package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;

public class PalCalValidSsrInformation extends SsrInformation {

	@Override
	public void getAncillaryInformation(List<PassengerInformation> passengerInformation) {
		ssrAncillaryInformation = PnlAdlUtil.getPassengerSSRDetails(passengerInformation, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL_AC);
		ssrAncillaryInformationDel = PnlAdlUtil.getPassengerSSRDetails(passengerInformation, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL_D);

	}

	@Override
	public void populatePassengerInformation(List<PassengerInformation> detailsDTOs) {
		for (PassengerInformation information : detailsDTOs) {
			List<AncillaryDTO> ssrInformation = getSsrAncillaryInformationListBy(information.getPnrPaxId(),
					information.getAdlAction());
			information.setSsrs(ssrInformation);
		}
	}

	private List<AncillaryDTO> getSsrAncillaryInformationListBy(Integer passengerId, String modType) {
		if (modType.equals("D")) {
			if (ssrAncillaryInformationDel.get(passengerId) == null) {
				return ssrAncillaryInformation.get(passengerId);
			} else {
				return ssrAncillaryInformationDel.get(passengerId);
			}

		} else {
			return ssrAncillaryInformation.get(passengerId);
		}

	}

}
