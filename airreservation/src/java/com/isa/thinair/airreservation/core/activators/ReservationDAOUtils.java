/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.activators;

import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.core.persistence.dao.AgentTopUpTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.AlertActionDAO;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airreservation.core.persistence.dao.BlacklistPAXDAO;
import com.isa.thinair.airreservation.core.persistence.dao.CreditSalesTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.FlexiRuleDAO;
import com.isa.thinair.airreservation.core.persistence.dao.FocusSystemDAO;
import com.isa.thinair.airreservation.core.persistence.dao.GroupBookingDAO;
import com.isa.thinair.airreservation.core.persistence.dao.IbeExitDetailsDAO;
import com.isa.thinair.airreservation.core.persistence.dao.LccDAO;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.airreservation.core.persistence.dao.NSRefundReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.OfficersMobileNumbersDAO;
import com.isa.thinair.airreservation.core.persistence.dao.OnholdReleaseTimeDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PalCalTimingDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PnlAdlTimingDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PnrGovDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReinstatedCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReprotectedPassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservarionPaxFareSegDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAirportTransferDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAutomaticCheckinDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationChargeDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaxFareDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSSRDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SalesDAO;
import com.isa.thinair.airreservation.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airreservation.core.persistence.dao.TaxInvoiceDAO;
import com.isa.thinair.airreservation.core.persistence.dao.TermsTemplateDAO;
import com.isa.thinair.airreservation.core.persistence.dao.XAPnlDAO;
import com.isa.thinair.commons.core.util.ExternalDAOSystemsConfig;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;


/**
 * Air Reservation DAO specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationDAOUtils {

	/** Hold the logger instance */
	// private static final Log log = LogFactory.getLog(ReservationDAOUtils.class);

	/**
	 * Returns a DAO for air reservation module
	 * 
	 * @param daoName
	 * @return
	 */
	private static Object getDAO(String daoName) {
		String daoFullName = "isa:base://modules/" + AirreservationConstants.MODULE_NAME + "?id=" + daoName + "Proxy";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(daoFullName);
	}

	public static ExternalDAOSystemsConfig getExternalDAOImplConfig() {
		String daoFullName = "isa:base://modules/" + AirreservationConstants.MODULE_NAME + "?id=externalDAOSystemsImplConfig";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (ExternalDAOSystemsConfig) lookupService.getBean(daoFullName);
	}

	/** Denotes the the DAO names of air reservation */
	private static interface DAONames {
		public static String RESERVATION_DAO = "ReservationDAO";

		public static String PASSENGER_DAO = "PassengerDAO";

		public static String RESERVATION_SEGMENT_DAO = "ReservationSegmentDAO";

		public static String RESERVATION_PAXFARE_DAO = "ReservationPaxFareDAO";

		public static String RESERVATION_PAXFARE_SEG_DAO = "ReservarionPaxFareSegDAO";

		public static String RESERVATION_CHARGE_DAO = "ReservationChargeDAO";

		public static String RESERVATION_CREDIT_DAO = "ReservationCreditDAO";

		public static String RESERVATION_TNX_DAO = "ReservationTnxDAO";

		public static String RESERVATION_PAYMENT_DAO = "ReservationPaymentDAO";

		public static String PAX_FINAL_SALES_DAO = "PaxFinalSalesDAO";

		public static String ONHOLD_RELEASE_TIME_DAO = "OnholdReleaseTimeDAO";

		public static String XA_PNL_DAO = "XAPnlDAO";

		public static String RESERVATION_ALERT_ACTION_DAO = "AlertActionDAO";

		public static String RESERVATION_AUXILLIARY_DAO = "ReservationAuxilliaryDAO";

		public static String SALES_DAO = "SalesDAO";

		public static String PNL_ADL_TIMINGS_DAO = "PnlAdlTimingDAO";
		
		public static String PAL_CAL_TIMING_DAO = "PalCalTimingDAO";

		public static String REINSTATED_CREDIT_DAO = "ReinstatedCreditDAO";

		public static String SEAT_MAP_DAO = "SeatMapDAO";

		public static String MEAL_DAO = "MealDAO";
		
		public static String RESERVATION_AIRPORT_TRANSFER_DAO = "ReservationAirportTransferDAO";

		public static String RESERVATION_SSR = "ReservationSSRDAO";

		public static String LCC_DAO = "LccDAO";

		// public static String DRY_BOOKING_DAO = "DryBookingDAO";

		public static String FLEXI_RULE_DAO = "FlexiRuleDAO";

		public static String BAGGAGE_DAO = "BaggageDAO";

		public static String ETICKET_DAO = "ETicketDAO";
		
		public static String SAGESYSTEM_DAO = "SageSystemDAO";

		public static String X3_SYSTEM_DAO = "X3SystemDAO";

		public static String X3_AGENTTOPUP_DAO = "x3AgentTopUpDAO";

		public static String FOCUSSYSTEM_DAO = "FocusSystemDAO";
		public static String PNRGOV_DAO = "PnrGovDAO";
		public static String NSREFUND_RESERVATION_DAO = "NSRefundReservationDAO";

		public static String GROUPBOOKING_REQUEST_DAO = "GroupBookingDAO";

		public static String TERMS_TEMPLATE_DAO = "TermsTemplateDAO";
		
		public static String IBE_EXIT_DETAILS_DAO = "IbeExitDetailsDAO";
		
		public static String BLACKLIST_PAX_DAO = "BlacklistPAXDAO";
		
		public static String REPROTECTED_PAX_DAO = "ReprotectedPassengerDAO";

		public static String OFFICERS_MOBILE_NUMBERS_DAO = "OfficersMobileNumbersDAO";
		
		public static String TAX_INVOICE_DAO = "TaxInvoiceDAO";

		public static String RESERVATION_AUTO_CHECKIN_DAO = "ReservationAutomaticCheckinDAO";
		
		
		}

	/** Denotes the the DAO instances of air reservation */
	public static interface DAOInstance {
		/** Holds the ReservationDAO instance */
		public static ReservationDAO RESERVATION_DAO = (ReservationDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_DAO);

		/** Holds the PassengerDAO instance */
		public static PassengerDAO PASSENGER_DAO = (PassengerDAO) getDAO(ReservationDAOUtils.DAONames.PASSENGER_DAO);

		/** Holds the ReservationSegmentDAO instance */
		public static ReservationSegmentDAO RESERVATION_SEGMENT_DAO = (ReservationSegmentDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_SEGMENT_DAO);

		/** Holds the ReservationPaxFareDAO instance */
		public static ReservationPaxFareDAO RESERVATION_PAXFARE_DAO = (ReservationPaxFareDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_PAXFARE_DAO);

		/** Holds the ReservarionPaxFareSegDAO instance */
		public static ReservarionPaxFareSegDAO RESERVATION_PAXFARE_SEG_DAO = (ReservarionPaxFareSegDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_PAXFARE_SEG_DAO);

		/** Holds the ReservationChargeDAO instance */
		public static ReservationChargeDAO RESERVATION_CHARGE_DAO = (ReservationChargeDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_CHARGE_DAO);

		/** Holds the ReservationCreditDAO instance */
		public static ReservationCreditDAO RESERVATION_CREDIT_DAO = (ReservationCreditDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_CREDIT_DAO);

		/** Holds the ReservationTnxDAO instance */
		public static ReservationTnxDAO RESERVATION_TNX_DAO = (ReservationTnxDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_TNX_DAO);

		/** Holds the ReservationPaymentDAO instance */
		public static ReservationPaymentDAO RESERVATION_PAYMENT_DAO = (ReservationPaymentDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_PAYMENT_DAO);

		/** Holds the PaxFinalSalesDAO instance */
		public static PaxFinalSalesDAO PAX_FINAL_SALES_DAO = (PaxFinalSalesDAO) getDAO(ReservationDAOUtils.DAONames.PAX_FINAL_SALES_DAO);

		/** Holds the OnholdReleaseTimeDAO instance */
		public static OnholdReleaseTimeDAO ONHOLD_RELEASE_TIME_DAO = (OnholdReleaseTimeDAO) getDAO(ReservationDAOUtils.DAONames.ONHOLD_RELEASE_TIME_DAO);

		/** Holds the XAPnlDAO instance */
		public static XAPnlDAO XA_PNL_DAO = (XAPnlDAO) getDAO(ReservationDAOUtils.DAONames.XA_PNL_DAO);

		/** Holds the AlertActionDAO instance */
		public static AlertActionDAO RESERVATION_ALERT_ACTION_DAO = (AlertActionDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_ALERT_ACTION_DAO);

		/** Holds the ReservationAuxilliaryDAO instance */
		public static ReservationAuxilliaryDAO RESERVATION_AUXILLIARY_DAO = (ReservationAuxilliaryDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_AUXILLIARY_DAO);

		/** Holds the SalesDAO instance */
		public static SalesDAO SALES_DAO = (SalesDAO) getDAO(ReservationDAOUtils.DAONames.SALES_DAO);

		/** Holds the PnlAdlTimingDAO instance */
		public static PnlAdlTimingDAO PNL_ADL_TIMINGS_DAO = (PnlAdlTimingDAO) getDAO(ReservationDAOUtils.DAONames.PNL_ADL_TIMINGS_DAO);

		/** Holds the PalCalTimingDAO instance */
		public static PalCalTimingDAO PAL_CAL_TIMINGS_DAO = (PalCalTimingDAO) getDAO(ReservationDAOUtils.DAONames.PAL_CAL_TIMING_DAO);

		/** Holds the ReinstatedCreditDAO instance */
		public static ReinstatedCreditDAO REINSTATED_CREDIT_DAO = (ReinstatedCreditDAO) getDAO(ReservationDAOUtils.DAONames.REINSTATED_CREDIT_DAO);

		/** Holds the SeatMapDO instance */
		public static SeatMapDAO SEAT_MAP_DAO = (SeatMapDAO) getDAO(ReservationDAOUtils.DAONames.SEAT_MAP_DAO);

		/** Holds the Seat MealDAO instance */
		public static MealDAO MEAL_DAO = (MealDAO) getDAO(ReservationDAOUtils.DAONames.MEAL_DAO);
		
		/** Holds the Seat MealDAO instance */
		public static ReservationAirportTransferDAO AIRPORT_TRANSFER_DAO = (ReservationAirportTransferDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_AIRPORT_TRANSFER_DAO);

		/** Holds the ReservationSSRDAO instance */
		public static ReservationSSRDAO RESERVATION_SSR = (ReservationSSRDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_SSR);

		/** Holds the LccDAO instance */
		public static LccDAO LCC_DAO = (LccDAO) getDAO(ReservationDAOUtils.DAONames.LCC_DAO);

		/** Holds the FlexiRuleDAO instance */
		public static FlexiRuleDAO FLEXI_RULE_DAO = (FlexiRuleDAO) getDAO(ReservationDAOUtils.DAONames.FLEXI_RULE_DAO);
		/** Holds the BaggageDAO instance */
		public static BaggageDAO BAGGAGE_DAO = (BaggageDAO) getDAO(ReservationDAOUtils.DAONames.BAGGAGE_DAO);

		/** Holds the ETicketDAO instance */
		public static ETicketDAO ETicketDAO = (ETicketDAO) getDAO(ReservationDAOUtils.DAONames.ETICKET_DAO);

		// public static SageSystemDAO sageSystemDAO = (SageSystemDAO)
		// getDAO(ReservationDAOUtils.DAONames.SAGESYSTEM_DAO);

		public static CreditSalesTransferDAO sageSystemDAO = (CreditSalesTransferDAO) getDAO(ReservationDAOUtils.DAONames.SAGESYSTEM_DAO);

		public static CreditSalesTransferDAO x3SystemDAO = (CreditSalesTransferDAO) getDAO(ReservationDAOUtils.DAONames.X3_SYSTEM_DAO);
		
		public static AgentTopUpTransferDAO x3AgentTopUpDAO = (AgentTopUpTransferDAO) getDAO(
				ReservationDAOUtils.DAONames.X3_AGENTTOPUP_DAO);

		public static FocusSystemDAO focusSystemDAO = (FocusSystemDAO) getDAO(ReservationDAOUtils.DAONames.FOCUSSYSTEM_DAO);
		
		public static PnrGovDAO PNRGOV_DAO = (PnrGovDAO) getDAO(ReservationDAOUtils.DAONames.PNRGOV_DAO);

		public static NSRefundReservationDAO NSREFUND_RESERVATION_DAO = (NSRefundReservationDAO) getDAO(ReservationDAOUtils.DAONames.NSREFUND_RESERVATION_DAO);

		public static GroupBookingDAO GrpBookingDAO = (GroupBookingDAO) getDAO(ReservationDAOUtils.DAONames.GROUPBOOKING_REQUEST_DAO);

		public static TermsTemplateDAO TERMS_TEMPLATE_DAO = (TermsTemplateDAO) getDAO(ReservationDAOUtils.DAONames.TERMS_TEMPLATE_DAO);

		public static IbeExitDetailsDAO IBE_EXIT_DETAILS_DAO = (IbeExitDetailsDAO) getDAO(ReservationDAOUtils.DAONames.IBE_EXIT_DETAILS_DAO);

		/** Holds the OfficersMobileNumbersDAO instance */
		public static OfficersMobileNumbersDAO OFFICERS_MOBILE_NUMBERS_DAO = (OfficersMobileNumbersDAO) getDAO(ReservationDAOUtils.DAONames.OFFICERS_MOBILE_NUMBERS_DAO);
		
		public static BlacklistPAXDAO BLACKLIST_PAX_DAO = (BlacklistPAXDAO) getDAO(ReservationDAOUtils.DAONames.BLACKLIST_PAX_DAO);
		
		public static ReprotectedPassengerDAO REPROTECTED_PAX_DAO = (ReprotectedPassengerDAO) getDAO(ReservationDAOUtils.DAONames.REPROTECTED_PAX_DAO);

		public static TaxInvoiceDAO TAX_INVOICE_DAO = (TaxInvoiceDAO) getDAO(ReservationDAOUtils.DAONames.TAX_INVOICE_DAO);
		
		/** Holds the Auto checkin DAO instance */
		public static ReservationAutomaticCheckinDAO RESERVATION_AUTO_CHECKIN_DAO = (ReservationAutomaticCheckinDAO) getDAO(ReservationDAOUtils.DAONames.RESERVATION_AUTO_CHECKIN_DAO);

	}
}
