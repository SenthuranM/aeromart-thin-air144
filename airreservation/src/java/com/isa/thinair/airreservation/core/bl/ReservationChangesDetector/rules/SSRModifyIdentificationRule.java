package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.ModificationTypeWiseSegsResponse;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SSRModifyIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	private final String CNF = "CNF";

	public SSRModifyIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {
		RuleResponseDTO response = getResponseDTO();

		inspaectSSRUpdateOccured(dataContext, response);
		return response;

	}

	private void inspaectSSRUpdateOccured(RuleExecuterDatacontext dataContext, RuleResponseDTO response) {

		Map<Integer, List<PaxSSRDTO>> existSsrInfoMap = getPaxWiseCNFSSrInfo(dataContext.getExisitingReservation()
				.getPassengers());
		Map<Integer, List<PaxSSRDTO>> updatedSsrInfoMap = getPaxWiseCNFSSrInfo(dataContext.getUpdatedReservation()
				.getPassengers());

		Set<Integer> allPnrSegIds = new HashSet<>();

		for (Integer pnrPaxId : updatedSsrInfoMap.keySet()) {

			Map<Integer, Set<String>> existingSegWiseMap = getSegmentWiseSsrCodes(existSsrInfoMap.get(pnrPaxId),
					getUncancelledSegIds(dataContext.getExisitingReservation()));
			Map<Integer, Set<String>> updatedSegWiseMap = getSegmentWiseSsrCodes(updatedSsrInfoMap.get(pnrPaxId),
					getUncancelledSegIds(dataContext.getUpdatedReservation()));

			allPnrSegIds.clear();
			allPnrSegIds.addAll(existingSegWiseMap.keySet());
			allPnrSegIds.addAll(updatedSegWiseMap.keySet());

			List<Integer> affectedPnrSegIds = getAffectedPnrSegId(existingSegWiseMap, updatedSegWiseMap, allPnrSegIds);

			if (isNullOrEmptyList(affectedPnrSegIds)) {
				response.getAffectedPaxWiseSegment().put(pnrPaxId,
						new ModificationTypeWiseSegsResponse(null, null, affectedPnrSegIds));
			}
		}

	}

	private List<Integer> getAffectedPnrSegId(Map<Integer, Set<String>> existingSegWiseMap,
			Map<Integer, Set<String>> updatedSegWiseMap, Set<Integer> allPnrSegIds) {

		List<Integer> affectedPnrSegIds = new ArrayList<Integer>();

		for (Integer pnrSegId : allPnrSegIds) {

			Set<String> existingResSsrsCode = existingSegWiseMap.get(pnrSegId);
			Set<String> updatedResSsrsCode = updatedSegWiseMap.get(pnrSegId);

			if ((existingResSsrsCode != null && updatedResSsrsCode != null)
					&& (!existingResSsrsCode.containsAll(updatedResSsrsCode) || !updatedResSsrsCode
							.containsAll(existingResSsrsCode))) {

				affectedPnrSegIds.add(pnrSegId);
			}
		}

		return affectedPnrSegIds;
	}

	private Map<Integer, Set<String>> getSegmentWiseSsrCodes(List<PaxSSRDTO> ssrList, List<Integer> unCancelledSegIds) {
		Map<Integer, Set<String>> segWiseSSrMap = getSegmentWiseSsrMap(unCancelledSegIds);

		if (ssrList != null) {
			for (PaxSSRDTO ssr : ssrList) {
				if (segWiseSSrMap.get(ssr.getPnrSegId()) != null) {
					segWiseSSrMap.get(ssr.getPnrSegId()).add(ssr.getSsrCode());
				}
			}
		}

		return segWiseSSrMap;
	}

	private Map<Integer, Set<String>> getSegmentWiseSsrMap(List<Integer> unCancelledSegIds) {
		Map<Integer, Set<String>> segWiseSSrMap = new HashMap<Integer, Set<String>>();

		for (Integer segId : unCancelledSegIds) {
			segWiseSSrMap.put(segId, new HashSet<String>());
		}

		return segWiseSSrMap;
	}

	private Map<Integer, List<PaxSSRDTO>> getPaxWiseCNFSSrInfo(Set<ReservationPax> paxList) {

		Map<Integer, List<PaxSSRDTO>> ssrMap = new HashMap<>();

		for (ReservationPax pax : paxList) {
			if (!isCNXPax(pax) && pax.getPaxSSR() != null) {
				List<PaxSSRDTO> ssrList = new ArrayList<>();

				for (PaxSSRDTO ssr : pax.getPaxSSR()) {
					if (CNF.equals(ssr.getStatus())) {
						ssrList.add(ssr);
					}
				}
				ssrMap.put(pax.getPnrPaxId(), ssrList);
			}
		}
		return ssrMap;
	}

}
