/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.PnrElementRuleContext;

/**
 * @author udithad
 *
 */
public class PnrAmmendValidityRule extends BaseRule<PnrElementRuleContext> {

	@Override
	public boolean validateRule(PnrElementRuleContext context) {
		boolean isValied = true;
		String pnr = context.getPnr();
		int pnrWisePassengerCount = 0;
		if(context.getPnrWisePassengerCount() != null){
			pnrWisePassengerCount = context.getPnrWisePassengerCount().get(pnr);
		}
		
		int pnrWiseReductionCount = 0;
		if(context.getPnrWisePaxReductionCout() != null){
			pnrWiseReductionCount = context.getPnrWisePaxReductionCout().get(
				pnr);
		}
		
		int utilizedPassengerCount = 0;
		if(context.getUtilizedPassengerCount()!= null){
			utilizedPassengerCount=context.getUtilizedPassengerCount();
		}
		

		if (context.getPnrWiseGroupCodes().containsKey(context.getPnr())) {
			if (pnrWisePassengerCount == (pnrWiseReductionCount + utilizedPassengerCount)) {
				isValied = true;
			} else {
				isValied = false;
			}
		}
		return isValied;
	}
}
