package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndChgCommission;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="confirmPendingAgentCommissions"
 * @author rimaz
 * 
 */
public class ConfirmPendingAgentCommissions extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(ConfirmPendingAgentCommissions.class);
	private final TravelAgentFinanceBD travelAgentFinanceBD;

	public ConfirmPendingAgentCommissions() {
		travelAgentFinanceBD = ReservationModuleUtils.getTravelAgentFinanceBD();
	}

	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");
		Boolean triggerPnrForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Reservation reservation = null;
		if (activatePendingCommissions(credentialsDTO, triggerPnrForceConfirmed)) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setLoadEtickets(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
			confirmPendingAgentCommissions(reservation, credentialsDTO);
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.RESERVATION, reservation);
		log.debug("Exit execute");
		return response;
	}

	private boolean activatePendingCommissions(CredentialsDTO credentialsDTO, boolean triggerPnrForceConfirmed) {
		boolean activate = false;
		if (AppSysParamsUtil.isAgentCommmissionEnabled() && credentialsDTO != null && credentialsDTO.getAgentCode() != null
				&& !credentialsDTO.getAgentCode().isEmpty() && !triggerPnrForceConfirmed) {
			activate = true;
		}
		return activate;
	}

	private void confirmPendingAgentCommissions(Reservation reservation, CredentialsDTO credentialsDTO) throws ModuleException {
		BigDecimal agentCommissionByPax = AccelAeroCalculator.getDefaultBigDecimalZero();
		String pnr = reservation.getPnr();
		Iterator<ReservationPax> reservationPaxIt = reservation.getPassengers().iterator();
		ReservationPax reservationPax = null;
		String performingAgent = credentialsDTO.getAgentCode();

		while (reservationPaxIt.hasNext()) {
			reservationPax = reservationPaxIt.next();
			if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax)) {
				Set<ReservationPaxFare> resPaxFares = reservationPax.getPnrPaxFares();
				for (ReservationPaxFare resPaxFare : resPaxFares) {
					for (ReservationPaxOndCharge resPaxOndCharge : resPaxFare.getCharges()) {
						if (resPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {

							if (isConfirmedNotCheckedSegment(resPaxOndCharge)) {
								Set<ReservationPaxOndChgCommission> appliedCommissions = resPaxOndCharge
										.getResPaxOndChgCommission();
								if (appliedCommissions != null && !appliedCommissions.isEmpty()) {
									for (ReservationPaxOndChgCommission appliedCommission : appliedCommissions) {
										String offeredCommisssionStatus = appliedCommission.getStatus();
										String commissionOwnerAgent = appliedCommission.getAgentCode();
										if (offeredCommisssionStatus
												.equals(AirTravelAgentConstants.AgentCommissionStatus.PENDING)) {
											if (commissionOwnerAgent.equals(performingAgent)) {
												appliedCommission
														.setStatus(AirTravelAgentConstants.AgentCommissionStatus.CONFIRMED);
												agentCommissionByPax = appliedCommission.getAmount();
												if (agentCommissionByPax.compareTo(BigDecimal.ZERO) > 0) {
													travelAgentFinanceBD.recordAgentCommission(performingAgent,
															agentCommissionByPax, getTracker(credentialsDTO), pnr, null);
												}
											} else {
												appliedCommission
														.setStatus(AirTravelAgentConstants.AgentCommissionStatus.CANCELED);
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}

	}

	private boolean isConfirmedNotCheckedSegment(ReservationPaxOndCharge resPaxOndCharge) {
		ReservationPaxFare resPaxFare = resPaxOndCharge.getReservationPaxFare();
		boolean isNotCheckedConfirmed = false;
		for (ReservationPaxFareSegment paxFareSeg : resPaxFare.getPaxFareSegments()) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(paxFareSeg.getSegment().getStatus())) {
				if (paxFareSeg.getCheckinStatus().equals(ReservationInternalConstants.PassengerCheckinStatus.TO_BE_CHECKED_IN)
						&& paxFareSeg.getStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED)) {
					isNotCheckedConfirmed = true;
				}
			}
		}
		return isNotCheckedConfirmed;
	}

	private String getTracker(CredentialsDTO credentialsDTO) {
		StringBuilder tracker = new StringBuilder();
		tracker.append("Pending commission confirmed ").append(credentialsDTO.getAgentCode());
		tracker.append(", by userId: ").append(credentialsDTO.getUserId());
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-mm-dd HH:mm");
		String date = dateFormatter.format(new Date());
		tracker.append(", at: ").append(date);
		tracker.append(" by balance payment");
		return tracker.toString();
	}
}
