package com.isa.thinair.airreservation.core.bl.segment;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.FareChargesTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.ChargeTypes;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO.RouteTypes;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * I was really pissed about why these commons stuff were replicated in both ChargeBO and CancelSegmentBO. This pissness
 * motivated me to move to a common functionality
 * 
 * @author Nilindra Fernando
 * @since Nov 9, 2011
 */
public class FeeBO {

	/**
	 * <p>
	 * <b>Return the appropriate cancellation charge</b>
	 * </p>
	 * 
	 * @param fareTO
	 * @param paxType
	 * @param paxFare
	 * @param setCharges
	 * @param chargeDetailDTO
	 * @param customChargesTO
	 * @param fareRuleFeeTO
	 * @param hasModFlexibility
	 * @param isCancelFlightsExists
	 * @param modOndPosition
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getModificationCharge(FareTO fareTO, String paxType, BigDecimal paxFare,
			Set<ReservationPaxOndCharge> setCharges, PnrChargeDetailTO chargeDetailDTO, CustomChargesTO customChargesTO,
			FareRuleFeeTO fareRuleFeeTO, boolean hasModFlexibility, boolean isCancelFlightsExists,
			boolean isSameFlightsModification, int modOndPosition) throws ModuleException {

		if (hasModFlexibility || isCancelFlightsExists || isSameFlightsModification) {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		if (ReservationModuleUtils.getAirReservationConfig().isModChargeForDiscountedFare()) {
			BigDecimal fareDiscount = getTotalDiscountAmount(setCharges);
			paxFare = AccelAeroCalculator.subtract(paxFare, fareDiscount);

		}

		overrideCustomCNXMODCharges(customChargesTO, fareTO.getOndCode(), modOndPosition);

		FareChargesTO fareChargesTO = fareTO.getFareChargesTO();
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			if (!fareTO.isAdultApplyModChg()) {
				return AccelAeroCalculator.getDefaultBigDecimalZero();
			} else if (customChargesTO != null && customChargesTO.hasUserDefinedModCharge(paxType)) {
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, customChargesTO.getCustomAdultMCharge(), fareTO.getFareId(),
						customChargesTO, ReservationInternalConstants.PassengerType.ADULT);
				return customChargesTO.getCustomAdultMCharge();
			} else if (customChargesTO != null && customChargesTO.getCustomAdultChargeTO() != null) {
				updateCustomCharges(customChargesTO.getCustomAdultChargeTO(), fareChargesTO);
				BigDecimal paxCnxCharge = customChargesTO.getCustomAdultChargeTO().getChargePercentage();
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.ADULT);
				return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else if (fareRuleFeeTO != null) {
				if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
					return fareRuleFeeTO.getChargeAmount();
				} else {
					updateCustomCharges(fareRuleFeeTO, fareChargesTO, false);
					BigDecimal paxCnxCharge = fareRuleFeeTO.getChargeAmount();
					fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
							ReservationInternalConstants.PassengerType.ADULT);
					return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
				}
			} else if (fareChargesTO != null
					&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getModificationChargeType())) {
				BigDecimal paxCnxCharge = fareTO.getAdultModificationCharge(); // readability
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.ADULT);
				return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else {
				return fareTO.getAdultModificationCharge();
			}
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			if (!fareTO.isChildApplyModChg()) {
				return AccelAeroCalculator.getDefaultBigDecimalZero();
			} else if (customChargesTO != null && customChargesTO.hasUserDefinedModCharge(paxType)) {
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, customChargesTO.getCustomChildMCharge(), fareTO.getFareId(),
						customChargesTO, ReservationInternalConstants.PassengerType.CHILD);
				return customChargesTO.getCustomChildMCharge();
			} else if (customChargesTO != null && customChargesTO.getCustomChildChargeTO() != null) {
				updateCustomCharges(customChargesTO.getCustomChildChargeTO(), fareChargesTO);
				BigDecimal paxCnxCharge = customChargesTO.getCustomChildChargeTO().getChargePercentage();
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.CHILD);
				return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else if (fareRuleFeeTO != null) {
				if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
					return fareRuleFeeTO.getChargeAmount();
				} else {
					updateCustomCharges(fareRuleFeeTO, fareChargesTO, false);
					BigDecimal paxCnxCharge = fareRuleFeeTO.getChargeAmount();
					fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
							ReservationInternalConstants.PassengerType.CHILD);
					return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
				}
			} else if (fareChargesTO != null
					&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getModificationChargeType())) {
				BigDecimal paxCnxCharge = fareTO.getChildModificationCharge(); // readability
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.CHILD);
				return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else {
				return fareTO.getChildModificationCharge();
			}
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			if (!fareTO.isInfantApplyModChg()) {
				return AccelAeroCalculator.getDefaultBigDecimalZero();
			} else if (customChargesTO != null && customChargesTO.hasUserDefinedModCharge(paxType)) {
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, customChargesTO.getCustomInfantMCharge(),
						fareTO.getFareId(), customChargesTO, ReservationInternalConstants.PassengerType.INFANT);
				return customChargesTO.getCustomInfantMCharge();
			} else if (customChargesTO != null && customChargesTO.getCustomInfantChargeTO() != null) {
				updateCustomCharges(customChargesTO.getCustomInfantChargeTO(), fareChargesTO);
				BigDecimal paxCnxCharge = customChargesTO.getCustomInfantChargeTO().getChargePercentage();
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.INFANT);
				return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else if (fareRuleFeeTO != null) {
				if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
					return fareRuleFeeTO.getChargeAmount();
				} else {
					updateCustomCharges(fareRuleFeeTO, fareChargesTO, false);
					BigDecimal paxCnxCharge = fareRuleFeeTO.getChargeAmount();
					fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
							ReservationInternalConstants.PassengerType.INFANT);
					return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
				}
			} else if (fareChargesTO != null
					&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getModificationChargeType())) {
				BigDecimal paxCnxCharge = fareTO.getInfantModificationCharge(); // readability
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.INFANT);
				return getAbsoluteModificationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else {
				return fareTO.getInfantModificationCharge();
			}
		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	// for display purposes
	private static void fillPaxChargeDetails(FareChargesTO fareChargesTO, PnrChargeDetailTO chargeDetailDTO,
			BigDecimal chargeAmount, Integer fareId, CustomChargesTO customChargesTO, String paxType) {

		// if user has defined custom percentage charges through UI
		if (customChargesTO != null && customChargesTO.getPaxCustomChargeDetail(paxType) != null) {
			PnrChargeDetailTO paxPnrChargeDetailTO = customChargesTO.getPaxCustomChargeDetail(paxType);
			chargeDetailDTO.setMinCancellationAmount(paxPnrChargeDetailTO.getMinCancellationAmount());
			chargeDetailDTO.setMaximumCancellationAmount(paxPnrChargeDetailTO.getMaximumCancellationAmount());
			chargeDetailDTO.setMinModificationAmount(paxPnrChargeDetailTO.getMinModificationAmount());
			chargeDetailDTO.setMaximumModificationAmount(paxPnrChargeDetailTO.getMaximumModificationAmount());
			chargeDetailDTO.setCancellationChargeType(paxPnrChargeDetailTO.getCancellationChargeType());
			chargeDetailDTO.setModificationChargeType(paxPnrChargeDetailTO.getModificationChargeType());
			chargeDetailDTO.setFareID(0);
			chargeDetailDTO.setCustomCharge(true);
			chargeDetailDTO.setChargePercentage(paxPnrChargeDetailTO.getChargePercentage());
		} else if (customChargesTO != null
				&& (customChargesTO.hasUserDefinedCNXCharge(paxType) || customChargesTO.hasUserDefinedModCharge(paxType))) {
			chargeDetailDTO.setCustomCharge(true);
		} else if (fareChargesTO != null) {// if fare has Value of Percentage charges
			chargeDetailDTO.setMinCancellationAmount(fareChargesTO.getMinCancellationAmount());
			chargeDetailDTO.setMaximumCancellationAmount(fareChargesTO.getMaximumCancellationAmount());
			chargeDetailDTO.setMinModificationAmount(fareChargesTO.getMinModificationAmount());
			chargeDetailDTO.setMaximumModificationAmount(fareChargesTO.getMaximumModificationAmount());
			chargeDetailDTO.setCancellationChargeType(fareChargesTO.getCancellationChargeType());
			chargeDetailDTO.setModificationChargeType(fareChargesTO.getModificationChargeType());
			chargeDetailDTO.setNameChangeChargeType(fareChargesTO.getNameChangeChargeType());
			chargeDetailDTO.setMinNameChangeAmount(fareChargesTO.getMinNameChangeChargeAmount());
			chargeDetailDTO.setMaxNameChangeAmount(fareChargesTO.getMaxNameChangeChargeAmount());
			chargeDetailDTO.setFareID(fareId);
		}

	}

	/**
	 * <p>
	 * <b>Return the appropriate cancellation charge</b>
	 * </p>
	 * 
	 * @param fareTO
	 * @param paxType
	 * @param modOndPosition
	 *            TODO
	 * @return
	 * @throws ModuleException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static BigDecimal getCancellationCharge(FareTO fareTO, String paxType, BigDecimal paxFare,
			Set<ReservationPaxOndCharge> setCharges, PnrChargeDetailTO chargeDetailDTO, CustomChargesTO customChargesTO,
			FareRuleFeeTO fareRuleFeeTO, boolean hasCnxFlexibility, boolean isCancelFlightsExists, boolean isVoidReservation,
			int modOndPosition) throws ModuleException {

		if (hasCnxFlexibility || isVoidReservation || isCancelFlightsExists) {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		if (ReservationModuleUtils.getAirReservationConfig().isCnxChargeForDiscountedFare()) {
			BigDecimal fareDiscount = getTotalDiscountAmount(setCharges);
			paxFare = AccelAeroCalculator.subtract(paxFare, fareDiscount);

		}

		overrideCustomCNXMODCharges(customChargesTO, fareTO.getOndCode(), modOndPosition);

		FareChargesTO fareChargesTO = fareTO.getFareChargesTO();
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			if (!fareTO.isAdultApplyCnxChg()) {
				return AccelAeroCalculator.getDefaultBigDecimalZero();
			} else if (customChargesTO != null && customChargesTO.hasUserDefinedCNXCharge(paxType)) {
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, customChargesTO.getCustomAdultCCharge(), fareTO.getFareId(),
						customChargesTO, ReservationInternalConstants.PassengerType.ADULT);
				return customChargesTO.getCustomAdultCCharge();
			} else if (customChargesTO != null && customChargesTO.getCustomAdultChargeTO() != null) {
				updateCustomCharges(customChargesTO.getCustomAdultChargeTO(), fareChargesTO);
				BigDecimal paxCnxCharge = customChargesTO.getCustomAdultChargeTO().getChargePercentage();
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.ADULT);
				return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else if (fareRuleFeeTO != null) {
				if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
					return fareRuleFeeTO.getChargeAmount();
				} else {
					updateCustomCharges(fareRuleFeeTO, fareChargesTO, true);
					BigDecimal paxCnxCharge = fareRuleFeeTO.getChargeAmount();
					fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
							ReservationInternalConstants.PassengerType.ADULT);
					return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
				}
			} else if (fareChargesTO != null
					&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getCancellationChargeType())) {
				BigDecimal paxCnxCharge = fareTO.getAdultCancellationCharge(); // readability
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.ADULT);
				return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else {
				return fareTO.getAdultCancellationCharge();
			}
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			if (!fareTO.isChildApplyCnxChg()) {
				return AccelAeroCalculator.getDefaultBigDecimalZero();
			} else if (customChargesTO != null && customChargesTO.hasUserDefinedCNXCharge(paxType)) {
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, customChargesTO.getCustomChildCCharge(), fareTO.getFareId(),
						customChargesTO, ReservationInternalConstants.PassengerType.CHILD);
				return customChargesTO.getCustomChildCCharge();
			} else if (customChargesTO != null && customChargesTO.getCustomChildChargeTO() != null) {
				updateCustomCharges(customChargesTO.getCustomChildChargeTO(), fareChargesTO);
				BigDecimal paxCnxCharge = customChargesTO.getCustomChildChargeTO().getChargePercentage();
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.CHILD);
				return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else if (fareRuleFeeTO != null) {
				if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
					return fareRuleFeeTO.getChargeAmount();
				} else {
					updateCustomCharges(fareRuleFeeTO, fareChargesTO, true);
					BigDecimal paxCnxCharge = fareRuleFeeTO.getChargeAmount();
					fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
							ReservationInternalConstants.PassengerType.CHILD);
					return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
				}
			} else if (fareChargesTO != null
					&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getCancellationChargeType())) {
				BigDecimal paxCnxCharge = fareTO.getChildCancellationCharge(); // readability
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.CHILD);
				return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else {
				return fareTO.getChildCancellationCharge();
			}
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			if (!fareTO.isInfantApplyCnxChg()) {
				return AccelAeroCalculator.getDefaultBigDecimalZero();
			} else if (customChargesTO != null && customChargesTO.hasUserDefinedCNXCharge(paxType)) {
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, customChargesTO.getCustomInfantCCharge(),
						fareTO.getFareId(), customChargesTO, ReservationInternalConstants.PassengerType.INFANT);
				return customChargesTO.getCustomInfantCCharge();
			} else if (customChargesTO != null && customChargesTO.getCustomInfantChargeTO() != null) {
				updateCustomCharges(customChargesTO.getCustomInfantChargeTO(), fareChargesTO);
				BigDecimal paxCnxCharge = customChargesTO.getCustomInfantChargeTO().getChargePercentage();
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.INFANT);
				return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else if (fareRuleFeeTO != null) {
				if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
					return fareRuleFeeTO.getChargeAmount();
				} else {
					updateCustomCharges(fareRuleFeeTO, fareChargesTO, true);
					BigDecimal paxCnxCharge = fareRuleFeeTO.getChargeAmount();
					fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
							ReservationInternalConstants.PassengerType.INFANT);
					return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
				}
			} else if (fareChargesTO != null
					&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getCancellationChargeType())) {
				BigDecimal paxCnxCharge = fareTO.getInfantCancellationCharge(); // readability
				fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxCnxCharge, fareTO.getFareId(), customChargesTO,
						ReservationInternalConstants.PassengerType.INFANT);
				return getAbsoluteCancellationCharges(fareTO, paxFare, setCharges, paxCnxCharge);
			} else {
				return fareTO.getInfantCancellationCharge();
			}
		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	public static BigDecimal getNameChangeCharge(FareTO fareTO, BigDecimal paxFare, Set<ReservationPaxOndCharge> setCharges,
			PnrChargeDetailTO chargeDetailDTO, FareRuleFeeTO fareRuleFeeTO) throws ModuleException {

		// TODO get effective fare for name change charge is fare discount enabled

		FareChargesTO fareChargesTO = fareTO.getFareChargesTO();
		if (fareRuleFeeTO != null) {
			BigDecimal paxNCCharge = fareRuleFeeTO.getChargeAmount();
			fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxNCCharge, fareTO.getFareId(), null, null);
			if (ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareRuleFeeTO.getChargeAmountType())) {
				return paxNCCharge;
			} else {
				updateCustomNCCharges(fareRuleFeeTO, fareChargesTO);
				return getAbsoluteNameChangeCharges(fareTO, paxFare, setCharges, paxNCCharge);
			}
		} else if (fareChargesTO != null
				&& !ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(fareChargesTO.getNameChangeChargeType())) {
			BigDecimal paxNCCharge = fareTO.getNameChangeChargeAmount();
			fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxNCCharge, fareTO.getFareId(), null, null);
			return getAbsoluteNameChangeCharges(fareTO, paxFare, setCharges, paxNCCharge);
		} else {
			BigDecimal paxNCCharge = fareTO.getNameChangeChargeAmount();
			fillPaxChargeDetails(fareChargesTO, chargeDetailDTO, paxNCCharge, fareTO.getFareId(), null, null);
			return paxNCCharge;
		}

	}

	private static void updateCustomNCCharges(FareRuleFeeTO fareRuleFeeTO, FareChargesTO fareChargesTO) {
		fareChargesTO.setMinNameChangeChargeAmount(fareRuleFeeTO.getMinimumPerChargeAmt());
		fareChargesTO.setMaxNameChangeChargeAmount(fareRuleFeeTO.getMaximumPerChargeAmt());
		fareChargesTO.setNameChangeChargeType(fareRuleFeeTO.getChargeAmountType());
	}

	private static void updateCustomCharges(PnrChargeDetailTO pnrChargeDetailTO, FareChargesTO fareChargesTO) {
		if (pnrChargeDetailTO != null) {
			fareChargesTO.setMinModificationAmount(pnrChargeDetailTO.getMinModificationAmount());
			fareChargesTO.setMaximumModificationAmount(pnrChargeDetailTO.getMaximumModificationAmount());
			fareChargesTO.setMinCancellationAmount(pnrChargeDetailTO.getMinCancellationAmount());
			fareChargesTO.setMaximumCancellationAmount(pnrChargeDetailTO.getMaximumCancellationAmount());
			fareChargesTO.setModificationChargeType(pnrChargeDetailTO.getModificationChargeType());
			fareChargesTO.setCancellationChargeType(pnrChargeDetailTO.getCancellationChargeType());
		}
	}

	private static void updateCustomCharges(FareRuleFeeTO fareRuleFeeTO, FareChargesTO fareChargesTO, boolean isCancellation) {
		if (isCancellation) {
			fareChargesTO.setMinCancellationAmount(fareRuleFeeTO.getMinimumPerChargeAmt());
			fareChargesTO.setMaximumCancellationAmount(fareRuleFeeTO.getMaximumPerChargeAmt());
			fareChargesTO.setCancellationChargeType(fareRuleFeeTO.getChargeAmountType());

			// fareChargesTO.setMinModificationAmount(null);
			// fareChargesTO.setMaximumModificationAmount(null);
			// fareChargesTO.setModificationChargeType(null);

		} else {
			// fareChargesTO.setMinCancellationAmount(null);
			// fareChargesTO.setMaximumCancellationAmount(null);
			// fareChargesTO.setCancellationChargeType(null);

			fareChargesTO.setMinModificationAmount(fareRuleFeeTO.getMinimumPerChargeAmt());
			fareChargesTO.setMaximumModificationAmount(fareRuleFeeTO.getMaximumPerChargeAmt());
			fareChargesTO.setModificationChargeType(fareRuleFeeTO.getChargeAmountType());
		}
	}

	/**
	 * Returns total applicable charges
	 * 
	 * @param setCharges
	 * @return
	 */
	private static BigDecimal getTotalCharges(Set<ReservationPaxOndCharge> setCharges) {
		BigDecimal totalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (setCharges != null && !setCharges.isEmpty()) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : setCharges) {
				// not mandatory to check for fare id null
				if (reservationPaxOndCharge.getFareId() == null
						&& (reservationPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.SUR) || reservationPaxOndCharge
								.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.INF))) {
					totalCharge = AccelAeroCalculator.add(totalCharge, reservationPaxOndCharge.getAmount());
				}
			}
		}
		return totalCharge;
	}

	/**
	 * Calculate Absolute Modification amount if modification amount defined as percentage
	 * 
	 * @author dumindaG
	 * @param fareTO
	 * @param paxFare
	 * @param totalCharges
	 * @return
	 */
	private static BigDecimal getAbsoluteModificationCharges(FareTO fareTO, BigDecimal paxFare,
			Set<ReservationPaxOndCharge> totalCharges, BigDecimal paxTypeModCharge) {
		FareChargesTO fareChargesTO = fareTO.getFareChargesTO();

		// Since for this path we only get percentages, only PFS and PF can come. PF is handled automatcially
		// with out a if condition
		if (ChargeTypes.PERCENTAGE_OF_FARE_AND_SUR_PFS.getChargeTypes().equals(fareChargesTO.getModificationChargeType())) {
			paxFare = AccelAeroCalculator.add(paxFare, getTotalCharges(totalCharges));
		}
		if (paxTypeModCharge != null) {
			return getPaxFareCharge(fareChargesTO.getMinModificationAmount(), fareChargesTO.getMaximumModificationAmount(),
					paxTypeModCharge, paxFare);
		}
		return new BigDecimal(0);
	}

	/**
	 * Calculate Absolute Cancellation amount if cancellation amount defined as percentage
	 * 
	 * @author dumindaG
	 * @param fareTO
	 * @param paxFare
	 * @param totalCharges
	 * @return
	 */
	private static BigDecimal getAbsoluteCancellationCharges(FareTO fareTO, BigDecimal paxFare,
			Set<ReservationPaxOndCharge> totalCharges, BigDecimal paxTypeCnxAmount) {
		FareChargesTO fareChargesTO = fareTO.getFareChargesTO();

		// Since for this path we only get percentages, only PFS and PF can come. PF is handled automatcially
		// with out a if condition
		if (ChargeTypes.PERCENTAGE_OF_FARE_AND_SUR_PFS.getChargeTypes().equals(fareChargesTO.getCancellationChargeType())) {
			paxFare = AccelAeroCalculator.add(paxFare, getTotalCharges(totalCharges));
		}

		if (paxTypeCnxAmount != null) {
			return getPaxFareCharge(fareChargesTO.getMinCancellationAmount(), fareChargesTO.getMaximumCancellationAmount(),
					paxTypeCnxAmount, paxFare);
		}
		return new BigDecimal(0);
	}

	private static BigDecimal getAbsoluteNameChangeCharges(FareTO fareTO, BigDecimal paxFare,
			Set<ReservationPaxOndCharge> totalCharges, BigDecimal paxTypeNameChangeCharge) {
		FareChargesTO fareChargesTO = fareTO.getFareChargesTO();

		// Since for this path we only get percentages, only PFS and PF can come. PF is handled automatcially
		// with out a if condition
		if (ChargeTypes.PERCENTAGE_OF_FARE_AND_SUR_PFS.getChargeTypes().equals(fareChargesTO.getNameChangeChargeType())) {
			paxFare = AccelAeroCalculator.add(paxFare, getTotalCharges(totalCharges));
		}
		if (paxTypeNameChangeCharge != null) {
			return getPaxFareCharge(fareChargesTO.getMinNameChangeChargeAmount(), fareChargesTO.getMaxNameChangeChargeAmount(),
					paxTypeNameChangeCharge, paxFare);
		}
		return new BigDecimal(0);
	}

	/**
	 * Returns valid modification / cancellation charge based on min / max thresholds
	 * 
	 * @param minThreshhold
	 * @param maxThreshhold
	 * @param modificationCharge
	 * @param paxFare
	 * @return
	 */
	private static BigDecimal getPaxFareCharge(BigDecimal minThreshhold, BigDecimal maxThreshhold, BigDecimal paxTypeAmount,
			BigDecimal paxFare) {

		// When min/max thresholds are not defined, calculated percentage amount
		// will return
		if (minThreshhold != null && !AccelAeroCalculator.isGreaterThan(paxTypeAmount, new BigDecimal(0))) {
			return minThreshhold;
		}

		BigDecimal absPaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (AppSysParamsUtil.isRoundUpOverwriteFeeEnabled()) {
			absPaxCharge = AccelAeroCalculator.divide(AccelAeroCalculator.multiply(paxFare, paxTypeAmount), new BigDecimal(100),
					0);
		} else {
			absPaxCharge = AccelAeroCalculator.divide(AccelAeroCalculator.multiply(paxFare, paxTypeAmount), new BigDecimal(100));
		}

		if (minThreshhold != null && maxThreshhold != null) {
			if (absPaxCharge.doubleValue() < minThreshhold.doubleValue()) {
				return minThreshhold;
				// Safety condition if maxthreshold is set as zero by mistake
			} else if (maxThreshhold.doubleValue() > 1 && (absPaxCharge.doubleValue() > maxThreshhold.doubleValue())) {
				return maxThreshhold;
			} else {
				return absPaxCharge;
			}
		} else if (minThreshhold == null && maxThreshhold == null) {
			return absPaxCharge;
		} else if (minThreshhold != null && maxThreshhold == null) {
			if (absPaxCharge.doubleValue() < minThreshhold.doubleValue()) {
				return minThreshhold;
			} else {
				return absPaxCharge;
			}
		} else {
			if (maxThreshhold.doubleValue() > 1 && (absPaxCharge.doubleValue() > maxThreshhold.doubleValue())) {
				return maxThreshhold;
			} else {
				return absPaxCharge;
			}
		}

	}

	private static BigDecimal getTotalDiscountAmount(Set<ReservationPaxOndCharge> setCharges) {
		BigDecimal totalFareDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (setCharges != null && !setCharges.isEmpty()) {
			for (ReservationPaxOndCharge reservationPaxOndCharge : setCharges) {
				if (reservationPaxOndCharge.getDiscount() != null && reservationPaxOndCharge.getDiscount().doubleValue() < 0) {
					totalFareDiscount = AccelAeroCalculator.add(totalFareDiscount, reservationPaxOndCharge.getDiscount().abs());
				}
			}
		}
		return totalFareDiscount;
	}

	public static void calculateCustomChargesONDWise(CustomChargesTO customChargesTO, int ondCount) {

		BigDecimal[] ondWiseAdultCharges = AccelAeroCalculator.getBigDecimalArray(ondCount,
				AccelAeroCalculator.getDefaultBigDecimalZero());
		BigDecimal[] ondWiseChildCharges = AccelAeroCalculator.getBigDecimalArray(ondCount,
				AccelAeroCalculator.getDefaultBigDecimalZero());
		BigDecimal[] ondWiseInfantCharges = AccelAeroCalculator.getBigDecimalArray(ondCount,
				AccelAeroCalculator.getDefaultBigDecimalZero());
		boolean overideCharges = false;
		if (customChargesTO != null && RouteTypes.TOTAL_ROUTE.getRouteTypes().equals(customChargesTO.getRouteType())) {

			if (customChargesTO.getCustomAdultMCharge() != null && customChargesTO.getCustomAdultMCharge().doubleValue() > 0) {
				ondWiseAdultCharges = AccelAeroCalculator.roundAndSplit(customChargesTO.getCustomAdultMCharge(), ondCount);
			}
			if (customChargesTO.getCustomChildMCharge() != null && customChargesTO.getCustomChildMCharge().doubleValue() > 0) {
				ondWiseChildCharges = AccelAeroCalculator.roundAndSplit(customChargesTO.getCustomChildMCharge(), ondCount);
			}
			if (customChargesTO.getCustomInfantMCharge() != null && customChargesTO.getCustomInfantMCharge().doubleValue() > 0) {
				ondWiseInfantCharges = AccelAeroCalculator.roundAndSplit(customChargesTO.getCustomInfantMCharge(), ondCount);
			}
			overideCharges = true;
		}

		if (overideCharges) {
			Map<String, BigDecimal> ondWiseCustomAdultCharge = new HashMap<String, BigDecimal>();
			Map<String, BigDecimal> ondWiseCustomChildCharge = new HashMap<String, BigDecimal>();
			Map<String, BigDecimal> ondWiseCustomInfantCharge = new HashMap<String, BigDecimal>();

			for (int sequence = 0; sequence < ondCount; sequence++) {
				ondWiseCustomAdultCharge.put(sequence + "", ondWiseAdultCharges[sequence]);
				ondWiseCustomChildCharge.put(sequence + "", ondWiseChildCharges[sequence]);
				ondWiseCustomInfantCharge.put(sequence + "", ondWiseInfantCharges[sequence]);

			}

			customChargesTO.setOndWiseCustomAdultCharge(ondWiseCustomAdultCharge);
			customChargesTO.setOndWiseCustomChildCharge(ondWiseCustomChildCharge);
			customChargesTO.setOndWiseCustomInfantCharge(ondWiseCustomInfantCharge);

		}

	}

	public static void overrideCustomCNXMODCharges(CustomChargesTO customChargesTO, String ondCode, int modOndPosition) {

		if (customChargesTO != null) {

			String ondPosition = modOndPosition + "";

			Map<String, BigDecimal> ondWiseCustomAdultCharge = customChargesTO.getOndWiseCustomAdultCharge();
			Map<String, BigDecimal> ondWiseCustomChildCharge = customChargesTO.getOndWiseCustomChildCharge();
			Map<String, BigDecimal> ondWiseCustomInfantCharge = customChargesTO.getOndWiseCustomInfantCharge();
			if (ondWiseCustomAdultCharge != null && ondWiseCustomAdultCharge.size() > 0) {
				if (ondWiseCustomAdultCharge.get(ondCode) != null && ondWiseCustomAdultCharge.get(ondPosition) == null) {
					ondPosition = ondCode;
				}
				customChargesTO.setCustomAdultCCharge(ondWiseCustomAdultCharge.get(ondPosition));
				customChargesTO.setCustomAdultMCharge(ondWiseCustomAdultCharge.get(ondPosition));
			}
			if (ondWiseCustomChildCharge != null && ondWiseCustomChildCharge.size() > 0) {
				if (ondWiseCustomChildCharge.get(ondCode) != null && ondWiseCustomChildCharge.get(ondPosition) == null) {
					ondPosition = ondCode;
				}
				customChargesTO.setCustomChildCCharge(ondWiseCustomChildCharge.get(ondPosition));
				customChargesTO.setCustomChildMCharge(ondWiseCustomChildCharge.get(ondPosition));
			}
			if (ondWiseCustomInfantCharge != null && ondWiseCustomInfantCharge.size() > 0) {
				if (ondWiseCustomInfantCharge.get(ondCode) != null && ondWiseCustomInfantCharge.get(ondPosition) == null) {
					ondPosition = ondCode;
				}
				customChargesTO.setCustomInfantCCharge(ondWiseCustomInfantCharge.get(ondPosition));
				customChargesTO.setCustomInfantMCharge(ondWiseCustomInfantCharge.get(ondPosition));
			}
		}

	}

}
