package com.isa.thinair.airreservation.core.bl.external;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public abstract class AccontingSystemTemplate {
	
	public static final int MYSQL_DUPLICATE_PK = 2627; 

	private String accountingSystem;
	
	private String driverClass;
	
	private String serverIp;
	
	private String port;
	
	private String databaseServer;
	
	private String url;
	
	private String dbUserName;
	
	private String dbPassword;
	
	/**
	 * Loads Configuration
	 */
	public Properties getProperties() {
		Properties props = new Properties();
		props.put("accountingSystem", PlatformUtiltiies.nullHandler(accountingSystem));
		props.put("driverClass", PlatformUtiltiies.nullHandler(driverClass));
		props.put("serverIp", PlatformUtiltiies.nullHandler(serverIp));
		props.put("port", PlatformUtiltiies.nullHandler(port));
		props.put("databaseServer", PlatformUtiltiies.nullHandler(databaseServer));
		props.put("url", PlatformUtiltiies.nullHandler(url));
		props.put("dbUserName", PlatformUtiltiies.nullHandler(dbUserName));
		props.put("dbPassword", PlatformUtiltiies.nullHandler(dbPassword));		
		return props;
	}

	public String getAccountingSystem() {
		return accountingSystem;
	}

	public void setAccountingSystem(String accountingSystem) {
		this.accountingSystem = accountingSystem;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabaseServer() {
		return databaseServer;
	}

	public void setDatabaseServer(String databaseServer) {
		this.databaseServer = databaseServer;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}	
	
	//TODO Move to separate class for DB Logic 
	public Connection getExternalConnection() throws CommonsDataAccessException {		
		Connection connection = null;
		String driverName = getDriverClass();
		String url = getDBUrl();
		String username = getDbUserName();
		String password = getDbPassword();	
		try {
			Class.forName(driverName);
			connection = (Connection) DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			throw new CommonsDataAccessException(e, "exdb.loaddriver.failed");
		} catch (SQLException e) {
			throw new CommonsDataAccessException(e, "exdb.getconnection.failed");
		}
		return connection;
	}

	private String getDBUrl() {
		String serverName = getServerIp();
		String portNumber = getPort();
		String mydatabase = serverName + ":" + portNumber + getDatabaseServer();
		return getUrl() + mydatabase;
	}

	/**
	 * 
	 * @param connection
	 * @throws CommonsDataAccessException
	 */
	public static void rollback(Connection connection) throws CommonsDataAccessException {
		try {
			connection.rollback();
			connection.close();
		} catch (SQLException e) {
			throw new CommonsDataAccessException(e, "exdb.rollback.error");

		}
	}

	/**
	 * 
	 * @param connection
	 * @throws CommonsDataAccessException
	 */
	public static void commit(Connection connection) throws CommonsDataAccessException {
		try {
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			throw new CommonsDataAccessException(e, "exdb.commit.error");

		}
	}

	public DriverManagerDataSource getExternalDatasource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(getDriverClass());
		dataSource.setUrl(getDBUrl());
		dataSource.setUsername(getDbUserName());
		dataSource.setPassword(getDbPassword());
		return dataSource;
	}

}
