/* ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.AnciTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.MealDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * MealDAOImpl is the business DAO hibernate implementation
 * 
 * @since 1.0
 * @isa.module.dao-impl dao-name="MealDAO"
 */
public class MealDAOImpl extends PlatformBaseHibernateDaoSupport implements MealDAO {

	public void saveOrUpdate(Collection<PassengerMeal> meals) {
		super.hibernateSaveOrUpdateAll(meals);
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getMealCodes(Collection<Integer> flightmealIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" select ml.meal_name  ");
		sql.append("     from ml_t_flight_meal_charge fmc, ml_t_meal ml, ml_t_meal_charge mc  ");
		sql.append("    where fmc.meal_charge_id = mc.meal_charge_id  ");
		sql.append("     and mc.meal_id = ml.meal_id ");
		sql.append("  AND fmc.flight_meal_charge_id in (" + Util.constructINStringForInts(flightmealIds) + ")");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<String> colmeals = (Collection<String>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> passengermealCodes = new ArrayList<String>();
				while (rs.next()) {
					passengermealCodes.add(rs.getString("meal_name"));
				}
				return passengermealCodes;
			}
		});

		return colmeals;

	}

	@SuppressWarnings("unchecked")
	public Collection<PaxMealTO> getReservationMeals(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {

		String sql = " select ppml.pnr_pax_seg_meal_id, ppml.pnr_pax_id, ppml.pnr_seg_id, ppml.amount, ppml.flight_meal_charge_id, ppml.status , ppml.meal_id, ppml.ppf_id, ml.meal_name, ml.meal_code, ml.meal_category_id, mc.meal_category, ppml.auto_cancellation_id, mch.meal_template_id , "
				+ "  (select rest.meal_category_id from ML_T_MEAL_TEMPL_RESTRICT_CAT rest where rest.meal_template_id =mch.meal_template_id and rest.meal_category_id =mc.meal_category_id) as restricted "
				+ " from ml_t_pnr_pax_seg_meal ppml "
				+ " , ml_t_meal ml,  ml_t_meal_category mc,"
				+ " ml_t_flight_meal_charge fmc, ml_t_meal_charge mch"
				+ " where ppml.meal_id = ml.meal_id "
				+ " AND ml.meal_category_id = mc.meal_category_id "
				+ " AND ppml.status = '"
				+ AirinventoryCustomConstants.FlightMealStatuses.RESERVED
				+ "'"
				+ " AND ppml.pnr_seg_id in ("
				+ Util.constructINStringForInts(pnrSegIds)
				+ ") "
				+ " AND ppml.pnr_pax_id in ("
				+ Util.constructINStringForInts(pnrPaxIds) + ")" 
				+ " AND ppml.flight_meal_charge_id = fmc.flight_meal_charge_id"
				+ " AND fmc.meal_charge_id = mch.meal_charge_id"
				+ " Order by ppml.pnr_seg_id";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<PaxMealTO> colMeals = (Collection<PaxMealTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxMealTO> passengermeals = new ArrayList<PaxMealTO>();
				while (rs.next()) {
					PaxMealTO paxmealTO = new PaxMealTO();
					paxmealTO.setSelectedFlightMealId(new Integer(rs.getInt("flight_meal_charge_id")));
					paxmealTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
					paxmealTO.setPnrSegId(new Integer(rs.getInt("pnr_seg_id")));
					paxmealTO.setMealCode(rs.getString("meal_code"));
					paxmealTO.setMealName(rs.getString("meal_name"));
					paxmealTO.setPkey(new Integer(rs.getInt("pnr_pax_seg_meal_id")));
					paxmealTO.setPnrPaxFareId(new Integer(rs.getInt("ppf_id")));
					paxmealTO.setMealId(new Integer(rs.getInt("meal_id")));
					paxmealTO.setMealCategoryID(new Integer(rs.getInt("meal_category_id")));
					paxmealTO.setMealCategoryCode(rs.getString("meal_category"));
					
					if (AppSysParamsUtil.isAutoCancellationEnabled() && rs.getInt("auto_cancellation_id") != 0) {
						paxmealTO.setAutoCancellationId(rs.getInt("auto_cancellation_id"));
					}

					MealExternalChgDTO externalChgDTO = new MealExternalChgDTO();
					externalChgDTO.setAmount(rs.getBigDecimal("amount"));
					externalChgDTO.setOfferedTemplateID(new Integer(rs.getInt("meal_template_id")));
					
					paxmealTO.setChgDTO(externalChgDTO);
					paxmealTO.setCategoryRestricted(rs.getString("restricted") !=null?true:false);
					passengermeals.add(paxmealTO);
				}
				return passengermeals;
			}
		});

		return colMeals;

	}

	@SuppressWarnings("unchecked")
	public Collection<PaxMealTO> getReservationMeals(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			String preferredLanguage) {

		String sql = " select ppml.pnr_pax_seg_meal_id, ppml.pnr_pax_id, ppml.pnr_seg_id, ppml.amount, ppml.flight_meal_charge_id, ppml.status , ppml.meal_id, ppml.ppf_id, ml.meal_name, ml.meal_code, md.meal_title_ol,ml.meal_category_id, mc.meal_category, ppml.auto_cancellation_id , "
				+ " (select rest.meal_category_id from ML_T_MEAL_TEMPL_RESTRICT_CAT rest where rest.meal_template_id =mch.meal_template_id and rest.meal_category_id =mc.meal_category_id) as restricted "
				+ " from ml_t_pnr_pax_seg_meal ppml , ml_t_flight_meal_charge fmc,ml_t_meal_charge mch, "
				+ " ml_t_meal ml LEFT OUTER JOIN ml_t_meal_for_display md "
				+ " ON ml.meal_id = md.meal_id AND md.language_code = '"
				+ preferredLanguage
				+ "',  ml_t_meal_category mc "
				+ " where ppml.meal_id = ml.meal_id "
				+ " AND ml.meal_category_id = mc.meal_category_id "
				+ " AND   ppml.flight_meal_charge_id = fmc.flight_meal_charge_id "
				+ " AND   fmc.meal_charge_id = mch.meal_charge_id "
				+ " AND ppml.status = '"
				+ AirinventoryCustomConstants.FlightMealStatuses.RESERVED
				+ "'"
				+ " AND ppml.pnr_seg_id in ("
				+ Util.constructINStringForInts(pnrSegIds)
				+ ") "
				+ " AND ppml.pnr_pax_id in ("
				+ Util.constructINStringForInts(pnrPaxIds) + ")" + " Order by ppml.pnr_seg_id";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<PaxMealTO> colMeals = (Collection<PaxMealTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxMealTO> passengermeals = new ArrayList<PaxMealTO>();
				while (rs.next()) {
					PaxMealTO paxmealTO = new PaxMealTO();
					paxmealTO.setSelectedFlightMealId(new Integer(rs.getInt("flight_meal_charge_id")));
					paxmealTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
					paxmealTO.setPnrSegId(new Integer(rs.getInt("pnr_seg_id")));
					paxmealTO.setMealCode(rs.getString("meal_code"));
					paxmealTO.setMealName(rs.getString("meal_name"));
					paxmealTO.setPkey(new Integer(rs.getInt("pnr_pax_seg_meal_id")));
					paxmealTO.setPnrPaxFareId(new Integer(rs.getInt("ppf_id")));
					paxmealTO.setMealId(new Integer(rs.getInt("meal_id")));
					paxmealTO.setTranslatedMealName(rs.getString("meal_title_ol"));
					paxmealTO.setMealCategoryID(new Integer(rs.getInt("meal_category_id")));
					paxmealTO.setMealCategoryCode(rs.getString("meal_category"));

					if (AppSysParamsUtil.isAutoCancellationEnabled() && rs.getInt("auto_cancellation_id") != 0) {
						paxmealTO.setAutoCancellationId(rs.getInt("auto_cancellation_id"));
					}

					MealExternalChgDTO externalChgDTO = new MealExternalChgDTO();
					externalChgDTO.setAmount(rs.getBigDecimal("amount"));
					paxmealTO.setChgDTO(externalChgDTO);
					paxmealTO.setCategoryRestricted(rs.getString("restricted") !=null?true:false);
					passengermeals.add(paxmealTO);
				}
				return passengermeals;
			}
		});

		return colMeals;

	}

	public Collection<PassengerMeal> getFlightMealsForPnrPaxFare(Collection<Integer> ppfIds) {
		Collection<PassengerMeal> passengermeals = new ArrayList<PassengerMeal>();
		String hql = "from PassengerMeal s where s.status = '" + AirinventoryCustomConstants.FlightMealStatuses.RESERVED
				+ "' and s.pPFID in (" + Util.buildIntegerInClauseContent(ppfIds) + ")";
		passengermeals = find(hql, PassengerMeal.class);
		return passengermeals;

	}

	public Collection<PassengerMeal> getFlightMealsForPnrPax(Collection<Integer> pnrPaxIds) {
		Collection<PassengerMeal> passengermeals = new ArrayList<PassengerMeal>();
		String hql = "from PassengerMeal s where s.status = '" + AirinventoryCustomConstants.FlightMealStatuses.RESERVED
				+ "' and s.pnrPaxId in (" + Util.buildIntegerInClauseContent(pnrPaxIds) + ")";
		passengermeals = find(hql, PassengerMeal.class);
		return passengermeals;

	}

	@SuppressWarnings("all")
	public Map<Integer, String> getMealDetailNames(Collection<Integer> flightMealIds) {
		StringBuilder sql = new StringBuilder();
		sql.append(" select ml.meal_name, ml.meal_id ");
		sql.append(" from ml_t_meal ml ");
		sql.append(" WHERE ");
		sql.append(" ml.meal_id in (" + Util.constructINStringForInts(flightMealIds) + ")");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> mapMeals = (Map) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map mapPaxMealCodes = new HashMap();
				while (rs.next()) {
					mapPaxMealCodes.put(rs.getInt("meal_id"), rs.getString("meal_name"));
				}
				return mapPaxMealCodes;
			}
		});

		return mapMeals;

	}

	/*
	 * Method send meal Details to Pnl
	 * 
	 * @see com.isa.thinair.airreservation.core.persistence.dao.MealDAO#getReservationMealsForPNL(java.util.Collection,
	 * java.util.Collection)
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getReservationMealsForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {

		final boolean mulipleMealsSelectionEnabled = AppSysParamsUtil.isMultipleMealSelectionEnabled();

		StringBuilder sql = new StringBuilder();
		sql.append(" select ppml.pnr_pax_id, ml.meal_code, ml.iata_code,count(ml.meal_code) as meal_count ");
		sql.append(" from ml_t_pnr_pax_seg_meal ppml, ml_t_flight_meal_charge fml, ml_t_meal_charge mlc, ml_t_meal ml ");
		sql.append(" where ppml.flight_meal_charge_id = fml.flight_meal_charge_id ");
		sql.append(" AND fml.meal_charge_id = mlc.meal_charge_id ");
		sql.append(" AND mlc.meal_id = ml.meal_id ");
		sql.append(" AND ppml.status = '" + AirinventoryCustomConstants.FlightMealStatuses.RESERVED + "'");
		sql.append(" AND ppml.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		sql.append(" AND ppml.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		sql.append(" GROUP BY ppml.pnr_pax_id,ml.meal_code,ml.iata_code ");
		sql.append(" order by ppml.pnr_pax_id, ml.meal_code ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> pnrMealMap = (Map<Integer, String>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> pnrMealMap = new HashMap<Integer, String>();

				while (rs.next()) {
					Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
					String paxMealName = pnrMealMap.get(pnrPaxId);
					String iataCode = rs.getString("iata_code");
					String mealCode = rs.getString("meal_code");
					String mealCount = rs.getString("meal_count");

					if (AppSysParamsUtil.enableIATAMealCodesInPNLADL()) {
						if (paxMealName == null) {
							paxMealName = iataCode;
						} else {
							paxMealName = iataCode;
						}
					} else {
						if (paxMealName == null) {
							paxMealName = mealCode;
							if (mealCode != null && !mealCode.equals("")) {
								if (mulipleMealsSelectionEnabled) {
									paxMealName += "%" + mealCount + mealCode;
								} else {
									paxMealName += "%" + mealCode;
								}
							}

						} else {
							if (mulipleMealsSelectionEnabled) {
								paxMealName += " " + mealCount + mealCode;
							} else {
								paxMealName += " " + mealCode;
							}
						}
					}
					pnrMealMap.put(pnrPaxId, paxMealName);
				}
				return pnrMealMap;
			}
		});

		return pnrMealMap;
	}

	public Map<Integer, List<AncillaryDTO>> getReservationMealsForPNLADL(Collection<Integer> pnrPaxIds,
			Collection<Integer> pnrSegIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" select ppml.pnr_pax_id, ml.meal_code, ml.iata_code");
		sql.append(" from ml_t_pnr_pax_seg_meal ppml, ml_t_flight_meal_charge fml, ml_t_meal_charge mlc, ml_t_meal ml ");
		sql.append(" where ppml.flight_meal_charge_id = fml.flight_meal_charge_id ");
		sql.append(" AND fml.meal_charge_id = mlc.meal_charge_id ");
		sql.append(" AND mlc.meal_id = ml.meal_id ");
		sql.append(" AND ppml.status = '" + AirinventoryCustomConstants.FlightMealStatuses.RESERVED + "'");
		sql.append(" AND ppml.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		sql.append(" AND ppml.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		// sql.append(" GROUP BY ppml.pnr_pax_id,ml.meal_code,ml.iata_code ");
		sql.append(" order by ppml.pnr_pax_id, ml.meal_code ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Map<Integer, List<AncillaryDTO>> pnrMealMap = (Map<Integer, List<AncillaryDTO>>) template.query(sql.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, List<AncillaryDTO>> pnrMealMap = new HashMap<Integer, List<AncillaryDTO>>();

						while (rs.next()) {
							Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
							if (pnrMealMap.get(pnrPaxId) == null) {
								List<AncillaryDTO> seats = new ArrayList<AncillaryDTO>();
								AncillaryDTO anci = new AncillaryDTO();
								anci.setAnciType(AnciTypes.MEAL);
								if (AppSysParamsUtil.enableIATAMealCodesInPNLADL()) {
									anci.setCode(rs.getString("iata_code"));
								}else {
									anci.setCode(rs.getString("meal_code").toUpperCase());
								}
								anci.setDescription(rs.getString("meal_code").toUpperCase());
								seats.add(anci);
								pnrMealMap.put(pnrPaxId, seats);

							} else {
								AncillaryDTO anci = new AncillaryDTO();
								anci.setAnciType(AnciTypes.MEAL);
								if (AppSysParamsUtil.enableIATAMealCodesInPNLADL()) {
									anci.setCode(rs.getString("iata_code"));
								}else {
									anci.setCode(rs.getString("meal_code").toUpperCase());
								}
								anci.setDescription(rs.getString("meal_code").toUpperCase());
								pnrMealMap.get(pnrPaxId).add(anci);

							}

						}
						return pnrMealMap;
					}
				});

		return pnrMealMap;
	}

	/**
	 * Method will return template Id for particular flight segment id.
	 */
	public Integer getTemplateForSegmentId(int flightSegId) {

		String sql = "SELECT meal_template_id FROM ml_t_meal_template WHERE meal_template_id "
				+ " IN( SELECT c.meal_template_id FROM ml_t_meal_charge c WHERE c.meal_charge_id "
				+ " IN( SELECT b.meal_charge_id FROM ml_t_flight_meal_charge b " + " WHERE b.flt_seg_id =" + flightSegId
				+ " and b.status = 'ACT'))";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Integer) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer templateId = null;
				while (rs.next()) {
					templateId = rs.getInt("meal_template_id");
				}
				return templateId;
			}
		});
	}

	public Collection<PassengerMeal> getPassengerMeals(Collection<Integer> paxMealIds) {
		Collection<PassengerMeal> passengermeals = new ArrayList<PassengerMeal>();
		String hql = "from PassengerMeal s where s.status = '" + AirinventoryCustomConstants.FlightMealStatuses.RESERVED
				+ "' and s.paxMealId in (" + Util.buildIntegerInClauseContent(paxMealIds) + ")";
		passengermeals = find(hql, PassengerMeal.class);
		return passengermeals;

	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<ReservationLiteDTO, List<PaxMealTO>> getExpiredMeals(Collection<Integer> cancellationIds, String marketingCarrier) {
		boolean isDry = AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier) ? false : true;

		StringBuilder query = new StringBuilder();
		query.append("SELECT r.pnr, pp.pnr_pax_id, ps.pnr_seg_id, ml.ppf_id, ml.flight_meal_charge_id, ml.meal_id, ml.amount charge, r.version, ml.auto_cancellation_id");
		query.append(" FROM t_reservation r, t_pnr_passenger pp, t_pnr_segment ps,  ml_t_pnr_pax_seg_meal ml");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id = ml.pnr_pax_id AND ps.pnr_seg_id = ml.pnr_seg_id");
		query.append(" AND r.status = ? AND pp.status = ? AND ps.status =? AND ml.status <>? AND r.DUMMY_BOOKING = ? AND ");
		query.append(Util.getReplaceStringForIN("ml.auto_cancellation_id", cancellationIds));
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.ORIGIN_CHANNEL_CODE ");
		if (isDry) {
			query.append("= ?");
		} else {
			query.append("<> ?");
		}

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), marketingCarrier,
				ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Map<ReservationLiteDTO, List<PaxMealTO>>) template.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<ReservationLiteDTO, List<PaxMealTO>> pnrWiseMeals = null;
				if (rs != null) {
					pnrWiseMeals = new HashMap<ReservationLiteDTO, List<PaxMealTO>>();
					while (rs.next()) {
						String pnr = rs.getString("pnr");
						long version = rs.getLong("version");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");

						ReservationLiteDTO resDTO = new ReservationLiteDTO(pnr, version, autoCancellationId);

						if (!pnrWiseMeals.containsKey(resDTO)) {
							pnrWiseMeals.put(resDTO, new ArrayList<PaxMealTO>());
						}

						PaxMealTO paxMealTO = new PaxMealTO();
						paxMealTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
						paxMealTO.setPnrSegId(rs.getInt("pnr_seg_id"));
						paxMealTO.setPnrPaxFareId(rs.getInt("ppf_id"));
						paxMealTO.setSelectedFlightMealId(rs.getInt("flight_meal_charge_id"));
						paxMealTO.setMealId(rs.getInt("meal_id"));
						paxMealTO.setChgDTO(new ExternalChgDTO());
						paxMealTO.getChgDTO().setAmount(rs.getBigDecimal("charge"));
						paxMealTO.setAllocatedQty(1);
						pnrWiseMeals.get(resDTO).add(paxMealTO);

					}
				}
				return pnrWiseMeals;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccMealsInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, ml.auto_cancellation_id ");
		query.append(" FROM t_reservation r, t_pnr_passenger pp, t_pnr_segment ps,  ml_t_pnr_pax_seg_meal ml");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id = ml.pnr_pax_id AND ps.pnr_seg_id = ml.pnr_seg_id");
		query.append(" AND r.status = ? AND pp.status = ? AND ps.status =? AND ml.status <>? AND r.ORIGIN_CHANNEL_CODE = ? ");
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.DUMMY_BOOKING = ? AND ml.auto_cancellation_id IN (");
		query.append(BeanUtils.constructINStringForInts(cancellationIds) + ")");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL, ReservationInternalConstants.SalesChannel.LCC,
				marketingCarrier, String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) template.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						autoCnxPNRs.put(pnr, autoCnxId);
					}
				}

				return autoCnxPNRs;
			}
		});
	}
}
