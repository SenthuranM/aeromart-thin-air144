package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameChangeExtChgDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ExternalNameChangeChargeBL {

	private ExternalNameChangeChargeBL() {

	}

	public static void reflectExternalChgForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Map<Integer, RevenueDTO> passengerRevenueMap,
			CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {
		// Apply these changes for the reservation
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation,
				pnrPaxIdAndPayments, credentialsDTO, chgTxnGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);

		// Apply these changes to the passenger revenue map
		addExternalChgsForRevenueMap(mapPnrPaxIdAdjustments, passengerRevenueMap);
	}

	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator chgTxnGen)
			throws ModuleException {
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		// BigDecimal appropriateChargePerPax;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> nameChangeExternalChgs;
		boolean updateExist = false;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());
				nameChangeExternalChgs = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.NAME_CHANGE_CHARGE);

				if (nameChangeExternalChgs.size() > 0) {
					for (ExternalChgDTO externalChgDTO : nameChangeExternalChgs) {
						NameChangeExtChgDTO nameChangeExternalChgDTO = (NameChangeExtChgDTO) externalChgDTO;
						reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
								nameChangeExternalChgDTO.getFlightSegId(), true);
						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
								nameChangeExternalChgDTO.getAmount(), null, nameChangeExternalChgDTO.getChgRateId(),
								nameChangeExternalChgDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, null, null,
								null, chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));
						// appropriateChargePerPax = smExternalChgDTO.getAmount();
						lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(reservationPax.getPnrPaxId());

						if (lstReservationPaxOndCharge == null) {
							lstReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
							lstReservationPaxOndCharge.add(reservationPaxOndCharge);

							mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), lstReservationPaxOndCharge);
						} else {
							lstReservationPaxOndCharge.add(reservationPaxOndCharge);
						}

						updateExist = true;
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	private static void addExternalChgsForPaymentAssembler(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
					paymentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(),
							reservationPaxOndCharge.getAmount()));
				}
			}
		}
	}

	private static void addExternalChgsForRevenueMap(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, RevenueDTO> passengerRevenueMap) {
		Iterator<Integer> itPnrPaxIds = passengerRevenueMap.keySet().iterator();
		RevenueDTO revenueDTO;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				revenueDTO = (RevenueDTO) passengerRevenueMap.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
					revenueDTO.setAddedTotal(
							AccelAeroCalculator.add(revenueDTO.getAddedTotal(), reservationPaxOndCharge.getAmount()));
					revenueDTO.getAddedCharges().add(reservationPaxOndCharge);
				}
			}
		}
	}

}
