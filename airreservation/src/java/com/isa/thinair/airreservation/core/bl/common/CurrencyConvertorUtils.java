package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

/**
 * @author Nilindra Fernando
 * @since 3:51 PM 1/5/2009
 */
class CurrencyConvertorUtils {

	protected static Collection<ReservationTnx> getAllReservationTnxs(Collection<Collection<ReservationTnx>> colReservationTnx) {
		Collection<ReservationTnx> allTnxs = new ArrayList<ReservationTnx>();

		for (Collection<ReservationTnx> singleColReservationTnx : colReservationTnx) {
			allTnxs.addAll(singleColReservationTnx);
		}

		return allTnxs;
	}

	protected static Collection<Integer> getTnxIds(Collection<ReservationTnx> colReservationTnx) {
		Collection<Integer> colTnxIds = new ArrayList<Integer>();

		for (ReservationTnx reservationTnx : colReservationTnx) {
			colTnxIds.add(reservationTnx.getTnxId());
		}

		return colTnxIds;
	}

	protected static Currency getCurrency(String currencyCode) throws ModuleException {
		Currency currency = ReservationModuleUtils.getCommonMasterBD().getCurrency(currencyCode);

		if (currency == null) {
			throw new ModuleException("airreservations.exchangerate.undefined");
		}

		return currency;
	}

	protected static BigDecimal getAmountInPayCurrency(BigDecimal amountInBaseCurrency, PayCurrencyDTO payCurrencyDTO) {
		return AccelAeroRounderPolicy.convertAndRound(amountInBaseCurrency, payCurrencyDTO.getPayCurrMultiplyingExchangeRate(),
				payCurrencyDTO.getBoundaryValue(), payCurrencyDTO.getBreakPointValue());
	}

	/**
	 * Gets the currency conversion rates from the passed transactions. Rate is for one unit of local currency. If paid
	 * amount in local currency is 200USD and paid amount in actual paid currency is 20000LKR returns 20000/200 = 100.
	 * Multiplier is calculated up to 10 decimal points
	 * 
	 * @param txnList
	 *            List of transactions
	 * @return A Map with TransactionID as the key and exchange rate multiplier as the value.
	 */
	protected static Map<Integer, BigDecimal> getExchangeRateOfTxn(List<ReservationTnx> txnList) {
		Map<Integer, BigDecimal> resultMap = new HashMap<Integer, BigDecimal>();
		for (ReservationTnx txn : txnList) {
			BigDecimal exchangeMultiplier = BigDecimal.ONE;
			if (txn.getPayCurrencyCode() != null && txn.getPayCurrencyAmount() != null) {
				exchangeMultiplier = txn.getPayCurrencyAmount().divide(txn.getAmount(), 10, BigDecimal.ROUND_UP);
			}
			resultMap.put(txn.getTnxId(), exchangeMultiplier);
		}
		return resultMap;
	}
}