package com.isa.thinair.airreservation.core.bl.ticketing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateRequestDTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateResponseDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegEtktTransition;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.TicketGeneratorCriteriaType;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.util.IATAETicketGenerator;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTicketSequnce;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.FinalEticketStatus;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

/**
 * @author eric
 */
public class ETicketBO {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ETicketBO.class);

	/**
	 * this is to use when creating the fresh reservation , create reservation
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForFreshBooking(
			Collection<ReservationPax> confirmPassengers, Reservation reservation, CreateTicketInfoDTO ticketingInfoDto)
			throws ModuleException {

		// Segments are sorted from departure time
		Collection<Integer> unflownPnrSegmentIds = ETicketBO.getUnflownReservationSegmentIds(reservation.getPnr());

		List<ReservationSegment> unflownSegmentList = (ArrayList<ReservationSegment>) ReservationApiUtils.getUnflownSegments(
				reservation, unflownPnrSegmentIds);
		// sort segments
		return ETicketBO.generateIATAETicketNumbers(confirmPassengers, unflownSegmentList, ticketingInfoDto);
	}

	/**
	 * this is to use when creating the GOSHOW reservation ,
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForGOSHOWBooking(
			Collection<ReservationPax> confirmPassengers, Reservation reservation, CreateTicketInfoDTO ticketingInfoDto)
			throws ModuleException {

		List<ReservationSegment> unflownSegmentList = new ArrayList<ReservationSegment>(reservation.getSegments());
		// sort segments
		return ETicketBO.generateIATAETicketNumbers(confirmPassengers, unflownSegmentList, ticketingInfoDto);
	}

	/**
	 * this is to use when creating the fresh reservation , create reservation
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForFreshBooking(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {

		// Segments are sorted from departure time
		Collection<Integer> unflownPnrSegmentIds = ETicketBO.getUnflownReservationSegmentIds(reservation.getPnr());

		List<ReservationSegment> unflownSegmentList = (ArrayList<ReservationSegment>) ReservationApiUtils.getUnflownSegments(
				reservation, unflownPnrSegmentIds);

		return ETicketBO.generateIATAETicketNumbers(reservation.getPassengers(), unflownSegmentList, ticketingInfoDto);
	}

	/**
	 * this is to use when modify booking
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForModifyBooking(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {

		// Segments are sorted from departure time
		Collection<Integer> unflownPnrSegmentIds = ETicketBO.getUnflownReservationSegmentIds(reservation.getPnr());

		List<ReservationSegment> unflownSegmentList = (ArrayList<ReservationSegment>) ReservationApiUtils.getUnflownSegments(
				reservation, unflownPnrSegmentIds);

		return ETicketBO.generateIATAETicketNumbers(reservation.getPassengers(), unflownSegmentList, ticketingInfoDto);
	}

	/**
	 *
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersAsSubstitution(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {

		// Segments are sorted from departure time
		Collection<Integer> unflownPnrSegmentIds = ETicketBO.getUnflownReservationSegmentIds(reservation.getPnr());

		List<ReservationSegment> unflownSegmentList = (ArrayList<ReservationSegment>) ReservationApiUtils.getUnflownSegments(
				reservation, unflownPnrSegmentIds);

		return ETicketBO.generateIATAETicketNumbers(reservation.getPassengers(), unflownSegmentList, ticketingInfoDto);
	}

	/**
	 * this is to use when add an infant - e tickets generated only for selected passenger set
	 * 
	 * @param reservation
	 * @param passengers
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForModifyBooking(Reservation reservation,
			Collection<ReservationPax> passengers, CreateTicketInfoDTO tktInfoDto) throws ModuleException {

		// Segment ids are sorted from departure time
		Collection<Integer> unflownPnrSegmentIds = ETicketBO.getUnflownReservationSegmentIds(reservation.getPnr());

		List<ReservationSegment> unflownSegmentList = (ArrayList<ReservationSegment>) ReservationApiUtils.getUnflownSegments(
				reservation, unflownPnrSegmentIds);

		return ETicketBO.generateIATAETicketNumbers(passengers, unflownSegmentList, tktInfoDto);
	}

	/**
	 * this is to use generate etickets for selected segments
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForGivenSegments(Reservation reservation,
			List<ReservationSegment> newSegmentList, CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {

		return ETicketBO.generateIATAETicketNumbers(reservation.getPassengers(), newSegmentList, ticketingInfoDto);
	}

	/**
	 * method to generate the e ticket numbers
	 * 
	 * @param reservation
	 * @param unflownSegmentList
	 * @return
	 */
	private static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbers(Collection<ReservationPax> passengers,
			List<ReservationSegment> unflownSegmentList, CreateTicketInfoDTO ticketingInfoDto) throws ModuleException {

		int numberOfEtickets = IATAETicketGenerator.getNoOfTickets(unflownSegmentList.size());
		String airLineCode = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BSP_TICKETING_AIRLINE_CODE);

		Map<Integer, Map<Integer, EticketDTO>> paxIATAEticketNumbers = new HashMap<Integer, Map<Integer, EticketDTO>>();

		TicketGeneratorCriteria ticketGeneratorCriteria = ETicketBO.createTicketGenerateCriteria(ticketingInfoDto);

		for (Iterator<ReservationPax> iterPassengers = passengers.iterator(); iterPassengers.hasNext();) {
			ReservationPax reservationPax = iterPassengers.next();

			Map<Integer, EticketDTO> eTickets = new HashMap<Integer, EticketDTO>();
			for (int eTCount = 1; eTCount <= numberOfEtickets; eTCount++) {
				int couponNo = 1;
				String serialNumber = ETicketBO.getNextTicketSequenceNo(ticketGeneratorCriteria);
				String eTicketNum = IATAETicketGenerator.getIATAETicketNumber(airLineCode, ticketGeneratorCriteria.getFormCode(),
						serialNumber);

				for (int segIndex = (eTCount - 1) * IATAETicketGenerator.NO_OF_COUPONS_PER_TICKET; segIndex < eTCount
						* IATAETicketGenerator.NO_OF_COUPONS_PER_TICKET; segIndex++) {
					if (segIndex >= unflownSegmentList.size()) {
						break;
					}
					ReservationSegment resSegment = unflownSegmentList.get(segIndex);
					EticketDTO eticketDto = new EticketDTO(eTicketNum, couponNo);
					eTickets.put(resSegment.getSegmentSeq(), eticketDto);
					couponNo++;
				}
			}
			paxIATAEticketNumbers.put(reservationPax.getPaxSequence(), eTickets);
		}
		return paxIATAEticketNumbers;
	}

	/**
	 * @param passengers
	 * @param lccSegments
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersForDryInterLine(
			Collection<Integer> passengers, List<Integer> lccSegments, CreateTicketInfoDTO ticketInfoDto) throws ModuleException {

		Map<Integer, Map<Integer, EticketDTO>> eTicketNumbers = new HashMap<Integer, Map<Integer, EticketDTO>>();

		int numberOfEtickets = IATAETicketGenerator.getNoOfTickets(lccSegments.size());
		String airLineCode = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BSP_TICKETING_AIRLINE_CODE);

		TicketGeneratorCriteria ticketGeneratorCriteria = ETicketBO.createTicketGenerateCriteria(ticketInfoDto);

		Iterator<Integer> passengerIterator = passengers.iterator();
		while (passengerIterator.hasNext()) {
			Integer paxSequnce = passengerIterator.next();

			Map<Integer, EticketDTO> eTickets = new HashMap<Integer, EticketDTO>();
			for (int eTCount = 1; eTCount <= numberOfEtickets; eTCount++) {
				int couponNo = 1;

				String serialNumber = ETicketBO.getNextTicketSequenceNo(ticketGeneratorCriteria);
				String eTicketNum = IATAETicketGenerator.getIATAETicketNumber(airLineCode, ticketGeneratorCriteria.getFormCode(),
						serialNumber);

				for (int segIndex = (eTCount - 1) * IATAETicketGenerator.NO_OF_COUPONS_PER_TICKET; segIndex < eTCount
						* IATAETicketGenerator.NO_OF_COUPONS_PER_TICKET; segIndex++) {
					if (segIndex >= lccSegments.size()) {
						break;
					}
					EticketDTO eticketDto = new EticketDTO(eTicketNum, couponNo);
					eTickets.put(lccSegments.get(segIndex), eticketDto);
					couponNo++;
				}
			}
			eTicketNumbers.put(paxSequnce, eTickets);
		}
		return eTicketNumbers;
	}

	/**
	 * @param paxEticketNumbers
	 * @param reservation
	 * @throws ModuleException
	 */
	public static void saveETickets(Map<Integer, Map<Integer, EticketDTO>> paxEticketNumbers,
			Collection<ReservationPax> passengers, Reservation reservation) throws ModuleException {

		Iterator<ReservationPax> iteReservationPax = passengers.iterator();

		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();
		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			Map<Integer, EticketDTO> eTicketNumbers = paxEticketNumbers.get(reservationPax.getPaxSequence());
			if (eTicketNumbers == null) {
				continue;
			}
			Map<String, ReservationPaxETicket> eTickets = new HashMap<String, ReservationPaxETicket>();

			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

				ReservationPaxETicket resETicket = null;
				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();

					EticketDTO eticketDTO = eTicketNumbers.get(paxfareSeg.getSegment().getSegmentSeq());

					if (eticketDTO != null) {

						ReservationPaxFareSegmentETicket resPaxFareSegEticket = new ReservationPaxFareSegmentETicket();
						resPaxFareSegEticket.setPnrPaxFareSegId(paxfareSeg.getPnrPaxFareSegId());
						// since new eticket issue, no need validation
						resPaxFareSegEticket.setStatus(EticketStatus.OPEN.code());
						resPaxFareSegEticket.setCouponNo(eticketDTO.getCouponNo());

						resETicket = eTickets.get(eticketDTO.getEticketNo());
						if (resETicket == null) {
							resETicket = new ReservationPaxETicket();
							resETicket.setEticketNumber(eticketDTO.getEticketNo());
							resETicket.setPnrPaxId(reservationPax.getPnrPaxId());
							resETicket.seteTicketStatus(ReservationPaxETicket.PURCHASED);
							eTickets.put(eticketDTO.getEticketNo(), resETicket);
						}
						resPaxFareSegEticket.setReservationPaxETicket(resETicket);
						resPaxFareSegEticketList.add(resPaxFareSegEticket);
					}
				}
			}
		}
		// save etickets
		if (!resPaxFareSegEticketList.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEticketList);
		}

		// send SSR TKNE message for codeshare segments
		sendSSRTKNEforNewETs(paxEticketNumbers, reservation);
	}

	private static void sendSSRTKNEforNewETs(Map<Integer, Map<Integer, EticketDTO>> eTicketInfo, Reservation reservation)
			throws ModuleException {
		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodesInConfirmedSegments(reservation);
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.seteTicketInfo(eTicketInfo);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.TICKETING.getCode());
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}
	}

	/**
	 * @param paxEticketNumbers
	 * @param reservation
	 * @throws ModuleException
	 */
	public static Collection<ReservationPaxFareSegmentETicket> updateETickets(
			Map<Integer, Map<Integer, EticketDTO>> paxEticketNumbers, Collection<ReservationPax> passengers,
			Reservation reservation, boolean exchangePreviousEtickets) throws ModuleException {

		Iterator<ReservationPax> iteReservationPax = passengers.iterator();

		// new e ticket list
		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();
		// modified e ticket list
		Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
		
		Map<Integer, Map<Integer, EticketDTO>> actualPaxEticketNumbers = new HashMap<Integer, Map<Integer, EticketDTO>>();
				
		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			Map<Integer, EticketDTO> eTicketNumbers = paxEticketNumbers.get(reservationPax.getPaxSequence());
			if (eTicketNumbers == null) {
				continue;
			}
			actualPaxEticketNumbers.put(reservationPax.getPaxSequence(), eTicketNumbers);
			
			Map<String, ReservationPaxETicket> eTickets = new HashMap<String, ReservationPaxETicket>();

			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());

			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

				ReservationPaxETicket resETicket = null;
				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();
					EticketDTO eticketDTO = eTicketNumbers.get(paxfareSeg.getSegment().getSegmentSeq());

					if (eticketDTO != null) {
						ReservationPaxFareSegmentETicket resPaxFareSegEticket = new ReservationPaxFareSegmentETicket();
						resPaxFareSegEticket.setPnrPaxFareSegId(paxfareSeg.getPnrPaxFareSegId());
						// since new eticket issue, no need validation
						resPaxFareSegEticket.setStatus(EticketStatus.OPEN.code());
						resPaxFareSegEticket.setCouponNo(eticketDTO.getCouponNo());

						resETicket = eTickets.get(eticketDTO.getEticketNo());
						if (resETicket == null) {
							resETicket = new ReservationPaxETicket();
							resETicket.setEticketNumber(eticketDTO.getEticketNo());
							resETicket.setPnrPaxId(reservationPax.getPnrPaxId());
							resETicket.seteTicketStatus(ReservationPaxETicket.PURCHASED);
							eTickets.put(eticketDTO.getEticketNo(), resETicket);
						}
						resPaxFareSegEticket.setReservationPaxETicket(resETicket);
						resPaxFareSegEticketList.add(resPaxFareSegEticket);
					}
					if (eticketDTO != null
							|| (paxfareSeg.getSegment() != null && paxfareSeg.getSegment().getStatus()
									.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL))) {
						ETicketBO.closePreviousEtickets(paxfareSegmentEtickets, modifiedPaxfareSegmentEtickets,
								paxfareSeg.getPnrPaxFareSegId(), false, exchangePreviousEtickets, true);
					}
				}
			}

		}
		// save new e-tickets
		if (!resPaxFareSegEticketList.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEticketList);
		}
		// update previously created e-tickets
		if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(modifiedPaxfareSegmentEtickets);
		}
		
		// send SSR TKNE message for codeshare segments
		sendSSRTKNEforNewETs(actualPaxEticketNumbers, reservation);

		return modifiedPaxfareSegmentEtickets;
	}

	/**
	 * @param reservation
	 * @param oldPnrSegIds
	 * @param newSegmentSeqNos
	 * @throws ModuleException
	 */
	public static void exchangeETickets(Reservation reservation, Collection<Integer> oldPnrSegIds,
			Collection<Integer> newSegmentSeqNos) throws ModuleException {

		// new e ticket list
		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();

		Iterator<ReservationPax> iteReservationPax = reservation.getPassengers().iterator();

		Map<Integer, Map<Integer, EticketDTO>> paxSegEticketNumbers = new HashMap<Integer, Map<Integer, EticketDTO>>();

		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			ArrayList<Integer> clneOldPnrSegIds = new ArrayList<Integer>(oldPnrSegIds);
			ArrayList<Integer> clneNewSegmentSeqNos = new ArrayList<Integer>(newSegmentSeqNos);

			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());
			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			Map<Integer, EticketDTO> segEticketNumbers = new HashMap<Integer, EticketDTO>();

			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();
				
				List<ReservationPaxFareSegment> paxFareSegList = new ArrayList<ReservationPaxFareSegment>();
				for(ReservationPaxFareSegment paxFareSegments : resPaxFare.getPaxFareSegments()) {
					paxFareSegList.add(paxFareSegments);
				}
				Collections.sort(paxFareSegList);

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = paxFareSegList.iterator();

				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();

					if (clneNewSegmentSeqNos.contains(paxfareSeg.getSegment().getSegmentSeq())) {
						int segIndex = clneNewSegmentSeqNos.indexOf(paxfareSeg.getSegment().getSegmentSeq());
						Integer oldPnrSegId = getOldPnrSegId(reservation.getSegments(), clneOldPnrSegIds, paxfareSeg.getSegment()
								.getFlightSegId());

						boolean removeOldId = false;
						if (oldPnrSegId.intValue() == -1 && !clneOldPnrSegIds.isEmpty()) {
							if (clneOldPnrSegIds.size() > segIndex) {
								oldPnrSegId = clneOldPnrSegIds.get(segIndex);
								removeOldId = true;
							}
						}

						// old segment found
						ReservationPaxFareSegmentETicket oldFareSegmentETicket = ETicketBO.getETicket(
								reservationPax.getPnrPaxFares(), paxfareSegmentEtickets, oldPnrSegId);

						if (oldFareSegmentETicket != null) {
							// old segment has a e ticket
							resPaxFareSegEticketList.addAll(ETicketBO.addEticketToNewSegment(reservationPax,
									oldFareSegmentETicket, clneNewSegmentSeqNos.get(segIndex), EticketStatus.OPEN.code()));

							EticketDTO eticketDTO = new EticketDTO(oldFareSegmentETicket.getReservationPaxETicket()
									.getEticketNumber(), oldFareSegmentETicket.getCouponNo());
							segEticketNumbers.put(clneNewSegmentSeqNos.get(segIndex), eticketDTO);

							if (removeOldId) {
								clneOldPnrSegIds.remove(segIndex);
							} else {
								removeProcessedOldPnr(clneOldPnrSegIds, oldPnrSegId);
							}
						}
						clneNewSegmentSeqNos.remove(segIndex);
					}
				}
			}
			paxSegEticketNumbers.put(reservationPax.getPaxSequence(), segEticketNumbers);
		}
		// save new e-tickets
		if (!resPaxFareSegEticketList.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEticketList);
		}

		// send SSR TKNR message for codeshare segments
		sendSSRTKNR(paxSegEticketNumbers, reservation);

	}
	
	/**
	 * @param reservation
	 * @param oldPnrSegIds
	 * @param newSegmentSeqNos
	 * @throws ModuleException
	 */
	public static void exchangeETicketsForGivenPassengers(Reservation reservation, Collection<Integer> oldPnrSegIds,
			Collection<Integer> newSegmentSeqNos, Collection<ReservationPax> passengers) throws ModuleException {

		// new e ticket list
		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();

		Iterator<ReservationPax> iteReservationPax = passengers.iterator();
		
		Map<Integer, Map<Integer, EticketDTO>> paxSegEticketNumbers = new HashMap<Integer, Map<Integer, EticketDTO>>();

		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			ArrayList<Integer> clneOldPnrSegIds = new ArrayList<Integer>(oldPnrSegIds);
			ArrayList<Integer> clneNewSegmentSeqNos = new ArrayList<Integer>(newSegmentSeqNos);

			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());
			
			List<ReservationPaxFare> paxFareList = new ArrayList<ReservationPaxFare>();
			for(ReservationPaxFare paxFare : reservationPax.getPnrPaxFares()) {
				paxFareList.add(paxFare);
			}
			Collections.sort(paxFareList);			
			Iterator<ReservationPaxFare> itePaxFare = paxFareList.iterator();
			Map<Integer, EticketDTO> segEticketNumbers = new HashMap<Integer, EticketDTO>();
			
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();
				List<ReservationPaxFareSegment> paxFareSegList = new ArrayList<ReservationPaxFareSegment>();
				for(ReservationPaxFareSegment paxFareSegments : resPaxFare.getPaxFareSegments()) {
					paxFareSegList.add(paxFareSegments);
				}
				Collections.sort(paxFareSegList);

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = paxFareSegList.iterator();

				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();

					if (clneNewSegmentSeqNos.contains(paxfareSeg.getSegment().getSegmentSeq())) {
						int segIndex = clneNewSegmentSeqNos.indexOf(paxfareSeg.getSegment().getSegmentSeq());

						Integer oldPnrSegId = getOldPnrSegId(reservation.getSegments(), clneOldPnrSegIds, paxfareSeg.getSegment()
								.getFlightSegId());

						// old segment found
						ReservationPaxFareSegmentETicket oldFareSegmentETicket = ETicketBO.getETicket(
								reservationPax.getPnrPaxFares(), paxfareSegmentEtickets, oldPnrSegId);

						if (oldFareSegmentETicket != null) {
							// old segment has a e ticket
							resPaxFareSegEticketList.addAll(ETicketBO.addEticketToNewSegment(reservationPax,
									oldFareSegmentETicket, clneNewSegmentSeqNos.get(segIndex), EticketStatus.OPEN.code()));
							
							EticketDTO eticketDTO = new EticketDTO(oldFareSegmentETicket.getReservationPaxETicket().getEticketNumber(), oldFareSegmentETicket.getCouponNo());
							segEticketNumbers.put(clneNewSegmentSeqNos.get(segIndex), eticketDTO);

							removeProcessedOldPnr(clneOldPnrSegIds, oldPnrSegId);	
						} 
						clneNewSegmentSeqNos.remove(segIndex);
					}
				}
			}
			paxSegEticketNumbers.put(reservationPax.getPaxSequence(), segEticketNumbers);
		}
		// save new e-tickets
		if (!resPaxFareSegEticketList.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEticketList);
		}

		//send SSR TKNR message for codeshare segments
		sendSSRTKNR(paxSegEticketNumbers, reservation);

	}

	private static void sendSSRTKNR(Map<Integer, Map<Integer, EticketDTO>> eTicketInfo, Reservation reservation)
			throws ModuleException {
		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodesInConfirmedSegments(reservation);
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.seteTicketInfo(eTicketInfo);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.NO_TICKET_CHANGE.getCode());
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}
	}

	/**
	 * @param paxFareSegment
	 * @param paxfareSegmentEtickets
	 * @param pnrSegmentId
	 * @return
	 */
	private static ReservationPaxFareSegmentETicket getETicket(Set<ReservationPaxFare> paxFare,
			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets, Integer pnrSegmentId) {

		Iterator<ReservationPaxFare> itePaxFare = paxFare.iterator();
		while (itePaxFare.hasNext()) {
			ReservationPaxFare resPaxFare = itePaxFare.next();
			Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

			while (itePaxfareSegs.hasNext()) {
				ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();

				if (paxfareSeg.getSegment().getPnrSegId().equals(pnrSegmentId)) {
					return ETicketBO.getPreviousEticket(paxfareSegmentEtickets, paxfareSeg.getPnrPaxFareSegId());
				}
			}
		}
		return null;

	}

	/**
	 * this is used when transfer segment happens
	 * 
	 * @param reservation
	 * @param pnrSegIdAndSequnceMap
	 * @throws ModuleException
	 */
	public static void updateETickets(Reservation reservation, Map<Integer, Integer> pnrSegIdAndSequnceMap)
			throws ModuleException {

		// new e ticket list
		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();

		Iterator<ReservationPax> iteReservationPax = reservation.getPassengers().iterator();

		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());

			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();

					if (pnrSegIdAndSequnceMap.containsKey(paxfareSeg.getPnrSegId())) {
						// old segment found
						ReservationPaxFareSegmentETicket oldFareSegmentETicket = ETicketBO.getPreviousEticket(
								paxfareSegmentEtickets, paxfareSeg.getPnrPaxFareSegId());
						if (oldFareSegmentETicket != null) {
							// old segment has a e ticket
							resPaxFareSegEticketList.addAll(ETicketBO.addEticketToNewSegment(reservationPax,
									oldFareSegmentETicket, pnrSegIdAndSequnceMap.get(paxfareSeg.getPnrSegId()),
									EticketStatus.CLOSED.code()));
						}
					}
				}
			}
		}
		// save new e-tickets
		if (!resPaxFareSegEticketList.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEticketList);
		}
	}

	/**
	 * @param reservationPax
	 * @param oldFareSegmentETicket
	 * @param newSegmentSeqNo
	 * @param resPaxFareSegEticketList
	 */
	private static Collection<ReservationPaxFareSegmentETicket> addEticketToNewSegment(ReservationPax reservationPax,
			ReservationPaxFareSegmentETicket oldFareSegmentETicket, Integer newSegmentSeqNo, String ticketStatus) {
		Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();

		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();

		while (itePaxFare.hasNext()) {
			ReservationPaxFare resPaxFare = itePaxFare.next();
			Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();
			while (itePaxfareSegs.hasNext()) {
				ReservationPaxFareSegment paxfareSegNew = itePaxfareSegs.next();

				if (paxfareSegNew.getSegment().getSegmentSeq().equals(newSegmentSeqNo)) {
					// new segment found
					ReservationPaxFareSegmentETicket newPaxFareSegmentETicket = new ReservationPaxFareSegmentETicket();
					newPaxFareSegmentETicket.setPnrPaxFareSegId(paxfareSegNew.getPnrPaxFareSegId());
					newPaxFareSegmentETicket.setReservationPaxETicket(oldFareSegmentETicket.getReservationPaxETicket());
					// Since we are adding new ET status, no need validation
					newPaxFareSegmentETicket.setStatus(ticketStatus);
					newPaxFareSegmentETicket.setCouponNo(oldFareSegmentETicket.getCouponNo());
					newPaxFareSegmentETicket.setExternalEticketNumber(oldFareSegmentETicket.getExternalEticketNumber());
					newPaxFareSegmentETicket.setExternalCouponNo(oldFareSegmentETicket.getExternalCouponNo());
					if (oldFareSegmentETicket.getExternalCouponStatus() != null
							&& !oldFareSegmentETicket.getExternalCouponStatus().isEmpty()) {
						newPaxFareSegmentETicket.setExternalCouponStatus(ticketStatus);
					}
					newPaxFareSegmentETicket.setExternalCouponControl(oldFareSegmentETicket.getExternalCouponControl());
					resPaxFareSegEticketList.add(newPaxFareSegmentETicket);
				}
			}
		}
		return resPaxFareSegEticketList;

	}

	/**
	 * this is used when cancel segments
	 * 
	 * @param reservation
	 * @param pnrSegIdAndSequnceMap
	 * @throws ModuleException
	 */
	public static void cancelETickets(Reservation reservation, Collection<Integer> userExchangedPnrSegIds) throws ModuleException {
		// updated e ticket list
		Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
		Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = new HashMap<Integer, Map<Integer, EticketDTO>>();
		Iterator<ReservationPax> iteReservationPax = reservation.getPassengers().iterator();

		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();

			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());

			if (paxfareSegmentEtickets.isEmpty()) {
				continue;
			}
			Map<Integer, EticketDTO> eTickets = new HashMap<Integer, EticketDTO>();
			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();
					if (paxfareSeg.getSegment().getStatus().equals(ReservationSegmentStatus.CANCEL)) {
						boolean exchanged = false;
						if (userExchangedPnrSegIds != null && userExchangedPnrSegIds.contains(paxfareSeg.getPnrSegId())) {
							exchanged = true;
						}
						EticketDTO eticketDTO = ETicketBO.closePreviousEtickets(paxfareSegmentEtickets,
								modifiedPaxfareSegmentEtickets, paxfareSeg.getPnrPaxFareSegId(), reservation.isVoidReservation(),
								exchanged, true);
						if (eticketDTO != null) {
							eTickets.put(paxfareSeg.getSegment().getSegmentSeq(), eticketDTO);
						}
					}
				}
			}
			if (!eTickets.isEmpty()) {
				eTicketInfo.put(reservationPax.getPaxSequence(), eTickets);
			}
		}
		// save updated e-tickets
		if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(modifiedPaxfareSegmentEtickets);
		}
	}

	public static void cancelETickets(Reservation reservation) throws ModuleException {
		cancelETickets(reservation, null);
	}

	/**
	 * @throws ModuleException
	 */
	public static void updatePassengerCouponStatus(PassengerCouponUpdateRQ paxCouponUpdateRQ, CredentialsDTO credentialsDTO,
			boolean allowExceptions) throws ModuleException {

		if (paxCouponUpdateRQ.getEticketStatus() == null && paxCouponUpdateRQ.getPaxStatus() == null) {
			throw new ModuleException("airreservations.invalid.args");
		}

		LccClientPassengerEticketInfoTO currentPaxCoupon = ReservationDAOUtils.DAOInstance.ETicketDAO
				.getEticketInfo(paxCouponUpdateRQ.getEticketId());

		if (paxCouponUpdateRQ.getEticketStatus() != null) {
			
			if (validateEtktTransition(currentPaxCoupon.getPaxETicketStatus(), allowExceptions)) {
				ReservationDAOUtils.DAOInstance.ETicketDAO.updatePassengerCouponStatus(paxCouponUpdateRQ.getEticketId(),
						paxCouponUpdateRQ.getEticketStatus());
			} else {
				throw new ModuleException("airreservations.eticket.invalid");
			}

			if (AppSysParamsUtil.isEnableEticketStatusTransitionRecording()) {
				ReservationPaxFareSegEtktTransition resPaxFareSegEtktTransition = TicketingUtils
						.recordEtktCouponStatusTransition(paxCouponUpdateRQ, currentPaxCoupon.getPaxETicketStatus(),
								paxCouponUpdateRQ.getEticketStatus());
				ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEtktTransition);
			}
		} else {
			paxCouponUpdateRQ.setEticketStatus(currentPaxCoupon.getPaxETicketStatus());
		}

		if (paxCouponUpdateRQ.getPaxStatus() != null) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.updatePassengerStatus(paxCouponUpdateRQ.getEticketId(),
					paxCouponUpdateRQ.getPaxStatus());
		} else {
			paxCouponUpdateRQ.setPaxStatus(currentPaxCoupon.getPaxStatus());
		}

		ReservationAudit reservationAudit = TicketingUtils.createPassengerCouponUpdateAudit(currentPaxCoupon,
				paxCouponUpdateRQ.getEticketStatus(), paxCouponUpdateRQ.getPaxStatus(), paxCouponUpdateRQ.getCouponAudit());

		ReservationAudit.createReservationAudit(reservationAudit, credentialsDTO, false);
		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
		// Saves the reservation
		ReservationModuleUtils.getAuditorBD().audit(reservationAudit, reservationAudit.getContentMap());
	}
	
	public static void updateGroupPassengerCouponStatus(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ,
			CredentialsDTO credentialsDTO, boolean allowExceptions) throws ModuleException {

		for (PassengerCouponUpdateRQ paxCouponRQ : groupPaxCouponUpdateRQ) {

			boolean hasEticketUpdated = false;
			boolean hasPFSUpdated = false;

			if (paxCouponRQ.getEticketStatus() == null && paxCouponRQ.getPaxStatus() == null) {
				throw new ModuleException("airreservations.invalid.args");
			}
			LccClientPassengerEticketInfoTO currentPaxCoupon = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getEticketInfo(paxCouponRQ.getEticketId());

			if (paxCouponRQ.getEticketStatus() != null) {

				if (!paxCouponRQ.getEticketStatus().equals(currentPaxCoupon.getPaxETicketStatus())) {
					hasEticketUpdated = true;
					if (validateEtktTransition(currentPaxCoupon.getPaxETicketStatus(), allowExceptions)) {
						ReservationDAOUtils.DAOInstance.ETicketDAO.updatePassengerCouponStatus(paxCouponRQ.getEticketId(),
								paxCouponRQ.getEticketStatus());
					} else {
						throw new ModuleException("airreservations.eticket.invalid");
					}
					if (AppSysParamsUtil.isEnableEticketStatusTransitionRecording()) {
						ReservationPaxFareSegEtktTransition resPaxFareSegEtktTransition = TicketingUtils
								.recordEtktCouponStatusTransition(paxCouponRQ, currentPaxCoupon.getPaxETicketStatus(),
										paxCouponRQ.getEticketStatus());
						ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEtktTransition);
					}
				}
			} else {
				paxCouponRQ.setEticketStatus(currentPaxCoupon.getPaxETicketStatus());
			}

			if (paxCouponRQ.getPaxStatus() != null) {
				if (!paxCouponRQ.getPaxStatus().equals(currentPaxCoupon.getPaxStatus())) {
					hasPFSUpdated = true;
					ReservationDAOUtils.DAOInstance.ETicketDAO.updatePassengerStatus(paxCouponRQ.getEticketId(),
							paxCouponRQ.getPaxStatus());
				}

			} else {
				paxCouponRQ.setPaxStatus(currentPaxCoupon.getPaxStatus());
			}
			if (hasPFSUpdated || hasEticketUpdated) {

				ReservationAudit reservationAudit = TicketingUtils.createPassengerCouponUpdateAudit(currentPaxCoupon,
						paxCouponRQ.getEticketStatus(), paxCouponRQ.getPaxStatus(), paxCouponRQ.getCouponAudit());
				ReservationAudit.createReservationAudit(reservationAudit, credentialsDTO, false);
				reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
				// Saves the reservation
				ReservationModuleUtils.getAuditorBD().audit(reservationAudit, reservationAudit.getContentMap());
			} else {
				throw new ModuleException("airreservations.eticket.invalid");
			}

		}
	}

	/**
	 * get last created e ticket for selected segment
	 * 
	 * @param paxfareSegmentEtickets
	 * @param pnrFareSegmentId
	 * @return
	 */
	private static ReservationPaxFareSegmentETicket getPreviousEticket(
			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets, Integer pnrFareSegmentId) {
		Iterator<ReservationPaxFareSegmentETicket> iteEticket = paxfareSegmentEtickets.iterator();
		ReservationPaxFareSegmentETicket lastCreatedFareSegmentETicket = null;
		while (iteEticket.hasNext()) {
			ReservationPaxFareSegmentETicket fareSegmentETicket = iteEticket.next();
			if (fareSegmentETicket.getPnrPaxFareSegId().equals(pnrFareSegmentId)) {
				if (lastCreatedFareSegmentETicket == null) {
					lastCreatedFareSegmentETicket = fareSegmentETicket;
				} else if (fareSegmentETicket.getPaxFareSegemntEticketId() > lastCreatedFareSegmentETicket
						.getPaxFareSegemntEticketId()) {
					lastCreatedFareSegmentETicket = fareSegmentETicket;
				}
			}
		}
		return lastCreatedFareSegmentETicket;

	}

	/**
	 * update status to inactive for previously created tickets
	 * 
	 * @param paxfareSegmentEtickets
	 * @param pnrpaxfareSegId
	 * @param exchanged
	 */
	private static EticketDTO closePreviousEtickets(Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets,
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets, Integer pnrpaxfareSegId,
			boolean isVoidReservationSegment, boolean exchanged, boolean allowExceptions) {

		EticketDTO eticketDTO = null;
		Iterator<ReservationPaxFareSegmentETicket> itePaxfareSegmentEticket = paxfareSegmentEtickets.iterator();

		while (itePaxfareSegmentEticket.hasNext()) {
			ReservationPaxFareSegmentETicket fareEticket = itePaxfareSegmentEticket.next();

			if (fareEticket.getPnrPaxFareSegId().intValue() == pnrpaxfareSegId
					&& EticketStatus.OPEN.code().equals(fareEticket.getStatus())) {
				if (validateEtktTransition(fareEticket.getStatus(), allowExceptions)) {
					if (isVoidReservationSegment) {
						fareEticket.setStatus(EticketStatus.VOID.code()); // mark as
																			// void
					} else if (exchanged) {
						fareEticket.setStatus(EticketStatus.EXCHANGED.code()); // mark as exchanged
						eticketDTO = new EticketDTO(fareEticket.getReservationPaxETicket().getEticketNumber(),
								fareEticket.getCouponNo());
						suspendExternalCoupon(fareEticket);
					} else {
						fareEticket.setStatus(EticketStatus.CLOSED.code());
						eticketDTO = new EticketDTO(fareEticket.getReservationPaxETicket().getEticketNumber(),
								fareEticket.getCouponNo());
						suspendExternalCoupon(fareEticket);
					}

					modifiedPaxfareSegmentEtickets.add(fareEticket);
				}
			}
		}
		return eticketDTO;
	}

	/**
	 * In any Etkt status transition, we need to validate it
	 * 
	 * @param oldEticketStatus
	 * @return
	 */
	private static boolean validateEtktTransition(String oldEticketStatus, boolean allowExceptions) {
		boolean isValid = true;

		// final status validation
		if (!allowExceptions) {
			for (FinalEticketStatus et : FinalEticketStatus.values()) {
				if (et.code().equals(oldEticketStatus)) {
					return false;
				}
			}
		}
		return isValid;
	}

	/**
	 * @param ticketingInfoDto
	 * @return
	 * @throws ModuleException
	 */
	private static TicketGeneratorCriteria createTicketGenerateCriteria(CreateTicketInfoDTO ticketingInfoDto)
			throws ModuleException {

		TicketGeneratorCriteria tktGenCriteria = new TicketGeneratorCriteria();

		if (ticketingInfoDto != null && ticketingInfoDto.getAppIndicator() != null) {
			tktGenCriteria.setFormCode(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.E_TICKET_FORM_CODE));
			if (SalesChannelsUtil.isAppIndicatorWebOrMobile(ticketingInfoDto.getAppIndicator())) {// IBE reservations
				tktGenCriteria.setTktGenarateMethod(TicketGeneratorCriteriaType.DEF_TKT);
			} else if (ticketingInfoDto.getPaymentAgent() != null) {// XBE and
																	// webservice
																	// reservations
				Agent agent = ReservationModuleUtils.getTravelAgentBD().getAgent(ticketingInfoDto.getPaymentAgent());

				if (ticketingInfoDto.isBSPPayment()) {
					// bsp e ticket
					tktGenCriteria.setTktGenarateMethod(TicketGeneratorCriteriaType.BSP_TKT);
					tktGenCriteria.setSequnceName(ReservationDAOUtils.DAOInstance.ETicketDAO.getAgentEticketSequnceName(
							ticketingInfoDto.getPaymentAgent(), TicketGeneratorCriteriaType.BSP_TKT));

				} else if (AppSysParamsUtil.isAgentWiseTicketEnable() && agent.getAgentWiseTicketEnable().equalsIgnoreCase("Y")) {
					// agent e ticket
					tktGenCriteria.setTktGenarateMethod(TicketGeneratorCriteriaType.AGNET_WISE_PAPER_TKT);
					tktGenCriteria.setSequnceName(ETicketBO.getValidAgentTicketSequnce(ReservationModuleUtils.getTravelAgentBD()
							.getAgentTicketStocks(ticketingInfoDto.getPaymentAgent())));
					tktGenCriteria.setFormCode(CommonsServices.getGlobalConfig().getBizParam(
							SystemParamKeys.PAPER_TICKET_FORM_CODE));
				} else {
					tktGenCriteria.setTktGenarateMethod(TicketGeneratorCriteriaType.DEF_TKT);
				}
			} else {
				tktGenCriteria.setTktGenarateMethod(TicketGeneratorCriteriaType.DEF_TKT);
			}
		} else {
			tktGenCriteria.setFormCode(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.E_TICKET_FORM_CODE));
			tktGenCriteria.setTktGenarateMethod(TicketGeneratorCriteriaType.DEF_TKT);
		}
		if (log.isDebugEnabled()) {
			log.debug(ticketingInfoDto.toString());
			log.debug(tktGenCriteria.toString());
		}
		return tktGenCriteria;
	}

	/**
	 * @param ticketGeneratorCriteria
	 * @return
	 * @throws ModuleException
	 */
	private static String getNextTicketSequenceNo(TicketGeneratorCriteria ticketGeneratorCriteria) throws ModuleException {
		ETicketDAO eTicketDao = ReservationDAOUtils.DAOInstance.ETicketDAO;
		String nextSequence = null;

		if (ticketGeneratorCriteria.getTktGenarateMethod().equals(TicketGeneratorCriteriaType.AGNET_WISE_PAPER_TKT)
				|| ticketGeneratorCriteria.getTktGenarateMethod().equals(TicketGeneratorCriteriaType.BSP_TKT)) {
			nextSequence = eTicketDao.getNextIATAETicketSQID(ticketGeneratorCriteria.getSequnceName());

		} else if (ticketGeneratorCriteria.getTktGenarateMethod().equals(TicketGeneratorCriteriaType.DEF_TKT)) {
			nextSequence = eTicketDao.getNextIATAETicketSQID();
		}
		return nextSequence;

	}

	/**
	 * @param agentTktSequenses
	 * @return
	 * @throws ModuleException
	 */
	private static String getValidAgentTicketSequnce(Collection<AgentTicketSequnce> agentTktSequenses) throws ModuleException {

		Iterator<AgentTicketSequnce> tktSeqIterator = agentTktSequenses.iterator();

		while (tktSeqIterator.hasNext()) {
			AgentTicketSequnce agentTicketSequnce = tktSeqIterator.next();
			if (agentTicketSequnce.getStatus().equals(AgentTicketSequnce.STATUS_ACTIVE)) {
				return agentTicketSequnce.getSequnceName();
			}
		}
		throw new ModuleException("airreservation.valid.agent.ticket.sequnce.notfound", AirreservationConstants.MODULE_NAME);
	}

	/**
	 * @param pnr
	 * @return
	 */
	private static Collection<Integer> getUnflownReservationSegmentIds(String pnr) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		return reservationDAO.getUnflownReservationSegmentIds(pnr, CalendarUtil.getCurrentSystemTimeInZulu());
	}

	/**
	 * this is used to update the e ticket status
	 * 
	 * @param reservationPzx
	 * @param flightSegId
	 * @param status
	 * @throws ModuleException
	 */
	public static String updateETicketStatus(ReservationPax reservationPax, Collection<Integer> flightSegIds, String status,
			boolean isThrowException, boolean allowExceptions) throws ModuleException {

		// updated e ticket list
		Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();

		Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
				.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());
		String usabilityStatus = "";

		if (!paxfareSegmentEtickets.isEmpty()) {

			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();
					if (flightSegIds.contains(paxfareSeg.getSegment().getFlightSegId())) {
						Iterator<ReservationPaxFareSegmentETicket> itePaxfareSegmentEticket = paxfareSegmentEtickets.iterator();
						while (itePaxfareSegmentEticket.hasNext()) {
							ReservationPaxFareSegmentETicket fareEticket = itePaxfareSegmentEticket.next();

							if (fareEticket.getPnrPaxFareSegId().intValue() == paxfareSeg.getPnrPaxFareSegId()) {
								if (EticketStatus.CLOSED.code().equals(fareEticket.getStatus())) {
									usabilityStatus = "NO_CIN"; // not_allowed_to_checkin
								} else if (EticketStatus.CHECKEDIN.code().equals(fareEticket.getStatus())) {
									if (EticketStatus.BOARDED.code().equals(status)) {
										usabilityStatus = "OK_BDD"; // allowed_to_board
									} else if (EticketStatus.CHECKEDIN.code().equals(status)) {
										usabilityStatus = "NO_CIN"; // not_allowed_to_checkin
									} else if (EticketStatus.OPEN.code().equals(status)) {
										usabilityStatus = "OK_OPN"; // allowed_to_open
									}
								} else if (EticketStatus.BOARDED.code().equals(fareEticket.getStatus())) {
									if (EticketStatus.BOARDED.code().equals(status)) {
										usabilityStatus = "NO_BDD"; // not_allowed_to_board
									} else if (EticketStatus.CHECKEDIN.code().equals(status)) {
										usabilityStatus = "NO_CIN"; // not_allowed_to_checkin
									}
								} else if (EticketStatus.OPEN.code().equals(fareEticket.getStatus())) {
									if (EticketStatus.CHECKEDIN.code().equals(status)) {
										usabilityStatus = "OK_CIN"; // allowed_to_checkin
									} else if (EticketStatus.BOARDED.code().equals(status)) {
										usabilityStatus = "OK_BDD"; // allowed_to_board
									}
								}
							}

							if (fareEticket.getPnrPaxFareSegId().intValue() == paxfareSeg.getPnrPaxFareSegId()
									&& !EticketStatus.CLOSED.code().equals(fareEticket.getStatus())
									&& validateEtktTransition(fareEticket.getStatus(), allowExceptions)) {
								fareEticket.setStatus(status);
								modifiedPaxfareSegmentEtickets.add(fareEticket);
							}
						}
					}
				}
			}
		}
		// save updated e-tickets
		if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(modifiedPaxfareSegmentEtickets);
		} else if (isThrowException) {
			throw new ModuleException("airreservation.eticket.update.invalid.data", "airreservation.desc");
		}

		return usabilityStatus;
	}

	public static ETicketUpdateResponseDTO updateETicketStatus(ETicketUpdateRequestDTO eTicketUpdateRequestDTO,
			CredentialsDTO credentialsDTO) throws ModuleException {

		String flightNo = eTicketUpdateRequestDTO.getFlightNo();
		Date flightDepartureDate = eTicketUpdateRequestDTO.getDepartureDate();
		String segmentCode = eTicketUpdateRequestDTO.getSegmentCode();
		String eTicketNo = eTicketUpdateRequestDTO.geteTicketNo();
		String status = eTicketUpdateRequestDTO.getStatus();
		String unaccompaniedETicketNo = eTicketUpdateRequestDTO.getUnaccompaniedETicketNo();
		String unaccompaniedStatus = eTicketUpdateRequestDTO.getUnaccompaniedStatus();
		String firstName = eTicketUpdateRequestDTO.getFirstName();
		String lastName = eTicketUpdateRequestDTO.getLastName();
		String infFirstName = eTicketUpdateRequestDTO.getInfantFirstName();
		String infLastName = eTicketUpdateRequestDTO.getInfantLsatName();
		String couponNumber = eTicketUpdateRequestDTO.getCouponNumber();
		String unaccompaniedCouponNo = eTicketUpdateRequestDTO.getUnaccompaniedCouponNo();
		String title = eTicketUpdateRequestDTO.getTitle();
		String cabinClassCode = eTicketUpdateRequestDTO.getCabinClassCode();

		String usabilityStatus = null;

		boolean isInfantUpdate = validateDataAndIsInfantUpdate(eTicketNo, flightNo, flightDepartureDate, segmentCode, status,
				unaccompaniedETicketNo, unaccompaniedStatus, infFirstName, infLastName, firstName, lastName, couponNumber,
				unaccompaniedCouponNo);
		ETicketDAO eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		String pnr = eTicketDAO.getPNRByEticketNo(eTicketNo);
		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("airreservation.eticket.update.reservation.not.found", "airreservation.desc");
		}
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadEtickets(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		ETicketUpdateResponseDTO searchResult = searchForSegment(reservation, segmentCode, flightDepartureDate, flightNo,
				cabinClassCode);

		ReservationPax passenger = getReservationPaxFromReseravationUsingETicketNumber(eTicketNo, reservation, firstName,
				lastName, searchResult.getSegmentId(), couponNumber, title);
		String currentStatus = getCurrentETicketStatus(passenger, searchResult.getSegmentId());
		usabilityStatus = updateETicketStatus(passenger, Collections.singleton(searchResult.getSegmentId()), status, true, true);
		doETicketUpdateAudit(
				pnr,
				eTicketNo,
				flightNo,
				flightDepartureDate,
				segmentCode,
				status,
				credentialsDTO,
				currentStatus,
				ReservationApiUtils.getPassengerName(null, passenger.getTitle(), passenger.getFirstName(),
						passenger.getLastName(), false), searchResult.isNOREC());
		if (isInfantUpdate) {
			ReservationPax infant = getReservationPaxFromReseravationUsingETicketNumber(unaccompaniedETicketNo, reservation,
					infFirstName, infLastName, searchResult.getSegmentId(), unaccompaniedCouponNo, null);
			if (!passenger.getPnrPaxId().equals(infant.getAccompaniedPaxId())) {
				throw new ModuleException("airreservation.eticket.update.wrong.infant.adult");
			} else {
				String currentInfantStatus = getCurrentETicketStatus(infant, searchResult.getSegmentId());
				usabilityStatus = updateETicketStatus(infant, Collections.singleton(searchResult.getSegmentId()),
						unaccompaniedStatus, true, true);
				doETicketUpdateAudit(pnr, unaccompaniedETicketNo, flightNo, flightDepartureDate, segmentCode,
						unaccompaniedStatus, credentialsDTO, currentInfantStatus, ReservationApiUtils.getPassengerName(null,
								infant.getTitle(), infant.getFirstName(), infant.getLastName(), false), searchResult.isNOREC());
			}
		}
		searchResult.setUsabilityStatus(usabilityStatus);

		return searchResult;

	}

	private static String getCurrentETicketStatus(ReservationPax passenger, Integer flightSegmentId) throws ModuleException {
		for (Iterator<ReservationPaxFare> paxFareIter = passenger.getPnrPaxFares().iterator(); paxFareIter.hasNext();) {
			for (Iterator<ReservationPaxFareSegment> paxFareSegIter = paxFareIter.next().getPaxFareSegments().iterator(); paxFareSegIter
					.hasNext();) {
				ReservationPaxFareSegment nextPaxFareSeg = paxFareSegIter.next();
				if (nextPaxFareSeg.getSegment().getFlightSegId().equals(flightSegmentId)) {
					Collection<ReservationPaxFareSegmentETicket> paxFareSegmentETickets = ReservationDAOUtils.DAOInstance.ETicketDAO
							.getReservationPaxFareSegmentETickets(Collections.singleton(nextPaxFareSeg.getPnrPaxFareSegId()));
					return paxFareSegmentETickets.iterator().next().getStatus();
				}
			}
		}
		return null;
	}

	public static void updateExternalEticketInfo(List<EticketTO> eticketTOs) throws ModuleException {
		ArrayList<Integer> pnrPaxFareSegmentIds = new ArrayList<Integer>();
		for (EticketTO eticketTO : eticketTOs) {
			pnrPaxFareSegmentIds.add(eticketTO.getPnrPaxFareSegId());
		}
		Collection<ReservationPaxFareSegmentETicket> paxFareSegmentETickets = ReservationDAOUtils.DAOInstance.ETicketDAO
				.getReservationPaxFareSegmentETickets(pnrPaxFareSegmentIds);
		for (EticketTO eticketTO : eticketTOs) {
			for (ReservationPaxFareSegmentETicket paxFareSegmentETicket : paxFareSegmentETickets) {
				if (paxFareSegmentETicket.getPaxFareSegemntEticketId().equals(eticketTO.getEticketId())) {
					if (eticketTO.getExternalCouponNo() != null) {
						paxFareSegmentETicket.setExternalCouponNo(eticketTO.getExternalCouponNo());
					}

					if (eticketTO.getExternalEticketNumber() != null) {
						paxFareSegmentETicket.setExternalEticketNumber(eticketTO.getExternalEticketNumber());
					}

					if (eticketTO.getExternalCouponStatus() != null) {
						paxFareSegmentETicket.setExternalCouponStatus(eticketTO.getExternalCouponStatus());
					}

					if (eticketTO.getExternalCouponControl() != null) {
						paxFareSegmentETicket.setExternalCouponControl(eticketTO.getExternalCouponControl());
					}
				}
			}
		}
		ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(paxFareSegmentETickets);
	}

	private static ETicketUpdateResponseDTO searchForSegment(Reservation reservation, String segmentCode, Date departureDate,
			String flightNumber, String cabinClassCode) throws ModuleException {
		ETicketUpdateResponseDTO segIdSearchResult = new ETicketUpdateResponseDTO();
		Map<Integer, ReservationSegment> segMap = new HashMap<Integer, ReservationSegment>();
		for (Iterator<ReservationSegment> resSegIter = reservation.getSegments().iterator(); resSegIter.hasNext();) {
			ReservationSegment resSeg = resSegIter.next();
			segMap.put(resSeg.getPnrSegId(), resSeg);
		}
		boolean segmentCodeFound = false;
		boolean cabinClassFound = false;
		for (Iterator<ReservationSegmentDTO> resSegDTOIter = reservation.getSegmentsView().iterator(); resSegDTOIter.hasNext();) {
			ReservationSegmentDTO resSegDTO = resSegDTOIter.next();
			String thisSegmentCode = resSegDTO.getSegmentCode();
			Date thisDepartureDate = resSegDTO.getDepartureDate();
			String thisFlightNumber = resSegDTO.getFlightNo();
			
			if (segmentCode.equals(thisSegmentCode)) {
				segmentCodeFound = true;
				if (cabinClassCode != null && !cabinClassCode.equals(segMap.get(resSegDTO.getPnrSegId()).getCabinClassCode())) {
					break;
				}
				cabinClassFound = true;
				if (CalendarUtil.isDateTimeNumericallyEqual(thisDepartureDate, departureDate)
						&& flightNumber.equals(thisFlightNumber)) {
					segIdSearchResult.setNOREC(false);
					segIdSearchResult.setSegmentId(resSegDTO.getFlightSegId());
					return segIdSearchResult;
				} else if (thisDepartureDate.compareTo(departureDate) > 0
						&& ((segIdSearchResult.getDepartureDate() == null) || (thisDepartureDate.compareTo(segIdSearchResult
								.getDepartureDate()) < 0))) {
					segIdSearchResult.setNOREC(true);
					segIdSearchResult.setSegmentId(resSegDTO.getFlightSegId());
					segIdSearchResult.setDepartureDate(thisDepartureDate);
					segIdSearchResult.setFlightNo(thisFlightNumber);
				} 
			}
		}
		if (!segmentCodeFound) {
			throw new ModuleException("airreservation.eticket.update.invalid.segment.code");
		} else if (!cabinClassFound) {
			throw new ModuleException("airreservation.eticket.update.invalid.cabinClassCode");
		} else if (!segIdSearchResult.isNOREC()) {
			throw new ModuleException("airreservation.eticket.update.departure.date.not.found");
		}
		return segIdSearchResult;
	}

	private static ReservationPax getReservationPaxFromReseravationUsingETicketNumber(String eTicketNumber,
			Reservation reservation, String firstName, String lastName, int fltSegId, String couponNumber, String title)
			throws ModuleException {
		ETicketDAO eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		EticketTO eTicket = eTicketDAO.getEticketInfo(eTicketNumber, new Integer(couponNumber));

		if (eTicket != null) {
			Integer passengerId = eTicket.getPnrPaxId();
			if (passengerId == null) {
				throw new ModuleException("airreservation.eticket.update.invalid.eticket.details");
			}
			if (fltSegId != eTicket.getFlightSegId()) {
				throw new ModuleException("airreservation.eticket.update.segmentcode.invalid");
			}
			for (Iterator<ReservationPax> passengerIterator = reservation.getPassengers().iterator(); passengerIterator.hasNext();) {
				ReservationPax nextPassenger = passengerIterator.next();
				if (nextPassenger.getPnrPaxId().equals(passengerId)) {
					if (!firstName.equalsIgnoreCase(nextPassenger.getFirstName())) {
						throw new ModuleException("airreservation.eticket.update.invalid.first.name");
					} else if (!lastName.equalsIgnoreCase(nextPassenger.getLastName())) {
						throw new ModuleException("airreservation.eticket.update.invalid.last.name");
					}
					if (nextPassenger.getTitle() != null && !title.equalsIgnoreCase(nextPassenger.getTitle())) {
						throw new ModuleException("airreservation.eticket.update.invalid.title");
					}
					return nextPassenger;
				}
			}
		} else {
			throw new ModuleException("airreservation.eticket.update.invalid.eticket.details");
		}
		return null;
	}

	private static void doETicketUpdateAudit(String pnr, String eTicketNo, String flightNo, Date flightDepartureDate,
			String segmentCode, String status, CredentialsDTO credentialsDTO, String previousStatus, String passngerName,
			boolean isNoRec) throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.UPDATED_ETICKET.getCode());
		reservationAudit.setUserNote(null);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.ETICKET_NUMBER, eTicketNo);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.FLIGHT_NUMBER, flightNo);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.SEGMENT_CODE, segmentCode);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.FLIGHT_DEPARTURE_DATE,
				BeanUtils.parseDateFormat(flightDepartureDate, "E, dd MMM yyyy HH:mm:ss") + (isNoRec ? " [NOREC]" : ""));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.STATUS, status);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PREVIOUS_STATUS, previousStatus);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PASSENGER_NAME, passngerName);
		ReservationBO.recordModification(pnr, reservationAudit, "", credentialsDTO);
	}

	private static boolean validateDataAndIsInfantUpdate(String eTicketNo, String flightNo, Date flightDepartureDate,
			String segmentCode, String status, String unaccompaniedETicketNo, String unaccompaniedStatus, String infFirstName,
			String infLastName, String firstName, String lastName, String couponNumber, String unaccompaniedCouponNumber)
			throws ModuleException {
		if (StringUtil.isNullOrEmpty(eTicketNo)) {
			throw new ModuleException("airreservation.eticket.update.eticketno.empty");
		}
		if (StringUtil.isNullOrEmpty(flightNo)) {
			throw new ModuleException("airreservation.eticket.update.flightnumber.empty");
		}
		if (flightDepartureDate == null) {
			throw new ModuleException("airreservation.eticket.update.departuredate.empty");
		}
		if (StringUtil.isNullOrEmpty(segmentCode)) {
			throw new ModuleException("airreservation.eticket.update.segmentcode.empty");
		} else if (segmentCode.split("/").length < 2) {
			throw new ModuleException("airreservation.eticket.update.segmentcode.invalid");
		}
		if (status == null) {
			throw new ModuleException("airreservation.eticket.update.status.empty");
		}

		if (StringUtil.isNullOrEmpty(firstName)) {
			throw new ModuleException("airreservation.eticket.update.firstName.empty");
		}
		if (StringUtil.isNullOrEmpty(lastName)) {
			throw new ModuleException("airreservation.eticket.update.lastName.empty");
		}
		if (StringUtil.isNullOrEmpty(couponNumber)) {
			throw new ModuleException("airreservation.eticket.update.couponno.empty");
		} else {
			try {
				Integer.parseInt(couponNumber);
			} catch (NumberFormatException ex) {
				throw new ModuleException("airreservation.eticket.update.couponno.invalid");
			}
		}

		if (StringUtil.isNullOrEmpty(unaccompaniedETicketNo) && unaccompaniedStatus == null) {
			return false;
		} else if (!StringUtil.isNullOrEmpty(unaccompaniedETicketNo) && !StringUtil.isNullOrEmpty(unaccompaniedCouponNumber)
				&& unaccompaniedStatus != null) {
			if (StringUtil.isNullOrEmpty(infFirstName)) {
				throw new ModuleException("airreservation.eticket.update.infant.firstName.empty");
			} else if (StringUtil.isNullOrEmpty(infLastName)) {
				throw new ModuleException("airreservation.eticket.update.infant.lastName.empty");
			}
			try {
				Integer.parseInt(unaccompaniedCouponNumber);
				return true;
			} catch (NumberFormatException ex) {
				throw new ModuleException("airreservation.eticket.update.infant.couponno.invalid");
			}
		} else if (StringUtil.isNullOrEmpty(unaccompaniedETicketNo)) {
			throw new ModuleException("airreservation.eticket.update.infant.eticketno.empty");
		} else if (StringUtil.isNullOrEmpty(unaccompaniedCouponNumber)) {
			throw new ModuleException("airreservation.eticket.update.infant.couponno.empty");
		} else {
			throw new ModuleException("airreservation.eticket.update.infant.status.empty");
		}
	}

	/**
	 * returns the matching old segment id,this code used as temporary fix to over come some coupon no related issues.
	 * 
	 * TODO:need to maintain a Map which old segment is replaced with which new segment. once a map is introduced to
	 * identify old segment id vs new segment id we can get rid of following code.
	 * 
	 * */
	private static Integer getOldPnrSegId(Set<ReservationSegment> pnrSegments, ArrayList<Integer> clneOldPnrSegIds,
			Integer newSegFltId) {
		Integer pnrSegId = -1;
		Integer modOldPnrSegId = -1;
		if (pnrSegments != null && pnrSegments.size() > 0 && newSegFltId != null && clneOldPnrSegIds != null
				&& clneOldPnrSegIds.size() > 0) {
			Iterator<ReservationSegment> pnrSegmentsItr = pnrSegments.iterator();

			while (pnrSegmentsItr.hasNext()) {
				ReservationSegment pnrSegment = pnrSegmentsItr.next();
				if (pnrSegment.getFlightSegId().intValue() == newSegFltId.intValue()
						&& clneOldPnrSegIds.contains(pnrSegment.getPnrSegId())
						&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(pnrSegment.getStatus())
						&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(pnrSegment.getSubStatus())) {
					// returns exchange segment old segment
					pnrSegId = pnrSegment.getPnrSegId();
					break;
				} else if (clneOldPnrSegIds.contains(pnrSegment.getPnrSegId())
						&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(pnrSegment.getStatus())
						&& !ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(pnrSegment.getSubStatus())) {
					Iterator<Integer> clneOldPnrSegIdsItr = clneOldPnrSegIds.iterator();
					while (clneOldPnrSegIdsItr.hasNext()) {
						Integer oldPnrSegId = clneOldPnrSegIdsItr.next();
						if (oldPnrSegId.intValue() == pnrSegment.getPnrSegId()) {
							// hold matching segment old segment
							if(modOldPnrSegId.intValue() == -1) {
								modOldPnrSegId = pnrSegment.getPnrSegId();
							}
						}
					}

				}

			}
			if (pnrSegId.intValue() == -1 && modOldPnrSegId.intValue() != -1) {
				pnrSegId = modOldPnrSegId;
			}

		}

		return pnrSegId;
	}

	private static void removeProcessedOldPnr(ArrayList<Integer> clneOldPnrSegIds, Integer pnrSegId) {
		Iterator<Integer> clneOldPnrSegIdsItr = clneOldPnrSegIds.iterator();
		while (clneOldPnrSegIdsItr.hasNext()) {
			Integer oldPnrSegId = clneOldPnrSegIdsItr.next();
			if (oldPnrSegId.intValue() == pnrSegId) {
				// hold matching segment old segment
				clneOldPnrSegIdsItr.remove();
			}
		}
	}

	private static void suspendExternalCoupon(ReservationPaxFareSegmentETicket fareEticket) {
		if (!StringUtil.isNullOrEmpty(fareEticket.getExternalCouponStatus())
				&& !StringUtil.isNullOrEmpty(fareEticket.getExternalCouponControl())) {
			fareEticket.setExternalCouponStatus(EticketStatus.SUSPENDED.code());
		}
	}

	/**
	 * update ticket, external ticket information for external ticket exchange operation
	 * 
	 * @param transitions
	 * @param cnxPnrSegIds
	 * 
	 * @throws ModuleException
	 */
	public static void exchangeETickets(Collection<ReservationPax> passengers,
			Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions, Collection<Integer> cnxPnrSegIds)
			throws ModuleException {

		Iterator<ReservationPax> iteReservationPax = passengers.iterator();

		// new e ticket list
		Collection<ReservationPaxFareSegmentETicket> resPaxFareSegEticketList = new ArrayList<ReservationPaxFareSegmentETicket>();
		// modified e ticket list
		Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
		ReservationPax reservationPax;

		while (iteReservationPax.hasNext()) {
			reservationPax = iteReservationPax.next();
			Map<Integer, TransitionTo<Coupon>> paxEtickets = transitions.get(reservationPax.getPaxSequence());

			if (paxEtickets == null || paxEtickets.isEmpty()) {
				continue;
			}

			Map<String, ReservationPaxETicket> eTickets = new HashMap<String, ReservationPaxETicket>();

			Collection<ReservationPaxFareSegmentETicket> paxfareSegmentEtickets = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getReservationPaxFareSegmentETickets(reservationPax.getPnrPaxId());

			Iterator<ReservationPaxFare> itePaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itePaxFare.hasNext()) {
				ReservationPaxFare resPaxFare = itePaxFare.next();

				Iterator<ReservationPaxFareSegment> itePaxfareSegs = resPaxFare.getPaxFareSegments().iterator();

				ReservationPaxETicket resETicket = null;
				while (itePaxfareSegs.hasNext()) {
					ReservationPaxFareSegment paxfareSeg = itePaxfareSegs.next();
					ReservationSegment segment = paxfareSeg.getSegment();
					if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())) {

						TransitionTo<Coupon> transitionTo = paxEtickets.get(paxfareSeg.getSegment().getSegmentSeq());
						Coupon newCoupon = transitionTo.getNewVal();

						if (newCoupon != null) {
							ReservationPaxFareSegmentETicket resPaxFareSegEticket = new ReservationPaxFareSegmentETicket();
							resPaxFareSegEticket.setPnrPaxFareSegId(paxfareSeg.getPnrPaxFareSegId());
							// since new eticket issue, no need validation
							resPaxFareSegEticket.setStatus(EticketStatus.OPEN.code());
							resPaxFareSegEticket.setCouponNo(newCoupon.getCouponNumber());
							resPaxFareSegEticket.setExternalEticketNumber(newCoupon.getExternalTicketNumber());
							resPaxFareSegEticket.setExternalCouponNo(newCoupon.getExternalCouponNumber());

							resETicket = eTickets.get(newCoupon.getTicketNumber());
							if (resETicket == null) {
								resETicket = new ReservationPaxETicket();
								resETicket.setEticketNumber(newCoupon.getTicketNumber());
								resETicket.setPnrPaxId(reservationPax.getPnrPaxId());
								resETicket.seteTicketStatus(ReservationPaxETicket.EXCHANGED);
								eTickets.put(newCoupon.getTicketNumber(), resETicket);
							}
							resPaxFareSegEticket.setReservationPaxETicket(resETicket);
							resPaxFareSegEticketList.add(resPaxFareSegEticket);
						}

					} else if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
							&& ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus())
							&& cnxPnrSegIds.contains(segment.getPnrSegId())) {
						ETicketBO.closePreviousEtickets(paxfareSegmentEtickets, modifiedPaxfareSegmentEtickets,
								paxfareSeg.getPnrPaxFareSegId(), false, true, true);
					}
				}
			}

		}
		// save new e-tickets
		if (!resPaxFareSegEticketList.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(resPaxFareSegEticketList);
		}
		// update exchanged etickets
		if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
			ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(modifiedPaxfareSegmentEtickets);
		}

	}
}
