/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditUsageDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * Implementation of the ReservationCreditDAO
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="ReservationCreditDAO"
 */
public class ReservationCreditDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationCreditDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationCreditDAOImpl.class);

	/**
	 * save reservation credit
	 */
	public void saveReservationCredit(ReservationCredit reservationCredit) {
		hibernateSaveOrUpdate(reservationCredit);
	}

	/**
	 * save all reservation credit
	 */
	public void saveReservationCredit(Collection<ReservationCredit> colReservationCredit) {
		hibernateSaveOrUpdateAll(colReservationCredit);
	}

	/**
	 * get the reservation credit by id
	 */
	public ReservationCredit getReservationCredit(long creditId) {
		return (ReservationCredit) get(ReservationCredit.class, new Integer((int) creditId));
	}

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<PaxCreditDTO> getReservationCredits(int customerId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sqlBuilder = new StringBuilder();

		sqlBuilder
				.append("SELECT RES.PNR PNR_NUMBER,RES.ORIGINATOR_PNR ORIGINATOR_PNR_NUMBER,P.PNR_PAX_ID PAX_ID,P.PAX_TYPE_CODE,P.TITLE TITLE,");
		sqlBuilder
				.append("P.FIRST_NAME F_NAME,P.LAST_NAME L_NAME,CREDITS.PAX_ID,CREDITS.DATE_EXP DATE_EXP,CREDITS.BALANCE BALANCE ");
		sqlBuilder.append("FROM  T_PNR_PASSENGER P, ");
		sqlBuilder.append("(SELECT PC.PNR_PAX_ID PAX_ID, MIN(PC.DATE_EXP) DATE_EXP ,SUM(PC.BALANCE) BALANCE ");
		sqlBuilder.append(" FROM T_PAX_CREDIT PC,T_PNR_PASSENGER PP,T_RESERVATION_CONTACT RC ");
		sqlBuilder
				.append(" WHERE PC.DATE_EXP > SYSDATE AND  PC.PNR_PAX_ID = PP.PNR_PAX_ID AND PP.PNR = RC.PNR AND RC.CUSTOMER_ID = ? ");
		sqlBuilder.append(" GROUP BY PC.PNR_PAX_ID) CREDITS, ");
		sqlBuilder.append("(SELECT R.PNR, R.ORIGINATOR_PNR ");
		sqlBuilder.append(" FROM T_RESERVATION R,T_RESERVATION_CONTACT RC ");
		sqlBuilder.append(" WHERE  R.PNR = RC.PNR AND RC.CUSTOMER_ID = ? ) RES ");
		sqlBuilder.append("WHERE P.PNR_PAX_ID = CREDITS.PAX_ID AND RES.PNR  = P.PNR ");
		sqlBuilder.append("ORDER BY P.FIRST_NAME, P.LAST_NAME, CREDITS.DATE_EXP ");

		Collection<PaxCreditDTO> collection = (Collection<PaxCreditDTO>) jt.query(sqlBuilder.toString(), new Object[] { new Integer(customerId),
				new Integer(customerId) }, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxCreditDTO> credits = new ArrayList<PaxCreditDTO>();
				PaxCreditDTO paxCreditDTO;

				if (rs != null) {
					while (rs.next()) {
						paxCreditDTO = new PaxCreditDTO();

						paxCreditDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR_NUMBER")));
						paxCreditDTO.setOriginatorPnr(BeanUtils.nullHandler(rs.getString("ORIGINATOR_PNR_NUMBER")));
						paxCreditDTO.setPaxId(BeanUtils.parseInteger(rs.getInt("PAX_ID")));
						paxCreditDTO.setPaxType(BeanUtils.nullHandler(rs.getString("PAX_TYPE_CODE")));
						paxCreditDTO.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
						paxCreditDTO.setFirstName(BeanUtils.nullHandler(rs.getString("F_NAME")));
						paxCreditDTO.setLastName(BeanUtils.nullHandler(rs.getString("L_NAME")));
						paxCreditDTO.setDateOfExpiry(rs.getDate("DATE_EXP"));
						paxCreditDTO.setBalance(rs.getBigDecimal("BALANCE"));

						credits.add(paxCreditDTO);
					}
				}

				return credits;
			}
		});

		return collection;
	}

	@SuppressWarnings("unchecked")
	public Collection<PaxCreditDTO> getPaxCreditForPnrs(List<String> pnrList) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String clause = Util.buildStringInClauseContent(pnrList);

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder
				.append("SELECT * FROM (SELECT r.PNR PNR_NUMBER, rCon.C_TITLE TITLE, rCon.C_FIRST_NAME F_NAME, rCon.C_LAST_NAME L_NAME, p.pax_sequence PAX_SEQ, ");
		sqlBuilder.append("(SELECT SUM(BALANCE) FROM T_PAX_CREDIT c WHERE c.PNR_PAX_ID = p.PNR_PAX_ID) BALANCE, ");
		sqlBuilder
				.append("(SELECT MIN(DATE_EXP) FROM T_PAX_CREDIT c WHERE c.PNR_PAX_ID = p.PNR_PAX_ID AND c.date_exp > sysdate) DATE_EXP ");
		sqlBuilder.append("FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon, T_PNR_PASSENGER p ");
		sqlBuilder.append("WHERE r.PNR=rCon.PNR AND r.PNR = p.PNR AND r.PNR IN (" + clause + "))");

		Collection<PaxCreditDTO> colPaxcCreditDTO = (Collection<PaxCreditDTO>) jt.query(sqlBuilder.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxCreditDTO> credits = new ArrayList<PaxCreditDTO>();
				PaxCreditDTO paxCreditDTO;

				if (rs != null) {
					while (rs.next()) {
								if (rs.getBigDecimal("BALANCE") != null
										&& rs.getDate("DATE_EXP") != null
								&& (rs.getBigDecimal("BALANCE").compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1)) {
							paxCreditDTO = new PaxCreditDTO();

							paxCreditDTO.setPnr(BeanUtils.nullHandler(rs.getString("PNR_NUMBER")));
							paxCreditDTO.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
							paxCreditDTO.setFirstName(BeanUtils.nullHandler(rs.getString("F_NAME")));
							paxCreditDTO.setLastName(BeanUtils.nullHandler(rs.getString("L_NAME")));
							paxCreditDTO.setPaxSequence(rs.getInt("PAX_SEQ"));
							paxCreditDTO.setDateOfExpiry(rs.getDate("DATE_EXP"));
							paxCreditDTO.setBalance(rs.getBigDecimal("BALANCE"));

							credits.add(paxCreditDTO);
						}
					}
				}

				return credits;
			}
		});

		return colPaxcCreditDTO;
	}

	/**
	 * Return Sum of reservation credits
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	public BigDecimal getSumOfReservationCredits(String pnrPaxId) {

		String hql = "select sum(r.balance) " + "from ReservationCredit r " + "where r.pnrPaxId = :pnrPaxId";

		BigDecimal d = (BigDecimal) (getSession().createQuery(hql).setParameter("pnrPaxId", pnrPaxId).uniqueResult());

		if (d == null) {
			d = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		return d;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<CreditInfoDTO>> getAcquiredCredit(Collection<Integer> pnrPaxIds)
			throws CommonsDataAccessException {
		log.debug("Inside acquiredCredit");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = " SELECT a.txn_id, a.pnr_pax_id, a.amount, a.amount-nvl(rc.reinstated_credit, 0) "
				+ " as available, a.dr_cr, a.tnx_date, a.nominal_code, a.pnr_pax_id,  a.lcc_unique_txn_id FROM t_pax_transaction a, "
				+ " (SELECT c.pax_txn_id, sum(c.amount) AS reinstated_credit "
				+ " FROM t_reinstated_credit c " + " WHERE c.PNR_PAX_ID IN (" + Util.buildIntegerInClauseContent(pnrPaxIds)
				+ ") " + " GROUP BY c.pax_txn_id) rc " + " WHERE a.TXN_ID = rc.pax_txn_id (+) " + " AND a.DR_CR = '"
				+ ReservationInternalConstants.TnxTypes.DEBIT + "' AND NOMINAL_CODE=" + ReservationTnxNominalCode.CREDIT_ACQUIRE
				+ " AND a.amount-NVL(rc.reinstated_credit, 0) <> 0 AND a.PNR_PAX_ID IN (" + Util.buildIntegerInClauseContent(pnrPaxIds) + ") ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Map<Integer, Collection<CreditInfoDTO>> paxWiseCredit = (Map<Integer, Collection<CreditInfoDTO>>) jt.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Collection<CreditInfoDTO>> paxWiseCredit = new HashMap<Integer, Collection<CreditInfoDTO>>();

						if (rs != null) {
							while (rs.next()) {
								CreditInfoDTO oCreditInfo = new CreditInfoDTO();
								oCreditInfo.setTxnId(rs.getInt("TXN_ID"));
								oCreditInfo.setExpireDate(rs.getDate("TNX_DATE"));
								oCreditInfo.setEnumStatus(CreditInfoDTO.status.EXPIRED);
								BigDecimal amount = rs.getBigDecimal("AVAILABLE");								
								oCreditInfo.setAmount(amount);
								oCreditInfo.setMcAmount(amount);
								oCreditInfo.setLccUniqueTnxId(rs.getString("lcc_unique_txn_id"));
								Integer pnrPaxId = rs.getInt("PNR_PAX_ID");
								if (!paxWiseCredit.containsKey(pnrPaxId)) {
									paxWiseCredit.put(pnrPaxId, new ArrayList<CreditInfoDTO>());
								}
								oCreditInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
								paxWiseCredit.get(pnrPaxId).add(oCreditInfo);
							}
						}

						return paxWiseCredit;
					}
				});
		log.debug("Exit acquiredCredit");
		return paxWiseCredit;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<CreditInfoDTO>> getAvailableCredit(Collection<Integer> pnrPaxIds)
			throws CommonsDataAccessException {
		log.debug("Inside availableCredit");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		final Collection<Integer> nominalCodes = ReservationTnxNominalCode.getNonRefundableTypePaymentCodes();		

		// Final SQL
		String sql = " SELECT  a.pay_id,a.txn_id, a.balance, a.date_exp, a.note, a.pnr_pax_id, pt.lcc_unique_txn_id, pt.nominal_code FROM t_pax_credit a, t_pax_transaction pt  "
				+ " WHERE a.pnr_pax_id IN (" + Util.buildIntegerInClauseContent(pnrPaxIds) + ") AND a.balance <> 0 AND pt.txn_id = a.txn_id";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Map<Integer, Collection<CreditInfoDTO>> paxWiseCredit = (Map<Integer, Collection<CreditInfoDTO>>) jt.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Collection<CreditInfoDTO>> paxWiseCredit = new HashMap<Integer, Collection<CreditInfoDTO>>();
						if (rs != null) {
							while (rs.next()) {
								CreditInfoDTO oCreditInfo = new CreditInfoDTO();
								oCreditInfo.setCreditId(rs.getInt("PAY_ID"));
								oCreditInfo.setTxnId(rs.getInt("TXN_ID"));
								oCreditInfo.setExpireDate(rs.getDate("DATE_EXP"));
								oCreditInfo.setEnumStatus(CreditInfoDTO.status.AVAILABLE);
								BigDecimal amount = rs.getBigDecimal("BALANCE");
								oCreditInfo.setAmount(amount);
								oCreditInfo.setMcAmount(amount);
								oCreditInfo.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
								oCreditInfo.setUserNote(rs.getString("NOTE"));
								oCreditInfo.setLccUniqueTnxId(rs.getString("LCC_UNIQUE_TXN_ID"));
								Integer pnrPaxId = rs.getInt("PNR_PAX_ID");
								Integer nominalCode = rs.getInt("NOMINAL_CODE");
								if (nominalCodes != null && !nominalCodes.isEmpty() && nominalCodes.contains(nominalCode)) {
									oCreditInfo.setNonRefundableCredit(true);
								}								
								if (!paxWiseCredit.containsKey(pnrPaxId)) {
									paxWiseCredit.put(pnrPaxId, new ArrayList<CreditInfoDTO>());
								}
								oCreditInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
								paxWiseCredit.get(pnrPaxId).add(oCreditInfo);
							}
						}

						return paxWiseCredit;
					}
				});
		log.debug("Exit availableCredit");
		return paxWiseCredit;
	}

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<LoyaltyCreditDTO> getMashreqCredits(int customerId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		log.debug("Inside getMashreqCredits");
		String sql = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT cus.customer_id, lc.loyalty_account_no, lc.credit_Balance, lc.date_earn,lc.date_exp ");
		sb.append(" FROM t_loyalty_credit lc, t_loyalty_customer_profile lcp, t_customer cus ");
		sb.append(" WHERE cus.customer_id = lcp.customer_id ");
		sb.append(" AND lcp.loyalty_account_no = lc.loyalty_account_no ");
		sb.append(" AND cus.customer_id = '" + customerId + "' ");
		sb.append(" AND lcp.status = 'O' ");
		sb.append(" ORDER BY lc.date_exp asc ");

		sql = sb.toString();
		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");
		Collection<LoyaltyCreditDTO> collection = (Collection<LoyaltyCreditDTO>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<LoyaltyCreditDTO> credits = new ArrayList<LoyaltyCreditDTO>();
				LoyaltyCreditDTO loyaltyCredits;

				if (rs != null) {
					while (rs.next()) {
						loyaltyCredits = new LoyaltyCreditDTO();

						loyaltyCredits.setCustomerID(BeanUtils.nullHandler(rs.getInt("CUSTOMER_ID")));
						loyaltyCredits.setLoyaltyAccountNo(BeanUtils.nullHandler(rs.getString("LOYALTY_ACCOUNT_NO")));
						loyaltyCredits.setCredit(new BigDecimal(BeanUtils.nullHandler(rs.getDouble("CREDIT_BALANCE"))));
						loyaltyCredits.setDateOfEarn(rs.getDate("DATE_EARN"));
						loyaltyCredits.setDateOfExpiry(rs.getDate("DATE_EXP"));

						credits.add(loyaltyCredits);
					}
				}

				return credits;
			}
		});
		log.debug("Exit getMashreqCredits");
		return collection;
	}

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<LoyaltyCreditUsageDTO> getMashreqCreditUsage(int customerId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		log.debug("Inside getMashreqCreditUsage");
		String sql = null;
		String nominalCode = "ONACCOUNT_PAYMENT";
		StringBuffer sb = new StringBuffer();

		sb.append(" SELECT r.pnr,r.originator_pnr,pt.tnx_date payment_date,ABS (pt.amount) amount ");
		sb.append(" FROM t_pax_transaction pt,t_pnr_passenger pp,t_reservation r, t_reservation_contact rc,t_customer c,t_loyalty_customer_profile lcp ");
		sb.append(" WHERE r.pnr=rc.pnr AND r.pnr = pp.pnr AND pp.pnr_pax_id = pt.pnr_pax_id AND rc.customer_id = c.customer_id ");
		sb.append(" AND c.customer_id = lcp.customer_id ");
		sb.append(" AND pt.nominal_code = (SELECT ptnc.nominal_code FROM t_pax_trnx_nominal_code ptnc ");
		sb.append(" where ptnc.description = '" + nominalCode + "') ");
		sb.append(" AND pt.agent_code IN (SELECT param_value FROM t_app_parameter WHERE param_key = 'RES_84') ");
		sb.append(" AND pt.amount <> 0 AND c.CUSTOMER_ID = '" + customerId + "' ");

		sql = sb.toString();

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");
		Collection<LoyaltyCreditUsageDTO> collection = (Collection<LoyaltyCreditUsageDTO>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<LoyaltyCreditUsageDTO> credits = new ArrayList<LoyaltyCreditUsageDTO>();
				LoyaltyCreditUsageDTO creditUsage;

				if (rs != null) {
					while (rs.next()) {
						creditUsage = new LoyaltyCreditUsageDTO();

						creditUsage.setPnr(new Integer(BeanUtils.nullHandler(rs.getInt("PNR"))));
						creditUsage.setOriginatorPnr(BeanUtils.nullHandler(rs.getString("ORIGINATOR_PNR")));
						creditUsage.setPaidAmount(new BigDecimal(BeanUtils.nullHandler(rs.getDouble("AMOUNT"))));
						creditUsage.setPaidDate(rs.getDate("PAYMENT_DATE"));
						credits.add(creditUsage);
					}
				}

				return credits;
			}
		});
		log.debug("Exit getMashreqCreditUsage");
		return collection;
	}

	@SuppressWarnings("unchecked")
	public Collection<ReservationCredit> getReservationCreditsOrderByExpiryDate(String pnrPaxId, boolean isAssending) {
		String orderBy = "";

		if (isAssending) {
			orderBy = " order by r.expiryDate asc ";
		} else {
			orderBy = " order by r.expiryDate desc ";
		}

		return find("select r from ReservationCredit r where r.pnrPaxId = ? " + orderBy, pnrPaxId, ReservationCredit.class);
	}

	public Collection<ReservationCredit> getReservationCredits(String pnrPaxId, Collection<Integer> tnxIds) {
		String clause = Util.buildIntegerInClauseContent(tnxIds);

		return find(
				"select r from ReservationCredit r where r.pnrPaxId = ? AND r.tnxId in (" + clause
						+ ") order by r.expiryDate asc ", pnrPaxId, ReservationCredit.class);
	}
	
	public void deleteInvalidPaxPaymentCredits() {
		String deleteZeroCreditRecordsHql = "DELETE FROM ReservationPaxPaymentCredit WHERE amount=0";

		getSession().createQuery(deleteZeroCreditRecordsHql).executeUpdate();
	}	
	
	public Collection<ReservationPaxPaymentCredit> getReservationPaxPaymentCredit(Long paymentTnxId) {

		String hql = "SELECT rppc FROM ReservationPaxPaymentCredit  rppc, ReservationCredit rc "
				+ "WHERE rc.creditId = rppc.reservationCredit.creditId AND rc.tnxId = ? ";

		Object[] params = { paymentTnxId };
		return find(hql, params, ReservationPaxPaymentCredit.class);

	}
	
}
