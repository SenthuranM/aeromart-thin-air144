package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.OfficerMobNumsDTO;
import com.isa.thinair.airreservation.api.model.OfficersMobileNumbers;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.OfficersMobileNumbersDAO;
import com.isa.thinair.commons.api.dto.OfficerMobNumsConfigsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 
 * @isa.module.dao-impl dao-name="OfficersMobileNumbersDAO"
 */

public class OfficersMobileNumbersDAOImpl  extends PlatformBaseHibernateDaoSupport implements OfficersMobileNumbersDAO {

	private static Log log = LogFactory.getLog(OfficersMobileNumbersDAOImpl.class);



	@Override
	public OfficersMobileNumbers getOfficersMobNums(int OfficerId) throws ModuleException {
		log.info("get OfficersMobileNumber.....");
		return (OfficersMobileNumbers) get(OfficersMobileNumbers.class, OfficerId);
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Page getOfficersMobileNumbers(OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTO ,int startIndex,int pageLength)
			throws ModuleException {

		String[] searchCriteria = getSearchCriteria(officerMobNumsConfigsSearchDTO,startIndex, pageLength);
		int totalresultCount = getOfficersMobileNumbersCount(searchCriteria[1]);
		Collection<OfficerMobNumsDTO> onholdReleaseTimes = getOfficersMobileNumbersConfigs(searchCriteria[0]);
		return new Page(totalresultCount, startIndex, startIndex + pageLength, onholdReleaseTimes);

	}

	private String[] getSearchCriteria(OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTO,int startIndex,int pageLength) {

		String[] searchCritria = new String[2];

		StringBuilder sbResultSearch = new StringBuilder(
					"SELECT * from (SELECT DISTINCT rownum seq,x.* from ("
						+ "select distinct omn.officer_id,omn.officer_name,omn.mobile_number,omn.version, omn.EMAIL_ADDRESS"
						+ " from t_officers_mobile_numbers omn"
						+" WHERE 1 = 1");
		
		if (officerMobNumsConfigsSearchDTO.getOfficerName() != null
				&& officerMobNumsConfigsSearchDTO.getOfficerName().trim().length() > 0) {
			sbResultSearch.append(" AND omn.officer_name = '" + officerMobNumsConfigsSearchDTO.getOfficerName() + "'");
		}
		
		if (officerMobNumsConfigsSearchDTO.getMobNumber() != null
				&& officerMobNumsConfigsSearchDTO.getMobNumber().trim().length() > 0) {
			sbResultSearch.append(" and omn.mobile_number like '%" + officerMobNumsConfigsSearchDTO.getMobNumber() + "%'");
		}


		StringBuilder sbResultsCount = new StringBuilder(
				"SELECT count(omn.officer_id) as REC_COUNT from t_officers_mobile_numbers omn where omn.mobile_number is not null ");

		sbResultSearch.append(" order by omn.officer_id ) x)");
		sbResultsCount.append(" order by omn.officer_id ");

		sbResultSearch.append(" WHERE seq BETWEEN " + (startIndex+1) + " AND " + (startIndex + pageLength));

		searchCritria[0] = sbResultSearch.toString();
		searchCritria[1] = sbResultsCount.toString();
		return searchCritria;

	
	}
	
	private int getOfficersMobileNumbersCount(String searchCountCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Integer resultCount = (Integer) jdbcTemplate.query(searchCountCriteria.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int resultCount = 0;
				while (rs.next()) {
					resultCount = rs.getInt("REC_COUNT");

				}
				return resultCount;
			}
		});
		return resultCount;
	}
	
	private Collection<OfficerMobNumsDTO> getOfficersMobileNumbersConfigs(String searchCriteria) {

		JdbcTemplate jbdcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Collection<OfficerMobNumsDTO> colMobNumbers = (Collection<OfficerMobNumsDTO>) jbdcTemplate.query(
				searchCriteria.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<OfficerMobNumsDTO> colOfficersMobileNumbers = new ArrayList<OfficerMobNumsDTO>();
						while (rs.next()) {
							OfficerMobNumsDTO officersMobileNumbers = new OfficerMobNumsDTO();
							
							officersMobileNumbers.setOfficerId(rs.getInt("officer_id"));
							officersMobileNumbers.setOfficerName(rs.getString("officer_name"));
							officersMobileNumbers.setMobNumber(rs.getString("mobile_number"));
							officersMobileNumbers.setVersion(rs.getLong("version"));
							officersMobileNumbers.setContactEmail(rs.getString("EMAIL_ADDRESS"));

							colOfficersMobileNumbers.add(officersMobileNumbers);
						}
						return colOfficersMobileNumbers;
					}
				});
		return colMobNumbers;
	}


	@Override
	public void deleteOfficersMobileNumbersConfig(int OfficerId)
			throws ModuleException {
			log.info("delete OfficersMobileNumber");
			OfficersMobileNumbers OfficersMobileNumbersObj = getOfficersMobNums(OfficerId);
			delete(OfficersMobileNumbersObj);
	}


	@Override
	public void saveOrUpdateOfficersMobileNumbersConfig(
			OfficersMobileNumbers OfficersMobNums) throws ModuleException {
			log.info("save or update OfficersMobileNumber.....");
			hibernateSaveOrUpdate(OfficersMobNums);
		
		
	}

	@SuppressWarnings("unchecked")
	public List<String> getOfficersMobileNumbersList() throws ModuleException {
		
		StringBuilder sbResultSearch = new StringBuilder("SELECT mn.mobile_number "
				+ "from t_officers_mobile_numbers mn"
				+ " where mn.mobile_number is not null "
				+ "order by mn.officer_id");

		JdbcTemplate jdbcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		List<String> mobileNums =  (List<String>) jdbcTemplate.query(sbResultSearch.toString(), new ResultSetExtractor() {
			
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> mobNums = new ArrayList<String>();
				while (rs.next()) {
					mobNums.add( rs.getString("mobile_number"));
				}
				return mobNums;
			}
		});
		return mobileNums;

	}


	@Override
	public Collection<String> getOfficersEmailList() throws ModuleException {
		String sql = "select email_address from t_officers_mobile_numbers where email_address is not null ";
		
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		
		List<String> emailList =  (List<String>) jt.query(sql.toString(), new ResultSetExtractor() {
			
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> emails = new ArrayList<String>();
				while (rs.next()) {
					emails.add( rs.getString("email_address"));
				}
				return emails;
			}
		});
		return emailList;
	}


}
