package com.isa.thinair.airreservation.core.bl.commission;

import java.util.List;

import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;

/**
 * Agent commission Strategy for add segment
 */
public class AddSegmentCS implements CommissionStrategy {

	@Override
	public boolean execute(PreferenceAnalyzer preferenceAnalyzer, String ondCode, List<String> journeyOnds, List<List<String>> journeyOndSummary) {
		preferenceAnalyzer.setOldFareId(null); // no need to remove commission from previous onds
		return true;
	}

}
