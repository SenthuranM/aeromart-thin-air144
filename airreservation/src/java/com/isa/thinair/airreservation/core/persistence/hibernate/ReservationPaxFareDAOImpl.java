/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaxFareDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * ReservarionPaxFareDAOImpl is the business DAO hibernate implmentation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationPaxFareDAO"
 */
public class ReservationPaxFareDAOImpl extends HibernateDaoSupport implements ReservationPaxFareDAO {

	/**
	 * Returns Object[]{pnrPaxId, pnrPaxFareId, paxType} corresponding to first adult or child pax with pnrPaxFareId,
	 * paxType linked to first segment.
	 * 
	 * @param pnr
	 * @return Object[]{pnrPaxId, pnrPaxFareId, paxType}
	 */
	public Object[] getFirstPaxPnrPaxFareId(String pnr) {
		String sql = "SELECT pnr_pax_id, ppf_id, pax_type_code FROM ( " + " 	SELECT PP.pnr_pax_id, PPF.ppf_id, PP.pax_type_code "
				+ " 	FROM    T_RESERVATION R, " + "				T_PNR_PAX_FARE PPF, " + " 			T_PNR_PASSENGER PP, "
				+ " 			T_PNR_SEGMENT PS, " + " 			T_PNR_PAX_FARE_SEGMENT PPFS, " + " 			T_FLIGHT_SEGMENT FS "
				+ " 	WHERE 	R.pnr = PP.pnr AND " + " 			R.pnr = PS.pnr AND " + "				PPF.pnr_pax_id = PP.pnr_pax_id AND "
				+ " 			PPF.ppf_id = PPFS.ppf_id AND " + " 			PPFS.pnr_seg_id = PS.pnr_seg_id AND "
				+ " 			PS.flt_seg_id = FS.flt_seg_id AND "
				+ " 			((PS.status = 'CNF' AND R.status <> 'CNX') OR R.status = 'CNX') AND " + " 			(PP.pax_type_code = '"
				+ ReservationInternalConstants.PassengerType.ADULT + "' " + "				 OR PP.pax_type_code = '"
				+ ReservationInternalConstants.PassengerType.CHILD + "') " + "				 AND R.pnr = ? "
				+ " 	ORDER BY pp.pax_sequence, fs.est_time_departure_local) " + " WHERE rownum=1";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		final Object[] responses = new Object[3];

		template.query(sql, new Object[] { pnr }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					responses[0] = rs.getInt("pnr_pax_id");
					responses[1] = rs.getInt("ppf_id");
					responses[2] = rs.getString("pax_type_code");
				}
				return responses;
			}
		});
		return responses;
	}

	/**
	 * Returns reservation segments
	 * 
	 * @param pnr
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ReservationPaxFare> getReservationPaxFare(Integer pnrPaxId) throws CommonsDataAccessException {
		String sql = "SELECT pnr_pax_id, ppf_id FROM t_pnr_pax_fare WHERE pnr_pax_id = ?";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		List<ReservationPaxFare> reservationPaxFares = (List<ReservationPaxFare>) template.query(sql, new Object[] { pnrPaxId },
				new ResultSetExtractor() {
					public List<ReservationPaxFare> extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<ReservationPaxFare> reservationPaxFares = new ArrayList<ReservationPaxFare>();
						while (rs.next()) {
							ReservationPax reservationPax = new ReservationPax();
							reservationPax.setPnrPaxId(rs.getInt("pnr_pax_id"));

							ReservationPaxFare reservationPaxFare = new ReservationPaxFare();
							reservationPaxFare.setPnrPaxFareId(rs.getInt("ppf_id"));
							reservationPaxFare.setReservationPax(reservationPax);

							reservationPaxFares.add(reservationPaxFare);
						}
						return reservationPaxFares;
					}
				});
		return reservationPaxFares;
	}

	public Integer getReservationPaxFareId(Integer pnrSegmentId) {
		// check booking code to filter infant
		String sql = "SELECT PPF_ID FROM T_PNR_PAX_FARE_SEGMENT " + "WHERE PNR_SEG_ID = ? AND BOOKING_CODE IS NOT NULL";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Integer ppfId = (Integer) template.query(sql, new Object[] { pnrSegmentId }, new ResultSetExtractor() {

			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer ppfId = new Integer(-1);
				while (rs.next()) {
					ppfId = rs.getInt("PPF_ID");
					break;
				}
				return ppfId;
			}
		});
		return ppfId;
	}
	
	public BigDecimal getTotalReturnJourneyFare(Integer pnrPaxId, Integer pnrPaxFareId) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT SUM(PPF.TOTAL_FARE) AS TOTAL_FARE FROM  T_PNR_PASSENGER PP, T_PNR_PAX_FARE PPF , "
				+ "T_PNR_PAX_FARE_SEGMENT PPFS,T_PNR_SEGMENT PS ");
		sql.append("WHERE  PP.PNR_PAX_ID = ? AND PPF.PNR_PAX_ID = PP.PNR_PAX_ID ");
		sql.append("AND PS.PNR_SEG_ID = PPFS.PNR_SEG_ID AND PS.RETURN_GROUP_ID IN (SELECT PS.RETURN_GROUP_ID ");
		sql.append("FROM T_PNR_PAX_FARE PPF , T_PNR_PAX_FARE_SEGMENT PPFS, T_PNR_SEGMENT PS ");
		sql.append("WHERE PPFS.PPF_ID = PPF.PPF_ID AND PS.PNR_SEG_ID = PPFS.PNR_SEG_ID AND PPF.PPF_ID  =?) ");
		sql.append("AND PPFS.PPF_ID = PPF.PPF_ID ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		BigDecimal totalRetJourneyFare = (BigDecimal) template.query(sql.toString(), new Object[] { pnrPaxId, pnrPaxFareId },
				new ResultSetExtractor() {
					public BigDecimal extractData(ResultSet rs) throws SQLException, DataAccessException {
						BigDecimal totalRetJourneyFare = AccelAeroCalculator.getDefaultBigDecimalZero();
						while (rs.next()) {
							totalRetJourneyFare = rs.getBigDecimal("TOTAL_FARE");
							break;
						}
						return totalRetJourneyFare;
					}
				});
		return totalRetJourneyFare;
	}
	
	
}
