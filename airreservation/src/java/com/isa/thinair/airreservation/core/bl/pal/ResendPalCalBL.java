package com.isa.thinair.airreservation.core.bl.pal;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.PnlAdlDTO;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ResendPalCalBL {

	private static Log log = LogFactory.getLog(ResendPalCalBL.class);

	private static final int DEFAULT_SEARCH_RANGE_IN_MINUTES = 60;

	public void checkAndResend() throws ModuleException {
		log.debug("############################ Checking Failed PAL or CAL");
		String searchRange = ReservationModuleUtils.getAirReservationConfig().getPalCalSearchRangeInMins();
		int rangeInMins = DEFAULT_SEARCH_RANGE_IN_MINUTES;

		if (searchRange != null) {
			rangeInMins = Integer.valueOf(searchRange).intValue();
		}

		log.info("############################ Going to Search for failed PALS/CALS withing range (mins) :" + rangeInMins);

		HashMap<Integer,PnlAdlDTO> mapPalCalHistoryDTO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getFailuresWithin(rangeInMins, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL);

		if (mapPalCalHistoryDTO == null || mapPalCalHistoryDTO.keySet().size() == 0) {
			log.info("############################  Failed PALS/CALS Found None ");
			return;
		}

		Iterator<Integer> listIterator = mapPalCalHistoryDTO.keySet().iterator();
		log.info("############################ Number of Failed PALs/CALs : " + mapPalCalHistoryDTO.keySet().size());

		while (listIterator.hasNext()) {
			Integer hisID = (Integer) listIterator.next();
			PnlAdlDTO palcalDTO = (PnlAdlDTO) mapPalCalHistoryDTO.get(hisID);
			sendMail(palcalDTO);
		}
	}

	/**
	 * Send Email
	 * 
	 * @param palCalDTO
	 * @throws ModuleException
	 */
	private void sendMail(PnlAdlDTO palCalDTO) throws ModuleException {
		String flightNumber = palCalDTO.getFlightNum();
		String depAirport = palCalDTO.getAirportCode();
		Date dateOfFlightZulu = palCalDTO.getFlightDepartureDateZulu();
		Date dateOfFlightLocal = palCalDTO.getFlightDepartureDateLocal();
		String[] sitaAddress = { palCalDTO.getSitaAddress() };
		String msgType = palCalDTO.getMessageType();

		if (flightNumber == null || depAirport == null || dateOfFlightZulu == null || sitaAddress.length == 0 || msgType == null) {
			log.error("################ CAN NOT RESEND FOR FAILED PAL; NULL PARAM FOUND");
			return;
		}

		log.info("##### GOING TO REGENERATE AND RESEND  " + msgType + " FOR : --");
		log.info("##### FLIGHT NUMBER : " + flightNumber);
		log.info("##### DEP AIRPORT : " + depAirport);
		log.info("##### DATE OF FLIGHT Zulu :" + dateOfFlightZulu);
		log.info("##### DATE OF FLIGHT Local :" + dateOfFlightLocal);
		log.info("##### SITA ADDRESS(s) :" + sitaAddress[0]);

	
		log.info("##### GOING TO CHECK IF NEED TO SEND ");
		if (hasExceededMaxAttempt(palCalDTO) || exceedsFlightDepartureTime(dateOfFlightZulu) || hasSucuessfullAttempt(palCalDTO)) {

			log.info("##### WILL NOT RESEND PAL/CAL");
			return;
		}
		try {

			if (PNLConstants.MessageTypes.PAL.equals(palCalDTO.getMessageType())) {
				new PALBL().sendPAL(flightNumber, depAirport, dateOfFlightLocal, sitaAddress,
						PNLConstants.SendingMethod.RESENDER);

			} else {
				new CALBL().sendCALMessage(flightNumber, depAirport, dateOfFlightLocal, sitaAddress,
						PNLConstants.SendingMethod.RESENDER);
			}
		} catch (ModuleException e) {
			log.error("########## Did not resend : " + e.getExceptionCode());
		}

	}

	/**
	 * Gets a count of records in PNLADLTXHISTORY, where sita=? flightid ? and Trnmission status = N
	 * 
	 * @param pnlAdlDTO
	 * @return
	 */
	private boolean hasExceededMaxAttempt(PnlAdlDTO pnlAdlDTO) {
		String retryLimit = ReservationModuleUtils.getAirReservationConfig().getPalCalRetryLimit();
		int maxThreshold;
		if (retryLimit == null || retryLimit.equals("0")) {
			maxThreshold = 2;
		} else {
			maxThreshold = Integer.valueOf(retryLimit).intValue();
		}
		int numberOfAttempts = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getNumberOfFailedAttempts(
				pnlAdlDTO.getFlightId(), pnlAdlDTO.getSitaAddress(), pnlAdlDTO.getMessageType());

		if (numberOfAttempts < maxThreshold) {
			return false;
		}
		log.info("##### HAS EXCEEDED MAX ATTEMPTS [NUM OF ATTEMPTS: " + numberOfAttempts + " THRESHOLD :" + maxThreshold + "]");
		return true;

	}

	private boolean exceedsFlightDepartureTime(Date flightDateTime) {
		GregorianCalendar calFlightDate = new GregorianCalendar();
		calFlightDate.setTime(flightDateTime);

		GregorianCalendar calNow = new GregorianCalendar();
		calNow.setTime(new Date());

		if (calNow.after(calFlightDate)) {
			log.info("##### FLIGHT HAS ALREADY DEPARTED FLIGHT TIME ZULU:" + flightDateTime);
			return true;
		}
		return false;
	}

	private boolean hasSucuessfullAttempt(PnlAdlDTO pnlAdlDTO) {
		int numberOfSuccssAttempts = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.getNumberOfSuccessAtempts(
				pnlAdlDTO.getFlightId(), pnlAdlDTO.getSitaAddress(), pnlAdlDTO.getMessageType(),
				pnlAdlDTO.getTransmissionTimeStamp());

		// manualy can send adls many time
		if (numberOfSuccssAttempts >= 1) {
			log.info("##### WILL NOT PROCEED! A SUCCESSFUL PAL/CAL WAS FOUND:[DEP AIRPORT:" + pnlAdlDTO.getAirportCode()
					+ " FLIGHT NUMBER :" + pnlAdlDTO.getFlightNum() + " SITA ADD :" + pnlAdlDTO.getSitaAddress() + "]");
			return true;
		}

		return false;
	}
}
