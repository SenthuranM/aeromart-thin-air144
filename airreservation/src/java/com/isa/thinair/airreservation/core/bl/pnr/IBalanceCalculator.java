package com.isa.thinair.airreservation.core.bl.pnr;

import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;

public interface IBalanceCalculator {
	public void calculate();

	public ReservationBalanceTO getReservationBalance();
}