package com.isa.thinair.airreservation.core.bl.segment;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ChargeTnxSeqGenerator {

	private Reservation reservation;
	private String pnr;
	private Map<String, Integer> paxTypeMap;
	private Map<Integer, Integer> paxMap;
	private boolean isNewTransaction = false;
	private Integer maxChgSequence;

	public ChargeTnxSeqGenerator(String pnr) throws ModuleException {
		this(pnr, true);
	}

	public ChargeTnxSeqGenerator(String pnr, boolean isNewTransaction) throws ModuleException {
		this.pnr = pnr;
		this.isNewTransaction = isNewTransaction;
	}

	public ChargeTnxSeqGenerator(Reservation reservation) throws ModuleException {
		this(reservation, true);
	}

	public ChargeTnxSeqGenerator(Reservation reservation, boolean isNewTransaction) throws ModuleException {
		this.reservation = reservation;
		this.pnr = reservation.getPnr();
		this.isNewTransaction = isNewTransaction;
	}

	private Reservation getReservation() throws ModuleException {
		if (reservation == null) {
			if (pnr != null) {
				loadReservation(pnr);
			}
		}
		return this.reservation;
	}

	private void loadReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		this.reservation = ReservationProxy.getReservation(pnrModesDTO);
	}

	public void generateSequence() throws ModuleException {
		if (paxMap == null) {
			Integer maxChgSequence = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getPNRMaxChargeTnxSequence(pnr);
			if (maxChgSequence == -1
					|| getReservation().getTotalChargeAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0
							&& AccelAeroCalculator.getDefaultBigDecimalZero()
									.compareTo(getReservation().getTotalAvailableBalance()) == 0) {
				if (isNewTransaction) {
					maxChgSequence = maxChgSequence + 1;
				}
			}
			this.maxChgSequence = maxChgSequence;
			this.paxTypeMap = new HashMap<>();
			this.paxMap = new HashMap<>();
			for (ReservationPax resPax : getReservation().getPassengers()) {

				paxTypeMap.put(resPax.getPaxType(), maxChgSequence);
				paxMap.put(resPax.getPnrPaxId(), maxChgSequence);
			}
		}
	}

	public Integer getTnxSequence(String paxType) throws ModuleException {
		generateSequence();
		Integer txnSeq = paxTypeMap.get(paxType);
		if (txnSeq == null || txnSeq == -1) {
			txnSeq = maxChgSequence;
		}

		return txnSeq;
	}

	public Integer getTnxSequence(Integer pnrPaxId) throws ModuleException {
		generateSequence();
		Integer txnSeq = paxMap.get(pnrPaxId);
		if (txnSeq == null || txnSeq == -1) {
			txnSeq = maxChgSequence;
		}

		return txnSeq;
	}

}
