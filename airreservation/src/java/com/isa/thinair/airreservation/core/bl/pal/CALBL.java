package com.isa.thinair.airreservation.core.bl.pal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext.CalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdlDeliveryInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class CALBL {

	private final Timestamp currentTimeStamp;

	private final Date currentDate;

	private Collection<PassengerInformation> addedPassengerList = new ArrayList<PassengerInformation>();

	private ArrayList<String> generatedCALfileNames = new ArrayList<String>();

	private ReservationAuxilliaryDAO auxilliaryDAO = null;

	private String msgType = null;

	private HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();

	private String lastGroupCode = new String();

	private HashMap<String, String> mapPnrPaxSegIDGroupCode = new HashMap<String, String>();

	private static Log log = LogFactory.getLog(CALBL.class);

	public static final String Email = "E";
	public static final String PAL = "PAL";
	public static final String CAL = "CAL";

	public CALBL() {

		this.auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		this.currentDate = new Date();
		this.currentTimeStamp = new Timestamp(currentDate.getTime());

		this.msgType = PNLConstants.MessageTypes.CAL;
	}
	
	
	public void sendCALMessage(int flightId, String depAirportCode) throws ModuleException {

		String flightNumber = "";
		Date dateOfflightLocal = null;
		
		flightNumber = auxilliaryDAO.getFlightNumber(flightId);

		Date zuluDate = auxilliaryDAO.getFlightZuluDate(flightId, depAirportCode);
		if (!auxilliaryDAO.hasAnyPALCALHistory(flightNumber, zuluDate, depAirportCode, true)) {
			try {
				new PALBL().sendPALMessage(flightId, depAirportCode);
			} catch (Exception e) {
				log.error("PAL Operation Failed during retry operation For flghtId :" + flightId + " Flt Num :"
						+ flightNumber + " airport :" + depAirportCode + " Error :" + e.getMessage());

			}
		}

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
		
		Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
		String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);

		Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(depAirportCode.trim(),
				flightNumber, ond, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL);
		List<String> sitaAddressesList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
		List<String> sitaTexAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
		List<String> sitaAddressAll = new ArrayList<String>();
		sitaAddressAll.addAll(sitaAddressesList);
		sitaAddressAll.addAll(sitaTexAddressList);

		log.debug("###################### Retrieved Sita List: fid: " + flightId + " airport : " + depAirportCode
				+ " Carrier code :" + carrierCode);

		dateOfflightLocal = auxilliaryDAO.getFlightLocalDate(flightId, depAirportCode);
		
		sendCALAuxiliaryNew(flightNumber, depAirportCode, dateOfflightLocal,
				(String[]) sitaAddressAll.toArray(new String[sitaAddressAll.size()]), PNLConstants.SendingMethod.SCHEDSERVICE);

		
	
	}

	public void sendCALMessage(String flightNumber, String departureStation, Date dateOfflight, String[] sitaaddresses,
			String sendingMethod) throws ModuleException {		
		sendCALAuxiliaryNew(flightNumber, departureStation, dateOfflight, sitaaddresses, sendingMethod);
	}
	
	@SuppressWarnings("unchecked")
	private void sendCALAuxiliaryNew(String flightNumber, String depAirportCode, Date dateOfflightLocal, String sitaaddresses[],
			String sendingMethod) throws ModuleException {
		
		synchronized (this) {

			boolean isFlightsAvailableForManulPAL = false;
		
			ModuleException exceptiontoThrow = null;
		
			HashMap<String, String> sitaStatusMap = new HashMap<String, String>();
			
			String fileContent = null;
			String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
			
			List<String> sitas = PnlAdlUtil.composeSitaAddresses(sitaaddresses);
			
			int flightId = auxilliaryDAO.getFlightID(flightNumber, depAirportCode, dateOfflightLocal);
	//		boolean saveWSCALHistory = false;
			if (flightId == -1) {
				isFlightsAvailableForManulPAL = true;
				throw new ModuleException("airreservations.auxilliary.invalidflight", "airreservations");
			}
			
			Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
			String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);

			Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(depAirportCode, flightNumber, ond,
					carrierCode, sitas, DCS_PAX_MSG_GROUP_TYPE.PAL_CAL);
			List<String> sitaAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
			List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
			List<String> arincAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);

			PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddress, sitaTexAddress, arincAddresses);

			try {

				String[] monthDay = PnlAdlUtil.getMonthDayArray(dateOfflightLocal);
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.PAL_CAL_DATA_GENERATOR_MACRO);
				command.setParameter(CommandParamNames.PNL_DATA_CONTEXT, 
						createCalDataContext(flightNumber, depAirportCode,
								dateOfflightLocal, sitaaddresses, sendingMethod));
				
				ServiceResponce serviceResponce = command.execute();
				
				MessageResponseAdditionals additionals = 
						(MessageResponseAdditionals)serviceResponce.getResponseParam
						(CommandParamNames.PNL_MESSAGE_RESPONSE_ADDITIONALS);
				Map<Integer,String> messageParts = (Map)serviceResponce.
						getResponseParam(CommandParamNames.PNL_MESSAGES_PARTS);
				BaseDataContext baseDataContext = (BaseDataContext)serviceResponce.
						getResponseParam(CommandParamNames.PNL_DATA_CONTEXT);
				
				lastGroupCode = additionals.getLastGroupCode();
				ccPaxMap = additionals.getFareClassWisePaxCount();
				this.generatedCALfileNames = (ArrayList)createMessaeFile(messageParts, 
						flightNumber, depAirportCode, monthDay[0]
						, monthDay[1]);
			
				PnlAdlUtil.prepareGroupCodeMapWith(additionals.getPnrPaxIdvsSegmentIds(), mapPnrPaxSegIDGroupCode, additionals.getPnrPaxVsGroupCodes());
				addedPassengerList = additionals.getPassengerInformations();

				/*if (baseDataContext.isWebserviceSuccess()) {
					saveWSCALHistory = true;
				}*/

				PnlAdlUtil.updateAirportMessagePassengerStatusFromPnlPaxIds(getAirportMsgPaxIds(additionals.getPassengerInformations()));

				Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();

				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddress);
				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddresses);
				
				sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedCALfileNames, msgType);
				sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddress, this.generatedCALfileNames, msgType,
						sitaStatusMap, null);

				fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedCALfileNames);

				updateCalStatusAndGroupId(addedPassengerList, mapPnrPaxSegIDGroupCode);

				PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);

			} catch (ModuleException exception) {

				exceptiontoThrow = exception;
				throw exceptiontoThrow;
			} catch (CommonsDataAccessException e) {

				exceptiontoThrow = new ModuleException(e, e.getExceptionCode());
				throw exceptiontoThrow;
			} catch (Exception e) {

				exceptiontoThrow = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

				throw exceptiontoThrow;

			} finally {

				log.debug("######### Before Inserting to History for CAL For FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						depAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoThrow, sendingMethod, lastGroupCode);
				log.debug("######### Inserted To History OK!");

				/*if(AppSysParamsUtil.isDCSConnectivityEnabled()){
					// Insert WS ADL history
					if (saveWSCALHistory) {
						PnlAdlUtil.saveWSADLHistory(depAirportCode, flightId, currentTimeStamp);
					}
				}*/
				
				if (exceptiontoThrow != null) {

					try {

						PnlAdlUtil.savePNLADLErrorLog(flightId, msgType, depAirportCode, flightNumber, currentDate,
								exceptiontoThrow);

						// if pax count Record was not found, will attempt to send the pnl.
						if (exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.PAL_COUNT_NOT_FOUND_FOR_CAL)) {
							new PALBL().sendPALMessage(flightId, depAirportCode);
						}

						// dont notify the flight closure error etc etc
						if (exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_CAL_ERROR_CODE)
								|| exceptiontoThrow.getExceptionCode().equals(
										PNLConstants.ERROR_CODES.FLIGHT_CLOSED_CALERROR_CODE)
								|| exceptiontoThrow.getExceptionCode().equals(
										PNLConstants.ERROR_CODES.PAL_COUNT_NOT_FOUND_FOR_CAL)
								|| exceptiontoThrow.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS)) {
						}// notify all other errors
						else {
							String sendingOperation = PnlAdlUtil.getSendingOperationDescription(sendingMethod);

							if (!isFlightsAvailableForManulPAL) {
								PnlAdlUtil.notifyAdminForManualAction(flightNumber, dateOfflightLocal, depAirportCode,
										exceptiontoThrow, PNLConstants.ADLGeneration.ADL + " " + sendingOperation, null);

								log.error("############### CAL " + sendingOperation + "Error flghtId :" + flightId + " Flt Num :"
										+ flightNumber + " airport :" + depAirportCode, exceptiontoThrow);
							}
							// throw exception and rollback pax status's
							throw exceptiontoThrow;
						}
					} catch (Throwable e) {
						if (e.equals(exceptiontoThrow)) {
							throw exceptiontoThrow;
						} else {
							log.error("Error in Finaly Method of sendCAL when logging errors or emailing errors ", e);
						}

					}

				}

				PnlAdlUtil.moveGeneratedFiles(this.generatedCALfileNames, msgType);
			}
			log.info("########## PAL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- " + depAirportCode
					+ " Flight Num : " + flightNumber);
		}
	
		
	}


	private CalDataContext createCalDataContext(String flightNumber, String departureAirportCode, Date dateOfflightLocal,
			String sitaaddresses[], String sendingMethod){
		
			
		FeaturePack featurePack = new FeaturePack();
		//featurePack.setRbdEnabled(AppSysParamsUtil.isRBDPNLADLEnabled());
		//featurePack.setShowBaggage(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE));
		//featurePack.setShowMeals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL));
		//featurePack.setShowSeatMap(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP));
		//featurePack.setShowEticketDetails(AppSysParamsUtil.isShowTicketDetailsInPnlAdl());
		//featurePack.setDcsConnectivityEnabled(AppSysParamsUtil.isDCSConnectivityEnabled());
		
		CalDataContext calDataContext = new CalDataContext();
		calDataContext.setFlightNumber(flightNumber);
		calDataContext.setDepartureAirportCode(departureAirportCode);
		calDataContext.setFlightLocalDate(dateOfflightLocal);
		calDataContext.setPnlAdlDeliveryInformation(createPnlCalDeliveryInformation(sitaaddresses, sendingMethod));
		calDataContext.setFeaturePack(featurePack);
		calDataContext.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightNumber));		
		return calDataContext;
		
	}
		private PnlAdlDeliveryInformation createPnlCalDeliveryInformation(String sitaaddresses[], String sendingMethod){
			
			PnlAdlDeliveryInformation pnlAdlDeliveryInformation = new PnlAdlDeliveryInformation();
			pnlAdlDeliveryInformation.setSendingMethod(sendingMethod);
			pnlAdlDeliveryInformation.setSitaaddresses(sitaaddresses);
			return pnlAdlDeliveryInformation;
			
		}
		
	private List<String> createMessaeFile(Map<Integer,String> messageParts,	String flightNo,String boardingAirport,String month,String day) throws ModuleException{
			
		List<String> messageFiles = new ArrayList<String>();
		try{
			for (Map.Entry<Integer, String> entry : messageParts.entrySet()) {
				messageFiles.add(createMessageFile(entry.getKey(),entry.getValue(),flightNo,boardingAirport, month, day));
			}
		}catch (Exception e) {
			log.error("Generate PAL files Error :", e);
			throw new ModuleException(e, PNLConstants.ERROR_CODES.CAL_GENERATION_ERROR_CODE);
		}
		
		return messageFiles;
	}


		private String createMessageFile(Integer partNumber,String messageContent,
				String flightNo,String boardingAirport,String month,String day) throws Exception{

			SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			List<String> filenamelist = new ArrayList<String>();
			if (Integer.parseInt(day) <= 9)
				day = "0" + day;
			String generatedpnlfilepath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
					.getGenCalPath();
			String date = simpleDateFormat.format(new Date());
			String fileName=generatedpnlfilepath + "/" + flightNo + "-" + boardingAirport + "-" + month + "" + day + "-" + "CAL"
					+ "-" + date + "-" + "PART"+ partNumber + ".txt";
			
			File file = new File(fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(messageContent);
			bw.close();
			return fileName;
		}
		
		//TODO here airport msg id pax is pnl pax id
		private List<Integer> getAirportMsgPaxIds(List<PassengerInformation> passengerInformations){
			List<Integer> pnlPaxIds = new ArrayList<Integer>();
			for(PassengerInformation passengerInformation:passengerInformations){
				pnlPaxIds.add(passengerInformation.getPnlPaxId());
			}
			return pnlPaxIds;
		}
		
		private void updateCalStatusAndGroupId(Collection<PassengerInformation> passengerList, HashMap<String, String> groupPaxMap)
				throws ModuleException {

			if (passengerList == null || passengerList.size() == 0) {
				return;
			}
			Iterator<PassengerInformation> it = passengerList.iterator();
			Collection<Integer> pnrPaxIdsForCALSTAT = new ArrayList<Integer>();
			Collection<Integer> pnrSegIdsForCALSTAT = new ArrayList<Integer>();
			try {
				while (it.hasNext()) {

					PassengerInformation dto = it.next();

					if (dto.getPnrSegId() == -1) {
						continue;
					}
					pnrPaxIdsForCALSTAT.add(dto.getPnrPaxId());
					pnrSegIdsForCALSTAT.add(dto.getPnrSegId());
				}

				if (pnrPaxIdsForCALSTAT.size() == 0 || pnrSegIdsForCALSTAT.size() == 0) {
					log.error("######### ERROR PNR PAXIDS ADD || PNR SEG IDS ADD IS ZERO");
				} else {
					PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIdsForCALSTAT, pnrPaxIdsForCALSTAT,
							PNLConstants.PaxPnlAdlStates.ADL_ADDED_STAT, groupPaxMap, PNLConstants.MessageTypes.CAL);
				}

				log.debug("######### CAL STATUS UPDATED");
			} catch (CommonsDataAccessException e) {
				log.error("Error in updateCAL Status :", e);
				throw new ModuleException(e, e.getExceptionCode());

			}

		}
}
