/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.transfer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.TransferSeatBcCount;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.TransferSeatList;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airreservation.api.dto.AgentAlertInfoDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationTransferAlertDTO;
import com.isa.thinair.airreservation.api.dto.RollforwardResultsSummaryDTO;
import com.isa.thinair.airreservation.api.dto.TransferSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AllocateEnum;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageTransferChecker;
import com.isa.thinair.airreservation.core.bl.common.BundledServiceAdaptor;
import com.isa.thinair.airreservation.core.bl.common.CloneBO;
import com.isa.thinair.airreservation.core.bl.common.MealAdapter;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.SSRAdaptor;
import com.isa.thinair.airreservation.core.bl.common.SeatMapAdapter;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReprotectedPassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.util.AlertReservationsUtil;
import com.isa.thinair.airreservation.core.util.ReprotectedPassengerUtil;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.core.bl.audit.AuditParams;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for transfer reservation
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="transferSeats"
 */
public class TransferSeats extends DefaultBaseCommand {

	private Log log = LogFactory.getLog(getClass());

	/** Holds the date,time format */
	private static final String dateTimeFormat = "dd/MM/yyyy HH:mm:ss";
	/** Holds the segment DAO */
	private final ReservationSegmentDAO segmentDAO;
	/** Holds the alerting delegate */
	private final AlertingBD alertingBD;
	/** Holds the flight inventory reservation delegate */
	private final FlightInventoryResBD flightInventoryResBD;
	/** Holds the flight inventory delegate */
	private final FlightInventoryBD flightInventoryBD;
	/** Holds the flight delegate */
	private final FlightBD flightBD;
	/** Holds the credentials information */
	private CredentialsDTO credentialsDTO;
	/** Holds the alert template */
	private AlertTemplate alertTemplate;
	/** Holds the dateFormat object */
	private SimpleDateFormat simpleDateFormat = null;
	/** Holds the whether or not to send emails for flight alterations */
	private boolean sendEmailsForFlightAlterations;

	/** Holds the whether or not to send sms for flight alterations */
	private boolean sendSMSForFlightAlterations;

	/** Holds the whether or not to send sms for confirmed bookings */
	private boolean sendSMSForConfirmedBookings;

	/** Holds the whether or not to send sms for on hold bookings */
	private boolean sendSMSForOnHoldBookings;

	/** Holds the whether or not to send alerts for origin agents */
	private boolean sendEmailForOriginAgents;

	/** Holds the whether or not to send alerts for owner agents */
	private boolean sendEmailsForOwnerAgents;

	/** Flag to save data for IBE re protection */
	private String isIBEVisible;

	/** Holds the whether or not to send alerts for re protection in IBE */
	private boolean sendEmailsIBEVisible;

	/** Flag for transfer flight selected */
	private String isTransferSelectedFlights;

	/** Re protected flight segment IDs */
	private Set<Integer> reprotectFlightSegIds;

	Collection<ReservationTransferAlertDTO> colTranserAlertDetails = new ArrayList<ReservationTransferAlertDTO>();

	/** Holds the reservation DAO */
	private final ReservationDAO reservationDAO;

	/** Alerted pnr segment list */
	private List<Integer> pnrSegList;

	private final ReservationBD reservationBD;

	private static GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

	Collection<ReprotectedPaxDTO> reprotectedPassengers = new ArrayList<ReprotectedPaxDTO>();

	/** Holds the Reprotected Passenger DAO */
	private final ReprotectedPassengerDAO reprotectedPassengerDAO;

	/**
	 * constructor of the TransferSeats command
	 */
	public TransferSeats() {
		flightInventoryBD = ReservationModuleUtils.getFlightInventoryBD();
		flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();
		alertingBD = ReservationModuleUtils.getAlertingBD();
		flightBD = ReservationModuleUtils.getFlightBD();
		segmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		reservationBD = ReservationModuleUtils.getReservationBD();
		reprotectedPassengerDAO = ReservationDAOUtils.DAOInstance.REPROTECTED_PAX_DAO;
	}

	/**
	 * execute method of the TransferSeats command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {
		Collection transferSeatList = (Collection) this.getParameter(CommandParamNames.TRANSFER_PAX_DTO);
		Boolean isAlert = (Boolean) this.getParameter(CommandParamNames.IS_ALEART);
		AllocateEnum overAlloc = (AllocateEnum) this.getParameter(CommandParamNames.OVER_ALLOC_ENUM);
		Integer overAllocSeats = (Integer) this.getParameter(CommandParamNames.OVER_ALLOC_SEATS);
		Boolean isRollforward = (Boolean) this.getParameter(CommandParamNames.IS_ROLLFORWARD);
		credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);

		simpleDateFormat = new SimpleDateFormat(dateTimeFormat);

		// Holds the collection of TransferSeatDTO
		Collection successTransferList = new ArrayList();

		// Holds the collection of transfer summary object
		Collection<RollforwardResultsSummaryDTO> colTransferSummary = new ArrayList<RollforwardResultsSummaryDTO>();

		if (isRollforward == null) {
			isRollforward = new Boolean(false);
		}
		// set customer notification status
		this.setArguments(credentialsDTO, flightAlertDTO);
		this.checkParams(transferSeatList, isAlert, overAlloc, overAllocSeats);

		int noOfSeatTransfered = 0;

		Collection<Reservation> reservationList = getTransferredReservationDetails(transferSeatList, true);
		Collection<Reservation> clonedReservations = getTransferredReservationDetails(transferSeatList, false);
		Iterator itTransferSeatList = transferSeatList.iterator();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(true);
		
		int loopCount = 1;
				
		while (itTransferSeatList.hasNext()) {
						
			log.info("Reprotection loop count: " + loopCount +", Status=START");

			TransferSeatDTO transferSeatDTO = (TransferSeatDTO) itTransferSeatList.next();
			defaultServiceResponse = transferSeat(transferSeatDTO, isAlert, overAlloc, overAllocSeats, isRollforward);
			defaultServiceResponse.addResponceParam(CommandParamNames.TRANSFER_SEAT_MSG,
					defaultServiceResponse.getResponseCode());

			if (defaultServiceResponse.isSuccess()) {
								
				log.info("Reprotection loop count: " + loopCount +", Status=SUCCESS");
				
				if (defaultServiceResponse.getResponseCode().equals(ResponseCodes.TRANSFER_SEATS_SUCCESSFULL)) {

					successTransferList.add(transferSeatDTO);
					RollforwardResultsSummaryDTO transferSummary = (RollforwardResultsSummaryDTO) defaultServiceResponse
							.getResponseParam(ResponseCodes.TRANSFER_RESERVATION_SEGMENT_SUCCESSFULL);

					Integer tempNoOfSeatTransfered = (Integer) defaultServiceResponse
							.getResponseParam(ResponseCodes.TRANSFER_SEAT_NUMBER);
					if (tempNoOfSeatTransfered != null) {
						noOfSeatTransfered = noOfSeatTransfered + tempNoOfSeatTransfered.intValue();
					}

					if (transferSummary != null) {
						colTransferSummary.add(transferSummary);
					}

					// Store Reprotected Passengers
					Collection<ReprotectedPassenger> rePaxCollection = new ArrayList<ReprotectedPassenger>();
					if (reprotectedPassengers.size() != 0) {
						for (ReprotectedPaxDTO rePaxDto : reprotectedPassengers) {
							ReprotectedPassenger rePax = ReprotectedPassengerUtil.toReprotectedPassenger(rePaxDto);
							rePaxCollection.add(rePax);
						}
						reprotectedPassengerDAO.saveAllReprotectedPassengers(rePaxCollection);
					}
				}
			} else {
				log.info("Reprotection loop count: " + loopCount +", Status=NOT_SUCCESS");
				return defaultServiceResponse;
			}
			loopCount++;
		}

		defaultServiceResponse.addResponceParam(CommandParamNames.MODIFY_DETECTOR_RES_LIST_BEFORE_MODIFY, clonedReservations);

		if (isRollforward.booleanValue() && colTransferSummary.size() == 0) {
			defaultServiceResponse.setSuccess(false);
			return defaultServiceResponse;
		}

		defaultServiceResponse.setSuccess(true);
		defaultServiceResponse.setResponseCode(ResponseCodes.TRANSFER_SEATS_SUCCESSFULL);
		defaultServiceResponse.addResponceParam(ResponseCodes.TRANSFER_SEAT_NUMBER, new Integer(noOfSeatTransfered));

		/** Email and SMS alerts */
		generateNotificationsForPnrSegments();

		for (Reservation res : reservationList) {
			ADLNotifyer.recordReservation(res, AdlActions.D, res.getPassengers(), null);
		}

		return defaultServiceResponse;
	}

	private Collection<Reservation> getTransferredReservationDetails(Collection transferSeatList, boolean isTransactional)
			throws ModuleException {
		Integer fltSegId = null;
		Collection<String> pnrList = new ArrayList<String>();
		Iterator iterator = transferSeatList.iterator();
		while (iterator.hasNext()) {
			TransferSeatDTO transferSeat = (TransferSeatDTO) iterator.next();
			Iterator iter = transferSeat.getSourceFltSegSeatDists().iterator();
			while (iter.hasNext()) {
				TransferSeatList transferSeats = (TransferSeatList) iter.next();
				if (transferSeats.getPnrList() != null) {
					pnrList.addAll(transferSeats.getPnrList());
				} else {
					fltSegId = transferSeats.getSourceSegmentId();
				}
			}
		}

		LCCClientPnrModesDTO pnrModesDto;
		Collection<Reservation> reservations;
		if (pnrList.size() > 0) {
			reservations = new ArrayList<>();

			for (String pnr : pnrList) {
				pnrModesDto = new LCCClientPnrModesDTO();
				pnrModesDto.setPnr(pnr);
				pnrModesDto.setLoadFares(true);
				pnrModesDto.setLoadSegView(true);
				pnrModesDto.setLoadSSRInfo(true);

				if (isTransactional) {
					reservations.add(reservationBD.getReservation(pnrModesDto, null));
				} else {
					reservations.add(reservationBD.getReservationNonTnx(pnrModesDto, null));
				}
			}

			return reservations;
		} else {
			return reservationBD.getReservationsByFlightSegmentId(fltSegId, true);
		}
	}

	/**
	 * Method to transfer passengers from source flight segments to target flight segment
	 * 
	 * Standby bookings in the source flight are excluded from transfering
	 * 
	 * @param transferSeatDTO
	 * @param isAlert
	 * @param overAlloc
	 * @param overAllocSeats
	 * @param isRollforward
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private DefaultServiceResponse transferSeat(TransferSeatDTO transferSeatDTO, Boolean isAlert, AllocateEnum overAlloc,
			Integer overAllocSeats, Boolean isRollforward) throws ModuleException {

		if (log.isInfoEnabled()) {
			log.info("Parameters: ");
			log.info(transferSeatDTO);
			log.info("isAlert: " + isAlert);
			log.info("overAlloc: " + overAlloc);
			log.info("overAllocSeats: " + overAllocSeats);
			log.info("isRollforward: " + isRollforward);
		}

		Map<String, Reservation> alertingReservationsMap = new HashMap<String, Reservation>();
		Map<String, Reservation> gdsReservationsMap = new HashMap<String, Reservation>();

		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(true);

		Collection<ReservationSegmentDTO> oldResSegList = new ArrayList<ReservationSegmentDTO>();
		Collection<ReservationSegment> resSegList = new ArrayList<ReservationSegment>();
		Collection<TransferSeatList> sourceSeatDistList = new ArrayList<TransferSeatList>();
		Collection<FlightSegmentDTO> flightSegmentDetails = new ArrayList<FlightSegmentDTO>();
		Collection<ReservationAudit> resModificationDetails = new ArrayList<ReservationAudit>();
		Collection<Alert> resAlertDetails = new ArrayList<Alert>();
		Collection<TypeBRQ> typeBReqDetails = new ArrayList<TypeBRQ>();
		Collection<PaxMealTO> totalCanceledMeals = new ArrayList<PaxMealTO>();
		Collection<PaxSeatTO> totalCanceledSeats = new ArrayList<PaxSeatTO>();
		Collection<PaxBaggageTO> totalCanceledBaggages = new ArrayList<PaxBaggageTO>();
		this.pnrSegList = new ArrayList<Integer>();

		int targetPaxCount = 0;
		int countPax = 0;
		boolean canTransfer = false;
		boolean groupReservationsTransfered = false;
		List<Integer> availabilities;
		ReservationSegmentDTO oldResSegment;
		String logicalCCCode = transferSeatDTO.getLogicalCCCode();
		Collection<Integer> flightSegIds = new ArrayList<Integer>();
		Collection<Integer> cancelledFlightSegIds = new ArrayList<Integer>();

		String cabinClassCode = transferSeatDTO.getTargetCabinClassCode();

		if (transferSeatDTO.isDirectToConnection()) {
			for (TransferSeatDTO connectingTransferSeatDTO : transferSeatDTO.getConnectingSeatDTOs()) {
				flightSegIds.add(connectingTransferSeatDTO.getTargetFlightSegId());
			}
		} else {
			flightSegIds.add(transferSeatDTO.getTargetFlightSegId());
		}

		/*
		 * During rollforwad if the user has selected transfer all option, rest of the schedules all passengers need to
		 * get transferred. Not the number of passengers on the first schedule.
		 */
		if (isRollforward.booleanValue()) {
			Collection<FlightInventorySummaryDTO> invSummList = new ArrayList<FlightInventorySummaryDTO>();
			Collection<FCCSegInvSummaryDTO> segInv = new ArrayList<FCCSegInvSummaryDTO>();
			Collection<TransferSeatList> sourceSeatDist = transferSeatDTO.getSourceFltSegSeatDists();

			for (TransferSeatList seatList : sourceSeatDist) {
				if (seatList != null) {
					ArrayList<Integer> flightSegId = new ArrayList<Integer>();
					flightSegId.add(new Integer(seatList.getSourceFlightId()));
					if (log.isDebugEnabled()) {
						log.debug("Retrieving flight segment inventories summary for flight segment id : " + flightSegId);
					}

					invSummList = flightInventoryBD.getFlightSegmentInventoriesSummary(flightSegId);

					if (log.isDebugEnabled()) {
						log.debug("Retrieved inventories summary : " + invSummList);
					}

					for (FlightInventorySummaryDTO invSummaryDTO : invSummList) {
						segInv = invSummaryDTO.getSegmentInventories();
					}
				}
			}

			Collection<TransferSeatList> newSourceSeatDistList = new ArrayList<TransferSeatList>();
			for (TransferSeatList seatList : sourceSeatDist) {
				if (seatList.isTransferAll()) {
					for (FCCSegInvSummaryDTO invSegDTO : segInv) {
						if (invSegDTO != null && invSegDTO.getFlightSegmentId() == seatList.getSourceSegmentId()) {
							seatList.setTransferSeatCount(invSegDTO.getAdultSold() + invSegDTO.getAdultOnhold());
							break;
						}
					}
				}
				newSourceSeatDistList.add(seatList);
			}
			transferSeatDTO.setSourceFltSegSeatDists(newSourceSeatDistList);
		}
		// End Rollforward
		int minTargetPaxCount = Integer.MAX_VALUE;
		if (overAlloc.equals(AllocateEnum.AUTO_FIT)) {
			if (transferSeatDTO.isDirectToConnection()) {
				for (TransferSeatDTO connectingTransferSeatDTO : transferSeatDTO.getConnectingSeatDTOs()) {
					availabilities = flightInventoryResBD.getAvailableNonFixedAndFixedSeatsCounts(
							connectingTransferSeatDTO.getTargetFlightId(), connectingTransferSeatDTO.getTargetSegCode(),
							logicalCCCode);
					if (availabilities != null && !availabilities.isEmpty()) {
						targetPaxCount = availabilities.get(0) - availabilities.get(1);
						if (targetPaxCount < minTargetPaxCount) {
							minTargetPaxCount = targetPaxCount;
						}
					}
				}
				// getting the miminum target pax count
				targetPaxCount = minTargetPaxCount;
			} else {
				availabilities = flightInventoryResBD.getAvailableNonFixedAndFixedSeatsCounts(transferSeatDTO.getTargetFlightId(),
						transferSeatDTO.getTargetSegCode(), logicalCCCode);
				if (availabilities != null && !availabilities.isEmpty()) {
					targetPaxCount = availabilities.get(0) - availabilities.get(1);
				}
			}

		} else if (overAlloc.equals(AllocateEnum.OVER_ALL)) {
			if (overAllocSeats != null && overAllocSeats.intValue() > 0) {
				if (transferSeatDTO.isDirectToConnection()) {
					for (TransferSeatDTO connectingTransferSeatDTO : transferSeatDTO.getConnectingSeatDTOs()) {
						if (log.isDebugEnabled()) {
							log.debug("Retrieving available non-fixed and fixed seat " + "count for flightID : "
									+ connectingTransferSeatDTO.getTargetFlightId() + ", segmentCode : "
									+ connectingTransferSeatDTO.getTargetSegCode() + ", and cabinClass : " + cabinClassCode);
						}

						availabilities = flightInventoryResBD.getAvailableNonFixedAndFixedSeatsCounts(
								connectingTransferSeatDTO.getTargetFlightId(), connectingTransferSeatDTO.getTargetSegCode(),
								logicalCCCode);
						if (log.isDebugEnabled()) {
							log.debug("Retrieved available non-fixed and fixed seat availability: " + availabilities);
						}
						if (availabilities != null && !availabilities.isEmpty()) {
							targetPaxCount = availabilities.get(0) - availabilities.get(1);
							targetPaxCount += overAllocSeats.intValue();
							if (targetPaxCount < minTargetPaxCount) {
								minTargetPaxCount = targetPaxCount;
							}
						}
					}
					// getting the miminum target pax count
					targetPaxCount = minTargetPaxCount;
				} else {
					if (log.isDebugEnabled()) {
						log.debug("Retrieving available non-fixed and fixed seat " + "count for flightID : "
								+ transferSeatDTO.getTargetFlightId() + ", segmentCode : " + transferSeatDTO.getTargetSegCode()
								+ ", and cabinClass : " + cabinClassCode);
					}

					availabilities = flightInventoryResBD.getAvailableNonFixedAndFixedSeatsCounts(
							transferSeatDTO.getTargetFlightId(), transferSeatDTO.getTargetSegCode(), logicalCCCode);
					if (log.isDebugEnabled()) {
						log.debug("Retrieved available non-fixed and fixed seat availability: " + availabilities);
					}
					if (availabilities != null && !availabilities.isEmpty()) {
						targetPaxCount = availabilities.get(0) - availabilities.get(1);
						targetPaxCount += overAllocSeats.intValue();
					}
				}

			} else {
				targetPaxCount = transferSeatDTO.calculateTotalSeatCount();
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Target Pax Count " + targetPaxCount);
		}

		int interlinedPax = 0;
		int standByOpenReturnPax = 0;

		Collection<AdjustCreditBO> colAdjustCreditBO = new ArrayList<AdjustCreditBO>();
		Collection<TransferSeatList> sourceSeatDist = transferSeatDTO.getSourceFltSegSeatDists();
		Collection<Integer> flightIds = new ArrayList<Integer>();

		boolean isrTargetLinkedWithOriginalReservationCsCarriers = false;
		Collection<Integer> csCarrierIds = new ArrayList<Integer>();
		Integer targetFlightId = transferSeatDTO.getTargetFlightId();
		Set<CodeShareMCFlight> csFlights = flightBD.getFlight(targetFlightId).getCodeShareMCFlights();
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		for (CodeShareMCFlight flight : csFlights) {
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty() && !gdsStatusMap.values().isEmpty()) {
				for (GDSStatusTO gdsStatus : gdsStatusMap.values()) {
					if (gdsStatus.getCarrierCode().equals(flight.getCsMCCarrierCode())) {
						csCarrierIds.add(gdsStatus.getGdsId());
					}
				}
			}
		}

		Set<Integer> flightPublishedGdsIds = flightBD.getFlight(targetFlightId).getGdsIds();

		for (TransferSeatList seatList : sourceSeatDist) {
			int transferdSegmentCount = 0;
			int noOfInfants = 0;
			int sourceFlightSegmentId = seatList.getSourceSegmentId();
			int sourceSeatCount = seatList.getTransferSeatCount();
			flightIds.add(seatList.getSourceFlightId());
			cancelledFlightSegIds.add(sourceFlightSegmentId);
			if (log.isDebugEnabled()) {
				log.debug("Retrieving reservations for source flight segment id : " + sourceFlightSegmentId);
			}

			// Collection reservationList =
			// segmentDAO.getPnrsForSegment(sourceFlightSegmentId);
			Collection<Reservation> reservationList = segmentDAO.getReservationsForTransfer(sourceFlightSegmentId, false,
					transferSeatDTO.getLogicalCCCode(), seatList.getPnrList());

			Collection<Reservation> intelrineReservationList = segmentDAO.getReservationSegmentsForTransfer(sourceFlightSegmentId,
					true, seatList.getPnrList());

			if (log.isDebugEnabled()) {
				log.debug("Reservations retrieved : " + reservationList.size());
			}

			if (log.isDebugEnabled()) {
				log.debug("Updated reservation list size, after excluding interlined reservations : " + reservationList.size());
			}

			// check if there are any reservation or not.
			// if no reservation nothing should be done except go to the
			// next
			// transferseatDTO
			if (reservationList == null && intelrineReservationList == null
					|| reservationList.size() == 0 && intelrineReservationList.size() == 0) {
				if (log.isDebugEnabled()) {
					log.debug("No reservations exist going to source flight segment from next transferSeatDTO.");
				}
				continue;
			}

			for (Reservation reservation : intelrineReservationList) {
				boolean isGroupReservation = !BeanUtils.nullHandler(reservation.getOriginatorPnr()).isEmpty();
				int resCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
				int countInfant = reservation.getTotalPaxInfantCount();

				// handle transfer through lcc if the reservation is
				// interline
				if (isGroupReservation) {
					if (countPax + resCount <= targetPaxCount && transferdSegmentCount + resCount <= sourceSeatCount) {
						List<Reservation> interlinePnr = new ArrayList<Reservation>();
						interlinePnr.add(reservation);

						boolean handleInterlineTransfer = handleInterlineTransfer(interlinePnr, transferSeatDTO, seatList);

						if (handleInterlineTransfer) { // Successful
														// interline
														// transfer
							countPax = countPax + resCount;
							transferdSegmentCount = transferdSegmentCount + resCount;
							noOfInfants = noOfInfants + countInfant;
							groupReservationsTransfered = true;
							interlinedPax = interlinedPax + resCount;
							// To Save Reprotected Passenger Data
							addReprotectedPassengersToList(reservation, sourceFlightSegmentId,
									transferSeatDTO.getTargetFlightSegId());

							for (ReservationSegment rSegment : reservation.getSegments()) {

								Integer flightSegId = rSegment.getFlightSegId();
								if (flightSegId != null && flightSegId.equals(new Integer(sourceFlightSegmentId))
										&& rSegment.getStatus() != null
										&& rSegment.getStatus().equalsIgnoreCase(ReservationSegmentStatus.CONFIRMED)) {

									oldResSegment = new ReservationSegmentDTO();
									oldResSegment.setFlightSegId(flightSegId);
									oldResSegment.setPnrSegId(rSegment.getPnrSegId());
									oldResSegment.setPnr(reservation.getPnr());
									oldResSegment.setStatus(rSegment.getStatus());
										oldResSegment.setSubStatus(rSegment.getSubStatus());
									oldResSegment.setSegmentSeq(rSegment.getSegmentSeq());
									oldResSegment.setJourneySeq(rSegment.getJourneySequence());
									oldResSegList.add(oldResSegment);
								}
							}
						}

						// moving to next reservation for transfer
					} else {
						// All passengers in the group pnr cannot be
						// transfered . moving to next reservation
					}
				}

			}

			// This is to handle own reservation transfer after transfering
			// interline reservations in order to avoid DB
			// locking
			OUTER: for (Reservation reservation : reservationList) {
				Collection<ReservationSegmentDTO> segmentsView = ReservationProxy.getPnrSegmentsView(reservation.getPnr(), true);
				reservation.setSegmentsView(segmentsView);

				isrTargetLinkedWithOriginalReservationCsCarriers = false;
				if (reservation.getGdsId() == null) {
					isrTargetLinkedWithOriginalReservationCsCarriers = true;
				} else if (reservation.getGdsId() != null && csCarrierIds.contains(reservation.getGdsId())) {
					isrTargetLinkedWithOriginalReservationCsCarriers = ReservationApiUtils
							.isCodeShareReservation(reservation.getGdsId());
				} else if (reservation.getGdsId() != null && flightPublishedGdsIds != null
						&& flightPublishedGdsIds.contains(reservation.getGdsId())) {
					isrTargetLinkedWithOriginalReservationCsCarriers = true;
				}

				if (!isrTargetLinkedWithOriginalReservationCsCarriers) {
					continue OUTER;
				}

				if (AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()) {
					ReservationProxy.loadOndChargesView(reservation);
				}

				for (ReservationSegment rSegment : reservation.getSegments()) {
					Integer flightSegId = rSegment.getFlightSegId();
					if (flightSegId != null && flightSegId.equals(new Integer(sourceFlightSegmentId))
							&& rSegment.getBookingType().equalsIgnoreCase(BookingClass.BookingClassType.STANDBY)
							&& transferSeatDTO.isSkipStandbyBookings()) {
						continue OUTER;
					}
				}

				int resCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
				int countInfant = reservation.getTotalPaxInfantCount();

				ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation.getPnr(), false);
				AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);
				colAdjustCreditBO.add(adjustCreditBO);

				// Count the number of stand-by/open-return passengers.
				standByOpenReturnPax = standByOpenReturnPax + reservation.getPassengers().size();

				// adding reservations for alerting
				alertingReservationsMap.put(reservation.getPnr(), reservation);

				if (reservation.getGdsId() != null) {
					gdsReservationsMap.put(reservation.getPnr(), reservation);
				}

				Collection<ReservationSegment> colOpenRTSegments = new ArrayList<ReservationSegment>();

				for (Iterator<ReservationSegment> reservationSegmentIterator = reservation.getSegments()
						.iterator(); reservationSegmentIterator.hasNext();) {
					ReservationSegment reservationSegment = reservationSegmentIterator.next();
					// To initialize return group IDs for segments before removing open return segments from list
					reservationSegment.getReturnGrpId();

					if (sourceFlightSegmentId == reservationSegment.getFlightSegId()) {
						// Identifying the stand by bookings
						if (reservationSegment.getBookingType() != null
								&& reservationSegment.getBookingType().equalsIgnoreCase(BookingClass.BookingClassType.OPEN_RETURN)
								|| (!transferSeatDTO.getLogicalCCCode().equals(reservationSegment.getLogicalCCCode())
										&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL
												.equals(reservationSegment.getStatus()))) {
							reservationSegmentIterator.remove();
							// Considering open return segments separately to
							// update the Confirm Before Date
							if (reservationSegment.getBookingType() != null && reservationSegment.getBookingType()
									.equalsIgnoreCase(BookingClass.BookingClassType.OPEN_RETURN)) {
								colOpenRTSegments.add(reservationSegment);
							}
						}
					}

				}

				if (reservation.getSegments().size() == 0) {
					if (log.isDebugEnabled()) {
						log.debug("Zero segments left for seat transfer, moving to next reservation.");
					}
					continue;
				}
				// End excluding STANDBY reservations from reprotecting

				// If all the passengers in the PNR can be transfered
				if (countPax + resCount <= targetPaxCount && transferdSegmentCount + resCount <= sourceSeatCount) {
					canTransfer = true;

					// setting the onhold and confirmed infant count.
					if (countInfant > 0) {
						Set<ReservationPax> reservationPaxs = reservation.getPassengers();
						if (reservationPaxs != null) {
							for (ReservationPax pax : reservationPaxs) {
								if (pax.getPaxType().equals(PassengerType.INFANT)) {
									if (pax.getStatus().equals(ReservationStatus.CONFIRMED)) {
										seatList.setConfirmedInfantCount(seatList.getConfirmedInfantCount() + 1);
									} else {
										seatList.setOnHoldInfantCount(seatList.getOnHoldInfantCount() + 1);
									}
								}
							}
						}
					}

					for (ReservationSegment rSegment : reservation.getSegments()) {

						Integer flightSegId = rSegment.getFlightSegId();
						if (flightSegId != null && flightSegId.equals(new Integer(sourceFlightSegmentId))
								&& rSegment.getStatus() != null
								&& rSegment.getStatus().equalsIgnoreCase(ReservationSegmentStatus.CONFIRMED)) {

							BundledFareDTO bundledFareDTO = BundledServiceAdaptor.refundBundledServiceChargeIfApplicable(
									adjustCreditBO, reservation, rSegment.getPnrSegId(), credentialsDTO, chgTnxGen);

							oldResSegment = new ReservationSegmentDTO();
							oldResSegment.setFlightSegId(flightSegId);
							oldResSegment.setPnrSegId(rSegment.getPnrSegId());
							oldResSegment.setPnr(reservation.getPnr());
							oldResSegment.setStatus(rSegment.getStatus());
							oldResSegment.setSubStatus(rSegment.getSubStatus());
							oldResSegment.setSegmentSeq(rSegment.getSegmentSeq());
							oldResSegment.setJourneySeq(rSegment.getJourneySequence());
							oldResSegList.add(oldResSegment);
							SeatMapAdapter.populateSeatInformation(reservation, rSegment.getPnrSegId());
							// SeatMapAdapter.cancellSeatsWhenTransferringSegment(res,
							// rSegment.getPnrSegId(),
							// credentialsDTO);

							// JIRA ID AARESAA-1352
							// possible Source seatmap template.
							Integer sTemplate = SeatMapAdapter.getReservationSeatTemplate(flightSegId.intValue());
							Collection<PaxSeatTO> canceledSeats = new ArrayList<PaxSeatTO>();
							boolean seatMapCancelededReprotected = false;
							for (Integer targetFlightSegId : flightSegIds) {
								// possible Target seatmap template.
								Integer tTemplate = SeatMapAdapter.getReservationSeatTemplate(targetFlightSegId);
								if (tTemplate != null && sTemplate != null && tTemplate.intValue() == sTemplate.intValue()
										&& ((AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()
												&& !BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO,
														EXTERNAL_CHARGES.SEAT_MAP.toString()))
												|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {
									if (!seatMapCancelededReprotected) {
										canceledSeats = SeatMapAdapter.cancellUpdateSeatsWhenTransferringSegment(adjustCreditBO,
												reservation, rSegment.getPnrSegId(), credentialsDTO,
												new Integer(targetFlightSegId), credentialsDTO.getTrackInfoDTO(), chgTnxGen);
										seatMapCancelededReprotected = true;
									} else {
										SeatMapAdapter.updateSeatsWhenTransferringSegment(adjustCreditBO, reservation,
												rSegment.getPnrSegId(), credentialsDTO, new Integer(targetFlightSegId),
												credentialsDTO.getTrackInfoDTO());
									}
								} else if (!seatMapCancelededReprotected || BundledServiceAdaptor
										.isServiceIncludedInBundle(bundledFareDTO, EXTERNAL_CHARGES.SEAT_MAP.toString())) {
									canceledSeats = SeatMapAdapter.cancellSeatsWhenTransferringSegment(adjustCreditBO,
											reservation, rSegment.getPnrSegId(), credentialsDTO, credentialsDTO.getTrackInfoDTO(),
											chgTnxGen);
									seatMapCancelededReprotected = true;
								}
							}

							// if there's one or more seats that could not
							// be re-protected indicate in the t_pnr_segment
							if (canceledSeats.size() > 0) {
								fillPaxNamesForSeats(canceledSeats, reservation.getPassengers());
								totalCanceledSeats.addAll(canceledSeats);
								rSegment.setSeatReprotectStatus(1);
							}

							// Adding the Meal Right after SeatMap
							MealAdapter.populateMealInformation(reservation, rSegment.getPnrSegId());

							Integer sMTemplate = MealAdapter.getReservationMealTemplate(flightSegId.intValue());

							Collection<PaxMealTO> canceledMeals = new ArrayList<PaxMealTO>();
							boolean mealsCancelededReprotected = false;
							for (Integer targetFlightSegId : flightSegIds) {
								Integer tMTemplate = MealAdapter.getReservationMealTemplate(targetFlightSegId);

								if (tMTemplate != null && sMTemplate != null
										&& ((AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()
												&& !BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO,
														EXTERNAL_CHARGES.MEAL.toString()))
												|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {
									if (!mealsCancelededReprotected) {
										canceledMeals = MealAdapter.cancellUpdateMealsWhenTransferringSegment(adjustCreditBO,
												reservation, rSegment.getPnrSegId(), credentialsDTO,
												new Integer(targetFlightSegId), chgTnxGen);
										mealsCancelededReprotected = true;
									} else {
										MealAdapter.updateMealsWhenTransferringSegment(adjustCreditBO, reservation,
												rSegment.getPnrSegId(), credentialsDTO, new Integer(targetFlightSegId));
									}
								} else if (!mealsCancelededReprotected || BundledServiceAdaptor
										.isServiceIncludedInBundle(bundledFareDTO, EXTERNAL_CHARGES.MEAL.toString())) {
									canceledMeals = MealAdapter.cancellMealWhenTransferringSegment(adjustCreditBO, reservation,
											rSegment.getPnrSegId(), credentialsDTO, chgTnxGen);
									mealsCancelededReprotected = true;
								}
							}

							// if there's one or more meals that could not
							// be re-protected indicate in the t_pnr_segment
							if (canceledMeals.size() > 0) {
								fillPaxNamesForMeals(canceledMeals, reservation.getPassengers());
								totalCanceledMeals.addAll(canceledMeals);
								rSegment.setMealReprotectStatus(1);
							}

							// reprotect baggages
							// FIXME Nili Oct 15, 2012.
							// I had a chat with ishan. The flightSegIds behaviour is buggy where it won't perform well
							// for the 2nd iteration onwards.
							// Infact connections it's not needed to handle through the transfer seats.
							// Ishan Please clean this and preserve the old behaviour.
							Integer tgtFlightSegId = (Integer) BeanUtils.getLastElement(flightSegIds);
							BaggageAdaptor.populateBaggageInformation(reservation, rSegment.getPnrSegId());
							Collection<PaxBaggageTO> canceledBaggages = new ArrayList<PaxBaggageTO>();

							BaggageTransferChecker baggageTransferChecker = new BaggageTransferChecker(flightSegId,
									rSegment.getPnrSegId(), reservation);
							boolean isSourceAndTargetBaggageTemplateMatches = baggageTransferChecker
									.isSrcAndTargetBaggageTemplateMatches(tgtFlightSegId);

							if ((isSourceAndTargetBaggageTemplateMatches || AppSysParamsUtil.isONDBaggaeEnabled())
									&& ((AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()
											&& !BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO,
													EXTERNAL_CHARGES.BAGGAGE.toString()))
											|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {
								canceledBaggages = BaggageAdaptor.cancelUpdateBaggagesWhenTransferringSegment(adjustCreditBO,
										reservation, rSegment.getPnrSegId(), credentialsDTO, new Integer(tgtFlightSegId),
										chgTnxGen);
							} else {
								canceledBaggages = BaggageAdaptor.cancelBaggageWhenTransferringSegment(adjustCreditBO,
										reservation, rSegment.getPnrSegId(), credentialsDTO, chgTnxGen);
							}

							// if there's one or more baggages that could
							// not be re-protected indicate in the
							// t_pnr_segment
							// TODO add reservation audits and alerts for
							// canceled baggages
							if (canceledBaggages.size() > 0) {
								fillPaxNamesForBaggages(canceledBaggages, reservation.getPassengers());
								totalCanceledBaggages.addAll(canceledBaggages);
								rSegment.setBaggageReprotectStatus(1);
							}

							// End reprotect baggages

							// Load In-flight Services & Airport Service
							SSRAdaptor.populateSSRInfo(reservation, rSegment.getPnrSegId());

							// Re-protect In-flight services
							boolean ssrCancelededReprotected = false;
							for (Integer targetFlightSegId : flightSegIds) {
								if (!ssrCancelededReprotected) {
									SSRAdaptor.reprotectInflightServices(adjustCreditBO, reservation, flightSegId,
											targetFlightSegId, rSegment.getPnrSegId(), credentialsDTO, chgTnxGen);
									ssrCancelededReprotected = true;
								} else {
									SSRAdaptor.reprotectInflightServicesWithoutCancelling(adjustCreditBO, reservation,
											flightSegId, targetFlightSegId, rSegment.getPnrSegId(), credentialsDTO);
								}
							}
							// Re-protect Airport services
							boolean aasCancelededReprotected = false;
							for (Integer targetFlightSegId : flightSegIds) {
								if (!aasCancelededReprotected
										|| ((AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption()
												&& BundledServiceAdaptor.isServiceIncludedInBundle(bundledFareDTO,
														EXTERNAL_CHARGES.AIRPORT_SERVICE.toString()))
												|| !AppSysParamsUtil.isBundledServiceFeeRefundableForFlightDisruption())) {
									SSRAdaptor.reprotectAirportServices(adjustCreditBO, reservation, rSegment.getPnrSegId(),
											credentialsDTO, targetFlightSegId, false, bundledFareDTO, chgTnxGen);
									aasCancelededReprotected = true;
								} else {
									SSRAdaptor.reprotectAirportServicesWithoutCancelling(adjustCreditBO, reservation,
											rSegment.getPnrSegId(), credentialsDTO, targetFlightSegId);
								}
							}

							for (ReservationSegment resSeg : reservation.getSegments()) {
								if (resSeg.getPnrSegId().intValue() == rSegment.getPnrSegId()) {
									rSegment = resSeg;
								}
							}
							// ------------------------------------------------------------------------/
							// only we need to set this for direct to direct
							// flight transfering
							if (!transferSeatDTO.isDirectToConnection()) {
								rSegment.setFlightSegId(transferSeatDTO.getTargetFlightSegId());
								// Re-calculate Open return confirm before date
								// for the new flight
								reCalculateOpenReturnConfirmBeforeDate(rSegment, colOpenRTSegments,
										transferSeatDTO.getTargetFlightSegId(), reservation);
							}

							if (isAlert.booleanValue()) {
								rSegment.setAlertFlag(1);
							}
							resSegList.add(rSegment);
							// Record ADL details to send in new flight

							if (!flightBD.areSameDaySameFlights(rSegment.getFlightSegId(), sourceFlightSegmentId)) {
								ADLNotifyer.recordAddedSegmentsForAdl(rSegment);
							}

							Map<String, TransferSeatBcCount> bcMap = segmentDAO
									.getBCCodesWithAvailability(rSegment.getPnrSegId().intValue(), false);

							if (bcMap != null && bcMap.size() > 0) {

								for (String bc : bcMap.keySet()) {
									TransferSeatBcCount trBcCount = bcMap.get(bc);
									if (bc == null || bc.equals("")) {
										throw new ModuleException("Booking Code Not found in data");
									}
									seatList.addBCCount(bc, trBcCount);
								}
							}
						}
					}

					resSegList.addAll(colOpenRTSegments);
					countPax = countPax + resCount;
					transferdSegmentCount = transferdSegmentCount + resCount;
					noOfInfants = noOfInfants + countInfant;

					// To Save Reprotected Passenger Data
					addReprotectedPassengersToList(reservation, sourceFlightSegmentId, transferSeatDTO.getTargetFlightSegId());

				} else {
					// All passengers in the pnr cannot be transfered .
					// moving to next reservation
				}

				if (countPax == targetPaxCount || transferdSegmentCount == sourceSeatCount) {
					break;
				}
			}

			if (canTransfer) {
				seatList.setTransferSeatCount(transferdSegmentCount);
				sourceSeatDistList.add(seatList);
			}

			if (countPax == targetPaxCount) {
				break;
			}
		}

		Integer noOfTransferedPax = Integer.valueOf(countPax);

		if (canTransfer) {
			transferSeatDTO.setSourceFltSegSeatDists(sourceSeatDistList);

			// Flight segment details of source and target flights
			flightSegmentDetails = getFlightSegmentDetails(sourceSeatDistList, flightSegIds);

			RollforwardResultsSummaryDTO transferSummary = getTransferedSummary(sourceSeatDistList, flightSegIds,
					flightSegmentDetails, oldResSegList);
			if (log.isDebugEnabled()) {
				log.debug("\n\r---------------- Reprotect --------------\n\r"
						+ "\n\r---------------- Source Flight Segment Distribution Details     --------------\n\r" + "\n\r "
						+ transferSummary.getSegmentDistributionSummary() + "\n\r " + transferSummary.getReservationSummary()
						+ "\n\r---------------- End Source Flight Segment Distribution Details --------------\n\r");
			}

			// Update Inventory information
			Map<Integer, String> newBookingClassCodes = null;
			try {
				newBookingClassCodes = flightInventoryResBD.transferSeats(transferSeatDTO);
				log.debug("updated flight inventory information");
			} catch (ModuleException e) {

				log.error("update of flight inventory information failed" + e.getMessage());
				defaultServiceResponse.setSuccess(false);

				HashMap<String, TransferSeatDTO> errorMap = new HashMap<String, TransferSeatDTO>();
				errorMap.put(e.getExceptionCode(), transferSeatDTO);

				defaultServiceResponse.addResponceParam(ResponseCodes.TRANSFER_SEATS_UNSUCCESSFULL, errorMap);
				return defaultServiceResponse;
			}

			if (transferSeatDTO.getConnectingSeatDTOs() != null && transferSeatDTO.getConnectingSeatDTOs().size() > 0) {
				ReservationSegment resSegment = null;
				for (Object rSeg : resSegList) {
					ReservationSegment oldReservationSegment = null;
					resSegment = (ReservationSegment) rSeg;
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(resSegment.getReservation().getPnr());
					pnrModesDTO.setLoadFares(true);
					pnrModesDTO.setLoadSegView(true);
					// Return the reservation
					Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
					Set<ReservationSegment> reservationSegments = new HashSet<ReservationSegment>();
					Set<ReservationSegment> setSegements = reservation.getSegments();
					if (setSegements == null || setSegements.size() == 0) {
						// No reservation segments to transfer
						continue;
					}

					reservationSegments.addAll(setSegements);
					for (ReservationSegment reservationSegment : reservationSegments) {
						if (reservationSegment.getPnrSegId().intValue() == resSegment.getPnrSegId().intValue()) {
							oldReservationSegment = reservationSegment;
							int maxOndGroupId = ReservationCoreUtils.getMaxOndGroupId(reservationSegments);
							int returnOndGroupId = ReservationCoreUtils.getMaxReturnOndGroupId(reservationSegments);
							int maxSegmentSeq = ReservationCoreUtils.getMaxSegmentSequence(reservationSegments);

							boolean firstSegmentSaved = false;
							for (TransferSeatDTO connectingTransferSeatDTO : transferSeatDTO.getConnectingSeatDTOs()) {
								ReservationSegment newReservationSegment = new ReservationSegment();
								newReservationSegment = (ReservationSegment) reservationSegment.clone();
								if (!firstSegmentSaved) {
									newReservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
									reservationSegment.setFlightSegId(connectingTransferSeatDTO.getTargetFlightSegId());

									newReservationSegment.setOndGroupId(new Integer(maxOndGroupId) + 1);
									newReservationSegment.setSegmentSeq(maxSegmentSeq + 1);
									newReservationSegment.setReturnOndGroupId(returnOndGroupId + 1);
									firstSegmentSaved = true;
								} else {
									newReservationSegment
											.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
									newReservationSegment.setFlightSegId(connectingTransferSeatDTO.getTargetFlightSegId());
									newReservationSegment.setOndGroupId(new Integer(maxOndGroupId));
									newReservationSegment.setSegmentSeq(maxSegmentSeq);
									newReservationSegment.setReturnOndGroupId(returnOndGroupId);
								}

								newReservationSegment.setAlertFlag(0);
								newReservationSegment.setSeatReprotectStatus(resSegment.getSeatReprotectStatus());
								newReservationSegment.setMealReprotectStatus(resSegment.getMealReprotectStatus());
								newReservationSegment.setBaggageReprotectStatus(resSegment.getBaggageReprotectStatus());
								newReservationSegment.setOpenReturnConfirmBeforeZulu(resSegment.getOpenReturnConfirmBeforeZulu());
								reservation.addSegment(newReservationSegment);

								Collection<ReservationPaxFareSegment> colReservationReservationPaxFareSegment;
								Map<Integer, ReservationPaxFare> newReservationPaxFares = new HashMap<Integer, ReservationPaxFare>();

								for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
									boolean paxFaresSaved = false;
									ReservationPax reservationPax = reservationPax2;

									for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
										ReservationPaxFare oldReservationPaxFare = reservationPaxFare;
										// By pass the newly added pax fares
										if (oldReservationPaxFare.getPnrPaxFareId() == null) {
											continue;
										}
										ReservationPaxFare newReservationPaxFare = (ReservationPaxFare) oldReservationPaxFare
												.clone();

										colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
										for (ReservationPaxFareSegment reservationPaxFareSegment2 : oldReservationPaxFare
												.getPaxFareSegments()) {
											ReservationPaxFareSegment reservationPaxFareSegment = reservationPaxFareSegment2;

											if (reservationPaxFareSegment.getSegment() != null) {
												ReservationPaxFareSegment newReservationPaxFareSegment = (ReservationPaxFareSegment) reservationPaxFareSegment
														.clone();
												// This means booking class has
												// been changed during transfer
												if (newBookingClassCodes != null && newBookingClassCodes
														.get(connectingTransferSeatDTO.getTargetFlightSegId()) != null) {
													reservationPaxFareSegment.setBookingCode(newBookingClassCodes
															.get(connectingTransferSeatDTO.getTargetFlightSegId()));
												}
												newReservationPaxFareSegment.setSegment(newReservationSegment);
												colReservationReservationPaxFareSegment.add(newReservationPaxFareSegment);

											}
										}

										if (colReservationReservationPaxFareSegment.size() > 0) {
											ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
													.getOndFareFromOndCharges(oldReservationPaxFare.getCharges());
											if (reservationPaxOndCharge != null) {
												if (!paxFaresSaved) {
													ReservationPaxOndCharge newReservationPaxOndDRCharge = reservationPaxOndCharge
															.cloneForNew(credentialsDTO, false);
													newReservationPaxOndDRCharge
															.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
													newReservationPaxOndDRCharge
															.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());
													newReservationPaxOndDRCharge
															.setDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
													newReservationPaxOndDRCharge.setDiscountValuePercentage(new Integer(0));
													newReservationPaxFare.addCharge(newReservationPaxOndDRCharge);
													newReservationPaxFare.setTotalFare(new BigDecimal(0));
													newReservationPaxFare.setTotalTaxCharge(new BigDecimal(0));
													newReservationPaxFare.setTotalSurCharge(new BigDecimal(0));
													newReservationPaxFare.setTotalCancelCharge(new BigDecimal(0));
													newReservationPaxFare.setTotalModificationCharge(new BigDecimal(0));
													newReservationPaxFare.setTotalAdjustmentCharge(new BigDecimal(0));
													newReservationPaxFare.setTotalDiscount(new BigDecimal(0));

													for (ReservationPaxFareSegment reservationPaxFareSegment : colReservationReservationPaxFareSegment) {
														newReservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
													}
													newReservationPaxFares.put(reservationPax.getPnrPaxId(),
															newReservationPaxFare);
													paxFaresSaved = true;
												} else {
													for (ReservationPaxFareSegment reservationPaxFareSegment : colReservationReservationPaxFareSegment) {
														oldReservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
													}
													newReservationPaxFares.put(reservationPax.getPnrPaxId(),
															oldReservationPaxFare);
												}
											}
										}
									}
								}

								for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
									ReservationPax reservationPax = reservationPax2;
									for (Integer pnrPaxId : newReservationPaxFares.keySet()) {
										if (pnrPaxId.intValue() == reservationPax.getPnrPaxId().intValue()) {
											reservationPax.addPnrPaxFare(newReservationPaxFares.get(pnrPaxId));
										}
									}
								}
							}

						}
					}

					reservation = reservationDAO.saveReservation(reservation);
					// generate new tickets,
					List<ReservationSegment> newResSegments = new ArrayList<ReservationSegment>();
					for (ReservationSegment resSeg : reservation.getSegments()) {
						if (resSeg.getOndGroupId().intValue() == oldReservationSegment.getOndGroupId().intValue()
								&& resSeg.getPnrSegId().intValue() != oldReservationSegment.getPnrSegId().intValue()) {
							// Get the newly added segment
							newResSegments.add(resSeg);
						}
					}

					CreateTicketInfoDTO ticketingInfoDto = TicketingUtils
							.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
					ETicketBO.updateETickets(ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation,
							reservation.getPassengers(), ticketingInfoDto), reservation.getPassengers(), reservation, false);
				}

			}
			// Update reservation segment booking classes if they changed
			else if (transferSeatDTO.isCabinClassChanged() && newBookingClassCodes != null && newBookingClassCodes.size() > 0) {

				ReservationSegment resSegment = null;
				for (Object rSeg : resSegList) {
					resSegment = (ReservationSegment) rSeg;
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(resSegment.getReservation().getPnr());
					pnrModesDTO.setLoadFares(true);
					pnrModesDTO.setLoadSegView(true);
					// Return the reservation
					Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
					Set<ReservationSegment> reservationSegments = new HashSet<ReservationSegment>();
					Set<ReservationSegment> setSegements = reservation.getSegments();
					if (setSegements == null || setSegements.size() == 0) {
						// No reservation segments to transfer
						continue;
					}

					reservationSegments.addAll(setSegements);
					for (ReservationSegment reservationSegment : reservationSegments) {
						if (reservationSegment.getPnrSegId().intValue() == resSegment.getPnrSegId().intValue()) {
							// what we do here is cancel the existing
							// segment and add new one with new details as
							// in transfer reservation segment

							int maxOndGroupId = ReservationCoreUtils.getMaxOndGroupId(reservationSegments) + 1;
							int returnOndGroupId = ReservationCoreUtils.getMaxReturnOndGroupId(reservationSegments) + 1;
							int maxSegmentSeq = ReservationCoreUtils.getMaxSegmentSequence(reservationSegments);
							ReservationSegment newReservationSegment = new ReservationSegment();
							newReservationSegment = (ReservationSegment) reservationSegment.clone();
							newReservationSegment.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CANCEL);
							newReservationSegment.setAlertFlag(0);
							newReservationSegment.setOndGroupId(new Integer(maxOndGroupId));
							newReservationSegment.setSegmentSeq(maxSegmentSeq);
							newReservationSegment.setReturnOndGroupId(returnOndGroupId);
							newReservationSegment.setSeatReprotectStatus(resSegment.getSeatReprotectStatus());
							newReservationSegment.setMealReprotectStatus(resSegment.getMealReprotectStatus());
							newReservationSegment.setBaggageReprotectStatus(resSegment.getBaggageReprotectStatus());
							newReservationSegment.setOpenReturnConfirmBeforeZulu(resSegment.getOpenReturnConfirmBeforeZulu());
							reservation.addSegment(newReservationSegment);

							reservationSegment.setFlightSegId(new Integer(transferSeatDTO.getTargetFlightSegId()));

							Collection<ReservationPaxFareSegment> colReservationReservationPaxFareSegment;
							Map<Integer, ReservationPaxFare> newReservationPaxFares = new HashMap<Integer, ReservationPaxFare>();

							for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
								ReservationPax reservationPax = reservationPax2;

								for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
									ReservationPaxFare oldReservationPaxFare = reservationPaxFare;
									ReservationPaxFare newReservationPaxFare = (ReservationPaxFare) oldReservationPaxFare.clone();

									colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
									for (ReservationPaxFareSegment reservationPaxFareSegment2 : oldReservationPaxFare
											.getPaxFareSegments()) {
										ReservationPaxFareSegment reservationPaxFareSegment = reservationPaxFareSegment2;

										if (reservationPaxFareSegment.getSegment() != null) {
											ReservationPaxFareSegment newReservationPaxFareSegment = (ReservationPaxFareSegment) reservationPaxFareSegment
													.clone();
											reservationPaxFareSegment.setBookingCode(
													newBookingClassCodes.get(transferSeatDTO.getTargetFlightSegId()));
											newReservationPaxFareSegment.setSegment(newReservationSegment);
											colReservationReservationPaxFareSegment.add(newReservationPaxFareSegment);

										}
									}

									if (colReservationReservationPaxFareSegment.size() > 0) {
										ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils
												.getOndFareFromOndCharges(oldReservationPaxFare.getCharges());
										if (reservationPaxOndCharge != null) {
											ReservationPaxOndCharge newReservationPaxOndDRCharge = reservationPaxOndCharge
													.cloneForNew(credentialsDTO, false);
											newReservationPaxOndDRCharge
													.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
											newReservationPaxOndDRCharge
													.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());
											newReservationPaxFare.addCharge(newReservationPaxOndDRCharge);
											newReservationPaxFare.setTotalFare(new BigDecimal(0));
											newReservationPaxFare.setTotalTaxCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalSurCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalCancelCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalModificationCharge(new BigDecimal(0));
											newReservationPaxFare.setTotalAdjustmentCharge(new BigDecimal(0));

											for (ReservationPaxFareSegment reservationPaxFareSegment : colReservationReservationPaxFareSegment) {
												newReservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
											}
											newReservationPaxFares.put(reservationPax.getPnrPaxId(), newReservationPaxFare);
										}
									}
								}
							}

							for (ReservationPax reservationPax2 : reservationSegment.getReservation().getPassengers()) {
								ReservationPax reservationPax = reservationPax2;
								for (Integer pnrPaxId : newReservationPaxFares.keySet()) {
									if (pnrPaxId.intValue() == reservationPax.getPnrPaxId().intValue()) {
										reservationPax.addPnrPaxFare(newReservationPaxFares.get(pnrPaxId));
									}
								}
							}
						}
					}
					reservationDAO.saveReservation(reservation);
				}

			} else {
				// Update Reservation Segment information
				segmentDAO.saveReservationSegments(resSegList);
			}

			if (colAdjustCreditBO != null && colAdjustCreditBO.size() > 0) {
				for (AdjustCreditBO adjustCreditBO : colAdjustCreditBO) {
					adjustCreditBO.callRevenueAccountingAfterReservationSave();
				}
			}
			// TODO change audint logic when its involving connections
			// Insert Modification information for each reservation
			resModificationDetails = getModificationDetails(flightSegIds, flightSegmentDetails, oldResSegList, totalCanceledMeals,
					totalCanceledSeats);
			cancelBookedAirportTransfers(new ArrayList<Integer>(cancelledFlightSegIds));
			Collection<FlightSegmentDTO> sourceFlightDetails = getSourceFlightDetails(flightIds, flightSegmentDetails);
			try {
				// Get the auditor deleagate
				// Added by Nili 11:20 AM 3/23/2006
				AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
				LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();
				contents.put(AuditParams.FLIGHT_ID, transferSeatDTO.getTargetFlightId());
				contents.put(AuditParams.LEG_DETAILS, transferSeatDTO.getTargetSegCode());
				for (FlightSegmentDTO flightSeg : sourceFlightDetails) {
					contents.put(AuditParams.FLIGHT_ID, flightSeg.getFlightId());
					contents.put(AuditParams.SOURCE_FLIGHT_NO, flightSeg.getFlightNumber());
					contents.put(AuditParams.SOURCE_FLIGHT_DEPT_TIME, flightSeg.getDepartureDateTimeZulu());
				}

				/*
				 * Collection<TransferSeatList> transferSeats = transferSeatDTO.getSourceFltSegSeatDists(); for
				 * (TransferSeatList seatDto : transferSeats) { contents.put(AuditParams.FLIGHT_ID + " _" +
				 * seatDto.getSourceFlightId(), seatDto.getTransferSeatCount());
				 * 
				 * }
				 */
				for (ReservationAudit reservationAudit : resModificationDetails) {
					reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
					// Saves the reservation audit
					auditorBD.audit(reservationAudit, reservationAudit.getContentMap());

				}
				log.debug("made reservation modification");

				Audit audit = new Audit();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_REPROTECT_FLIGHTS));
				audit.setAppCode("airadmin");
				if (credentialsDTO.getUserId() != null)
					audit.setUserId(credentialsDTO.getUserId());
				else
					audit.setUserId("");

				audit.setTimestamp(new Date());

				// contents.put(AuditParams.FLIGHT_ID, );
				// contents.put(AuditParams.FLIGHT_NO, flight.getFlightNumber());
				// contents.put(AuditParams.COPIED_TO_DATE, dateFormat.format(flight.getDepartureDate()));
				auditorBD.audit(audit, contents);

				// add record to the source reprotect flight

			} catch (Exception ex) {
				log.error("reservation modification failed" + ex.getMessage());
				throw new ModuleException("airreservation.arg.invalidResModification");
			}

			Date oldDepartureDate = new Date();

			// Generate alert for each reservation - if alert is checked
			if (isAlert.booleanValue()) {
				Collection<ReservationSegmentDTO> filteredOldResSegList = filterOldResSegListForDryInterline(oldResSegList);
				resAlertDetails = getAlertDetails(flightSegIds, flightSegmentDetails, filteredOldResSegList, totalCanceledMeals,
						totalCanceledSeats);

				if (flightSegmentDetails != null && flightSegmentDetails.size() > 0) {
					// Object obj[] = flightSegmentDetails.toArray();
					// FlightSegmentDTO flightSegmentDTO1 = (FlightSegmentDTO) obj[0];
					// Date oldDepDate1 = flightSegmentDTO1.getDepartureDateTime();
					//
					// FlightSegmentDTO flightSegmentDTO2 = (FlightSegmentDTO) obj[1];
					// Date oldDepDate2 = flightSegmentDTO2.getDepartureDateTime();
					//
					// if (flightSegmentDTO1.getSegmentId().intValue() != targetFlightSegId) {
					// oldDepartureDate = new Date(oldDepDate1.getTime());
					// } else {
					// oldDepartureDate = new Date(oldDepDate2.getTime());
					// }

					for (FlightSegmentDTO flightSegDTO : flightSegmentDetails) {
						if (!flightSegIds.contains(flightSegDTO.getSegmentId().intValue())) {
							oldDepartureDate = new Date(flightSegDTO.getDepartureDateTime().getTime());
						}
					}

				}

				for (Alert alert : resAlertDetails) {
					alert.setOriginalDepDate(oldDepartureDate);
				}

				try {
					alertingBD.addAlerts(resAlertDetails);
					log.debug("alerts sent");
				} catch (Exception ex) {
					log.error("generating alerts failed" + ex.getMessage());
					throw new ModuleException("airreservation.arg.invalidResModification");
				}
			}

			defaultServiceResponse.setResponseCode(ResponseCodes.TRANSFER_SEATS_SUCCESSFULL);
			defaultServiceResponse.addResponceParam(ResponseCodes.TRANSFER_RESERVATION_SEGMENT_SUCCESSFULL, transferSummary);
			defaultServiceResponse.addResponceParam(ResponseCodes.TRANSFER_SEAT_NUMBER, noOfTransferedPax);
			defaultServiceResponse.addResponceParam(ResponseCodes.FLIGHT_REPROTECT_CANCELED_SEATS, totalCanceledSeats);
			defaultServiceResponse.addResponceParam(ResponseCodes.FLIGHT_REPROTECT_CANCELED_MEALS, totalCanceledMeals);
			/** set Details to send alerts for customers **/
			if (sendEmailsForFlightAlterations || sendSMSForFlightAlterations || sendEmailForOriginAgents
					|| sendEmailsForOwnerAgents || sendEmailsIBEVisible) {
				if (log.isDebugEnabled()) {
					log.debug("Collecting information to send customer alerts");
				}
				for (Integer targetFlightSegId : flightSegIds) {
					ReservationTransferAlertDTO reservationTransferAlertDTO = new ReservationTransferAlertDTO();
					reservationTransferAlertDTO.setAlertingReservations(alertingReservationsMap.values());
					reservationTransferAlertDTO.setSourceSegment(null); // TODO
					reservationTransferAlertDTO.setTargetSegment(null);
					reservationTransferAlertDTO.setFlightReservationAlertDTO(
							getFlightAlertFiller(new Integer(targetFlightSegId), flightSegmentDetails, oldResSegList));
					this.colTranserAlertDetails.add(reservationTransferAlertDTO);
				}
			}

			if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
				if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
						&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
						&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
					sendTypeBRequests(gdsReservationsMap, flightSegIds, flightSegmentDetails, oldResSegList);
				}
			}
			if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
				sendCSTypeBRequests(alertingReservationsMap, flightSegIds, flightSegmentDetails, oldResSegList);
			}

		} else {
			// If it is rollforward defaultServiceResponse should not be
			// setted to false.
			if (!isRollforward.booleanValue()) {

				if (standByOpenReturnPax + interlinedPax == targetPaxCount) {
					defaultServiceResponse.setSuccess(true);
					defaultServiceResponse.setResponseCode(ResponseCodes.TRANSFER_SEATS_SUCCESSFULL);
					defaultServiceResponse.addResponceParam(ResponseCodes.TRANSFER_SEAT_NUMBER, noOfTransferedPax);
				} else {
					defaultServiceResponse.setSuccess(false);
				}
			}
			flightSegmentDetails = getFlightSegmentDetails(sourceSeatDistList, flightSegIds);
			// Only group prns transfered in the process. No Own reservation
			// is transfered. Returning the success.
			if (groupReservationsTransfered && noOfTransferedPax == targetPaxCount) {
				defaultServiceResponse.setResponseCode(ResponseCodes.TRANSFER_SEATS_SUCCESSFULL);
				defaultServiceResponse.addResponceParam(ResponseCodes.TRANSFER_SEAT_NUMBER, noOfTransferedPax);
				defaultServiceResponse.setSuccess(true);
				/** set Details to send alerts for customers **/
				if (sendEmailsForFlightAlterations || sendSMSForFlightAlterations || sendEmailForOriginAgents
						|| sendEmailsForOwnerAgents || sendEmailsIBEVisible) {
					if (log.isDebugEnabled()) {
						log.debug("Collecting information to send customer alerts");
					}
					for (Integer targetFlightSegId : flightSegIds) {
						ReservationTransferAlertDTO reservationTransferAlertDTO = new ReservationTransferAlertDTO();
						reservationTransferAlertDTO.setAlertingReservations(alertingReservationsMap.values());
						reservationTransferAlertDTO.setSourceSegment(null); // TODO
						reservationTransferAlertDTO.setTargetSegment(null);
						reservationTransferAlertDTO.setFlightReservationAlertDTO(
								getFlightAlertFiller(new Integer(targetFlightSegId), flightSegmentDetails, oldResSegList));
						this.colTranserAlertDetails.add(reservationTransferAlertDTO);
					}
				}
			}
		}

		return defaultServiceResponse;
	}

	/**
	 * This method calls lcc and invoke transfer segment operation for each reservation that needed to be transfers.
	 * This may not be the optimum solution for handing reProtect interline reservations but due to time constrains
	 * falling to this this option. Need proper solution later
	 */
	private boolean handleInterlineTransfer(List<Reservation> interlinedReservationList, TransferSeatDTO transferSeatDTO,
			TransferSeatList seatList) throws ModuleException {

		Map<String, SegmentDetails> oldSegmentDetailsMap = new HashMap<String, SegmentDetails>();
		Map<String, SegmentDetails> newSegmentDetailsMap = new HashMap<String, SegmentDetails>();
		LCCClientTransferSegmentTO transferSegmentTO = new LCCClientTransferSegmentTO();

		Map<Integer, String> fltSegToPnrSegMap = new HashMap<Integer, String>();

		Flight flight = flightBD.getFlight(seatList.getSourceFlightId());
		FlightSegement sourceFlightSeg = flightBD.getFlightSegment(seatList.getSourceSegmentId());

		SegmentDetails oldSegmentDetail = new LCCClientTransferSegmentTO().new SegmentDetails();
		oldSegmentDetail.setDepartureDate(sourceFlightSeg.getEstTimeDepatureLocal());
		oldSegmentDetail.setDepartureDateZulu(sourceFlightSeg.getEstTimeDepatureZulu());
		oldSegmentDetail.setArrivalDate(sourceFlightSeg.getEstTimeArrivalLocal());
		oldSegmentDetail.setArrivalDateZulu(sourceFlightSeg.getEstTimeDepatureZulu());
		oldSegmentDetail.setSegmentCode(sourceFlightSeg.getSegmentCode());
		oldSegmentDetail.setFlightSegId(sourceFlightSeg.getFltSegId());
		oldSegmentDetail.setCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		oldSegmentDetail.setFlightNumber(flight.getFlightNumber());
		oldSegmentDetail.setStatus(ReservationInternalConstants.ReservationStatus.CANCEL);
		oldSegmentDetailsMap.put(oldSegmentDetail.getSegmentCode(), oldSegmentDetail);

		FlightSegement targerFlightSeg = flightBD.getFlightSegment(transferSeatDTO.getTargetFlightSegId());
		Flight targerFlight = flightBD.getFlight(transferSeatDTO.getTargetFlightId());
		// TODO : Logical CC Change - sending only COS to LCC
		String targetCabinClassCode = transferSeatDTO.getCabinClassCode();

		SegmentDetails newSegmentDetails = new LCCClientTransferSegmentTO().new SegmentDetails();
		newSegmentDetails.setDepartureDate(targerFlightSeg.getEstTimeDepatureLocal());
		newSegmentDetails.setDepartureDateZulu(targerFlightSeg.getEstTimeDepatureZulu());
		newSegmentDetails.setArrivalDate(targerFlightSeg.getEstTimeArrivalLocal());
		newSegmentDetails.setArrivalDateZulu(targerFlightSeg.getEstTimeArrivalZulu());
		newSegmentDetails.setFlightSegId(targerFlightSeg.getFltSegId());
		newSegmentDetails.setSegmentCode(targerFlightSeg.getSegmentCode());
		newSegmentDetails.setCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		newSegmentDetails.setCabinClass(targetCabinClassCode);
		newSegmentDetails.setFlightNumber(targerFlight.getFlightNumber());
		newSegmentDetails.setLogicalCabinClass(transferSeatDTO.getLogicalCCCode());
		newSegmentDetails.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
		newSegmentDetailsMap.put(newSegmentDetails.getSegmentCode(), newSegmentDetails);

		transferSegmentTO.setOldSegmentDetails(oldSegmentDetailsMap);
		transferSegmentTO.setNewSegmentDetails(newSegmentDetailsMap);
		if (this.reprotectFlightSegIds != null && this.reprotectFlightSegIds.size() > 0) {
			transferSegmentTO.setReprotectFlightSegIds(this.reprotectFlightSegIds);
			transferSegmentTO.setTransferSelectedFlightsFlag("Y");
		}
		if (this.isIBEVisible != null) {
			transferSegmentTO.setIBEVisibleFlag(this.isIBEVisible);
		}

		// get the old pnr segments for transfer.
		for (Reservation reservation : interlinedReservationList) {
			Set<ReservationSegment> segments = reservation.getSegments();
			for (ReservationSegment reservationSegment : segments) {

				if (oldSegmentDetail.getFlightSegId().equals(reservationSegment.getFlightSegId())) { // found
																										// the
																										// transfer
																										// segment
					Integer pnrSegId = reservationSegment.getPnrSegId();
					String cabinClassCode = reservationSegment.getCabinClassCode();

					StringBuilder pnrInfo = new StringBuilder();
					pnrInfo.append(reservation.getOriginatorPnr()).append("-");
					pnrInfo.append(reservation.getVersion()).append("-");
					pnrInfo.append(pnrSegId.toString()).append("-");
					pnrInfo.append(cabinClassCode);

					fltSegToPnrSegMap.put(reservationSegment.getFlightSegId(), pnrInfo.toString());
				}
			}
		}

		Collection<String> failedRes = ReservationModuleUtils.getLCCReservationBD().reprotect(transferSegmentTO,
				fltSegToPnrSegMap);

		if (failedRes != null && !failedRes.isEmpty()) {
			log.error("\n\r---------------- Reprotect faild for group pnr--------------\n\r");
			for (String faildReprotectRes : failedRes) {
				log.error("Faild Reprotect group reservation pnr = [" + faildReprotectRes + "]");
			}
			return false;
		} else {
			if (log.isDebugEnabled()) {
				log.debug("\n\r---------------- Reprotect for group pnr--------------\n\r");
				for (Reservation reservation : interlinedReservationList) {
					log.debug("group pnr = [" + reservation.getOriginatorPnr() + "]");
				}
			}
			return true;
		}
	}

	/**
	 * Method to get the flight details for source and target flight Ids
	 * 
	 * @param transferSeatDTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<FlightSegmentDTO> getFlightSegmentDetails(Collection<TransferSeatList> sourceSeatDistList,
			Collection<Integer> targetFlightSegIds) throws ModuleException {
		Collection<Integer> flightSegmentIds = new ArrayList<Integer>();
		flightSegmentIds.addAll(getSourceFlightSegmentsIds(sourceSeatDistList));
		flightSegmentIds.addAll(targetFlightSegIds);
		Collection<FlightSegmentDTO> flightSegmentDetails = flightBD.getFlightSegments(flightSegmentIds);
		if (flightSegmentDetails == null) {
			flightSegmentDetails = new ArrayList<FlightSegmentDTO>();
		}
		return flightSegmentDetails;
	}

	/**
	 * Method to get the PNR Modification details
	 * 
	 * @param transferSeatDTO
	 * @param flightSegmentDetails
	 * @param resSegList
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationAudit> getModificationDetails(Collection<Integer> targetFlightSegIds,
			Collection<FlightSegmentDTO> flightSegmentDetails, Collection<ReservationSegmentDTO> resSegList,
			Collection<PaxMealTO> totalCanceledMeals, Collection<PaxSeatTO> totalCanceledSeats) throws ModuleException {
		Collection<ReservationAudit> resSegmentDetails = new ArrayList<ReservationAudit>();
		ReservationSegmentDTO resSegment;
		ReservationAudit reservationAudit;
		FlightSegmentDTO sourceFlightSegment;
		FlightSegmentDTO targetFlightSegment;
		Integer sourceFlightSegId;
		Integer pnrSegId;
		String pnr;

		for (Integer targetFlightSegId : targetFlightSegIds) {
			targetFlightSegment = getFlightSegment(flightSegmentDetails, targetFlightSegId);

			Iterator<ReservationSegmentDTO> itResSeg = resSegList.iterator();
			while (itResSeg.hasNext()) {
				resSegment = itResSeg.next();
				pnr = resSegment.getPnr();
				sourceFlightSegId = resSegment.getFlightSegId();
				sourceFlightSegment = getFlightSegment(flightSegmentDetails, sourceFlightSegId);

				pnrSegId = resSegment.getPnrSegId();
				String canceledMealsStr = getCanceledMealsString(pnrSegId, totalCanceledMeals);
				String canceledSeatsStr = getCanceledSeatsString(pnrSegId, totalCanceledSeats);

				if (sourceFlightSegment != null && targetFlightSegment != null) {

					reservationAudit = new ReservationAudit();

					this.constuctResModAuditContent(sourceFlightSegment, targetFlightSegment, canceledSeatsStr, canceledMealsStr,
							reservationAudit);

					ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);

					resSegmentDetails.add(reservationAudit);
				}
			}
		}
		return resSegmentDetails;
	}

	/**
	 * Method to get the Alert details
	 * 
	 * @param transferSeatDTO
	 * @param flightSegmentDetails
	 * @param resSegList
	 * @return
	 * @throws ModuleException
	 */
	private Collection<Alert> getAlertDetails(Collection<Integer> targetFlightSegIds,
			Collection<FlightSegmentDTO> flightSegmentDetails, Collection<ReservationSegmentDTO> resSegList,
			Collection<PaxMealTO> totalCanceledMeals, Collection<PaxSeatTO> totalCanceledSeats) throws ModuleException {
		Collection<Alert> resAlertDetails = new ArrayList<Alert>();
		ReservationSegmentDTO resSegment;
		FlightSegmentDTO sourceFlightSegment;
		FlightSegmentDTO targetFlightSegment;
		Integer sourceFlightSegId;
		Integer pnrSegId;
		String pnr;
		String alertContent;

		for (Integer targetFlightSegId : targetFlightSegIds) {
			targetFlightSegment = getFlightSegment(flightSegmentDetails, targetFlightSegId);

			Iterator<ReservationSegmentDTO> itResSeg = resSegList.iterator();
			while (itResSeg.hasNext()) {
				resSegment = itResSeg.next();
				pnr = resSegment.getPnr();
				pnrSegId = resSegment.getPnrSegId();
				if (!this.pnrSegList.contains(pnrSegId)) {
					this.pnrSegList.add(pnrSegId);
				}
				String canceledMealsStr = getCanceledMealsString(pnrSegId, totalCanceledMeals);
				String canceledSeatsStr = getCanceledSeatsString(pnrSegId, totalCanceledSeats);

				sourceFlightSegId = resSegment.getFlightSegId();
				sourceFlightSegment = getFlightSegment(flightSegmentDetails, sourceFlightSegId);

				String immediateConnectingFlightDetail = getImmediateFlightDetails(pnr, resSegment.getJourneySeq(),
						resSegment.getSegmentSeq());
				if (sourceFlightSegment != null && targetFlightSegment != null) {
					alertContent = getTemplateContent(sourceFlightSegment, targetFlightSegment, pnr, canceledMealsStr,
							canceledSeatsStr, immediateConnectingFlightDetail);
					Alert alert = new Alert(alertContent, pnrSegId);
					if(this.isIBEVisible != null){
						alert.setVisibleIbeOnly(this.isIBEVisible);
					}
					if(this.isTransferSelectedFlights != null){
						alert.setTransferSelectedFlights(this.isTransferSelectedFlights);
					}
					if(this.reprotectFlightSegIds != null){
						alert.setFligthSegIds(this.reprotectFlightSegIds);
					}
					resAlertDetails.add(alert);
				}
			}
		}
		return resAlertDetails;
	}

	/**
	 * Method to get the TypeB request details
	 * 
	 * @param transferSeatDTO
	 * @param flightSegmentDetails
	 * @param resSegList
	 * @return
	 * @throws ModuleException
	 */
	private void sendTypeBRequests(Map<String, Reservation> gdsReservationsMap, Collection<Integer> targetFlightSegIds,
			Collection<FlightSegmentDTO> flightSegmentDetails, Collection<ReservationSegmentDTO> resSegList)
			throws ModuleException {

		ReservationSegmentDTO resSegment;
		FlightSegmentDTO sourceFlightSegment;
		FlightSegmentDTO targetFlightSegment;
		Integer sourceFlightSegId;
		String pnr;
		Reservation reservation;

		for (Integer targetFlightSegId : targetFlightSegIds) {
			targetFlightSegment = getFlightSegment(flightSegmentDetails, targetFlightSegId);

			Iterator<ReservationSegmentDTO> itResSeg = resSegList.iterator();
			while (itResSeg.hasNext()) {
				resSegment = itResSeg.next();
				pnr = resSegment.getPnr();
				if (gdsReservationsMap.keySet().contains(pnr)) {
					sourceFlightSegId = resSegment.getFlightSegId();
					sourceFlightSegment = getFlightSegment(flightSegmentDetails, sourceFlightSegId);
					reservation = gdsReservationsMap.get(pnr);
					if (sourceFlightSegment != null && targetFlightSegment != null) {
						TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
						typeBRequestDTO.setReservation(reservation);
						typeBRequestDTO.setReprotectedSourceFltSeg(sourceFlightSegment);
						typeBRequestDTO.setReprotectedTargetFltSeg(targetFlightSegment);
						typeBRequestDTO.setReprotectedPnrSegId(resSegment.getPnrSegId());
						typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.TRANSFER_SEGMENT.getCode());
						TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
					}
				}
			}
		}
	}

	/**
	 * Method to get the Codeshare TypeB request details
	 * 
	 * @param transferSeatDTO
	 * @param flightSegmentDetails
	 * @param resSegList
	 * @return
	 * @throws ModuleException
	 */
	private void sendCSTypeBRequests(Map<String, Reservation> alertingReservationsMap, Collection<Integer> targetFlightSegIds,
			Collection<FlightSegmentDTO> flightSegmentDetails, Collection<ReservationSegmentDTO> resSegList)
			throws ModuleException {

		ReservationSegmentDTO resSegment;
		FlightSegmentDTO sourceFlightSegment;
		FlightSegmentDTO targetFlightSegment;
		Integer sourceFlightSegId;
		String pnr;
		Reservation reservation;
		boolean csTargetSeg = false;
		boolean csSourceSeg = false;
		Set<String> csOcCarrierList = new HashSet<String>();
		String csTargetCarrier = null;

		for (Integer targetFlightSegId : targetFlightSegIds) {
			targetFlightSegment = getFlightSegment(flightSegmentDetails, targetFlightSegId);
			if (targetFlightSegment.getCsOcCarrierCode() != null) {
				csTargetSeg = true;
				csTargetCarrier = targetFlightSegment.getCsOcCarrierCode();
			}
			Iterator<ReservationSegmentDTO> itResSeg = resSegList.iterator();
			while (itResSeg.hasNext()) {
				resSegment = itResSeg.next();
				pnr = resSegment.getPnr();
				if (csTargetCarrier != null) {
					csOcCarrierList.add(csTargetCarrier);
				}
				if (alertingReservationsMap.keySet().contains(pnr)) {
					sourceFlightSegId = resSegment.getFlightSegId();
					sourceFlightSegment = getFlightSegment(flightSegmentDetails, sourceFlightSegId);
					if (sourceFlightSegment.getCsOcCarrierCode() != null) {
						csSourceSeg = true;
						csOcCarrierList.add(sourceFlightSegment.getCsOcCarrierCode());
					}
					reservation = alertingReservationsMap.get(pnr);
					if (sourceFlightSegment != null && targetFlightSegment != null && (csTargetSeg || csSourceSeg)) {
						TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
						typeBRequestDTO.setReservation(reservation);
						List<TransferSegmentDTO> transferSegmentDTOs = new ArrayList<TransferSegmentDTO>();
						TransferSegmentDTO transferSegmentDTO = new TransferSegmentDTO();
						transferSegmentDTO.setSourceFlightSegment(sourceFlightSegment);
						transferSegmentDTO.setTargetFlightSegment(targetFlightSegment);
						transferSegmentDTO.setPnrSegId(resSegment.getPnrSegId());
						transferSegmentDTOs.add(transferSegmentDTO);
						typeBRequestDTO.setTransferSegmentDTOs(transferSegmentDTOs);
						typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.TRANSFER_SEGMENT.getCode());
						TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOcCarrierList);
						csTargetSeg = false;
						csSourceSeg = false;
						csOcCarrierList.clear();
					}
				}
			}
		}
	}

	private String getCanceledMealsString(Integer pnrSegId, Collection<PaxMealTO> totalCanceledMeals) {

		StringBuilder sb = new StringBuilder();
		boolean matchingMealsFound = false;

		if (totalCanceledMeals != null && totalCanceledMeals.size() > 0) {
			for (PaxMealTO meal : totalCanceledMeals) {
				if (meal.getPnrSegId().equals(pnrSegId)) {
					matchingMealsFound = true;
					sb.append(String.format(" %s[%s] ", meal.getMealName(), meal.getPaxName()));
				}
			}
		}

		if (matchingMealsFound) {
			return " Meal(s) canceled:" + sb.toString() + ".";
		}

		return sb.toString();
	}

	private String getCanceledSeatsString(Integer pnrSegId, Collection<PaxSeatTO> totalCanceledSeats) {

		StringBuilder sb = new StringBuilder();
		boolean matchingMealsFound = false;

		if (totalCanceledSeats != null && totalCanceledSeats.size() > 0) {
			for (PaxSeatTO seat : totalCanceledSeats) {
				if (seat.getPnrSegId().equals(pnrSegId)) {
					matchingMealsFound = true;
					sb.append(String.format(" %s[%s] ", seat.getSeatCode(), seat.getPaxName()));
				}
			}
		}
		if (matchingMealsFound) {
			return " Seat(s) canceled:" + sb.toString() + ".";
		}
		return sb.toString();
	}

	/**
	 * Method to get the rollforwarded summary detail
	 * 
	 * @param transferSeatDTO
	 * @param flightSegmentDetails
	 * @param resSegList
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private RollforwardResultsSummaryDTO getTransferedSummary(Collection sourceSeatDistList,
			Collection<Integer> targetFlightSegIds, Collection flightSegmentDetails, Collection resSegList)
			throws ModuleException {
		RollforwardResultsSummaryDTO transferSummary = new RollforwardResultsSummaryDTO();
		FlightSegmentDTO sourceFlightSegment;
		// FlightSegmentDTO targetFlightSegment;
		Integer sourceFlightSegId;

		Collection sourceFlightSegments = new ArrayList();
		Iterator itSourceSeg = getSourceFlightSegmentsIds(sourceSeatDistList).iterator();
		while (itSourceSeg.hasNext()) {
			sourceFlightSegId = (Integer) itSourceSeg.next();
			sourceFlightSegment = getFlightSegment(flightSegmentDetails, sourceFlightSegId);
			if (sourceFlightSegment != null) {
				sourceFlightSegments.add(sourceFlightSegment);
			}
		}
		// targetFlightSegment = getFlightSegment(flightSegmentDetails, targetFlightSegId);

		transferSummary.setSourceFltSegSeatDists(sourceSeatDistList);
		if (sourceFlightSegments.size() > 0) {
			transferSummary.setSourceFlightSegmentDetails(sourceFlightSegments);
		}
		// Commented this out as this is not needed
		// if (targetFlightSegment != null) {
		// transferSummary.setTargetFlightSegmentDetails(targetFlightSegment);
		// }
		transferSummary.setReservationSegmentDetails(resSegList);
		return transferSummary;
	}

	/**
	 * Returns the source flight segment ids
	 * 
	 * @param transferSeatDTO
	 * @return
	 */
	private Collection<Integer> getSourceFlightSegmentsIds(Collection<TransferSeatList> sourceSeatDistList) {
		TransferSeatList seatList;
		Integer segmentId;
		Collection<Integer> sourceFlightSegIds = new ArrayList<Integer>();
		Iterator<TransferSeatList> itSource = sourceSeatDistList.iterator();
		while (itSource.hasNext()) {
			seatList = itSource.next();
			segmentId = new Integer(seatList.getSourceSegmentId());
			if (segmentId != null && !sourceFlightSegIds.contains(segmentId)) {
				sourceFlightSegIds.add(segmentId);
			}
		}
		return sourceFlightSegIds;
	}

	/**
	 * Method to get the Flight Segment Detail of a particular flightSegmentId
	 * 
	 * @param flightSegmentDetails
	 * @param flightSegmentId
	 * @return
	 */
	private FlightSegmentDTO getFlightSegment(Collection<FlightSegmentDTO> flightSegmentDetails, Integer flightSegmentId) {
		FlightSegmentDTO flgSegment = null;
		Iterator<FlightSegmentDTO> itFlgSegDet = flightSegmentDetails.iterator();
		while (itFlgSegDet.hasNext()) {
			flgSegment = itFlgSegDet.next();
			if ((flgSegment != null) && (flgSegment.getSegmentId() != null) && (flightSegmentId != null)
					&& (flgSegment.getSegmentId().equals(flightSegmentId))) {
				break;
			}
		}
		return flgSegment;
	}

	/**
	 * Method to get the template content for transfer pax
	 * 
	 * @param sourceFlgSegment
	 * @param targetFlgSegment
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getTemplateContent(FlightSegmentDTO sourceFlgSegment, FlightSegmentDTO targetFlgSegment, String pnr,
			String canceledSeats, String canceledMeals, String immediateConnectingFlightDetail) throws ModuleException {
		HashMap alertParams = new HashMap();
		String completeAlertMessage = null;
		if (alertTemplate == null) {
			alertTemplate = alertingBD.getAlertTemplate(AlertTemplateEnum.TEMPLATE_TRANSFER_PAX);
		}
		String templateContent = alertTemplate.getAlertTemplateContent();

		alertParams.put(AlertTemplateEnum.TemplateParams.PNR, pnr);
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_FLIGHT_NO, sourceFlgSegment.getFlightNumber());
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_DEPARTURE,
				simpleDateFormat.format(sourceFlgSegment.getDepartureDateTime()));
		alertParams.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_CODE, sourceFlgSegment.getSegmentCode());
		alertParams.put(AlertTemplateEnum.TemplateParams.NEW_FLIGHT_NO, targetFlgSegment.getFlightNumber());
		alertParams.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_DEPARTURE,
				simpleDateFormat.format(targetFlgSegment.getDepartureDateTime()));
		alertParams.put(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_CODE, targetFlgSegment.getSegmentCode());
		alertParams.put(AlertTemplateEnum.TemplateParams.CANCELED_MEALS, canceledMeals);
		alertParams.put(AlertTemplateEnum.TemplateParams.CANCELED_SEATS, canceledSeats);
		alertParams.put(AlertTemplateEnum.TemplateParams.IMMEDIATE_CONNECTING_FLIGHT_DETAIL, immediateConnectingFlightDetail);
		completeAlertMessage = Util.setStringToPlaceHolderIndex(templateContent, alertParams);

		return completeAlertMessage;
	}

	/**
	 * Construct Reservation modification audit content
	 * 
	 * @param sourceFlgSegment
	 * @param targetFlgSegment
	 * @param reservationAudit
	 * @throws ModuleException
	 */
	private void constuctResModAuditContent(FlightSegmentDTO sourceFlgSegment, FlightSegmentDTO targetFlgSegment,
			String canceledSeats, String canceledMeals, ReservationAudit reservationAudit) throws ModuleException {

		reservationAudit.setModificationType(AuditTemplateEnum.TRANSFERED_SEGMENT.getCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.OLD_FLIGHT_NUMBER,
				sourceFlgSegment.getFlightNumber());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.OLD_FLIGHT_DATE,
				simpleDateFormat.format(sourceFlgSegment.getDepartureDateTime()));

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.OLD_FLIGHT_SEGMENT,
				sourceFlgSegment.getSegmentCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.NEW_FLIGHT_NUMBER,
				targetFlgSegment.getFlightNumber());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.NEW_FLIGHT_DATE,
				simpleDateFormat.format(targetFlgSegment.getDepartureDateTime()));

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.NEW_FLIGHT_SEGMENT,
				targetFlgSegment.getSegmentCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.CANCELED_MEALS, canceledMeals);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.TransferedSegment.CANCELED_SEATS, canceledSeats);
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkParams(Collection transferSeatList, Boolean isAlert, AllocateEnum overAlloc, Integer overAllocSeats)
			throws ModuleException {

		if (transferSeatList == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (isAlert == null) {
			isAlert = new Boolean(true);
		}

		if (overAlloc == null) {
			overAlloc = AllocateEnum.AUTO_FIT;
		}

		if (overAlloc.equals(AllocateEnum.OVER_LIMIT) && overAllocSeats == null) {
			// exception
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}

	/**
	 * Generate Notifications for Pnr Segments
	 * 
	 * @throws ModuleException
	 */
	private void generateNotificationsForPnrSegments() throws ModuleException {

		// Profile List
		List<MessageProfile> messageProfiles = new ArrayList<MessageProfile>();
		// FlightReservationAlertDTO flightReservationAlertDTO;
		ReservationTransferAlertDTO reservationTransferAlertDTO;

		for (ReservationTransferAlertDTO reservationTransferAlertDTO2 : colTranserAlertDetails) {
			reservationTransferAlertDTO = reservationTransferAlertDTO2;

			Map<String, Set<AgentAlertInfoDTO>> agentAlertingResInfo = new HashMap<String, Set<AgentAlertInfoDTO>>();

			for (Reservation booking : reservationTransferAlertDTO.getAlertingReservations()) {

				Reservation res = loadDetailedReservation(booking.getPnr());

				// find email address and mobile number of customer
				String emailAddress = BeanUtils.nullHandler(booking.getContactInfo().getEmail());
				String mobileNumber = BeanUtils.nullHandler(booking.getContactInfo().getMobileNo());

				if (emailAddress.length() > 0 && emailAddress.indexOf("@") != -1 && sendEmailsForFlightAlterations) {
					// Reservation res =
					// loadDetailedReservation(booking.getPnr());
					MessageProfile profile = getMessageProfileForFlightAlterations(res, null,
							reservationTransferAlertDTO.getFlightReservationAlertDTO(),
							ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_EMAIL, res.getContactInfo().getEmail(),
							null);
					messageProfiles.add(profile);
					if (log.isDebugEnabled()) {
						log.debug(" Email to customer " + booking.getPnr() + " " + res.getContactInfo().getEmail() + "\n\r");
					}
				}
				
				if(pnrSegList != null){
					if (pnrSegList.size() > 0 && sendEmailsIBEVisible) {
						MessageProfile profile = getMessageProfileForFlightAlterationsIBE(res, null,
								reservationTransferAlertDTO.getFlightReservationAlertDTO(),
								ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_EMAIL_IBE, res.getContactInfo()
										.getEmail(), null);
						messageProfiles.add(profile);
						if (log.isDebugEnabled()) {
							log.debug(" Email to customer " + booking.getPnr() + " " + res.getContactInfo().getEmail() + "\n\r");
						}
					}
				}

				if (mobileNumber.length() > 0 && sendSMSForFlightAlterations) {
					// Reservation res =
					// loadDetailedReservation(booking.getPnr());
					boolean isBookingOnhold = AccelAeroCalculator.add(res.getTotalChargeAmounts()).doubleValue() > res
							.getTotalPaidAmount().doubleValue();

					String sender = "";
					String senderTemplate = "";
					if (SystemPropertyUtil.isSmsToEmail()) {
						sender = res.getContactInfo().getEmail();
						senderTemplate = ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_SMS;
					} else {
						sender = res.getContactInfo().getMobileNo();
						senderTemplate = ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_SMS;
					}

					if (isBookingOnhold && sendSMSForOnHoldBookings) {
						MessageProfile profile = getMessageProfileForFlightAlterations(res, null,
								reservationTransferAlertDTO.getFlightReservationAlertDTO(), senderTemplate,
								res.getContactInfo().getEmail(), null);
						messageProfiles.add(profile);
					}

					if (!isBookingOnhold && sendSMSForConfirmedBookings) {
						MessageProfile profile = getMessageProfileForFlightAlterations(res, null,
								reservationTransferAlertDTO.getFlightReservationAlertDTO(), senderTemplate, sender, null);
						messageProfiles.add(profile);
					}

					if (log.isDebugEnabled()) {
						log.debug(" SMS to customer " + booking.getPnr() + " " + res.getContactInfo().getMobileNo() + "\n\r");
					}
				}

				String originAgentCode = BeanUtils.nullHandler(booking.getAdminInfo().getOriginAgentCode());
				String originAgentChannel = BeanUtils.nullHandler(booking.getAdminInfo().getOriginChannelId());
				String ownerAgentCode = BeanUtils.nullHandler(booking.getAdminInfo().getOwnerAgentCode());
				String ownerAgentChannel = BeanUtils.nullHandler(booking.getAdminInfo().getOwnerChannelId());

				boolean agentAlertingEnabled = AppSysParamsUtil.isAllowEmailsToAgentsInFlightReprotect();

				if (agentAlertingEnabled) {
					AgentAlertInfoDTO agentAlertInfo = new AgentAlertInfoDTO();
					agentAlertInfo.setPnr(res.getPnr());
					agentAlertInfo.setContactEMail(BeanUtils.nullHandler(res.getContactInfo().getEmail()));
					agentAlertInfo.setContactName(BeanUtils.nullHandler(res.getContactInfo().getFirstName()) + " "
							+ BeanUtils.nullHandler(res.getContactInfo().getLastName()));
					agentAlertInfo.setContactPhoneNo(BeanUtils.nullHandler(res.getContactInfo().getPhoneNo()));

					if (sendEmailForOriginAgents && originAgentCode.length() > 0
							&& originAgentChannel.equals(Integer.toString(SalesChannelsUtil.SALES_CHANNEL_AGENT))) {
						if (agentAlertingResInfo.get(originAgentCode) == null) {
							agentAlertingResInfo.put(originAgentCode, new TreeSet<AgentAlertInfoDTO>());
						}
						agentAlertingResInfo.get(originAgentCode).add(agentAlertInfo);
					}
					if (sendEmailsForOwnerAgents && ownerAgentCode.length() > 0
							&& ownerAgentChannel.equals(Integer.toString(SalesChannelsUtil.SALES_CHANNEL_AGENT))) {
						if (agentAlertingResInfo.get(ownerAgentCode) == null) {
							agentAlertingResInfo.put(ownerAgentCode, new TreeSet<AgentAlertInfoDTO>());
						}
						agentAlertingResInfo.get(ownerAgentCode).add(agentAlertInfo);
					}
				}
			}

			// adding message profiles for email alerts of agents
			for (String agentCode : agentAlertingResInfo.keySet()) {
				Agent agent = ReservationModuleUtils.getTravelAgentBD().getAgent(agentCode);
				String agentEmail = BeanUtils.nullHandler(agent.getEmailId());

				if (agentEmail.length() > 0 && agentEmail.indexOf("@") != -1) {
					MessageProfile profile = getMessageProfileForFlightAlterationsForAgents(agent,
							agentAlertingResInfo.get(agentCode), null, reservationTransferAlertDTO.getFlightReservationAlertDTO(),
							ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_EMAIL_AGENT, agentEmail, null);
					messageProfiles.add(profile);
					if (log.isDebugEnabled()) {
						log.debug(" Email to Agent " + agentCode + " " + agentEmail + "\n\r");
					}
				}
			}

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Create Message profile for flight alterations
	 * 
	 * @param reservation
	 * @param flightAlertDTO
	 * @param flightReservationAlertDTO
	 * @param topicName
	 * @param toAddress
	 * @param colReservationAudit
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MessageProfile getMessageProfileForFlightAlterations(Reservation reservation, FlightAlertDTO flightAlertDTO,
			FlightReservationAlertDTO flightReservationAlertDTO, String topicName, String toAddress,
			Collection colReservationAudit) throws ModuleException {

		String carrierCode = AppSysParamsUtil
				.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO().getFlightNumber());
		String templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_SMS;
		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(toAddress);
		messageList.add(user);

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();
		String preferredLanguage = reservation.getContactInfo().getPreferredLanguage();
		if (flightReservationAlertDTO.getAlertDetails() == null) {
			HashMap alertDetails = AlertUtil.prepareFlightSegmentAlertDto(flightReservationAlertDTO.getOldFlightAlertInfoDTO(),
					flightReservationAlertDTO.getNewFlightAlertInfoDTO());
			flightReservationAlertDTO.setAlertDetails(alertDetails);
		}

		topic.setTopicName(topicName);
		topic.setLocale(new Locale(preferredLanguage));
		// FIXME
		// map.put("alertTemplateEnum",
		// flightReservationAlertDTO.getAlertTemplateIds());
		map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());
		map.put("pnr", reservation.getPnr());
		map.put("title", BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getTitle()));
		map.put("firstName", BeanUtils.getFirst20Characters(reservation.getContactInfo().getFirstName(), 20));
		map.put("user", BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getFirstName()) + " "
				+ BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getLastName()));
		map.put("companyAddress", ReservationApiUtils.getCompanyAddressInformation(carrierCode));
		map.put("reservationPaxDTOs", getAffectedSegments(reservation, flightReservationAlertDTO.getOldFlightAlertInfoDTO()));
		map.put("ibeURL", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		map.put("phone", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("logoImageName", getLogoImageName(carrierCode));
		String strNotifyMessage = Util.setStringToPlaceHolderIndex(
				AlertReservationsUtil.getMessage(flightReservationAlertDTO, preferredLanguage, map, templateName), map);
		map.put("message", strNotifyMessage);
		topic.setTopicParams(map);
		// topic.setAuditInfo(colReservationAudit);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

	/**
	 * Create Message profile for flight alterations V2
	 * 
	 * @param reservation
	 * @param flightAlertDTO
	 * @param flightReservationAlertDTO
	 * @param topicName
	 * @param toAddress
	 * @param colReservationAudit
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MessageProfile getMessageProfileForFlightAlterationsIBE(Reservation reservation, FlightAlertDTO flightAlertDTO,
			FlightReservationAlertDTO flightReservationAlertDTO, String topicName, String toAddress,
			Collection colReservationAudit) throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO()
				.getFlightNumber());
		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(toAddress);
		messageList.add(user);

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();
		String preferredLanguage = reservation.getContactInfo().getPreferredLanguage();
		if (flightReservationAlertDTO.getAlertDetails() == null) {
			HashMap alertDetails = AlertUtil.prepareFlightSegmentAlertDto(flightReservationAlertDTO.getOldFlightAlertInfoDTO(),
					flightReservationAlertDTO.getNewFlightAlertInfoDTO());
			flightReservationAlertDTO.setAlertDetails(alertDetails);
		}

		Collection<Integer> tempCol = reservation.getPnrSegIds();
		Collection<Integer> pnrSegIdCol = new ArrayList<Integer>();
		Map alertPnrSeg = new HashMap();
		Long pnrSegId = null;
		String pnr = reservation.getPnr();
		Long alertID = null;
		for (Integer pnrSegIdTemp : tempCol) {
			if (this.pnrSegList.contains(pnrSegIdTemp)) {
				pnrSegId = pnrSegIdTemp.longValue();
				pnrSegIdCol.add(pnrSegIdTemp);
				break;
			}
		}
		if (pnrSegIdCol.size() > 0) {
			alertPnrSeg = alertingBD.getAlertsForPnrSegments(pnrSegIdCol);
			if (alertPnrSeg.size() > 0) {
				List<Alert> alertList = (List<Alert>) alertPnrSeg.get(pnrSegId);
				Alert TempAlert = alertList.get(0);
				for (int i = 1; i < alertList.size(); i++) {
					if (alertList.size() == 1) {
						// do nothing
					} else {
						if (TempAlert.getTimestamp().compareTo(alertList.get(i).getTimestamp()) < 0) {
							TempAlert = alertList.get(i);
						}
					}
				}
				alertID = TempAlert.getAlertId();
			}
		}
		Integer oldFlightSegmentId = flightReservationAlertDTO.getOldFlightAlertInfoDTO().getFlightSegmentId();
		String ibeSecureURL = AppSysParamsUtil.getSecureServiceAppIBEUrl();
		String ibePnrReprotectionUrl = ibeSecureURL + "/ibe/reservation.html#/self-reprotect" + "/" + preferredLanguage.toUpperCase()
				+ "/" + pnr + "/" + alertID.toString() + "/" + pnrSegId.toString() + "/" + oldFlightSegmentId.toString();
		topic.setTopicName(topicName);
		topic.setLocale(new Locale(preferredLanguage));
		// FIXME
		// map.put("alertTemplateEnum",
		// flightReservationAlertDTO.getAlertTemplateIds());
		map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());
		map.put("pnr", reservation.getPnr());
		map.put("user",
				BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getFirstName()) + " "
						+ BeanUtils.makeFirstLetterCapital(reservation.getContactInfo().getLastName()));
		map.put("companyAddress", ReservationApiUtils.getCompanyAddressInformation(carrierCode));
		map.put("reservationPaxDTOs", getAffectedSegments(reservation, flightReservationAlertDTO.getOldFlightAlertInfoDTO()));
		map.put("ibeURL", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		map.put("phone", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("selfReprotectUrl", ibePnrReprotectionUrl);
		map.put("originAptName", ReservationModuleUtils.getAirportBD()
				.getAirport(flightReservationAlertDTO.getOldFlightAlertInfoDTO().getOriginAptCode()).getAirportName());
		map.put("destinationAptName", ReservationModuleUtils.getAirportBD()
				.getAirport(flightReservationAlertDTO.getOldFlightAlertInfoDTO().getDestinationAptCode()).getAirportName());
		map.put("callCenterUrl", AppSysParamsUtil.getCallCenterUrl());
		topic.setTopicParams(map);
		// topic.setAuditInfo(colReservationAudit);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MessageProfile getMessageProfileForFlightAlterationsForAgents(Agent agent, Set<AgentAlertInfoDTO> alertInfoDTOs,
			FlightAlertDTO flightAlertDTO, FlightReservationAlertDTO flightReservationAlertDTO, String topicName,
			String toAddress, Collection colReservationAudit) throws ModuleException {

		String carrierCode = AppSysParamsUtil
				.extractCarrierCode(flightReservationAlertDTO.getOldFlightAlertInfoDTO().getFlightNumber());

		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(toAddress);
		messageList.add(user);

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();

		if (flightReservationAlertDTO.getAlertDetails() == null) {
			HashMap alertDetails = AlertUtil.prepareFlightSegmentAlertDto(flightReservationAlertDTO.getOldFlightAlertInfoDTO(),
					flightReservationAlertDTO.getNewFlightAlertInfoDTO());
			flightReservationAlertDTO.setAlertDetails(alertDetails);
		}

		topic.setTopicName(topicName);

		map.put("oldFlightAlertInfoDTO", flightReservationAlertDTO.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", flightReservationAlertDTO.getNewFlightAlertInfoDTO());
		map.put("user", BeanUtils.nullHandler(agent.getAgentName()));
		map.put("companyAddress", ReservationApiUtils.getCompanyAddressInformation(carrierCode));
		map.put("ibeURL", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		map.put("phone", ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("alertInfoSet", alertInfoDTOs);
		topic.setTopicParams(map);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

	/**
	 * Returns the affected segments
	 * 
	 * @param reservation
	 * @param oldFlightAlertInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection getAffectedSegments(Reservation reservation, FlightAlertInfoDTO oldFlightAlertInfoDTO)
			throws ModuleException {
		Collection colReservationSegmentDTO = reservation.getSegmentsView();
		List lstAffectedReservationSegmentDTO = new ArrayList();
		Collection colAirportCodes = new ArrayList();
		ReservationSegmentDTO reservationSegmentDTO;

		for (Iterator iter = colReservationSegmentDTO.iterator(); iter.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) iter.next();

			if (ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())
					&& reservationSegmentDTO.getFlightNo().equals(oldFlightAlertInfoDTO.getFlightNumber())
					&& reservationSegmentDTO.getDepartureDate().compareTo(oldFlightAlertInfoDTO.getDepartureDate()) == 0
					&& reservationSegmentDTO.getSegmentCode().indexOf(oldFlightAlertInfoDTO.getOriginAptCode()) != -1
					&& reservationSegmentDTO.getSegmentCode().indexOf(oldFlightAlertInfoDTO.getDestinationAptCode()) != -1) {
				lstAffectedReservationSegmentDTO.add(reservationSegmentDTO);
				colAirportCodes.addAll(Arrays.asList(reservationSegmentDTO.getSegmentCode().split("/")));
			}
		}

		if (lstAffectedReservationSegmentDTO.size() > 0) {
			AirportBD airportBD = ReservationModuleUtils.getAirportBD();
			Map<String, CachedAirportDTO> mapAirportCodes = airportBD.getCachedOwnAirportMap(colAirportCodes);

			for (Iterator iter = lstAffectedReservationSegmentDTO.iterator(); iter.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) iter.next();

				String composedSegment = "";
				String[] segments = reservationSegmentDTO.getSegmentCode().split("/");

				for (int i = 0; i < segments.length; i++) {
					CachedAirportDTO airport = mapAirportCodes.get(segments[i]);
					if (i != 0) {
						composedSegment += " / ";
					}
					composedSegment += airport.getAirportName();
				}

				reservationSegmentDTO.setSegmentDescription(composedSegment);
			}

			// Sorting via the depature date
			Collections.sort(lstAffectedReservationSegmentDTO);
		}

		return lstAffectedReservationSegmentDTO;
	}

	/**
	 * Returns the logo image name
	 * 
	 * @param carrierCode
	 * @return
	 */
	private String getLogoImageName(String carrierCode) {
		String logoImageName = "LogoAni.gif";
		if (carrierCode != null) {
			logoImageName = "LogoAni" + carrierCode + ".gif";
		}
		return StaticFileNameUtil.getCorrected(logoImageName);
	}

	/**
	 * Loads the detail reservation. Earlier reservation entity only contains the high level reservation information
	 * only
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private Reservation loadDetailedReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	/**
	 * Set Arguments
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param credentialsDTO
	 */
	private void setArguments(CredentialsDTO credentialsDTO, FlightAlertDTO flightAlertDTO) {
		this.credentialsDTO = credentialsDTO;

		this.sendEmailsForFlightAlterations = flightAlertDTO.isSendEmailsForFlightAlterations();
		this.sendSMSForFlightAlterations = flightAlertDTO.isSendSMSForFlightAlterations();
		this.sendSMSForConfirmedBookings = flightAlertDTO.isSendSMSForConfirmedBookings();
		this.sendSMSForOnHoldBookings = flightAlertDTO.isSendSMSForOnHoldBookings();
		this.sendEmailForOriginAgents = flightAlertDTO.isSendEmailToOriginAgent();
		this.sendEmailsForOwnerAgents = flightAlertDTO.isSendEmailToOwnerAgent();
		this.isIBEVisible = flightAlertDTO.getVisibleIbeOnly();
		this.reprotectFlightSegIds = flightAlertDTO.getFligthSegIds();
		if (isIBEVisible != null) {
			if (this.reprotectFlightSegIds != null && this.reprotectFlightSegIds.size() > 0) {
				this.sendEmailsIBEVisible = true;
				this.isTransferSelectedFlights = "Y";
			} else {
				this.sendEmailsIBEVisible = false;
			}
		} else {
			// do nothing
		}
	}

	/**
	 * Method to get the Alert details
	 * 
	 * @param transferSeatDTO
	 * @param flightSegmentDetails
	 * @param resSegList
	 * @return
	 * @throws ModuleException
	 */
	private FlightReservationAlertDTO getFlightAlertFiller(Integer targetFlightSegId,
			Collection<FlightSegmentDTO> flightSegmentDetails, Collection<ReservationSegmentDTO> resSegList)
			throws ModuleException {
		// ReservationSegmentDTO resSegment;
		FlightSegmentDTO sourceFlightSegment = new FlightSegmentDTO();
		FlightSegmentDTO targetFlightSegment;
		Integer sourceFlightSegId;

		targetFlightSegment = getFlightSegment(flightSegmentDetails, targetFlightSegId);

		Iterator<ReservationSegmentDTO> itResSeg = resSegList.iterator();
		while (itResSeg.hasNext()) {
			ReservationSegmentDTO resSegment = itResSeg.next();
			sourceFlightSegId = resSegment.getFlightSegId();
			sourceFlightSegment = getFlightSegment(flightSegmentDetails, sourceFlightSegId);

		}

		FlightReservationAlertDTO flightReservationAlertDTO = new FlightReservationAlertDTO();
		FlightAlertInfoDTO oldFlightAlertInfoDTO = new FlightAlertInfoDTO();
		FlightAlertInfoDTO newFlightAlertInfoDTO = new FlightAlertInfoDTO();
		// TODO set additional details

		oldFlightAlertInfoDTO.setDepartureDate(sourceFlightSegment.getDepartureDateTime());
		oldFlightAlertInfoDTO.setDestinationAptCode(sourceFlightSegment.getToAirport());
		oldFlightAlertInfoDTO.setFlightId(sourceFlightSegment.getFlightId());
		oldFlightAlertInfoDTO.setFlightNumber(sourceFlightSegment.getFlightNumber());
		oldFlightAlertInfoDTO.setLegDetails(null);
		oldFlightAlertInfoDTO.setModelNumber(sourceFlightSegment.getFlightModelNumber()); // TODO
		oldFlightAlertInfoDTO.setOriginAptCode(sourceFlightSegment.getFromAirport());
		oldFlightAlertInfoDTO.setRoute(null);
		oldFlightAlertInfoDTO.setScheduleId(null);
		oldFlightAlertInfoDTO.setSegmentDetails(sourceFlightSegment.getSegmentCode());
		oldFlightAlertInfoDTO.setSegments(null);
		oldFlightAlertInfoDTO.setStatus(null);
		oldFlightAlertInfoDTO.setFlightSegmentId(sourceFlightSegment.getSegmentId());;

		newFlightAlertInfoDTO.setDepartureDate(targetFlightSegment.getDepartureDateTime());
		newFlightAlertInfoDTO.setDestinationAptCode(targetFlightSegment.getToAirport());
		newFlightAlertInfoDTO.setFlightId(targetFlightSegment.getFlightId());
		newFlightAlertInfoDTO.setFlightNumber(targetFlightSegment.getFlightNumber());
		newFlightAlertInfoDTO.setLegDetails(null);
		newFlightAlertInfoDTO.setModelNumber(targetFlightSegment.getFlightModelNumber());
		newFlightAlertInfoDTO.setOriginAptCode(targetFlightSegment.getFromAirport());
		newFlightAlertInfoDTO.setRoute(null);
		newFlightAlertInfoDTO.setScheduleId(null);
		newFlightAlertInfoDTO.setSegmentDetails(targetFlightSegment.getSegmentCode());
		newFlightAlertInfoDTO.setSegments(null);
		newFlightAlertInfoDTO.setStatus(null);
		newFlightAlertInfoDTO.setFlightSegmentId(targetFlightSegment.getSegmentId());

		flightReservationAlertDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
		flightReservationAlertDTO.setNewFlightAlertInfoDTO(newFlightAlertInfoDTO);

		return flightReservationAlertDTO;
	}

	private static String getLastAirportCode(String segmentCode) {
		segmentCode = BeanUtils.nullHandler(segmentCode);

		if (segmentCode.length() > 0) {
			String[] segments = segmentCode.split("/");
			return segments[segments.length - 1];
		} else {
			return "";
		}
	}

	private void reCalculateOpenReturnConfirmBeforeDate(ReservationSegment rSegment,
			Collection<ReservationSegment> colOpenRTSegments, int targetFlightSegId, Reservation reservation)
			throws ModuleException {
		Iterator<ReservationSegment> iterOpenRTSegments = colOpenRTSegments.iterator();
		while (iterOpenRTSegments.hasNext()) {
			ReservationSegment rOpenRTSegment = iterOpenRTSegments.next();
			for (ReservationSegment resSeg : reservation.getSegments()) {
				if (rOpenRTSegment.getPnrSegId().intValue() == resSeg.getPnrSegId()) {
					rOpenRTSegment = resSeg;
				}
			}
			if (rOpenRTSegment.getReturnGrpId().equals(rSegment.getReturnGrpId())) { // matching
																						// open
																						// return
																						// segment
				FlightSegmentDTO targetFlightSegDTO = segmentDAO.getFlightSegment(targetFlightSegId);
				if (rSegment.getFareTO(null) != null) {
					FareSummaryDTO fareSummaryDTO = new FareSummaryDTO(rSegment.getFareTO(null));
					String airportCode = getLastAirportCode(targetFlightSegDTO.getSegmentCode());
					if (fareSummaryDTO != null && targetFlightSegDTO.getArrivalDateTimeZulu() != null
							&& !BeanUtils.nullHandler(airportCode).isEmpty()) {
						long openRTConfPeriodInMins = fareSummaryDTO.getOpenRTConfPeriodInMins();
						long openRTConfPeriodInMonths = fareSummaryDTO.getOpenRTConfPeriodInMonths();
						long maximumStayOverInMins = fareSummaryDTO.getMaximumStayOverInMins();
						long maximumStayOverInMonths = fareSummaryDTO.getMaximumStayOverInMonths();

						// For an open return booking maximum stay over should
						// not be zero
						Date travelExpiryDate = CalendarUtil.add(targetFlightSegDTO.getArrivalDateTimeZulu(), 0,
								(int) maximumStayOverInMonths, 0, 0, (int) maximumStayOverInMins, 0);
						Date confirmBeforeDate;

						// If the user hasn't specified confirm before. This
						// will be same as travel expiry date
						if (openRTConfPeriodInMins == 0 && openRTConfPeriodInMonths == 0) {
							confirmBeforeDate = travelExpiryDate;
						} else {
							confirmBeforeDate = CalendarUtil.add(travelExpiryDate, 0, (int) -openRTConfPeriodInMonths, 0, 0,
									(int) -openRTConfPeriodInMins, 0);
						}
						rOpenRTSegment.setOpenReturnConfirmBeforeZulu(confirmBeforeDate);
					}
				}
			}
		} //
	}

	private void fillPaxNamesForSeats(Collection<PaxSeatTO> seats, Set<ReservationPax> passengers) {
		for (PaxSeatTO seat : seats) {
			for (ReservationPax pax : passengers) {
				if (seat.getPnrPaxId().equals(pax.getPnrPaxId())) {
					seat.setPaxName(BeanUtils.makeFirstLetterCapital(pax.getFirstName()) + " "
							+ BeanUtils.makeFirstLetterCapital(pax.getLastName()));
					break;
				}
			}
		}
	}

	private void fillPaxNamesForMeals(Collection<PaxMealTO> meals, Set<ReservationPax> passengers) {
		for (PaxMealTO meal : meals) {
			for (ReservationPax pax : passengers) {
				if (meal.getPnrPaxId().equals(pax.getPnrPaxId())) {
					meal.setPaxName(BeanUtils.makeFirstLetterCapital(pax.getFirstName()) + " "
							+ BeanUtils.makeFirstLetterCapital(pax.getLastName()));
					break;
				}
			}
		}
	}

	private void fillPaxNamesForBaggages(Collection<PaxBaggageTO> baggages, Set<ReservationPax> passengers) {
		for (PaxBaggageTO baggage : baggages) {
			for (ReservationPax pax : passengers) {
				if (baggage.getPnrPaxId().equals(pax.getPnrPaxId())) {
					baggage.setPaxName(BeanUtils.makeFirstLetterCapital(pax.getFirstName()) + " "
							+ BeanUtils.makeFirstLetterCapital(pax.getLastName()));
					break;
				}
			}
		}
	}

	private String getImmediateFlightDetails(String pnr, Integer journeySeq, Integer segmentSeq) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		StringBuilder immediateFlightDetail = new StringBuilder();
		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (journeySeq.intValue() == reservationSegmentDTO.getJourneySeq().intValue()
					&& reservationSegmentDTO.getSegmentSeq().intValue() == (segmentSeq + 1)) {
				immediateFlightDetail.append("Immediate Connecting Flight ");
				immediateFlightDetail.append(reservationSegmentDTO.getFlightNo() + ", ");
				immediateFlightDetail.append(simpleDateFormat.format(reservationSegmentDTO.getDepartureDate()) + " ");
				immediateFlightDetail.append(reservationSegmentDTO.getSegmentCode() + ".");
			}
		}

		return immediateFlightDetail.toString();
	}

	private Collection<FlightSegmentDTO> getSourceFlightDetails(Collection<Integer> sourceFlightId,
			Collection<FlightSegmentDTO> flightSegmentDetails) {
		Collection<FlightSegmentDTO> sourceFlightDetails = new ArrayList<FlightSegmentDTO>();
		for (Integer flightId : sourceFlightId) {
			for (FlightSegmentDTO flightSeg : flightSegmentDetails) {
				if (flightId == flightSeg.getFlightId().intValue()) {
					sourceFlightDetails.add(flightSeg);
				}
			}
		}
		return sourceFlightDetails;
	}

	private static void cancelBookedAirportTransfers(List<Integer> fltSegIDs) throws ModuleException {
		if (fltSegIDs != null && fltSegIDs.size() > 0) {
			Collection<ReservationSegment> reservationSegments = ReservationModuleUtils.getSegmentBD().getPnrSegments(fltSegIDs,
					true, true);
			Collection<Integer> pnrSegIds = new ArrayList<Integer>();
			Collection<Reservation> reservationList = new ArrayList<Reservation>();
			for (ReservationSegment seg : reservationSegments) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(seg.getStatus())) {
					pnrSegIds.add(seg.getPnrSegId());
					Reservation reservation = seg.getReservation();
					reservationList.add(reservation);
				}
			}

			for (Reservation reservation : reservationList) {
				Collection<Integer> pnrSegmentIds = new ArrayList<Integer>();
				loadAirportTransferInfo(reservation);
				Map<Integer, Collection<PaxAirportTransferTO>> cancelledAptMap = new HashMap<Integer, Collection<PaxAirportTransferTO>>();
				if (reservation.getAirportTransfers() != null) {
					for (PaxAirportTransferTO paxAirportTransferTO : reservation.getAirportTransfers()) {
						if (pnrSegIds.contains(paxAirportTransferTO.getPnrSegId())) {
							if (cancelledAptMap.containsKey(paxAirportTransferTO.getPnrPaxId())) {
								cancelledAptMap.get(paxAirportTransferTO.getPnrPaxId()).add(paxAirportTransferTO);
							} else {
								Collection<PaxAirportTransferTO> cancelledApts = new ArrayList<PaxAirportTransferTO>();
								cancelledApts.add(paxAirportTransferTO);
								cancelledAptMap.put(paxAirportTransferTO.getPnrPaxId(), cancelledApts);
								pnrSegmentIds.add(paxAirportTransferTO.getPnrSegId());
							}
						}
					}
					if (pnrSegmentIds.size() > 0) {
						ReservationModuleUtils.getSegmentBD().cancelAirportTransferRequests(reservation, pnrSegmentIds,
								cancelledAptMap, null);
					}
				}
			}
		}
	}

	private static void loadAirportTransferInfo(Reservation reservation) throws ModuleException {
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
				Collection<PaxAirportTransferTO> paxAirportTransferDTOs = null;
				paxAirportTransferDTOs = ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO
						.getReservationAirportTransfersByPaxId(pnrPaxIds);
				reservation.setAirportTransfers(paxAirportTransferDTOs);
			} else {
				throw new ModuleException("airreservations.load.reservation");
			}
		}
	}
	
	private Collection<ReservationSegmentDTO> filterOldResSegListForDryInterline(Collection<ReservationSegmentDTO> OldResSegList) {
		Collection<ReservationSegmentDTO> FilteredOldResSegList = new ArrayList<ReservationSegmentDTO>();
		if (OldResSegList != null && this.pnrSegList != null && this.pnrSegList.size() > 0) {
			Iterator itOldResSegList = OldResSegList.iterator();
			while (itOldResSegList.hasNext()) {
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) itOldResSegList.next();
				if (!this.pnrSegList.contains(reservationSegmentDTO.getPnrSegId())) {
					FilteredOldResSegList.add(reservationSegmentDTO);
				}
			}
			return FilteredOldResSegList;
		} else {
			return OldResSegList;
		}

	}

	private void addReprotectedPassengersToList(Reservation res, Integer fromFlightSegId, Integer toFlightSegId) {

		Collection<ReservationSegmentDTO> reservationSegments = res.getSegmentsView();
		Integer pnrSegIdForReleventFlight = null;

		if (reservationSegments != null && !reservationSegments.isEmpty()) {
			for (ReservationSegmentDTO resSegDTO : reservationSegments) {
				Integer flightSegmentId = resSegDTO.getFlightSegId();
				if (flightSegmentId.equals(fromFlightSegId)) {
					pnrSegIdForReleventFlight = resSegDTO.getPnrSegId();
				}
			}
		}

		if (pnrSegIdForReleventFlight != null) {
			for (ReservationPax pax : res.getPassengers()) {
				ReprotectedPaxDTO reprotectedPax = new ReprotectedPaxDTO();
				Integer paxId = pax.getPnrPaxId();
				reprotectedPax.setFrmFltSegId(fromFlightSegId);
				reprotectedPax.setToFltSegId(toFlightSegId);
				reprotectedPax.setPnrPaxId(paxId);
				reprotectedPax.setPnrSegmentId(pnrSegIdForReleventFlight);
				reprotectedPassengers.add(reprotectedPax);
			}
		}
	}
}
