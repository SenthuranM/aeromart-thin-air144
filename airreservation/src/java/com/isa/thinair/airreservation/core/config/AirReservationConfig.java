/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.core.config;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * Holds Air Reservation related module Configurations
 * 
 * @author Nilindra Fernando, ISuru Nishan, Byorn John
 * @since 1.0
 * @isa.module.config-bean
 */
public class AirReservationConfig extends DefaultModuleConfig {

	/* Mail Server Related */
	private String defaultMailServer;
	private String popMailServer;
	private String defaultMailServerUsername;
	private String defaultMailServerPassword;

	private String ttyMailServer;
	private String ttyMailServerUserName;
	private String ttyMailServerPassword;

	/* PNL / ADL Related */
	private String pnlAdlMailExtension;
	private String genpnlpath;
	private String genadlpath;
	private String sentpnlpath;
	private String sentadlpath;
	private String templatesroot;
	private String retryLimit;
	private String searchRangeInMins;
	private String sitaMessageSender;
	private Properties emailPnlAdlErrorTo;
	private Map<String, String> baggageWeightIndexMap;
	private List<String> bookingClassesToSkipForNoshow;
	private String arincSenderAddress;

	/* PAL / CAL Related */
	private String palCalMailExtension;
	private String genPalPath;
	private String genCalPath;
	private String sentPalPath;
	private String sentcalPath;
	private String palCalRetryLimit;
	private String palCalSearchRangeInMins;
	private String palCalSitaTexSenderAddress;
	private Properties emailPalCalErrorTo;

	/* PFS Related */
	private String pfsProcessPath;
	private String pfsParsedPath;
	private String pfsErrorPath;
	private String pfsPartialDownloadPath;
	private String pfsMailServerUsername;
	private String pfsMailServerPassword;
	private String pfsFetchFromMailServer;
	private String pfsDeleteFromMailServer;
	private Properties emailPfsErrorTo;

	/* XA PNL Related */
	private String xaPnlProcessPath;
	private String xaPnlParsedPath;
	private String xaPnlErrorPath;
	private String xaPnlMailServerUsername;
	private String xaPnlMailServerPassword;
	private String xaFetchPnlFromMailServer;
	private String xaDeleteFromMailServer;
	private String emiratesDefaultAgentCode;
	private Properties emailXAPnlErrorTo;
	private String xaPnlRetryLimit;
	private Map<String, String> xaFlightNumberMap;

	/* CASH/CREDI SALES Related */
	private String transferToSqlServer;
	private String maxNumOfDaysAllowed;

	/* External payment transactions related */
	private Map<String, String> onAccAgentForBankChannelMap;
	private boolean sendEmailAfterExtPayRecon;
	private Map<String, String> extPayReconEmailRecipientsMap;

	/* External charges related */
	private Map<String, String> externalChargesMap;

	private Map<String, String> ancillaryChargeCodesMap;

	/* charge adjustments */
	private Map<String, String> chargeAdjustmentMap;

	private Map<String, String> exchangeExcludeChargesMap;

	/* Bar code related configurations */
	private Map<String, String> barCodeConfMap;
	/* PNR validate related rules */
	private Map<String, String> pnrValidationRulesMap;

	private Map<String, String> pnrGenerationConfigMap;

	/* E Ticket related configurations */
	private Map<String, String> etktConfMap;

	/* Itinerary Related */
	private String itineraryPrintTemplate;
	private String itineraryEmailTemplate;
	private String historyPrintTemplate;

	private String recieptPrintTemplate;

	/* ITINERARY APPOX. PRICE */
	private Map<String, String> ancillaryAppoxPrice;

	/* Interline Itinerary Related */
	private String interlineItineraryPrintTemplate;
	private String interlineItineraryEmailTemplate;
	private Boolean firstDepartingCarrierWiseTNCEnabled;

	/* SMS Related */
	private Collection<String> smsAgents;
	private Collection<String> smsExcludeAgents;
	private Collection<String> smsIDDCode;
	private Collection<String> smsAreaCode;

	/* Interline segment transfer recipients map */
	private Map<String, String> interlineSegTransRecipientsMap;

	/* Tax invoice related */
	private String taxInvoicePrintTemplate;

	/* Alternate Mail server */
	private Map<String, String> alternateMessageSender;

	/* Flight Manifest Related */
	private String flightManifestPrintTemplate;

	/* CCC Insurance Configuration data */
	private Map<String, String> cccInsuranceConfigMap;
	/* Accounting system */
	private String accountingSystem;

	/* calculate CNX charge for discounted fare */
	private boolean cnxChargeForDiscountedFare;
	/* calculate MOD charge for discounted fare */
	private boolean modChargeForDiscountedFare;
	/* allow TBA PAX with PFS processing */
	private boolean processPFSWithTBAPax;
	/* to handle OFF_LOAD PAX same as NO_SHOW PAX, Ex:apply no show charge,CNX segment logic, split reservation logic */
	private boolean processOffLoadPAXAsNoShow;
	/* calculate NO_SHOW charge for discounted fare */
	private boolean noShowChargeForDiscountedFare;

	private String flightSearchDateGap;

	private boolean inventoryMismatchTrackingForOHDRelease;

	/* view fare basis code in the itinerary */
	private boolean viewFareBasisCode;

	private String sitaTexSenderAddress;

	private String airportTransferAirlineEmail;

	private Collection<String> groupBookingEmailRecipientList;

	Map<String, Map<String, String>> modificationDetectorObserverWiseRulesMap;

	private List<String> cashSalesAccountingSystemList;

	private List<String> creditCardSalesAccountingSystemList;

	private List<String> topUpAccountingSystemList;

	public boolean isViewFareBasisCode() {
		return viewFareBasisCode;
	}

	public void setViewFareBasisCode(boolean viewFareBasisCode) {
		this.viewFareBasisCode = viewFareBasisCode;
	}

	/**
	 * @return Returns the defaultMailServer.
	 */
	public String getDefaultMailServer() {
		return defaultMailServer;
	}

	/**
	 * @param defaultMailServer
	 *            The defaultMailServer to set.
	 */
	public void setDefaultMailServer(String defaultMailServer) {
		this.defaultMailServer = defaultMailServer;
	}

	/**
	 * @return Returns the defaultMailServerPassword.
	 */
	public String getDefaultMailServerPassword() {
		return defaultMailServerPassword;
	}

	/**
	 * @param defaultMailServerPassword
	 *            The defaultMailServerPassword to set.
	 */
	public void setDefaultMailServerPassword(String defaultMailServerPassword) {
		this.defaultMailServerPassword = defaultMailServerPassword;
	}

	/**
	 * @return Returns the defaultMailServerUsername.
	 */
	public String getDefaultMailServerUsername() {
		return defaultMailServerUsername;
	}

	/**
	 * @param defaultMailServerUsername
	 *            The defaultMailServerUsername to set.
	 */
	public void setDefaultMailServerUsername(String defaultMailServerUsername) {
		this.defaultMailServerUsername = defaultMailServerUsername;
	}

	/**
	 * @return Returns the emailXAPnlErrorTo.
	 */
	public Properties getEmailXAPnlErrorTo() {
		return emailXAPnlErrorTo;
	}

	/**
	 * @param emailXAPnlErrorTo
	 *            The emailXAPnlErrorTo to set.
	 */
	public void setEmailXAPnlErrorTo(Properties emailXAPnlErrorTo) {
		this.emailXAPnlErrorTo = emailXAPnlErrorTo;
	}

	/**
	 * @return Returns the emiratesDefaultAgentCode.
	 */
	public String getEmiratesDefaultAgentCode() {
		return emiratesDefaultAgentCode;
	}

	/**
	 * @param emiratesDefaultAgentCode
	 *            The emiratesDefaultAgentCode to set.
	 */
	public void setEmiratesDefaultAgentCode(String emiratesDefaultAgentCode) {
		this.emiratesDefaultAgentCode = emiratesDefaultAgentCode;
	}

	/**
	 * @return Returns the genadlpath.
	 */
	public String getGenadlpath() {
		return genadlpath;
	}

	/**
	 * @param genadlpath
	 *            The genadlpath to set.
	 */
	public void setGenadlpath(String genadlpath) {
		this.genadlpath = genadlpath;
	}

	/**
	 * @return Returns the genpnlpath.
	 */
	public String getGenpnlpath() {
		return genpnlpath;
	}

	/**
	 * @param genpnlpath
	 *            The genpnlpath to set.
	 */
	public void setGenpnlpath(String genpnlpath) {
		this.genpnlpath = genpnlpath;
	}

	/**
	 * @return Returns the pfsDeleteFromMailServer.
	 */
	public String getPfsDeleteFromMailServer() {
		return pfsDeleteFromMailServer;
	}

	/**
	 * @param pfsDeleteFromMailServer
	 *            The pfsDeleteFromMailServer to set.
	 */
	public void setPfsDeleteFromMailServer(String pfsDeleteFromMailServer) {
		this.pfsDeleteFromMailServer = pfsDeleteFromMailServer;
	}

	/**
	 * @return Returns the pfsErrorPath.
	 */
	public String getPfsErrorPath() {
		return pfsErrorPath;
	}

	/**
	 * @param pfsErrorPath
	 *            The pfsErrorPath to set.
	 */
	public void setPfsErrorPath(String pfsErrorPath) {
		this.pfsErrorPath = pfsErrorPath;
	}

	/**
	 * @return Returns the pfsFetchFromMailServer.
	 */
	public String getPfsFetchFromMailServer() {
		return pfsFetchFromMailServer;
	}

	/**
	 * @param pfsFetchFromMailServer
	 *            The pfsFetchFromMailServer to set.
	 */
	public void setPfsFetchFromMailServer(String pfsFetchFromMailServer) {
		this.pfsFetchFromMailServer = pfsFetchFromMailServer;
	}

	/**
	 * @return Returns the pfsMailServerPassword.
	 */
	public String getPfsMailServerPassword() {
		return pfsMailServerPassword;
	}

	/**
	 * @param pfsMailServerPassword
	 *            The pfsMailServerPassword to set.
	 */
	public void setPfsMailServerPassword(String pfsMailServerPassword) {
		this.pfsMailServerPassword = pfsMailServerPassword;
	}

	/**
	 * @return Returns the pfsMailServerUsername.
	 */
	public String getPfsMailServerUsername() {
		return pfsMailServerUsername;
	}

	/**
	 * @param pfsMailServerUsername
	 *            The pfsMailServerUsername to set.
	 */
	public void setPfsMailServerUsername(String pfsMailServerUsername) {
		this.pfsMailServerUsername = pfsMailServerUsername;
	}

	/**
	 * @return Returns the pfsParsedPath.
	 */
	public String getPfsParsedPath() {
		return pfsParsedPath;
	}

	/**
	 * @param pfsParsedPath
	 *            The pfsParsedPath to set.
	 */
	public void setPfsParsedPath(String pfsParsedPath) {
		this.pfsParsedPath = pfsParsedPath;
	}

	/**
	 * @return Returns the pfsProcessPath.
	 */
	public String getPfsProcessPath() {
		return pfsProcessPath;
	}

	/**
	 * @param pfsProcessPath
	 *            The pfsProcessPath to set.
	 */
	public void setPfsProcessPath(String pfsProcessPath) {
		this.pfsProcessPath = pfsProcessPath;
	}

	public String getPfsPartialDownloadPath() {
		return pfsPartialDownloadPath;
	}

	public void setPfsPartialDownloadPath(String pfsPartialDownloadPath) {
		this.pfsPartialDownloadPath = pfsPartialDownloadPath;
	}

	/**
	 * @return Returns the pnlAdlMailExtension.
	 */
	public String getPnlAdlMailExtension() {
		return pnlAdlMailExtension;
	}

	/**
	 * @param pnlAdlMailExtension
	 *            The pnlAdlMailExtension to set.
	 */
	public void setPnlAdlMailExtension(String pnlAdlMailExtension) {
		this.pnlAdlMailExtension = pnlAdlMailExtension;
	}

	/**
	 * @return Returns the popMailServer.
	 */
	public String getPopMailServer() {
		return popMailServer;
	}

	/**
	 * @param popMailServer
	 *            The popMailServer to set.
	 */
	public void setPopMailServer(String popMailServer) {
		this.popMailServer = popMailServer;
	}

	/**
	 * @return Returns the retryLimit.
	 */
	public String getRetryLimit() {
		return retryLimit;
	}

	/**
	 * @param retryLimit
	 *            The retryLimit to set.
	 */
	public void setRetryLimit(String retryLimit) {
		this.retryLimit = retryLimit;
	}

	/**
	 * @return Returns the searchRangeInMins.
	 */
	public String getSearchRangeInMins() {
		return searchRangeInMins;
	}

	/**
	 * @param searchRangeInMins
	 *            The searchRangeInMins to set.
	 */
	public void setSearchRangeInMins(String searchRangeInMins) {
		this.searchRangeInMins = searchRangeInMins;
	}

	/**
	 * @return Returns the sentadlpath.
	 */
	public String getSentadlpath() {
		return sentadlpath;
	}

	/**
	 * @param sentadlpath
	 *            The sentadlpath to set.
	 */
	public void setSentadlpath(String sentadlpath) {
		this.sentadlpath = sentadlpath;
	}

	/**
	 * @return Returns the sentpnlpath.
	 */
	public String getSentpnlpath() {
		return sentpnlpath;
	}

	/**
	 * @param sentpnlpath
	 *            The sentpnlpath to set.
	 */
	public void setSentpnlpath(String sentpnlpath) {
		this.sentpnlpath = sentpnlpath;
	}

	/**
	 * @return Returns the templatesroot.
	 */
	public String getTemplatesroot() {
		return templatesroot;
	}

	/**
	 * @param templatesroot
	 *            The templatesroot to set.
	 */
	public void setTemplatesroot(String templatesroot) {
		this.templatesroot = templatesroot;
	}

	/**
	 * @return Returns the xaDeleteFromMailServer.
	 */
	public String getXaDeleteFromMailServer() {
		return xaDeleteFromMailServer;
	}

	/**
	 * @param xaDeleteFromMailServer
	 *            The xaDeleteFromMailServer to set.
	 */
	public void setXaDeleteFromMailServer(String xaDeleteFromMailServer) {
		this.xaDeleteFromMailServer = xaDeleteFromMailServer;
	}

	/**
	 * @return Returns the xaFetchPnlFromMailServer.
	 */
	public String getXaFetchPnlFromMailServer() {
		return xaFetchPnlFromMailServer;
	}

	/**
	 * @param xaFetchPnlFromMailServer
	 *            The xaFetchPnlFromMailServer to set.
	 */
	public void setXaFetchPnlFromMailServer(String xaFetchPnlFromMailServer) {
		this.xaFetchPnlFromMailServer = xaFetchPnlFromMailServer;
	}

	/**
	 * @return Returns the xaPnlErrorPath.
	 */
	public String getXaPnlErrorPath() {
		return xaPnlErrorPath;
	}

	/**
	 * @param xaPnlErrorPath
	 *            The xaPnlErrorPath to set.
	 */
	public void setXaPnlErrorPath(String xaPnlErrorPath) {
		this.xaPnlErrorPath = xaPnlErrorPath;
	}

	/**
	 * @return Returns the xaPnlMailServerPassword.
	 */
	public String getXaPnlMailServerPassword() {
		return xaPnlMailServerPassword;
	}

	/**
	 * @param xaPnlMailServerPassword
	 *            The xaPnlMailServerPassword to set.
	 */
	public void setXaPnlMailServerPassword(String xaPnlMailServerPassword) {
		this.xaPnlMailServerPassword = xaPnlMailServerPassword;
	}

	/**
	 * @return Returns the xaPnlMailServerUsername.
	 */
	public String getXaPnlMailServerUsername() {
		return xaPnlMailServerUsername;
	}

	/**
	 * @param xaPnlMailServerUsername
	 *            The xaPnlMailServerUsername to set.
	 */
	public void setXaPnlMailServerUsername(String xaPnlMailServerUsername) {
		this.xaPnlMailServerUsername = xaPnlMailServerUsername;
	}

	/**
	 * @return Returns the xaPnlParsedPath.
	 */
	public String getXaPnlParsedPath() {
		return xaPnlParsedPath;
	}

	/**
	 * @param xaPnlParsedPath
	 *            The xaPnlParsedPath to set.
	 */
	public void setXaPnlParsedPath(String xaPnlParsedPath) {
		this.xaPnlParsedPath = xaPnlParsedPath;
	}

	/**
	 * @return Returns the xaPnlProcessPath.
	 */
	public String getXaPnlProcessPath() {
		return xaPnlProcessPath;
	}

	/**
	 * @param xaPnlProcessPath
	 *            The xaPnlProcessPath to set.
	 */
	public void setXaPnlProcessPath(String xaPnlProcessPath) {
		this.xaPnlProcessPath = xaPnlProcessPath;
	}

	/**
	 * @return Returns the xaPnlRetryLimit.
	 */
	public String getXaPnlRetryLimit() {
		return xaPnlRetryLimit;
	}

	/**
	 * @param xaPnlRetryLimit
	 *            The xaPnlRetryLimit to set.
	 */
	public void setXaPnlRetryLimit(String xaPnlRetryLimit) {
		this.xaPnlRetryLimit = xaPnlRetryLimit;
	}

	/**
	 * @return Returns the xaFlightNumberMap.
	 */
	public Map<String, String> getXaFlightNumberMap() {
		return xaFlightNumberMap;
	}

	/**
	 * @param xaFlightNumberMap
	 *            The xaFlightNumberMap to set.
	 */
	public void setXaFlightNumberMap(Map<String, String> xaFlightNumberMap) {
		this.xaFlightNumberMap = xaFlightNumberMap;
	}

	/**
	 * @return Returns the emailPfsErrorTo.
	 */
	public Properties getEmailPfsErrorTo() {
		return emailPfsErrorTo;
	}

	/**
	 * @param emailPfsErrorTo
	 *            The emailPfsErrorTo to set.
	 */
	public void setEmailPfsErrorTo(Properties emailPfsErrorTo) {
		this.emailPfsErrorTo = emailPfsErrorTo;
	}

	/**
	 * @return Returns the transferToSqlServer.
	 */
	public String getTransferToSqlServer() {
		return transferToSqlServer;
	}

	/**
	 * @param transferToSqlServer
	 *            The transferToSqlServer to set.
	 */
	public void setTransferToSqlServer(String transferToSqlServer) {
		this.transferToSqlServer = transferToSqlServer;
	}

	/**
	 * @return Returns the sitaMessageSender.
	 */
	public String getSitaMessageSender() {
		return sitaMessageSender;
	}

	/**
	 * @param sitaMessageSender
	 *            The sitaMessageSender to set.
	 */
	public void setSitaMessageSender(String sitaMessageSender) {
		this.sitaMessageSender = sitaMessageSender;
	}

	/**
	 * @return Returns the emailPnlAdlErrorTo.
	 */
	public Properties getEmailPnlAdlErrorTo() {
		return emailPnlAdlErrorTo;
	}

	/**
	 * @param emailPnlAdlErrorTo
	 *            The emailPnlAdlErrorTo to set.
	 */
	public void setEmailPnlAdlErrorTo(Properties emailPnlAdlErrorTo) {
		this.emailPnlAdlErrorTo = emailPnlAdlErrorTo;
	}

	/**
	 * @return Returns the maxNumOfDaysAllowed.
	 */
	public String getMaxNumOfDaysAllowed() {
		return maxNumOfDaysAllowed;
	}

	/**
	 * @param maxNumOfDaysAllowed
	 *            The maxNumOfDaysAllowed to set.
	 */
	public void setMaxNumOfDaysAllowed(String maxNumOfDaysAllowed) {
		this.maxNumOfDaysAllowed = maxNumOfDaysAllowed;
	}

	/**
	 * @return Returns the Map<bankChannelCode, onAccAgentCode>
	 */
	public Map<String, String> getOnAccAgentForBankChannelMap() {
		return onAccAgentForBankChannelMap;
	}

	public void setOnAccAgentForBankChannelMap(Map<String, String> onAccAgentForBankChannelMap) {
		this.onAccAgentForBankChannelMap = onAccAgentForBankChannelMap;
	}

	/**
	 * @return Returns the itineraryPrintTemplate.
	 */
	public String getItineraryPrintTemplate() {
		return itineraryPrintTemplate;
	}

	/**
	 * @param itineraryPrintTemplate
	 *            The itineraryPrintTemplate to set.
	 */
	public void setItineraryPrintTemplate(String itineraryTemplate) {
		this.itineraryPrintTemplate = itineraryTemplate;
	}

	/**
	 * @return Returns the interlineItineraryPrintTemplate.
	 */
	public String getInterlineItineraryPrintTemplate() {
		return interlineItineraryPrintTemplate;
	}

	/**
	 * @param interlineItineraryTemplate
	 *            The interline itinerary template to set.
	 */
	public void setInterlineItineraryPrintTemplate(String interlineItineraryTemplate) {
		this.interlineItineraryPrintTemplate = interlineItineraryTemplate;
	}

	public String getHistoryPrintTemplate() {
		return historyPrintTemplate;
	}

	public void setHistoryPrintTemplate(String historyPrintTemplate) {
		this.historyPrintTemplate = historyPrintTemplate;
	}

	/**
	 * @return Returns the interlineItineraryEmailTemplate.
	 */
	public String getInterlineItineraryEmailTemplate() {
		return interlineItineraryEmailTemplate;
	}

	/**
	 * @param interlineItineraryTemplate
	 *            The interline itinerary template to set.
	 */
	public void setInterlineItineraryEmailTemplate(String interlineItineraryTemplate) {
		this.interlineItineraryEmailTemplate = interlineItineraryTemplate;
	}

	/**
	 * @return Returns the firstDepartingCarrierWiseTNCEnabled.
	 */
	public Boolean getFirstDepartingCarrierWiseTNCEnabled() {
		return firstDepartingCarrierWiseTNCEnabled;
	}

	/**
	 * @param firstDepartingCarrierWiseTNCEnabled
	 *            The first Departing CarrierWiseTNCEnabled to set.
	 */
	public void setFirstDepartingCarrierWiseTNCEnabled(Boolean firstDepartingCarrierWiseTNCEnabled) {
		this.firstDepartingCarrierWiseTNCEnabled = firstDepartingCarrierWiseTNCEnabled;
	}

	/**
	 * @return Returns the itineraryEmailTemplate.
	 */
	public String getItineraryEmailTemplate() {
		return itineraryEmailTemplate;
	}

	/**
	 * @param itineraryEmailTemplate
	 *            The itineraryEmailTemplate to set.
	 */
	public void setItineraryEmailTemplate(String itineraryEmailTemplate) {
		this.itineraryEmailTemplate = itineraryEmailTemplate;
	}

	/**
	 * @return Returns the externalChargesMap.
	 */
	public Map<String, String> getExternalChargesMap() {
		return externalChargesMap;
	}

	/**
	 * @param externalChargesMap
	 *            The externalChargesMap to set.
	 */
	public void setExternalChargesMap(Map<String, String> externalChargesMap) {
		this.externalChargesMap = externalChargesMap;
	}

	public Map<String, String> getExchangeExcludeChargesMap() {
		return exchangeExcludeChargesMap;
	}

	public void setExchangeExcludeChargesMap(Map<String, String> exchangeExcludeChargesMap) {
		this.exchangeExcludeChargesMap = exchangeExcludeChargesMap;
	}

	/**
	 * @return
	 */
	public Map<String, String> getChargeAdjustmentMap() {
		return chargeAdjustmentMap;
	}

	/**
	 * @param chargeAdjustmentMap
	 */
	public void setChargeAdjustmentMap(Map<String, String> chargeAdjustmentMap) {
		this.chargeAdjustmentMap = chargeAdjustmentMap;
	}

	/**
	 * @return sms agents
	 */
	public Collection<String> getSmsAgents() {
		return smsAgents;
	}

	/**
	 * @param smsAgents
	 *            the sms agents to set
	 */
	public void setSmsAgents(Collection<String> smsAgents) {
		this.smsAgents = smsAgents;
	}

	/**
	 * @return Returns the barCodeConfMap.
	 */
	public Map<String, String> getBarCodeConfMap() {
		return barCodeConfMap;
	}

	/**
	 * @param barCodeConfMap
	 *            The barCodeConfMap to set.
	 */
	public void setBarCodeConfMap(Map<String, String> barCodeConfMap) {
		this.barCodeConfMap = barCodeConfMap;
	}

	/**
	 * @return Returns the extPayReconEmailRecipientsMap.
	 */
	public Map<String, String> getExtPayReconEmailRecipientsMap() {
		return extPayReconEmailRecipientsMap;
	}

	/**
	 * @param extPayReconEmailRecipientsMap
	 *            The extPayReconEmailRecipientsMap to set.
	 */
	public void setExtPayReconEmailRecipientsMap(Map<String, String> extPayReconEmailRecipientsMap) {
		this.extPayReconEmailRecipientsMap = extPayReconEmailRecipientsMap;
	}

	/**
	 * @return Returns the sendEmailAfterExtPayRecon.
	 */
	public boolean isSendEmailAfterExtPayRecon() {
		return sendEmailAfterExtPayRecon;
	}

	/**
	 * @param sendEmailAfterExtPayRecon
	 *            The sendEmailAfterExtPayRecon to set.
	 */
	public void setSendEmailAfterExtPayRecon(boolean sendEmailAfterExtPayRecon) {
		this.sendEmailAfterExtPayRecon = sendEmailAfterExtPayRecon;
	}

	/**
	 * @return sms Exclude agents
	 */
	public Collection<String> getSmsExcludeAgents() {
		return smsExcludeAgents;
	}

	/**
	 * @param smsExcludeAgents
	 *            the sms exclude agents to set
	 */
	public void setSmsExcludeAgents(Collection<String> smsExcludeAgents) {
		this.smsExcludeAgents = smsExcludeAgents;
	}

	/**
	 * @return sms IDD Code
	 */
	public Collection<String> getSmsIDDCode() {
		return smsIDDCode;
	}

	/**
	 * @param smsIDDCode
	 *            the sms IDD Code to set
	 */
	public void setSmsIDDCode(Collection<String> smsIDDCode) {
		this.smsIDDCode = smsIDDCode;
	}

	/**
	 * @return sms Area Code
	 */
	public Collection<String> getSmsAreaCode() {
		return smsAreaCode;
	}

	/**
	 * @param smsAreaCode
	 *            the sms Area Code to set
	 */
	public void setSmsAreaCode(Collection<String> smsAreaCode) {
		this.smsAreaCode = smsAreaCode;
	}

	/**
	 * @return Returns the interlineSegTransRecipientsMap.
	 */
	public Map<String, String> getInterlineSegTransRecipientsMap() {
		return interlineSegTransRecipientsMap;
	}

	/**
	 * @param interlineSegTransRecipientsMap
	 *            The interlineSegTransRecipientsMap to set.
	 */
	public void setInterlineSegTransRecipientsMap(Map<String, String> interlineSegTransRecipientsMap) {
		this.interlineSegTransRecipientsMap = interlineSegTransRecipientsMap;
	}

	/**
	 * @return Returns the etktConfMap.
	 */
	public Map<String, String> getEtktConfMap() {
		return etktConfMap;
	}

	/**
	 * @param etktConfMap
	 *            The etktConfMap to set.
	 */
	public void setEtktConfMap(Map<String, String> etktConfMap) {
		this.etktConfMap = etktConfMap;
	}

	/**
	 * @return the pnrValidationRulesMap
	 */
	public Map<String, String> getPnrValidationRulesMap() {
		return pnrValidationRulesMap;
	}

	/**
	 * @param pnrValidationRulesMap
	 *            the pnrValidationRulesMap to set
	 */
	public void setPnrValidationRulesMap(Map<String, String> pnrValidationRulesMap) {
		this.pnrValidationRulesMap = pnrValidationRulesMap;
	}

	/**
	 * @return Returns the alternateMessageSender.
	 */
	public Map<String, String> getAlternateMessageSender() {
		return alternateMessageSender;
	}

	/**
	 * @param alternateMessageSender
	 *            The alternateMessageSender to set.
	 */
	public void setAlternateMessageSender(Map<String, String> alternateMessageSender) {
		this.alternateMessageSender = alternateMessageSender;
	}

	/**
	 * @return the recieptPrintTemplate
	 */
	public String getRecieptPrintTemplate() {
		return recieptPrintTemplate;
	}

	/**
	 * @param recieptPrintTemplate
	 *            the recieptPrintTemplate to set
	 */
	public void setRecieptPrintTemplate(String recieptPrintTemplate) {
		this.recieptPrintTemplate = recieptPrintTemplate;
	}

	/**
	 * @return the ancillaryAppoxPrice
	 */
	public Map<String, String> getAncillaryAppoxPrice() {
		return ancillaryAppoxPrice;
	}

	/**
	 * @param ancillaryAppoxPrice
	 *            the ancillaryAppoxPrice to set
	 */
	public void setAncillaryAppoxPrice(Map<String, String> ancillaryAppoxPrice) {
		this.ancillaryAppoxPrice = ancillaryAppoxPrice;
	}

	/**
	 * 
	 * @return
	 */
	public String getFlightManifestPrintTemplate() {
		return flightManifestPrintTemplate;
	}

	public String getAirportTransferAirlineEmail() {
		return airportTransferAirlineEmail;
	}

	public void setAirportTransferAirlineEmail(String airportTransferAirlineEmail) {
		this.airportTransferAirlineEmail = airportTransferAirlineEmail;
	}

	/**
	 * 
	 * @param flightManifestPrintTemplate
	 */
	public void setFlightManifestPrintTemplate(String flightManifestPrintTemplate) {
		this.flightManifestPrintTemplate = flightManifestPrintTemplate;
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getCccInsuranceConfigMap() {
		return cccInsuranceConfigMap;
	}

	/**
	 * 
	 * @param cccInsuranceConfigMap
	 */
	public void setCccInsuranceConfigMap(Map<String, String> cccInsuranceConfigMap) {
		this.cccInsuranceConfigMap = cccInsuranceConfigMap;
	}

	public Map<String, String> getBaggageWeightIndexMap() {
		return baggageWeightIndexMap;
	}

	public void setBaggageWeightIndexMap(Map<String, String> baggageWeightIndexMap) {
		this.baggageWeightIndexMap = baggageWeightIndexMap;
	}

	public String getAccountingSystem() {
		return accountingSystem;
	}

	public void setAccountingSystem(String accountingSystem) {
		this.accountingSystem = accountingSystem;
	}

	public List<String> getBookingClassesToSkipForNoshow() {
		return bookingClassesToSkipForNoshow;
	}

	public void setBookingClassesToSkipForNoshow(List<String> bookingClassesToSkipForNoshow) {
		this.bookingClassesToSkipForNoshow = bookingClassesToSkipForNoshow;
	}

	public void setPnrGenerationConfigMap(Map<String, String> pnrGenerationConfigMap) {
		this.pnrGenerationConfigMap = pnrGenerationConfigMap;
	}

	public Map<String, String> getPnrGenerationConfigMap() {
		return pnrGenerationConfigMap;
	}

	/**
	 * @return the ancillaryChargeCodesMap
	 */
	public Map<String, String> getAncillaryChargeCodesMap() {
		return ancillaryChargeCodesMap;
	}

	/**
	 * @param ancillaryChargeCodesMap
	 *            the ancillaryChargeCodesMap to set
	 */
	public void setAncillaryChargeCodesMap(Map<String, String> ancillaryChargeCodesMap) {
		this.ancillaryChargeCodesMap = ancillaryChargeCodesMap;
	}

	public boolean isCnxChargeForDiscountedFare() {
		return cnxChargeForDiscountedFare;
	}

	public void setCnxChargeForDiscountedFare(boolean cnxChargeForDiscountedFare) {
		this.cnxChargeForDiscountedFare = cnxChargeForDiscountedFare;
	}

	public boolean isModChargeForDiscountedFare() {
		return modChargeForDiscountedFare;
	}

	public void setModChargeForDiscountedFare(boolean modChargeForDiscountedFare) {
		this.modChargeForDiscountedFare = modChargeForDiscountedFare;
	}

	public boolean isProcessPFSWithTBAPax() {
		return processPFSWithTBAPax;
	}

	public void setProcessPFSWithTBAPax(boolean processPFSWithTBAPax) {
		this.processPFSWithTBAPax = processPFSWithTBAPax;
	}

	public boolean isProcessOffLoadPAXAsNoShow() {
		return processOffLoadPAXAsNoShow;
	}

	public void setProcessOffLoadPAXAsNoShow(boolean processOffLoadPAXAsNoShow) {
		this.processOffLoadPAXAsNoShow = processOffLoadPAXAsNoShow;
	}

	public String getTtyMailServer() {
		return ttyMailServer;
	}

	public void setTtyMailServer(String ttyMailServer) {
		this.ttyMailServer = ttyMailServer;
	}

	public String getTtyMailServerUserName() {
		return ttyMailServerUserName;
	}

	public void setTtyMailServerUserName(String ttyMailServerUserName) {
		this.ttyMailServerUserName = ttyMailServerUserName;
	}

	public String getTtyMailServerPassword() {
		return ttyMailServerPassword;
	}

	public void setTtyMailServerPassword(String ttyMailServerPassword) {
		this.ttyMailServerPassword = ttyMailServerPassword;
	}

	public boolean isNoShowChargeForDiscountedFare() {
		return noShowChargeForDiscountedFare;
	}

	public void setNoShowChargeForDiscountedFare(boolean noShowChargeForDiscountedFare) {
		this.noShowChargeForDiscountedFare = noShowChargeForDiscountedFare;
	}

	public String getFlightSearchDateGap() {
		return flightSearchDateGap;
	}

	public void setFlightSearchDateGap(String flightSearchDateGap) {
		this.flightSearchDateGap = flightSearchDateGap;
	}

	public boolean getInventoryMismatchTrackingForOHDRelease() {
		return inventoryMismatchTrackingForOHDRelease;
	}

	public void setInventoryMismatchTrackingForOHDRelease(boolean inventoryMismatchTrackingForOHDRelease) {
		this.inventoryMismatchTrackingForOHDRelease = inventoryMismatchTrackingForOHDRelease;
	}

	public String getSitaTexSenderAddress() {
		return sitaTexSenderAddress;
	}

	public void setSitaTexSenderAddress(String sitaTexSenderAddress) {
		this.sitaTexSenderAddress = sitaTexSenderAddress;
	}

	public Collection<String> getGroupBookingEmailRecipientList() {
		return groupBookingEmailRecipientList;
	}

	public void setGroupBookingEmailRecipientList(Collection<String> groupBookingEmailRecipientList) {
		this.groupBookingEmailRecipientList = groupBookingEmailRecipientList;
	}

	public String getPalCalMailExtension() {
		return palCalMailExtension;
	}

	public void setPalCalMailExtension(String palCalMailExtension) {
		this.palCalMailExtension = palCalMailExtension;
	}

	public String getGenPalPath() {
		return genPalPath;
	}

	public void setGenPalPath(String genPalPath) {
		this.genPalPath = genPalPath;
	}

	public String getGenCalPath() {
		return genCalPath;
	}

	public void setGenCalPath(String genCalPath) {
		this.genCalPath = genCalPath;
	}

	public String getSentPalPath() {
		return sentPalPath;
	}

	public void setSentPalPath(String sentPalPath) {
		this.sentPalPath = sentPalPath;
	}

	public String getSentcalPath() {
		return sentcalPath;
	}

	public void setSentcalPath(String sentcalPath) {
		this.sentcalPath = sentcalPath;
	}

	public String getPalCalRetryLimit() {
		return palCalRetryLimit;
	}

	public void setPalCalRetryLimit(String palCalRetryLimit) {
		this.palCalRetryLimit = palCalRetryLimit;
	}

	public String getPalCalSearchRangeInMins() {
		return palCalSearchRangeInMins;
	}

	public void setPalCalSearchRangeInMins(String palCalSearchRangeInMins) {
		this.palCalSearchRangeInMins = palCalSearchRangeInMins;
	}

	public String getPalCalSitaTexSenderAddress() {
		return palCalSitaTexSenderAddress;
	}

	public void setPalCalSitaTexSenderAddress(String palCalSitaTexSenderAddress) {
		this.palCalSitaTexSenderAddress = palCalSitaTexSenderAddress;
	}

	public Properties getEmailPalCalErrorTo() {
		return emailPalCalErrorTo;
	}

	public void setEmailPalCalErrorTo(Properties emailPalCalErrorTo) {
		this.emailPalCalErrorTo = emailPalCalErrorTo;
	}

	public Map<String, Map<String, String>> getModificationDetectorObserverWiseRulesMap() {
		return modificationDetectorObserverWiseRulesMap;
	}

	public void setModificationDetectorObserverWiseRulesMap(
			Map<String, Map<String, String>> modificationDetectorObserverWiseRulesMap) {
		this.modificationDetectorObserverWiseRulesMap = modificationDetectorObserverWiseRulesMap;
	}

	public String getTaxInvoicePrintTemplate() {
		return taxInvoicePrintTemplate;
	}

	public void setTaxInvoicePrintTemplate(String taxInvoicePrintTemplate) {
		this.taxInvoicePrintTemplate = taxInvoicePrintTemplate;
	}

	public String getArincSenderAddress() {
		return arincSenderAddress;
	}

	public void setArincSenderAddress(String arincSenderAddress) {
		this.arincSenderAddress = arincSenderAddress;
	}

	public List<String> getCashSalesAccountingSystemList() {
		return cashSalesAccountingSystemList;
	}

	public void setCashSalesAccountingSystemList(List<String> cashSalesAccountingSystemList) {
		this.cashSalesAccountingSystemList = cashSalesAccountingSystemList;
	}

	public List<String> getCreditCardSalesAccountingSystemList() {
		return creditCardSalesAccountingSystemList;
	}

	public void setCreditCardSalesAccountingSystemList(List<String> creditCardSalesAccountingSystemList) {
		this.creditCardSalesAccountingSystemList = creditCardSalesAccountingSystemList;
	}

	public List<String> getTopUpAccountingSystemList() {
		return topUpAccountingSystemList;
	}

	public void setTopUpAccountingSystemList(List<String> topUpAccountingSystemList) {
		this.topUpAccountingSystemList = topUpAccountingSystemList;
	}

}
