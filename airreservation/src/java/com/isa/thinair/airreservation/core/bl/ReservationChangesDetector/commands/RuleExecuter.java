package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @isa.module.command name="ruleExecuter"
 * 
 */
public class RuleExecuter extends DefaultBaseCommand {

	private ResModifyDtectorDataContext dataContext;

	private Map<String, RuleResponseDTO> ruleWiseResponseMap = new HashMap<>();

	private List<ResModifyIdentificationRule<RuleExecuterDatacontext>> optimizedRuleList;

	@Override
	public ServiceResponce execute() throws ModuleException {
		resolveCommandParameters();
		executeRules();

		return createResponseObject();
	}

	private void executeRules() throws ModuleException {

		RuleExecuterDatacontext ruleDataContext = createRuleDataContext();

		for (ResModifyIdentificationRule<RuleExecuterDatacontext> rule : optimizedRuleList) {
			RuleResponseDTO response = rule.executeRule(ruleDataContext);
			if (response.hasEffectedPax()) {
				ruleWiseResponseMap.put(rule.getRuleRef(), response);
			}
		}
	}

	private RuleExecuterDatacontext createRuleDataContext() throws ModuleException {

		RuleExecuterDatacontext ruleDataContext = new RuleExecuterDatacontext();
		ruleDataContext.setExisitingReservation(dataContext.getExisitingReservation());
		ruleDataContext.setUpdatedReservation(dataContext.getUpdatedReservation());

		return ruleDataContext;
	}

	private ServiceResponce createResponseObject() {
		ServiceResponce response = new DefaultServiceResponse();
		dataContext.setRuleWiseResponseMap(ruleWiseResponseMap);
		response.addResponceParam(CommandParamNames.RES_MODIFY_DETECT_DATA_CONTEXT, dataContext);
		return response;
	}

	private void resolveCommandParameters() {
		this.dataContext = (ResModifyDtectorDataContext) this.getParameter(CommandParamNames.RES_MODIFY_DETECT_DATA_CONTEXT);
		this.optimizedRuleList = (List<ResModifyIdentificationRule<RuleExecuterDatacontext>>) this
				.getParameter(CommandParamNames.RES_MODIFY_OPTIMIZED_RULE_LIST);

	}
}
