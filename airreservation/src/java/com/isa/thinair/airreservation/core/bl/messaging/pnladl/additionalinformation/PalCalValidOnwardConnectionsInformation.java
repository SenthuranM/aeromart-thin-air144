package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

import java.util.ArrayList;
import java.util.List;

public class PalCalValidOnwardConnectionsInformation extends OnwardConnectionsInformation {
	public PalCalValidOnwardConnectionsInformation(String departureAirportCode, Integer flightId) {
		super(departureAirportCode, flightId);
	}


	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformations) {

		for (PassengerInformation passengerInformation : passengerInformations) {
			ArrayList<OnWardConnectionDTO> onwardConnections = (ArrayList<OnWardConnectionDTO>) reservationAuxilliaryDAO
					.getAllOutBoundSequences(
							passengerInformation.getPnr(),
							passengerInformation.getPnrPaxId(),
							departureAirportCode,
							flightId,
							passengerInformation.getPnrStatus().equalsIgnoreCase("CNF"));
			this.onwardConnectionsInformation.put(
					passengerInformation.getPnrPaxId(), onwardConnections);
		}
	}
}
