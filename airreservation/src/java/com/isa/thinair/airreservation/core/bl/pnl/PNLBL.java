/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 * 
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlight;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegment;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSFlightSegmentMapElement;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassenger;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSPassportInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.DCSVisaInformation;
import com.isa.connectivity.profiles.dcs.v1.common.dto.PassengersMapElement;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRS;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.RemarksElementDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.pnl.PNLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PNLRecordDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCountRecordDTO;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PNLADLElements;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PNLADLStatusCode;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdlDeliveryInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.util.StringBufferedHandler;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.SSRCode;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * This class includes all the business logic for ADL Generations
 * 
 * @author Byorn
 */
public class PNLBL {

	/** Holds the Current Time..set by constructor **/
	private final Timestamp currentTimeStamp;

	/** Holds the current date.. set by constructor **/
	private final Date currentDate;

	/** Holds the ADL Passenger List...set in generateADL method **/
	private Collection<ReservationPaxDetailsDTO> pnlCollectionTobeUpdated;

	/** Generated ADL Files set in generatePNL method, used for logging **/
	private ArrayList<String> generatedPnlfileNames = new ArrayList<String>();

	/** Per PNR number of pax ids **/
	private final ArrayList<Integer> paxIdsPerRecord = new ArrayList<Integer>();

	/** AuxDAO set in constructor **/
	private ReservationAuxilliaryDAO auxilliaryDAO = null;

	private final ETicketDAO eTicketDAO;

	/** Holds the message type "PNL" **/
	private String msgType = null;

	/** Holds the Total Number of Passengers in the flight (i.e for all cabic classes also **/
	// private int paxCount=0;
	private HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();

	private String lastGroupCode = new String();

	private final HashMap<String, String> mapPnrPaxSegIDGroupCode = new HashMap<String, String>();

	private static Log log = LogFactory.getLog(PNLBL.class);

	private Set<String> pnlPnrs = new HashSet<String>();

	public PNLBL() {

		this.auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		this.eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		this.currentDate = new Date();
		this.currentTimeStamp = new Timestamp(currentDate.getTime());
		this.msgType = PNLConstants.PNLGeneration.PNL;
	}

	/**
	 * NOTE: --------------------------------------------------------------------------- This Operation is used by
	 * Scheduled Service Only. If any errors occurs in this operation, the eror will be thrown and any updates to
	 * passenger table will get rolled back.
	 * 
	 * Insertion to History runs in a separate transaction, a history record will always be maintained. *
	 * 
	 * This enables the Resender/Manual UI to search the error pnl And getting the passengers for the PNL will not be a
	 * problem.
	 * 
	 * Email will be sent of the error. ---------------------------------------------------------------------------
	 * 
	 * @param flightId
	 * @param depAirportCode
	 * @param mdto
	 * @throws ModuleException
	 */
	@Deprecated
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendPNLDup(int flightId, String depAirportCode) throws ModuleException {

		synchronized (this) {
			log.debug("######### Starting to Send Pnl FlightID: " + flightId + " Airport : " + depAirportCode);
			// holds the flight number
			String flightNumber = "";
			// holds the exception to be logged, if occured
			ModuleException exceptiontoLog = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap sitaStatusMap = new HashMap();
			// generated files content
			String fileContent = null;
			// holds the flt local date
			Date fltLocalDAte = null;
			try {

				// There might be scenarios where flight get canceled and reprotected passengers to a new flight and
				// PNL/ADL jobs already scheduled for old flight. This is to check and validate flight id
				FlightBD flightBD = ReservationModuleUtils.getFlightBD();
				Flight flight = flightBD.getFlight(flightId);
				if (flight != null && flight.getStatus().equals(FlightStatusEnum.CANCELLED.getCode())) {
					log.info(" #####################     FLIGHT CANCELLED AND LOADING NEW FLIGHT ID : " + flightId);
					Integer actFlightId = null;
					for (FlightSegement fs : (flight.getFlightSegements())) {
						actFlightId = flightBD.getFlightID(fs.getSegmentCode(), flight.getFlightNumber(),
								fs.getEstTimeDepatureZulu());
						if (actFlightId != null) {
							flightId = actFlightId;
							log.info(" #####################     NEW FLIGHT ID TO SEND PNL : " + flightId);
							break;
						} else {
							log.info(" #####################     UNABLE TO FIND NEW FLIGHT ID AND USING OLD ONE : " + flightId);
						}
					}
				}
				// get flight number
				flightNumber = auxilliaryDAO.getFlightNumber(flightId);
				log.debug("######### Retrieved Flight Number For: " + flightId + " Airport : " + depAirportCode + " FlightNum :"
						+ flightNumber);

				// get Carrier code from flight number
				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);

				// get a list of sita addresses
				log.debug("######### Retrieved SITA List For: " + flightId + " Airport : " + depAirportCode + " Carrier code :"
						+ carrierCode);
				
				Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(depAirportCode, legNumberWiseOndMap);
				
				Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(depAirportCode.trim(),
						flightNumber, ond, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
				List<String> sitaAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				List<String> arincAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
				List<String> sitaAddressAll = new ArrayList<String>();
				sitaAddressAll.addAll(sitaAddresses);
				sitaAddressAll.addAll(sitaTexAddress);
				sitaAddressAll.addAll(arincAddresses);

				// populate map with sita address, and failure status.
				PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddressAll, null,arincAddresses);

				// check if flight already closed
				checkPnlTimeExceedsFlightClosureTime(flightId);
				log.debug("######### Checked If Flight Already Closed FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);

				// get pax list

				Collection<PNLADLDestinationDTO> pnlRecordList = auxilliaryDAO.getPNL(flightId, depAirportCode,
						AppSysParamsUtil.extractCarrierCode(flightNumber));
				log.debug("######### Retrieved Pnl Record List FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);

				if (pnlRecordList != null) {
					// prepare PNLMetaDataDTO for generatePNL(), i.e vel template
					// IA 23Nov2009 Changint to facilitate multileg local depature timings
					fltLocalDAte = auxilliaryDAO.getFlightLocalDate(flightId, depAirportCode);

					String[] monthDay = PnlAdlUtil.getMonthDayArray(fltLocalDAte);
					PNLMetaDataDTO pnlmetadatadto = new PNLMetaDataDTO();
					pnlmetadatadto.setBoardingairport(depAirportCode);
					pnlmetadatadto.setDay(monthDay[1]);
					pnlmetadatadto.setFlight(flightNumber);
					pnlmetadatadto.setMessageIdentifier(msgType);
					pnlmetadatadto.setMonth(monthDay[0]);
					pnlmetadatadto.setPartnumber(PNLConstants.PNLGeneration.PART_ONE);
					pnlmetadatadto.setPart2(true);

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						pnlmetadatadto.setRbd(PnlAdlUtil.generateRBDValues(flightId));
						pnlmetadatadto.setRbdEnabled(true);
					}

					// generate the pnl
					log.debug("######### Before Calling Generate PNL For FID: " + flightId + " Airport : " + depAirportCode
							+ " FlightNum :" + flightNumber);

					try {
						if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
							Airport airport = ReservationModuleUtils.getAirportBD().getAirport(depAirportCode);

							if (airport.isWSAADCSEnabled()) {
								Flight flightObj = flightBD.getFlight(flightId);
								pnlmetadatadto.setOriginator(flight.getOriginAptCode());
								Date departureDateZulu = null;
								if (flightObj != null) {
									for (FlightSegement fs : (flightObj.getFlightSegements())) {
										if (fs.getEstTimeDepatureZulu() != null && fs.getSegmentCode().startsWith(depAirportCode)) {
											departureDateZulu = fs.getEstTimeDepatureZulu();
											break;
										}
									}
								}
								sendPNLToDCS(pnlRecordList, pnlmetadatadto, departureDateZulu, flightId);
							}
							// update the pnl status
							// updatePNLStatus(pnlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode);
						}
					} catch (Exception e) {
						// sending PNL/ADL to DCS failed
						log.error("######### Error sendPNLToDCS for Flight Id : " + flightId);
						updateHistory(pnlmetadatadto.getBoardingairport(), flightId, PNLConstants.SITAMsgTransmission.FAIL);
					}

					// Normal Sita PNL sending
					generatePNL(pnlRecordList, pnlmetadatadto, depAirportCode, flightId);
					log.debug("######### Generated OK!");

					// get sita address status map..each part file is mailed to each sita
					log.debug("######### Before Sending Mails For FID: " + flightId + " Airport : " + depAirportCode
							+ " FlightNum :" + flightNumber);
					Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();
					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddresses);
					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_SITATEX, sitaTexAddress);
					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddresses);
					sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedPnlfileNames, msgType);
					sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddress, this.generatedPnlfileNames, msgType,
							sitaStatusMap, null);
					log.debug("######### Sent OK!");
					// get all the content of the geerated files

					// if no error in transmission then only update the pax status
					log.debug("######### Before Updating PNL Status For FID: " + flightId + " Airport : " + depAirportCode
							+ " FlightNum :" + flightNumber);
					updatePNLStatus(pnlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode, pnlPnrs,sitaStatusMap);
					log.debug("######### Updated PAX Status OK!");

					// get the file content for record keeping purporse
					log.debug("######### Before Getting File Content For FID: " + flightId + " Airport : " + depAirportCode
							+ " FlightNum :" + flightNumber);
					fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedPnlfileNames);
					log.debug("######### Got File content OK!");

					// if an error occured in transmission throw the exception details for notifiying adminstrator
					log.debug("######### Before Checking If Sita Status Map Has Errors  FID: " + flightId + " Airport : "
							+ depAirportCode + " FlightNum :" + flightNumber);
					PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);
					log.debug("######### Checked OK!");
					// End normal sita PNL sending

				}

			} catch (ModuleException e) {
				exceptiontoLog = e;
			} catch (CommonsDataAccessException e) {
				exceptiontoLog = new ModuleException(e, e.getExceptionCode());
			} catch (Exception e) {
				exceptiontoLog = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);
			} finally {
				log.info("######### Before Inserting to History for PNL For FID: " + flightId + " Airport : " + depAirportCode
						+ " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						depAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoLog,
						PNLConstants.SendingMethod.SCHEDSERVICE, lastGroupCode);
				log.info("######### Inserted To History OK!");

				if (exceptiontoLog != null) {
					log.error("########### Error PNL SS FlightNum :" + flightNumber + " Airport :" + depAirportCode,
							exceptiontoLog);
					try {
						PnlAdlUtil.savePNLADLErrorLog(flightId, msgType, depAirportCode, flightNumber, currentDate,
								exceptiontoLog);

						// Throw an Error Except for Flight Closure Error, and etc
						if (exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.FLIGHT_CLOSED_PNLERROR_CODE)
								|| exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.EMPTY_SITA_ADDRESS)) {
						} else {

							if (exceptiontoLog.getExceptionCode().equals(PNLConstants.ERROR_CODES.SITA_STATUS_ERROR)) {
								PnlAdlUtil.notifyAdminForManualAction(flightNumber, fltLocalDAte, depAirportCode, exceptiontoLog,
										PNLConstants.PNLGeneration.PNL,
										" One of the Sita Addresses Or Part of a file was not transmitted");
								// trhow exception
								throw exceptiontoLog;
							} else {
								PnlAdlUtil.notifyAdminForManualAction(flightNumber, fltLocalDAte, depAirportCode, exceptiontoLog,
										PNLConstants.PNLGeneration.PNL, null);
								// throw exception
								throw exceptiontoLog;
							}

						}
					} catch (Throwable e) {
						if (e.equals(exceptiontoLog)) {
							throw exceptiontoLog;
						}
						log.error(
								"Error occured in  Logging Errors and Notifying Admin for Manual Action section in Finaly Method of sendPNL ",
								e);
					}

				}

				log.debug("######### Moving Generate Files FID: " + flightId + " Airport : " + depAirportCode + " FlightNum :"
						+ flightNumber);
				PnlAdlUtil.moveGeneratedFiles(this.generatedPnlfileNames, msgType);
				log.debug("######### Moved OK!");

				// if code come here all scenarios were successful.
				log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- " + depAirportCode
						+ " Flight Num : " + flightNumber);

			}

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sendPNL(String flightNumber, String departureAirportCode, Date dateOfflightLocal,
			String sitaaddresses[], String sendingMethod) throws ModuleException {
		sendPNLMessage(flightNumber,departureAirportCode,dateOfflightLocal,sitaaddresses,sendingMethod);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendPNL(int flightId, String departureAirportCode) throws ModuleException {
		
		String flightNumber = "";
		HashMap sitaStatusMap = new HashMap();
		String fileContent = null;
		Date dateOfflightLocal = null;
		
		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		Flight flight = flightBD.getFlight(flightId);
		if (flight != null && flight.getStatus().equals(FlightStatusEnum.CANCELLED.getCode())) {
			log.info(" #####################     FLIGHT CANCELLED AND LOADING NEW FLIGHT ID : " + flightId);
			Integer actFlightId = null;
			for (FlightSegement fs : (flight.getFlightSegements())) {
				actFlightId = flightBD.getFlightID(fs.getSegmentCode(), flight.getFlightNumber(),
						fs.getEstTimeDepatureZulu());
				if (actFlightId != null) {
					flightId = actFlightId;
					log.info(" #####################     NEW FLIGHT ID TO SEND PNL : " + flightId);
					break;
				} else {
					log.info(" #####################     UNABLE TO FIND NEW FLIGHT ID AND USING OLD ONE : " + flightId);
				}
			}
		}
		// get flight number
		flightNumber = auxilliaryDAO.getFlightNumber(flightId);
		log.debug("######### Retrieved Flight Number For: " + flightId + " Airport : " + departureAirportCode + " FlightNum :"
				+ flightNumber);

		// get Carrier code from flight number
		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);

		// get a list of sita addresses
		log.debug("######### Retrieved SITA List For: " + flightId + " Airport : " + departureAirportCode + " Carrier code :"
				+ carrierCode);
		
		Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
		String ond = PnlAdlUtil.getOndFromDepartureAirport(departureAirportCode, legNumberWiseOndMap);
		
		Map<String, List<String>> sitaAddressesMap = auxilliaryDAO.getActiveSITAAddresses(departureAirportCode.trim(),
				flightNumber, ond, carrierCode, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
		List<String> sitaAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
		List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
		List<String> airincAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
		List<String> sitaAddressAll = new ArrayList<String>();
		sitaAddressAll.addAll(sitaAddresses);
		sitaAddressAll.addAll(sitaTexAddress);
		sitaAddressAll.addAll(airincAddress);
		
		dateOfflightLocal = auxilliaryDAO.getFlightLocalDate(flightId, departureAirportCode);
		
		
		sendPNLMessage(flightNumber, departureAirportCode, dateOfflightLocal,
				(String[]) sitaAddressAll.toArray(new String[sitaAddressAll.size()]), PNLConstants.SendingMethod.SCHEDSERVICE);
	}
	
	private void sendPNLMessage(String flightNumber, String departureAirportCode, Date dateOfflightLocal, String sitaaddresses[],
			String sendingMethod) throws ModuleException {

		synchronized (this) {
			PnlDataContext contx=null;
			boolean isFlightsAvailableForManulPNL = false;
			// holds the exception to be logged, if occured
			ModuleException exceptiontoThrow = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap sitaStatusMap = new HashMap();
			// generated files content
			String fileContent = null;
			try {

				// get flight id
				contx = auxilliaryDAO.getFlightInfo(flightNumber, departureAirportCode, dateOfflightLocal);
			
				if (contx.getFlightId() == -1) {
					isFlightsAvailableForManulPNL = true;
					throw new ModuleException("airreservations.auxilliary.invalidflight", "airreservations");
				}
				
				Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(contx.getFlightId());
				String ond = PnlAdlUtil.getOndFromDepartureAirport(departureAirportCode, legNumberWiseOndMap);

				// check if flight already closed
				checkPnlTimeExceedsFlightClosureTime(contx.getFlightId());

				// need to put all the sita's to a List
				List<String> sitas = PnlAdlUtil.composeSitaAddresses(sitaaddresses);

				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(departureAirportCode, flightNumber, ond,
						carrierCode, sitas, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
				List<String> sitaAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				List<String> arincAddressList = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
				
				// map will have sita address, and failure status.
				PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddress, sitaTexAddress, arincAddressList);

				String[] monthDay = PnlAdlUtil.getMonthDayArray(dateOfflightLocal);

				PNLMetaDataDTO pnlmetadatadto = new PNLMetaDataDTO();

				pnlmetadatadto.setBoardingairport(departureAirportCode);
				pnlmetadatadto.setDay(monthDay[1]);
				pnlmetadatadto.setMonth(monthDay[0]);
				pnlmetadatadto.setFlight(flightNumber);

				pnlmetadatadto.setMessageIdentifier(msgType);

				pnlmetadatadto.setPartnumber(PNLConstants.PNLGeneration.PART_ONE);
				pnlmetadatadto.setPart2(true);
				if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
					pnlmetadatadto.setRbd(PnlAdlUtil.generateRBDValues(contx.getFlightId()));
					pnlmetadatadto.setRbdEnabled(true);
				}

				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.PNL_ADL_DATA_GENERATOR_MACRO);
				command.setParameter(
						CommandParamNames.PNL_DATA_CONTEXT,
						createPnlDataContext(flightNumber, contx.getFlightOriginAirportCode(),
								contx.getFlightDestinationAirportCode(), departureAirportCode, dateOfflightLocal, sitaaddresses,
								sendingMethod));

				ServiceResponce serviceResponce = command.execute();

				MessageResponseAdditionals additionals = (MessageResponseAdditionals) serviceResponce
						.getResponseParam(CommandParamNames.PNL_MESSAGE_RESPONSE_ADDITIONALS);
				Map<Integer, String> messageParts = (Map) serviceResponce.getResponseParam(CommandParamNames.PNL_MESSAGES_PARTS);
				BaseDataContext baseDataContext = (BaseDataContext) serviceResponce
						.getResponseParam(CommandParamNames.PNL_DATA_CONTEXT);

				if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
					if (baseDataContext.isWebserviceSuccess()) {
						updateHistory(pnlmetadatadto.getBoardingairport(), contx.getFlightId(), PNLConstants.SITAMsgTransmission.SUCCESS);
					} else {
						updateHistory(pnlmetadatadto.getBoardingairport(), contx.getFlightId(), PNLConstants.SITAMsgTransmission.FAIL);
					}
				}

				pnlPnrs = new HashSet<String>(additionals.getPnrCollection());
				lastGroupCode = additionals.getLastGroupCode();
				ccPaxMap = additionals.getFareClassWisePaxCount();
				this.generatedPnlfileNames = (ArrayList) createMessaeFile(messageParts, flightNumber, departureAirportCode,
						pnlmetadatadto.getMonth(), pnlmetadatadto.getDay());

				// sent pnl parts files to each sita address, return the sita add->status map
				
				Map<String,List<String>> deliverTypeAddressMap = new HashMap<String,List<String>>();
				
				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddress);
				deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddressList);
				
				sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedPnlfileNames, msgType);
				sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddress, this.generatedPnlfileNames, msgType,
						sitaStatusMap, null);

				PnlAdlUtil.prepareGroupCodeMapWith(additionals.getPnrPaxIdvsSegmentIds(), mapPnrPaxSegIDGroupCode,
						additionals.getPnrPaxVsGroupCodes());

				updatePNLStatus(getPnrPaxIds(additionals.getPnrPaxIdvsSegmentIds()),
						getPnrSegmentIds(additionals.getPnrPaxIdvsSegmentIds()), mapPnrPaxSegIDGroupCode, pnlPnrs);

				// get the content (all 3 parts merged)
				fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedPnlfileNames);

				// check of errors in the sita status map, if so
				PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);
				// End normal Sita PNL sending

			} catch (ModuleException exception) {
				exceptiontoThrow = exception;

			} catch (CommonsDataAccessException e) {
				exceptiontoThrow = new ModuleException(e, e.getExceptionCode());

			} catch (Exception e) {
				exceptiontoThrow = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

			} finally {

				log.debug("######### Before Inserting to History for PNL For FID: " + contx.getFlightId() + " Airport : "
						+ departureAirportCode + " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, contx.getFlightId(),
						departureAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoThrow, sendingMethod,
						lastGroupCode);
				log.debug("######### Inserted To History OK!");

				
				if (exceptiontoThrow != null) {
					// if error occured while sending error mail, a rollback to the entire operation
					// should not occur.
					try {
						String fromMethod = PnlAdlUtil.getSendingOperationDescription(sendingMethod);
						log.error("Error In PNL" + fromMethod + "  PNL Flight ID:" + contx.getFlightId() + " Flight Num : " + flightNumber
								+ " Airport :" + departureAirportCode, exceptiontoThrow);
						if (!isFlightsAvailableForManulPNL) {
							PnlAdlUtil.notifyAdminForManualAction(flightNumber, dateOfflightLocal, departureAirportCode,
									exceptiontoThrow, PNLConstants.PNLGeneration.PNL + "\n ####" + fromMethod
											+ " SENDING FAILURE ###", null);
						}
						throw exceptiontoThrow;

					} catch (Throwable e) {
						if (e.equals(exceptiontoThrow)) {

							throw exceptiontoThrow;
						}
						log.error(
								"Error occured in  Logging Errors and Notifying Admin for Manual Action section in Finaly Method of sendPNL ",
								e);
					}

				}
				log.debug("######### Moving Generate Files FID: " + contx.getFlightId() + " Airport : " + departureAirportCode
						+ " FlightNum :" + flightNumber);
				PnlAdlUtil.moveGeneratedFiles(this.generatedPnlfileNames, msgType);
				log.debug("######### Moved OK!");

				log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + contx.getFlightId() + "|| Dept Airport:- "
						+ departureAirportCode + " Flight Num : " + flightNumber);
			}
		}

	}
	
	private PnlDataContext createPnlDataContext(String flightNumber, String flightOriginAirportCode,String flightDestinationAirportCode, String departureAirportCode,
			Date dateOfflightLocal, String sitaaddresses[], String sendingMethod) {

		FeaturePack featurePack = new FeaturePack();
		featurePack.setRbdEnabled(AppSysParamsUtil.isRBDPNLADLEnabled());
		featurePack.setShowBaggage(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE));
		featurePack.setShowMeals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL));
		featurePack.setShowSeatMap(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP));
		featurePack.setShowEticketDetails(AppSysParamsUtil.isShowTicketDetailsInPnlAdl());
		featurePack.setDcsConnectivityEnabled(AppSysParamsUtil.isDCSConnectivityEnabled());

		PnlDataContext pnlDataContext = new PnlDataContext();
		pnlDataContext.setFlightNumber(flightNumber);
		pnlDataContext.setDepartureAirportCode(departureAirportCode);
		pnlDataContext.setFlightLocalDate(dateOfflightLocal);
		pnlDataContext.setPnlAdlDeliveryInformation(createPnlAdlDeliveryInformation(sitaaddresses, sendingMethod));
		pnlDataContext.setFeaturePack(featurePack);
		pnlDataContext.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightNumber));
		pnlDataContext.setFlightOriginAirportCode(flightOriginAirportCode);
		pnlDataContext.setFlightDestinationAirportCode(flightDestinationAirportCode);
		return pnlDataContext;

	}
	
	private PnlAdlDeliveryInformation createPnlAdlDeliveryInformation(String sitaaddresses[], String sendingMethod){
		
		PnlAdlDeliveryInformation pnlAdlDeliveryInformation = new PnlAdlDeliveryInformation();
		pnlAdlDeliveryInformation.setSendingMethod(sendingMethod);
		pnlAdlDeliveryInformation.setSitaaddresses(sitaaddresses);
		return pnlAdlDeliveryInformation;
		
	}
	
	/**
	 * NOTE: --------------------------------------------------------------------------- The Auxiliary Operation is used
	 * by the Manual Screen and the Resender Job If any errors occurs in this operation, the eror will be thrown and pax
	 * status data will get rolled back. Email will be sent of the error. Record will be inserted to History in new Tnx.
	 * ---------------------------------------------------------------------------
	 * 
	 * @param flightNumber
	 * @param departureAirportCode
	 * @param dateOfflight
	 * @param sitaaddresses
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void sendPNLAuxiliary(String flightNumber, String departureAirportCode, Date dateOfflightLocal,
			String sitaaddresses[], String sendingMethod) throws ModuleException {

		synchronized (this) {
			int flightId = 0;
			boolean isFlightsAvailableForManulPNL = false;
			// holds the exception to be logged, if occured
			ModuleException exceptiontoThrow = null;
			// sitaaddresss - status("y", or "<exception desc>"
			HashMap sitaStatusMap = new HashMap();
			// generated files content
			String fileContent = null;
			try {

				// get flight id
				flightId = auxilliaryDAO.getFlightID(flightNumber, departureAirportCode, dateOfflightLocal);

				if (flightId == -1) {
					isFlightsAvailableForManulPNL = true;
					throw new ModuleException("airreservations.auxilliary.invalidflight", "airreservations");
				}

				// check if flight already closed
				checkPnlTimeExceedsFlightClosureTime(flightId);

				// need to put all the sita's to a List/**
				List<String> sitas = PnlAdlUtil.composeSitaAddresses(sitaaddresses);

				String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				Map<String, String> legNumberWiseOndMap = auxilliaryDAO.getFlightLegs(flightId);
				String ond = PnlAdlUtil.getOndFromDepartureAirport(departureAirportCode, legNumberWiseOndMap);
				Map<String, List<String>> sitaAddressesMap = PnlAdlUtil.getSitaAddressMap(departureAirportCode, flightNumber, ond,
						carrierCode, sitas, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
				List<String> sitaAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_MCONNECT);
				List<String> sitaTexAddress = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_SITATEX);
				List<String> arincAddresses = sitaAddressesMap.get(SITAAddress.SITA_MESSAGE_VIA_ARINC);
				// map will have sita address, and failure status.
				PnlAdlUtil.populateSitaAddresses(sitaStatusMap, sitaAddress, sitaTexAddress, arincAddresses);

				// get a list of pnl pax
				Collection pnlRecordList = auxilliaryDAO.getPNL(flightId, departureAirportCode,
						AppSysParamsUtil.extractCarrierCode(flightNumber));

				if (pnlRecordList != null) {
					// preapre the PNLMetaDataDTO for vel template

					String[] monthDay = PnlAdlUtil.getMonthDayArray(dateOfflightLocal);

					PNLMetaDataDTO pnlmetadatadto = new PNLMetaDataDTO();

					pnlmetadatadto.setBoardingairport(departureAirportCode);
					pnlmetadatadto.setDay(monthDay[1]);
					pnlmetadatadto.setMonth(monthDay[0]);
					pnlmetadatadto.setFlight(flightNumber);

					pnlmetadatadto.setMessageIdentifier(msgType);

					pnlmetadatadto.setPartnumber(PNLConstants.PNLGeneration.PART_ONE);
					pnlmetadatadto.setPart2(true);
					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						pnlmetadatadto.setRbd(PnlAdlUtil.generateRBDValues(flightId));
						pnlmetadatadto.setRbdEnabled(true);
					}

					try {
						if (AppSysParamsUtil.isDCSConnectivityEnabled()) {
							Airport airport = ReservationModuleUtils.getAirportBD().getAirport(departureAirportCode);

							if (airport.isWSAADCSEnabled()) {
								FlightBD flightBD = ReservationModuleUtils.getFlightBD();
								Flight flight = flightBD.getFlight(flightId);
								pnlmetadatadto.setOriginator(flight.getOriginAptCode());
								Date departureDateZulu = null;
								if (flight != null) {
									for (FlightSegement fs : (flight.getFlightSegements())) {
										if (fs.getEstTimeDepatureZulu() != null
												&& fs.getSegmentCode().startsWith(departureAirportCode)) {
											departureDateZulu = fs.getEstTimeDepatureZulu();
											break;
										}
									}
								}
								sendPNLToDCS(pnlRecordList, pnlmetadatadto, departureDateZulu, flightId);
							}

							// update the pnl status
							// updatePNLStatus(pnlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode);
						}
					} catch (Exception e) {
						// sending PNL/ADL to DCS failed
						log.error("######### Error sendPNLToDCS for Flight Id : " + flightId);
						updateHistory(pnlmetadatadto.getBoardingairport(), flightId, PNLConstants.SITAMsgTransmission.FAIL);
					}

					// Normal Sita PNL sending
					// generate pnl
					generatePNL(pnlRecordList, pnlmetadatadto, departureAirportCode, flightId);

					
					Map<String, List<String>> deliverTypeAddressMap = new HashMap<String, List<String>>();

					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, sitaAddress);
					deliverTypeAddressMap.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, arincAddresses);
					
					// sent pnl parts files to each sita address, return the sita add->status map
					sitaStatusMap = PnlAdlUtil.sendMails(deliverTypeAddressMap, this.generatedPnlfileNames, msgType);
					sitaStatusMap = PnlAdlUtil.sendPNLADLViaSitatex(sitaTexAddress, this.generatedPnlfileNames, msgType,
							sitaStatusMap, null);
					// update the pnl status
					updatePNLStatus(pnlCollectionTobeUpdated, mapPnrPaxSegIDGroupCode, pnlPnrs,sitaStatusMap);

					// get the content (all 3 parts merged)
					fileContent = PnlAdlUtil.getGenerateFilesContent(this.generatedPnlfileNames);

					// check of errors in the sita status map, if so
					try{
						PnlAdlUtil.checkErrorsInSitaStatusMap(sitaStatusMap);
					}catch(ModuleException exception){
						exceptiontoThrow = exception;
					}
					// End normal Sita PNL sending

				}
			} catch (ModuleException exception) {
				exceptiontoThrow = exception;

			} catch (CommonsDataAccessException e) {
				exceptiontoThrow = new ModuleException(e, e.getExceptionCode());

			} catch (Exception e) {
				exceptiontoThrow = new ModuleException(e, PNLConstants.ERROR_CODES.UNEXPECTED_ERROR_CODE);

			} finally {

				log.debug("######### Before Inserting to History for PNL For FID: " + flightId + " Airport : "
						+ departureAirportCode + " FlightNum :" + flightNumber);
				ReservationModuleUtils.getReservationAuxilliaryBD().insertToHistory(msgType, sitaStatusMap, flightId,
						departureAirportCode, fileContent, currentTimeStamp, ccPaxMap, exceptiontoThrow, sendingMethod,
						lastGroupCode);
				log.debug("######### Inserted To History OK!");

				if (exceptiontoThrow != null) {
					// if error occured while sending error mail, a rollback to the entire operation
					// should not occur.
					try {
						String fromMethod = PnlAdlUtil.getSendingOperationDescription(sendingMethod);
						log.error("Error In PNL" + fromMethod + "  PNL Flight ID:" + flightId + " Flight Num : " + flightNumber
								+ " Airport :" + departureAirportCode, exceptiontoThrow);
						if (!isFlightsAvailableForManulPNL) {
							PnlAdlUtil.notifyAdminForManualAction(flightNumber, dateOfflightLocal, departureAirportCode,
									exceptiontoThrow, PNLConstants.PNLGeneration.PNL + "\n ####" + fromMethod
											+ " SENDING FAILURE ###", null);
						}
						throw exceptiontoThrow;

					} catch (Throwable e) {
						if (e.equals(exceptiontoThrow)) {

							throw exceptiontoThrow;
						}
						log.error(
								"Error occured in  Logging Errors and Notifying Admin for Manual Action section in Finaly Method of sendPNL ",
								e);
					}

				}
				log.debug("######### Moving Generate Files FID: " + flightId + " Airport : " + departureAirportCode
						+ " FlightNum :" + flightNumber);
				PnlAdlUtil.moveGeneratedFiles(this.generatedPnlfileNames, msgType);
				log.debug("######### Moved OK!");

				log.info("########## PNL OPERATION SUCCESS FOR : Flight_ID:- " + flightId + "|| Dept Airport:- "
						+ departureAirportCode + " Flight Num : " + flightNumber);
			}
		}
	}

	private void updateHistory(String departureAirportCode, int flightId, String transmissionStatus) throws ModuleException {
		PnlAdlHistory adlHistory = new PnlAdlHistory();
		adlHistory.setAirportCode(departureAirportCode);
		adlHistory.setEmail("DCS_WS");
		adlHistory.setFlightID(flightId);
		adlHistory.setMessageType("PNL");// PNL
		adlHistory.setTransmissionStatus(transmissionStatus);
		adlHistory.setTransmissionTimeStamp(currentTimeStamp);
		ReservationModuleUtils.getReservationAuxilliaryBD().saveDCSPnlAdlHistory(adlHistory);
	}

	/**
	 * Generation of PNL
	 * 
	 * @param pdlc
	 *            - Passenger List -->PNLADLDestinationDTO Collection | -- destinationAirportCode; | -- String
	 *            bookingCode; | -- Collection passenger; -- >ReservationPaxDetailsDTO Collection
	 * 
	 * 
	 * 
	 * 
	 * @param mdto
	 * @param depAirportCod
	 * @param flightId
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private void generatePNL(Collection<PNLADLDestinationDTO> pdlc, PNLMetaDataDTO mdto, String depAirportCode, int flightId)
			throws ModuleException {
		try {
			String foidNumberTag = "/P/";
			String nicNumberTag = "/I/";
			Map<Integer, String> pnrPaxIdMealMap = new HashMap<Integer, String>();
			// FIXME
			Map<Integer, String> pnrPaxIdSeatMap = new HashMap<Integer, String>();

			Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap = new HashMap<Integer, Collection<PaxSSRDetailDTO>>();

			Map<Integer, String> pnrPaxIdBaggageMap = new HashMap<Integer, String>();

			if (!pdlc.isEmpty()
					&& ("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP)))) {
				pnrPaxIdSeatMap = PnlAdlUtil.getSeatMapDetails(pdlc, true);
			}

			if (!pdlc.isEmpty() && ("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL)))) {
				pnrPaxIdMealMap = PnlAdlUtil.getMealsDetails(pdlc, true);
			}

			if (!pdlc.isEmpty()
					&& ("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE)))) {
				pnrPaxIdBaggageMap = PnlAdlUtil.getBaggagesDetails(pdlc, true, depAirportCode);
			}

			if (!pdlc.isEmpty()) {
				pnrPaxIdSSRMap = PnlAdlUtil.getSSRDetails(pdlc, true);
			}

			// list sent to pnl formatter
			ArrayList<ArrayList<PassengerCountRecordDTO>> finalList = new ArrayList<ArrayList<PassengerCountRecordDTO>>();

			// list keeps passenger statuss to be updated
			Collection<ReservationPaxDetailsDTO> pnlList = new ArrayList<ReservationPaxDetailsDTO>();

			// array to store tourIDs
			String[] tourIds = PnlAdlUtil.populateTourIds();

			ArrayList<PassengerCountRecordDTO> pnlDataList = new ArrayList<PassengerCountRecordDTO>();
			Iterator<PNLADLDestinationDTO> it = pdlc.iterator();
			int tour = 0;
			Collection<String> ccCodeList = new ArrayList<String>();
			String destinationAirport = "";
			int tourIdIndex = 0;
			while (it.hasNext()) {
				PNLADLDestinationDTO dt = it.next();
				PassengerCountRecordDTO cdto = new PassengerCountRecordDTO();
				cdto.setDestination(dt.getDestinationAirportCode());

				if (AppSysParamsUtil.isRBDPNLADLEnabled() && !AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					cdto.setFareClass(dt.getBookingCode());
				} else {
					cdto.setFareClass(dt.getCabinClassCode());
				}
				
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					ccCodeList.add(dt.getCabinClassCode());
				} else {
					ccCodeList.add(dt.getBookingCode());
				}
				destinationAirport = dt.getDestinationAirportCode();
				cdto.setTotalpassengers(dt.getPassenger().size());
				ArrayList<PNLRecordDTO> pnlRecords = new ArrayList<PNLRecordDTO>();
				ArrayList<ReservationPaxDetailsDTO> recordarray = (ArrayList<ReservationPaxDetailsDTO>) dt.getPassenger();

				HashMap<String, ArrayList<ReservationPaxDetailsDTO>> pnrPassengersMap = PnlAdlUtil
						.getPNRPassengerList(recordarray);
				Iterator<String> itPnr = pnrPassengersMap.keySet().iterator();
				// Collection<PaxSSRDetailDTO> ssrCollectionForFirstNames;
				while (itPnr.hasNext()) {

					String pnr = itPnr.next();

					ArrayList<ReservationPaxDetailsDTO> passengers = pnrPassengersMap.get(pnr); // Passenger

					pnlList.addAll(passengers);

					int dupIndex = 0;
					int firstNameCountPNL = 0;

					int totalPaxCountForPnr = passengers.size();
					int i = 0;
					while (passengers.size() > 0) {

						i++;
						ReservationPaxDetailsDTO pax = passengers.get(0);
						firstNameCountPNL = PnlAdlUtil.getFirstNamesCount(passengers);
						ArrayList<ReservationPaxDetailsDTO> passengerList = new ArrayList<ReservationPaxDetailsDTO>();

						if (passengers.size() > 1 && firstNameCountPNL > 1) {
							Comparator<ReservationPaxDetailsDTO> firstNameOrder = new Comparator<ReservationPaxDetailsDTO>() {
								public int compare(ReservationPaxDetailsDTO pax1, ReservationPaxDetailsDTO pax2) {

									int isSameSurname = pax1.getLastName().toUpperCase()
											.compareTo(pax2.getLastName().toUpperCase());
									if (isSameSurname == 0) {
										return pax1.getFirstName().toUpperCase().compareTo(pax2.getFirstName().toUpperCase());
									} else {
										return isSameSurname;
									}
								}
							};
							Collections.sort(passengers, firstNameOrder);
						}

						passengerList.addAll(passengers);

						ArrayList<NameDTO> firstNames = PnlAdlUtil.getFirstNames(passengers, firstNameCountPNL,
								totalPaxCountForPnr, this.paxIdsPerRecord, pnrPaxIdSeatMap);

						Collections.sort(firstNames);
						PNLRecordDTO pnrc = new PNLRecordDTO();
						if (!firstNames.isEmpty()
								&& PnlAdlUtil.isMultipleSeatsBooked(firstNames.get(0).getPnrPaxId(), pnrPaxIdSeatMap)) {
							pnrc.setMultipleSeatsSelected(true);
							firstNameCountPNL = firstNames.size();
							pnrc.setNumberPAD(firstNameCountPNL);
							firstNameCountPNL = firstNames.size() - 1;// Actual first name count should be without EXST
						} else {
							Collections.sort(firstNames);
							firstNameCountPNL = firstNames.size();
							pnrc.setNumberPAD(firstNameCountPNL);
						}

						// MARKETING/CODESHARE FLIGHT INFORMATION
						pnrc.setCodeShareFlightInfo(PnlAdlUtil.composeMarketingFlightElement(pax));
						pnrc.setCodeShareFlight(pax.isCodeShareFlight());

						firstNameCountPNL = firstNames.size();
						pnrc.setNames(firstNames);
						pnrc.setLastname(pax.getLastName().toUpperCase());
						pnrc.setAutomatedPNR(pax.getPnr());
						pnrc.setNumberPAD(firstNameCountPNL);
						pnrc.setExternalPnr(pax.getExternalPnr());

						pnlPnrs.add(pax.getPnr());
						// Extract PaxSSRDetails from all firstNames list
						// ssrCollectionForFirstNames = null; //FIXME
						for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
							NameDTO tmpNameDTO = firstNamesIt.next();
							String chdDob = "";
							// Adding child SSR Details
							if (tmpNameDTO.getPaxType() != null
									&& tmpNameDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {

								for (Iterator<ReservationPaxDetailsDTO> paxItChd = passengerList.iterator(); paxItChd.hasNext();) {
									ReservationPaxDetailsDTO paxDTOChd = paxItChd.next();
									if (tmpNameDTO.getPnrPaxId() == paxDTOChd.getPnrPaxId()) {
										chdDob = paxDTOChd.getDob();
										break;
									}
								}

								RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
								remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
										+ PNLConstants.PNLADLElements.CHLD);
								remarksElementDTO.setStatusCode("HK1");
								StringBuilder ssrText = new StringBuilder();
								ssrText.append(" ");
								if (chdDob != null && !chdDob.equals("")) {
									chdDob = chdDob.toUpperCase();
									ssrText.append(chdDob);
								}
								ssrText.append("-1").append(pnrc.getLastname().toUpperCase()).append("/")
										.append(tmpNameDTO.getFirstname().toUpperCase()).append(tmpNameDTO.getTitle());

								remarksElementDTO.setText(ssrText.toString());

								pnrc.addRequest(remarksElementDTO);
							}
							for (Iterator<ReservationPaxDetailsDTO> paxIt = passengerList.iterator(); paxIt.hasNext();) {
								ReservationPaxDetailsDTO paxDTO = paxIt.next();
								if (tmpNameDTO.getPnrPaxId() == paxDTO.getPnrPaxId()) {
									for (PaxSSRDetailDTO paxSSR : paxDTO.getSsrDetails()) {
										if (SSRCode.PSPT.toString().equals(paxSSR.getSsrCode())) {
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ paxSSR.getSsrCode());
											remarksElementDTO.setStatusCode("HK1");
											StringBuilder ssrText = new StringBuilder();
											ssrText.append(" " + paxSSR.getSsrText().toUpperCase()).append("/")
													.append(paxDTO.getFoidPlace()).append("///").append("-1")
													.append(pnrc.getLastname().toUpperCase()).append("/")
													.append(tmpNameDTO.getFirstname().toUpperCase())
													.append(tmpNameDTO.getTitle());
											;
											remarksElementDTO.setText(ssrText.toString());// FIXME

											pnrc.addRequest(remarksElementDTO);
										} else if (SSRCode.DOCS.toString().equals(paxSSR.getSsrCode())) {
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ paxSSR.getSsrCode());
											remarksElementDTO.setStatusCode("HK1");
											StringBuilder ssrText = new StringBuilder();
											String foidPlace = "";
											String foidExpiry = "";
											if (!paxSSR.isNICSentInPNLADL()) {
												foidPlace = paxDTO.getFoidPlace();
												foidExpiry = paxDTO.getFoidExpiry().toUpperCase();
											}
											ssrText.append(paxSSR.isNICSentInPNLADL() ? nicNumberTag : foidNumberTag)
													.append(foidPlace).append("/").append(paxSSR.getSsrText().toUpperCase())
													.append("/").append(paxDTO.getNationalityIsoCode()).append("/")
													.append(paxDTO.getDob().toUpperCase()).append("/").append(paxDTO.getGender())
													.append("/").append(foidExpiry).append("/")
													.append(pnrc.getLastname().toUpperCase()).append("/")
													.append(tmpNameDTO.getFirstname().toUpperCase()).append("-1")
													.append(pnrc.getLastname().toUpperCase()).append("/")
													.append(tmpNameDTO.getFirstname().toUpperCase())
													.append(tmpNameDTO.getTitle());
											;
											remarksElementDTO.setText(ssrText.toString());

											pnrc.addRequest(remarksElementDTO);
										} else if (SSRCode.DOCO.toString().equals(paxSSR.getSsrCode())) {
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ paxSSR.getSsrCode());
											remarksElementDTO.setStatusCode("HK1");
											String infantIndicator = "";
											if (tmpNameDTO.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
												infantIndicator = "/I";
											}
											StringBuilder ssrText = new StringBuilder();
											ssrText.append("/").append(paxDTO.getPlaceOfBirth().toUpperCase()).append("/")
													.append(paxDTO.getTravelDocumentType().toUpperCase()).append("/")
													.append(paxDTO.getVisaDocNumber()).append("/")
													.append(paxDTO.getVisaDocPlaceOfIssue().toUpperCase())
													.append("/")
													.append(paxDTO.getVisaDocIssueDate())
													.append("/")
													.append(paxDTO.getVisaApplicableCountry())
													.append(infantIndicator)
													// .append(pnrc.getLastname().toUpperCase()).append("/")
													// .append(tmpNameDTO.getFirstname().toUpperCase())
													.append("-1").append(pnrc.getLastname().toUpperCase()).append("/")
													.append(tmpNameDTO.getFirstname().toUpperCase())
													.append(tmpNameDTO.getTitle());
											;
											remarksElementDTO.setText(ssrText.toString());

											pnrc.addRequest(remarksElementDTO);
										} else if (SSRCode.TKNA.toString().equals(paxSSR.getSsrCode())
												|| SSRCode.TKNE.toString().equals(paxSSR.getSsrCode())) {
											if (AppSysParamsUtil.isShowTicketDetailsInPnlAdl()) {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSR.getSsrCode());
												remarksElementDTO.setStatusCode(PNLADLStatusCode.HK1);
												StringBuilder ssrText = new StringBuilder(PNLADLElements.SPACE);
												ssrText.append(paxSSR.getSsrText().toUpperCase()).append("-1")
														.append(pnrc.getLastname().toUpperCase()).append("/")
														.append(tmpNameDTO.getFirstname().toUpperCase())
														.append(tmpNameDTO.getTitle());
												remarksElementDTO.setText(ssrText.toString());// FIXME

												pnrc.addRequest(remarksElementDTO);
											}
										} else {
											log.error("Generate PNL ERROR : SSR Details are incorrect");
											throw new ModuleException(PNLConstants.ERROR_CODES.PNL_GENERATION_ERROR_CODE);
										}
									}
								}
							}
						}

						if (firstNameCountPNL == totalPaxCountForPnr) {
							passengers.clear();
						}

						pnrc.setPnrPaxId(pax.getPnrPaxId());
						pnrc.setPnrSegId(pax.getPnrSegId());

						if (pax.isWaitingListPax()) {
							ReservationWLPriority rwlp = ReservationDAOUtils.DAOInstance.RESERVATION_DAO
									.getWaitListedReservationPriority(pnr, pax.getPnrSegId());
							pnrc.setHasWaitingList(true);
							pnrc.setWaitingListStart(PNLConstants.PNLADLElements.WAITLIST
									+ ((rwlp != null && rwlp.getPriority() != null) ? rwlp.getPriority() : ""));
						}

						if (pax.isIDPassenger()) {
							pnrc.setHasIDpax(true);
							pnrc.setStandByIDpax(PNLConstants.PNLADLElements.ID2N2);
						}

						if ("Y".equalsIgnoreCase(ReservationModuleUtils.getGlobalConfig().getBizParam(
								SystemParamKeys.SHOW_SEAT_MAP))) {
							String seatNumber = null;
							NameDTO tmpNameDTO = null;
							Integer tmpPnrPaxId = null;
							if (this.paxIdsPerRecord.size() == 1) {
								seatNumber = pnrPaxIdSeatMap.get(this.paxIdsPerRecord.get(0));
								if (seatNumber != null) {
									// In case multiple seats blocked for single pax
									String seatNumbers[] = seatNumber.split(" ");
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
									remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.SEATMAP);
									remarksElementDTO.setStatusCode("HK1");
									remarksElementDTO.setText(" " + seatNumbers[0]);

									pnrc.addRequest(remarksElementDTO);

									if (pnrc.isMultipleSeatsSelected()) {
										// This is to add (extra)exst seat
										RemarksElementDTO exstRemarksElementDTO = new RemarksElementDTO();
										exstRemarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.EXTRA_SEAT);
										exstRemarksElementDTO.setStatusCode("HK1");

										tmpNameDTO = firstNames.get(1);
										exstRemarksElementDTO.setText(" " + seatNumbers[1] + "-1"
												+ pnrc.getLastname().toUpperCase() + "/"
												+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());

										pnrc.addRequest(exstRemarksElementDTO);
									}
								}
							} else {
								for (Iterator<Integer> paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
									tmpPnrPaxId = paxIdIt.next();
									seatNumber = pnrPaxIdSeatMap.get(tmpPnrPaxId);
									if (seatNumber != null) {
										// In case multiple seats blocked for single pax
										String seatNumbers[] = seatNumber.split(" ");
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.SEATMAP);
										remarksElementDTO.setStatusCode("HK1");

										if (pnrc.isMultipleSeatsSelected()) {
											// This is to add (extra)exst seat
											RemarksElementDTO exstRemarksElementDTO = new RemarksElementDTO();
											exstRemarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.EXTRA_SEAT);
											exstRemarksElementDTO.setStatusCode("HK1");

											tmpNameDTO = firstNames.get(1);
											exstRemarksElementDTO.setText(" " + seatNumbers[0] + " " + seatNumbers[1] + "-1"
													+ pnrc.getLastname().toUpperCase() + "/"
													+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());

											pnrc.addRequest(remarksElementDTO);
											pnrc.addRequest(exstRemarksElementDTO);
										} else {

											tmpNameDTO = null;
											for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt.hasNext();) {
												tmpNameDTO = firstNamesIt.next();
												if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
													break;
												}
											}
											remarksElementDTO.setText(" " + seatNumber + "-1" + pnrc.getLastname().toUpperCase()
													+ "/" + tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
											pnrc.addRequest(remarksElementDTO);
										}
									}
								}
							}

						}

						if (("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL)))) {

							String tmpMealCode = null;
							NameDTO tmpNameDTO = null;
							Integer tmpPnrPaxId = null;
							if (this.paxIdsPerRecord.size() == 1) {
								tmpMealCode = pnrPaxIdMealMap.get(this.paxIdsPerRecord.get(0));
								if (tmpMealCode != null) {
									// Split IATA code and meal code
									String[] mealCodes = tmpMealCode.split("%");
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
									if (mealCodes.length > 1) {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ mealCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK1");
										remarksElementDTO.setText(" " + mealCodes[1].toUpperCase());
									} else {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ mealCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK1");
										remarksElementDTO.setText("");
									}
									pnrc.addRequest(remarksElementDTO);
								}
							} else {
								boolean allHasSameMeal = true;
								int mealsCount = 0;
								for (Iterator<Integer> paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
									tmpPnrPaxId = paxIdIt.next();
									if (tmpMealCode != null) {
										if (pnrPaxIdMealMap.get(tmpPnrPaxId) != null) {
											if (!tmpMealCode.equals(pnrPaxIdMealMap.get(tmpPnrPaxId))) {
												allHasSameMeal = false;
												break;
											}
										}
									}
									tmpMealCode = pnrPaxIdMealMap.get(tmpPnrPaxId);
									if (tmpMealCode != null)
										++mealsCount;
								}

								if (allHasSameMeal && this.paxIdsPerRecord.size() == mealsCount) {
									// All the pax has same meal

									// Split IATA code and meal code
									String[] mealCodes = tmpMealCode.split("%");
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
									if (mealCodes.length > 1) {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ mealCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK" + mealsCount);
										remarksElementDTO.setText(" " + mealCodes[1].toUpperCase());
									} else {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ mealCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK" + mealsCount);
										remarksElementDTO.setText("");
									}
									pnrc.addRequest(remarksElementDTO);
								} else {
									// Not all the pax has the same meal
									for (Iterator<Integer> paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
										tmpPnrPaxId = paxIdIt.next();
										tmpMealCode = pnrPaxIdMealMap.get(tmpPnrPaxId);
										if (tmpMealCode != null) {
											// Split IATA code and meal code
											String[] mealCodes = tmpMealCode.split("%");
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											if (mealCodes.length > 1) {
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ mealCodes[0].toUpperCase());
												remarksElementDTO.setStatusCode("HK1");

												tmpNameDTO = null;
												for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt
														.hasNext();) {
													tmpNameDTO = firstNamesIt.next();
													if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
														break;
													}
												}
												remarksElementDTO.setText(" " + mealCodes[1].toUpperCase() + "-1"
														+ pnrc.getLastname().toUpperCase() + "/"
														+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
											} else {
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ mealCodes[0].toUpperCase());
												remarksElementDTO.setStatusCode("HK1");

												tmpNameDTO = null;
												for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt
														.hasNext();) {
													tmpNameDTO = firstNamesIt.next();
													if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
														break;
													}
												}
												remarksElementDTO.setText("-1" + pnrc.getLastname().toUpperCase() + "/"
														+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
											}
											pnrc.addRequest(remarksElementDTO);
										}
									}
								}
							}

						}

						if (("Y".equals(ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE)))) {

							String tmpBaggageCode = null;
							NameDTO tmpNameDTO = null;
							Integer tmpPnrPaxId = null;
							if (this.paxIdsPerRecord.size() == 1) {
								tmpBaggageCode = pnrPaxIdBaggageMap.get(this.paxIdsPerRecord.get(0));
								if (tmpBaggageCode != null) {
									// Split IATA code and baggage code
									String[] baggageCodes = tmpBaggageCode.split("%");
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
									if (baggageCodes.length > 1) {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ baggageCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK1");
										remarksElementDTO.setText(" " + baggageCodes[1].toUpperCase());
									} else {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ baggageCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK1");
										remarksElementDTO.setText("");
									}
									pnrc.addRequest(remarksElementDTO);
								}
							} else {
								boolean allHasSameBaggage = true;
								int baggagesCount = 0;
								for (Iterator<Integer> paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
									tmpPnrPaxId = paxIdIt.next();
									if (tmpBaggageCode != null) {
										if (pnrPaxIdBaggageMap.get(tmpPnrPaxId) != null) {
											if (!tmpBaggageCode.equals(pnrPaxIdBaggageMap.get(tmpPnrPaxId))) {
												allHasSameBaggage = false;
												break;
											}
										}
									}
									tmpBaggageCode = pnrPaxIdBaggageMap.get(tmpPnrPaxId);
									if (tmpBaggageCode != null)
										++baggagesCount;
								}

								if (allHasSameBaggage && this.paxIdsPerRecord.size() == baggagesCount) {

									String[] baggageCodes = tmpBaggageCode.split("%");
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
									if (baggageCodes.length > 1) {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ baggageCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK" + baggagesCount);
										remarksElementDTO.setText(" " + baggageCodes[1].toUpperCase());
									} else {
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
												+ baggageCodes[0].toUpperCase());
										remarksElementDTO.setStatusCode("HK" + baggagesCount);
										remarksElementDTO.setText("");
									}
									pnrc.addRequest(remarksElementDTO);
								} else {
									// Not all the pax has the same baggage
									for (Iterator<Integer> paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
										tmpPnrPaxId = paxIdIt.next();
										tmpBaggageCode = pnrPaxIdBaggageMap.get(tmpPnrPaxId);
										if (tmpBaggageCode != null) {

											String[] baggageCodes = tmpBaggageCode.split("%");
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											if (baggageCodes.length > 1) {
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ baggageCodes[0].toUpperCase());
												remarksElementDTO.setStatusCode("HK1");

												tmpNameDTO = null;
												for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt
														.hasNext();) {
													tmpNameDTO = firstNamesIt.next();
													if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
														break;
													}
												}
												remarksElementDTO.setText(" " + baggageCodes[1].toUpperCase() + "-1"
														+ pnrc.getLastname().toUpperCase() + "/"
														+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
											} else {
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ baggageCodes[0].toUpperCase());
												remarksElementDTO.setStatusCode("HK1");

												tmpNameDTO = null;
												for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt
														.hasNext();) {
													tmpNameDTO = firstNamesIt.next();
													if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
														break;
													}
												}
												remarksElementDTO.setText("-1" + pnrc.getLastname().toUpperCase() + "/"
														+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
											}
											pnrc.addRequest(remarksElementDTO);
										}
									}
								}
							}

						}

						NameDTO tmpNameDTO = null;
						Integer tmpPnrPaxId = null;
						if (this.paxIdsPerRecord.size() == 1) {
							Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxIdSSRMap.get(this.paxIdsPerRecord.get(0));
							if (paxSSRDetailDTOs != null && paxSSRDetailDTOs.size() > 0) {
								for (PaxSSRDetailDTO paxSSRDetailDTO : paxSSRDetailDTOs) {

									if (ValidationUtils.skipAirportService(paxSSRDetailDTO.getSsrCatId())) {
										continue;
									}

									if (paxSSRDetailDTO.getSsrCode() != null) {
										if (paxSSRDetailDTO.getSsrCode().equals(SSRCode.FQTV.toString())) {
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ paxSSRDetailDTO.getSsrCode());

											remarksElementDTO.setStatusCode(AppSysParamsUtil.getCarrierCode());
											remarksElementDTO.setText(paxSSRDetailDTO.getSsrText() != null
													? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
													: "");
											pnrc.addRequest(remarksElementDTO);
										} else {
											RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
											remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
													+ paxSSRDetailDTO.getSsrCode());
											remarksElementDTO.setStatusCode("HK1");
											remarksElementDTO.setText(paxSSRDetailDTO.getSsrText() != null
													? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
													: "");
											pnrc.addRequest(remarksElementDTO);
										}
									}
								}
							}
						} else {
							for (Iterator<Integer> paxIdIt = this.paxIdsPerRecord.iterator(); paxIdIt.hasNext();) {
								tmpPnrPaxId = paxIdIt.next();
								// ssrText = (String) pnrPaxIdSSRMap.get(tmpPnrPaxId);
								Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxIdSSRMap.get(tmpPnrPaxId);

								if (paxSSRDetailDTOs != null && paxSSRDetailDTOs.size() > 0) {
									for (PaxSSRDetailDTO paxSSRDetailDTO : paxSSRDetailDTOs) {

										if (ValidationUtils.skipAirportService(paxSSRDetailDTO.getSsrCatId())) {
											continue;
										}

										if (paxSSRDetailDTO.getSsrCode() != null) {
											if (paxSSRDetailDTO.getSsrCode().equals(SSRCode.FQTV.toString())) {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSRDetailDTO.getSsrCode());
												remarksElementDTO.setStatusCode(AppSysParamsUtil.getCarrierCode());
												tmpNameDTO = null;
												for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt
														.hasNext();) {
													tmpNameDTO = firstNamesIt.next();
													if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
														break;
													}
												}
												remarksElementDTO.setText((paxSSRDetailDTO.getSsrText() != null
														? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
														: "")
														+ "-1"
														+ pnrc.getLastname().toUpperCase()
														+ "/"
														+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
												pnrc.addRequest(remarksElementDTO);
											} else {
												RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
												remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK
														+ paxSSRDetailDTO.getSsrCode());
												remarksElementDTO.setStatusCode("HK1");

												tmpNameDTO = null;
												for (Iterator<NameDTO> firstNamesIt = firstNames.iterator(); firstNamesIt
														.hasNext();) {
													tmpNameDTO = firstNamesIt.next();
													if (tmpPnrPaxId.equals(tmpNameDTO.getPnrPaxId())) {
														break;
													}
												}
												remarksElementDTO.setText((paxSSRDetailDTO.getSsrText() != null
														? (" " + paxSSRDetailDTO.getSsrText().toUpperCase())
														: "")
														+ "-1"
														+ pnrc.getLastname().toUpperCase()
														+ "/"
														+ tmpNameDTO.getFirstname().toUpperCase() + tmpNameDTO.getTitle());
												pnrc.addRequest(remarksElementDTO);
											}
										}
									}
								}

							}
						}

						// ====== END SSR Details ==============================================================

						// Setting CreditCard Details
						if (ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SEND_CC_DETAILS_WITH_PNL)
								.equalsIgnoreCase("Y")) {

							String ccDetails = "";
							ccDetails = pax.getCcDigits();
							if (ccDetails != null && !ccDetails.trim().equals("")) {
								pnrc.setHasCCDetails(true);
								pnrc.setCCDetails(" CC/XXXX-XXXX-XXXX-" + ccDetails);
								pnrc.setCCDetailsRequestCode(PNLConstants.PNLADLElements.EPAY);
								pnrc.setCCDetailsStatusCode(" KK" + firstNameCountPNL); // Status code ex: KK2,HK1
							}
						}

						ReservationPax rpa = null;
						// String infantString = "";
						// String infantEticketString = "";
						int infantSize = 0;
						// StringBuffer buff = new StringBuffer();
						for (int p = 0; p < this.paxIdsPerRecord.size(); p++) {
							int paxid = (this.paxIdsPerRecord.get(p)).intValue();
							rpa = ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassenger(paxid, false, false, false);
							Set<ReservationPax> cset = rpa.getInfants();
							Iterator<ReservationPax> it1 = cset.iterator();
							Flight flight = null;
							if (cset.size() > 0) {
								flight = ReservationModuleUtils.getFlightBD().getFlight(flightId);
							}
							while (it1.hasNext()) {
								ReservationPax rpat = (it1.next());
								String firstName = rpat.getFirstName();
								String lastName = rpat.getLastName();
								String title = rpat.getTitle();

								if (firstName == null) {
									firstName = "";
								}
								if (lastName == null) {
									lastName = "";
								}
								if (title == null) {
									title = "";
								}

								firstName = BeanUtils.removeExtraChars(firstName.toUpperCase());
								firstName = BeanUtils.removeSpaces(firstName).toUpperCase();

								lastName = BeanUtils.removeExtraChars(lastName.toUpperCase());
								lastName = BeanUtils.removeSpaces(lastName).toUpperCase();

								Date infDob = rpat.getDateOfBirth();
								String dobInf = "";
								if (infDob != null) {
									DateFormat formatter = new SimpleDateFormat("ddMMMyy");
									dobInf = formatter.format(infDob).toString().toUpperCase();
								}

								// Add infants as remarks
								RemarksElementDTO infRemarksElementDTO = new RemarksElementDTO();

								infRemarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK + PNLADLElements.INFANT2);
								infRemarksElementDTO.setStatusCode(PNLADLStatusCode.HK1);
								StringBuilder infText = new StringBuilder(PNLADLElements.SPACE);
								if (!dobInf.equals("")) {
									infText.append(dobInf).append(PNLADLElements.SPACE);
								}
								infText.append(lastName).append("/").append(firstName + title);
								infRemarksElementDTO.setText(infText.toString());
								pnrc.addRequest(infRemarksElementDTO);

								ReservationPaxAdditionalInfo resPaxAdditionalInfo = rpat.getPaxAdditionalInfo();
								if (resPaxAdditionalInfo != null) {
									String ssrCode = null;
									SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
									List<BasePaxConfigDTO> paxConfigDTOList = AirproxyModuleUtils.getGlobalConfig().getpaxConfig(
											AppIndicatorEnum.APP_XBE.toString());

									for (BasePaxConfigDTO ssr : paxConfigDTOList) {
										if (ssr.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
											ssrCode = ssr.getSsrCode();
											break;
										}
									}

									if (ssrCode != null) {
										RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
										remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK + ssrCode);
										remarksElementDTO.setStatusCode("HK1 ");
										String infantIndicator = "/I";
										String psptIssuedCountry = resPaxAdditionalInfo.getPassportIssuedCntry() == null
												? ""
												: resPaxAdditionalInfo.getPassportIssuedCntry();
										String foidNo = "";
										if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
											if (("DOM").equals(flight.getFlightType())
													&& resPaxAdditionalInfo.getNationalIDNo() != null
													&& !("").equals(resPaxAdditionalInfo.getNationalIDNo().trim())) {
												foidNo = resPaxAdditionalInfo.getNationalIDNo();
												psptIssuedCountry = "";
											} else {
												if (resPaxAdditionalInfo.getPassportNo() != null
														&& !resPaxAdditionalInfo.getPassportNo().equals("")) {
													foidNo = resPaxAdditionalInfo.getPassportNo();
												}
											}
										} else {
											if (resPaxAdditionalInfo.getPassportNo() != null
													&& !resPaxAdditionalInfo.getPassportNo().equals("")) {
												foidNo = resPaxAdditionalInfo.getPassportNo();
											}
										}

										if (!("").equals(foidNo)) {
											StringBuilder ssrText = new StringBuilder();
											ssrText.append(foidNo).append("/").append(psptIssuedCountry).append("/")
													.append(sf.format(rpat.getDateOfBirth())).append("/")
													.append(lastName.toUpperCase()).append("/").append(firstName.toUpperCase())
													.append(infantIndicator);

											remarksElementDTO.setText(ssrText.toString());
											pnrc.addRequest(remarksElementDTO);
										}

									}
								}

								// infant e ticket including
								EticketTO eTicket = eTicketDAO.getPassengerETicketDetail(rpat.getPnrPaxId(), flightId);
								if (eTicket != null && AppSysParamsUtil.isShowInfantETicketInPNLADL()
										&& AppSysParamsUtil.isShowTicketDetailsInPnlAdl()) {
									String SsrCode;

									// Add parent name for infant E-ticket details
									String paFirstName = rpa.getFirstName();
									String paLastName = rpa.getLastName();
									String paTitle = rpa.getTitle();
									if (paFirstName == null) {
										paFirstName = "";
									}
									if (paLastName == null) {
										paLastName = "";
									}
									if (paTitle == null) {
										paTitle = "";
									}
									paFirstName = BeanUtils.removeExtraChars(paFirstName.toUpperCase());
									paFirstName = BeanUtils.removeSpaces(paFirstName).toUpperCase();

									paLastName = BeanUtils.removeExtraChars(paLastName.toUpperCase());
									paLastName = BeanUtils.removeSpaces(paLastName).toUpperCase();
									String eticket = eTicket.getEticketNumber();
									int couponNo = eTicket.getCouponNo();
									if (pax.isCodeShareFlight()) {
										if (eTicket.getExternalEticketNumber() != null) {
											eticket = eTicket.getExternalEticketNumber();
										}

										if (eTicket.getExternalCouponNo() != null) {
											couponNo = eTicket.getExternalCouponNo();
										}

									}

									if (AppSysParamsUtil.isPNLEticketEnabled()) {
										SsrCode = CommonsConstants.PNLticketType.TKNE;
									} else {
										SsrCode = CommonsConstants.PNLticketType.TKNA;
									}
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();

									remarksElementDTO.setRequestCode(PNLConstants.PNLADLElements.REMARK + SsrCode);
									remarksElementDTO.setStatusCode(PNLADLStatusCode.HK1);
									StringBuilder infSsrText = new StringBuilder(PNLADLElements.SPACE);
									infSsrText.append(PNLADLElements.INFANT + eticket.toUpperCase()).append("/").append(couponNo)
											.append("-1").append(paLastName).append("/").append(paFirstName).append(paTitle);

									remarksElementDTO.setText(infSsrText.toString());
									pnrc.addRequest(remarksElementDTO);
								}

								// infant DOCO information
								if (resPaxAdditionalInfo != null && resPaxAdditionalInfo.getVisaDocNumber() != null
										&& !resPaxAdditionalInfo.getVisaDocNumber().equals("")) {
									RemarksElementDTO remarksElementDTO = new RemarksElementDTO();
									remarksElementDTO
											.setRequestCode(PNLConstants.PNLADLElements.REMARK + SSRCode.DOCO.toString());
									remarksElementDTO.setStatusCode("HK1");
									String infantIndicator = "/I";
									String visaDocIssueDate = "";
									if (resPaxAdditionalInfo.getVisaDocIssueDate() != null) {
										SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
										visaDocIssueDate = sf.format(resPaxAdditionalInfo.getVisaDocIssueDate());
									}
									String placeOfBirth = ((resPaxAdditionalInfo.getPlaceOfBirth() == null)
											? ""
											: resPaxAdditionalInfo.getPlaceOfBirth());
									String travelDocumentType = ((resPaxAdditionalInfo.getTravelDocumentType() == null)
											? ""
											: resPaxAdditionalInfo.getTravelDocumentType());
									String visaDocPlaceOfIssue = ((resPaxAdditionalInfo.getVisaDocPlaceOfIssue() == null)
											? ""
											: resPaxAdditionalInfo.getVisaDocPlaceOfIssue());
									String visaApplicableCountry = ((resPaxAdditionalInfo.getVisaApplicableCountry() == null)
											? ""
											: resPaxAdditionalInfo.getVisaApplicableCountry());

									StringBuilder ssrText = new StringBuilder();
									ssrText.append("/").append(placeOfBirth.toUpperCase()).append("/")
											.append(travelDocumentType.toUpperCase()).append("/")
											.append(resPaxAdditionalInfo.getVisaDocNumber()).append("/")
											.append(visaDocPlaceOfIssue.toUpperCase()).append("/").append(visaDocIssueDate)
											.append("/").append(visaApplicableCountry)
											.append(infantIndicator)
											// .append(pnrc.getLastname().toUpperCase()).append("/")
											// .append(tmpNameDTO.getFirstname().toUpperCase())
											.append("-1").append(rpat.getLastName().toUpperCase()).append("/")
											.append(rpat.getFirstName().toUpperCase());
									;
									remarksElementDTO.setText(ssrText.toString());

									pnrc.addRequest(remarksElementDTO);
								}
							}
							infantSize += cset.size();
						}
						this.paxIdsPerRecord.clear();
						pnrc.setInfantString("");
						// infantString = ".R/" + infantSize + "INF " + buff.toString();

						// if (infantSize == 0) {
						// pnrc.setInfantString("");
						// } else {
						// pnrc.setInfantString(" " + infantString.trim());
						// }

						InboundConnectionDTO idt = auxilliaryDAO.getInBoundConnectionList(pax.getPnr(), pax.getPnrPaxId()
								.intValue(), depAirportCode, flightId);

						if (idt != null) {
							if (idt.getDate() != null)
								pnrc.setInboundDate(idt.getDate().trim());
							if (idt.getDepartureStation() != null)
								pnrc.setInboundDepartureStation(idt.getDepartureStation().trim());
							if (idt.getFareClass() != null)
								pnrc.setInboundFareClass(idt.getFareClass().trim());
							if (idt.getFlightNumber() != null) {
								pnrc.setInboundFlightNumber(idt.getFlightNumber());
								pnrc.setInboundStart(".I/");
								pnrc.setHasInbound(true);
							}
						}

						ArrayList<OnWardConnectionDTO> list = (ArrayList<OnWardConnectionDTO>) auxilliaryDAO
								.getAllOutBoundSequences(pax.getPnr(), pax.getPnrPaxId().intValue(), depAirportCode, flightId);

						if (list != null) {
							pnrc.setOnwardconnectionlist(list);
						}

						if (totalPaxCountForPnr != firstNameCountPNL) {
							pnrc.setDuplicatedPNRaddress(true);
							pnrc.setTourID(tourIds[tourIdIndex]);
							pnrc.setIndex(dupIndex);
							dupIndex++;
						}
						pnrc.setTotalSeats(totalPaxCountForPnr);
						pnlRecords.add(pnrc);
						// pnlRecordDTOs.add(pnrc);
						// taking precaution..variable i should never reach 200 in real time.
						if (i > 500) {
							String error = "##### Unexpect Problem! Passenger Size not being removed in PnlADLUtil.getFirstNames Method. Check This. For flight ID :"
									+ flightId;
							log.error(error);
							PnlAdlUtil.notifyAdminForManualAction(null, null, null, null, null, error);
							break;
						}
					}

				}

				this.lastGroupCode = PnlAdlUtil.populateTourIDsForPNLNew(pnlRecords, tour, tourIds);

				PnlAdlUtil.prepareGroupCodeMap(pnlRecords, mapPnrPaxSegIDGroupCode);

				Collections.sort(pnlRecords);

				cdto.setPnlrecords(pnlRecords);

				if (!pnlDataList.contains(cdto)) {
					ArrayList<PNLRecordDTO> list = cdto.getPnlrecords();
					int tot = 0;
					for (int i = 0; i < list.size(); i++) {
						PNLRecordDTO dtc = list.get(i);
						tot += dtc.getNumberPAD();
					}
					cdto.setTotalpassengers(tot);
					pnlDataList.add(cdto);
				}
				ccPaxMap.put(cdto.getFareClass(), new Integer(cdto.getTotalpassengers()));
				// this.paxCount=cdto.getTotalpassengers();

				// For multi-leg flight, tourId should be continued from last tourId used
				tour = PnlAdlUtil.determineNextTourIndex(lastGroupCode, tourIds);
			}

			if (!finalList.contains(pnlDataList)) {
				finalList.add(pnlDataList);
			}

			PnlAdlUtil.checkIFAllCOSToBeShown(PNLConstants.PNLGeneration.PNL, ccCodeList, flightId, destinationAirport,
					finalList, ccPaxMap);
			PNLFormatter format = new PNLFormatter(finalList, mdto);

			this.generatedPnlfileNames = format.getFileNameList();

			pnlCollectionTobeUpdated = pnlList;

		} catch (CommonsDataAccessException e) {
			log.error("Generate PNL ERROR :", e);
			throw new ModuleException(e, e.getExceptionCode());
		} catch (ModuleException e) {
			log.error("Generate PNL ERROR :", e);
			throw e;

		} catch (Exception e) {
			log.error("Generate PNL ERROR :", e);
			throw new ModuleException(e, PNLConstants.ERROR_CODES.PNL_GENERATION_ERROR_CODE);
		}
	}

	private void sendPNLToDCS(Collection<PNLADLDestinationDTO> pdlc, PNLMetaDataDTO mdto, Date departureDateTime, int flightId)
			throws ModuleException {
		DCSPNLInfoRQ pnlInfo = new DCSPNLInfoRQ();

		DCSFlight dcsFlight = new DCSFlight();
		dcsFlight.setDepartureAirportCode(mdto.getBoardingairport());
		dcsFlight.setFlightNumber(mdto.getFlight());
		dcsFlight.setDepartureTime(departureDateTime);
		dcsFlight.setOriginAirportCode(mdto.getOriginator());
		pnlInfo.setFlightInfo(dcsFlight);

		this.compileFlightSegmentDetails(pdlc, pnlInfo, mdto.getBoardingairport(), flightId);
		DCSPNLInfoRS dcsPNLInfoRS = ReservationModuleUtils.getDCSClientBD().sendPNLToDCS(pnlInfo);
		if (dcsPNLInfoRS.getResponseAttributes().getSuccess() != null) {
			// update history
			updateHistory(mdto.getBoardingairport(), flightId, PNLConstants.SITAMsgTransmission.SUCCESS);
		}

	}

	private List<String> createMessaeFile(Map<Integer,String> messageParts,
			String flightNo,String boardingAirport,String month,String day) throws ModuleException{
		
		List<String> messageFiles = new ArrayList<String>();
		try{
			for (Map.Entry<Integer, String> entry : messageParts
				.entrySet()) {
				messageFiles.add(createMessageFile(entry.getKey(),
					entry.getValue(),flightNo,
					boardingAirport, month, day));
			}
		}catch (Exception e) {
			log.error("Generate PNL files Error :", e);
			throw new ModuleException(e, PNLConstants.ERROR_CODES.PNL_GENERATION_ERROR_CODE);
		}
		
		return messageFiles;
	}
	
	private String createMessageFile(Integer partNumber,String messageContent,
			String flightNo,String boardingAirport,String month,String day) throws Exception{

		SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
		List<String> filenamelist = new ArrayList<String>();
		if (Integer.parseInt(day) <= 9)
			day = "0" + day;
		String generatedpnlfilepath = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
				.getGenpnlpath();
		String date = simpleDateFormat.format(new Date());
		String fileName=generatedpnlfilepath + "/" + flightNo + "-" + boardingAirport + "-" + month + "" + day + "-" + "PNL"
				+ "-" + date + "-" + "PART"+ partNumber + ".txt";
		
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(messageContent);
		bw.close();
		return fileName;
	}
	
	private void compileFlightSegmentDetails(Collection<PNLADLDestinationDTO> pdlc, DCSPNLInfoRQ pnlInfo, String depAirportCode,
			int flightId) throws ModuleException {
		// Loading meal details
		Map<Integer, String> pnrPaxIdMealMap = PnlAdlUtil.getMealsDetails(pdlc, true);
		// Loading setMap details
		Map<Integer, String> pnrPaxIdSeatMap = PnlAdlUtil.getSeatMapDetails(pdlc, true);
		// Loading ssr details
		Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxIdSSRMap = PnlAdlUtil.getSSRDetails(pdlc, true);
		// Loading baggage details
		Map<Integer, String> pnrPaxIdBaggageMap = PnlAdlUtil.getBaggagesWeights(pdlc, true, depAirportCode);

		for (PNLADLDestinationDTO pnlAdlDto : pdlc) {
			Map<String, List<ReservationPaxDetailsDTO>> pnrPaxMap = new HashMap<String, List<ReservationPaxDetailsDTO>>();
			DCSFlightSegmentMapElement dfsme = new DCSFlightSegmentMapElement();
			if (pnlAdlDto.getCabinClassCode() != null && pnlAdlDto.getCabinClassCode().length() > 0) {
				dfsme.setCabinClassCode(pnlAdlDto.getCabinClassCode());

				DCSFlightSegment dfs = new DCSFlightSegment();

				dfs.setSegmentCode(PnlAdlUtil.findMatchingFlightSegmentcode(flightId, depAirportCode,
						pnlAdlDto.getDestinationAirportCode()));
				for (ReservationPaxDetailsDTO rpddto : pnlAdlDto.getPassenger()) {
					if (pnrPaxMap.get(rpddto.getPnr()) == null) {
						List<ReservationPaxDetailsDTO> pnrPax = new ArrayList<ReservationPaxDetailsDTO>();
						pnrPax.add(rpddto);
						pnrPaxMap.put(rpddto.getPnr(), pnrPax);

					} else {
						pnrPaxMap.get(rpddto.getPnr()).add(rpddto);
					}
				}
				for (String pnr : pnrPaxMap.keySet()) {
					PassengersMapElement pme = new PassengersMapElement();
					pme.setPnr(pnr);
					List<ReservationPaxDetailsDTO> rpddtos = pnrPaxMap.get(pnr);
					if (pnlCollectionTobeUpdated == null) {
						pnlCollectionTobeUpdated = new ArrayList<ReservationPaxDetailsDTO>();

					}
					pnlCollectionTobeUpdated.addAll(rpddtos);
					// load first pax details to load cc Details
					ReservationPaxDetailsDTO ccDetails = (ReservationPaxDetailsDTO) BeanUtils.getFirstElement(rpddtos);
					if (ccDetails.getCcDigits() != null && !ccDetails.getCcDigits().trim().equals("")) {
						pme.setCcDigits("CC/XXXX-XXXX-XXXX-" + ccDetails.getCcDigits());
					}

					for (ReservationPaxDetailsDTO rpddto : rpddtos) {
						DCSPassenger dp = new DCSPassenger();
						dp.setTitle(rpddto.getTitle());
						dp.setFirstName(rpddto.getFirstName());
						dp.setLastName(rpddto.getLastName());
						dp.setPaxType(rpddto.getPaxType());
						dp.setGroupID(rpddto.getGroupId());
						dp.setStandByPax(rpddto.isIDPassenger());
						dp.setNationality(rpddto.getNationalityIsoCode());
						if (rpddto.getSsrDetails() != null) {

							for (PaxSSRDetailDTO paxSsr : rpddto.getSsrDetails()) {
								if ((paxSsr.getSsrCode().equals(CommonsConstants.PNLticketType.TKNE) || paxSsr.getSsrCode()
										.equals(CommonsConstants.PNLticketType.TKNA)) && paxSsr.getSsrText() != null) {
									dp.setETicket(paxSsr.getSsrText().split("/")[0]);
									// dp.setCoupnNo(paxSsr.getSsrText().split("/")[1]);
								}

							}
						}

						DCSPassportInformation dpi = new DCSPassportInformation();
						if (rpddto.getFoidPlace() != null) {
							dpi.setIssuedCountry(rpddto.getFoidPlace());
						}
						if (rpddto.getFoidExpiry() != null) {
							try {
								DateFormat formatter = new SimpleDateFormat("ddMMMyy");
								dpi.setPassportExpiryDate(formatter.parse(rpddto.getFoidExpiry()));
							} catch (Exception e) {

							}
						}
						if (rpddto.getFoidNumber() != null) {
							dpi.setPassportNumber(rpddto.getFoidNumber());
						}
						dp.setPassportInfo(dpi);

						if (rpddto.getDob() != null) {
							try {
								SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
								dp.setDateOfBirth(sf.parse(rpddto.getDob()));
							} catch (Exception e) {

							}
						}

						DCSVisaInformation dvi = new DCSVisaInformation();
						if (rpddto.getVisaDocNumber() != null) {
							dvi.setVisaDocNumber(rpddto.getVisaDocNumber());

							if (rpddto.getPlaceOfBirth() != null) {
								dvi.setPlaceOfBirth(rpddto.getPlaceOfBirth());
							}
							if (rpddto.getVisaDocPlaceOfIssue() != null) {
								dvi.setVisaDocPlaceOfIssue(rpddto.getVisaDocPlaceOfIssue());
							}
							if (rpddto.getVisaApplicableCountry() != null) {
								dvi.setVisaApplicableCountry(rpddto.getVisaApplicableCountry());
							}
							if (rpddto.getVisaDocIssueDate() != null) {
								try {
									SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
									dvi.setVisaDocIssueDate(sf.parse(rpddto.getVisaDocIssueDate()));
								} catch (Exception e) {
								}
							}
							if (rpddto.getTravelDocumentType() != null) {
								dvi.setTravelDocumentType(rpddto.getTravelDocumentType());
							}
						}
						dp.setVisaInfo(dvi);

						ReservationPax rpa = ReservationDAOUtils.DAOInstance.PASSENGER_DAO.getPassenger(rpddto.getPnrPaxId(),
								false, false, false);
						Set<ReservationPax> cset = rpa.getInfants();
						Iterator<ReservationPax> ite = cset.iterator();
						while (ite.hasNext()) {
							ReservationPax infant = (ite.next());
							DCSPassenger dpInfant = new DCSPassenger();
							dpInfant.setTitle(infant.getTitle());
							dpInfant.setFirstName(infant.getFirstName() != null
									? BeanUtils.removeSpaces(infant.getFirstName())
									: "");
							dpInfant.setLastName(infant.getLastName() != null ? BeanUtils.removeSpaces(infant.getLastName()) : "");
							dpInfant.setPaxType(infant.getPaxType());
							dpInfant.setGroupID(rpddto.getGroupId());
							dpInfant.setDateOfBirth(infant.getDateOfBirth());
							EticketTO eTicket = eTicketDAO.getPassengerETicketDetail(infant.getPnrPaxId(), flightId);
							if (eTicket != null) {
								dpInfant.setETicket(eTicket.getEticketNumber());
							}
							if (infant.getNationalityCode() != null) {
								Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(
										infant.getNationalityCode());
								if (nat != null && nat.getIsoCode() != null) {
									dpInfant.setNationality(nat.getIsoCode());
								}
							}
							dp.setInfant(dpInfant);
						}
						PnlAdlUtil.compileMealDetails(pnrPaxIdMealMap, rpddto.getPnrPaxId(), dp);
						PnlAdlUtil.compileSeatDetails(pnrPaxIdSeatMap, rpddto.getPnrPaxId(), dp);
						PnlAdlUtil.compileBaggageDetails(pnrPaxIdBaggageMap, rpddto.getPnrPaxId(), dp);
						PnlAdlUtil.compileSsrDetails(pnrPaxIdSSRMap, rpddto.getPnrPaxId(), dp);
						pme.getPassengers().add(dp);
					}
					dfs.getPnrAndPassengers().add(pme);
				}
				dfsme.getFlightSegments().add(dfs);

				pnlInfo.getSegmentInfo().add(dfsme);
			}
		}
	}

	/**
	 * Collection contains ReservationPaxDetailsDTO's
	 * 
	 * @param passengerList
	 */
	private void updatePNLStatus(Collection<ReservationPaxDetailsDTO> passengerList, HashMap<String, String> groupPaxMap,
			Set<String> pnlPnrs,HashMap<String, String> sitaStatusMap) throws ModuleException {

		if (passengerList == null || passengerList.size() == 0) {
			return;
		}

		Iterator<ReservationPaxDetailsDTO> it = passengerList.iterator();
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();
		while (it.hasNext()) {

			ReservationPaxDetailsDTO dto1 = it.next();

			if (dto1.getPnrSegId() == -1) {
				continue;
			}

			int id = dto1.getPnrPaxId().intValue();
			pnrPaxIds.add(new Integer(id));
			pnrSegIds.add(dto1.getPnrSegId());

		}

		if (pnrSegIds.size() == 0 || pnrPaxIds.size() == 0) {
			log.error("Empty PnrSegIds or PnrPaxIds Found!");
			throw new ModuleException(PNLConstants.ERROR_CODES.EMPTY_PNRSEGS_PAXIDS_ERROR);
		} else {
			if(PnlAdlUtil.isFailedForAllEmails(sitaStatusMap)){
				PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIds, pnrPaxIds, PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT,
						groupPaxMap, PNLConstants.MessageTypes.ADL);
				PnlAdlUtil.updatePnlPassengerStatus(pnrSegIds);
			}else{
				ReservationModuleUtils.getReservationAuxilliaryBD().updatePaxFareSegments(pnrSegIds, pnrPaxIds, PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT,
						groupPaxMap);
			}

		}

	}

	/**
	 * Collection contains ReservationPaxDetailsDTO's
	 * 
	 * @param passengerList
	 */
	private void updatePNLStatus(Collection<Integer> pnrPaxIds,Collection<Integer> pnrSegIds, HashMap<String, String> groupPaxMap,
			Set<String> pnlPnrs) throws ModuleException {

		if (pnrSegIds.size() == 0 || pnrPaxIds.size() == 0) {
			log.error("Empty PnrSegIds or PnrPaxIds Found!");
			throw new ModuleException(PNLConstants.ERROR_CODES.EMPTY_PNRSEGS_PAXIDS_ERROR);
		} else {

			PnlAdlUtil.updatePNLADLStatusInPaxFareSegmentsNew(pnrSegIds, pnrPaxIds, PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT,
					groupPaxMap, PNLConstants.MessageTypes.PNL);
			PnlAdlUtil.updatePnlPassengerStatus(pnrSegIds);

		}

	}
	
	private Collection<Integer> getPnrPaxIds(Map<Integer,Integer> pnrPaxVsSegmentIds){
		return pnrPaxVsSegmentIds.keySet();
	}
	
	private Collection<Integer> getPnrSegmentIds(Map<Integer,Integer> pnrPaxVsSegmentIds){
		return pnrPaxVsSegmentIds.values();
	}
	
	/**
	 * Check if Adl Time Exceeds Flight Closure Time
	 * 
	 * @param flightId
	 * @throws ModuleException
	 */
	private void checkPnlTimeExceedsFlightClosureTime(int flightId) throws ModuleException {

		long nowts = currentTimeStamp.getTime();
		FlightBD fbd = ReservationModuleUtils.getFlightBD();
		Flight flight = fbd.getFlight(flightId);
		String fgap = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP);
		int ifgap = Integer.parseInt(fgap);
		long tsl = flight.getDepartureDate().getTime() - ifgap * 60 * 1000;

		if (nowts > tsl)
			throw new ModuleException("airreservations.auxilliary.pnltimeexceeded", "airreservations");
	}

	private void replaceCabinClassWithBookingClass(Collection<PNLADLDestinationDTO> pdlc) {
		for (PNLADLDestinationDTO pnlAdl : pdlc) {
			pnlAdl.setCabinClassCode(pnlAdl.getBookingCode());
		}

	}
}