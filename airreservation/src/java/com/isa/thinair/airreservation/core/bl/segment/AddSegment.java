/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 

 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CSTypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.airporttransfer.AirportTransferBL;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AirportTransferAdaptor;
import com.isa.thinair.airreservation.core.bl.common.AutoCancellationBO;
import com.isa.thinair.airreservation.core.bl.common.AutomaticCheckinAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.CloneBO;
import com.isa.thinair.airreservation.core.bl.common.ExternalAirportTransferChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalAutoCheckinChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalBaggageChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalFlexiChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalMealChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalNameChangeChargeBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalSMChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalSSRChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalServiceTaxChargesBL;
import com.isa.thinair.airreservation.core.bl.common.MealAdapter;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.util.ETicketGenerator;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for adds segments to an existing reservation
 * 
 * Business Rules: (1) No Passenger level charges exist (2) Only ond charges exist
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="addSegment"
 */
public class AddSegment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(AddSegment.class);

	/** Holds whether modification charges applies or not */
	private Boolean isModifyChgOperation;

	/** Holds reservation payment types */
	private Integer pnrPaymentTypes;

	/** Holds whether Credentials information */
	private CredentialsDTO credentialsDTO;

	/** Holds the confirmed pnr passenger ids */
	private final Collection<Integer> confirmPnrPaxIds = new ArrayList<Integer>();

	private boolean isSegmentExchanged;

	/** AddSegment Constructor */
	public AddSegment() {
	}

	/**
	 * Set arguments
	 * 
	 * @param isModifyChgApply
	 * @param pnrPaymentTypes
	 * @param credentialsDTO
	 * @param isSegmentExchanged
	 */
	private void setArguments(Boolean isModifyChgOperation, Integer pnrPaymentTypes, CredentialsDTO credentialsDTO,
			boolean isSegmentExchanged) {
		this.isModifyChgOperation = isModifyChgOperation;
		this.pnrPaymentTypes = pnrPaymentTypes;
		this.credentialsDTO = credentialsDTO;
		this.isSegmentExchanged = isSegmentExchanged;
	}

	/**
	 * Execute method of the AddSegment command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = this.getParameter(CommandParamNames.PNR, String.class);
		Collection<ReservationSegment> pnrSegments = this.getParameter(CommandParamNames.RESERVATION_SEGMENTS, Collection.class);
		Collection<ExternalPnrSegment> pnrExtSegments = this.getParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS,
				Collection.class);
		Collection<ReservationPaxFare> pnrFaresForAdult = this.getParameter(CommandParamNames.RESERVATION_PAX_FARE_FOR_ADULT,
				Collection.class);
		Collection<ReservationPaxFare> pnrFaresForChild = this.getParameter(CommandParamNames.RESERVATION_PAX_FARE_FOR_CHILD,
				Collection.class);
		Collection<ReservationPaxFare> pnrFaresForInfant = this.getParameter(CommandParamNames.RESERVATION_PAX_FARE_FOR_INFANT,
				Collection.class);
		Boolean isModifyChgOperation = this.getParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION, Boolean.class);
		Boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.class);
		Boolean isNoBalanceToPay = this.getParameter(CommandParamNames.IS_NO_BALANCE_TO_PAY, Boolean.class);
		Integer pnrPaymentTypes = this.getParameter(CommandParamNames.PNR_PAYMENT_TYPES, Integer.class);
		Map<Integer, RevenueDTO> passengerRevenueMap = this.getParameter(CommandParamNames.PASSENGER_REVENUE_MAP, Map.class);
		Map<Integer, IPayment> pnrPaxIdAndPayments = this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, Map.class);
		CredentialsDTO credentialsDTO = this.getParameter(CommandParamNames.CREDENTIALS_DTO, CredentialsDTO.class);
		Collection<TempSegBcAlloc> blockIds = this.getParameter(CommandParamNames.BLOCK_KEY_IDS, Collection.class);
		Date zuluReleaseTimeStamp = this.getParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, Date.class);
		Long version = this.getParameter(CommandParamNames.VERSION, Long.class);
		Collection<OndFareDTO> colOndFareDTO = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
		Date lastModificationTimeStamp = this.getParameter(CommandParamNames.LAST_MODIFICATION_TIMESTAMP, Date.class);
		String lastCurrencyCode = this.getParameter(CommandParamNames.LAST_CURRENCY_CODE, String.class);
		Boolean isFlexiSelected = this.getParameter(CommandParamNames.IS_FLEXI_SELECTED, Boolean.class);
		Collection<Integer> linkedGroundStations = this.getParameter(CommandParamNames.CANCELLED_SURFACE_SEGMENT_LIST,
				Collection.class);
		Integer connectingFltSegId = this.getParameter(CommandParamNames.CONNECTING_FLT_SEG_ID, Integer.class);
		DefaultServiceResponse output = this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE,
				DefaultServiceResponse.class);
		Boolean useOtherCarrierPaxCerdit = this.getParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED, Boolean.class);
		Collection<PaymentInfo> colPaymentInfo = this.getParameter(CommandParamNames.PAYMENT_INFO, Collection.class);
		String fareDiscountAudit = this.getParameter(CommandParamNames.FARE_DISCOUNT_AUDIT, String.class);
		String fareDiscountCode = this.getParameter(CommandParamNames.FARE_DISCOUNT_CODE, String.class);
		String userNotes = this.getParameter(CommandParamNames.USER_NOTES, String.class);
		String userNoteType = this.getParameter(CommandParamNames.USER_NOTE_TYPE, String.class);
		String endorsementNotes = this.getParameter(CommandParamNames.ENDORSEMENT_NOTES, String.class);
		Boolean hasExternalCarrierPayments = this.getParameter(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS, Boolean.class);
		Boolean isModifyingConfSegOfOr = this.getParameter(CommandParamNames.IS_OPEN_RETURN_CONF_SEG_MODIFICATION, Boolean.class);
		Boolean isRequote = getParameter(CommandParamNames.IS_REQUOTE, Boolean.FALSE, Boolean.class);
		Collection<Integer> exchangedPnrSegIds = getParameter(CommandParamNames.EXCHANGE_PNR_SEGMENT_IDS, Collection.class);
		Map<Integer, Map<Integer, Integer>> exgReprotectSsrFltSegInv = getParameter(
				CommandParamNames.EXCHG_REPROTD_SSR_FLTSEG_INVENT, Map.class);
		FlownAssitUnit flownAssitUnit = getParameter(CommandParamNames.FLOWN_ASSIT_UNIT, FlownAssitUnit.class);
		Boolean updateFlownValidity = getParameter(CommandParamNames.UPDATE_FLOWN_VALIDITY, Boolean.FALSE, Boolean.class);
		Date ticketValidTillDate = getParameter(CommandParamNames.TICKET_VALID_TILL_DATE, Date.class);
		Collection<Integer> validityApplicableFltSegIds = getParameter(CommandParamNames.TICKET_VALID_FLT_SEG_IDS,
				Collection.class);
		boolean doRevPurchase = getParameter(CommandParamNames.DO_REV_PURCHASE, Boolean.FALSE, Boolean.class);
		Collection<Integer> exchangedSegInvIds = getParameter(CommandParamNames.EXCHANGE_SEG_INV_IDS, Collection.class);
		boolean isCancelOnDOperation = getParameter(CommandParamNames.IS_CANCEL_OND_OPERATION, Boolean.FALSE, Boolean.class);
		AutoCancellationInfo autoCancellationInfo = getParameter(CommandParamNames.AUTO_CANCELLATION_INFO,
				AutoCancellationInfo.class);
		Map<String, String> groundFltSegByFltSegIdMap = this.getParameter(CommandParamNames.GROUND_SEG_BY_AIR_SEG_ID_MAP,
				Map.class);

		Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap = this.getParameter(CommandParamNames.OND_WISE_PAX_DUE_AMOUNT_MAP,
				Map.class);
		CSTypeBRequestDTO csTypeBRequestDTO = getParameter(CommandParamNames.CS_TYPE_B_REQUEST, CSTypeBRequestDTO.class);
		Boolean triggerPaidAmountError = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PAID_AMOUNT_ERROR);
		Boolean triggerPnrForceConfirmed = (Boolean) this.getParameter(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED);

		ReservationDiscountDTO reservationDiscountDTO = (ReservationDiscountDTO) this
				.getParameter(CommandParamNames.RESERVATION_DISCOUNT_DTO);

		ChargeTnxSeqGenerator chgTnxGen = this.getParameter(CommandParamNames.CHG_TRNX_GEN, ChargeTnxSeqGenerator.class);

		boolean isSegmentExchanged = false;
		if (isRequote) {
			isSegmentExchanged = (exchangedPnrSegIds != null && exchangedPnrSegIds.size() > 0);
		}
		// old pnr segment Ids
		Collection<Integer> oldSegmentIds = this.getParameter(CommandParamNames.PNR_SEGMENT_IDS, Collection.class);

		// only when cnx is present in requote
		if (isRequote && (colOndFareDTO == null || colOndFareDTO.size() == 0 || pnrSegments == null || pnrSegments.size() == 0)) {
			doRevPurchase = false;
			ServiceResponce r = new DefaultServiceResponse(true);
			r.addResponceParam(CommandParamNames.DO_REV_PURCHASE, doRevPurchase);
			return r;
		} else if (isRequote && doRevPurchase && !isSegmentExchanged) {
			doRevPurchase = true;
		}
		// Checking params
		this.validateParams(pnr, pnrSegments, pnrFaresForAdult, pnrFaresForChild, pnrFaresForInfant, isModifyChgOperation,
				passengerRevenueMap, pnrPaxIdAndPayments, pnrPaymentTypes, blockIds, version, colOndFareDTO, credentialsDTO,
				isRequote);

		// Set Arguments
		this.setArguments(isModifyChgOperation, pnrPaymentTypes, credentialsDTO, isSegmentExchanged);

		// Retrieves the Reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
		if (chgTnxGen == null) {
			chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		}

		Set<String> originalCsOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
		Map<Integer, List<SSRExternalChargeDTO>> anciMap = null;
		if (this.getParameter(CommandParamNames.SELECTED_ANCI_MAP) != null) {
			anciMap = (Map<Integer, List<SSRExternalChargeDTO>>) this.getParameter(CommandParamNames.SELECTED_ANCI_MAP);
		}
		if (reservation.getAdminInfo().getOwnerChannelId() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES
				&& AppSysParamsUtil.enableCutoffTimeForAPIReservation()) {
			validateSegAvailability(pnrSegments, reservation.getAdminInfo().getOwnerChannelId());
		}

		String originalReservationStatus = reservation.getStatus();

		boolean isFirstPayment = false;

		if ((originalReservationStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD))
				|| (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)
						&& reservation.getTotalPaidAmount().doubleValue() == 0)) {
			isFirstPayment = true;
		}

		// Save auto cancellation information
		AutoCancellationBO.saveAutoCancellationInfo(autoCancellationInfo, pnr, credentialsDTO);

		// Add auto cancellation information to segments
		this.addAutoCancellationInfo(pnrSegments, autoCancellationInfo, oldSegmentIds, reservation.getSegments(), isRequote,
				exchangedPnrSegIds);

		// add segment sequence for Airport Service, In-flight Service
		this.updateAncillarySegmentSequence(pnrSegments, pnrPaxIdAndPayments);

		// Check add segment constraints
		this.checkAddSegmentConstraints(reservation, pnrSegments, pnrPaxIdAndPayments, version, isFlexiSelected);

		this.updateGroupPnr(reservation, pnr, useOtherCarrierPaxCerdit);

		// Allocate booking classes
		this.allocateBookingClasses(pnrFaresForAdult, pnrFaresForChild, pnrFaresForInfant, reservation, colOndFareDTO);

		// get exchanged pnr segment's pnl status

		// Add segments
		this.addSegments(reservation, pnrSegments);

		// Add external segments
		this.addExternalSegments(reservation, pnrExtSegments);

		// Add fares and charges
		this.addFaresAndCharges(reservation, pnrFaresForAdult, pnrFaresForChild, pnrFaresForInfant, passengerRevenueMap,
				pnrPaxIdAndPayments, zuluReleaseTimeStamp, ondWisePaxDueAdjMap, reservationDiscountDTO,
				reservation.isInfantPaymentRecordedWithInfant(), flownAssitUnit);

		// Adds any external charges for confirmed passengers
		this.addExternalCharges(reservation, pnrPaxIdAndPayments, passengerRevenueMap, credentialsDTO, anciMap, chgTnxGen);

		// If dummy booking the change the dummy booking flag to no
		this.changeDummyState(reservation);

		this.appendEndorsementNotes(reservation, endorsementNotes);

		// re-quote flow exchanging segment pax status should remain same as original/old ReservationPaxFareSegment
		if (isSegmentExchanged) {
			updateExchangeSegmentPaxFareSegment(flownAssitUnit, reservation, exchangedPnrSegIds);
		}

		if (reservation.getFareDiscountCode() == null && fareDiscountCode != null) {
			// Save discount code if there is no discount code while creating the reservation
			reservation.setFareDiscountCode(fareDiscountCode);
		}

		// Leave the states as it is
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {

			// Handling confirmed reservations
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
				if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)) {
					// If it's confirmed get the ownership
					this.captureOwnerShip(reservation);
				}
				// Handling cancel reservations
			} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
				// Update cancel states
				this.updateCancelStates(reservation, hasExternalCarrierPayments);
			}
		} else {
			// Remove infant external charge payment here. Currently for flexi
			if (isFlexiSelected != null && isFlexiSelected.booleanValue()) {
				this.linkInfantExternalCharges(reservation, pnrPaxIdAndPayments, true);
			}

			// Update the general states
			this.updateGeneralStates(reservation, pnrPaxIdAndPayments);

			// Process force mode
			this.checkForceMode(reservation);
		}

		// Check Restricted Segment constraints
		ValidationUtils.checkRestrictedSegmentConstraints(colOndFareDTO, reservation.getStatus(), pnrPaymentTypes);

		this.checkForIBEOpenReturnSegExistsAndTransferOwnership(reservation, colOndFareDTO);

		// notify adl connected segments
		Collection<Integer> addflightSegIds = new ArrayList<Integer>();
		Iterator<ReservationSegment> iterPnrSegments = pnrSegments.iterator();
		while (iterPnrSegments.hasNext()) {
			ReservationSegment resSegment = iterPnrSegments.next();
			addflightSegIds.add(resSegment.getFlightSegId());
		}
		if (!reservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			ADLNotifyer.updateConnectedSegmentsWhenAddSegment(reservation, addflightSegIds);
		}

		ReservationApiUtils.updateModificationStates(reservation, lastModificationTimeStamp, lastCurrencyCode, credentialsDTO);

		// add connection refernce if adding a surface segment
		if (connectingFltSegId != null) {
			this.addConnectedSegmentReferance(reservation, pnrSegments, connectingFltSegId);
		}
		// update surface segment on update segment
		this.updateSurfaceSegmentReferance(reservation, pnrSegments, linkedGroundStations);

		// Update Bus Segment related information on Re-Quote flow
		if (isRequote && AppSysParamsUtil.isGroundServiceEnabled()) {
			ReservationApiUtils.updateGroundSegmentReference(pnrSegments, groundFltSegByFltSegIdMap);
		}

		if (updateFlownValidity && flownAssitUnit != null) {
			FlownSegmentBO.updateTicketValidity(reservation, flownAssitUnit, ticketValidTillDate, validityApplicableFltSegIds);
			updateFlownValidity = false;
		}

		Collection<Integer> newlyAddedFlightSegIds = getNewlyAddedFlightSegIds(reservation, colOndFareDTO, flownAssitUnit);

		if (AppSysParamsUtil.isAgentCommmissionEnabled() && !isRequote) { // TODO: handle for requote
			Command agentCommission = (Command) ReservationModuleUtils.getBean(CommandNames.GRANT_AGENT_COMMISSION);
			agentCommission.setParameter(CommandParamNames.PAYMENT_INFO, this.getParameter(CommandParamNames.PAYMENT_INFO));
			agentCommission.setParameter(CommandParamNames.RESERVATION, reservation);
			agentCommission.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			agentCommission.setParameter(CommandParamNames.IS_CAPTURE_PAYMENT,
					this.getParameter(CommandParamNames.IS_CAPTURE_PAYMENT));
			agentCommission.setParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
			agentCommission.setParameter(CommandParamNames.AGENT_COMMISSION_DTO,
					this.getParameter(CommandParamNames.AGENT_COMMISSION_DTO));
			agentCommission.setParameter(CommandParamNames.ONHOLD_OR_FORCE_CONFIRM,
					this.getParameter(CommandParamNames.ONHOLD_OR_FORCE_CONFIRM));
			agentCommission.execute();
		}

		// update pnl status of passenger for exchange segments
		// if (isRequote) {
		// this.updatePassengerPnlStatusOfNewSegments(reservation, exchangedPnrSegIds);
		// }

		// Save the Reservation
		ReservationProxy.saveReservation(reservation);

		// check selected SSRs inventory available
		if (AppSysParamsUtil.isInventoryCheckForSSREnabled()) {
			ValidationUtils.checkSSRInventoryAvailability(reservation, exgReprotectSsrFltSegInv);
		}

		if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
			// send typeB message
			TypeBRequestDTO typeBSyncRequestDTO = new TypeBRequestDTO();
			typeBSyncRequestDTO.setReservation(reservation);
			typeBSyncRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.ADD_SEGMENT.getCode());
			typeBSyncRequestDTO.setNewlyAddedFlightSegIds(newlyAddedFlightSegIds);
			TypeBRequestComposer.sendTypeBRequest(typeBSyncRequestDTO);

			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
				TypeBRequestDTO ctypeBRequestDTO = new TypeBRequestDTO();
				ctypeBRequestDTO.setReservation(reservation);
				ctypeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.ONHOLD_PAY_CONFIRM.getCode());
				TypeBRequestComposer.sendTypeBRequest(ctypeBRequestDTO);
			}
		}

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(pnrSegments);

		if (csTypeBRequestDTO != null) {
			TypeBRequestDTO typeBRequestDTO = csTypeBRequestDTO.getTypeBRequestDTO();
			Map<Integer, SegmentSSRAssembler> ssrAssemblerMap = getSSRAssemblerMap(reservation);
			if (csOCCarrirs == null) {
				csOCCarrirs = new HashSet<String>();
			}
			csOCCarrirs.addAll(csTypeBRequestDTO.getCsOCCarrirs());
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setNewlyAddedFlightSegIds(newlyAddedFlightSegIds);
			typeBRequestDTO.setExchangedPnrSegIds(exchangedPnrSegIds);
			typeBRequestDTO.setSsrAssemblerMap(ssrAssemblerMap);
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		} else if (csOCCarrirs != null) {
			Map<Integer, SegmentSSRAssembler> ssrAssemblerMap = getSSRAssemblerMap(reservation);
			Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps = getPaxSegmentSSRDTOMap(
					ssrAssemblerMap, reservation);
			if (originalCsOCCarrirs == null) {
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CREATE_ONHOLD.getCode());
				typeBRequestDTO.setSsrAssemblerMap(ssrAssemblerMap);
				typeBRequestDTO.setPaxSegmentSSRDTOMaps(paxSegmentSSRDTOMaps);
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			} else {
				// send Codeshare typeB message
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(reservation);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.ADD_SEGMENT.getCode());
				typeBRequestDTO.setNewlyAddedFlightSegIds(newlyAddedFlightSegIds);
				typeBRequestDTO.setSsrAssemblerMap(ssrAssemblerMap);
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			}
		}

		Boolean[] isOrOrModification = new Boolean[] { new Boolean(false) };
		Collection<ReservationPax> etGenerationEligiblePaxForAdl = reservation.getPassengers();
		Collection<ReservationPax> passengersAlreadyHavingEticket = (Collection<ReservationPax>) this
				.getParameter(CommandParamNames.ETICKET_HAVING_PASSENGERS);

		// save e tickets for own booking
		if ((reservation.getOriginatorPnr() == null || (reservation.getOriginatorPnr() != null && useOtherCarrierPaxCerdit))
				&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			// Retrieves the Reservation
			pnrModesDTO.setLoadSegView(true);
			reservation = ReservationProxy.getReservation(pnrModesDTO);
			CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
			tktInfoDto.setBSPPayment(ReservationApiUtils.isBSPpayment(colPaymentInfo));

			if (originalReservationStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)
					|| (!isRequote && !isModifyChgOperation)) {
				// generate new tickets,
				ETicketBO.updateETickets(ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto),
						reservation.getPassengers(), reservation, false);
				etGenerationEligiblePaxForAdl = reservation.getPassengers();
			} else if (isRequote || isModifyChgOperation) {
				Collection<Integer> newSegmentList = new ArrayList<Integer>(addflightSegIds);
				Collection<Integer> oldPnrSegIdList = new ArrayList<Integer>(oldSegmentIds);
				if (isSegmentExchanged) {
					oldPnrSegIdList.addAll(exchangedPnrSegIds);
				}
				if (this.isOndChange(oldPnrSegIdList, newSegmentList, reservation, isModifyingConfSegOfOr, isOrOrModification)
						&& !isCancelOnDOperation) {
					if (isActualPayment || isNoBalanceToPay) {
						// generate new tickets for ond change for all pax

						Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
						modifiedPaxfareSegmentEtickets = ETicketBO.updateETickets(
								ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto),
								reservation.getPassengers(), reservation, false);
						etGenerationEligiblePaxForAdl = reservation.getPassengers();
						if (!modifiedPaxfareSegmentEtickets.isEmpty()) {
							ADLNotifyer.recordReservation(reservation, AdlActions.A, etGenerationEligiblePaxForAdl, null);
							ADLNotifyer.recordExchangedETicketForAdl(modifiedPaxfareSegmentEtickets, reservation,
									etGenerationEligiblePaxForAdl);
						}
					} else {
						Collection<ReservationPax> etGenerationEligiblePax = ETicketGenerator
								.getETGenerationEligiblePaxInCNFBookings(reservation, pnrPaxIdAndPayments, triggerPaidAmountError,
										triggerPnrForceConfirmed, false, false, credentialsDTO);
						Collection<ReservationPax> remainingPax = ReservationApiUtils.getRemainingPax(reservation,
								etGenerationEligiblePax);
						if (!etGenerationEligiblePax.isEmpty()
								&& !originalReservationStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
							// generate new tickets for ond change for pax who get confirm from previous credits
							ETicketBO.updateETickets(
									ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto),
									etGenerationEligiblePax, reservation, false);
						}
						if (!remainingPax.isEmpty()) {
							// exchange tickets for date modification if no payment included
							ETicketBO.exchangeETicketsForGivenPassengers(reservation,
									getSortedOldpnrSegmentIds(oldPnrSegIdList, reservation, isModifyingConfSegOfOr,
											isOrOrModification[0].booleanValue()),
									getNewFlightSegmentSequnceNos(pnrSegments, reservation), remainingPax);
						}
						etGenerationEligiblePaxForAdl = etGenerationEligiblePax;
					}
				} else {
					if (isActualPayment) {
						// generate new tickets for date modification if payment included
						ETicketBO.updateETickets(ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto),
								reservation.getPassengers(), reservation, false);
						etGenerationEligiblePaxForAdl = reservation.getPassengers();
					} else {
						Collection<ReservationPax> etGenerationEligiblePax = ETicketGenerator
								.getETGenerationEligiblePaxInCNFBookings(reservation, pnrPaxIdAndPayments, triggerPaidAmountError,
										triggerPnrForceConfirmed, false, false, credentialsDTO);
						if (isNoBalanceToPay && reservation.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
							if ((!etGenerationEligiblePax.isEmpty()
									&& !originalReservationStatus.equals(ReservationInternalConstants.ReservationStatus.ON_HOLD))
									|| isCancelOnDOperation) {
								// Generate ET for previously forced confirmed adults/childs
								ETicketBO.updateETickets(
										ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, tktInfoDto),
										reservation.getPassengers(), reservation, false);
							}
						} else {
							// exchange tickets for date modification if no payment included
							ETicketBO.exchangeETickets(reservation,
									getSortedOldpnrSegmentIds(oldPnrSegIdList, reservation, isModifyingConfSegOfOr,
											isOrOrModification[0].booleanValue()),
									getNewFlightSegmentSequnceNos(pnrSegments, reservation));
						}
						etGenerationEligiblePaxForAdl = etGenerationEligiblePax;
					}
				}
			}
		}

		// Adding added segment details to send ADL
		if (!reservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			ADLNotifyer.recordAddedSegmentsForAdl(reservation, addflightSegIds, originalReservationStatus,
					etGenerationEligiblePaxForAdl, passengersAlreadyHavingEticket);
		}

		// send SSR add notifications
		SSRBL.sendSSRAddNotifications(reservation, pnrSegments, credentialsDTO);

		Map<Integer, LinkedList<Integer>> flgtSegmentMap = this.getFlightSegmentMap(pnrSegments);

		// Save the seating information
		Map<Integer, String> flightSeatIdsAndPaxTypes = ReservationBO.saveSeats(pnrPaxIdAndPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Save the Meal Information
		Collection[] arrFlightMealInfo = ReservationBO.saveMeals(pnrPaxIdAndPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Saves the airport transfer information
		Collection[] flightAirportTransferInfo = ReservationBO.saveAirportTransfers(pnrPaxIdAndPayments, flgtSegmentMap,
				reservation, credentialsDTO);

		// Save the Baggage Information
		Collection[] arrFlightBaggageInfo = BaggageAdaptor.saveBaggages(pnrPaxIdAndPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Saves the airport transfer information
		Collection[] flightAutoCheckinsInfo = ReservationBO.saveAutoCheckins(pnrPaxIdAndPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		sendAirportTransfeNotifications(reservation, isSegmentExchanged, exchangedPnrSegIds, pnrSegments, flownAssitUnit);
		// Process Seats for passengers
		this.processPassengerSeats(blockIds, reservation, flightSeatIdsAndPaxTypes, arrFlightMealInfo[1], arrFlightBaggageInfo[1],
				exchangedSegInvIds);

		// Get the seat information
		String seatCodes = getSeatCodesForAudit(flightSeatIdsAndPaxTypes);

		// Updated Passaznger Segments Ancillary Status
		ReservationCoreUtils.updateSegmentsAncillaryStatus(reservation);

		// Get the meal information
		Collection[] colResPaxInfo = { reservation.getPassengers(), reservation.getSegmentsView() };
		String mealCodes = MealAdapter.recordPaxWiseAuditHistoryForModifyMeals(reservation.getPnr(), arrFlightMealInfo[0],
				credentialsDTO, null, colResPaxInfo);

		String aptDetails = AirportTransferAdaptor.recordPaxWiseAuditHistoryForModifyAirportTransfers(reservation.getPnr(),
				flightAirportTransferInfo[0], credentialsDTO, null, colResPaxInfo);

		String baggageNames = BaggageAdaptor.recordPaxWiseAuditHistoryForModifyBaggages(reservation.getPnr(),
				arrFlightBaggageInfo[0], credentialsDTO, null, colResPaxInfo);

		String autoCheckins = null;
		if (flightAutoCheckinsInfo != null && flightAutoCheckinsInfo.length > 0) {
			autoCheckins = AutomaticCheckinAdaptor.recordPaxWiseAuditHistoryForModifyAutoCheckin(flightAutoCheckinsInfo[0]);
		}

		// Get flexi info
		// Saves flexi charges
		Collection<FlexiExternalChgDTO> flexiCharges = ReservationBO.saveFlexiCharges(pnrPaxIdAndPayments, flgtSegmentMap,
				reservation, credentialsDTO);
		String flexiInfo = "";
		if (flexiCharges != null && flexiCharges.size() > 0) {
			flexiInfo = ReservationApiUtils.getFlexiInfoForAudit(flexiCharges, reservation,
					getNewSegmentSequenceNos(pnrSegments, flownAssitUnit, reservation));
		}

		if (isSegmentExchanged) {
			reprotectAlerts(flownAssitUnit, reservation, exchangedPnrSegIds);
		}

		String bookingType = "";
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			bookingType = (String) this.getParameter(CommandParamNames.BOOKING_TYPE);
		}

		// Composing the reservation audit object
		Collection<ReservationAudit> colReservationAudit = this.composeAudit(pnr, pnrSegments, pnrPaxIdAndPayments, reservation,
				zuluReleaseTimeStamp, seatCodes, mealCodes, aptDetails, baggageNames, flexiInfo, autoCheckins, fareDiscountAudit,
				userNotes,
				bookingType, userNoteType);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);

		if (isRequote) {
			Collection<ReservationAudit> colRequoteReservationAudit = new ArrayList<ReservationAudit>();
			if (this.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION) != null) {
				colRequoteReservationAudit = (Collection<ReservationAudit>) this
						.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION);
			}
			colRequoteReservationAudit.addAll(colReservationAudit);
			response.addResponceParam(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION, colRequoteReservationAudit);
		}

		if (flexiInfo != null && !"".equals(flexiInfo)) {
			response.addResponceParam(CommandParamNames.UPDATED_FLEXIBILITIES_INFORMATION, flexiInfo);
		}

		Iterator<ReservationAudit> itColReservationAudit = colReservationAudit.iterator();
		ReservationAudit reservationAudit;
		while (itColReservationAudit.hasNext()) {
			reservationAudit = itColReservationAudit.next();

			if (reservationAudit.getContentMap()
					.containsKey(AuditTemplateEnum.TemplateParams.AddedNewSegment.SEGMENT_INFORMATION)) {
				response.addResponceParam(CommandParamNames.ADD_SEGMENT_INFORMATION, reservationAudit.getContentMap()
						.get(AuditTemplateEnum.TemplateParams.AddedNewSegment.SEGMENT_INFORMATION));
			}
		}

		if (output == null) {
			output = new DefaultServiceResponse();
			output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		} else {
			output.addResponceParam(CommandParamNames.VERSION, new Long(reservation.getVersion()));
		}

		// Remove infant external charge payment here. Currently for flexi
		if (isFlexiSelected != null && isFlexiSelected.booleanValue()) {
			this.linkInfantExternalCharges(reservation, pnrPaxIdAndPayments, false);
		}

		response.addResponceParam(CommandParamNames.RESERVATION_PAYMENT_META_TO,
				TnxGranularityReservationUtils.getReservationPaymentMetaTO(reservation));
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.FLIGHT_MEAL_IDS, arrFlightMealInfo);
		response.addResponceParam(CommandParamNames.FLIGHT_AIRPORT_TRANSFER_IDS, flightAirportTransferInfo);
		response.addResponceParam(CommandParamNames.FLIGHT_BAGGAGE_IDS, arrFlightBaggageInfo);
		response.addResponceParam(CommandParamNames.DO_REV_PURCHASE, doRevPurchase);

		response.addResponceParam(CommandParamNames.FLIGHT_SEAT_IDS_AND_PAX_TYPES, flightSeatIdsAndPaxTypes);
		response.addResponceParam(CommandParamNames.FLIGHT_AUTO_CHECKIN_IDS, flightAutoCheckinsInfo);
		response.addResponceParam(CommandParamNames.NEW_SEGMENT_SEQUENCE_NUMBERS,
				getNewSegmentSequenceNos(pnrSegments, flownAssitUnit, reservation));
		response.addResponceParam(CommandParamNames.NEW_NONSTB_SEGMENT_SEQUENCE_NUMBERS,
				getNonSTBSegmentSequenceNos(pnrSegments));
		response.addResponceParam(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());
		response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS, newlyAddedFlightSegIds);
		response.addResponceParam(CommandParamNames.PNR, pnr);
		response.addResponceParam(CommandParamNames.RESERVATION, reservation);
		response.addResponceParam(CommandParamNames.IS_FIRST_PAYMENT, isFirstPayment);
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}

		// to send medical ssr email
		// MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(pnrPaxIdAndPayments, reservation);
		// medicalSsrDTO.sendEmail();

		log.debug("Exit execute");

		return response;
	}

	private void sendAirportTransfeNotifications(Reservation reservation, boolean isSegmentExchanged,
			Collection<Integer> exchangedPnrSegIds, Collection<ReservationSegment> pnrSegments, FlownAssitUnit flownAssitUnit)
			throws ModuleException {

		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			Collection<Integer> notifyPnrSegIds = ReservationApiUtils.getConfirmedPnrSegIds(pnrSegments);
			if (notifyPnrSegIds != null && !notifyPnrSegIds.isEmpty()) {
				if (isSegmentExchanged) {
					for (Integer exchangedPnrSegId : exchangedPnrSegIds) {
						if (notifyPnrSegIds.contains(exchangedPnrSegId)) {
							notifyPnrSegIds.remove(exchangedPnrSegId);
						}
					}
				}
				removeCorrelatedPnrSegIdsForExchagedPnrSegs(notifyPnrSegIds, exchangedPnrSegIds, reservation);
				if (notifyPnrSegIds != null && !notifyPnrSegIds.isEmpty()) {
					AirportTransferBL.sendAirportTransferAddNotifications(reservation.getPnr(), notifyPnrSegIds, credentialsDTO);
				}
			}
		}
	}

	private void removeCorrelatedPnrSegIdsForExchagedPnrSegs(Collection<Integer> notifyPnrSegIds,
			Collection<Integer> exchangedPnrSegIds, Reservation reservation) {

		Collection<Integer> exchangedFlightSegIds = new ArrayList<Integer>();
		for (ReservationSegment resSegment : reservation.getSegments()) {
			Integer pnrSegId = resSegment.getPnrSegId();
			if (exchangedPnrSegIds.contains(pnrSegId)) {
				exchangedFlightSegIds.add(resSegment.getFlightSegId());
			}

		}

		Collection<Integer> correspondingSegsForExchanged = new ArrayList<Integer>();
		if (!exchangedFlightSegIds.isEmpty()) {
			for (ReservationSegment resSegment : reservation.getSegments()) {
				Integer fltSegId = resSegment.getFlightSegId();
				if (resSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
					if (exchangedFlightSegIds.contains(fltSegId)) {
						correspondingSegsForExchanged.add(resSegment.getPnrSegId());
					}
				}
			}
		}

		if (!correspondingSegsForExchanged.isEmpty()) {
			notifyPnrSegIds.removeAll(correspondingSegsForExchanged);
		}

	}

	private Collection<Integer> getNewlyAddedFlightSegIds(Reservation reservation, Collection<OndFareDTO> colOndFareDTO,
			FlownAssitUnit flownAssitUnit) throws ModuleException {
		Collection<Integer> exchangeFlightSegIds = new ArrayList<Integer>();
		Collection<Integer> newlyAddedFlightSegIds = new ArrayList<Integer>();

		if (flownAssitUnit != null) {
			for (ReservationSegment reservationSeg : reservation.getSegments()) {
				if (flownAssitUnit.getModifyAsst().getExchangedPnrSegIds().contains(reservationSeg.getPnrSegId())) {
					exchangeFlightSegIds.add(reservationSeg.getFlightSegId());
				}
			}
		}

		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			for (Integer fltSegId : ondFareDTO.getFlightSegmentIds()) {
				if (!exchangeFlightSegIds.contains(fltSegId)) {
					newlyAddedFlightSegIds.add(fltSegId);
				}
			}
		}

		return newlyAddedFlightSegIds;
	}

	private void appendEndorsementNotes(Reservation reservation, String endorsementNotes) {
		if (BeanUtils.nullHandler(endorsementNotes).length() > 0) {
			String newNote = endorsementNotes;
			if (BeanUtils.nullHandler(reservation.getEndorsementNote()).length() > 0) {
				newNote = reservation.getEndorsementNote() + " " + endorsementNotes;
			}
			reservation.setEndorsementNote(newNote);
		}
	}

	/**
	 * @param pnrSegments
	 * @return get the sorted segment sequence numbers , sort by segment zulu dep date
	 */
	private Collection<Integer> getNewFlightSegmentSequnceNos(Collection<ReservationSegment> pnrSegments,
			Reservation reservation) {

		List<ReservationSegmentDTO> segmentDTOs = new ArrayList<ReservationSegmentDTO>(reservation.getSegmentsView());
		Collections.sort(segmentDTOs);
		Collection<Integer> segmentSeqNos = new ArrayList<Integer>();

		for (ReservationSegmentDTO segmentDto : segmentDTOs) {
			Iterator<ReservationSegment> segments = pnrSegments.iterator();
			while (segments.hasNext()) {
				ReservationSegment segment = segments.next();
				if (segmentDto.getSegmentSeq().equals(segment.getSegmentSeq())) {
					segmentSeqNos.add(segment.getSegmentSeq());
					break;
				}
			}
			if (segmentSeqNos.size() == pnrSegments.size()) {
				break;
			}
		}
		return segmentSeqNos;
	}

	/**
	 * @param pnrSegments
	 * @return get the sorted segment sequence numbers , sort by segment zulu dep date
	 * @throws ModuleException
	 */
	private Collection<Integer> getSortedOldpnrSegmentIds(Collection<Integer> oldPnrSegIds, Reservation reservation,
			boolean isModifyingConfSegOfOr, boolean isOrOrModification) throws ModuleException {

		List<ReservationSegmentDTO> segmentDTOs = new ArrayList<ReservationSegmentDTO>(reservation.getSegmentsView());
		Collections.sort(segmentDTOs);
		Collection<Integer> sortedPnrSegIds = new ArrayList<Integer>();

		for (ReservationSegmentDTO segmentDto : segmentDTOs) {
			if (oldPnrSegIds.contains(segmentDto.getPnrSegId())) {
				sortedPnrSegIds.add(segmentDto.getPnrSegId());
				for (ReservationSegmentDTO seg : segmentDTOs) {
					if (seg.getOpenRtConfirmBeforeZulu() != null && isModifyingConfSegOfOr && isOrOrModification
							&& seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
							&& segmentDto.getReturnOndGroupId() != null && seg.getReturnOndGroupId() != null
							&& segmentDto.getReturnOndGroupId().equals(seg.getReturnOndGroupId())) {
						sortedPnrSegIds.add(seg.getPnrSegId());
					}
				}
			}
		}

		return sortedPnrSegIds;
	}

	/**
	 * @param oldPnrSegIds
	 * @param newFltSegIds
	 * @param reservation
	 * @param isOrModification
	 * @return
	 * @throws ModuleException
	 */

	private boolean isOndChange(Collection<Integer> oldPnrSegIds, Collection<Integer> newFltSegIds, Reservation reservation,
			boolean isOrModification, Boolean[] isOrOrModification) throws ModuleException {

		List<ReservationSegmentDTO> segmentDTOs = new ArrayList<ReservationSegmentDTO>(reservation.getSegmentsView());
		Collections.sort(segmentDTOs);

		Collection<String> oldSegCode = new ArrayList<String>();
		Collection<String> newSegCode = new ArrayList<String>();
		Collection<String> oldOrSegCode = new ArrayList<String>();
		Collection<String> newOrSegCode = new ArrayList<String>();
		for (ReservationSegmentDTO segment : segmentDTOs) {
			if (oldPnrSegIds.contains(segment.getPnrSegId())) {
				oldSegCode.add(segment.getSegmentCode());

				for (ReservationSegmentDTO seg : segmentDTOs) {
					if (seg.getOpenRtConfirmBeforeZulu() != null
							&& seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
							&& segment.getReturnOndGroupId() != null && seg.getReturnOndGroupId() != null
							&& segment.getReturnOndGroupId().equals(seg.getReturnOndGroupId()) && isOrModification) {
						oldOrSegCode.add(seg.getSegmentCode());
					}
				}

			} else if (newFltSegIds.contains(segment.getFlightSegId())
					&& !segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				newSegCode.add(segment.getSegmentCode());
				newFltSegIds.remove(segment.getFlightSegId());
				if (segment.getOpenRtConfirmBeforeZulu() != null) {
					newOrSegCode.add(segment.getSegmentCode());
				}

			}

		}

		if (newOrSegCode.size() != 0 && oldOrSegCode.size() != 0) {
			String oldOrOndCode = ReservationApiUtils.getOndCode(oldOrSegCode);
			String newOrOndCode = ReservationApiUtils.getOndCode(newOrSegCode);

			if (oldOrOndCode.equals(newOrOndCode)) {
				oldSegCode.addAll(oldOrSegCode);
				isOrOrModification[0] = true;
			}

		}

		String oldOndCode = ReservationApiUtils.getOndCode(oldSegCode);
		String newOndCode = ReservationApiUtils.getOndCode(newSegCode);

		return !oldOndCode.equals(newOndCode);

	}

	/**
	 * save the reservation as a group pnr if it use othe carrier pax credit
	 * 
	 * @param reservation
	 * @param pnr
	 * @param useOtherCarrierPaxCerdit
	 */
	private void updateGroupPnr(Reservation reservation, String pnr, Boolean useOtherCarrierPaxCerdit) {
		if (useOtherCarrierPaxCerdit) {
			reservation.setOriginatorPnr(pnr);
		}
	}

	private void changeDummyState(Reservation reservation) {
		if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.YES) {
			reservation.setDummyBooking(ReservationInternalConstants.DummyBooking.NO);
		}
	}

	private Collection<Integer> getNewSegmentSequenceNos(Collection<ReservationSegment> pnrSegments,
			FlownAssitUnit flownAssitUnit, Reservation reservation) throws ModuleException {
		Collection<Integer> segmentSegNos = new ArrayList<Integer>();
		Set<Integer> exchangedFltSegments = new HashSet<Integer>();
		if (flownAssitUnit != null && flownAssitUnit.getModifyAsst() != null) {
			Map<Integer, Integer> fltSegPnrSegMap = flownAssitUnit.getModifyAsst().getExchangedPnrSegByFltSegIdMap();
			if (fltSegPnrSegMap != null) {
				exchangedFltSegments = fltSegPnrSegMap.keySet();
			}
		}
		
		for (ReservationSegment reservationSegment : pnrSegments) {
			if (exchangedFltSegments.contains(reservationSegment.getFlightSegId())) {
				continue;
			}
			if (shouldRemoveFlexi(reservation, reservationSegment)) {
				continue;
			}
			// Ignore grounds segments from new segment list
			if (!(reservationSegment.getSubStationShortName() != null && !"".equals(reservationSegment.getSubStationShortName()))) {
				segmentSegNos.add(reservationSegment.getSegmentSeq());
			}
		}

		return segmentSegNos;
	}

	private Collection<Integer> getNonSTBSegmentSequenceNos(Collection<ReservationSegment> pnrSegments) throws ModuleException {
		Collection<Integer> segmentSegNos = new ArrayList<Integer>();

		for (ReservationSegment reservationSegment2 : pnrSegments) {
			ReservationSegment reservationSegment = reservationSegment2;
			if (!reservationSegment.getBookingType().equals(BookingClass.BookingClassType.STANDBY)) {
				segmentSegNos.add(reservationSegment.getSegmentSeq());
			}
		}

		return segmentSegNos;
	}

	/**
	 * Check Whether the user is modifying an IBE booking. If it's an IBE booking changing the ownership of the
	 * reservation for the Agent who's performing this operation.
	 * 
	 * @param reservation
	 * @param colOndFareDTO
	 */
	private void checkForIBEOpenReturnSegExistsAndTransferOwnership(Reservation reservation,
			Collection<OndFareDTO> colOndFareDTO) {
		if (SalesChannelsUtil.isSalesChannelWebOrMobile(reservation.getAdminInfo().getOwnerChannelId())) {

			if (isOpenReturnBookingClassTypeExists(colOndFareDTO)) {
				captureOwnerShip(reservation);
			}
		}
	}

	/**
	 * Checks whether any open return booking classes exists or not
	 * 
	 * @param colOndFareDTO
	 * @return
	 */
	private boolean isOpenReturnBookingClassTypeExists(Collection<OndFareDTO> colOndFareDTO) {
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		SeatDistribution seatDistribution;
		OndFareDTO ondFareDTO;

		for (OndFareDTO ondFareDTO2 : colOndFareDTO) {
			ondFareDTO = ondFareDTO2;

			if (ondFareDTO != null && ondFareDTO.getSegmentSeatDistsDTO().size() > 0) {

				for (SegmentSeatDistsDTO segmentSeatDistsDTO2 : ondFareDTO.getSegmentSeatDistsDTO()) {
					segmentSeatDistsDTO = segmentSeatDistsDTO2;

					if (segmentSeatDistsDTO.getEffectiveSeatDistribution() != null
							&& segmentSeatDistsDTO.getEffectiveSeatDistribution().size() > 0) {

						for (SeatDistribution seatDistribution2 : segmentSeatDistsDTO.getEffectiveSeatDistribution()) {
							seatDistribution = seatDistribution2;
							if (BookingClassUtil.isOpenReturnBookingClassType(seatDistribution.getBookingClassCode())) {
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Returns flight segment map to the pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private Map<Integer, LinkedList<Integer>> getFlightSegmentMap(Collection<ReservationSegment> pnrSegments) {
		Map<Integer, LinkedList<Integer>> flgtSegmentMap = new HashMap<Integer, LinkedList<Integer>>();

		ReservationSegment reservationSegment;
		LinkedList<Integer> pnrSegIds;
		for (ReservationSegment reservationSegment2 : pnrSegments) {
			reservationSegment = reservationSegment2;

			pnrSegIds = flgtSegmentMap.get(reservationSegment.getFlightSegId());

			if (pnrSegIds == null) {
				pnrSegIds = new LinkedList<Integer>();
				pnrSegIds.add(reservationSegment.getPnrSegId());

				flgtSegmentMap.put(reservationSegment.getFlightSegId(), pnrSegIds);
			} else {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			}
		}

		return flgtSegmentMap;
	}

	/**
	 * Add external segments
	 * 
	 * @param reservation
	 * @param pnrExtSegments
	 */
	private void addExternalSegments(Reservation reservation, Collection<ExternalPnrSegment> pnrExtSegments) {
		if (pnrExtSegments != null && pnrExtSegments.size() > 0) {
			ExternalPnrSegment reservationExternalSegment;
			for (ExternalPnrSegment externalPnrSegment : pnrExtSegments) {
				reservationExternalSegment = externalPnrSegment;
				reservation.addExternalReservationSegment(reservationExternalSegment);
			}
		}
	}

	/**
	 * Allocate Booking classes
	 * 
	 * @param pnrFaresForAdult
	 * @param pnrFaresForChild
	 * @param pnrFaresForInfant
	 * @param reservation
	 * @param colOndFareDTO
	 * @throws ModuleException
	 */
	private void allocateBookingClasses(Collection<ReservationPaxFare> pnrFaresForAdult,
			Collection<ReservationPaxFare> pnrFaresForChild, Collection<ReservationPaxFare> pnrFaresForInfant,
			Reservation reservation, Collection<OndFareDTO> colOndFareDTO) throws ModuleException {

		// Calling the segment bucket
		// Segment bucket is the one which keep tracks of booking codes
		// Remember infants won't have any booking codes
		SegmentBucketBO segmentBucketBO = new SegmentBucketBO(colOndFareDTO);
		boolean[] paxTypesExist = ReservationApiUtils.isPaxTypesExist(reservation.getPassengers());

		if (paxTypesExist[0]) {
			this.assignBookingClasses(pnrFaresForAdult, segmentBucketBO);
		}

		if (paxTypesExist[1]) {
			this.assignBookingClasses(pnrFaresForChild, segmentBucketBO);
		}
	}

	/**
	 * Assign Booking classes
	 * 
	 * @param colReservationPaxFare
	 * @param segmentBucketBO
	 * @throws ModuleException
	 */
	private void assignBookingClasses(Collection<ReservationPaxFare> colReservationPaxFare, SegmentBucketBO segmentBucketBO)
			throws ModuleException {
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		for (ReservationPaxFare reservationPaxFare2 : colReservationPaxFare) {
			reservationPaxFare = reservationPaxFare2;

			for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
				reservationPaxFareSegment = reservationPaxFareSegment2;
				reservationPaxFareSegment
						.setBookingCode(segmentBucketBO.getBookingCode(reservationPaxFareSegment.getSegment().getFlightSegId()));
			}
		}
	}

	/**
	 * Adds external charges
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param passengerRevenueMap
	 * @param anciMap
	 *            TODO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked" })
	private void addExternalCharges(Reservation reservation, Map pnrPaxIdAndPayments,
			Map<Integer, RevenueDTO> passengerRevenueMap, CredentialsDTO credentialsDTO,
			Map<Integer, List<SSRExternalChargeDTO>> anciMap, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {

		// Reflected CC external charges
		ExternalGenericChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// Reflected SM external charges
		ExternalSMChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// Meal Charges
		ExternalMealChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// Baggage Charges
		ExternalBaggageChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// AutoCheckin Charges
		ExternalAutoCheckinChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// In-flight & Airport Service charges
		ExternalSSRChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, anciMap, chgTxnGen);

		// Flexi charges
		ExternalFlexiChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// Service Tax Charges
		ExternalServiceTaxChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// Airport Service Charges
		ExternalAirportTransferChargesBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);

		// Name Change Charges
		ExternalNameChangeChargeBL.reflectExternalChgForANewSegment(reservation, pnrPaxIdAndPayments, passengerRevenueMap,
				credentialsDTO, chgTxnGen);
	}

	/**
	 * Update Cancel States
	 * 
	 * @param reservation
	 * @param hasExternalCarrierPayments
	 */
	private void updateCancelStates(Reservation reservation, boolean hasExternalCarrierPayments) {
		Collection<Integer> passengerIds = new ArrayList<Integer>();
		ReservationPax reservationPax;

		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			// Catching the parent and adult ids
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				passengerIds.add(reservationPax.getPnrPaxId());
			}
		}

		Map<Integer, Collection<ReservationTnx>> pnrPaxIdsAndColReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPaymentsAndRefunds(passengerIds);
		Collection<ReservationTnx> colAllReservationTnx = new ArrayList<ReservationTnx>();
		Collection<ReservationTnx> colReservationTnx;
		ReservationTnx reservationTnx;
		BigDecimal paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Capturing all transactions
		for (Collection<ReservationTnx> collection : pnrPaxIdsAndColReservationTnx.values()) {
			colReservationTnx = collection;
			colAllReservationTnx.addAll(colReservationTnx);
		}

		// Find the exact payment amount
		for (ReservationTnx reservationTnx2 : colAllReservationTnx) {
			reservationTnx = reservationTnx2;
			paymentAmount = AccelAeroCalculator.add(paymentAmount, reservationTnx.getAmount());
		}

		// Reservation already made payments
		if (hasExternalCarrierPayments || paymentAmount.doubleValue() < 0) {
			for (ReservationPax reservationPax2 : reservation.getPassengers()) {
				reservationPax = reservationPax2;
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED);
			}
			reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);

			if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)) {
				// If it's confirmed get the ownership
				this.captureOwnerShip(reservation);
			}
		} else {
			for (ReservationPax reservationPax2 : reservation.getPassengers()) {
				reservationPax = reservationPax2;
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);
			}
			reservation.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
		}
	}

	/**
	 * Check Add Segment Constraints
	 * 
	 * @param reservation
	 * @param pnrSegments
	 * @param pnrPaxIdAndPayments
	 * @param version
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkAddSegmentConstraints(Reservation reservation, Collection<ReservationSegment> pnrSegments,
			Map pnrPaxIdAndPayments, Long version, Boolean isFlexiSelected) throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Checking the concurrent update
		Collection<Integer> colCurrentOndIds = new HashSet<Integer>();
		Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator();
		ReservationSegment reservationSegment;
		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();
			colCurrentOndIds.add(reservationSegment.getOndGroupId());
		}

		Iterator<ReservationSegment> itPnrSegments = pnrSegments.iterator();
		while (itPnrSegments.hasNext()) {
			reservationSegment = itPnrSegments.next();

			if (colCurrentOndIds.contains(reservationSegment.getOndGroupId())) {
				throw new ModuleException("airreservations.arg.concurrentUpdate");
			}
		}

		// Check if it's valid payments or not
		ReservationCoreUtils.checkValidPaxPaymentsForReservation(reservation, pnrPaxIdAndPayments, isFlexiSelected);
	}

	/**
	 * Update the general states
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateGeneralStates(Reservation reservation, Map pnrPaxIdAndPayments) throws ModuleException {
		Iterator itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		int confirmPaxCount = 0;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(reservationPax.getStatus())) {
				confirmPnrPaxIds.add(reservationPax.getPnrPaxId());
			}

			// Setting the passenger states
			if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
							.equals(pnrPaymentTypes)) {
				// Confirming the passenger
				reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
			} else {
				boolean isOfflinePayment = false;
				PaymentAssembler paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());
				if (paymentAssembler != null && paymentAssembler.getPayments() != null
						&& !paymentAssembler.getPayments().isEmpty()
						&& (paymentAssembler.getPayments().iterator().next() instanceof OfflinePaymentInfo)) {
					isOfflinePayment = true;
				}

				// Returns the passenger confirm status
				if (isOfflinePayment) {
					reservationPax.setStatus(ReservationPaxStatus.ON_HOLD);

				} else {
					boolean status = ReservationApiUtils.isPassengerConfirmedForAModifyBooking(reservationPax,
							pnrPaxIdAndPayments, false, false);
					if (status) {
						confirmPaxCount++;
						reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
					} else {
						if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT.equals(pnrPaymentTypes)) {
							throw new ModuleException("airreservations.arg.paidAmountError");
						} else {
							reservationPax.setStatus(ReservationPaxStatus.ON_HOLD);
						}
					}
				}
			}
		}

		// Confirming the reservation
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {
			reservation.setStatus(ReservationStatus.CONFIRMED);
		} else {
			// If confirmed making the reservation status to confirm
			if (confirmPaxCount == reservation.getPassengers().size()) {
				reservation.setStatus(ReservationStatus.CONFIRMED);
			}
			// If it's not totally confirmed make the reservation on hold
			else {
				reservation.setStatus(ReservationStatus.ON_HOLD);
			}
		}
	}

	/**
	 * Compose the audit
	 * 
	 * @param pnr
	 * @param pnrSegments
	 * @param pnrPaxIdAndPayments
	 * @param reservation
	 * @param zuluReleaseTimeStamp
	 * @param userNotes
	 * @param bookingType TODO
	 * @param userNoteType TODO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked" })
	private Collection<ReservationAudit>
 composeAudit(String pnr, Collection<ReservationSegment> pnrSegments,
			Map pnrPaxIdAndPayments, Reservation reservation, Date zuluReleaseTimeStamp, String seatCodes, String strMeals,
			String aptDetails, String strBaggages, String flexiInfo, String autoCheckinInfo, String fareDiscountInfo,
			String userNotes, String bookingType, String userNoteType)
			throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.ADDED_NEW_SEGMENT.getCode());
		reservationAudit.setUserNote(userNotes);
		reservationAudit.setUserNoteType(userNoteType);

		Iterator<ReservationSegment> itPnrSegments = pnrSegments.iterator();
		ReservationSegment reservationSegment;
		Collection<Integer> colPnrSegmentIds = new ArrayList<Integer>();
		Map<Integer, String> mapPnrSegIdAndCabinClass = new HashMap<Integer, String>();
		Map<Integer, String> mapPnrSegIdAndLogicalCabinClass = new HashMap<Integer, String>();

		while (itPnrSegments.hasNext()) {
			reservationSegment = itPnrSegments.next();
			colPnrSegmentIds.add(reservationSegment.getPnrSegId());
			mapPnrSegIdAndCabinClass.put(reservationSegment.getPnrSegId(), reservationSegment.getCabinClassCode());
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				mapPnrSegIdAndLogicalCabinClass.put(reservationSegment.getPnrSegId(), reservationSegment.getLogicalCCCode());
			}
		}

		// Get main flight information
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		// Collect the reservation segment information
		Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationSegmentDAO
				.getSegmentInformation(colPnrSegmentIds);
		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		StringBuffer changes = new StringBuffer();
		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itColReservationSegmentDTO.next();

			changes.append(" Flight No : " + reservationSegmentDTO.getFlightNo());
			changes.append(" Segment Code : " + reservationSegmentDTO.getSegmentCode());
			changes.append(" Cabin Class Code : " + mapPnrSegIdAndCabinClass.get(reservationSegmentDTO.getPnrSegId()));
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				changes.append(" Logical Cabin Class Code : "
						+ mapPnrSegIdAndLogicalCabinClass.get(reservationSegmentDTO.getPnrSegId()));
			}
			changes.append(" Departure Date : " + reservationSegmentDTO.getDepartureStringDate("E, dd MMM yyyy HH:mm:ss"));
		}

		// Compose the payment information
		String audit = ReservationCoreUtils.getPassengerPaymentInfo(pnrPaxIdAndPayments, reservation);
		String overriddenZuluReleaseTimeAudit = getOverriddenZuluReleaseTimeAudit(zuluReleaseTimeStamp);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewSegment.SEGMENT_INFORMATION,
				changes.toString() + audit + overriddenZuluReleaseTimeAudit);

		// Setting the force confirm information
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewSegment.FORCE_CONFIRM,
					ReservationTemplateUtils.AUDIT_FORCE_CONFIRM_TEXT);
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewSegment.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewSegment.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		if (seatCodes != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.SEATS, seatCodes);
		}

		if (strMeals != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.MEALS, strMeals);
		}

		if (aptDetails != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.AIRPORTTRANSFERS, aptDetails);
		}

		if (strBaggages != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.BAGGAGES, strBaggages);
		}

		if (flexiInfo != null && !"".equals(flexiInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Flexibilities.FLEXIBILITIES, flexiInfo);
		}

		if (fareDiscountInfo != null && !"".equals(fareDiscountInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.FareDiscount.FARE_DISCOUNT_INFO, fareDiscountInfo);
		}

		if (!bookingType.isEmpty() && bookingType.equals("OVERBOOK")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.BOOKING_TYPE, bookingType);
		}

		if (autoCheckinInfo != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutomaticCheckins.AUTOMATICCHECKINS, autoCheckinInfo);
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}

	/**
	 * Returns the overridden zulu release time audit
	 * 
	 * @param zuluReleaseTimeStamp
	 * @return
	 */
	private String getOverriddenZuluReleaseTimeAudit(Date zuluReleaseTimeStamp) {
		String auditDesc = "";

		if (zuluReleaseTimeStamp != null) {
			auditDesc = " Overridden Release Time " + BeanUtils.parseDateFormat(zuluReleaseTimeStamp, "E, dd MMM yyyy HH:mm:ss")
					+ ReservationTemplateUtils.ZULU_TIME;
		}

		return auditDesc;
	}

	/**
	 * Process passenger seats
	 * 
	 * @param blockIds
	 * @param reservation
	 * @param exchangedSegInvIds
	 *            TODO
	 * @throws ModuleException
	 */
	private void processPassengerSeats(Collection<TempSegBcAlloc> blockIds, Reservation reservation,
			Map<Integer, String> flightSeatIdsAndPaxTypes, Collection<Integer> flightmealIds,
			Collection<Integer> flightbaggageIds, Collection<Integer> exchangedSegInvIds) throws ModuleException {
		// If the reservation is confirmed
		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			// If any newly confirm passengers exist
			if (confirmPnrPaxIds.size() > 0) {
				// Move the blocked seats to on hold state
				ReservationBO.moveBlockSeats(blockIds, new Integer(0), new Integer(0), flightSeatIdsAndPaxTypes, flightmealIds,
						flightbaggageIds);

				// Move newly added seats and and existing onhold seats to sold state
				ReservationBO.moveOnholdSeatsToConfirm(reservation.getPassengers(), confirmPnrPaxIds, exchangedSegInvIds);
			} else {
				// Move the seats to the sold state
				if (!hasOverbookBuckets(blockIds)) {
					ReservationBO.moveBlockedSeatsToSold(blockIds, flightSeatIdsAndPaxTypes, flightmealIds, flightbaggageIds);
				} else {
					Integer[] paxCountArr = getPaxCounts(reservation);
					ReservationBO.moveBlockSeats(blockIds, paxCountArr[0], paxCountArr[1], flightSeatIdsAndPaxTypes,
							flightmealIds, flightbaggageIds);
				}

			}
		}
		// If the reservation is on hold
		else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			// Move the seats to the on hold state
			ReservationBO.moveBlockSeats(blockIds, new Integer(0), new Integer(0), flightSeatIdsAndPaxTypes, flightmealIds,
					flightbaggageIds);
		}
	}

	private Integer[] getPaxCounts(Reservation reservation) {
		Integer[] arr = new Integer[] { 0, 0 };
		for (ReservationPax pax : reservation.getPassengers()) {
			if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
				arr[1] = arr[1] + 1;
			} else {
				arr[0] = arr[0] + 1;
			}
		}
		return arr;
	}

	private boolean hasOverbookBuckets(Collection<TempSegBcAlloc> blockIds) {
		if (blockIds != null) {
			for (TempSegBcAlloc blk : blockIds) {
				if (blk.getBookingClassType().equals(BookingClass.BookingClassType.OVERBOOK)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Check and process force mode
	 * 
	 * @param reservation
	 * @param triggerPnrForceConfirm
	 */
	private void checkForceMode(Reservation reservation) {
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)) {
			// Capture the ownership
			this.captureOwnerShip(reservation);
		}
	}

	/**
	 * Capture ownership
	 * 
	 * @param reservation
	 */
	private void captureOwnerShip(Reservation reservation) {
		// This is force confirmation therefore
		// If IBE user(s) exist should change the channel and the agent code
		if (AppSysParamsUtil.isOwnerAgentTransferEnabled()) {
			reservation.getAdminInfo().setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
			reservation.getAdminInfo().setOwnerAgentCode(credentialsDTO.getAgentCode());
		}
	}

	/**
	 * Adds segments
	 * 
	 * @param reservation
	 * @param pnrSegments
	 */
	private void addSegments(Reservation reservation, Collection<ReservationSegment> pnrSegments) {
		Iterator<ReservationSegment> itPnrSegments = pnrSegments.iterator();
		ReservationSegment reservationSegment;
		while (itPnrSegments.hasNext()) {
			reservationSegment = itPnrSegments.next();
			reservation.addSegment(reservationSegment);
			ReservationApiUtils.captureSegmentMarketingInfo(reservationSegment, this.credentialsDTO);
		}
	}

	/**
	 * Adds fares and charges
	 * 
	 * @param reservation
	 * @param pnrFaresForAdult
	 * @param pnrFaresForChild
	 * @param pnrFaresForInfant
	 * @param passengerRevenueMap
	 * @param pnrPaxIdAndPayments
	 * @param zuluReleaseTimeStamp
	 * @param ondWisePaxDueAdjMap
	 * @param reservationDiscountDTO
	 * @param infantPaymentSeparated
	 *            TODO
	 * @param flownAssitUnit
	 *            TODO
	 * @throws ModuleException
	 */
	private void addFaresAndCharges(Reservation reservation, Collection<ReservationPaxFare> pnrFaresForAdult,
			Collection<ReservationPaxFare> pnrFaresForChild, Collection<ReservationPaxFare> pnrFaresForInfant,
			Map<Integer, RevenueDTO> passengerRevenueMap, Map<Integer, IPayment> pnrPaxIdAndPayments, Date zuluReleaseTimeStamp,
			Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap, ReservationDiscountDTO reservationDiscountDTO,
			boolean infantPaymentSeparated, FlownAssitUnit flownAssitUnit) throws ModuleException {

		Map<Integer, Collection<ReservationPaxFare>> mapClonedFares = getRespectiveInfantFareCloneMap(reservation,
				pnrFaresForInfant, reservationDiscountDTO, ondWisePaxDueAdjMap);

		BigDecimal[] totalReservationAmounts = ReservationApiUtils.getChargeTypeAmounts();
		BigDecimal totalChargeAmount;
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			// Over ride the zulu release time stamp
			this.overrideZuluReleaseTimeStamp(reservationPax, zuluReleaseTimeStamp);
			Collection<DiscountChargeTO> discountChargeTOs = null;
			String discountCode = null;
			double discountPercentage = 0;
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				if (reservationDiscountDTO != null && reservationDiscountDTO.isDiscountExist()) {
					discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
					discountCode = reservationDiscountDTO.getDiscountCode();

					if (ChargeCodes.FARE_DISCOUNT.equals(discountCode)) {
						discountPercentage = reservationDiscountDTO.getDiscountPercentage();
					}

				}
			}

			// If INFANT
			if (ReservationApiUtils.isInfantType(reservationPax)) {
				Collection<ReservationPaxFare> colReservationPaxFare = mapClonedFares.get(reservationPax.getPnrPaxId());
				updateServiceTaxPenalty(colReservationPaxFare, reservation, pnrPaxIdAndPayments, reservationPax, flownAssitUnit);
				totalChargeAmount = CloneBO.linkFaresToThePassenger(reservationPax, colReservationPaxFare);
				if (infantPaymentSeparated) {
					if (isModifyChgOperation.booleanValue() == true || isSegmentExchanged) {
						BigDecimal newCharge = this.captureRevenueModification(passengerRevenueMap, totalChargeAmount,
								colReservationPaxFare, reservation, reservationPax, pnrPaxIdAndPayments, flownAssitUnit);
						// Set the total charge
						this.setTotalCharge(reservationPax.getPnrPaxId(), newCharge, pnrPaxIdAndPayments);
					} else {
						// Set the total charge
						this.setTotalCharge(reservationPax.getPnrPaxId(), totalChargeAmount, pnrPaxIdAndPayments);
					}
				}
			}
			// If ADULT
			else if (ReservationApiUtils.isSingleAfterSave(reservationPax)
					|| (infantPaymentSeparated && ReservationApiUtils.isParentAfterSave(reservationPax))) {
				Collection<ReservationPaxFare> colReservationPaxFare = CloneBO.cloneFares(reservationPax, pnrFaresForAdult,
						ondWisePaxDueAdjMap, discountChargeTOs, discountCode, discountPercentage);
				updateServiceTaxPenalty(colReservationPaxFare, reservation, pnrPaxIdAndPayments, reservationPax, flownAssitUnit);
				totalChargeAmount = CloneBO.linkFaresToThePassenger(reservationPax, colReservationPaxFare);

				if (isModifyChgOperation.booleanValue() == true || isSegmentExchanged) {
					BigDecimal newCharge = this.captureRevenueModification(passengerRevenueMap, totalChargeAmount,
							colReservationPaxFare, reservation, reservationPax, pnrPaxIdAndPayments, flownAssitUnit);
					// Set the total charge
					this.setTotalCharge(reservationPax.getPnrPaxId(), newCharge, pnrPaxIdAndPayments);
				} else {
					// Set the total charge
					this.setTotalCharge(reservationPax.getPnrPaxId(), totalChargeAmount, pnrPaxIdAndPayments);
				}
			}
			// If CHILD
			else if (ReservationApiUtils.isChildType(reservationPax)) {
				Collection<ReservationPaxFare> colReservationPaxFare = CloneBO.cloneFares(reservationPax, pnrFaresForChild,
						ondWisePaxDueAdjMap, discountChargeTOs, discountCode, discountPercentage);
				updateServiceTaxPenalty(colReservationPaxFare, reservation, pnrPaxIdAndPayments, reservationPax, flownAssitUnit);
				totalChargeAmount = CloneBO.linkFaresToThePassenger(reservationPax, colReservationPaxFare);

				if (isModifyChgOperation.booleanValue() == true || isSegmentExchanged) {
					BigDecimal newCharge = this.captureRevenueModification(passengerRevenueMap, totalChargeAmount,
							colReservationPaxFare, reservation, reservationPax, pnrPaxIdAndPayments, flownAssitUnit);
					// Set the tPadukkaotal charge
					this.setTotalCharge(reservationPax.getPnrPaxId(), newCharge, pnrPaxIdAndPayments);
				} else {
					// Set the total charge
					this.setTotalCharge(reservationPax.getPnrPaxId(), totalChargeAmount, pnrPaxIdAndPayments);
				}
			}
			// IF PARENT
			else if (!infantPaymentSeparated && ReservationApiUtils.isParentAfterSave(reservationPax)) {
				Collection<ReservationPaxFare> colAdultReservationPaxFare = CloneBO.cloneFares(reservationPax, pnrFaresForAdult,
						ondWisePaxDueAdjMap, discountChargeTOs, discountCode, discountPercentage);
				updateServiceTaxPenalty(colAdultReservationPaxFare, reservation, pnrPaxIdAndPayments, reservationPax,
						flownAssitUnit);
				totalChargeAmount = CloneBO.linkFaresToThePassenger(reservationPax, colAdultReservationPaxFare);
				Collection<ReservationPaxFare> colInfantReservationPaxFare = mapClonedFares
						.get(reservationPax.getAccompaniedPaxId());
				BigDecimal totalInfantAmount = CloneBO.getTotalCharges(colInfantReservationPaxFare);

				Collection<ReservationPaxFare> totalReservationPaxFare = new ArrayList<ReservationPaxFare>();
				totalReservationPaxFare.addAll(colAdultReservationPaxFare);
				totalReservationPaxFare.addAll(colInfantReservationPaxFare);

				if (isModifyChgOperation.booleanValue() == true || isSegmentExchanged) {
					BigDecimal newCharge = this.captureRevenueModification(passengerRevenueMap,
							AccelAeroCalculator.add(totalChargeAmount, totalInfantAmount), totalReservationPaxFare, reservation,
							reservationPax, pnrPaxIdAndPayments, flownAssitUnit);
					// Set the total charge
					this.setTotalCharge(reservationPax.getPnrPaxId(), newCharge, pnrPaxIdAndPayments);
				} else {
					// Set the total charge
					this.setTotalCharge(reservationPax.getPnrPaxId(),
							AccelAeroCalculator.add(totalChargeAmount, totalInfantAmount), pnrPaxIdAndPayments);
				}
			}

			// Updating the totals
			ReservationCoreUtils.updateTotals(totalReservationAmounts, reservationPax.getTotalChargeAmounts(), true);
		}

		// Setting the total charge amount
		reservation.setTotalChargeAmounts(totalReservationAmounts);
	}

	/**
	 * Returns the infant with their respective cloned reservation passenger fare objects
	 * 
	 * @param reservation
	 * @param pnrFaresForInfant
	 * @param reservationDiscountDTO
	 * @param ondWisePaxDueAdjMap
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map<Integer, Collection<ReservationPaxFare>> getRespectiveInfantFareCloneMap(Reservation reservation,
			Collection pnrFaresForInfant, ReservationDiscountDTO reservationDiscountDTO,
			Map<Integer, Map<Integer, Double>> ondWisePaxDueAdjMap) throws ModuleException {
		Map<Integer, Collection<ReservationPaxFare>> mapClonedFares = new HashMap<Integer, Collection<ReservationPaxFare>>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			Collection<DiscountChargeTO> discountChargeTOs = null;
			String discountCode = null;
			double discountPercentage = 0;

			// If INFANT
			if (ReservationApiUtils.isInfantType(reservationPax)) {

				if (reservationDiscountDTO != null && reservationDiscountDTO.isDiscountExist()) {
					discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
					discountCode = reservationDiscountDTO.getDiscountCode();

					if (ChargeCodes.FARE_DISCOUNT.equals(discountCode)) {
						discountPercentage = reservationDiscountDTO.getDiscountPercentage();
					}

				}

				Collection<ReservationPaxFare> colReservationPaxFare = CloneBO.cloneFares(reservationPax, pnrFaresForInfant,
						ondWisePaxDueAdjMap, discountChargeTOs, discountCode, discountPercentage);
				mapClonedFares.put(reservationPax.getPnrPaxId(), colReservationPaxFare);
			}
		}

		return mapClonedFares;
	}

	/**
	 * Over ride the zulu release time stamp
	 * 
	 * @param reservationPax
	 * @param zuluReleaseTimeStamp
	 */
	private void overrideZuluReleaseTimeStamp(ReservationPax reservationPax, Date zuluReleaseTimeStamp) {
		if (zuluReleaseTimeStamp != null) {
			reservationPax.setZuluReleaseTimeStamp(zuluReleaseTimeStamp);
		}
	}

	/**
	 * Set the total charge
	 * 
	 * @param pnrPaxId
	 * @param totalChargeAmount
	 * @param pnrPaxIdAndPayments
	 */
	private void setTotalCharge(Integer pnrPaxId, BigDecimal totalChargeAmount, Map<Integer, IPayment> pnrPaxIdAndPayments) {
		// Adding the new segment charge
		if (pnrPaxIdAndPayments.containsKey(pnrPaxId)) {
			PaymentAssembler paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(pnrPaxId);
			paymentAssembler.setTotalChargeAmount(totalChargeAmount);
		}
	}

	/**
	 * Capture the additional revenue accounting information
	 * 
	 * @param passengerRevenueMap
	 * @param totalChargeAmount
	 * @param reservation
	 *            TODO
	 * @param reservationPax
	 *            TODO
	 * @param pnrPaxIdAndPayments
	 *            TODO
	 * @param flownAssitUnit
	 *            TODO
	 */
	private BigDecimal captureRevenueModification(Map<Integer, RevenueDTO> passengerRevenueMap, BigDecimal totalChargeAmount,
			Collection<ReservationPaxFare> colReservationPaxFares, Reservation reservation, ReservationPax reservationPax,
			Map<Integer, IPayment> pnrPaxIdAndPayments, FlownAssitUnit flownAssitUnit) throws ModuleException {
		RevenueDTO revenueDTO = passengerRevenueMap.get(reservationPax.getPnrPaxId());
		BigDecimal addedSrviceTaxes = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (revenueDTO == null) {
			revenueDTO = new RevenueDTO();
			revenueDTO.setPnrPaxId(reservationPax.getPnrPaxId());
			passengerRevenueMap.put(reservationPax.getPnrPaxId(), revenueDTO);
		}

		BigDecimal modificationPenalty = ReservationCoreUtils.getTolalPenaltyCharges(colReservationPaxFares);

		if (modificationPenalty.compareTo(BigDecimal.ZERO) > 0) {
			reservationPax.setTotalModificationPenalty(modificationPenalty);
			totalChargeAmount = AccelAeroCalculator.subtract(totalChargeAmount, modificationPenalty);
			revenueDTO.setModificationPenalty(modificationPenalty);
		}

		revenueDTO.setAddedTotal(totalChargeAmount);
		revenueDTO.setActionNC(ReservationTnxNominalCode.PAX_CANCEL.getCode());
		revenueDTO.setChargeNC(ReservationTnxNominalCode.ALTER_CHARGE.getCode());
		revenueDTO.getAddedCharges().addAll(getChargesMetaInfo(colReservationPaxFares));

		return AccelAeroCalculator.subtract((AccelAeroCalculator.add(revenueDTO.getAddedTotal(),
				revenueDTO.getCancelOrModifyTotal(), revenueDTO.getModificationPenalty(), addedSrviceTaxes)),
				revenueDTO.getCreditTotal());
	}

	/**
	 * Returns the charges meta information
	 * 
	 * @param colReservationPaxFare
	 * @return
	 */
	private Collection<ReservationPaxOndCharge> getChargesMetaInfo(Collection<ReservationPaxFare> colReservationPaxFare) {
		Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();

		for (ReservationPaxFare reservationPaxFare : colReservationPaxFare) {
			colReservationPaxOndCharge.addAll(reservationPaxFare.getCharges());
		}

		return colReservationPaxOndCharge;
	}

	/**
	 * Validate parameters
	 * 
	 * @param pnr
	 * @param pnrSegments
	 * @param pnrFaresForAdult
	 * @param pnrFaresForChild
	 * @param pnrFaresForInfant
	 * @param isModifyChgOperation
	 * @param passengerRevenueMap
	 * @param pnrPaxIdAndPayments
	 * @param pnrPaymentTypes
	 * @param blockIds
	 * @param version
	 * @param colOndFareDTO
	 * @param credentialsDTO
	 * @param isRequote
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Collection<ReservationSegment> pnrSegments,
			Collection<ReservationPaxFare> pnrFaresForAdult, Collection<ReservationPaxFare> pnrFaresForChild,
			Collection<ReservationPaxFare> pnrFaresForInfant, Boolean isModifyChgOperation, Map passengerRevenueMap,
			Map<Integer, IPayment> pnrPaxIdAndPayments, Integer pnrPaymentTypes, Collection<TempSegBcAlloc> blockIds,
			Long version, Collection<OndFareDTO> colOndFareDTO, CredentialsDTO credentialsDTO, Boolean isRequote)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrSegments == null || pnrFaresForAdult == null || pnrFaresForChild == null
				|| pnrFaresForInfant == null || passengerRevenueMap == null || isModifyChgOperation == null
				|| pnrPaxIdAndPayments == null || pnrPaymentTypes == null || blockIds == null || version == null
				|| colOndFareDTO == null || colOndFareDTO.size() == 0 || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	/**
	 * Returns the seat codes for audit
	 * 
	 * @param flightSeatIdsAndPaxTypes
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getSeatCodesForAudit(Map flightSeatIdsAndPaxTypes) {
		String seatCodes = null;

		if (flightSeatIdsAndPaxTypes != null && flightSeatIdsAndPaxTypes.keySet().size() > 0) {
			Collection<String> seatCodesValues = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
					.getSeatCodes(flightSeatIdsAndPaxTypes.keySet());

			if (seatCodesValues != null && seatCodesValues.size() > 0) {
				seatCodes = SMUtil.getSeatAuditString(seatCodesValues);
			}
		}

		return seatCodes;
	}

	@SuppressWarnings("rawtypes")
	private void linkInfantExternalCharges(Reservation reservation, Map pnrPaxIdAndPayments, boolean checkPaxCredit) {
		for (ReservationPax parent : reservation.getPassengers()) {
			PaymentAssembler parentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(parent.getPnrPaxId());
			if (!parent.getPaxType().equals(PaxTypeTO.INFANT)) {
				// Validation to check total amount is made from pax credit
				if (parentAssembler != null && checkPaxCredit && parentAssembler.getTotalPayAmount()
						.doubleValue() <= parentAssembler.getTotalPaxCredit().doubleValue()) {
					linkInfantExternalCharges(parent, pnrPaxIdAndPayments, parentAssembler);
				} else if (!checkPaxCredit && parentAssembler != null) {
					linkInfantExternalCharges(parent, pnrPaxIdAndPayments, parentAssembler);
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void linkInfantExternalCharges(ReservationPax parent, Map pnrPaxIdAndPayments, PaymentAssembler parentAssembler) {
		if (parent.getInfants() != null && parent.getInfants().size() > 0) {
			for (ReservationPax infant : parent.getInfants()) {
				PaymentAssembler infantAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(infant.getPnrPaxId());
				if (infantAssembler != null) {
					parentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(parentAssembler.getTotalChargeAmount(),
							infantAssembler.getTotalExternalChargesAmountWithoutConsume()));
					pnrPaxIdAndPayments.remove(infant.getPnrPaxId());
				}
			}
		}
	}

	/**
	 * Update Surface Segment reference in modify surface segment operation
	 * 
	 * @param reservationSegment
	 */
	private void updateSurfaceSegment(Collection<ReservationSegment> colReservationSegments,
			Collection<Integer> colOldGroundSegments, Reservation reservation) {
		if (reservation.getSegments() != null) {
			ReservationSegment resSegment = BeanUtils.getFirstElement(colReservationSegments);

			for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
				ReservationSegment reservationSegment = reservationSegment2;
				if (reservationSegment.getGroundSegment() != null
						&& colOldGroundSegments.contains(reservationSegment.getGroundSegment().getPnrSegId())) {
					// Check whether resSegment is a ground segment.
					if (resSegment != null && resSegment.getSubStationShortName() != null) {
						reservationSegment.setGroundSegment(resSegment);
						break;
					}
				}
			}
		}
	}

	/**
	 * Update Connected segment if is Surface Segment
	 * 
	 * @param reservation
	 * @param colNewRervationSegments
	 * @param connectingPnrSegID
	 */
	private void updateConnectedSegment(Reservation reservation, Collection<ReservationSegment> colNewRervationSegments,
			Integer connectingPnrSegID) {
		if (reservation.getSegments() != null) {
			ReservationSegment resSegment = BeanUtils.getFirstElement(colNewRervationSegments);
			if (resSegment != null) {
				for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
					ReservationSegment reservationSegment = reservationSegment2;
					if (reservationSegment.getPnrSegId() != null
							&& reservationSegment.getPnrSegId().compareTo(connectingPnrSegID) == 0) {
						reservationSegment.setGroundSegment(resSegment);
					}
				}
			}
		}
	}

	private void addConnectedSegmentReferance(Reservation reservation, Collection<ReservationSegment> colNewRervationSegments,
			Integer connectingPnrSegID) {
		if (connectingPnrSegID != null) {
			this.updateConnectedSegment(reservation, colNewRervationSegments, connectingPnrSegID);
		}
	}

	private void updateSurfaceSegmentReferance(Reservation reservation, Collection<ReservationSegment> colNewRervationSegments,
			Collection<Integer> oldGroundSegList) {
		if (oldGroundSegList != null && oldGroundSegList.size() > 0) {
			this.updateSurfaceSegment(colNewRervationSegments, oldGroundSegList, reservation);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateAncillarySegmentSequence(Collection<ReservationSegment> pnrSegments, Map pnrPaxIdAndPayments) {

		if (pnrSegments != null && pnrPaxIdAndPayments != null && pnrPaxIdAndPayments.size() > 0) {
			Iterator<Integer> itr = pnrPaxIdAndPayments.keySet().iterator();

			Collection<ExternalChgDTO> ccExternalChgs;

			Map<Integer, Integer> newFltSegIdSegSeqMap = new HashMap<Integer, Integer>();
			for (ReservationSegment pnrSeg : pnrSegments) {
				newFltSegIdSegSeqMap.put(pnrSeg.getFlightSegId(), pnrSeg.getSegmentSeq());
			}
			// int[] nextSegSeq = getNextSegAndOndSequences(pnrSegments);

			while (itr.hasNext()) {

				Integer key = itr.next();
				PaymentAssembler paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(key);

				ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);

				if (ccExternalChgs.size() > 0) {
					setSegSeqInExtCharge(ccExternalChgs, newFltSegIdSegSeqMap);
				}

				ccExternalChgs = paymentAssembler.getPerPaxExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);

				if (ccExternalChgs.size() > 0) {
					setSegSeqInExtCharge(ccExternalChgs, newFltSegIdSegSeqMap);
				}

			}

		}

	}

	private void setSegSeqInExtCharge(Collection<ExternalChgDTO> ccExternalChgs, Map<Integer, Integer> newFltSegIdSegSeqMap) {
		for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
			SSRExternalChargeDTO ssrxternalChargeDTO = (SSRExternalChargeDTO) externalChgDTO;

			if (ssrxternalChargeDTO.getSegmentSequece() == null && newFltSegIdSegSeqMap
					.containsKey(FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrxternalChargeDTO.getFlightRPH()))) {
				ssrxternalChargeDTO.setSegmentSequece(newFltSegIdSegSeqMap
						.get(FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrxternalChargeDTO.getFlightRPH())));
			}
		}
	}

	private void addAutoCancellationInfo(Collection<ReservationSegment> newSegments, AutoCancellationInfo autoCnxInfo,
			Collection<Integer> cancelingSegmentIds, Set<ReservationSegment> oldSegments, boolean isRequote,
			Collection<Integer> exchangedPnrSegIds) {
		if (autoCnxInfo != null) {
			ReservationSegment modifiedFrom = null;
			Collection<Integer> exchangedFlightSegIds = new ArrayList<Integer>();
			boolean assigned = false;
			if (!isRequote) {
				if (cancelingSegmentIds != null) {
					// To track modification flow
					int cancelingSeg = cancelingSegmentIds.iterator().next();
					for (ReservationSegment reservationSegment : oldSegments) {
						if (reservationSegment.getPnrSegId().intValue() == cancelingSeg) {
							modifiedFrom = reservationSegment;
							break;
						}
					}
				}
			} else if (exchangedPnrSegIds != null) {
				for (ReservationSegment reservationSegment : oldSegments) {
					if (exchangedPnrSegIds.contains(reservationSegment.getPnrSegId())
							&& reservationSegment.getAutoCancellationId() == null) {
						exchangedFlightSegIds.add(reservationSegment.getFlightSegId());
					}
				}
			}

			for (ReservationSegment reservationSegment : newSegments) {
				if (exchangedFlightSegIds.contains(reservationSegment.getFlightSegId())) {
					continue;
				}
				reservationSegment.setAutoCancellationId(autoCnxInfo.getAutoCancellationId());
				if ((!assigned) && modifiedFrom != null) {
					assigned = true;
					reservationSegment.setModifiedFrom(modifiedFrom.getPnrSegId());
				}
			}
		}
	}

	private void updateExchangeSegmentPaxFareSegment(FlownAssitUnit flownAssitUnit, Reservation reservation,
			Collection<Integer> exchangedPnrSegIds) throws ModuleException {
		if (reservation != null && flownAssitUnit != null && exchangedPnrSegIds != null && exchangedPnrSegIds.size() > 0) {
			if (flownAssitUnit.getModifyAsst() != null && flownAssitUnit.getModifyAsst().getExchangedPnrSegByFltSegIdMap() != null
					&& flownAssitUnit.getModifyAsst().getExchangedPnrSegPaxStatusByPaxIdMap() != null) {
				Map<Integer, Integer> exchangedPnrSegByFltSegIdMap = flownAssitUnit.getModifyAsst()
						.getExchangedPnrSegByFltSegIdMap();

				Map<Integer, Map<Integer, String>> exchangedPnrSegPaxStatusByPaxIdMap = flownAssitUnit.getModifyAsst()
						.getExchangedPnrSegPaxStatusByPaxIdMap();

				for (ReservationSegment resSeg : reservation.getSegments()) {
					if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {

						if (exchangedPnrSegByFltSegIdMap.containsKey(resSeg.getFlightSegId())) {
							Integer oldResSegId = exchangedPnrSegByFltSegIdMap.get(resSeg.getFlightSegId());

							if (exchangedPnrSegIds.contains(oldResSegId)) {
								Map<Integer, String> paxFareStatusByPnrPaxIdMap = exchangedPnrSegPaxStatusByPaxIdMap
										.get(oldResSegId);
								updateExchangeNewSegPaxStatus(resSeg, paxFareStatusByPnrPaxIdMap);
							}

						}

					}
				}

			}
		}

	}

	private void reprotectAlerts(FlownAssitUnit flownAssitUnit, Reservation reservation, Collection<Integer> exchangedPnrSegIds)
			throws ModuleException {
		if (reservation != null && flownAssitUnit != null && exchangedPnrSegIds != null && exchangedPnrSegIds.size() > 0) {
			AlertingBD alertingBD = ReservationModuleUtils.getAlertingBD();
			Map<Long, Collection<Alert>> mapPnrSegIdAlerts = alertingBD
					.getAlertsForPnrSegments(new ArrayList<Integer>(exchangedPnrSegIds));
			Map<Integer, Integer> exchangedPnrSegByFltSegIdMap = flownAssitUnit.getModifyAsst().getExchangedPnrSegByFltSegIdMap();
			Collection<Alert> colAlert = new ArrayList<Alert>();

			for (ReservationSegment resSeg : reservation.getSegments()) {
				if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& exchangedPnrSegByFltSegIdMap.containsKey(resSeg.getFlightSegId())) {
					Integer oldResSegId = exchangedPnrSegByFltSegIdMap.get(resSeg.getFlightSegId());
					if (mapPnrSegIdAlerts.containsKey(new Long(oldResSegId))) {
						Collection<Alert> oldSegAlerts = mapPnrSegIdAlerts.get(new Long(oldResSegId));
						for (Alert newAlert : oldSegAlerts) {
							newAlert.setPnrSegId(new Long(resSeg.getPnrSegId()));
							colAlert.add(newAlert);
						}
					}
				}
			}

			if (colAlert.size() > 0) {
				alertingBD.updateAlerts(colAlert);
			}
		}

	}

	private void updateExchangeNewSegPaxStatus(ReservationSegment resSegment, Map<Integer, String> paxFareStatusByPnrPaxIdMap) {
		if (resSegment != null) {
			Reservation reservation = resSegment.getReservation();
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				Integer pnrPaxId = reservationPax.getPnrPaxId();

				String paxStatus = paxFareStatusByPnrPaxIdMap.get(pnrPaxId);

				for (ReservationPaxFare resPaxFare : reservationPax.getPnrPaxFares()) {
					for (ReservationPaxFareSegment resPaxFareSegment : resPaxFare.getPaxFareSegments()) {

						if (resSegment.getFlightSegId() != null
								&& resSegment.getFlightSegId().equals(resPaxFareSegment.getSegment().getFlightSegId())) {
							resPaxFareSegment.setStatus(paxStatus);
						}
					}
				}

			}
		}

	}

	private void validateSegAvailability(Collection<ReservationSegment> resSeg, int originalSalesTerminal)
			throws ModuleException {
		log.debug("Inside validateSegAvailability");
		Collection<Integer> flightSegmentIds = new ArrayList<Integer>();
		Collection<Date> estDepartureZulu = new ArrayList<Date>();
		boolean firstSegmentIsDomestic = false;

		for (ReservationSegment reservationSegment : resSeg) {
			flightSegmentIds.add(reservationSegment.getFlightSegId());
		}

		Collection<FlightSegmentDTO> colFlightSegmentDetails = ReservationModuleUtils.getFlightBD()
				.getFlightSegments(flightSegmentIds);

		for (FlightSegmentDTO flightSegmentDTO : colFlightSegmentDetails) {
			if (flightSegmentDTO.getFlightStatus().equals(FlightStatusEnum.CANCELLED.getCode())) {
				throw new ModuleException("airreservations.arg.invalidSegCancel");
			}
		}
		for (FlightSegmentDTO flightSegementDTO : colFlightSegmentDetails) {
			if (CalendarUtil.getCurrentSystemTimeInZulu().before(flightSegementDTO.getDepartureDateTimeZulu())) {
				estDepartureZulu.add(flightSegementDTO.getDepartureDateTimeZulu());
				if (flightSegementDTO.isDomesticFlight()) {
					firstSegmentIsDomestic = true;
				}
			}
		}

		boolean isFlightInClosure = false;
		if (originalSalesTerminal == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES) {
			if (firstSegmentIsDomestic) {
				isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(
						CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.API_RESERVATION_CUTOFF_TIME_FOR_DOMESTIC),
						estDepartureZulu);
			} else {
				isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(CommonsServices.getGlobalConfig()
						.getBizParam(SystemParamKeys.API_RESERVATION_CUTOFF_TIME_FOR_INTERNATIONAL), estDepartureZulu);
			}
		}

		if (isFlightInClosure) {
			throw new ModuleException("airreservations.arg.flightInClosure");
		}

	}

	private Map<Integer, SegmentSSRAssembler> getSSRAssemblerMap(Reservation reservation) {

		Map<Integer, SegmentSSRAssembler> ssrAssemblerMap = new HashMap<Integer, SegmentSSRAssembler>();
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		Collection<ReservationPax> resPaxList = reservation.getPassengers();
		Collection<Integer> untouchedPnrSegIds = new ArrayList<Integer>();

		for (ReservationPax resPax : resPaxList) {
			pnrPaxIds.add(resPax.getPnrPaxId());
		}

		Map<Integer, Collection<PaxSSRDetailDTO>> paxSSRDetails = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
				.getPNRPaxSSRs(pnrPaxIds, null, false, false, true);

		if (!paxSSRDetails.isEmpty()) {

			Map<Integer, Integer> segSeqPNRSegIdMap = new HashMap<Integer, Integer>();

			for (ReservationSegment segment : reservation.getSegments()) {
				if (!isUntouchedSegment(segment, reservation.getSegments())) {
					segSeqPNRSegIdMap.put(segment.getPnrSegId(), segment.getSegmentSeq());
				} else {
					untouchedPnrSegIds.add(segment.getPnrSegId());
				}
			}

			for (ReservationPax resPax : resPaxList) {

				Collection<PaxSSRDetailDTO> ssrDetailDTOs = paxSSRDetails.get(resPax.getPnrPaxId());

				if (ssrDetailDTOs != null && !ssrDetailDTOs.isEmpty()) {

					for (PaxSSRDetailDTO ssrDetailsDTO : ssrDetailDTOs) {

						SegmentSSRAssembler ipaxSSR = ssrAssemblerMap.get(resPax.getPaxSequence());

						if (ipaxSSR == null) {
							ssrAssemblerMap.put(resPax.getPaxSequence(), new SegmentSSRAssembler());
							ipaxSSR = ssrAssemblerMap.get(resPax.getPaxSequence());
						}

						if (!untouchedPnrSegIds.contains(ssrDetailsDTO.getPNRSegId())) {
							ipaxSSR.addPaxSegmentSSR(segSeqPNRSegIdMap.get(ssrDetailsDTO.getPNRSegId()), ssrDetailsDTO.getSsrId(),
									ssrDetailsDTO.getSsrDesc(), null, new BigDecimal(ssrDetailsDTO.getChargeAmount()),
									ssrDetailsDTO.getSsrCode(), null, null, null, ReservationPaxSegmentSSR.APPLY_ON_SEGMENT, null,
									null, null, null);
						}

					}

				}
			}
		}

		return ssrAssemblerMap;

	}

	private Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>>
			getPaxSegmentSSRDTOMap(Map<Integer, SegmentSSRAssembler> ssrAssemblerMap, Reservation reservation) {

		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps = new HashMap<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>>();

		for (ReservationPax pax : reservation.getPassengers()) {

			if (!ReservationApiUtils.isInfantType(pax)) {
				if (ssrAssemblerMap.get(pax.getPaxSequence()) != null) {
					SegmentSSRAssembler segSSRAssembler = ssrAssemblerMap.get(pax.getPaxSequence());
					Map<Integer, Collection<PaxSegmentSSRDTO>> ssrDTOMap = segSSRAssembler.getNewSegmentSSRDTOMap();

					if (paxSegmentSSRDTOMaps.get(pax.getPaxSequence()) == null) {
						paxSegmentSSRDTOMaps.put(pax.getPaxSequence(), ssrDTOMap);
					}
				}
			}
		}

		return paxSegmentSSRDTOMaps;
	}

	private boolean isUntouchedSegment(ReservationSegment currentResSeg, Set<ReservationSegment> allResSegs) {
		boolean isUntoucedSegment = false;
		if (currentResSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
			int fltSegId = currentResSeg.getFlightSegId();
			for (ReservationSegment resSeg : allResSegs) {
				if (resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
						&& resSeg.getSubStatus() != null
						&& resSeg.getSubStatus().equals(ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED)
						&& resSeg.getFlightSegId().intValue() == fltSegId) {
					isUntoucedSegment = true;
					break;
				}
			}
		}
		return isUntoucedSegment;
	}

	private void updateServiceTaxPenalty(Collection<ReservationPaxFare> colReservationPaxFare, Reservation reservation,
			Map<Integer, IPayment> pnrPaxIdAndPayments, ReservationPax reservationPax, FlownAssitUnit flownAssitUnit)
			throws ModuleException {
		if (credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_AGENT)
				|| credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_LCC)) {
			for (ReservationPaxFare paxFare : colReservationPaxFare) {
				BigDecimal charges = AccelAeroCalculator.getDefaultBigDecimalZero();
				charges = AccelAeroCalculator.add(charges, paxFare.getTotalPenaltyCharge());
				if (flownAssitUnit.getPenaltyTargetOnd() != null
						&& flownAssitUnit.getPenaltyTargetOnd().getFareId() == paxFare.getFareId()
						&& charges.compareTo(BigDecimal.ZERO) > 0) {
					
					Collection<String> nonTicketingRevenueChargeGroups = AppSysParamsUtil.getChargeGroupsToQuoteGSTOnNonTicketingRevenue();
					
					if((nonTicketingRevenueChargeGroups != null
							&& nonTicketingRevenueChargeGroups.contains(ReservationInternalConstants.ChargeGroup.PEN))){
						boolean trimPenalty = false;
						ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForNonTktDTO();
						Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges = new HashMap<>();
						PaxCnxModPenChargeForServiceTaxDTO paxCnxModPenChargeForServiceTaxDTO = new PaxCnxModPenChargeForServiceTaxDTO();
						ServiceTaxQuoteForNonTicketingRevenueRS nonTicketingServiceTaxRS = null;

						LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
						chgDTO.setCode(ReservationInternalConstants.ChargeGroup.PEN);
						chgDTO.setAmount(charges);

						List<LCCClientExternalChgDTO> chgs = new ArrayList<>();
						chgs.add(chgDTO);

						paxCnxModPenChargeForServiceTaxDTO.setTotalAmountIncludingServiceTax(trimPenalty);
						paxCnxModPenChargeForServiceTaxDTO.setCharges(chgs);
						paxWiseCharges.put(1, paxCnxModPenChargeForServiceTaxDTO);

						UserPrincipal userPrincipal = new UserPrincipal();
						userPrincipal = (UserPrincipal) userPrincipal.createIdentity(null, credentialsDTO.getSalesChannelCode(),
								credentialsDTO.getAgentCode(), credentialsDTO.getAgentStation(), null, credentialsDTO.getUserId(),
								credentialsDTO.getColUserDST(), credentialsDTO.getPassword(), null, null, null, null,
								credentialsDTO.getAgentCurrencyCode(), null, null, null, null);

						serviceTaxQuoteCriteriaDTO.setLccTransactionIdentifier(null);
						serviceTaxQuoteCriteriaDTO.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
						serviceTaxQuoteCriteriaDTO.setPaxState(reservation.getContactInfo().getState());
						serviceTaxQuoteCriteriaDTO.setPaxWiseCharges(paxWiseCharges);
						serviceTaxQuoteCriteriaDTO.setPrefSystem(ProxyConstants.SYSTEM.AA);

						nonTicketingServiceTaxRS = ReservationModuleUtils.getAirproxyReservationBD()
								.quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO, credentialsDTO.getTrackInfoDTO(),
										userPrincipal);

						if (nonTicketingServiceTaxRS != null && nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes() != null
								&& nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1) != null
								&& AccelAeroCalculator.isGreaterThan(
										nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
												.getEffectiveCnxModPenChargeAmount(),
										AccelAeroCalculator.getDefaultBigDecimalZero())) {
							// updating pax fare penalty amount
							if (trimPenalty) {
								paxFare.setTotalPenaltyCharge(nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
										.getEffectiveCnxModPenChargeAmount());
								for (ReservationPaxOndCharge paxOndCharge : paxFare.getCharges()) {
									if (paxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.PEN)) {
										paxOndCharge.setAmount(nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
												.getEffectiveCnxModPenChargeAmount());
									}
								}
							}
							List<ExternalChgDTO> penaltyRelatedServiceTaxes = new ArrayList<ExternalChgDTO>();
							String flightRefNum = null;

							// assuming penalty is only charge for one ond
							for (Entry<Integer, FlightSegmentDTO> entry : flownAssitUnit.getPenaltyTargetOnd().getSegmentsMap()
									.entrySet()) {
								for (ReservationPaxFareSegment reservationPaxFareSegment : paxFare.getPaxFareSegments()) {
									if (reservationPaxFareSegment.getSegment().getFlightSegId().equals(entry.getKey())) {
										flightRefNum = FlightRefNumberUtil.composeFlightRPH(entry.getValue());
									}
								}
							}
							for (ServiceTaxDTO serviceTaxDTO : nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
									.getCnxModPenServiceTaxes()) {

								ServiceTaxExtChgDTO chg = new ServiceTaxExtChgDTO();

								chg = (ServiceTaxExtChgDTO) getServiceTax(serviceTaxDTO.getChargeCode()).clone();

								chg.setAmount(serviceTaxDTO.getAmount());
								chg.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
								chg.setChargeCode(serviceTaxDTO.getChargeCode());
								chg.setFlightRefNumber(flightRefNum);
								chg.setChgGrpCode(serviceTaxDTO.getChargeGroupCode());
								chg.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
								chg.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
								chg.setTaxDepositCountryCode(nonTicketingServiceTaxRS.getServiceTaxDepositeCountryCode());
								chg.setTaxDepositStateCode(nonTicketingServiceTaxRS.getServiceTaxDepositeStateCode());
								penaltyRelatedServiceTaxes.add(chg);
							}

							if (!penaltyRelatedServiceTaxes.isEmpty()
									&& pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
								pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId()).addExternalCharges(penaltyRelatedServiceTaxes);
							}
						}
						
					}else{
						try {
							ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketing = null;
							ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ = ReservationApiUtils
									.createPaxWiseTicketingServiceTaxQuoteRQForPenalty(charges, flownAssitUnit, reservationPax, false);
							serviceTaxForTicketing = ReservationModuleUtils.getChargeBD()
									.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);

							if (serviceTaxForTicketing != null && serviceTaxForTicketing.getPaxWiseServiceTaxes() != null
									&& !serviceTaxForTicketing.getPaxWiseServiceTaxes().isEmpty()) {

								List<ExternalChgDTO> penaltyRelatedServiceTaxes = new ArrayList<ExternalChgDTO>();
								String flightRefNum = null;

								// assuming penalty is only charge for one ond
								for (Entry<Integer, FlightSegmentDTO> entry : flownAssitUnit.getPenaltyTargetOnd().getSegmentsMap()
										.entrySet()) {
									for (ReservationPaxFareSegment reservationPaxFareSegment : paxFare.getPaxFareSegments()) {
										if (reservationPaxFareSegment.getSegment().getFlightSegId().equals(entry.getKey())) {
											flightRefNum = FlightRefNumberUtil.composeFlightRPH(entry.getValue());
										}
									}
								}
								for (ServiceTaxDTO serviceTaxDTO : serviceTaxForTicketing.getPaxWiseServiceTaxes().get(reservationPax.getPaxSequence())) {

									ServiceTaxExtChgDTO chg = new ServiceTaxExtChgDTO();

									chg = (ServiceTaxExtChgDTO) getServiceTax(serviceTaxDTO.getChargeCode()).clone();

									chg.setAmount(serviceTaxDTO.getAmount());
									chg.setExternalChargesEnum(EXTERNAL_CHARGES.SERVICE_TAX);
									chg.setChargeCode(serviceTaxDTO.getChargeCode());
									chg.setFlightRefNumber(flightRefNum);
									chg.setChgGrpCode(serviceTaxDTO.getChargeGroupCode());
									chg.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
									chg.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
									chg.setTaxDepositCountryCode(serviceTaxForTicketing.getServiceTaxDepositeCountryCode());
									chg.setTaxDepositStateCode(serviceTaxForTicketing.getServiceTaxDepositeStateCode());
									chg.setTicketingRevenue(true);
									penaltyRelatedServiceTaxes.add(chg);
								}

								if (!penaltyRelatedServiceTaxes.isEmpty()
										&& pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
									pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId()).addExternalCharges(penaltyRelatedServiceTaxes);
								}
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
			}
		}
	}

	private ServiceTaxExtChgDTO getServiceTax(String chargeCode) throws ModuleException {

		ServiceTaxExtChgDTO externalChg = (ServiceTaxExtChgDTO) ((ServiceTaxExtCharges) getExternalChargesMap()
				.get(EXTERNAL_CHARGES.SERVICE_TAX)).getRespectiveServiceChgDTO(chargeCode);
		if (externalChg != null) {
			externalChg = (ServiceTaxExtChgDTO) externalChg.clone();
		}
		return externalChg;
	}

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap() throws ModuleException {

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChargeMap = new HashMap<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO>();
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SERVICE_TAX);

		extChargeMap = ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MODIFY_ONLY);

		return extChargeMap;

	}
	
	private static boolean sameDateFlightModification(ReservationSegment reservationSegment,
			Set<ReservationSegment> setResSegmentTOs) throws ModuleException {
		String uniqueSegmentKeyWithOutStatus = getUniqueSegmentKey(reservationSegment);
		for (ReservationSegment resSegmentTO : setResSegmentTOs) {
			String uniqueSegmentKey = getUniqueSegmentKey(resSegmentTO);
			if (uniqueSegmentKeyWithOutStatus.equals(uniqueSegmentKey)
					&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSegmentTO.getStatus())) {
				return true;
			}
		}
		return false;
	}
	
	private static String getUniqueSegmentKey(ReservationSegment reservationSegment) throws ModuleException {
		FlightSegement flightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(reservationSegment.getFlightSegId());
		Flight flight = ReservationModuleUtils.getFlightBD().getFlight(flightSegment.getFlightId());
		String cabinClassCode = reservationSegment.getCabinClassCode();

		return ReservationApiUtils.getUniqueSegmentKey(flight.getFlightNumber(), flightSegment.getSegmentCode(),
				flightSegment.getEstTimeDepatureLocal(), cabinClassCode, null);
	}
	
	private Integer getOldSegmentBundleFareId(Set<ReservationSegment> reservationSegments, Integer flightSegId) {
		Integer bundleFareId = -1;
		for (ReservationSegment resSegment : reservationSegments) {
			if (flightSegId.equals(resSegment.getFlightSegId())
					&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSegment.getStatus())) {
				bundleFareId = resSegment.getBundledFarePeriodId();
				break;
			}
		}
		return bundleFareId;
	}
	
	private String getPreviousReservationStatus() {
		return this.getParameter(CommandParamNames.RESERVATION_STATUS_BEFORE_UPDATE, String.class);
	}
	
	private boolean shouldRemoveFlexi(Reservation reservation, ReservationSegment reservationSegment) throws ModuleException {
		Integer oldSegmentBundleFareId = getOldSegmentBundleFareId(reservation.getSegments(), reservationSegment.getFlightSegId());
		Integer newSegmentBundleFareId = reservationSegment.getBundledFarePeriodId();
		return AppSysParamsUtil.shouldRemoveReprotectedBundledAnciForOHD()
				&& ReservationStatus.ON_HOLD.equals(getPreviousReservationStatus())
				&& sameDateFlightModification(reservationSegment, reservation.getSegments())
				&& ReservationApiUtils.shouldReprotectedAncillariesRemoved(oldSegmentBundleFareId, newSegmentBundleFareId);
	}
}
