/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.commands;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.PnlAdlFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessage;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessageResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdditionalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlAbstractDataFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory.PnlAdlDataFactoryProducer;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.messagebuilderfactory.PnlAdlMessageBuilderFactory;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses.MessageResponseAdditionals;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for creating a modification object for the call center reservation
 * 
 * @author Uditha Dissanayake
 * @since 1.0
 * @isa.module.command name="pnlAdlMessageBuilderCommand"
 */
public class PnlAdlMessageBuilderCommand extends DefaultBaseCommand {

	/** Holds the logger instance ye */
	private static Log log = LogFactory
			.getLog(PnlAdlMessageBuilderCommand.class);

	private PnlAdlMessageBuilderFactory pnlAdlMessageBuilderFactory;
	private PnlAdlMessage pnlAdlMessage;
	private PassengerCollection passengerCollection;
	private PnlAdlMessageResponse pnlAdlMessageResponse;
	private BaseDataContext baseDataContext;
	private String messageType;
	private DefaultServiceResponse response;

	/**
	 * Execute method of the pnlAdlAdditionalInfoExtractor command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		createDataFactory();
		resolveCommandParameters();
		messageType = getMessageType();
		pnlAdlMessage = pnlAdlMessageBuilderFactory
				.getPnlAdlMessegeBuilder(messageType);
		pnlAdlMessageResponse = pnlAdlMessage.buildMessage(baseDataContext);
		createResponseObject(response);
		return response;
	}

	private void createDataFactory() {
		pnlAdlMessageBuilderFactory = new PnlAdlMessageBuilderFactory();
	}

	private void resolveCommandParameters() {
		baseDataContext = (BaseDataContext) this
				.getParameter(CommandParamNames.PNL_DATA_CONTEXT);
		response = (DefaultServiceResponse) this
				.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		passengerCollection = baseDataContext.getPassengerCollection();
	}

	private String getMessageType() {
		return baseDataContext.getMessageType();
	}
	
	private MessageResponseAdditionals createMessageResponseAdditionals(){
		MessageResponseAdditionals additionals = new MessageResponseAdditionals();
		additionals.setFareClassWisePaxCount((HashMap)pnlAdlMessageResponse.getFareClassWisePaxCount());
		additionals.setPnrCollection(pnlAdlMessageResponse.getPnrCollection());
		additionals.setPnrPaxIdvsSegmentIds(pnlAdlMessageResponse.getPnrPaxIdvsSegmentIds());
		additionals.setPnrPaxVsGroupCodes(pnlAdlMessageResponse.getPnrPaxVsGroupCodes());
		additionals.setLastGroupCode(pnlAdlMessageResponse.getLastGroupCode());
		additionals.setPassengerInformations(passengerCollection.getPassengerInformations());
		return additionals;
	}

	private PnlAdditionalDataContext createAdditionalDataContext() {
		PnlAdditionalDataContext additionalDataContext = new PnlAdditionalDataContext();
		additionalDataContext.setDepartureAirportCode(baseDataContext
				.getDepartureAirportCode());
		additionalDataContext.setPassengerCollection(passengerCollection);
		return additionalDataContext;
	}

	private void createResponseObject(DefaultServiceResponse response) {
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE,
				response);
		response.addResponceParam(CommandParamNames.PNL_DATA_CONTEXT,
				baseDataContext);
		response.addResponceParam(CommandParamNames.PNL_MESSAGES_PARTS,
				pnlAdlMessageResponse.getMessagePartMap());
		response.addResponceParam(CommandParamNames.PNL_MESSAGE_RESPONSE_ADDITIONALS, createMessageResponseAdditionals());
	}

}
