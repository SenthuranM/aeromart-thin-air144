/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.PaymentGatewayBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.util.PaymentUtil;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for handles reverse payments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="reversePayment"
 */
public class ReversePayment extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReversePayment.class);

	/** Holds travel agent finance delegate */
	private TravelAgentFinanceBD travelAgentFinanceBD;

	// private TravelAgentBD travelAgentBD;
	
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * Construct the Reverse Payment
	 */
	public ReversePayment() {
		travelAgentFinanceBD = ReservationModuleUtils.getTravelAgentFinanceBD();
		// travelAgentBD = ReservationModuleUtils.getTravelAgentBD();
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/**
	 * Execute method of the ReversePayment command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection colPaymentInfo = (Collection) this.getParameter(CommandParamNames.PAYMENT_INFO);
		Collection colTnxIds = (Collection) this.getParameter(CommandParamNames.TEMPORY_PAYMENT_IDS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Boolean setPreviousCreditCardPayment = (Boolean) this.getParameter(CommandParamNames.SET_PREVIOUS_CREDIT_CARD_PAYMENT);
		Boolean isCaptureRefund = (Boolean) this.getParameter(CommandParamNames.IS_CAPTURE_PAYMENT);
		ReservationContactInfo contactInfo = (ReservationContactInfo) this
				.getParameter(CommandParamNames.RESERVATION_CONTACT_INFO);

		Map pnrPaxIdAndPayments = (Map) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);

		// Checking params
		this.validateParams(pnr, colPaymentInfo, colTnxIds, credentialsDTO);

		// Check Refund request is valid
		this.checkRefundConstraints(pnrPaxIdAndPayments);

		Collection<CardPaymentInfo> colCardPaymentInfo = new ArrayList<CardPaymentInfo>();
		Collection<OfflinePaymentInfo> colOfflinePaymentInfo = new ArrayList<OfflinePaymentInfo>();
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfo;
		Collection<AgentTotalSaledDTO> agentTotalSaledDTOs = new ArrayList<AgentTotalSaledDTO>();

		if (isCaptureRefund) {
			this.checkBSPRefundConstrains(pnr, colPaymentInfo);
			while (itColPaymentInfo.hasNext()) {
				paymentInfo = (PaymentInfo) itColPaymentInfo.next();

				// Card Payment
				if (paymentInfo instanceof CardPaymentInfo) {
					cardPaymentInfo = (CardPaymentInfo) paymentInfo;

					// Collect the CardPaymentInfo objects for compress
					PaymentGatewayBO.cardPaymentInfoCompressor(cardPaymentInfo, colCardPaymentInfo);
				}
				// Passenger Credit
				else if (paymentInfo instanceof PaxCreditInfo) {
					PaxCreditInfo paxCreditInfo = (PaxCreditInfo) paymentInfo;
					ReservationModuleUtils.getCreditAccountBD()
							.broughtForwardCredit(paxCreditInfo.getPaxCredit(), credentialsDTO);
				}
				// Agent Credit
				else if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					// Calling travel agents to increase the necessary payment amounts
					AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
					agentTotalSaledDTO = travelAgentFinanceBD.recordRefund(agentCreditInfo.getAgentCode(),
							agentCreditInfo.getTotalAmount(), pnr, pnr, agentCreditInfo.getPayCurrencyDTO(),
							agentCreditInfo.getPaymentReferenceTO(), null);

					if (agentTotalSaledDTO != null) {
						agentTotalSaledDTOs.add(agentTotalSaledDTO);
					}

				}
				// Cash Payment
				else if (paymentInfo instanceof CashPaymentInfo) {
					// Do noting since this can be for many reasons
				}

				// For Loyalty Payments
				else if (paymentInfo instanceof LMSPaymentInfo) {
					// Nothing to be done here
				} else if (paymentInfo instanceof OfflinePaymentInfo) {
					OfflinePaymentInfo offlinePaymentInfo = (OfflinePaymentInfo) paymentInfo;
					PaymentGatewayBO.getUniqueOfflinePaymentObject(offlinePaymentInfo, colOfflinePaymentInfo);
				}
			}
		}

		if (!agentTotalSaledDTOs.isEmpty() && AppSysParamsUtil.isTransactInvoiceEnable()) {
			this.processInvoiceReceipts(agentTotalSaledDTOs);
		}

		// Process card payments
		Object[] responseMaps = this.processCardPayments(colCardPaymentInfo, colPaymentInfo, colTnxIds, credentialsDTO,
				setPreviousCreditCardPayment);
		// Process offline payments
		if (responseMaps[0] == null && responseMaps[1] == null) {
			responseMaps = this.processOfflinePayments(colOfflinePaymentInfo, colPaymentInfo, colTnxIds, credentialsDTO,
					setPreviousCreditCardPayment, pnr);
		}
		// processOfflinePayments(contactInfo, credentialsDTO, colOfflinePaymentInfo, pnr);
		// is this null safe ???
		Map<String, String> paymentAuthIdMap = (Map<String, String>) responseMaps[0];
		Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) responseMaps[1];

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.PAYMENT_AUTHID_MAP, paymentAuthIdMap);
		response.addResponceParam(CommandParamNames.PAYMENT_BROKER_REF_MAP, paymentBrokerRefMap);

		log.debug("Exit execute");
		return response;
	}

	private Object[] processOfflinePayments(Collection<OfflinePaymentInfo> colCardPaymentInfo,
			Collection<PaymentInfo> colPaymentInfo, Collection<Integer> colTnxIds, CredentialsDTO credentialsDTO,
			boolean setPreviousCreditCardPayment, String pnr) throws ModuleException {
		// If any credit card payment exist
		Map<String, String> responseAuthIdMap = null;
		Map<String, Integer> responsePaymentBrokerRefMap = null;
		if (colCardPaymentInfo.size() > 0) {
			responseAuthIdMap = new HashMap<String, String>();
			responsePaymentBrokerRefMap = new HashMap<String, Integer>();
			// Capture the card payment
			Iterator<OfflinePaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
			CardPaymentInfo cardPaymentInfo = null;
			OfflinePaymentInfo offlinePaymentInfo;
			String key;
			while (itColCardPaymentInfo.hasNext()) {
				offlinePaymentInfo = (OfflinePaymentInfo) itColCardPaymentInfo.next();
				CreditCardTransaction ccTransaction = ReservationModuleUtils.getPaymentBrokerBD()
						.loadOfflineConfirmedTransactionByReference(pnr);
				if (ccTransaction != null) {
					cardPaymentInfo = new CardPaymentInfo();
					cardPaymentInfo
							.setAppIndicator(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null ? credentialsDTO
									.getTrackInfoDTO().getAppIndicator() : null);
					cardPaymentInfo.setPayCurrencyDTO(offlinePaymentInfo.getPayCurrencyDTO());
					cardPaymentInfo.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

					cardPaymentInfo.setCcdId(offlinePaymentInfo.getActualPaymentMethod());
					cardPaymentInfo.setPnr(pnr);
					cardPaymentInfo.setTotalAmount(offlinePaymentInfo.getTotalAmount());
					cardPaymentInfo.setPayCurrencyDTO((PayCurrencyDTO) offlinePaymentInfo.getPayCurrencyDTO().clone());

					cardPaymentInfo.setIpgIdentificationParamsDTO(offlinePaymentInfo.getIpgIdentificationParamsDTO());
					cardPaymentInfo.setPayCarrier(offlinePaymentInfo.getPayCarrier());
					cardPaymentInfo.setPaymentTnxId(offlinePaymentInfo.getPaymentTnxId());
					cardPaymentInfo.setTemporyPaymentId((Integer)colTnxIds.toArray()[0]);
					cardPaymentInfo.setOfflinePayment(true);

					// Send Payment request and capture the response
					Object[] responses = PaymentGatewayBO.debitPayment(cardPaymentInfo, colTnxIds, credentialsDTO,
							setPreviousCreditCardPayment);
					int paymentBrokerRefNo = Integer.parseInt((String) responses[0]);
					String operationType = (String) responses[1];
					String authorizationId = (String) responses[2];
					key = ReservationApiUtils.getPaymentKey(cardPaymentInfo);
					responseAuthIdMap.put(key, authorizationId);
					responsePaymentBrokerRefMap.put(key, Integer.valueOf(paymentBrokerRefNo));

					// Update the references
					PaymentGatewayBO.updatePaymentBrokerReferences(cardPaymentInfo, colPaymentInfo, paymentBrokerRefNo,
							operationType, authorizationId);
				}
			}
		}
		return new Object[] { responseAuthIdMap, responsePaymentBrokerRefMap };
	}

	/**
	 * Method Will generate - invoices for all the OnAccount Refunds if INVOICE_NO_GENERATION_METHOD set to "T" .
	 * AARESAA-2363
	 * 
	 * @param txnIdCollection
	 * @throws ModuleException
	 */
	private void processInvoiceReceipts(Collection<AgentTotalSaledDTO> agenTotalSaledDTOs) throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("=====================Generating Transaction Refund Invoice========================");
		}

		SearchInvoicesReceiptsDTO invRDto = new SearchInvoicesReceiptsDTO();
		invRDto.setTxnDate(new Date());
		ReservationModuleUtils.getInvoicingBD().generateInvoicesReciptsForRefunds(agenTotalSaledDTOs, invRDto);
	}

	/**
	 * Process card payments
	 * 
	 * @param colCardPaymentInfo
	 * @param colPaymentInfo
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private Object[] processCardPayments(Collection<CardPaymentInfo> colCardPaymentInfo, Collection<PaymentInfo> colPaymentInfo,
			Collection<Integer> colTnxIds, CredentialsDTO credentialsDTO, boolean setPreviousCreditCardPayment)
			throws ModuleException {
		// If any credit card payment exist
		Map<String, String> responseAuthIdMap = null;
		Map<String, Integer> responsePaymentBrokerRefMap = null;
		if (colCardPaymentInfo.size() > 0) {
			responseAuthIdMap = new HashMap<String, String>();
			responsePaymentBrokerRefMap = new HashMap<String, Integer>();
			// Capture the card payment
			Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
			CardPaymentInfo cardPaymentInfo;
			String key;
			while (itColCardPaymentInfo.hasNext()) {
				cardPaymentInfo = (CardPaymentInfo) itColCardPaymentInfo.next();

				// Send Payment request and capture the response
				Object[] responses = PaymentGatewayBO.debitPayment(cardPaymentInfo, colTnxIds, credentialsDTO,
						setPreviousCreditCardPayment);
				int paymentBrokerRefNo = Integer.parseInt((String) responses[0]);
				String operationType = (String) responses[1];
				String authorizationId = (String) responses[2];
				key = ReservationApiUtils.getPaymentKey(cardPaymentInfo);
				responseAuthIdMap.put(key, authorizationId);
				responsePaymentBrokerRefMap.put(key, Integer.valueOf(paymentBrokerRefNo));

				// Update the references
				PaymentGatewayBO.updatePaymentBrokerReferences(cardPaymentInfo, colPaymentInfo, paymentBrokerRefNo,
						operationType, authorizationId);
			}
		}
		return new Object[] { responseAuthIdMap, responsePaymentBrokerRefMap };
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param colPaymentInfo
	 * @param reservationContactInfo
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(String pnr, Collection colPaymentInfo, Collection colTnxIds, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || colPaymentInfo == null || colTnxIds == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	/**
	 * Return the passenger payment amount
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	private BigDecimal getPassengerPaymentAmount(String pnrPaxId) {
		Collection<ReservationTnx> conReservationTnx = reservationTnxDao.getPNRPaxPayments(pnrPaxId);
		Iterator<ReservationTnx> itReservationTnx = conReservationTnx.iterator();
		ReservationTnx reservationTnx;
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

		while (itReservationTnx.hasNext()) {
			reservationTnx = (ReservationTnx) itReservationTnx.next();
			amount = AccelAeroCalculator.add(amount, reservationTnx.getAmount());
		}

		return amount;
	}

	private void checkRefundConstraints(Map<Integer, PaymentAssembler> pnrPaxIdAndPayments) throws ModuleException {

		if (pnrPaxIdAndPayments != null && pnrPaxIdAndPayments.size() > 0) {
			Iterator<Integer> itPaxIds = pnrPaxIdAndPayments.keySet().iterator();

			while (itPaxIds.hasNext()) {
				Integer pnrPaxId = (Integer) itPaxIds.next();

				PaymentAssembler paymentAssembler = ((PaymentAssembler) pnrPaxIdAndPayments.get(pnrPaxId));
				BigDecimal refundAmount = paymentAssembler.getTotalPayAmount();

				// Get the passenger payment amount
				BigDecimal pnrPaxPaymentAmount = this.getPassengerPaymentAmount(pnrPaxId.toString());

				if (pnrPaxPaymentAmount.doubleValue() < 0) {

					// Requesting refund for more than the paid amount.
					if (refundAmount.compareTo(pnrPaxPaymentAmount.negate()) > 0) {

						BigDecimal difference = refundAmount.subtract(pnrPaxPaymentAmount.negate()).abs();

						// We allow up to 0.03 difference
						if (difference.doubleValue() > 0.03) {
							// revac.invalid.amount
							throw new ModuleException("revac.payments.invalid");
						}
					}
				} else {
					throw new ModuleException("revac.payments.invalid");
				}
			}
		}

	}

	private void checkBSPRefundConstrains(String pnr, Collection colPaymentInfo) throws ModuleException {
		PaymentInfo paymentInfo;
		String ownerAgentCode = "";
		Iterator<PaymentInfo> paymentInfoInterator = colPaymentInfo.iterator();
		if (checkBSPRefundExists(colPaymentInfo)) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			if (reservation.getAdminInfo() != null) {
				ownerAgentCode = BeanUtils.nullHandler(reservation.getAdminInfo().getOwnerAgentCode());
			}

			while (paymentInfoInterator.hasNext()) {
				paymentInfo = (PaymentInfo) paymentInfoInterator.next();
				if (paymentInfo instanceof AgentCreditInfo) {
					AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
					PaymentReferenceTO paymentReferenceTO = agentCreditInfo.getPaymentReferenceTO();
					if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
							&& paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)
							&& !agentCreditInfo.getAgentCode().equals(ownerAgentCode)) {
						throw new ModuleException("airreservation.modify.refund.bsp.owner.agent");
					}
				}
			}
		}

	}

	private boolean checkBSPRefundExists(Collection colPaymentInfo) {
		PaymentInfo paymentInfo;
		Iterator<PaymentInfo> paymentInfoInterator = colPaymentInfo.iterator();
		while (paymentInfoInterator.hasNext()) {
			paymentInfo = (PaymentInfo) paymentInfoInterator.next();
			if (paymentInfo instanceof AgentCreditInfo) {
				AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
				PaymentReferenceTO paymentReferenceTO = agentCreditInfo.getPaymentReferenceTO();
				if (paymentReferenceTO != null && paymentReferenceTO.getPaymentRefType() != null
						&& paymentReferenceTO.getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
					return true;
				}
			}
		}
		return false;
	}

}
