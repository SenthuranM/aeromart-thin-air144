/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.intermediate.MessageTypeIdentifier;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.DestinationElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.TotalByDestinationElementBuilderTemplate;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.PNLADLMessageConstants;

/**
 * @author udithad
 *
 */
public class TotalByDestinationElementBuilder extends MessageTypeIdentifier {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private String departureAirportCode;
	private String fareClass;
	private Integer numberOfPassengers;

	private PassengerElementsContext pEContext;

	private BaseRuleExecutor<RulesDataContext> destinationElementRuleExecutor = new DestinationElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		RulesDataContext rulesContext = null;
		currentElement = "";
		initContextData(context);
		if(isCountExceeded()){
			if (validateUsage(pEContext.getDestinationFare(), hasPaxToAmmend())) {
				pEContext.getDestinationFare().setOnceUsed(true);
				destinationElementTemplate();
				executeConcatenationElementBuilder(pEContext);
				response = validateSubElement(currentElement);
				ammendMessageDataAccordingTo(response);
			}
			executeNext();
		}else{
			pEContext.setPartCountExceeded(true);
		}
	}
	
	private boolean isCountExceeded(){
		boolean isValied=true;
		String messageLine = pEContext.getMessageString().toString();
		if(messageLine != null && !messageLine.isEmpty()){
			String[] separated = messageLine.split("\n");
			if(separated != null && (separated.length > 60)){
				isValied = false;
			}
		}
		return isValied;
	}
	
	private boolean validateUsage(DestinationFare destinationFare,boolean hasPax){
		boolean isValied=false;
		
		if(destinationFare != null){
			if(!destinationFare.isOnceUsed()){
				isValied = true;
			}else if(destinationFare.isOnceUsed() && hasPax){
				isValied = true;
			}else if(!destinationFare.isOnceUsed() && !hasPax){
				isValied = true;
			}
		}
		
		return isValied;
	}

	private void initContextData(ElementContext context) {
		pEContext = (PassengerElementsContext) context;
		currentLine = pEContext.getCurrentMessageLine();
		messageLine = pEContext.getMessageString();
		departureAirportCode = pEContext.getDestinationFare()
				.getDestinationAirportCode();
		fareClass = pEContext.getDestinationFare().getFareClass();
		numberOfPassengers = pEContext.getDestinationFare()
				.getNumberOfPreviousPassengers();

	}

	private void destinationElementTemplate() {
		TotalByDestinationElementBuilderTemplate builderTemplate = getTotalByDestinationElementBuilderTemplate(pEContext
				.getMessageType());		
		currentElement = builderTemplate.buildElementTemplate(pEContext);
		/*StringBuilder elementTemplate = new StringBuilder();
		elementTemplate.append(hyphen());
		elementTemplate.append(departureAirportCode);
		elementTemplate.append(getNumberOfPassengers(this.numberOfPassengers));
		elementTemplate.append(fareClass);
		currentElement = elementTemplate.toString();*/
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			executeConcatenationElementBuilder(pEContext);
			incrementPassengerCount();
			ammendToBaseLine(currentElement, currentLine, messageLine);
			
		}
	}

	private void incrementPassengerCount() {
		pEContext.setPassengersPerPart(pEContext.getPassengersPerPart() + 1);
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = destinationElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		RulesDataContext rulesDataContext = new RulesDataContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(pEContext);
		}
	}

	private boolean hasPaxToAmmend() {
		boolean hasPax = false;
		if (pEContext.getDestinationFare() != null
				&& pEContext.getDestinationFare().getDeletePassengers() != null
				&& pEContext.getDestinationFare().getDeletePassengers().size() > 0) {
			hasPax = true;
		} else if (pEContext.getDestinationFare() != null
				&& pEContext.getDestinationFare().getAddPassengers() != null
				&& pEContext.getDestinationFare().getAddPassengers().size() > 0) {
			hasPax = true;
		} else if (pEContext.getDestinationFare() != null
				&& pEContext.getDestinationFare().getChangePassengers() != null
				&& pEContext.getDestinationFare().getChangePassengers().size() > 0) {
			hasPax = true;
		}
		return hasPax;
	}

}
