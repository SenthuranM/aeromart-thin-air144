package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.ResChangesDetectorConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.AddAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.ChangeAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.DelAddAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.DelAirportMessagePaxTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base.BaseObseverActionTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.ModificationTypeWiseSegsResponse;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.base.AirportMessageObserver;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PalCalMessageObserver extends AirportMessageObserver {

	private Map<Integer, Boolean> segWisePalSentSatusMap;

	public PalCalMessageObserver(String observerRef) {
		super(observerRef);
	}

	@Override
	public void notifyObserver(ResModifyDtectorDataContext dataContext) throws ModuleException {

		this.dataContext = dataContext;

		populateObserverValidRuleResponses();
		if (validForObserver()) {
			populateSegWisePalSentSatusMap(dataContext.getUpdatedReservation().getSegments());

			Map<Integer, Set<Integer>> paxWisePalCalValidSegExistRes = getValidPaxWiseSegMapForPalCal(dataContext
					.getExisitingReservation());
			Map<Integer, Set<Integer>> paxWisePalCalValidSegUpdatRes = getValidPaxWiseSegMapForPalCal(dataContext
					.getUpdatedReservation());

			populatePaxWiseAddDelChgSegMaps();
			optimseChgSegMap(); // if remove rm pax is in ADD or DEL remove it from CHG list
			removeAllInvalidSegsForPalCal(paxWisePalCalValidSegExistRes, paxWisePalCalValidSegUpdatRes);
			populateRemoveAllSSROnUpdate(paxWisePalCalValidSegExistRes, paxWisePalCalValidSegUpdatRes);

			executeActionTemplates();
		}
	}

	private void populateRemoveAllSSROnUpdate(Map<Integer, Set<Integer>> paxWisePalCalValidSegExistRes,
			Map<Integer, Set<Integer>> paxWisePalCalValidSegUpdatRes) {

		Set<String> exsitPaxSegCompundKeySet = new HashSet<>();
		Set<String> updatPaxSegCompundKeySet = new HashSet<>();

		for (Integer paxId : paxWisePalCalValidSegExistRes.keySet()) {
			for (Integer segId : paxWisePalCalValidSegExistRes.get(paxId)) {
				exsitPaxSegCompundKeySet.add(createCompundKey(paxId, segId));
			}
		}

		for (Integer paxId : paxWisePalCalValidSegUpdatRes.keySet()) {
			for (Integer segId : paxWisePalCalValidSegUpdatRes.get(paxId)) {
				updatPaxSegCompundKeySet.add(createCompundKey(paxId, segId));
			}
		}

		exsitPaxSegCompundKeySet.removeAll(updatPaxSegCompundKeySet);
		removeAllSSROnUpdate.addAll(exsitPaxSegCompundKeySet);
	}

	private void executeActionTemplates() throws ModuleException {

		ObserverActionDataContext oAdataContext = getObserverActionDataContext();

		if ((addPaxWiseSegMap != null && !addPaxWiseSegMap.isEmpty())
				&& (delPaxWiseSegMap != null && !delPaxWiseSegMap.isEmpty())) {

			actionTemplateList.add(new DelAddAirportMessagePaxTemplate());

		} else if ((addPaxWiseSegMap != null && !addPaxWiseSegMap.isEmpty())
				&& (delPaxWiseSegMap == null || delPaxWiseSegMap.isEmpty())) {

			actionTemplateList.add(new AddAirportMessagePaxTemplate());

		} else if ((addPaxWiseSegMap == null || addPaxWiseSegMap.isEmpty())
				&& (delPaxWiseSegMap != null && !delPaxWiseSegMap.isEmpty())) {

			actionTemplateList.add(new DelAirportMessagePaxTemplate());
		}

		if (chgPaxWiseSegMap != null && !chgPaxWiseSegMap.isEmpty()) {

			actionTemplateList.add(new ChangeAirportMessagePaxTemplate());
		}

		for (BaseObseverActionTemplate<ObserverActionDataContext> actionTrmpl : actionTemplateList) {
			actionTrmpl.executeTemplateAction(oAdataContext);
		}

	}

	private void removeAllInvalidSegsForPalCal(Map<Integer, Set<Integer>> paxWisePalCalValidSegExistRes,
			Map<Integer, Set<Integer>> paxWisePalCalValidSegUpdatRes) {

		if (addPaxWiseSegMap != null && !addPaxWiseSegMap.isEmpty()) {
			removeInValidSegs(addPaxWiseSegMap, paxWisePalCalValidSegUpdatRes);
		}

		if (delPaxWiseSegMap != null && !delPaxWiseSegMap.isEmpty()) {
			removeInValidSegs(delPaxWiseSegMap, paxWisePalCalValidSegExistRes);
		}

		if (chgPaxWiseSegMap != null && !chgPaxWiseSegMap.isEmpty()) {
			paxWisePalCalValidSegExistRes.putAll(paxWisePalCalValidSegUpdatRes);
			removeInValidSegs(chgPaxWiseSegMap, paxWisePalCalValidSegExistRes);
		}

	}

	private void removeInValidSegs(Map<Integer, Set<Integer>> paxWiseSegMap, Map<Integer, Set<Integer>> paxWisePalCalValidSegMap) {
	
		for (Integer paxId : new ArrayList<>(paxWiseSegMap.keySet())) {
			// if Pax has atleast one pal/cal valid segment
			if (paxWisePalCalValidSegMap.get(paxId) != null) {

				paxWiseSegMap.get(paxId).retainAll(paxWisePalCalValidSegMap.get(paxId));

				if (paxWiseSegMap.get(paxId).isEmpty()) {
					paxWiseSegMap.remove(paxId);
				}

			} else {

				paxWiseSegMap.remove(paxId);

			}
		}

	}

	// if Change values in ADD or DEL remove in change map
	private void optimseChgSegMap() {

		if (chgPaxWiseSegMap != null && !chgPaxWiseSegMap.isEmpty()) {

			for (Integer paxid : chgPaxWiseSegMap.keySet()) {

				if (addPaxWiseSegMap.get(paxid) != null) {

					chgPaxWiseSegMap.get(paxid).removeAll(addPaxWiseSegMap.get(paxid));
				}

				if (delPaxWiseSegMap.get(paxid) != null) {

					chgPaxWiseSegMap.get(paxid).removeAll(delPaxWiseSegMap.get(paxid));

				}
			}
		}
	}

	private void populatePaxWiseAddDelChgSegMaps() {

		for (RuleResponseDTO ruleResponse : dataContext.getRuleWiseResponseMap().values()) {

			String actionTemplateRef = dataContext.getObserverWiseRulesAndActionMap().get(observerRef)
					.get(ruleResponse.getRuleRef());
			Map<Integer, ModificationTypeWiseSegsResponse> affectedPaxWiseSegment = ruleResponse.getAffectedPaxWiseSegment();

			if (ResChangesDetectorConstants.ActionTemplateRef.ADD_ACTION.equals(actionTemplateRef)
					|| ResChangesDetectorConstants.ActionTemplateRef.DEL_ADD_ACTION.equals(actionTemplateRef)) {

				for (Integer paxId : affectedPaxWiseSegment.keySet()) {

					populateMap(paxId, affectedPaxWiseSegment.get(paxId).getAddSegIds(), addPaxWiseSegMap);

				}

			}
			if (ResChangesDetectorConstants.ActionTemplateRef.DEL_ACTION.equals(actionTemplateRef)
					|| ResChangesDetectorConstants.ActionTemplateRef.DEL_ADD_ACTION.equals(actionTemplateRef)) {

				for (Integer paxId : affectedPaxWiseSegment.keySet()) {

					populateMap(paxId, affectedPaxWiseSegment.get(paxId).getDelSegIds(), delPaxWiseSegMap);

				}

			}
			if (ResChangesDetectorConstants.ActionTemplateRef.CHG_ACTION.equals(actionTemplateRef)) {

				for (Integer paxId : affectedPaxWiseSegment.keySet()) {

					populateMap(paxId, affectedPaxWiseSegment.get(paxId).getChgSegIds(), chgPaxWiseSegMap);

				}
			}
		}
	}

	private static void populateMap(Integer paxId, List<Integer> segIdList, Map<Integer, Set<Integer>> paxWiseSegMap) {

		if (paxWiseSegMap.get(paxId) == null) {

			paxWiseSegMap.put(paxId, new HashSet<Integer>(segIdList));

		} else {

			paxWiseSegMap.get(paxId).addAll(segIdList);
		}

	}

	@Override
	protected boolean validForObserver() {

		if (BigDecimal.ZERO.compareTo(dataContext.getUpdatedReservation().getTotalAvailableBalance()) < 0) {
			return false;
		}

		if (observerValidRuleResponses.isEmpty()) {
			return false;
		}
		return true;
	}

	private void populateSegWisePalSentSatusMap(Set<ReservationSegment> segments) throws ModuleException {

		segWisePalSentSatusMap = new HashMap<>();
		for (ReservationSegment resSeg : segments) {

			if (resSeg.getSubStatus() != null
					&& resSeg.getSubStatus().equals(ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED)) {
				segWisePalSentSatusMap.put(resSeg.getPnrSegId(), false); // invalid fro cal notify

			} else {
				segWisePalSentSatusMap.put(resSeg.getPnrSegId(), isValidSegmentToTriggerCal(resSeg));

			}
		}
	}

	private static boolean isValidSegmentToTriggerCal(ReservationSegment resSeg) throws ModuleException {

		List<Integer> flightSegmentIds = new ArrayList<Integer>();
		flightSegmentIds.add(resSeg.getFlightSegId());

		Collection<FlightSegmentDTO> flightSegmentDTOs = ReservationModuleUtils.getFlightBD().getFlightSegments(flightSegmentIds);
		FlightSegmentDTO fsDTO = BeanUtils.getFirstElement(flightSegmentDTOs);

		return ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO.hasAnyPALCALHistory(fsDTO.getFlightNumber(),
				fsDTO.getDepartureDateTimeZulu(), fsDTO.getDepartureAirportCode(), false);

	}

	private Map<Integer, Set<Integer>> getValidPaxWiseSegMapForPalCal(Reservation reservation) {

		Map<Integer, Set<Integer>> validPaxWiseSegMapForPalCal = new HashMap<>();

		if (reservation == null) {
			return validPaxWiseSegMapForPalCal;
		}

		for (ReservationPax pax : reservation.getPassengers()) {
			if (pax.getPaxSSR() != null) {
				for (PaxSSRDTO ssr : pax.getPaxSSR()) {
					if (ssr.isInclideInPalCAl() && checkPALSengingStatus(pax, ssr.getPnrSegId())) {

						if (validPaxWiseSegMapForPalCal.get(pax.getPnrPaxId()) != null) {
							validPaxWiseSegMapForPalCal.get(pax.getPnrPaxId()).add(ssr.getPnrSegId());
						} else {
							Set<Integer> segIds = new HashSet<Integer>();
							segIds.add(ssr.getPnrSegId());
							validPaxWiseSegMapForPalCal.put(pax.getPnrPaxId(), segIds);
						}

					}
				}
			}
		}

		return validPaxWiseSegMapForPalCal;
	}

	private boolean checkPALSengingStatus(ReservationPax pax, Integer segId) {
		/*
		 * for (ReservationPaxFare fare : pax.getPnrPaxFares()) { for (ReservationPaxFareSegment fareSeg :
		 * fare.getPaxFareSegments()) { if (fareSeg.getPnrSegId() != null && segId.equals(fareSeg.getPnrSegId()) &&
		 * !isCNXSeg(fareSeg.getSegment())) { return
		 * PNLConstants.PaxPnlAdlStates.PNL_PNL_STAT.equals(fareSeg.getPnlStat()); } } }
		 */

		if (segWisePalSentSatusMap.get(segId) != null) {
			return segWisePalSentSatusMap.get(segId);
		}

		return false;
	}

}
