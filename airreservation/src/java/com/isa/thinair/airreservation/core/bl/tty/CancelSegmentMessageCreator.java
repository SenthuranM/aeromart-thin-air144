package com.isa.thinair.airreservation.core.bl.tty;


/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class CancelSegmentMessageCreator extends TypeBReservationMessageCreator {
	
	public CancelSegmentMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForCancelSegment();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}
}
