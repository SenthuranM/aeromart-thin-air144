/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import oracle.jdbc.proxy.annotation.GetDelegate;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.AdultEticketElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.BaggageElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.ChildElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.ClassCodeElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.CreditCardElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.DOCOElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.DOCSElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.DestinationBulkElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.EndElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.FlightNumberElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.GroupCodeElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.HeaderElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.InboundConnectionElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.InfantElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.MarketingFlightElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.MealElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.NameElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.NewLineElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.OnwardConnectionElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.PaxModifierElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.PnrElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.SSRElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.SeatConfigurationElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.SeatElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.SpaceElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.StandByPassengerElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.TotalByDestinationElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.WaitListPassengerElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.BaggageElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.NameElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.StandByElementRuleExecutor;

/**
 * @author udithad
 *
 */
public abstract class BaseMessageBuilder implements PnlAdlMessage {

	protected BaseElementBuilder initiatingHeaderElement;
	private List<BaseElementBuilder> elementsBucket;
	private String messgeBuilderType;
	protected Map<Integer,String> messagePartMap;
	
	public abstract void buildBridge();
	
	protected BaggageElementBuilder baggageElementBuilder = null;
	protected CreditCardElementBuilder creditCardElementBuilder = null;
	protected SSRElementBuilder ssrElementBuilder = null;
	protected MealElementBuilder mealElementBuilder = null;
	protected SeatElementBuilder seatElementBuilder = null;
	protected DOCSElementBuilder docsElementBuilder = null;
	protected DOCOElementBuilder docoElementBuilder = null;
	protected InfantElementBuilder infantElementBuilder = null;
	protected ChildElementBuilder childElementBuilder = null;
	protected AdultEticketElementBuilder adultEticketElementBuilder = null;
	protected StandByPassengerElementBuilder standByPassengerElementBuilder = null;
	protected WaitListPassengerElementBuilder waitListPassengerElementBuilder = null;
	protected OnwardConnectionElementBuilder onwardConnectionElementBuilder = null;
	protected InboundConnectionElementBuilder inboundConnectionElementBuilder = null;
	protected MarketingFlightElementBuilder marketingFlightElementBuilder = null;
	protected PnrElementBuilder pnrElementBuilder = null;
	protected GroupCodeElementBuilder groupCodeElementBuilder = null;
	protected NameElementBuilder nameElementBuilder = null;
	protected PaxModifierElementBuilder paxModifierElementBuilder = null;
	protected DestinationBulkElementBuilder destinationBulkElementBuilder = null;
	protected TotalByDestinationElementBuilder totalByDestinationElement = null;
	protected ClassCodeElementBuilder classCodeElementBuilder = null;
	protected EndElementBuilder endElementBuilder = null;
	protected HeaderElementBuilder headerElementBuilder = null;
	protected FlightNumberElementBuilder flightNumberElementBuilder = null;
	protected SeatConfigurationElementBuilder seatConfigurationElementBuilder = null;
	
	protected abstract void buildMessageHeaderReference();
	
	protected abstract void setElementChain() ;
	
	public BaseMessageBuilder(String messgeBuilderType) {
		this.messgeBuilderType = messgeBuilderType;
		messagePartMap = new HashMap<Integer,String>();
		initializeProductionFlow();
	}

	private void initializeProductionFlow() {
		elementsBucket = new ArrayList<BaseElementBuilder>();
		buildMessageHeaderReference();
		setElementChain();
		initiatingHeaderElement = getStartingElement();		
	}

	private BaseElementBuilder getStartingElement() {
		return headerElementBuilder;
	}

	protected HeaderElementBuilder headerElementBuilder() {
		headerElementBuilder = new HeaderElementBuilder();
		return headerElementBuilder;
	}

	protected BaggageElementBuilder baggageElementBuilder(){
		baggageElementBuilder = new BaggageElementBuilder();		
		return baggageElementBuilder;
	}
	
	protected CreditCardElementBuilder creditCardElementBuilder(){
		creditCardElementBuilder = new CreditCardElementBuilder();	
		return creditCardElementBuilder;
	}
	
	protected SSRElementBuilder ssrElementBuilder(){
		ssrElementBuilder = new SSRElementBuilder();
		return ssrElementBuilder;
	}
	
	protected MealElementBuilder mealElementBuilder(){
		mealElementBuilder = new MealElementBuilder();
		return mealElementBuilder;
	}
	
	protected SeatElementBuilder seatElementBuilder(){
		seatElementBuilder = new SeatElementBuilder();
		return seatElementBuilder;
	}
	
	protected DOCSElementBuilder docsElementBuilder(){
		docsElementBuilder = new DOCSElementBuilder();

		return docsElementBuilder;
	}
	
	protected DOCOElementBuilder docoElementBuilder(){
		docoElementBuilder = new DOCOElementBuilder();

		return docoElementBuilder;
	}
	
	protected InfantElementBuilder infantElementBuilder(){
		infantElementBuilder = new InfantElementBuilder();

		return infantElementBuilder;
	}
	
	protected ChildElementBuilder childElementBuilder(){
		childElementBuilder = new ChildElementBuilder();

		return childElementBuilder;
	}

	protected AdultEticketElementBuilder adultEticketElementBuilder(){
		adultEticketElementBuilder = new AdultEticketElementBuilder();

		return adultEticketElementBuilder;
	}
	
	protected StandByPassengerElementBuilder standByPassengerElementBuilder() {
		standByPassengerElementBuilder = new StandByPassengerElementBuilder();

		return standByPassengerElementBuilder;
	}
	
	protected WaitListPassengerElementBuilder waitListPassengerElementBuilder() {
		waitListPassengerElementBuilder = new WaitListPassengerElementBuilder();

		return waitListPassengerElementBuilder;
	}
	
	protected OnwardConnectionElementBuilder onwardConnectionElementBuilder() {
		onwardConnectionElementBuilder = new OnwardConnectionElementBuilder();

		return onwardConnectionElementBuilder;
	}
	
	protected InboundConnectionElementBuilder inboundConnectionElementBuilder() {
		inboundConnectionElementBuilder = new InboundConnectionElementBuilder();

		return inboundConnectionElementBuilder;
	}
	
	protected MarketingFlightElementBuilder marketingFlightElementBuilder() {
		marketingFlightElementBuilder = new MarketingFlightElementBuilder();

		return marketingFlightElementBuilder;
	}

	protected PnrElementBuilder pnrElementBuilder() {
		pnrElementBuilder = new PnrElementBuilder();

		return pnrElementBuilder;
	}

	protected GroupCodeElementBuilder groupCodeElementBuilder() {
		groupCodeElementBuilder = new GroupCodeElementBuilder();

		return groupCodeElementBuilder;
	}

	protected NameElementBuilder nameElementBuilder() {
		nameElementBuilder = new NameElementBuilder();

		return nameElementBuilder;
	}

	protected PaxModifierElementBuilder passengerModifierElementBuilder() {
		paxModifierElementBuilder = new PaxModifierElementBuilder();

		return paxModifierElementBuilder;
	}

	protected DestinationBulkElementBuilder destinationBulkElementBuilder() {
		destinationBulkElementBuilder = new DestinationBulkElementBuilder();
		return destinationBulkElementBuilder;
	}

	protected TotalByDestinationElementBuilder totalByDestinationFareBuilder() {
		totalByDestinationElement = new TotalByDestinationElementBuilder();
		return totalByDestinationElement;
	}

	protected ClassCodeElementBuilder classCodeElementBuilder() {
		classCodeElementBuilder = new ClassCodeElementBuilder();
		return classCodeElementBuilder;
	}

	protected EndElementBuilder endElementBuilder(){
		endElementBuilder = new EndElementBuilder();
		endElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		return endElementBuilder;
	}

	protected FlightNumberElementBuilder buildFlightNumberElementBuilder() {
		flightNumberElementBuilder = new FlightNumberElementBuilder();
		return flightNumberElementBuilder;
	}

	protected SeatConfigurationElementBuilder buildSeatConfigurationElementBuilder(){
		seatConfigurationElementBuilder = new SeatConfigurationElementBuilder();
		return seatConfigurationElementBuilder;
	}
	
	
	protected void setNextElementPair(int startingIndex) {
		if (elementsBucket != null
				&& startingIndex < (elementsBucket.size() - 1)) {
			BaseElementBuilder currentElement = elementsBucket
					.get(startingIndex);
			BaseElementBuilder nextElement = elementsBucket
					.get(++startingIndex);
			currentElement.setNextElementBuilder(nextElement);
			setNextElementPair(startingIndex);
		}
	}

	protected boolean isRemainingPassengersIn(PassengerCollection passengerCollection){
		
		boolean isAvailablePassengers=false;
		for (Map.Entry<String, List<DestinationFare>> entry : passengerCollection.getPnlAdlDestinationMap().entrySet()) {
			for(DestinationFare destinationFare:entry.getValue()){
				if(destinationFare.getAddPassengers() != null && destinationFare.getAddPassengers().size() > 0){
					isAvailablePassengers = true;
				}
				if(destinationFare.getDeletePassengers() != null && destinationFare.getDeletePassengers().size() > 0){
					isAvailablePassengers = true;
				}
				if(destinationFare.getChangePassengers() != null && destinationFare.getChangePassengers().size() > 0){
					isAvailablePassengers = true;
				}
				if(!isAvailablePassengers){
					if(!destinationFare.isOnceUsed()){
						isAvailablePassengers = true;
					}
				}
			}
		}
		return isAvailablePassengers;
	}
	
	protected void clearMessageContainers(ElementContext elementContext){
		elementContext.getMessageString().setLength(0);
		elementContext.getCurrentMessageLine().setLength(0);
	}
	
	protected void addMessagePart(Integer partNumber,String message){
		if(messagePartMap != null){
			messagePartMap.put(partNumber, message);
		}
	}
	
}
