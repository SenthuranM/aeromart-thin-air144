package com.isa.thinair.airreservation.core.bl.common;

import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author Nilindra Fernando
 */
public class LccBL {

	public static TempPaymentTnx recordTemporyPaymentEntry(TemporyPaymentTO temporyPaymentTO, CredentialsDTO credentialsDTO)
			throws ModuleException {

		// Inserting a record to T_TEMP_PAYMENT_TNX
		TempPaymentTnx tempPaymentTnx = new TempPaymentTnx();

		tempPaymentTnx.setTelephoneNo(BeanUtils.nullHandler(temporyPaymentTO.getTelephoneNo()));
		tempPaymentTnx.setMobileNo(BeanUtils.nullHandler(temporyPaymentTO.getMobileNo()));
		tempPaymentTnx.setAmount(temporyPaymentTO.getAmount());
		tempPaymentTnx.setPaymentCurrencyAmount(temporyPaymentTO.getPaymentCurrencyAmount());
		tempPaymentTnx.setPaymentCurrencyCode(temporyPaymentTO.getPaymentCurrencyCode());
		
		String contactName = temporyPaymentTO.getContactFullName();
		if (StringUtil.isNullOrEmpty(contactName)) {
			contactName = BeanUtils.nullHandler(temporyPaymentTO.getFirstName()) + " "
					+ BeanUtils.nullHandler(temporyPaymentTO.getLastName());
		}
	
		tempPaymentTnx.setContactPerson(contactName);
		tempPaymentTnx.setCustomerId(credentialsDTO.getCustomerId());
		tempPaymentTnx.setEmailAddress(temporyPaymentTO.getEmailAddress());

		tempPaymentTnx.setUserId(credentialsDTO.getUserId());
		tempPaymentTnx.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		tempPaymentTnx.setPaymentTimeStamp(new Date());
		tempPaymentTnx.setLast4DigitsCC(BeanUtils.getLast4Digits(temporyPaymentTO.getCcNo()));
		tempPaymentTnx.setPnr(temporyPaymentTO.getGroupPnr());
		tempPaymentTnx.setStatus(ReservationInternalConstants.TempPaymentTnxTypes.INITIATED);
		tempPaymentTnx.setHistory(tempPaymentTnx.getStatus());
		if (temporyPaymentTO.getProductType() != null) {
			tempPaymentTnx.setProductType(temporyPaymentTO.getProductType());
		}
		if (temporyPaymentTO.isOfflinePayment()) {
			tempPaymentTnx.setIsOfflinePayment("Y");
		}

		// charith
		// To avoid payments without currency conversion
		if (!AppSysParamsUtil.getBaseCurrency().equals(tempPaymentTnx.getPaymentCurrencyCode())
				&& tempPaymentTnx.getAmount().doubleValue() == tempPaymentTnx.getPaymentCurrencyAmount().doubleValue()) {
			throw new ModuleException("airreservations.temporyPayment.currencyConvertionFailed");
		}
		// Setting the IP Address if specified
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			tempPaymentTnx.setIpAddress(credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Is Credit
		if (temporyPaymentTO.isCredit()) {
			tempPaymentTnx.setTnxType(ReservationInternalConstants.TnxTypes.CREDIT);
		}
		// Is Debit
		else {
			tempPaymentTnx.setTnxType(ReservationInternalConstants.TnxTypes.DEBIT);
		}

		ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveTempPaymentInfo(tempPaymentTnx);

		return tempPaymentTnx;
	}
}
