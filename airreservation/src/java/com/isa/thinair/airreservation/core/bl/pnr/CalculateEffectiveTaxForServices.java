package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.bl.tax.EffectiveServiceTaxCalculator;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for Handling Tax calculation for Services
 * 
 * @author rumesh
 * 
 * @isa.module.command name="calculateEffectiveTaxForServices"
 */
public class CalculateEffectiveTaxForServices extends DefaultBaseCommand {

	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Map<Integer, IPayment> pnrPaxIdAndPayments = this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, Map.class);
		Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges = this
				.getParameter(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, Map.class);
		Collection<OndFareDTO> ondFareDTOs = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
		Map<String, String> requoteSegmentMap = (Map<String, String>) this.getParameter(CommandParamNames.REQUOTE_SEGMENT_MAP);
		Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExtChgMap = this
				.getParameter(CommandParamNames.PAX_TAXING_EXT_CHARGE_MAP, Map.class);
		Boolean isCancelOndOperation = this.getParameter(CommandParamNames.IS_CANCEL_OND_OPERATION, Boolean.class);
		Boolean isAddSegmentOperation = this.getParameter(CommandParamNames.IS_ADD_OND_OPERATION, Boolean.class);
		Collection<Integer> flownPnrSegIds = this.getParameter(CommandParamNames.FLOWN_PNR_SEGMENT_IDS, Collection.class);

		Map<Integer, List<ExternalChgDTO>> paxEffectiveExtraFees = new HashMap<Integer, List<ExternalChgDTO>>();

		IExtraFeeCalculator extraFeeCalculator = new AncillaryExtraFeeCalculator(reservation, EXTERNAL_CHARGES.JN_ANCI,
				ondFareDTOs, requoteSegmentMap, paxSeqWiseExtChgMap, reprotectedExternalCharges, isCancelOndOperation,
				isAddSegmentOperation, flownPnrSegIds);
		extraFeeCalculator.calculate();

		IExtraFeeCalculator nameChangeExtraFeeCalculator = new NameChangeExtraFeeCalculator(reservation,
				EXTERNAL_CHARGES.JN_OTHER, ondFareDTOs, requoteSegmentMap, reprotectedExternalCharges);
		nameChangeExtraFeeCalculator.calculate();

		IExtraFeeCalculator serviceTaxCalculator = EffectiveServiceTaxCalculator.getCalculatorForTicketingRevenue(reservation,
				ondFareDTOs, reprotectedExternalCharges, paxSeqWiseExtChgMap, flownPnrSegIds);
		serviceTaxCalculator.calculate();

		List<IExtraFeeCalculator> extraFeeCalculators = new ArrayList<>();
		extraFeeCalculators.add(extraFeeCalculator);
		extraFeeCalculators.add(nameChangeExtraFeeCalculator);
		extraFeeCalculators.add(serviceTaxCalculator);

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			Integer paymentPaxId = null;
			Integer pnrPaxId = reservationPax.getPnrPaxId();
			Integer paxSequence = reservationPax.getPaxSequence();

			if (!reservation.isInfantPaymentRecordedWithInfant() && PaxTypeTO.INFANT.equals(reservationPax.getPaxType())) {
				paymentPaxId = reservationPax.getParent().getPnrPaxId();
			} else {
				paymentPaxId = reservationPax.getPnrPaxId();
			}

			for (IExtraFeeCalculator feeCal : extraFeeCalculators) {
				if (feeCal.hasExtraFeeFor(pnrPaxId)) {

					Collection<? extends ExternalChgDTO> paxExtraFees = feeCal.getPaxExtraFee(pnrPaxId);
					addChageToPaymentAssembler(pnrPaxIdAndPayments, paymentPaxId, paxExtraFees);

					if (!paxEffectiveExtraFees.containsKey(paxSequence)) {
						paxEffectiveExtraFees.put(paxSequence, new ArrayList<ExternalChgDTO>());
					}

					paxEffectiveExtraFees.get(paxSequence).addAll(paxExtraFees);

					if (!reprotectedExternalCharges.containsKey(pnrPaxId)) {
						reprotectedExternalCharges.put(pnrPaxId, new ArrayList<ExternalChgDTO>());
					}

					reprotectedExternalCharges.get(pnrPaxId).addAll(paxExtraFees);
				}
			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, reprotectedExternalCharges);
		response.addResponceParam(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, pnrPaxIdAndPayments);
		response.addResponceParam(CommandParamNames.EFFECTIVE_PAX_TAX_CHARGES, paxEffectiveExtraFees);
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		if (this.getParameter(CommandParamNames.SELECTED_ANCI_MAP) != null) {
			response.addResponceParam(CommandParamNames.SELECTED_ANCI_MAP,
					this.getParameter(CommandParamNames.SELECTED_ANCI_MAP));
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	private void addChageToPaymentAssembler(Map<Integer, IPayment> pnrPaxIdAndPayments, Integer pnrPaxId,
			Collection<? extends ExternalChgDTO> paxExtraFees) {
		if (pnrPaxIdAndPayments != null && paxExtraFees.size() > 0) {
			if (!pnrPaxIdAndPayments.containsKey(pnrPaxId)) {
				pnrPaxIdAndPayments.put(pnrPaxId, new PaymentAssembler());
			}

			// Remove existing JN tax calculation
			PaymentAssembler paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(pnrPaxId);
			if (paxExtraFees.iterator().next().getExternalChargesEnum() != EXTERNAL_CHARGES.SERVICE_TAX) {
				paymentAssembler.removePerPaxExternalCharge(paxExtraFees.iterator().next().getExternalChargesEnum());
			}

			pnrPaxIdAndPayments.get(pnrPaxId).addExternalCharges((Collection<ExternalChgDTO>) paxExtraFees);
		}
	}

}
