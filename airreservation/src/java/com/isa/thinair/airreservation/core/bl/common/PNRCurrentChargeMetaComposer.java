package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Currently in order to pick the reservation current charge meta info, we need to rely on ReservationServiceBean's
 * getPnrCharges. With requote, this is no longer valid for already canceled segments. This motivated me to write this,
 * to be used with requote flow
 * 
 * @author Nilindra Fernando
 * @since Sep 7, 2012
 */
public class PNRCurrentChargeMetaComposer {

	@SuppressWarnings("unchecked")
	public static Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseCurrentCharges(String pnr, long version,
			boolean excludedPnrSegCharges, List<String> excludedPnrSegIds)
			throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Check Constraints
		checkConstraints(reservation, version);

		Collection<Integer> chgRateIds = getAllChargeRateIds(reservation);
		FaresAndChargesTO faresAndChargesTO = ReservationModuleUtils.getFareBD().getRefundableStatuses(null, chgRateIds, null);
		Map<Integer, ChargeTO> mapChgTO = faresAndChargesTO.getChargeTOs();

		Map<Integer, Collection<ChargeMetaTO>> mapPaxWiseChargeMetaInfo = new HashMap<Integer, Collection<ChargeMetaTO>>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				if (ReservationApiUtils.isParentAfterSave(reservationPax) && !reservation.isInfantPaymentRecordedWithInfant()) {
					// Since one infant is there for a parent.
					ReservationPax infant = BeanUtils.getFirstElement(reservationPax.getInfants());
					mapPaxWiseChargeMetaInfo.put(reservationPax.getPnrPaxId(), ChargeMetaTO.sum(
							getReservationPaxChargeMetaInfo(reservationPax, mapChgTO, excludedPnrSegCharges, excludedPnrSegIds),
							getReservationPaxChargeMetaInfo(infant, mapChgTO, excludedPnrSegCharges, excludedPnrSegIds)));
				} else {
					mapPaxWiseChargeMetaInfo.put(reservationPax.getPnrPaxId(),
							getReservationPaxChargeMetaInfo(reservationPax, mapChgTO, excludedPnrSegCharges, excludedPnrSegIds));
				}
			} else {
				mapPaxWiseChargeMetaInfo.put(reservationPax.getPnrPaxId(),
						getReservationPaxChargeMetaInfo(reservationPax, mapChgTO, excludedPnrSegCharges, excludedPnrSegIds));
			}
		}

		return mapPaxWiseChargeMetaInfo;
	}

	private static Collection<ChargeMetaTO> getReservationPaxChargeMetaInfo(ReservationPax reservationPax,
			Map<Integer, ChargeTO> mapChgTO, boolean excludedPnrSegCharges, List<String> excludedPnrSegIds)
			throws ModuleException {
		Collection<ChargeMetaTO> colChargeMetaTO = new ArrayList<ChargeMetaTO>();

		for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
			if (!excludedPnrSegCharges && excludedPnrSegIds == null) {
				colChargeMetaTO.addAll(convert(reservationPaxFare.getCharges(), mapChgTO));
			} else {
				for (ReservationPaxFareSegment resPaxFareSeg : reservationPaxFare.getPaxFareSegments()) {
					if (excludedPnrSegCharges && excludedPnrSegIds != null
							&& excludedPnrSegIds.contains(String.valueOf(resPaxFareSeg.getSegment().getPnrSegId()))) {
						colChargeMetaTO.addAll(convert(reservationPaxFare.getCharges(), mapChgTO));
					}
				}
			}
		}

		return colChargeMetaTO;
	}

	private static Collection<ChargeMetaTO> convert(Collection<ReservationPaxOndCharge> charges, Map<Integer, ChargeTO> mapChgTO)
			throws ModuleException {
		Collection<ChargeMetaTO> colChargeMetaTO = new ArrayList<ChargeMetaTO>();

		for (ReservationPaxOndCharge reservationPaxOndCharge : charges) {
			colChargeMetaTO.add(TOAssembler.createChargeMetaTO(reservationPaxOndCharge, mapChgTO));
		}

		return colChargeMetaTO;
	}

	private static Collection<Integer> getAllChargeRateIds(Reservation reservation) {
		Collection<Integer> chargeRateIds = new HashSet<Integer>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxFare.getCharges()) {
					if (reservationPaxOndCharge.getChargeRateId() != null) {
						chargeRateIds.add(reservationPaxOndCharge.getChargeRateId());
					}
				}
			}
		}

		return chargeRateIds;
	}

	private static void checkConstraints(Reservation reservation, long version) throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version);
	}

}
