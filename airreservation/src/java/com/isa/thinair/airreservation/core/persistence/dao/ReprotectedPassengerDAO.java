package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;

/**
 * ReprotectedPassengerDAO is the business DAO interface for the  Reprotected Passenger service apis
 * 
 * @author Chanaka
 */

public interface ReprotectedPassengerDAO {	
	
	public void saveAllReprotectedPassengers (Collection<ReprotectedPassenger> reprotectedPassengers);
	
	public List<ReprotectedPassenger> getAllEntriesOfPaxList (List<Integer> pnrPaxIds);	

	public Page<ReprotectedPaxDTO> searchReprotectedPax(int start, int pageSize, ReprotectedPaxSearchrequest reprotectedPaxSearchrequest);

	public int getReprotectedPaxCount (ReprotectedPaxSearchrequest reprotectedPaxSearchrequest);
}
