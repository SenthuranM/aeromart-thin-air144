package com.isa.thinair.airreservation.core.persistence.hibernate.external;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardTypeDTO;
import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentFocusDTO;
import com.isa.thinair.airreservation.api.model.CreditCardSalesFocus;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.airreservation.core.persistence.dao.FocusSystemDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * FocusSalesDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @isa.module.dao-impl dao-name="FocusSystemDAO"
 */
public class FocusSalesDAOImpl extends PlatformBaseHibernateDaoSupport implements FocusSystemDAO {
	
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(FocusSalesDAOImpl.class);
	
	/*
	 * Returns total of generated CreditCard sales from internal summary table
	 * 
	 * @Param fromDate
	 * @Param toDate
	 */
	public BigDecimal getGeneratedCreditCardSalesTotal(Date fromDate, Date toDate) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { fromDate, toDate };

		String sql = "select sum(total_daily_sales) as tot from t_creditcard_sales where date_of_sale between ?  and ?";

		BigDecimal d = (BigDecimal) templete.query(sql, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal amount = null;
				while (rs.next()) {
					amount = rs.getBigDecimal("tot");
				}
				return amount;
			}
		});

		return d;
	}
	
	/*
	 * Returns total of transferred CreditCard sales from external system
	 * 
	 * @Param fromDate
	 * @Param toDate
	 * @Param AccontingSystemTemplate
	 */
	public BigDecimal getTransferedCreditCardSalesTotal(Date fromDate, Date toDate, AccontingSystemTemplate template) {
		BigDecimal totAmount = new BigDecimal(-1);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String frmDate = simpleDateFormat.format(fromDate);
		String TODate = simpleDateFormat.format(toDate);
		Connection connection = template.getExternalConnection();

		String selectStatment = "SELECT SUM(TOTAL_DAILY_SALES) as amt " + " FROM INT_T_CREDITCARD_SALES "
				+ " WHERE (DATE_OF_SALE BETWEEN '" + frmDate + "' AND '" + TODate + "')";

		PreparedStatement selectStmt = null;
		// create the prepared statements
		try {
			selectStmt = connection.prepareStatement(selectStatment);
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", AirreservationConstants.MODULE_NAME);
		}

		try {
			ResultSet res = selectStmt.executeQuery();
			while (res.next()) {
				totAmount = res.getBigDecimal("amt");
			}
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, "exdb.getconnection.retrieval.failed", AirreservationConstants.MODULE_NAME);
		}

		return totAmount;
	}
	
	/*
	 * Clear sales in internal summary tables for a date
	 * 
	 * @Param date
	 */
	public void clearCreditCardSalesTable(Date date) {

		String hql = "select creditSales from CreditCardSalesFocus creditSales where creditSales.dateOfSale=?";
		Collection<?> pojos = getSession().createQuery(hql).setDate(0, date).list();

		if (pojos != null && pojos.size() > 0) {
			deleteAll(pojos);
		}

	}
	
	/*
	 * Gets CC payments for a date
	 * 
	 * @Param date
	 */
	@SuppressWarnings("unchecked")
	public Collection<CreditCardPaymentFocusDTO> getCreditCardPayments(Date date) {
		Collection<CreditCardPaymentFocusDTO> creditDTO = new ArrayList<CreditCardPaymentFocusDTO>();
		Collection<Integer> nominalCodes = new ArrayList<Integer>();
		HashMap<Integer,BigDecimal> nominalCodeAmountMap = new HashMap<Integer,BigDecimal>();
		BigDecimal paymentNCAmount = null;
		BigDecimal refundNCAmount = null;
		Integer cTypeId = null;
		Integer paymentNC = null;
		Integer refundNC = null;
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		int cardTypeId = 0;
		String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();

		Collection<CreditCardTypeDTO> creditCardTypeList = getCreditCardPaymentRefundNominalCodes();
		Iterator<CreditCardTypeDTO> it = creditCardTypeList.iterator();
		// Adding payment, refund nominal codes for credit card types
		while (it.hasNext()) {
			CreditCardTypeDTO ccTypeDTO = it.next();
			if (ccTypeDTO.getPaymentNominalCode() != null) {
				nominalCodes.add(ccTypeDTO.getPaymentNominalCode());
			}
			if (ccTypeDTO.getRefundNominalCode() != null) {
				nominalCodes.add(ccTypeDTO.getRefundNominalCode());
			}
		}

		if (nominalCodes.size() != 0) {
			// Returns the amount for payment, refund nominal codes for credit
			// card types
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT SUM(x.amount) AS amount , x.nominal_code as nominal_code " );
			sql.append(" FROM ( ");
			sql.append(" SELECT   amount AS amount, nominal_code " );
			sql.append(" FROM     T_PAX_TRANSACTION ");
			sql.append(" WHERE    nominal_code IN (");
			sql.append(Util.buildIntegerInClauseContent(nominalCodes) );
			sql.append(")  ");
			sql.append(" AND (PAYMENT_CARRIER_CODE = '");
			sql.append(thisAirlineCode);
			sql.append("' OR PAYMENT_CARRIER_CODE IS NULL) ");
			sql.append(" AND      TRUNC(tnx_date) = ? ");
			sql.append( " UNION ALL  SELECT amount AS amount, nominal_code ");
			sql.append(" FROM     T_PAX_EXT_CARRIER_TRANSACTIONS  WHERE    nominal_code IN (" );
			sql.append(Util.buildIntegerInClauseContent(nominalCodes) );
			sql.append(")  AND  TRUNC(TXN_TIMESTAMP) = ?   ) x ");
			sql.append(" GROUP BY x.nominal_code");
			
			Object input[] = new Object[2];
			input[0] = date;
			input[1] = date;
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate template = new JdbcTemplate(ds);
			nominalCodeAmountMap = (HashMap<Integer,BigDecimal>) template.query(sql.toString(), input, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap<Integer,BigDecimal> amountMap = new HashMap<Integer,BigDecimal>();
					if (rs != null) {
						Integer nominalCode = null;
						BigDecimal amount = null;
						while (rs.next()) {
							nominalCode = new Integer(rs.getInt("nominal_code"));
							amount = rs.getBigDecimal("amount");
							if ((nominalCode != null) && (amount != null)) {
								amountMap.put(nominalCode, amount);
							}
						}
					}
					return amountMap;
				}
			});
		}

		it = creditCardTypeList.iterator();
		while (it.hasNext()) {
			CreditCardTypeDTO ccTypeDTO = it.next();
			cTypeId = ccTypeDTO.getCardTypeId();
			paymentNC = ccTypeDTO.getPaymentNominalCode();
			refundNC = ccTypeDTO.getRefundNominalCode();

			if ((cTypeId != null) && (paymentNC != null) && (refundNC != null)) {
				cardTypeId = 0;
				if (cTypeId.intValue() == 1) {
					cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.MASTER;
				} else if (cTypeId.intValue() == 2) {
					cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.VISA;
				} else if (cTypeId.intValue() == 3) {
					cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.AMEX;
				} else if (cTypeId.intValue() == 4) {
					cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.DINERS;
				} else if (cTypeId.intValue() == 5) {
					cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.GENERIC;
				} else if (cTypeId.intValue() == 6) {
					cardTypeId = ReservationInternalConstants.CreditCardPaymentMappings.CMI;
				}

				paymentNCAmount = null;
				refundNCAmount = null;
				if ((nominalCodeAmountMap != null) && (!nominalCodeAmountMap.isEmpty())) {
					paymentNCAmount = nominalCodeAmountMap.get(paymentNC);
					refundNCAmount = nominalCodeAmountMap.get(refundNC);
				}
				if ((paymentNCAmount == null)) {
					paymentNCAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				}
				if (refundNCAmount == null) {
					refundNCAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				}

				if ((cardTypeId != 0)) {
					CreditCardPaymentFocusDTO ccPaymentDTO = new CreditCardPaymentFocusDTO();
					ccPaymentDTO.setCardTypeID(cardTypeId);
					ccPaymentDTO.setDateOfSale(date);

					totalAmount = AccelAeroCalculator.add(paymentNCAmount, refundNCAmount).negate();

					ccPaymentDTO.setTotalSales(totalAmount);
					creditDTO.add(ccPaymentDTO);
				}
			}
		}
		return creditDTO;
	}
		
	/*
	 * Inserting Credit Card Sales to internal summary table via Hiberatne.
	 */
	public Collection<CreditCardSalesFocus> insertCreditCardSalesToInternal(Collection<CreditCardPaymentFocusDTO> col) {
		log.info("#################### Inserting Credit Card Sales To Internal Focus");
		Collection<CreditCardSalesFocus> creditCardPOJOS = new ArrayList<CreditCardSalesFocus>();
		Date now = new Date();
		Iterator<CreditCardPaymentFocusDTO> it = col.iterator();

		if (col != null) {
			while (it.hasNext()) {
				CreditCardPaymentFocusDTO dto = it.next();

				CreditCardSalesFocus cardSales = new CreditCardSalesFocus();
				cardSales.setCardTypeId(dto.getCardTypeID());
				cardSales.setDateOfSale(dto.getDateOfSale());
				cardSales.setTotalDailySales(dto.getTotalSales());
				cardSales.setTransferStatus('N');
				cardSales.setTransferTimeStamp(now);
				creditCardPOJOS.add(cardSales);
			}
		}
		saveOrUpdateAllCreditCardSales(creditCardPOJOS);
		log.info("######## Finished Inserting Credit Card Sales To Internal Focus");
		return creditCardPOJOS;
	}

	/*
	 * Inserting CreditCard Sales data to external system
	 * 
	 * @Param Collection<CreditCardPaymentSageDTO>
	 * @Param AccontingSystemTemplate
	 * 
	 * @throws ClassNotFoundException, SQLException, Exception
	 */
	public void insertCreditCardSalesToExternal(Collection<CreditCardPaymentFocusDTO> col, AccontingSystemTemplate template) throws ClassNotFoundException, SQLException, Exception {
		log.info("##################### Going to Insert CreditCard Sales To Focus XDB");
		Connection connection = null;
		
		try {			
			connection = template.getExternalConnection();
			connection.setAutoCommit(false);
			Iterator<CreditCardPaymentFocusDTO> it = col.iterator();

			// common sql query
			CreditCardPaymentFocusDTO dto = null;
			String sql = "insert into  INT_T_CREDITCARD_SALES  values (?,?,?,?)";
			PreparedStatement stmt = connection.prepareStatement(sql);
			PreparedStatement select = connection
					.prepareStatement("select * from INT_T_CREDITCARD_SALES where DATE_OF_SALE=? and CARD_TYPE=?");
			String sqlq = "delete from INT_T_CREDITCARD_SALES where DATE_OF_SALE=? and CARD_TYPE=?";
			PreparedStatement stmtq = connection.prepareStatement(sqlq);

			while (it.hasNext()) {
				dto = it.next();
				select.setDate(1, new java.sql.Date(dto.getDateOfSale().getTime()));
				select.setString(2, String.valueOf(dto.getCardTypeID()));
				ResultSet res = select.executeQuery();
				while (res.next()) {

					stmtq.setDate(1, res.getDate("DATE_OF_SALE"));
					stmtq.setString(2, res.getString("CARD_TYPE"));
					stmtq.executeUpdate();
				}

				stmt.setDate(1, new java.sql.Date(dto.getDateOfSale().getTime()));
				stmt.setString(2, String.valueOf(dto.getCardTypeID()));
				stmt.setBigDecimal(3, dto.getTotalSales());
				stmt.setInt(4, 0);

				stmt.executeUpdate();
			}

		} catch (Exception exception) {
			log.error("###### Error Inserting credit card Sales to Focus XDB", exception);
			connection.rollback();
			if (exception instanceof SQLException || exception instanceof ClassNotFoundException) {
				throw exception;
			} else {

				throw new CommonsDataAccessException(exception, "transfer.creditcardsales.failed");
			}
		}

		try {
			log.debug("########## Committing Credit Card Sales to Focus XDB");
			connection.commit();
		} catch (Exception e) {
			log.error("###### Error Committing credit card Sales to Focus XDB", e);
			throw new CommonsDataAccessException(e, "transfer.creditcardsales.failed");
		} finally {
			// connection.setAutoCommit(prevAutoCommitStatus);
			connection.close();
		}
		log.info("############ Successfuly Inserted CreditCard Sales To Focus XDB");

	}
	
	/*
	 * insert in hibernate pojos
	 * 
	 * @param creditCardSalesPOJOS
	 */
	private void saveOrUpdateAllCreditCardSales(Collection<CreditCardSalesFocus> creditCardSalesPOJOS) {
		hibernateSaveOrUpdateAll(creditCardSalesPOJOS);
	}
	
	/*
	 * Updates transferStatus in internal Summary table from 'N' to 'Y'
	 * 
	 * @Param Collection<CreditCardSalesSage>
	 */
	public void updateInternalCreditTable(Collection<CreditCardSalesFocus> pojos) {

		Iterator<CreditCardSalesFocus> it = pojos.iterator();
		while (it.hasNext()) {
			CreditCardSalesFocus creditCardSales = it.next();
			creditCardSales.setTransferStatus('Y');
		}
		saveOrUpdateAllCreditCardSales(pojos);
	}
	
	/*
	 * Returns total of CreditCard Sales on each date
	 * 
	 * @Param fromDate
	 * @Param toDate
	 */
	public Object[] getCreditCardSalesHistory(Date fromDate, Date toDate) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final ArrayList<CCSalesHistoryDTO> dtoList = new ArrayList<CCSalesHistoryDTO>();
		final ArrayList<java.sql.Date> dateList = new ArrayList<java.sql.Date>();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String from = sdf.format(fromDate);
		String to = sdf.format(toDate);
		StringBuffer sql = new StringBuffer();

		sql.append(" select t.date_of_sale as dos, sum(t.total_daily_sales) as total, t.transfer_timestamp as transfertime, t.transfer_status as tnsstatus from t_creditcard_sales t where ");
		sql.append(" trunc(t.date_of_sale) between '");
		sql.append(from);
		sql.append("' and '");
		sql.append(to);
		sql.append("' group by t.date_of_sale, t.transfer_timestamp, t.transfer_status order by t.date_of_sale");

		templete.query(sql.toString(),

				new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					CCSalesHistoryDTO dto = new CCSalesHistoryDTO();

					dto.setDateOfsale(rs.getDate("dos"));

					dto.setCardType("All Cards Total");
					dto.setTotalDailySales(rs.getBigDecimal("total"));
					dto.setTransferStatus(rs.getString("tnsstatus"));
					dto.setTransferTimeStamp(rs.getTimestamp("transfertime"));

					dateList.add(rs.getDate("dos"));
					dtoList.add(dto);
				}
				return dtoList;
			}
		});

		return new Object[] { dtoList, dateList };

	}	
	
	/*
	 * Returns the credit card type information
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<CreditCardTypeDTO> getCreditCardPaymentRefundNominalCodes() {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		String sql = "SELECT card_type_id, card_type, payment_nc, reund_nc,account_code " + " FROM T_CREDITCARD_TYPE ";
		return (ArrayList<CreditCardTypeDTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<CreditCardTypeDTO> creditCardTypes = new ArrayList<CreditCardTypeDTO>();
				if (rs != null) {
					while (rs.next()) {
						CreditCardTypeDTO ccTypeDTO = new CreditCardTypeDTO();
						ccTypeDTO.setCardTypeId(new Integer(rs.getInt("card_type_id")));
						ccTypeDTO.setCardType(rs.getString("card_type"));
						ccTypeDTO.setPaymentNominalCode(new Integer(rs.getInt("payment_nc")));
						ccTypeDTO.setRefundNominalCode(new Integer(rs.getInt("reund_nc")));
						ccTypeDTO.setAccountCode(rs.getString("account_code"));
						creditCardTypes.add(ccTypeDTO);
					}
				}
				return creditCardTypes;
			}
		});
	}

	@Override
	public void clearAgentTopUpTable(Date date) {
		// TODO Auto-generated method stub

	}

}
