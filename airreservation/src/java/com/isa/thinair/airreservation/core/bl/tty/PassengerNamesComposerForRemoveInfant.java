package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class PassengerNamesComposerForRemoveInfant implements PassengerNamesComposingStrategy {

	@Override
	public List<SSRDTO> composePassengerNames(Reservation reservation, BookingRequestDTO bookingRequestDTO,
			TypeBRequestDTO typeBRequestDTO) {

		List<NameDTO> names = new ArrayList<NameDTO>();
		List<SSRDTO> ssrDTOs = new ArrayList<SSRDTO>();
		NameDTO paxName = null;
		SSRChildDTO childDTO = null;
		SSRInfantDTO infantDTO = null;
		names = new ArrayList<NameDTO>();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (reservationPax != null) {
				if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
					paxName = new NameDTO();
					TypeBPassengerAdopter.adoptPax(paxName, reservationPax);
					names.add(paxName);
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType())) {
					paxName = new NameDTO();
					TypeBPassengerAdopter.adoptPax(paxName, reservationPax);
					names.add(paxName);
					childDTO = new SSRChildDTO();
					TypeBPassengerAdopter.adoptChild(childDTO, reservationPax);
					if (typeBRequestDTO.getCsOCCarrierCode() != null) {
						childDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
						childDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode());
					} else {
						childDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						childDTO.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
					}
					ssrDTOs.add(childDTO);
				} else {
					for (BookingSegmentDTO bookingSegment : bookingRequestDTO.getBookingSegmentDTOs()) {
						if (bookingSegment.getCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {
							infantDTO = new SSRInfantDTO();
							TypeBPassengerAdopter.adoptInfant(infantDTO, reservationPax);
							if (typeBRequestDTO.getCsOCCarrierCode() != null) {
								infantDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
							} else {
								infantDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
							}
							infantDTO.setAdviceOrStatusCode(bookingSegment.getAdviceOrStatusCode());
							infantDTO.setSegmentDTO(bookingSegment);
							ssrDTOs.add(infantDTO);
						}
					}
				}
			}
		}
		bookingRequestDTO.setNewNameDTOs(names);
		
		List<SSRInfantDTO> splitedInfants = typeBRequestDTO.getSplitedInfants();
		if (splitedInfants != null && splitedInfants.size() > 0) {
			for (SSRInfantDTO infantDto : splitedInfants) {
				for (BookingSegmentDTO bookingSegment : bookingRequestDTO.getBookingSegmentDTOs()) {
					if (bookingSegment.getCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {
						SSRInfantDTO infant = new SSRInfantDTO();
						infant.setInfantTitle(infantDto.getInfantTitle());
						infant.setInfantFirstName(infantDto.getInfantFirstName());
						infant.setInfantLastName(infantDto.getInfantLastName());
						infant.setInfantDateofBirth(infantDto.getInfantDateofBirth());
						infant.setGuardianFirstName(infantDto.getGuardianFirstName());
						infant.setGuardianLastName(infantDto.getGuardianLastName());
						infant.setGuardianTitle(infantDto.getGuardianTitle());
						infant.setCarrierCode(infantDto.getCarrierCode());
						infant.setAdviceOrStatusCode(infantDto.getAdviceOrStatusCode());
						infant.setSegmentDTO(bookingSegment);
						ssrDTOs.add(infant);
					}
				}
			}
		}
		return ssrDTOs;
	}

}
