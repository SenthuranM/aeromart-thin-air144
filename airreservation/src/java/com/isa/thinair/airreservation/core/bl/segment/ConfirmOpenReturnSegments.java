/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.SeatsForSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ssr.MedicalSsrDTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalMealChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ExternalSMChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.util.IATAETicketGenerator;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for confirming open return segments
 * 
 * There could be 2 types of open return transfers mainly 1. Direct Transfer - Which means that the old segment(s) count
 * and the new segment(s) count match. 2. InDirect Transfer - Which means that the old segment(s) count and the new
 * segment(s) count mismatch
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="confirmOpenReturnSegments"
 */
public class ConfirmOpenReturnSegments extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ConfirmOpenReturnSegments.class);

	/** Holds reservation payment types */
	private Integer pnrPaymentTypes;

	/** Holds whether Credentials information */
	private CredentialsDTO credentialsDTO;

	/** Holds the confirmed pnr passenger ids */
	private final Collection<Integer> confirmPnrPaxIds = new ArrayList<Integer>();

	/** Construct the ConfirmOpenReturnSegments object */
	public ConfirmOpenReturnSegments() {
	}

	/**
	 * Execute method of the ConfirmOpenReturnSegments command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Inside execute");
		}

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection<Integer> pnrSegIds = (Collection) this.getParameter(CommandParamNames.PNR_SEGMENT_IDS);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		Collection colOndFareDTO = (Collection) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		Map segObjectsMap = (Map) this.getParameter(CommandParamNames.SEG_OBJECTS_MAP);
		Collection pnrExtSegments = (Collection) this.getParameter(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS);
		Collection<ReservationSegment> colReservationSegment = (Collection<ReservationSegment>) this
				.getParameter(CommandParamNames.RESERVATION_SEGMENTS);
		Collection<Integer> orderdNewFlgSegIds = (Collection<Integer>) this
				.getParameter(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS);
		Collection blockSeatIds = (Collection) this.getParameter(CommandParamNames.BLOCK_KEY_IDS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Integer pnrPaymentTypes = (Integer) this.getParameter(CommandParamNames.PNR_PAYMENT_TYPES);
		Map pnrPaxIdAndPayments = (Map) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		Date zuluReleaseTimeStamp = (Date) this.getParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP);
		Date lastModificationTimeStamp = (Date) this.getParameter(CommandParamNames.LAST_MODIFICATION_TIMESTAMP);
		String lastCurrencyCode = (String) this.getParameter(CommandParamNames.LAST_CURRENCY_CODE);

		// Checking params
		this.validateParams(pnr, pnrSegIds, version, colOndFareDTO, segObjectsMap, colReservationSegment, orderdNewFlgSegIds,
				blockSeatIds, pnrPaxIdAndPayments, pnrPaymentTypes, credentialsDTO);

		// Set Arguments
		this.setArguments(pnrPaymentTypes, credentialsDTO);

		// Get the reservation
		Reservation reservation = loadReservation(pnr);

		// Check cancel segment constraints
		this.checkOpenReturnConfirmSegmentsConstraints(reservation, pnrSegIds, pnrPaxIdAndPayments, colOndFareDTO, version);

		// Overrides the zulu release time
		this.overrideZuluReleaseTimeStamp(reservation, zuluReleaseTimeStamp);

		// Releasing the open return bucket seats
		this.releaseOpenReturnBucketSeats(reservation, pnrSegIds);

		// Add external segments
		this.addExternalSegments(reservation, pnrExtSegments);

		Collection<Integer> newPnrSegIds = new ArrayList<Integer>();
		Collection<ReservationSegment> ammendedSegments = new HashSet<ReservationSegment>();
		List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
		List<Integer> forcedConfirmedPaxSeqsAfterChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);
		Collection<ReservationPax> etGenerationEligiblePax = ReservationApiUtils.getConfirmedPaxByOperationUsingForceCNFSeqs(
				forcedConfirmedPaxSeqsBeforeChange, forcedConfirmedPaxSeqsAfterChange, reservation);

		if (pnrSegIds.size() == orderdNewFlgSegIds.size()) {
			reservation = processDirectTransfer(reservation, pnrSegIds, orderdNewFlgSegIds, colOndFareDTO, credentialsDTO);
			newPnrSegIds.addAll(pnrSegIds);
			ammendedSegments.addAll(getPNRSegments(pnrSegIds, reservation));
		} else {
			reservation = processInDirectTransfer(reservation, pnrSegIds, orderdNewFlgSegIds, colOndFareDTO,
					colReservationSegment, credentialsDTO);
			trackNewPnrSegIds(newPnrSegIds, colReservationSegment);
			ammendedSegments.addAll(colReservationSegment);
		}

		// TODO : Need to check balance to pay and pass
		if (!reservation.getStatus().equals(ReservationStatus.ON_HOLD)) {
			ADLNotifyer.recordAddedSegmentsForAdl(reservation, orderdNewFlgSegIds, reservation.getStatus(),
					etGenerationEligiblePax, null);
		}

		// Adds any external charges for confirmed passengers
		this.addExternalCharges(reservation, pnrPaxIdAndPayments, credentialsDTO);

		// Leave the states as it is
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {

			// Handling confirmed reservations
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
				if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)) {
					// If it's confirmed get the ownership
					this.captureOwnerShip(reservation);
				}
				// Handling cancel reservations
			} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
				// Update cancel states
				this.updateCancelStates(reservation);
			}
		} else {
			// Update the general states
			this.updateGeneralStates(reservation, pnrPaxIdAndPayments);

			// Process force mode
			this.checkForceMode(reservation);
		}

		ADLNotifyer.updateConnectedSegmentsWhenAddSegment(reservation, orderdNewFlgSegIds);
		ReservationApiUtils.updateModificationStates(reservation, lastModificationTimeStamp, lastCurrencyCode, credentialsDTO);

		// Saves the reservation
		ReservationProxy.saveReservation(reservation);

		Map<Integer, LinkedList<Integer>> flgtSegmentMap = this.getFlightSegmentMap(ammendedSegments);
		// Save the seating information
		Map<Integer, String> flightSeatIdsAndPaxTypes = ReservationBO.saveSeats(pnrPaxIdAndPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Save the Meal information
		Collection[] flightMealInfo = ReservationBO.saveMeals(pnrPaxIdAndPayments, flgtSegmentMap, reservation, credentialsDTO);

		Collection[] flightBaggageInfo = BaggageAdaptor.saveBaggages(pnrPaxIdAndPayments, flgtSegmentMap, reservation,
				credentialsDTO);

		// Process Seats for passengers
		this.processPassengerSeats(blockSeatIds, reservation, flightSeatIdsAndPaxTypes, flightMealInfo[1], flightBaggageInfo[1]);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		ReservationAudit reservationAudit = this.composeAudit(pnr, newPnrSegIds, credentialsDTO);

		Collection colReservationAudit = new ArrayList();
		colReservationAudit.add(reservationAudit);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, reservation.isEnableTransactionGranularity());

		/*
		 * TODO open return segment not active in airarabia uncomment this when it is active to send medical ssr email
		 */

		/*
		 * MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(pnrPaxIdAndPayments, reservation); medicalSsrDTO.sendEmail();
		 */

		if (log.isDebugEnabled()) {
			log.debug("Exit execute");
		}
		return response;
	}

	/**
	 * Load the reservation
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private Reservation loadReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	/**
	 * Returns ReservationSegments
	 * 
	 * @param pnrSegIds
	 * @param reservation
	 * @return
	 */
	private Collection<ReservationSegment> getPNRSegments(Collection<Integer> pnrSegIds, Reservation reservation) {
		Collection<ReservationSegment> filteredSegments = new HashSet<ReservationSegment>();
		ReservationSegment reservationSegment;
		for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
			reservationSegment = reservationSegment2;

			if (pnrSegIds.contains(reservationSegment.getPnrSegId())) {
				filteredSegments.add(reservationSegment);
			}
		}

		return filteredSegments;
	}

	/**
	 * Add External Charges
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @throws ModuleException
	 */
	private void addExternalCharges(Reservation reservation, Map<Integer, PaymentAssembler> pnrPaxIdAndPayments,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		ExternalGenericChargesBL.reflectExternalChgForAPaymentBooking(reservation, pnrPaxIdAndPayments, credentialsDTO, chgTnxGen);
		ExternalSMChargesBL.reflectExternalChgForAnOpenReturnConfirmSegment(reservation, pnrPaxIdAndPayments, credentialsDTO, chgTnxGen);
		ExternalMealChargesBL.reflectExternalChgForAnOpenReturnConfirmSegment(reservation, pnrPaxIdAndPayments, credentialsDTO,
				chgTnxGen);
	}

	/**
	 * Add External Segments
	 * 
	 * @param reservation
	 * @param pnrExtSegments
	 */
	private void addExternalSegments(Reservation reservation, Collection<ExternalPnrSegment> pnrExtSegments) {
		if (pnrExtSegments != null && pnrExtSegments.size() > 0) {
			ExternalPnrSegment reservationExternalSegment;
			for (ExternalPnrSegment externalPnrSegment : pnrExtSegments) {
				reservationExternalSegment = externalPnrSegment;
				reservation.addExternalReservationSegment(reservationExternalSegment);
			}
		}
	}

	/**
	 * Ammend Pnr Seg Ids
	 * 
	 * @param newPnrSegIds
	 * @param colReservationSegment
	 */
	private void trackNewPnrSegIds(Collection<Integer> newPnrSegIds, Collection<ReservationSegment> colReservationSegment) {
		ReservationSegment reservationSegment;

		for (ReservationSegment reservationSegment2 : colReservationSegment) {
			reservationSegment = reservationSegment2;
			newPnrSegIds.add(reservationSegment.getPnrSegId());
		}
	}

	/**
	 * Release open return bucket seats
	 * 
	 * @param reservation
	 * @param pnrSegIds
	 * @throws ModuleException
	 */
	private void releaseOpenReturnBucketSeats(Reservation reservation, Collection<Integer> pnrSegIds) throws ModuleException {
		Collection<SeatsForSegmentDTO> colSeatsForSegmentDTO = new ArrayList<SeatsForSegmentDTO>();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		SeatsForSegmentDTO seatsForSegmentDTO;
		boolean isOnHoldSeat;
		boolean isParent;

		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				isOnHoldSeat = ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(reservationPax.getStatus());
				isParent = ReservationApiUtils.isParentAfterSave(reservationPax);

				for (ReservationPaxFare reservationPaxFare2 : reservationPax.getPnrPaxFares()) {
					reservationPaxFare = reservationPaxFare2;

					for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
						reservationPaxFareSegment = reservationPaxFareSegment2;

						boolean isWaitListedSeat = ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
								.equals(reservationPaxFareSegment.getSegment().getStatus());

						if (pnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
							seatsForSegmentDTO = new SeatsForSegmentDTO(reservationPaxFare.getPnrPaxFareId().intValue(),
									reservationPaxFareSegment.getPnrPaxFareSegId().intValue(),
									reservationPaxFareSegment.getSegment().getFlightSegId().intValue(),
									reservationPaxFareSegment.getBookingCode(), 1, isParent ? 1 : 0, isOnHoldSeat,
									isWaitListedSeat);

							colSeatsForSegmentDTO.add(seatsForSegmentDTO);
						}
					}
				}
			}
		}

		Collection<SegmentSeatDistsDTOForRelease> colSegmentSeatDistsDTOForRelease = CancelSegmentBO
				.getSegmentSeatDistribution(colSeatsForSegmentDTO);
		ReservationBO.releaseSoldOrOnholdFlightInventory(colSegmentSeatDistsDTOForRelease);
	}

	/**
	 * Process In Direct Transfer
	 * 
	 * @param reservation
	 * @param pnrSegIds
	 * @param orderdNewFlgSegIds
	 * @param colOndFareDTO
	 * 
	 * @throws ModuleException
	 */
	private Reservation processInDirectTransfer(Reservation reservation, Collection<Integer> pnrSegIds,
			Collection<Integer> orderdNewFlgSegIds, Collection<OndFareDTO> colOndFareDTO,
			Collection<ReservationSegment> colReservationSegment, CredentialsDTO credentialsDTO) throws ModuleException {
		SegmentBucketBO segmentBucketBO = new SegmentBucketBO(colOndFareDTO);
		ReservationSegment reservationSegment;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<ReservationPaxFareSegment> colReservationReservationPaxFareSegment;
		ReservationPaymentDAO paymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		// TODO
		// Nili 10:55 PM 7/19/2009
		// This is a very heavy in nature due to hibernate and DB trigger constraints.
		// Please improve this in the future
		Collection<Integer> pnrPaxFareSegIds = getPnrPaxFareSegIds(reservation, pnrSegIds);
		if (pnrPaxFareSegIds.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO.removeFareSegChargesAndPayments(pnrPaxFareSegIds);
		}
		Map<Integer, ReservationPaxOndCharge> oldChargeIdNewChargeMapping = new HashMap<Integer, ReservationPaxOndCharge>();
		Collection<ReservationPaxOndPayment> paxOndPaymentToRemove = new ArrayList<ReservationPaxOndPayment>();
		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;
			Set<Long> paxwiseOldPnrPaxChgIds = new HashSet<Long>();
			for (ReservationPaxFare reservationPaxFare2 : reservationPax.getPnrPaxFares()) {
				reservationPaxFare = reservationPaxFare2;

				// Removing the old Pnr Pax Fare Segment(s)
				colReservationReservationPaxFareSegment = new HashSet<ReservationPaxFareSegment>();
				for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
					reservationPaxFareSegment = reservationPaxFareSegment2;

					if (reservationPaxFareSegment.getSegment().getPnrSegId() != null
							&& pnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
						colReservationReservationPaxFareSegment.add(reservationPaxFareSegment);
					}
				}

				if (colReservationReservationPaxFareSegment.size() > 0) {
					// Adding the new charges and removing the old ones.
					// We need to get this done in order to get the trigger working correctly with the new segment
					// charges prorated correctly
					Collection<ReservationPaxOndCharge> colReservationPaxOndCharge = reservationPaxFare.getCharges();
					Collection<ReservationPaxOndCharge> colNewReservationPaxOndCharge = new HashSet<ReservationPaxOndCharge>();
					Collection<ReservationPaxOndCharge> colOldReservationPaxOndCharge = new HashSet<ReservationPaxOndCharge>();
					ReservationPaxOndCharge reservationPaxOndCharge;

					for (ReservationPaxOndCharge reservationPaxOndCharge2 : colReservationPaxOndCharge) {
						reservationPaxOndCharge = reservationPaxOndCharge2;
						colOldReservationPaxOndCharge.add(reservationPaxOndCharge);

						reservationPaxOndCharge = reservationPaxOndCharge.cloneForNew(credentialsDTO, false);
						colNewReservationPaxOndCharge.add(reservationPaxOndCharge);

						paxwiseOldPnrPaxChgIds.add(new Long(reservationPaxOndCharge2.getPnrPaxOndChgId()));
						oldChargeIdNewChargeMapping.put(reservationPaxOndCharge2.getPnrPaxOndChgId(), reservationPaxOndCharge);
					}

					for (ReservationPaxOndCharge oldReservationPaxOndCharge : colOldReservationPaxOndCharge) {
						reservationPaxFare.removeCharge(oldReservationPaxOndCharge);
					}

					for (ReservationPaxOndCharge newReservationPaxOndCharge : colNewReservationPaxOndCharge) {
						reservationPaxFare.addCharge(newReservationPaxOndCharge);
					}

					// Adding the new Pnr Pax Fare Segment(s)
					for (ReservationSegment reservationSegment2 : colReservationSegment) {
						reservationSegment = reservationSegment2;

						reservationPaxFareSegment = new ReservationPaxFareSegment();
						reservationPaxFareSegment.setSegment(reservationSegment);

						if (!ReservationApiUtils.isInfantType(reservationPax)) {
							reservationPaxFareSegment
									.setBookingCode(segmentBucketBO.getBookingCode(reservationSegment.getFlightSegId()));
						}

						reservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
					}
				}
			}
			Map<Long, Collection<ReservationPaxOndPayment>> paxOndPayments = paymentDAO
					.getReservationPaxOndPaymentByOndChargeId(reservationPax.getPnrPaxId(), paxwiseOldPnrPaxChgIds);
			for (Collection<ReservationPaxOndPayment> ondPayments : paxOndPayments.values()) {
				paxOndPaymentToRemove.addAll(ondPayments);
			}
		}

		paymentDAO.removeReservationPaxOndPayments(paxOndPaymentToRemove);

		// Adding the new segments
		int ondGroupId = -1;
		int returnGroupId = -1;
		for (ReservationSegment reservationSegment2 : colReservationSegment) {
			reservationSegment = reservationSegment2;
			// FIXME Temporary hack to get the same ond code should move this logic to assemblers
			if (ondGroupId == -1) {
				ondGroupId = reservationSegment.getOndGroupId();
				returnGroupId = reservationSegment.getReturnOndGroupId();
			} else {
				reservationSegment.setOndGroupId(ondGroupId);
				reservationSegment.setReturnOndGroupId(returnGroupId);
			}
			reservation.addSegment(reservationSegment);
		}

		ReservationProxy.saveReservation(reservation);

		// Save granularity shuffle
		// FIXME move to granularity BOs
		Collection<ReservationPaxOndPayment> newPaxOndPayments = new ArrayList<ReservationPaxOndPayment>();
		for (ReservationPaxOndPayment ondPayment : paxOndPaymentToRemove) {
			ReservationPaxOndCharge newOndCharge = oldChargeIdNewChargeMapping.get(ondPayment.getPnrPaxOndChgId().intValue());

			ReservationPaxOndPayment reservationPaxOndPayment = new ReservationPaxOndPayment();
			reservationPaxOndPayment.setAmount(ondPayment.getAmount());
			reservationPaxOndPayment.setChargeGroupCode(ondPayment.getChargeGroupCode());
			reservationPaxOndPayment.setOriginalPaxOndPaymentId(ondPayment.getOriginalPaxOndPaymentId());
			reservationPaxOndPayment.setPaxOndPaymentId(ondPayment.getPaxOndPaymentId());
			reservationPaxOndPayment.setPaxOndRefundId(ondPayment.getPaxOndRefundId());
			reservationPaxOndPayment.setPaymentTnxId(ondPayment.getPaymentTnxId());
			reservationPaxOndPayment.setPnrPaxId(ondPayment.getPnrPaxId());
			reservationPaxOndPayment.setPnrPaxOndChgId(new Long(newOndCharge.getPnrPaxOndChgId()));
			reservationPaxOndPayment.setRemarks(ondPayment.getRemarks());
			reservationPaxOndPayment.setNominalCode(ondPayment.getNominalCode());
			newPaxOndPayments.add(reservationPaxOndPayment);
		}

		paymentDAO.saveReservationPaxOndPayment(newPaxOndPayments);

		if (pnrPaxFareSegIds.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_CHARGE_DAO.removeFareSegChargesAndPayments(pnrPaxFareSegIds);
			ReservationDAOUtils.DAOInstance.ETicketDAO.removeEtickets(pnrPaxFareSegIds);
			ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO.removePnrPaxFareSegments(pnrPaxFareSegIds);

			// FIXME Fix missing ET for pnr segments - should be refactored to ET generation logic in EticketBO
			List<ReservationPaxFareSegmentETicket> missingETs = new ArrayList<ReservationPaxFareSegmentETicket>();
			for (ReservationPax pax : reservation.getPassengers()) {
				Collection<ReservationPaxFareSegmentETicket> paxSegTickets = ReservationDAOUtils.DAOInstance.ETicketDAO
						.getReservationPaxFareSegmentETickets(pax.getPnrPaxId());
				Set<Integer> existingPnrPaxFareSegIds = new HashSet<Integer>();
				final Map<Integer, Integer> pnrSegIdsToSegSequenceMap = new HashMap<Integer, Integer>();
				for (ReservationPaxFare fares : pax.getPnrPaxFares()) {
					for (ReservationPaxFareSegment segFare : fares.getPaxFareSegments()) {
						if (!pnrPaxFareSegIds.contains(segFare.getPnrPaxFareSegId())) {
							existingPnrPaxFareSegIds.add(segFare.getPnrPaxFareSegId());
							pnrSegIdsToSegSequenceMap.put(segFare.getPnrPaxFareSegId(), segFare.getSegment().getSegmentSeq());
						}
					}
				}
				if (existingPnrPaxFareSegIds.size() > paxSegTickets.size()) {
					List<Integer> eticketSegFareIds = new ArrayList<Integer>();
					Map<Integer, ReservationPaxFareSegmentETicket> ppfsIdETs = new HashMap<Integer, ReservationPaxFareSegmentETicket>();
					for (ReservationPaxFareSegmentETicket paxET : paxSegTickets) {
						eticketSegFareIds.add(paxET.getPnrPaxFareSegId());
						ppfsIdETs.put(paxET.getPnrPaxFareSegId(), paxET);
					}
					Collections.sort(eticketSegFareIds, new Comparator<Integer>() {
						@Override
						public int compare(Integer o1, Integer o2) {
							o1 = pnrSegIdsToSegSequenceMap.get(o1);
							o2 = pnrSegIdsToSegSequenceMap.get(o2);
							return o1.compareTo(o2);
						}
					});

					ReservationPaxFareSegmentETicket lastET = ppfsIdETs.get(eticketSegFareIds.get(eticketSegFareIds.size() - 1));
					int couponNo = lastET.getCouponNo() + 1;
					List<Integer> nonTicketdFareSegIds = new ArrayList<Integer>(existingPnrPaxFareSegIds);
					nonTicketdFareSegIds.removeAll(ppfsIdETs.keySet());
					Collections.sort(nonTicketdFareSegIds, new Comparator<Integer>() {
						@Override
						public int compare(Integer o1, Integer o2) {
							o1 = pnrSegIdsToSegSequenceMap.get(o1);
							o2 = pnrSegIdsToSegSequenceMap.get(o2);
							return o1.compareTo(o2);
						}
					});
					for (Integer paxFareSegId : nonTicketdFareSegIds) {
						if (couponNo == IATAETicketGenerator.NO_OF_COUPONS_PER_TICKET) {
							throw new ModuleException("Cannot accommodate more coupons");
						}
						ReservationPaxFareSegmentETicket ppfsET = new ReservationPaxFareSegmentETicket();
						ppfsET.setCouponNo(couponNo++);
						ppfsET.setPnrPaxFareSegId(paxFareSegId);
						ppfsET.setReservationPaxETicket(lastET.getReservationPaxETicket());
						ppfsET.setStatus(EticketStatus.OPEN.code());
						missingETs.add(ppfsET);
					}
				}
			}
			if (missingETs.size() > 0) {
				ReservationDAOUtils.DAOInstance.ETicketDAO.saveOrUpdate(missingETs);
			}
		}

		reservation = loadReservation(reservation.getPnr());

		Collection<ReservationSegment> colRemoveReservationSegment = new ArrayList<ReservationSegment>();
		for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
			reservationSegment = reservationSegment2;

			if (pnrSegIds.contains(reservationSegment.getPnrSegId())) {
				colRemoveReservationSegment.add(reservationSegment);
			}
		}

		for (ReservationSegment rmvReservationSegment : colRemoveReservationSegment) {
			reservation.removeSegment(rmvReservationSegment);
		}

		return reservation;
	}

	private Collection<Integer> getPnrPaxFareSegIds(Reservation reservation, Collection<Integer> pnrSegIds) {
		Collection<Integer> colPnrPaxFareSegIds = new HashSet<Integer>();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			for (ReservationPaxFare reservationPaxFare2 : reservationPax.getPnrPaxFares()) {
				reservationPaxFare = reservationPaxFare2;

				for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
					reservationPaxFareSegment = reservationPaxFareSegment2;

					if (reservationPaxFareSegment.getSegment().getPnrSegId() != null
							&& pnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
						colPnrPaxFareSegIds.add(reservationPaxFareSegment.getPnrPaxFareSegId());
					}
				}
			}
		}

		return colPnrPaxFareSegIds;
	}

	/**
	 * Process Direct Transfer of Segments
	 * 
	 * @param reservation
	 * @param pnrSegIds
	 * @param orderedNewFlgSegIds
	 * @param colOndFareDTO
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private Reservation processDirectTransfer(Reservation reservation, Collection<Integer> pnrSegIds,
			Collection<Integer> orderedNewFlgSegIds, Collection<OndFareDTO> colOndFareDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		ReservationSegment reservationSegment;
		int i = 0;

		for (ReservationSegment reservationSegment2 : reservation.getSegments()) {
			reservationSegment = reservationSegment2;

			if (pnrSegIds.contains(reservationSegment.getPnrSegId())) {
				reservationSegment.setFlightSegId(new Integer((orderedNewFlgSegIds.toArray())[i].toString()));
				reservationSegment.setOpenReturnConfirmBeforeZulu(null);
				i++;
			}
		}

		SegmentBucketBO segmentBucketBO = new SegmentBucketBO(colOndFareDTO);
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		// We need to set the new booking classes
		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				for (ReservationPaxFare reservationPaxFare2 : reservationPax.getPnrPaxFares()) {
					reservationPaxFare = reservationPaxFare2;

					for (ReservationPaxFareSegment reservationPaxFareSegment2 : reservationPaxFare.getPaxFareSegments()) {
						reservationPaxFareSegment = reservationPaxFareSegment2;

						if (pnrSegIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
							reservationPaxFareSegment.setBookingCode(
									segmentBucketBO.getBookingCode(reservationPaxFareSegment.getSegment().getFlightSegId()));
						}
					}
				}
			}
		}

		return reservation;
	}

	/**
	 * Process passenger seats
	 * 
	 * @param blockIds
	 * @param reservation
	 * @throws ModuleException
	 */
	private void processPassengerSeats(Collection<TempSegBcAlloc> blockIds, Reservation reservation,
			Map<Integer, String> flightSeatIdsAndPaxTypes, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException {
		// If the reservation is confirmed
		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			// If any newly confirm passengers exist
			if (confirmPnrPaxIds.size() > 0) {
				// Move the blocked seats to on hold state
				ReservationBO.moveBlockSeats(blockIds, new Integer(0), new Integer(0), flightSeatIdsAndPaxTypes, flightMealIds,
						flightBaggageIds);

				// Move newly added seats and and existing onhold seats to sold state
				ReservationBO.moveOnholdSeatsToConfirm(reservation.getPassengers(), confirmPnrPaxIds, null);
			} else {
				// Move the seats to the sold state
				ReservationBO.moveBlockedSeatsToSold(blockIds, flightSeatIdsAndPaxTypes, flightMealIds, flightBaggageIds);
			}
		}
		// If the reservation is on hold
		else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			// Move the seats to the on hold state
			ReservationBO.moveBlockSeats(blockIds, new Integer(0), new Integer(0), flightSeatIdsAndPaxTypes, flightMealIds,
					flightBaggageIds);
		}
	}

	/**
	 * Compose the audit
	 * 
	 * @param pnr
	 * @param newPnrSegIds
	 * @param credentialsDTO
	 * @return
	 */
	private ReservationAudit composeAudit(String pnr, Collection<Integer> newPnrSegIds, CredentialsDTO credentialsDTO) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.OPEN_RETURN_CONFIRM.getCode());
		reservationAudit.setUserNote(null);

		StringBuilder segmentInfomation = new StringBuilder();
		segmentInfomation.append(
				" Confirmed Open Return Segment(s) : [" + ReservationCoreUtils.getSegmentInformation(newPnrSegIds) + "] ");

		// Setting the segment information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OpenReturnConfirmation.SEGMENT_INFORMATION,
				segmentInfomation.toString());

		return reservationAudit;
	}

	/**
	 * Check confirm open return segments constraints
	 * 
	 * @param reservation
	 * @param pnrSegIds
	 * @param version
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkOpenReturnConfirmSegmentsConstraints(Reservation reservation, Collection<Integer> pnrSegIds,
			Map pnrPaxIdAndPayments, Collection<OndFareDTO> colOndFareDTO, Long version) throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Checking if the reservation is already cancelled
		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			throw new ModuleException("airreservations.arg.invalidPnrCancel");
		}

		Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		// Checking cancelling a already cancelled segment
		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itReservationSegmentDTO.next();

			if (pnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
					throw new ModuleException("airreservations.arg.invalidSegCancel");
				}

				if (!reservationSegmentDTO.isOpenReturnSegment()) {
					throw new ModuleException("airreservations.confirm.only.caters.for.open.return.segments");
				}
			}
		}

		itReservationSegmentDTO = reservation.getSegmentsView().iterator();
		Collection<Integer> ondGroupIds = new HashSet<Integer>();

		// Checking cancelling a already cancelled segment
		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itReservationSegmentDTO.next();

			if (pnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
				ondGroupIds.add(reservationSegmentDTO.getFareGroupId());
			}
		}

		// The new Adding segments should belongs to one ond only
		if (ondGroupIds.size() == 1) {
			Integer ondGroupId = BeanUtils.getFirstElement(ondGroupIds);
			itReservationSegmentDTO = reservation.getSegmentsView().iterator();
			Collection<Integer> ondGroupWisePnrSegIds = new HashSet<Integer>();

			// Checking cancelling a already cancelled segment
			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = itReservationSegmentDTO.next();

				if (ondGroupId.equals(reservationSegmentDTO.getFareGroupId())) {
					ondGroupWisePnrSegIds.add(reservationSegmentDTO.getPnrSegId());
				}
			}

			if (ondGroupWisePnrSegIds.size() == pnrSegIds.size() && ondGroupWisePnrSegIds.containsAll(pnrSegIds)) {
			} else {
				throw new ModuleException("airreservations.openreturn.invalid.open.return.segments");
			}
		} else {
			throw new ModuleException("airreservations.openreturn.invalid.open.return.segments");
		}

		// Check if it's valid payments or not
		ReservationCoreUtils.checkValidPaxPaymentsForReservation(reservation, pnrPaxIdAndPayments, null);

		// Check Restricted Segment constraints
		ValidationUtils.checkRestrictedSegmentConstraints(colOndFareDTO, reservation.getStatus(), pnrPaymentTypes);
	}

	/**
	 * Sets the arguments
	 * 
	 * @param pnrPaymentTypes
	 * @param credentialsDTO
	 */
	private void setArguments(Integer pnrPaymentTypes, CredentialsDTO credentialsDTO) {
		this.pnrPaymentTypes = pnrPaymentTypes;
		this.credentialsDTO = credentialsDTO;
	}

	/**
	 * Capture ownership
	 * 
	 * @param reservation
	 */
	private void captureOwnerShip(Reservation reservation) {
		// This is force confirmation therefore
		// If IBE user(s) exist should change the channel and the agent code
		if (AppSysParamsUtil.isOwnerAgentTransferEnabled()) {
			reservation.getAdminInfo().setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
			reservation.getAdminInfo().setOwnerAgentCode(credentialsDTO.getAgentCode());
		}
	}

	/**
	 * Update Cancel States
	 * 
	 * @param reservation
	 */
	private void updateCancelStates(Reservation reservation) {
		Collection<Integer> passengerIds = new ArrayList<Integer>();
		ReservationPax reservationPax;

		for (ReservationPax reservationPax2 : reservation.getPassengers()) {
			reservationPax = reservationPax2;

			// Catching the parent and adult ids
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				passengerIds.add(reservationPax.getPnrPaxId());
			}
		}

		Map<Integer, Collection<ReservationTnx>> pnrPaxIdsAndColReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPaymentsAndRefunds(passengerIds);
		Collection<ReservationTnx> colAllReservationTnx = new ArrayList<ReservationTnx>();
		Collection<ReservationTnx> colReservationTnx;
		ReservationTnx reservationTnx;
		BigDecimal paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Capturing all transactions
		for (Collection<ReservationTnx> collection : pnrPaxIdsAndColReservationTnx.values()) {
			colReservationTnx = collection;
			colAllReservationTnx.addAll(colReservationTnx);
		}

		// Find the exact payment amount
		for (ReservationTnx reservationTnx2 : colAllReservationTnx) {
			reservationTnx = reservationTnx2;
			paymentAmount = AccelAeroCalculator.add(paymentAmount, reservationTnx.getAmount());
		}

		// Reservation already made payments
		if (paymentAmount.doubleValue() < 0) {
			for (ReservationPax reservationPax2 : reservation.getPassengers()) {
				reservationPax = reservationPax2;
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED);
			}
			reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);

			// Changing the ownership
			this.captureOwnerShip(reservation);
		} else {
			for (ReservationPax reservationPax2 : reservation.getPassengers()) {
				reservationPax = reservationPax2;
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);
			}
			reservation.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
		}
	}

	/**
	 * Update the general states
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @throws ModuleException
	 */
	private void updateGeneralStates(Reservation reservation, Map<Integer, IPayment> pnrPaxIdAndPayments) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		int confirmPaxCount = 0;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(reservationPax.getStatus())) {
				confirmPnrPaxIds.add(reservationPax.getPnrPaxId());
			}

			// Setting the passenger states
			if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
							.equals(pnrPaymentTypes)) {
				// Confirming the passenger
				reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
			} else {
				// Returns the passenger confirm status
				boolean status = ReservationApiUtils.isPassengerConfirmedForAModifyBooking(reservationPax, pnrPaxIdAndPayments,
						false, false);
				if (status) {
					confirmPaxCount++;
					reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
				} else {
					if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT.equals(pnrPaymentTypes)) {
						throw new ModuleException("airreservations.arg.paidAmountError");
					} else {
						reservationPax.setStatus(ReservationPaxStatus.ON_HOLD);
					}
				}
			}
		}

		// Confirming the reservation
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {
			reservation.setStatus(ReservationStatus.CONFIRMED);
		} else {
			// If confirmed making the reservation status to confirm
			if (confirmPaxCount == reservation.getPassengers().size()) {
				reservation.setStatus(ReservationStatus.CONFIRMED);
			}
			// If it's not totally confirmed make the reservation on hold
			else {
				reservation.setStatus(ReservationStatus.ON_HOLD);
			}
		}
	}

	/**
	 * Check and process force mode
	 * 
	 * @param reservation
	 * @param triggerPnrForceConfirm
	 */
	private void checkForceMode(Reservation reservation) {
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)) {
			// Capture the ownership
			this.captureOwnerShip(reservation);
		}
	}

	/**
	 * Returns flight segment map to the pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private Map<Integer, LinkedList<Integer>> getFlightSegmentMap(Collection<ReservationSegment> pnrSegments) {
		Map<Integer, LinkedList<Integer>> flgtSegmentMap = new HashMap<Integer, LinkedList<Integer>>();

		ReservationSegment reservationSegment;
		LinkedList<Integer> pnrSegIds;
		for (ReservationSegment reservationSegment2 : pnrSegments) {
			reservationSegment = reservationSegment2;

			pnrSegIds = flgtSegmentMap.get(reservationSegment.getFlightSegId());

			if (pnrSegIds == null) {
				pnrSegIds = new LinkedList<Integer>();
				pnrSegIds.add(reservationSegment.getPnrSegId());

				flgtSegmentMap.put(reservationSegment.getFlightSegId(), pnrSegIds);
			} else {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			}
		}

		return flgtSegmentMap;
	}

	/**
	 * Over ride the zulu release time stamp
	 * 
	 * @param reservation
	 * @param zuluReleaseTimeStamp
	 */
	private void overrideZuluReleaseTimeStamp(Reservation reservation, Date zuluReleaseTimeStamp) {
		if (zuluReleaseTimeStamp != null) {
			Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
			ReservationPax reservationPax;

			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.setZuluReleaseTimeStamp(zuluReleaseTimeStamp);
			}
		}
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param pnrSegIds
	 * @param version
	 * @param colOndFareDTO
	 * @param segObjectsMap
	 * @param colReservationSegment
	 * @param orderdNewFlgSegIds
	 * @param blockSeatIds
	 * @param pnrPaxIdAndPayments
	 * @param pnrPaymentTypes
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(String pnr, Collection pnrSegIds, Long version, Collection colOndFareDTO, Map segObjectsMap,
			Collection colReservationSegment, Collection orderdNewFlgSegIds, Collection blockSeatIds, Map pnrPaxIdAndPayments,
			Integer pnrPaymentTypes, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || pnrSegIds == null || version == null || colOndFareDTO == null || segObjectsMap == null
				|| colReservationSegment == null || orderdNewFlgSegIds == null || orderdNewFlgSegIds.size() == 0
				|| blockSeatIds == null || pnrPaxIdAndPayments == null || pnrPaymentTypes == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
