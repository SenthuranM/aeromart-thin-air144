package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.jdbc.core.JdbcTemplate;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutoCheckinExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckinSeat;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAutomaticCheckinDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @author Mohamed Rizwan
 *
 * @isa.module.dao-impl dao-name="ReservationAutomaticCheckinDAO"
 */
public class ReservationAutomaticCheckinDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationAutomaticCheckinDAO {

	@Override
	public void saveOrUpdate(Collection<ReservationPaxSegAutoCheckin> automaticCheckins) {
		super.hibernateSaveOrUpdateAll(automaticCheckins);

	}

	@Override
	public void cancelAutomaticCheckin(Collection<Integer> pnrPaxSegAutoCheckinIds) {
		StringBuilder hql = new StringBuilder("UPDATE ReservationPaxSegAutoCheckin SET status=:status ");
		hql.append("WHERE pnrPaxSegAutoCheckinId in (:autoChknIds)");

		Query qry = getSession().createQuery(hql.toString());
		qry.setParameter("status", AirinventoryCustomConstants.AutoCheckinConstants.INACTIVE);
		qry.setParameterList("autoChknIds", pnrPaxSegAutoCheckinIds);
		qry.executeUpdate();

	}

	@Override
	public Collection<PaxAutomaticCheckinTO> getReservationAutomaticCheckinsByPaxId(Collection<Integer> pnrPaxIds) {
		Collection<PaxAutomaticCheckinTO> colAutomaticCheckins = new ArrayList<PaxAutomaticCheckinTO>();
		String hql;
		Query query;
		if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
			Collection<ReservationPaxSegAutoCheckin>  reservationPaxSegAutoCheckins;
			Collection<ReservationPaxSegAutoCheckinSeat> reservationPaxSegAutoCheckinSeats;
			
			hql = "from ReservationPaxSegAutoCheckinSeat  rasc where rasc.reservationPaxSegAutoCheckin.status = :status  and  rasc.reservationPaxSegAutoCheckin.pnrPaxId IN :pnrpaxIds Order by rasc.reservationPaxSegAutoCheckin.pnrSegId";
			query = getSession().createQuery(hql.toString());
			query.setParameter("status", AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE);
			query.setParameterList("pnrpaxIds", pnrPaxIds);
			reservationPaxSegAutoCheckinSeats = query.list();
			hql = "from ReservationPaxSegAutoCheckin  where status = :status  and  pnrPaxId IN :pnrpaxIds ";
			if (reservationPaxSegAutoCheckinSeats != null && reservationPaxSegAutoCheckinSeats.size() > 0) {
				hql	+= " and pnrPaxSegAutoCheckinId not In :pnrPaxSegAutoCheckinIds ";
			}
			hql	+= " Order by pnrSegId";
			query = getSession().createQuery(hql.toString());
			if (reservationPaxSegAutoCheckinSeats != null && reservationPaxSegAutoCheckinSeats.size() > 0) {
				query.setParameterList("pnrPaxSegAutoCheckinIds", getPnrPaxSegAutoCheckinIds(reservationPaxSegAutoCheckinSeats));
			}
			query.setParameter("status", AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE);
			query.setParameterList("pnrpaxIds", pnrPaxIds);
			reservationPaxSegAutoCheckins = query.list();
			colAutomaticCheckins = collectAutoCheckinPaxsbyId(reservationPaxSegAutoCheckins,reservationPaxSegAutoCheckinSeats);
		}
		return colAutomaticCheckins;
	}

	private Collection<Integer> getPnrPaxSegAutoCheckinIds(
			Collection<ReservationPaxSegAutoCheckinSeat> reservationPaxSegAutoCheckinSeats) {
		Collection<Integer> pnrPaxSegAutoCheckInIds = new ArrayList<Integer>();
		for (ReservationPaxSegAutoCheckinSeat reservationPaxSegAutoCheckinSeat : reservationPaxSegAutoCheckinSeats) {
			if (reservationPaxSegAutoCheckinSeat.getReservationPaxSegAutoCheckin() != null) {
				pnrPaxSegAutoCheckInIds.add(reservationPaxSegAutoCheckinSeat.getReservationPaxSegAutoCheckin()
						.getPnrPaxSegAutoCheckinId());
			}
		}
		return pnrPaxSegAutoCheckInIds;
	}


	private Collection<PaxAutomaticCheckinTO> collectAutoCheckinPaxsbyId(
			Collection<ReservationPaxSegAutoCheckin> colAutomaticCheckins,
			Collection<ReservationPaxSegAutoCheckinSeat> reservationPaxSegAutoCheckinSeats) {
		Collection<PaxAutomaticCheckinTO> paxAutomaticCheckinTOs = new ArrayList<PaxAutomaticCheckinTO>();
		PaxAutomaticCheckinTO autoCheckinPax = null;
		if (colAutomaticCheckins != null && colAutomaticCheckins.size() > 0) {
			for (ReservationPaxSegAutoCheckin colAutomaticCheckin : colAutomaticCheckins) {
				autoCheckinPax = setAutoCheckinPaxTo(colAutomaticCheckin, AirinventoryCustomConstants.AutoCheckinConstants.SIT_TOGETHER);
				paxAutomaticCheckinTOs.add(autoCheckinPax);
			}
		}
		if (reservationPaxSegAutoCheckinSeats != null && reservationPaxSegAutoCheckinSeats.size() > 0) {
			for (ReservationPaxSegAutoCheckinSeat reservationPaxSegAutoCheckinSeat : reservationPaxSegAutoCheckinSeats) {
				autoCheckinPax = setAutoCheckinPaxTo(reservationPaxSegAutoCheckinSeat.getReservationPaxSegAutoCheckin(),
						reservationPaxSegAutoCheckinSeat.getSeatTypePreference());
				paxAutomaticCheckinTOs.add(autoCheckinPax);
			}
		}
		return paxAutomaticCheckinTOs;
	}

	private PaxAutomaticCheckinTO setAutoCheckinPaxTo(ReservationPaxSegAutoCheckin colAutomaticCheckin, String seatPreference) {
		PaxAutomaticCheckinTO autoCheckinPax = new PaxAutomaticCheckinTO();
		autoCheckinPax.setAmount(colAutomaticCheckin.getAmount());
		autoCheckinPax.setAutoCheckinTemplateId(colAutomaticCheckin.getAutoCheckinTemplateId());
		autoCheckinPax.setRequestTimeStamp(colAutomaticCheckin.getRequestTimeStamp());
		autoCheckinPax.setStatus(colAutomaticCheckin.getStatus());
		autoCheckinPax.setDcsCheckinStatus(colAutomaticCheckin.getDcsCheckinStatus());
		autoCheckinPax.setDcsResponseText(colAutomaticCheckin.getDcsResponseText());
		if (seatPreference != null) {
			autoCheckinPax.setSeatTypePreference(seatPreference);
		}
		autoCheckinPax.setPnrPaxId(colAutomaticCheckin.getPnrPaxId());
		autoCheckinPax.setPnrSegId(colAutomaticCheckin.getPnrSegId());
		autoCheckinPax.setPpfId(colAutomaticCheckin.getPpfId());
		autoCheckinPax.setFlightAmSeatId(colAutomaticCheckin.getFlightAmSeatId());
		if (colAutomaticCheckin.getFlightAmSeatId() != null) {
			autoCheckinPax.setSeatCode(getSeatCode(colAutomaticCheckin.getFlightAmSeatId()));
		}
		autoCheckinPax.setSalesChannelCode(colAutomaticCheckin.getSalesChannelCode());
		autoCheckinPax.setUserId(colAutomaticCheckin.getUserId());
		autoCheckinPax.setCustomerId(colAutomaticCheckin.getCustomerId());
		autoCheckinPax.setEmail(colAutomaticCheckin.getEmail());
		autoCheckinPax.setPnrPaxSegAutoCheckinId(colAutomaticCheckin.getPnrPaxSegAutoCheckinId());
		autoCheckinPax.setPkey(colAutomaticCheckin.getPnrPaxSegAutoCheckinId());
		AutoCheckinExternalChgDTO autoCheckinExternalChgDTO = new AutoCheckinExternalChgDTO();
		autoCheckinExternalChgDTO.setAmount(colAutomaticCheckin.getAmount());
		autoCheckinExternalChgDTO.setAutoCheckinId(colAutomaticCheckin.getAutoCheckinTemplateId());
		autoCheckinPax.setChgDTO(autoCheckinExternalChgDTO);
		return autoCheckinPax;
	}
	
	private String getSeatCode(Integer flightAmSeatId) {
		Object[] aciParams = {AirinventoryCustomConstants.FlightSeatStatuses.RESERVED, flightAmSeatId };
		StringBuilder seatCodeSql = new StringBuilder();

		seatCodeSql.append(" SELECT d.seat_code");
		seatCodeSql.append(" FROM sm_t_flight_am_seat b, sm_t_am_seat_charge c, sm_t_aircraft_model_seats d ");
		seatCodeSql.append(" WHERE ");
		seatCodeSql.append(" b.ams_charge_id = c.ams_charge_id ");
		seatCodeSql.append(" AND c.am_seat_id = d.am_seat_id ");
		seatCodeSql.append(" AND b.status = ? ");
		seatCodeSql.append(" AND b.flight_am_seat_id = ?");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String seatCode = (String) template.queryForObject(seatCodeSql.toString(), String.class, aciParams);
		
		return seatCode;
	}

	/**
	 * @param fieldValue
	 * @return
	 */
	public int bigDecimalToInt(Object fieldValue) {
		int intVal = 0;
		if (fieldValue != null) {
			BigDecimal bDVal = (BigDecimal) fieldValue;
			intVal = bDVal.intValueExact();
		}
		return intVal;
	}
	
	@Override
	public Collection<ReservationPaxSegAutoCheckin> getReservationAutomaticCheckins(Collection<Integer> pnrPaxSegAutoCheckinIds) {
		String autocheckinhql;
		Query autocheckinQuery;
		Collection<ReservationPaxSegAutoCheckin> reservationPaxSegAutoCheckins = null;
		Collection<ReservationPaxSegAutoCheckinSeat> reservationPaxSegAutoCheckinSeats = null;
		if (pnrPaxSegAutoCheckinIds != null && pnrPaxSegAutoCheckinIds.size() > 0) {
			autocheckinhql = "from ReservationPaxSegAutoCheckinSeat  rasc where rasc.reservationPaxSegAutoCheckin.status = :status "
					+ " and  rasc.reservationPaxSegAutoCheckin.pnrPaxSegAutoCheckinId IN :pnrPaxSegAutoCheckinIds "
					+ " Order by rasc.reservationPaxSegAutoCheckin.pnrSegId ";
			autocheckinQuery = getSession().createQuery(autocheckinhql.toString());
			autocheckinQuery.setParameter("status", AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE);
			autocheckinQuery.setParameterList("pnrPaxSegAutoCheckinIds", pnrPaxSegAutoCheckinIds);
			reservationPaxSegAutoCheckinSeats = autocheckinQuery.list();
			pnrPaxSegAutoCheckinIds = getUniquePnrPaxIds(getPnrPaxSegAutoCheckinIds(reservationPaxSegAutoCheckinSeats),
					pnrPaxSegAutoCheckinIds);
			if (pnrPaxSegAutoCheckinIds != null && pnrPaxSegAutoCheckinIds.size() > 0) {
				autocheckinhql = "from ReservationPaxSegAutoCheckin  where status = :status "
						+ "and pnrPaxSegAutoCheckinId In :pnrPaxSegAutoCheckinIds Order by pnrSegId ";
				autocheckinQuery = getSession().createQuery(autocheckinhql.toString());
				autocheckinQuery.setParameter("status", AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE);
				autocheckinQuery.setParameterList("pnrPaxSegAutoCheckinIds", pnrPaxSegAutoCheckinIds);
				reservationPaxSegAutoCheckins = autocheckinQuery.list();
			}
		}
		return collectAutoCheckin(reservationPaxSegAutoCheckinSeats, reservationPaxSegAutoCheckins);
	}
	
	

	private Collection<Integer> getUniquePnrPaxIds(Collection<Integer> pnrPaxSegAutoCheckinIds,Collection<Integer> parentPnrPaxSegAutoCheckinIds) {
		Collection<Integer> uniquePnrPaxSegAutoCheckinIdList = new ArrayList<Integer>();
		for (Integer pnrPaxSegAutoCheckinId : parentPnrPaxSegAutoCheckinIds) {
		    if (!pnrPaxSegAutoCheckinIds.contains(pnrPaxSegAutoCheckinId)) {
		    	uniquePnrPaxSegAutoCheckinIdList.add(pnrPaxSegAutoCheckinId);
		    } 
		}
		return uniquePnrPaxSegAutoCheckinIdList;
	}

	private Collection<ReservationPaxSegAutoCheckin> collectAutoCheckin(
			Collection<ReservationPaxSegAutoCheckinSeat> reservationPaxSegAutoCheckinSeats,
			Collection<ReservationPaxSegAutoCheckin> resPaxSegAutoCheckins) {
		Collection<ReservationPaxSegAutoCheckin> reservationPaxSegAutoCheckins = new ArrayList<ReservationPaxSegAutoCheckin>();
		if (reservationPaxSegAutoCheckinSeats != null && reservationPaxSegAutoCheckinSeats.size() > 0) {
			for (ReservationPaxSegAutoCheckinSeat reservationPaxSegAutoCheckinSeat : reservationPaxSegAutoCheckinSeats) {
				ReservationPaxSegAutoCheckin autoCheckin = reservationPaxSegAutoCheckinSeat.getReservationPaxSegAutoCheckin();
				reservationPaxSegAutoCheckinSeat.setReservationPaxSegAutoCheckin(autoCheckin);
				autoCheckin.setReservationPaxSegAutoCheckinSeat(reservationPaxSegAutoCheckinSeat);
				reservationPaxSegAutoCheckins.add(autoCheckin);
			}
		}
		if (resPaxSegAutoCheckins != null && resPaxSegAutoCheckins.size() > 0) {
			reservationPaxSegAutoCheckins.addAll(resPaxSegAutoCheckins);
		}
		return reservationPaxSegAutoCheckins;
	}

	@Override
	public Collection<ReservationPaxSegAutoCheckin> getReservationPaxAutoCheckinList(String pnr) {
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT AUTOCHECKIN.PPAC_ID," + " AUTOCHECKIN.PNR_PAX_ID as PAX_PNR,  "
				+ " AUTOCHECKIN.PNR_SEG_ID as PAX_SEG, " + " AUTOCHECKIN.AMOUNT as CHARGE_AMOUNT,  "
				+ " AUTOCHECKIN.AUTOMATIC_CHECKIN_TEMPLATE_ID as AUTOMATIC_CHECKIN_ID     "
				+ " FROM T_PNR_PAX_SEG_AUTO_CHECKIN AUTOCHECKIN, T_PNR_PASSENGER PPS "
				+ " WHERE PPS.PNR_PAX_ID = AUTOCHECKIN.PNR_PAX_ID    " + " and AUTOCHECKIN.STATUS= ?  " + " and PPS.PNR =  ? ";

		Object[] param = { AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE, pnr };

		Collection<ReservationPaxSegAutoCheckin> pnrAutocheckinList = new ArrayList<ReservationPaxSegAutoCheckin>();
		Collection<Map<String, Object>> rsList = template.queryForList(sql.toString(), param);
		rsList.stream().forEach(pnrAutocheckin -> mapReservationPaxSegAutoCheckin(pnrAutocheckinList, pnrAutocheckin));
		return pnrAutocheckinList;

	}

	private Collection<ReservationPaxSegAutoCheckin> mapReservationPaxSegAutoCheckin(
			Collection<ReservationPaxSegAutoCheckin> pnrAutocheckinList, Map<String, Object> pnrAutocheckin) {
		ReservationPaxSegAutoCheckin passengerAutocheckin = new ReservationPaxSegAutoCheckin();
		if (pnrAutocheckin.get("PPAC_ID") != null) {
			passengerAutocheckin.setPnrPaxSegAutoCheckinId(Integer.valueOf(String.valueOf(pnrAutocheckin.get("PPAC_ID"))));
		}
		if (pnrAutocheckin.get("PNR_PAX") != null) {
			passengerAutocheckin.setPnrPaxId(Integer.valueOf(String.valueOf(pnrAutocheckin.get("PNR_PAX"))));
		}
		if (pnrAutocheckin.get("PNR_SEG") != null) {
			passengerAutocheckin.setPnrSegId(Integer.valueOf(String.valueOf(pnrAutocheckin.get("PNR_SEG"))));
		}
		if (pnrAutocheckin.get("CHARGE_AMOUNT") != null) {
			passengerAutocheckin
					.setAmount(BigDecimal.valueOf(Double.valueOf(String.valueOf(pnrAutocheckin.get("CHARGE_AMOUNT")))));
		}
		if (pnrAutocheckin.get("AUTOMATIC_CHECKIN_ID") != null) {
			passengerAutocheckin.setAutoCheckinTemplateId(Integer.valueOf(String.valueOf(pnrAutocheckin
					.get("AUTOMATIC_CHECKIN_ID"))));
		}
		pnrAutocheckinList.add(passengerAutocheckin);
		return pnrAutocheckinList;
	}

	@Override
	public int updateReservationPaxSegAutoCheckin(Collection<Integer> pnrpaxIds) throws ModuleException {
		int result = 0;
		String hql = "update ReservationPaxSegAutoCheckin set status = :status" + " where pnrPaxId in ( :pnrpaxIds )";
		Query query = getSession().createQuery(hql.toString());
		query.setParameter("status", AirinventoryCustomConstants.AutoCheckinConstants.INACTIVE);
		query.setParameterList("pnrpaxIds", pnrpaxIds);
		result = query.executeUpdate();
		return result;

	}
	
	@Override
	public String getSeatPreferenceByFlightAmSeatId(Integer flightAmSeatId) {
		StringBuilder seatPreferenceSql = new StringBuilder();
		Object[] aciParams = {flightAmSeatId};
		seatPreferenceSql.append("select ams.SEAT_LOCATION_TYPE from SM_T_FLIGHT_AM_SEAT ");
		seatPreferenceSql.append("fas JOIN SM_T_AIRCRAFT_MODEL_SEATS ams ON ams.AM_SEAT_ID=fas.AM_SEAT_ID where fas.FLIGHT_AM_SEAT_ID=?");
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String seat = (String) template.queryForObject(seatPreferenceSql.toString(), String.class, aciParams);
		return seat;
	}

}
