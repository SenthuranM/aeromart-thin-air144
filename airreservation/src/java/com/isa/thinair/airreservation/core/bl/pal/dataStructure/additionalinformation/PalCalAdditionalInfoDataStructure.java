package com.isa.thinair.airreservation.core.bl.pal.dataStructure.additionalinformation;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.InboundConnectionInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.InfantInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.OnwardConnectionsInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.PalCalValidOnwardConnectionsInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.PalCalValidSsrInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation.WaitListInformation;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.AdditionalInformationBaseStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.BaseAdditionalInformation;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PalCalAdditionalInfoDataStructure <K extends BasePassengerCollection, T extends BaseDataContext>
extends AdditionalInformationBaseStructure{

	@Override
	public void buildSsrInfomration() throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildSsrInformation = new PalCalValidSsrInformation();
		buildSsrInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);		
	}

	@Override
	public void buildWaitlistPassengerInformation() throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildWaitlistPassengerInformation = new WaitListInformation();
		buildWaitlistPassengerInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	@Override
	public void buildInboundConnectionsInformation(String departureAirportCode, Integer flightId) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildInboundConnectionInformation = new InboundConnectionInformation(
				departureAirportCode, flightId);
		buildInboundConnectionInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	@Override
	public void buildOnwardConnectionsInformation(String departureAirportCode, Integer flightId) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildOnwardConnectionInformation = new PalCalValidOnwardConnectionsInformation(
				departureAirportCode, flightId);
		buildOnwardConnectionInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
	}

	@Override
	public void buildInfantsInformation(Integer flightId) throws ModuleException {
		BaseAdditionalInformation<List<PassengerInformation>> buildInfantInformation = new InfantInformation(flightId);
		buildInfantInformation
				.populateAdditionalInformation(passengerDetailDtoCollection);
		
	}

	@Override
	public void buildSeatInfomration() throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void buildMealInfomration() throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void buildBaggageInfomration(String departureAirportCode) throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected List<PassengerInformation> getPassengerDetailsDTOBy(List<DestinationFare> destinationDTOs) {
		List<PassengerInformation> passengerInformations = new ArrayList<PassengerInformation>();
		for (DestinationFare destinationDTO : destinationDTOs) {
			if (destinationDTO.getAddPassengers() != null) {
				populatePassengers(passengerInformations, destinationDTO.getAddPassengers());
			}
			if (destinationDTO.getChangePassengers() != null) {
				populatePassengers(passengerInformations, destinationDTO.getChangePassengers());
			}
			if (destinationDTO.getDeletePassengers() != null) {
				populatePassengers(passengerInformations, destinationDTO.getDeletePassengers());
			}
		}
		return passengerInformations;
	}
}
