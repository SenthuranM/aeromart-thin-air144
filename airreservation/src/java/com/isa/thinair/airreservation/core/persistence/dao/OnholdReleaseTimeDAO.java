package com.isa.thinair.airreservation.core.persistence.dao;

import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.commons.api.dto.ConfigOnholdTimeLimitsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author asiri
 * 
 */
public interface OnholdReleaseTimeDAO {

	public void saveOrUpdateOnholdReleaseTimeConfig(OnholdReleaseTime onholdReleaseTime) throws ModuleException;

	public void deleteOnholdReleaseTimeConfig(int releaseTimeId) throws ModuleException;

	public boolean isOhdRelCfgLegal(OnholdReleaseTime onholdRelTime) throws ModuleException;

	public OnholdReleaseTime getOnholdReleaseTime(int releaseTimeId) throws ModuleException;

	public Page getOnholdReleaseTimes(ConfigOnholdTimeLimitsSearchDTO configOnholdTimeLimitsSearchDTO, int start, int recSize)
			throws ModuleException;

}
