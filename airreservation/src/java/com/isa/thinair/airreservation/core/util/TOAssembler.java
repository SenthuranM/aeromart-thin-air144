/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCreditPromotion;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelSegmentDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;

/**
 * Data Transfer Object Assembler
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class TOAssembler {

	/**
	 * Componse an Ond charge
	 * 
	 * @param amount
	 * @param fareId
	 * @param chargeRateId
	 * @param chargeGroupCode
	 * @param credentialsDTO
	 * @param isRefundableOperation
	 * @param discValuePercentage
	 * @param discount
	 * @param adjustment
	 * @param paxOndChargeDTO
	 * @param transactionSeq
	 *            TODO
	 * @return
	 */
	public static ReservationPaxOndCharge createReservationPaxOndCharge(BigDecimal amount, Integer fareId, Integer chargeRateId,
			String chargeGroupCode, CredentialsDTO credentialsDTO, boolean isRefundableOperation, double discValuePercentage,
			BigDecimal discount, BigDecimal adjustment, PaxOndChargeDTO paxOndChargeDTO, Integer transactionSeq) {
		ReservationPaxOndCharge ondCharge = new ReservationPaxOndCharge();
		ondCharge.setAmount(amount);
		ondCharge.setChargeRateId(chargeRateId);
		ondCharge.setFareId(fareId);
		ondCharge.setChargeGroupCode(chargeGroupCode);
		ondCharge.setZuluChargeDate(CalendarUtil.getCurrentSystemTimeInZulu());

		if (discount == null) {
			discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		ondCharge.setDiscount(discount);
		// discount percentage
		Integer discValuePer = 0;
		if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeGroupCode) && discount.doubleValue() != 0)
			discValuePer = (int) discValuePercentage;

		if (adjustment == null) {
			adjustment = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		ondCharge.setAdjustment(adjustment);

		ondCharge.setDiscountValuePercentage(discValuePer);

		ondCharge.setAgentCode(credentialsDTO.getAgentCode());
		ondCharge.setUserId(credentialsDTO.getUserId());
		ondCharge.setRefundableOperation(isRefundableOperation ? "Y" : "N");

		BigDecimal taxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal nonTaxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		String taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY;
		String taxDepositStateCode = null;
		String taxDepositCountryCode = null;

		if (paxOndChargeDTO != null) {
			taxableAmount = paxOndChargeDTO.getTaxableAmount();
			nonTaxableAmount = paxOndChargeDTO.getNonTaxableAmount();
			taxAppliedCategory = paxOndChargeDTO.getTaxAppliedCategory();
			taxDepositStateCode = paxOndChargeDTO.getTaxDepositStateCode();
			taxDepositCountryCode = paxOndChargeDTO.getTaxDepositCountryCode();
		}

		ondCharge.setTaxableAmount(taxableAmount);
		ondCharge.setNonTaxableAmount(nonTaxableAmount);
		ondCharge.setTaxAppliedCategory(taxAppliedCategory);
		ondCharge.setTaxDepositStateCode(taxDepositStateCode);
		ondCharge.setTaxDepositCountryCode(taxDepositCountryCode);
		ondCharge.setTransactionSeq(transactionSeq);

		return ondCharge;
	}

	/**
	 * Create credit card payment
	 * 
	 * @param cardPaymentInfo
	 * @return
	 * @throws ModuleException
	 */
	public static CreditCardPayment createCreditCardPayment(CardPaymentInfo cardPaymentInfo, boolean setPreviousCreditCardPayment,
			TravelDTO travelData) throws ModuleException {

		CreditCardPayment creditCardPayment = new CreditCardPayment();

		// paypal
		if (cardPaymentInfo.getUserInputDTO() != null) {
			creditCardPayment.setUserInputDTO(cardPaymentInfo.getUserInputDTO());
		}

		creditCardPayment.setCardholderName(cardPaymentInfo.getName());
		creditCardPayment.setCardNumber(cardPaymentInfo.getNo());
		creditCardPayment.setExpiryDate(cardPaymentInfo.getEDate());

		PayCurrencyDTO payCurrencyDTO = cardPaymentInfo.getPayCurrencyDTO();
		creditCardPayment.setAmount(payCurrencyDTO.getTotalPayCurrencyAmount().toString());
		creditCardPayment.setCurrency(payCurrencyDTO.getPayCurrencyCode());

		int noOfDecimalPoints = BeanUtils.countNoOfDecimals(payCurrencyDTO.getBoundaryValue());
		if (noOfDecimalPoints > 0) {
			creditCardPayment.setNoOfDecimalPoints(noOfDecimalPoints);
		}

		creditCardPayment.setCvvField(cardPaymentInfo.getSecurityCode());
		creditCardPayment.setPnr(cardPaymentInfo.getPnr());
		creditCardPayment.setAppIndicator(cardPaymentInfo.getAppIndicator());
		creditCardPayment.setTnxMode(cardPaymentInfo.getTnxMode());
		creditCardPayment.setTemporyPaymentId(cardPaymentInfo.getTemporyPaymentId());
		creditCardPayment.setPaymentBrokerRefNo(cardPaymentInfo.getPaymentBrokerRefNo());
		creditCardPayment.setIpgIdentificationParamsDTO(cardPaymentInfo.getIpgIdentificationParamsDTO());
		creditCardPayment.setCardType(cardPaymentInfo.getType());
		if (setPreviousCreditCardPayment)
			creditCardPayment.setPreviousCCPayment(createPreviousCreditCardPayment(cardPaymentInfo));
		creditCardPayment.setTravelDTO(travelData);

		return creditCardPayment;
	}

	/**
	 * Create previous credit card payment
	 * 
	 * @param cardPaymentInfo
	 * @return
	 * @throws ModuleException
	 */
	private static PreviousCreditCardPayment createPreviousCreditCardPayment(CardPaymentInfo cardPaymentInfo)
			throws ModuleException {

		PreviousCreditCardPayment previousCCPayment = new PreviousCreditCardPayment();

		previousCCPayment.setNoFirstDigits(cardPaymentInfo.getNoFirstDigits());
		previousCCPayment.setNoLastDigits(cardPaymentInfo.getNoLastDigits());
		previousCCPayment.setName(cardPaymentInfo.getName());
		previousCCPayment.setEDate(cardPaymentInfo.getEDate());
		previousCCPayment.setSecurityCode(cardPaymentInfo.getSecurityCode());
		previousCCPayment.setPnr(cardPaymentInfo.getPnr());
		previousCCPayment.setPaymentBrokerRefNo(cardPaymentInfo.getPaymentBrokerRefNo());
		previousCCPayment.setTemporyPaymentId(cardPaymentInfo.getTemporyPaymentId());

		PayCurrencyDTO payCurrencyDTO = cardPaymentInfo.getPayCurrencyDTO();
		previousCCPayment.setTotalAmount(payCurrencyDTO.getTotalPayCurrencyAmount());

		return previousCCPayment;
	}

	/**
	 * Creates the ChargeMetaTO Object
	 * 
	 * @param reservationPaxOndCharge
	 * @param mapChgTO
	 * @return
	 * @throws ModuleException
	 */
	public static ChargeMetaTO createChargeMetaTO(ReservationPaxOndCharge reservationPaxOndCharge,
			Map<Integer, ChargeTO> mapChgTO) throws ModuleException {
		ChargeMetaTO chargeMetaTO = new ChargeMetaTO();

		chargeMetaTO.setPnrPaxId(reservationPaxOndCharge.getReservationPaxFare().getReservationPax().getPnrPaxId());
		chargeMetaTO.setPaxType(reservationPaxOndCharge.getReservationPaxFare().getReservationPax().getPaxType());
		chargeMetaTO.setChargeDate(reservationPaxOndCharge.getZuluChargeDate());
		chargeMetaTO.setChargeAmount(reservationPaxOndCharge.getAmount());
		chargeMetaTO.setChargeGroupCode(reservationPaxOndCharge.getChargeGroupCode());
		chargeMetaTO.setOndCode(ReservationApiUtils.getOndCode(reservationPaxOndCharge.getReservationPaxFare()));

		if (reservationPaxOndCharge.getChargeRateId() != null) {
			chargeMetaTO.setChargeRateId(reservationPaxOndCharge.getChargeRateId());

			ChargeTO chargeTO = (ChargeTO) mapChgTO.get(reservationPaxOndCharge.getChargeRateId());
			chargeMetaTO.setChargeCode(chargeTO.getChargeCode());
			chargeMetaTO.setChargeDescription(chargeTO.getChargeDescription());
			chargeMetaTO.setAirportTaxCategory(chargeTO.getAirportTaxCategory());
		}

		chargeMetaTO.setDiscountAmount(reservationPaxOndCharge.getDiscount());
		chargeMetaTO.setAdjustmentAmount(reservationPaxOndCharge.getAdjustment());

		return chargeMetaTO;
	}

	/**
	 * Fraud checking data
	 * 
	 * @param ondFareDTOs
	 *            TODO
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static TravelDTO createTravelDTO(ReservationContactInfo contactInfo, Collection<OndFareDTO> ondFareDTOs)
			throws ModuleException {
		TravelDTO travel = new TravelDTO();

		if (contactInfo != null) {

			if (contactInfo != null) {
				travel.setEmailAddress(contactInfo.getEmail());
				travel.setResidenceCountryCode(contactInfo.getCountryCode());
				String phone = contactInfo.getMobileNo();
				if (phone == null || phone.trim().isEmpty()) {
					if (contactInfo.getPhoneNo() != null && !contactInfo.getPhoneNo().isEmpty()) {
						phone = contactInfo.getPhoneNo();
					}
				}
				travel.setContactNumber(phone);
				travel.setContactName(BeanUtils.nullHandler(contactInfo.getFirstName()) + " "
						+ BeanUtils.nullHandler(contactInfo.getLastName()));
			}
		}

		if (ondFareDTOs != null && !ondFareDTOs.isEmpty()) {
			if (ondFareDTOs != null) {
				List<TravelSegmentDTO> segmentDTOs = new ArrayList<TravelSegmentDTO>();
				TravelSegmentDTO segmentDTO = null;
				String depSegmentCode = null;
				String arrivalSegmentCode = null;
				Date systeDate = Calendar.getInstance().getTime();
				String depDummayAirportName = "Dummay airport";
				Object depAirportName = null;
				Object arrAirportName = null;
				Map<String, CachedAirportDTO> airportMap = ReservationModuleUtils.getAirportBD().getAllAirportOperatorMap();
				BookingClassBD bookingClassBD = ReservationModuleUtils.getBookingClassBD();

				for (OndFareDTO fareDTO : ondFareDTOs) {
					String cabinClassCode = null;
					if (fareDTO.getFareSummaryDTO() != null && fareDTO.getFareSummaryDTO().getBookingClassCode() != null) {
						cabinClassCode = bookingClassBD.getBookingClass(fareDTO.getFareSummaryDTO().getBookingClassCode())
								.getCabinClassCode();
					} else {
						cabinClassCode = fareDTO.getSegmentSeatDistsDTO().iterator().next().getCabinClassCode();
					}

					if (fareDTO.getSegmentsMap() != null) {
						for (FlightSegmentDTO flightSegmentDTO : fareDTO.getSegmentsMap().values()) {
							if (systeDate.compareTo(flightSegmentDTO.getDepartureDateTimeZulu()) < 0) {
								segmentDTO = new TravelSegmentDTO();
								segmentDTO.setFlightNumber(Util.extractFlightNumber(flightSegmentDTO.getFlightNumber()));
								segmentDTO
										.setCarrierCode(AppSysParamsUtil.extractCarrierCode(flightSegmentDTO.getFlightNumber()));
								segmentDTO.setFlightDate(flightSegmentDTO.getDepartureDateTime());
								depSegmentCode = Util.getDepartureAirportCode(flightSegmentDTO.getSegmentCode());
								segmentDTO.setDepartureAirportCode(depSegmentCode);
								depAirportName = airportMap.get(depSegmentCode).getAirportName();
								if (depAirportName == null)
									depAirportName = depDummayAirportName;
								segmentDTO.setDepartureAirportName((String) depAirportName);
								arrivalSegmentCode = Util.getAirvalAirportCode(flightSegmentDTO.getSegmentCode());
								segmentDTO.setArrivalAirportCode(arrivalSegmentCode);
								arrAirportName = airportMap.get(arrivalSegmentCode).getAirportName();
								if (arrAirportName == null)
									arrAirportName = depDummayAirportName;
								segmentDTO.setArrivalAirportName((String) arrAirportName);
								segmentDTO.setNoOfStopvers(flightSegmentDTO.getSegmentCode().split("/").length - 2);
								segmentDTO.setClassOfService(cabinClassCode);
								segmentDTO.setDepartureDateZulu(flightSegmentDTO.getDepartureDateTimeZulu());
								segmentDTOs.add(segmentDTO);
							}
						}
					}
				}
				if (!segmentDTOs.isEmpty()) {
					Collections.sort(segmentDTOs);
				}
				travel.setTravelSegments(segmentDTOs);
			}

		}

		return travel;
	}

	public static ReservationPaxOndCreditPromotion createReservationPaxOndCreditPromotion(BigDecimal amount, Integer fareId,
			Integer chargeRateId, String chargeGroupCode, CredentialsDTO credentialsDTO, BigDecimal discount) {
		ReservationPaxOndCreditPromotion ondChargeCreditPromotion = new ReservationPaxOndCreditPromotion();
		ondChargeCreditPromotion.setAmount(amount);
		ondChargeCreditPromotion.setChargeRateId(chargeRateId);
		ondChargeCreditPromotion.setFareId(fareId);
		ondChargeCreditPromotion.setChargeGroupCode(chargeGroupCode);
		ondChargeCreditPromotion.setZuluChargeDate(new Date());

		if (discount == null) {
			discount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		ondChargeCreditPromotion.setDiscount(discount);
		ondChargeCreditPromotion.setAgentCode(credentialsDTO.getAgentCode());
		ondChargeCreditPromotion.setUserId(credentialsDTO.getUserId());

		return ondChargeCreditPromotion;
	}

	public static PaxOndChargeDTO createPaxOndChargeTaxInfoDTO(BigDecimal nonTaxableAmount, BigDecimal taxableAmount,
			String taxAppliedCategory, String taxDepositCountryCode, String taxDepositStateCode) {

		PaxOndChargeDTO paxOndChargeDTO = new PaxOndChargeDTO();
		paxOndChargeDTO.setNonTaxableAmount(nonTaxableAmount);
		paxOndChargeDTO.setTaxableAmount(taxableAmount);

		if (StringUtil.isNullOrEmpty(taxAppliedCategory)) {
			taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY;
		}
		paxOndChargeDTO.setTaxDepositCountryCode(taxDepositCountryCode);
		paxOndChargeDTO.setTaxDepositStateCode(taxDepositStateCode);

		paxOndChargeDTO.setTaxAppliedCategory(taxAppliedCategory);
		return paxOndChargeDTO;
	}

}
