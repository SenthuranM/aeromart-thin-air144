package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.revacc.ReservationCreditTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 * 
 * @since August 5, 2010
 */
public class TnxGranularityFullFillmentDeriverBO {

	private Collection<ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO;

	public TnxGranularityFullFillmentDeriverBO(
			Collection<ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO) {
		this.colReservationPaxChargeMetaTO = colReservationPaxChargeMetaTO;
	}

	public Collection<ReservationPaxChargeMetaTO> execute(BigDecimal amount, Integer pnrPaxId) throws ModuleException {
		Collection<ReservationPaxChargeMetaTO> selectedReservationPaxOndMetaTOs = new ArrayList<ReservationPaxChargeMetaTO>();

		if (colReservationPaxChargeMetaTO != null && colReservationPaxChargeMetaTO.size() > 0) {
			Collection<ReservationPaxChargeMetaTO> newReservationPaxOndMetaTOs = new ArrayList<ReservationPaxChargeMetaTO>();
			Collection<String> colChargeGroupOrder = TnxGranularityRules.getPaymentChargeGroupFulFillmentOrder();

			for (String chargeGroupCode : colChargeGroupOrder) {

				if (amount.doubleValue() > 0) {
					Collection<ReservationPaxChargeMetaTO> colChargeTypeMetaTO = TnxGranularityUtils
							.getChargeMetaTOByChargeGroupCode(chargeGroupCode, colReservationPaxChargeMetaTO);

					for (ReservationPaxChargeMetaTO reservationPaxChargeMetaTO : colChargeTypeMetaTO) {

						if (amount.doubleValue() > 0) {
							BigDecimal chargeAmount = reservationPaxChargeMetaTO.getAmount();
							
							if (amount.doubleValue() >= chargeAmount.doubleValue()) {

								// if (chargeAmount.doubleValue() > 0)
								amount = AccelAeroCalculator.subtract(amount, chargeAmount);

								selectedReservationPaxOndMetaTOs.add(reservationPaxChargeMetaTO);

							} else {
								BigDecimal balanceAmount = AccelAeroCalculator.subtract(chargeAmount, amount);

								reservationPaxChargeMetaTO.setAmount(amount);
								amount = AccelAeroCalculator.getDefaultBigDecimalZero();
								selectedReservationPaxOndMetaTOs.add(reservationPaxChargeMetaTO);

								// remaining charge should be link with the next payment.
								reservationPaxChargeMetaTO = reservationPaxChargeMetaTO.clone();
								reservationPaxChargeMetaTO.setAmount(balanceAmount);
								newReservationPaxOndMetaTOs.add(reservationPaxChargeMetaTO);
							}
						} else {
							break;
						}
					}
				} else {
					break;
				}
			}

			// if excess payments exist (amount > 0) we will leave it as pax credit. It will be utilized in other
			// modification

			// charge divide among multiple payments. remaining portions is added to be link with the next payment
			if (newReservationPaxOndMetaTOs.size() > 0) {
				colReservationPaxChargeMetaTO.addAll(newReservationPaxOndMetaTOs);
			}

			// already link with the payment.Thus removing from the cache
			if (selectedReservationPaxOndMetaTOs.size() > 0) {
				colReservationPaxChargeMetaTO.removeAll(selectedReservationPaxOndMetaTOs);
			}
		}

		return selectedReservationPaxOndMetaTOs;
	}
	
	/**
	 * following function will adjust the payment charges collection[ADD/UPDATE] with respect to the mismatch with
	 * credit amount. always we assume credit amount is the correct value.
	 * */
	public static void validateAndAdjustWithReservationCredit(Collection<ReservationCredit> colReservationCredit) {
		if (colReservationCredit != null && colReservationCredit.size() > 0) {

			// Refund order
			Collection<String> colChargeGroupCodes = TnxGranularityRules.getRefundChargeGroupFulFillmentOrder();

			for (ReservationCredit reservationCredit : colReservationCredit) {
				boolean isCreditUpdated = false;
				if (reservationCredit.getBalance().doubleValue() != reservationCredit.getPaymentCreditTotal().doubleValue()) {
					if (reservationCredit.getBalance().doubleValue() > reservationCredit.getPaymentCreditTotal().doubleValue()) {
						BigDecimal difference = AccelAeroCalculator.subtract(reservationCredit.getBalance(),
								reservationCredit.getPaymentCreditTotal());
						ReservationPaxPaymentCredit paxPaymentCredit = new ReservationPaxPaymentCredit();
						paxPaymentCredit.setPaxOndPaymentId(null);
						paxPaymentCredit.setAmount(difference);
						paxPaymentCredit.setRemarks("Balance Out Amount");
						paxPaymentCredit.setChargeGroupCode(ChargeGroup.ADJ);

						reservationCredit.addPayments(paxPaymentCredit);

						isCreditUpdated = true;
					} else {
						BigDecimal eliminateCredit = AccelAeroCalculator.subtract(reservationCredit.getPaymentCreditTotal(),
								reservationCredit.getBalance());
						if (colChargeGroupCodes != null && colChargeGroupCodes.size() > 0) {
							for (String chargeGroupCode : colChargeGroupCodes) {

								Collection<ReservationPaxPaymentCredit> selectedPaymentCredits = TnxGranularityUtils
										.getApplicablePaymentCreditByChargeGroup(chargeGroupCode, reservationCredit.getPayments(), false);

								if (selectedPaymentCredits != null && selectedPaymentCredits.size() > 0) {
									for (ReservationPaxPaymentCredit paxPaymentCredit : selectedPaymentCredits) {
										BigDecimal amount = paxPaymentCredit.getAmount();

										if (paxPaymentCredit != null && eliminateCredit.doubleValue() > 0
												&& amount.doubleValue() > 0) {

											if (amount.doubleValue() >= eliminateCredit.doubleValue()) {
												paxPaymentCredit.setAmount(AccelAeroCalculator.subtract(amount, eliminateCredit));
												eliminateCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
											} else {
												paxPaymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
												eliminateCredit = AccelAeroCalculator.subtract(eliminateCredit, amount);

											}
											isCreditUpdated = true;

										}

									}

								}
							}
						}

					}
				}

				if (isCreditUpdated) {
					// if ReservationCredit modified update it
					ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.saveReservationCredit(reservationCredit);
				}

			}
		}

	}

	private static void normalizeAvailablePaymentCredit(Collection<ReservationCredit> colReservationCredit,
			ReservationCreditTO maxCreditAmountTO) {
		if (colReservationCredit != null && colReservationCredit.size() > 0 && maxCreditAmountTO != null) {

			Collection<String> colChargeGroupOrder = new ArrayList<String>();
			colChargeGroupOrder.add(ChargeGroup.FAR);
			colChargeGroupOrder.add(ChargeGroup.TAX);
			colChargeGroupOrder.add(ChargeGroup.SUR);
			colChargeGroupOrder.add(ChargeGroup.ANY);

			for (String chargeGroup : colChargeGroupOrder) {

				Map<String, BigDecimal> chargeConsumedView = maxCreditAmountTO.getAppliedChargeGroupsNew(chargeGroup);

				if (chargeConsumedView != null) {
					for (String consumedGroup : chargeConsumedView.keySet()) {

						BigDecimal consumedAmount = chargeConsumedView.get(consumedGroup);
						if (consumedAmount.doubleValue() > 0) {

							for (ReservationCredit reservationCredit : colReservationCredit) {

								for (ReservationPaxPaymentCredit paxPaymentCredit : reservationCredit.getPayments()) {
									BigDecimal amount = paxPaymentCredit.getAmount();
									String chargeGroupCode = paxPaymentCredit.getChargeGroupCode();

									if (consumedGroup.equals(chargeGroupCode)
											|| (TnxGranularityUtils.isValidOtherChargeGroup(chargeGroupCode) && ChargeGroup.ANY
													.equals(consumedGroup)) && amount.doubleValue() > 0) {

										if (amount.doubleValue() > consumedAmount.doubleValue()) {

											BigDecimal balance = AccelAeroCalculator.subtract(amount, consumedAmount);
											paxPaymentCredit.setAmount(balance);

											ReservationPaxPaymentCredit clonedPaxPaymentCredit = paxPaymentCredit.clone();
											clonedPaxPaymentCredit.setAmount(consumedAmount);
											if (!chargeGroup.equals(consumedGroup)
													|| (ChargeGroup.ANY.equals(chargeGroup) && TnxGranularityUtils
															.isValidOtherChargeGroup(consumedGroup))) {

												if (ChargeGroup.ANY.equals(chargeGroup)) {
													chargeGroup = ChargeGroup.ADJ;
												}

												clonedPaxPaymentCredit.setChargeGroupCode(chargeGroup);
											}

											reservationCredit.addPayments(clonedPaxPaymentCredit);

											consumedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
											break;

										} else {
											consumedAmount = AccelAeroCalculator.subtract(consumedAmount, amount);

											if (!chargeGroup.equals(consumedGroup)
													|| (ChargeGroup.ANY.equals(chargeGroup) && TnxGranularityUtils
															.isValidOtherChargeGroup(consumedGroup))) {

												if (ChargeGroup.ANY.equals(chargeGroup)) {
													chargeGroup = ChargeGroup.ADJ;
												}

												paxPaymentCredit.setChargeGroupCode(chargeGroup);
											}

										}

									}

								}

								ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.saveReservationCredit(reservationCredit);

							}

						}

					}
				}

			}

		}

	}

	public static ReservationCreditTO adjustAvailableReservationCredit(
			Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTOForNewCharges,
			Collection<ReservationCredit> colReservationCredit) {

		// current charges view, required payment amounts charge group wise
		ReservationCreditTO requiredAmounts = new ReservationCreditTO();
		requiredAmounts.addPaymentCreditsOpt(colReservationPaxChargeMetaTOForNewCharges);

		// adjust payment credit record if there is a mismatch with exact credit amount
		validateAndAdjustWithReservationCredit(colReservationCredit);

		// available total credits charge group wise
		ReservationCreditTO availableAmounts = new ReservationCreditTO();
		availableAmounts.processAvailableCredits(colReservationCredit);

		// calculates optimized available credit amount to utilization
		ReservationCreditTO maxCreditAmountTO = TnxGranularityUtils.optimizedCreditToUtilizeFromAvailableCredit(availableAmounts,
				requiredAmounts);

		normalizeAvailablePaymentCredit(colReservationCredit, maxCreditAmountTO);

		return maxCreditAmountTO;
	}

	/**
	 * function remove ReservationPaxPaymentCredit from respective ReservationCredit when payment has been captured from
	 * CF.
	 * */
	public static void wipeOfCreditPayments(ReservationCredit reservationCredit, ReservationCredit clonedReservationCredit,
			BigDecimal amountToConsume, String chargeGroupCode) {

		Set<ReservationPaxPaymentCredit> payments = reservationCredit.getPayments();
		if (reservationCredit != null && clonedReservationCredit != null && payments != null && payments.size() > 0) {
			Iterator<ReservationPaxPaymentCredit> paymentCReditItr = payments.iterator();
			BigDecimal balancePaymentCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal remainingPaymentToConsume = new BigDecimal(amountToConsume.doubleValue());
			while (paymentCReditItr.hasNext()) {

				ReservationPaxPaymentCredit paymentCredit = paymentCReditItr.next();
				BigDecimal paymentCreditAmount = paymentCredit.getAmount();
				ReservationPaxPaymentCredit paymentCreditModified = paymentCredit.clone();

				if (chargeGroupCode.equals(paymentCredit.getChargeGroupCode())
						|| (TnxGranularityUtils.isValidOtherChargeGroup(paymentCredit.getChargeGroupCode()) && TnxGranularityUtils
								.isValidOtherChargeGroup(chargeGroupCode)) && paymentCreditAmount.doubleValue() > 0) {

					if (paymentCreditAmount.doubleValue() >= remainingPaymentToConsume.doubleValue()) {
						balancePaymentCredit = AccelAeroCalculator.subtract(paymentCreditAmount, remainingPaymentToConsume);

						paymentCreditModified.setAmount(remainingPaymentToConsume);
						if (balancePaymentCredit.doubleValue() > 0) {
							paymentCredit.setAmount(balancePaymentCredit);

						} else if (balancePaymentCredit.doubleValue() == 0) {
							paymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						}
						clonedReservationCredit.addPayments(paymentCreditModified);
						remainingPaymentToConsume = AccelAeroCalculator.getDefaultBigDecimalZero();
						break;

					} else {
						remainingPaymentToConsume = AccelAeroCalculator.subtract(remainingPaymentToConsume, paymentCreditAmount);
						paymentCredit.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());

						paymentCreditModified.setAmount(paymentCreditAmount);

						clonedReservationCredit.addPayments(paymentCreditModified);
					}

				}

			}
			ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO.saveReservationCredit(reservationCredit);

		}
	}

}
