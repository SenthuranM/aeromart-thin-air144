package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import java.util.List;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class RemovePaxIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public RemovePaxIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {
		RuleResponseDTO response = getResponseDTO();

		List<Integer> paxInOldRes = getUncancelledPaxIds(dataContext.getExisitingReservation());
		List<Integer> paxInNewRes = getUncancelledPaxIds(dataContext.getUpdatedReservation());

		paxInOldRes.removeAll(paxInNewRes);
		updatePaxWiseAffectedSegmentListToAllGivenPax(response, null,
				getUncancelledSegIds(dataContext.getExisitingReservation()), paxInOldRes);

		return response;
	}

}
