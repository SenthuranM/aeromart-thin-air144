/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ChildElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.MealElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.SSRElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;

/**
 * @author udithad
 *
 */
public class MealElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private UtilizedPassengerContext uPContext;
	private List<PassengerInformation> passengerInformations;

	private BaseRuleExecutor<RulesDataContext> mealElementRuleExecutor = new MealElementRuleExecutor();

	private boolean isStartWithNewLine = false;

	@Override
	public void buildElement(ElementContext context) {
		if (context != null && context.getFeaturePack() != null) {
			initContextData(context);
			if (context.getFeaturePack().isShowMeals()) {
				mealElementTemplate();
			}
		}
		executeNext();
	}

	private void mealElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation != null
					&& passengerInformation.getMeals() != null) {
				List<AncillaryDTO> meals = passengerInformation.getMeals();
				if (meals != null) {
					for (AncillaryDTO anci : fileterCommonAncillariesDescription(meals)) {
						buildMealElement(elementTemplate, passengerInformation,
								anci);
						currentElement = elementTemplate.toString();
						if (currentElement != null && !currentElement.isEmpty()) {
							ammendmentPreValidation(isStartWithNewLine, currentElement, uPContext, mealElementRuleExecutor);
						}

					}

				}
			}
		}
	}

	private List<AncillaryDTO> fileterCommonAncillariesDescription(
			List<AncillaryDTO> ancillaries) {
		List<AncillaryDTO> filteredAncillaries = new ArrayList<AncillaryDTO>();
		Map<AncillaryDTO, Integer> anciMap = new HashMap<AncillaryDTO, Integer>();
		for (AncillaryDTO anci : ancillaries) {
			if (isAnciIdenticle(anciMap, anci)) {
				AncillaryDTO ancillaryDTO = getIdenticleAnci(anciMap, anci);
				Integer value = anciMap.get(ancillaryDTO);
				if (value != null) {
					value = value + 1;
					anciMap.put(ancillaryDTO, value);
					ancillaryDTO.setCount(value);
				}
			} else {
				anciMap.put(anci, 1);
				anci.setCount(1);
			}
		}

		AncillaryDTO ancillaryDTO = new AncillaryDTO();
		String code = "";
		Map<String, AncillaryDTO> treeMap = new TreeMap<String, AncillaryDTO>();
		for (Map.Entry<AncillaryDTO, Integer> entry : anciMap.entrySet()) {
			treeMap.put(entry.getKey().getCode(), entry.getKey());
		}
		for (Map.Entry<String, AncillaryDTO> ancilMap : treeMap.entrySet()) {
			if (code != null && code.isEmpty()) {
				code = ancilMap.getValue().getCount() + ""
						+ ancilMap.getValue().getCode();
			} else {
				code = code + " " + ancilMap.getValue().getCount() + ""
						+ ancilMap.getValue().getCode();
			}
		}
		
		ancillaryDTO.setCode(code);
		filteredAncillaries.add(ancillaryDTO);
		return filteredAncillaries;
	}

	private boolean isAnciIdenticle(Map<AncillaryDTO, Integer> anciMap,
			AncillaryDTO currenAnciDTO) {
		boolean isIdenticle = false;
		for (Map.Entry<AncillaryDTO, Integer> entry : anciMap.entrySet()) {
			AncillaryDTO ancillaryDTO = entry.getKey();
			if (currenAnciDTO.getDescription().equals(
					entry.getKey().getDescription())) {
				isIdenticle = true;
			}
		}
		return isIdenticle;
	}

	private AncillaryDTO getIdenticleAnci(Map<AncillaryDTO, Integer> anciMap,
			AncillaryDTO currenAnciDTO) {
		AncillaryDTO anci = null;
		for (Map.Entry<AncillaryDTO, Integer> entry : anciMap.entrySet()) {
			AncillaryDTO ancillaryDTO = entry.getKey();
			if (currenAnciDTO.getCode().equals(
					entry.getKey().getCode())) {
				anci = entry.getKey();
			}
		}
		return anci;
	}

	private void buildMealElement(StringBuilder elementTemplate,
			PassengerInformation passengerInformation, AncillaryDTO anciliary) {
		elementTemplate.setLength(0);
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.REMARKS);
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.SPML);
		elementTemplate.append(MessageComposerConstants.SP);
		elementTemplate
				.append(MessageComposerConstants.PNLADLMessageConstants.HK + 1);
		elementTemplate.append(MessageComposerConstants.SP);
		elementTemplate.append(anciliary.getCode());
		if (passengerInformations.size() > 1) {
			elementTemplate.append(MessageComposerConstants.HYPHEN);
			elementTemplate.append(1);
			elementTemplate.append(passengerInformation.getLastName());
			elementTemplate.append(MessageComposerConstants.FWD);
			elementTemplate.append(passengerInformation.getFirstName());
			elementTemplate.append(passengerInformation.getTitle());
		}

	}

	private void initContextData(ElementContext context) {
		uPContext = (UtilizedPassengerContext) context;
		currentLine = uPContext.getCurrentMessageLine();
		messageLine = uPContext.getMessageString();
		passengerInformations = getPassengerInUtilizedList();
		currentElement = "";
	}

	private List<PassengerInformation> getPassengerInUtilizedList() {
		List<PassengerInformation> passengerInformations = null;
		if (uPContext.getUtilizedPassengers() != null
				&& uPContext.getUtilizedPassengers().size() > 0) {
			passengerInformations = uPContext.getUtilizedPassengers();
		}
		return passengerInformations;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(uPContext);
		}
	}

}
