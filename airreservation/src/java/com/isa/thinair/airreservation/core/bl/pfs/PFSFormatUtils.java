/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pfs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.PfsLogDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * The utility to validate PFS formating and ground keeping work
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PFSFormatUtils {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PFSFormatUtils.class);

	/** Holds the pfs file name date format */
	private static final String PFS_FILE_NAME_DATE_FORMAT = "ddMMyyyyHHmmss";

	/** Holds the pfs file extension */
	private static final String PFS_FILE_EXTENSION = ".txt";

	/**
	 * Returns the pfs file name
	 * 
	 * @param currentDate
	 * @param message
	 * @return
	 */
	public static String getPfsFileName(Date currentDate, int message) {
		return "msg-" + BeanUtils.parseDateFormat(currentDate, PFS_FILE_NAME_DATE_FORMAT) + "-" + message + PFS_FILE_EXTENSION;
	}

	/**
	 * Move and delete pfs documents
	 * 
	 * @param pfsProcessPath
	 * @param pfsParsedPath
	 */
	public static void moveAndDeletePfs(String pfsProcessPath, String pfsParsedPath, String strMsgName) {
		File dirPfsPath = new File(pfsProcessPath);
		File[] files = dirPfsPath.listFiles();

		// Moving the new files
		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			// If file name exist
			if (strMsgName.equals(file.getName())) {
				File dirParsedPfs = new File(pfsParsedPath);

				// If directory does not exist create one
				if (!dirParsedPfs.exists()) {
					dirParsedPfs.mkdirs();
				}

				File copyFile = new File(dirParsedPfs, file.getName());
				copyAndIgnoreErrors(file, copyFile);
				file.delete();
			}
		}
	}

	/**
	 * Return message names
	 * 
	 * @param path
	 * @return
	 */
	public static Map<String, String> getMessageNames(String path, Map<String, String> pfsContents) {
		File directory = new File(path);
		File[] files = directory.listFiles();
		// Collection colMsgNames = new ArrayList();
		Map<String, String> pfsContentsWithNames = new HashMap<String, String>();

		for (int i = 0; i < files.length; i++) {
			// colMsgNames.add(files[i].getName());
			pfsContentsWithNames.put(files[i].getName(),
					pfsContents.get(files[i].getName()) != null ? pfsContents.get(files[i].getName()) : "");
		}

		return pfsContentsWithNames;
	}

	/**
	 * Validate the messages
	 * 
	 * @param pfsProcessPath
	 * @param pfsErrorPath
	 */
	public static Map<String, String> validateMessages(String pfsProcessPath, String pfsErrorPath, String partialDownloadPath) {
		Map<String,Map<Integer,String>> uniqueKeyAndPfsPartsMap = new HashMap<String,Map<Integer,String>>();
		Collection<String> errorFileNames = new ArrayList<String>();
		Map<Integer,String> partIdAndPfsDocs;
		String strHolder;
		BufferedReader buffer;
		boolean isPfsFormatCompliant;
		boolean isNextValueUniqueKey;
		boolean isMultiplePfsExist;
		int partNumber = -1;
		String uniqueKey = null;
		StringBuffer pfsText = null;
		Map<String, String> pfsContents = new HashMap<String, String>();
		Map<String, Integer> pfsPartCount = new HashMap<String, Integer>();

		// Move partially downloaded files during the previous scheduler to process directory
		File pdDirectory = new File(partialDownloadPath);
		moveAllFiles(pdDirectory.listFiles(), pfsProcessPath);

		File directory = new File(pfsProcessPath);
		File[] files = directory.listFiles();

		// Validating the pfs documents
		for (int i = 0; i < files.length; i++) {
			try {
				FileReader fileReader = new FileReader(files[i]);
				buffer = new BufferedReader(fileReader);
				isPfsFormatCompliant = false;
				isNextValueUniqueKey = false;
				isMultiplePfsExist = false;
				partNumber = -1;
				uniqueKey = null;
				strHolder = null;
				pfsText = new StringBuffer();

				while ((strHolder = buffer.readLine()) != null) {
					pfsText.append(strHolder + "\n");
					// Checking for PFS text
					if (strHolder.matches("PFS")) {
						isPfsFormatCompliant = true;
						isNextValueUniqueKey = true;

						// Checking for flight number, departure date, departure station and part number
					} else if (isNextValueUniqueKey) {
						strHolder = BeanUtils.nullHandler(strHolder);
						uniqueKey = strHolder.substring(0, strHolder.indexOf("PART"));
						uniqueKey = BeanUtils.nullHandler(uniqueKey);
						partNumber = Integer.parseInt(strHolder.substring(strHolder.indexOf("PART") + 4, strHolder.length()));
						isNextValueUniqueKey = false;
					}

					// This means that pfs is spanning for multiple parts
					if (isMultiplePfsExist == false && uniqueKey != null
							&& (strHolder.indexOf("ENDPART") != -1 || partNumber > 1)) {
						isMultiplePfsExist = true;

						// If unique key already exist
						if (uniqueKeyAndPfsPartsMap.containsKey(uniqueKey)) {
							partIdAndPfsDocs = (Map<Integer,String>) uniqueKeyAndPfsPartsMap.get(uniqueKey);
							partIdAndPfsDocs.put(new Integer(partNumber), files[i].getName());
						} else {
							// Initializing a Tree Map inorder to keep the parts sorted
							partIdAndPfsDocs = new TreeMap<Integer,String>();
							partIdAndPfsDocs.put(new Integer(partNumber), files[i].getName());

							uniqueKeyAndPfsPartsMap.put(uniqueKey, partIdAndPfsDocs);
						}
					}
					if (BeanUtils.nullHandler(strHolder).matches("ENDPFS")) {
						pfsPartCount.put(uniqueKey, partNumber);
					}
				}

				// If any non pfs format compliant document exist
				if (isPfsFormatCompliant == false) {
					errorFileNames.add(files[i].getName());
					mailErrors(files[i].getName() + "\n" + pfsText.toString(), null, "Erroneous PFS received.");
				} else {
					pfsContents.put(files[i].getName(), pfsText.toString());
				}
				buffer.close();
				buffer = null;
				fileReader.close();
				fileReader = null;
			} catch (Exception e) {
				mailErrors(files[i].getName(), e, "Inside PfsFormatUtils.validateMessages");

				// Purposly catching the exception to continue with other documents
				log.error(" ERROR VALIDATING THE PFS NAMED : " + files[i].getName() + " " + e.getMessage());
			}
		}

		// Move the error file to another directory
		moveErrorFiles(errorFileNames, files, pfsErrorPath, true);

		// Handling the multiple pfs documents
		Iterator<String> itUniqueKey = uniqueKeyAndPfsPartsMap.keySet().iterator();
		while (itUniqueKey.hasNext()) {
			uniqueKey = (String) itUniqueKey.next();
			partIdAndPfsDocs = (Map<Integer,String>) uniqueKeyAndPfsPartsMap.get(uniqueKey);

			// If Mulitple pfs document found
			// Otherwise the part will left over till the complete parts are available
			if (partIdAndPfsDocs.values().size() > 1 || pfsPartCount.get(uniqueKey) == null || pfsPartCount.get(uniqueKey) > 1) {
				if (pfsPartCount.get(uniqueKey) != null && pfsPartCount.get(uniqueKey) == partIdAndPfsDocs.values().size()) {
					mergeMultiplePfstoOnePfs(pfsProcessPath, uniqueKey, partIdAndPfsDocs.values(), pfsContents);
				} else {
					// Move partially downloaded files to another directory
					moveErrorFiles(partIdAndPfsDocs.values(), files, partialDownloadPath, false);
				}
			}
		}
		return pfsContents;
	}

	// method to be called when validating and merging pfs's if error occures... for the ease of locating the error.
	private static void mailErrors(String fileName, Throwable e, String customError) {

		PfsLogDTO pfsLogDTO = new PfsLogDTO();
		if (e != null) {
			pfsLogDTO.setExceptionDescription(e.getLocalizedMessage());
			pfsLogDTO.setStackTraceElements(e.getStackTrace());
		} else {
			pfsLogDTO.setExceptionDescription("Errorneous PFS File detected");
			pfsLogDTO.setStackTraceElements(null);
		}
		pfsLogDTO.setFileName(fileName);
		pfsLogDTO.setCustomDescription(customError);
		try {
			PfsMailError.notifyError(pfsLogDTO);
		} catch (ModuleException e1) {

		}
	}

	/**
	 * Move the error file names
	 * 
	 * @param errorFileNames
	 * @param files
	 * @param pfsErrorPath
	 */
	private static void moveErrorFiles(Collection<String> errorFileNames, File[] files, String pfsErrorPath, boolean isAddPrefix) {
		// Moving the pfs error documents to a error folder
		if (errorFileNames.size() > 0) {
			for (int i = 0; i < files.length; i++) {
				File f = files[i];

				// If file name exist
				if (errorFileNames.contains(f.getName())) {
					File dirErrorPfs = new File(pfsErrorPath);

					// If directory does not exist create one
					if (!dirErrorPfs.exists()) {
						dirErrorPfs.mkdirs();
					}

					File copyFile = new File(dirErrorPfs, (isAddPrefix ? "NON-" : "") + f.getName());
					// Copy and ignore errors
					copyAndIgnoreErrors(f, copyFile);

					// Delete the current file
					f.delete();
				}
			}
		}
	}

	private static void moveAllFiles(File[] files, String pfsPath) {

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				File f = files[i];

				File dirErrorPfs = new File(pfsPath);

				// If directory does not exist create one
				if (!dirErrorPfs.exists()) {
					dirErrorPfs.mkdirs();
				}

				File copyFile = new File(dirErrorPfs, f.getName());
				// Copy and ignore errors
				copyAndIgnoreErrors(f, copyFile);

				// Delete the current file
				f.delete();

			}
		}

	}

	/**
	 * Merging multiple pfs documents to a one pfs document
	 * 
	 * @param pfsProcessPath
	 * @param uniqueKey
	 * @param colFileNames
	 */
	private static void mergeMultiplePfstoOnePfs(String pfsProcessPath, String uniqueKey, Collection<String> colFileNames,
			Map<String, String> pfsContents) {
		StringBuffer holder = new StringBuffer();
		boolean isFirstEntry = true;
		boolean started = false;
		BufferedReader buffer;
		String lineText;
		File file;
		Collection<String> filesToBeDeleted = new ArrayList<String>();
		// Build Multiple pfs file name
		String fileName = buildMultiplePfsFileName(colFileNames);

		try {
			for (Iterator<String> itFileName = colFileNames.iterator(); itFileName.hasNext();) {
				String fileN = (String) itFileName.next();
				filesToBeDeleted.add(fileN);
				file = new File(pfsProcessPath, fileN);
				FileReader f = new FileReader(file);
				buffer = new BufferedReader(f);

				while ((lineText = buffer.readLine()) != null) {
					// If it's the first pfs document we may need the header information too
					if (isFirstEntry) {
						if (lineText.indexOf("ENDPART") == -1 && !lineText.equals("")) {
							holder.append(lineText + "\n");
						} else if (lineText.indexOf("ENDPART") != -1) {
							break;
						}
						// Handling the other pfs documents
					} else {
						// Found the exact pfs information
						if (lineText.equals("PFS")) {
							started = true;
						} else if (started) {
							if (lineText.indexOf("ENDPART") == -1 && lineText.indexOf("ENDPFS") == -1
									&& lineText.indexOf(uniqueKey) == -1 && !lineText.equals("")) {
								holder.append(lineText + "\n");
							} else if (lineText.indexOf("ENDPART") != -1 || lineText.indexOf("ENDPFS") != -1) {
								// append endpfs part at the end
								if (lineText.indexOf("ENDPFS") != -1) {
									holder.append(lineText);
								}
								break;
							}
						}
					}

				}
				buffer.close();
				buffer = null;
				f.close();
				f = null;
				isFirstEntry = false;
				started = false;
			}

			// Just in case checking to see if any holder information exist
			if (holder.length() > 0) {
				File f = new File(pfsProcessPath, fileName);
				FileWriter ft = new FileWriter(f);
				BufferedWriter out = new BufferedWriter(ft);
				out.write(holder.toString());
				out.close();
				out = null;
				ft.close();
				ft = null;
				deleteFiles(filesToBeDeleted, pfsProcessPath);
				// storing pfs multi part message in the map, in order to add in error emails
				pfsContents.put(fileName, holder.toString());
			}
		} catch (Exception e) {
			mailErrors("General Error", e, " Inside PfsFormatUtils.mergeMultiplePfstoOnePfs <BR> Files Are : --><BR>"
					+ PlatformUtiltiies.constructINStringForCharactors(colFileNames) + " <BR> " + " Merged File Name : "
					+ fileName);
			log.error(" ERROR MERGING MULTIPLE PFS DOCUMENTS TO : " + fileName);
		}
	}

	private static void deleteFiles(Collection<String> filesToBeDeleted, String pfsProcessPath) {
		Iterator<String> iterator = filesToBeDeleted.iterator();
		while (iterator.hasNext()) {
			String file = (String) iterator.next();
			File f = new File(pfsProcessPath, file);
			if (f.delete()) {
				continue;
			} else {
				f.delete();
			}
		}
	}

	/**
	 * Build multiple pfs file name
	 * 
	 * @param colFileNames
	 * @return
	 */
	private static String buildMultiplePfsFileName(Collection<String> colFileNames) {
		try {
			String fileName = null;

			for (Iterator<String> itFileNames = colFileNames.iterator(); itFileNames.hasNext();) {
				if (fileName != null) {
					String value = (String) itFileNames.next();

					int lastIndex = value.lastIndexOf("-");
					int lastNumber = Integer.parseInt(value.substring(lastIndex + 1, value.indexOf(PFS_FILE_EXTENSION)));

					if (fileName.indexOf(PFS_FILE_EXTENSION) == -1) {
						fileName = fileName + "," + lastNumber;
					} else {
						fileName = fileName.substring(0, fileName.indexOf(PFS_FILE_EXTENSION)) + "," + lastNumber;
					}
				} else {
					fileName = (String) itFileNames.next();
				}
			}

			if (fileName == null) {
				return "error-" + getPfsFileName(new Date(), 0);
			} else {
				return fileName + PFS_FILE_EXTENSION;
			}
		} catch (Exception e) {
			mailErrors("General Error", e, " Inside PfsFormatUtils.buildMultiplePfsFileName<BR>Files Are : --><BR>"
					+ PlatformUtiltiies.constructINStringForCharactors(colFileNames));
		}
		return "error-" + getPfsFileName(new Date(), 0);
	}

	/**
	 * Copies src file to dst file. If the dst file does not exist, it is created
	 * 
	 * @param src
	 * @param dst
	 */
	private static void copyAndIgnoreErrors(File src, File dst) {
		try {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dst);

			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;

			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			in.close();
			out.close();

		} catch (Exception e) {
			// This can not happen practically
			log.info("################## UN EXCEPECTED PFS COPYING ERROR OCCURED ", e);
		}
	}

	/**
	 * @param args
	 * 
	 *            Temporary Main Method
	 */
	public static void main(String[] args) throws Exception {
		moveAndDeletePfs("c:/PfsProcessPath", "c:/PfsParsedPath", "msg-18072006105728-0.txt");
	}
}
