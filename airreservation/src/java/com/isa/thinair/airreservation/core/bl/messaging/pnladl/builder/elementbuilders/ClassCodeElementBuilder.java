/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.MessageHeaderElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.DestinationFareContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.ClassCodeElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.CCodeElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class ClassCodeElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private Map<String, List<String>> classCodeElements;

	private MessageHeaderElementContext heContext;
	private DestinationFareContext pEContext;

	private BaseRuleExecutor<RulesDataContext> classCodeElementRuleExecutor = new ClassCodeElementRuleExecutor();

	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		currentElement = "";
		initContextData(context);
		if (heContext.getPartNumber() != null && heContext.getPartNumber() == 1) {
			classCodeElementTemplate();
			response = validateSubElement(currentElement);
			ammendMessageDataAccordingTo(response);
		}
		executeNext();
	}

	private void initContextData(ElementContext context) {
		heContext = (MessageHeaderElementContext) context;
		pEContext = (DestinationFareContext) context;
		currentLine = heContext.getCurrentMessageLine();
		messageLine = heContext.getMessageString();
		classCodeElements = heContext.getClassCodeElements();
	}

	private void classCodeElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		elementTemplate.append(classCodeElementPrefix());
		for (Map.Entry<String, List<String>> entry : classCodeElements
				.entrySet()) {
			elementTemplate.append(space());
			elementTemplate.append(entry.getKey());
			elementTemplate.append(forwardSlash());
			appendFareClassCodes(elementTemplate, entry.getValue());
		}
		currentElement = elementTemplate.toString();
	}

	private void appendFareClassCodes(StringBuilder elementTemplate,
			List<String> fareClassElements) {
		removeDuplicates(fareClassElements);
		Collections.sort(fareClassElements);
		if (fareClassElements != null) {
			for (String fareClassElement : fareClassElements) {
				elementTemplate.append(fareClassElement.charAt(0));
			}
		}
	}

	private void removeDuplicates(List<String> fareClassElements) {
		Set<String> hs = new HashSet<>();
		hs.addAll(fareClassElements);
		fareClassElements.clear();
		fareClassElements.addAll(hs);
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			if (currentElement != null && !currentElement.isEmpty()) {
				executeConcatenationElementBuilder(heContext);
				ammendToBaseLine(currentElement, currentLine, messageLine);
			}
		}
	}

	private RuleResponse validateSubElement(String elementText) {
		RuleResponse ruleResponse = new RuleResponse();
		RulesDataContext rulesDataContext = createRuleDataContext(
				currentLine.toString(), elementText, 0);
		ruleResponse = classCodeElementRuleExecutor
				.validateElementRules(rulesDataContext);
		return ruleResponse;
	}

	private RulesDataContext createRuleDataContext(String currentLine,
			String ammendingLine, int reductionLength) {
		CCodeElementRuleContext rulesDataContext = new CCodeElementRuleContext();
		rulesDataContext.setCurrentLine(currentLine);
		rulesDataContext.setAmmendingLine(ammendingLine);
		rulesDataContext.setReductionLength(reductionLength);
		if (pEContext.getFeaturePack() != null) {
			rulesDataContext.setRbdEnabled(pEContext.getFeaturePack()
					.isRbdEnabled());
		}
		return rulesDataContext;
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(pEContext);
		}
	}

}
