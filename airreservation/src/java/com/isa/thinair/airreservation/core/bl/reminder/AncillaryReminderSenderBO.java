package com.isa.thinair.airreservation.core.bl.reminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryStatusDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.FlightSegReminderNotificationDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.NotificationDetailInfoDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.PnrFlightInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AncillaryReminderBL;
import com.isa.thinair.airreservation.core.bl.common.PnrZuluProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class AncillaryReminderSenderBO {

	private final Log log = LogFactory.getLog(AncillaryReminderSenderBO.class);

	private final Map<Integer, String> flightSegmentSeatMap = new HashMap<Integer, String>();
	private final Map<Integer, String> flightSegmentMealMap = new HashMap<Integer, String>();
	private final Map<Integer, String> flightSegmentBaggageMap = new HashMap<Integer, String>();

	public void sendAncillaryReminderNotification(AncillaryReminderDetailDTO reminderDetailDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("Inside sendAncillaryReminderNotification() --- With Flight Segment <> "
					+ reminderDetailDTO.getFlightSegId());
		}

		List<PnrFlightInfoDTO> pnrInfoList;
		List<NotificationDetailInfoDTO> pnrsListTosendReminder = new ArrayList<NotificationDetailInfoDTO>();
		Integer flightSegmentId = reminderDetailDTO.getFlightSegId();
		Integer fltSegEventId = null;
		boolean success = false;
		Throwable exceptionToAudit = null;

		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		String airportOnlineCheckin = reminderDetailDTO.getAirportOnlineCheckin();
		Integer ancillaryCutOverStartTime = reminderDetailDTO.getAnciNotificationStartCutOverTime();
		String ibeURL = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL);
		String secureServiceAppIBEUrl = AppSysParamsUtil.getSecureServiceAppIBEUrl();
		String appParamCarLink = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.RENT_A_CAR_URL);
		String appParamHotelLink = AppSysParamsUtil.getHolidaysAncilaryReminderURL();
		Date flightDepTimeZulu = reminderDetailDTO.getDepartureTimeZulu();

		boolean showCarBookingLink = (appParamCarLink != null && !appParamCarLink.trim().equals("")) ? true : false;
		boolean showHoteBookinglLink = (appParamHotelLink != null && !appParamHotelLink.trim().equals("")) ? true : false;
		boolean showAPOnlineCheckInLink = reminderDetailDTO.getAirportOnlineCheckin().equalsIgnoreCase("Y") ? true : false;

		try {
			if (log.isDebugEnabled()) {
				log.debug("AncillaryReminder:::ReservationServiceBean::Check For Flight Segemnt Ancillary  --- Flight Segment ---> "
						+ reminderDetailDTO.getFlightSegId());
			}
			// Step 1: Check for Ancillary Avalibility for the given flight segment & ancillary selection cut over time
			flightSegmentSeatMap.put(flightSegmentId, checkSeatAvalibilityForFlightSeg(flightSegmentId, flightDepTimeZulu));
			flightSegmentMealMap.put(flightSegmentId, checkMealAvalibilityForFlightSeg(flightSegmentId, flightDepTimeZulu));
			flightSegmentBaggageMap.put(flightSegmentId, checkBaggageAvalibilityForFlightSeg(flightSegmentId, flightDepTimeZulu));

			// if(seatAvailableForFlightSegment || mealAvailableForFlightSegment){
			// Step 2: Update the Flight Segment Status
			// First Check for existing segment with FAILED AND NOTSENT STATUS
			// And Update Status as 'NOTSENT'
			// REMOVE THIS NOTSENT Status
			/*
			 * fltSegEventId = updateFlightSegmentNotifyStatus(flightSegmentId,
			 * reminderDetailDTO.getFlightSegmentNotificationEventId(),
			 * ReservationInternalConstants.FlightSegNotifyEvent.NOTSENT, "");
			 */

			// Step 3: Get the list of Pnr For the given Flight Segment starting the origin Airport
			pnrInfoList = (List<PnrFlightInfoDTO>) reservationDAO.getPNRListFromFlightSegment(flightSegmentId);
			if (log.isDebugEnabled()) {
				log.debug("AncillaryReminder:::ReservationServiceBean::  --- Got Pnr List .");
			}

			// Step 4: Update Flight Segment Status as 'INPROGRESS'
			if (pnrInfoList != null && pnrInfoList.size() > 0) {
				fltSegEventId = flightBD.updateFlightSegmentNotifyStatus(flightSegmentId,
						reminderDetailDTO.getFlightSegmentNotificationEventId(),
						ReservationInternalConstants.FlightSegNotifyEvent.INPROGRESS, "", 0,
						ReservationInternalConstants.NotificationType.ANCI_REMINDER);
				if (log.isDebugEnabled()) {
					log.debug("AncillaryReminder:::ReservationServiceBean::  --- UPDATED FLIGHT SEGMENT STATUS AS 'INPOGRCESS'.");
				}
			}

			// Step 5: Iterate each PNR and check its segment to send ancillary reminder
			for (PnrFlightInfoDTO pnrInfo : pnrInfoList) {
				String pnr = pnrInfo.getPnr();

				if (log.isDebugEnabled()) {
					log.debug("AncillaryReminder:::ReservationServiceBean::   Iteration For PNR ---- " + pnr);
				}
				NotificationDetailInfoDTO notificationDetailInfoDTO = new NotificationDetailInfoDTO();
				String ibePnrModificationLink = "";
				List<AncillaryStatusDTO> ancStatusList = (List<AncillaryStatusDTO>) reservationDAO.getPnrAncillaryStatus(pnr);
				if (ancStatusList != null && ancStatusList.size() > 0) {
					notificationDetailInfoDTO = reservationDAO.getPnrSegListToSendReminder(ancStatusList);

					notificationDetailInfoDTO.setAirportOnlineCheckin(airportOnlineCheckin);
					notificationDetailInfoDTO.setAncillaryCutOverStartTime(ancillaryCutOverStartTime);
					notificationDetailInfoDTO.setFlightSegmentId(flightSegmentId);
					notificationDetailInfoDTO.setFlightSegNotificationEventId(fltSegEventId);
					notificationDetailInfoDTO.setCarBookingLink(createRentACarLink(appParamCarLink,
							notificationDetailInfoDTO.getFlightSegmentsList()));
					notificationDetailInfoDTO.setDynamicCarLink(AppSysParamsUtil.isRentACarLinkDynamic());

					notificationDetailInfoDTO.setQrtzJobName(reminderDetailDTO.getJobName());
					notificationDetailInfoDTO.setQrtzJobGroupName(reminderDetailDTO.getJobGroupName());
					notificationDetailInfoDTO.setFirstSegFlightDepDateTime(pnrInfo.getFirstSegDepDate());

					notificationDetailInfoDTO.setIbeURL(ibeURL);
					
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(pnr);
					pnrModesDTO.setLoadSegView(true);
					pnrModesDTO.setLoadLocalTimes(true);
					pnrModesDTO.setLoadFares(true);
					Reservation reservation = PnrZuluProxy.getReservationWithLocalTimes(pnrModesDTO, credentialsDTO);

					SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
					Date d = sdf.parse(notificationDetailInfoDTO.getFirstFlightSegDepDateTime("dd/MM/yyyy"));
					sdf.applyPattern("yyyy-mm-dd");
					String departureDateNew = sdf.format(d);

					StringBuilder urlBuild = new StringBuilder();
					urlBuild.append(secureServiceAppIBEUrl)
							.append("/ibe/reservation.html#/modify/reservation/")
							.append((new StringBuilder()).append(notificationDetailInfoDTO.getIteneraryLanguage().toUpperCase()).append("/")
							.append(reservation.getLastCurrencyCode()).append("//")
							.append(pnr).append("/").append(notificationDetailInfoDTO.getLastName()).append("/")
							.append(departureDateNew).toString());
				
					ibePnrModificationLink = urlBuild.toString();
					notificationDetailInfoDTO.setIbePnrModificationLink(ibePnrModificationLink);

					int pnrSegmentsCount = notificationDetailInfoDTO.getFlightSegmentsList().size();
					boolean seatReminderReq = false;
					boolean mealReminderReq = false;
					boolean insuReminderReq = false;
					boolean busReminderReq = false;
					boolean baggageReminderReq = false;

					boolean seatInventoryAvail = false;
					boolean mealInventoryAvail = false;
					boolean baggageInventoryAvail = false;

					if (pnrSegmentsCount > 0) {
						if (log.isDebugEnabled()) {
							log.debug("AncillaryReminder:::ReservationServiceBean::   PNR Identified to send email ---  PNR ---- "
									+ pnr);
						}

						// Check For Insurance Availability for the given PNR for a journey
						if (notificationDetailInfoDTO.getInsuranceStatus() != null
								&& notificationDetailInfoDTO.getInsuranceStatus().equals("N")) {
							if (AncillaryReminderBL.checkInsuranceStatus(notificationDetailInfoDTO)) {
								insuReminderReq = true;
							}
						}
						if (log.isDebugEnabled()) {
							log.debug("AncillaryReminder:::ReservationServiceBean::   Checked For Insurance ");
						}

						// Check bus reminder status for given PNR
						if (notificationDetailInfoDTO.getBusReminderReq() != null
								&& notificationDetailInfoDTO.getBusReminderReq().equals("Y")) {
							busReminderReq = true;
						}

						if (log.isDebugEnabled()) {
							log.debug("AncillaryReminder:::ReservationServiceBean::   Checked For Bus reminders ");
						}

						// Retrieve Flight Segment Details
						AncillaryReminderBL.getSegmentsDetails(notificationDetailInfoDTO.getFlightSegmentsList());

						// Retrieve Airport check-in times.
						AncillaryReminderBL.getCheckInTimes(notificationDetailInfoDTO.getFlightSegmentsList());

						if (notificationDetailInfoDTO.getOrigin() != null && !notificationDetailInfoDTO.getOrigin().isEmpty()) {
							notificationDetailInfoDTO.setOriginDescription(ReservationModuleUtils.getAirportBD()
									.getAirport(notificationDetailInfoDTO.getOrigin()).getAirportName());
						} else {
							if (log.isDebugEnabled()) {
								log.debug("AncillaryReminder:::ReservationServiceBean:: Null Check for Airport ID.Airport ID value is----  "
										+ notificationDetailInfoDTO.getOrigin());
							}
							continue;

						}
						notificationDetailInfoDTO.setDestinationDescription(ReservationModuleUtils.getAirportBD()
								.getAirport(notificationDetailInfoDTO.getDestination()).getAirportName());

						notificationDetailInfoDTO.setHotelBookingLink(createHotelLink(appParamHotelLink,
								notificationDetailInfoDTO.getDestination()));

						// Check For Seat Inventory Available
						for (FlightSegReminderNotificationDTO segReminderNotificationDTO : notificationDetailInfoDTO
								.getFlightSegmentsList()) {
							if (isSeatAvailableInInventory(segReminderNotificationDTO)) {
								seatInventoryAvail = true;
								break;
							}
						}

						// Check For Meal Inventory Available
						for (FlightSegReminderNotificationDTO segReminderNotificationDTO : notificationDetailInfoDTO
								.getFlightSegmentsList()) {
							if (isMealAvailableInInventory(segReminderNotificationDTO)) {
								mealInventoryAvail = true;
								break;
							}
						}

						// Check For Baggage Inventory Available
						for (FlightSegReminderNotificationDTO segReminderNotificationDTO : notificationDetailInfoDTO
								.getFlightSegmentsList()) {
							if (isBaggageAvailableInInventory(segReminderNotificationDTO)) {
								baggageInventoryAvail = true;
								break;
							}
						}

						for (FlightSegReminderNotificationDTO segReminderNotificationDTO : notificationDetailInfoDTO
								.getFlightSegmentsList()) {
							if (segReminderNotificationDTO.getMealSelectionStatus().equals(
									ReservationInternalConstants.SegmentAncillaryStatus.ALL)) {
								mealReminderReq = false;
							} else if ((segReminderNotificationDTO.getMealSelectionStatus().equals(
									ReservationInternalConstants.SegmentAncillaryStatus.NONE) || segReminderNotificationDTO
									.getMealSelectionStatus().equals(ReservationInternalConstants.SegmentAncillaryStatus.PARTIAL))
									&& mealInventoryAvail) {
								mealReminderReq = true;
								break;
							}
						}

						for (FlightSegReminderNotificationDTO segReminderNotificationDTO : notificationDetailInfoDTO
								.getFlightSegmentsList()) {
							if (segReminderNotificationDTO.getBaggageSelectionStatus().equals(
									ReservationInternalConstants.SegmentAncillaryStatus.ALL)) {
								baggageReminderReq = false;
							} else if ((segReminderNotificationDTO.getBaggageSelectionStatus().equals(
									ReservationInternalConstants.SegmentAncillaryStatus.NONE) || segReminderNotificationDTO
									.getBaggageSelectionStatus().equals(
											ReservationInternalConstants.SegmentAncillaryStatus.PARTIAL))
									&& baggageInventoryAvail) {
								baggageReminderReq = true;
								break;
							}
						}

						for (FlightSegReminderNotificationDTO segReminderNotificationDTO : notificationDetailInfoDTO
								.getFlightSegmentsList()) {
							if (segReminderNotificationDTO.getSeatSelectionStatus().equals(
									ReservationInternalConstants.SegmentAncillaryStatus.ALL)) {
								seatReminderReq = false;
							} else if ((segReminderNotificationDTO.getSeatSelectionStatus().equals(
									ReservationInternalConstants.SegmentAncillaryStatus.NONE) || segReminderNotificationDTO
									.getSeatSelectionStatus().equals(ReservationInternalConstants.SegmentAncillaryStatus.PARTIAL))
									&& seatInventoryAvail) {
								seatReminderReq = true;
								break;
							}
						}

						notificationDetailInfoDTO.setFlightSegNotificationEventId(fltSegEventId);

						if (seatReminderReq) {
							notificationDetailInfoDTO.setSeatReminderReq("Y");
						} else {
							notificationDetailInfoDTO.setSeatReminderReq("N");
						}

						if (mealReminderReq) {
							notificationDetailInfoDTO.setMealReminderReq("Y");
						} else {
							notificationDetailInfoDTO.setMealReminderReq("N");
						}

						if (baggageReminderReq) {
							notificationDetailInfoDTO.setBaggageReminderReq("Y");
						} else {
							notificationDetailInfoDTO.setBaggageReminderReq("N");
						}

						if (insuReminderReq) {
							notificationDetailInfoDTO.setInsReminderReq("Y");
						} else {
							notificationDetailInfoDTO.setInsReminderReq("N");
						}

						if (showCarBookingLink || showHoteBookinglLink || showAPOnlineCheckInLink || seatReminderReq
								|| mealReminderReq || baggageReminderReq || insuReminderReq || busReminderReq) {
							pnrsListTosendReminder.add(notificationDetailInfoDTO);
						}

						if (log.isDebugEnabled()) {
							log.debug("Preparing ancillary reminder content. " + " [PNR=" + pnr + ", noOfDays="
									+ notificationDetailInfoDTO.getDaysRemainingInflights() + ", nextDepLocal="
									+ notificationDetailInfoDTO.getFlightDepartureTimeLocal() + ", showCheckin="
									+ showAPOnlineCheckInLink + ", seatReminder=" + seatReminderReq + ", mealReminder="
									+ mealReminderReq + ", baggageReminder=" + baggageReminderReq + ", insuReminder="
									+ insuReminderReq + ", busReminder=" + busReminderReq + "]");
						}
					}
				}
			}

			// Send Email to list of Pnrs
			if (pnrsListTosendReminder.size() > 0) {
				log.info("AncillaryReminder:::ReservationServiceBean::   Send Email For Ancillary Reminder ");
				AncillaryReminderBL.emailAncillaryReminder(pnrsListTosendReminder, credentialsDTO);
			}

			success = true;
		} catch (ModuleException exception) {
			log.error(" ((o)) ModuleException::sendAncillaryReminderNotification ", exception);
			exceptionToAudit = exception;
			throw new ModuleException(exception, exception.getExceptionCode(), exception.getModuleCode());
		} catch (CommonsDataAccessException exception) {
			log.error(" ((o)) ModuleException::sendAncillaryReminderNotification ", exception);
			exceptionToAudit = exception;
			throw new ModuleException(exception, exception.getExceptionCode(), exception.getModuleCode());
		} catch (Exception exception) {
			log.error(" ((o)) ModuleException::sendAncillaryReminderNotification ", exception);
			exceptionToAudit = exception;
		} finally {
			if (!success) {
				log.info("AncillaryReminder:::ReservationServiceBean::   Update Status for Flight by 'FAILED' inside Finally ");
				// Update Flight Segment Status as 'FAILED'
				flightBD.updateFlightSegmentNotifyStatus(flightSegmentId, fltSegEventId,
						ReservationInternalConstants.FlightSegNotifyEvent.FAILED, exceptionToAudit.getMessage(), 0, null);
			}
		}
	}

	public static String createRentACarLink(String carLink, List<FlightSegReminderNotificationDTO> flightSegmentsList)
			throws ModuleException {
		if (carLink.length() > 0 && AppSysParamsUtil.isShowRentACarLink()) {
			if (AppSysParamsUtil.isRentACarLinkDynamic()) {
				ArrayList<FlightSegReminderNotificationDTO> flightDepDetails = new ArrayList<FlightSegReminderNotificationDTO>();
				ArrayList<FlightSegReminderNotificationDTO> flightRetDetails = new ArrayList<FlightSegReminderNotificationDTO>();

				for (FlightSegReminderNotificationDTO flightSegNotify : flightSegmentsList) {
					if (flightSegNotify.getReturnFlag().equals("true")) {
						flightRetDetails.add(flightSegNotify);
					} else {
						flightDepDetails.add(flightSegNotify);
					}
				}
				String pkDateTime = "";
				String rtDateTime = "";
				String locCode = "";
				String residencyId = "";
				String lang = "EN";
				if (flightDepDetails != null && flightDepDetails.size() > 0) {
					FlightSegReminderNotificationDTO lastOutboundSeg = flightDepDetails.get(flightDepDetails.size() - 1);
					if (lastOutboundSeg != null) {
						pkDateTime = formatDateTimeYYYYMMDDHHMM(lastOutboundSeg.getArrivalDateTime()); // make 2 hours
						// after arrival
						// time
						String[] locCodes = lastOutboundSeg.getSegmentCode().split("/");
						locCode = locCodes[locCodes.length - 1];
						Collection<String> colAirPortCodes = new ArrayList<String>();
						colAirPortCodes.add(locCode);
						Map<String, CachedAirportDTO> mapAirports = new HashMap<String, CachedAirportDTO>();
						if (colAirPortCodes.size() != mapAirports.size()) {
							mapAirports = ReservationModuleUtils.getAirportBD().getCachedAllAirportMap(colAirPortCodes);
							residencyId = mapAirports.get(locCode) != null ? mapAirports.get(locCode).getCountryCode() : "";
						}
					}
				}
				if (flightRetDetails != null && flightRetDetails.size() > 0) {
					FlightSegReminderNotificationDTO firstInboundSeg = flightRetDetails.get(0);
					if (firstInboundSeg != null) {
						rtDateTime = formatDateTimeYYYYMMDDHHMM(firstInboundSeg.getDepartureDateTime());
					}
				}
				if (rtDateTime.equals("")) {
					rtDateTime = pkDateTime;
				}
				carLink = carLink + "?clientId=" + AppSysParamsUtil.getRentACarAncillaryClientId() + "&pkDateTime=" + pkDateTime
						+ "&rtDateTime=" + rtDateTime + "&locCode=" + locCode + "&lang=" + lang + "&residencyId=" + residencyId;
			}
			return carLink;
		} else {
			return ""; // should be empty for UI validation
		}
	}

	public static String createHotelLink(String hotelLink, String destinationAirport) throws ModuleException {
		String[] seg = new String[1];
		seg[0] = destinationAirport;
		String destination[] = SelectListGenerator.getUfiCity(seg);
		if (destination != null && destination.length == 3) {
			if (destination[2] != null && destination[2].length() > 0) {
				hotelLink = destination[2];
			}
		}
		return hotelLink;
	}

	public static String formatDateTimeYYYYMMDDHHMM(Date date) {
		if (date != null) {
			SimpleDateFormat formetter = new SimpleDateFormat("yyyyMMddHHmm");
			return formetter.format(date);
		} else {
			return null;
		}
	}

	private boolean isSeatAvailableInInventory(FlightSegReminderNotificationDTO segReminderNotificationDTO)
			throws ModuleException {
		boolean seatAvailable = false;

		if (flightSegmentSeatMap.get(segReminderNotificationDTO.getFlightSegmentId()) != null) {
			seatAvailable = true;
		} else {
			String seatAvalabletyStatus = checkSeatAvalibilityForFlightSeg(segReminderNotificationDTO.getFlightSegmentId(),
					segReminderNotificationDTO.getDepartureDateTimeZulu());
			flightSegmentSeatMap.put(segReminderNotificationDTO.getFlightSegmentId(), seatAvalabletyStatus);
			seatAvailable = flightSegmentSeatMap.get(segReminderNotificationDTO.getFlightSegmentId()) != null ? true : false;
		}

		return seatAvailable;
	}

	private boolean isMealAvailableInInventory(FlightSegReminderNotificationDTO segReminderNotificationDTO)
			throws ModuleException {
		boolean mealAvailable = false;

		if (flightSegmentMealMap.get(segReminderNotificationDTO.getFlightSegmentId()) != null) {
			mealAvailable = true;
		} else {
			String mealAvalabletyStatus = checkMealAvalibilityForFlightSeg(segReminderNotificationDTO.getFlightSegmentId(),
					segReminderNotificationDTO.getDepartureDateTimeZulu());
			flightSegmentMealMap.put(segReminderNotificationDTO.getFlightSegmentId(), mealAvalabletyStatus);
			mealAvailable = flightSegmentMealMap.get(segReminderNotificationDTO.getFlightSegmentId()) != null ? true : false;
		}

		return mealAvailable;
	}

	private boolean isBaggageAvailableInInventory(FlightSegReminderNotificationDTO segReminderNotificationDTO)
			throws ModuleException {
		boolean baggageAvailable = false;

		if (flightSegmentBaggageMap.get(segReminderNotificationDTO.getFlightSegmentId()) != null) {
			baggageAvailable = true;
		} else {
			String baggageAvalabletyStatus = checkBaggageAvalibilityForFlightSeg(segReminderNotificationDTO.getFlightSegmentId(),
					segReminderNotificationDTO.getDepartureDateTimeZulu());
			flightSegmentBaggageMap.put(segReminderNotificationDTO.getFlightSegmentId(), baggageAvalabletyStatus);
			baggageAvailable = flightSegmentBaggageMap.get(segReminderNotificationDTO.getFlightSegmentId()) != null
					? true
					: false;
		}

		return baggageAvailable;
	}

	private static String checkSeatAvalibilityForFlightSeg(Integer flightSegmentID, Date flightDeptTimeZulu)
			throws ModuleException {
		SeatMapBD seatMapBD = ReservationModuleUtils.getSeatMapBD();
		String seatAvailable = null;
		Collection<SeatDTO> seatCollection = null;
		Map<Integer, FlightSeatsDTO> availableSeatMap = new HashMap<Integer, FlightSeatsDTO>();
		List<Integer> flightSegIds = new ArrayList<Integer>();
		flightSegIds.add(flightSegmentID);

		availableSeatMap = seatMapBD.getFlightSeats(flightSegIds, null, false);
		FlightSeatsDTO flightSeatsDTO = availableSeatMap.get(flightSegmentID);
		if (flightSeatsDTO != null) {
			seatCollection = flightSeatsDTO.getSeats();
		}

		if (seatCollection != null && seatCollection.size() > 0) {
			seatAvailable = ReservationApiUtils.isServiceAvailable(ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP, flightDeptTimeZulu)
					? "Y"
					: null;
		}

		return seatAvailable;
	}

	private static String checkMealAvalibilityForFlightSeg(Integer flightSegmentID, Date flightDeptTimeZulu)
			throws ModuleException {
		MealBD mealBD = ReservationModuleUtils.getMealBD();
		String mealAvailable = null;
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		List<FlightMealDTO> mealCollection = null;
		Map<Integer, List<FlightMealDTO>> availableMealMap = new HashMap<Integer, List<FlightMealDTO>>();

		flightSegIdWiseCos.put(flightSegmentID, null);
		availableMealMap = mealBD.getMeals(flightSegIdWiseCos, null, false, null, SalesChannelsUtil.SALES_CHANNEL_WEB);
		mealCollection = availableMealMap.get(flightSegmentID);

		if (mealCollection != null && mealCollection.size() > 0) {
			mealAvailable =ReservationApiUtils.isServiceAvailable(ReservationInternalConstants.SERVICE_CALLER.MEAL, flightDeptTimeZulu) ? "Y" : null;
		}

		return mealAvailable;
	}

	private static String checkBaggageAvalibilityForFlightSeg(Integer flightSegmentID, Date flightDeptTimeZulu)
			throws ModuleException {
		BaggageBusinessDelegate baggageBD = ReservationModuleUtils.getBaggageBD();
		String baggageAvailable = null;
		Collection<FlightBaggageDTO> baggageCollection = null;
		Map<Integer, List<FlightBaggageDTO>> availableBaggageMap = new HashMap<Integer, List<FlightBaggageDTO>>();

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		flightSegIdWiseCos.put(flightSegmentID, null);

		// TODO Need to verify this for ond baggages
		availableBaggageMap = baggageBD.getBaggages(flightSegIdWiseCos, false, false);
		baggageCollection = availableBaggageMap.get(flightSegmentID);

		if (baggageCollection != null && baggageCollection.size() > 0) {
			baggageAvailable = ReservationApiUtils.isServiceAvailable(ReservationInternalConstants.SERVICE_CALLER.BAGGAGE, flightDeptTimeZulu)
					? "Y"
					: null;
		}

		return baggageAvailable;
	}

}
