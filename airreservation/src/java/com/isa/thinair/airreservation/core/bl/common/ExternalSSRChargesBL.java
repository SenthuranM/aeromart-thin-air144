/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Reservation related external credit card charges business logic implementation
 * 
 * @author Dumindag
 * 
 * @since 1.0
 */
public class ExternalSSRChargesBL {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ExternalSSRChargesBL.class);

	/**
	 * Hide the constructor
	 */
	private ExternalSSRChargesBL() {

	}

	/**
	 * Reflect external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 *            TODO
	 * @param reservationDiscountDTO
	 * @param externalChgDTO
	 * @throws ModuleException
	 */
	public static void reflectExternalChgsForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		// Apply these changes for the reservation
		applyExternalChargesForAFreshBooking(reservation, credentialsDTO, paxDiscInfo, reservationDiscountDTO);
	}

	/**
	 * Applies the external charges for a fresh booking
	 * 
	 * @param reservation
	 * @param paxDiscInfo
	 *            TODO
	 * @param reservationDiscountDTO
	 * @param colExternalChgDTO
	 * @throws ModuleException
	 */
	private static void applyExternalChargesForAFreshBooking(Reservation reservation, CredentialsDTO credentialsDTO,
			PaxDiscountInfoDTO paxDiscInfo, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Start applyExternalChargesForAFreshBooking");
		}
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs = null;
		boolean updateExist = false;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			ccExternalChgs = new ArrayList<ExternalChgDTO>();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				paymentAssembler = (PaymentAssembler) reservationPax.getPayment();

				Collection<ExternalChgDTO> ccAPSExternalChgs = paymentAssembler
						.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
				Collection<ExternalChgDTO> ccIFSExternalChgs = paymentAssembler
						.getPerPaxExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);

				if (ccAPSExternalChgs != null && ccAPSExternalChgs.size() > 0) {
					ccExternalChgs.addAll(ccAPSExternalChgs);
				}

				if (ccIFSExternalChgs != null && ccIFSExternalChgs.size() > 0) {
					ccExternalChgs.addAll(ccIFSExternalChgs);
				}

				if (ccExternalChgs.size() > 0) {
					Collection<DiscountChargeTO> discountChargeTOs = null;
					if (reservationDiscountDTO != null) {
						discountChargeTOs = reservationDiscountDTO.getPaxDiscountChargeTOs(reservationPax.getPaxSequence());
					}
					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						SSRExternalChargeDTO ssrExternalChgDTO = (SSRExternalChargeDTO) externalChgDTO;
						reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
								FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrExternalChgDTO.getFlightRPH()), true);

						ReservationApiUtils.updatePaxOndExternalChargeDiscountInfo(paxDiscInfo, externalChgDTO, discountChargeTOs,
								reservationPax.getPaxSequence());

						ReservationCoreUtils.captureReservationPaxOndCharge(ssrExternalChgDTO.getAmount(), null,
								ssrExternalChgDTO.getChgRateId(), ssrExternalChgDTO.getChgGrpCode(), reservationPaxFare,
								credentialsDTO, false, paxDiscInfo, null, null, 0);
						updateExist = true;
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}
	}

	public static void reflectExternalChgForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, Map<Integer, RevenueDTO> passengerRevenueMap,
			CredentialsDTO credentialsDTO, Map<Integer, List<SSRExternalChargeDTO>> anciMap, ChargeTnxSeqGenerator chgTxnGen)
			throws ModuleException {
		// Apply these changes for the reservation

		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = applyExternalChargesForANewSegment(reservation,
				pnrPaxIdAndPayments, credentialsDTO, anciMap, chgTxnGen);

		// Apply these changes to the passenger payment map
		addExternalChgsForPaymentAssembler(mapPnrPaxIdAdjustments, pnrPaxIdAndPayments);

		// Apply these changes to the passenger revenue map
		addExternalChgsForRevenueMap(mapPnrPaxIdAdjustments, passengerRevenueMap);

	}

	/**
	 * Apply external charges for a new segment
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param chgTxnGen
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, List<ReservationPaxOndCharge>> applyExternalChargesForANewSegment(Reservation reservation,
			Map<Integer, PaymentAssembler> pnrPaxIdAndPayments, CredentialsDTO credentialsDTO,
			Map<Integer, List<SSRExternalChargeDTO>> anciMap, ChargeTnxSeqGenerator chgTxnGen) throws ModuleException {
		Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments = new HashMap<Integer, List<ReservationPaxOndCharge>>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		PaymentAssembler paymentAssembler;
		Collection<ExternalChgDTO> ccExternalChgs = null;
		boolean updateExist = false;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			ccExternalChgs = new ArrayList<ExternalChgDTO>();

			if (pnrPaxIdAndPayments.containsKey(reservationPax.getPnrPaxId())) {
				paymentAssembler = (PaymentAssembler) pnrPaxIdAndPayments.get(reservationPax.getPnrPaxId());

				// Get external charges related to airport services
				Collection<ExternalChgDTO> ccAPserviceExternalChgs = paymentAssembler
						.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
				if (ccAPserviceExternalChgs != null && ccAPserviceExternalChgs.size() > 0) {
					ccExternalChgs.addAll(ccAPserviceExternalChgs);
				}

				// Get external charges related to in-flight services
				Collection<ExternalChgDTO> ccInflightServiceExternalChgs = paymentAssembler
						.getPerPaxExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
				if (ccInflightServiceExternalChgs != null && ccInflightServiceExternalChgs.size() > 0) {
					ccExternalChgs.addAll(ccInflightServiceExternalChgs);
				}

				if (ccExternalChgs.size() > 0) {
					ReservationSSRUtil.addSSRsForExternalCharges(reservationPax, ccExternalChgs, anciMap,
							credentialsDTO.getSalesChannelCode());

					for (ExternalChgDTO externalChgDTO : ccExternalChgs) {
						SSRExternalChargeDTO ssrxternalChargeDTO = (SSRExternalChargeDTO) externalChgDTO;
						reservationPaxFare = ReservationCoreUtils.getReservationPaxFare(reservationPax.getPnrPaxFares(),
								FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrxternalChargeDTO.getFlightRPH()), true);
						ReservationPaxOndCharge reservationPaxOndCharge = ReservationCoreUtils.captureReservationPaxOndCharge(
								ssrxternalChargeDTO.getAmount(), null, ssrxternalChargeDTO.getChgRateId(),
								ssrxternalChargeDTO.getChgGrpCode(), reservationPaxFare, credentialsDTO, false, null, null, null,
								chgTxnGen.getTnxSequence(reservationPax.getPnrPaxId()));
						lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(reservationPax.getPnrPaxId());

						if (lstReservationPaxOndCharge == null) {
							lstReservationPaxOndCharge = new ArrayList<ReservationPaxOndCharge>();
							lstReservationPaxOndCharge.add(reservationPaxOndCharge);

							mapPnrPaxIdAdjustments.put(reservationPax.getPnrPaxId(), lstReservationPaxOndCharge);
						} else {
							lstReservationPaxOndCharge.add(reservationPaxOndCharge);
						}

						updateExist = true;
					}
				}
			}
		}

		if (updateExist) {
			// Set reservation and passenger total amounts
			ReservationCoreUtils.setPnrAndPaxTotalAmounts(reservation);
		}

		return mapPnrPaxIdAdjustments;
	}

	/**
	 * Add external charges for payment assembler
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerPayment
	 */
	private static void addExternalChgsForPaymentAssembler(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, PaymentAssembler> passengerPayment) {
		Iterator<Integer> itPnrPaxIds = passengerPayment.keySet().iterator();
		PaymentAssembler paymentAssembler;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				paymentAssembler = (PaymentAssembler) passengerPayment.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
					paymentAssembler.setTotalChargeAmount(AccelAeroCalculator.add(paymentAssembler.getTotalChargeAmount(),
							reservationPaxOndCharge.getAmount()));
				}
			}
		}
	}

	/**
	 * Add external charges for the revenue information
	 * 
	 * @param mapPnrPaxIdAdjustments
	 * @param passengerRevenueMap
	 */
	private static void addExternalChgsForRevenueMap(Map<Integer, List<ReservationPaxOndCharge>> mapPnrPaxIdAdjustments,
			Map<Integer, RevenueDTO> passengerRevenueMap) {
		Iterator<Integer> itPnrPaxIds = passengerRevenueMap.keySet().iterator();
		RevenueDTO revenueDTO;
		Integer pnrPaxId;
		List<ReservationPaxOndCharge> lstReservationPaxOndCharge;

		while (itPnrPaxIds.hasNext()) {
			pnrPaxId = (Integer) itPnrPaxIds.next();

			if (mapPnrPaxIdAdjustments.keySet().contains(pnrPaxId)) {
				revenueDTO = (RevenueDTO) passengerRevenueMap.get(pnrPaxId);
				lstReservationPaxOndCharge = mapPnrPaxIdAdjustments.get(pnrPaxId);

				for (ReservationPaxOndCharge reservationPaxOndCharge : lstReservationPaxOndCharge) {
					revenueDTO.setAddedTotal(
							AccelAeroCalculator.add(revenueDTO.getAddedTotal(), reservationPaxOndCharge.getAmount()));
					revenueDTO.getAddedCharges().add(reservationPaxOndCharge);
				}
			}
		}
	}
}