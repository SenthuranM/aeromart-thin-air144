/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;

/**
 * @author udithad
 *
 */
public abstract class PnlAdlAbstractDataFactory<K extends BasePassengerCollection, T extends BaseDataContext> {
	public abstract MessageDataStructure createDataStructure(String messageType);
}
