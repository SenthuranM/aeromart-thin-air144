/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.core.bl.passenger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.airreservation.core.bl.common.AutoCancellationBO;
import com.isa.thinair.airreservation.core.bl.common.ExternalGenericChargesBL;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.bl.ticketing.TicketingUtils;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for making an infant This will create reservation required objects structure inorder to persist in the
 * database
 * 
 * Business Rules: (1) Infant charges are kept in infant level. (2) No fare for an infant. (3) Infant belongs to only
 * one parent at a given time (4) Switching a parent is allowed.
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="makeInfant"
 */
public class MakeInfant extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(MakeInfant.class);

	/** Holds the payment types */
	private Integer pnrPaymentTypes;

	/** Holds the credentials information */
	private CredentialsDTO credentialsDTO;

	/** Holds the confirm pnr passenger ids */
	private final Collection<Integer> confirmPnrPaxIds = new ArrayList<Integer>();

	/**
	 * Execute method of the MakeInfant command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection colOndFareDTOs = (Collection) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		Collection<ReservationPax> colReservationPax = (Collection) this.getParameter(CommandParamNames.INFANTS);
		Integer pnrPaymentTypes = (Integer) this.getParameter(CommandParamNames.PNR_PAYMENT_TYPES);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Collection blockIds = (Collection) this.getParameter(CommandParamNames.BLOCK_KEY_IDS);
		Map passengerPaymentMap = (Map) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps = (Map) this
				.getParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS);
		Collection colPaymentInfo = (Collection) this.getParameter(CommandParamNames.PAYMENT_INFO);
		Map<Integer, Map<Integer, EticketDTO>> eTicketInfo = (Map) this.getParameter(CommandParamNames.E_TICKET_MAP);
		Boolean isGoShowProcess = (Boolean) this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS);
		Boolean isOtherCarrierPaxCreditUse = (Boolean) this.getParameter(CommandParamNames.OTHER_CARRIER_CREDIT_USED);
		AutoCancellationInfo autoCancellationInfo = this.getParameter(CommandParamNames.AUTO_CANCELLATION_INFO,
				AutoCancellationInfo.class);

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		// Checking params
		this.validateParams(pnr, colReservationPax, colOndFareDTOs, pnrPaymentTypes, blockIds, passengerPaymentMap, version,
				credentialsDTO);

		// Set the arguments
		this.setArguments(pnrPaymentTypes, credentialsDTO);

		// Loads the reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadEtickets(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		List<Integer> forcedConfirmedPaxSeqsBeforeChange = ReservationApiUtils.getForcedConfirmedPaxSeqs(reservation);

		// original reservation status
		String originalReservationStatus = reservation.getStatus();

		// Check any add infant constraints
		this.checkAddInfantConstraints(reservation, passengerPaymentMap, version);

		// Build the segment objects Map
		Map<Integer, ReservationSegment> segObjectsMap = this.buildSegmentObjectsMap(reservation);

		ChargeTnxSeqGenerator tnxGen = new ChargeTnxSeqGenerator(reservation);
		// build the infants
		Collection<Integer> parentIds = this.buildInfants(reservation, colReservationPax, colOndFareDTOs, segObjectsMap,
				passengerPaymentMap, credentialsDTO, tnxGen);

		// Save auto cancellation information
		AutoCancellationBO.saveAutoCancellationInfo(autoCancellationInfo, pnr, credentialsDTO);

		// Add auto cancellation information to infant
		this.addAutoCancellationInfo(colReservationPax, autoCancellationInfo);

		// Adds any external charges for confirmed passengers
		this.addExternalCharges(reservation, passengerPaymentMap, credentialsDTO, colReservationPax, tnxGen);

		// adds ssrs to infants
		addPaxSegmentSSRS(colReservationPax, paxSegmentSSRDTOMaps);

		// Leave the states as it is
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {

			if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS.equals(pnrPaymentTypes)
					&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
				// If it's confirmed get in the ownership
				this.captureOwnerShip(reservation);
			}

			// Change infant states
			this.changeInfantStates(reservation);
		} else {
			// Updates the reservation and the passenger states
			this.updateStates(reservation, passengerPaymentMap);

			// Process force mode
			this.checkForceMode(reservation);
		}

		// Check Restricted Segment constraints
		ValidationUtils.checkRestrictedSegmentConstraints(colOndFareDTOs, reservation.getStatus(), pnrPaymentTypes);

		// Tracks PNL/ADL Statistics message
		// TODO remove and refactor this
		ADLNotifyer.updateStatusToCHG(reservation);
		// Saves the reservation
		ReservationProxy.saveReservation(reservation);

		replaceParentIdFromInfantIdInPaymentMap(colReservationPax, passengerPaymentMap, isGoShowProcess,
				reservation.isInfantPaymentRecordedWithInfant());

		ADLNotifyer.recordReservation(reservation, AdlActions.D, getEtGenerationEligiblePaxForAdl(parentIds),
				originalReservationStatus);

		if (AccelAeroCalculator.getDefaultBigDecimalZero().equals(reservation.getTotalChargeAmount())) {
			List<Integer> parentsIncludingNewInfants = new ArrayList<Integer>(parentIds);
			for (ReservationPax pax : reservation.getPassengers()) {
				if (pax.getParent() != null && parentIds.contains(pax.getParent().getPnrPaxId())) {
					parentsIncludingNewInfants.add(pax.getPnrPaxId());
				}
			}
			ADLNotifyer.recordReservation(reservation, AdlActions.A, getEtGenerationEligiblePaxForAdl(parentsIncludingNewInfants),
					originalReservationStatus);
		}
		// Process Seats for passengers
		this.processPassengerSeats(blockIds, reservation);

		// Process seat map information for the reservation
		this.processSeats(parentIds, reservation);

		Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodes(reservation);
		if (csOCCarrirs != null) {
			// send Codeshare typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.ADD_INFANT.getCode());
			typeBRequestDTO.setAddInfantPnrPaxIds(ReservationApiUtils.getPNRPaxIds(colReservationPax));
			TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
		}

		// add e tickets to the infant
		if ((reservation.getOriginatorPnr() == null || (reservation.getOriginatorPnr() != null
				&& isOtherCarrierPaxCreditUse != null && isOtherCarrierPaxCreditUse.booleanValue()))
				&& ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {

			Collection<ReservationPax> colTKTReservationPax = new ArrayList<ReservationPax>(colReservationPax);

			CreateTicketInfoDTO tktInfoDto = TicketingUtils.createTicketingInfoDto(credentialsDTO.getTrackInfoDTO());
			tktInfoDto.setBSPPayment(ReservationApiUtils.isBSPpayment(colPaymentInfo));
			if (eTicketInfo == null) {
				// If Eticket is not provided by externally we generate our own one
				if (isGoShowProcess) {
					eTicketInfo = ETicketBO.generateIATAETicketNumbersForGOSHOWBooking(colTKTReservationPax, reservation,
							tktInfoDto);
				} else {
					colTKTReservationPax = etGenerationEligiblePax(colReservationPax, passengerPaymentMap, reservation,
							forcedConfirmedPaxSeqsBeforeChange);
					if (!colTKTReservationPax.isEmpty()) {
						eTicketInfo = ETicketBO.generateIATAETicketNumbersForModifyBooking(reservation, colTKTReservationPax,
								tktInfoDto);
					}
				}
			}
			if (eTicketInfo != null) {
				ETicketBO.saveETickets(eTicketInfo, colTKTReservationPax, reservation);
			}
		}

		// Get the reservation audit
		Collection colReservationAudit = this.composeAudit(pnr, colReservationPax, reservation, passengerPaymentMap,
				credentialsDTO);
		ADLNotifyer.recordReservation(reservation, AdlActions.A, getEtGenerationEligiblePaxForAdl(parentIds),
				originalReservationStatus);
		SSRBL.sendSSRAddPaxNotifications(reservation, colReservationPax, credentialsDTO);

		if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null
				&& credentialsDTO.getTrackInfoDTO().getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
			// send typeB message
			TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
			typeBRequestDTO.setReservation(reservation);
			typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.ADD_INFANT.getCode());
			typeBRequestDTO.setAddInfantPnrPaxIds(ReservationApiUtils.getPNRPaxIds(colReservationPax));
			TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION_PAYMENT_META_TO,
				TnxGranularityReservationUtils.getReservationPaymentMetaTO(reservation));
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	private void replaceParentIdFromInfantIdInPaymentMap(Collection<ReservationPax> colReservationPax,
			Map<Integer, PaymentAssembler> passengerPaymentMap, Boolean isGoShowProcess, Boolean isInfantPaymentSeparated) throws ModuleException {

		Iterator<ReservationPax> itReservationPax = colReservationPax.iterator();
		ReservationPax reservationPax;
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			Integer parentId = reservationPax.getAccompaniedPaxId();
			if (parentId != null) {
				if (passengerPaymentMap.get(parentId) != null && (isInfantPaymentSeparated || isGoShowProcess)) {
					passengerPaymentMap.put(reservationPax.getPnrPaxId(), passengerPaymentMap.get(parentId));
					passengerPaymentMap.remove(parentId);
				}
			}
		}
	}

	private List<ReservationPax> etGenerationEligiblePax(Collection<ReservationPax> colReservationPax,
			Map<Integer, IPayment> passengerPaymentMap, Reservation reservation, List<Integer> forcedConfirmedPaxSeqsBeforeChange)
			throws ModuleException {
		List<ReservationPax> colTKTReservationPax = new ArrayList<ReservationPax>();
		Map<Integer, ReservationPax> parentIdInfantMap = new HashMap<Integer, ReservationPax>();
		for (ReservationPax infant : colReservationPax) {
			parentIdInfantMap.put(infant.getParent().getPnrPaxId(), infant);
		}

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (!ReservationApiUtils.isInfantType(reservationPax) && ReservationApiUtils
					.checkPassengerConfirmationForAModifyBooking(reservationPax, passengerPaymentMap, false, false)) {
				if (parentIdInfantMap.get(reservationPax.getPnrPaxId()) != null) {
					colTKTReservationPax.add(parentIdInfantMap.get(reservationPax.getPnrPaxId()));
				}
				if (!forcedConfirmedPaxSeqsBeforeChange.isEmpty()
						&& forcedConfirmedPaxSeqsBeforeChange.contains(reservationPax.getPaxSequence())) {
					colTKTReservationPax.add(reservationPax);
					for (ReservationPax infant : reservationPax.getInfants()) {
						if (forcedConfirmedPaxSeqsBeforeChange.contains(infant.getPaxSequence())) {
							colTKTReservationPax.add(infant);
						}
					}
				}
			}
		}

		return colTKTReservationPax;
	}

	/**
	 * Process Seats
	 * 
	 * @param parentIds
	 * @param reservation
	 * @throws ModuleException
	 */
	private void processSeats(Collection<Integer> parentIds, Reservation reservation) throws ModuleException {
		Collection<PaxSeatTO> seats = reservation.getSeats();

		if (seats != null) {
			Map<Integer, String> effectiveFltAmSeatIdsPaxType = new HashMap<Integer, String>();
			for (PaxSeatTO paxSeatTO : seats) {
				if (parentIds.contains(paxSeatTO.getPnrPaxId())) {
					effectiveFltAmSeatIdsPaxType.put(paxSeatTO.getSelectedFlightSeatId(),
							ReservationInternalConstants.PassengerType.PARENT);
				}
			}

			if (effectiveFltAmSeatIdsPaxType != null && effectiveFltAmSeatIdsPaxType.size() > 0) {
				ReservationModuleUtils.getSeatMapBD().updatePaxType(effectiveFltAmSeatIdsPaxType, null);
			}
		}
	}

	/**
	 * Adds external charges
	 * 
	 * @param reservation
	 * @param passengerPaymentMap
	 * @param tnxGen
	 * @throws ModuleException
	 */
	private void addExternalCharges(Reservation reservation, Map<Integer, PaymentAssembler> passengerPaymentMap,
			CredentialsDTO credentialsDTO, Collection<ReservationPax> infants, ChargeTnxSeqGenerator tnxGen)
			throws ModuleException {
		ExternalGenericChargesBL.reflectExternalChargeForInfants(reservation, passengerPaymentMap, credentialsDTO, infants,
				tnxGen.getTnxSequence(ReservationInternalConstants.PassengerType.INFANT));
	}

	/**
	 * Check Add Infant Constraints
	 * 
	 * @param reservation
	 * @param passengerPaymentMap
	 * @param version
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkAddInfantConstraints(Reservation reservation, Map passengerPaymentMap, Long version)
			throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		// Check if it's valid payments or not
		ReservationCoreUtils.checkValidPaxPaymentsForReservation(reservation, passengerPaymentMap, null);
	}

	/**
	 * Change infant states
	 * 
	 * @param reservation
	 *            Modified to set CANCEL state as well, this case will only get fired for interline reservations.
	 */
	private void changeInfantStates(Reservation reservation) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED);
			}
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);
			}
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CANCEL);
			}
		}
	}

	/**
	 * Set the arguments
	 * 
	 * @param pnrPaymentTypes
	 * @param credentialsDTO
	 */
	private void setArguments(Integer pnrPaymentTypes, CredentialsDTO credentialsDTO) {
		this.pnrPaymentTypes = pnrPaymentTypes;
		this.credentialsDTO = credentialsDTO;
	}

	/**
	 * Update the reservation and the passenger status
	 * 
	 * @param reservation
	 * @param passengerPaymentMap
	 * @throws ModuleException
	 */
	private void updateStates(Reservation reservation, Map<Integer, IPayment> passengerPaymentMap) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		int confirmPaxCount = 0;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			if (ReservationInternalConstants.ReservationPaxStatus.ON_HOLD.equals(reservationPax.getStatus())) {
				confirmPnrPaxIds.add(reservationPax.getPnrPaxId());
			}

			// Setting the passenger states
			if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
					|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
							.equals(pnrPaymentTypes)) {
				// Confirming the passenger
				reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
			} else {
				// Returns the passenger confirm status
				boolean status = ReservationApiUtils.isPassengerConfirmedForAModifyBooking(reservationPax, passengerPaymentMap,
						false, false);
				if (status) {
					confirmPaxCount++;
					reservationPax.setStatus(ReservationPaxStatus.CONFIRMED);
				} else {
					if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT.equals(pnrPaymentTypes)) {
						throw new ModuleException("airreservations.arg.paidAmountError");
					} else {
						reservationPax.setStatus(ReservationPaxStatus.ON_HOLD);
					}
				}
			}
		}

		// Confirming the reservation
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {
			reservation.setStatus(ReservationStatus.CONFIRMED);
		} else {
			// If confirmed making the reservation status to confirm
			if (confirmPaxCount == reservation.getPassengers().size()) {
				reservation.setStatus(ReservationStatus.CONFIRMED);
			}
			// If it's not totally confirmed make the reservation on hold
			else {
				reservation.setStatus(ReservationStatus.ON_HOLD);
			}
		}
	}

	/**
	 * Compose an audit
	 * 
	 * @param pnr
	 * @param colReservationPax
	 * @param reservation
	 * @param passengerPaymentMap
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationAudit> composeAudit(String pnr, Collection<ReservationPax> colReservationPax,
			Reservation reservation, Map<Integer, PaymentAssembler> passengerPaymentMap, CredentialsDTO credentialsDTO)
			throws ModuleException {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.ADDED_NEW_PASSENGER.getCode());
		reservationAudit.setUserNote(null);

		// Setting the passenger information
		Iterator<ReservationPax> itReservationPax = colReservationPax.iterator();
		ReservationPax reservationPax;
		StringBuffer passengeInfo = new StringBuffer();

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			passengeInfo.append(" " + BeanUtils.nullHandler(reservationPax.getTitle()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getFirstName()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getLastName()) + " - " + reservationPax.getPaxTypeDescription() + " ("
					+ BeanUtils.nullHandler(reservationPax.getParent().getTitle()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getParent().getFirstName()) + ", "
					+ BeanUtils.nullHandler(reservationPax.getParent().getLastName()) + " - "
					+ reservationPax.getParent().getPaxTypeDescription() + ") " + " Charges "
					+ reservationPax.getTotalChargeAmount());
		}

		// Get the passenger payment information
		String audit = ReservationCoreUtils.getPassengerPaymentInfo(passengerPaymentMap, reservation);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewPassenger.PASSENGER_DETAILS,
				passengeInfo.toString() + audit);

		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)
				|| ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE
						.equals(pnrPaymentTypes)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewPassenger.FORCE_CONFIRM,
					ReservationTemplateUtils.AUDIT_FORCE_CONFIRM_TEXT);
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewPassenger.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedNewPassenger.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		colReservationAudit.add(reservationAudit);
		return colReservationAudit;
	}

	/**
	 * Process passenger seats
	 * 
	 * @param blockIds
	 * @param reservation
	 * @throws ModuleException
	 */
	private void processPassengerSeats(Collection<TempSegBcAlloc> blockIds, Reservation reservation) throws ModuleException {
		// If the reservation is confirmed
		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			// Move the seats to the sold state
			ReservationBO.moveBlockedSeatsToSold(blockIds);
			// If any newly confirm passengers exist
			if (confirmPnrPaxIds.size() > 0) {
				ReservationBO.moveOnholdSeatsToConfirm(reservation.getPassengers(), confirmPnrPaxIds, null);
			}
		}
		// If the reservation is on hold
		else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			// Move the seats to the on hold state
			ReservationBO.moveBlockSeats(blockIds, new Integer(0), new Integer(0));
		}
	}

	/**
	 * Capture ownership
	 * 
	 * @param reservation
	 */
	private void captureOwnerShip(Reservation reservation) {
		// This is force confirmation therefore
		// If IBE user(s) exist should change the channel and the agent code
		if (AppSysParamsUtil.isOwnerAgentTransferEnabled()) {
			reservation.getAdminInfo().setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
			reservation.getAdminInfo().setOwnerAgentCode(credentialsDTO.getAgentCode());
		}
	}

	/**
	 * Check and process force mode
	 * 
	 * @param reservation
	 */
	private void checkForceMode(Reservation reservation) {
		if (ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED.equals(pnrPaymentTypes)) {
			this.captureOwnerShip(reservation);
		}
	}

	/**
	 * Build the Segment objects Map
	 * 
	 * @param reservation
	 * @return
	 */
	private Map<Integer, ReservationSegment> buildSegmentObjectsMap(Reservation reservation) {
		Map<Integer, ReservationSegment> segObjectsMap = new HashMap<Integer, ReservationSegment>();
		Iterator<ReservationSegment> itReservationSegment = reservation.getSegments().iterator();
		ReservationSegment reservationSegment;

		while (itReservationSegment.hasNext()) {
			reservationSegment = itReservationSegment.next();

			// Keep the reservation segment id mapping
			segObjectsMap.put(reservationSegment.getPnrSegId(), reservationSegment);
		}

		return segObjectsMap;
	}

	/**
	 * Build the reservation with the new adults
	 * 
	 * @param reservation
	 * @param colReservationPax
	 * @param colOndFareDTOs
	 * @param segObjectsMap
	 * @param passengerPaymentMap
	 * @throws ModuleException
	 */
	private Collection<Integer> buildInfants(Reservation reservation, Collection<ReservationPax> colReservationPax,
			Collection<OndFareDTO> colOndFareDTOs, Map<Integer, ReservationSegment> segObjectsMap,
			Map<Integer, PaymentAssembler> passengerPaymentMap, CredentialsDTO credentialsDTO, ChargeTnxSeqGenerator tnxGen)
			throws ModuleException {
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		OndFareDTO ondFareDTO;

		Iterator<OndFareDTO> itOndFares;

		// For Reservation
		int totalPaxInfantCount = 0;
		BigDecimal infantCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<Integer, ReservationPax> infantsMap = new HashMap<Integer, ReservationPax>();
		Map<Integer, ReservationPax> parentMap = new HashMap<Integer, ReservationPax>();
		Iterator<ReservationPax> itReservationPax = colReservationPax.iterator();
		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			// Holds the infant Map - Index Id is the parent Id
			// Parent Seq Id - Infant Instance
			parentMap.put(reservationPax.getIndexId(), reservationPax);
			// Infant Seq Id - Parent Instance
			infantsMap.put(reservationPax.getPaxSequence(), this.getParent(reservation, reservationPax.getIndexId()));

			itOndFares = colOndFareDTOs.iterator();

			while (itOndFares.hasNext()) {
				ondFareDTO = itOndFares.next();
				reservationPaxFare = new ReservationPaxFare();

				// Infants could have fare Id(s)
				if (ReservationApiUtils.isFareExist(ondFareDTO.getInfantFare())) {
					reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));
					ReservationCoreUtils.captureReservationPaxOndCharge(
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()), new Integer(ondFareDTO.getFareId()),
							null, ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false, null,
							null, null, tnxGen.getTnxSequence(ReservationInternalConstants.PassengerType.INFANT));
				}

				// ############# Setting Ond Fare Charges ##############################
				this.setInfantOndFareCharges(reservationPaxFare, ondFareDTO, credentialsDTO,
						tnxGen.getTnxSequence(ReservationInternalConstants.PassengerType.INFANT));
				// Set total amounts
				ReservationCoreUtils.updateTotals(reservationPax, reservationPaxFare.getTotalChargeAmounts(), true);
				// ############# Setting Ond Fare Charges ##############################

				// ############# Setting Fare Segments ##############################
				this.setInfantOndFareSegments(reservationPaxFare, ondFareDTO, segObjectsMap);
				// ############# Setting Fare Segments ##############################

				// Set pnr pax fares
				reservationPax.addPnrPaxFare(reservationPaxFare);
			}

			// Incrementing the infant count
			totalPaxInfantCount = totalPaxInfantCount + 1;

			// Adding the infant charge
			infantCharge = reservationPax.getTotalChargeAmount();

			// Update total amounts
			this.updateTotalAmounts(reservation, reservationPax);
		}

		// Adding it to the main reservation
		reservation.setTotalPaxInfantCount(reservation.getTotalPaxInfantCount() + totalPaxInfantCount);

		// Add the infant
		this.addInfant(reservation, colReservationPax);

		// Map the parent and infant mappings
		Collection<Integer> parentIds = this.trackParentInfantMappings(reservation, parentMap, infantsMap);

		// Map infant charge to the parent
		this.mapInfantChargeToParent(infantsMap, passengerPaymentMap, infantCharge);

		return parentIds;
	}

	/**
	 * Map the infant charge to the parent
	 * 
	 * @param infantsMap
	 * @param passengerPaymentMap
	 * @param infantCharge
	 * @throws ModuleException
	 */
	private void mapInfantChargeToParent(Map<Integer, ReservationPax> infantsMap,
			Map<Integer, PaymentAssembler> passengerPaymentMap, BigDecimal infantCharge) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = infantsMap.values().iterator();
		ReservationPax reservationPax;
		PaymentAssembler paymentAssembler;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			paymentAssembler = passengerPaymentMap.get(reservationPax.getPnrPaxId());
			paymentAssembler.setTotalChargeAmount(infantCharge);
		}
	}

	/**
	 * Add the infant
	 * 
	 * @param reservation
	 * @param colReservationPax
	 */
	private void addInfant(Reservation reservation, Collection<ReservationPax> colReservationPax) {
		Iterator<ReservationPax> itColReservationPax = colReservationPax.iterator();
		ReservationPax reservationPax;

		while (itColReservationPax.hasNext()) {
			reservationPax = itColReservationPax.next();
			reservation.addPassenger(reservationPax);
		}
	}

	/**
	 * Update total amounts
	 * 
	 * @param reservation
	 * @param reservationPax
	 */
	private void updateTotalAmounts(Reservation reservation, ReservationPax reservationPax) {
		ReservationCoreUtils.updateTotals(reservation, reservationPax.getTotalChargeAmounts(), true);
	}

	/**
	 * Returns the parent
	 * 
	 * @param reservation
	 * @param indexId
	 * @return
	 * @throws ModuleException
	 */
	private ReservationPax getParent(Reservation reservation, Integer indexId) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax = null;
		ReservationPax parent = null;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();

			// If the parent found
			if (reservationPax.getPaxSequence().equals(indexId)) {
				parent = reservationPax;
				break;
			}
		}

		if (parent == null) {
			throw new ModuleException("airreservations.arg.invalidParent");
		}

		return parent;
	}

	/**
	 * Correcting infant/parent mapping
	 * 
	 * @param reservation
	 * @param parentMap
	 * @param infantsMap
	 * @throws ModuleException
	 */
	private Collection<Integer> trackParentInfantMappings(Reservation reservation, Map<Integer, ReservationPax> parentMap,
			Map<Integer, ReservationPax> infantsMap) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax currentPax;
		ReservationPax mappingPax;
		Collection<Integer> pnrPaxIdsParents = new HashSet<Integer>();
		while (itReservationPax.hasNext()) {
			currentPax = itReservationPax.next();

			// Carrying out the parent mapping
			if (parentMap.containsKey(currentPax.getPaxSequence())) {
				mappingPax = parentMap.get(currentPax.getPaxSequence());
				currentPax.addInfants(mappingPax);
				pnrPaxIdsParents.add(currentPax.getPnrPaxId());
			}

			// Carrying out the infant mapping
			if (infantsMap.containsKey(currentPax.getPaxSequence())) {
				mappingPax = infantsMap.get(currentPax.getPaxSequence());

				// Setting the parent
				currentPax.addParent(mappingPax);

				// Setting the Parent's Zulu Release Time for the infant
				// What ever the value which was set in the IPassenger will be overriden from here
				if (mappingPax.getZuluReleaseTimeStamp() != null) {
					currentPax.setZuluReleaseTimeStamp(mappingPax.getZuluReleaseTimeStamp());
				}
				pnrPaxIdsParents.add(mappingPax.getPnrPaxId());
			}
		}

		return pnrPaxIdsParents;
	}

	/**
	 * Set ond fare charges for infant
	 * 
	 * @param reservationPaxFare
	 * @param ondFareDTO
	 * @param tnxSequence
	 */
	private void setInfantOndFareCharges(ReservationPaxFare reservationPaxFare, OndFareDTO ondFareDTO,
			CredentialsDTO credentialsDTO, Integer tnxSequence) {
		log.debug("Inside setInfantOndFareCharges");

		// If charges are not empty
		if (ondFareDTO.getAllCharges() != null) {
			Iterator<QuotedChargeDTO> itCharges = ondFareDTO.getAllCharges().iterator();
			QuotedChargeDTO quotedChargeDTO;

			while (itCharges.hasNext()) {
				quotedChargeDTO = itCharges.next();
				if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
						|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {
					ReservationCoreUtils.captureReservationPaxOndCharge(
							AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)), null,
							new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
							reservationPaxFare, credentialsDTO, false, null, null, null, tnxSequence);
				}
			}
		}

		log.debug("Exit setInfantOndFareCharges");
	}

	/**
	 * Set ond fare segments and charges for infant
	 * 
	 * @param reservationPaxFare
	 * @param ondFareDTO
	 * @param segObjectsMap
	 */
	private void setInfantOndFareSegments(ReservationPaxFare reservationPaxFare, OndFareDTO ondFareDTO,
			Map<Integer, ReservationSegment> segObjectsMap) {
		log.debug("Inside setInfantOndFareSegments");

		Iterator<SegmentSeatDistsDTO> itSegAva = ondFareDTO.getSegmentSeatDistsDTO().iterator();
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itSegAva.hasNext()) {
			segmentSeatDistsDTO = itSegAva.next();
			reservationPaxFareSegment = new ReservationPaxFareSegment();

			// Set the Reservation Segment for relationship tracking
			reservationPaxFareSegment.setSegment(segObjectsMap.get(new Integer(segmentSeatDistsDTO.getPnrSegId())));

			// There won't be any booking code set to the infant

			reservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
		}

		log.debug("Exit setInfantOndFareSegments");
	}

	/**
	 * Validating the parameters
	 * 
	 * @param pnr
	 * @param colReservationPax
	 * @param colOndFareDTOs
	 * @param pnrPaymentTypes
	 * @param blockIds
	 * @param passengerPaymentMap
	 * @param version
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(String pnr, Collection<ReservationPax> colReservationPax, Collection colOndFareDTOs,
			Integer pnrPaymentTypes, Collection blockIds, Map passengerPaymentMap, Long version, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || colReservationPax == null || colReservationPax.size() == 0 || colOndFareDTOs == null
				|| colOndFareDTOs.size() == 0 || pnrPaymentTypes == null || blockIds == null || passengerPaymentMap == null
				|| version == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

	public static void addPaxSegmentSSRS(Collection<ReservationPax> colReservationPax,
			Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps) throws ModuleException {
		if (paxSegmentSSRDTOMaps != null && paxSegmentSSRDTOMaps.size() > 0) {
			for (ReservationPax reservationPax : colReservationPax) {
				ReservationPax pax = reservationPax;
				Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap = paxSegmentSSRDTOMaps.get(pax.getPaxSequence());

				ReservationSSRUtil.addPaxSegmentSSRs(pax, segmentSSRDTOMap, null);
				ReservationSSRUtil.addSSRsForExternalCharges(pax);
			}
		}
	}

	private void addAutoCancellationInfo(Collection<ReservationPax> infants, AutoCancellationInfo autoCnxInfo) {
		if (autoCnxInfo != null) {
			for (ReservationPax infant : infants) {
				infant.setAutoCancellationId(autoCnxInfo.getAutoCancellationId());
			}
		}
	}

	private Collection<ReservationPax> getEtGenerationEligiblePaxForAdl(Collection<Integer> parentId) {
		Collection<ReservationPax> etGenerationEligiblePaxForAdl = new ArrayList<ReservationPax>();
		for (Integer pnrPaxId : parentId) {
			ReservationPax pax = new ReservationPax();
			pax.setPnrPaxId(pnrPaxId);
			etGenerationEligiblePaxForAdl.add(pax);
		}
		return etGenerationEligiblePaxForAdl;
	}
}
