package com.isa.thinair.airreservation.core.bl.tax;

import java.util.Collection;

import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class TaxUtil {

	private static Collection<Charge> charges = null;

	public static Collection<Charge> getServiceTaxes() throws ModuleException {
		if (charges == null) {
			ChargeSearchCriteria criteria = new ChargeSearchCriteria();
			criteria.setChargeCategoryCode(Charge.CHARGE_CATEGORY_SERVICE_TAX);
			criteria.setChargeStatus("All");
			charges = ReservationModuleUtils.getChargeBD().getCharges(criteria);
		}
		return charges;

	}
}
