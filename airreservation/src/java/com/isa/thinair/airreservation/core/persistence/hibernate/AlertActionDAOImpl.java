/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.util.Collection;

import com.isa.thinair.airreservation.api.model.AlertAction;
import com.isa.thinair.airreservation.core.persistence.dao.AlertActionDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @author Byorn
 * @since 1.0
 * @isa.module.dao-impl dao-name="AlertActionDAO"
 */
public class AlertActionDAOImpl extends PlatformBaseHibernateDaoSupport implements AlertActionDAO {

	public Collection<AlertAction> getAlertActions() {
		return find("from AlertAction", AlertAction.class);
	}

}
