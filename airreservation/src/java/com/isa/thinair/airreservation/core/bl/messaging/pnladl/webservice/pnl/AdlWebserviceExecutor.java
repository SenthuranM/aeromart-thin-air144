/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.pnl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLPassengerData;
import com.isa.thinair.airreservation.api.dto.adl.FlightInformation;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.webservice.BaseWebserviceExecutor;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public class AdlWebserviceExecutor extends BaseWebserviceExecutor {

	private static Log log = LogFactory.getLog(AdlWebserviceExecutor.class);
	private ADLDTO adlElements = null;
	private BaseDataContext baseContext;

	@Override
	public boolean executewebService(BaseDataContext baseContext)
			throws ModuleException {
		this.baseContext = baseContext;
		adlElements = createAdlDto();
		return adlServiceCaller();
	}

	private boolean adlServiceCaller() throws ModuleException {
		boolean isInvokedSuccess = false;
		isInvokedSuccess = ReservationModuleUtils.getPNLADLServiceBD()
				.sendADLToDcs(adlElements);

		return isInvokedSuccess;
	}

	private ADLDTO createAdlDto() {
		adlElements = new ADLDTO();
		adlElements.setFlightInfo(createFlightInformation(
				baseContext.getFlightNumber(), baseContext.getFlightId(),
				baseContext.getDepartureAirportCode(),
				baseContext.getFlightLocalDate()));
		adlElements.setClassCodes(getClassCodes());
		adlElements.setPassengerData(createPassengerData(baseContext
				.getPassengerCollection()));

		return adlElements;
	}

	private FlightInformation createFlightInformation(String flightNumber,
			Integer flightId, String boardingAirport, Date flightDate) {
		FlightInformation flightInformation = new FlightInformation();
		flightInformation.setFlightNumber(flightNumber);
		flightInformation.setBoardingAirport(boardingAirport);
		flightInformation.setFlightId(flightId);
		flightInformation.setFlightDate(flightDate);
		return flightInformation;
	}

	private Map<String, List<String>> getClassCodes() {
		return baseContext.getPassengerCollection().getRbdFareCabinMap();
	}



	private Map<String, Map<String, ADLPassengerData>> createPassengerData(
			PassengerCollection passengerCollection) {
		Map<String, Map<String, ADLPassengerData>> passengerData = new HashMap<String, Map<String, ADLPassengerData>>();

		for (Map.Entry<String, List<DestinationFare>> entry : passengerCollection
				.getPnlAdlDestinationMap().entrySet()) {
			for (DestinationFare destinationFare : entry.getValue()) {

				Map<String, ADLPassengerData> paxData = new HashMap<String, ADLPassengerData>();
				paxData.put(destinationFare.getFareClass(),
						createADLPassengerData(destinationFare));
				passengerData.put(destinationFare.getDestinationAirportCode(),
						paxData);
			}
		}

		return passengerData;
	}

	private ADLPassengerData createADLPassengerData(
			DestinationFare destinationFare) {
		ADLPassengerData adlPassengerData = new ADLPassengerData();
		if (destinationFare.getAddPassengers() != null) {
			addPassengers(destinationFare.getAddPassengers(),
					adlPassengerData.getAddedPassengers());
		}

		if (destinationFare.getDeletePassengers() != null) {
			addPassengers(destinationFare.getDeletePassengers(),
					adlPassengerData.getDeletedPassengers());
		}

		if (destinationFare.getChangePassengers() != null) {
			addPassengers(destinationFare.getChangePassengers(),
					adlPassengerData.getChangedPassengers());
		}
		return adlPassengerData;
	}

	private void addPassengers(
			Map<String, List<PassengerInformation>> passengerCollection,
			List<PassengerInformation> addedPassengers) {
		for (Map.Entry<String, List<PassengerInformation>> entry : passengerCollection
				.entrySet()) {
			addedPassengers.addAll(entry.getValue());
		}
	}

}
