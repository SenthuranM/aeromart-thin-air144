package com.isa.thinair.airreservation.core.bl.tty;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ChangePaxDetailsMessageCreator extends TypeBReservationMessageCreator {
	
	public ChangePaxDetailsMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}

	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {
		ssrDTOs.addAll(TTYMessageCreatorUtil.composeSsrDocsInfo(typeBRequestDTO.getAdditionalInfoChangedPaxMap(), reservation,
				typeBRequestDTO));
		ssrDTOs.addAll(TTYMessageCreatorUtil.composeSsrDocoInfo(typeBRequestDTO.getAdditionalInfoChangedPaxMap(), reservation,
				typeBRequestDTO));
		return ssrDTOs;
	}
}
