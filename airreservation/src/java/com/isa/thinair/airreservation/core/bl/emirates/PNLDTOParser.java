/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.emirates;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.emirates.EMPNLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.emirates.EMPNLRecordDTO;
import com.isa.thinair.airreservation.api.dto.emirates.EMRecordDTO;
import com.isa.thinair.airreservation.api.dto.emirates.XAPaxNameListDTO;
import com.isa.thinair.airreservation.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.airreservation.api.model.XAPnl;
import com.isa.thinair.airreservation.api.model.XAPnlPaxEntry;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.XAParserConstants;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.XAPnlDAO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * The utility to parse a PNL document
 * 
 * @author Byorn
 * @since 1.0
 * @todo : need to cater for this .R/OKOB OKOB HK1-1PERERA/THELMAMS
 */
public class PNLDTOParser {

	/**
	 * holds the key if tour id was null, which means a continuation to part 2 is possible
	 */

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PNLDTOParser.class);

	/**
	 * Returns PNL meta data transfer information
	 * 
	 * @param f
	 * @return
	 * @throws ModuleException
	 */
	private static EMPNLMetaDataDTO getMetaDataDTO(File f) throws ModuleException {
		Throwable exception = null;
		String pnlContent = "";
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(f));
			StringBuffer parserFriendlyText = new StringBuffer();
			StringBuffer originalPNLText = new StringBuffer();
			boolean destinationFound = false;
			boolean prevWasDestinationFound = false;
			boolean isValidPnl = false;
			String continuationText = null;

			String strHolder = null;
			String fromAddress = "";
			boolean started = false;

			while ((strHolder = buffer.readLine()) != null) {

				// Capturing the from address
				if (strHolder.length() > 5 && strHolder.substring(0, 5).equalsIgnoreCase("From:")) {
					fromAddress = BeanUtils.nullHandler(strHolder.substring(5));
					int start = fromAddress.indexOf("<");
					int end = fromAddress.lastIndexOf(">");

					// This means Email Address is nilindra@abc.com
					if (start == -1 || end == -1) {
						fromAddress = BeanUtils.nullHandler(fromAddress);
						// This mean Email Address is Nilindra <nilindra@abc.com>
					} else {
						fromAddress = BeanUtils.nullHandler(fromAddress.substring(start + 1, end));
					}
				}
				// Processing the PNL part
				else if (strHolder.trim().equalsIgnoreCase("PNL") || started) {
					isValidPnl = true;

					// for continuation from part 1 check the line
					if (prevWasDestinationFound) {
						prevWasDestinationFound = false;
						if (!isANormalPaxRecordLine(strHolder)) {
							continuationText = strHolder;
							continue;
						}

					}

					originalPNLText.append(strHolder + "\n");
					started = true;

					// If it's PNL label

					if (strHolder.equalsIgnoreCase("PNL")) {
						parserFriendlyText.append(strHolder + "\n");
						// Process down the PNL label
					} else if (strHolder.trim().length() > 2 && !isNumber(strHolder.trim().substring(0, 3))) {

						if ((strHolder.charAt(0) == '-') && destinationFound == false) {
							parserFriendlyText.append(strHolder + "\n");
							prevWasDestinationFound = true;
							destinationFound = true;
						} else if ((strHolder.charAt(0) == '-') && destinationFound == true) {
							parserFriendlyText.append(strHolder + "\n");
						} else if (strHolder.charAt(3) == ' ') {
							parserFriendlyText.append(strHolder.substring(0, 7).replaceAll("/", "") + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".L/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);
							parserFriendlyText.append(" " + strHolder + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".R/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".L/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".C/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".I/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".O/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".RN/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".O2/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);

							parserFriendlyText.append(" " + strHolder + "\n");
						}

						else {
							// If it's the end of the pnl making it as
							// process ended
							if (strHolder.indexOf("ENDPNL") != -1 || strHolder.indexOf("ENDPART") != -1) {
								started = false;

							}

							/**/
							if (isNumber(strHolder.substring(0, 1))) {
								if (isNumber(strHolder.substring(1, 2))) {
									strHolder = new StringBuffer(strHolder).insert(2, " ").toString();
									// parserFriendlyText.insert(2, "
									// ");
								} else {
									strHolder = new StringBuffer(strHolder).insert(1, " ").toString();
									// parserFriendlyText.insert(1, "
									// ");
								}

							}
							parserFriendlyText.append(strHolder + "\n");

							/**/

						}
					}

				}
			}
			buffer.close();
			if (!isValidPnl) {
				throw new ModuleException(new Exception("Invalid IATA Format"), "airreservations.pnl.cannotParsePNL");
			}
			String text = parserFriendlyText.toString();
			pnlContent = text.replaceAll("\n", "<br>");

			EMPNLMetaDataDTO metaDataDTO = PNLParser.process(text);
			metaDataDTO.setFromAddress(fromAddress);
			metaDataDTO.setPnlContent(originalPNLText.toString());

			// if by anychance a addtional text appears after the destination line, it is due to a continuation of the
			// previous pnl
			// therefore in that text (for now) only the xa pnr number will be extracted as important.
			if (continuationText != null) {

				String contdXaPnrNumber = searchForXAPnrNumber(continuationText);
				if (contdXaPnrNumber != null) {
					metaDataDTO.addTourIdTicketNumber(XAParserConstants.MAPKEY_CONT, contdXaPnrNumber);
				}

			}
			return metaDataDTO;

		} catch (IOException e) {
			exception = e;
			log.error(" ERROR ", e);
			throw new ModuleException(e, "airreservations.pnl.cannotLocatePNL");
		} catch (ModuleException e) {
			exception = e;
			log.error(" ERROR ", e);
			throw new ModuleException(e, "airreservations.pnl.cannotParsePNL");
		} finally {

			// if exception occured mail to relavent parties.
			if (exception != null) {

				XAPnlLogDTO pnlLogDTO = new XAPnlLogDTO();
				pnlLogDTO.setExceptionDescription(exception.getMessage());
				pnlLogDTO.setStackTraceElements(exception.getStackTrace());
				pnlLogDTO.setXaPnlContent(pnlContent);
				pnlLogDTO.setFileName(f.getName());
				XAMailError.notifyError(pnlLogDTO);
			}

		}
	}

	private static boolean isANormalPaxRecordLine(String content) {
		if (isNumber(content.substring(0, 1))) {
			return true;

		}
		return false;
	}

	private static String searchForXAPnrNumber(String continuationText) {
		String xaPnrNumber = null;

		if (continuationText.indexOf(".L/") != -1) {
			int indexOf_DOT_EL = continuationText.indexOf(".L/");
			String newStr = continuationText.substring(indexOf_DOT_EL);
			String y = null;
			if (newStr.indexOf(" ") != -1) {
				y = newStr.substring(3, newStr.indexOf(" "));
				xaPnrNumber = y;
			} else {
				xaPnrNumber = newStr.substring(3);
			}
		}
		return xaPnrNumber;
	}

	/**
	 * Find out whether it's a number or not
	 * 
	 * @param str
	 * @return
	 */
	private static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * Insert to the pfs entries
	 * 
	 * @param colMsgNames
	 * @return
	 * @throws ModuleException
	 */
	public static int insertPnlToDatabase(Collection<String> msgNamesWithPartTwoOrThree, String strMsgName) throws ModuleException {

		AirReservationConfig airReservationConfig = ReservationModuleUtils.getAirReservationConfig();

		XAPnlDAO pnlDAO = ReservationDAOUtils.DAOInstance.XA_PNL_DAO;
		String pnlProcessPath = airReservationConfig.getXaPnlProcessPath();
		Collection<XAPaxNameListDTO> colPaxNameListsDTO = getAllPaxNameListDTOs(pnlProcessPath, strMsgName, msgNamesWithPartTwoOrThree);

		XAPaxNameListDTO paxNameListsDTO;
		XAPnlPaxEntry pnlPaxEntry;

		int pnlId = 0;

		for (Iterator<XAPaxNameListDTO> iter = colPaxNameListsDTO.iterator(); iter.hasNext();) {
			paxNameListsDTO = (XAPaxNameListDTO) iter.next();
			pnlPaxEntry = new XAPnlPaxEntry();

			pnlPaxEntry.setCabinClassCode(paxNameListsDTO.getCabinClassCode());
			pnlPaxEntry.setDepartureAirport(paxNameListsDTO.getDepartureAirportCode());
			pnlPaxEntry.setTitle(paxNameListsDTO.getTitle());
			pnlPaxEntry.setFirstName(paxNameListsDTO.getFirstName());
			pnlPaxEntry.setLastName(paxNameListsDTO.getLastName());
			pnlPaxEntry.setFlightNumber(paxNameListsDTO.getFlightNumber());
			pnlPaxEntry.setXaPnr(paxNameListsDTO.getXAPnr());
			pnlPaxEntry.setProcessedStatus(ReservationInternalConstants.XAPnlProcessStatus.NOT_PROCESSED);
			pnlPaxEntry.setFlightDate(paxNameListsDTO.getRealFlightDate());
			pnlPaxEntry.setArrivalAirport(paxNameListsDTO.getArrivalAirportCode());
			pnlPaxEntry.setReceivedDate(paxNameListsDTO.getDateDownloaded());
			pnlPaxEntry.setPnlId(new Integer(paxNameListsDTO.getPnlId()));
			pnlPaxEntry.setInfantFirstName(paxNameListsDTO.getInfantFirstName());
			pnlPaxEntry.setInfantLastName(paxNameListsDTO.getInfantLastName());
			pnlPaxEntry.setInfantTitle(paxNameListsDTO.getInfantTitle());
			pnlPaxEntry.setPaxType(paxNameListsDTO.getPaxType());
			pnlPaxEntry.setOutBoundInfo(paxNameListsDTO.getOutboundInfo());
			pnlPaxEntry.setInBoundInfo(paxNameListsDTO.getInboundInfo());
			pnlPaxEntry.setInfantTitle("");
			// pnlPaxEntry.setInfantSSRCode(checkSSRCodeDataIntegrity(paxNameListsDTO.getInfantSSRCode()));
			// pnlPaxEntry.setInfantSSRRemarks(paxNameListsDTO.getInfantSSRText());
			pnlPaxEntry.setAdultSSRCode(paxNameListsDTO.getSsrCode());
			pnlPaxEntry.setAdultSSRRemarks(paxNameListsDTO.getSsrRemarks());
			pnlPaxEntry.setErrorDescription(paxNameListsDTO.getErrorDescription());
			pnlDAO.saveXAPnlPaxEntry(pnlPaxEntry);

			pnlId = pnlPaxEntry.getPnlId().intValue();
		}

		return pnlId;
	}

	/**
	 * A Temporary Method
	 * 
	 * @TODO: Incorporate Singleton/Lazy Load Design Pattern
	 * @param pnlPaxEntry
	 */
	private static String checkToAppendTKNA(String ssrCode) {

		if (ssrCode == null || ssrCode.equals("") || ssrCode.equals(" ")) {
			return XAParserConstants.SSR_CODES.CODE_TKNA;
		}

		if (ssrCode.equals(XAParserConstants._VIP_CASE)) {
			return XAParserConstants.SSR_CODES.CODE_VIP;
		}

		return ssrCode;

	}

	/**
	 * Returns passenger final sales data transfer information
	 * 
	 * @param file
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<XAPaxNameListDTO> getXAPaxNameListDTO(File file, Collection<String> msgNamesHavingPartTwoThree, String path)
			throws ModuleException {
		PassengerDAO passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;
		Collection<String> colPaxTitles = new ArrayList<String>(passengerDAO.getPassengerTitles().keySet());
		XAPaxNameListDTO paxNameListDTO = null;

		EMPNLRecordDTO empnlRecordDTO = null;
		EMRecordDTO recordDTO = null;

		EMPNLMetaDataDTO emPnlMetaDataDTO = getMetaDataDTO(file);

		emPnlMetaDataDTO.getPnlContent();
		ArrayList<XAPaxNameListDTO> colPaxNameListDTO = new ArrayList<XAPaxNameListDTO>();

		XAPnl pnl = new XAPnl();
		String mappedSystemFlightNumber = "";

		checkIfXAPnlAlreadyExists(emPnlMetaDataDTO);
		// NOTE: Deciding of the Carrier code should be based on a value
		// extracted from the XA PNL file.
		pnl.setCarrierCode(decideCarrierCode("undecided param"));
		mappedSystemFlightNumber = decideSystemFlightNumber(emPnlMetaDataDTO.getFlightNumber());
		pnl.setFlightNumber(mappedSystemFlightNumber);
		pnl.setXaFlightNumber(emPnlMetaDataDTO.getFlightNumber());
		pnl.setFromAddress(emPnlMetaDataDTO.getFromAddress());
		pnl.setNumberOfPassengers(new Integer(emPnlMetaDataDTO.getTotNumberOfPax()).intValue());
		pnl.setToAirport(emPnlMetaDataDTO.getDestinationAirport());
		pnl.setFromAirport(emPnlMetaDataDTO.getBoardingAirport());
		pnl.setDateDownloaded(emPnlMetaDataDTO.getDateDownloaded());
		pnl.setXaPnlContent(emPnlMetaDataDTO.getPnlContent());
		pnl.setFromAddress(emPnlMetaDataDTO.getFromAddress());
		pnl.setDepartureDate(emPnlMetaDataDTO.getDepartureDate());
		pnl.setPartNumber(Integer.valueOf(emPnlMetaDataDTO.getPartNumber()).intValue());
		pnl.setNumOfAttempts(0);

		// Get the flight error occured status
		boolean flightErrorOccured = isRealDepartureDateErrorOccured(emPnlMetaDataDTO);

		if (flightErrorOccured) {
			pnl.setProcessedStatus(ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED);
		} else {
			pnl.setProcessedStatus(ReservationInternalConstants.XAPnlProcessStatus.NOT_PROCESSED);
		}

		Iterator<EMPNLRecordDTO> empnlRecordDTOIterator = emPnlMetaDataDTO.getPassengerRecords().iterator();

		while (empnlRecordDTOIterator.hasNext()) {

			String xaPnrNumber = "";

			paxNameListDTO = new XAPaxNameListDTO();
			empnlRecordDTO = (EMPNLRecordDTO) empnlRecordDTOIterator.next();

			// preapre the remarks dto..
			XAPNLFormatUtils.prepareRemarks(empnlRecordDTO);

			xaPnrNumber = getXAPnrNumber(emPnlMetaDataDTO, empnlRecordDTO, msgNamesHavingPartTwoThree, path);

			Iterator<EMRecordDTO> itRecordDTO = empnlRecordDTO.getRecords().iterator();
			while (itRecordDTO.hasNext()) {

				recordDTO = (EMRecordDTO) itRecordDTO.next();
				// Setting the PaxNameListsDTO information
				paxNameListDTO.setXAPnr(xaPnrNumber);

				paxNameListDTO.setTitle(getPassengerDerivedInfo(recordDTO.getParseTitle(), colPaxTitles)[0].toString());

				Object[] passengerInfo = getPassengerDerivedInfo(recordDTO.getFirstNameWithTitle(), colPaxTitles);
				paxNameListDTO.setFirstName(passengerInfo[1].toString());
				paxNameListDTO.setPaxType(passengerInfo[2].toString());
				paxNameListDTO.setLastName(recordDTO.getLastName());

				String ssrCode = "";
				paxNameListDTO.setSsrCode(ssrCode = checkToAppendTKNA(recordDTO.getSsrCode()));
				if (ssrCode.equals(XAParserConstants.SSR_CODES.CODE_TKNA)) {
					paxNameListDTO.setSsrRemarks(xaPnrNumber);
				} else {
					paxNameListDTO.setSsrRemarks(recordDTO.getSsrText() + " " + xaPnrNumber);
				}
				paxNameListDTO.setInfantFirstName(recordDTO.getInfantName());
				paxNameListDTO.setErrorDescription(paxNameListDTO.getErrorDescription() + " " + recordDTO.getErrorDescription());
				paxNameListDTO.setInfantLastName(recordDTO.getInfantLastName());
				paxNameListDTO.setInboundInfo(empnlRecordDTO.getInboundDetails());
				paxNameListDTO.setOutboundInfo(empnlRecordDTO.getOutboundDetails());

				paxNameListDTO.setDay(emPnlMetaDataDTO.getDay());
				paxNameListDTO.setMonth(emPnlMetaDataDTO.getMonth());
				paxNameListDTO.setArrivalAirportCode(emPnlMetaDataDTO.getDestinationAirport());
				paxNameListDTO.setDepartureAirportCode(emPnlMetaDataDTO.getBoardingAirport());
				paxNameListDTO.setFlightNumber(mappedSystemFlightNumber);
				paxNameListDTO.setDateDownloaded(emPnlMetaDataDTO.getDateDownloaded());
				paxNameListDTO.setRealFlightDate(emPnlMetaDataDTO.getRealDepartureDate());
				paxNameListDTO.setCabinClassCode(emPnlMetaDataDTO.getCcCode());

				colPaxNameListDTO.add(paxNameListDTO);
				if (itRecordDTO.hasNext()) {
					paxNameListDTO = new XAPaxNameListDTO();
				}
			}
		}

		XAPnlDAO pnlDAO = ReservationDAOUtils.DAOInstance.XA_PNL_DAO;
		pnlDAO.saveXAPnlEntry(pnl);

		for (Iterator<XAPaxNameListDTO> itColPaxNameListDTO = colPaxNameListDTO.iterator(); itColPaxNameListDTO.hasNext();) {
			paxNameListDTO = (XAPaxNameListDTO) itColPaxNameListDTO.next();
			paxNameListDTO.setPnlId(pnl.getPnlId());
		}

		return colPaxNameListDTO;
	}

	private static String decideSystemFlightNumber(String flightNumber) throws ModuleException {
		String sysFlightNumber = null;
		if (flightNumber.substring(0, 2).equals(AppSysParamsUtil.getCarrierCode())) {
			return flightNumber;
		}
		Map<String,String> hashMap = ReservationModuleUtils.getAirReservationConfig().getXaFlightNumberMap();
		sysFlightNumber = (String) hashMap.get(flightNumber);
		if (sysFlightNumber == null) {
			throw new ModuleException("airreservations.xapnl.flight.notmapped");
		}
		return sysFlightNumber;
	}

	/**
	 * Returns passenger derived information It is assumed that MR, MS, CHILD codes should be there for any lines.
	 * However there could be more codes too. The first priority will be given for the MR, MS and CHILD codes TODO Find
	 * ways to make it improve
	 * 
	 * @param name
	 * @param colTitles
	 * @return
	 */
	private static Object[] getPassengerDerivedInfo(String name, Collection<String> colTitles) {
		String title = ReservationInternalConstants.PassengerTitle.MR;
		String paxType = ReservationInternalConstants.PassengerType.ADULT;
		String firstName = name;
		boolean matchFound = false;
		String dbTitle;

		if (name.endsWith("MR")) {
			title = ReservationInternalConstants.PassengerTitle.MR;
			firstName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("MS")) {
			title = ReservationInternalConstants.PassengerTitle.MS;
			firstName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("CHD")) {
			title = ReservationInternalConstants.PassengerTitle.CHILD;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			firstName = name.substring(0, name.length() - 3);
		} else {
			for (Iterator<String> itColTitles = colTitles.iterator(); itColTitles.hasNext();) {
				dbTitle = (String) itColTitles.next();

				if (name.endsWith(dbTitle)) {
					matchFound = true;
					title = dbTitle;
					firstName = name.substring(0, name.length() - dbTitle.length());
				}
			}
		}

		// This means there is no matching record
		if (!matchFound) {
			if (name.endsWith("MRS")) {
				title = ReservationInternalConstants.PassengerTitle.MS;
				firstName = name.substring(0, name.length() - 3);
			} else if (name.endsWith("MISS")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				firstName = name.substring(0, name.length() - 4);
			} else if (name.endsWith("MSTR")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				firstName = name.substring(0, name.length() - 4);
			}
		}

		return new Object[] { title, firstName, paxType };
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Integer> getFailedXAPnls() throws ModuleException {

		XAPnlDAO pnlDAO = ReservationDAOUtils.DAOInstance.XA_PNL_DAO;

		Collection<Integer> pnlIds = pnlDAO.getFailedXAPnlIds(Integer.valueOf(
				ReservationModuleUtils.getAirReservationConfig().getXaPnlRetryLimit()).intValue());
		if (pnlIds == null || pnlIds.size() > 0) {
			Iterator<Integer> iterPnlIdsIterator = pnlIds.iterator();
			while (iterPnlIdsIterator.hasNext()) {
				Integer xaPnlId = (Integer) iterPnlIdsIterator.next();
				XAPnl pnl = pnlDAO.getXAPnlEntry(xaPnlId.intValue());
				pnl.setNumOfAttempts(pnl.getNumOfAttempts() + 1);
				Collection<XAPnlPaxEntry> paxEntries = pnlDAO.getXAPnlPaxEntries(xaPnlId.intValue(),
						ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED);
				if (paxEntries != null && paxEntries.size() > 0) {

					Iterator<XAPnlPaxEntry> iterPaxEntries = paxEntries.iterator();
					while (iterPaxEntries.hasNext()) {
						XAPnlPaxEntry paxEntry = (XAPnlPaxEntry) iterPaxEntries.next();
						paxEntry.setProcessedStatus(ReservationInternalConstants.XAPnlProcessStatus.NOT_PROCESSED);

					}
				}
				pnlDAO.saveXAPnlPaxEntries(paxEntries);
				pnlDAO.saveXAPnlEntry(pnl);
			}

			return pnlIds;
		}

		return null;
	}

	private static void checkIfXAPnlAlreadyExists(EMPNLMetaDataDTO dataDTO) throws ModuleException {
		XAPnlDAO pnlDAO = ReservationDAOUtils.DAOInstance.XA_PNL_DAO;

		if (pnlDAO.hasAnEqualXAPnl(dataDTO.getFlightNumber(), dataDTO.getBoardingAirport(), dataDTO.getDepartureDate(),
				Integer.valueOf(dataDTO.getPartNumber()))) {
			throw new ModuleException("airreservations.xapnl.already.exists");
		}
	}

	/**
	 * 
	 * @param metaDataDTO
	 * @param recordDTO
	 * @return
	 * @throws ModuleException
	 */
	private static String getXAPnrNumber(EMPNLMetaDataDTO metaDataDTO, EMPNLRecordDTO recordDTO,
			Collection<String> msgNamesHavingPartTwoOrThree, String path) throws ModuleException {

		boolean hasPartTwo = false;
		if (msgNamesHavingPartTwoOrThree != null && msgNamesHavingPartTwoOrThree.size() > 1) {
			hasPartTwo = true;
		}

		String xaPnr = recordDTO.getXAPnrNumber();
		if (xaPnr == null) {
			String tourID = recordDTO.getTourId();
			if (tourID == null && hasPartTwo) {
				xaPnr = checkXAPnrInOtherPartsTourIDs(metaDataDTO, XAParserConstants.MAPKEY_CONT, msgNamesHavingPartTwoOrThree,
						path);
				return xaPnr;
			} else {
				xaPnr = metaDataDTO.getXAPnrNumber(tourID);
			}
		}
		if (xaPnr == null) {

			log.error("############## XA PNR Number Was Null Going to Search in partTwo or Three###########");
			log.error("########## Tour Id: " + recordDTO.getTourId() + " ###########");
			log.error("########## Flight Number :" + metaDataDTO.getFlightNumber());
			log.error("########## Boarding Airport :" + metaDataDTO.getBoardingAirport());
			log.error("########## Destination Airport :" + metaDataDTO.getDestinationAirport());
			log.error("########## Departure Date :" + metaDataDTO.getDepartureDate());
			if (hasPartTwo) {
				xaPnr = checkXAPnrInOtherPartsTourIDs(metaDataDTO, recordDTO.getTourId(), msgNamesHavingPartTwoOrThree, path);
			}
			// throw new ModuleException("airreservations.xaPnl.xaPnrNull");
		}
		if (xaPnr == null || xaPnr.equals("")) {
			xaPnr = " ";
			recordDTO.setErrorDescription("searching for tour id in next part caused error");
		}
		return xaPnr;
	}

	private static String checkXAPnrInOtherPartsTourIDs(EMPNLMetaDataDTO metaDataDTO, String tourId,
			Collection<String> msgNamesHavingPartTwoOrThree, String path) throws ModuleException {

		if (msgNamesHavingPartTwoOrThree == null || msgNamesHavingPartTwoOrThree.size() <= 1) {
			return null;
		}

		try {
			Iterator<String> iterMsgs = msgNamesHavingPartTwoOrThree.iterator();
			int currentPartNumber = Integer.valueOf(metaDataDTO.getPartNumber()).intValue();

			while (iterMsgs.hasNext()) {
				File f = new File(path, iterMsgs.next().toString());

				EMPNLMetaDataDTO dataDTO = PNLDTOParser.getMetaDataDTO(f);
				if (dataDTO != null) {

					if (dataDTO.getFlightNumber().equals(metaDataDTO.getFlightNumber())
							&& (currentPartNumber != Integer.valueOf(dataDTO.getPartNumber()).intValue()
									&& metaDataDTO.getBoardingAirport().equals(dataDTO.getBoardingAirport()) && metaDataDTO
									.getDepartureDate().equals(dataDTO.getDepartureDate()))) {

						return dataDTO.getXAPnrNumber(tourId);

					}
				}
			}
		} catch (Exception e) {
			throw new ModuleException("pnldtoparser.searchtourid.error");
		}

		return null;
	}

	/**
	 * For Future integrations with Other AirLines This method will decide what the carrier code which is based on the
	 * value that was parsed from the PNL text file to the DTO.
	 * 
	 * @param carrierCode
	 *            - will be the value that was extracted from the Ex Airlines PNL.
	 * @return for now will return EK as we are integrated with Emirates only.
	 */
	private static String decideCarrierCode(String undecidedParam) {
		return "EK";

	}

	/**
	 * Finds out if the real departure date error occured or not
	 * 
	 * @param pfsMetaDataDTO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isRealDepartureDateErrorOccured(EMPNLMetaDataDTO metaDataDTO) throws ModuleException {
		boolean errorOccured = false;
		FlightBD flightBD = ReservationModuleUtils.getFlightBD();
		Date departureDate = metaDataDTO.getDepartureDate();

		// Checking for flight segment information
		try {
			Collection<FlightSegement> colFlightSegement = flightBD.getFlightSegmentsForLocalDate(metaDataDTO.getBoardingAirport(), null,
					metaDataDTO.getFlightNumber(), departureDate, false);

			// Checking the departure date
			if (colFlightSegement != null) {
				if (colFlightSegement.size() == 1) {
					FlightSegement fseg = (FlightSegement) ((ArrayList<FlightSegement>) (colFlightSegement)).get(0);
					metaDataDTO.setRealDepartureDate(fseg.getEstTimeDepatureLocal());
				} else {
					metaDataDTO.setRealDepartureDate(departureDate);
					errorOccured = true;
				}
			} else {
				metaDataDTO.setRealDepartureDate(departureDate);
				errorOccured = true;
			}

		} catch (ModuleException e) {
			metaDataDTO.setRealDepartureDate(departureDate);
			errorOccured = true;
		}

		return errorOccured;
	}

	/**
	 * Returns all passenger final sales DTO(s)
	 * 
	 * @param path
	 * @param strMsgName
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<XAPaxNameListDTO> getAllPaxNameListDTOs(String path, String strMsgName, Collection<String> msgNamesHavingPartTwoThree)
			throws ModuleException {
		File fileMsgName = new File(path, strMsgName);
		return getXAPaxNameListDTO(fileMsgName, msgNamesHavingPartTwoThree, path);
	}

	/**
	 * @param args
	 * 
	 *            Temporary Main Method
	 */
//	public static void main(String[] args) throws Exception {
//		Collection<String> colFileNames = new ArrayList<String>();// FolderReader.getFiles("c://isaconfig)new ArrayList();
//		colFileNames.add("TEST.txt");
//
//		try {
//			System.out.println(" ############### ABOUT TO PARSE " + colFileNames.size() + " DOCUMENTS ");
//			Iterator<String> itColFileNames = colFileNames.iterator();
//			String fileName;
//			while (itColFileNames.hasNext()) {
//				fileName = (String) itColFileNames.next();
//
//				File f = new File("C://isaconfig//xapnlprocesspath", fileName);
//				System.out.println(fileName);
//				EMPNLMetaDataDTO emPnlMetaDataDTO = PNLDTOParser.getMetaDataDTO(f);
//				System.out.println("BOARDING AIRPORT : " + emPnlMetaDataDTO.getBoardingAirport());
//				System.out.println("CC CODE : " + emPnlMetaDataDTO.getCcCode());
//				System.out.println("DAY  : " + emPnlMetaDataDTO.getDay());
//				System.out.println("DAY MONTH : " + emPnlMetaDataDTO.getDayMonth());
//				System.out.println("DEPARTURE DATE  : " + emPnlMetaDataDTO.getDepartureDate());
//				System.out.println("DESAIRPOT NUM PAX CCCODE  : " + emPnlMetaDataDTO.getDesAirportNumPaxCCCode());
//				System.out.println("DESTINATION AIRPORT : " + emPnlMetaDataDTO.getDestinationAirport());
//				System.out.println("FLIGHT NUMBER : " + emPnlMetaDataDTO.getFlightNumber());
//				System.out.println("FROM ADDRESS : " + emPnlMetaDataDTO.getFromAddress());
//				System.out.println("MONTH : " + emPnlMetaDataDTO.getMonth());
//				System.out.println("PART NUMBER : " + emPnlMetaDataDTO.getPartNumber());
//				System.out.println("REAL DEP DATE : " + emPnlMetaDataDTO.getRealDepartureDate());
//				System.out.println("TOT NUM OF PAX : " + emPnlMetaDataDTO.getTotNumberOfPax());
//				System.out.println("BOARDING AIRPORT : " + emPnlMetaDataDTO.getRealDepartureDate());
//				Iterator iterEmPnlRecordDTO = emPnlMetaDataDTO.getPassengerRecords().iterator();
//				System.out.println("###########################################  PAX DETAILS   #############################");
//				int i = 0;
//				while (iterEmPnlRecordDTO.hasNext()) {
//
//					i++;
//					System.out.println("#################### PAX DETAILS IN LINE " + i
//							+ "#######################################");
//					EMPNLRecordDTO recordDTO = (EMPNLRecordDTO) iterEmPnlRecordDTO.next();
//					XAPNLFormatUtils.prepareRemarks(recordDTO);
//					System.out.println("ERROR DESCRIPTION : " + recordDTO.getErrorDescription());
//					System.out.println("INBOUND DETAILS : " + recordDTO.getInboundDetails());
//					System.out.println("OUTBOUND DETAILS : " + recordDTO.getOutboundDetails());
//					System.out.println("TICKET NUMBER : " + recordDTO.getTicketnumber());
//					System.out.println("TOUR ID : " + recordDTO.getTourId());
//					System.out.println("XA PNR NUMBER : " + recordDTO.getXAPnrNumber());
//					System.out.println("#################### NAMES AND REMARKS #######################################");
//					Iterator<EMRecordDTO> iterEmRecDto = recordDTO.getRecords().iterator();
//					int j = 0;
//					while (iterEmRecDto.hasNext()) {
//
//						j++;
//						System.out.println("####################LINE: " + i + " PAX Record : " + j);
//						EMRecordDTO recordDTO2 = (EMRecordDTO) iterEmRecDto.next();
//						System.out.println("ERROR DESCRIPTION : " + recordDTO2.getErrorDescription());
//						System.out.println("TITLE : " + recordDTO2.getParseTitle());
//						System.out.println("FIRST NAME : " + recordDTO2.getFirstNameWithTitle());
//						System.out.println("FIRST NAME WITH TITLE : " + recordDTO2.getFirstNameWithTitle());
//						System.out.println("LAST NAME : " + recordDTO2.getLastName());
//						System.out.println("INFANT NAME :" + recordDTO2.getInfantName());
//						System.out.println("INFANT LAST NAME : " + recordDTO2.getInfantLastName());
//						System.out.println("INFANT SSR CODE : " + recordDTO2.getInfantSSRCode());
//						System.out.println("INFANT SSR TEXT : " + recordDTO2.getInfantSSRText());
//						System.out.println("SSR CODE : " + recordDTO2.getSsrCode());
//						System.out.println("SSR TEXT : " + recordDTO2.getSsrText());
//
//					}
//				}
//
//				System.out.println(emPnlMetaDataDTO);
//			}
//			System.out.println(" ############### FINISHED PARSING " + colFileNames.size() + " DOCUMENTS ");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
