/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext;

/**
 * @author udithad
 *
 */
public class PnlAdlDeliveryInformation {

	private String sitaaddresses[];
	private String sendingMethod;

	public String[] getSitaaddresses() {
		return sitaaddresses;
	}

	public void setSitaaddresses(String[] sitaaddresses) {
		this.sitaaddresses = sitaaddresses;
	}

	public String getSendingMethod() {
		return sendingMethod;
	}

	public void setSendingMethod(String sendingMethod) {
		this.sendingMethod = sendingMethod;
	}
	
	
	
}
