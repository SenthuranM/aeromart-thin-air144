/**
 * 
 */
package com.isa.thinair.airreservation.core.persistence.dao;

/**
 * @author lsandeepa
 *
 */

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.external.AgentTopUpX3DTO;
import com.isa.thinair.airreservation.api.model.AgentTopUp;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;


public interface AgentTopUpTransferDAO extends ExternalSystemDAO {
	
	public Collection<AgentTopUp> insertTopUpToInternal(Collection<AgentTopUpX3DTO> col);
	
	public void updateInternalTopUpTable(Date date) throws Exception;

	public Collection<AgentTopUpX3DTO> getTopUps(Date date);

	public void insertTopUpToExternal(Collection<AgentTopUpX3DTO> col, AccontingSystemTemplate template)
			throws ClassNotFoundException, SQLException, Exception;
}
