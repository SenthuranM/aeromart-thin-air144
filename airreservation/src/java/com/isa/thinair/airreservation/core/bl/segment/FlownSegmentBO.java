package com.isa.thinair.airreservation.core.bl.segment;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;

public class FlownSegmentBO {
	private static Log log = LogFactory.getLog(FlownSegmentBO.class);

	public static void updateTicketValidity(Reservation reservation, FlownAssitUnit flownAssitUnit, Date ticketValidTill,
			Collection<Integer> validityApplicableFltSegIds) throws ModuleException {
		if (reservation != null && flownAssitUnit != null) {
			for (ReservationSegment pnrSeg : reservation.getSegments()) {
				if (flownAssitUnit.getFlownPnrSegIds().contains(pnrSeg.getPnrSegId())) {
					if (validityApplicableFltSegIds != null && validityApplicableFltSegIds.contains(pnrSeg.getFlightSegId())) {
						pnrSeg.setTicketValidTill(ticketValidTill);
					} else {
						pnrSeg.setTicketValidTill(null);
					}
				}
			}
		}
	}
}
