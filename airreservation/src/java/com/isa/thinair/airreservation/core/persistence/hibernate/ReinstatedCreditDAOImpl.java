/**
 * 
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

import com.isa.thinair.airreservation.api.model.ReinstatedCredit;
import com.isa.thinair.airreservation.core.persistence.dao.ReinstatedCreditDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @author indika
 * @isa.module.dao-impl dao-name="ReinstatedCreditDAO"
 */
public class ReinstatedCreditDAOImpl extends PlatformBaseHibernateDaoSupport implements ReinstatedCreditDAO {

	/**
	 * Save or update Reinstated Credit
	 */
	public void saveReinstateCredit(ReinstatedCredit reinstateCredit) {
		hibernateSaveOrUpdate(reinstateCredit);
	}
}
