/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.FeaturePack;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlAdditionalDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public abstract class AdditionalInformationBaseStructure implements
		MessageDataStructure<PassengerCollection, PnlAdditionalDataContext> {

	private Map<String, List<DestinationFare>> pnlAdlDestinationMap;

	protected List<PassengerInformation> passengerDetailDtoCollection;

	public abstract void buildSeatInfomration() throws ModuleException;

	public abstract void buildMealInfomration() throws ModuleException;

	public abstract void buildBaggageInfomration(String departureAirportCode)
			throws ModuleException;

	public abstract void buildSsrInfomration() throws ModuleException;

	public abstract void buildWaitlistPassengerInformation()
			throws ModuleException;

	public abstract void buildInboundConnectionsInformation(
			String departureAirportCode, Integer flightId)
			throws ModuleException;

	public abstract void buildOnwardConnectionsInformation(
			String departureAirportCode, Integer flightId)
			throws ModuleException;

	public abstract void buildInfantsInformation(Integer flightId)
			throws ModuleException;

	protected List<PassengerInformation> getPassengerDetailsDTOList(
			Map<String, List<DestinationFare>> pnlAdlDestinationMap) {
		List<PassengerInformation> reservationPaxDetailsDTOs = new ArrayList<PassengerInformation>();
		for (Map.Entry<String, List<DestinationFare>> entry : pnlAdlDestinationMap
				.entrySet()) {
			List<DestinationFare> destinationDTOs = entry.getValue();
			reservationPaxDetailsDTOs
					.addAll(getPassengerDetailsDTOBy(destinationDTOs));
		}
		return reservationPaxDetailsDTOs;
	}

	protected List<PassengerInformation> getPassengerDetailsDTOBy(
			List<DestinationFare> destinationDTOs) {
		List<PassengerInformation> passengerInformations = new ArrayList<PassengerInformation>();
		for (DestinationFare destinationDTO : destinationDTOs) {	
			if(destinationDTO
					.getAddPassengers()!=null){
				populatePassengers(passengerInformations, destinationDTO
					.getAddPassengers());
			}
			if(destinationDTO
					.getChangePassengers() != null){
				populatePassengers(passengerInformations, destinationDTO
					.getChangePassengers());
			}
		}
		return passengerInformations;
	}

	protected void populatePassengers(
			List<PassengerInformation> passengerInformations,
			Map<String, List<PassengerInformation>> passengersMap) {
		for (Map.Entry<String, List<PassengerInformation>> entry : passengersMap
				.entrySet()) {
			passengerInformations.addAll(entry.getValue());
		}
	}

	protected Map<String, List<DestinationFare>> getPnlAdlDestinationMap(
			PassengerCollection passengerCollection) {
		return passengerCollection.getPnlAdlDestinationMap();
	}

	@Override
	public PassengerCollection getDataStructure(PnlAdditionalDataContext context)
			throws ModuleException {
		pnlAdlDestinationMap = getPnlAdlDestinationMap(context
				.getPassengerCollection());
		passengerDetailDtoCollection = getPassengerDetailsDTOList(pnlAdlDestinationMap);
		FeaturePack featurePack = context.getFeaturePack();
		if (featurePack != null) {
			if (featurePack.isShowSeatMap()) {
				buildSeatInfomration();
			}

			if (featurePack.isShowMeals()) {
				buildMealInfomration();
			}

			if (featurePack.isShowBaggage()) {
				buildBaggageInfomration(context.getDepartureAirportCode());
			}

			buildSsrInfomration();
			buildWaitlistPassengerInformation();
			buildInboundConnectionsInformation(
					context.getDepartureAirportCode(), context.getFlightId());
			buildOnwardConnectionsInformation(
					context.getDepartureAirportCode(), context.getFlightId());
			buildInfantsInformation(context.getFlightId());
		}

		return context.getPassengerCollection();
	}
}
