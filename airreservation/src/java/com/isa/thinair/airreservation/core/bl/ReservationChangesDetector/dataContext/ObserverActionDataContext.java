package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext;

import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.Reservation;

public class ObserverActionDataContext {

	private Reservation oldReservation;
	private Reservation newReservation;
	private Map<Integer, Set<Integer>> addMap;
	private Map<Integer, Set<Integer>> delMap;
	private Map<Integer, Set<Integer>> chgMap;
	private Set<String>removeAllSSROnUpdate;

	public Reservation getOldReservation() {
		return oldReservation;
	}

	public void setOldReservation(Reservation oldReservation) {
		this.oldReservation = oldReservation;
	}

	public Reservation getNewReservation() {
		return newReservation;
	}

	public void setNewReservation(Reservation newReservation) {
		this.newReservation = newReservation;
	}

	public Map<Integer, Set<Integer>> getAddMap() {
		return addMap;
	}

	public void setAddMap(Map<Integer, Set<Integer>> addPax) {
		this.addMap = addPax;
	}

	public Map<Integer, Set<Integer>> getDelMap() {
		return delMap;
	}

	public void setDelMap(Map<Integer, Set<Integer>> delPax) {
		this.delMap = delPax;
	}

	public Map<Integer, Set<Integer>> getChgMap() {
		return chgMap;
	}

	public void setChgMap(Map<Integer, Set<Integer>> chgPax) {
		this.chgMap = chgPax;
	}

	public Set<String> getRemoveAllSSROnUpdate() {
		return removeAllSSROnUpdate;
	}

	public void setRemoveAllSSROnUpdate(Set<String> removeAllSSROnUpdate) {
		this.removeAllSSROnUpdate = removeAllSSROnUpdate;
	}

}
