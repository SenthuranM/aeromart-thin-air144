/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryChargesDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryPaymentsDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaymentDetailDTO;
import com.isa.thinair.airreservation.api.dto.RecieptLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.RecieptPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxSegmentSSRStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils.TEMPLATE_TYPES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Indika Athauda
 * 
 */
public class RecieptBL {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(RecieptBL.class);

	/** Holds the airreservation config instance */
	private static final AirReservationConfig config = ReservationModuleUtils.getAirReservationConfig();

	/**
	 * Hide the constructor
	 */
	private RecieptBL() {

	}

	/**
	 * Gets itinerary for print
	 * 
	 * @param recieptLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public static String getRecieptForPrint(RecieptLayoutModesDTO recieptLayoutModesDTO,
			Collection<Integer> colSelectedPnrPaxIds, CredentialsDTO credentialsDTO, boolean isCheckIndividual)
			throws ModuleException {
		String templateName = config.getRecieptPrintTemplate();
		recieptLayoutModesDTO.setTemplateName(templateName);

		if (isCheckIndividual) {
			Iterator<Integer> iterator = colSelectedPnrPaxIds.iterator();

			StringBuilder sb = new StringBuilder();
			Collection<Integer> paxIds;

			while (iterator.hasNext()) {
				paxIds = new ArrayList<Integer>();
				paxIds.add(iterator.next());
				sb.append(RecieptBL.getItineraryForPrint(recieptLayoutModesDTO, paxIds, credentialsDTO));

				if (iterator.hasNext()) {
					sb.append("<h2 style='page-break-before: always;'></h2>");
				}
			}

			return sb.toString();
		} else {
			return RecieptBL.getItineraryForPrint(recieptLayoutModesDTO, colSelectedPnrPaxIds, credentialsDTO);
		}
	}

	/**
	 * Get the itinerary
	 * 
	 * @param recieptLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private static String getItineraryForPrint(RecieptLayoutModesDTO recieptLayoutModesDTO,
			Collection<Integer> colSelectedPnrPaxIds, CredentialsDTO credentialsDTO) throws ModuleException {
		Reservation reservation = getReservation(recieptLayoutModesDTO, credentialsDTO);

		HashMap itineraryMap = (HashMap) getItineraryDataMap(recieptLayoutModesDTO, reservation, colSelectedPnrPaxIds,
				credentialsDTO);
		TemplateEngine engine = null;
		String templateName = recieptLayoutModesDTO.getTemplateName();
		Locale locale = recieptLayoutModesDTO.getLocale();
		String localeSpecificTemplateName = templateName;
		StringWriter writer = null;

		if ((locale != null) && (!locale.getLanguage().equals(Locale.getDefault().getLanguage()))) {
			if (templateName.indexOf(".") > 0) {
				localeSpecificTemplateName = templateName.substring(0, templateName.indexOf(".")) + "_"
						+ locale.getLanguage().trim().toUpperCase() + templateName.substring(templateName.indexOf("."));
			} else {
				localeSpecificTemplateName = templateName + "_" + locale.getLanguage().trim();
			}
		}

		try {
			engine = new TemplateEngine();
			writer = new StringWriter();
		} catch (Exception e) {
			throw new ModuleException("airreservations.itinerary.errorCreatingFromTemplate", e);
		}
		if (itineraryMap != null) {
			engine.writeTemplate(itineraryMap, localeSpecificTemplateName, writer);
		}

		return writer.toString();
	}

	/**
	 * Loads the reservation
	 * 
	 * @param recieptLayoutModesDTO
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Reservation getReservation(RecieptLayoutModesDTO recieptLayoutModesDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		Reservation reservation = null;

		// Validating the parameters
		if (recieptLayoutModesDTO.getPnr() == null || recieptLayoutModesDTO.getLocale() == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		// Loads the reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(recieptLayoutModesDTO.getPnr());
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		// If it's calling from XBE
		if (credentialsDTO.getSalesChannelCode() != null
				&& credentialsDTO.getSalesChannelCode().intValue() == SalesChannel.TRAVEL_AGENT) {
			pnrModesDTO.setLoadLocalTimes(true);
			reservation = PnrZuluProxy.getReservationWithLocalTimes(pnrModesDTO, credentialsDTO);
			String defaultCarrierCode = credentialsDTO.getDefaultCarrierCode();
			if (defaultCarrierCode == null || "".equals(defaultCarrierCode)) {
				defaultCarrierCode = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			}
			recieptLayoutModesDTO.setCarrierCode(defaultCarrierCode);
			// If it's from IBE
		} else {
			reservation = ReservationProxy.getReservation(pnrModesDTO);
		}

		return reservation;
	}

	/**
	 * Returns the Itinerary Data Map
	 * 
	 * @param recieptLayoutModesDTO
	 * @param reservation
	 * @param colSelectedPnrPaxIds
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static Map<String, Object> getItineraryDataMap(RecieptLayoutModesDTO recieptLayoutModesDTO, Reservation reservation,
			Collection<Integer> colSelectedPnrPaxIds, CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside getItineraryMap");

		CommonTemplatingDTO commonTemplatingDTO = null;
		String termsNConditionsData;
		List<ReservationInsurance> resinsurences = null;

		if (CommonsServices.getGlobalConfig().getActiveCabinClassesMap() == null
				|| CommonsServices.getGlobalConfig().getActiveCabinClassesMap().size() == 1) {
			recieptLayoutModesDTO.setIncludeClassOfService(false);
		} else {
			recieptLayoutModesDTO.setIncludeClassOfService(true);
		}

		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_CARRIER_LOGO_WITH_FLT_INFO))) {
			recieptLayoutModesDTO.setIncludeThumbnailLogo(true);
		}

		// If it's calling from XBE
		if (credentialsDTO.getSalesChannelCode() != null
				&& credentialsDTO.getSalesChannelCode().intValue() == SalesChannel.TRAVEL_AGENT) {
			termsNConditionsData = ReservationTemplateUtils.getTermsNConditions(recieptLayoutModesDTO.getLocale(),
					TEMPLATE_TYPES.XBE_TEMPLATE);
			// If it's from IBE
		} else {
			termsNConditionsData = ReservationTemplateUtils.getTermsNConditions(recieptLayoutModesDTO.getLocale(),
					TEMPLATE_TYPES.IBE_TEMPLATE);
			commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(recieptLayoutModesDTO.getCarrierCode());
		}

		// Check whether all passengers are correct passengers
		Map<Integer, Integer> adultAndInfantMap = checkValidityOfSelectedPassengers(reservation, colSelectedPnrPaxIds);

		if (colSelectedPnrPaxIds != null && adultAndInfantMap.size() == 0) {
			return null;
		} else {
			boolean showAmountsInPayCurrency = AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled();
			// Return the reservation for the template
			ReservationPaxDTO reservationPaxDTO = ReservationCoreUtils.parseReservation(reservation, adultAndInfantMap);

			// Setting the base currency
			String baseCurrency = AppSysParamsUtil.getBaseCurrency();
			reservationPaxDTO.setBaseCurrency(baseCurrency);
			reservationPaxDTO.setShowAmountsInPayCurrency(showAmountsInPayCurrency);

			// Set Agent details
			Agent ownerAgent = null;
			if (reservation.getAdminInfo().getOwnerAgentCode() != null) {
				TravelAgentBD travelAgentBD = ReservationModuleUtils.getTravelAgentBD();
				ownerAgent = travelAgentBD.getAgent(reservation.getAdminInfo().getOwnerAgentCode());
			}

			if (showAmountsInPayCurrency) {
				String totalPaidAmountInPayCurrency = getTotalPaidAmountInPayCurrency(reservation.getPassengers());

				String totalAvailBalanceInPayCurrency = getAvailableBalance(reservation);

				reservationPaxDTO.setSpecifiedCurrencyCode("");

				// If no payments exist
				if (BeanUtils.nullHandler(totalPaidAmountInPayCurrency).isEmpty()) {
					String lastCurrencyCode = BeanUtils.nullHandler(reservation.getLastCurrencyCode());
					StringBuilder lastCurrencyAmtMsg = new StringBuilder();
					lastCurrencyAmtMsg.append(AccelAeroCalculator.getDefaultBigDecimalZero() + " ");

					if (lastCurrencyCode.isEmpty()) {
						lastCurrencyAmtMsg.append(AppSysParamsUtil.getBaseCurrency());
					} else {
						lastCurrencyAmtMsg.append(lastCurrencyCode);
					}
					reservationPaxDTO.setTotalPaidAmountInPayCurrency(lastCurrencyAmtMsg.toString());
					reservationPaxDTO.setTotalAvailableBalanceInPayCurrency(totalAvailBalanceInPayCurrency);
				} else {
					reservationPaxDTO.setTotalPaidAmountInPayCurrency(totalPaidAmountInPayCurrency);
					reservationPaxDTO.setTotalAvailableBalanceInPayCurrency(totalAvailBalanceInPayCurrency);
				}
			} else {
				String currencyCode = BeanUtils.nullHandler(getSpecifiedCurrencyCode(ownerAgent, baseCurrency));

				if (currencyCode.length() > 0) {
					reservationPaxDTO.setSpecifiedCurrencyCode(currencyCode);
					reservationPaxDTO.setSpecifiedCurrency(CurrencyConvertorUtils.getCurrency(currencyCode));
					reservationPaxDTO.setCurrencyExchangeRate(new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
							.getExchangeRate(baseCurrency, currencyCode));
				}
			}

			// If only segments exist. Cos this segments will have only the
			// confirmed segments.
			// Thus cancelled segments will be removed
			if (reservationPaxDTO.getSegments() != null && reservationPaxDTO.getSegments().size() != 0) {
				// Changing the segment codes to segment descriptions
				Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservationPaxDTO.getSegments().iterator();
				ReservationSegmentDTO reservationSegmentDTO;
				Collection<String> colAirportCodes = new ArrayList<String>();

				while (itReservationSegmentDTO.hasNext()) {
					reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
					colAirportCodes.addAll(Arrays.asList(reservationSegmentDTO.getSegmentCode().split("/")));
				}

				AirportBD airportBD = ReservationModuleUtils.getAirportBD();
				Map<String, CachedAirportDTO> mapAirportCodes = airportBD.getCachedOwnAirportMap(colAirportCodes);

				itReservationSegmentDTO = reservationPaxDTO.getSegments().iterator();
				while (itReservationSegmentDTO.hasNext()) {
					reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

					String composedSegment = "";
					String[] segments = reservationSegmentDTO.getSegmentCode().split("/");

					for (int i = 0; i < segments.length; i++) {
						CachedAirportDTO airport = mapAirportCodes.get(segments[i]);
						// Haider 4Feb09
						if (AppSysParamsUtil.isHideStopOverEnabled() && segments.length > 2 && i > 0 && i < (segments.length - 1))
							continue;
						if (i != 0) {
							composedSegment += " / ";
						}
						composedSegment += airport.getAirportName();
					}

					reservationSegmentDTO.setSegmentDescription(composedSegment);
				}
			}

			Map<Integer, Map<String, ItineraryPaymentsDTO>> itineraryPayDTOsByPaxByCur = new LinkedHashMap<Integer, Map<String, ItineraryPaymentsDTO>>();
			Collection<ItineraryChargesDTO> colItineraryChargesDTO = new ArrayList<ItineraryChargesDTO>();

			// Capturing the payment details
			if (recieptLayoutModesDTO.isIncludeDetailPayments()) {
				itineraryPayDTOsByPaxByCur = getRecieptPaymentsDTOs(reservation, adultAndInfantMap, recieptLayoutModesDTO);
			} else {
				// Check to see whether or not to show any credit card information
				// in itinerary
				if (AppSysParamsUtil.showCreditCardInfoInItineraryAlways()) {
					itineraryPayDTOsByPaxByCur = getReceiptCreditCardPaymentsOnly(reservation, adultAndInfantMap,
							recieptLayoutModesDTO);
				}
			}

			boolean showPayCurrencyAmtsInPayDetails = false;
			Collection<ItineraryPaymentsDTO> colItineraryPaymentDTOs = new ArrayList<ItineraryPaymentsDTO>();
			if (itineraryPayDTOsByPaxByCur != null && itineraryPayDTOsByPaxByCur.size() > 0) {
				Integer pnrPaxId = null;

				Map<String, ItineraryPaymentsDTO> mapPaymentPerPaxByCur = null;
				Collection<ItineraryPaymentsDTO> colPaymentsPerPax = null;
				String currencyCode = null;
				for (Iterator<Integer> itineraryPayDTOsByPaxByCurIt = itineraryPayDTOsByPaxByCur.keySet().iterator(); itineraryPayDTOsByPaxByCurIt
						.hasNext();) {
					pnrPaxId = (Integer) itineraryPayDTOsByPaxByCurIt.next();
					mapPaymentPerPaxByCur = (Map<String, ItineraryPaymentsDTO>) itineraryPayDTOsByPaxByCur.get(pnrPaxId);
					if (!showPayCurrencyAmtsInPayDetails) {
						if (mapPaymentPerPaxByCur != null) {
							for (Iterator<String> mapPaymentPerPaxByCurKeyIt = mapPaymentPerPaxByCur.keySet().iterator(); mapPaymentPerPaxByCurKeyIt
									.hasNext();) {
								currencyCode = (String) mapPaymentPerPaxByCurKeyIt.next();
								if (!baseCurrency.equals(currencyCode)) {
									showPayCurrencyAmtsInPayDetails = true;
									break;
								}
							}
						}
					}

					colPaymentsPerPax = ReservationApiUtils.getSerializableValuesCollection(mapPaymentPerPaxByCur);
					if (colPaymentsPerPax != null && colPaymentsPerPax.size() > 0) {
						colItineraryPaymentDTOs.addAll(colPaymentsPerPax);
					}
				}
			}

			// Capturing the charges
			if (recieptLayoutModesDTO.isIncludeDetailCharges()) {
				colItineraryChargesDTO = getItineraryChargesDTOs(reservation, adultAndInfantMap);
			}

			// Get the external charges
			Collection<ExternalChgDTO> colExternalChgDTO = getExternalCharges(reservation, adultAndInfantMap, credentialsDTO);

			// Get Seating, Meal details
			// Collection colItinerarySeatDTO = getSeatInformation(reservation);
			PaxSSRInfoDTO colItinerarySeatDTO = getAdditionalServiceInformation(reservation, false);

			PaxSSRInfoDTO colItinerarySSRDTO = getAdditionalServiceInformation(reservation, true);

			// Create carrier code legend
			String strCarrierCodeLegend = composeCarrierDescLegend(reservationPaxDTO);

			RecieptPaxDTO recieptPaxDTO = buildRecieptPaxDTO(reservationPaxDTO);

			boolean printPassengerDetails = false;

			resinsurences = reservation.getReservationInsurance();

			// Setting the Paramters
			Map<String, Object> itineraryDatamap = new HashMap<String, Object>();
			itineraryDatamap.put("reservationPaxDTO", reservationPaxDTO);
			itineraryDatamap.put("recieptPaxDTO", recieptPaxDTO);
			itineraryDatamap.put("printPassengerDetails", printPassengerDetails);
			itineraryDatamap.put("termsNConditions", termsNConditionsData);
			itineraryDatamap.put("ownerAgent", ownerAgent);
			itineraryDatamap.put("includePrices", new Boolean(recieptLayoutModesDTO.isIncludePassengerPrices()));
			itineraryDatamap.put("companyAddress", strItineraryAddress(recieptLayoutModesDTO.getCarrierCode()));
			itineraryDatamap.put("showPayCurrencyAmtsInPayDetails", new Boolean(showPayCurrencyAmtsInPayDetails));
			itineraryDatamap.put("colItineraryPaymentsDTO", colItineraryPaymentDTOs);
			itineraryDatamap.put("colItineraryChargesDTO", colItineraryChargesDTO);
			itineraryDatamap.put("colExternalChgDTO", colExternalChgDTO);
			itineraryDatamap.put("cabinClassesLegend",
					ReservationApiUtils.getCabinClassesLegend(recieptLayoutModesDTO.isIncludeClassOfService()));
			itineraryDatamap.put("barcode",
					generateBarcode(reservationPaxDTO.getPnr(), reservationPaxDTO.getSegmentsOrderedByZuluDepartureDate()));
			itineraryDatamap.put("carrierLegend", strCarrierCodeLegend);
			itineraryDatamap.put("colSeatPaxDTO", colItinerarySeatDTO);
			itineraryDatamap.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
			if (commonTemplatingDTO != null) {
				itineraryDatamap.put("commonTemplatingDTO", commonTemplatingDTO);
			}
//			if (resinsurence != null && resinsurence.getPolicyCode() != null) {
//				itineraryDatamap.put("policyNo", resinsurence.getPolicyCode().trim());
//			} else {
//				itineraryDatamap.put("policyNo", "");
//			}
			itineraryDatamap.put("policyNo",getPolicyCodeString(resinsurences));
			itineraryDatamap.put("logoImageName", recieptLayoutModesDTO.getLogoImageName());
			itineraryDatamap.put("includeClassOfService", new Boolean(recieptLayoutModesDTO.isIncludeClassOfService()));
			itineraryDatamap.put("includeThumbnailLogo", new Boolean(recieptLayoutModesDTO.isIncludeThumbnailLogo()));
			itineraryDatamap.put("colSSRPaxDTO", colItinerarySSRDTO);
			itineraryDatamap.put("recieptNo", recieptLayoutModesDTO.getRecieptNumber());
			itineraryDatamap.put("paymentMode", recieptLayoutModesDTO.getPaymentMode());
			itineraryDatamap.put("recieptLayoutModesDTO", recieptLayoutModesDTO);
			log.debug("Exit getItineraryMap");
			return itineraryDatamap;
		}
	}

	private static RecieptPaxDTO buildRecieptPaxDTO(ReservationPaxDTO reservationPaxDTO) {

		RecieptPaxDTO recieptPaxDTO = new RecieptPaxDTO();

		// --- Per Pax -----------
		BigDecimal perAdultFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perChildFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perInfantFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		/*
		 * Per pax charge, ant per pax total cannot be used statically as different pax might got different charge based
		 * on charge adjustments
		 */

		// --- Pax Type -------------
		BigDecimal AdultsTotalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal ChildsTotalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal InfantsTotalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal AdultsTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal ChildsTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal InfantsTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal AdultsTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal ChildsTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal InfantsTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		recieptPaxDTO.setTotalAdultCount(reservationPaxDTO.getAdultCount());
		recieptPaxDTO.setTotalChildCount(reservationPaxDTO.getChildCount());
		recieptPaxDTO.setTotalInfantCount(reservationPaxDTO.getInfantCount());

		recieptPaxDTO.setTotalCharge(reservationPaxDTO.getTotalTicketCharge());
		recieptPaxDTO.setTotalFare(reservationPaxDTO.getTotalTicketFare());
		recieptPaxDTO.setTotalReservation(reservationPaxDTO.getTotalTicketAmount());

		Collection<ReservationPaxDetailsDTO> reservationPaxDetailsDTOs = reservationPaxDTO.getPassengers();
		for (Iterator<ReservationPaxDetailsDTO> iterator = reservationPaxDetailsDTOs.iterator(); iterator.hasNext();) {
			ReservationPaxDetailsDTO reservationPaxDetailsDTO = (ReservationPaxDetailsDTO) iterator.next();

			if (reservationPaxDetailsDTO.getPaxType().equalsIgnoreCase(PaxTypeTO.ADULT)) {
				if (perAdultFare.compareTo(new BigDecimal(0)) == 0) { // Per Adult fare value is 0
					perAdultFare = reservationPaxDetailsDTO.getTotalFare();
				}
				AdultsTotalFare = AdultsTotalFare.add(reservationPaxDetailsDTO.getTotalFare());
				AdultsTotalCharge = AdultsTotalCharge.add(reservationPaxDetailsDTO.getTotalChargeAmount());
				AdultsTotal = AdultsTotal.add(AccelAeroCalculator.add(reservationPaxDetailsDTO.getTotalFare(),
						reservationPaxDetailsDTO.getTotalChargeAmount()));

			}

			if (reservationPaxDetailsDTO.getPaxType().equalsIgnoreCase(PaxTypeTO.CHILD)) {
				if (perChildFare.compareTo(new BigDecimal(0)) == 0) { // Per Child fare value is 0
					perChildFare = reservationPaxDetailsDTO.getTotalFare();
				}
				ChildsTotalFare = ChildsTotalFare.add(reservationPaxDetailsDTO.getTotalFare());
				ChildsTotalCharge = ChildsTotalCharge.add(reservationPaxDetailsDTO.getTotalChargeAmount());
				ChildsTotal = ChildsTotal.add(AccelAeroCalculator.add(reservationPaxDetailsDTO.getTotalFare(),
						reservationPaxDetailsDTO.getTotalChargeAmount()));
			}

			if (reservationPaxDetailsDTO.getPaxType().equalsIgnoreCase(PaxTypeTO.INFANT)) {
				if (perInfantFare.compareTo(new BigDecimal(0)) == 0) { // Per Infant fare value is 0
					perInfantFare = reservationPaxDetailsDTO.getTotalFare();
				}
				InfantsTotalFare = InfantsTotalFare.add(reservationPaxDetailsDTO.getTotalFare());
				InfantsTotalCharge = InfantsTotalCharge.add(reservationPaxDetailsDTO.getTotalChargeAmount());
				InfantsTotal = InfantsTotal.add(AccelAeroCalculator.add(reservationPaxDetailsDTO.getTotalFare(),
						reservationPaxDetailsDTO.getTotalChargeAmount()));
			}
		}

		recieptPaxDTO.setPerAdultFare(perAdultFare);
		recieptPaxDTO.setPerChildFare(perChildFare);
		recieptPaxDTO.setPerInfantFare(perInfantFare);
		recieptPaxDTO.setAdultsTotalFare(AdultsTotalFare);
		recieptPaxDTO.setAdultsTotalCharge(AdultsTotalCharge);
		recieptPaxDTO.setAdultsTotal(AdultsTotal);
		recieptPaxDTO.setChildsTotalFare(ChildsTotalFare);
		recieptPaxDTO.setChildsTotalCharge(ChildsTotalCharge);
		recieptPaxDTO.setChildsTotal(ChildsTotal);
		recieptPaxDTO.setInfantsTotalFare(InfantsTotalFare);
		recieptPaxDTO.setInfantsTotalCharge(InfantsTotalCharge);
		recieptPaxDTO.setInfantsTotal(InfantsTotal);

		return recieptPaxDTO;
	}

	private static String getAvailableBalance(Reservation reservation) throws ModuleException {
		BigDecimal avlConvertedBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		String selectedCurrency = "";
		StringBuilder strAvailBal = new StringBuilder();

		if (reservation.getLastCurrencyCode() != null && reservation.getLastCurrencyCode() != "") {
			selectedCurrency = reservation.getLastCurrencyCode();
		} else {
			selectedCurrency = AppSysParamsUtil.getBaseCurrency();
		}

		BigDecimal avlBalance = reservation.getTotalAvailableBalance();

		try {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
			BigDecimal exchangeRate = currExRate.getMultiplyingExchangeRate();

			if (currExRate != null && avlBalance.abs().doubleValue() > 0) {
				Currency currency = currExRate.getCurrency();
				avlConvertedBalance = AccelAeroRounderPolicy.convertAndRound(avlBalance, exchangeRate,
						currency.getBoundryValue(), currency.getBreakPoint());
			}

			strAvailBal.append(avlConvertedBalance.toString() + " " + selectedCurrency);
		} catch (ModuleException e) {
			log.warn("No exchange rate defined for currency " + selectedCurrency, e);
		}

		return strAvailBal.toString();
	}

	private static String getSpecifiedCurrencyCode(Agent ownerAgent, String baseCurrencyCode) {

		if (ownerAgent != null && !ownerAgent.getCurrencyCode().equals(baseCurrencyCode)) {
			return ownerAgent.getCurrencyCode();
		}

		return null;
	}

	private static String getTotalPaidAmountInPayCurrency(Collection<ReservationPax> passengers) throws ModuleException {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		Iterator<ReservationPax> itReservationPax = passengers.iterator();
		ReservationPax reservationPax;
		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}

		Map<Integer, Collection<ReservationTnx>> mapTnxs = ReservationBO.getPNRPaxPaymentsAndRefunds(pnrPaxIds, false, true);
		Map<String, BigDecimal> currencyWisePayAmounts = new HashMap<String, BigDecimal>();
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown;

		for (Collection<ReservationTnx> tnx : mapTnxs.values()) {
			for (ReservationTnx reservationTnx : tnx) {
				reservationPaxTnxBreakdown = reservationTnx.getReservationPaxTnxBreakdown();

				if (reservationPaxTnxBreakdown != null) {
					BigDecimal amount = currencyWisePayAmounts.get(reservationPaxTnxBreakdown.getPaymentCurrencyCode());

					if (amount != null) {
						amount = AccelAeroCalculator.add(amount, reservationPaxTnxBreakdown.getTotalPriceInPayCurrency());
						currencyWisePayAmounts.put(reservationPaxTnxBreakdown.getPaymentCurrencyCode(), amount);
					} else {
						currencyWisePayAmounts.put(reservationPaxTnxBreakdown.getPaymentCurrencyCode(),
								reservationPaxTnxBreakdown.getTotalPriceInPayCurrency());
					}

				} else {
					BigDecimal amount = currencyWisePayAmounts.get(baseCurrencyCode);

					if (amount != null) {
						amount = AccelAeroCalculator.add(amount, reservationTnx.getAmount());
						currencyWisePayAmounts.put(baseCurrencyCode, amount);
					} else {
						currencyWisePayAmounts.put(baseCurrencyCode, reservationTnx.getAmount());
					}
				}
			}
		}

		StringBuilder strAmounts = new StringBuilder();
		for (String currencyCode : currencyWisePayAmounts.keySet()) {
			BigDecimal amount = currencyWisePayAmounts.get(currencyCode).abs();

			if (amount.doubleValue() != 0d) {
				if (strAmounts.length() == 0) {
					strAmounts.append(amount.toString() + " " + currencyCode);
				} else {
					strAmounts.append(" + " + amount.toString() + " " + currencyCode);
				}
			}
		}

		return strAmounts.toString();
	}

	// private static Collection getSeatInformation(Reservation reservation) {
	//
	// Collection colSeatDetails = new ArrayList();
	// SeatPaxDTO paxSeat = null;
	// Collection<PaxSeatTO> colSeat = reservation.getSeats();
	// PaxSeatTO paxseatTo = null;
	// if (colSeat != null) {
	// Iterator<PaxSeatTO> seatIter = colSeat.iterator();
	// while (seatIter.hasNext()) {
	// paxseatTo = (PaxSeatTO) seatIter.next();
	// paxSeat = new SeatPaxDTO();
	// paxSeat.setSeatCode(paxseatTo.getSeatCode());
	// paxSeat.setAmount(paxseatTo.getChgDTO().getAmount());
	// ReservationSegmentDTO seg = getSegmentDTO(reservation.getSegmentsView(), paxseatTo.getPnrSegId());
	// // String strSegCode = getSegmentCode(reservation.getSegmentsView(), paxseatTo.getPnrSegId());
	// if (seg != null) {
	// String strSegCode = seg.getSegmentCode();
	// // Haider 1Mar09
	// if (AppSysParamsUtil.isHideStopOverEnabled() && strSegCode != null) {
	// String newSeg = "";
	// String[] segArr = strSegCode.split("/");
	// for (int i = 0; i < segArr.length; i++) {
	// if (segArr.length > 2 && i > 0 && i < (segArr.length - 1))
	// continue;
	// if (i > 0)
	// newSeg += "/";
	// newSeg += segArr[i];
	// }
	// strSegCode = newSeg;
	// }
	//
	// paxSeat.setSegment(strSegCode);
	// paxSeat.setPassenger(getPassengerName(reservation.getPassengers(), paxseatTo.getPnrPaxId()));
	// paxSeat.setFlightNumber(seg.getFlightNo());
	// paxSeat.setDepartureDate(seg.getDepartureStringDate("ddMMMyy HH:mm"));
	// colSeatDetails.add(paxSeat);
	// }
	// }
	//
	// }
	//
	// return colSeatDetails;
	// }

	/**
	 * Gets the Meal \ Seat \ Airport Services Details for Itinerary
	 * 
	 * @param reservation
	 * @return
	 */
	private static PaxSSRInfoDTO getAdditionalServiceInformation(Reservation reservation, boolean isAirportService)
			throws ModuleException {

		Collection<PaxSSRDTO> colSSRDetails = new ArrayList<PaxSSRDTO>();
		PaxSSRDTO paxssrDto = null;
		Collection<PaxSeatTO> colSeat = reservation.getSeats();
		Collection<PaxMealTO> colMeal = reservation.getMeals();
		Collection<ReservationPax> colPax = reservation.getPassengers();
		Collection<ReservationSegmentDTO> colSegs = reservation.getSegmentsView();
		PaxSSRInfoDTO paxInfoDto = new PaxSSRInfoDTO();

		for (ReservationPax resPax : colPax) {
			Integer paxId = resPax.getPnrPaxId();
			String strPax = "";
			if (resPax.getTitle() != null) {
				strPax = resPax.getTitle() + " " + resPax.getFirstName() + " " + resPax.getLastName();
			} else {
				strPax = resPax.getFirstName() + " " + resPax.getLastName();
			}
			for (ReservationSegmentDTO segDto : colSegs) {
				if (segDto.getStatus().equals(ReservationSegmentStatus.CANCEL)) {
					continue;
				}
				paxssrDto = new PaxSSRDTO();
				paxssrDto.setSeatCode("");
				paxssrDto.setMealCode("");

				if (!isAirportService) {
					for (PaxSeatTO paxSeat : colSeat) {
						if (paxId.intValue() == paxSeat.getPnrPaxId().intValue()
								&& segDto.getPnrSegId().intValue() == paxSeat.getPnrSegId().intValue()) {
							paxInfoDto.setHasSeats(true);
							paxssrDto.setPnrSegId(paxSeat.getPnrSegId());
							paxssrDto.setSeatCode(paxSeat.getSeatCode());
						}
					}

					for (PaxMealTO paxMeal : colMeal) {
						if (paxId.intValue() == paxMeal.getPnrPaxId().intValue()
								&& segDto.getPnrSegId().intValue() == paxMeal.getPnrSegId().intValue()) {
							paxInfoDto.setHasMeals(true);
							paxssrDto.setPnrSegId(paxMeal.getPnrSegId());
							paxssrDto.setMealCode(paxMeal.getMealCode());
						}
					}
				} else {
					// multi SSR info
					if (resPax.getPaxSSR() != null && !resPax.getPaxSSR().isEmpty()) {
						Collection<PaxSSRDTO> colSSrDTO = resPax.getPaxSSR();
						Collection<PaxSSRDetailDTO> colPaxSSRDetial = new ArrayList<PaxSSRDetailDTO>();
						// boolean validSSR =
						for (PaxSSRDTO passengerSSRDetail : colSSrDTO) {
							if ((passengerSSRDetail.getPnrSegId().compareTo(segDto.getPnrSegId()) == 0)
									&& (passengerSSRDetail.getSsrSubCatID().intValue() == 1 || passengerSSRDetail
											.getSsrSubCatID().intValue() == 2)
									&& ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(passengerSSRDetail.getStatus())) {
								PaxSSRDetailDTO paxSSRDetailDTO = new PaxSSRDetailDTO();

								paxSSRDetailDTO.setPnrPaxId(passengerSSRDetail.getPnrPaxId());
								paxSSRDetailDTO.setSsrId(passengerSSRDetail.getSsrId());
								paxSSRDetailDTO.setContextId(passengerSSRDetail.getContextId());
								paxSSRDetailDTO.setSsrText(passengerSSRDetail.getSsrText());
								paxSSRDetailDTO.setChargeAmount(passengerSSRDetail.getChargeAmount());
								paxSSRDetailDTO.setSsrCode(passengerSSRDetail.getSsrCode());
								paxSSRDetailDTO.setSsrDesc(passengerSSRDetail.getSsrDesc());
								paxssrDto.setPnrSegId(passengerSSRDetail.getPnrSegId());
								colPaxSSRDetial.add(paxSSRDetailDTO);

							}
						}
						paxssrDto.setColPaxSSRDetailDTO(colPaxSSRDetial);
						// paxssrDto.setColPaxSSRDetailDTO(resPax.getPaxSSR());
						if (!colPaxSSRDetial.isEmpty()) {
							paxInfoDto.setHasAirportService(true);
						}

					}
				}

				if (paxssrDto.getPnrSegId() != null
						&& (paxInfoDto.isHasMeals() || paxInfoDto.isHasSeats() || paxInfoDto.isHasAirportService())) {
					paxssrDto.setPassenger(strPax);

					String strSegCode = segDto.getSegmentCode();
					// Haider 1Mar09
					if (AppSysParamsUtil.isHideStopOverEnabled() && strSegCode != null) {
						String newSeg = "";
						String[] segArr = strSegCode.split("/");
						for (int i = 0; i < segArr.length; i++) {
							if (segArr.length > 2 && i > 0 && i < (segArr.length - 1))
								continue;
							if (i > 0)
								newSeg += "/";
							newSeg += segArr[i];
						}
						strSegCode = newSeg;
					}
					paxssrDto.setSegment(strSegCode);
					paxssrDto.setFlightNo(segDto.getFlightNo());
					paxssrDto.setDepartureDate(segDto.getDepartureStringDate("ddMMMyy HH:mm"));

					colSSRDetails.add(paxssrDto);
				}
			}

		}
		paxInfoDto.setSsrDto(colSSRDetails);
		return paxInfoDto;
	}

	/**
	 * method to get the segment for seta details
	 * 
	 * @param colSegs
	 * @param segId
	 * @return
	 */
	// private static String getSegmentCode(Collection<ReservationSegmentDTO> colSegs, Integer segId) {
	// String strSegCode = "";
	// ReservationSegmentDTO seg = null;
	// if (colSegs != null) {
	// Iterator<ReservationSegmentDTO> segiter = colSegs.iterator();
	// while (segiter.hasNext()) {
	// seg = (ReservationSegmentDTO) segiter.next();
	// if (segId.intValue() == seg.getPnrSegId().intValue()) {
	// return seg.getSegmentCode();
	// }
	// }
	// }
	//
	// return strSegCode;
	// }

	/**
	 * method to get the segment DTO
	 * 
	 * @param colSegs
	 * @param segId
	 * @return
	 */
	// private static ReservationSegmentDTO getSegmentDTO(Collection<ReservationSegmentDTO> colSegs, Integer segId) {
	// ReservationSegmentDTO seg = null;
	// if (colSegs != null) {
	// Iterator<ReservationSegmentDTO> segiter = colSegs.iterator();
	// while (segiter.hasNext()) {
	// seg = (ReservationSegmentDTO) segiter.next();
	// if (segId.intValue() == seg.getPnrSegId().intValue()) {
	// return seg;
	// }
	// }
	// }
	//
	// return null;
	// }

	/**
	 * Method to get the pax name for seat details
	 * 
	 * @param colPax
	 * @param paxId
	 * @return
	 */
	// private static String getPassengerName(Collection<ReservationPax> colPax, Integer paxId) {
	// String strPax = "";
	// ReservationPax pax = null;
	// if (colPax != null) {
	// Iterator<ReservationPax> paxIter = colPax.iterator();
	// while (paxIter.hasNext()) {
	// pax = (ReservationPax) paxIter.next();
	// if (paxId.intValue() == pax.getPnrPaxId().intValue()) {
	// if (pax.getTitle() != null) {
	// strPax = pax.getTitle() + " " + pax.getFirstName() + " " + pax.getLastName();
	// } else {
	// strPax = pax.getFirstName() + " " + pax.getLastName();
	// }
	// return strPax;
	// }
	// }
	// }
	// return strPax;
	// }

	/**
	 * Returns the external charges
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ExternalChgDTO> getExternalCharges(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap, CredentialsDTO credentialsDTO) throws ModuleException {
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = getExternalChargesMap(credentialsDTO);
		ExternalChgDTO handlingChargeDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		BigDecimal totalHandlingChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;

		for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (adultAndInfantMap == null || adultAndInfantMap.containsKey(reservationPax.getPnrPaxId())) {
				for (Iterator<ReservationPaxFare> itReservationPaxFare = reservationPax.getPnrPaxFares().iterator(); itReservationPaxFare
						.hasNext();) {
					reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

					for (Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator(); itReservationPaxOndCharge
							.hasNext();) {
						reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();

						if (reservationPaxOndCharge.getChargeRateId() != null
								&& handlingChargeDTO.isChargeRateExist(reservationPaxOndCharge.getChargeRateId())) {
							totalHandlingChargeAmount = AccelAeroCalculator.add(totalHandlingChargeAmount,
									reservationPaxOndCharge.getAmount());
						}
					}
				}
			}
		}

		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();

		if (totalHandlingChargeAmount.doubleValue() > 0) {
			handlingChargeDTO.setAmount(totalHandlingChargeAmount);
			colExternalChgDTO.add(handlingChargeDTO);
		}

		return colExternalChgDTO;
	}

	/**
	 * Load the external charges map based on the external charge type Currently on the handling charge is shown
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private static Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap(CredentialsDTO credentialsDTO)
			throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

		return ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MAKE_ONLY);
	}

	/**
	 * Encrypts a given string
	 * 
	 * @param msg
	 * @return
	 * @throws ModuleException
	 */
	private static String encrypt(String msg) throws ModuleException {
		return ReservationModuleUtils.getCryptoServiceBD().encrypt(msg);
	}

	/**
	 * Generates the barcode <img ... /img> URL
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private static String generateBarcode(String pnr, Collection<ReservationSegmentDTO> segmentsOrderdByDep)
			throws ModuleException {
		Map<String, String> barCodeConfMap = config.getBarCodeConfMap();
		String showBarcode = (String) barCodeConfMap.get("showBarcode");

		StringBuilder URL = new StringBuilder();

		// create URL
		if (showBarcode.equalsIgnoreCase("true")) {
			String carrierCode = AppSysParamsUtil.getCarrierCode();

			String barcodeType = (String) barCodeConfMap.get("barcodeType");
			String humanReadable = (String) barCodeConfMap.get("humanReadable");
			String format = (String) barCodeConfMap.get("format");
			String resolution = (String) barCodeConfMap.get("resolution");
			String height = (String) barCodeConfMap.get("height");
			String path = PlatformUtiltiies.nullHandler(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.ACCELAERO_IBE_URL))
					+ "/public";
			String showAirline = (String) barCodeConfMap.get("showAirline");
			String mw = (String) barCodeConfMap.get("widthFactor");
			String showSegments = (String) barCodeConfMap.get("showSegments");
			String showEticket = (String) barCodeConfMap.get("showEticket");
			String showPnr = (String) barCodeConfMap.get("showPnr");
			StringBuilder barcodeMsg = new StringBuilder();
			if (showAirline.equalsIgnoreCase("true")) {
				barcodeMsg.append(carrierCode);
			}
			if (showPnr.equalsIgnoreCase("true")) {
				barcodeMsg.append(pnr);
			} else if (showPnr.equalsIgnoreCase("false") && showEticket.equalsIgnoreCase("false")) {
				barcodeMsg.append(pnr);
			}
			if (showSegments.equalsIgnoreCase("true")) {
				StringBuilder strSeg = new StringBuilder();
				for (Iterator<ReservationSegmentDTO> iterator = segmentsOrderdByDep.iterator(); iterator.hasNext();) {
					ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) iterator.next();
					strSeg.append(reservationSegmentDTO.getSegmentCode());
				}
				barcodeMsg.append(strSeg.toString());
			}
			String enptBarCodeMsg = encrypt(barcodeMsg.toString());
			String humanReadablePhaseEnableOrNot = encrypt(humanReadable);

			try {
				enptBarCodeMsg = URLEncoder.encode(enptBarCodeMsg, "UTF-8");
				humanReadablePhaseEnableOrNot = URLEncoder.encode(humanReadablePhaseEnableOrNot, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				log.error("Barcode could not encode messege or parameter ");
				throw new ModuleException("airreservations.barcode.encodingNotSupported");
			}

			URL.append("<img src='").append(path).append("/genbc?type=").append(barcodeType).append("&msg=")
					.append(enptBarCodeMsg).append("&hrp=").append(humanReadablePhaseEnableOrNot).append("&fmt=").append(format)
					.append("&res=").append(resolution).append("&height=").append(height).append("&mw=").append(mw).append("'/>");

			log.debug("Barcode URL created");
		}

		return URL.toString();
	}

	/**
	 * Check validity of selected passengers
	 * 
	 * @param reservation
	 * @param colSelectedPnrPaxIds
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, Integer> checkValidityOfSelectedPassengers(Reservation reservation,
			Collection<Integer> colSelectedPnrPaxIds) throws ModuleException {

		Map<Integer, Integer> adultAndInfantMap = null;

		if (colSelectedPnrPaxIds != null) {
			if (colSelectedPnrPaxIds.size() == 0) {
				throw new ModuleException("airreservations.emailItinerary.paxNotLocated");
			} else {
				adultAndInfantMap = new HashMap<Integer, Integer>();
				ReservationPax reservationPax;
				for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax
						.hasNext();) {
					reservationPax = (ReservationPax) itReservationPax.next();

					if (colSelectedPnrPaxIds.contains(reservationPax.getPnrPaxId())) {
						// Parent
						if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
							adultAndInfantMap.put(reservationPax.getPnrPaxId(), reservationPax.getAccompaniedPaxId());
							// Adult
						} else if (ReservationApiUtils.isSingleAfterSave(reservationPax)) {
							adultAndInfantMap.put(reservationPax.getPnrPaxId(), null);
							// Child
						} else if (ReservationApiUtils.isChildType(reservationPax)) {
							adultAndInfantMap.put(reservationPax.getPnrPaxId(), null);
						}
					}
				}
			}
		}

		return adultAndInfantMap;
	}

	/**
	 * Return payment details
	 * 
	 * @param reservation
	 * @param lstNominalCodes
	 * @return Collection of PaymentDetailsDTO
	 */
	private static Collection<PaymentDetailDTO> getPaymentDetails(Reservation reservation, List<Integer> lstNominalCodes) {
		log.debug("Inside getPaymentDetails");

		// Capturing the adults and parents
		ReservationPax reservationPax;
		String fullName;
		Map<Integer, String> mapPaxIdAndName = new HashMap<Integer, String>();

		for (Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = (ReservationPax) itReservationPax.next();

			// If it's an adult
			if (ReservationApiUtils.isAdultType(reservationPax)) {
				fullName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), false);

				mapPaxIdAndName.put(reservationPax.getPnrPaxId(), fullName);
				// If it's a child
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				fullName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), true);

				mapPaxIdAndName.put(reservationPax.getPnrPaxId(), fullName);
			}
		}

		ReservationPaymentDAO reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		Map<Integer, Collection<PaymentDetailDTO>> mapPnrPaxIdsAndColPaymentDetailDTO = reservationPaymentDAO
				.getDetailPNRPaxPayments(mapPaxIdAndName.keySet(), lstNominalCodes);

		Iterator<Integer> itMapPaxIdAndName = mapPaxIdAndName.keySet().iterator();
		Integer pnrPaxId;
		String passengerName;
		Collection<PaymentDetailDTO> colPaymentDetailDTO;

		PaymentDetailDTO paymentDetailDTO;

		Collection<PaymentDetailDTO> colAllPaymentDetailDTO = new ArrayList<PaymentDetailDTO>();
		Collection<Integer> colTnxIds = new HashSet<Integer>();

		// Assembling the PaymentDetailDTO with basic information
		while (itMapPaxIdAndName.hasNext()) {
			pnrPaxId = (Integer) itMapPaxIdAndName.next();
			passengerName = (String) mapPaxIdAndName.get(pnrPaxId);
			colPaymentDetailDTO = (Collection<PaymentDetailDTO>) mapPnrPaxIdsAndColPaymentDetailDTO.get(pnrPaxId);

			if (colPaymentDetailDTO != null) {
				for (Iterator<PaymentDetailDTO> itColPaymentDetailDTO = colPaymentDetailDTO.iterator(); itColPaymentDetailDTO
						.hasNext();) {
					paymentDetailDTO = (PaymentDetailDTO) itColPaymentDetailDTO.next();
					paymentDetailDTO.setPassengerName(passengerName);

					colTnxIds.add(paymentDetailDTO.getTnxId());
					colAllPaymentDetailDTO.add(paymentDetailDTO);
				}
			}
		}

		if (colTnxIds.size() > 0) {
			// Assembling the PaymentDetailDTO with authorization ids
			Map<Integer, String> mapTnxIdAndAuthorizationId = reservationPaymentDAO.getAuthorizationIdsForCCPayments(colTnxIds);
			String authorizationId;

			for (Iterator<PaymentDetailDTO> itColPaymentDetailDTO = colAllPaymentDetailDTO.iterator(); itColPaymentDetailDTO
					.hasNext();) {
				paymentDetailDTO = (PaymentDetailDTO) itColPaymentDetailDTO.next();
				authorizationId = (String) mapTnxIdAndAuthorizationId.get(paymentDetailDTO.getTnxId());
				paymentDetailDTO.setAuthorizationId(BeanUtils.nullHandler(authorizationId));
			}
		}

		log.debug("Exit getPaymentDetails");
		return colAllPaymentDetailDTO;
	}

	/**
	 * Return charge details
	 * 
	 * @param reservation
	 * @return collection of ChargesDetailDTO
	 * @throws ModuleException
	 */
	private static Collection<ChargesDetailDTO> getChargeDetails(Reservation reservation) throws ModuleException {
		log.debug("Inside getChargeDetails");

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxOndCharge reservationPaxOndCharge;
		ReservationPaxFareSegment reservationPaxFareSegment;
		ChargesDetailDTO chargesDetailDTO;
		Collection<ChargesDetailDTO> colChargesDetailDTO = new ArrayList<ChargesDetailDTO>();
		Collection<Integer> colChargeRateIds = new HashSet<Integer>();
		Collection<String> colAirportCodes = new HashSet<String>();
		String ondCode;
		String passengerName;
		String bookingCode;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			passengerName = "";

			// If it's an adult
			if (ReservationApiUtils.isAdultType(reservationPax)) {
				passengerName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), false);
				// If it's a child
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				passengerName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), true);
				// If it's an infant
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				passengerName = ReservationApiUtils.getPassengerName(reservationPax.getPaxTypeDescription(),
						reservationPax.getTitle(), reservationPax.getFirstName(), reservationPax.getLastName(), true);
			}

			// Navigate through passengers
			for (Iterator<ReservationPaxFare> itReservationPaxFare = reservationPax.getPnrPaxFares().iterator(); itReservationPaxFare
					.hasNext();) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();
				ondCode = ReservationApiUtils.getOndCode(reservationPaxFare);
				colAirportCodes.addAll(Arrays.asList(ondCode.split("/")));

				// Pick Booking Codes
				Collection<String> colBookingCodes = new HashSet<String>();
				for (Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments()
						.iterator(); itReservationPaxFareSegment.hasNext();) {
					reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
					bookingCode = BeanUtils.nullHandler(reservationPaxFareSegment.getBookingCode());

					if (!bookingCode.equals("")) {
						colBookingCodes.add(bookingCode);
					}
				}

				// Capturing the ChargesDetailDTO
				for (Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator(); itReservationPaxOndCharge
						.hasNext();) {
					reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();
					chargesDetailDTO = new ChargesDetailDTO();
					chargesDetailDTO.setPassengerName(passengerName);
					chargesDetailDTO.setPnrPaxId(reservationPax.getPnrPaxId());
					chargesDetailDTO.setChargeDate(reservationPaxOndCharge.getLocalChargeDate());
					chargesDetailDTO.setChargeAmount(reservationPaxOndCharge.getAmount());
					chargesDetailDTO.setChargeGroupCode(reservationPaxOndCharge.getChargeGroupCode());
					chargesDetailDTO.setOndCode(ondCode);
					chargesDetailDTO.setBookingCodes(colBookingCodes);
					chargesDetailDTO.setChargeDate(reservationPaxOndCharge.getZuluChargeDate());

					if (reservationPaxOndCharge.getChargeRateId() != null) {
						colChargeRateIds.add(reservationPaxOndCharge.getChargeRateId());
						chargesDetailDTO.setChargeRateId(reservationPaxOndCharge.getChargeRateId());
					}

					colChargesDetailDTO.add(chargesDetailDTO);
				}
			}
		}

		AirportBD airportBD = ReservationModuleUtils.getAirportBD();
		Map<String, CachedAirportDTO> mapAirportCodes = airportBD.getCachedOwnAirportMap(colAirportCodes);

		FareBD fareBD = ReservationModuleUtils.getFareBD();
		ChargeBD chargeBD = ReservationModuleUtils.getChargeBD();
		Map<String, String> mapChargeGroups = chargeBD.getChargeGroups();
		FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(null, colChargeRateIds, null);
		String chgGrpCodeDescription;
		ChargeTO chargeTO;

		// Assign the Charge Descriptions and Segment Descriptions
		for (Iterator<ChargesDetailDTO> itColChargesDetailDTO = colChargesDetailDTO.iterator(); itColChargesDetailDTO.hasNext();) {
			chargesDetailDTO = (ChargesDetailDTO) itColChargesDetailDTO.next();

			// Setting the segment descriptions
			String composedSegment = "";
			String[] segments = chargesDetailDTO.getOndCode().split("/");

			for (int i = 0; i < segments.length; i++) {
				CachedAirportDTO airport = mapAirportCodes.get(segments[i]);
				if (i != 0) {
					composedSegment += " / ";
				}
				composedSegment += airport.getAirportName();
			}
			chargesDetailDTO.setOndDescription(composedSegment);

			// Setting the charge descriptions
			chgGrpCodeDescription = (String) mapChargeGroups.get(chargesDetailDTO.getChargeGroupCode());
			chargesDetailDTO.setChargeType(chgGrpCodeDescription);

			if (chargesDetailDTO.getChargeRateId() != null) {
				chargeTO = (ChargeTO) faresAndChargesTO.getChargeTOs().get(chargesDetailDTO.getChargeRateId());
				chargesDetailDTO.setChargeCode(chargeTO.getChargeCode());
				chargesDetailDTO.setChargeDescription(chargeTO.getChargeDescription());
			} else {
				chargesDetailDTO.setChargeCode(chargesDetailDTO.getChargeGroupCode());
				bookingCode = BeanUtils.nullHandler(chargesDetailDTO.getBookingCode());

				// If it's an adult or a child
				if (!bookingCode.equals("")) {
					chargesDetailDTO.setChargeDescription(chargesDetailDTO.getChargeType() + "/" + bookingCode);
					// If it's an infant
				} else {
					chargesDetailDTO.setChargeDescription(chargesDetailDTO.getChargeType());
				}
			}
		}

		log.debug("Exit getChargeDetails");
		return colChargesDetailDTO;
	}

	/**
	 * Assemble detail charges
	 * 
	 * @param colChargesDetailDTO
	 * @return
	 */
	private static Collection<ItineraryChargesDTO> assembleCharges(Collection<ChargesDetailDTO> colChargesDetailDTO) {
		log.debug("Inside assembleCharges");

		ChargesDetailDTO chargesDetailDTO;
		Collection<ChargesDetailDTO> ondSpecificCharges;
		BigDecimal chargeAmount;
		Map<String, Collection<ChargesDetailDTO>> ondCharges = new HashMap<String, Collection<ChargesDetailDTO>>();
		Map<String, BigDecimal> ondChargeAmounts = new HashMap<String, BigDecimal>();

		for (Iterator<ChargesDetailDTO> itColChargesDetailDTO = colChargesDetailDTO.iterator(); itColChargesDetailDTO.hasNext();) {
			chargesDetailDTO = (ChargesDetailDTO) itColChargesDetailDTO.next();

			if (ondCharges.containsKey(chargesDetailDTO.getOndDescription())) {
				ondSpecificCharges = (Collection<ChargesDetailDTO>) ondCharges.get(chargesDetailDTO.getOndDescription());
				chargeAmount = (BigDecimal) ondChargeAmounts.get(chargesDetailDTO.getOndDescription());

				ondSpecificCharges.add(chargesDetailDTO);
				ondChargeAmounts.put(chargesDetailDTO.getOndDescription(),
						AccelAeroCalculator.add(chargeAmount, chargesDetailDTO.getChargeAmount()));
			} else {
				ondSpecificCharges = new ArrayList<ChargesDetailDTO>();
				ondSpecificCharges.add(chargesDetailDTO);

				ondCharges.put(chargesDetailDTO.getOndDescription(), ondSpecificCharges);
				ondChargeAmounts.put(chargesDetailDTO.getOndDescription(), chargesDetailDTO.getChargeAmount());
			}
		}

		ItineraryChargesDTO itineraryChargesDTO;
		String ondDescription;
		Collection<ItineraryChargesDTO> colItineraryChargesDTO = new ArrayList<ItineraryChargesDTO>();

		for (Iterator<String> itOndInformation = ondCharges.keySet().iterator(); itOndInformation.hasNext();) {
			ondDescription = (String) itOndInformation.next();

			itineraryChargesDTO = new ItineraryChargesDTO();
			itineraryChargesDTO.setOndDescription(ondDescription);
			itineraryChargesDTO.setColChargesDetailDTO((Collection<ChargesDetailDTO>) ondCharges.get(ondDescription));
			itineraryChargesDTO.setOndChargeAmount((BigDecimal) ondChargeAmounts.get(ondDescription));

			colItineraryChargesDTO.add(itineraryChargesDTO);
		}

		log.debug("Exit assembleCharges");
		return colItineraryChargesDTO;
	}

	/**
	 * Assemble Payments
	 * 
	 * @param paymentDetails
	 * @return
	 */
	private static Map<Integer, Map<String, ItineraryPaymentsDTO>> assemblePayments(
			Collection<PaymentDetailDTO> colPaymentDetailDTO) {
		log.debug("Inside assemblePayments");

		PaymentDetailDTO paymentDetailDTO = null;
		ItineraryPaymentsDTO itineraryPaymentsDTO = null;

		Map<Integer, Map<String, ItineraryPaymentsDTO>> paymentsByPax = new LinkedHashMap<Integer, Map<String, ItineraryPaymentsDTO>>();
		Map<String, ItineraryPaymentsDTO> paymentsPerPaxByCur = null;
		String strBaseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		String payCurrencyCode = null;
		BigDecimal payCurrencyAmount = null;

		for (Iterator<PaymentDetailDTO> itColPaymentDetailDTO = colPaymentDetailDTO.iterator(); itColPaymentDetailDTO.hasNext();) {
			paymentDetailDTO = (PaymentDetailDTO) itColPaymentDetailDTO.next();

			if (paymentsByPax.containsKey(paymentDetailDTO.getPnrPaxId())) {
				paymentsPerPaxByCur = (Map<String, ItineraryPaymentsDTO>) paymentsByPax.get(paymentDetailDTO.getPnrPaxId());
			} else {
				paymentsPerPaxByCur = new LinkedHashMap<String, ItineraryPaymentsDTO>();
				paymentsByPax.put(paymentDetailDTO.getPnrPaxId(), paymentsPerPaxByCur);
			}

			if (paymentDetailDTO.getPayCurrencyCode() == null || "".equals(paymentDetailDTO.getPayCurrencyCode())) {
				// Assumption : When payCurrencyCode is not present, payment is assumed to be in base currency
				payCurrencyCode = strBaseCurrencyCode;
				payCurrencyAmount = paymentDetailDTO.getAmount();
			} else {
				payCurrencyCode = paymentDetailDTO.getPayCurrencyCode();
				payCurrencyAmount = paymentDetailDTO.getPayCurrencyAmount();
			}

			if (paymentsPerPaxByCur.containsKey(payCurrencyCode)) {
				itineraryPaymentsDTO = (ItineraryPaymentsDTO) paymentsPerPaxByCur.get(payCurrencyCode);
			} else {
				itineraryPaymentsDTO = new ItineraryPaymentsDTO();
				itineraryPaymentsDTO.setPaymentCurrencyCode(payCurrencyCode);
				paymentsPerPaxByCur.put(payCurrencyCode, itineraryPaymentsDTO);
			}

			itineraryPaymentsDTO.addPaymentDetailDTO(paymentDetailDTO);
			itineraryPaymentsDTO.setPaxPaymentAmount(AccelAeroCalculator.add(itineraryPaymentsDTO.getPaxPaymentAmount(),
					paymentDetailDTO.getAmount()));
			itineraryPaymentsDTO.setPaxPaymentAmountInPayCurrency(AccelAeroCalculator.add(
					itineraryPaymentsDTO.getPaxPaymentAmountInPayCurrency(), payCurrencyAmount));
		}

		log.debug("Exit assemblePayments");
		return paymentsByPax;
	}

	/**
	 * Return itinerary specific payment information
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @param recieptLayoutModesDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, Map<String, ItineraryPaymentsDTO>> getRecieptPaymentsDTOs(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap, RecieptLayoutModesDTO recieptLayoutModesDTO) throws ModuleException {
		return assemblePayments(filterPassengersFromPaymentsAndforPayment(
				getPaymentDetails(reservation, ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()), adultAndInfantMap,
				recieptLayoutModesDTO));
	}

	/**
	 * Returns itinerary specific payment information for a given nominal codes
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @return
	 * @throws ModuleException
	 */
	private static Map<Integer, Map<String, ItineraryPaymentsDTO>> getReceiptCreditCardPaymentsOnly(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap, RecieptLayoutModesDTO recieptLayoutModesDTO) throws ModuleException {
		return assemblePayments(filterPassengersFromPaymentsAndforPayment(
				getPaymentDetails(reservation, ReservationTnxNominalCode.getCreditCardNominalCodes()), adultAndInfantMap,
				recieptLayoutModesDTO));
	}

	/**
	 * Return itinerary specific charge information
	 * 
	 * @param reservation
	 * @param adultAndInfantMap
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ItineraryChargesDTO> getItineraryChargesDTOs(Reservation reservation,
			Map<Integer, Integer> adultAndInfantMap) throws ModuleException {
		return assembleCharges(filterPassengersFromCharges(getChargeDetails(reservation), adultAndInfantMap));
	}

	/**
	 * Filter passengers from charges
	 * 
	 * @param colChargesDetailDTO
	 * @param adultAndInfantMap
	 * @return
	 */
	private static Collection<ChargesDetailDTO> filterPassengersFromCharges(Collection<ChargesDetailDTO> colChargesDetailDTO,
			Map<Integer, Integer> adultAndInfantMap) {
		if (adultAndInfantMap != null) {
			Collection<Integer> colAllPassengerIds = new ArrayList<Integer>();
			Integer pnrPassengerId;
			Integer pnrInfantId;

			for (Iterator<Integer> itPnrPaxId = adultAndInfantMap.keySet().iterator(); itPnrPaxId.hasNext();) {
				pnrPassengerId = (Integer) itPnrPaxId.next();
				pnrInfantId = (Integer) adultAndInfantMap.get(pnrPassengerId);
				colAllPassengerIds.add(pnrPassengerId);

				if (pnrInfantId != null) {
					colAllPassengerIds.add(pnrInfantId);
				}
			}

			Collection<ChargesDetailDTO> colFilteredChargesDetailDTO = new ArrayList<ChargesDetailDTO>();
			ChargesDetailDTO chargesDetailDTO;

			for (Iterator<ChargesDetailDTO> itChargeDetailDTO = colChargesDetailDTO.iterator(); itChargeDetailDTO.hasNext();) {
				chargesDetailDTO = (ChargesDetailDTO) itChargeDetailDTO.next();

				// Adding only the passengers we needed
				if (colAllPassengerIds.contains(chargesDetailDTO.getPnrPaxId())) {
					colFilteredChargesDetailDTO.add(chargesDetailDTO);
				}
			}

			return colFilteredChargesDetailDTO;
		} else {
			return colChargesDetailDTO;
		}
	}

	/**
	 * Filter passengers from payments
	 * 
	 * @param colPaymentDetailDTO
	 * @param adultAndInfantMap
	 * @param recieptLayoutModesDTO
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<PaymentDetailDTO> filterPassengersFromPaymentsAndforPayment(
			Collection<PaymentDetailDTO> colPaymentDetailDTO, Map<Integer, Integer> adultAndInfantMap,
			RecieptLayoutModesDTO recieptLayoutModesDTO) throws ModuleException {

		Collection<PaymentDetailDTO> colFilteredPaymentDetailDTObyReceipt = new ArrayList<PaymentDetailDTO>();

		if (recieptLayoutModesDTO.getRecieptNumber() == null) {
			setRecieptNumber(colPaymentDetailDTO, recieptLayoutModesDTO);
		}

		/*
		 * Filter payments to get only selected payments
		 */
		if (recieptLayoutModesDTO.getRecieptNumber() != null && !recieptLayoutModesDTO.getRecieptNumber().trim().equals("")) {
			BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalPayCur = AccelAeroCalculator.getDefaultBigDecimalZero();
			String payCurrency = "";
			for (PaymentDetailDTO paymentDetailDTO : colPaymentDetailDTO) {
				if (paymentDetailDTO.getRecieptNumber().equalsIgnoreCase(recieptLayoutModesDTO.getRecieptNumber())) {
					total = AccelAeroCalculator.add(total, paymentDetailDTO.getAmount());
					totalPayCur = AccelAeroCalculator.add(totalPayCur, paymentDetailDTO.getPayCurrencyAmount());
					payCurrency = paymentDetailDTO.getPayCurrencyCode();
					colFilteredPaymentDetailDTObyReceipt.add(paymentDetailDTO);
				}
			}
			recieptLayoutModesDTO.setAmount(total);
			recieptLayoutModesDTO.setAmountPayCurr(totalPayCur);
			recieptLayoutModesDTO.setPayCurr(payCurrency);

		} else {
			throw new ModuleException("airreservations.recieptprint.noreceipt");
		}

		if (adultAndInfantMap != null) {
			Collection<PaymentDetailDTO> colFilteredPaymentDetailDTO = new ArrayList<PaymentDetailDTO>();
			PaymentDetailDTO paymentDetailDTO;

			for (Iterator<PaymentDetailDTO> itPaymentDetailDTO = colFilteredPaymentDetailDTObyReceipt.iterator(); itPaymentDetailDTO
					.hasNext();) {
				paymentDetailDTO = (PaymentDetailDTO) itPaymentDetailDTO.next();

				if (adultAndInfantMap.keySet().contains(paymentDetailDTO.getPnrPaxId())) {
					colFilteredPaymentDetailDTO.add(paymentDetailDTO);
				}
			}

			return colFilteredPaymentDetailDTO;
		} else {
			return colFilteredPaymentDetailDTObyReceipt;
		}
	}

	/**
	 * Method filter out the t_pnr_txn record using selected txn_id and set corresponding receipt number to
	 * RecieptLayoutModesDTO
	 * 
	 * @param colPaymentDetailDTO
	 * @param recieptLayoutModesDTO
	 * @throws ModuleException
	 */
	private static String setRecieptNumber(Collection<PaymentDetailDTO> colPaymentDetailDTO,
			RecieptLayoutModesDTO recieptLayoutModesDTO) throws ModuleException {
		String payId = recieptLayoutModesDTO.getPayId();
		String recieptNumber = null;
		if (payId != null) { // Old booking, need to filter payment
			for (PaymentDetailDTO paymentDetailDTO : colPaymentDetailDTO) {
				if (paymentDetailDTO.getTnxId().toString().equalsIgnoreCase(payId)) {
					recieptLayoutModesDTO.setRecieptNumber(paymentDetailDTO.getRecieptNumber());
					recieptLayoutModesDTO.setPaymentMode(ReservationTnxNominalCode.getDescription(paymentDetailDTO
							.getNominalCode()));
					recieptLayoutModesDTO.setPaymentDate(paymentDetailDTO.getPaymentDate());
					recieptLayoutModesDTO.setActualPaymentMethod(paymentDetailDTO.getPaymentMode());
					break; // First matching instance exit
				}
			}

		} else {
			for (PaymentDetailDTO paymentDetailDTO : colPaymentDetailDTO) {
				if (paymentDetailDTO.getRecieptNumber() != null) {
					recieptLayoutModesDTO.setRecieptNumber(paymentDetailDTO.getRecieptNumber());
					recieptLayoutModesDTO.setPaymentMode(ReservationTnxNominalCode.getDescription(paymentDetailDTO
							.getNominalCode()));
					recieptLayoutModesDTO.setPaymentDate(paymentDetailDTO.getPaymentDate());
					recieptLayoutModesDTO.setActualPaymentMethod(paymentDetailDTO.getPaymentMode());
					break; // First matching instance exit
				}

			}
		}
		return recieptNumber;

	}

	/**
	 * Compose the audit
	 * 
	 * @param pnr
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	// private static Collection<ReservationAudit> composeAudit(String pnr, CredentialsDTO credentialsDTO) throws
	// ModuleException {
	// ReservationAudit reservationAudit = new ReservationAudit();
	// reservationAudit.setModificationType(AuditTemplateEnum.EMAIL_ITINERARY.getCode());
	// ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);
	//
	// // Setting the ip address
	// if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
	// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.IP_ADDDRESS, credentialsDTO
	// .getTrackInfoDTO().getIpAddress());
	// }
	//
	// // Setting the origin carrier code
	// if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
	// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.ORIGIN_CARRIER, credentialsDTO
	// .getTrackInfoDTO().getCarrierCode());
	// }
	//
	// Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
	// colReservationAudit.add(reservationAudit);
	//
	// return colReservationAudit;
	// }

	/**
	 * Compose carrier Legend
	 * 
	 * @param reservationPaxDTO
	 * @return
	 * @throws ModuleException
	 */
	private static String composeCarrierDescLegend(ReservationPaxDTO reservationPaxDTO) throws ModuleException {

		String carrierLegend = "";

		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_OPERATING_CARRIER_LEGEND))) {
			Map<String, Collection<String>> carrierCodeAndFlightsMap = new HashMap<String, Collection<String>>();
			Collection<ReservationSegmentDTO> colReservationSegmentDTO = reservationPaxDTO.getSegments();

			if (colReservationSegmentDTO != null && colReservationSegmentDTO.size() > 0) {
				Iterator<ReservationSegmentDTO> itReservationSegmentDTO = colReservationSegmentDTO.iterator();
				ReservationSegmentDTO reservationSegmentDTO;
				Collection<String> colFlights;
				String carrierCode;
				String strFltNo;

				while (itReservationSegmentDTO.hasNext()) {
					reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
					strFltNo = reservationSegmentDTO.getFlightNo();

					carrierCode = AppSysParamsUtil.extractCarrierCode(strFltNo);

					// add description to exsisting carrier
					if (carrierCodeAndFlightsMap.containsKey(carrierCode)) {
						colFlights = (Collection<String>) carrierCodeAndFlightsMap.get(carrierCode);
						colFlights.add(strFltNo);

						// add description to new carrier
					} else {
						colFlights = new HashSet<String>();
						colFlights.add(strFltNo);

						carrierCodeAndFlightsMap.put(carrierCode, colFlights);
					}
				}
			}
			carrierLegend = ReservationApiUtils.formatCarrierLegend(carrierCodeAndFlightsMap);
		}

		return carrierLegend;
	}

	/**
	 * Formats the company address in the itinerary
	 * 
	 * @param carrierCode
	 * @return
	 * @throws ModuleException
	 */
	private static String strItineraryAddress(String carrierCode) throws ModuleException {
		if (AppSysParamsUtil.isHideAddressInItinarary()) {
			return "";
		} else {
			return ReservationApiUtils.getCompanyAddressInformation(carrierCode);
		}
	}
	
	private static String getPolicyCodeString(List<ReservationInsurance> insurances){
		String policyCode = "";
		Set<String> policycode =  new HashSet<String>();
		for (ReservationInsurance resinsurence : insurances) {
			if (resinsurence != null && resinsurence.getPolicyCode() != null){
				policycode.add(resinsurence.getPolicyCode());
			}			
		}
		int index = 0;
		for (String policyID : policycode) {
			if(index>0){
				policyCode +=" , ";
			}
			policyCode += policyID;
			index++;
		}
		
		
		return policyCode;
	}
	
}
