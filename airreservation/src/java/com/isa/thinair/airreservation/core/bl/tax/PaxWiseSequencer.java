package com.isa.thinair.airreservation.core.bl.tax;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;

public class PaxWiseSequencer implements Sequencer {

	private Sequencer tnxKeySequencer;
	private int minimumSequence = -1;

	public PaxWiseSequencer(Sequencer tnxKeySequencer) {
		this.tnxKeySequencer = tnxKeySequencer;
	}

	@Override
	public Long getKey(ReservationPaxOndCharge charge) {
		return captureMinimumSequence(tnxKeySequencer.getKey(charge));
	}

	@Override
	public Long getKey(ReservationTnx resTnx) {
		return captureMinimumSequence(tnxKeySequencer.getKey(resTnx));
	}

	private long captureMinimumSequence(long value) {
		if (minimumSequence == -1 || value < minimumSequence) {
			minimumSequence = (int) value;
		}
		return value;
	}

	@Override
	public int getMinmumSequence() {
		return minimumSequence;
	}

}
