package com.isa.thinair.airreservation.core.bl.airporttransfer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.AncilaryNotificationDTO.NotificationType;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

/**
 * 
 * @author rimaz, manoj
 * 
 */
public class AirportTransferBL {

	private static final String AIRLINE_ADMIN = "Airline Admin";
	
	private static final String AIRLINE_PASSENGER = "Airline Passenger";

	// ADD
	public static void sendAirportTransferAddNotifications(String pnr, Collection<Integer> notifyPnrSegIds,
			CredentialsDTO credentialsDTO) throws ModuleException {

		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			sendNotifications(NotificationType.ADD, pnr, notifyPnrSegIds, null, false, null);
		}

	}

	// CANCEL
	public static void sendAirportTransferCancelNotifications(Reservation reservation, Collection<Integer> pnrSegmentIds,
			Map<Integer, Collection<PaxAirportTransferTO>> cancelledSSRMap, CredentialsDTO credentialsDTO, String oldPnr)
			throws ModuleException {

		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			sendNotifications(NotificationType.CANCEL, reservation.getPnr(), pnrSegmentIds,
					getpaxSegAptIdsFromDto(cancelledSSRMap), true, oldPnr);
		}

	}

	// MODIFY
	public static void modifyAirportTransfer(Reservation reservation, AdjustCreditBO adjustCreditBO, String pnr,
			Collection<ReservationPaxSegAirportTransfer> passengerAPTsToAdd,
			Collection<ReservationPaxSegAirportTransfer> passengerAPTsToCancel) throws ModuleException {
		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			ReservationProxy.saveReservation(reservation);
			adjustCreditBO.callRevenueAccountingAfterReservationSave();
			if (passengerAPTsToAdd != null && !passengerAPTsToAdd.isEmpty()) {
				sendNotifications(NotificationType.ADD, pnr, null, getpaxSegAptIds(passengerAPTsToAdd), false, null);
			}
			if (passengerAPTsToCancel != null && !passengerAPTsToCancel.isEmpty()) {
				sendNotifications(NotificationType.CANCEL, pnr, null, getpaxSegAptIds(passengerAPTsToCancel), false, null);
			}
		}
	}

	private static void sendNotifications(NotificationType type, String pnr, Collection<Integer> pnrSegmentIds,
			Collection<Integer> paxSegAptIds, boolean loadCancelled, String oldPnr) throws ModuleException {

		Collection<AirportTransferNotificationDetailsDTO> aptResultDTOs = getAirportTranferRequests(pnr, pnrSegmentIds,
				paxSegAptIds);
		if (aptResultDTOs != null && !aptResultDTOs.isEmpty()) {
			updateSSRResultsForRemovePassegner(aptResultDTOs, oldPnr);
			for (AirportTransferNotificationDetailsDTO aptResultDTO : aptResultDTOs) {
				sendEmails(type, aptResultDTO);
			}
		}
	}

	private static void updateSSRResultsForRemovePassegner(Collection<AirportTransferNotificationDetailsDTO> aptResultDTOs, String oldPnr) {
		if (oldPnr != null && !oldPnr.isEmpty()) {
			for (AirportTransferNotificationDetailsDTO aptResultDTO : aptResultDTOs) {
				// in remove pax, cancellation notification should carry the old reserved pnr.
				aptResultDTO.setPnr(oldPnr);
			}
		}
	}

	private static Collection<AirportTransferNotificationDetailsDTO> getAirportTranferRequests(String pnr,
			Collection<Integer> pnrSegIds, Collection<Integer> paxSegAptIds) {
		Collection<AirportTransferNotificationDetailsDTO> aptResultDTOs = ReservationDAOUtils.DAOInstance.AIRPORT_TRANSFER_DAO
				.getAirportTransfesForNotification(pnr, pnrSegIds, paxSegAptIds);
		return aptResultDTOs;
	}

	/**
	 * Mail will be send to <br>
	 * 1) Airport level service provider and <br>
	 * 2) SSR sub-category level configured notifier email <br>
	 * Email status of service provider will be updated
	 */
	private static void sendEmails(NotificationType notification, AirportTransferNotificationDetailsDTO requestForAirport) {

		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		HashMap<String, Object> emailDataMap = (HashMap<String, Object>) getEMailDataMap(notification.toString(),
				requestForAirport);

		UserMessaging airportServiceProvider = new UserMessaging();
		airportServiceProvider.setFirstName(requestForAirport.getProviderName());
		airportServiceProvider.setLastName("");
		airportServiceProvider.setToAddres(requestForAirport.getProviderEmail());
		messages.add(airportServiceProvider);

		UserMessaging airlineAdmin = new UserMessaging();
		airlineAdmin.setFirstName(AIRLINE_ADMIN);
		airlineAdmin.setLastName("");
		airlineAdmin.setToAddres(ReservationModuleUtils.getAirReservationConfig().getAirportTransferAirlineEmail());
		messages.add(airlineAdmin);

		if(requestForAirport.getPaxEmail() != null && !requestForAirport.getPaxEmail().isEmpty()) {
			UserMessaging passenger = new UserMessaging();
			passenger.setFirstName(AIRLINE_PASSENGER);
			passenger.setLastName("");
			passenger.setToAddres(requestForAirport.getPaxEmail());
			messages.add(passenger);
		}

		Topic topic = new Topic();
		topic.setTopicParams(emailDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.AIRPORT_TRANSFER_EMAIL_TEMPLATE);
		topic.setLocale(null);
		topic.setAttachMessageBody(true);
		// topic.setObjectInfo(updateEmailStatusInfoDTOList);

		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		messagingServiceBD.sendMessage(messageProfile);

	}

	public static Map<String, Object> getEMailDataMap(String notifyType, AirportTransferNotificationDetailsDTO requestForAirport) {

		CommonTemplatingDTO commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(null);
		Map<String, Object> emailDataMap = new HashMap<String, Object>();
		emailDataMap.put("notifyType", notifyType);
		emailDataMap.put("carrierName", AppSysParamsUtil.getDefaultCarrierName());
		emailDataMap.put("anciResultDTO", requestForAirport);
		if (commonTemplatingDTO != null) {
			emailDataMap.put("commonTemplatingDTO", commonTemplatingDTO);
		}

		return emailDataMap;
	}

	private static Collection<Integer> getpaxSegAptIds(Collection<ReservationPaxSegAirportTransfer> paxAPTs) {
		Collection<Integer> paxSegAptIds = new ArrayList<Integer>();
		if (paxAPTs != null && !paxAPTs.isEmpty()) {
			for (ReservationPaxSegAirportTransfer paxSegApt : paxAPTs) {
				paxSegAptIds.add(paxSegApt.getPnrPaxSegAirportTransferId());
			}
		}
		return paxSegAptIds;
	}
	
	private static Collection<Integer> getpaxSegAptIdsFromDto(Map<Integer, Collection<PaxAirportTransferTO>> cancelledSSRMap) {
		Collection<Integer> paxSegAptIds = new ArrayList<Integer>();
		if (cancelledSSRMap != null && !cancelledSSRMap.isEmpty()) {
			for (Collection<PaxAirportTransferTO> paxAPTs : cancelledSSRMap.values()) {
				if (paxAPTs != null && !paxAPTs.isEmpty()) {
					for (PaxAirportTransferTO paxSegApt : paxAPTs) {
						paxSegAptIds.add(paxSegApt.getPnrPaxSegAptId());
					}
				}
			}
		}
		return paxSegAptIds;
	}
	
	public static Map<Integer, Collection<PaxAirportTransferTO>> getpaxSegAptForReservation(Reservation reservation) {
		Map<Integer, Collection<PaxAirportTransferTO>> cancelledAptMap = new HashMap<Integer, Collection<PaxAirportTransferTO>>();
		if (reservation.getAirportTransfers() != null && !reservation.getAirportTransfers().isEmpty()) {
			for (PaxAirportTransferTO paxAirportTransferTO : reservation.getAirportTransfers()) {
				if (cancelledAptMap.containsKey(paxAirportTransferTO.getPnrPaxId())) {
					cancelledAptMap.get(paxAirportTransferTO.getPnrPaxId()).add(paxAirportTransferTO);
				} else {
					Collection<PaxAirportTransferTO> cancelledApts = new ArrayList<PaxAirportTransferTO>();
					cancelledApts.add(paxAirportTransferTO);
					cancelledAptMap.put(paxAirportTransferTO.getPnrPaxId(), cancelledApts);
				}
			}
		}
		return cancelledAptMap;
	}

}
