package com.isa.thinair.airreservation.core.util;

import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;

/**
 * @author chanaka
 * 
 */
public class ReprotectedPassengerUtil {	

	public static ReprotectedPassenger toReprotectedPassenger(ReprotectedPaxDTO rePaxDto) {

		ReprotectedPassenger rePax = new ReprotectedPassenger();
		rePax.setPnrPaxId(rePaxDto.getPnrPaxId());
		rePax.setFromFlightSegId(rePaxDto.getFrmFltSegId());
		rePax.setToFlightSegId(rePaxDto.getToFltSegId());
		rePax.setPnrSegId(rePaxDto.getPnrSegmentId());		
		return rePax;
	}

}
