package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext;

import com.isa.thinair.airreservation.api.model.Reservation;

public class RuleExecuterDatacontext {

	Reservation exisitingReservation;

	Reservation updatedReservation;

	public Reservation getExisitingReservation() {
		return exisitingReservation;
	}

	public void setExisitingReservation(Reservation exisitingReservation) {
		this.exisitingReservation = exisitingReservation;
	}

	public Reservation getUpdatedReservation() {
		return updatedReservation;
	}

	public void setUpdatedReservation(Reservation updatedReservation) {
		this.updatedReservation = updatedReservation;
	}

}
