package com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.validator.base.BaseElementBuilderValidator;

public class PaxModifierElementPalCalValidator extends BaseElementBuilderValidator {

	@Override
	public boolean validate(String messageType) {
		boolean isValid = false;
		if (MessageTypes.CAL.toString().equals(messageType)) {
			isValid = true;
		}
		return isValid;
	}

	@Override
	public PassengerStoreTypes getOngoingStoreType(String messageType, PassengerStoreTypes paxStoreType) {
		if (MessageTypes.CAL.toString().equals(messageType)) {
			return paxStoreType;
		} else if (MessageTypes.PAL.toString().equals(messageType)) {
			return PassengerStoreTypes.ADDED;
		}
		return null;
	}
}
