package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReprotectedPassengerDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;

/**
 * ReprotectedPassengerDAOImpl is the business DAO hibernate implementation
 * 
 * @author Chanaka
 * @isa.module.dao-impl dao-name="ReprotectedPassengerDAO"
 */

public class ReprotectedPassengerDAOImpl extends PlatformBaseHibernateDaoSupport implements ReprotectedPassengerDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReprotectedPassengerDAOImpl.class);

	@Override
	public void saveAllReprotectedPassengers(Collection<ReprotectedPassenger> reprotectedPassengers) {
		hibernateSaveOrUpdateAll(reprotectedPassengers);
	}

	@Override
	public List<ReprotectedPassenger> getAllEntriesOfPaxList(List<Integer> pnrPaxIds) {
		StringBuilder hql = new StringBuilder();
		hql.append("FROM ReprotectedPassenger p ");
		hql.append("WHERE p.pnrPaxId in (:pnrPaxIds) ");
		Query query = getSession().createQuery(hql.toString());
		query.setParameterList("pnrPaxIds", pnrPaxIds);

		@SuppressWarnings("unchecked")
		List<ReprotectedPassenger> recordList = query.list();
		return recordList;
	}

	@Override
	public Page<ReprotectedPaxDTO> searchReprotectedPax(int start, int pageSize,
			ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) {

		int end = start + pageSize;
		String flightNumber = reprotectedPaxSearchrequest.getFlightNumber();
		String date = reprotectedPaxSearchrequest.getSqlDate();
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM ( ");
		sql.append(
				"SELECT ROWNUM rnum, p.pnr, r.reprotected_pax_id, r.pnr_pax_id, p.first_name, p.last_name, f.flight_number, f.departure_date, "
						+ "c.c_email, c.c_mobile_no, r.voucher_id, r.from_flt_seg_id, r.to_flt_seg_id, r.pnr_seg_id ");
		sql.append("FROM t_pnr_passenger p,  t_flight f,  t_reprotected_pax r,  t_flight_segment s, t_reservation_contact c ");
		sql.append(" WHERE f.flight_number = '" + flightNumber + "' ");

		if (!"".equals(date)) {
			sql.append(" AND f.departure_date BETWEEN TO_DATE('" + date + " 00:00:00', 'dd-mon-yyyy HH24:mi:ss')  AND TO_DATE ('"
					+ date + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss') ");
		}

		sql.append(
				"AND f.flight_id = s.flight_id AND s.flt_seg_id = r.from_flt_seg_id AND p.pnr_pax_id = r.pnr_pax_id AND c.pnr = p.pnr ");
		sql.append("AND p.status != 'OHD' ");
		sql.append(") WHERE rnum BETWEEN " + start + " AND " + end);
		sql.append(" ORDER BY reprotected_pax_id ");

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		int count = getReprotectedPaxCount(reprotectedPaxSearchrequest);
		@SuppressWarnings("unchecked")
		Collection<ReprotectedPaxDTO> rePaxList = (Collection<ReprotectedPaxDTO>) jt.query(sql.toString(),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<ReprotectedPaxDTO> paxList = new HashSet<ReprotectedPaxDTO>();

						if (rs != null) {
							while (rs.next()) {
								ReprotectedPaxDTO reprotectedPaxDTO = new ReprotectedPaxDTO();
								reprotectedPaxDTO.setFlightNumber(rs.getString("flight_number"));
								reprotectedPaxDTO.setFlightDate(rs.getDate("departure_date").toString());
								reprotectedPaxDTO.setPaxFirstName(rs.getString("first_name"));
								reprotectedPaxDTO.setPaxlastName(rs.getString("last_name"));
								reprotectedPaxDTO.setReprotectedPaxid(rs.getInt("reprotected_pax_id"));
								reprotectedPaxDTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
								reprotectedPaxDTO.setPaxEmail(rs.getString("c_email"));
								reprotectedPaxDTO.setMobileNumber(rs.getString("c_mobile_no"));
								reprotectedPaxDTO.setVoucherId(rs.getString("voucher_id"));
								reprotectedPaxDTO.setFrmFltSegId(rs.getInt("from_flt_seg_id"));
								reprotectedPaxDTO.setToFltSegId(rs.getInt("to_flt_seg_id"));
								reprotectedPaxDTO.setPnrSegmentId(rs.getInt("pnr_seg_id"));
								reprotectedPaxDTO.setPnr(rs.getString("pnr"));

								paxList.add(reprotectedPaxDTO);
							}
						}
						return paxList;
					}
				});

		List<ReprotectedPaxDTO> records = new ArrayList<ReprotectedPaxDTO>(rePaxList);

		return new Page<ReprotectedPaxDTO>(pageSize, start, start + pageSize, count, records);

	}

	@Override
	public int getReprotectedPaxCount(ReprotectedPaxSearchrequest reprotectedPaxSearchrequest) {

		int recordCount = 0;
		String flightNumber = reprotectedPaxSearchrequest.getFlightNumber();
		String date = reprotectedPaxSearchrequest.getSqlDate();
		StringBuilder sqlData = new StringBuilder();

		sqlData = new StringBuilder();
		sqlData.append("SELECT COUNT(*) AS RECORDCOUNT ");
		sqlData.append("FROM t_pnr_passenger p,  t_flight f,  t_reprotected_pax r,  t_flight_segment s ");
		sqlData.append("WHERE f.flight_number = '" + flightNumber + "' ");

		if (!"".equals(date)) {
			sqlData.append(" AND f.departure_date BETWEEN TO_DATE('" + date
					+ " 00:00:00', 'dd-mon-yyyy HH24:mi:ss')  AND TO_DATE ('" + date + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss') ");
		}

		sqlData.append("AND f.flight_id = s.flight_id AND s.flt_seg_id = r.from_flt_seg_id AND p.pnr_pax_id = r.pnr_pax_id");

		log.debug("############################################");
		log.debug(" SQL to excute count            : " + sqlData);
		log.debug("############################################");

		JdbcTemplate templateData = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		recordCount = (Integer) templateData.query(sqlData.toString(), new ResultSetExtractor() {

			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = 0;
				while (rs.next()) {
					count = (Integer) rs.getInt("RECORDCOUNT");
				}
				return count;
			}
		});

		return recordCount;

	}

}
