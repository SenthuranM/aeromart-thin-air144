/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.pnl.datastructure.masterinformation;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.api.dto.pnl.PNLMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerCollectionStructure;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PnlADlBaseDataStructure;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author udithad
 *
 */
public class PnlMasterDataStructure<K extends BasePassengerCollection, T extends BaseDataContext>
		extends PnlADlBaseDataStructure implements
		MessageDataStructure<PassengerCollection, PnlDataContext> {

	private Collection<DestinationFare> destinationFares;
	private PassengerCollection passengerCollection = null;
	private PnlDataContext context;
	private PassengerCollectionStructure passengerCollectionStructure;
	private List<DestinationFare> dummyDestinationFares = null;
	

	public PnlMasterDataStructure() {
		fareClassPaxCountMap = new HashMap<String, Integer>();
	}
	
	@Override
	public PassengerCollection getDataStructure(PnlDataContext context) throws ModuleException {
		this.context = context;
		populateFlightIdentityInformation(context);
		passengerCollectionStructure = new PassengerCollectionStructure();
		setSeatConfigurationElement(context);
		populateFareCabinRbdMap(context.getFlightId());
		destinationFares = getPnlRelatedDestinationInformation(
				context.getFlightId(), context.getDepartureAirportCode(),
				context.getFlightNumber());
		passengerCollection = passengerCollectionStructure
				.getPassengerCollection(destinationFares, cabinFareRbdMap);
		populateFareClassPassengerCountAndGroupCode(
				context.getDepartureAirportCode(), context.getFlightId());
		populatePnrWisePassengerCountsToPaxCollection();
		populateLastGroupCode();
		populateDummyDestinationFares();
		sortDestinationFareCollection(passengerCollection.getPnlAdlDestinationMap());
		return passengerCollection;
	}
	
	private void populateDummyDestinationFares() {
		for (String destinationAirportCode : destinationAirportCodes) {
			dummyDestinationFares = createDummyEntriesForNonUsedFareClasses(destinationAirportCode);
			if (dummyDestinationFares != null
					&& !dummyDestinationFares.isEmpty()) {
				if (passengerCollection.getPnlAdlDestinationMap().containsKey(
						destinationAirportCode)) {
					passengerCollection.getPnlAdlDestinationMap()
							.get(destinationAirportCode)
							.addAll(dummyDestinationFares);
				}
			}
		}
	}
	
	private void sortDestinationFareCollection(Map<String,List<DestinationFare>> destinationFares){
		for (Map.Entry<String, List<DestinationFare>> entry : destinationFares
				.entrySet()) {
			passengerCollectionStructure.sortDestinationFares(entry.getValue());
		}
	}
	
	private Collection<DestinationFare> getPnlRelatedDestinationInformation(
			Integer flightId, String departureAirportCode, String flightNumber) throws ModuleException {

		Collection<DestinationFare> destinationFares = null;
		Timestamp currentTimeStamp = new Timestamp(new Date().getTime());
		List<PassengerInformation> passengerInformation = auxilliaryDAO
				.getPNLPassengerDetails(flightId, departureAirportCode,
						currentTimeStamp, context.getCarrierCode());
		if(passengerInformation != null && !passengerInformation.isEmpty()){
			destinationFares = getDestinationFareCollectionBy(passengerInformation);
		}else{
			throw new ModuleException("reservationauxilliary.pnl.noreservations", "airreservations");
		}
		return destinationFares;

	}

	protected void populateFareClassPassengerCountAndGroupCode(
			String departureAirportCode, Integer flightId) {
		getnerateFareClassCountMap();
		lastGroupCode = getFirstTourIdCode(departureAirportCode,flightId);
	}

	@SuppressWarnings("unchecked")
	private void populatePnrWisePassengerCountsToPaxCollection() {
		passengerCollection.setGroupCodes(PnlAdlUtil.populateTourIds());
		passengerCollection.setPnrWisePassengerCount(pnrWisePassengerCount);
		passengerCollection
				.setPnrWisePassengerReductionCount(pnrWisePassengerReductionCount);
		passengerCollection.setPnrPaxIdvsSegmentIds(pnrPaxIdvsSegmentIds);
		passengerCollection.setPnrCollection(pnrCollection);
		passengerCollection.setFareClassWisePaxCount(fareClassWisePaxCount);
	}
	
	private void populateLastGroupCode(){
		passengerCollection.setLastGroupCode(lastGroupCode);
	}

	private void getnerateFareClassCountMap() {
		if (destinationFares != null) {
			for (DestinationFare destinationFare : destinationFares) {
				if (destinationFare.getAddPassengers() != null) {
					int paxCount = 0;
					for (Entry<String, List<PassengerInformation>> entry : destinationFare.getAddPassengers().entrySet()) {
						paxCount += entry.getValue().size();
					}
					fareClassPaxCountMap.put(destinationFare.getFareClass(), paxCount);
				}
			}
		}
	}

	private String getFirstTourIdCode(String departureAirportCode, Integer flightId) {
		int index = 0;

		Object[] pnlInformation = auxilliaryDAO.getPaxCountInPnlADL(flightId, departureAirportCode);
		String lastGroupCode = "";

		if (pnlInformation[1] != null) {
			lastGroupCode = (String) pnlInformation[1];
		} else {
			String[] tourIds = PnlAdlUtil.populateTourIds();
			lastGroupCode = getTrourIdByIndex(tourIds, index);
		}

		return lastGroupCode;
	}

	private String getTrourIdByIndex(String[] tourIds, int index) {
		String tourId = null;
		if (tourIds != null) {
			tourId = tourIds[index];
		}
		return tourId;
	}
	
	

}
