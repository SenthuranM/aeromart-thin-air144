/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.DestinationFareContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.MessageHeaderElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class HeaderElementBuilder extends BaseElementBuilder {

	private BaseRuleExecutor<RulesDataContext> headerRuleExecutor;
	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;
	private MessageHeaderElementContext heContext;
	private DestinationFareContext pEContext;
	
	public HeaderElementBuilder() {
		headerRuleExecutor 
		= new HeaderElementRuleExecutor();
	}
	
	@Override
	public void buildElement(ElementContext context) {
		RuleResponse response = null;
		RulesDataContext rulesContext = null;

		initContextData(context);
		rulesContext = createRlesDataContext();
		response = headerRuleExecutor.validateElementRules(rulesContext);
		
		ammendMessageDataAccordingTo(response);
		
	}

	private void initContextData(ElementContext context) {
		heContext = (MessageHeaderElementContext) context;
		pEContext = (DestinationFareContext)context;
		currentLine = heContext.getCurrentMessageLine();
		messageLine = heContext.getMessageString();
		currentElement = heContext.getHeaderElement();
	}

	private RulesDataContext createRlesDataContext() {
		RulesDataContext rulesContext = new RulesDataContext();
		rulesContext.setCurrentLine(heContext.getCurrentMessageLine()
				.toString());
		rulesContext.setAmmendingLine(heContext.getHeaderElement());
		return rulesContext;
	}

	private void ammendMessageDataAccordingTo(RuleResponse response) {
		if (response.isProceedNextElement()) {
			ammendToBaseLine(currentElement, currentLine, messageLine);
			executeConcatenationElementBuilder(heContext);
			executeNext();
		}
	}
	
	private void executeNext(){
		if(nextElementBuilder != null){
			nextElementBuilder.buildElement(pEContext);
		}
	}
	

}
