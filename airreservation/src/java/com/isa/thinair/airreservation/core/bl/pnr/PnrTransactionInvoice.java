/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;

/**
 * <b>Create transaction wise invoices on update PNR</b>
 * 
 * @author dumindag
 */
class PnrTransactionInvoice {

	private static Date formatDateInfo(Date lastModificatinTime) {
		if (lastModificatinTime == null) {
			String strTimeZone = "GMT";
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone(strTimeZone));
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal.getTime();
		} else {
			return lastModificatinTime;
		}
	}

	/**
	 * Create Invoices
	 * 
	 * @param strPNR
	 * @param agentCode
	 * @param colTnxIds
	 * @param lastModificatinTime
	 * @throws ModuleException
	 */
	protected static void
			createInvoices(String strPNR, String agentCode, Collection<Integer> colTnxIds, Date lastModificatinTime)
					throws ModuleException {
		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			Date formattedDate = formatDateInfo(lastModificatinTime);
			SearchInvoicesReceiptsDTO invoicesReceiptsDTO = new SearchInvoicesReceiptsDTO();
			invoicesReceiptsDTO.setPnr(strPNR);
			invoicesReceiptsDTO.setAgentCode(agentCode);
			// invoicesReceiptsDTO.setTnxId(tnxID);
			invoicesReceiptsDTO.setColTnxIds(colTnxIds);
			invoicesReceiptsDTO.setTxnDate(formattedDate);

			ReservationModuleUtils.getInvoicingBD().generateInvoicesRecipts(invoicesReceiptsDTO);
		}
	}
}
