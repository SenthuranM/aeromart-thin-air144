package com.isa.thinair.airreservation.core.bl.common;

import com.isa.thinair.airreservation.core.bl.tty.AddInfantMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.AddSegmentMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.CancelReservationMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.CancelSegmentMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ChangeCabinMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ChangeContactDetailsMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ChangePaxDetailsMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ChangeUserNoteMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.CreateReservationMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ExtendOnholdMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ModifySSRMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.ModifySegmentMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.NameChangeMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.NoTicketChangeMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.OnholdPayConfirmMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.OnholdReleaseMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.RemoveInfantMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.RemovePaxMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.SplitPaxMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.TicketingMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.TransferSegmentMessageCreator;
import com.isa.thinair.airreservation.core.bl.tty.TypeBReservationMessageCreator;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

public class TypeBReservationMessageCreatorFactory extends TypeBMessageCreatorFactory {

	public TypeBReservationMessageCreator createTypeBReservationMessageCreator(GDSInternalCodes.GDSNotifyAction gdsNotifyAction) {

		TypeBReservationMessageCreator typeBReservationMessageCreator = null;

		if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CREATE_ONHOLD) {
			typeBReservationMessageCreator = new CreateReservationMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.ONHOLD_RELEASE) {
			typeBReservationMessageCreator = new OnholdReleaseMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.ONHOLD_PAY_CONFIRM) {
			typeBReservationMessageCreator = new OnholdPayConfirmMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.NAME_CHANGE) {
			typeBReservationMessageCreator = new NameChangeMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CANCEL_RESERVATION) {
			typeBReservationMessageCreator = new CancelReservationMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.MODIFY_SEGMENT) {
			typeBReservationMessageCreator = new ModifySegmentMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CANCEL_SEGMENT) {
			typeBReservationMessageCreator = new CancelSegmentMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.ADD_SEGMENT) {
			typeBReservationMessageCreator = new AddSegmentMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.REMOVE_PAX) {
			typeBReservationMessageCreator = new RemovePaxMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.SPLIT_PAX) {
			typeBReservationMessageCreator = new SplitPaxMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.ADD_INFANT) {
			typeBReservationMessageCreator = new AddInfantMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.MODIFY_SSR) {
			typeBReservationMessageCreator = new ModifySSRMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CHANGE_PAX_DETAILS) {
			typeBReservationMessageCreator = new ChangePaxDetailsMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.EXTEND_ONHOLD) {
			typeBReservationMessageCreator = new ExtendOnholdMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CHANGE_CONTACT_DETAILS) {
			typeBReservationMessageCreator = new ChangeContactDetailsMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.REMOVE_PAX_INFANT) {
			typeBReservationMessageCreator = new RemovePaxMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.REMOVE_INFANT) {
			typeBReservationMessageCreator = new RemoveInfantMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CHANGE_USER_NOTE) {
			typeBReservationMessageCreator = new ChangeUserNoteMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.TRANSFER_SEGMENT) {
			typeBReservationMessageCreator = new TransferSegmentMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.CABIN_CHANGED) { // Not done---
			typeBReservationMessageCreator = new ChangeCabinMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.TICKETING) {
			typeBReservationMessageCreator = new TicketingMessageCreator();
		} else if (gdsNotifyAction == GDSInternalCodes.GDSNotifyAction.NO_TICKET_CHANGE) {
			typeBReservationMessageCreator = new NoTicketChangeMessageCreator();
		}

		return typeBReservationMessageCreator;
	}

}
