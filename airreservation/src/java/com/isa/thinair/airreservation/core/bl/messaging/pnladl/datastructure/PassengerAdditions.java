/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerElement;

/**
 * @author udithad
 *
 */
public abstract class PassengerAdditions<T> extends
		BaseAdditionalInformation<T> {
}
