package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Reconciles reservation payments with Bank payment for a given external payment transaction.
 * 
 * @author Nasly
 * @isa.module.command name="reconExtPayTransaction"
 */
public class ReconcileExternalPaymentTnx extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(ReconcileExternalPaymentTnx.class);

	private ReservationBD resBD = null;

	private ChargeBD chargeBD = null;

	private ReconcileExternalPaymentTnx() {
		resBD = ReservationModuleUtils.getReservationBD();
		chargeBD = ReservationModuleUtils.getChargeBD();
	}

	public ServiceResponce execute() throws ModuleException {
		String onAccAgentCode = (String) this.getParameter(CommandParamNames.ONACC_AGENTCODE_FOR_EXT_PAY);
		ExternalPaymentTnx tnxToRecon = (ExternalPaymentTnx) this.getParameter(CommandParamNames.EXTERNAL_PAY_TX_INFO);
		boolean isManualRecon = (Boolean) this.getParameter(CommandParamNames.TRIGGER_EXTERNAL_PAY_TX_MANUAL_RECON);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		log.info("Starting External payment transaction reconciliation " + "[pnr=" + tnxToRecon.getPnr() + ",balQKey="
				+ tnxToRecon.getBalanceQueryKey() + "]");
		if (log.isDebugEnabled())
			log.debug("Txn Recon - Requested Ext Txn Information ::" + tnxToRecon.getSummary().toString());

		Reservation res = null;
		ExternalPaymentTnx existingTnxToRecon = getExtPayTnxToReconcile(tnxToRecon.getPnr(), tnxToRecon.getBalanceQueryKey());

		if (log.isDebugEnabled())
			log.debug("Txn Recon - Existing Ext Txn Information ::" + existingTnxToRecon.getSummary().toString());

		// Successfully reconciled transactions are not allowed reconcile again
		if ((ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED.equals(existingTnxToRecon.getReconcilationStatus()) && (ReservationInternalConstants.ExtPayTxStatus.SUCCESS
				.equals(existingTnxToRecon.getStatus())
				|| ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS.equals(existingTnxToRecon.getStatus()) || ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS
				.equals(existingTnxToRecon.getStatus())))) {
			// already successfully reconciled
			log.warn("Txn Recon - Reconciliation requested for an already successfully reconciled external payment transaction "
					+ "[pnr=" + existingTnxToRecon.getPnr() + ",balQKey=" + existingTnxToRecon.getBalanceQueryKey() + "]");
		} else {
			// set common attributes
			existingTnxToRecon
					.setReconciliationSource(isManualRecon ? ReservationInternalConstants.ExtPayTxReconSourceType.MANUAL
							: ReservationInternalConstants.ExtPayTxReconSourceType.SCHEDULED_JOB);
			if (isManualRecon) {
				existingTnxToRecon.setManualReconBy(credentialsDTO.getUserId());
			}
			existingTnxToRecon.setReconciledTimestamp(new Date());

			if (existingTnxToRecon.getAmount().compareTo(tnxToRecon.getAmount()) != 0) {
				log.error("Txn Recon - External Payment Txn Reconciliation failed - amounts mismatch" + "[pnr="
						+ tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + ",amount="
						+ tnxToRecon.getAmount() + "]");
				throw new ModuleException("airreservations.reconExtPay.invalidData", AirreservationConstants.MODULE_NAME);
			}

			// Needs recording payment against the reservation
			if (ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS.equals(tnxToRecon.getExternalPayStatus())) {

				if (ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(existingTnxToRecon.getStatus())) {
					log.error("Txn Recon - Bank Excess External Payment Txn Reconciliation failed - invalid bank status"
							+ "[pnr=" + tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + ",bankStatus="
							+ tnxToRecon.getExternalPayStatus() + ",aaStatus=" + existingTnxToRecon.getStatus() + "]");
					throw new ModuleException("airreservations.reconExtPay.invalidBankStatus");
				}

				res = getReservation(tnxToRecon.getPnr());
				BigDecimal balanceAmountToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (Iterator<ReservationPax> it = res.getPassengers().iterator(); it.hasNext();) {
					ReservationPax pax = (ReservationPax) it.next();
					boolean status = (0 < pax.getTotalAvailableBalance().doubleValue());
					if (status) {
						balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay, pax.getTotalAvailableBalance());
					} else {
						balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
								AccelAeroCalculator.getDefaultBigDecimalZero());
					}
				}

				// Service Charge
				QuotedChargeDTO serviceChargeDTO = chargeBD.getCharge(AppSysParamsUtil.getEBIServiceChargeCode(),
						existingTnxToRecon.getInternalTnxStartTimestamp(), null, null, false);
				Object[] firstPaxPnrPaxFareId = null;
				if (serviceChargeDTO != null) {
					firstPaxPnrPaxFareId = ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_DAO.getFirstPaxPnrPaxFareId(res
							.getPnr());
					balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay, AccelAeroCalculator
							.parseBigDecimal(serviceChargeDTO
									.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)));
				}

				// Reservation already cancelled
				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(res.getStatus())) {
					log.warn("Txn Recon - Cannot reconcile bank excess external payment transaction, reservation already cancelled "
							+ "[pnr=" + tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + "]");

					existingTnxToRecon.setStatus(ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS);
					existingTnxToRecon.setExternalPayStatus(ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS);
					existingTnxToRecon.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED);
					existingTnxToRecon.setExternalPayId(tnxToRecon.getExternalPayId());
					existingTnxToRecon.setInternalTnxEndTimestamp(new Date());
					existingTnxToRecon.setExternalTnxEndTimestamp(tnxToRecon.getExternalTnxEndTimestamp());
					existingTnxToRecon.setRemarks((existingTnxToRecon.getRemarks() == null ? "" : existingTnxToRecon.getRemarks()
							+ "|")
							+ "Reconciliation failed, Reservation already cancelled");

					updateExistingExtPayTnx(existingTnxToRecon);

					// Reservation already paid
				} else if (balanceAmountToPay.doubleValue() == 0) {
					log.warn("Txn Recon - Reconciling bank excess external payment transaction, reservation already paid "
							+ "[pnr=" + tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + "]");

					existingTnxToRecon.setStatus(ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS);
					existingTnxToRecon.setExternalPayStatus(ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS);
					existingTnxToRecon.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED);
					existingTnxToRecon.setExternalPayId(tnxToRecon.getExternalPayId());
					existingTnxToRecon.setInternalTnxEndTimestamp(new Date());
					existingTnxToRecon.setExternalTnxEndTimestamp(tnxToRecon.getExternalTnxEndTimestamp());
					existingTnxToRecon.setRemarks((existingTnxToRecon.getRemarks() == null ? "" : existingTnxToRecon.getRemarks()
							+ "|")
							+ "Reconciliation failed, Reservation already paid");

					updateExistingExtPayTnx(existingTnxToRecon);

					// Reservation has balance to pay
				} else {
					boolean isForceConfirm = false;

					if (tnxToRecon.getAmount().doubleValue() < balanceAmountToPay.doubleValue()) {
						// payment is less than balance to pay
						isForceConfirm = true;
						log.warn("Txn Recon - Going to reconcile [pnr=" + res.getPnr() + ",balQueryKey="
								+ tnxToRecon.getBalanceQueryKey() + "] " + "with reservation being partially paid");
					} else if (balanceAmountToPay.doubleValue() < tnxToRecon.getAmount().doubleValue()) {
						log.warn("Txn Recon - Going to reconcile [pnr=" + res.getPnr() + ",balQueryKey="
								+ tnxToRecon.getBalanceQueryKey() + "] "
								+ "with first adult pax credited with additional payment");
					}

					BigDecimal amountConsumed = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					PassengerAssembler paxAssembler = new PassengerAssembler(null);
					int adultSeq = 1;
					for (Iterator<ReservationPax> it = res.getPassengers().iterator(); it.hasNext();) {
						ReservationPax pax = (ReservationPax) it.next();
						if (!ReservationInternalConstants.PassengerType.INFANT.equals(pax.getPaxType())) {
							if (amountConsumed.doubleValue() == tnxToRecon.getAmount().doubleValue()) {
								paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							} else {
								boolean status = (0 < pax.getTotalAvailableBalance().doubleValue());
								if (status) {
									paxPaymentAmount = pax.getTotalAvailableBalance();
								} else {
									paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
								}
							}

							if (firstPaxPnrPaxFareId != null && pax.getPnrPaxId().equals(firstPaxPnrPaxFareId[0])) {
								// Service charge
								if (serviceChargeDTO != null) {
									paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, AccelAeroCalculator
											.parseBigDecimal(serviceChargeDTO
													.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)));
								} else {
									paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
											AccelAeroCalculator.getDefaultBigDecimalZero());
								}
							}

							if (adultSeq == 1 && (balanceAmountToPay.doubleValue() < tnxToRecon.getAmount().doubleValue())) {
								// add additional payment to the first adult as credit
								paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
										AccelAeroCalculator.subtract(tnxToRecon.getAmount(), balanceAmountToPay));
							}
							if (AccelAeroCalculator.subtract(tnxToRecon.getAmount(), amountConsumed).doubleValue() < paxPaymentAmount
									.doubleValue()) {
								// partial payment for current pax
								// TODO - verify the integrity of this case
								log.warn("Txn Recon - Going to accept partial payment for pax in reconciliation [paxId="
										+ pax.getPnrPaxId() + ",balanceToPay=" + pax.getTotalAvailableBalance()
										+ ",paymentAvailable="
										+ AccelAeroCalculator.subtract(tnxToRecon.getAmount(), amountConsumed) + "]");
								paxPaymentAmount = AccelAeroCalculator.subtract(tnxToRecon.getAmount(), amountConsumed);
							}

							if (0 < paxPaymentAmount.doubleValue()) {
								amountConsumed = AccelAeroCalculator.add(amountConsumed, paxPaymentAmount);
								IPayment paxPayment = new PaymentAssembler();
								paxPayment.addAgentCreditPayment(onAccAgentCode, paxPaymentAmount, null, null, null, null, null, null, null);

								paxAssembler.addPassengerPayments(pax.getPnrPaxId(), paxPayment);
							}
							++adultSeq;
						}
					}
					log.info("Txn Recon - Updating reservation payment for external payment transaction in reconciling bank excess "
							+ "[pnr="
							+ tnxToRecon.getPnr()
							+ ",balQKey="
							+ tnxToRecon.getBalanceQueryKey()
							+ ",onAccAgent= "
							+ onAccAgentCode + ",totalPayment=" + amountConsumed + "]");

					existingTnxToRecon.setAmount(tnxToRecon.getAmount());
					existingTnxToRecon.setExternalTnxEndTimestamp(tnxToRecon.getExternalTnxEndTimestamp());
					existingTnxToRecon.setExternalPayStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);
					existingTnxToRecon.setExternalPayId(tnxToRecon.getExternalPayId());
					existingTnxToRecon.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
					existingTnxToRecon.setRemarks("Reconciled bank excess");

					paxAssembler.addExternalPaymentTnxInfo(existingTnxToRecon);

					if (serviceChargeDTO == null || firstPaxPnrPaxFareId == null) {
						resBD.updateReservationForPayment(res.getPnr(), paxAssembler, isForceConfirm, false, res.getVersion(),
								null, true, false, true, false, false, false, null, false, null);
					} else {
						// First adjustment for service charge, then do the payment in single transation.
						resBD.updateResForPaymentWithSeriveCharge(
								res.getPnr(),
								paxAssembler,
								isForceConfirm,
								false,
								(Integer) firstPaxPnrPaxFareId[1],
								serviceChargeDTO.getChargeRateId(),
								AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
										.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)),
								"Booking paid from channel "
										+ SalesChannelsUtil.getSalesChannelName(Integer.parseInt(existingTnxToRecon.getChannel())),
								res.getVersion(), null);
					}
				}

				// Needs withdrawing the payment from reservation
			} else if (ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS.equals(tnxToRecon.getExternalPayStatus())) {
				// TODO - implement pax refund.
				log.warn("TODO - Txn Recon - implement AA excess external payment transaction reconciliation for " + "[pnr="
						+ tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + "]");

				if (!ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(existingTnxToRecon.getStatus())) {
					log.error("Txn Recon - AA Excess External Payment Txn Reconciliation failed - invalid bank status" + "[pnr="
							+ tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + ",bankStatus="
							+ tnxToRecon.getExternalPayStatus() + ",aaStatus=" + existingTnxToRecon.getStatus() + "]");
					throw new ModuleException("airreservations.reconExtPay.invalidBankStatus");
				}

				// Success at both ends - no need of doing anything special
			} else if (ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(tnxToRecon.getExternalPayStatus())) {
				log.info("Txn Recon - Reconciling external payment transaction success at both ends " + "[pnr="
						+ tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + "]");
				if (!ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(existingTnxToRecon.getStatus())) {
					log.error("Txn Recon - Reconciling external payment transaction success at both ends failed" + "[pnr="
							+ tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + ",bankStatus="
							+ tnxToRecon.getExternalPayStatus() + ",aaStatus=" + existingTnxToRecon.getStatus() + "]");
					throw new ModuleException("airreservations.reconExtPay.invalidBankStatus");
				}

				existingTnxToRecon.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
				existingTnxToRecon.setRemarks((existingTnxToRecon.getRemarks() == null ? "" : existingTnxToRecon.getRemarks()
						+ "|")
						+ "Successfully reconciled");

				updateExistingExtPayTnx(existingTnxToRecon);

				// Only balance query present
			} else if (ReservationInternalConstants.ExtPayTxStatus.INITIATED.equals(tnxToRecon.getExternalPayStatus())
					|| ReservationInternalConstants.ExtPayTxStatus.FAILED.equals(tnxToRecon.getExternalPayStatus())
					|| ReservationInternalConstants.ExtPayTxStatus.UNKNOWN.equals(tnxToRecon.getExternalPayStatus())
					|| ReservationInternalConstants.ExtPayTxStatus.BANK_ROLLBACKED.equals(existingTnxToRecon.getStatus())) {
				log.info("Txn Recon - Reconciling external payment transaction failed at bank/rolled back at bank/not present in bank  "
						+ "[pnr=" + tnxToRecon.getPnr() + ",balQKey=" + tnxToRecon.getBalanceQueryKey() + "]");
				if (!(ReservationInternalConstants.ExtPayTxStatus.INITIATED.equals(existingTnxToRecon.getStatus())
						|| ReservationInternalConstants.ExtPayTxStatus.FAILED.equals(existingTnxToRecon.getStatus())
						|| ReservationInternalConstants.ExtPayTxStatus.UNKNOWN.equals(existingTnxToRecon.getStatus()) || ReservationInternalConstants.ExtPayTxStatus.ABORTED
						.equals(existingTnxToRecon.getStatus()))) {
					log.error("Txn Recon - Reconciling external payment transaction failed at bank/rolled back at bank/not present in bank is failed"
							+ "[pnr="
							+ tnxToRecon.getPnr()
							+ ",balQKey="
							+ tnxToRecon.getBalanceQueryKey()
							+ ",bankStatus="
							+ tnxToRecon.getExternalPayStatus() + ",aaStatus=" + existingTnxToRecon.getStatus() + "]");
					throw new ModuleException("airreservations.reconExtPay.invalidBankStatus");
				}

				existingTnxToRecon.setStatus(ReservationInternalConstants.ExtPayTxStatus.ABORTED);
				existingTnxToRecon.setExternalPayStatus(tnxToRecon.getExternalPayStatus());
				existingTnxToRecon.setReconcilationStatus(ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED);
				existingTnxToRecon.setRemarks((existingTnxToRecon.getRemarks() == null ? "" : existingTnxToRecon.getRemarks()
						+ "|")
						+ "Successfully reconciled");

				updateExistingExtPayTnx(existingTnxToRecon);

				// Unexpected transaction in recon request
			} else {
				log.error("Txn Recon - Unexpected transaction in external payment reconciliation " + "[pnr="
						+ tnxToRecon.getPnr() + ", balQueryKey=" + tnxToRecon.getBalanceQueryKey() + "]");
				throw new ModuleException("airreservations.reconExtPay.invalidBankStatus");
			}

			log.info("Txn Recon - External payment transaction reconciliation complete for" + "[pnr=" + tnxToRecon.getPnr()
					+ ",balQKey=" + tnxToRecon.getBalanceQueryKey() + "]");
		}

		return new DefaultServiceResponse(true);
	}

	/**
	 * Load the reservation with minimum required information.
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	private Reservation getReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadPaxAvaBalance(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	/**
	 * Retrieves external pay transaction for given pnr and balance query key.
	 * 
	 * @param pnr
	 * @param balQueryKey
	 * @return
	 * @throws ModuleException
	 */
	private ExternalPaymentTnx getExtPayTnxToReconcile(String pnr, String balQueryKey) throws ModuleException {
		ExternalPaymentTnx extPayTnxToRecon = null;
		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setPnr(pnr);
		criteriaDTO.setBalanceQueryKey(balQueryKey);

		Map<String,PNRExtTransactionsTO> pnrTnxMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getExtPayTransactions(criteriaDTO);

		PNRExtTransactionsTO pnrPayTnxs = null;
		if (pnrTnxMap != null && pnrTnxMap.containsKey(pnr)) {
			pnrPayTnxs = (PNRExtTransactionsTO) pnrTnxMap.get(pnr);
			if (pnrPayTnxs.getExtPayTransactions() == null || pnrPayTnxs.getExtPayTransactions().size() == 0) {
				log.error("Requested external payment transaction not found for reconciliation " + "[pnr=" + pnr + ",balQKey="
						+ balQueryKey + "]");

				throw new ModuleException("airreservations.recordExtPay.initRecNotFound");
			}
		} else {
			log.error("Txn Recon - Requested external payment transaction not found for reconciliation " + "[pnr=" + pnr
					+ ",balQKey=" + balQueryKey + "]");

			throw new ModuleException("airreservations.reconExtPay.initRecNotFound");
		}
		extPayTnxToRecon = (ExternalPaymentTnx) pnrPayTnxs.getExtPayTransactions().iterator().next();

		return extPayTnxToRecon;
	}

	private void updateExistingExtPayTnx(ExternalPaymentTnx externalPaymentTnx) {
		ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveOrUpdateExternalPayTxInfo(externalPaymentTnx);
	}
}
