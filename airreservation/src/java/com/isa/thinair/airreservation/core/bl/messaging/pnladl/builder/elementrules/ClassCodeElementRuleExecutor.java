/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules;

import java.util.ArrayList;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.AvailableSpaceRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.ClassCodeElementRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class ClassCodeElementRuleExecutor extends
   BaseRuleExecutor<RulesDataContext> {

	public ClassCodeElementRuleExecutor() {
		if(rulesList == null){
			rulesList = new ArrayList<BaseRule>();
		}
		rulesList.add(new AvailableSpaceRule());
		rulesList.add(new ClassCodeElementRule());
	}
	
	@Override
	public RuleResponse validateElementRules(RulesDataContext context) {
		boolean isValied = true;
		response = new RuleResponse();
		
		for(BaseRule<RulesDataContext> rule:rulesList){
			isValied = rule.validateRule(context);
			if(!isValied){
				break;
			}
		}
		
		if(isValied){
			response.setProceedNextElement(isValied);
		}
		return response;
	}

}
