package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ChargeRateDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.InsuranceSegExtChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutoCheckinExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class AncillaryExtraFeeCalculator extends BaseExtraFeeCalculator implements IExtraFeeCalculator {

	private Collection<OndFareDTO> colOndFareDTOs;
	private Map<String, String> requoteSegmentMap;
	private Collection<Integer> flownPnrSegIds;
	private Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap;
	private Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges;

	private Map<Integer, BigDecimal> paxWiseAddedAnciTotal = new HashMap<Integer, BigDecimal>();
	private Map<Integer, BigDecimal> paxWiseReprotectedAnciTotal = new HashMap<Integer, BigDecimal>();

	private Set<Integer> newFlightSegIds = new HashSet<Integer>();

	public AncillaryExtraFeeCalculator(Reservation reservation, EXTERNAL_CHARGES externalCharge,
			Collection<OndFareDTO> colOndFareDTOs, Map<String, String> requoteSegmentMap,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap,
			Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges, boolean isCancelOndOperation,
			boolean isAddSegmentOperation, Collection<Integer> flownPnrSegIds) {
		this.reservation = reservation;
		this.externalCharge = externalCharge;
		this.colOndFareDTOs = colOndFareDTOs;
		this.requoteSegmentMap = requoteSegmentMap;
		this.paxExtChgMap = paxExtChgMap;
		this.reprotectedExternalCharges = reprotectedExternalCharges;
		this.isCancelOndOperation = isCancelOndOperation;
		this.isAddSegmentOperation = isAddSegmentOperation;
		this.flownPnrSegIds = flownPnrSegIds;
	}

	@Override
	public void calculate() throws ModuleException {

		processSelectedAncillaries();
		if (isExtraFeeApplicable(colOndFareDTOs, requoteSegmentMap)) {
			Map<Integer, BigDecimal> taxablePaxWiseAnciTotal = null;

			boolean isOriginalReservationTaxed = isOriginalReservationTaxed();

			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
				processReprotectedAncillaries(true);
				taxablePaxWiseAnciTotal = mergeAncillaryCharges();

			} else if (isAddSegmentOperation) {

				if (isTaxApplicableFlightSegNew && !isOriginalReservationTaxed) {
					processReprotectedAncillaries(true);
					taxablePaxWiseAnciTotal = mergeAncillaryCharges();

				} else {
					taxablePaxWiseAnciTotal = new HashMap<Integer, BigDecimal>(paxWiseAddedAnciTotal);
				}

			} else {

				// Handle for Modify Segment operation
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(reservation.getPnr());
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSeatingInfo(true);
				pnrModesDTO.setLoadMealInfo(true);
				pnrModesDTO.setLoadBaggageInfo(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadAutoCheckinInfo(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				reservation = ReservationProxy.getReservation(pnrModesDTO);

				if (isCancelOndOperation && !isTaxCalculatiningToExistingSegment() && !isOriginalReservationTaxed) {
					// If cancel segment operation, check original reservation is taxable. If not, apply tax for all
					// re-protecting ancillaries
					processReprotectedAncillaries(true);
					taxablePaxWiseAnciTotal = new HashMap<Integer, BigDecimal>(paxWiseReprotectedAnciTotal);

				} else if (!isCancelOndOperation) {

					if (!isOriginalReservationTaxed) {
						processReprotectedAncillaries(true);
						taxablePaxWiseAnciTotal = mergeAncillaryCharges();

					} else {

						processReprotectedAncillaries(false);
						taxablePaxWiseAnciTotal = new HashMap<Integer, BigDecimal>();
						Map<Integer, BigDecimal> effectivePaxWiseNewAnciTotal = mergeAncillaryCharges();
						Map<Integer, BigDecimal> paxWiseExistingAnciTotal = getPaxWiseAnciWiseTotal(reservation);

						for (Entry<Integer, BigDecimal> newEntry : effectivePaxWiseNewAnciTotal.entrySet()) {
							Integer pnrPaxId = newEntry.getKey();
							BigDecimal newAnciTotal = newEntry.getValue();
							if (newAnciTotal != null) {
								if (paxWiseExistingAnciTotal.containsKey(pnrPaxId)) {
									BigDecimal existingAnciTotal = paxWiseExistingAnciTotal.get(pnrPaxId);

									if (existingAnciTotal != null
											&& AccelAeroCalculator.isGreaterThan(newAnciTotal, existingAnciTotal)) {
										taxablePaxWiseAnciTotal.put(pnrPaxId,
												AccelAeroCalculator.subtract(newAnciTotal, existingAnciTotal));
									}

								} else {
									taxablePaxWiseAnciTotal.put(pnrPaxId, newEntry.getValue());
								}
							}

						}
					}

				}

			}

			if (taxablePaxWiseAnciTotal != null && !taxablePaxWiseAnciTotal.isEmpty()) {
				for (Entry<Integer, BigDecimal> paxEntry : taxablePaxWiseAnciTotal.entrySet()) {
					Integer pnrPaxId = paxEntry.getKey();
					BigDecimal paxAnciTotal = paxEntry.getValue();

					if (AccelAeroCalculator.isGreaterThan(paxAnciTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
						BigDecimal taxAmount = AccelAeroCalculator.multiplyDefaultScale(paxAnciTotal, getRatio());
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) getExternalChgDTO();
						serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) serviceTaxExtChgDTO.clone();
						serviceTaxExtChgDTO.setAmount(taxAmount);
						serviceTaxExtChgDTO.setFlightRefNumber(taxApplicableFlightRefNumber);
						addPaxFee(pnrPaxId, serviceTaxExtChgDTO);
					}
				}

			}
		}

	}

	private Map<Integer, BigDecimal> mergeAncillaryCharges() {
		Map<Integer, BigDecimal> mergeAnciMap = new HashMap<Integer, BigDecimal>(paxWiseReprotectedAnciTotal);
		if (!paxWiseAddedAnciTotal.isEmpty()) {
			for (Entry<Integer, BigDecimal> entry : paxWiseAddedAnciTotal.entrySet()) {
				Integer paxId = entry.getKey();
				BigDecimal addedAnciTotal = entry.getValue();
				if (!mergeAnciMap.containsKey(paxId)) {
					mergeAnciMap.put(paxId, addedAnciTotal);
				} else {
					mergeAnciMap.put(paxId, AccelAeroCalculator.add(mergeAnciMap.get(paxId), addedAnciTotal));
				}
			}
		}
		return mergeAnciMap;
	}

	private ReservationSegmentDTO getNonModifyingFirstConfirmedSegment(List<ReservationSegmentDTO> colResSegmentDTO) {

		Collection<String> modifyingPnrSegIds = requoteSegmentMap.values();
		for (ReservationSegmentDTO reservationSegmentDTO : colResSegmentDTO) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
				if (modifyingPnrSegIds == null || modifyingPnrSegIds.isEmpty()) {
					return reservationSegmentDTO;
				} else {
					Set<Integer> targetFltSegIds = getTargetFlightSegmentIds(reservationSegmentDTO.getPnrSegId());
					if (targetFltSegIds.size() == 1 && targetFltSegIds.contains(reservationSegmentDTO.getFlightSegId())) {
						return reservationSegmentDTO;
					}
				}
			}
		}

		return null;
	}

	private Set<Integer> getTargetFlightSegmentIds(int pnrSegId) {
		Set<Integer> targetFltSegIds = new HashSet<Integer>();

		for (Entry<String, String> requoteEntry : requoteSegmentMap.entrySet()) {
			if (requoteEntry.getValue() != null) {
				Integer targetFltSegId = Integer.parseInt(requoteEntry.getKey());
				Integer tmpPnrSegId;
				if (requoteEntry.getValue().indexOf("#") != -1) {
					String arr[] = requoteEntry.getValue().split("#");
					tmpPnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]);
				} else {
					tmpPnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(requoteEntry.getValue());
				}

				if (pnrSegId == tmpPnrSegId) {
					targetFltSegIds.add(targetFltSegId);
				}
			}
		}
		return targetFltSegIds;
	}

	private void processSelectedAncillaries() {
		if (paxExtChgMap != null && !paxExtChgMap.isEmpty()) {
			Map<Integer, Integer> paxSequencePaxIdMap = getPaxSeqPaxIdMap();
			for (Entry<Integer, List<LCCClientExternalChgDTO>> extChgEntry : paxExtChgMap.entrySet()) {
				Integer paxSeq = extChgEntry.getKey();
				Integer pnrPaxId = paxSequencePaxIdMap.get(paxSeq);

				if (!paxWiseAddedAnciTotal.containsKey(pnrPaxId)) {
					paxWiseAddedAnciTotal.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
				}

				BigDecimal currentTotal = paxWiseAddedAnciTotal.get(pnrPaxId);

				List<LCCClientExternalChgDTO> paxAnciList = extChgEntry.getValue();
				if (paxAnciList != null && !paxAnciList.isEmpty()) {
					for (LCCClientExternalChgDTO lccClientExternalChgDTO : paxAnciList) {

						if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.JN_ANCI
								|| lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.JN_OTHER
								|| lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.CREDIT_CARD
								|| lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.HANDLING_CHARGE) {
							continue;
						}

						Integer fltSegId = FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(lccClientExternalChgDTO.getFlightRefNumber());
						newFlightSegIds.add(fltSegId);

						currentTotal = AccelAeroCalculator.add(currentTotal, lccClientExternalChgDTO.getAmount());
					}

					paxWiseAddedAnciTotal.put(pnrPaxId, currentTotal);
				}

			}
		}
	}

	private void processReprotectedAncillaries(boolean includeFlexi) {
		if (reprotectedExternalCharges != null && !reprotectedExternalCharges.isEmpty()) {

			for (Entry<Integer, Collection<ExternalChgDTO>> entry : reprotectedExternalCharges.entrySet()) {
				Integer pnrPaxId = entry.getKey();
				Collection<ExternalChgDTO> externalChgDTOs = entry.getValue();

				if (!paxWiseReprotectedAnciTotal.containsKey(pnrPaxId)) {
					paxWiseReprotectedAnciTotal.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
				}

				BigDecimal currentTotal = paxWiseReprotectedAnciTotal.get(pnrPaxId);

				if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
					for (ExternalChgDTO externalChgDTO : externalChgDTOs) {
						Integer fltSegId = null;
						BigDecimal anciCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (externalChgDTO instanceof SMExternalChgDTO) {
							SMExternalChgDTO smExternalChgDTO = (SMExternalChgDTO) externalChgDTO;
							fltSegId = smExternalChgDTO.getFlightSegId();
							anciCharge = smExternalChgDTO.getAmount();
						} else if (externalChgDTO instanceof MealExternalChgDTO) {
							MealExternalChgDTO mealExternalChgDTO = (MealExternalChgDTO) externalChgDTO;
							fltSegId = mealExternalChgDTO.getFlightSegId();
							anciCharge = mealExternalChgDTO.getAmount();
						} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
							BaggageExternalChgDTO baggageExternalChgDTO = (BaggageExternalChgDTO) externalChgDTO;
							fltSegId = baggageExternalChgDTO.getFlightSegId();
							anciCharge = baggageExternalChgDTO.getAmount();
						} else if (externalChgDTO instanceof AutoCheckinExternalChgDTO) {
							AutoCheckinExternalChgDTO autoCheckinExternalChgDTO = (AutoCheckinExternalChgDTO) externalChgDTO;
							fltSegId = autoCheckinExternalChgDTO.getFlightSegId();
							anciCharge = autoCheckinExternalChgDTO.getAmount();
						} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
							SSRExternalChargeDTO ssrExternalChargeDTO = (SSRExternalChargeDTO) externalChgDTO;
							fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrExternalChargeDTO.getFlightRPH());
							anciCharge = ssrExternalChargeDTO.getAmount();
						} else if (externalChgDTO instanceof AirportTransferExternalChgDTO) {
							AirportTransferExternalChgDTO aptExternalChargeDTO = (AirportTransferExternalChgDTO) externalChgDTO;
							fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(aptExternalChargeDTO.getFlightRPH());
							anciCharge = aptExternalChargeDTO.getAmount();
						} else if (externalChgDTO instanceof InsuranceSegExtChgDTO) {
							InsuranceSegExtChgDTO ins = (InsuranceSegExtChgDTO) externalChgDTO;
							fltSegId = ins.getFlightSegId();
							anciCharge = ins.getAmount();
						} else if (externalChgDTO instanceof FlexiExternalChgDTO) {
							FlexiExternalChgDTO flexi = (FlexiExternalChgDTO) externalChgDTO;
							fltSegId = flexi.getFlightSegId();
							if (includeFlexi) {
								anciCharge = flexi.getAmount();
							}
							if (!isNewFlexiSelection(pnrPaxId, flexi)) {
								if (!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
									// reset flexi charge after tax calculation
									flexi.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
								}
							}
						}

						if (fltSegId == null || (newFlightSegIds != null && newFlightSegIds.contains(fltSegId))) {
							continue;
						}

						currentTotal = AccelAeroCalculator.add(currentTotal, anciCharge);
					}

					paxWiseReprotectedAnciTotal.put(pnrPaxId, currentTotal);
				}
			}
		}
	}

	private Map<Integer, BigDecimal> getPaxWiseAnciWiseTotal(Reservation reservation) throws ModuleException {
		Map<Integer, BigDecimal> paxWiseTotal = null;
		List<Integer> colModifiedPnrSeg = new ArrayList<Integer>();

		for (ReservationSegmentDTO resSeg : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())
					&& (flownPnrSegIds == null || flownPnrSegIds.isEmpty() || !flownPnrSegIds.contains(resSeg.getPnrSegId()))) {
				colModifiedPnrSeg.add(resSeg.getPnrSegId());
			}
		}
		paxWiseTotal = new HashMap<Integer, BigDecimal>();

		boolean hasInsurance = false;
		boolean showTravelInsurance = AppSysParamsUtil.isShowTravelInsurance();
		if ((reservation.getReservationInsurance() != null) && !reservation.getReservationInsurance().isEmpty()) {
			for (ReservationInsurance insurances : reservation.getReservationInsurance()) {
				if (!ReservationInternalConstants.INSURANCE_STATES.OH.toString().equals(insurances.getState())) {
					hasInsurance = true;
				}
			}
		}

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, ChargeRateOperationType.MODIFY_ONLY);

		List<Integer> insChargeRateIds = new ArrayList<Integer>();
		ExternalChgDTO insCharge = null;
		if (!hasInsurance && showTravelInsurance) {
			insCharge = extExternalChgDTOMap.get(EXTERNAL_CHARGES.INSURANCE);
			for (ChargeRateDTO chgRate : insCharge.getChargeRates()) {
				insChargeRateIds.add(chgRate.getChgRateId());
			}
			insChargeRateIds.add(insCharge.getChgRateId());
		}

		for (ReservationPax pax : reservation.getPassengers()) {
			for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
				for (ReservationPaxFareSegment paxSegFare : paxFare.getPaxFareSegments()) {

					if (!colModifiedPnrSeg.contains(paxSegFare.getPnrSegId())) {
						continue;
					}

					for (ReservationPaxSegmentSSR ssr : paxSegFare.getReservationPaxSegmentSSRs()) {
						addToAnciTotal(paxWiseTotal, pax.getPnrPaxId(), ssr.getChargeAmount());
					}

					if (!hasInsurance) {
						for (ReservationPaxOndCharge charge : paxSegFare.getReservationPaxFare().getCharges()) {
							if (insChargeRateIds.contains(charge.getChargeRateId())) {
								addToAnciTotal(paxWiseTotal, pax.getPnrPaxId(), charge.getEffectiveAmount());
							}
						}
					}

				}

			}

		}

		for (PaxSeatTO seat : reservation.getSeats()) {
			if (!colModifiedPnrSeg.contains(seat.getPnrSegId())) {
				continue;
			}

			addToAnciTotal(paxWiseTotal, seat.getPnrPaxId(), seat.getChgDTO().getAmount());

		}

		for (PaxMealTO meal : reservation.getMeals()) {
			if (!colModifiedPnrSeg.contains(meal.getPnrSegId())) {
				continue;
			}
			addToAnciTotal(paxWiseTotal, meal.getPnrPaxId(), meal.getChgDTO().getAmount());
		}

		for (PaxBaggageTO baggage : reservation.getBaggages()) {
			if (!colModifiedPnrSeg.contains(baggage.getPnrSegId())) {
				continue;
			}
			addToAnciTotal(paxWiseTotal, baggage.getPnrPaxId(), baggage.getChgDTO().getAmount());
		}

		for (PaxAutomaticCheckinTO autoCheckin : reservation.getAutoCheckins()) {
			if (!colModifiedPnrSeg.contains(autoCheckin.getPnrSegId())) {
				continue;
			}
			addToAnciTotal(paxWiseTotal, autoCheckin.getPnrPaxId(), autoCheckin.getChgDTO().getAmount());
		}

		return paxWiseTotal;
	}

	private void addToAnciTotal(Map<Integer, BigDecimal> paxWiseTotal, Integer pnrPaxId, BigDecimal chgAmount) {
		if (!paxWiseTotal.containsKey(pnrPaxId)) {
			paxWiseTotal.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		BigDecimal currTotalCharge = paxWiseTotal.get(pnrPaxId);
		paxWiseTotal.put(pnrPaxId, AccelAeroCalculator.add(currTotalCharge, chgAmount));
	}

	private List<Integer> getModifyingPnrSegList() {
		List<Integer> modifyingPnrSegList = null;
		Collection<String> colModifiedPnrSeg = requoteSegmentMap.values();

		if (colModifiedPnrSeg != null && !colModifiedPnrSeg.isEmpty()) {
			modifyingPnrSegList = new ArrayList<Integer>();
			for (String strPnrSegId : colModifiedPnrSeg) {
				if (strPnrSegId != null && !"".equals(strPnrSegId) && !"null".equals(strPnrSegId)) {
					if (strPnrSegId.indexOf("#") != -1) {
						String arr[] = strPnrSegId.split("#");
						modifyingPnrSegList.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
					} else {
						modifyingPnrSegList.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(strPnrSegId));
					}
				}
			}
		}

		return modifyingPnrSegList;
	}

	private boolean isNewFlexiSelection(int pnrPaxId, FlexiExternalChgDTO flexi) {
		Map<Integer, Integer> paxIdPaxSeqMap = getPaxIdPaxSeqMap();
		Integer paxSeq = paxIdPaxSeqMap.get(pnrPaxId);
		if (paxExtChgMap != null && !paxExtChgMap.isEmpty() && paxSeq != null && paxExtChgMap.containsKey(paxSeq)) {
			List<LCCClientExternalChgDTO> lccClientExternalChgDTOs = paxExtChgMap.get(paxSeq);
			if (lccClientExternalChgDTOs != null && !lccClientExternalChgDTOs.isEmpty()) {
				for (LCCClientExternalChgDTO lccClientExternalChgDTO : lccClientExternalChgDTOs) {
					Integer fltSegId = FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(lccClientExternalChgDTO.getFlightRefNumber());
					if (lccClientExternalChgDTO.getCode() != null
							&& lccClientExternalChgDTO.getCode().equals(flexi.getChargeCode())
							&& flexi.getFlightSegId().equals(fltSegId)) {
						return true;
					}
				}
			}
		}

		return false;
	}
}
