/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.commons.api.dto.Page;

/**
 * PNL/ADL Timing DAO
 * 
 * @author Byorn de Silva
 * @since 1.0
 */
public interface PnlAdlTimingDAO {

	/**
	 * For Scheduled Service
	 * 
	 * @param defaultPnldepartureGap
	 * @return
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightsForPNLScheduling(int defaultPnldepartureGap, Date date);

	/**
	 * For Scheduled Service
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param flightDepZulu
	 * @return
	 */
	public boolean hasPNLADLTiming(String flightNumber, String airportCode, Date flightDepZulu);

	/**
	 * For Scheduled Service
	 * 
	 * @param airportCode
	 * @param flightDepZulu
	 * @return
	 */
	public boolean hasPNLADLTimingForAllFlightsInAirport(String airportCode, Date flightDepZulu);

	/**
	 * For Scheduled Service
	 * 
	 * @param transMissionDetailsDTO
	 * @return
	 */
	public PNLTransMissionDetailsDTO getPnlAdlTiming(PNLTransMissionDetailsDTO transMissionDetailsDTO);

	/**
	 * 
	 * @param transMissionDetailsDTO
	 * @return
	 */
	public PNLTransMissionDetailsDTO getPnlAdlAirportTiming(PNLTransMissionDetailsDTO transMissionDetailsDTO);

	/**
	 * For Front End
	 * 
	 * @param timing
	 */
	public void saveTiming(PnlAdlTiming timing);

	/**
	 * For Front End
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param startRec
	 * @param numOfRecs
	 * @return
	 */
	public Page getAllTimings(String flightNumber, String airportCode, int startRec, int numOfRecs);

	/**
	 * For Front End
	 * 
	 * @param airportCode
	 * @param flightNumber
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public boolean isOverlappingPNLADLTiming(String airportCode, String flightNumber, Date fromDate, Date toDate);

	public boolean isOverlappingPNLADLTimingEditMode(String airportCode, String flightNumber, Date fromDate, Date toDate,
			Integer id);

	/**
	 * For Front End
	 * 
	 * @param airportCode
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public boolean hasDuplicateGlobalPNLADLTiming(String airportCode, Date fromDate, Date toDate);

	public boolean hasDuplicateGlobalPNLADLTimingEditMode(String airportCode, Date fromDate, Date toDate, Integer id);

	/**
	 * 
	 * @return
	 */
	public Collection<String[]> getAirportTimings(Date date);

	/**
	 * 
	 * @return
	 */
	public boolean hasAirportTiming(Date date);

	/**
	 * 
	 * @param date
	 * @return
	 */
	public boolean hasFlightTimings(Date date);

	/**
	 * 
	 * @return
	 */
	public Collection<String[]> getFlightTimings(Date date);

	/**
	 * 
	 * @param date
	 * @param defaultPnlGap
	 * @param overidingPnldepartureGap
	 * @param airportCode
	 * @return
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightsForPNLSchedulingForAirport(Date date, int defaultPnlGap, int overidingPnldepartureGap,
			String airportCode);

	/**
	 * 
	 * @param date
	 * @param defaultPnldepartureGap
	 * @param overridingPnlDepGap
	 * @param flightNumber
	 * @return
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightsForPNLScheduling(Date date, int defaultPnldepartureGap, int overridingPnlDepGap,
			String flightNumber);

}
