package com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base.TotalByDestinationElementBuilderTemplate;

public class TotalByDestinationElementBuilderTemplatePal extends TotalByDestinationElementBuilderTemplate {

	@Override
	public String buildElementTemplate(PassengerElementsContext context) {

		StringBuilder elementTemplate = new StringBuilder();
		elementTemplate.append(hyphen());
		elementTemplate.append(context.getDestinationFare().getDestinationAirportCode());
		elementTemplate.append(space());
		// elementTemplate.append(context.getDestinationFare().getNumberOfPreviousPassengers());
		elementTemplate.append(context.getDestinationFare().getFareClass());
		if (context.getDestinationFare().getNumberOfPreviousPassengers() == 0) {
			elementTemplate.append(space());
			elementTemplate.append(nil());
		}

		return elementTemplate.toString();

	}

}
