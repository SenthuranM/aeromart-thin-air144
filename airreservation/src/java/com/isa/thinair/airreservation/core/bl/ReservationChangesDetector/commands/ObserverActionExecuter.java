package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.commands;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.factory.ResModifyIdentificationObserverFactory;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.observers.base.BaseResModifyObserver;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @isa.module.command name="observerActionExecuter"
 * 
 */

public class ObserverActionExecuter extends DefaultBaseCommand {

	private ResModifyDtectorDataContext dataContext;
	List<BaseResModifyObserver<ResModifyDtectorDataContext>> observerList = new ArrayList<>();

	@Override
	public ServiceResponce execute() throws ModuleException {
		resolveCommandParameters();
		createObservers();
		notifyObservers();
		return createResponseObject();
	}

	private void notifyObservers() throws ModuleException {
		
		for (BaseResModifyObserver<ResModifyDtectorDataContext> observer : observerList) {
			observer.notifyObserver(dataContext);
		}
	}

	private void createObservers() {
		if (dataContext.getObserverWiseRulesAndActionMap() != null) {
			for (String observerRef : dataContext.getObserverWiseRulesAndActionMap().keySet()) {
				observerList.add(ResModifyIdentificationObserverFactory.getResModifyIdentificationObserver(observerRef));
			}
		}
	}

	private void resolveCommandParameters() {
		dataContext = (ResModifyDtectorDataContext) this.getParameter(CommandParamNames.RES_MODIFY_DETECT_DATA_CONTEXT);
	}

	private ServiceResponce createResponseObject() {
		ServiceResponce response = new DefaultServiceResponse();
		((DefaultServiceResponse) response).setSuccess(true);
		return response;
	}
}
