package com.isa.thinair.airreservation.core.bl.messaging.pnladl.templatebuilder.base;

import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.PNLADLMessageConstants;

public abstract class BaseTemplateBuilder<T> {

	public abstract String buildElementTemplate(T t);

	protected String hyphen() {
		return PNLADLMessageConstants.HYPHEN;
	}
	
	protected  String  nil() {
		return PNLADLMessageConstants.NIL;
	}
	
	protected String space() {
		return PNLADLMessageConstants.SP;
	}
}
