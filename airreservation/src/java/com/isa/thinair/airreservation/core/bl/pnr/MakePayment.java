/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.core.util.PaymentUtil;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.LMSPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.PaymentGatewayBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for making payments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="makePayment"
 */
public class MakePayment extends DefaultBaseCommand {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(MakePayment.class);

	/** Holds the travel agent delegate */
	private final TravelAgentFinanceBD travelAgentFinanceBD;

	private final PaymentBrokerBD paymentBrokerBD;

	/**
	 * Construct MakePayment
	 * 
	 */
	public MakePayment() {
		travelAgentFinanceBD = ReservationModuleUtils.getTravelAgentFinanceBD();
		paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();
	}

	/**
	 * Execute method of the MakePayment command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");
		String fatoRef=new String();
		this.getParameter(CommandParamNames.TEMPORY_PAYMENT_IDS);
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Collection<PaymentInfo> colPaymentInfo = (Collection<PaymentInfo>) this.getParameter(CommandParamNames.PAYMENT_INFO);
		Collection<OndFareDTO> ondFareDTOs = (Collection<OndFareDTO>) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		Boolean triggerBlockSeats = (Boolean) this.getParameter(CommandParamNames.TRIGGER_BLOCK_SEATS);
		Collection<TempSegBcAlloc> blockIds = (Collection<TempSegBcAlloc>) this.getParameter(CommandParamNames.BLOCK_KEY_IDS);
		Collection<Integer> colTnxIds = (Collection<Integer>) this.getParameter(CommandParamNames.TEMPORY_PAYMENT_IDS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Boolean enableCCFraudCheck = (Boolean) this.getParameter(CommandParamNames.CC_FRAUD_ENABLE);
		// TODO we need to remove the reservation dependecy from make payment as make payment call call as a individual
		// command in dry/interline capture payments. In that flow no reservation is available in the context
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		TrackInfoDTO trackInfo = (TrackInfoDTO) this.getParameter(CommandParamNames.RESERVATIN_TRACINFO);
		String reservationType = (String) this.getParameter(CommandParamNames.RESERVATION_TYPE);
		Integer connectingPnrSegment = (Integer) this.getParameter(CommandParamNames.CONNECTING_FLT_SEG_ID);
		Boolean isCapturePayment = (Boolean) this.getParameter(CommandParamNames.IS_CAPTURE_PAYMENT);
		Boolean isModifyChgOperation = (Boolean) this.getParameter(CommandParamNames.IS_MODIFY_CHG_OPERATION);

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		Boolean triggerExternalPayTxUpdate = (Boolean) this.getParameter(CommandParamNames.TRIGGER_EXTERNAL_PAY_TX_UPDATE);
		ExternalPaymentTnx externalPaymentInfo = (ExternalPaymentTnx) this.getParameter(CommandParamNames.EXTERNAL_PAY_TX_INFO);
		Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) this
				.getParameter(CommandParamNames.PAX_CREDIT_PAYMENTS);
		if (paxCreditPayments == null) {
			paxCreditPayments = new HashMap<Integer, Collection<TnxCreditPayment>>();
		}

		// Checking params
		this.validateParams(pnr, reservation, colPaymentInfo, credentialsDTO, triggerBlockSeats, ondFareDTOs, blockIds,
				colTnxIds, isModifyChgOperation);

		Collection<CardPaymentInfo> colCardPaymentInfo = new ArrayList<CardPaymentInfo>();
		Collection<OfflinePaymentInfo> colOfflinePaymentInfo = new ArrayList<OfflinePaymentInfo>();
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		Integer agentSaleTxId = null;
		Integer paymentGatewayID = null;
		List<CardDetailConfigDTO> cardDetailConfigData = new ArrayList<CardDetailConfigDTO>();

		if (triggerBlockSeats.booleanValue()) {
			blockIds = ReservationBO.blockSeats(ondFareDTOs, null);
		}

		Collection<Integer> colAgentSaleTxId = new ArrayList<Integer>();
		String agentCode = null;
		boolean hasOnAccPayments = false;

		if (isCapturePayment) {
			while (itColPaymentInfo.hasNext()) {
				paymentInfo = itColPaymentInfo.next();
				if (paymentInfo.getPayCarrier().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					// Card Payment
					if (paymentInfo instanceof CardPaymentInfo) {
						CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) paymentInfo;
						// Collect the CardPaymentInfo objects for compress
						PaymentGatewayBO.cardPaymentInfoCompressor(cardPaymentInfo, colCardPaymentInfo);
					}
					// Passenger Credit
					else if (paymentInfo instanceof PaxCreditInfo) {
						// Note : There might be a scenario that reservation is paid by other carrier pax credit
						// and marketing carrier pax credit for a normal own airline booking. For this case we may
						// getting initially
						// captured paxCreditpayments. We are merging them because those need to be recorded in pax
						// transactions.
						PaxCreditInfo paxCreditInfo = (PaxCreditInfo) paymentInfo;
						paxCreditInfo.getPaxCredit().setCreditUtilizingPnr(pnr);
						boolean enableTnxGranularity = ReservationModuleUtils.getReservationBD()
								.isReservationTnxGranularityEnable(paxCreditInfo.getPaxCredit().getPnr());
						DefaultServiceResponse response = (DefaultServiceResponse) ReservationModuleUtils.getCreditAccountBD()
								.carryForwardCredit(paxCreditInfo.getPaxCredit(), credentialsDTO, enableTnxGranularity);

						Map<Integer, Collection<TnxCreditPayment>> responseParam = (Map<Integer, Collection<TnxCreditPayment>>) response
								.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);

						combineCreditPayments(paxCreditPayments, responseParam);
						itColPaymentInfo.remove();
					}
					// Agent Credit
					else if (paymentInfo instanceof AgentCreditInfo) {
						AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
						if (AppSysParamsUtil.isAeroMartPayEnabled() && trackInfo != null && trackInfo.getDirectBillId() != null) {
							agentCreditInfo.getPaymentReferenceTO().setPaymentRef(trackInfo.getDirectBillId());
						}
						// Calling travel agents to reduce necessary payment amounts
						AgentTransaction agentTransaction = travelAgentFinanceBD.recordSale(agentCreditInfo.getAgentCode(),
								agentCreditInfo.getTotalAmount(), pnr, pnr, agentCreditInfo.getPayCurrencyDTO(),
								agentCreditInfo.getPaymentReferenceTO(), null, null);

						agentSaleTxId = agentTransaction.getTnxId();
						// FIXME revise this logic . Are we adding a invoice rec for
						// dry agent debiting also ?
						colAgentSaleTxId.add(agentSaleTxId);
						agentCode = agentCreditInfo.getAgentCode();
						hasOnAccPayments = true;
					}
					// Cash Payment
					else if (paymentInfo instanceof CashPaymentInfo) {
						// Do nothing since it's just a payment
					}
					else if (paymentInfo instanceof LMSPaymentInfo) {
						// Nothing to be done here
					}

					else if (paymentInfo instanceof VoucherPaymentInfo) {
						// Nothing to be done here
					} else if (paymentInfo instanceof OfflinePaymentInfo) {
						OfflinePaymentInfo offlinePaymentInfo = (OfflinePaymentInfo) paymentInfo;
						PaymentGatewayBO.getUniqueOfflinePaymentObject(offlinePaymentInfo, colOfflinePaymentInfo);
					}
				}
			} // end while
		}

		if (hasOnAccPayments) {
			PnrTransactionInvoice.createInvoices(pnr, agentCode, colAgentSaleTxId, null);
		}

		/** EuroCommerce Fraud Checking */
		ReservationBO.processCCFraudCheck(pnr, ondFareDTOs, colTnxIds, credentialsDTO, reservation, trackInfo,
				colCardPaymentInfo, enableCCFraudCheck, reservationType);
		// Card Details configuration data

		paymentGatewayID = getPaymentGatewayID(colCardPaymentInfo);
		if (paymentGatewayID != null && getPaymentGatewayConfigExists(colCardPaymentInfo)) {
			cardDetailConfigData = paymentBrokerBD.getPaymentGatewayCardConfigData(paymentGatewayID.toString());
		}
		ReservationContactInfo contactInfo = (reservation != null) ? reservation.getContactInfo() : null;
		TravelDTO createTravelDTO = TOAssembler.createTravelDTO(contactInfo, ondFareDTOs);
		Object[] responseMaps = this.processCardPayments(colCardPaymentInfo, colPaymentInfo, blockIds, colTnxIds, credentialsDTO,
				cardDetailConfigData, createTravelDTO, pnr);
		if (colOfflinePaymentInfo.size() > 0) {
			contactInfo = contactInfo != null ? contactInfo : (ReservationContactInfo) this.getParameter(CommandParamNames.RESERVATION_CONTACT_INFO);
			if (reservation != null) {
				this.processOfflinePayments(reservation, credentialsDTO, colOfflinePaymentInfo);
			} else if (StringUtils.isNoneBlank(pnr) && contactInfo != null) {
				contactInfo.setPnr(pnr);
				this.processOfflinePayments(contactInfo, credentialsDTO, colOfflinePaymentInfo, pnr);
			}
		//assuming only one item
		
			for (OfflinePaymentInfo payInfo : colOfflinePaymentInfo) {
				fatoRef=payInfo.getPayReference();
			}
		}
		Map<String, String> paymentAuthIdMap = (Map<String, String>) responseMaps[0];
		Map<String, Integer> paymentBrokerRefMap = (Map<String, Integer>) responseMaps[1];

		// Update external transaction details
		if (triggerExternalPayTxUpdate != null && triggerExternalPayTxUpdate.booleanValue()) {
			externalPaymentInfo.setInternalPayId(agentSaleTxId);
			processExternalPayTx(externalPaymentInfo, credentialsDTO);
		}

		if (ondFareDTOs != null) {
			Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = ReservationCoreUtils.getSegmentSeatDistsDTOs(ondFareDTOs);
			for (SegmentSeatDistsDTO segmentSeatDistsDTO : segmentSeatDistsDTOs) {
				Collection<SeatDistribution> seatDistributions = segmentSeatDistsDTO.getSeatDistribution();
				for (SeatDistribution seatDist : seatDistributions) {
					if (seatDist.isOverbook() && !seatDist.isSameBookingClassAsExisting()
							&& !seatDist.isBookedFlightCabinBeingSearched() && !segmentSeatDistsDTO.isFixedQuotaSeats()) {
						Collection<String> strFltSegIds = new ArrayList<String>();
						strFltSegIds.add(String.valueOf(segmentSeatDistsDTO.getFlightSegId()));
						ReservationModuleUtils.getFlightInventoryBD().doAuditOverBookings(strFltSegIds,
								segmentSeatDistsDTO.getBookingCode(), null, pnr,
								String.valueOf(segmentSeatDistsDTO.getFlightId()), credentialsDTO.getUserId(),
								seatDist.getNoOfSeats());
					} else {
						Collection<String> strFltSegIds = new ArrayList<String>();
						strFltSegIds.add(String.valueOf(segmentSeatDistsDTO.getFlightSegId()));
						ReservationModuleUtils.getFlightInventoryBD().doAuditSeatSold(segmentSeatDistsDTO,
								String.valueOf(segmentSeatDistsDTO.getFlightId()), credentialsDTO.getUserId(), seatDist);
					}
				}
			}
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		this.setParameter(CommandParamNames.CMI_FATOURATI_REF, fatoRef);
		if(output != null){
		output.addResponceParam(CommandParamNames.CMI_FATOURATI_REF, fatoRef);
		}
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		response.addResponceParam(CommandParamNames.BLOCK_KEY_IDS, blockIds);
		response.addResponceParam(CommandParamNames.PAYMENT_AUTHID_MAP, paymentAuthIdMap);
		response.addResponceParam(CommandParamNames.PAYMENT_BROKER_REF_MAP, paymentBrokerRefMap);
		response.addResponceParam(CommandParamNames.PAX_CREDIT_PAYMENTS, paxCreditPayments);
		response.addResponceParam(CommandParamNames.CONNECTING_FLT_SEG_ID, connectingPnrSegment);
		response.addResponceParam(CommandParamNames.PAYMENT_CARD_CONFIG_DATA, cardDetailConfigData);
		response.addResponceParam(CommandParamNames.CMI_FATOURATI_REF, fatoRef);
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		if (this.getParameter(CommandParamNames.SELECTED_ANCI_MAP) != null) {
			response.addResponceParam(CommandParamNames.SELECTED_ANCI_MAP, this.getParameter(CommandParamNames.SELECTED_ANCI_MAP));
		}
		if (this.getParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER) != null) {
			response.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER,
					this.getParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER));
			response.addResponceParam(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY,
					this.getParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY));
		}
		log.debug("Exit execute");
		return response;
	}

	/**
	 * Get Payment gateway ID
	 * 
	 * @param colCardPaymentInfo
	 * @return
	 */
	private Integer getPaymentGatewayID(Collection<CardPaymentInfo> colCardPaymentInfo) {
		Integer paymentGatewayID = null;
		if (colCardPaymentInfo != null && !colCardPaymentInfo.isEmpty()) {
			for (CardPaymentInfo cardPaymentInfo : colCardPaymentInfo) {
				if (cardPaymentInfo.getIpgIdentificationParamsDTO() != null) {
					paymentGatewayID = cardPaymentInfo.getIpgIdentificationParamsDTO().getIpgId();
					if (paymentGatewayID != null) {
						break;
					}
				}
			}
		}
		return paymentGatewayID;
	}

	private boolean getPaymentGatewayConfigExists(Collection<CardPaymentInfo> colCardPaymentInfo) {
		boolean configExists = false;
		if (colCardPaymentInfo != null && !colCardPaymentInfo.isEmpty()) {
			for (CardPaymentInfo cardPaymentInfo : colCardPaymentInfo) {
				if (cardPaymentInfo.getIpgIdentificationParamsDTO() != null) {
					configExists = cardPaymentInfo.getIpgIdentificationParamsDTO().is_isIPGConfigurationExists();
					if (configExists == true) {
						break;
					}
				}
			}
		}
		return configExists;
	}

	private void combineCreditPayments(Map<Integer, Collection<TnxCreditPayment>> existingPaxCreditPayments,
			Map<Integer, Collection<TnxCreditPayment>> newPaxCreditPayments) {
		for (Integer paxSequence : newPaxCreditPayments.keySet()) {
			if (existingPaxCreditPayments.get(paxSequence) == null) {
				existingPaxCreditPayments.put(paxSequence, newPaxCreditPayments.get(paxSequence));
			} else {
				existingPaxCreditPayments.get(paxSequence).addAll(newPaxCreditPayments.get(paxSequence));
			}
		}
	}

	/**
	 * Process Enternal Payment Transactions
	 * 
	 * @param externalPaymentTnx
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void processExternalPayTx(ExternalPaymentTnx externalPaymentTnx, CredentialsDTO credentialsDTO)
			throws ModuleException {
		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setBalanceQueryKey(externalPaymentTnx.getBalanceQueryKey());
		criteriaDTO.setPnr(externalPaymentTnx.getPnr());
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.INITIATED);

		// Transactions with status 'Aborted' are allowed to manually reconcile
		// any number of times
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.ABORTED);
		// Recon failed transaction are allowed reconcile again
		if (!ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED.equals(externalPaymentTnx.getReconcilationStatus())) {
			criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS);
		}

		Map<String, PNRExtTransactionsTO> pnrTnxMap = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
				.getExtPayTransactions(criteriaDTO);

		PNRExtTransactionsTO pnrPayTnxs = null;
		if (pnrTnxMap != null) {
			pnrPayTnxs = pnrTnxMap.get(externalPaymentTnx.getPnr());
			if (pnrPayTnxs == null || pnrPayTnxs.getExtPayTransactions() == null
					|| pnrPayTnxs.getExtPayTransactions().size() == 0) {
				throw new ModuleException("airreservations.recordExtPay.initRecNotFound");
			}
		}

		ExternalPaymentTnx currentExternalPaymentTnx = pnrPayTnxs.getExtPayTransactions().iterator().next();

		currentExternalPaymentTnx.setExternalPayId(externalPaymentTnx.getExternalPayId());
		currentExternalPaymentTnx.setStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);
		currentExternalPaymentTnx.setInternalTnxEndTimestamp(new Date());
		currentExternalPaymentTnx.setExternalTnxEndTimestamp(externalPaymentTnx.getExternalTnxEndTimestamp());
		currentExternalPaymentTnx.setExternalPayStatus(externalPaymentTnx.getExternalPayStatus());
		currentExternalPaymentTnx.setInternalPayId(externalPaymentTnx.getInternalPayId());// Agent transaction id
		currentExternalPaymentTnx.setAmount(externalPaymentTnx.getAmount());// for CDM, value is rounded up to nearest
																			// 10
		if (currentExternalPaymentTnx.getUserId() == null) {// do not override the user, if already set
			currentExternalPaymentTnx.setUserId(externalPaymentTnx.getUserId() != null
					? externalPaymentTnx.getUserId()
					: credentialsDTO.getUserId());
		}
		if (externalPaymentTnx.getRemarks() != null) {
			currentExternalPaymentTnx.setRemarks((currentExternalPaymentTnx.getRemarks() != null ? currentExternalPaymentTnx
					.getRemarks() + "|" : "")
					+ externalPaymentTnx.getRemarks());
		}
		if (externalPaymentTnx.getReconcilationStatus() != null) {
			currentExternalPaymentTnx.setReconcilationStatus(externalPaymentTnx.getReconcilationStatus());
		}
		if (externalPaymentTnx.getReconciledTimestamp() != null) {
			currentExternalPaymentTnx.setReconciledTimestamp(externalPaymentTnx.getReconciledTimestamp());
		}

		// TODO set additional parameters
		ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.saveOrUpdateExternalPayTxInfo(currentExternalPaymentTnx);
	}

	/**
	 * Process card payments
	 * 
	 * @param colCardPaymentInfo
	 * @param colPaymentInfo
	 * @param blockIds
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @param pnr TODO
	 * @return
	 * @throws ModuleException
	 */
	private Object[] processCardPayments(Collection<CardPaymentInfo> colCardPaymentInfo, Collection<PaymentInfo> colPaymentInfo,
			Collection<TempSegBcAlloc> blockIds, Collection<Integer> colTnxIds, CredentialsDTO credentialsDTO,
			List<CardDetailConfigDTO> cardDetailConfigData, TravelDTO travelData, String pnr) throws ModuleException {
		// If any credit card payments exist
		Map<String, String> responseAuthIdMap = null;
		Map<String, Integer> responsePaymentBrokerRefMap = null;
		if (colCardPaymentInfo.size() > 0) {
			responseAuthIdMap = new HashMap<String, String>();
			responsePaymentBrokerRefMap = new HashMap<String, Integer>();
			// Capture the card payment
			Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
			CardPaymentInfo cardPaymentInfo;
			String key;
			while (itColCardPaymentInfo.hasNext()) {
				cardPaymentInfo = itColCardPaymentInfo.next();

				// Send Payment request and capture the response
				Object[] responses = PaymentGatewayBO.creditPayment(cardPaymentInfo, blockIds, colTnxIds, credentialsDTO,
						cardDetailConfigData, travelData, pnr);
				int paymentBrokerRefNo = Integer.parseInt((String) responses[0]);
				String operationType = (String) responses[1];
				String authorizationId = (String) responses[2];
				key = ReservationApiUtils.getPaymentKey(cardPaymentInfo);
				responseAuthIdMap.put(key, authorizationId);
				responsePaymentBrokerRefMap.put(key, Integer.valueOf(paymentBrokerRefNo));
				// Update the references
				PaymentGatewayBO.updatePaymentBrokerReferences(cardPaymentInfo, colPaymentInfo, paymentBrokerRefNo,
						operationType, authorizationId);
			}
		}
		return new Object[] { responseAuthIdMap, responsePaymentBrokerRefMap };
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param reservation
	 * @param colPaymentInfo
	 * @param credentialsDTO
	 * @param triggerBlockSeats
	 * @param ondFareDTOs
	 * @param blockIds
	 * @param colTnxIds
	 * @param isModifyChgOperation
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void validateParams(String pnr, Reservation reservation, Collection colPaymentInfo, CredentialsDTO credentialsDTO,
			Boolean triggerBlockSeats, Collection ondFareDTOs, Collection blockIds, Collection<Integer> colTnxIds,
			Boolean isModifyChgOperation) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || colPaymentInfo == null || credentialsDTO == null || triggerBlockSeats == null || colTnxIds == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (triggerBlockSeats.booleanValue()) {
			if (ondFareDTOs == null) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}
		} else {
			if (blockIds == null) {
				throw new ModuleException("airreservations.arg.invalid.null");
			}
		}
		// validate payment
		if (isInvalidPayment(colPaymentInfo)) {
			throw new ModuleException("airreservations.arg.invalid.payment");
		}
		// validate BSP payment
		if (ReservationApiUtils.isBSPpayment(colPaymentInfo) && reservation != null && reservation.getSegments() != null
				&& getNumberOfSegments(reservation) > 8) {
			throw new ModuleException("airreservations.arg.invalid.bsp.payment");
		}

		if (ReservationApiUtils.isBSPpayment(colPaymentInfo) && !AppSysParamsUtil.isBspPaymentsAcceptedForOCAlterReservation()) {
			if (isModifyChgOperation != null && isModifyChgOperation) {
				throw new ModuleException("airreservations.arg.bsp.payment.for.cancel.segments.not.allowed");
			}

			if (reservation != null && reservation.getSegments() != null) {
				if (reservation.isAnyCancelSegmentsExists()) {
					throw new ModuleException("airreservations.arg.bsp.payment.for.cancel.segments.not.allowed");
				}
			}
		}

		log.debug("Exit validateParams");
	}

	/**
	 * @param colPaymentInfo
	 * @return
	 */
	private boolean isInvalidPayment(Collection<PaymentInfo> colPaymentInfo) {

		Iterator<PaymentInfo> iteratorPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		boolean anyotherPaymentFound = false;
		boolean bspPaymentFound = false;
		while (iteratorPaymentInfo.hasNext()) {
			paymentInfo = iteratorPaymentInfo.next();
			if (paymentInfo instanceof AgentCreditInfo) {
				AgentCreditInfo agentCreditInfo = (AgentCreditInfo) paymentInfo;
				if (agentCreditInfo.getPaymentReferenceTO() != null
						&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType() != null
						&& agentCreditInfo.getPaymentReferenceTO().getPaymentRefType().equals(PAYMENT_REF_TYPE.BSP)) {
					bspPaymentFound = true;
				} else {
					anyotherPaymentFound = true;
				}
			} else {
				anyotherPaymentFound = true;
			}
		}
		return (anyotherPaymentFound && bspPaymentFound && !AppSysParamsUtil.isBspPaymentsAcceptedForOCAlterReservation());
	}

	/**
	 * get the number of segments excluding exchanged segments to do validation
	 * 
	 * @param reservation
	 * @return
	 */
	private int getNumberOfSegments(Reservation reservation) {
		int noOfSegments = 0;
		if (reservation != null && reservation.getSegments() != null) {
			for (ReservationSegment reservationSegment : reservation.getSegments()) {
				if (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equals(reservationSegment.getSubStatus())) {
					noOfSegments++;
				}
			}
		}
		return noOfSegments;

	}

	private void processOfflinePayments(Reservation reservation, CredentialsDTO credentialsDTO,
			Collection<OfflinePaymentInfo> colOfflinePaymentInfo) throws ModuleException {
		processOfflinePayments(reservation.getContactInfo(), credentialsDTO, colOfflinePaymentInfo, reservation.getPnr());
	}

	private void processOfflinePayments(ReservationContactInfo reservationContactInfo, CredentialsDTO credentialsDTO,
										Collection<OfflinePaymentInfo> colOfflinePaymentInfo, String pnr) throws ModuleException {
		Date onHoldReleaseDate = (Date) this.getParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP);
		AppIndicatorEnum appIndicatorEnum = credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null ? credentialsDTO.getTrackInfoDTO().getAppIndicator() : null;
		PaymentUtil.cancelPreviousOfflineTransaction(pnr, appIndicatorEnum, true);
		if (colOfflinePaymentInfo.size() > 0) {
			Iterator<OfflinePaymentInfo> itColOfflinePaymentInfo = colOfflinePaymentInfo.iterator();
			OfflinePaymentInfo offlinePaymentInfo;
			while (itColOfflinePaymentInfo.hasNext()) {
				offlinePaymentInfo = itColOfflinePaymentInfo.next();
				Integer temporyPaymentId = ReservationModuleUtils.getReservationBD().recordTemporyPaymentEntryForOffline(
						reservationContactInfo, offlinePaymentInfo, credentialsDTO, true, true, pnr);
				Object[] response = PaymentGatewayBO
						.getRequestData(offlinePaymentInfo, temporyPaymentId, reservationContactInfo, onHoldReleaseDate);
				if (ArrayUtils.getLength(response) >= 3) {
					this.setParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER, response[2]);
					if (ArrayUtils.getLength(response) >= 4) {
						this.setParameter(CommandParamNames.PAYMENT_GATEWAY_BILL_NUMBER_LABEL_KEY, response[3]);
					}
				}
			}
		}
	}
}
