/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.adl.datastructure.AdlAdditionalInfoDataStructure;
import com.isa.thinair.airreservation.core.bl.adl.datastructure.AdlMasterDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext.PalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.AdlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.pal.dataStructure.additionalinformation.PalCalAdditionalInfoDataStructure;
import com.isa.thinair.airreservation.core.bl.pnl.datastructure.additionalinformation.PnlAdditionalInfoDataStructure;
import com.isa.thinair.airreservation.core.bl.pnl.datastructure.masterinformation.PnlMasterDataStructure;

/**
 * @author udithad
 *
 */
public class PnlAdlAdditionalDataFactory<K extends BasePassengerCollection, T extends BaseDataContext>
		extends
		PnlAdlAbstractDataFactory<BasePassengerCollection, BaseDataContext> {

	@Override
	public MessageDataStructure createDataStructure(String messageType) {
		MessageDataStructure messageDataStructure = null;
		if (messageType.equals(MessageTypes.PNL)) {
			messageDataStructure = new PnlAdditionalInfoDataStructure<PassengerCollection, PnlDataContext>();
		} else if (messageType.equals(MessageTypes.ADL)) {
			messageDataStructure = new AdlAdditionalInfoDataStructure<PassengerCollection, AdlDataContext>();
		}else if (messageType.equals(MessageTypes.PAL) || messageType.equals(MessageTypes.CAL)){
			messageDataStructure = new PalCalAdditionalInfoDataStructure<PassengerCollection, PalDataContext>();
		}
		return messageDataStructure;
	}

}
