package com.isa.thinair.airreservation.core.bl.commission;

import java.util.List;

import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;

/**
 * 
 * Agent commission Strategy for create reservation
 * 
 */
public class CreateResCS implements CommissionStrategy {

	@Override
	public boolean execute(PreferenceAnalyzer preferenceAnalyzer, String ondCode, List<String> journeyOnds, List<List<String>> journeyOndSummary) {
		preferenceAnalyzer.setOldFareId(null);
		return true;
	}

}
