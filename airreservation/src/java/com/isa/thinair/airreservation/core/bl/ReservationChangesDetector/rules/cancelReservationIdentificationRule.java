package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class cancelReservationIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public cancelReservationIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {

		RuleResponseDTO response = getResponseDTO();

		if (!isCNXReservation(dataContext.getExisitingReservation()) && isCNXReservation(dataContext.getUpdatedReservation())) {

			updatePaxWiseAffectedSegmentListToAllGivenPax(response, null,
					getUncancelledSegIds(dataContext.getExisitingReservation()),
					getUncancelledPaxIds(dataContext.getUpdatedReservation()));

		}

		return response;
	}

}
