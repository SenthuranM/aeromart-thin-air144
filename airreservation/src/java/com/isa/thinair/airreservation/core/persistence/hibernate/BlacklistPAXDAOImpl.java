package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleSearchTO;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistPAXRule;
import com.isa.thinair.airreservation.api.model.BlacklistReservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.airreservation.core.persistence.dao.BlacklistPAXDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 * Class implementing the Blacklist PAX class hierachy's data access operations.
 * 
 * @author rajiv
 * 
 * @isa.module.dao-impl dao-name="BlacklistPAXDAO"
 */
public class BlacklistPAXDAOImpl extends PlatformBaseHibernateDaoSupport implements BlacklistPAXDAO {
	
	private static Log log = LogFactory.getLog(BlacklistPAXDAOImpl.class);
	
	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}
	

	@Override
	public void saveBlacklistPAX(BlacklistPAX blacklistPAX) {
		hibernateSave(blacklistPAX);
	}

	@Override
	public Page<BlacklistPAX> searchBlacklistPAX(BlacklistPAXCriteriaSearchTO blacklistPAXCriteriaSearchTO, Integer start,
			Integer size) {
		
		if (blacklistPAXCriteriaSearchTO != null) {
			
			String sql = "from BlacklistPAX bp where bp.blacklistType= :blacklistType ";   
			
			if(!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getPaxFullName())){
				sql += "and bp.fullName= :paxFullName "; 
			}

			if (!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getPassportNo())) {
				sql += "and bp.passportNo= :passportNo "; 
			}

			if (!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getNationality())) {
				if (blacklistPAXCriteriaSearchTO.getNationality() != "-")
					sql += "and bp.nationality= :nationalityCode"; 
			}
			
			if (!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getPaxStatus())) {
				if (blacklistPAXCriteriaSearchTO.getNationality() != "-")
					sql += "and bp.status= :paxStatus "; 
			}

			if (blacklistPAXCriteriaSearchTO.getDateOfBirth() != null) {
				sql += "and bp.dateOfBirth= :dob"; 
			}

			Query query = getSession().createQuery(sql);
			query.setParameter("blacklistType",blacklistPAXCriteriaSearchTO.getBlacklistType());
			
			if(!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getPaxFullName())){
				query.setParameter("paxFullName" , blacklistPAXCriteriaSearchTO.getPaxFullName());
			}

			if (!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getPassportNo())) {
				query.setParameter("passportNo" ,blacklistPAXCriteriaSearchTO.getPassportNo());
			}

			if (!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getNationality())) {
				if (blacklistPAXCriteriaSearchTO.getNationality() != "-")
					query.setParameter("nationalityCode",new Integer(blacklistPAXCriteriaSearchTO.getNationality()));
			}
			
			if (!StringUtil.isNullOrEmpty(blacklistPAXCriteriaSearchTO.getPaxStatus())) {
				if (blacklistPAXCriteriaSearchTO.getNationality() != "-")
					query.setParameter("paxStatus",blacklistPAXCriteriaSearchTO.getPaxStatus());
			}

			if (blacklistPAXCriteriaSearchTO.getDateOfBirth() != null) {
				query.setParameter("dob",blacklistPAXCriteriaSearchTO.getDateOfBirth());
			}

			int count = query.list().size();

			query.setFirstResult(start).setMaxResults(size);

			Collection<BlacklistPAX> results = query.list();

			return new Page<BlacklistPAX>(count, start, start + results.size(), results);
		}
		return new Page<BlacklistPAX>(0, 0, 0, new ArrayList<BlacklistPAX>());
	}

	@Override
	public BlacklistPAX searchBlacklistPAXByBlacklistPAXId(Long blacklistPAXCriteriaID) {

		if (blacklistPAXCriteriaID != null) {
			BlacklistPAX blacklistPAX = new BlacklistPAX();
			String sql = "from BlacklistPAX bp where bp.blacklistPAXCriteriaID= '" + blacklistPAXCriteriaID + "' ";

			Query query = getSession().createQuery(sql);
	
			Collection<BlacklistPAX> results = query.list();

			if (results != null && results.size() > 0) {
				blacklistPAX = BeanUtils.getFirstElement(results);
			}

			return blacklistPAX;
		}
		return null;
	}

	@Override
	public long updateBlacklistPAX(BlacklistPAX blacklistPAX) {
		BlacklistPAX existPAX = searchBlacklistPAXByBlacklistPAXId(blacklistPAX.getBlacklistPAXCriteriaID());
		if (existPAX == null) {
			return (long) -1;
		} else {
			BlacklistPAXUtil.merge(existPAX, blacklistPAX);
			update(existPAX);
			return 1;
		}
	}

	@Override
	public Page<BlacklisPaxReservationTO> searchBlacklistPaxResTO(BlacklisPaxReservationTO blPaxResTOSearch, Integer start,
			Integer size) throws ModuleException {
	
		try{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		List<Object> listParams = new ArrayList<Object>();
		List<Object> listParamsDummy = new ArrayList<Object>();
		
		String sql		="	select  " 
						+"	pnrpax.pnr as PNR, pnrpax.pnr_pax_id as PNRPAXID, " 
					    +"	res.status as STATS,res.dummy_booking as DUMMYBOOKING, "
					    +"	blpax.PAX_FULL_NAME as PAXFULLNAME, blpax.passport_number as PPNO, blpax.bl_type as BLTYPE, blpax.dob as DOB, "
					    +"	blres.is_actioned as ISACT, blres.BL_PNR_PASSENGER_ID as BLRESID,blres.bl_passenger_id as BLPAXID,blres.version as VERSION,blres.reason_to_allow as REASON, blres.REASON_TO_CLEAR as REASONFORWHITELISTED, "
					    +"	fltseg.est_time_departure_zulu as DEPTTIME, fltseg.est_time_arrival_zulu as ARRTIME, "
					    +"	nat.description as NATION,"
					    +"	flt.flight_number as FLTNO";
		
		String sqlDummy ="	select  "  
						+"	pnrpax.pnr as PNR, pnrpax.pnr_pax_id as PNRPAXID, " 
					    +"	res.status as STATS,res.dummy_booking as DUMMYBOOKING, "
					    +"	blpax.PAX_FULL_NAME as PAXFULLNAME, blpax.passport_number as PPNO, blpax.bl_type as BLTYPE, blpax.dob as DOB, "
					    +"	blres.is_actioned as ISACT, blres.BL_PNR_PASSENGER_ID as BLRESID,blres.bl_passenger_id as BLPAXID,blres.version as VERSION,blres.reason_to_allow as REASON, blres.REASON_TO_CLEAR as REASONFORWHITELISTED, "
					    +"	extfltseg.est_time_departure_zulu as DEPTTIME, extfltseg.est_time_arrival_zulu as ARRTIME, "
					    +"	nat.description as NATION,"
					    +"	extfltseg.flight_number as FLTNO";
				
		
		String strfrom = "  from "
						+"	T_BL_PNR_PASSENGER  blres,"
						+"	t_bl_passenger   blpax,"
						+"	t_pnr_passenger  pnrpax,"
						+"	t_pnr_segment    pnrseg,"
						+"	t_flight_segment fltseg,"
						+"	t_flight         flt,"
						+"	t_reservation    res,"
						+"	t_nationality    nat " ;
		
		String strfromDummy = "  from "
						+"	T_BL_PNR_PASSENGER  blres,"
						+"	t_bl_passenger   blpax,"
						+"	t_pnr_passenger  pnrpax,"
						+"	t_ext_pnr_segment    extpnrseg,"
						+"	t_ext_flight_segment extfltseg,"
						+"	t_reservation    res,"
						+"	t_nationality    nat " ;
			
		String strWhere ="  where "
						+"	blres.bl_passenger_id = blpax.bl_passenger_id and"
					    +"	pnrpax.pnr_pax_id = blres.pnr_pax_id  and"
					    +"	pnrseg.pnr = pnrpax.pnr and"
					    +"	fltseg.flt_seg_id = pnrseg.flt_seg_id and"
					    +"	flt.flight_id = fltseg.flight_id and"
					    +"	flt.status != 'CNX' and"
					    +"	res.pnr = pnrpax.pnr and"
					    +"	nat.nationality_code = blpax.nationality_code and"
					    +"	pnrseg.pnr_seg_id in"
					    +"	(select pnr_seg_id from t_pnr_segment a"
						+"	inner join (select a.pnr, min(a.segment_seq) as seq from t_pnr_segment a group by a.pnr) temp"
						+"	on temp.seq = a.segment_seq and temp.pnr=a.pnr)";  // use dpet date to find out first pnr sgement
		
		String strWhereDummy ="  where "
				+"	blres.bl_passenger_id = blpax.bl_passenger_id and"
			    +"	pnrpax.pnr_pax_id = blres.pnr_pax_id  and"
			    +"	extpnrseg.pnr = pnrpax.pnr and"
			    +"	extfltseg.ext_flt_seg_id = extpnrseg.ext_flt_seg_id and"
			    +"	extfltseg.status != 'CNX' and"
			    +"	res.DUMMY_BOOKING       ='Y' and"   // get only dummy
			    +"	res.pnr = pnrpax.pnr and"
			    +"	nat.nationality_code = blpax.nationality_code and"
			    +"	extpnrseg.ext_pnr_seg_id in"
			    +"	(select ext_pnr_seg_id from t_ext_pnr_segment a"
				+"	inner join (select a.pnr, min(a.segment_seq) as seq from t_ext_pnr_segment a group by a.pnr) temp"
				+"	on temp.seq = a.segment_seq and temp.pnr=a.pnr)";
		
		//where for input	
		
		if(!StringUtil.isNullOrEmpty(blPaxResTOSearch.getFullName())){
			String fullNameStr = "%"+blPaxResTOSearch.getFullName().toUpperCase()+"%";
			fullNameStr= fullNameStr.trim().replaceAll(" +", BlacklistPAXUtil.PAX_NAME_SEPARATOR);			
			strWhere = strWhere+ "	and upper(blpax.PAX_FULL_NAME) like ? ";		
			strWhereDummy = strWhereDummy+ "	and upper(blpax.PAX_FULL_NAME) like? ";
			listParams.add(fullNameStr);
			listParamsDummy.add(fullNameStr);
		}		
		if(!StringUtil.isNullOrEmpty(blPaxResTOSearch.getPassportNo())){
			strWhere = strWhere+ "	and blpax.passport_number =? ";
			listParams.add(blPaxResTOSearch.getPassportNo());
			
			strWhereDummy = strWhereDummy+ "	and blpax.passport_number =? ";
			listParamsDummy.add(blPaxResTOSearch.getPassportNo());
		}
		if(blPaxResTOSearch.getNationalityCode() >0){
			strWhere = strWhere+ "	and blpax.nationality_code =? ";
			listParams.add(blPaxResTOSearch.getNationalityCode());
			
			strWhereDummy = strWhereDummy+ "	and blpax.nationality_code =? ";
			listParamsDummy.add(blPaxResTOSearch.getNationalityCode());
		}
		if(blPaxResTOSearch.getDateOfBirth() != null){
			strWhere = strWhere+ "	and blpax.dob =? ";
			listParams.add(blPaxResTOSearch.getDateOfBirth());
			
			strWhereDummy = strWhereDummy+ "	and blpax.dob =? ";
			listParamsDummy.add(blPaxResTOSearch.getDateOfBirth());
		}
		if(!StringUtil.isNullOrEmpty(blPaxResTOSearch.getBlacklistType())){
			strWhere = strWhere+ "	and blpax.bl_type =? ";
			listParams.add(blPaxResTOSearch.getBlacklistType());
			
			strWhereDummy = strWhereDummy+ "	and blpax.bl_type =? ";
			listParamsDummy.add(blPaxResTOSearch.getBlacklistType());
		}
		if(!StringUtil.isNullOrEmpty(blPaxResTOSearch.getPnr())){
			strWhere = strWhere+ "	and res.pnr =? ";
			listParams.add(blPaxResTOSearch.getPnr());
			
			strWhereDummy = strWhereDummy+ "	and res.pnr =? ";
			listParamsDummy.add(blPaxResTOSearch.getPnr());
		}
		if(!StringUtil.isNullOrEmpty(blPaxResTOSearch.getIsActioned())){
			strWhere = strWhere+ "	and blres.IS_ACTIONED =? ";
			listParams.add(blPaxResTOSearch.getIsActioned());
			
			strWhereDummy = strWhereDummy+ "	and blres.IS_ACTIONED =? ";
			listParamsDummy.add(blPaxResTOSearch.getIsActioned());
		}
		
		
		Date fromDate =null;
		Date toDate =null;
		if(blPaxResTOSearch.getEffectiveFrom()!=null && !blPaxResTOSearch.getEffectiveFrom().equals("")){
			 try {
				fromDate = CalendarUtil.getParsedTime(blPaxResTOSearch.getEffectiveFrom(), CalendarUtil.PATTERN1);
			} catch (ParseException e) {			
				log.error("calendar issue "+e.getLocalizedMessage());
				throw new ModuleException(e, e.getMessage());
			}
		}
		if(blPaxResTOSearch.getEffectiveTo()!=null && !blPaxResTOSearch.getEffectiveTo().equals("")){
			try {
				toDate   = CalendarUtil.getParsedTime(blPaxResTOSearch.getEffectiveTo(), CalendarUtil.PATTERN1);
			} catch (ParseException e) {
				log.error("calendar issue "+e.getLocalizedMessage());
				throw new ModuleException(e, e.getMessage());
			}
		}		
		if( fromDate !=null && toDate !=null ){
			strWhere = strWhere +"	and	"
								+"	((blpax.EFFECTIVE_FROM>=? and blpax.EFFECTIVE_TO>=? and blpax.EFFECTIVE_FROM<=? ) "
								+"	or "
								+"	(blpax.EFFECTIVE_FROM<=? and blpax.EFFECTIVE_TO<=? and blpax.EFFECTIVE_TO>=?) " 
								+"  or "
								+"	(blpax.EFFECTIVE_FROM<=? and blpax.EFFECTIVE_TO>=?) "
								+"	or "
								+" (blpax.EFFECTIVE_FROM>=? and blpax.EFFECTIVE_TO<=?)) ";
					
			listParams.add(fromDate);			
			listParams.add(toDate);			
			listParams.add(toDate);			
			listParams.add(fromDate);			
			listParams.add(toDate);			
			listParams.add(fromDate);			
			listParams.add(fromDate);			
			listParams.add(toDate);			
			listParams.add(fromDate);			
			listParams.add(toDate);	
			
			
			strWhereDummy = strWhereDummy +"	and	"
					+"	((blpax.EFFECTIVE_FROM>=? and blpax.EFFECTIVE_TO>=? and blpax.EFFECTIVE_FROM<=? ) "
					+"	or "
					+"	(blpax.EFFECTIVE_FROM<=? and blpax.EFFECTIVE_TO<=? and blpax.EFFECTIVE_TO>=?) " 
					+"  or "
					+"	(blpax.EFFECTIVE_FROM<=? and blpax.EFFECTIVE_TO>=?) "
					+"	or "
					+" (blpax.EFFECTIVE_FROM>=? and blpax.EFFECTIVE_TO<=?)) ";
		
			listParamsDummy.add(fromDate);			
			listParamsDummy.add(toDate);			
			listParamsDummy.add(toDate);			
			listParamsDummy.add(fromDate);			
			listParamsDummy.add(toDate);			
			listParamsDummy.add(fromDate);			
			listParamsDummy.add(fromDate);			
			listParamsDummy.add(toDate);			
			listParamsDummy.add(fromDate);			
			listParamsDummy.add(toDate);
		}
		
		sql     = sql      + strfrom 	   + strWhere;
		sqlDummy= sqlDummy + strfromDummy + strWhereDummy ;
		
		String sqlUnion ="  union  ";
		
		sql     = sql + sqlUnion + sqlDummy;
		listParams.addAll(listParamsDummy);
		
		String sqlCount =" select count(*) as CNT from ( ";
		sqlCount        =sqlCount + sql + " ) ";
		
		
		String 	sqlRownmStrt	="	SELECT * FROM(	"
								+"	SELECT m.*, rownum r	"
								+"	FROM (	"	;		
		
		String sqlRowNmEnds =  "	) m ) where r > "+ start+" and r <= " + (start+size); 
		
		sql  =sqlRownmStrt+ sql + sqlRowNmEnds;
		
		@SuppressWarnings("unchecked")
		Collection<BlacklisPaxReservationTO>colList = (Collection<BlacklisPaxReservationTO>) jdbcTemplate.query(sql, listParams.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<BlacklisPaxReservationTO> blPaxTOList = new ArrayList<BlacklisPaxReservationTO>();
				while (rs.next()) {
					BlacklisPaxReservationTO blPaxTO = new BlacklisPaxReservationTO();
					
					blPaxTO.setPnr(rs.getString("PNR"));
					blPaxTO.setPnrPaxId(rs.getInt("PNRPAXID"));		
					blPaxTO.setFullName(rs.getString("PAXFULLNAME"));
					blPaxTO.setPassportNo(rs.getString("PPNO"));
					blPaxTO.setBlacklistType(rs.getString("BLTYPE"));
					blPaxTO.setDateOfBirth(rs.getDate("DOB"));
					blPaxTO.setBlacklistPaxId(rs.getInt("BLPAXID"));
					blPaxTO.setIsActioned(rs.getString("ISACT"));  // status
					blPaxTO.setBlacklistReservationId(rs.getLong("BLRESID"));
					blPaxTO.setVersion(rs.getInt("VERSION"));
					blPaxTO.setReasonToAllow(rs.getString("REASON"));
					blPaxTO.setFlightNo(rs.getString("FLTNO"));
					blPaxTO.setPnrStatus(rs.getString("STATS"));//reservation status 
					blPaxTO.setDepatureDate(DateUtil.formatDate(rs.getDate("DEPTTIME"),"ddMMMyyyy"));
					blPaxTO.setArrivalDate(DateUtil.formatDate(rs.getDate("ARRTIME"),"ddMMMyyyy"));   
					blPaxTO.setNationality(rs.getString("NATION")); 
					blPaxTO.setDummyBooking(rs.getString("DUMMYBOOKING"));
					blPaxTO.setReasonForWhitelisted(rs.getString("REASONFORWHITELISTED")); // reason for whitelisted
					
					blPaxTOList.add(blPaxTO);
				}
				return blPaxTOList;
			}
		});
		
		Integer count = (Integer) jdbcTemplate.query(sqlCount, listParams.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("CNT"));
				}
				return null;

			}
		});
		
		
		return new Page<BlacklisPaxReservationTO>(count, start, start + size - 1, colList);
		}catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode());
		}
	}

	
	@Override
	public void updateBlacklistReservation(BlacklistReservation blacklistResToSave) throws ModuleException {
		try{
			
			update(blacklistResToSave);
		
		}catch(CommonsDataAccessException e){
			throw new ModuleException(e, e.getMessage());
		}
	}
	
	@Override
	public BlacklistReservation searchBlacklistReservationById(Long blacklistResId) throws ModuleException {
		try {
			if (blacklistResId != null) {
				BlacklistReservation blacklistPAX = new BlacklistReservation();

				String hql = "from BlacklistReservation blRes where blRes.blacklistReservationId=:blacklistReservationId  ";
				Query query = getSession().createQuery(hql);
				query.setParameter("blacklistReservationId", blacklistResId);

				@SuppressWarnings("unchecked")
				Collection<BlacklistReservation> results = query.list();

				if (results != null && results.size() > 0) {
					blacklistPAX = BeanUtils.getFirstElement(results);
				}
				return blacklistPAX;
			}
			return null;
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode());
		}
	}


	@Override
	public void saveBlacklistedPnrPassengerList(List<BlacklistReservation> blackListPnrPassengerList)throws ModuleException {
		try{
			hibernateSaveOrUpdateAll(blackListPnrPassengerList);
		} catch (CommonsDataAccessException e) {
			log.error("eror at saving blacklist pnr passenger "+e.getMessage());
			throw new ModuleException(e, e.getExceptionCode());
		}
		
	}
	
	@Override
	public Page<BlacklistPAXRule> searchBlacklistPAXRules(BlacklistPAXRuleSearchTO blacklistPAXRuleSearchTO, Integer start,
			Integer size) {
		if (blacklistPAXRuleSearchTO != null) {
			String sql;
			if (StringUtil.isNullOrEmpty(blacklistPAXRuleSearchTO.getStatus())) {
				sql = "from BlacklistPAXRule bpr where bpr.status= 'INA' OR  bpr.status= 'ACT'";
			} else {
				sql = "from BlacklistPAXRule bpr where bpr.status= '" + blacklistPAXRuleSearchTO.getStatus() + "' ";
			}

			if (!StringUtil.isNullOrEmpty(blacklistPAXRuleSearchTO.getLabel())) {
				sql += "and bpr.label= '" + blacklistPAXRuleSearchTO.getLabel() + "' ";
			}

			Query query = getSession().createQuery(sql);

			int count = query.list().size();

			query.setFirstResult(start).setMaxResults(size);

			Collection<BlacklistPAXRule> results = query.list();

			return new Page<BlacklistPAXRule>(count, start, start + results.size(), results);
		}
		return null;
	}

	@Override
	public void saveBlacklistPAXRule(BlacklistPAXRule blacklistPAXRule) {
		if (blacklistPAXRule.getRuleId() == null) {
			hibernateSave(blacklistPAXRule);
		} else {
			update(blacklistPAXRule);
		}

	}


	public List<BlacklistPAXRule> getBlacklistPAXRules() {

		String sql = "from BlacklistPAXRule";

		Query query = getSession().createQuery(sql);

		int count = query.list().size();

		List<BlacklistPAXRule> results = query.list();

		return results;

	}
	
	@Override
	public List<BlacklistPAXRule> getActiveBlacklistPAXRules() {

		String sql = "from BlacklistPAXRule bpr where bpr.status= 'ACT'";

		Query query = getSession().createQuery(sql);

		int count = query.list().size();

		List<BlacklistPAXRule> results = query.list();

		return results;

	}
	
	@Override
	public List<BlacklistPAX> getBlacklistedPaxForToday(){
		String sql1 = "from BlacklistPAX bp where bp.blacklistType='T' AND bp.status='ACT' AND sysdate BETWEEN bp.effectiveFrom and bp.effectiveTo";
		
		String sql2 = "from BlacklistPAX bp where bp.blacklistType='P' AND bp.status='ACT'  ";
		
		Query query1 = getSession().createQuery(sql1);

		int count1 = query1.list().size();
		
		List<BlacklistPAX> results1 = query1.list();
		
		Query query2 = getSession().createQuery(sql2);

		int count2 = query2.list().size();
		
		List<BlacklistPAX> results2 = query2.list();
		
		results1.addAll(results2);
		
		return results1;
		
	}
	
	public List<BlacklistPAX> getBlacklistedPaXByRule(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO, boolean nameChecked, boolean dobChecked, boolean nationalityChecked, boolean passportChecked){
		
		if(!nameChecked && !dobChecked && !nationalityChecked && !passportChecked){
			return new ArrayList<BlacklistPAX>();
		}
		
		String sql1 = "from BlacklistPAX bp where bp.blacklistType='T' AND sysdate BETWEEN bp.effectiveFrom and bp.effectiveTo and bp.status = 'ACT' ";
		
		if(nameChecked){
			sql1 += " and upper(bp.fullName)= '" + blacklistPAXCriteriaTO.getPaxFullName().toUpperCase() + "' ";
		}
		if (dobChecked) {
			sql1 += " and bp.dateOfBirth = :dob";
		}
		if (nationalityChecked) {
			sql1 += " and bp.nationality= :nationality ";
		}
		if (passportChecked) {
			sql1 += " and bp.passportNo= '" + blacklistPAXCriteriaTO.getPassportNo() + "' ";
		}
		
		Query query1 = getSession().createQuery(sql1);
		if (dobChecked) {
			query1.setParameter("dob", blacklistPAXCriteriaTO.getDateOfBirth());
		}
		if (nationalityChecked) {
			query1.setParameter("nationality", new Integer(blacklistPAXCriteriaTO.getNationality()));
		}
		
		int count1 = query1.list().size();
		List<BlacklistPAX> results1 = query1.list();
		
		
		String sql2 = "from BlacklistPAX bp where bp.blacklistType='P' and bp.status = 'ACT' ";
		
		if(nameChecked){
			sql2 += " and upper(bp.fullName)= '" + blacklistPAXCriteriaTO.getPaxFullName().toUpperCase() + "' ";
		}
		if (dobChecked) {
			sql2 += " and bp.dateOfBirth = :dob";
		}
		if (nationalityChecked) {
			sql2 += " and bp.nationality= :nationality ";
		}
		if (passportChecked) {
			sql2 += " and bp.passportNo= '" + blacklistPAXCriteriaTO.getPassportNo() + "' ";
		}
		
		Query query2 = getSession().createQuery(sql2);
		if (dobChecked) {
			query2.setParameter("dob", blacklistPAXCriteriaTO.getDateOfBirth());
		}
		if (nationalityChecked) {
			query2.setParameter("nationality", new Integer(blacklistPAXCriteriaTO.getNationality()));
		}
		int count2 = query2.list().size();
		List<BlacklistPAX> results2 = query2.list();
		
		results1.addAll(results2);
		
		return results1;
		
	}


	@Override
	public Collection<Integer> getBLackListPaxIdVsPnrPaxId(String pnr) throws ModuleException {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		List<Object> listParams   = new ArrayList<Object>();
		String sql ="select pnr_pax_id pnrPaxid "
				+ "	from "
				+ "	t_bl_pnr_passenger blpnr, "
				+ "	t_bl_passenger  blpax "
				+ "	where blpax.bl_passenger_id = blpnr.bl_passenger_id and blpnr.is_actioned='N' and "
				+ "	pnr_pax_id in (select pnr_pax_id from  t_pnr_passenger where pnr=?) and "
				+ " ((blpax.bl_type ='T' AND  sysdate BETWEEN blpax.effective_from AND blpax.effective_to ) "
				+ " or blpax.bl_type='P') ";
			//	+ "	sysdate between blpax.effective_from and blpax.effective_to ";
		listParams.add(pnr);
				
		@SuppressWarnings("unchecked")
		Collection<Integer> coln= (Collection<Integer>) jdbcTemplate.query(sql, listParams.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> col  = new ArrayList<Integer>();
				while (rs.next()) {
					col.add(rs.getInt("pnrPaxid"));										
				}
				return col;
			}
		});
		
		return coln;
	}


	@Override
	public void deleteBlacklistPAX(BlacklistPAX blacklistPAX) throws ModuleException {
		delete(blacklistPAX);
	}

	@Override
	public boolean isBlackListPaxAlreadyInReservations(Integer blacklistedPaxId) throws ModuleException {
		try {
			String hql = "from BlacklistReservation blRes where blRes.isActioned='N' AND blRes.blacklistPaxId=:blacklistedPaxId ";
			Query query = getSession().createQuery(hql);
			query.setParameter("blacklistedPaxId", blacklistedPaxId);

			return query.list().size() > 0 ? true : false;

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode());
		}
	}	
}
