package com.isa.thinair.airreservation.core.bl.tax;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.SortUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils.DAOInstance;
import com.isa.thinair.airreservation.core.util.TaxInvoiceNumberGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GSTTaxInvoiceFactory implements TaxInvoiceFactory {

	private static final String TAX3 = "TAX3";
	private static final String TAX2 = "TAX2";
	private static final String TAX1 = "TAX1";

	public static final String INVOICE_TYPE_INVOICE = "I";
	public static final String INVOICE_TYPE_DEBIT_NOTE = "D";
	public static final String INVOICE_TYPE_CREDIT_NOTE = "C";

	private Map<String, String> serviceTaxes = null;

	private Map<String, String> taxCountryCurrency = null;

	public GSTTaxInvoiceFactory() throws ModuleException {

		Collection<Charge> charges = TaxUtil.getServiceTaxes();
		serviceTaxes = new HashMap<>();
		for (Charge chg : charges) {
			if ("IN".equals(chg.getCountryCode())) {
				serviceTaxes.put(chg.getChargeCode(), chg.getInvoiceTaxCategory());
			}
		}

		taxCountryCurrency = new HashMap<>();
		taxCountryCurrency.put("IN", "INR"); // FIXME load from a configuration file
	}

	enum InvoiceType {

		INVOICE(INVOICE_TYPE_INVOICE), DEBIT_NOTE(INVOICE_TYPE_DEBIT_NOTE), CREDIT_NOTE(INVOICE_TYPE_CREDIT_NOTE);
		private String code;

		InvoiceType(String code) {
			this.code = code;
		}

		String getCode() {
			return this.code;
		}
	}

	@Override public boolean isApplicable(OperationChgTnx chargeTnx) {
		if (serviceTaxes.size() > 0) {
			for (String chargeCode : chargeTnx.getChargeCodes()) {
				if (serviceTaxes.keySet().contains(chargeCode)) {
					return true;
				}
			}
		}

		return false;

	}

	@Override public List<String> generate(Reservation reservation, TransactionSequence transactionSequence,
			List<TaxInvoice> taxInvoices, Collection<ReservationSegmentDTO> segmentsBeforeCancellation) throws ModuleException {
		Map<String, TaxInvoice> mapInvoices = new HashMap<>();
		String invoiceType = null;

		for (String chargeKey : transactionSequence.getChargeTnx().getChargeKeyWiseSummarizedCharges().keySet()) {
			ReservationPaxOndCharge charge = transactionSequence.getChargeTnx().getChargeKeyWiseSummarizedCharges()
					.get(chargeKey);
			String taxCategory = charge.getTaxAppliedCategory();

			if (!ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY.equals(taxCategory)) {
				if (!mapInvoices.containsKey(taxCategory)) {
					TaxInvoice ti = new TaxInvoice();
					ti.setDateOfIssue(transactionSequence.getDate());
					if (taxInvoices == null || taxInvoices.size() == 0) {
						ti.setInvoiceType(InvoiceType.INVOICE.getCode());
						invoiceType = InvoiceType.INVOICE.getCode();
					} else {
						TaxInvoice firstInvoice = taxInvoices.get(0);
						ti.setInvoiceType(InvoiceType.DEBIT_NOTE.getCode());
						ti.setOriginalTaxInvoiceId(firstInvoice.getTaxInvoiceId());
						invoiceType = InvoiceType.DEBIT_NOTE.getCode();
					}

					Collection<ReservationSegmentDTO> reservationSegmentDTOs =
							segmentsBeforeCancellation != null ? segmentsBeforeCancellation : reservation.getSegmentsView();
					if (reservationSegmentDTOs != null && reservationSegmentDTOs.size() > 0) {
						ReservationSegmentDTO[] resSegs = SortUtil.sortByDepDate(reservationSegmentDTOs);
						String segmentCodes = "";
						for (ReservationSegmentDTO segment : resSegs) {
							if ((ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED).equals(segment.getStatus())) {
								segmentCodes += segment.getSegmentCode() + ", ";
							}
						}
						segmentCodes = segmentCodes.replaceAll(", $", "");
						ti.setJourneyOND(segmentCodes);
					}
					ti.setOriginalPnr(reservation.getPnr());
					ti.setStateCode(charge.getTaxDepositStateCode());
					ti.setTnxSeq(transactionSequence.getTnxSequence());
					ti.setTaxRegistrationNumber(reservation.getContactInfo().getTaxRegNo());
					ti.setTotalDiscount(BigDecimal.ZERO);
					ti.setTaxRate1Amount(BigDecimal.ZERO);
					ti.setTaxRate2Amount(BigDecimal.ZERO);
					ti.setTaxRate3Amount(BigDecimal.ZERO);
					ti.addReservation(reservation);
					ti.setCurrencyCode(getCurrencyCode(charge.getTaxDepositCountryCode()));
					ti.setTaxInvoiceNumber(TaxInvoiceNumberGenerator
							.getTaxInvoiceNumber(charge.getTaxDepositStateCode(), invoiceType));
					mapInvoices.put(taxCategory, ti);
				}

				TaxInvoice taxInvoice = mapInvoices.get(taxCategory);
				String reportingGrp = getReportingChargeGrp(charge);

				if (TAX1.equals(reportingGrp)) {
					taxInvoice.setTaxRate1Id(charge.getChargeRateId());
					taxInvoice.setTaxRate1Amount(charge.getAmount());
				} else if (TAX2.equals(reportingGrp)) {
					taxInvoice.setTaxRate2Id(charge.getChargeRateId());
					taxInvoice.setTaxRate2Amount(charge.getAmount());
				} else if (TAX3.equals(reportingGrp)) {
					taxInvoice.setTaxRate3Id(charge.getChargeRateId());
					taxInvoice.setTaxRate3Amount(charge.getAmount());
				} else {
					throw new ModuleException("Invalid tax category definition");
				}

				taxInvoice.setTaxableAmount(
						AccelAeroCalculator.getNullSafeValue(charge.getTaxableAmount(), BigDecimal.ZERO));
				taxInvoice.setNonTaxableAmount(
						AccelAeroCalculator.getNullSafeValue(charge.getNonTaxableAmount(), BigDecimal.ZERO));
			}

		}
		List<String> invoiceIds = new ArrayList<>();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(transactionSequence.getDate());

		convertInvoiceValues(mapInvoices.values(), exchangeRateProxy);
		for (TaxInvoice taxInvoice : mapInvoices.values()) {
			taxInvoice = DAOInstance.TAX_INVOICE_DAO.saveTaxInvoice(taxInvoice);
			invoiceIds.add(taxInvoice.getTaxInvoiceId() + "");
		}
		return invoiceIds;
	}

	private String getCurrencyCode(String taxDepositCountryCode) {
		if (taxCountryCurrency.containsKey(taxDepositCountryCode)) {
			return taxCountryCurrency.get(taxDepositCountryCode);
		}
		return AppSysParamsUtil.getBaseCurrency();
	}

	private void convertInvoiceValues(Collection<TaxInvoice> invoices, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {
		for (TaxInvoice invoice : invoices) {
			if (!AppSysParamsUtil.getBaseCurrency().equals(invoice.getCurrencyCode())) {
				String currencyCode = invoice.getCurrencyCode();
				if (invoice.getNonTaxableAmount() != null) {
					invoice.setNonTaxableAmount(
							exchangeRateProxy.convert(currencyCode, invoice.getNonTaxableAmount(), true));
				}
				if (invoice.getTaxableAmount() != null) {
					invoice.setTaxableAmount(exchangeRateProxy.convert(currencyCode, invoice.getTaxableAmount(), true));
				}
				if (invoice.getTotalDiscount() != null) {
					invoice.setTotalDiscount(exchangeRateProxy.convert(currencyCode, invoice.getTotalDiscount(), true));
				}
				if (invoice.getTaxRate1Amount() != null) {
					invoice.setTaxRate1Amount(
							exchangeRateProxy.convert(currencyCode, invoice.getTaxRate1Amount(), true));
				}
				if (invoice.getTaxRate2Amount() != null) {
					invoice.setTaxRate2Amount(
							exchangeRateProxy.convert(currencyCode, invoice.getTaxRate2Amount(), true));
				}
				if (invoice.getTaxRate3Amount() != null) {
					invoice.setTaxRate3Amount(
							exchangeRateProxy.convert(currencyCode, invoice.getTaxRate3Amount(), true));
				}
				invoice.setCurrencyCode(currencyCode);
			}
		}
	}

	private String getReportingChargeGrp(ReservationPaxOndCharge charge) {
		return serviceTaxes.get(charge.getChargeCode());
	}

}
