package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentSubStatus;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.ModificationTypeWiseSegsResponse;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public abstract class ResModifyIdentificationRule<T> {

	private String ruleRef;

	public ResModifyIdentificationRule(String ruleRef) {
		this.ruleRef = ruleRef;
	}

	public abstract RuleResponseDTO executeRule(T dataContext) throws ModuleException;

	protected RuleResponseDTO getResponseDTO() {
		RuleResponseDTO ruleResponse = new RuleResponseDTO();
		ruleResponse.setRuleRef(ruleRef);
		return ruleResponse;
	}

	public String getRuleRef() {
		return ruleRef;
	}

	protected boolean isNotNull(Object object) {
		return object != null;
	}

	protected boolean isCNFReservation(Reservation reservation) {
		return ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus());
	}

	protected boolean isOHDReservation(Reservation reservation) {
		return ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus());
	}

	protected boolean isCNXReservation(Reservation reservation) {
		return ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus());
	}

	protected boolean isCNXSegment(ReservationSegment segment) {
		return ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus());
	}

	protected boolean isCNXPax(ReservationPax pax) {
		return ReservationInternalConstants.ReservationPaxStatus.CANCEL.equals(pax.getStatus());
	}

	protected boolean isExchangeSeg(ReservationSegment segment) {

		return ReservationSegmentSubStatus.EXCHANGED.equals(segment.getSubStatus());

	}

	
	public void updatePaxWiseAffectedSegmentListToAllGivenPax(RuleResponseDTO response, List<Integer> addList,
			List<Integer> delList, List<Integer> paxIds) {

		for (Integer paxId : paxIds) {
			if (isNullOrEmptyList(addList) || isNullOrEmptyList(delList)) {
				response.getAffectedPaxWiseSegment().put(paxId, new ModificationTypeWiseSegsResponse(addList, delList, null));
			}
		}
	}

	/*public void updatePaxWiseAffectedSegmentList(RuleResponseDTO response, List<Integer> affectedResSegId,
			List<Integer> uncancelledPaxIds) {
		if (affectedResSegId != null && !affectedResSegId.isEmpty()) {
			for (Integer paxId : uncancelledPaxIds) {
				response.getAffectedPaxWiseSegment().put(paxId, affectedResSegId);
			}
		}
	}*/

	public List<Integer> getUncancelledPaxIds(Reservation reservation) {

		List<Integer> uncancelledPaxIds = new ArrayList<>();

		for (ReservationPax pax : reservation.getPassengers()) {
			if (!isCNXPax(pax)) {
				uncancelledPaxIds.add(pax.getPnrPaxId());
			}
		}

		return uncancelledPaxIds;
	}

	public List<Integer> getUncancelledSegIds(Reservation reservation) {

		List<Integer> uncancelledSegIds = new ArrayList<>();

		for (ReservationSegment seg : reservation.getSegments()) {
			if (!isCNXSegment(seg)) {
				uncancelledSegIds.add(seg.getPnrSegId());
			}
		}

		return uncancelledSegIds;
	}

	public Collection<Integer> getUncancelledFlightSegIds(Reservation reservation) {

		Set<Integer> uncancelledSegIds = new HashSet<>();

		for (ReservationSegment seg : reservation.getSegments()) {
			if (!isCNXSegment(seg)) {
				uncancelledSegIds.add(seg.getFlightSegId());
			}
		}

		return uncancelledSegIds;
	}
	
	protected boolean isNullOrEmptyList(List<Integer> segIds) {
		
		if( segIds !=null && !segIds.isEmpty()) {
		  return true;
		}
		
		return false;
	}
}
