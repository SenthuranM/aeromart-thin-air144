package com.isa.thinair.airreservation.core.bl.commission;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;

/**
 * Agent commission Strategy for Modify Ond
 * 
 * 1) If a same route modification (ie. date change):
 * 
 * 2)If one way route is modified to another one way route:
 * 
 * 3)If one way route modified to connection route:
 * 
 * For above 1,2 & 3, a)Don't offer commissions for new OND, b)Don't remove commissions from old modifying OND if
 * commission is granted
 * 
 * 4)If one way route modified to return route: a)Offer commissions for new OND, b)Don't remove commissions from old
 * modifying OND if commission is granted [CLARIFY: Offer]
 * 
 * 5)Connection modification: a)Offer commissions for new OND, b)remove commissions from old modifying OND if commission
 * is granted
 * 
 * 6) Default behavior: a)Offer commissions for new OND, b)remove commissions from old modifying OND if commission is
 * granted
 */
public class ModifyOndCS implements CommissionStrategy {

	private static final Log log = LogFactory.getLog(ModifyOndCS.class);

	private final static String OND_DELIM = "/";

	@Override
	public boolean execute(PreferenceAnalyzer preferenceAnalyzer, String ondCode, List<String> newlyOfferingOnds,
			List<List<String>> journeyOndSummary) {
		String modifiedOndCode = preferenceAnalyzer.getModifiedOndCode(); // / 1) CMB/SHJ, CMB/SHJ/DEL
		int newlyOfferingOndCount = -1;
		List<String> modifiedSegs = new ArrayList<String>();

		if (isValidParams(ondCode, modifiedOndCode, newlyOfferingOnds, journeyOndSummary)) {
			newlyOfferingOndCount = newlyOfferingOnds.size();
			modifiedSegs = preferenceAnalyzer.getModifiedSegCodes();
			int offeringOndSeqs = journeyOndSummary.size();

			if (isSameRouteModification(ondCode, modifiedOndCode, modifiedSegs, newlyOfferingOnds, journeyOndSummary)) {
				log.info("BL-1");
				return false; // 1-BL
			} else {
				if (isSingleSegModification(modifiedSegs)) {
					// single seg modification
					if (offeringOndSeqs == 1) {
						if (newlyOfferingOndCount == 1) {
							log.info("BL-2");
							return false;
						}
						if (newlyOfferingOndCount > 1) {
							log.info("BL-3");
							return false;
						}
					} else if (isReturnRoutePossible(journeyOndSummary)) {
						log.info("BL-4");
						return true;
					}
				} else {
					// connection modification
					log.info("BL-5");
					preferenceAnalyzer.setOldFareId(null);
					return true; // 5-BL
				}
			}

		}

		preferenceAnalyzer.setOldFareId(null);
		return true; // 6-BL
	}

	private boolean isSingleSegModification(List<String> modifiedSegs) {
		boolean singleSegModification = false;
		boolean isSingleSegOrMultileg = (modifiedSegs.size() == 1);
		if (isSingleSegOrMultileg) {
			String modifiedSingleOnd = modifiedSegs.get(0);
			singleSegModification = (modifiedSingleOnd.length() > 7) ? false : true;
		}

		return singleSegModification;
	}

	private boolean isValidParams(String ondCode, String modifiedOndCode, List<String> journeyOnds,
			List<List<String>> journeyOndSummary) {
		return (ondCode != null && !ondCode.isEmpty() && modifiedOndCode != null && !modifiedOndCode.isEmpty()
				&& journeyOnds != null && !journeyOnds.isEmpty() && journeyOndSummary != null && !journeyOndSummary.isEmpty());
	}

	private boolean isSameRouteModification(String ondCode, String modifiedOndCode, List<String> modifiedSegs,
			List<String> newlyOfferingOnds, List<List<String>> journeyOndSummary) {
		boolean isSameRoute = false;
		int journeySequencesInJourney = journeyOndSummary.size();
		int newlyOfferingOndCount = newlyOfferingOnds.size();

		if (isSingleSegModification(modifiedSegs)) {
			if (journeySequencesInJourney == 1 && newlyOfferingOndCount == 1) {
				if (modifiedOndCode.equals(ondCode)) {
					isSameRoute = true;
				}
			}
		} else {
			// connection modifications or multi-leg modification
			if (journeySequencesInJourney == 1) {
				String offeringOnd = SegmentUtil.getOndCode(newlyOfferingOnds);
				if (modifiedOndCode.equals(offeringOnd)) {
					isSameRoute = true;
				}
			}
		}

		return isSameRoute;

	}

	private boolean isReturnRoutePossible(List<List<String>> journeyOndSummary) {
		StringBuilder segBuilder = new StringBuilder();
		for (List<String> journeyOnds : journeyOndSummary) {
			Iterator<String> it = journeyOnds.iterator();
			while (it.hasNext()) {
				segBuilder.append(it.next());
			}
		}

		String segmentCode = segBuilder.toString();
		String arr[] = segmentCode.split(OND_DELIM);

		String origin = arr[0];
		String destination = arr[arr.length - 1];

		return origin.equals(destination);
	}

}
