/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

/**
 * @author dev
 *
 */

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentSageDTO;
import com.isa.thinair.airreservation.api.model.CreditCardSalesSage;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;

public interface CreditSalesTransferDAO extends ExternalSystemDAO {

	public void clearCreditCardSalesTable(Date date);

	public Collection<CreditCardSalesSage> insertCreditCardSalesToInternal(Collection<CreditCardPaymentSageDTO> col);

	public void updateInternalCreditTable(Collection<CreditCardSalesSage> col);

	public Collection<CreditCardPaymentSageDTO> getCreditCardPayments(Date date);

	public void insertCreditCardSalesToExternal(Collection<CreditCardPaymentSageDTO> col, AccontingSystemTemplate template)
			throws ClassNotFoundException, SQLException, Exception;
}
