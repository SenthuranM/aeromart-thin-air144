package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ChargeRateDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Business Object to process any extra charges added to the reservation apart from Cancellation or Modification
 * charges.
 * 
 * @author Nilindra Fernando
 * @since August 16, 2012
 */
public class ExtraFeeBO {

	private Reservation reservation;

	private Boolean isJNTaxApplicable;

	private ExternalChgDTO externalChgDTO;

	private Collection<Integer> newFltSegIds;

	public ExtraFeeBO(Reservation reservation, Collection<Integer> newFltSegIds) {
		this.reservation = reservation;
		this.newFltSegIds = newFltSegIds;
	}

	public BigDecimal getFee(BigDecimal identifiedCharge, String cabinClassCode) throws ModuleException {
		BigDecimal totalJNTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (isExtraFeeApplicable()) {
			totalJNTaxAmount = AccelAeroCalculator.add(totalJNTaxAmount,
					AccelAeroCalculator.multiplyDefaultScale(identifiedCharge, getRatio(cabinClassCode)));
		}

		return totalJNTaxAmount;
	}

	public BigDecimal getFee(boolean applyCnxCharge, BigDecimal identifiedCAmount, boolean applyModCharge,
			BigDecimal identifiedMAmount, String cabinClassCode) throws ModuleException {
		BigDecimal totalJNTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (isExtraFeeApplicable()) {
			if (applyCnxCharge) {
				totalJNTaxAmount = AccelAeroCalculator.add(totalJNTaxAmount,
						AccelAeroCalculator.multiplyDefaultScale(identifiedCAmount, getRatio(cabinClassCode)));
			}

			if (applyModCharge) {
				totalJNTaxAmount = AccelAeroCalculator.add(totalJNTaxAmount,
						AccelAeroCalculator.multiplyDefaultScale(identifiedMAmount, getRatio(cabinClassCode)));
			}
		}

		return totalJNTaxAmount;
	}

	public boolean isExtraFeeApplicable() throws ModuleException {
		if (isJNTaxApplicable == null) {
			isJNTaxApplicable = isJNTaxApplicable();
		}

		return isJNTaxApplicable;
	}

	private BigDecimal getRatio(String cabinClassCode) throws ModuleException {
		if (cabinClassCode != null && !getExternalChgDTO().getChargeRates().isEmpty()) {
			for (ChargeRateDTO chargeRateDTO : getExternalChgDTO().getChargeRates()) {
				if (cabinClassCode.equals(chargeRateDTO.getCabinClassCode())) {
					return chargeRateDTO.getRatioValue();
				}
			}
		}
		return getExternalChgDTO().getRatioValue();
	}

	public Collection<Integer> getChargeRateIds() throws ModuleException {
		Collection<Integer> colChargeRateIds = new ArrayList<Integer>();
		if (isExtraFeeApplicable()) {
			colChargeRateIds.add(getExternalChgDTO().getChgRateId());
		}
		return colChargeRateIds;
	}

	public ExternalChgDTO getExternalChgDTO() throws ModuleException {
		if (externalChgDTO == null) {
			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_TAX);

			externalChgDTO = BeanUtils.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null)
					.values());
		}

		return externalChgDTO;
	}

	private boolean isJNTaxApplicable() throws ModuleException {
		if (AppSysParamsUtil.isJNTaxApplyForCnxModNoShowCharges()) {
			String firstSegmentCode = null;
			if (newFltSegIds != null && newFltSegIds.size() > 0) {
				Collection<FlightSegmentDTO> flightSegmentDetails = ReservationModuleUtils.getFlightBD().getFlightSegments(
						newFltSegIds);

				FlightSegmentDTO[] sortedFlightSegments = ReservationApiUtils.sortFlightSegmentByDepDate(flightSegmentDetails);
				// TODO: pnrSegmentStatus,flightSegmentStatus,flightStatus
				for (FlightSegmentDTO flightSegmentDTO : sortedFlightSegments) {
					if (flightSegmentDTO.getSegmentCode() != null) {
						firstSegmentCode = flightSegmentDTO.getSegmentCode();
						break;
					}
				}

			} else {
				Map<Integer, ReservationSegmentDTO> segmentSeqOrderedMap = new TreeMap<Integer, ReservationSegmentDTO>();
				for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
					segmentSeqOrderedMap.put(reservationSegmentDTO.getSegmentSeq(), reservationSegmentDTO);
				}

				ReservationSegmentDTO reservationSegmentDTO = getFirstConfirmedSegment(segmentSeqOrderedMap.values());
				if (reservationSegmentDTO != null) {
					firstSegmentCode = reservationSegmentDTO.getSegmentCode();
				}

			}

			if (firstSegmentCode != null) {
				return ReservationModuleUtils.getChargeBD().isValidChargeExists(getExternalChgDTO().getChargeCode(),
						firstSegmentCode);
			}
		}

		return false;
	}

	private ReservationSegmentDTO getFirstConfirmedSegment(Collection<ReservationSegmentDTO> colReservationSegmentDTO) {
		for (ReservationSegmentDTO reservationSegmentDTO : colReservationSegmentDTO) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {
				return reservationSegmentDTO;
			}
		}

		return null;
	}

}
