package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ModifySegmentIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public ModifySegmentIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	private RuleExecuterDatacontext dataContext;

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {

		this.dataContext = dataContext;

		RuleResponseDTO response = getResponseDTO();

		boolean isApplicable;

		// modify reservation
		isApplicable = dataContext.getExisitingReservation().getSegments().size() != dataContext.getUpdatedReservation().getSegments().size();

		// flight re-protect
		if (!isApplicable) {
			Collection<Integer> existingFlightSegIds = getUncancelledFlightSegIds(dataContext.getExisitingReservation());
			Collection<Integer> updateFlightSegIds = getUncancelledFlightSegIds(dataContext.getUpdatedReservation());

			isApplicable = !(existingFlightSegIds.containsAll(updateFlightSegIds) && updateFlightSegIds.containsAll(existingFlightSegIds));

		}

		if (isApplicable) {

			updatePaxWiseAffectedSegmentListToAllGivenPax(response, getCreatedSegIDsFromFrocess(), getCancelSegIDsFromProcess(),
					getUncancelledPaxIds(dataContext.getUpdatedReservation()));

		}

		return response;
	}

	private List<Integer> getCancelSegIDsFromProcess() {

		List<Integer> cancelSegIDsFromProcess = new ArrayList<Integer>();

		for (ReservationSegment existSeg : dataContext.getExisitingReservation().getSegments()) {
			if (!isCNXSegment(existSeg)) {
				for (ReservationSegment updatedSeg : dataContext.getUpdatedReservation().getSegments()) {
					if (existSeg.getPnrSegId().equals(updatedSeg.getPnrSegId()) && isCNXSegment(updatedSeg)
							&& !isExchangeSeg(updatedSeg)) {
						cancelSegIDsFromProcess.add(updatedSeg.getPnrSegId());
					}
				}
			}
		}
		return cancelSegIDsFromProcess;
	}

	private List<Integer> getCreatedSegIDsFromFrocess() {

		List<Integer> newSegIDsFromFrocess = new ArrayList<Integer>();

		Set<Integer> existResSegids;

		if (dataContext.getExisitingReservation() != null) {
			existResSegids = (Set<Integer>) dataContext.getExisitingReservation().getPnrSegIds();
		} else {
			existResSegids = new HashSet<>();
		}

		for (ReservationSegment seg : dataContext.getUpdatedReservation().getSegments()) {
			if (!isCNXSegment(seg) && !existResSegids.contains(seg)) {
				newSegIDsFromFrocess.add(seg.getPnrSegId());
			}
		}
		return newSegIDsFromFrocess;
	}
}
