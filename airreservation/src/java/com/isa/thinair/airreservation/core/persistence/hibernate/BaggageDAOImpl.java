package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.AnciTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.BaggageDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * BaggageDAOImpl is the business DAO hibernate implementation
 * 
 * @author mano
 * @since 1.0
 * @isa.module.dao-impl dao-name="BaggageDAO"
 */
public class BaggageDAOImpl extends PlatformBaseHibernateDaoSupport implements BaggageDAO {

	@Override
	public Collection<PassengerBaggage> getFlightBaggagesForPnrPax(Collection<Integer> pnrPaxIds) {

		Collection<PassengerBaggage> passengerbaggages = new ArrayList<PassengerBaggage>();
		String hql = "from PassengerBaggage s where s.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED
				+ "' and s.pnrPaxId in (" + Util.buildIntegerInClauseContent(pnrPaxIds) + ")";
		passengerbaggages = find(hql, PassengerBaggage.class);
		return passengerbaggages;
	}

	@Override
	public void saveOrUpdate(Collection<PassengerBaggage> baggages) {

		super.hibernateSaveOrUpdateAll(baggages);

	}

	@SuppressWarnings("unchecked")
	public Collection<PaxBaggageTO> getReservationBaggages(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			boolean isOndBaggage, String preferredLanguage) {
		if (preferredLanguage == null || "".equals(preferredLanguage.trim())) {
			preferredLanguage = "en";
		}
		String sql = " select ppbg.pnr_pax_seg_baggage_id, ppbg.pnr_pax_id, ppbg.pnr_seg_id, ppbg.amount, ppbg.FLT_SEG_BAGGAGE_CHARGE_ID,"
				+ "ppbg.baggage_id, ppbg.ppf_id, bg.baggage_name, bg.baggage_desc, seg.baggage_ond_group_id, ppbg.auto_cancellation_id, "
				+ (isOndBaggage ? " ppbg.ond_baggage_charge_id as baggage_charge_id " : " bc.baggage_charge_id ")
				+ " ,msg.message_content "
				+ " from bg_t_pnr_pax_seg_baggage ppbg, "
				+ " bg_t_baggage bg, t_pnr_segment seg, (select * from t_i18n_message where message_locale  = '"
				+ preferredLanguage
				+ "')  msg"
				+ (isOndBaggage ? "" : ", bg_t_baggage_charge bc, bg_t_flt_seg_baggage_charge fs")
				+ " where ppbg.baggage_id = bg.baggage_id "
				+ (isOndBaggage
						? ""
						: " AND bc.baggage_id = bg.baggage_id AND ppbg.flt_seg_baggage_charge_id = fs.flt_seg_baggage_charge_id AND fs.baggage_charge_id = bc.baggage_charge_id")
				+ " AND ppbg.pnr_seg_id = seg.pnr_seg_id"
				+ " AND ppbg.status = '"
				+ AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED
				+ "'"
				+ " AND bg.i18n_message_key = msg.i18n_message_key(+) "
				+ " AND ppbg.pnr_seg_id in ("
				+ Util.constructINStringForInts(pnrSegIds)
				+ ") "
				+ " AND ppbg.pnr_pax_id in ("
				+ Util.constructINStringForInts(pnrPaxIds) + ")" + " Order by ppbg.pnr_seg_id";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<PaxBaggageTO> colBaggages = (Collection<PaxBaggageTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<PaxBaggageTO> passengerbaggages = new ArrayList<PaxBaggageTO>();
				while (rs.next()) {
					PaxBaggageTO paxbaggageTO = new PaxBaggageTO();
					paxbaggageTO.setSelectedFlightBaggageId(new Integer(rs.getInt("FLT_SEG_BAGGAGE_CHARGE_ID")));
					paxbaggageTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
					paxbaggageTO.setPnrSegId(new Integer(rs.getInt("pnr_seg_id")));
					paxbaggageTO.setBaggageName(rs.getString("baggage_name"));
					paxbaggageTO.setBaggageDescription(rs.getString("baggage_desc"));
					paxbaggageTO.setPkey(new Integer(rs.getInt("pnr_pax_seg_baggage_id")));
					paxbaggageTO.setPnrPaxFareId(new Integer(rs.getInt("ppf_id")));
					paxbaggageTO.setBaggageId(new Integer(rs.getInt("baggage_id")));
					paxbaggageTO.setBaggageOndGroupId(new Integer(rs.getInt("baggage_ond_group_id")).toString());
					paxbaggageTO.setBaggageChrgId(new Integer(rs.getInt("baggage_charge_id")));
					if (null != rs.getClob("message_content")) {
						paxbaggageTO.setTranslatedBaggageDescription(StringUtil.clobToString(rs.getClob("message_content")));
					}
					BaggageExternalChgDTO externalChgDTO = new BaggageExternalChgDTO();
					externalChgDTO.setAmount(rs.getBigDecimal("amount"));
					paxbaggageTO.setChgDTO(externalChgDTO);

					if (AppSysParamsUtil.isAutoCancellationEnabled() && rs.getInt("auto_cancellation_id") != 0) {
						paxbaggageTO.setAutoCancellationId(rs.getInt("auto_cancellation_id"));
					}

					passengerbaggages.add(paxbaggageTO);
				}
				return passengerbaggages;
			}
		});

		return colBaggages;

	}

	@SuppressWarnings("unchecked")
	public Map<Integer, String> getBaggageDetailNames(Collection<Integer> flightBaggageIds) {
		StringBuilder sql = new StringBuilder();
		sql.append(" select bg.baggage_name, bg.baggage_id ");
		sql.append(" from bg_t_baggage bg ");
		sql.append(" WHERE ");
		sql.append(" bg.baggage_id in (" + Util.constructINStringForInts(flightBaggageIds) + ")");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> mapBaggages = (Map<Integer, String>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> mapPaxBaggageNames = new HashMap<Integer, String>();
				while (rs.next()) {
					mapPaxBaggageNames.put(rs.getInt("baggage_id"), rs.getString("baggage_name"));
				}
				return mapPaxBaggageNames;
			}
		});

		return mapBaggages;

	}

	public Collection<PassengerBaggage> getFlightBaggagesForPnrPaxFare(Collection<Integer> ppfIds) {
		Collection<PassengerBaggage> passengerBaggages = new ArrayList<PassengerBaggage>();
		String hql = "from PassengerBaggage s where s.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED
				+ "' and s.pPFID in (" + Util.buildIntegerInClauseContent(ppfIds) + ")";
		passengerBaggages = find(hql, PassengerBaggage.class);
		return passengerBaggages;

	}

	/*
	 * Method send baggage Details to Pnl
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getReservationBaggagesForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			final String ssrCode) {

		Map<String, String> tmpWeightIndexMap = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from (");
		sql.append(" ( select ppbg.pnr_pax_id, bg.iata_code, bg.baggage_name, bg.baggage_weight ");
		sql.append(" from bg_t_pnr_pax_seg_baggage ppbg,");
		sql.append("bg_t_ond_baggage_charge obg,bg_t_baggage bg ");
		sql.append(" where ppbg.ond_baggage_charge_id = obg.ond_baggage_charge_id ");
		sql.append(" and obg.baggage_id = bg.baggage_id ");
		sql.append(" and ppbg.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED + "'");
		sql.append(" and ppbg.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		sql.append(" and ppbg.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + "))");
		sql.append(" union ");
		sql.append(" ( select ppbg.pnr_pax_id, bg.iata_code, bg.baggage_name, bg.baggage_weight ");
		sql.append(" from bg_t_pnr_pax_seg_baggage ppbg,");
		sql.append(" bg_t_flt_seg_baggage_charge fbg, bg_t_baggage_charge bgc, bg_t_baggage bg ");
		sql.append(" where ppbg.flt_seg_baggage_charge_id = fbg.flt_seg_baggage_charge_id ");
		sql.append(" and fbg.baggage_charge_id = bgc.baggage_charge_id ");
		sql.append(" and bgc.baggage_id = bg.baggage_id ");
		sql.append(" and ppbg.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED + "'");
		sql.append(" and ppbg.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		sql.append(" and ppbg.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + "))");
		sql.append(" ) order by pnr_pax_id, baggage_weight,baggage_name ");

		// to load baggage weight, index map if ssrCode= BAG
		if (!"XBAG".equals(ssrCode)) {
			tmpWeightIndexMap = ((AirReservationConfig) ReservationModuleUtils.getInstance().getModuleConfig())
					.getBaggageWeightIndexMap();
		}
		final Map<String, String> weightIndexMap = tmpWeightIndexMap;
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> pnrBaggageMap = (Map<Integer, String>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> pnrBaggageMap = new HashMap<Integer, String>();

				while (rs.next()) {
					Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
					String paxBaggageName = "";
					if (null != ssrCode) {
						paxBaggageName = ssrCode;
						// String baggageName = rs.getString("baggage_name");
						String baggageWeight = rs.getString("baggage_weight");
						if ("XBAG".equals(ssrCode)) {
							paxBaggageName += "%" + "BAGS " + baggageWeight;
						} else if ("BAG".equals(ssrCode) && null != weightIndexMap) {
							String weightIndex = weightIndexMap.get(baggageWeight.toString()) != null ? weightIndexMap.get(
									baggageWeight.toString()).toString() : "0";
							paxBaggageName += weightIndex;
						}

					}

					pnrBaggageMap.put(pnrPaxId, paxBaggageName);
				}
				return pnrBaggageMap;
			}
		});

		return pnrBaggageMap;
	}

	public Map<Integer, AncillaryDTO>
			getReservationBaggagesForPNLADL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" select ppbg.pnr_pax_id, bg.iata_code, bg.baggage_name, bg.baggage_weight ");
		sql.append(" from bg_t_pnr_pax_seg_baggage ppbg, bg_t_flt_seg_baggage_charge fbg, bg_t_baggage_charge bgc, bg_t_baggage bg ");
		sql.append(" where ppbg.flt_seg_baggage_charge_id = fbg.flt_seg_baggage_charge_id ");
		sql.append(" and fbg.baggage_charge_id = bgc.baggage_charge_id ");
		sql.append(" and bgc.baggage_id = bg.baggage_id ");
		sql.append(" and ppbg.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED + "'");
		sql.append(" and ppbg.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		sql.append(" and ppbg.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		sql.append(" order by ppbg.pnr_pax_id, bg.baggage_weight,bg.baggage_name ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Map<Integer, AncillaryDTO> pnrBaggageMap = (Map<Integer, AncillaryDTO>) template.query(sql.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, AncillaryDTO> pnrBaggageMap = new HashMap<Integer, AncillaryDTO>();

						while (rs.next()) {
							Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
							if (pnrBaggageMap.get(pnrPaxId) == null) {

								AncillaryDTO anci = new AncillaryDTO();
								anci.setAnciType(AnciTypes.BAGGAGE);
								anci.setDescription(rs.getString("baggage_weight").toUpperCase());
								pnrBaggageMap.put(pnrPaxId, anci);

							}
						}
						return pnrBaggageMap;
					}
				});

		return pnrBaggageMap;
	}

	/*
	 * Method send baggage Details to Pnl
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getReservationBaggageWeightsForPNL(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" select ppbg.pnr_pax_id, bg.iata_code, bg.baggage_name, bg.baggage_weight ");
		sql.append(" from bg_t_pnr_pax_seg_baggage ppbg, bg_t_flt_seg_baggage_charge fbg, bg_t_baggage_charge bgc, bg_t_baggage bg ");
		sql.append(" where ppbg.flt_seg_baggage_charge_id = fbg.flt_seg_baggage_charge_id ");
		sql.append(" and fbg.baggage_charge_id = bgc.baggage_charge_id ");
		sql.append(" and bgc.baggage_id = bg.baggage_id ");
		sql.append(" and ppbg.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED + "'");
		sql.append(" and ppbg.pnr_seg_id in (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		sql.append(" and ppbg.pnr_pax_id in (" + Util.constructINStringForInts(pnrPaxIds) + ")");
		// sql.append(" GROUP BY ppbg.pnr_pax_id,bg.iata_code ");
		sql.append(" order by ppbg.pnr_pax_id, bg.baggage_weight,bg.baggage_name ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Map<Integer, String> pnrBaggageMap = (Map<Integer, String>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> pnrBaggageMap = new HashMap<Integer, String>();

				while (rs.next()) {
					Integer pnrPaxId = new Integer(rs.getInt("pnr_pax_id"));
					String baggageWeight = rs.getString("baggage_weight");
					pnrBaggageMap.put(pnrPaxId, baggageWeight);
				}
				return pnrBaggageMap;
			}
		});

		return pnrBaggageMap;
	}

	/*
	 * Method get baggage ssr code fro given airport
	 */
	public String getBaggageSsrCodesToSendPNL(String airportCode) {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT max(a.ssr_code) as ssr_code");
		sql.append(" FROM t_ssr_info a,t_ssr_applicability_containts b, t_ssr_applicable_airport c, t_ssr_sub_category d ");
		sql.append(" WHERE b.ssr_id = a.ssr_id ");
		sql.append(" AND b.ssr_app_con_id = c.ssr_app_con_id ");
		sql.append(" AND d.ssr_sub_cat_id = a.ssr_sub_cat_id ");
		sql.append(" AND d.ssr_sub_cat_id = 5");
		sql.append(" AND c.airport_code='" + airportCode + "' ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String ssrCode = (String) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {

					String ssrCode = rs.getString("ssr_code");
					return ssrCode;
				}
				return null;
			}
		});

		return ssrCode;
	}

	public Collection<PassengerBaggage> getPassengerBaggages(Collection<Integer> paxBaggageIds) {
		Collection<PassengerBaggage> passengerbaggages = new ArrayList<PassengerBaggage>();
		String hql = "from PassengerBaggage s where s.status = '" + AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED
				+ "' and s.paxBaggageId in (" + Util.buildIntegerInClauseContent(paxBaggageIds) + ")";
		passengerbaggages = find(hql, PassengerBaggage.class);
		return passengerbaggages;

	}

	/**
	 * Method will return template Id for particular flight segment id.
	 */
	public Integer getTemplateForSegmentId(int flightSegId) {

		String sql = "SELECT baggage_template_id FROM bg_t_baggage_template WHERE baggage_template_id "
				+ " IN( SELECT c.baggage_template_id FROM bg_t_baggage_charge c WHERE c.baggage_charge_id "
				+ " IN( SELECT b.baggage_charge_id FROM bg_t_flt_seg_baggage_charge b " + " WHERE b.flt_seg_id =" + flightSegId
				+ " and b.status = 'ACT'))";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Integer) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer templateId = null;
				while (rs.next()) {
					templateId = rs.getInt("baggage_template_id");
				}
				return templateId;
			}
		});
	}

	public Integer getONDChargeTemplateId(int pnrSegId) {

		String sql = "SELECT DISTINCT OND_CHARGE_TEMPLATE_ID FROM BG_T_PNR_PAX_SEG_BAGGAGE A, "
				+ " BG_T_OND_BAGGAGE_CHARGE B WHERE A.OND_BAGGAGE_CHARGE_ID = B.OND_BAGGAGE_CHARGE_ID "
				+ " AND A.PNR_SEG_ID = ? ";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Integer) template.query(sql, new Object[] { pnrSegId }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer templateId = null;
				while (rs.next()) {
					templateId = rs.getInt("OND_CHARGE_TEMPLATE_ID");
				}
				return templateId;
			}
		});
	}

	public Integer getONDBaggageId(long baggageChargeGroupId) {
		String sql = " SELECT DISTINCT B.BAGGAGE_ID From Bg_T_Ond_Baggage_Charge Bc, Bg_T_Baggage B "
				+ " WHERE Bc.Baggage_Id = B.Baggage_Id and Bc.Ond_Baggage_Charge_Id = ? ";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Integer) template.query(sql, new Object[] { baggageChargeGroupId }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer baggageId = null;
				if (rs.next()) {
					baggageId = rs.getInt("BAGGAGE_ID");
				}
				return baggageId;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<ReservationLiteDTO, List<PaxBaggageTO>> getExpiredBaggages(Collection<Integer> cancellationIds,
			String marketingCarrier) {
		boolean isDry = AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier) ? false : true;

		StringBuilder query = new StringBuilder();
		query.append("SELECT r.pnr, pp.pnr_pax_id, ps.pnr_seg_id, bg.ppf_id, bg.flt_seg_baggage_charge_id, bg.baggage_id, bg.amount charge, r.version, bg.auto_cancellation_id");
		query.append(" FROM t_reservation r, t_pnr_passenger pp, t_pnr_segment ps,  bg_t_pnr_pax_seg_baggage bg");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id = bg.pnr_pax_id AND ps.pnr_seg_id = bg.pnr_seg_id");
		query.append(" AND r.status = ? AND pp.status = ? AND ps.status =? AND bg.status <>? AND r.DUMMY_BOOKING = ? AND ");
		query.append(Util.getReplaceStringForIN("bg.auto_cancellation_id", cancellationIds));
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.ORIGIN_CHANNEL_CODE ");
		if (isDry) {
			query.append("= ?");
		} else {
			query.append("<> ?");
		}

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), marketingCarrier,
				ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Map<ReservationLiteDTO, List<PaxBaggageTO>>) template.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<ReservationLiteDTO, List<PaxBaggageTO>> pnrWiseBaggages = null;
				if (rs != null) {
					pnrWiseBaggages = new HashMap<ReservationLiteDTO, List<PaxBaggageTO>>();
					while (rs.next()) {
						String pnr = rs.getString("pnr");
						long version = rs.getLong("version");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");

						ReservationLiteDTO resDTO = new ReservationLiteDTO(pnr, version, autoCancellationId);

						if (!pnrWiseBaggages.containsKey(resDTO)) {
							pnrWiseBaggages.put(resDTO, new ArrayList<PaxBaggageTO>());
						}

						PaxBaggageTO paxBaggageTO = new PaxBaggageTO();
						paxBaggageTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
						paxBaggageTO.setPnrSegId(rs.getInt("pnr_seg_id"));
						paxBaggageTO.setPnrPaxFareId(rs.getInt("ppf_id"));
						paxBaggageTO.setSelectedFlightBaggageId(rs.getInt("flt_seg_baggage_charge_id"));
						paxBaggageTO.setBaggageId(rs.getInt("baggage_id"));
						paxBaggageTO.setChgDTO(new ExternalChgDTO());
						paxBaggageTO.getChgDTO().setAmount(rs.getBigDecimal("charge"));
						pnrWiseBaggages.get(resDTO).add(paxBaggageTO);

					}
				}
				return pnrWiseBaggages;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccBaggagesInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, bg.auto_cancellation_id");
		query.append(" FROM t_reservation r, t_pnr_passenger pp, t_pnr_segment ps,  bg_t_pnr_pax_seg_baggage bg");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id = bg.pnr_pax_id AND ps.pnr_seg_id = bg.pnr_seg_id");
		query.append(" AND r.status = ? AND pp.status = ? AND ps.status =? AND bg.status <>? AND r.ORIGIN_CHANNEL_CODE = ? ");
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.DUMMY_BOOKING = ? AND bg.auto_cancellation_id IN (");
		query.append(BeanUtils.constructINStringForInts(cancellationIds) + ")");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL, ReservationInternalConstants.SalesChannel.LCC,
				marketingCarrier, String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) template.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						autoCnxPNRs.put(pnr, autoCnxId);
					}
				}

				return autoCnxPNRs;
			}
		});
	}
	
	public Integer getOndBaggageChargeTemplateId(Integer ondBaggageChargeId){
		StringBuilder query = new StringBuilder();
		query.append("SELECT OND_CHARGE_TEMPLATE_ID FROM BG_T_OND_BAGGAGE_CHARGE WHERE OND_BAGGAGE_CHARGE_ID = ?");
		Object[] args = new Object[] {ondBaggageChargeId} ;
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		
		return (Integer)template.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer ondBaggageChargeTemplateId = null;
				while (rs.next()) {
					ondBaggageChargeTemplateId = rs.getInt("OND_CHARGE_TEMPLATE_ID");
				}
				return ondBaggageChargeTemplateId;
			}
		});		
	}
}
