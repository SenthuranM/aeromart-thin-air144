/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext;

import java.util.Map;

/**
 * @author udithad
 *
 */
public class PnrElementRuleContext extends RulesDataContext {
	private Map<String, Integer> pnrWisePassengerCount;
	private Map<String, Integer> pnrWisePaxReductionCout;
	private Map<String, String> pnrWiseGroupCodes;
	private Integer utilizedPassengerCount;
	private String pnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<String, Integer> getPnrWisePassengerCount() {
		return pnrWisePassengerCount;
	}

	public void setPnrWisePassengerCount(
			Map<String, Integer> pnrWisePassengerCount) {
		this.pnrWisePassengerCount = pnrWisePassengerCount;
	}

	public Map<String, Integer> getPnrWisePaxReductionCout() {
		return pnrWisePaxReductionCout;
	}

	public void setPnrWisePaxReductionCout(
			Map<String, Integer> pnrWisePaxReductionCout) {
		this.pnrWisePaxReductionCout = pnrWisePaxReductionCout;
	}

	public Map<String, String> getPnrWiseGroupCodes() {
		return pnrWiseGroupCodes;
	}

	public void setPnrWiseGroupCodes(Map<String, String> pnrWiseGroupCodes) {
		this.pnrWiseGroupCodes = pnrWiseGroupCodes;
	}

	public Integer getUtilizedPassengerCount() {
		return utilizedPassengerCount;
	}

	public void setUtilizedPassengerCount(Integer utilizedPassengerCount) {
		this.utilizedPassengerCount = utilizedPassengerCount;
	}

}
