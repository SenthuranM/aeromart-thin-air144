/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.DestinationFareContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.PassengerElementsContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;

/**
 * @author udithad
 *
 */
public class DestinationBulkElementBuilder extends BaseElementBuilder {

	private DestinationFareContext pEContext;
	private PassengerElementsContext passengerElementsContext = null;
	private List<DestinationFare> destinationFaresList;
	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String messageType;

	@Override
	public void buildElement(ElementContext context) {
		initContextData(context);
		createDestinationWisePassengerElements();
	}

	private void initContextData(ElementContext context) {
		pEContext = (DestinationFareContext) context;
		passengerElementsContext = new PassengerElementsContext();
		destinationFaresList = pEContext.getDestinationFareElement();
		currentLine = pEContext.getCurrentMessageLine();
		messageLine = pEContext.getMessageString();
		messageType = pEContext.getMessageType();

		passengerElementsContext.setCurrentMessageLine(context.getCurrentMessageLine());
		passengerElementsContext.setMessageString(context.getMessageString());
		passengerElementsContext.setLastGroupCode(context.getLastGroupCode());
		passengerElementsContext.setMessageType(context.getMessageType());		
		passengerElementsContext.setPnrWisePassengerCount(context
				.getPnrWisePassengerCount());
		passengerElementsContext.setPnrWisePassengerReductionCount(context
				.getPnrWisePassengerReductionCount());
		passengerElementsContext.setGroupCodesList(context.getGroupCodesList());
		passengerElementsContext.setPnrWiseGroupCodes(context.getPnrWiseGroupCodes());
		passengerElementsContext.setFeaturePack(context.getFeaturePack());	
		passengerElementsContext.setPartNumbers(context.getPartNumbers());
		passengerElementsContext.setLastGroupCode(context.getLastGroupCode());
		passengerElementsContext.setPartCountExceeded(context.isPartCountExceeded());
		passengerElementsContext.setPassengersPerPart(context.getPassengersPerPart());
		passengerElementsContext.setPnrPaxVsGroupCodes(context.getPnrPaxVsGroupCodes());
		passengerElementsContext.setDepartureAirportCode(context.getDepartureAirportCode());
		passengerElementsContext.setGroupCodeAvailabilityMap(context.getGroupCodeAvailabilityMap());
	}

	private void createDestinationWisePassengerElements() {

		for (DestinationFare destinationFare : destinationFaresList) {
			passengerElementsContext.setDestinationFare(destinationFare);
			executeNext();
			// End it will reach here\
			int i = 0;
			setLastGroupCode();
			setPartCountExceededStatus();
			if(passengerElementsContext.isPartCountExceeded()){
				return;
			}
		}
	}

	private void setLastGroupCode(){
		pEContext.setLastGroupCode(passengerElementsContext.getLastGroupCode());
	}
	
	private void setPartCountExceededStatus(){
		pEContext.setPartCountExceeded(passengerElementsContext.isPartCountExceeded());
	}
	
	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(passengerElementsContext);
		}
	}

}
