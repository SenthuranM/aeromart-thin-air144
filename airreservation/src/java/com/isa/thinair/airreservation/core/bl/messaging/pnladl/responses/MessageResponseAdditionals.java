/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.responses;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

/**
 * @author udithad
 *
 */
public class MessageResponseAdditionals {

	private Map<Integer, Integer> pnrPaxIdvsSegmentIds;
	private HashMap<String, Integer> fareClassWisePaxCount;
	private List<String> pnrCollection;
	private Map<Integer, String> pnrPaxVsGroupCodes;
	private String lastGroupCode;
	private List<PassengerInformation> passengerInformations;

	public Map<Integer, Integer> getPnrPaxIdvsSegmentIds() {
		return pnrPaxIdvsSegmentIds;
	}

	public void setPnrPaxIdvsSegmentIds(
			Map<Integer, Integer> pnrPaxIdvsSegmentIds) {
		this.pnrPaxIdvsSegmentIds = pnrPaxIdvsSegmentIds;
	}

	public HashMap<String, Integer> getFareClassWisePaxCount() {
		return fareClassWisePaxCount;
	}

	public void setFareClassWisePaxCount(
			HashMap<String, Integer> fareClassWisePaxCount) {
		this.fareClassWisePaxCount = fareClassWisePaxCount;
	}

	public List<String> getPnrCollection() {
		return pnrCollection;
	}

	public void setPnrCollection(List<String> pnrCollection) {
		this.pnrCollection = pnrCollection;
	}

	public Map<Integer, String> getPnrPaxVsGroupCodes() {
		return pnrPaxVsGroupCodes;
	}

	public void setPnrPaxVsGroupCodes(Map<Integer, String> pnrPaxVsGroupCodes) {
		this.pnrPaxVsGroupCodes = pnrPaxVsGroupCodes;
	}

	public String getLastGroupCode() {
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

	public List<PassengerInformation> getPassengerInformations() {
		return passengerInformations;
	}

	public void setPassengerInformations(
			List<PassengerInformation> passengerInformations) {
		this.passengerInformations = passengerInformations;
	}

}
