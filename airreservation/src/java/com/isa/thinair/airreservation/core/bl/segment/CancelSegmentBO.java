/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.security.UserPrincipal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.SeatDistForRelease;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.SeatsForSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.revacc.RefundablesMetaInfoTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.FeeCalculator;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.revacc.RecordCancelBO;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityReservationUtils;
import com.isa.thinair.airreservation.core.util.DiscountHandlerUtil;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Cancel segment business object
 * 
 * Notes: Supports per OND cancellation which means at a given time calling this method is for a CONFIRMED OND and per
 * passenger only
 * <p>
 * Business Rules: <br />
 * (1) Cancellation charges applies for per passenger per fare (OND). <br />
 * (2) Any infant will not have any cancellation or modification charges. <br />
 * (3) All adult/infant charges are refundable based on refundable or not <br />
 * (4) If it's refundable there will be record(s) written to ReservationPaxOndCharge level <br />
 * (5) Custom cancellation charge is for an ond when writing refundble records to ReservationPaxOndCharge level
 * corresponding fare Id or charge rate Id will be removed and the refundable amount will be written as minus values
 * </p>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CancelSegmentBO {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CancelSegmentBO.class);

	/** Holds the revenue accounting delegate */
	// private RevenueAccountBD revenueAccountBD;

	/** Holds the fare delegate */
	private final FareBD fareBD;

	/**
	 * Holds whether this is a passenger cancellation or air line cancellation
	 */
	private Boolean isPaxCancel;

	/** Holds the custom charges TO */
	private CustomChargesTO customChargesTO;

	/** Holds the credentials information */
	private CredentialsDTO credentialsDTO;

	private boolean isCancelChgOperation;

	// private boolean isModifyChgOperation;

	private boolean hasExternalCarrierPayments;

	private boolean utilizeAllRefundables;

	/** Holds a collection of seats for segment in order to release later */
	private Collection<SeatsForSegmentDTO> adultOrChildOrParentColSeatsForSegmentDTO;

	/** Holds a collection of infant only seats for segment in order to release later */
	private Collection<SeatsForSegmentDTO> infantOnlyColSeatsForSegmentDTO;

	/** Holds infant only booking class map */
	private Map<Integer, String> infantOnlyBCMap;

	/** Holds remove infant's parent pax fare id(s) */
	private Collection<Integer> removeInfantParentPaxFareIds;

	/** Holds per passenger wise revenue */
	private final Map<Integer, RevenueDTO> mapPerPaxWiseRevenue;

	private Collection<Integer> bcNonChangedExgFltSegIds;

	private boolean removeAgentCommission = false;

	private Set<Integer> commissionRemovalExcludedFareIds;

	private Set<Integer> removedPnrPaxFareIds = null;

	private boolean infantPaymentSeparated;

	private TrackInfoDTO trackInfoDTO;

	private UserPrincipal userPrincipal;

	/** Cancel Segment BO constructing delegates */
	private CancelSegmentBO() {
		// revenueAccountBD = ReservationModuleUtils.getRevenueAccountBD();

		fareBD = ReservationModuleUtils.getFareBD();

		mapPerPaxWiseRevenue = new HashMap<Integer, RevenueDTO>();
		removedPnrPaxFareIds = new HashSet<Integer>();
	}

	/**
	 * Cancel Segment BO constructing delegates
	 * 
	 * @param isPaxCancel
	 * @param customChargesTO
	 * @param utilizedAllRefundables
	 * @param hasExternalCarrierPayments
	 * @param credentialsDTO
	 * @param bcNonChangedExgFltSegIds
	 * @param infantPaymentSeparated
	 *            TODO
	 * @throws ModuleException
	 */
	public CancelSegmentBO(Boolean isPaxCancel, CustomChargesTO customChargesTO, Boolean isCancelChgOperation,
			Boolean isModifyChgOperation, Boolean utilizedAllRefundables, Boolean hasExternalCarrierPayments,
			CredentialsDTO credentialsDTO, Collection<Integer> bcNonChangedExgFltSegIds, Boolean removeAgentCommission,
			Set<Integer> commissionRemovalExcludedFareIds, boolean infantPaymentSeparated) throws ModuleException {
		this();

		if (isPaxCancel == null || customChargesTO == null || isCancelChgOperation == null || isModifyChgOperation == null
				|| utilizedAllRefundables == null || hasExternalCarrierPayments == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		this.isPaxCancel = isPaxCancel;
		this.customChargesTO = customChargesTO;
		this.isCancelChgOperation = isCancelChgOperation;
		// this.isModifyChgOperation = isModifyChgOperation;
		this.utilizeAllRefundables = utilizedAllRefundables;
		this.hasExternalCarrierPayments = hasExternalCarrierPayments;
		this.credentialsDTO = credentialsDTO;
		this.bcNonChangedExgFltSegIds = bcNonChangedExgFltSegIds;
		this.removeAgentCommission = removeAgentCommission;
		this.commissionRemovalExcludedFareIds = commissionRemovalExcludedFareIds;
		this.infantPaymentSeparated = infantPaymentSeparated;

		if (isCancelChgOperation.booleanValue() && isModifyChgOperation.booleanValue()) {
			throw new ModuleException("airreservations.arg.invalidFatalCancelSegment");
		}

	}

	/**
	 * Process credit per passenger
	 * 
	 * NOTE: Make sure you always pass confirmed ond(s)
	 * 
	 * @param reservationPax
	 * @param reservationPaxFareAdult
	 * @param mapRevenueFares
	 * @param showCnxCharge
	 * @param showModCharge
	 * @param isExchanged
	 * @param modOndPosition
	 * @param isApplyCustomCharge
	 *            TODO
	 * @param chgTnxGen
	 * @return
	 * @throws ModuleException
	 */
	private void processPassengerCreditPerOnd(ReservationPax reservationPax, ReservationPaxFare reservationPaxFareAdult,
			Map<Integer, RevenueDTO> mapRevenueFares, boolean applyCnxCharge, boolean applyModCharge,
			boolean isSameFlightsModification, boolean isVoidReservation, FlownAssitUnit flownAsstUnit, boolean isExchanged,
			boolean isFareAdjustmentValid, boolean isTargetFare, int modOndPosition, boolean isApplyCustomCharge,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {
		log.debug("Inside getPassengerCreditPerOnd");

		Collection<Integer> fareIds = new ArrayList<Integer>();
		Collection<Integer> chgRateIds = new ArrayList<Integer>();
		ReservationPaxFare reservationPaxFareInfant = null;
		Date executionDate = new Date();

		boolean isParent = ReservationApiUtils.isParentAfterSave(reservationPax);
		boolean isCancelFlightsExists = CancellationUtils.isAnyCancelFlightsExists(reservationPaxFareAdult);

		// Parent
		if (isParent && !infantPaymentSeparated) {
			Collection<Integer> parentFareSegIds = this.getFareSegmentIds(reservationPaxFareAdult);
			// Locate the corresponding infant ond
			reservationPaxFareInfant = this.getInfantReservationPaxFare(reservationPax.getInfants(), parentFareSegIds);
			this.getFareIdsAndChargeIds(reservationPaxFareAdult, fareIds, chgRateIds);
			this.getFareIdsAndChargeIds(reservationPaxFareInfant, fareIds, chgRateIds);
		}
		// Adult / Child
		else {
			this.getFareIdsAndChargeIds(reservationPaxFareAdult, fareIds, chgRateIds);
		}

		Collection<Integer> newFltSegIds = null;
		Collection<OndFareDTO> ondFares = null;
		if (flownAsstUnit != null) {
			newFltSegIds = flownAsstUnit.getNewFltSegIds();
			ondFares = flownAsstUnit.getOndFares();
		}

		ExtraFeeBO extraFeeBO = new ExtraFeeBO(reservationPax.getReservation(), newFltSegIds);
		ServiceFeeBO serviceFeeBO = new ServiceFeeBO(reservationPax.getReservation(), getComposedUserPrincipal(), ondFares);

		Boolean isUserModification = null;

		if (flownAsstUnit != null && flownAsstUnit.getModifyAsst() != null) {
			isUserModification = flownAsstUnit.getModifyAsst().isUserModification();
		} else {
			isUserModification = applyModCharge;
		}
		// Collect fares and charges information
		FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(fareIds, chgRateIds, null);
		Collection<String> excludeChargeCodes = CancellationUtils.getExcludeChargeCodes(isUserModification);
		Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();
		Map<Integer, ChargeTO> mapChgTO = faresAndChargesTO.getChargeTOs();
		Map<Integer, Collection<FareRuleFeeTO>> mapFareRuleFeeTO = faresAndChargesTO.getFareRuleFeeTOs();

		CustomChargesTO customCharge = this.customChargesTO;
		if (!isApplyCustomCharge) {
			customCharge = new CustomChargesTO();
		}

		// Finding the refundable and cancellation charges
		// Parent
		if (isParent && !infantPaymentSeparated) {
			BigDecimal originalChgAmtBeforeCancellation = AccelAeroCalculator.add(reservationPaxFareAdult.getTotalChargeAmount(),
					reservationPaxFareInfant.getTotalChargeAmount());
			RefundablesMetaInfoTO adultRefundablesMetaInfoTO = this.getAmounts(reservationPaxFareAdult, mapFareTO,
					mapFareRuleFeeTO, mapChgTO, excludeChargeCodes, isCancelFlightsExists, isSameFlightsModification,
					isVoidReservation, executionDate, isExchanged, applyModCharge, isTargetFare, modOndPosition,
					isApplyCustomCharge, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));
			RefundablesMetaInfoTO infantRefundablesMetaInfoTO = this.getAmounts(reservationPaxFareInfant, mapFareTO,
					mapFareRuleFeeTO, mapChgTO, excludeChargeCodes, isCancelFlightsExists, isSameFlightsModification,
					isVoidReservation, executionDate, isExchanged, applyModCharge, isTargetFare, modOndPosition,
					isApplyCustomCharge, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

			Map<Long, ReservationPaxOndCharge> mapRefundablePaxOndCharges = new HashMap<Long, ReservationPaxOndCharge>();
			mapRefundablePaxOndCharges.putAll(adultRefundablesMetaInfoTO.getMapRefundablePaxOndChargeMetaInfo());
			mapRefundablePaxOndCharges.putAll(infantRefundablesMetaInfoTO.getMapRefundablePaxOndChargeMetaInfo());

			BigDecimal penalty = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (flownAsstUnit != null) {
				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFareAdult)) {
					penalty = flownAsstUnit.getPenaltyForPaxFare(reservationPaxFareAdult);
				}
				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFareInfant)) {
					penalty = AccelAeroCalculator.add(penalty, flownAsstUnit.getPenaltyForPaxFare(reservationPaxFareInfant));
				}

				applyCnxCharge = CancellationUtils.applyCancelChargeForFlown(applyCnxCharge,
						flownAsstUnit.hasFlownInternationalSegments());
			}

			BigDecimal effectiveCnxCharge = AccelAeroCalculator.add(adultRefundablesMetaInfoTO.getCancellationTotal(),
					infantRefundablesMetaInfoTO.getCancellationTotal());
			if (customCharge.isCompareWithExistingModificationCharges()) {
				effectiveCnxCharge = getEffectiveCancellationCharge(customCharge, adultRefundablesMetaInfoTO,
						infantRefundablesMetaInfoTO, ReservationInternalConstants.PassengerType.PARENT);
			}

			// Infant won't have any cancellation charge thus infantAmount[1] will always be zero
			// Added to reflect correct values if data corrupted
			this.processCancellationOrModification(reservationPax, reservationPaxFareAdult,
					AccelAeroCalculator.add(adultRefundablesMetaInfoTO.getRefundableTotal(),
							infantRefundablesMetaInfoTO.getRefundableTotal()),
					effectiveCnxCharge,
					AccelAeroCalculator.add(adultRefundablesMetaInfoTO.getModificationTotal(),
							infantRefundablesMetaInfoTO.getModificationTotal()),
					reservationPax.getTotalAvailableBalance(), reservationPax.isAccountHolder(), mapRevenueFares, applyCnxCharge,
					applyModCharge, originalChgAmtBeforeCancellation, mapRefundablePaxOndCharges, penalty, extraFeeBO,
					serviceFeeBO, flownAsstUnit, chgTnxGen);

			// Set the total passenger fare total amounts
			CancellationUtils.setPnrPaxFareTotalAmounts(reservationPaxFareAdult);
			CancellationUtils.setPnrPaxFareTotalAmounts(reservationPaxFareInfant);
		}
		// Adult / Child
		else {
			BigDecimal originalChgAmtBeforeCancellation = reservationPaxFareAdult.getTotalChargeAmount();
			RefundablesMetaInfoTO adultRefundablesMetaInfoTO = this.getAmounts(reservationPaxFareAdult, mapFareTO,
					mapFareRuleFeeTO, mapChgTO, excludeChargeCodes, isCancelFlightsExists, isSameFlightsModification,
					isVoidReservation, executionDate, isExchanged, applyModCharge, isTargetFare, modOndPosition,
					isApplyCustomCharge, chgTnxGen.getTnxSequence(reservationPax.getPnrPaxId()));

			Map<Long, ReservationPaxOndCharge> mapRefundablePaxOndCharges = adultRefundablesMetaInfoTO
					.getMapRefundablePaxOndChargeMetaInfo();

			BigDecimal penalty = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (flownAsstUnit != null) {
				if (flownAsstUnit.isPenaltyApplicablePaxFare(reservationPaxFareAdult)) {
					penalty = flownAsstUnit.getPenaltyForPaxFare(reservationPaxFareAdult);
				}
				applyCnxCharge = CancellationUtils.applyCancelChargeForFlown(applyCnxCharge,
						flownAsstUnit.hasFlownInternationalSegments());
			}

			BigDecimal effectiveCnxCharge = adultRefundablesMetaInfoTO.getCancellationTotal();
			if (customCharge.isCompareWithExistingModificationCharges()) {
				effectiveCnxCharge = getEffectiveCancellationCharge(customCharge, adultRefundablesMetaInfoTO, null,
						reservationPax.getPaxType());
			}

			this.processCancellationOrModification(reservationPax, reservationPaxFareAdult,
					adultRefundablesMetaInfoTO.getRefundableTotal(), effectiveCnxCharge,
					adultRefundablesMetaInfoTO.getModificationTotal(), reservationPax.getTotalAvailableBalance(),
					reservationPax.isAccountHolder(), mapRevenueFares, applyCnxCharge, applyModCharge,
					originalChgAmtBeforeCancellation, mapRefundablePaxOndCharges, penalty, extraFeeBO, serviceFeeBO,
					flownAsstUnit, chgTnxGen);

			// Set the total passenger fare total amounts
			CancellationUtils.setPnrPaxFareTotalAmounts(reservationPaxFareAdult);
		}

		// Release block seats
		boolean isOnHoldSeat = ReservationInternalConstants.ReservationPaxStatus.ON_HOLD
				.equals(reservationPaxFareAdult.getReservationPax().getStatus());

		ReservationPaxFareSegment reservationPaxFareSegment;
		SeatsForSegmentDTO seatsForSegmentDTO;

		for (Iterator<ReservationPaxFareSegment> itColReservationPaxFareSegment = reservationPaxFareAdult.getPaxFareSegments()
				.iterator(); itColReservationPaxFareSegment.hasNext();) {
			reservationPaxFareSegment = itColReservationPaxFareSegment.next();
			this.addRemovingPnrPaxFareId(reservationPaxFareAdult.getPnrPaxFareId());
			if (ignoreSameFlightBookingClsExgForRequote(reservationPaxFareSegment)
					|| ReservationApiUtils.isInfantType(reservationPax)) {
				log.debug("Booking Class Level Inventory Release skipped, for requote exchange Of [pnrPaxFareId="
						+ reservationPaxFareAdult.getPnrPaxFareId() + ", pnrPaxFareSegId="
						+ reservationPaxFareSegment.getPnrPaxFareSegId() + ", fltSegId="
						+ reservationPaxFareSegment.getSegment().getFlightSegId() + ", bookingCode="
						+ reservationPaxFareSegment.getBookingCode() + ", isParent=" + isParent + ", isOnHoldSeat=" + isOnHoldSeat
						+ "]");
				continue;
			}

			boolean isWaitListedSeat = ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
					.equals(reservationPaxFareSegment.getSegment().getStatus());

			seatsForSegmentDTO = new SeatsForSegmentDTO(reservationPaxFareAdult.getPnrPaxFareId().intValue(),
					reservationPaxFareSegment.getPnrPaxFareSegId().intValue(),
					reservationPaxFareSegment.getSegment().getFlightSegId().intValue(),
					reservationPaxFareSegment.getSegment().getBookingCode(), 1, isParent ? 1 : 0, isOnHoldSeat, isWaitListedSeat);
			this.addAdultOrChildOrParentSeatsForSegmentDTO(seatsForSegmentDTO);
		}

		log.debug("Exit getPassengerCreditPerOnd");
	}

	private BigDecimal getEffectiveCancellationCharge(CustomChargesTO customChargesTO,
			RefundablesMetaInfoTO adultRefundablesMetaInfoTO, RefundablesMetaInfoTO infantRefundablesMetaInfoTO, String paxType) {

		BigDecimal effectiveCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (!infantPaymentSeparated && ReservationInternalConstants.PassengerType.PARENT.equals(paxType)) {
			BigDecimal parentCnxCharge = AccelAeroCalculator.add(adultRefundablesMetaInfoTO.getCancellationTotal(),
					infantRefundablesMetaInfoTO.getCancellationTotal());
			if (customChargesTO.getExistingModificationChargeAdult() == null
					&& customChargesTO.getExistingModificationChargeInfant() == null) {
				effectiveCnxCharge = parentCnxCharge;
			} else {
				BigDecimal parentExistingModCharge = AccelAeroCalculator.add(customChargesTO.getExistingModificationChargeAdult(),
						customChargesTO.getExistingModificationChargeInfant());

				if (parentCnxCharge.compareTo(parentExistingModCharge) == 1) {
					effectiveCnxCharge = AccelAeroCalculator.subtract(parentCnxCharge, parentExistingModCharge);
				}
			}
		} else {
			BigDecimal adultCnxCharge = adultRefundablesMetaInfoTO.getCancellationTotal();
			BigDecimal existingModCharge = null;
			if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
				existingModCharge = customChargesTO.getExistingModificationChargeAdult();
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
				existingModCharge = customChargesTO.getExistingModificationChargeChild();
			}

			if (existingModCharge == null) {
				effectiveCnxCharge = adultCnxCharge;
			} else if (adultCnxCharge.compareTo(existingModCharge) == 1) {
				effectiveCnxCharge = AccelAeroCalculator.subtract(adultCnxCharge, existingModCharge);
			}
		}

		return effectiveCnxCharge;
	}

	private boolean ignoreSameFlightBookingClsExgForRequote(ReservationPaxFareSegment paxFareSeg) {
		if (paxFareSeg != null && this.bcNonChangedExgFltSegIds != null
				&& this.bcNonChangedExgFltSegIds.contains(paxFareSeg.getSegment().getFlightSegId())) {
			return true;
		}
		return false;
	}

	/**
	 * Return segment ids for an OND
	 * 
	 * @param reservationPaxFare
	 * @return collection of pnr segment ids
	 */
	private Collection<Integer> getFareSegmentIds(ReservationPaxFare reservationPaxFare) {
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> segmentIds = new ArrayList<Integer>();

		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegment.next();

			if (reservationPaxFareSegment.getPnrSegId() != null) {

				segmentIds.add(reservationPaxFareSegment.getPnrSegId());
			}
		}

		return segmentIds;
	}

	/**
	 * Return the corresponding infant reservation passenger fare
	 * 
	 * @param set
	 *            infants
	 * @throws ModuleException
	 */
	private ReservationPaxFare getInfantReservationPaxFare(Collection<ReservationPax> infants,
			Collection<Integer> parentFareSegIds) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = infants.iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFare reservationPaxFareInfant = null;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Collection<Integer> infantSegIds;

		while (itReservationPax.hasNext()) {
			reservationPax = itReservationPax.next();
			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = itReservationPaxFare.next();
				itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

				infantSegIds = new ArrayList<Integer>();

				while (itReservationPaxFareSegment.hasNext()) {
					reservationPaxFareSegment = itReservationPaxFareSegment.next();

					if (reservationPaxFareSegment.getPnrSegId() != null) {
						infantSegIds.add(reservationPaxFareSegment.getPnrSegId());
					}
				}

				if (parentFareSegIds.size() == infantSegIds.size() && parentFareSegIds.containsAll(infantSegIds)) {
					reservationPaxFareInfant = reservationPaxFare;
					break;
				}
			}
		}

		// If the infant ond fare is not found. Practically this can not happend
		if (reservationPaxFareInfant == null) {
			throw new ModuleException("airreservations.arg.invalidSegInfant");
		}

		return reservationPaxFareInfant;
	}

	/**
	 * Return fare Ids and charge rate Ids
	 * 
	 * @param reservationPaxFare
	 * @param fareIds
	 * @param chgRateIds
	 */
	private void getFareIdsAndChargeIds(ReservationPaxFare reservationPaxFare, Collection<Integer> fareIds,
			Collection<Integer> chgRateIds) {
		Iterator<ReservationPaxOndCharge> itCharges = reservationPaxFare.getCharges().iterator();
		ReservationPaxOndCharge reservationPaxOndCharge;

		while (itCharges.hasNext()) {
			reservationPaxOndCharge = itCharges.next();

			if (reservationPaxOndCharge.getChargeRateId() != null) {
				chgRateIds.add(reservationPaxOndCharge.getChargeRateId());
			} else if (reservationPaxOndCharge.getFareId() != null) {
				fareIds.add(reservationPaxOndCharge.getFareId());
			}
		}
	}

	/**
	 * Returns the Action Code
	 * 
	 * @param isPaxCancel
	 * @return
	 */
	private int getActionCode(Boolean isPaxCancel) {
		if (isPaxCancel.booleanValue()) {
			return ReservationTnxNominalCode.PAX_CANCEL.getCode();
		} else {
			return ReservationTnxNominalCode.AIR_LINE_CANCEL.getCode();
		}
	}

	/**
	 * Return amounts
	 * 
	 * @param reservationPaxFare
	 * @param mapFareTO
	 * @param mapFareRuleFeeTO
	 * @param mapChgTO
	 * @param excludeChargeCodes
	 * @param isVoidReservation
	 * @param executionDate
	 * @param isExchanged
	 * @param modOndPosition
	 * @param isApplyCustomCharge
	 * @param tnxSequence
	 * @return
	 * @throws ModuleException
	 */
	private RefundablesMetaInfoTO getAmounts(ReservationPaxFare reservationPaxFare, Map<Integer, FareTO> mapFareTO,
			Map<Integer, Collection<FareRuleFeeTO>> mapFareRuleFeeTO, Map<Integer, ChargeTO> mapChgTO,
			Collection<String> excludeChargeCodes, boolean isCancelFlightsExists, boolean isSameFlightsModification,
			boolean isVoidReservation, Date executionDate, boolean isExchanged, boolean isAModification, boolean isTargetFare,
			int modOndPosition, boolean isApplyCustomCharge, Integer tnxSequence) throws ModuleException {

		boolean isOnHoldReservation = ReservationInternalConstants.ReservationStatus.ON_HOLD
				.equals(reservationPaxFare.getReservationPax().getReservation().getStatus()) ? true : false;

		BigDecimal paxRefundableTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal cancelTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal modifyTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		String paxType = reservationPaxFare.getReservationPax().getPaxType();
		ReservationPaxOndCharge reservationPaxOndCharge;
		FareTO fareTO;
		ChargeTO chargeTO;
		Collection<FareRuleFeeTO> colFareRuleFeeTO;
		RefundablesMetaInfoTO refundablesMetaInfoTO = new RefundablesMetaInfoTO();

		Collection<ReservationPaxOndCharge> newOndCharges = new ArrayList<ReservationPaxOndCharge>();
		Iterator<ReservationPaxOndCharge> itCharges = reservationPaxFare.getCharges().iterator();

		boolean[] applyFlexiFlags = CancellationUtils.getApplyFlexibilitiesToOndFlags(reservationPaxFare);
		boolean hasCnxFlexibility = applyFlexiFlags[1];
		boolean hasModFlexibility = applyFlexiFlags[0];

		boolean isRefundNonRefundableFaresInEx = AppSysParamsUtil.isRefundNonRefundableFaresInExchange();

		Collection<String> exchangeExcludeChargeCodes = new HashSet<String>();
		if (isExchanged) {
			exchangeExcludeChargeCodes = CancellationUtils.getExchangeExcludeChargeCodes();
		}

		while (itCharges.hasNext()) {
			reservationPaxOndCharge = itCharges.next();
			BigDecimal actualOndChargeAmount = reservationPaxOndCharge.getEffectiveAmount();

			if (reservationPaxOndCharge.getChargeRateId() != null) {
				chargeTO = mapChgTO.get(reservationPaxOndCharge.getChargeRateId());

				PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
						reservationPaxOndCharge.getNonTaxableAmount().negate(), reservationPaxOndCharge.getTaxableAmount().negate(),
						reservationPaxOndCharge.getTaxAppliedCategory(), reservationPaxOndCharge.getTaxDepositCountryCode(),
						reservationPaxOndCharge.getTaxDepositStateCode());

				if (chargeTO.isRefundable() && (isExchanged || !chargeTO.isRefundableOnlyForMOD()
						|| (chargeTO.isRefundableOnlyForMOD() && isAModification))) {
					// Capturing the refundable amount
					paxRefundableTotal = AccelAeroCalculator.add(paxRefundableTotal, actualOndChargeAmount);

					// Entering a minus record for the refundable amount
					ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
							reservationPaxOndCharge.getAmount().negate(), null, reservationPaxOndCharge.getChargeRateId(),
							reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
							reservationPaxOndCharge.getDiscount().negate(), reservationPaxOndCharge.getAdjustment().negate(),
							paxOndChargeDTO, tnxSequence);

					newOndCharges.add(tmpReservationPaxOndCharge);
					refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
							tmpReservationPaxOndCharge);
				} else {
					if ((isOnHoldReservation && !excludeChargeCodes.contains(chargeTO.getChargeCode()))
							|| (isVoidReservation && !exchangeExcludeChargeCodes.contains(chargeTO.getChargeCode()))
							|| (!isOnHoldReservation && isExchanged && chargeTO.isRefundableWhenExchange())) {
						// Capturing the refundable amount
						paxRefundableTotal = AccelAeroCalculator.add(paxRefundableTotal, actualOndChargeAmount);

						// Entering a minus record for the refundable amount
						ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
								reservationPaxOndCharge.getAmount().negate(), null, reservationPaxOndCharge.getChargeRateId(),
								reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
								reservationPaxOndCharge.getDiscount().negate(), reservationPaxOndCharge.getAdjustment().negate(),
								paxOndChargeDTO, tnxSequence);

						newOndCharges.add(tmpReservationPaxOndCharge);
						refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
								tmpReservationPaxOndCharge);
					}
				}
			} else if (reservationPaxOndCharge.getFareId() != null) {
				fareTO = mapFareTO.get(reservationPaxOndCharge.getFareId());
				colFareRuleFeeTO = mapFareRuleFeeTO.get(fareTO.getFareRuleID());

				if (AppSysParamsUtil.isAgentCommmissionEnabled() && isTargetFare) {
					if (ReservationApiUtils.isRefundable(fareTO, paxType)) {

						if (this.removeAgentCommission) {
							if (this.commissionRemovalExcludedFareIds != null
									&& !this.commissionRemovalExcludedFareIds.isEmpty()) {
								if (!commissionRemovalExcludedFareIds.contains(reservationPaxOndCharge.getFareId())) {
									callBackAgentCommission(reservationPaxOndCharge);
								}
							} else {
								callBackAgentCommission(reservationPaxOndCharge);
							}
						}
					}
				}

				if (ReservationApiUtils.isRefundable(fareTO, paxType)) {
					// Capturing the refundable amount
					paxRefundableTotal = AccelAeroCalculator.add(paxRefundableTotal, actualOndChargeAmount);

					// Entering the minus record for the refundable amount
					ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
							reservationPaxOndCharge.getAmount().negate(), reservationPaxOndCharge.getFareId(), null,
							reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
							reservationPaxOndCharge.getDiscount().negate(), reservationPaxOndCharge.getAdjustment().negate(),
							null, tnxSequence);

					newOndCharges.add(tmpReservationPaxOndCharge);
					refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
							tmpReservationPaxOndCharge);
				} else {
					if ((isOnHoldReservation || isVoidReservation) || (isExchanged && isRefundNonRefundableFaresInEx)) {
						// Capturing the refundable amount
						paxRefundableTotal = AccelAeroCalculator.add(paxRefundableTotal, actualOndChargeAmount);

						// Entering the minus record for the refundable amount
						ReservationPaxOndCharge tmpReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(
								reservationPaxOndCharge.getAmount().negate(), reservationPaxOndCharge.getFareId(), null,
								reservationPaxOndCharge.getChargeGroupCode(), credentialsDTO, true, 0,
								reservationPaxOndCharge.getDiscount().negate(), reservationPaxOndCharge.getAdjustment().negate(),
								null, tnxSequence);

						newOndCharges.add(tmpReservationPaxOndCharge);
						refundablesMetaInfoTO.addRefundableCharge(reservationPaxOndCharge.getPnrPaxOndChgId(),
								tmpReservationPaxOndCharge);
					}
				}

				FeeCalculator cal = new FeeCalculator(reservationPaxFare, colFareRuleFeeTO, executionDate,
						fareTO.isNoonDayFeePolicyEnabled());
				cal.execute();

				FareRuleFeeTO cancelFee = cal.getCancelFee();
				FareRuleFeeTO modifyFee = cal.getModifyFee();

				CustomChargesTO customCharge = this.customChargesTO;
				if (!isApplyCustomCharge) {
					customCharge = null;
				}

				// Capturing the cancellation amount
				cancelTotalCharge = AccelAeroCalculator.add(cancelTotalCharge,
						FeeBO.getCancellationCharge(fareTO, paxType, reservationPaxOndCharge.getAmount(),
								reservationPaxFare.getCharges(), new PnrChargeDetailTO(), customCharge, cancelFee,
								hasCnxFlexibility, isCancelFlightsExists, isVoidReservation, modOndPosition));

				// Capturing the modification amount
				modifyTotalCharge = AccelAeroCalculator.add(modifyTotalCharge,
						FeeBO.getModificationCharge(fareTO, paxType, reservationPaxOndCharge.getAmount(),
								reservationPaxFare.getCharges(), new PnrChargeDetailTO(), customCharge, modifyFee,
								hasModFlexibility, isCancelFlightsExists, isSameFlightsModification, modOndPosition));
			}
		}

		// Split Discount amount based on refundable/non-refndable charges
		boolean isFiltered = DiscountHandlerUtil.filterDiscount(refundablesMetaInfoTO);
		if (isFiltered) {
			paxRefundableTotal = refundablesMetaInfoTO.getRefundableTotal();
		}

		// Adding the new charges
		for (ReservationPaxOndCharge newReservationPaxOndCharge : newOndCharges) {
			reservationPaxFare.addCharge(newReservationPaxOndCharge);
		}

		refundablesMetaInfoTO.setRefundableTotal(paxRefundableTotal);
		if (this.utilizeAllRefundables) {
			// if this flag is set we need to utilize all the refundables generated from the cancelation
			modifyTotalCharge = paxRefundableTotal;
			cancelTotalCharge = paxRefundableTotal;
		}

		refundablesMetaInfoTO.setModificationTotal(modifyTotalCharge);
		refundablesMetaInfoTO.setCancellationTotal(cancelTotalCharge);

		return refundablesMetaInfoTO;
	}

	/**
	 * Call back agent commission from respective agent account [pax, ond] wise if commission was applied
	 * 
	 * @param reservationPaxOndCharge
	 * @param fareTO
	 * @param paxType
	 * @throws ModuleException
	 */
	private void callBackAgentCommission(ReservationPaxOndCharge reservationPaxOndCharge) throws ModuleException {
		AgentCommissionRemovalBO agentCommissionBO = new AgentCommissionRemovalBO(reservationPaxOndCharge, this.credentialsDTO);
		agentCommissionBO.execute();
	}

	/**
	 * Return identified cancellation amount
	 * 
	 * @param pnrPaxId
	 * @param reservationPaxFareAdult
	 * @param creditAmount
	 * @param cancellationCharge
	 * @param availableBalance
	 * @param adjustCNXCharge
	 * @return
	 */
	private BigDecimal getIdentifiedCancellationCharge(Integer pnrPaxId, ReservationPaxFare reservationPaxFareAdult,
			BigDecimal creditAmount, BigDecimal cancellationCharge, BigDecimal availableBalance, boolean adjustCNXCharge) {

		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationTnx> colReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPayments(pnrPaxId.toString());

		if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			// Find out whether any non expired segment exists
			boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFareAdult);

			if (isAnyNonExpiredSegExists && !adjustCNXCharge) {
				identifiedAmount = cancellationCharge;
			} else {
				identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
			}
		} else {
			identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
		}

		return identifiedAmount;
	}

	private FeeHolder getFeeHolderForCancellation(Integer pnrPaxId, ReservationPaxFare reservationPaxFare,
			BigDecimal creditAmount, BigDecimal cancellationCharge, BigDecimal availableBalance, boolean adjustCNXCharge,
			ServiceFeeBO serviceFeeBO) throws ModuleException {

		FeeHolder fee = null;
		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationTnx> colReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPayments(pnrPaxId.toString());
		BigDecimal maxAmount = null;
		if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			// Find out whether any non expired segment exists
			boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFare);

			if (isAnyNonExpiredSegExists && !adjustCNXCharge) {
				identifiedAmount = cancellationCharge;
			} else {
				identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
				maxAmount = AccelAeroCalculator.add(availableBalance, creditAmount.negate()).negate();

			}
		} else {
			identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, cancellationCharge, availableBalance);
			maxAmount = AccelAeroCalculator.add(availableBalance, creditAmount.negate()).negate();
		}

		if (maxAmount != null && maxAmount.doubleValue() < 0) {
			maxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		if (serviceFeeBO.isExtraFeeApplicable()) {
			String cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(reservationPaxFare);
			fee = serviceFeeBO.getFeeHolder(identifiedAmount, maxAmount, ReservationInternalConstants.ChargeGroup.CNX,
					cabinClassCode, pnrPaxId);
		} else {
			fee = new FeeHolder(identifiedAmount);
		}

		return fee;
	}

	private BigDecimal getIdentifiedPenaltyCharge(Integer pnrPaxId, ReservationPaxFare reservationPaxFareAdult,
			BigDecimal creditAmount, BigDecimal penaltyCharge, BigDecimal availableBalance, FlownAssitUnit flownAsstUnit) {

		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationTnx> colReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPayments(pnrPaxId.toString());

		if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			// Find out whether any non expired segment exists
			boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFareAdult);

			if (isAnyNonExpiredSegExists) {
				identifiedAmount = penaltyCharge;
			} else {
				identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, penaltyCharge, availableBalance);
			}
		} else {
			identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, penaltyCharge, availableBalance);
		}

		// add the identified penalty amount to audit if the amount got altered
		if (flownAsstUnit != null && penaltyCharge != null && identifiedAmount.doubleValue() != penaltyCharge.doubleValue()) {
			if (flownAsstUnit.getAuditDetails() != null && flownAsstUnit.getAuditDetails().length() > 0) {
				flownAsstUnit.getAuditDetails().append(", ADJESTED PENALTY:" + identifiedAmount);
			}
		}

		return identifiedAmount;
	}

	/**
	 * Return the adjusted cancellation/penalty charge
	 * 
	 * @param creditAmount
	 * @param chargeAmount
	 * @param availableBalance
	 * @param penaltyCharge
	 * @return
	 */
	private BigDecimal getAdjustedCnxPenCharge(BigDecimal creditAmount, BigDecimal chargeAmount, BigDecimal availableBalance) {
		BigDecimal adjustedCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalAvCredits = AccelAeroCalculator.add(availableBalance, creditAmount.negate());

		if (totalAvCredits.doubleValue() > 0) {
			adjustedCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		} else {
			if (AccelAeroCalculator.add(totalAvCredits, chargeAmount).doubleValue() <= 0) {
				adjustedCancellationAmount = chargeAmount;
			} else {
				adjustedCancellationAmount = totalAvCredits.negate();
			}
		}

		return adjustedCancellationAmount;
	}

	/**
	 * Process Cancellation or modification
	 * 
	 * @param reservationPaxFareAdult
	 * @param creditAmount
	 * @param cancellationCharge
	 * @param modificationCharge
	 * @param availableBalance
	 * @param isAccountHolder
	 * @param mapRevenueFares
	 * @param originalChgAmtBeforeCancellation
	 * @param serviceFeeBO
	 *            TODO
	 * @param flownAsstUnit
	 *            TODO
	 * @param chgTnxGen
	 * @param pnrPaxId
	 * @param showCnxCharge
	 * @param showModCharge
	 * @param allRefundableChargeMetaTOs
	 * 
	 * @throws ModuleException
	 */
	private void processCancellationOrModification(ReservationPax reservationPax, ReservationPaxFare reservationPaxFareAdult,
			BigDecimal creditAmount, BigDecimal cancellationCharge, BigDecimal modificationCharge, BigDecimal availableBalance,
			boolean isAccountHolder, Map<Integer, RevenueDTO> mapRevenueFares, boolean applyCnxCharge, boolean applyModCharge,
			BigDecimal originalChgAmtBeforeCancellation, Map<Long, ReservationPaxOndCharge> mapRefundablePaxOndCharges,
			BigDecimal penalty, ExtraFeeBO extraFeeBO, ServiceFeeBO serviceFeeBO, FlownAssitUnit flownAsstUnit,
			ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		Integer pnrPaxId = reservationPax.getPnrPaxId();

		RevenueDTO revenueDTO = new RevenueDTO();
		revenueDTO.setPnrPaxId(pnrPaxId);
		revenueDTO.setCreditTotal(creditAmount);
		revenueDTO.setTotalChgBeforeCancellation(originalChgAmtBeforeCancellation);

		ReservationPaxOndCharge newReservationPaxOndCharge;
		BigDecimal effectiveCreditAmount = creditAmount;
		
		boolean isOnlyCancellation = applyCnxCharge && !applyModCharge ? true : false;

		if (penalty != null && penalty.compareTo(BigDecimal.ZERO) > 0) {
			BigDecimal identifiedPenaltyCharge = getIdentifiedPenaltyCharge(pnrPaxId, reservationPaxFareAdult,
					effectiveCreditAmount, penalty, availableBalance, flownAsstUnit);
			BigDecimal appliedPenaltyCharge = updateServiceTaxPenalty(reservationPaxFareAdult, identifiedPenaltyCharge,
					flownAsstUnit, isOnlyCancellation, chgTnxGen.getTnxSequence(pnrPaxId));
			revenueDTO.setModificationPenalty(identifiedPenaltyCharge);

			newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(appliedPenaltyCharge, null, null,
					ReservationInternalConstants.ChargeGroup.PEN, credentialsDTO, false, 0, null, null, null,
					chgTnxGen.getTnxSequence(pnrPaxId));

			// Entering the plus record for the identified modification amount
			reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);
			effectiveCreditAmount = CancellationUtils.reValidateCreditAmount(effectiveCreditAmount, identifiedPenaltyCharge);
		}

		// check if segment is no show then skip cancellation charge
		applyCnxCharge = CancellationUtils.applyCancelChargeForNoShowSegment(applyCnxCharge, reservationPaxFareAdult);

		// CnxModPenServiceTaxDTO cnxModPenServiceTaxes = null;

		// If cancellation charges applies
		// Remember this is been used by change segments too... So in that case there won't be any
		// Cancellation charges involved. This is to make the command reusable through out the suite
		if (applyCnxCharge) {
			boolean adjustCNXCharge = AppSysParamsUtil.isSystemChargeAdjustmentForDueAmountEnabled();
			// Get the identified Cancellation amount
			// BigDecimal identifiedCCharge = this.getIdentifiedCancellationCharge(pnrPaxId, reservationPaxFareAdult,
			// effectiveCreditAmount, cancellationCharge, availableBalance, adjustCNXCharge);

			FeeHolder cnxFeeHolder = this.getFeeHolderForCancellation(pnrPaxId, reservationPaxFareAdult, effectiveCreditAmount,
					cancellationCharge, availableBalance, adjustCNXCharge, serviceFeeBO);

			BigDecimal identifiedCCharge = cnxFeeHolder.getChargeAmount();

			effectiveCreditAmount = CancellationUtils.reValidateCreditAmount(effectiveCreditAmount, cnxFeeHolder.getTotal());

			BigDecimal identifiedExtraCharge = this.getIdentifiedExtraFeeCharge(pnrPaxId, reservationPaxFareAdult,
					effectiveCreditAmount, identifiedCCharge, availableBalance, extraFeeBO, false);
			effectiveCreditAmount = CancellationUtils.reValidateCreditAmount(effectiveCreditAmount, identifiedExtraCharge);

			if (identifiedExtraCharge.doubleValue() > 0) {
				revenueDTO.setCancelOrModifyTotal(AccelAeroCalculator.add(cnxFeeHolder.getTotal(), identifiedExtraCharge));

				newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(identifiedCCharge, null, null,
						ReservationInternalConstants.ChargeGroup.CNX, credentialsDTO, false, 0, null, null, null,
						chgTnxGen.getTnxSequence(pnrPaxId));

				// Entering the plus record for the cancellation amount
				reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);

				newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(identifiedExtraCharge, null,
						extraFeeBO.getExternalChgDTO().getChgRateId(), ReservationInternalConstants.ChargeGroup.TAX,
						credentialsDTO, false, 0, null, null, null, chgTnxGen.getTnxSequence(pnrPaxId));

				// Entering the plus record for the cancellation amount
				reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);
			} else {
				revenueDTO.setCancelOrModifyTotal(cnxFeeHolder.getTotal());
				newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(identifiedCCharge, null, null,
						ReservationInternalConstants.ChargeGroup.CNX, credentialsDTO, false, 0, null, null, null,
						chgTnxGen.getTnxSequence(pnrPaxId));

				// Entering the plus record for the cancellation amount
				reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);
			}

			if (cnxFeeHolder.hasFees()) {
				for (ReservationPaxOndCharge newPaxOndChargeFee : cnxFeeHolder.getOndChargesForFee(credentialsDTO,
						chgTnxGen.getTnxSequence(pnrPaxId), ReservationInternalConstants.ChargeGroup.CNX)) {
					reservationPaxFareAdult.addCharge(newPaxOndChargeFee);
				}
			}

		} else if (applyModCharge) {
			// Get the identified Modification charge
			// BigDecimal identifiedMCharge = this.getIdentifiedModificationCharge(pnrPaxId, modificationCharge);

			FeeHolder modFeeHolder = this.getFeeHolderForModification(pnrPaxId, modificationCharge, reservationPaxFareAdult,
					serviceFeeBO);
			BigDecimal identifiedMCharge = modFeeHolder.getChargeAmount();

			BigDecimal identifiedExtraCharge = this.getIdentifiedExtraFeeCharge(pnrPaxId, reservationPaxFareAdult,
					effectiveCreditAmount, identifiedMCharge, availableBalance, extraFeeBO, applyModCharge);
			effectiveCreditAmount = CancellationUtils.reValidateCreditAmount(effectiveCreditAmount, identifiedExtraCharge);

			if (identifiedExtraCharge.doubleValue() > 0) {
				revenueDTO.setCancelOrModifyTotal(AccelAeroCalculator.add(modFeeHolder.getTotal(), identifiedExtraCharge));

				newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(identifiedMCharge, null, null,
						ReservationInternalConstants.ChargeGroup.MOD, credentialsDTO, false, 0, null, null, null,
						chgTnxGen.getTnxSequence(pnrPaxId));

				// Entering the plus record for the identified modification amount
				reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);

				newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(identifiedExtraCharge, null,
						extraFeeBO.getExternalChgDTO().getChgRateId(), ReservationInternalConstants.ChargeGroup.TAX,
						credentialsDTO, false, 0, null, null, null, chgTnxGen.getTnxSequence(pnrPaxId));

				// Entering the plus record for the identified modification amount
				reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);
			} else {
				revenueDTO.setCancelOrModifyTotal(modFeeHolder.getTotal());

				newReservationPaxOndCharge = TOAssembler.createReservationPaxOndCharge(identifiedMCharge, null, null,
						ReservationInternalConstants.ChargeGroup.MOD, credentialsDTO, false, 0, null, null, null,
						chgTnxGen.getTnxSequence(pnrPaxId));

				// Entering the plus record for the identified modification amount
				reservationPaxFareAdult.addCharge(newReservationPaxOndCharge);
			}

			if (modFeeHolder.hasFees()) {
				for (ReservationPaxOndCharge newPaxOndChargeFee : modFeeHolder.getOndChargesForModFee(credentialsDTO,
						chgTnxGen.getTnxSequence(pnrPaxId))) {
					reservationPaxFareAdult.addCharge(newPaxOndChargeFee);
				}
			}

		}

		revenueDTO.getMapRefundableCharges().putAll(mapRefundablePaxOndCharges);

		// Add the RevenueDTO
		mapRevenueFares.put(reservationPaxFareAdult.getPnrPaxFareId(), revenueDTO);
	}

	private static FareSummaryDTO getDummyFareSummaryDTO() {
		BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();
		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
		fareSummaryDTO.setAdultFare(fare.doubleValue());
		fareSummaryDTO.setChildFare(fare.doubleValue());
		fareSummaryDTO.setInfantFare(fare.doubleValue());
		fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V);
		return fareSummaryDTO;
	}

	private BigDecimal getIdentifiedExtraFeeCharge(Integer pnrPaxId, ReservationPaxFare reservationPaxFareAdult,
			BigDecimal creditAmount, BigDecimal identifiedCharge, BigDecimal availableBalance, ExtraFeeBO extraFeeBO,
			boolean applyModCharge) throws ModuleException {

		BigDecimal identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (extraFeeBO.isExtraFeeApplicable()) {
			String cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(reservationPaxFareAdult);
			BigDecimal applicableTotalExtraFee = extraFeeBO.getFee(identifiedCharge, cabinClassCode);

			Collection<ReservationTnx> colReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
					.getPNRPaxPayments(pnrPaxId.toString());

			if (applyModCharge) {
				if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
					identifiedAmount = applicableTotalExtraFee;
				} else {
					identifiedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				}
			} else {
				if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
					// Find out whether any non expired segment exists
					boolean isAnyNonExpiredSegExists = ReservationCoreUtils.isAnyOpenSegExists(reservationPaxFareAdult);

					if (isAnyNonExpiredSegExists) {
						identifiedAmount = applicableTotalExtraFee;
					} else {
						identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, applicableTotalExtraFee, availableBalance);
					}
				} else {
					identifiedAmount = this.getAdjustedCnxPenCharge(creditAmount, applicableTotalExtraFee, availableBalance);
				}
			}
		}

		return identifiedAmount;
	}

	/**
	 * Return the identified modification charge
	 * 
	 * @param pnrPaxId
	 * @param modificationCharge
	 * @return
	 */
	private BigDecimal getIdentifiedModificationCharge(Integer pnrPaxId, BigDecimal modificationCharge) {
		BigDecimal identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationTnx> colReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPayments(pnrPaxId.toString());

		// If any payment exist modification charge is the default charge
		if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			identifiedModificationCharge = modificationCharge;
		}
		// If no payment exist modification charge is zero
		else {
			identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		}

		return identifiedModificationCharge;
	}

	private FeeHolder getFeeHolderForModification(Integer pnrPaxId, BigDecimal modificationCharge,
			ReservationPaxFare reservationPaxFare, ServiceFeeBO serviceFeeBO) throws ModuleException {

		BigDecimal identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ReservationTnx> colReservationTnx = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO
				.getPNRPaxPayments(pnrPaxId.toString());
		// If any payment exist modification charge is the default charge
		if (this.hasExternalCarrierPayments || ReservationApiUtils.hasPayment(colReservationTnx)) {
			identifiedModificationCharge = modificationCharge;
		}
		// If no payment exist modification charge is zero
		else {
			identifiedModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		FeeHolder fee = null;
		if (serviceFeeBO.isExtraFeeApplicable()) {
			String cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(reservationPaxFare);
			fee = serviceFeeBO.getFeeHolder(identifiedModificationCharge, null, ReservationInternalConstants.ChargeGroup.MOD,
					cabinClassCode, pnrPaxId);
		} else {
			fee = new FeeHolder(identifiedModificationCharge);
		}

		return fee;
	}

	/**
	 * Record revenue
	 * 
	 * @param mapRevenueFares
	 * @return
	 * @throws ModuleException
	 */
	private RevenueDTO recordRevenue(Map<Integer, RevenueDTO> mapRevenueFares) throws ModuleException {
		Iterator<RevenueDTO> itRevenueTOs = mapRevenueFares.values().iterator();
		RevenueDTO revenueDTOHolder = new RevenueDTO();
		RevenueDTO revenueDTO;

		while (itRevenueTOs.hasNext()) {
			revenueDTO = itRevenueTOs.next();

			revenueDTOHolder.setPnrPaxId(revenueDTO.getPnrPaxId());
			revenueDTOHolder
					.setCreditTotal(AccelAeroCalculator.add(revenueDTOHolder.getCreditTotal(), revenueDTO.getCreditTotal()));
			revenueDTOHolder.setCancelOrModifyTotal(
					AccelAeroCalculator.add(revenueDTOHolder.getCancelOrModifyTotal(), revenueDTO.getCancelOrModifyTotal()));
			revenueDTOHolder.setModificationPenalty(
					AccelAeroCalculator.add(revenueDTOHolder.getModificationPenalty(), revenueDTO.getModificationPenalty()));
			revenueDTOHolder.setTotalChgBeforeCancellation(AccelAeroCalculator
					.add(revenueDTOHolder.getTotalChgBeforeCancellation(), revenueDTO.getTotalChgBeforeCancellation()));
			revenueDTOHolder.setAddedTotal(AccelAeroCalculator.add(revenueDTOHolder.getAddedTotal(), revenueDTO.getAddedTotal()));

			revenueDTOHolder.getMapRefundableCharges().putAll(revenueDTO.getMapRefundableCharges());
			revenueDTOHolder.getAddedCharges().addAll(revenueDTO.getAddedCharges());
		}

		return revenueDTOHolder;
	}

	/**
	 * Process passenger credit for Onds
	 * 
	 * @param reservationPax
	 * @param colReservationPaxFare
	 * @param isApplyCustomCharge
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	public void processPaxCreditForOnds(ReservationPax reservationPax, Collection<ReservationPaxFare> colReservationPaxFare,
			boolean applyCnxCharge, boolean applyModCharge, boolean isVoidReservation, boolean isFareAdjustmentValid,
			boolean isApplyCustomCharge, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		Map<Integer, RevenueDTO> mapRevenueFares = new HashMap<Integer, RevenueDTO>();

		Iterator<ReservationPaxFare> itReservationPaxFare = colReservationPaxFare.iterator();
		ReservationPaxFare reservationPaxFare;

		while (itReservationPaxFare.hasNext()) {
			reservationPaxFare = itReservationPaxFare.next();
			this.processPassengerCreditPerOnd(reservationPax, reservationPaxFare, mapRevenueFares, applyCnxCharge, applyModCharge,
					false, isVoidReservation, null, false, isFareAdjustmentValid, false, 0, isApplyCustomCharge, chgTnxGen);
		}

		RevenueDTO revenueDTO = this.recordRevenue(mapRevenueFares);
		this.capturePerPaxRevenue(reservationPax, revenueDTO);
	}

	/**
	 * Process passenger credit for Ond
	 * 
	 * @param reservationPax
	 * @param reservationPaxFare
	 * @param showCnxCharge
	 * @param showModCharge
	 * @param isSameFlightsModification
	 * @param isVoidReservation
	 * @param flownAsstUnit
	 * @param isExchanged
	 * @param modOndPosition
	 * @param isApplyCustomCharge
	 *            TODO
	 * @param chgTnxGen
	 * @throws ModuleException
	 */
	protected void processPaxCreditForOnd(ReservationPax reservationPax, ReservationPaxFare reservationPaxFare,
			boolean applyCnxCharge, boolean applyModCharge, boolean isSameFlightsModification, boolean isVoidReservation,
			FlownAssitUnit flownAsstUnit, boolean isExchanged, boolean isFareAdjustmentValid, boolean isTargetFare,
			int modOndPosition, boolean isApplyCustomCharge, ChargeTnxSeqGenerator chgTnxGen) throws ModuleException {

		Map<Integer, RevenueDTO> mapRevenueFares = new HashMap<Integer, RevenueDTO>();

		this.processPassengerCreditPerOnd(reservationPax, reservationPaxFare, mapRevenueFares, applyCnxCharge, applyModCharge,
				isSameFlightsModification, isVoidReservation, flownAsstUnit, isExchanged, isFareAdjustmentValid, isTargetFare,
				modOndPosition, isApplyCustomCharge, chgTnxGen);

		RevenueDTO revenueDTO = this.recordRevenue(mapRevenueFares);
		this.capturePerPaxRevenue(reservationPax, revenueDTO);
	}

	/**
	 * Capture Revenue to be processed later
	 * 
	 * @param reservationPax
	 * @param revenueDTO
	 */
	private void capturePerPaxRevenue(ReservationPax reservationPax, RevenueDTO revenueDTO) {
		RevenueDTO revenueDTOTarget = this.getMapPerPaxWiseRevenue().get(reservationPax.getPnrPaxId());

		if (revenueDTOTarget != null) {
			revenueDTOTarget.setAddedTotal(AccelAeroCalculator.add(revenueDTOTarget.getAddedTotal(), revenueDTO.getAddedTotal()));
			revenueDTOTarget.setCancelOrModifyTotal(
					AccelAeroCalculator.add(revenueDTOTarget.getCancelOrModifyTotal(), revenueDTO.getCancelOrModifyTotal()));
			revenueDTOTarget.setModificationPenalty(
					AccelAeroCalculator.add(revenueDTOTarget.getModificationPenalty(), revenueDTO.getModificationPenalty()));
			revenueDTOTarget
					.setCreditTotal(AccelAeroCalculator.add(revenueDTOTarget.getCreditTotal(), revenueDTO.getCreditTotal()));
			revenueDTOTarget.setTotalChgBeforeCancellation(AccelAeroCalculator
					.add(revenueDTOTarget.getTotalChgBeforeCancellation(), revenueDTO.getTotalChgBeforeCancellation()));

			revenueDTOTarget.getMapRefundableCharges().putAll(revenueDTO.getMapRefundableCharges());
			revenueDTOTarget.getAddedCharges().addAll(revenueDTO.getAddedCharges());
		} else {
			this.getMapPerPaxWiseRevenue().put(reservationPax.getPnrPaxId(), revenueDTO);
		}
	}

	/**
	 * Propergate the saved charge revenue for accounting. We need to do this operation after reservation save since the
	 * new charges primary keys are generated after saving to the reservation
	 * 
	 * @throws ModuleException
	 */
	public void propergateTheSavedChargeRevenueForAccounting(Reservation reservation) throws ModuleException {

		if (isCancelChgOperation) {
			ReservationPaymentMetaTO reservationPaymentMetaTO = TnxGranularityReservationUtils
					.getReservationPaymentMetaTO(reservation);
			RecordCancelBO recordCancelBO = new RecordCancelBO();

			for (RevenueDTO revenueDTO : this.getMapPerPaxWiseRevenue().values()) {
				// Calling revenue accounting
				ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = new ReservationPaxPaymentMetaTO();
				reservationPaxPaymentMetaTO.setPnrPaxId(revenueDTO.getPnrPaxId());
				reservationPaxPaymentMetaTO.addMapPerPaxWiseOndRefundableCharges(revenueDTO.getMapRefundableCharges());
				reservationPaxPaymentMetaTO.addColPerPaxWiseOndNewCharges(reservationPaymentMetaTO.getMapPaxWisePaymentMetaInfo()
						.get(revenueDTO.getPnrPaxId()).getColPerPaxWiseOndNewCharges());

				recordCancelBO.addRecord(revenueDTO.getPnrPaxId().toString(), revenueDTO.getCreditTotal(),
						revenueDTO.getCancelOrModifyTotal(), revenueDTO.getModificationPenalty(), this.getActionCode(isPaxCancel),
						ReservationTnxNominalCode.CANCEL_CHARGE.getCode(), reservationPaxPaymentMetaTO, credentialsDTO,
						reservation.isEnableTransactionGranularity());
			}

			recordCancelBO.execute();

		}

	}

	private void addRemovingPnrPaxFareId(Integer pnrPaxFareId) {
		removedPnrPaxFareIds.add(pnrPaxFareId);
	}

	/**
	 * Adds seats for ond data transfer information (PARENT OR ADULT)
	 * 
	 * @param seatsForSegmentDTO
	 */
	private void addAdultOrChildOrParentSeatsForSegmentDTO(SeatsForSegmentDTO seatsForSegmentDTO) {
		if (this.getAdultOrChildOrParentColSeatsForSegmentDTO() == null) {
			this.setAdultOrChildOrParentColSeatsForSegmentDTO(new ArrayList<SeatsForSegmentDTO>());
		}

		this.getAdultOrChildOrParentColSeatsForSegmentDTO().add(seatsForSegmentDTO);
	}

	/**
	 * Adds seats for ond data transfer information (INFANT ONLY)
	 * 
	 * @param seatsForSegmentDTO
	 */
	public void addInfantOnlySeatsForSegmentDTO(SeatsForSegmentDTO seatsForSegmentDTO) {
		if (this.getInfantOnlyColSeatsForSegmentDTO() == null) {
			this.setInfantOnlyColSeatsForSegmentDTO(new ArrayList<SeatsForSegmentDTO>());
		}

		this.getInfantOnlyColSeatsForSegmentDTO().add(seatsForSegmentDTO);
	}

	/**
	 * @param infantOnlyBCMap
	 *            The infantOnlyBCMap to set.
	 */
	public void setInfantOnlyBCMap(Map<Integer, String> infantOnlyBCMap) {
		this.infantOnlyBCMap = infantOnlyBCMap;
	}

	/**
	 * @return Returns the infantOnlyBCMap.
	 */
	private Map<Integer, String> getInfantOnlyBCMap() {
		return infantOnlyBCMap;
	}

	/**
	 * @return Returns the infantOnlyColSeatsForSegmentDTO.
	 */
	private Collection<SeatsForSegmentDTO> getInfantOnlyColSeatsForSegmentDTO() {
		return infantOnlyColSeatsForSegmentDTO;
	}

	/**
	 * @param infantOnlyColSeatsForSegmentDTO
	 *            The infantOnlyColSeatsForSegmentDTO to set.
	 */
	private void setInfantOnlyColSeatsForSegmentDTO(Collection<SeatsForSegmentDTO> infantOnlyColSeatsForSegmentDTO) {
		this.infantOnlyColSeatsForSegmentDTO = infantOnlyColSeatsForSegmentDTO;
	}

	/**
	 * @return Returns the adultOrChildOrParentColSeatsForSegmentDTO.
	 */
	private Collection<SeatsForSegmentDTO> getAdultOrChildOrParentColSeatsForSegmentDTO() {
		return adultOrChildOrParentColSeatsForSegmentDTO;
	}

	private Collection<Integer> getRemovedPnrPaxFareIds() {
		return removedPnrPaxFareIds;
	}

	/**
	 * @param adultOrChildOrParentColSeatsForSegmentDTO
	 *            The adultOrChildOrParentColSeatsForSegmentDTO to set.
	 */
	private void setAdultOrChildOrParentColSeatsForSegmentDTO(
			Collection<SeatsForSegmentDTO> adultOrChildOrParentColSeatsForSegmentDTO) {
		this.adultOrChildOrParentColSeatsForSegmentDTO = adultOrChildOrParentColSeatsForSegmentDTO;
	}

	/**
	 * @return the removeInfantParentPaxFareIds
	 */
	public Collection<Integer> getRemoveInfantParentPaxFareIds() {
		return removeInfantParentPaxFareIds;
	}

	/**
	 * @param removeInfantParentPaxFareIds
	 *            the removeInfantParentPaxFareIds to set
	 */
	public void setRemoveInfantParentPaxFareIds(Collection<Integer> removeInfantParentPaxFareIds) {
		this.removeInfantParentPaxFareIds = removeInfantParentPaxFareIds;
	}

	/**
	 * Assing the parent's cabin class to the infant. Remember infants won't have any booking codes
	 * 
	 * @param colSegmentSeatDistsDTOForRelease
	 * @param removeInfantBCMap
	 * @throws ModuleException
	 */
	private void assignCabinClass(Collection<SegmentSeatDistsDTOForRelease> colSegmentSeatDistsDTOForRelease,
			Map<Integer, String> removeInfantBCMap) throws ModuleException {

		// If Any SegmentSeatDistsDTOForRelease exist
		if (colSegmentSeatDistsDTOForRelease.size() > 0) {
			BookingClassBD bookingClassBD = ReservationModuleUtils.getBookingClassBD();
			Iterator<SegmentSeatDistsDTOForRelease> itColSegmentSeatDistsDTOForRelease = colSegmentSeatDistsDTOForRelease
					.iterator();
			Collection<String> colBookingCodes = new ArrayList<String>();
			SegmentSeatDistsDTOForRelease segmentSeatDistsDTOForRelease;
			String bookingCode;
			BookingClass bookingClass;

			while (itColSegmentSeatDistsDTOForRelease.hasNext()) {
				segmentSeatDistsDTOForRelease = itColSegmentSeatDistsDTOForRelease.next();
				bookingCode = removeInfantBCMap.get(new Integer(segmentSeatDistsDTOForRelease.getFlightSegId()));
				colBookingCodes.add(bookingCode);
			}

			// Get the booking codes
			HashMap<String, BookingClass> mapBookingCodes = bookingClassBD.getBookingClassesMap(colBookingCodes);

			itColSegmentSeatDistsDTOForRelease = colSegmentSeatDistsDTOForRelease.iterator();
			while (itColSegmentSeatDistsDTOForRelease.hasNext()) {
				segmentSeatDistsDTOForRelease = itColSegmentSeatDistsDTOForRelease.next();
				bookingCode = removeInfantBCMap.get(new Integer(segmentSeatDistsDTOForRelease.getFlightSegId()));
				bookingClass = mapBookingCodes.get(bookingCode);
				segmentSeatDistsDTOForRelease.setBookingClassType(bookingClass.getBcType());
				segmentSeatDistsDTOForRelease.setLogicalCCCode(bookingClass.getLogicalCCCode());
			}
		}
	}

	/**
	 * Release Block Seats Permanently
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("static-access")
	public ReservationAudit releaseInventory() throws ModuleException {

		ReservationAudit reservationAudit = null;

		// Releasing for Adults or Parents
		if (this.getAdultOrChildOrParentColSeatsForSegmentDTO() != null
				&& this.getAdultOrChildOrParentColSeatsForSegmentDTO().size() > 0) {
			Collection<SegmentSeatDistsDTOForRelease> segmentSeatDistributionForRelease = this
					.getSegmentSeatDistribution(this.getAdultOrChildOrParentColSeatsForSegmentDTO());
			ReservationBO.releaseSoldOrOnholdFlightInventory(segmentSeatDistributionForRelease);
		}

		Collection<Integer> pnrPaxFareIds = this.getRemovedPnrPaxFareIds();
		if (pnrPaxFareIds != null && pnrPaxFareIds.size() > 0) {
			Collection<Integer> flightSeats = ReservationBO.trackSeatMapCancellations(pnrPaxFareIds);
			Collection<Integer> flightMeals = ReservationBO.trackMealCancellations(pnrPaxFareIds);
			// TODO this method name should change to reflect the correct functionality.
			Collection<Integer> flightBaggages = BaggageAdaptor.trackBaggageCancellations(pnrPaxFareIds);
			reservationAudit = ReservationBO.releaseAncillaries(flightSeats, flightMeals, flightBaggages);
		}

		// Releasing for infants
		if (this.getInfantOnlyColSeatsForSegmentDTO() != null && this.getInfantOnlyColSeatsForSegmentDTO().size() > 0) {
			if (this.getInfantOnlyBCMap() == null || this.getInfantOnlyBCMap().size() == 0) {
				throw new ModuleException("airreservations.arg.invalidSplit");
			}

			Collection<SegmentSeatDistsDTOForRelease> colSegmentSeatDistsDTOForRelease = this
					.getSegmentSeatDistribution(this.getInfantOnlyColSeatsForSegmentDTO());

			this.assignCabinClass(colSegmentSeatDistsDTOForRelease, this.getInfantOnlyBCMap());

			ReservationBO.releaseSoldOrOnholdFlightInventory(colSegmentSeatDistsDTOForRelease);

			Collection<Integer> removeInfantParentPaxFareIds = this.getRemoveInfantParentPaxFareIds();

			// Remember this gets triggered via leading to isolated infant(s) scenario
			// In this case the earlier parent's should be notified as adults for the seat map API.
			// Otherwise those people will be still treated as parents.
			if (removeInfantParentPaxFareIds != null && removeInfantParentPaxFareIds.size() > 0) {
				Collection<PassengerSeating> passengerSeating = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
						.getFlightSeatsForPnrPaxFare(removeInfantParentPaxFareIds);

				if (passengerSeating != null && passengerSeating.size() > 0) {
					Map<Integer, String> effectiveFltAmSeatIdsPaxType = new HashMap<Integer, String>();
					for (PassengerSeating ps : passengerSeating) {
						effectiveFltAmSeatIdsPaxType.put(ps.getFlightSeatId(), ReservationInternalConstants.PassengerType.ADULT);
					}

					if (effectiveFltAmSeatIdsPaxType != null && effectiveFltAmSeatIdsPaxType.size() > 0) {
						ReservationModuleUtils.getSeatMapBD().updatePaxType(effectiveFltAmSeatIdsPaxType, null);
					}
				}
			}

		}

		return reservationAudit;
	}

	/**
	 * Returns the Pnr Pax Fare Id(s)
	 * 
	 * @param adultOrChildOrParentColSeatsForSegmentDTO2
	 * @return
	 */
	private Collection<Integer> getPnrPaxFareIds(Collection<SeatsForSegmentDTO> adultOrChildOrParentColSeatsForSegmentDTO2) {
		Collection<Integer> pnrPaxFareIds = new HashSet<Integer>();
		SeatsForSegmentDTO seatsForSegmentDTO;

		for (Iterator<SeatsForSegmentDTO> itColSeatsForSegmentDTO = adultOrChildOrParentColSeatsForSegmentDTO2
				.iterator(); itColSeatsForSegmentDTO.hasNext();) {
			seatsForSegmentDTO = itColSeatsForSegmentDTO.next();
			pnrPaxFareIds.add(seatsForSegmentDTO.getPnrPaxFareId());
		}

		return pnrPaxFareIds;
	}

	/**
	 * Return Segment Seat Distribution in order to release from the inventory
	 * 
	 * @param colSeatsForSegmentDTO
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected static Collection<SegmentSeatDistsDTOForRelease>
			getSegmentSeatDistribution(Collection<SeatsForSegmentDTO> colSeatsForSegmentDTO) {
		final Map<Integer, SegmentSeatDistsDTOForRelease> mapFlightSegmentWithSeats = new HashMap<Integer, SegmentSeatDistsDTOForRelease>();
		final Map<Integer, Map> mapFlgSegWithBookingCodesMap = new HashMap<Integer, Map>();

		SegmentSeatDistsDTOForRelease segmentSeatDistsDTOForRelease;
		SeatDistForRelease seatDistForRelease;
		SeatDistForRelease olderSeatDistForRelease;
		SeatsForSegmentDTO seatsForSegmentDTO;
		Integer flightSegmentId;
		Map<String, SeatDistForRelease> mapBookingCodeWithSeats;

		for (Iterator<SeatsForSegmentDTO> itColSeatsForSegmentDTO = colSeatsForSegmentDTO.iterator(); itColSeatsForSegmentDTO
				.hasNext();) {
			seatsForSegmentDTO = itColSeatsForSegmentDTO.next();

			flightSegmentId = new Integer(seatsForSegmentDTO.getFlightSegmentId());

			segmentSeatDistsDTOForRelease = mapFlightSegmentWithSeats.get(flightSegmentId);

			// ######### Performing the Segment #########
			if (segmentSeatDistsDTOForRelease == null) {
				segmentSeatDistsDTOForRelease = new SegmentSeatDistsDTOForRelease();
				segmentSeatDistsDTOForRelease.setFlightSegId(flightSegmentId.intValue());

				// If it's a onhold seat
				if (seatsForSegmentDTO.isOnHoldSeat()) {
					// Infant
					if (!seatsForSegmentDTO.isWaitListedSeat()) {
						segmentSeatDistsDTOForRelease.setOnholdInfantSeatsForRelease(seatsForSegmentDTO.getNoOfInfants());
					} else {
						segmentSeatDistsDTOForRelease.setOnholdInfantSeatsForRelease(0);
					}
					segmentSeatDistsDTOForRelease.setSoldInfantSeatsForRelease(0);
					// If it's a sold seat
				} else {
					// Infant
					segmentSeatDistsDTOForRelease.setOnholdInfantSeatsForRelease(0);
					if (!seatsForSegmentDTO.isWaitListedSeat()) {
						segmentSeatDistsDTOForRelease.setSoldInfantSeatsForRelease(seatsForSegmentDTO.getNoOfInfants());
					} else {
						segmentSeatDistsDTOForRelease.setSoldInfantSeatsForRelease(0);
					}

				}

				mapBookingCodeWithSeats = new HashMap<String, SeatDistForRelease>();
				mapBookingCodeWithSeats.put(seatsForSegmentDTO.getBookingCode(), null);

				mapFlgSegWithBookingCodesMap.put(flightSegmentId, mapBookingCodeWithSeats);
				mapFlightSegmentWithSeats.put(flightSegmentId, segmentSeatDistsDTOForRelease);
			} else {
				// If it's a onhold seat
				if (seatsForSegmentDTO.isOnHoldSeat()) {
					// Infant
					if (!seatsForSegmentDTO.isWaitListedSeat()) {
						segmentSeatDistsDTOForRelease
								.setOnholdInfantSeatsForRelease(segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease()
										+ seatsForSegmentDTO.getNoOfInfants());
					} else {
						segmentSeatDistsDTOForRelease.setOnholdInfantSeatsForRelease(0);
					}
					segmentSeatDistsDTOForRelease.setSoldInfantSeatsForRelease(0);
					// If it's a sold seat
				} else {
					// Infant
					segmentSeatDistsDTOForRelease.setOnholdInfantSeatsForRelease(0);

					if (!seatsForSegmentDTO.isWaitListedSeat()) {
						segmentSeatDistsDTOForRelease
								.setSoldInfantSeatsForRelease(segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease()
										+ seatsForSegmentDTO.getNoOfInfants());
					} else {
						segmentSeatDistsDTOForRelease.setSoldInfantSeatsForRelease(0);
					}

				}
			}

			// ######### Performing the Booking Codes #########
			seatDistForRelease = new SeatDistForRelease();
			seatDistForRelease.setBookingCode(seatsForSegmentDTO.getBookingCode());

			if (seatsForSegmentDTO.isWaitListedSeat()) {
				seatDistForRelease.setWaitListed(true);
			}
			// If it's a onhold seat
			if (seatsForSegmentDTO.isOnHoldSeat()) {
				// Adult
				seatDistForRelease.setOnHoldSeats(seatsForSegmentDTO.getNoOfAdults());
				seatDistForRelease.setSoldSeats(0);
				// If it's a sold seat
			} else {
				// Adult
				seatDistForRelease.setOnHoldSeats(0);
				seatDistForRelease.setSoldSeats(seatsForSegmentDTO.getNoOfAdults());
			}

			mapBookingCodeWithSeats = mapFlgSegWithBookingCodesMap.get(flightSegmentId);
			olderSeatDistForRelease = mapBookingCodeWithSeats.get(seatsForSegmentDTO.getBookingCode());

			// This means no SeatDistForRelease record exist previously
			if (olderSeatDistForRelease == null) {
				olderSeatDistForRelease = seatDistForRelease;
				mapBookingCodeWithSeats.put(seatsForSegmentDTO.getBookingCode(), olderSeatDistForRelease);
			} else {
				olderSeatDistForRelease
						.setOnHoldSeats(olderSeatDistForRelease.getOnHoldSeats() + seatDistForRelease.getOnHoldSeats());
				olderSeatDistForRelease.setSoldSeats(olderSeatDistForRelease.getSoldSeats() + seatDistForRelease.getSoldSeats());
			}
		}

		Collection<SegmentSeatDistsDTOForRelease> allColSegmentSeatDistsDTOForRelease = new ArrayList<SegmentSeatDistsDTOForRelease>();
		Collection<SeatDistForRelease> allColSeatDistForRelease;

		for (Iterator<Integer> itMapFlightSegmentWithSeats = mapFlightSegmentWithSeats.keySet()
				.iterator(); itMapFlightSegmentWithSeats.hasNext();) {
			flightSegmentId = itMapFlightSegmentWithSeats.next();
			segmentSeatDistsDTOForRelease = mapFlightSegmentWithSeats.get(flightSegmentId);
			mapBookingCodeWithSeats = mapFlgSegWithBookingCodesMap.get(flightSegmentId);
			allColSeatDistForRelease = new ArrayList<SeatDistForRelease>();

			// Getting the values for booking codes
			for (Iterator<SeatDistForRelease> itCollection = mapBookingCodeWithSeats.values().iterator(); itCollection
					.hasNext();) {
				seatDistForRelease = itCollection.next();
				allColSeatDistForRelease.add(seatDistForRelease);
			}

			segmentSeatDistsDTOForRelease.setSeatDistributionsForRelease(allColSeatDistForRelease);

			allColSegmentSeatDistsDTOForRelease.add(segmentSeatDistsDTOForRelease);
		}

		return allColSegmentSeatDistsDTOForRelease;
	}

	/**
	 * @return the mapPerPaxWiseRevenue
	 */
	public Map<Integer, RevenueDTO> getMapPerPaxWiseRevenue() {
		return mapPerPaxWiseRevenue;
	}

	public Set<Integer> getCommissionRemovalExcludedFareIds() {
		return commissionRemovalExcludedFareIds;
	}

	public CustomChargesTO getCustomChargesTO() {
		return customChargesTO;
	}

	public void setCustomChargesTO(CustomChargesTO customChargesTO) {
		this.customChargesTO = customChargesTO;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	private BigDecimal updateServiceTaxPenalty(ReservationPaxFare reservationPaxFareAdult, BigDecimal identifiedPenaltyCharge,
			FlownAssitUnit flownAsstUnit, boolean isOnlyCancellation, Integer transactionSeq) throws ModuleException {
		try {
			if (identifiedPenaltyCharge.compareTo(BigDecimal.ZERO) > 0) {

				Collection<String> onTicketingRevenueChargeGroups = AppSysParamsUtil
						.getChargeGroupsToQuoteGSTOnNonTicketingRevenue();

				if (onTicketingRevenueChargeGroups != null
						&& onTicketingRevenueChargeGroups.contains(ReservationInternalConstants.ChargeGroup.PEN)) {
					if (credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_AGENT)
							|| credentialsDTO.getSalesChannelCode().equals(SalesChannelsUtil.SALES_CHANNEL_LCC)) {

						UserPrincipal userPrincipal = composeUserPrincipal();

						ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = ReservationApiUtils
								.createPaxWiseNonTicketingServiceTaxQuoteRQForPenalty(identifiedPenaltyCharge,
										reservationPaxFareAdult.getReservationPax(), isOnlyCancellation);

						ServiceTaxQuoteForNonTicketingRevenueRS nonTicketingServiceTaxRS = ReservationModuleUtils
								.getAirproxyReservationBD().quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO,
										credentialsDTO.getTrackInfoDTO(), userPrincipal);

						if (nonTicketingServiceTaxRS != null && nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes() != null
								&& nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1) != null
								&& AccelAeroCalculator.isGreaterThan(
										nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
												.getEffectiveCnxModPenChargeAmount(),
										AccelAeroCalculator.getDefaultBigDecimalZero())) {
							// updating pax fare penalty amount
							if (isOnlyCancellation) {
								identifiedPenaltyCharge = nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
										.getEffectiveCnxModPenChargeAmount();
							}

							for (ServiceTaxDTO serviceTaxDTO : nonTicketingServiceTaxRS.getPaxWiseCnxModPenServiceTaxes().get(1)
									.getCnxModPenServiceTaxes()) {

								ServiceTaxExtChgDTO serviceTaxExtChg = (ServiceTaxExtChgDTO) getServiceTax(
										serviceTaxDTO.getChargeCode()).clone();

								PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
										serviceTaxDTO.getNonTaxableAmount(), serviceTaxDTO.getTaxableAmount(),
										ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY,
										nonTicketingServiceTaxRS.getServiceTaxDepositeCountryCode(),
										nonTicketingServiceTaxRS.getServiceTaxDepositeStateCode());

								ReservationPaxOndCharge serviceTaxPenaltyOndCharge = TOAssembler.createReservationPaxOndCharge(
										serviceTaxDTO.getAmount(), null, serviceTaxExtChg.getChgRateId(),
										serviceTaxExtChg.getChgGrpCode(), credentialsDTO, false, 0, null, null, paxOndChargeDTO,
										transactionSeq);
								reservationPaxFareAdult.addCharge(serviceTaxPenaltyOndCharge);
							}

						}
					}
				} else {

					ReservationPax reservationPax = reservationPaxFareAdult.getReservationPax();
					ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ = ReservationApiUtils
							.createPaxWiseTicketingServiceTaxQuoteRQForPenalty(identifiedPenaltyCharge, flownAsstUnit,
									reservationPax, isOnlyCancellation);
					ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketingRS = ReservationModuleUtils.getChargeBD()
							.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);

					BigDecimal paxServiceTaxForPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (serviceTaxForTicketingRS != null && serviceTaxForTicketingRS.getPaxWiseServiceTaxes() != null
							&& !serviceTaxForTicketingRS.getPaxWiseServiceTaxes().isEmpty()) {

						for (ServiceTaxDTO serviceTaxDTO : serviceTaxForTicketingRS.getPaxWiseServiceTaxes()
								.get(reservationPax.getPaxSequence())) {

							ServiceTaxExtChgDTO serviceTaxExtChg = (ServiceTaxExtChgDTO) getServiceTax(
									serviceTaxDTO.getChargeCode()).clone();

							PaxOndChargeDTO paxOndChargeDTO = TOAssembler.createPaxOndChargeTaxInfoDTO(
									serviceTaxDTO.getNonTaxableAmount(), serviceTaxDTO.getTaxableAmount(),
									ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY,
									serviceTaxForTicketingRS.getServiceTaxDepositeCountryCode(),
									serviceTaxForTicketingRS.getServiceTaxDepositeStateCode());

							ReservationPaxOndCharge serviceTaxPenaltyOndCharge = TOAssembler.createReservationPaxOndCharge(
									serviceTaxDTO.getAmount(), null, serviceTaxExtChg.getChgRateId(),
									serviceTaxExtChg.getChgGrpCode(), credentialsDTO, false, 0, null, null, paxOndChargeDTO,
									transactionSeq);
							reservationPaxFareAdult.addCharge(serviceTaxPenaltyOndCharge);

							paxServiceTaxForPenalty = AccelAeroCalculator.add(paxServiceTaxForPenalty, serviceTaxDTO.getAmount());

						}
					}

					if (isOnlyCancellation) {
						identifiedPenaltyCharge = AccelAeroCalculator.scaleValueDefault(
								AccelAeroCalculator.subtract(identifiedPenaltyCharge, paxServiceTaxForPenalty));
					}

				}

			}
		} catch (Exception e) {
			log.error("ERROR @ updateServiceTaxPenalty");
			e.printStackTrace();
		}

		return identifiedPenaltyCharge;
	}

	private ServiceTaxExtChgDTO getServiceTax(String chargeCode) throws ModuleException {

		ServiceTaxExtChgDTO externalChg = (ServiceTaxExtChgDTO) ((ServiceTaxExtCharges) getExternalChargesMap()
				.get(EXTERNAL_CHARGES.SERVICE_TAX)).getRespectiveServiceChgDTO(chargeCode);
		if (externalChg != null) {
			externalChg = (ServiceTaxExtChgDTO) externalChg.clone();
		}
		return externalChg;
	}

	private Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap() throws ModuleException {

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChargeMap = new HashMap<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO>();
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SERVICE_TAX);

		extChargeMap = ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MODIFY_ONLY);

		return extChargeMap;

	}

	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}

	public UserPrincipal getUserPrincipal() {
		return userPrincipal;
	}

	public void setUserPrincipal(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	private UserPrincipal getComposedUserPrincipal() {
		if (this.userPrincipal != null) {
			return this.userPrincipal;
		} else if (credentialsDTO != null) {
			return composeUserPrincipal();
		}
		return null;
	}

	private UserPrincipal composeUserPrincipal() {
		return (UserPrincipal) userPrincipal.createIdentity(null, credentialsDTO.getSalesChannelCode(),
				credentialsDTO.getAgentCode(), credentialsDTO.getAgentStation(), null, credentialsDTO.getUserId(),
				credentialsDTO.getColUserDST(), credentialsDTO.getPassword(), null, null, null, null,
				credentialsDTO.getAgentCurrencyCode(), null, null, null, null);
	}
}
