/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.service.CreditAccountBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to acqure crecdit
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="debitAccout"
 */
public class DebitAccount extends DefaultBaseCommand {

	// Dao's
	private ReservationTnxDAO reservationTnxDao;

	// BD's
	private CreditAccountBD creditAccountBD;

	/**
	 * constructor of the AcquireCredit command
	 */
	public DebitAccount() {

		// looking up daos
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;

		// looking up bd's
		creditAccountBD = ReservationModuleUtils.getCreditAccountBD();
	}

	/**
	 * execute method of the AcquireCredit command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		BigDecimal amount = (BigDecimal) this.getParameter(CommandParamNames.AMOUNT);
		Integer actionNC = (Integer) this.getParameter(CommandParamNames.ACTION_NC);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		// check params
		this.checkParams(pnrPaxId, amount, actionNC, credentialsDTO);

		ReservationTnx debitTnx = TnxFactory.getDebitInstance(pnrPaxId, amount, actionNC.intValue(), credentialsDTO, new Date(),
				null, null, false);
		reservationTnxDao.saveTransaction(debitTnx);

		BigDecimal pnrPaxBalance = reservationTnxDao.getPNRPaxBalance(pnrPaxId);

		if (pnrPaxBalance != null && pnrPaxBalance.doubleValue() < 0) {
			creditAccountBD.balanceCredit(pnrPaxId, pnrPaxBalance.negate());

		} else if (pnrPaxBalance != null && 0 <= pnrPaxBalance.doubleValue()) {
			creditAccountBD.balanceCredit(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param credentialsDTO
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(String pnrPaxId, BigDecimal amount, Integer actionNC, CredentialsDTO credentialsDTO)
			throws ModuleException {

		if (pnrPaxId == null || amount == null || actionNC == null || credentialsDTO == null)// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}

}
