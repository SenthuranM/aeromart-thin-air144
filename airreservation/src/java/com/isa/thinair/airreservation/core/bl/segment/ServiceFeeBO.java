package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airpricing.api.dto.CnxModPenServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

public class ServiceFeeBO {

	private UserPrincipal userPrincipal;
	private ReservationContactInfo contactInfo;
	private String taxDepositCountry;
	private String taxDepositState;
	private Map<Integer, ReservationPax> paxMap;
	private Collection<OndFareDTO> ondFares;
	private List<ReservationSegmentDTO> confirmedSegments;

	public ServiceFeeBO(Reservation reservation, UserPrincipal userPrincipal, Collection<OndFareDTO> ondFares) {
		this.contactInfo = reservation.getContactInfo();
		this.confirmedSegments = ReservationApiUtils.getConfirmedSegments(reservation.getSegmentsView());
		paxMap = new HashMap<Integer, ReservationPax>();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			paxMap.put(reservationPax.getPnrPaxId(), reservationPax);
		}
		this.userPrincipal = userPrincipal;
		this.ondFares = ondFares;
	}

	public boolean isExtraFeeApplicable() {
		return true;
	}

	public FeeHolder getFeeHolder(BigDecimal amount, BigDecimal maxAmount, String chargeGroup, String cabinClassCode,
			Integer pnrPaxId) throws ModuleException {

		FeeHolder feeHolder = null;
		if (amount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			CnxModPenServiceTaxDTO taxDTO = getCnxModPenServiceTaxes(chargeGroup, amount, cabinClassCode, pnrPaxId, false);
			if (taxDTO != null) {
				if (maxAmount != null) {
					BigDecimal totalWithTax = AccelAeroCalculator.add(amount, taxDTO.getTotalTaxAmount());
					if (totalWithTax.compareTo(maxAmount) > 0) {
						taxDTO = getCnxModPenServiceTaxes(chargeGroup, maxAmount, cabinClassCode, pnrPaxId, true);
					}
				}
				feeHolder = new FeeHolder(taxDTO, taxDepositCountry, taxDepositState);
			}
		}

		if (feeHolder == null) {
			feeHolder = new FeeHolder(amount);
		}
		return feeHolder;
	}

	private CnxModPenServiceTaxDTO getCnxModPenServiceTaxes(String chargeGroup, BigDecimal amount, String cabinClassCode,
			Integer pnrPaxId, boolean includeServiceTaxForTotal) throws ModuleException {

		CnxModPenServiceTaxDTO cnxModPenServiceTaxDTO = null;
		Collection<String> nonTicketingRevenueChargeGroups = AppSysParamsUtil.getChargeGroupsToQuoteGSTOnNonTicketingRevenue();

		if (nonTicketingRevenueChargeGroups != null && nonTicketingRevenueChargeGroups.contains(chargeGroup)) {
			ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForNonTktDTO();
			Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges = new HashMap<>();

			PaxCnxModPenChargeForServiceTaxDTO paxCnxModPenChargeForServiceTaxDTO = new PaxCnxModPenChargeForServiceTaxDTO();

			LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
			chgDTO.setCode(chargeGroup);
			chgDTO.setAmount(amount);

			List<LCCClientExternalChgDTO> chgs = new ArrayList<>();
			chgs.add(chgDTO);

			paxCnxModPenChargeForServiceTaxDTO.setCharges(chgs);
			paxCnxModPenChargeForServiceTaxDTO.setTotalAmountIncludingServiceTax(includeServiceTaxForTotal);
			paxWiseCharges.put(1, paxCnxModPenChargeForServiceTaxDTO);
			serviceTaxQuoteCriteriaDTO.setPrefSystem(ProxyConstants.SYSTEM.AA);
			serviceTaxQuoteCriteriaDTO.setPaxWiseCharges(paxWiseCharges);
			serviceTaxQuoteCriteriaDTO.setPaxCountryCode(contactInfo.getCountryCode());
			serviceTaxQuoteCriteriaDTO.setPaxState(contactInfo.getState());

			ServiceTaxQuoteForNonTicketingRevenueRS serviceTaxForNonTicketingRevenue = ReservationModuleUtils
					.getAirproxyReservationBD().quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO, null,
							userPrincipal);

			if (serviceTaxForNonTicketingRevenue.getPaxWiseCnxModPenServiceTaxes() != null
					&& serviceTaxForNonTicketingRevenue.getPaxWiseCnxModPenServiceTaxes().containsKey(1)) {
				cnxModPenServiceTaxDTO = serviceTaxForNonTicketingRevenue.getPaxWiseCnxModPenServiceTaxes().get(1);
			}
			this.taxDepositCountry = serviceTaxForNonTicketingRevenue.getServiceTaxDepositeCountryCode();
			this.taxDepositState = serviceTaxForNonTicketingRevenue.getServiceTaxDepositeStateCode();

		} else {
			if (ondFares != null || ReservationInternalConstants.ChargeGroup.CNX.equals(chargeGroup)) {
				ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = new ServiceTaxQuoteForTicketingRevenueRQ();

				List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = getSimplifiedFlightSegmentDTOs();
				Collections.sort(simplifiedFlightSegmentDTOs);
				int segmentSequence = 0;
				String flightRefNumber = null;
				for (SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO : simplifiedFlightSegmentDTOs) {
					simplifiedFlightSegmentDTO.setSegmentSequence(segmentSequence);
					segmentSequence++;
					if (flightRefNumber == null) {
						flightRefNumber = simplifiedFlightSegmentDTO.getFlightRefNumber();
					}
				}
				serviceTaxRQ.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);

				Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = new HashMap<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>();
				List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
				SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
				paxDTO.setPaxSequence(paxMap.get(pnrPaxId).getPaxSequence());
				paxDTO.setPaxType(paxMap.get(pnrPaxId).getPaxType());
				paxDTO.setTotalAmountIncludingServiceTax(includeServiceTaxForTotal);
				SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
				chargeDTO.setAmount(amount);
				chargeDTO.setChargeGroupCode(chargeGroup);
				chargeDTO.setChargeCode(chargeGroup);
				chargeDTO.setFlightRefNumber(flightRefNumber);
				chargesList.add(chargeDTO);
				paxWiseCharges.put(paxDTO, chargesList);
				serviceTaxRQ.setPaxWiseExternalCharges(paxWiseCharges);

				serviceTaxRQ.setPaxState(contactInfo.getState());
				serviceTaxRQ.setPaxCountryCode(contactInfo.getCountryCode());
				serviceTaxRQ.setPaxTaxRegistered(contactInfo.getTaxRegNo() != null ? true : false);
				if (userPrincipal.getSalesChannel() == SalesChannelsUtil.SALES_CHANNEL_GDS) {
					serviceTaxRQ.setChannelCode(SalesChannelsUtil
							.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
				} else {
					serviceTaxRQ.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil
							.getSalesChannelName(userPrincipal.getSalesChannel())));
				}
				ServiceTaxQuoteForTicketingRevenueRS serviceTaxForTicketing = ReservationModuleUtils.getChargeBD()
						.quoteServiceTaxForTicketingRevenue(serviceTaxRQ);
				if (serviceTaxForTicketing.getPaxWiseServiceTaxes() != null
						&& !serviceTaxForTicketing.getPaxWiseServiceTaxes().isEmpty()) {
					cnxModPenServiceTaxDTO = new CnxModPenServiceTaxDTO();
					List<ServiceTaxDTO> taxList = new ArrayList<ServiceTaxDTO>();
					BigDecimal effectiveTax = null;
					for (ServiceTaxDTO tax : serviceTaxForTicketing.getPaxWiseServiceTaxes().get(paxMap.get(pnrPaxId).getPaxSequence())) {
						tax.setFlightRefNumber(null);
						if (effectiveTax == null) {
							effectiveTax = tax.getTaxableAmount();
						}
						taxList.add(tax);
					}
					cnxModPenServiceTaxDTO.setCnxModPenServiceTaxes(taxList);
					cnxModPenServiceTaxDTO.setEffectiveCnxModPenChargeAmount(effectiveTax);
				}
				this.taxDepositCountry = serviceTaxForTicketing.getServiceTaxDepositeCountryCode();
				this.taxDepositState = serviceTaxForTicketing.getServiceTaxDepositeStateCode();
			}
		}
		return cnxModPenServiceTaxDTO;
	}

	private List<SimplifiedFlightSegmentDTO> getSimplifiedFlightSegmentDTOs() {
		Map<Integer, Collection<OndFareDTO>> jouneyONDWiseFareONDs = new HashMap<>();
		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<>();
		if (ondFares != null) {
			for (OndFareDTO ondFareDTO : ondFares) {
				if (jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()) == null) {
					jouneyONDWiseFareONDs.put(ondFareDTO.getOndSequence(), new ArrayList<>());
				}
				jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
			}
			List<Collection<OndFareDTO>> jouneyONDWiseFareONDList = new ArrayList<>(jouneyONDWiseFareONDs.values());
			Map<Integer, SimplifiedFlightSegmentDTO> fltSegIdSegments = new HashMap<>();

			boolean isReturnFareEligible = isReturnFareEligible(jouneyONDWiseFareONDList);
			for (OndFareDTO ondFare : ondFares) {

				Map<Integer, SegmentSeatDistsDTO> fltSegIdSD = new HashMap<>();
				for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
					fltSegIdSD.put(segSeatDist.getFlightSegId(), segSeatDist);
				}
				boolean returnFlag = isReturnFareEligible && ondFare.getOndSequence() == OndSequence.IN_BOUND;
				for (FlightSegmentDTO flightSegmentTO : ondFare.getSegmentsMap().values()) {
					SegmentSeatDistsDTO segSeatDist = fltSegIdSD.get(flightSegmentTO.getSegmentId());
					if (segSeatDist != null) {
						SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
						simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
						simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
						simplifiedFlightSegmentDTO.setOndSequence(ondFare.getOndSequence());
						simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
						simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
						simplifiedFlightSegmentDTO.setLogicalCabinClassCode(segSeatDist.getLogicalCabinClass());
						simplifiedFlightSegmentDTO.setReturnFlag(returnFlag);
						simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getCarrierCode());
						fltSegIdSegments.put(flightSegmentTO.getSegmentId(), simplifiedFlightSegmentDTO);
						simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
					}
				}
			}
		} else if (confirmedSegments != null) {
			for (ReservationSegmentDTO reservationSegmentDTO : confirmedSegments) {
				SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
				simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(reservationSegmentDTO.getZuluDepartureDate());
				simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(reservationSegmentDTO.getZuluArrivalDate());
				simplifiedFlightSegmentDTO.setSegmentCode(reservationSegmentDTO.getSegmentCode());
				simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(reservationSegmentDTO));
				simplifiedFlightSegmentDTO.setLogicalCabinClassCode(reservationSegmentDTO.getLogicalCCCode());
				simplifiedFlightSegmentDTO.setOperatingAirline(reservationSegmentDTO.getCarrierCode());
				simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
			}
		}
		return simplifiedFlightSegmentDTOs;
	}

	private boolean isReturnFareEligible(List<Collection<OndFareDTO>> jouneyONDWiseFareONDs) {

		if (jouneyONDWiseFareONDs.size() == 2) {
			List<FlightSegmentDTO> fltSegments = new ArrayList<>();
			for (OndFareDTO ondFare : jouneyONDWiseFareONDs.get(0)) {
				fltSegments.addAll(ondFare.getSegmentsMap().values());
			}
			Collections.sort(fltSegments);
			String outboundOrigin = fltSegments.get(0).getFromAirport();
			String outboundDestination = fltSegments.get(fltSegments.size() - 1).getToAirport();

			fltSegments = new ArrayList<>();
			for (OndFareDTO ondFare : jouneyONDWiseFareONDs.get(1)) {
				fltSegments.addAll(ondFare.getSegmentsMap().values());
			}
			Collections.sort(fltSegments);
			String inboundOrigin = fltSegments.get(0).getFromAirport();
			String inboundDestination = fltSegments.get(fltSegments.size() - 1).getToAirport();

			if (inboundOrigin.equals(outboundDestination) && outboundOrigin.equals(inboundDestination)) {
				return true;
			}
		}

		return false;
	}

}
