package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.FareConvertUtil;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AdjustCreditBO;
import com.isa.thinair.airreservation.core.bl.common.ProcessNoShowRules;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.segment.ChargeTnxSeqGenerator;
import com.isa.thinair.airreservation.core.bl.segment.ExtraFeeBO;
import com.isa.thinair.airreservation.core.bl.ticketing.ETicketBO;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PaxFinalSalesDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import org.springframework.util.CollectionUtils;

public class ReconcileReservationServices {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReconcileReservation.class);

	/** Holds the reservation segment dao instance */
	private ReservationSegmentDAO reservationSegmentDAO;

	private ETicketDAO eticketDAO;

	/** Hold the reservation bd instance **/
	private ReservationBD reservationBD;

	private PassengerBD passangerBD;

	/** Holds the flight inventory res bd **/
	private FlightInventoryResBD flightInventoryResBD;

	/** Holds fare bd **/
	private FareBD fareBD;
	/** Holds Segment bd **/
	// private SegmentBD segmentBD;

	/** Holds the passenger final sales dao instance */
	private PaxFinalSalesDAO paxFinalSalesDAO;

	private static final String NO_REC = "NO REC";
	private static final String NO_SHOW = "NO SHOW";
	private static final String GO_SHOW = "GO SHOW";
	private static final String OFF_LOAD = "OFF LOAD";

	public ReconcileReservationServices() {

		reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;

		reservationBD = ReservationModuleUtils.getReservationBD();

		flightInventoryResBD = ReservationModuleUtils.getFlightInventoryResBD();

		fareBD = ReservationModuleUtils.getFareBD();

		passangerBD = ReservationModuleUtils.getPassengerBD();

		// segmentBD = ReservationModuleUtils.getSegmentBD();

		paxFinalSalesDAO = ReservationDAOUtils.DAOInstance.PAX_FINAL_SALES_DAO;
	}

	public Object[] reconcilePfsEntry(PfsPaxEntry pfsParsedEntry, Collection<PfsPaxEntry> colErrorPfsParsedEntry,
			CredentialsDTO credentialsDTO, String checkinMethod, Map<Integer, FlightReconcileDTO> allFlightReconcileDTOs, Pfs pfs)
			throws ModuleException {

		ExternalChgDTO externalChgDTO = this.getNoShoreCharge();
		String taxInvoicePnr = null;

		ReservationAudit reservationAudit = new ReservationAudit();
		Collection<FlightReconcileDTO> colAllFlightReconcileDTO = new ArrayList<FlightReconcileDTO>();

		// GO SHOW
		if (ReservationInternalConstants.PfsPaxStatus.GO_SHORE.equals(pfsParsedEntry.getEntryStatus())) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_STATUS, GO_SHOW);
			FlightReconcileDTO flightSegReconcileData = this.getflightSegmentReconsilationData(pfsParsedEntry);
			this.processGoShow(pfsParsedEntry, flightSegReconcileData, colErrorPfsParsedEntry, credentialsDTO, reservationAudit);
			// no need to update pnr segments
		}
		// No Show
		else if (ReservationInternalConstants.PfsPaxStatus.NO_SHORE.equals(pfsParsedEntry.getEntryStatus())) {
			// AUDIT TYPE Entry
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_STATUS, NO_SHOW);
			Collection<FlightReconcileDTO> colFlightReconcileDTO = this.getSegmentsForReconcilation(pfsParsedEntry);
			colAllFlightReconcileDTO.addAll(colFlightReconcileDTO);
			taxInvoicePnr = processNoShow(pfsParsedEntry, colFlightReconcileDTO, colErrorPfsParsedEntry, externalChgDTO,
					credentialsDTO);
			this.updatePnrSegmentList(allFlightReconcileDTOs, colFlightReconcileDTO);
		}

		// OFFLK, OFFLN
		// Mahan requested to process this same as NO SHOW - AARESAA-7333
		else if (ReservationInternalConstants.PfsPaxStatus.OFF_LD.equals(pfsParsedEntry.getEntryStatus())) {
			// AUDIT TYPE Entry
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_STATUS, OFF_LOAD);
			// If passenger is off loaded process as no show
			Collection<FlightReconcileDTO> colFlightReconcileDTO = this.getSegmentsForReconcilation(pfsParsedEntry);
			colAllFlightReconcileDTO.addAll(colFlightReconcileDTO);
			this.processOffLoad(pfsParsedEntry, colFlightReconcileDTO, colErrorPfsParsedEntry, externalChgDTO, credentialsDTO);
			this.updatePnrSegmentList(allFlightReconcileDTOs, colFlightReconcileDTO);
		}

		// No Rec
		else if (ReservationInternalConstants.PfsPaxStatus.NO_REC.equals(pfsParsedEntry.getEntryStatus())) {
			// AUDIT TYPE EntryNO_REC_PAX
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_STATUS, NO_REC);
			// TODO Nili Nov 18, 2011
			// These statements causing a exception e.g. pnr not existing from the auto PFS flow.
			// will cause a exception and that will not be tracked with the PfsParsedEntry.
			// Please improve this.
			if (BeanUtils.nullHandler(pfsParsedEntry.getPnr()).length() > 0) {
				boolean isPaxFromDifferentFlight = this.isPaxFromDifferentFlight(pfs, pfsParsedEntry);
				// Only processing as norec if pax from different flight
				if (isPaxFromDifferentFlight) {
					// Transfer Passenger to correct flight
					this.transferPassengers(pfsParsedEntry, pfs, credentialsDTO);

					Collection<FlightReconcileDTO> colTargetFlightReconcileDTO = this.getSegmentsForReconcilation(pfs,
							pfsParsedEntry.getArrivalAirport());
					colAllFlightReconcileDTO.addAll(colTargetFlightReconcileDTO);
					// changing pfs parsed entry's flight date and flight number as it is transfered now
					pfsParsedEntry.setFlightDate(pfs.getDepartureDate());
					pfsParsedEntry.setFlightNumber(pfs.getFlightNumber());
					this.processNoRec(pfsParsedEntry, colTargetFlightReconcileDTO, colErrorPfsParsedEntry, externalChgDTO,
							credentialsDTO);
					this.updatePnrSegmentList(allFlightReconcileDTOs, colTargetFlightReconcileDTO);
				}
				// Infant details are not coming with auto pfs for the moment,
				// when manually processing at this stage adult already
				// transfered. so we have to set infant status correctly
				else if (pfsParsedEntry.getPaxType() != null
						&& pfsParsedEntry.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
					Collection<FlightReconcileDTO> colTargetFlightReconcileDTO = this.getSegmentsForReconcilation(pfs,
							pfsParsedEntry.getArrivalAirport());
					this.processNoRec(pfsParsedEntry, colTargetFlightReconcileDTO, colErrorPfsParsedEntry, externalChgDTO,
							credentialsDTO);
				}
				// When manually updating sometimes only one norec record might add and process, if that record from the
				// same flight, we have to load reconciledtos in order to update update flown pax correctly
				else if (allFlightReconcileDTOs.isEmpty() || !(isPaxFromDifferentFlight)) {
					Collection<FlightReconcileDTO> colTargetFlightReconcileDTO = this.getSegmentsForReconcilation(pfs,
							pfsParsedEntry.getArrivalAirport());
					this.updatePnrSegmentList(allFlightReconcileDTOs, colTargetFlightReconcileDTO);
					pfsParsedEntry.setErrorDescription(
							ReconcileReservationServices.ErrorDescription.PFS_NOREC_PROCESS_ERROR_SAME_FLIGHT);
					pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
					colErrorPfsParsedEntry.add(pfsParsedEntry);
				}
			} else {
				// If any error occured for this entry making it has error occured
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PNR_DOES_NOT_EXIST);
				pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
				colErrorPfsParsedEntry.add(pfsParsedEntry);
			}

		}

		// TODO remove this. This is wrong and not necassary
		// if (colErrorPfsParsedEntry.size() == 0) {
		// pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.PROCESSED);
		// } else {
		// pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
		// }

		paxFinalSalesDAO.savePfsParseEntry(pfsParsedEntry);

		// REservation Process Audit
		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		reservationAudit.setModificationType(AuditTemplateEnum.PAX_PFS_ENTRY.getCode());
		this.createPFSAudit(colReservationAudit, reservationAudit, pfsParsedEntry, pfs, colAllFlightReconcileDTO);
		// Record Audit Entries
		this.postPfsAuditRecord(colReservationAudit, credentialsDTO);

		Object values[] = new Object[4];
		values[0] = pfsParsedEntry;
		values[1] = allFlightReconcileDTOs;
		values[2] = colErrorPfsParsedEntry;
		values[3] = taxInvoicePnr;

		return values;

	}

	private FlightReconcileDTO getflightSegmentReconsilationData(PfsPaxEntry pfsParsedEntry) throws ModuleException {

		if (BeanUtils.nullHandler(pfsParsedEntry.getDepartureAirport()).isEmpty()
				|| BeanUtils.nullHandler(pfsParsedEntry.getArrivalAirport()).isEmpty()) {
			throw new ModuleException("airreservation.pfs.departure.and.arrival.empty");
		}

		return reservationSegmentDAO.getFlightSegmentReconcileDTO(pfsParsedEntry.getFlightNumber(),
				pfsParsedEntry.getFlightDate(), pfsParsedEntry.getDepartureAirport(), pfsParsedEntry.getArrivalAirport());

	}

	/**
	 * Return segments for reconcilation
	 * 
	 * @param pfsParsedEntry
	 * @return
	 * @throws ModuleException
	 */
	private Collection<FlightReconcileDTO> getSegmentsForReconcilation(PfsPaxEntry pfsParsedEntry) throws ModuleException {

		// Sometimes this will return invalid flightSegId if we have multi leg flight

		// Integer flightSegId = reservationSegmentDAO.getFlightSegmentId(pfsParsedEntry.getFlightNumber(),
		// pfsParsedEntry.getFlightDate(), pfsParsedEntry.getDepartureAirport(), pfsParsedEntry.getArrivalAirport());

		Collection<Integer> flightSegIds = reservationSegmentDAO.getFlightSegmentIds(pfsParsedEntry.getFlightNumber(),
				pfsParsedEntry.getFlightDate(), pfsParsedEntry.getDepartureAirport(), pfsParsedEntry.getArrivalAirport());

		return reservationSegmentDAO.getPnrSegmentsForReconcilation(flightSegIds, pfsParsedEntry.getPnr(), false);
	}

	private boolean isPaxFromDifferentFlight(Pfs pfs, PfsPaxEntry pfsParsedEntry) throws ModuleException {

		Collection<Integer> flightSegIds = reservationSegmentDAO.getFlightSegmentIds(pfs.getFlightNumber(),
				pfs.getDepartureDate(), pfsParsedEntry.getDepartureAirport(), pfsParsedEntry.getArrivalAirport());

		Collection<ReservationSegment> resSegments = reservationSegmentDAO.getPnrSegments(pfsParsedEntry.getPnr());
		List<ReservationSegment> matchingSegments = new ArrayList<ReservationSegment>();
		for (ReservationSegment resSeg : resSegments) {
			if (flightSegIds.contains(resSeg.getFlightSegId())
					&& resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				matchingSegments.add(resSeg);
			}
		}
		return matchingSegments.isEmpty();
	}

	/**
	 * Process Go Shore
	 * 
	 * @param pfsParsedEntry
	 * @param colFlightReconcileDTO
	 * @param colErrorPfsParsedEntry
	 * @param fromSegmentCode
	 * @throws ModuleException
	 */
	private void processGoShow(PfsPaxEntry pfsParsedEntry, FlightReconcileDTO reconcileDTO,
			Collection<PfsPaxEntry> colErrorPfsParsedEntry, CredentialsDTO credentialsDTO, ReservationAudit reservationAudit) {

		try {
			// Get a fare quote for go show
			ReconcilePassengersTO goShowPassengersTO = new ReconcilePassengersTO();

			// Tracks the number of passengers for Pax Types
			trackNoOfPassengersForPaxTypes(pfsParsedEntry.getPaxType(), goShowPassengersTO);

			String cabinClass = pfsParsedEntry.getCabinClassCode();
			// Need to get cabin class from the booking class if RBD level enabled.
			if (AppSysParamsUtil.isRBDPNLADLEnabled() && pfsParsedEntry.getBookingClassCode() != null) {
				BookingClass bc = ReservationModuleUtils.getBookingClassBD()
						.getBookingClass(pfsParsedEntry.getBookingClassCode());
				if (bc != null) {
					cabinClass = bc.getCabinClassCode();
				}
			}

			if (ReservationInternalConstants.PassengerType.INFANT.equals(pfsParsedEntry.getPaxType())
					&& pfsParsedEntry.getPnr() == null) {
				pfsParsedEntry.setPnr(reservationSegmentDAO.getParentPnr(pfsParsedEntry.getParentPpId()));

			}

			goShowPassengersTO.setCabinClassCode(cabinClass);
			goShowPassengersTO.setFlightId(reconcileDTO.getFlightId().intValue());
			goShowPassengersTO.setFlightNumber(pfsParsedEntry.getFlightNumber());
			goShowPassengersTO.setSegmentCode(reconcileDTO.getSegementCode());
			goShowPassengersTO.setFlightSegId(reconcileDTO.getFlightSegId().intValue());
			goShowPassengersTO.setDepartureDateTimeLocal(reconcileDTO.getDepartureDateTimeLocal());
			goShowPassengersTO.setDepartureDateTimeZulu(reconcileDTO.getDepartureDateTimeZulu());
			goShowPassengersTO.setArrivalDateTimeLocal(reconcileDTO.getArrivalDateTimeLocal());
			goShowPassengersTO.setArrivalDateTimeZulu(reconcileDTO.getArrivalDateTimeZulu());
			goShowPassengersTO.setPaxCategoryCode(getGoShorePaxCategoryCode(pfsParsedEntry.getPaxCategoryCode()));
			injectParentSegmentIdforInfant(goShowPassengersTO, pfsParsedEntry);

			Agent goshoAgent = this.getGoshoAgent(pfsParsedEntry.getDepartureAirport());
			String agentCode = goshoAgent.getAgentCode();
			goShowPassengersTO.setAgentCode(agentCode);
			goShowPassengersTO.setPosStation(goshoAgent.getStationCode());

			// Get the ond fare DTO(s)
			Collection<OndFareDTO> ondFareDTOs = flightInventoryResBD.quoteGoshowFares(goShowPassengersTO);
			
			Collection<ExternalChgDTO> colExternalChgDTOs = calculateServiceTax(ondFareDTOs, pfsParsedEntry);
			 
			// Return the agent code
			PayCurrencyDTO payCurrencyDTO = null;
			String agentCurrencyCode = goshoAgent.getCurrencyCode();

			if (AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled()) {
				CurrencyExchangeRate currExRate = getCurrencyExchangeRate(agentCurrencyCode);
				if (currExRate == null) {
					throw new ModuleException("airmaster.exchangerate.undefined");
				}
				payCurrencyDTO = new PayCurrencyDTO(currExRate.getCurrency().getCurrencyCode(),
						currExRate.getMultiplyingExchangeRate(), currExRate.getCurrency().getBoundryValue(),
						currExRate.getCurrency().getBreakPoint());
			}

			// Create the iReservation with fare details.
			IReservation iReservation = new ReservationAssembler(ondFareDTOs, null);

			// Setting GOSHO agent currency as last modification currency
			iReservation.setLastCurrencyCode(agentCurrencyCode);
			iReservation.setLastModificationTimestamp(new Date());

			IPayment iPaymentPassenger = new PaymentAssembler();
			iPaymentPassenger.addExternalCharges(colExternalChgDTOs);
			BigDecimal amount = ReservationApiUtils.getPayedAmountFromOndFareDto(ondFareDTOs, pfsParsedEntry.getPaxType());
			iPaymentPassenger.addAgentCreditPayment(agentCode, amount, null, payCurrencyDTO, null, null, null);

			SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
			PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();

			// If the passenger type is adult adding as a single
			if (ReservationInternalConstants.PassengerType.ADULT.equals(pfsParsedEntry.getPaxType())) {
				iReservation.addSingle(pfsParsedEntry.getFirstName(), pfsParsedEntry.getLastName(), pfsParsedEntry.getTitle(),
						null, null, 1, paxAdditionalInfoDTO, null, iPaymentPassenger, segmentSSRs, null, null, null, null,
						CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
				// If the passenger type is child adding as a child
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(pfsParsedEntry.getPaxType())) {
				iReservation.addChild(pfsParsedEntry.getFirstName(), pfsParsedEntry.getLastName(), pfsParsedEntry.getTitle(),
						null, null, 1, paxAdditionalInfoDTO, null, iPaymentPassenger, segmentSSRs, null, null, null, null,
						CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(pfsParsedEntry.getPaxType())) {
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pfsParsedEntry.getPnr());
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
				Reservation infParentReservation = ReservationProxy.getReservation(pnrModesDTO);

				if (infParentReservation.getTotalPaxInfantCount() >= 1) {
					throw new ModuleException("airreservations.adult.exceeds.infant");
				}

				Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
				Integer parentID = ((ReservationPax) infParentReservation.getPassengers().iterator().next()).getPnrPaxId();

				IPassenger iPassenger = new PassengerAssembler(ondFareDTOs);
				iPassenger.addPassengerPayments(parentID, iPaymentPassenger);
				iPassenger.addInfant(pfsParsedEntry.getFirstName(), pfsParsedEntry.getLastName(), pfsParsedEntry.getTitle(), null,
						null, 2, 1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
						null);
				List<TempSegBcAlloc> blockIds = new ArrayList<TempSegBcAlloc>();
				passangerBD.addInfant(pfsParsedEntry.getPnr(), iPassenger, blockIds, intPaymentMode,
						infParentReservation.getVersion(), null, credentialsDTO.getTrackInfoDTO(), false, true, false, null);

			} else {
				throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
			}
			String resPnr = pfsParsedEntry.getPnr();
			String strPNR = "";
			if (!ReservationInternalConstants.PassengerType.INFANT.equals(pfsParsedEntry.getPaxType())) {
				int segSeqIdAndOndGroupId = 1;
				iReservation.addOutgoingSegment(segSeqIdAndOndGroupId, reconcileDTO.getFlightSegId().intValue(),
						new Integer(segSeqIdAndOndGroupId), 1, null, null, null, null);

				ReservationContactInfo contactInfo = new ReservationContactInfo();
				contactInfo.setTitle(pfsParsedEntry.getTitle());
				contactInfo.setFirstName(pfsParsedEntry.getFirstName());
				contactInfo.setLastName(pfsParsedEntry.getLastName());
				contactInfo.setPreferredLanguage(Locale.ENGLISH.toString());

				iReservation.addContactInfo("GO SHOW", contactInfo, null);

				ServiceResponce serviceResponce = reservationBD.createGoShoreReservation(iReservation, null);
				strPNR = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);
				if (strPNR != null && !strPNR.equals("")) {
					resPnr = strPNR;
				}

			}
			// Update reservation status
			this.updateReservationStatus(resPnr, agentCode);
			// Setting the generated pnr number
			pfsParsedEntry.setPnr(resPnr);
			// Make the entry processed
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.PROCESSED);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.GOSHOW_AGENT, agentCode);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.TOTAL_CHARGES, amount.toString());
			// This is to update the Eticket status
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(resPnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			updateEticketStatus(reservation, pfsParsedEntry, EticketStatus.FLOWN.code(), reconcileDTO.getFlightId());

		} catch (ModuleException e) {
			// If any error occured for this entry making it has error occured
			log.error("Error in processGoShore", e);
			pfsParsedEntry.setErrorDescription(e.getMessageString());
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		} catch (Exception e) {
			// If any error occured for this entry making it has error occured
			log.error("Error in processGoShore", e);
			pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PFS_APP_ERROR);
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		}

	}
	
	private Collection<ExternalChgDTO> calculateServiceTax(Collection<OndFareDTO> ondFareDTOs, PfsPaxEntry pfsParsedEntry)
			throws ModuleException {

		ServiceTaxQuoteForTicketingRevenueRQ request = new ServiceTaxQuoteForTicketingRevenueRQ();

		Map<Integer, Collection<OndFareDTO>> jouneyONDWiseFareONDs = new HashMap<Integer, Collection<OndFareDTO>>();
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()) == null) {
				jouneyONDWiseFareONDs.put(ondFareDTO.getOndSequence(), new ArrayList<OndFareDTO>());
			}
			jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
		}

		IPaxCountAssembler paxAssm = new PaxCountAssembler(ReservationInternalConstants.PassengerType.ADULT.equals(pfsParsedEntry
				.getPaxType()) ? 1 : 0, ReservationInternalConstants.PassengerType.CHILD.equals(pfsParsedEntry.getPaxType())
				? 1
				: 0, ReservationInternalConstants.PassengerType.INFANT.equals(pfsParsedEntry.getPaxType()) ? 1 : 0);
		List<Collection<OndFareDTO>> jouneyONDWiseFareONDList = new ArrayList<Collection<OndFareDTO>>(
				jouneyONDWiseFareONDs.values());
		FareTypeTO fareTypeTO = FareConvertUtil.getFareTypeTO(jouneyONDWiseFareONDList, paxAssm, null, null, null);
		request.setPaxTypeWiseCharges(ServiceTaxConverterUtil.composePaxTypeWiseCharges(fareTypeTO));

		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<SimplifiedFlightSegmentDTO>();
		for (OndFareDTO ondFare : ondFareDTOs) {
			Map<Integer, SegmentSeatDistsDTO> fltSegIdSD = new HashMap<>();
			for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
				fltSegIdSD.put(segSeatDist.getFlightSegId(), segSeatDist);
			}
			for (FlightSegmentDTO flightSegmentTO : ondFare.getSegmentsMap().values()) {
				SegmentSeatDistsDTO segSeatDist = fltSegIdSD.get(flightSegmentTO.getSegmentId());
				if (segSeatDist != null) {
					SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
					simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
					simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
					simplifiedFlightSegmentDTO.setOndSequence(ondFare.getOndSequence());
					simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
					simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
					simplifiedFlightSegmentDTO.setLogicalCabinClassCode(segSeatDist.getLogicalCabinClass());
					simplifiedFlightSegmentDTO.setReturnFlag(false);
					simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getCarrierCode());
					simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
				}
			}
		}
		request.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);
		request.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		ServiceTaxQuoteForTicketingRevenueRS serviceTaxRS = ReservationModuleUtils.getChargeBD()
				.quoteServiceTaxForTicketingRevenue(request);
		Collection<ExternalChgDTO> colExternalChgDTOs = new ArrayList<ExternalChgDTO>();
		if (serviceTaxRS != null && serviceTaxRS.getPaxTypeWiseServiceTaxes() != null
				&& !serviceTaxRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
			List<ServiceTaxDTO> serviceTaxs = null;
			if (ReservationInternalConstants.PassengerType.ADULT.equals(pfsParsedEntry.getPaxType())) {
				serviceTaxs = serviceTaxRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.ADULT);
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(pfsParsedEntry.getPaxType())) {
				serviceTaxs = serviceTaxRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.CHILD);
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(pfsParsedEntry.getPaxType())) {
				serviceTaxs = serviceTaxRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT);
			}
			if (serviceTaxs != null && !serviceTaxs.isEmpty()) {
				if (serviceTaxs != null) {
					for (ServiceTaxDTO serviceTax : serviceTaxs) {
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = ServiceTaxConverterUtil.externalChargeAdaptor(serviceTax,
								serviceTaxRS.getServiceTaxDepositeCountryCode(), serviceTaxRS.getServiceTaxDepositeStateCode());
						colExternalChgDTOs.add(serviceTaxExtChgDTO);
					}
				}
			}
		}
		return colExternalChgDTOs;
	}

	private void injectParentSegmentIdforInfant(ReconcilePassengersTO goShowPassengersTO, PfsPaxEntry pfsParsedEntry)
			throws ModuleException {

		if (ReservationInternalConstants.PassengerType.INFANT.equals(pfsParsedEntry.getPaxType())) {

			if (BeanUtils.nullHandler(pfsParsedEntry.getPnr()).isEmpty()) {
				throw new ModuleException("airreservation.pfs.cannot.locate.parent");
			} else if (BeanUtils.nullHandler(pfsParsedEntry.getDepartureAirport()).isEmpty()
					|| BeanUtils.nullHandler(pfsParsedEntry.getArrivalAirport()).isEmpty()) {
				throw new ModuleException("airreservation.pfs.departure.and.arrival.empty");
			}

			Integer derivedPnrSegId = reservationSegmentDAO.getReservationSegmentId(pfsParsedEntry.getPnr(),
					pfsParsedEntry.getDepartureAirport(), pfsParsedEntry.getArrivalAirport());
			goShowPassengersTO.setPnrSegmentId(derivedPnrSegId);

		}
	}

	private void updatePnrSegmentList(Map<Integer, FlightReconcileDTO> flightReconcileDTOs,
			Collection<FlightReconcileDTO> flightReconcileList) {

		for (FlightReconcileDTO flightreconcileDto : flightReconcileList) {

			if (!flightReconcileDTOs.containsKey(flightreconcileDto.getPnrSegId())) {
				flightReconcileDTOs.put(flightreconcileDto.getPnrSegId(), flightreconcileDto);
			}
		}
	}

	/**
	 * Process No Show
	 * 
	 * @param pfsParsedEntry
	 * @param colFlightReconcileDTO
	 * @param colErrorPfsParsedEntry
	 * @throws ModuleException
	 */
	private String processNoShow(PfsPaxEntry pfsParsedEntry, Collection<FlightReconcileDTO> colFlightReconcileDTO,
			Collection<PfsPaxEntry> colErrorPfsParsedEntry, ExternalChgDTO externalChgDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside processNoShore");
		String taxInvoicePnr = null;

		try {
			// Will validate records
			this.validateReconcileRecords(colFlightReconcileDTO);

			// Process no shore
			taxInvoicePnr = this.processPassenger(pfsParsedEntry, colFlightReconcileDTO, colErrorPfsParsedEntry,
					ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE, externalChgDTO, credentialsDTO);
		} catch (ModuleException e) {
			// If any error occured for this entry making it has error occured
			pfsParsedEntry.setErrorDescription(e.getMessageString());
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		} catch (Exception e) {
			// If any error occured for this entry making it has error occured
			log.error("Error Occurred while processing noshow " + pfsParsedEntry.getFirstName() + " "
					+ pfsParsedEntry.getLastName() + " " + e.getMessage());
			pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PFS_APP_ERROR);
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		}

		log.debug("Exit processNoShore");
		return taxInvoicePnr;
	}

	/**
	 * Process Off Load
	 * 
	 * @param pfsParsedEntry
	 * @param colFlightReconcileDTO
	 * @param colErrorPfsParsedEntry
	 * @throws ModuleException
	 */
	private void processOffLoad(PfsPaxEntry pfsParsedEntry, Collection<FlightReconcileDTO> colFlightReconcileDTO,
			Collection<PfsPaxEntry> colErrorPfsParsedEntry, ExternalChgDTO externalChgDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside processNoShore");

		try {
			// Will validate records
			this.validateReconcileRecords(colFlightReconcileDTO);

			// Process no shore
			this.processPassenger(pfsParsedEntry, colFlightReconcileDTO, colErrorPfsParsedEntry,
					ReservationInternalConstants.PaxFareSegmentTypes.OFF_LOADED, externalChgDTO, credentialsDTO);
		} catch (ModuleException e) {
			// If any error occured for this entry making it has error occured
			pfsParsedEntry.setErrorDescription(e.getMessageString());
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		} catch (Exception e) {
			// If any error occured for this entry making it has error occured
			pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PFS_APP_ERROR);
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		}

		log.debug("Exit processNoShore");
	}

	private Collection<FlightReconcileDTO> filterNORECFlightSeg(Collection<FlightReconcileDTO> colSourceFlightReconcileDTO,
			PfsPaxEntry pfsParsedEntry) {
		Collection<FlightReconcileDTO> colNorecRecords = new ArrayList<FlightReconcileDTO>();
		Iterator<FlightReconcileDTO> itrColSourceFlight = colSourceFlightReconcileDTO.iterator();
		while (itrColSourceFlight.hasNext()) {
			FlightReconcileDTO flightReconcileDTO = (FlightReconcileDTO) itrColSourceFlight.next();
			if (flightReconcileDTO.getPnr().equalsIgnoreCase(pfsParsedEntry.getPnr())) {
				colNorecRecords.add(flightReconcileDTO);
			}
		}
		return colNorecRecords;
	}

	private void transferPassengers(PfsPaxEntry pfsParsedEntry, Pfs pfs, CredentialsDTO credentialsDTO) throws ModuleException {

		Integer flightSegID = reservationSegmentDAO.getFlightSegmentIdForSegmentTransfer(pfs.getFlightNumber(),
				pfs.getDepartureDate(), pfs.getFromAirport(), pfsParsedEntry.getArrivalAirport());
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pfsParsedEntry.getPnr());
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
		boolean isGroupPNR = reservation.getOriginatorPnr() != null && !reservation.getOriginatorPnr().equals("");

		// get total pax count
		int totPaxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();

		// If more than 1 pax, then split that passenger
		if (totPaxCount > 1) {
			Collection<Integer> colPax = new ArrayList<Integer>();
			Set<ReservationPax> passengers = reservation.getPassengers();
			Iterator<ReservationPax> itrPax = passengers.iterator();
			int pnrPaxId = 0;
			ReservationPax resPax = null;
			while (itrPax.hasNext()) {
				resPax = (ReservationPax) itrPax.next();

				if (this.isPaxFound(resPax, pfsParsedEntry)) {
					pnrPaxId = resPax.getPnrPaxId();
					break;
				}
			}

			if (pnrPaxId != 0) {
				colPax.add(pnrPaxId);
				String splitNewPnr = null;

				LCCClientPnrModesDTO pnrModesDTOForSplit = getPnrModesDTO(reservation.getPnr(), isGroupPNR);
				ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
				paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);
				TrackInfoDTO trackInfo = new TrackInfoDTO();
				LCCClientReservation lccReservation = ReservationModuleUtils.getAirproxyReservationBD()
						.searchReservationByPNR(pnrModesDTOForSplit, paramRQInfo, trackInfo);

				LCCClientReservation lccClientReservationForSplit = new LCCClientReservation();
				lccClientReservationForSplit.setPNR(reservation.getPnr());
				lccClientReservationForSplit.setVersion(lccReservation.getVersion());
				lccClientReservationForSplit.setStatus(lccReservation.getStatus());
				lccClientReservationForSplit.setBookingCategory(lccReservation.getBookingCategory());
				lccClientReservationForSplit.setOriginCountryOfCall(lccReservation.getOriginCountryOfCall());

				LCCClientReservationPax lccReservationPax = trasformResPaxToLCCClientResPax(resPax);
				lccClientReservationForSplit.addPassenger(lccReservationPax);

				LCCClientReservation clientReservation = ReservationModuleUtils.getAirproxyReservationBD()
						.splitReservation(lccClientReservationForSplit, isGroupPNR, trackInfo);
				splitNewPnr = clientReservation.getPNR();

				if (splitNewPnr != null && !splitNewPnr.equals("")) {

					pnrModesDTO.setPnr(splitNewPnr);
					Reservation newResvation = ReservationProxy.getReservation(pnrModesDTO);

					pfsParsedEntry.setPnr(splitNewPnr);

					// Populate FlightReconcileDTO objects by identifying correct ReservationSegment from new
					// reservation
					Collection<FlightReconcileDTO> colNorecFlightReconcileDTO = new ArrayList<FlightReconcileDTO>();
					List<ReservationSegmentDTO> reservationSegmentDTO = new ArrayList<ReservationSegmentDTO>(
							newResvation.getSegmentsView());
					Collections.sort(reservationSegmentDTO);
					for (ReservationSegmentDTO resSegment : reservationSegmentDTO) {
						if (resSegment.getSegmentCode().startsWith(pfsParsedEntry.getDepartureAirport())
								&& resSegment.getSegmentCode().endsWith(pfsParsedEntry.getArrivalAirport())
								&& resSegment.getStatus().equals(ReservationSegmentStatus.CONFIRMED)
								&& (resSegment.getDepartureDate().after(pfsParsedEntry.getFlightDate())
										|| resSegment.getDepartureDate().getTime() == pfsParsedEntry.getFlightDate().getTime())) {
							FlightReconcileDTO flightReconcileDTO = new FlightReconcileDTO();
							flightReconcileDTO.setPnr(splitNewPnr);
							flightReconcileDTO.setPnrSegId(resSegment.getPnrSegId());
							flightReconcileDTO.setFlightSegId(resSegment.getFlightSegId());
							flightReconcileDTO.setCabinClassCode(resSegment.getCabinClassCode());
							flightReconcileDTO.setFlightNumber(resSegment.getFlightNo());
							flightReconcileDTO.setArrivalDateTimeLocal(resSegment.getArrivalDate());
							flightReconcileDTO.setArrivalDateTimeZulu(resSegment.getZuluArrivalDate());
							flightReconcileDTO.setDepartureDateTimeLocal(resSegment.getDepartureDate());
							flightReconcileDTO.setDepartureDateTimeZulu(resSegment.getZuluDepartureDate());
							flightReconcileDTO.setSegementCode(resSegment.getSegmentCode());
							colNorecFlightReconcileDTO.add(flightReconcileDTO);
							// Setting pfs parsed entry's pnr
							pfsParsedEntry.setPnr(splitNewPnr);
							break;
						}
					}

					transferReservationSegmentForNoRec(colNorecFlightReconcileDTO, newResvation.getVersion(), flightSegID,
							pfsParsedEntry, isGroupPNR);

				} else {
					log.info("Invalid split operation detected. PNR - " + pfsParsedEntry.getPnr() + " PNR pax id - " + pnrPaxId);
					// throw new ModuleException("airreservations.splitReservation.unsuccess");
					pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.INVALID_SPLIT);
				}
			} else {
				// Pax not found
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PAX_NOT_FOUND);
			}

		} else {
			// Populate FlightReconcileDTO objects by identifying correct ReservationSegment from reservation
			Collection<FlightReconcileDTO> colNorecFlightReconcileDTO = new ArrayList<FlightReconcileDTO>();
			List<ReservationSegmentDTO> reservationSegmentDTO = new ArrayList<ReservationSegmentDTO>(
					reservation.getSegmentsView());
			Collections.sort(reservationSegmentDTO);
			for (ReservationSegmentDTO resSegment : reservationSegmentDTO) {
				if (resSegment.getSegmentCode().startsWith(pfsParsedEntry.getDepartureAirport())
						&& resSegment.getSegmentCode().endsWith(pfsParsedEntry.getArrivalAirport())
						&& resSegment.getStatus().equals(ReservationSegmentStatus.CONFIRMED)
						&& (resSegment.getDepartureDate().after(pfsParsedEntry.getFlightDate())
								|| resSegment.getDepartureDate().getTime() == pfsParsedEntry.getFlightDate().getTime())) {
					FlightReconcileDTO flightReconcileDTO = new FlightReconcileDTO();
					flightReconcileDTO.setPnr(pfsParsedEntry.getPnr());
					flightReconcileDTO.setPnrSegId(resSegment.getPnrSegId());
					flightReconcileDTO.setFlightSegId(resSegment.getFlightSegId());
					flightReconcileDTO.setCabinClassCode(resSegment.getCabinClassCode());
					flightReconcileDTO.setFlightNumber(resSegment.getFlightNo());
					flightReconcileDTO.setArrivalDateTimeLocal(resSegment.getArrivalDate());
					flightReconcileDTO.setArrivalDateTimeZulu(resSegment.getZuluArrivalDate());
					flightReconcileDTO.setDepartureDateTimeLocal(resSegment.getDepartureDate());
					flightReconcileDTO.setDepartureDateTimeZulu(resSegment.getZuluDepartureDate());
					flightReconcileDTO.setSegementCode(resSegment.getSegmentCode());
					colNorecFlightReconcileDTO.add(flightReconcileDTO);
					break;
				}
			}
			transferReservationSegmentForNoRec(colNorecFlightReconcileDTO, reservation.getVersion(), flightSegID, pfsParsedEntry,
					isGroupPNR);
		}
	}

	/**
	 * Return segments for reconcilation
	 * 
	 * @param pfsEntry
	 * @param arrivalAirport
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<FlightReconcileDTO> getSegmentsForReconcilation(Pfs pfsEntry, String arrivalAirport)
			throws ModuleException {

		Integer flightSegId = reservationSegmentDAO.getFlightSegmentIdForSegmentTransfer(pfsEntry.getFlightNumber(),
				pfsEntry.getDepartureDate(), pfsEntry.getFromAirport(), arrivalAirport);
		Collection<Integer> flightSegIds = new ArrayList<Integer>();
		flightSegIds.add(flightSegId);

		return reservationSegmentDAO.getPnrSegmentsForReconcilation(flightSegIds, null, false);
	}

	/**
	 * Process No Rec
	 * 
	 * @param pfsParsedEntry
	 * @param colFlightReconcileDTO
	 * @param colErrorPfsParsedEntry
	 * @throws ModuleException
	 */
	private void processNoRec(PfsPaxEntry pfsParsedEntry, Collection<FlightReconcileDTO> colFlightReconcileDTO,
			Collection<PfsPaxEntry> colErrorPfsParsedEntry, ExternalChgDTO externalChgDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside processNoRec");

		try {
			// Will validate records
			this.validateReconcileRecords(colFlightReconcileDTO);

			// Process no rec
			this.processPassenger(pfsParsedEntry, colFlightReconcileDTO, colErrorPfsParsedEntry,
					ReservationInternalConstants.PaxFareSegmentTypes.NO_REC, externalChgDTO, credentialsDTO);
		} catch (ModuleException e) {
			// If any error occured for this entry making it has error occured
			pfsParsedEntry.setErrorDescription(e.getMessageString());
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		} catch (Exception e) {
			// If any error occured for this entry making it has error occured
			pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PFS_APP_ERROR);
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		}

		log.debug("Exit processNoRec");
	}

	/** Donotes error descriptions */
	private static interface ErrorDescription {
		public static final String SEG_ALREADY_CANCELLED = new ModuleException("airreservations.arg.invalidSegCancel")
				.getMessageString();
		public static final String PNR_ALREADY_CANCEL = new ModuleException("airreservations.arg.invalidPnrCancel")
				.getMessageString();
		public static final String ALREADY_RECONCILED = new ModuleException("airreservations.arg.alreadyReconiled")
				.getMessageString();
		public static final String SEG_NOT_FOUND = new ModuleException("airreservations.arg.segmentNotFoundInRes")
				.getMessageString();
		public static final String PNR_SEGS_NOT_FOUND = new ModuleException("airreservations.arg.segmentsDoesNotExistInRes")
				.getMessageString();
		public static final String PAX_NOT_FOUND = new ModuleException("airreservations.arg.passengerDoesNotExist")
				.getMessageString();
		public static final String PNR_DOES_NOT_EXIST = new ModuleException("airreservations.arg.pnrNumberDoesNotExist")
				.getMessageString();
		public static final String PFS_APP_ERROR = new ModuleException("airreservations.arg.appErrorWhileReconcile")
				.getMessageString();
		public static final String INVALID_SPLIT = new ModuleException("airreservations.splitReservation.unsuccess")
				.getMessageString();
		public static final String CHARTER_BC_FOUND_FOR_NOSHO = new ModuleException("airreservation.pfs.found.charterbc.nosho")
				.getMessageString();
		public static final String ETICKET_IS_PRINT_EXCHANGED = new ModuleException("airreservations.arg.eticket.print.exchanged")
				.getMessageString();
		public static final String PFS_NOREC_PROCESS_ERROR_SAME_FLIGHT = new ModuleException(
				"airreservation.pfs.norec.process.error.same.flight").getMessageString();
	}

	/**
	 * Validate reconcile records
	 * 
	 * @param colFlightReconcileDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateReconcileRecords(Collection colFlightReconcileDTO) throws ModuleException {
		if (colFlightReconcileDTO == null || colFlightReconcileDTO.size() == 0) {
			throw new ModuleException("airreservations.arg.emptyRecFlightInfo");
		}
	}

	/**
	 * Tracks the number of passengers for passenger types
	 * 
	 * @param paxType
	 * @param goshowPassengersTO
	 * @throws ModuleException
	 */
	private void trackNoOfPassengersForPaxTypes(String paxType, ReconcilePassengersTO goshowPassengersTO) throws ModuleException {
		// Adult
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			goshowPassengersTO.setNoOfAdults(1);
			// Child
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			goshowPassengersTO.setNoOfChildren(1);
			// Any other types are not correctly supported
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {

		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	/**
	 * Returns the go shore passenger category code
	 * 
	 * @param paxCategoryCode
	 * @return
	 */
	private String getGoShorePaxCategoryCode(String paxCategoryCode) {
		paxCategoryCode = BeanUtils.nullHandler(paxCategoryCode);

		if (!paxCategoryCode.equals("")) {
			return paxCategoryCode;
		} else {
			return AirPricingCustomConstants.BookingPaxType.ANY;
		}
	}

	/**
	 * Returns the agent code
	 * 
	 * @param fromSegmentCode
	 * @return
	 * @throws ModuleException
	 */
	public Agent getGoshoAgent(String fromSegmentCode) throws ModuleException {
		// Get GOSHOW Agent Code
		Airport oAirPort = ReservationModuleUtils.getAirportBD().getAirport(fromSegmentCode);
		if (oAirPort == null) {
			throw new ModuleException("airreservations.arg.gsaDoesNotExist");
		} else {
			String goShowAgentCode = BeanUtils.nullHandler(oAirPort.getGoshowAgentCode());
			if (goShowAgentCode.equals("")) {
				throw new ModuleException("airreservations.arg.gsaDoesNotExist");
			} else {
				Agent oAgent = ReservationModuleUtils.getTravelAgentBD().getAgent(goShowAgentCode);

				if (oAgent == null) {
					throw new ModuleException("airreservations.arg.gsaDoesNotExist");
				} else if (Agent.STATUS_INACTIVE.equals(oAgent.getStatus())) {
					throw new ModuleException("airreservations.arg.goShowAgentDoesNotActive");
				}

				return oAgent;
			}
		}
	}

	/**
	 * Returns currency exchange rate
	 * 
	 * @param currencyCode
	 * @param effectiveDate
	 * @return
	 * @throws ModuleException
	 */
	private static CurrencyExchangeRate getCurrencyExchangeRate(String currencyCode) {
		CurrencyExchangeRate currencyExchangeRate = null;

		try {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);

			if (currencyExchangeRate != null) {
				Currency currency = currencyExchangeRate.getCurrency();

				if (currency.getStatus().equals("INA") || currency.getXBEVisibility() == 0) {
					currencyExchangeRate = null;
				}
			}
		} catch (ModuleException me) {
			log.warn("No exchange rate defined for currency " + currencyCode, me);
		}

		return currencyExchangeRate;
	}

	/**
	 * Update reservation status
	 * 
	 * @param strPNR
	 * @param agentCode
	 * @throws ModuleException
	 */
	private void updateReservationStatus(String strPNR, String agentCode) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(strPNR);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		// Update segment status to go shore
		this.updateSegmentStatusToGoShore(reservation);

		// Update the ownership information
		this.updateOwnerShipInfo(reservation, agentCode);

		// Save the reservation
		ReservationProxy.saveReservation(reservation);
	}

	private void updateEticketStatus(Reservation reservation, PfsPaxEntry pfsParsedEntry, String status, Integer flightId)
			throws ModuleException {
		Set<ReservationPax> passengers = reservation.getPassengers();
		Iterator<ReservationPax> itrPax = passengers.iterator();
		ReservationPax resPax = null;
		while (itrPax.hasNext()) {
			resPax = (ReservationPax) itrPax.next();
			if (this.isPaxFound(resPax, pfsParsedEntry)) {
				ETicketBO.updateETicketStatus(resPax, getFlightSegmentIdsFromPFSParsedEntry(pfsParsedEntry), status, false, true);

				if (AppSysParamsUtil.isUpdateInfantStateWhenProcessParentPFS()) {
					updateAttachedInfantEticketStatus(resPax, status, pfsParsedEntry);
				}

				// This is to save eticket number in pfs parsed table
				if (flightId != null) {
					EticketTO eTicket = eticketDAO.getPassengerETicketDetail(resPax.getPnrPaxId(), flightId);
					if (eTicket != null) {
						pfsParsedEntry.seteTicketNo(eTicket.getEticketNumber());
					}
				}
				break;
			}
		}
	}

	/**
	 * Process passenger
	 * 
	 * @param pfsParsedEntry
	 * @param colFlightReconcileDTO
	 * @param colErrorPfsParsedEntry
	 * @param processStatus
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private String processPassenger(PfsPaxEntry pfsParsedEntry, Collection<FlightReconcileDTO> colFlightReconcileDTO,
			Collection<PfsPaxEntry> colErrorPfsParsedEntry, String processStatus, ExternalChgDTO externalChgDTO,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside processPassenger");
				// If the pnr is not empty
		String taxInvoicePnr = null;
		if (pfsParsedEntry.getPnr() != null) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pfsParsedEntry.getPnr());
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);

			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			// If the reservation is cancelled
			if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PNR_ALREADY_CANCEL);
				pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
				colErrorPfsParsedEntry.add(pfsParsedEntry);
				// If it's a confirmed or onhold
			} else {
				boolean isProcessOffLoadPAXAsNoShow = ReservationModuleUtils.getAirReservationConfig()
						.isProcessOffLoadPAXAsNoShow();
				Collection<ReservationPaxFare> colPnrPaxFares = this.processConfirmReservationStructure(pfsParsedEntry,
						reservation, colFlightReconcileDTO, colErrorPfsParsedEntry, processStatus, isProcessOffLoadPAXAsNoShow);
				if (colPnrPaxFares.size() > 0) {
					reservation = ReservationProxy.saveReservation(reservation);

					// Calculate nosho charge before noshow fules apply
					Map<String, Map<Integer, BigDecimal>> pnrPaxFareIdsAndNoshoCharges = this
							.calculateChargesForTheReservationBeforeProcessNoshowRules(reservation, processStatus, colPnrPaxFares,
									externalChgDTO, isProcessOffLoadPAXAsNoShow);
					// apply no shore rule set
					if ((processStatus.equals(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE)
							|| (isProcessOffLoadPAXAsNoShow
									&& processStatus.equals(ReservationInternalConstants.PaxFareSegmentTypes.OFF_LOADED)))
							&& (AppSysParamsUtil.isCancelOutOfSequnceSegments() || AppSysParamsUtil.isCancelNoShowSegment())) {
						ProcessNoShowRules processNoShowrules = new ProcessNoShowRules(reservation, pfsParsedEntry,
								colFlightReconcileDTO);
						reservation = processNoShowrules.process();
						pfsParsedEntry.setPnr(reservation.getPnr());
					}

					AdjustCreditBO adjustCreditBO = new AdjustCreditBO(reservation);

					if (pnrPaxFareIdsAndNoshoCharges != null && pnrPaxFareIdsAndNoshoCharges.size() > 0) {
						boolean hasServiceTaxApplied = this
								.applyNoshowChargesWithServiceTax(pnrPaxFareIdsAndNoshoCharges.get("NoShowChargeAmounts"),
										adjustCreditBO, reservation, externalChgDTO, credentialsDTO,
										pnrPaxFareIdsAndNoshoCharges.get("NoShowChargeBasisData"));
						if (hasServiceTaxApplied) {
							taxInvoicePnr = reservation.getPnr();
						}
					}

					ReservationProxy.saveReservation(reservation);
					adjustCreditBO.callRevenueAccountingAfterReservationSave();
					// This is to update the Eticket status
					if (AppSysParamsUtil.isUpdateEticketStatusWithPFS()) {
						if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(processStatus)) {
							updateEticketStatus(reservation, pfsParsedEntry, EticketStatus.OPEN.code(), null);
						} else if (!(ReservationInternalConstants.PaxFareSegmentTypes.OFF_LOADED.equals(processStatus)
								|| ReservationInternalConstants.PaxFareSegmentTypes.GO_SHN.equals(processStatus)
								|| ReservationInternalConstants.PaxFareSegmentTypes.CH_GFL.equals(processStatus))) {
							updateEticketStatus(reservation, pfsParsedEntry, EticketStatus.FLOWN.code(), null);
						}
					}
				}
			}
		} else {
			// It is assumed that the for no shore and no rec pnr should always
			// exist
			pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PNR_DOES_NOT_EXIST);
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		}

		log.debug("Exit processPassenger");
		return taxInvoicePnr;
	}

	/**
	 * Find out the passenger
	 * 
	 * @param reservationPax
	 * @param pfsParsedEntry
	 * @return
	 */
	private boolean isPaxFound(ReservationPax reservationPax, PfsPaxEntry pfsParsedEntry) {
		String strDbTitle = BeanUtils.nullHandler(reservationPax.getTitle()).toUpperCase();
		String strDbFirstName = BeanUtils.nullHandler(reservationPax.getFirstName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");
		String strDbLastName = BeanUtils.nullHandler(reservationPax.getLastName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");

		String strDbPaxType = BeanUtils.nullHandler(reservationPax.getPaxType());

		String strPfsTitle = BeanUtils.nullHandler(pfsParsedEntry.getTitle()).toUpperCase();
		String strPfsFirstName = BeanUtils.nullHandler(pfsParsedEntry.getFirstName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");
		String strPfsLastName = BeanUtils.nullHandler(pfsParsedEntry.getLastName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");

		String strPfsPaxType = BeanUtils.nullHandler(pfsParsedEntry.getPaxType());

		String strDbFullName = strDbFirstName + strDbLastName;
		String strPfsFullName = strPfsFirstName + strPfsLastName;

		if (AppSysParamsUtil.skipPaxTypeForPFSProcessing()) {
			// If it's an adult title should be same
			if (strDbFullName.equals(strPfsFullName)) {
				return true;
			} else {
				return false;
			}
		} else {

			if (strDbPaxType.equals(strPfsPaxType) && strDbFullName.equals(strPfsFullName)) {
				// If it's an adult title should be same
				if (ReservationInternalConstants.PassengerType.ADULT.equals(strDbPaxType)) {
					// Removing the title check as requested - 31MAY2012
					// if (strDbTitle.equals(strPfsTitle)) {
					return true;
					// }
					// If it's a child we are not comparing the titles. Cos for
					// PNL/ADL we send the child as CHD. Titles
					// will be ignored.
					// Please improve this with a later implementation
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(strDbPaxType)) {
					return true;
					// If it's an infant
				} else if (ReservationInternalConstants.PassengerType.INFANT.equals(strDbPaxType)) {
					return true;
				}
				return false;
			} else {
				return false;
			}
		}
	}

	private Long transferReservationSegmentForNoRec(Collection<FlightReconcileDTO> colSourceFlightReconcileDTO,
			Long currentVersion, Integer flightSegID, PfsPaxEntry pfsParsedEntry, boolean isGroupPNR) throws ModuleException {
		Iterator<FlightReconcileDTO> itrColSourceFlightReconcileDTO = colSourceFlightReconcileDTO.iterator();
		Long reservationVersion = currentVersion;

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		LCCClientTransferSegmentTO transferSegmentTO = new LCCClientTransferSegmentTO();

		String carrierCode = null;
		if (isGroupPNR) {
			Map<String, SegmentDetails> oldSegmentDetails = new HashMap<String, SegmentDetails>();
			FlightReconcileDTO flightReconcileDTO = null;
			while (itrColSourceFlightReconcileDTO.hasNext()) {
				flightReconcileDTO = (FlightReconcileDTO) itrColSourceFlightReconcileDTO.next();
				SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
				segmentDetails.setDepartureDate(flightReconcileDTO.getDepartureDateTimeLocal());
				segmentDetails.setDepartureDateZulu(flightReconcileDTO.getDepartureDateTimeZulu());
				segmentDetails.setArrivalDate(flightReconcileDTO.getArrivalDateTimeLocal());
				segmentDetails.setArrivalDateZulu(flightReconcileDTO.getArrivalDateTimeZulu());

				segmentDetails.setPnrSegId(flightReconcileDTO.getPnrSegId());
				segmentDetails.setSegmentCode(flightReconcileDTO.getSegementCode());
				carrierCode = AppSysParamsUtil.extractCarrierCode(flightReconcileDTO.getFlightNumber());
				segmentDetails.setCarrier(carrierCode);
				segmentDetails.setCabinClass(flightReconcileDTO.getCabinClassCode());
				segmentDetails.setFlightNumber(pfsParsedEntry.getFlightNumber());
				oldSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

			}
			transferSegmentTO.setOldSegmentDetails(oldSegmentDetails);

			Map<String, SegmentDetails> newSegmentDetails = new HashMap<String, SegmentDetails>();
			FlightSegement segment = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegID);
			SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
			segmentDetails.setDepartureDate(segment.getEstTimeDepatureLocal());
			segmentDetails.setDepartureDateZulu(segment.getEstTimeDepatureZulu());
			segmentDetails.setArrivalDate(segment.getEstTimeArrivalLocal());
			segmentDetails.setArrivalDateZulu(segment.getEstTimeArrivalZulu());
			segmentDetails.setFlightSegId(segment.getFltSegId());
			segmentDetails.setSegmentCode(segment.getSegmentCode());
			segmentDetails.setCarrier(carrierCode);
			segmentDetails.setCabinClass(flightReconcileDTO.getCabinClassCode());
			segmentDetails.setFlightNumber(flightReconcileDTO.getFlightNumber());
			newSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

			transferSegmentTO.setNewSegmentDetails(newSegmentDetails);
			transferSegmentTO.setReservationVersion(carrierCode + "-" + currentVersion.toString());
		} else {
			Map<String, Integer> pnrSegIdAndNewFlgSegId = new HashMap<String, Integer>();
			while (itrColSourceFlightReconcileDTO.hasNext()) {
				FlightReconcileDTO flightReconcileDTO = (FlightReconcileDTO) itrColSourceFlightReconcileDTO.next();
				carrierCode = AppSysParamsUtil.extractCarrierCode(flightReconcileDTO.getFlightNumber());

				pnrSegIdAndNewFlgSegId.put(FlightRefNumberUtil.composeFlightRPH(flightReconcileDTO, carrierCode), flightSegID);

			}
			transferSegmentTO.setOldNewFlightSegId(pnrSegIdAndNewFlgSegId);
			transferSegmentTO.setReservationVersion(currentVersion.toString());
		}

		transferSegmentTO.setPnr(pfsParsedEntry.getPnr());
		transferSegmentTO.setGroupPNR(isGroupPNR);

		transferSegmentTO.setAlert(false);
		transferSegmentTO.setSegmentTransferredByNorecProcess(true);

		ReservationModuleUtils.getAirproxySegmentBD().transferSegments(transferSegmentTO, trackInfo, null);

		return reservationVersion;
	}

	/**
	 * Update ownership information
	 * 
	 * @param reservation
	 * @param agentCode
	 * @throws ModuleException
	 */
	private void updateOwnerShipInfo(Reservation reservation, String agentCode) throws ModuleException {
		ReservationAdminInfo adminInfo = reservation.getAdminInfo();

		adminInfo.setOriginChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
		adminInfo.setOwnerChannelId(new Integer(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT));
		adminInfo.setOriginUserId(null);
		adminInfo.setOriginAgentCode(agentCode);
		adminInfo.setOwnerAgentCode(agentCode);
	}

	/**
	 * Update segment status to go shore
	 * 
	 * @param reservation
	 */
	private void updateSegmentStatusToGoShore(Reservation reservation) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();

		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();
			while (itReservationPaxFare.hasNext()) {
				reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

				itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
				while (itReservationPaxFareSegment.hasNext()) {
					reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
					reservationPaxFareSegment.setStatus(ReservationInternalConstants.PaxFareSegmentTypes.GO_SHORE);
					reservationPaxFareSegment.setCheckinStatus(ReservationInternalConstants.PassengerCheckinStatus.CHECKED_IN);
				}
			}
		}
	}

	private Collection<Integer> getFlightSegmentIdsFromPFSParsedEntry(PfsPaxEntry pfsParsedEntry) {
		return reservationSegmentDAO.getFlightSegmentIds(pfsParsedEntry.getFlightNumber(), pfsParsedEntry.getFlightDate(),
				pfsParsedEntry.getDepartureAirport(), pfsParsedEntry.getArrivalAirport());
	}

	/**
	 * Process confirmed reservations structure
	 * 
	 * @param pfsParsedEntry
	 * @param reservation
	 * @param colFlightReconcileDTO
	 * @param colErrorPfsParsedEntry
	 * @param processStatus
	 * @param isProcessOffLoadPAXAsNoShow
	 */
	private Collection<ReservationPaxFare> processConfirmReservationStructure(PfsPaxEntry pfsParsedEntry, Reservation reservation,
			Collection<FlightReconcileDTO> colFlightReconcileDTO, Collection<PfsPaxEntry> colErrorPfsParsedEntry,
			String processStatus, boolean isProcessOffLoadPAXAsNoShow) throws ModuleException {
		ReservationPaxFareSegment reservationPaxFareSegment;
		ReservationPaxFare reservationPaxFare;
		ReservationPax reservationPax;

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		Iterator<ReservationPaxFare> itReservationPaxFare;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment;

		Map<Integer, FlightReconcileDTO> mapPnrSegments = this.getPnrSegmentMap(pfsParsedEntry.getPnr(), colFlightReconcileDTO);

		Collection<ReservationPaxFare> colPnrPaxFares = new HashSet<ReservationPaxFare>();
		boolean pnrSegmentsFound = false;
		boolean passengerFound = false;
		boolean segmentFound = false;
		boolean segmentConfirmed = false;

		// If pnr segments found
		if (mapPnrSegments.size() > 0) {
			pnrSegmentsFound = true;

			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();

				// Check if any passenger found
				if (colPnrPaxFares.size() == 0 && this.isPaxFound(reservationPax, pfsParsedEntry)) {
					passengerFound = true;

					itReservationPaxFare = reservationPax.getPnrPaxFares().iterator();

					while (itReservationPaxFare.hasNext()) {
						reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

						itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();

						while (itReservationPaxFareSegment.hasNext()) {
							reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

							if (mapPnrSegments.keySet().contains(reservationPaxFareSegment.getPnrSegId())) {
								List<String> bookingClassesToSkip = ((AirReservationConfig) ReservationModuleUtils.getInstance()
										.getModuleConfig()).getBookingClassesToSkipForNoshow();
								// This is to skip NOSHOW processing for charter booking classes
								if (processStatus.equals(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE)
										&& bookingClassesToSkip.contains(reservationPaxFareSegment.getBookingCode())) {
									pfsParsedEntry.setErrorDescription(
											ReconcileReservationServices.ErrorDescription.CHARTER_BC_FOUND_FOR_NOSHO);
									pfsParsedEntry
											.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
									colErrorPfsParsedEntry.add(pfsParsedEntry);
									return colPnrPaxFares;
								}

								// This is to save pax eticket number in pfs parsed table. Handling this here as we no
								// need go through pax again to find out pnr pax id
								FlightReconcileDTO flightReconcileDTO = (FlightReconcileDTO) mapPnrSegments
										.get(reservationPaxFareSegment.getPnrSegId());
								if (flightReconcileDTO != null && flightReconcileDTO.getFlightId() != null) {
									EticketTO eTicket = eticketDAO.getPassengerETicketDetail(reservationPax.getPnrPaxId(),
											flightReconcileDTO.getFlightId());
									if (eTicket != null) {
										pfsParsedEntry.seteTicketNo(eTicket.getEticketNumber());
										pfsParsedEntry.setExtETicketNo(eTicket.getExternalEticketNumber());
										// This is to skip FLOWN passenger processing
										if (AppSysParamsUtil.isUpdateEticketStatusWithPFS()) {
											if (eTicket.getTicketStatus().equals(EticketStatus.FLOWN.code())) {
												pfsParsedEntry.setErrorDescription(
														ReconcileReservationServices.ErrorDescription.ALREADY_RECONCILED);
												pfsParsedEntry.setProcessedStatus(
														ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
												colErrorPfsParsedEntry.add(pfsParsedEntry);
												return colPnrPaxFares;
											} else if (eTicket.getTicketStatus().equals(EticketStatus.PRINT_EXCHANGED.code())) {
												pfsParsedEntry.setErrorDescription(
														ReconcileReservationServices.ErrorDescription.ETICKET_IS_PRINT_EXCHANGED);
												pfsParsedEntry.setProcessedStatus(
														ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
												colErrorPfsParsedEntry.add(pfsParsedEntry);
												return colPnrPaxFares;
											}
										} else {
											if (eTicket.getTicketStatus().equals(EticketStatus.FLOWN.code())) {
												pfsParsedEntry.setErrorDescription(
														ReconcileReservationServices.ErrorDescription.ALREADY_RECONCILED);
												pfsParsedEntry.setProcessedStatus(
														ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
												colErrorPfsParsedEntry.add(pfsParsedEntry);
												return colPnrPaxFares;
											}
										}
									}
								}
								segmentFound = true;

								// If the segment is confirmed
								if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
										.equals(reservationPaxFareSegment.getSegment().getStatus())) {
									segmentConfirmed = true;
									processStatus = (processStatus
											.equals(ReservationInternalConstants.PaxFareSegmentTypes.OFF_LOADED)
											&& isProcessOffLoadPAXAsNoShow)
													? ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE
													: processStatus;

									// Only updating the confirmed entries or flown entries
									if (ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED
											.equals(reservationPaxFareSegment.getStatus())
											|| ReservationInternalConstants.PaxFareSegmentTypes.FLOWN
													.equals(reservationPaxFareSegment.getStatus())) {
										colPnrPaxFares.add(reservationPaxFare);
										reservationPaxFareSegment.setStatus(processStatus);

										// update infant status
										if (AppSysParamsUtil.isUpdateInfantStateWhenProcessParentPFS()) {
											updateAttachedInfantStatus(reservationPaxFareSegment.getPnrSegId(),
													reservationPax.getPnrPaxId(), reservationPax, processStatus);
										}

									}
								}
							}
						}
					}
				}
			}
		}

		// Update the reservation if update exist
		if (colPnrPaxFares.size() > 0) {
			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.PROCESSED);
		} else {
			if (pnrSegmentsFound == false) {
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PNR_SEGS_NOT_FOUND);
			} else if (passengerFound == false) {
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.PAX_NOT_FOUND);
			} else if (segmentFound == false) {
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.SEG_NOT_FOUND);
			} else if (segmentConfirmed == false) {
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.SEG_ALREADY_CANCELLED);
			} else {
				pfsParsedEntry.setErrorDescription(ReconcileReservationServices.ErrorDescription.ALREADY_RECONCILED);
			}

			pfsParsedEntry.setProcessedStatus(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED);
			colErrorPfsParsedEntry.add(pfsParsedEntry);
		}

		return colPnrPaxFares;
	}

	/**
	 * Add Charges for the reservation
	 * 
	 * @param adjustCreditBO
	 * @param reservation
	 * @param processStatus
	 * @param colPnrPaxFares
	 * @param externalChgDTO
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private void addChargesForTheReservation(AdjustCreditBO adjustCreditBO, Reservation reservation, String processStatus,
			Collection<ReservationPaxFare> colPnrPaxFares, ExternalChgDTO externalChgDTO, CredentialsDTO credentialsDTO)
			throws ModuleException {

		if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(processStatus)) {
			Collection<ReservationPaxFare> applicablePnrPaxFares = new ArrayList<ReservationPaxFare>();

			for (ReservationPaxFare reservationPaxFare : colPnrPaxFares) {
				boolean isNoShoreChargeApplied = checkNoShoreChargeApplied(reservationPaxFare, externalChgDTO.getChgRateId());

				if (!isNoShoreChargeApplied) {
					applicablePnrPaxFares.add(reservationPaxFare);
				}
			}

			if (applicablePnrPaxFares.size() > 0) {
				Collection<Integer> fareIds = getFareIds(applicablePnrPaxFares);
				FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(fareIds, null, null);
				Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();
				ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation, false);

				BigDecimal noShoreChargeAmount;
				FareTO fareTO;
				for (ReservationPaxFare reservationPaxFare : applicablePnrPaxFares) {
					noShoreChargeAmount = getNoShoreChargeAmount(reservationPaxFare, mapFareTO);
					fareTO = getApplicableFareTO(reservationPaxFare, mapFareTO);

					noShoreChargeAmount = calculateNoShowChargeAmont(reservationPaxFare, fareTO, noShoreChargeAmount, reservation);
					// Only apply if charge exist
					if (noShoreChargeAmount.doubleValue() > 0) {
						// Adjusting the credit manually
						ReservationBO.adjustCreditManual(adjustCreditBO, reservation, reservationPaxFare.getPnrPaxFareId(),
								externalChgDTO.getChgRateId(), noShoreChargeAmount, ReservationInternalConstants.ChargeGroup.SUR,
								"Added NOSHO Charge", credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);
					}
				}
			}
		}
	}

	private Map<String, Map<Integer, BigDecimal>> calculateChargesForTheReservationBeforeProcessNoshowRules(
			Reservation reservation, String processStatus, Collection<ReservationPaxFare> colPnrPaxFares,
			ExternalChgDTO externalChgDTO, boolean isProcessOffLoadPAXAsNoShow) throws ModuleException {
		Map<String, Map<Integer, BigDecimal>> noShowChargeDataMap = new LinkedHashMap<String, Map<Integer, BigDecimal>>();
		Map<Integer, BigDecimal> noShowChargeBasisMap = new HashMap<Integer, BigDecimal>();
		Map<Integer, BigDecimal> pnrPaxFareIdsAndNoshoCharges = new HashMap<Integer, BigDecimal>();
		if (ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE.equals(processStatus) || (isProcessOffLoadPAXAsNoShow
				&& ReservationInternalConstants.PaxFareSegmentTypes.OFF_LOADED.equals(processStatus))) {
			Collection<ReservationPaxFare> applicablePnrPaxFares = new ArrayList<ReservationPaxFare>();

			for (ReservationPaxFare reservationPaxFare : colPnrPaxFares) {
				boolean isNoShoreChargeApplied = checkNoShoreChargeApplied(reservationPaxFare, externalChgDTO.getChgRateId());

				if (!isNoShoreChargeApplied) {
					applicablePnrPaxFares.add(reservationPaxFare);
				}
			}

			if (applicablePnrPaxFares.size() > 0) {
				Collection<Integer> fareIds = getFareIds(applicablePnrPaxFares);
				FaresAndChargesTO faresAndChargesTO = fareBD.getRefundableStatuses(fareIds, null, null);
				Map<Integer, FareTO> mapFareTO = faresAndChargesTO.getFareTOs();
				BigDecimal noShoreChargeAmount;
				FareTO fareTO;
				for (ReservationPaxFare reservationPaxFare : applicablePnrPaxFares) {
					noShoreChargeAmount = getNoShoreChargeAmount(reservationPaxFare, mapFareTO);
					noShowChargeBasisMap.put(reservationPaxFare.getPnrPaxFareId(), noShoreChargeAmount);
					fareTO = getApplicableFareTO(reservationPaxFare, mapFareTO);
					noShoreChargeAmount = calculateNoShowChargeAmont(reservationPaxFare, fareTO, noShoreChargeAmount,
							reservation);
					pnrPaxFareIdsAndNoshoCharges.put(reservationPaxFare.getPnrPaxFareId(), noShoreChargeAmount);
				}
				noShowChargeDataMap.put("NoShowChargeAmounts", pnrPaxFareIdsAndNoshoCharges);
				noShowChargeDataMap.put("NoShowChargeBasisData", noShowChargeBasisMap);
			}
		}
		return noShowChargeDataMap;
	}

	private boolean applyNoshowChargesWithServiceTax(Map<Integer, BigDecimal> pnrPaxFareIdsAndNoshoCharges,
			AdjustCreditBO adjustCreditBO, Reservation reservation, ExternalChgDTO externalChgDTO, CredentialsDTO credentialsDTO,
			Map<Integer, BigDecimal> noShowChargeBasisMap) throws ModuleException {

		ExtraFeeBO extraFeeBO = new ExtraFeeBO(reservation, null);
		ChargeTnxSeqGenerator chgTnxGen = new ChargeTnxSeqGenerator(reservation);
		boolean isServiceTaxApplied = false;
		for (Integer pnrPaxFareId : pnrPaxFareIdsAndNoshoCharges.keySet()) {
			BigDecimal noShoreChargeAmount = pnrPaxFareIdsAndNoshoCharges.get(pnrPaxFareId);
			if (noShowChargeBasisMap.get(pnrPaxFareId).doubleValue() > 0 || (noShoreChargeAmount.doubleValue() > 0)) {
				ReservationBO.adjustCreditManual(adjustCreditBO, reservation, pnrPaxFareId, externalChgDTO.getChgRateId(),
						noShoreChargeAmount, ReservationInternalConstants.ChargeGroup.SUR, "Added NO_SHOW Charge",
						credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);

				String cabinClassCode = null;
				ReservationPax reservationPax = null;
				for (ReservationPax passenger : reservation.getPassengers()) {
					for (ReservationPaxFare paxFare : passenger.getPnrPaxFares()) {
						if (paxFare.getPnrPaxFareId().intValue() == pnrPaxFareId.intValue()) {
							cabinClassCode = ReservationCoreUtils.getCabinClassFromPaxFare(paxFare);
							reservationPax = passenger;
						}
					}
				}

				if (extraFeeBO.isExtraFeeApplicable()) {
					BigDecimal applicableNoshowTax = extraFeeBO.getFee(noShoreChargeAmount, cabinClassCode);

					ReservationBO.adjustCreditManual(adjustCreditBO, reservation, pnrPaxFareId,
							extraFeeBO.getExternalChgDTO().getChgRateId(), applicableNoshowTax,
							ReservationInternalConstants.ChargeGroup.TAX, "Added JN Tax for NO_SHOW",
							credentialsDTO.getTrackInfoDTO(), credentialsDTO, chgTnxGen);
				}

				List<ExternalChgDTO> externalChgDTOs = calculateServiceTax(reservation,
						externalChgDTO, noShoreChargeAmount, reservationPax);
				if (!CollectionUtils.isEmpty(externalChgDTOs)) {
					for (ExternalChgDTO chgDTO : externalChgDTOs) {
						ReservationBO.adjustCreditManual(adjustCreditBO, reservation, pnrPaxFareId, chgDTO.getChgRateId(),
								chgDTO.getAmount(), ReservationInternalConstants.ChargeGroup.TAX,
								ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY,
								"Added " + chgDTO.getChargeCode() + " for NO_SHOW ", credentialsDTO.getTrackInfoDTO(),
								credentialsDTO, chgTnxGen);
					}
					if (!isServiceTaxApplied) {
						isServiceTaxApplied = true;
					}
				}

			}
		}
		return isServiceTaxApplied;
	}

	/**
	 * Return reservation segment mapping
	 * 
	 * @param pnr
	 * @param colFlightReconcileDTO
	 * @return
	 */
	private Map<Integer, FlightReconcileDTO> getPnrSegmentMap(String pnr, Collection<FlightReconcileDTO> colFlightReconcileDTO) {
		Iterator<FlightReconcileDTO> itColFlightReconcileDTO = colFlightReconcileDTO.iterator();
		FlightReconcileDTO flightReconcileDTO;
		Map<Integer, FlightReconcileDTO> mapPnrSegId = new HashMap<Integer, FlightReconcileDTO>();

		while (itColFlightReconcileDTO.hasNext()) {
			flightReconcileDTO = (FlightReconcileDTO) itColFlightReconcileDTO.next();

			// Since go shore reservations are there this can come with ""
			if (flightReconcileDTO.getPnr() != null && !flightReconcileDTO.getPnr().equals("")) {
				if (BeanUtils.nullHandler(flightReconcileDTO.getPnr()).equals(BeanUtils.nullHandler(pnr))) {
					mapPnrSegId.put(flightReconcileDTO.getPnrSegId(), flightReconcileDTO);
				}
			}
		}

		return mapPnrSegId;
	}

	/**
	 * Returns the Pnr Segment Id(s)
	 * 
	 * @param reservationPaxFare
	 * @return
	 */
	private Collection<Integer> getPnrSegIds(ReservationPaxFare reservationPaxFare) {
		Collection<Integer> pnrSegIds = new HashSet<Integer>();
		ReservationPaxFareSegment reservationPaxFareSegment;
		for (Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments()
				.iterator(); itReservationPaxFareSegment.hasNext();) {
			reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();
			pnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
		}

		return pnrSegIds;
	}

	private boolean checkNoShoreChargeApplied(ReservationPaxFare reservationPaxFare, Integer chgRateId) {
		Iterator<ReservationPaxOndCharge> itReservationPaxOndCharge = reservationPaxFare.getCharges().iterator();
		ReservationPaxOndCharge reservationPaxOndCharge;

		while (itReservationPaxOndCharge.hasNext()) {
			reservationPaxOndCharge = (ReservationPaxOndCharge) itReservationPaxOndCharge.next();

			if (reservationPaxOndCharge.getChargeRateId() != null
					&& reservationPaxOndCharge.getChargeRateId().equals(chgRateId)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Return fare Ids
	 * 
	 * @param reservationPaxFare
	 * @param fareIds
	 */
	private Collection<Integer> getFareIds(Collection<ReservationPaxFare> colReservationPaxFare) {
		Collection<Integer> fareIds = new HashSet<Integer>();
		ReservationPaxOndCharge reservationPaxOndCharge;
		Iterator<ReservationPaxOndCharge> itCharges;

		for (ReservationPaxFare reservationPaxFare : colReservationPaxFare) {
			itCharges = reservationPaxFare.getCharges().iterator();

			while (itCharges.hasNext()) {
				reservationPaxOndCharge = (ReservationPaxOndCharge) itCharges.next();

				if (reservationPaxOndCharge.getFareId() != null) {
					fareIds.add(reservationPaxOndCharge.getFareId());
				}
			}
		}

		return fareIds;
	}

	/**
	 * Returns the no shore charge amount
	 * 
	 * @param reservationPaxFare
	 * @param mapFareTO
	 * @return
	 */
	private BigDecimal getNoShoreChargeAmount(ReservationPaxFare reservationPaxFare, Map<Integer, FareTO> mapFareTO) {
		BigDecimal noShoreChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Integer fareId = getFareId(reservationPaxFare);
		FareTO fareTO = (FareTO) mapFareTO.get(fareId);

		if (fareTO != null) {
			ReservationPax reservationPax = reservationPaxFare.getReservationPax();

			if (ReservationApiUtils.isAdultType(reservationPax)) {
				noShoreChargeAmount = fareTO.getAdultNoShoreCharge();
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				noShoreChargeAmount = fareTO.getChildNoShoreCharge();
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				noShoreChargeAmount = fareTO.getInfantNoShoreCharge();
			}
		}

		return noShoreChargeAmount;
	}

	private FareTO getApplicableFareTO(ReservationPaxFare reservationPaxFare, Map<Integer, FareTO> mapFareTO) {
		Integer fareId = getFareId(reservationPaxFare);
		FareTO fareTO = (FareTO) mapFareTO.get(fareId);
		return fareTO;
	}

	/**
	 * The following method calculates the No Show Charge Amount according to the charge type defined for the given fare
	 * 
	 * @param reservationPaxFare
	 * @param fareTO
	 * @param noShowChargeAmount
	 * @return noShowChargeAmount
	 */
	private BigDecimal calculateNoShowChargeAmont(ReservationPaxFare reservationPaxFare, FareTO fareTO,
			BigDecimal noShowChargeAmount, Reservation reservation) throws ModuleException {
		String noShowChargeBasis = getNoShoreChargeBasis(reservationPaxFare, fareTO);
		BigDecimal noShowChargeBreakPoint = getNoShoreChargeBreakPoint(reservationPaxFare, fareTO);
		BigDecimal noShowChargeBoundry = getNoShoreChargeBoundary(reservationPaxFare, fareTO);
		if (fareTO != null && noShowChargeAmount.doubleValue() > 0) {
			BigDecimal fareAmount = reservationPaxFare.getTotalFare();

			if (ReservationModuleUtils.getAirReservationConfig().isNoShowChargeForDiscountedFare()) {
				fareAmount = AccelAeroCalculator.subtract(fareAmount, reservationPaxFare.getTotalDiscount());

				if (fareAmount.doubleValue() < 0) {
					fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				}
			}
			if ("PF".equals(noShowChargeBasis)) {
				BigDecimal noShowCharge = AccelAeroCalculator.calculatePercentage(fareAmount, noShowChargeAmount.intValue());
				noShowChargeAmount = AccelAeroRounderPolicy.getRoundedValue(noShowCharge, noShowChargeBoundry,
						noShowChargeBreakPoint);
			} else if ("PFS".equals(noShowChargeBasis)) {
				BigDecimal fareAndSurcharge = AccelAeroCalculator.add(fareAmount, reservationPaxFare.getTotalSurCharge());
				BigDecimal noShowCharge = AccelAeroCalculator.calculatePercentage(fareAndSurcharge,
						noShowChargeAmount.intValue());
				noShowChargeAmount = AccelAeroRounderPolicy.getRoundedValue(noShowCharge, noShowChargeBoundry,
						noShowChargeBreakPoint);
			} else if ("PTF".equals(noShowChargeBasis)) {
				// Finding the inverse segments and fares to get Total
				Map<Collection<Integer>, Collection<Integer>> mapPnrSegIdsAndInversePnrSegIds = ReservationApiUtils
						.getInverseSegments(reservation, true);
				Collection<Integer> pnrSegIds = this.getPnrSegIds(reservationPaxFare);
				Collection<Integer> inversePnrSegIds = (Collection<Integer>) mapPnrSegIdsAndInversePnrSegIds.get(pnrSegIds);
				BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (inversePnrSegIds.size() > 0) {
					ReservationPaxFare reservationPaxFareInverse = ReservationCoreUtils
							.getPnrPaxFare(reservationPaxFare.getReservationPax().getPnrPaxFares(), inversePnrSegIds);
					BigDecimal inverSegFareAmount = reservationPaxFareInverse.getTotalFare();
					if (ReservationModuleUtils.getAirReservationConfig().isNoShowChargeForDiscountedFare()) {
						inverSegFareAmount = AccelAeroCalculator.subtract(inverSegFareAmount,
								reservationPaxFareInverse.getTotalDiscount());
					}
					totalFare = AccelAeroCalculator.add(fareAmount, inverSegFareAmount);
				} else {
					totalFare = fareAmount;
				}

				BigDecimal noShowCharge = AccelAeroCalculator.calculatePercentage(totalFare, noShowChargeAmount.intValue());
				noShowChargeAmount = AccelAeroRounderPolicy.getRoundedValue(noShowCharge, noShowChargeBoundry,
						noShowChargeBreakPoint);
			}
		}
		return noShowChargeAmount;
	}

	/**
	 * Return fare Id
	 * 
	 * @param reservationPaxFare
	 * @param fareId
	 */
	private Integer getFareId(ReservationPaxFare reservationPaxFare) {
		Collection<ReservationPaxFare> colReservationPaxFare = new ArrayList<ReservationPaxFare>();
		colReservationPaxFare.add(reservationPaxFare);
		Collection<Integer> fareIds = getFareIds(colReservationPaxFare);

		return (Integer) BeanUtils.getFirstElement(fareIds);
	}

	/**
	 * Returns the no shore charge basis
	 * 
	 * @param reservationPaxFare
	 * @param mapFareTO
	 * @return
	 */
	private String getNoShoreChargeBasis(ReservationPaxFare reservationPaxFare, FareTO fareTO) {
		String noShowChargeBasis = null;

		if (fareTO != null) {
			ReservationPax reservationPax = reservationPaxFare.getReservationPax();

			if (ReservationApiUtils.isAdultType(reservationPax)) {
				noShowChargeBasis = fareTO.getAdultNoShowChargeBasis();
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				noShowChargeBasis = fareTO.getChildNoShowChargeBasis();
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				noShowChargeBasis = fareTO.getInfantNoShowChargeBasis();
			}
		}

		return noShowChargeBasis;
	}

	/**
	 * Returns the no shore charge BreakPoint
	 * 
	 * @param reservationPaxFare
	 * @param mapFareTO
	 * @return
	 */
	private BigDecimal getNoShoreChargeBreakPoint(ReservationPaxFare reservationPaxFare, FareTO fareTO) {
		BigDecimal noShowChargeBreakPoint = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (fareTO != null) {
			ReservationPax reservationPax = reservationPaxFare.getReservationPax();

			if (ReservationApiUtils.isAdultType(reservationPax)) {
				noShowChargeBreakPoint = fareTO.getAdultNoShowChargeBreakPoint();
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				noShowChargeBreakPoint = fareTO.getChildNoShowChargeBreakPoint();
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				noShowChargeBreakPoint = fareTO.getInfantNoShowChargeBreakPoint();
			}
		}

		return noShowChargeBreakPoint;
	}

	/**
	 * Returns the no shore charge Boundary
	 * 
	 * @param reservationPaxFare
	 * @param mapFareTO
	 * @return
	 */
	private BigDecimal getNoShoreChargeBoundary(ReservationPaxFare reservationPaxFare, FareTO fareTO) {
		BigDecimal noShoreChargeBoundary = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (fareTO != null) {
			ReservationPax reservationPax = reservationPaxFare.getReservationPax();

			if (ReservationApiUtils.isAdultType(reservationPax)) {
				noShoreChargeBoundary = fareTO.getAdultNoShowChargeBoundary();
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				noShoreChargeBoundary = fareTO.getChildNoShowChargeBoundary();
			} else if (ReservationApiUtils.isInfantType(reservationPax)) {
				noShoreChargeBoundary = fareTO.getInfantNoShowChargeBoundary();
			}
		}

		return noShoreChargeBoundary;
	}

	private void createPFSAudit(Collection<ReservationAudit> colReservationAudit, ReservationAudit reservationAudit,
			PfsPaxEntry pfsPaxEntry, Pfs pfs, Collection<FlightReconcileDTO> colSourceFlightReconcileDTO) {

		/** Holds the date,time format */
		SimpleDateFormat sdfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Collection<FlightReconcileDTO> colNorecFlightReconcileDTO = filterNORECFlightSeg(colSourceFlightReconcileDTO,
				pfsPaxEntry);
		String bookingClass = "";

		Iterator<FlightReconcileDTO> itFltReconcileDto = colNorecFlightReconcileDTO.iterator();
		String segment = "";
		Integer flightSegId = 0;
		while (itFltReconcileDto.hasNext()) {
			FlightReconcileDTO flightReconcileDTO = (FlightReconcileDTO) itFltReconcileDto.next();
			segment += flightReconcileDTO.getSegementCode() + " ";
			flightSegId = flightReconcileDTO.getFlightSegId();
		}

		bookingClass = paxFinalSalesDAO.getReservationBookingclass(pfsPaxEntry.getPnr(), flightSegId, false);

		// Got No_SHOW Booking class and segemnt
		if (reservationAudit.getContentMap().get(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_STATUS).equals(GO_SHOW)) {
			bookingClass = paxFinalSalesDAO.getReservationBookingclass(pfsPaxEntry.getPnr(), 0, true);
			segment = pfsPaxEntry.getDepartureAirport() + "/" + pfsPaxEntry.getArrivalAirport();
		}

		reservationAudit.setPnr(pfsPaxEntry.getPnr());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PAX_NAME,
				pfsPaxEntry.getFirstName() + " " + pfsPaxEntry.getLastName());
		// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.E_TICKET_NO, "");
		if (ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED.equals(pfsPaxEntry.getProcessedStatus())
				|| ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED.equals(pfsPaxEntry.getProcessedStatus())) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.PROCESS_ERROR,
					pfsPaxEntry.getErrorDescription());
		} else {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.FLIGHT_NO, pfsPaxEntry.getFlightNumber());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.SEGMENT,
					segment + " - " + sdfmt.format(pfs.getDepartureDate()));
			// reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.BOOKING_CLASS,
			// pfsPaxEntry.getCabinClassCode());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.BOOKING_CLASS, bookingClass);
			if (ReservationInternalConstants.PfsPaxStatus.NO_REC.equals(pfsPaxEntry.getEntryStatus())) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.TRAVEL_DATE,
						sdfmt.format(pfs.getDepartureDate()));
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.BOOKED_DATE,
						sdfmt.format(pfsPaxEntry.getFlightDate()));
			}
		}

		colReservationAudit.add(reservationAudit);
	}

	@SuppressWarnings("unchecked")
	private void postPfsAuditRecord(Collection<ReservationAudit> colReservationAudit, CredentialsDTO credentialsDTO)
			throws ModuleException {
		// Get the auditor deleagate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Iterator<ReservationAudit> itReservationAuditCol = colReservationAudit.iterator();
		while (itReservationAuditCol.hasNext()) {
			ReservationAudit reservationAudit = (ReservationAudit) itReservationAuditCol.next();

			reservationAudit.setCustomerId(credentialsDTO.getCustomerId());
			reservationAudit.setUserId(credentialsDTO.getUserId());
			reservationAudit.setZuluModificationDate(new Date());
			reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.IP_ADDDRESS,
						credentialsDTO.getTrackInfoDTO().getIpAddress());
			}

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PFSEntry.ORIGIN_CARRIER,
						credentialsDTO.getTrackInfoDTO().getCarrierCode());
			}

			reservationAudit.getContentMap().put(AuditTemplateEnum.TemplateParams.PFSEntry.REMOTE_USER,
					credentialsDTO.getDefaultCarrierCode() + "/" + "[" + credentialsDTO.getAgentCode() + "]" + "/" + "["
							+ credentialsDTO.getUserId() + "]");

			reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
		}
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		boolean loadFares = true;
		boolean loadLastUserNote = false;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.XBE);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadAutoCheckinInfo(true);

		return pnrModesDTO;
	}

	private LCCClientReservationPax trasformResPaxToLCCClientResPax(ReservationPax resPax) {
		LCCClientReservationPax lccResPax = new LCCClientReservationPax();
		lccResPax.setTitle(resPax.getTitle());
		lccResPax.setFirstName(resPax.getFirstName());
		lccResPax.setLastName(resPax.getLastName());
		lccResPax.setPaxSequence(resPax.getPaxSequence());
		String strCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		lccResPax.setTravelerRefNumber(strCarrierCode + "|" + PaxTypeUtils.travelerReference(resPax));
		return lccResPax;
	}

	private ExternalChgDTO getNoShoreCharge() throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.NO_SHORE);

		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, null);

		return (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.NO_SHORE);
	}

	public void updateAttachedInfantEticketStatus(ReservationPax resPax, String status, PfsPaxEntry pfsParsedEntry)
			throws ModuleException {
		if (resPax != null && resPax.getInfants() != null && resPax.getInfants().size() > 0) {

			Set<ReservationPax> infant = resPax.getInfants();
			Iterator<ReservationPax> itrInfant = infant.iterator();
			while (itrInfant.hasNext()) {
				ReservationPax resInfantPax = itrInfant.next();
				if (resInfantPax != null) {
					ETicketBO.updateETicketStatus(resInfantPax, getFlightSegmentIdsFromPFSParsedEntry(pfsParsedEntry), status,
							false, true);
				}

			}
		}

	}

	/**
	 * update the infant status in ReservationPaxFareSegment when processing the parent record
	 * 
	 * @param pnrSegId
	 * @param pnrPaxId
	 * @param reservationPax
	 * @param processStatus
	 * 
	 */
	public void updateAttachedInfantStatus(Integer pnrSegId, Integer pnrPaxId, ReservationPax reservationPax,
			String processStatus) throws ModuleException {

		if (reservationPax != null && reservationPax.getInfants() != null && reservationPax.getInfants().size() > 0) {

			Set<ReservationPax> infant = reservationPax.getInfants();
			Iterator<ReservationPax> itrInfant = infant.iterator();
			while (itrInfant.hasNext()) {
				ReservationPax resInfantPax = itrInfant.next();
				if (resInfantPax != null) {

					Iterator<ReservationPaxFare> itReservationPaxFare = resInfantPax.getPnrPaxFares().iterator();

					while (itReservationPaxFare.hasNext()) {
						ReservationPaxFare reservationPaxFare = (ReservationPaxFare) itReservationPaxFare.next();

						Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments()
								.iterator();

						while (itReservationPaxFareSegment.hasNext()) {
							ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment
									.next();

							if (pnrSegId.equals(reservationPaxFareSegment.getPnrSegId())) {

								if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
										.equals(reservationPaxFareSegment.getSegment().getStatus())) {

									// Only updating the confirmed entries or flown entries
									if (ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED
											.equals(reservationPaxFareSegment.getStatus())
											|| ReservationInternalConstants.PaxFareSegmentTypes.FLOWN
													.equals(reservationPaxFareSegment.getStatus())) {
										reservationPaxFareSegment.setStatus(processStatus);

									}
								}

							}
						}
					}

				}
			}
		}

	}

	private List<ExternalChgDTO> calculateServiceTax(Reservation reservation, ExternalChgDTO externalChgDTO, BigDecimal amount,
			ReservationPax reservationPax) throws ModuleException {
		ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = new ServiceTaxQuoteForTicketingRevenueRQ();
		List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<>();
		for (ReservationSegmentDTO reservationSegmentDTO : ReservationApiUtils
				.getConfirmedSegments(reservation.getSegmentsView())) {
			SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
			simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(reservationSegmentDTO.getZuluDepartureDate());
			simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(reservationSegmentDTO.getZuluArrivalDate());
			simplifiedFlightSegmentDTO.setSegmentCode(reservationSegmentDTO.getSegmentCode());
			simplifiedFlightSegmentDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(reservationSegmentDTO));
			simplifiedFlightSegmentDTO.setLogicalCabinClassCode(reservationSegmentDTO.getLogicalCCCode());
			simplifiedFlightSegmentDTO.setOperatingAirline(reservationSegmentDTO.getCarrierCode());
			simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
		}

		Collections.sort(simplifiedFlightSegmentDTOs);
		int segmentSequence = 0;
		String flightRefNumber = null;
		for (SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO : simplifiedFlightSegmentDTOs) {
			simplifiedFlightSegmentDTO.setSegmentSequence(segmentSequence);
			segmentSequence++;
			if (flightRefNumber == null) {
				flightRefNumber = simplifiedFlightSegmentDTO.getFlightRefNumber();
			}
		}
		serviceTaxRQ.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);

		Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = new HashMap<>();
		List<SimplifiedChargeDTO> chargesList = new ArrayList<>();
		SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
		paxDTO.setPaxSequence(reservationPax.getPaxSequence());
		paxDTO.setPaxType(reservationPax.getPaxType());
		SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
		chargeDTO.setAmount(amount);
		chargeDTO.setChargeGroupCode(externalChgDTO.getChgGrpCode());
		chargeDTO.setChargeCode(externalChgDTO.getChargeCode());
		chargeDTO.setFlightRefNumber(flightRefNumber);
		chargesList.add(chargeDTO);
		paxWiseCharges.put(paxDTO, chargesList);
		serviceTaxRQ.setPaxWiseExternalCharges(paxWiseCharges);

		serviceTaxRQ.setPaxState(reservation.getContactInfo().getState());
		serviceTaxRQ.setPaxCountryCode(reservation.getContactInfo().getCountryCode());
		serviceTaxRQ.setPaxTaxRegistered(StringUtils.isNoneBlank(reservation.getContactInfo().getTaxRegNo()));
		ServiceTaxQuoteForTicketingRevenueRS serviceTaxRS = ReservationModuleUtils.getChargeBD()
				.quoteServiceTaxForTicketingRevenue(serviceTaxRQ);
		List<ExternalChgDTO> colExternalChgDTOs = new ArrayList<>();
		if (serviceTaxRS != null && !CollectionUtils.isEmpty(serviceTaxRS.getPaxWiseServiceTaxes())) {
			List<ServiceTaxDTO> serviceTaxs = serviceTaxRS.getPaxWiseServiceTaxes().get(reservationPax.getPaxSequence());

			if (serviceTaxs != null && !serviceTaxs.isEmpty()) {
				if (serviceTaxs != null) {
					for (ServiceTaxDTO serviceTax : serviceTaxs) {
						ServiceTaxExtChgDTO serviceTaxExtChgDTO = ServiceTaxConverterUtil
								.externalChargeAdaptor(serviceTax, serviceTaxRS.getServiceTaxDepositeCountryCode(),
										serviceTaxRS.getServiceTaxDepositeStateCode());
						colExternalChgDTOs.add(serviceTaxExtChgDTO);
					}
				}
			}
		}
		return colExternalChgDTOs;
	}


}
