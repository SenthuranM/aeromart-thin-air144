/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class AvailableSpaceRule extends BaseRule<RulesDataContext> {

	public static final int MAXIMUM_CHARACTERS_PER_LINE = 64;

	@Override
	public boolean validateRule(RulesDataContext context) {
		boolean isValied = false;

		int currentLineLength = getCurrentLineLength(context.getCurrentLine());
		int ammendingLineLength = getAmmendingLineLength(context
				.getAmmendingLine());
		int reductionLength = context.getReductionLength();
		int totalLineLength = getTotalLineLength(currentLineLength,
				ammendingLineLength) + reductionLength;

		if (totalLineLength <= MAXIMUM_CHARACTERS_PER_LINE) {
			isValied = true;
		}
		return isValied;
	}

	private int getTotalLineLength(int currentLineLength,
			int ammendingLineLength) {
		return currentLineLength + ammendingLineLength;
	}

	private int getCurrentLineLength(String currentLine) {
		int length = 0;
		if (currentLine != null) {
			length = currentLine.length();
		}
		return length;
	}

	private int getAmmendingLineLength(String ammendingLine) {
		int length = 0;
		if (ammendingLine != null) {
			length = ammendingLine.length();
		}
		return length;
	}
}
