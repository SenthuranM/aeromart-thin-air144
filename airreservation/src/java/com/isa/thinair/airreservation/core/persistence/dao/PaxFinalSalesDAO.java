/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.PFSMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.pfs.CSPaxBookingCount;
import com.isa.thinair.airreservation.api.dto.pfs.PFSDTO;
import com.isa.thinair.airreservation.api.model.CodeSharePfs;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * PaxFinalSalesDAO is the business DAO interface for the passenger final sales (PFS) service apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface PaxFinalSalesDAO {
	/**
	 * 
	 * @param pfsID
	 * @return
	 */
	public Pfs getPFS(int pfsID);

	/**
	 * Returns Page object with PFS data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 */
	public Page getPagedPFSData(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList);

	/**
	 * Save Pfs Entry
	 * 
	 * @param pfs
	 */
	public void savePfsEntry(Pfs pfs);

	/**
	 * 
	 * @param pfs
	 * @return ppId
	 */
	public int savePfsEntryForAudit(Pfs pfs);

	/**
	 * Delete the pfs
	 * 
	 * @param pfsParsed
	 */
	public void deletePfsEntry(Pfs pfs);

	/**
	 * Return pfs parse entries
	 * 
	 * @param pfsId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<PfsPaxEntry> getPfsParseEntries(int pfsId) throws CommonsDataAccessException;

	/**
	 * Delete the PfsParseEntry
	 * 
	 * @param pfsParsed
	 */
	public void deletePfsParseEntry(PfsPaxEntry pfsParsed);

	/**
	 * Return pfs parse entries count
	 * 
	 * @param pfsId
	 * @param processedStatus
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public int getPfsParseEntriesCount(int pfsId, String processedStatus) throws CommonsDataAccessException;

	/**
	 * Save Pfs Parse Entry
	 * 
	 * @param pfsParsed
	 */
	public void savePfsParseEntry(PfsPaxEntry pfsParsed);

	/**
	 * Save Pfs Parse Entries
	 * 
	 * @param colPfsParseEntry
	 */
	public void savePfsParseEntries(Collection<PfsPaxEntry> colPfsParseEntry);

	/**
	 * Return pfs parse entries
	 * 
	 * @param pfsId
	 * @param status
	 * @return
	 */
	public Collection<PfsPaxEntry> getPfsParseEntries(int pfsId, String status);

	/**
	 * Returns pfs entries count
	 * 
	 * @param flightNo
	 * @param dateDownloaded
	 * @param departureDate
	 * @return
	 */
	public int getPfsEntriesCount(String flightNo, String airport, Date departureDate);

	/**
	 * Returns the true if a pfs already exists for the parsed params
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @return
	 */
	public boolean hasPFS(String flightNumber, String airportCode, Date departureDate);

	/**
	 * 
	 * @param pfsID
	 * @param statuss
	 * @return
	 */
	public Collection<String> getPaxNamesForMail(Integer pfsID, Collection<String> statuss);

	/**
	 * 
	 * @param pfsID
	 * @return
	 */
	public int getPFS(String fromAirport, String flightNumber, Date departureDate);

	/**
	 * 
	 * @param pfsID
	 * @param pnr
	 * @param paxType
	 * @return
	 */
	public int getPaxTypeCount(Integer pfsID, String pnr, String paxType);

	/**
	 * 
	 * @param pnr
	 * @return
	 */
	public String getReservationCabinclass(String pnr, Integer pnrSegID);

	/**
	 * 
	 * @param pnr
	 * @return Booking Class
	 */
	public String getReservationBookingclass(String pnr, Integer flightSegId, boolean isGoShow);

	/**
	 * 
	 * @param PfsPaxEntry
	 * @return
	 */
	public void saveParent(PfsPaxEntry pfsParsed);

	/**
	 * 
	 * @param PPID
	 * @return PfsPaxEntry
	 */
	public PfsPaxEntry getParent(Integer childInfPPID);

	/**
	 * 
	 * @param PPID
	 * @return PfsPaxEntry
	 */
	public PfsPaxEntry getPFSPaxEntry(Integer ppId);

	public boolean hasPFSFile(Date date);
	
	public boolean checkIfPfsAlreadyExists(Date departureDate, String flightNumber, String fromAirport);

	public void deleteAllErrorEntries(int pfsId) throws CommonsDataAccessException;

	public boolean isNOSHOPaxEntry(String eTicketNo, String paxStatus, String segmentCode);
	
	public boolean isFlownOrNoShowPaxEntry(String eTicketNo, String segmentCode);

	/**
	 * 
	 * @param PassedDateCount
	 * @return Map<Integer, PFSMetaDataDTO>
	 */
	public Map<Integer, PFSMetaDataDTO> getWaitingPFSList(Integer passedDateCount);
	
	public Collection<PFSDTO> getCodeShareFlightDetailsForPfs(String flightNumber, Date departureDate);
	
	public Collection<CSPaxBookingCount> getCodeSharePaxCountByBookingClass(String csFlightNumber, Date departureDate,String carrierCode);
	
	public void saveCodeSharePfsEntry(CodeSharePfs pfs) throws CommonsDataAccessException;
	
	public boolean checkCodeSharePfsAlreadySent(Date departureDate, String flightNumber, String fromAirport,String carrierCode) throws CommonsDataAccessException;
}
