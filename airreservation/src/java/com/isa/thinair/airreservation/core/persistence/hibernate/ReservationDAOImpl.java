/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.type.IntegerType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airreservation.api.dto.CarrierDTO;
import com.isa.thinair.airreservation.api.dto.FlightConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldBookingInfoDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO.MODULE;
import com.isa.thinair.airreservation.api.dto.OnholdReservatoinSearchDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ResContactInfoSearchDTO;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationBasicsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryStatusDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.FlightSegReminderNotificationDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.NotificationDetailInfoDTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.PnrFlightInfoDTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.PNRInfoDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVResInfoDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakInsuredTraveler;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.model.OnholdAlert;
import com.isa.thinair.airreservation.api.model.PassengerBaggage;
import com.isa.thinair.airreservation.api.model.PassengerMeal;
import com.isa.thinair.airreservation.api.model.PassengerSeating;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.model.PromotionAttribute;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AncillaryReminderBL;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * ReservationDAOImpl is the business DAO hibernate implementation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationDAO"
 */
public class ReservationDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationDAOImpl.class);

	private static final String DLIM = "\\^";

	/**
	 * The PNR generation method indicates whether to use a sequence ("S") or a function ("F") to generate the PNR
	 */
	private static String pnrGenerationMethod = "S";

	/**
	 * pnrLength indicates the length of the PNR. Will only be use by the function.
	 */
	private static int pnrLength = 6;

	/**
	 * funnctionOrSequenceName indicates the function or sequence to be used to generate the sequence
	 */
	private static String functionOrSequenceName = "S_RESERVATION";
	private static String functionOrSequenceNameGds = "S_GDS_RESERVATION";

	/**
	 * SQL statement that will be used to create PNRs.
	 */
	private static String pnrGenerationSql;
	private static String pnrGenerationSqlGds;

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH.mm.ss");

	static {
		Map<String, String> configMap = ReservationModuleUtils.getAirReservationConfig().getPnrGenerationConfigMap();
		if (configMap != null) {
			pnrGenerationMethod = configMap.get("type") == null ? pnrGenerationMethod : configMap.get("type").trim();
			pnrLength = configMap.get("length") == null ? pnrLength : Integer.parseInt(configMap.get("length"));
			functionOrSequenceName = configMap.get("name") == null ? functionOrSequenceName : configMap.get("name").trim();
		}

		if (pnrGenerationMethod.equalsIgnoreCase("S")) {
			pnrGenerationSql = "SELECT " + functionOrSequenceName + ".NEXTVAL PNR FROM DUAL";
			pnrGenerationSqlGds = "SELECT " + functionOrSequenceNameGds + ".NEXTVAL PNR FROM DUAL";
		} else {
			pnrGenerationSql = "{CALL ?:=" + functionOrSequenceName + "(?)}";
			pnrGenerationSqlGds = "{CALL ?:=" + functionOrSequenceNameGds + "(?)}";
		}
	}

	/**
	 * Saves the given reservation
	 * 
	 * @param reservation
	 */
	@Override
	public Reservation saveReservation(Reservation reservation) {
		log.debug("Inside saveReservation");
		reservation.getStatus();
		hibernateSaveOrUpdate(reservation);

		if (reservation.getContactInfo() != null) {
			log.debug("In saving reservation contact info.");
			hibernateSaveOrUpdate(reservation.getContactInfo());
			log.debug("Reservation contact info saved.");
		}

		log.debug("Exit saveReservation");
		return reservation;
	}

	/**
	 * Returns a reservation
	 * 
	 * @param pnr
	 * @param loadFares
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@Override
	public Reservation getReservation(String pnr, boolean loadFares) throws CommonsDataAccessException {
		Reservation reservation = (Reservation) get(Reservation.class, pnr);

		if (reservation != null) {
			// Load fares
			if (loadFares) {
				Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
				ReservationPax reservationPax;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}
		} else {
			log.error("Reservation is null : " + pnr);
			throw new CommonsDataAccessException("airreservations.arg.noLongerExist");
		}

		return reservation;
	}

	/**
	 * Returns the reservation by the originator PNR
	 * 
	 * @param originatorPnr
	 * @param loadFares
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Reservation getReservationByOriginatorPnr(String originatorPnr, boolean loadFares) throws CommonsDataAccessException {
		String hql = "SELECT r FROM Reservation r WHERE r.originatorPnr = ? ";
		Object[] params = { originatorPnr };
		Collection<Reservation> colReservation = find(hql, params, Reservation.class);
		Reservation reservation = null;

		if (colReservation != null && colReservation.size() > 0) {
			reservation = BeanUtils.getFirstElement(colReservation);
			// Load fares
			if (loadFares) {
				Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
				ReservationPax reservationPax;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}
		}

		return reservation;
	}

	public Collection<Reservation> getReservationsByPnr(Collection<String> pnrList, boolean loadFares)
			throws CommonsDataAccessException {
		String hql = "SELECT r FROM Reservation r WHERE r.pnr IN ( " + BeanUtils.constructINString(pnrList) + " )";
		Collection<Reservation> reservationList = find(hql, Reservation.class);
		for (Reservation reservation : reservationList) {
			if (loadFares) {
				Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
				ReservationPax reservationPax;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}
		}
		return reservationList;
	}

	public Collection<Reservation> getReservationsByFlightSegmentId(Integer flightSegId, boolean loadFares)
			throws ModuleException {
		String hql = "SELECT rs.reservation FROM ReservationSegment rs WHERE rs.status = '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' AND rs.flightSegId = " + flightSegId;

		Collection<Reservation> reservationList = find(hql, Reservation.class);
		for (Reservation reservation : reservationList) {
			if (loadFares) {
				Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
				ReservationPax reservationPax;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}
		}
		return reservationList;
	}

	/**
	 * Returns the reservation by the originator PNR
	 * 
	 * @param loadFares
	 * @param originatorPnr
	 * 
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Reservation getReservationByExtRecordLocator(String extRLoc, String creationDate, boolean loadFares)
			throws CommonsDataAccessException {

		Reservation reservation = null;

		String hql = "SELECT r FROM Reservation r WHERE r.externalRecordLocator like ? ";

		// 2011-03-02T08:07:58Z - Date format
		String recordLoc = extRLoc;
		if (creationDate != null && creationDate.length() >= 10) {
			recordLoc = extRLoc + creationDate.substring(0, 10);

			Object[] paramsOnlyDate = { recordLoc + "%" };
			Collection<Reservation> colReservation = find(hql, paramsOnlyDate, Reservation.class);

			if (colReservation != null && colReservation.size() > 0) {
				if (colReservation.size() > 1 && creationDate.length() >= 16) {
					recordLoc = recordLoc + creationDate.substring(10, 16);
					Object[] paramsDateTime = { recordLoc + "%" };
					colReservation = find(hql, paramsDateTime, Reservation.class);
				}

				if (colReservation != null && colReservation.size() > 0) {
					reservation = BeanUtils.getFirstElement(colReservation);
					// Load fares
					if (loadFares) {
						Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
						ReservationPax reservationPax;
						while (itReservationPax.hasNext()) {
							reservationPax = itReservationPax.next();
							reservationPax.getPnrPaxFares().isEmpty();
						}
					}
				}
			}

		} else {
			// create date is null
			Object[] paramsOnlyDate = { recordLoc + "%" };
			Collection<Reservation> colReservation = find(hql, paramsOnlyDate, Reservation.class);
			reservation = BeanUtils.getFirstElement(colReservation);
			if (reservation != null && loadFares) {
				Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
				ReservationPax reservationPax;
				while (itReservationPax.hasNext()) {
					reservationPax = itReservationPax.next();
					reservationPax.getPnrPaxFares().isEmpty();
				}
			}

		}

		return reservation;
	}

	/**
	 * Retrive the Candidate parent reservation for given externalLocator
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Reservation getCandidateParentReservationByExtRecordLocator(String extRLoc) throws CommonsDataAccessException {
		String hql = "SELECT r FROM Reservation r WHERE r.externalRecordLocator like ? and r.totalPaxInfantCount = 0";
		Object[] params = { extRLoc + "%" };
		Collection<Reservation> colReservation = find(hql, params, Reservation.class);
		Reservation reservation = BeanUtils.getFirstElement(colReservation);
		return reservation;
	}

	/**
	 * Returns Reservation contact information
	 * 
	 * @param pnr
	 * @return
	 * @throws CommonsDataAccessException
	 */
	@Override
	public ReservationContactInfo getContactInfo(String pnr) throws CommonsDataAccessException {
		return this.getReservation(pnr, false).getContactInfo();
	}

	/**
	 * Returns reservation passengers
	 * 
	 * @param pnr
	 * @param loadFares
	 * @return
	 */
	@Override
	public Collection<ReservationPax> getPassengers(String pnr, boolean loadFares) {
		String hql = "SELECT p FROM ReservationPax p WHERE p.reservation.pnr = ? ";
		Object[] params = { pnr };
		Collection<ReservationPax> passengers = find(hql, params, ReservationPax.class);

		if (loadFares) {
			Iterator<ReservationPax> iterator = passengers.iterator();
			ReservationPax reservationPax;

			while (iterator.hasNext()) {
				reservationPax = iterator.next();
				reservationPax.getPnrPaxFares().isEmpty();
			}
		}

		return passengers;
	}

	/**
	 * Returns reservation segments
	 * 
	 * @param pnr
	 * @return
	 */
	@Override
	public Collection<ReservationSegment> getSegments(String pnr) {
		String hql = "SELECT rs FROM ReservationSegment rs WHERE rs.reservation.pnr = ? ";

		Object[] params = { pnr };
		return find(hql, params, ReservationSegment.class);
	}

	/**
	 * Compose the SearchReservation SQL Used by getReservations
	 * 
	 * @param reservationSearchDTO
	 * @param isExternalTable
	 *            : A boolean flag to indicate that the where clause should be generated to query external tables or
	 *            not.
	 * @return
	 */
	private String composeGetReservationsSql(ReservationSearchDTO reservationSearchDTO, boolean isExternalTable) {
		Map<String, Boolean> map = new LinkedHashMap<String, Boolean>();

		String filterByOwnerAgentCodeSql = "";
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode())) {
			filterByOwnerAgentCodeSql += " AND ( r.OWNER_AGENT_CODE = ? ";
			if (reservationSearchDTO.isSearchIBEBookings()) {
				filterByOwnerAgentCodeSql += " OR (r.OWNER_AGENT_CODE is null AND (r.OWNER_CHANNEL_CODE=' "
						+ ReservationInternalConstants.SalesChannel.WEB + "' OR r.OWNER_CHANNEL_CODE=' "
						+ ReservationInternalConstants.SalesChannel.IOS + "'OR r.OWNER_CHANNEL_CODE=' "
						+ ReservationInternalConstants.SalesChannel.ANDROID + "' )) ";
			}
			if (reservationSearchDTO.isSearchGSABookings()) {
				filterByOwnerAgentCodeSql += " OR r.OWNER_AGENT_CODE in ( SELECT agent_code from  t_agent where report_to_gsa ='Y' and gsa_code in (SELECT AG.AGENT_CODE FROM T_AGENT AG WHERE AG.AGENT_TYPE_CODE='GSA' AND AG.AGENT_CODE=?))";
			}
			if (reservationSearchDTO.getBookingCategories() != null && !reservationSearchDTO.getBookingCategories().isEmpty()) {
				filterByOwnerAgentCodeSql += " OR r.BOOKING_CATEGORY_CODE IN (  "
						+ BeanUtils.constructINString(reservationSearchDTO.getBookingCategories()) + " ) ";
			}
			filterByOwnerAgentCodeSql += " ) ";
		}

		map.put(" AND ( rCon.UPPER_C_FIRST_NAME LIKE ? OR p.UPPER_FIRST_NAME LIKE ? ) ",
				BeanUtils.isNullHandler(reservationSearchDTO.getFirstName()));
		map.put(" AND ( rCon.UPPER_C_LAST_NAME LIKE ? OR p.UPPER_LAST_NAME LIKE ? ) ",
				BeanUtils.isNullHandler(reservationSearchDTO.getLastName()));
		map.put(" AND r.PNR LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getPnr()));
		map.put(" AND r.ORIGINATOR_PNR LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginatorPnr()));
		map.put(" AND pa.PASSPORT_NUMBER LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getPassport()));
		map.put(" AND fSeg.Segment_Code LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getFromAirport()));
		map.put(" AND fSeg.SEGMENT_CODE LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getToAirport()));
		map.put(" AND fSeg.EST_TIME_DEPARTURE_LOCAL >= ? ", BeanUtils.isNullHandler(reservationSearchDTO.getDepartureDate()));
		map.put(" AND fSeg.EST_TIME_ARRIVAL_LOCAL <= ? ", BeanUtils.isNullHandler(reservationSearchDTO.getReturnDate()));
		map.put(" AND fSeg.EST_TIME_DEPARTURE_LOCAL <= ? ", BeanUtils.isNullHandler(reservationSearchDTO.getEarlyDepartureDate()));
		map.put(" AND ( rCon.C_PHONE_NO LIKE ? OR rCon.C_MOBILE_NO LIKE ? ) ",
				BeanUtils.isNullHandler(reservationSearchDTO.getTelephoneNo()));
		map.put(" AND rCon.CUSTOMER_ID = ? ", BeanUtils.isNullHandler(reservationSearchDTO.getCustomerId()));
		if (!isExternalTable) {
			// for own reservations we have the flight number at t_flight table
			map.put(" AND f.FLIGHT_NUMBER LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getFlightNo()));
		} else {
			// for dummy reservations we have the flight number in t_ext_flight_segment
			map.put(" AND fSeg.FLIGHT_NUMBER LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getFlightNo()));
		}

		map.put(" AND r.ORIGIN_CHANNEL_CODE IN ("
				+ (reservationSearchDTO.getOriginChannelIds() == null ? "" : BeanUtils
						.constructINStringForInts(reservationSearchDTO.getOriginChannelIds())) + ")",
				BeanUtils.isNullHandler(reservationSearchDTO.getOriginChannelIds()));
		map.put(" AND r.OWNER_CHANNEL_CODE = ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOwnerChannelId()));
		map.put(" AND r.ORIGIN_AGENT_CODE LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginAgentCode()));
		map.put(filterByOwnerAgentCodeSql, BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode()));
		map.put(" AND r.ORIGIN_USER_ID LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginUserId()));
		map.put(" AND r.STATUS IN ("
				+ (reservationSearchDTO.getReservationStates() == null ? "" : BeanUtils.constructINString(reservationSearchDTO
						.getReservationStates())) + ")", BeanUtils.isNullHandler(reservationSearchDTO.getReservationStates()));
		if (!isExternalTable) {
			map.put(" AND bookclass.BC_TYPE LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getBookingClassType()));
		} else {
			// we don't have the booking class information for dummy reservations
			// TODO- change this one open return support for interline and dry are in place.
			map.put("AND 'NORMAL' LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getBookingClassType()));
		}

		// Generating the dynamic sql
		return BeanUtils.sqlGenerator(map, false);
	}

	private String composeGetReservationsSqlForSimpleSearch(ReservationSearchDTO reservationSearchDTO) {
		String sqlWhereClause = "";
		String strWhereClauseForSimpleSearch = composeWhereClauseForSimpleSearch(reservationSearchDTO);
		String strWhereClauseForIBECustomerBookngs = "";
		String strWhereClauseForTelephoneNoSearch = "";
		String strWhereClauseForPassportSearch = "";

		if (!BeanUtils.isNull(reservationSearchDTO.getCustomerId())) {
			strWhereClauseForIBECustomerBookngs = " rCon.CUSTOMER_ID = ? ";
		}

		if (!BeanUtils.isNull(reservationSearchDTO.getPassport())) {
			strWhereClauseForPassportSearch = " pa.PASSPORT_NUMBER = ? ";
		}

		if (!BeanUtils.isNull(reservationSearchDTO.getTelephoneNo())) {
			strWhereClauseForTelephoneNoSearch = " ( rCon.C_PHONE_NO LIKE ? OR rCon.C_MOBILE_NO LIKE ? ) ";
		}

		String equalsOp = reservationSearchDTO.isExactMatch() ? "=" : "LIKE";

		if (!BeanUtils.isNull(reservationSearchDTO.getCustomerId())) {// IBE Registered User Bookings
			sqlWhereClause = "SELECT distinct r.PNR PNR_NUMBER, r.booking_timestamp "
					+ " FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR "
					+ strWhereClauseForIBECustomerBookngs;
		} else if (!BeanUtils.isNull(reservationSearchDTO.getPassport())) { // Only passport number specified
			sqlWhereClause = " SELECT distinct r.PNR PNR_NUMBER , r.booking_timestamp "
					+ " FROM T_RESERVATION r, T_PNR_PASSENGER p, t_pnr_pax_additional_info pa WHERE r.PNR = p.PNR AND p.PNR_PAX_ID=pa.PNR_PAX_ID "
					+ " AND " + strWhereClauseForPassportSearch;

		} else if (BeanUtils.isNull(reservationSearchDTO.getFirstName()) && BeanUtils.isNull(reservationSearchDTO.getLastName())
				&& !BeanUtils.isNull(reservationSearchDTO.getTelephoneNoForSQL())) {// Only TelephoneNo specified

			sqlWhereClause = "SELECT distinct r.PNR PNR_NUMBER, r.booking_timestamp "
					+ " FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR "
					+ strWhereClauseForTelephoneNoSearch;
		} else {
			if (!BeanUtils.isNull(reservationSearchDTO.getFirstName()) && !BeanUtils.isNull(reservationSearchDTO.getLastName())) {
				sqlWhereClause = "SELECT distinct r.PNR PNR_NUMBER, r.booking_timestamp "
						+ "FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE r.pnr=rCon.pnr AND rCon.UPPER_C_FIRST_NAME "
						+ equalsOp
						+ " ? AND rCon.UPPER_C_LAST_NAME "
						+ equalsOp
						+ " ? "
						+ (!"".equals(strWhereClauseForTelephoneNoSearch) ? " AND " : "")
						+ strWhereClauseForTelephoneNoSearch
						+ strWhereClauseForSimpleSearch
						+ " UNION "
						+ "SELECT distinct r.PNR PNR_NUMBER , r.booking_timestamp "
						+ "FROM T_RESERVATION r, T_PNR_PASSENGER p, T_RESERVATION_CONTACT rCon WHERE r.PNR = p.PNR AND r.PNR=rCon.PNR "
						+ "AND p.UPPER_FIRST_NAME " + equalsOp + " ? AND p.UPPER_LAST_NAME " + equalsOp + " ? "
						+ (!"".equals(strWhereClauseForTelephoneNoSearch) ? " AND " : "") + strWhereClauseForTelephoneNoSearch
						+ strWhereClauseForSimpleSearch;

			} else if (!BeanUtils.isNull(reservationSearchDTO.getFirstName())) {
				sqlWhereClause = "SELECT distinct r.PNR PNR_NUMBER, r.booking_timestamp "
						+ "FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR AND rCon.UPPER_C_FIRST_NAME "
						+ equalsOp
						+ " ? "
						+ (!"".equals(strWhereClauseForTelephoneNoSearch) ? " AND " : "")
						+ strWhereClauseForTelephoneNoSearch
						+ strWhereClauseForSimpleSearch
						+ " UNION "
						+ "SELECT distinct r.PNR PNR_NUMBER , r.booking_timestamp "
						+ "FROM T_RESERVATION r, T_PNR_PASSENGER p, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR AND r.PNR = p.PNR "
						+ "AND p.UPPER_FIRST_NAME " + equalsOp + " ? "
						+ (!"".equals(strWhereClauseForTelephoneNoSearch) ? " AND " : "") + strWhereClauseForTelephoneNoSearch
						+ strWhereClauseForSimpleSearch;

			} else if (!BeanUtils.isNull(reservationSearchDTO.getLastName())) {
				sqlWhereClause = "SELECT distinct r.PNR PNR_NUMBER, r.booking_timestamp "
						+ "FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR AND rCon.UPPER_C_LAST_NAME "
						+ equalsOp
						+ " ? "
						+ (!"".equals(strWhereClauseForTelephoneNoSearch) ? " AND " : "")
						+ strWhereClauseForTelephoneNoSearch
						+ strWhereClauseForSimpleSearch
						+ " UNION "
						+ "SELECT distinct r.PNR PNR_NUMBER , r.booking_timestamp "
						+ "FROM T_RESERVATION r, T_PNR_PASSENGER p, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR AND r.PNR = p.PNR "
						+ "AND p.UPPER_LAST_NAME " + equalsOp + " ? "
						+ (!"".equals(strWhereClauseForTelephoneNoSearch) ? " AND " : "") + strWhereClauseForTelephoneNoSearch
						+ strWhereClauseForSimpleSearch;
			}
		}

		return sqlWhereClause;
	}

	private String composeWhereClauseForSimpleSearch(ReservationSearchDTO reservationSearchDTO) {

		Map<String, Boolean> map = new LinkedHashMap<String, Boolean>();

		String filterByOwnerAgentCodeSql = "";
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode())) {
			filterByOwnerAgentCodeSql += " AND ( r.OWNER_AGENT_CODE = ? ";
			if (reservationSearchDTO.isSearchIBEBookings()) {
				filterByOwnerAgentCodeSql += " OR (r.OWNER_AGENT_CODE is null AND (r.OWNER_CHANNEL_CODE=' "
						+ ReservationInternalConstants.SalesChannel.WEB + "' OR r.OWNER_CHANNEL_CODE=' "
						+ ReservationInternalConstants.SalesChannel.IOS + "'OR r.OWNER_CHANNEL_CODE=' "
						+ ReservationInternalConstants.SalesChannel.ANDROID + "' )) ";
			}
			if (reservationSearchDTO.isSearchGSABookings()) {
				filterByOwnerAgentCodeSql += " OR r.OWNER_AGENT_CODE in ( SELECT agent_code from  t_agent where report_to_gsa ='Y' and gsa_code in (SELECT AG.AGENT_CODE FROM T_AGENT AG WHERE AG.AGENT_TYPE_CODE='GSA' AND AG.AGENT_CODE=?))";
			}
			if (reservationSearchDTO.isSearchAllReportingAgentBookings()) {
				filterByOwnerAgentCodeSql += "OR r.OWNER_AGENT_CODE in ( SELECT AGENT_CODE FROM t_agent CONNECT BY PRIOR agent_code = gsa_code START WITH agent_code = ?)";
			} else if (reservationSearchDTO.isSearchReportingAgentBookings()) {
				filterByOwnerAgentCodeSql += "OR r.OWNER_AGENT_CODE in ( SELECT AGENT_CODE FROM t_agent WHERE gsa_code = ? OR agent_code = ?)";
			}
			if (reservationSearchDTO.getBookingCategories() != null && !reservationSearchDTO.getBookingCategories().isEmpty()) {
				filterByOwnerAgentCodeSql += " OR r.BOOKING_CATEGORY_CODE IN (  "
						+ BeanUtils.constructINString(reservationSearchDTO.getBookingCategories()) + " ) ";
			}
			filterByOwnerAgentCodeSql += " ) ";
		}

		map.put(" AND r.ORIGIN_CHANNEL_CODE IN ("
				+ (reservationSearchDTO.getOriginChannelIds() == null ? "" : BeanUtils
						.constructINStringForInts(reservationSearchDTO.getOriginChannelIds())) + ")",
				BeanUtils.isNullHandler(reservationSearchDTO.getOriginChannelIds()));
		map.put(" AND r.OWNER_CHANNEL_CODE = ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOwnerChannelId()));
		map.put(" AND r.ORIGIN_AGENT_CODE LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginAgentCode()));
		map.put(filterByOwnerAgentCodeSql, BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode()));
		map.put(" AND r.ORIGIN_USER_ID LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginUserId()));
		map.put(" AND r.STATUS IN ("
				+ (reservationSearchDTO.getReservationStates() == null ? "" : BeanUtils.constructINString(reservationSearchDTO
						.getReservationStates())) + ")", BeanUtils.isNullHandler(reservationSearchDTO.getReservationStates()));
		// Generating the dynamic sql
		return BeanUtils.sqlGenerator(map, false);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeArgsForSimpleSearch(ReservationSearchDTO reservationSearchDTO) {
		// Generating the dynamic values
		Collection values = new ArrayList();

		// Owner Channel Code
		if (!BeanUtils.isNull(reservationSearchDTO.getOwnerChannelId())) {
			values.add(reservationSearchDTO.getOwnerChannelId());
		}

		// Origin Agent Code
		if (!BeanUtils.isNull(reservationSearchDTO.getOriginAgentCode())) {
			values.add(reservationSearchDTO.getOriginAgentCode());
		}

		// Owner Agent Code
		if (!BeanUtils.isNull(reservationSearchDTO.getOwnerAgentCode())) {
			values.add(reservationSearchDTO.getOwnerAgentCode());
			if (reservationSearchDTO.isSearchGSABookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
			}
			if (reservationSearchDTO.isSearchAllReportingAgentBookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
			} else if (reservationSearchDTO.isSearchReportingAgentBookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
				values.add(reservationSearchDTO.getOwnerAgentCode());
			}
		}

		// Origin User Id
		if (!BeanUtils.isNull(reservationSearchDTO.getOriginUserId())) {
			values.add(reservationSearchDTO.getOriginUserId());
		}

		return values;
	}

	/**
	 * Compose the SearchReservation Arguments Used by getReservations
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeGetReservationsArgs(ReservationSearchDTO reservationSearchDTO) {
		// Generating the dynamic values
		Collection values = new ArrayList();
		// First Name
		if (!BeanUtils.isNull(reservationSearchDTO.getFirstName())) {
			// Need two parameters
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
		}

		// Last Name
		if (!BeanUtils.isNull(reservationSearchDTO.getLastName())) {
			// Need two parameters
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
		}

		// PNR Number
		if (!BeanUtils.isNull(reservationSearchDTO.getPnr())) {
			values.add(reservationSearchDTO.getPnr());
		}

		// Originator PNR Number
		if (!BeanUtils.isNull(reservationSearchDTO.getOriginatorPnr())) {
			values.add(reservationSearchDTO.getOriginatorPnr());
		}

		// Passport Number
		if (!BeanUtils.isNull(reservationSearchDTO.getPassport())) {
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getPassport(), null));
		}

		// From Airport
		if (!BeanUtils.isNull(reservationSearchDTO.getFromAirport())) {
			values.add(reservationSearchDTO.getFromAirport() + BeanUtils.SEPARATOR_ALL);
		}

		// To Airport
		if (!BeanUtils.isNull(reservationSearchDTO.getToAirport())) {
			values.add(BeanUtils.ALL_SEPARATOR + reservationSearchDTO.getToAirport());
		}

		// Departure Date
		if (!BeanUtils.isNull(reservationSearchDTO.getDepartureDate())) {
			values.add(BeanUtils.getTimestamp(reservationSearchDTO.getDepartureDate()));
		}

		// Return Date
		if (!BeanUtils.isNull(reservationSearchDTO.getReturnDate())) {
			values.add(BeanUtils.getTimestamp(reservationSearchDTO.getReturnDate()));
		}

		// Early Departure Date
		if (!BeanUtils.isNull(reservationSearchDTO.getEarlyDepartureDate())) {
			values.add(BeanUtils.getTimestamp(reservationSearchDTO.getEarlyDepartureDate()));
		}

		// Telephone Number
		if (!BeanUtils.isNull(reservationSearchDTO.getTelephoneNo())) {
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getTelephoneNo(), null));
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getTelephoneNo(), null));
		}

		// Customer Id
		if (!BeanUtils.isNull(reservationSearchDTO.getCustomerId())) {
			values.add(reservationSearchDTO.getCustomerId());
		}

		// Flight Number
		if (!BeanUtils.isNull(reservationSearchDTO.getFlightNo())) {
			values.add(reservationSearchDTO.getFlightNo());
		}

		// Owner Channel Code
		if (!BeanUtils.isNull(reservationSearchDTO.getOwnerChannelId())) {
			values.add(reservationSearchDTO.getOwnerChannelId());
		}

		// Origin Agent Code
		if (!BeanUtils.isNull(reservationSearchDTO.getOriginAgentCode())) {
			values.add(reservationSearchDTO.getOriginAgentCode());

		}

		// Owner Agent Code
		if (!BeanUtils.isNull(reservationSearchDTO.getOwnerAgentCode())) {
			values.add(reservationSearchDTO.getOwnerAgentCode());
			if (reservationSearchDTO.isSearchGSABookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
			}
		}

		// Origin User Id
		if (!BeanUtils.isNull(reservationSearchDTO.getOriginUserId())) {
			values.add(reservationSearchDTO.getOriginUserId());
		}

		// Booking Class Type
		if (!BeanUtils.isNull(reservationSearchDTO.getBookingClassType())) {
			values.add(reservationSearchDTO.getBookingClassType());
		}

		return values;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeGetReservationsArgsForSimpleSearch(ReservationSearchDTO reservationSearchDTO) {
		// Generating the dynamic values
		Collection values = new ArrayList();

		if (!BeanUtils.isNull(reservationSearchDTO.getCustomerId())) { // Registered User Bookings
			values.add(reservationSearchDTO.getCustomerId());
		} else if (!BeanUtils.isNull(reservationSearchDTO.getPassport())) { // Taking passport unique for a pax
			values.add(reservationSearchDTO.getPassport());
		} else if (BeanUtils.isNull(reservationSearchDTO.getFirstName()) && BeanUtils.isNull(reservationSearchDTO.getLastName())
				&& BeanUtils.isNull(reservationSearchDTO.getPassport())
				&& !BeanUtils.isNull(reservationSearchDTO.getTelephoneNoForSQL())) { // Only Telephone No specified
			values.add(reservationSearchDTO.getTelephoneNoForSQL());
			values.add(reservationSearchDTO.getTelephoneNoForSQL());
		} else {
			if (!BeanUtils.isNull(reservationSearchDTO.getFirstName()) && !BeanUtils.isNull(reservationSearchDTO.getLastName())) {
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
				createReservationsArgsForNameSearch(values, reservationSearchDTO);
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
				createReservationsArgsForNameSearch(values, reservationSearchDTO);
			} else if (!BeanUtils.isNull(reservationSearchDTO.getFirstName())) {
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
				createReservationsArgsForNameSearch(values, reservationSearchDTO);
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
				createReservationsArgsForNameSearch(values, reservationSearchDTO);

			} else if (!BeanUtils.isNull(reservationSearchDTO.getLastName())) {
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
				createReservationsArgsForNameSearch(values, reservationSearchDTO);
				values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
				createReservationsArgsForNameSearch(values, reservationSearchDTO);
			}
		}

		return values;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection createReservationsArgsForNameSearch(Collection values, ReservationSearchDTO reservationSearchDTO) {
		if (!BeanUtils.isNull(reservationSearchDTO.getTelephoneNoForSQL())) {
			values.add(reservationSearchDTO.getTelephoneNoForSQL());
			values.add(reservationSearchDTO.getTelephoneNoForSQL());
		}
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerChannelId()).booleanValue()) {
			values.add(reservationSearchDTO.getOwnerChannelId());
		}
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOriginAgentCode()).booleanValue()) {
			values.add(reservationSearchDTO.getOriginAgentCode());
		}
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode()).booleanValue()) {
			values.add(reservationSearchDTO.getOwnerAgentCode());
			if (reservationSearchDTO.isSearchGSABookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
			}
			if (reservationSearchDTO.isSearchAllReportingAgentBookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
			} else if (reservationSearchDTO.isSearchReportingAgentBookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
				values.add(reservationSearchDTO.getOwnerAgentCode());
			}
		}
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOriginUserId()).booleanValue()) {
			values.add(reservationSearchDTO.getOriginUserId());
		}

		return values;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeInnerSql(ReservationSearchDTO reservationSearchDTO) {

		int rowsPerPage = AppSysParamsUtil.getRecordsPerReservationSearchPage();
		int pageNum = reservationSearchDTO.getPageNo();
		if (pageNum <= 0) {
			pageNum = 1;
		}
		int fromRowNum = (rowsPerPage * (pageNum - 1)) + 1;
		int toRowNum = rowsPerPage * pageNum;

		// Generating the dynamic SQL
		String sqlWhereClause = null;

		// Final innerSql
		String innerSql = "";
		Collection values = null;

		if (isSimpleSearch(reservationSearchDTO)) {
			if (log.isDebugEnabled()) {
				log.debug("Reservation search qualified for Reservation and/or Pax details search only["
						+ CommonsConstants.NEWLINE
						+ (!BeanUtils.isNull(reservationSearchDTO.getPassport()) ? ",Passport="
								+ BeanUtils.isNull(reservationSearchDTO.getPassport()) + CommonsConstants.NEWLINE : "")
						+ (!BeanUtils.isNull(reservationSearchDTO.getCustomerId()) ? ",CustomerId="
								+ BeanUtils.isNull(reservationSearchDTO.getCustomerId()) + CommonsConstants.NEWLINE : "")
						+ (!BeanUtils.isNull(reservationSearchDTO.getFirstName()) ? ",FirstName="
								+ BeanUtils.isNull(reservationSearchDTO.getFirstName()) + CommonsConstants.NEWLINE : "")
						+ (!BeanUtils.isNull(reservationSearchDTO.getLastName()) ? ",LastName="
								+ BeanUtils.isNull(reservationSearchDTO.getLastName()) + CommonsConstants.NEWLINE : "")
						+ (!BeanUtils.isNull(reservationSearchDTO.getTelephoneNo()) ? ",TelephoneNo="
								+ BeanUtils.isNull(reservationSearchDTO.getTelephoneNo()) + CommonsConstants.NEWLINE : "")
						+ (!BeanUtils.isNull(reservationSearchDTO.getTelephoneNoForSQL()) ? ",TelephoneNoForSQL="
								+ BeanUtils.isNull(reservationSearchDTO.getTelephoneNoForSQL()) + CommonsConstants.NEWLINE : ""));
			}

			sqlWhereClause = composeGetReservationsSqlForSimpleSearch(reservationSearchDTO);
			if (sqlWhereClause == null || "".equals(sqlWhereClause.trim())) {
				throw new CommonsDataAccessException("airreservations.arg.invalid.null");
			}
			innerSql = "SELECT PNR_NUMBER FROM (SELECT ROWNUM rnum, PNR_NUMBER from (" + sqlWhereClause
					+ " ORDER BY booking_timestamp DESC)) ";
			values = composeGetReservationsArgsForSimpleSearch(reservationSearchDTO);
		} else {
			sqlWhereClause = composeGetReservationsSql(reservationSearchDTO, false);
			// a separate where clause for external table query as details are different. (Eg flight number is in
			// T_EXT_FLIGHT_SEGMENT)
			String sqlWhereClauseForExtTable = composeGetReservationsSql(reservationSearchDTO, true);
			if (sqlWhereClause == null || "".equals(sqlWhereClause.trim())) {
				throw new CommonsDataAccessException("airreservations.arg.invalid.null");
			}

			// Old sql to get the PNR List
			// TODO- remove once the new sql is reviewed. (kept for debugging and comparison purposes)
			innerSql = "SELECT PNR_NUMBER FROM (SELECT ROWNUM rnum, PNR_NUMBER from (SELECT distinct r.PNR PNR_NUMBER , r.booking_timestamp "
					+ "FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon, T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg, T_FLIGHT f, T_PNR_PASSENGER p, T_PNR_PAX_ADDITIONAL_INFO pa, "
					+ "T_PNR_PAX_FARE_SEGMENT paxFareSeg, T_BOOKING_CLASS bookclass WHERE r.PNR=rCon.PNR AND r.PNR = rSeg.PNR "
					+ "AND rSeg.FLT_SEG_ID = fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID = f.FLIGHT_ID AND r.PNR = p.PNR AND p.PNR_PAX_ID=pa.PNR_PAX_ID "
					+ "AND paxFareSeg.PNR_SEG_ID = rSeg.PNR_SEG_ID AND paxFareSeg.BOOKING_CODE = bookclass.BOOKING_CODE(+) "
					+ sqlWhereClause + " ORDER BY r.booking_timestamp DESC)) ";

			/**
			 * There are information in t_fligh_segments t_pnr_segments and T_EXT_FLIGHT_SEGMENT and T_EXT_PNR_SEGMENT
			 * As we have to get the PNRs from both DBs we use this query.
			 * 
			 */
			innerSql = "SELECT DISTINCT PNR_NUMBER " + "FROM " + "  (SELECT ROWNUM rnum , " + "    PNR_NUMBER " + "  FROM ( "
					+ "    (SELECT DISTINCT r.PNR PNR_NUMBER , " + "      r.booking_timestamp booking_timestamp "
					+ "    FROM T_RESERVATION r , " + "      T_RESERVATION_CONTACT rCon , " + "      T_EXT_PNR_SEGMENT rSeg , "
					+ "      T_EXT_FLIGHT_SEGMENT fSeg , " + "      T_PNR_PASSENGER p , " + "      T_PNR_PAX_ADDITIONAL_INFO pa "
					+ "    WHERE r.PNR                        =rCon.PNR " + "    AND r.PNR                          = rSeg.PNR "
					+ "    AND rSeg.EXT_FLT_SEG_ID            = fSeg.EXT_FLT_SEG_ID "
					+ "    AND r.PNR                          = p.PNR "
					+ "    AND p.PNR_PAX_ID                   =pa.PNR_PAX_ID "
					+ sqlWhereClauseForExtTable

					+ "    UNION "

					+ "    SELECT DISTINCT r.PNR PNR_NUMBER , "
					+ "      r.booking_timestamp booking_timestamp "
					+ "    FROM T_RESERVATION r , "
					+ "      T_RESERVATION_CONTACT rCon , "
					+ "      T_PNR_SEGMENT rSeg , "
					+ "      T_FLIGHT_SEGMENT fSeg , "
					+ "      T_FLIGHT f , "
					+ "      T_PNR_PASSENGER p , "
					+ "      T_PNR_PAX_ADDITIONAL_INFO pa , "
					+ "      T_PNR_PAX_FARE_SEGMENT paxFareSeg, "
					+ "      T_BOOKING_CLASS bookclass "
					+ "    WHERE r.PNR                        =rCon.PNR "
					+ "    AND r.PNR                          = rSeg.PNR "
					+ "    AND rSeg.FLT_SEG_ID                = fSeg.FLT_SEG_ID "
					+ "    AND fSeg.FLIGHT_ID                 = f.FLIGHT_ID "
					+ "    AND r.PNR                          = p.PNR "
					+ "    AND p.PNR_PAX_ID                   =pa.PNR_PAX_ID "
					+ "    AND paxFareSeg.PNR_SEG_ID          = rSeg.PNR_SEG_ID "
					+ "    AND paxFareSeg.BOOKING_CODE        = bookclass.BOOKING_CODE(+) "
					+ sqlWhereClause
					+ "    ) "
					+ "  ORDER BY booking_timestamp DESC ) " + "  )";

			values = composeGetReservationsArgs(reservationSearchDTO);
			// we get a copy of the arg values for the ext table query
			Collection valuesForExtTables = composeGetReservationsArgs(reservationSearchDTO);
			values.addAll(valuesForExtTables);
		}

		if (BeanUtils.isNull(reservationSearchDTO.getCustomerId())) {
			innerSql += "WHERE rnum BETWEEN " + fromRowNum + " AND " + toRowNum;
		}

		log.debug("Inside composeInnerSql");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Object[] arrValues = values.toArray();

		if (log.isDebugEnabled()) {
			StringBuffer debugSb = new StringBuffer();
			int size = arrValues.length;
			debugSb.append("############################################");
			debugSb.append(" Inner SQL to excute            : " + innerSql);
			if (reservationSearchDTO.getCustomerId() == null) {
				debugSb.append("Current page requested 		: from " + fromRowNum + " to " + toRowNum);
			}
			debugSb.append(" Inner SQL number of parameters : " + size + CommonsConstants.NEWLINE);
			for (int i = 0; i < size; i++) {
				debugSb.append(arrValues[i]).append(((i != (size - 1)) ? "|" : ""));
			}
			debugSb.append("############################################");
			log.debug(debugSb.toString());
		}

		Collection pnrList = (Collection) jt.query(innerSql, arrValues, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String pnr;
				Collection pnrCol = null;
				if (rs != null) {
					while (rs.next()) {
						pnr = BeanUtils.nullHandler(rs.getString("PNR_NUMBER"));
						if (!pnr.equals("") && pnr != null) {
							if (pnrCol == null) {
								pnrCol = new ArrayList();
							}
							pnrCol.add(pnr);
						}
					}
				}
				return pnrCol;
			}
		});

		log.debug("Exit composeInnerSql");
		return pnrList;
	}

	private boolean isSimpleSearch(ReservationSearchDTO reservationSearchDTO) {
		if (BeanUtils.isNull(reservationSearchDTO.getFromAirport()) && BeanUtils.isNull(reservationSearchDTO.getToAirport())
				&& BeanUtils.isNull(reservationSearchDTO.getDepartureDate())
				&& BeanUtils.isNull(reservationSearchDTO.getReturnDate())
				&& BeanUtils.isNull(reservationSearchDTO.getEarlyDepartureDate())
				&& BeanUtils.isNull(reservationSearchDTO.getFlightNo())
				&& BeanUtils.isNull(reservationSearchDTO.getBookingClassType())) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isSearchByUniqueField(ReservationSearchDTO reservationSearchDTO) {
		if (reservationSearchDTO instanceof ReservationPaymentDTO) {
			return false;
		}
		if (!BeanUtils.isNull(reservationSearchDTO.getPnr()) || !BeanUtils.isNull(reservationSearchDTO.getOriginatorPnr())
				|| !BeanUtils.isNull(reservationSearchDTO.getETicket())
				|| !BeanUtils.isNull(reservationSearchDTO.getInvoiceNumber())
				|| !BeanUtils.isNull(reservationSearchDTO.getExternalETicket())
				|| !BeanUtils.isNull(reservationSearchDTO.getExternalRecordLocator())) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isSearchByCCDetails(ReservationSearchDTO reservationSearchDTO) {
		ReservationPaymentDTO reservationPaymentDTO;
		if (reservationSearchDTO instanceof ReservationPaymentDTO) {
			reservationPaymentDTO = (ReservationPaymentDTO) reservationSearchDTO;
			return ((!BeanUtils.isNull(reservationPaymentDTO.getCreditCardNo())) && (!BeanUtils.isNull(reservationPaymentDTO
					.getEDate()))) || (!BeanUtils.isNull(reservationPaymentDTO.getCcAuthorCode()));
		} else {
			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection<String> composeInnerSqlForCCInfo(ReservationPaymentDTO reservationPaymentDTO) {

		String no = reservationPaymentDTO.getCreditCardNo();
		String eDate = reservationPaymentDTO.getEDate();
		String ccAuthCode = reservationPaymentDTO.getCcAuthorCode();
		int pageNum = reservationPaymentDTO.getPageNo();

		// Validating the parameters
		if ((no == null || no.equals("") || eDate == null) && ccAuthCode == null) {
			log.error("Error occured for pnr:" + reservationPaymentDTO.getPnr() + " credit card no :" + no + " expiry date:"
					+ eDate + "auth code:" + ccAuthCode);
			throw new CommonsDataAccessException("airreservations.arg.invalidCNoAndEDate");
		}

		String filterByOwnerAgentCodeSql = "";
		if (!BeanUtils.isNullHandler(reservationPaymentDTO.getOwnerAgentCode())) {
			filterByOwnerAgentCodeSql += " AND ( r.OWNER_AGENT_CODE = ? ";
			if (reservationPaymentDTO.isSearchGSABookings()) {
				filterByOwnerAgentCodeSql += " OR r.OWNER_AGENT_CODE in ( SELECT agent_code from  t_agent where report_to_gsa ='Y' and gsa_code in (SELECT AG.AGENT_CODE FROM T_AGENT AG WHERE AG.AGENT_TYPE_CODE='GSA' AND AG.AGENT_CODE=?))";
			}
			if (reservationPaymentDTO.isSearchAllReportingAgentBookings()) {
				filterByOwnerAgentCodeSql += "OR r.OWNER_AGENT_CODE in ( SELECT AGENT_CODE FROM t_agent CONNECT BY PRIOR agent_code = gsa_code START WITH agent_code = ?)";
			} else if (reservationPaymentDTO.isSearchReportingAgentBookings()) {
				filterByOwnerAgentCodeSql += "OR r.OWNER_AGENT_CODE in ( SELECT AGENT_CODE FROM t_agent WHERE gsa_code = ? OR agent_code = ? )";
			}
			if (reservationPaymentDTO.getBookingCategories() != null && !reservationPaymentDTO.getBookingCategories().isEmpty()) {
				filterByOwnerAgentCodeSql += " OR r.BOOKING_CATEGORY_CODE IN (  "
						+ BeanUtils.constructINString(reservationPaymentDTO.getBookingCategories()) + " ) ";
			}
			filterByOwnerAgentCodeSql += " ) ";
		}

		String sqlFirstPart = "SELECT PNR FROM (SELECT ROWNUM rnum, PNR FROM ( ";

		String sql = " SELECT DISTINCT cc.PNR PNR, r.BOOKING_TIMESTAMP FROM T_RES_PCD cc, T_RESERVATION r WHERE cc.NO = ? AND cc.E_DATE = ? AND cc.PNR = r.PNR ";
		if (!BeanUtils.isNullHandler(reservationPaymentDTO.getOwnerAgentCode())) {
			sql += filterByOwnerAgentCodeSql;
		}
		sql += " ORDER BY r.BOOKING_TIMESTAMP DESC";

		if (ccAuthCode != null) {
			sql = " SELECT DISTINCT cc.PNR PNR, r.BOOKING_TIMESTAMP FROM T_RES_PCD cc, T_CCARD_PAYMENT_STATUS ac, T_RESERVATION r "
					+ " WHERE cc.TRANSACTION_REF_NO = ac.TRANSACTION_REF_NO ";

			if (no != null) {
				sql += " AND cc.NO = ? AND cc.E_DATE = ? AND ac.AID_CCCOMPNAY = ? AND cc.PNR = r.PNR ORDER BY r.BOOKING_TIMESTAMP DESC";
			} else {
				sql += " AND ac.AID_CCCOMPNAY = ? AND cc.PNR = r.PNR ORDER BY r.BOOKING_TIMESTAMP DESC";
			}
		}

		int rowsPerPage = AppSysParamsUtil.getRecordsPerReservationSearchPage();
		if (pageNum <= 0) {
			pageNum = 1;
		}
		int fromRowNum = (rowsPerPage * (pageNum - 1)) + 1;
		int toRowNum = rowsPerPage * pageNum;

		String sqlLastPart = ")) WHERE rnum BETWEEN " + fromRowNum + " AND " + toRowNum;
		String innerSql = sqlFirstPart + sql + sqlLastPart;

		log.debug("Inside composeInnerSqlForCCInfo");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Generating the dynamic SQL arguments
		Collection values = composeGetReservationsArgsForCCInfo(reservationPaymentDTO);

		log.debug("############################################");
		log.debug(" Inner SQL to excute            : " + innerSql);
		log.debug(" Inner SQL number of parameters : " + values.size());
		log.debug("############################################");

		Collection<String> pnrList = (Collection<String>) jt.query(innerSql, values.toArray(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String pnr;
				Collection pnrCol = null;
				if (rs != null) {
					while (rs.next()) {
						pnr = BeanUtils.nullHandler(rs.getString("PNR"));
						if (!pnr.equals("") && pnr != null) {
							if (pnrCol == null) {
								pnrCol = new ArrayList();
							}
							pnrCol.add(pnr);
						}
					}
				}
				return pnrCol;
			}
		});

		log.debug("Exit composeInnerSqlForCCInfo");

		return pnrList;
	}

	/**
	 * Compose the SearchReservation Arguments for cc info search Used by getReservations
	 * 
	 * @param reservationPaymentDTO
	 * @return
	 */
	private Collection<String> composeGetReservationsArgsForCCInfo(ReservationPaymentDTO reservationPaymentDTO) {
		String no = reservationPaymentDTO.getCreditCardNo();
		String eDate = reservationPaymentDTO.getEDate();
		String ccAuthCode = reservationPaymentDTO.getCcAuthorCode();

		Collection<String> values = new ArrayList<String>();
		values.add(no);
		values.add(eDate);

		if (!BeanUtils.isNullHandler(reservationPaymentDTO.getOwnerAgentCode())) {
			values.add(reservationPaymentDTO.getOwnerAgentCode());
			if (reservationPaymentDTO.isSearchGSABookings()) {
				values.add(reservationPaymentDTO.getOwnerAgentCode());
			}
			if (reservationPaymentDTO.isSearchAllReportingAgentBookings()) {
				values.add(reservationPaymentDTO.getOwnerAgentCode());
			} else if (reservationPaymentDTO.isSearchReportingAgentBookings()) {
				values.add(reservationPaymentDTO.getOwnerAgentCode());
				values.add(reservationPaymentDTO.getOwnerAgentCode());
			}
		}

		if (no != null && ccAuthCode != null) {
			values = new ArrayList<String>();
			values.add(no);
			values.add(eDate);
			values.add(ccAuthCode);
		}

		if (no == null && ccAuthCode != null) {
			values = new ArrayList<String>();
			values.add(ccAuthCode);
		}
		return values;
	}

	/**
	 * Search Reservations based on the dynamic user parameters NOTE: BETTER TO BE USED FOR READ ONLY PURPOSES (FAST
	 * LANE READER)
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<ReservationDTO> getReservations(ReservationSearchDTO reservationSearchDTO) {
		log.debug("Inside getReservations");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection pnrList = null;
		String sqlUniequeFiledsCondition = "";
		String sqlForSearchByEticket = "";
		String sqlForSearchByExtraRecordLocator = "";
		String sqlForSearchOtherAirlineSegments = "";
		String sqlForOriginOwnerAgentValidations = "";

		if (isSearchByCCDetails(reservationSearchDTO)) {
			ReservationPaymentDTO reservationPaymentDTO = (ReservationPaymentDTO) reservationSearchDTO;
			pnrList = composeInnerSqlForCCInfo(reservationPaymentDTO);

			if (pnrList == null || pnrList.size() == 0) {
				log.error("Reservations not found for :" + reservationPaymentDTO.getPnr());
				throw new CommonsDataAccessException("um.airreservations.arg.pnrs.null");
			}

		} else {
			if (isSearchByUniqueField(reservationSearchDTO)) {
				if (!BeanUtils.isNull(reservationSearchDTO.getPnr())) {
					log.debug("Reservation Search by PNR [PNR=" + reservationSearchDTO.getPnr() + "]");

					sqlForSearchOtherAirlineSegments = " OALPNRSEG.PNR  = ? ";
					sqlUniequeFiledsCondition = " P.PNR = ? ";
					pnrList = new ArrayList();
					pnrList.add(reservationSearchDTO.getPnr());
					pnrList.add(reservationSearchDTO.getPnr());
					if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
						pnrList.add(reservationSearchDTO.getPnr());
					}
				} else if (!BeanUtils.isNull(reservationSearchDTO.getOriginatorPnr())) {
					log.debug("Reservation Search by Originator PNR [OriginatorPNR=" + reservationSearchDTO.getOriginatorPnr()
							+ "]");

					sqlUniequeFiledsCondition = " P.ORIGINATOR_PNR = ? ";
					pnrList = new ArrayList();
					pnrList.add(reservationSearchDTO.getOriginatorPnr());
					pnrList.add(reservationSearchDTO.getOriginatorPnr());
				} else if (!BeanUtils.isNull(reservationSearchDTO.getETicket())) {
					log.debug("Reservation Search by E-ticket Number [E-ticket Number=" + reservationSearchDTO.getETicket() + "]");

					sqlForSearchByEticket = ", T_PAX_E_TICKET PET ";
					sqlUniequeFiledsCondition = " p.pnr_pax_id = pet.pnr_pax_id AND pet.e_ticket_number=?  ";
					sqlForSearchOtherAirlineSegments = " OALPNRSEG.PNR = P.PNR AND p.pnr_pax_id = pet.pnr_pax_id AND pet.e_ticket_number=?   ";
					pnrList = new ArrayList();
					pnrList.add(reservationSearchDTO.getETicket());
					pnrList.add(reservationSearchDTO.getETicket());
					if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
						pnrList.add(reservationSearchDTO.getETicket());
					}
				}  else if (!BeanUtils.isNull(reservationSearchDTO.getInvoiceNumber())) {
					log.debug("Reservation Search by Invoice number [Invoice number=" + reservationSearchDTO.getInvoiceNumber() + "]");
					sqlForSearchOtherAirlineSegments = " OALPNRSEG.PNR  in  (select distinct pnr from t_pnr_tax_invoice where ti_id  in (select ti_id from t_tax_invoice tti where tti.INVOICE_NUMBER = ?) ) ";
					sqlUniequeFiledsCondition = " P.PNR in (select distinct pnr from t_pnr_tax_invoice where ti_id  in (select ti_id from t_tax_invoice tti where tti.INVOICE_NUMBER = ?) ) ";
					pnrList = new ArrayList();
					pnrList.add(reservationSearchDTO.getInvoiceNumber());
					pnrList.add(reservationSearchDTO.getInvoiceNumber());
					if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
						pnrList.add(reservationSearchDTO.getInvoiceNumber());
					}
				}  else if (!BeanUtils.isNull(reservationSearchDTO.getExternalETicket())) {
					log.debug("Reservation Search by External E-ticket Number [External E-ticket Number="
							+ reservationSearchDTO.getExternalETicket() + "]");

					sqlForSearchByEticket = " , t_pax_e_ticket pet , T_PNR_PAX_FARE_SEG_E_TICKET PPFSE ";
					sqlUniequeFiledsCondition = " p.pnr_pax_id = pet.pnr_pax_id AND pet.pax_e_ticket_id= PPFSE.pax_e_ticket_id  AND PPFSE.ext_e_ticket_number=?  ";
					sqlForSearchOtherAirlineSegments = " OALPNRSEG.PNR = P.PNR AND p.pnr_pax_id = pet.pnr_pax_id AND pet.pax_e_ticket_id= PPFSE.pax_e_ticket_id  AND PPFSE.ext_e_ticket_number=?  ";
					pnrList = new ArrayList();
					pnrList.add(reservationSearchDTO.getExternalETicket());
					pnrList.add(reservationSearchDTO.getExternalETicket());
					if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
						pnrList.add(reservationSearchDTO.getExternalETicket());
					}
				} else if (!BeanUtils.isNull(reservationSearchDTO.getExternalRecordLocator())) {
					log.debug("Reservation Search by External Record Locator [External Record Locator="
							+ reservationSearchDTO.getExternalRecordLocator() + "]");

					sqlForSearchByExtraRecordLocator = " , T_RESERVATION R ";
					sqlUniequeFiledsCondition = " R.EXTERNAL_REC_LOCATOR = ? AND P.PNR = R.PNR ";
					sqlForSearchOtherAirlineSegments = " R.EXTERNAL_REC_LOCATOR = ?  AND OALPNRSEG.PNR = R.PNR ";
					pnrList = new ArrayList();
					pnrList.add(reservationSearchDTO.getExternalRecordLocator());
					pnrList.add(reservationSearchDTO.getExternalRecordLocator());
					if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
						pnrList.add(reservationSearchDTO.getExternalRecordLocator());
					}
				}
				sqlForOriginOwnerAgentValidations = composeWhereClauseForSimpleSearch(reservationSearchDTO);
				pnrList.addAll(composeArgsForSimpleSearch(reservationSearchDTO));
			} else {
				Collection pnrs = composeInnerSql(reservationSearchDTO);
				pnrList = new ArrayList();
				if (pnrs != null) {
					for (int i = 0; i < 2; i++) {
						for (Iterator iterator = pnrs.iterator(); iterator.hasNext();) {
							String pnr = (String) iterator.next();
							pnrList.add(pnr);
						}
					}
				} else {
					pnrs = new ArrayList();
				}
				
				sqlUniequeFiledsCondition = " P.PNR IN (" + BeanUtils.constructSQLPlaceHolderString(pnrs.size()) + " ) ";
				sqlForSearchOtherAirlineSegments = " OALPNRSEG.PNR IN ("
						+ BeanUtils.constructINString(pnrs) + " ) ";
				if (pnrList == null || pnrList.size() == 0) {
				//	if (!BeanUtils.isNull(reservationSearchDTO.getCustomerId())) { // Temp fix for reg user no bookings
						return new ArrayList();
				//	}
				//	log.error("Reservations not found for :" + reservationSearchDTO.getPnr());
				//	throw new CommonsDataAccessException("um.airreservations.arg.pnrs.null");
				}
			}
		}

		String strDisplayOtherAirlineSegments = "";
		if (AppSysParamsUtil.isDisplayOtherAirlineSegments()) {
			strDisplayOtherAirlineSegments = " UNION ALL "

					+ " SELECT OALPNRSEG.PNR               AS PNR , OALPNRSEG.PNR_OTHER_AIRLINE_SEG_ID AS PNR_SEG_ID, "
					+ " CASE WHEN OALPNRSEG.STATUS = 'HK' THEN 'CNF' WHEN OALPNRSEG.STATUS = 'CK' THEN 'CNF' "
					+ " WHEN OALPNRSEG.STATUS = 'HX' THEN 'CNX' WHEN OALPNRSEG.STATUS = 'CX' THEN 'CNX' "
					+ " ELSE OALPNRSEG.STATUS END          			AS STATUS,"
					+ " NULL                               AS SUB_STATUS,"
					+ " OALPNRSEG.FLT_SEG_ID               			AS FLT_SEG_ID,"
					+ " NULL                               			AS RETURN_FLAG,"
					+ " NULL                               			AS ALERT_FLAG,"
					+ " 'NORMAL'	                AS BC_TYPE,"
					+ " NULL   				   			            AS LOGICAL_CABIN_CLASS_CODE,"// TODO : Take this from relavant
					// table
					+ " OALPNRSEG.FLIGHT_NUMBER            			AS FLIGHT_NUMBER ,"
					+ " NULL                     		   			AS FLIGHT_STATUS ,"
					+ " NULL                               			AS CS_OC_CARRIER_CODE ,"
					+ " NULL                               			AS CS_OC_FLIGHT_NUMBER ,"
					+ " CONCAT(CONCAT(ORIGIN , '/'),DESTINATION) 	AS SEGMENT_CODE ,"
					+ " OALPNRSEG.EST_TIME_DEPARTURE_LOCAL 			AS EST_TIME_DEPARTURE_LOCAL,"
					+ " OALPNRSEG.EST_TIME_ARRIVAL_LOCAL   			AS EST_TIME_ARRIVAL_LOCAL ,"
					+ " OALPNRSEG.EST_TIME_DEPARTURE_LOCAL 			AS EST_TIME_DEPARTURE_ZULU," // TODO : SET correct zulu
					// time
					+ " OALPNRSEG.EST_TIME_ARRIVAL_LOCAL  			AS EST_TIME_ARRIVAL_ZULU,"
					+ " NULL                              			AS MODEL_NUMBER,"
					+ " NULL                               			AS FLT_DESCRIPTION,"
					+ " NULL                               			AS CS_FLIGHT_NUMBER,"
					+ " OALPNRSEG.BOOKING_CODE            			AS CS_BOOKING_CLASS,"
					+ " NULL 										AS RELEASE_TIMESTAMP,"
					+ " NULL                               			AS FLT_REMARKS"
					+ " FROM T_PNR_OTHER_AIRLINE_SEGMENT OALPNRSEG "
					+ (!sqlForSearchByEticket.isEmpty() ? " ,T_PNR_PASSENGER P " + sqlForSearchByEticket : (!BeanUtils
							.isNull(reservationSearchDTO.getExternalRecordLocator()) ? sqlForSearchByExtraRecordLocator : ""))
					+ (!sqlUniequeFiledsCondition.isEmpty() ? " WHERE " : " ") + sqlForSearchOtherAirlineSegments;
		}// T_PNR_PASSENGER P ,

		final boolean isIncludePaxInfo = reservationSearchDTO.isIncludePaxInfo();

		// Final SQL - Obsolete - left for review/comparison/debug purposes
		// TODO - remove after the new query is reviewed and verified.
		String sql = " SELECT DISTINCT r.PNR PNR_NUMBER, r.ORIGINATOR_PNR ORIGINATOR_PNR, rCon.C_FIRST_NAME CF_NAME, rCon.C_LAST_NAME CL_NAME, "
				+ " rCon.C_EMAIL C_EMAIL, rCon.C_PHONE_NO C_PHONE, rCon.C_MOBILE_NO C_MOBILE, rCon.SEND_PROMO_EMAIL SEND_PROMO_EMAIL, "
				+ " r.TOTAL_PAX_COUNT PAX_COUNT, r.TOTAL_PAX_CHILD_COUNT CHILD_COUNT, r.TOTAL_PAX_INFANT_COUNT INF_COUNT, "
				+ " r.BOOKING_TIMESTAMP BOOK_DATE, r.STATUS R_STATUS, "
				+ " r.ORIGIN_CHANNEL_CODE OR_CHL_CODE, r.OWNER_CHANNEL_CODE OW_CHL_CODE, r.ORIGIN_AGENT_CODE OR_AGT_CODE, "
				+ " r.OWNER_AGENT_CODE OW_AGT_CODE, r.ORIGIN_USER_ID OR_USR_ID, "
				+ (isIncludePaxInfo ? " p.PNR_PAX_ID PNR_PAX_ID, p.TITLE TITLE, "
						+ " p.FIRST_NAME FIRST_NAME, p.LAST_NAME LAST_NAME, p.PAX_TYPE_CODE PAX_TYPE_CODE, " : "")
				+ " rSeg.PNR_SEG_ID PNR_SEG_ID, rSeg.STATUS S_STATUS, rSeg.RETURN_FLAG RETURN_FLAG, rSeg.ALERT_FLAG ALERT_FLAG, "
				+ " fSeg.SEGMENT_CODE SEG_CODE, fSeg.EST_TIME_DEPARTURE_LOCAL DEP_DATE, "
				+ " fSeg.EST_TIME_ARRIVAL_LOCAL ARR_DATE, "
				+ " fSeg.EST_TIME_DEPARTURE_ZULU DEP_DATE_ZULU, fSeg.EST_TIME_ARRIVAL_ZULU ARR_DATE_ZULU, "
				+ " P.RELEASE_TIMESTAMP RELEASE_TIMESTAMP, f.FLIGHT_NUMBER F_NUMBER, bookclass.BC_TYPE "
				+ " FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon, T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg, T_FLIGHT f, T_PNR_PASSENGER p, "
				+ " T_PNR_PAX_ADDITIONAL_INFO pa, T_PNR_PAX_FARE_SEGMENT paxFareSeg, T_BOOKING_CLASS bookclass "
				+ " WHERE r.PNR=rCon.PNR AND r.PNR = rSeg.PNR AND rSeg.FLT_SEG_ID = fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID = f.FLIGHT_ID "
				+ " AND r.PNR = p.PNR AND p.PNR_PAX_ID=pa.PNR_PAX_ID AND paxFareSeg.PNR_SEG_ID = rSeg.PNR_SEG_ID AND paxFareSeg.BOOKING_CODE = bookclass.BOOKING_CODE(+) "
				+ (!sqlUniequeFiledsCondition.isEmpty() ? sqlUniequeFiledsCondition : (" AND r.pnr IN ( "
						+ BeanUtils.constructSQLPlaceHolderString(pnrList.size()) + " ) ORDER BY r.booking_timestamp DESC"));

		// New sql with unified view including external flight-segment and external pnr-segment details.
		sql = "SELECT DISTINCT " + "  r.PNR PNR_NUMBER , " + "  r.ORIGINATOR_PNR ORIGINATOR_PNR , "
				+ "  rCon.C_FIRST_NAME CF_NAME , " + "  rCon.C_LAST_NAME CL_NAME , "
				+ "  rCon.C_EMAIL C_EMAIL, rCon.C_PHONE_NO C_PHONE, rCon.C_MOBILE_NO C_MOBILE, rCon.SEND_PROMO_EMAIL SEND_PROMO_EMAIL, "
				+ "  r.TOTAL_PAX_COUNT PAX_COUNT , " + "  r.TOTAL_PAX_CHILD_COUNT CHILD_COUNT , "
				+ "  r.TOTAL_PAX_INFANT_COUNT INF_COUNT , " + "  r.BOOKING_TIMESTAMP BOOK_DATE , " + "  r.STATUS R_STATUS , "

				+ "  r.VOID_RESERVATION R_VOID_RESERVATION," + "  r.ORIGIN_CHANNEL_CODE OR_CHL_CODE , "
				+ "  r.OWNER_CHANNEL_CODE OW_CHL_CODE , " + "  r.ORIGIN_AGENT_CODE OR_AGT_CODE , "
				+ "  r.OWNER_AGENT_CODE OW_AGT_CODE , " + "  r.ORIGIN_USER_ID OR_USR_ID , "
				+ (isIncludePaxInfo ? " p.PNR_PAX_ID PNR_PAX_ID, p.TITLE TITLE, "
						+ " p.FIRST_NAME FIRST_NAME, p.LAST_NAME LAST_NAME, p.PAX_TYPE_CODE PAX_TYPE_CODE, " : "")
				+ "  rSeg.PNR_SEG_ID PNR_SEG_ID , "
				+ "  rSeg.STATUS S_STATUS , "
				+ "  rSeg.SUB_STATUS S_SUB_STATUS , "
				+ "  rSeg.RETURN_FLAG RETURN_FLAG , "
				+ "  rSeg.ALERT_FLAG ALERT_FLAG , "
				+ "  rSeg.SEGMENT_CODE SEG_CODE , "
				+ "  rSeg.EST_TIME_DEPARTURE_LOCAL DEP_DATE , "
				+ "  rSeg.EST_TIME_ARRIVAL_LOCAL ARR_DATE , "
				+ "  rSeg.EST_TIME_DEPARTURE_ZULU DEP_DATE_ZULU, "
				+ "  rSeg.EST_TIME_ARRIVAL_ZULU ARR_DATE_ZULU , "
				+ "  rSeg.RELEASE_TIMESTAMP, "
				+ "  rSeg.FLIGHT_NUMBER F_NUMBER , "
				+ "  rSeg.FLIGHT_STATUS F_STATUS , "
				+ "  rSeg.BC_TYPE, "
				+ "  rSeg.LOGICAL_CABIN_CLASS_CODE, "
				+ "  rSeg.MODEL_NUMBER, "
				+ "  rSeg.FLT_DESCRIPTION, "
				+ "  rSeg.CS_FLIGHT_NUMBER, "
				+ "  rSeg.CS_BOOKING_CLASS, "
				+ "  rSeg.CS_OC_CARRIER_CODE, "
				+ "  rSeg.CS_OC_FLIGHT_NUMBER, "
				+ "  rSeg.FLT_REMARKS "
				+ "FROM "
				+ "  T_RESERVATION r , "
				+ "  T_RESERVATION_CONTACT rCon , "
				+

				// get the combined view of segments and external segments [ (the t_pnr_segment/t_flight_segment and
				// t_ext_pnr_segment/t_ext_flight_segment) ]
				" (SELECT PNRSEG.PNR               AS PNR , "
				+ "  PNRSEG.PNR_SEG_ID               AS PNR_SEG_ID, "
				+ "  PNRSEG.STATUS                   AS STATUS, "
				+ "  PNRSEG.SUB_STATUS               AS SUB_STATUS, "
				+ "  PNRSEG.FLT_SEG_ID               AS FLT_SEG_ID, "
				+ "  PNRSEG.RETURN_FLAG              AS RETURN_FLAG, "
				+ "  PNRSEG.ALERT_FLAG               AS ALERT_FLAG, "
				+ "  BOOKCLASS.BC_TYPE               AS BC_TYPE, "
				+ "  BOOKCLASS.LOGICAL_CABIN_CLASS_CODE               AS LOGICAL_CABIN_CLASS_CODE, "
				+ "  F.FLIGHT_NUMBER                 AS FLIGHT_NUMBER , "
				+ "  F.STATUS                 		 AS FLIGHT_STATUS , "
				+ "  F.CS_OC_CARRIER_CODE      		 AS CS_OC_CARRIER_CODE , "
				+ "  F.CS_OC_FLIGHT_NUMBER      	 AS CS_OC_FLIGHT_NUMBER , "
				+ "  FLTSEG.SEGMENT_CODE             AS SEGMENT_CODE , "
				+ "  FLTSEG.EST_TIME_DEPARTURE_LOCAL AS EST_TIME_DEPARTURE_LOCAL, "
				+ "  FLTSEG.EST_TIME_ARRIVAL_LOCAL   AS EST_TIME_ARRIVAL_LOCAL , "
				+ "  FLTSEG.EST_TIME_DEPARTURE_ZULU  AS EST_TIME_DEPARTURE_ZULU, "
				+ "  FLTSEG.EST_TIME_ARRIVAL_ZULU    AS EST_TIME_ARRIVAL_ZULU, "
				+ "  FLTMOD.MODEL_NUMBER AS MODEL_NUMBER, "
				+ "  FLTMOD.ITIN_DESCRIPTION AS FLT_DESCRIPTION, "
				+ "  PNRSEG.CS_FLIGHT_NUMBER 		 AS CS_FLIGHT_NUMBER, "
				+ "  PNRSEG.CS_BOOKING_CLASS         AS CS_BOOKING_CLASS, "
				+ "  P.RELEASE_TIMESTAMP RELEASE_TIMESTAMP, "
				+ "  F.REMARKS AS FLT_REMARKS "
				+ " FROM T_PNR_SEGMENT PNRSEG , "
				+ "  T_FLIGHT_SEGMENT FLTSEG, "
				+ "  t_flight f, "
				+ "  T_AIRCRAFT_MODEL FLTMOD, "
				+ "  T_PNR_PAX_FARE_SEGMENT paxFareSeg, "
				+ "  t_booking_class bookclass, "
				+ "  T_PNR_PASSENGER P "
				+ sqlForSearchByEticket
				+ sqlForSearchByExtraRecordLocator
				+ " WHERE PNRSEG.PNR_SEG_ID     = paxFareSeg.PNR_SEG_ID "
				+ " AND PAXFARESEG.BOOKING_CODE = BOOKCLASS.BOOKING_CODE(+) "
				+ " AND FLTSEG.FLT_SEG_ID       = PNRSEG.FLT_SEG_ID "
				+ " AND FLTSEG.FLIGHT_ID        = F.FLIGHT_ID "
				+ " AND F.model_number = FLTMOD.model_number "
				+ " AND PNRSEG.PNR = P.PNR "
				+ (!sqlUniequeFiledsCondition.isEmpty() ? "AND " + sqlUniequeFiledsCondition : sqlUniequeFiledsCondition)
				+

				" UNION ALL "
				+
				// t_ext_pnr_segment/t_ext_flight_segment view
				"  SELECT EXTPNRSEG.PNR               AS PNR , "
				+ "  EXTPNRSEG.EXT_PNR_SEG_ID           AS PNR_SEG_ID, "
				+ "  EXTPNRSEG.STATUS                   AS STATUS, "
				+ "  EXTPNRSEG.SUB_STATUS               AS SUB_STATUS, "
				+ "  EXTPNRSEG.EXT_FLT_SEG_ID           AS FLT_SEG_ID, "
				+ "  EXTPNRSEG.RETURN_FLAG              AS RETURN_FLAG, "
				+ "  0                                  AS ALERT_FLAG, "
				+ "  'NORMAL'                           AS BC_TYPE, "
				+ "  EXTPNRSEG.LOGICAL_CABIN_CLASS_CODE AS LOGICAL_CABIN_CLASS_CODE, "
				+ "  EXTFLTSEG.FLIGHT_NUMBER            AS FLIGHT_NUMBER , "
				+ "  null                   AS FLIGHT_STATUS , "
				+ "  null     		        AS CS_OC_CARRIER_CODE , "
				+ "  null      	  			AS CS_OC_FLIGHT_NUMBER , "
				+ "  EXTFLTSEG.SEGMENT_CODE             AS SEGMENT_CODE , "
				+ "  EXTFLTSEG.EST_TIME_DEPARTURE_LOCAL AS EST_TIME_DEPARTURE_LOCAL, "
				+ "  EXTFLTSEG.EST_TIME_ARRIVAL_LOCAL   AS EST_TIME_ARRIVAL_LOCAL , "
				+ "  EXTFLTSEG.EST_TIME_DEPARTURE_ZULU  AS EST_TIME_DEPARTURE_ZULU, "
				+ "  EXTFLTSEG.EST_TIME_ARRIVAL_ZULU    AS EST_TIME_ARRIVAL_ZULU, "
				+ "  null AS MODEL_NUMBER, "
				+ "  null AS FLT_DESCRIPTION, "
				+ "  null AS CS_FLIGHT_NUMBER, "
				+ "  null AS CS_BOOKING_CLASS, "
				+ "  P.RELEASE_TIMESTAMP RELEASE_TIMESTAMP, "
				+ "  null AS FLT_REMARKS "
				+ " FROM T_EXT_PNR_SEGMENT EXTPNRSEG , "
				+ "  T_EXT_FLIGHT_SEGMENT EXTFLTSEG ,"
				+ "  T_PNR_PASSENGER P "
				+ sqlForSearchByEticket
				+ sqlForSearchByExtraRecordLocator
				+ " WHERE EXTFLTSEG.EXT_FLT_SEG_ID = EXTPNRSEG.EXT_FLT_SEG_ID "
				+ " AND EXTPNRSEG.PNR = P.PNR "
				+ (!sqlUniequeFiledsCondition.isEmpty() ? "AND " + sqlUniequeFiledsCondition : sqlUniequeFiledsCondition)
				+ strDisplayOtherAirlineSegments
				+ " ) rSeg"
				// <- combined view of (the t_pnr_segment/t_flight_segment and t_ext_pnr_segment/t_ext_flight_segment)

				// append the eticket table and external eticket table
				// + ((!BeanUtils.isNull(reservationSearchDTO.getExternalETicket())) ?
				// " , t_pax_e_ticket pet , T_PNR_PAX_FARE_SEG_E_TICKET PPFSE " : " ")
				+ " WHERE "
				+ "  r.PNR             =rCon.PNR "
				+ "  AND r.PNR           = rSeg.PNR "
				+ sqlForOriginOwnerAgentValidations + "  ORDER BY r.booking_timestamp DESC, rSeg.BC_TYPE ";

		Object[] arrValues = pnrList.toArray();

		if (log.isDebugEnabled()) {
			StringBuffer debugSb = new StringBuffer();
			int size = arrValues.length;

			debugSb.append("############################################" + CommonsConstants.NEWLINE);
			debugSb.append(" GetReservations SQL to excute      : " + sql + CommonsConstants.NEWLINE);
			debugSb.append(" Inner SQL number of PNRs 			: " + size + CommonsConstants.NEWLINE);
			for (int i = 0; i < size; i++) {
				debugSb.append(arrValues[i]).append(((i != (size - 1)) ? "|" : ""));
			}
			debugSb.append("############################################");
			log.debug(debugSb.toString());
		}

		Collection collection = (Collection) jt.query(sql, arrValues, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ReservationDTO reservationDTO;
				ReservationSegmentDTO reservationSegmentDTO;
				Collection bookings = new ArrayList();
				String pnr;
				Integer pnrSegId;
				Integer pnrPaxId;
				String paxTypeCode;
				Set pnrSegIdsSet = new HashSet();
				Set pnrPaxIdsSet = new HashSet();
				Map pnrMap = new LinkedHashMap();

				if (rs != null) {
					while (rs.next()) {
						// Pick pnr number
						pnr = BeanUtils.nullHandler(rs.getString("PNR_NUMBER"));
						// Pick PNR segID
						pnrSegId = BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID"));
						// If pnr number exist
						if (pnrMap.keySet().contains(pnr)) {
							reservationDTO = (ReservationDTO) pnrMap.get(pnr);

							if (!pnrSegIdsSet.contains(pnrSegId)) {
								// Pick the segment information
								reservationSegmentDTO = getPnrSegmentInformationForReservationSearch(rs);

								// Adding segment information to the reservation
								reservationDTO.addSegment(reservationSegmentDTO);
								reservationDTO.setZuluReleaseTimeStamp(rs.getTimestamp("RELEASE_TIMESTAMP"));

								pnrSegIdsSet.add(pnrSegId);
							}

							// Adding PNR adult passenger details (upto 5)
							if (isIncludePaxInfo) {
								pnrPaxId = BeanUtils.parseInteger(rs.getInt("PNR_PAX_ID"));
								if (!pnrPaxIdsSet.contains(pnrPaxId)) {
									if (((ReservationPaxDTO) reservationDTO).getPassengers() == null
											|| ((ReservationPaxDTO) reservationDTO).getPassengers().size() < 6) {
										paxTypeCode = BeanUtils.nullHandler(rs.getString("PAX_TYPE_CODE"));
										if (ReservationInternalConstants.PassengerType.ADULT.equals(paxTypeCode)
												|| ReservationInternalConstants.PassengerType.CHILD.equals(paxTypeCode)) {
											((ReservationPaxDTO) reservationDTO).addPassenger(getPnrPassengerDetails(rs,
													paxTypeCode));
										}
									}
									pnrPaxIdsSet.add(pnrPaxId);
								}
							}
						} else {
							// Pick the PNR information
							reservationDTO = getPnrInformation(pnr, rs, true);

							// Pick PNR Segment information
							reservationSegmentDTO = getPnrSegmentInformationForReservationSearch(rs);

							// Adding PNR segment information to the reservation
							reservationDTO.addSegment(reservationSegmentDTO);
							reservationDTO.setZuluReleaseTimeStamp(rs.getTimestamp("RELEASE_TIMESTAMP"));

							pnrSegIdsSet.add(pnrSegId);

							// Adding PNR adult passenger details (upto 5)
							if (isIncludePaxInfo) {
								pnrPaxId = BeanUtils.parseInteger(rs.getInt("PNR_PAX_ID"));
								if (!pnrPaxIdsSet.contains(pnrPaxId)) {
									if (((ReservationPaxDTO) reservationDTO).getPassengers() == null
											|| ((ReservationPaxDTO) reservationDTO).getPassengers().size() < 6) {
										paxTypeCode = BeanUtils.nullHandler(rs.getString("PAX_TYPE_CODE"));
										if (ReservationInternalConstants.PassengerType.ADULT.equals(paxTypeCode)
												|| ReservationInternalConstants.PassengerType.CHILD.equals(paxTypeCode)) {
											((ReservationPaxDTO) reservationDTO).addPassenger(getPnrPassengerDetails(rs,
													paxTypeCode));
										}
									}
									pnrPaxIdsSet.add(pnrPaxId);
								}
							}

							// Stores the PNR number
							pnrMap.put(pnr, reservationDTO);
						}
					}
				}

				// Returning the values
				bookings = pnrMap.values();

				return new ArrayList(bookings);
			}

		});
		
		if (isSearchByUniqueField(reservationSearchDTO)) {
			if (!BeanUtils.isNull(reservationSearchDTO.getPnr())) {
				int Listsize = 0;
				if (collection != null && !collection.isEmpty()) {
					Listsize = collection.size();
				}
				String dbTime = null;
				if (Listsize == 0) {
					dbTime = (String) jt.query("select sysdate from dual", new ResultSetExtractor() {
						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							String stringDate = "";

							if (rs != null) {
								if (rs.next()) {
									stringDate = BeanUtils.nullHandler(rs.getString("sysdate"));
								}
							}

							return stringDate;
						}

					});
				}
				log.debug("###WSLOGS### LOAD RESERVATION LIST COMPLETED** STATUS*** PNR : " + reservationSearchDTO.getPnr()
						+ " RESULT SET SIZE : " + Listsize + " DB TIME : " + (dbTime != null ? dbTime : ""));

			}
		}

		log.debug("Exit getReservations");
		return collection;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getPnrList(int customerId) {
		log.debug("Inside get PNR List");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT r.PNR FROM T_RESERVATION r, T_RESERVATION_CONTACT rc" + " WHERE r.PNR=rc.PNR AND rc.CUSTOMER_ID=? ";

		Object[] param = { customerId };

		List<String> pnrList = (List<String>) jt.query(sql, param, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> strList = new ArrayList<String>();
				if (rs != null) {

					while (rs.next()) {
						strList.add(BeanUtils.nullHandler(rs.getString("PNR")));
					}
				}
				return strList;
			}
		});

		return pnrList;

	}

	/**
	 * Get passenger information. Used by getReservations
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ReservationPaxDetailsDTO getPnrPassengerDetails(ResultSet rs, String paxTypeCode) throws SQLException {
		ReservationPaxDetailsDTO paxDTO = new ReservationPaxDetailsDTO();

		paxDTO.setPaxType(paxTypeCode);
		paxDTO.setPnrPaxId(BeanUtils.parseInteger(rs.getInt("PNR_PAX_ID")));
		paxDTO.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
		paxDTO.setFirstName(BeanUtils.nullHandler(rs.getString("FIRST_NAME")));
		paxDTO.setLastName(BeanUtils.nullHandler(rs.getString("LAST_NAME")));

		return paxDTO;
	}

	/**
	 * Get PNR Information Used by getReservations, getPassengerCredit
	 * 
	 * @param pnr
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ReservationPaxDTO getPnrInformation(String pnr, ResultSet rs, boolean additionalDetails) throws SQLException {
		ReservationPaxDTO reservationPaxDTO = new ReservationPaxDTO();
		reservationPaxDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		ReservationContactInfo reservationContactInfo = new ReservationContactInfo();
		ReservationAdminInfo reservationAdminInfo = new ReservationAdminInfo();

		reservationPaxDTO.setPnr(pnr);
		reservationPaxDTO.setOriginatorPnr(BeanUtils.nullHandler(rs.getString("ORIGINATOR_PNR")));
		reservationContactInfo.setFirstName(BeanUtils.nullHandler(rs.getString("CF_NAME")));
		reservationContactInfo.setLastName(BeanUtils.nullHandler(rs.getString("CL_NAME")));
		if (additionalDetails) {
			reservationContactInfo.setEmail(BeanUtils.nullHandler(rs.getString("C_EMAIL")));
			reservationContactInfo.setPhoneNo(BeanUtils.nullHandler(rs.getString("C_PHONE")));
			reservationContactInfo.setMobileNo(BeanUtils.nullHandler(rs.getString("C_MOBILE")));
			reservationContactInfo.setSendPromoEmail(rs.getString("SEND_PROMO_EMAIL").equals("Y") ? true : false);
		}		
		reservationAdminInfo.setOriginAgentCode(BeanUtils.nullHandler(rs.getString("OR_AGT_CODE")));
		reservationAdminInfo.setOwnerAgentCode(BeanUtils.nullHandler(rs.getString("OW_AGT_CODE")));
		reservationAdminInfo.setOriginChannelId(BeanUtils.parseInteger(rs.getInt("OR_CHL_CODE")));
		reservationAdminInfo.setOwnerChannelId(BeanUtils.parseInteger(rs.getInt("OW_CHL_CODE")));
		reservationAdminInfo.setOriginUserId(BeanUtils.nullHandler(rs.getString("OR_USR_ID")));

		reservationPaxDTO.setAdultCount(BeanUtils.parseInteger(rs.getInt("PAX_COUNT")));
		reservationPaxDTO.setChildCount(BeanUtils.parseInteger(rs.getInt("CHILD_COUNT")));
		reservationPaxDTO.setInfantCount(BeanUtils.parseInteger(rs.getInt("INF_COUNT")));
		reservationPaxDTO.setPnrDate(rs.getTimestamp("BOOK_DATE"));

		if (ReservationInternalConstants.VoidReservation.YES == BeanUtils.nullHandler(rs.getString("R_VOID_RESERVATION")).charAt(
				0)) {
			reservationPaxDTO.setStatus(ReservationInternalConstants.ReservationStatus.VOID);
		} else {
			reservationPaxDTO.setStatus(BeanUtils.nullHandler(rs.getString("R_STATUS")));
		}

		// Adding reservation contact information
		reservationPaxDTO.setReservationContactInfo(reservationContactInfo);

		// Adding reservation admin information
		reservationPaxDTO.setReservationAdminInfo(reservationAdminInfo);

		return reservationPaxDTO;
	}

	/**
	 * Get PNR Segment information Used by getReservations
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ReservationSegmentDTO getPnrSegmentInformationForReservationSearch(ResultSet rs) throws SQLException {
		ReservationSegmentDTO reservationSegmentDTO = new ReservationSegmentDTO();

		reservationSegmentDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID")));
		reservationSegmentDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("F_NUMBER")));
		reservationSegmentDTO.setFlightStatus(rs.getString("F_STATUS"));
		reservationSegmentDTO.setCodeShareFlightNo(rs.getString("CS_FLIGHT_NUMBER"));
		reservationSegmentDTO.setCodeShareBc(rs.getString("CS_BOOKING_CLASS"));
		reservationSegmentDTO.setCsOcCarrierCode(rs.getString("CS_OC_CARRIER_CODE")); 
		reservationSegmentDTO.setCsOcFlightNumber(rs.getString("CS_OC_FLIGHT_NUMBER")); 
		reservationSegmentDTO.setArrivalDate(rs.getTimestamp("ARR_DATE"));
		reservationSegmentDTO.setDepartureDate(rs.getTimestamp("DEP_DATE"));
		reservationSegmentDTO.setZuluArrivalDate(rs.getTimestamp("ARR_DATE_ZULU"));
		reservationSegmentDTO.setZuluDepartureDate(rs.getTimestamp("DEP_DATE_ZULU"));
		reservationSegmentDTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("SEG_CODE")));
		reservationSegmentDTO.setStatus(BeanUtils.nullHandler(rs.getString("S_STATUS")));
		reservationSegmentDTO.setSubStatus(rs.getString("S_SUB_STATUS"));
		reservationSegmentDTO.setReturnFlag(BeanUtils.nullHandler(rs.getString("RETURN_FLAG")));
		reservationSegmentDTO.setAlertFlag(BeanUtils.parseInteger(rs.getInt("ALERT_FLAG")));
		reservationSegmentDTO.setBookingType(BeanUtils.nullHandler(rs.getString("BC_TYPE")));
		reservationSegmentDTO.setLogicalCCCode(BeanUtils.nullHandler(rs.getString("LOGICAL_CABIN_CLASS_CODE")));

		reservationSegmentDTO.setFlightModelNumber(BeanUtils.nullHandler(rs.getString("MODEL_NUMBER")));
		reservationSegmentDTO.setFlightModelDescription(BeanUtils.nullHandler(rs.getString("FLT_DESCRIPTION")));

		String durationTime = getDuration(rs.getTimestamp("DEP_DATE_ZULU"), rs.getTimestamp("ARR_DATE_ZULU"));
		reservationSegmentDTO.setFlightDuration(durationTime);
		reservationSegmentDTO.setRemarks(BeanUtils.nullHandler(rs.getString("FLT_REMARKS")));

		return reservationSegmentDTO;
	}

	/**
	 * Get PNR Segment information Used by getPassengerCredit
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ReservationSegmentDTO getPnrSegmentInformationForCreditSearch(ResultSet rs) throws SQLException {
		ReservationSegmentDTO reservationSegmentDTO = new ReservationSegmentDTO();

		reservationSegmentDTO.setPnrSegId(BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID")));
		reservationSegmentDTO.setFlightNo(BeanUtils.nullHandler(rs.getString("F_NUMBER")));
		reservationSegmentDTO.setArrivalDate(rs.getTimestamp("ARR_DATE"));
		reservationSegmentDTO.setDepartureDate(rs.getTimestamp("DEP_DATE"));
		reservationSegmentDTO.setZuluArrivalDate(rs.getTimestamp("ARR_DATE_ZULU"));
		reservationSegmentDTO.setZuluDepartureDate(rs.getTimestamp("DEP_DATE_ZULU"));
		reservationSegmentDTO.setSegmentCode(BeanUtils.nullHandler(rs.getString("SEG_CODE")));
		reservationSegmentDTO.setStatus(BeanUtils.nullHandler(rs.getString("S_STATUS")));
		reservationSegmentDTO.setSubStatus(BeanUtils.nullHandler(rs.getString("S_SUB_STATUS")));
		reservationSegmentDTO.setReturnFlag(BeanUtils.nullHandler(rs.getString("RETURN_FLAG")));
		reservationSegmentDTO.setAlertFlag(BeanUtils.parseInteger(rs.getInt("ALERT_FLAG")));

		return reservationSegmentDTO;
	}

	/**
	 * Get PNR Passenger information Used by getPassengerCredit
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ReservationPaxDetailsDTO getPnrPassengerInformation(Integer pnrPaxId, ResultSet rs) throws SQLException {
		ReservationPaxDetailsDTO reservationPaxDetailsDTO = new ReservationPaxDetailsDTO();

		reservationPaxDetailsDTO.setPnrPaxId(pnrPaxId);
		reservationPaxDetailsDTO.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
		reservationPaxDetailsDTO.setPaxType(BeanUtils.nullHandler(rs.getString("PAX_TYPE")));
		reservationPaxDetailsDTO.setFirstName(BeanUtils.nullHandler(rs.getString("PF_NAME")));
		reservationPaxDetailsDTO.setLastName(BeanUtils.nullHandler(rs.getString("PL_NAME")));
		reservationPaxDetailsDTO.setCredit(rs.getBigDecimal("BALANCE"));
		return reservationPaxDetailsDTO;
	}

	/**
	 * Compose the GetPassengerCredit SQL Used by getPassengerCredit
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String composeGetPassengerCreditSql(ReservationSearchDTO reservationSearchDTO) {
		Map map = new LinkedHashMap();

		String filterByOwnerAgentCodeSql = "";
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode())) {
			filterByOwnerAgentCodeSql += " ( OW_AGT_CODE = ? ";
			if (reservationSearchDTO.isSearchGSABookings()) {
				filterByOwnerAgentCodeSql += " OR OW_AGT_CODE in ( SELECT agent_code from  t_agent where report_to_gsa ='Y' and gsa_code in (SELECT AG.AGENT_CODE FROM T_AGENT AG WHERE AG.AGENT_TYPE_CODE='GSA' AND AG.AGENT_CODE=?))";
			}
			if (reservationSearchDTO.getBookingCategories() != null && !reservationSearchDTO.getBookingCategories().isEmpty()) {
				filterByOwnerAgentCodeSql += " OR R_BOOKING_CATEGORY IN (  "
						+ BeanUtils.constructINString(reservationSearchDTO.getBookingCategories()) + " ) ";
			}
			filterByOwnerAgentCodeSql += " ) ";
		}

		/**
		 * AARESAA-5027: Restrict to use credit of one pax to another M. Naeem Akhtar (NA) 14OCT2010
		 * **/
		// Reservation Contact First Name and Passenger First Name
		map.put(" UPF_NAME LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getFirstName()));

		// Reservation Contact Last Name and Passenger Last Name
		map.put(" UPL_NAME LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getLastName()));

		// PNR Number
		map.put(" PNR_NUMBER LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getPnr()));

		// Originator PNR
		map.put(" ORIGINATOR_PNR LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginatorPnr()));

		// From Airport
		map.put(" SEG_CODE LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getFromAirport()));

		// To Airport
		map.put(" Seg_Code LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getToAirport()));

		// Departure Date
		map.put(" DEP_DATE >= ? ", BeanUtils.isNullHandler(reservationSearchDTO.getDepartureDate()));

		// Return Date
		map.put(" ARR_DATE <= ? ", BeanUtils.isNullHandler(reservationSearchDTO.getReturnDate()));

		// Early Departure Date
		map.put(" DEP_DATE <= ? ", BeanUtils.isNullHandler(reservationSearchDTO.getEarlyDepartureDate()));

		// Telephone Number and Mobile Number
		map.put(" ( rCon.C_PHONE LIKE ? OR rCon.C_MOBILE LIKE ? ) ",
				BeanUtils.isNullHandler(reservationSearchDTO.getTelephoneNo()));

		// Customer Id
		map.put(" rCon.CUSTOMER_ID = ? ", BeanUtils.isNullHandler(reservationSearchDTO.getCustomerId()));

		// Flight Number
		map.put(" F_NUMBER LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getFlightNo()));

		// Origin Channel Ids
		map.put(" OR_CHL_CODE IN ("
				+ (reservationSearchDTO.getOriginChannelIds() == null ? "" : BeanUtils
						.constructINStringForInts(reservationSearchDTO.getOriginChannelIds())) + ")",
				BeanUtils.isNullHandler(reservationSearchDTO.getOriginChannelIds()));

		// Owner Channel Id
		map.put(" OW_CHL_CODE = ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOwnerChannelId()));

		// Origin Agent Code
		map.put(" OR_AGT_CODE LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginAgentCode()));

		// Owner Agent Code
		map.put(filterByOwnerAgentCodeSql, BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode()));

		// Origin User Id
		map.put(" OR_USR_ID LIKE ? ", BeanUtils.isNullHandler(reservationSearchDTO.getOriginUserId()));

		// Reservation States
		map.put(" R_STATUS IN ("
				+ (reservationSearchDTO.getReservationStates() == null ? "" : BeanUtils.constructINString(reservationSearchDTO
						.getReservationStates())) + ")", BeanUtils.isNullHandler(reservationSearchDTO.getReservationStates()));

		// Balance Amount
		map.put(" BALANCE <> 0 ",
				BeanUtils.isNullHandler(reservationSearchDTO.isFilterZeroCredits() == true ? new Boolean(true) : null));

		// Generating the dynamic sql
		return BeanUtils.sqlGenerator(map, true);
	}

	/**
	 * Compose GetPassengerCredit arguments Used by getPassengerCredit
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeGetPassengerCreditArgs(ReservationSearchDTO reservationSearchDTO) {
		// Generating the dynamic values
		Collection values = new ArrayList();

		/**
		 * AARESAA-5027: Restrict to use credit of one pax to another M. Naeem Akhtar (NA) 14OCT2010
		 * **/
		// First Name
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getFirstName()).booleanValue()) {
			// Need two parameters
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getFirstName(), null).toUpperCase());
		}

		// Last Name
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getLastName()).booleanValue()) {
			// Need two parameters
			values.add(BeanUtils.wildCardParser(reservationSearchDTO.getLastName(), null).toUpperCase());
		}

		// PNR Number
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getPnr()).booleanValue()) {
			values.add(reservationSearchDTO.getPnr());
		}

		// Originator PNR Number
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOriginatorPnr()).booleanValue()) {
			values.add(reservationSearchDTO.getOriginatorPnr());
		}

		// From Airport
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getFromAirport()).booleanValue()) {
			values.add(reservationSearchDTO.getFromAirport() + BeanUtils.SEPARATOR_ALL);
		}

		// To Airport
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getToAirport()).booleanValue()) {
			values.add(BeanUtils.ALL_SEPARATOR + reservationSearchDTO.getToAirport());
		}

		// Departure Date
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getDepartureDate()).booleanValue()) {
			values.add(BeanUtils.getTimestamp(reservationSearchDTO.getDepartureDate()));
		}

		// Return Date
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getReturnDate()).booleanValue()) {
			values.add(BeanUtils.getTimestamp(reservationSearchDTO.getReturnDate()));
		}

		// Early Departure Date
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getEarlyDepartureDate()).booleanValue()) {
			values.add(BeanUtils.getTimestamp(reservationSearchDTO.getEarlyDepartureDate()));
		}

		// Telephone Number
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getTelephoneNo()).booleanValue()) {
			values.add(reservationSearchDTO.getTelephoneNoForSQL());
			values.add(reservationSearchDTO.getTelephoneNoForSQL());
		}

		// Customer Id
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getCustomerId()).booleanValue()) {
			values.add(reservationSearchDTO.getCustomerId());
		}

		// Flight Number
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getFlightNo()).booleanValue()) {
			values.add(reservationSearchDTO.getFlightNo());
		}

		// Owner Channel Code
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerChannelId()).booleanValue()) {
			values.add(reservationSearchDTO.getOwnerChannelId());
		}

		// Origin Agent Code
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOriginAgentCode()).booleanValue()) {
			values.add(reservationSearchDTO.getOriginAgentCode());
		}

		// Owner Agent Code
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOwnerAgentCode()).booleanValue()) {
			values.add(reservationSearchDTO.getOwnerAgentCode());
			if (reservationSearchDTO.isSearchGSABookings()) {
				values.add(reservationSearchDTO.getOwnerAgentCode());
			}
		}

		// Origin User Id
		if (!BeanUtils.isNullHandler(reservationSearchDTO.getOriginUserId()).booleanValue()) {
			values.add(reservationSearchDTO.getOriginUserId());
		}

		return values;
	}

	/**
	 * Search Passenger Credit based on the dynamic user parameters NOTE: BETTER TO BE USED FOR READ ONLY PURPOSES (FAST
	 * LANE READER)
	 * 
	 * @param reservationSearchDTO
	 * @return
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<ReservationPaxDTO> getPassengerCredit(ReservationSearchDTO reservationSearchDTO, boolean withAllPaxCredits) {
		log.debug("Inside getPassengerCredit");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Holds All Passengers
		final Set<Integer> paxSet = new HashSet<Integer>();

		// Compose the dynamic SQL
		String sqlWhereClause = composeGetPassengerCreditSql(reservationSearchDTO);

		// Compose the dynamic SQL arguments
		Collection values = composeGetPassengerCreditArgs(reservationSearchDTO);

		String sql = " SELECT * FROM (SELECT r.PNR PNR_NUMBER, r.ORIGINATOR_PNR ORIGINATOR_PNR, rCon.C_FIRST_NAME CF_NAME, rCon.C_LAST_NAME CL_NAME, "
				+ " rCon.UPPER_C_FIRST_NAME UCF_NAME, rCon.UPPER_C_LAST_NAME UCL_NAME, r.TOTAL_PAX_COUNT PAX_COUNT, "
				+ " r.TOTAL_PAX_CHILD_COUNT CHILD_COUNT, r.TOTAL_PAX_INFANT_COUNT INF_COUNT, r.BOOKING_TIMESTAMP BOOK_DATE, r.VOID_RESERVATION R_VOID_RESERVATION ,"
				+ " r.STATUS R_STATUS , r.BOOKING_CATEGORY_CODE R_BOOKING_CATEGORY , r.ORIGIN_CHANNEL_CODE OR_CHL_CODE, r.OWNER_CHANNEL_CODE OW_CHL_CODE, "
				+ " r.ORIGIN_AGENT_CODE OR_AGT_CODE, r.OWNER_AGENT_CODE OW_AGT_CODE, r.ORIGIN_USER_ID OR_USR_ID, "
				+ " rCon.CUSTOMER_ID CUSTOMER_ID, rCon.C_PHONE_NO C_PHONE, rCon.C_MOBILE_NO C_MOBILE, rCon.SEND_PROMO_EMAIL SEND_PROMO_EMAIL, "
				+ " p.PNR_PAX_ID PAX_ID, p.FIRST_NAME PF_NAME, p.LAST_NAME PL_NAME, p.TITLE TITLE, p.PAX_TYPE_CODE PAX_TYPE, "
				+ " p.UPPER_FIRST_NAME UPF_NAME, p.UPPER_LAST_NAME UPL_NAME, "
				+ " rSeg.PNR_SEG_ID PNR_SEG_ID, rSeg.RETURN_FLAG RETURN_FLAG, rSeg.STATUS S_STATUS, rSeg.SUB_STATUS S_SUB_STATUS, rSeg.ALERT_FLAG ALERT_FLAG, "
				+ " fSeg.EST_TIME_DEPARTURE_LOCAL DEP_DATE, fSeg.EST_TIME_ARRIVAL_LOCAL ARR_DATE, "
				+ " fSeg.EST_TIME_DEPARTURE_ZULU DEP_DATE_ZULU, fSeg.EST_TIME_ARRIVAL_ZULU ARR_DATE_ZULU, "
				+ " fSeg.SEGMENT_CODE SEG_CODE, "
				+ " f.FLIGHT_NUMBER F_NUMBER, (SELECT SUM(BALANCE) FROM T_PAX_CREDIT c WHERE c.PNR_PAX_ID = p.PNR_PAX_ID) BALANCE "
				+ " FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon, T_PNR_SEGMENT rSeg, T_FLIGHT_SEGMENT fSeg, T_FLIGHT f, T_PNR_PASSENGER p, T_GDS gds "
				+ " WHERE r.PNR=rCon.PNR AND r.PNR = rSeg.PNR AND rSeg.FLT_SEG_ID = fSeg.FLT_SEG_ID AND fSeg.FLIGHT_ID = f.FLIGHT_ID  "
				+ " AND r.PNR = p.PNR AND (r.GDS_ID IS NULL OR gds.gds_id = r.gds_id AND gds.cs_carrier  ='N')) " + sqlWhereClause
				+ " ORDER BY DEP_DATE DESC ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL number of parameters : " + values.size());
		log.debug("############################################");

		List<ReservationPaxDTO> collection = (ArrayList<ReservationPaxDTO>) jt.query(sql, values.toArray(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<ReservationPaxDTO> bookings = new ArrayList();
						ReservationPaxDTO reservationPaxDTO;
						String pnr;
						Integer pnrPaxId;
						Integer pnrSegId;
						Set<String> pnrSet = new HashSet<String>();
						Set segmentSet = new HashSet();
						Map<String, ReservationPaxDTO> pnrMap = new LinkedHashMap();

						if (rs != null) {
							while (rs.next()) {
								// Pick pnr number
								pnr = BeanUtils.nullHandler(rs.getString("PNR_NUMBER"));

								// If pnr number exist
								if (pnrSet.contains(pnr)) {
									reservationPaxDTO = pnrMap.get(pnr);
									pnrPaxId = BeanUtils.parseInteger(rs.getInt("PAX_ID"));

									if (paxSet.contains(pnrPaxId)) {
										// Fill PNR Segment information
										pnrSegId = BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID"));
										if (!segmentSet.contains(pnrSegId)) {
											reservationPaxDTO = composeSegments(rs, reservationPaxDTO);
											segmentSet.add(pnrSegId);
										}
									} else {
										// Fill PNR Passenger information
										reservationPaxDTO = composePassengers(pnrPaxId, rs, reservationPaxDTO);

										// Fill PNR Segment information
										pnrSegId = BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID"));
										if (!segmentSet.contains(pnrSegId)) {
											reservationPaxDTO = composeSegments(rs, reservationPaxDTO);
											segmentSet.add(pnrSegId);
										}

										// Update the Set with the PNR Pax number
										paxSet.add(pnrPaxId);
									}

									// Stores the Pnr
									pnrMap.put(pnr, reservationPaxDTO);
								} else {
									// Fill PNR Information
									reservationPaxDTO = getPnrInformation(pnr, rs, false);
									pnrPaxId = BeanUtils.parseInteger(rs.getInt("PAX_ID"));

									if (paxSet.contains(pnrPaxId)) {
										// Fill PNR Segment information
										pnrSegId = BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID"));
										if (!segmentSet.contains(pnrSegId)) {
											reservationPaxDTO = composeSegments(rs, reservationPaxDTO);
											segmentSet.add(pnrSegId);
										}
									} else {
										// Fill PNR Passenger information
										reservationPaxDTO = composePassengers(pnrPaxId, rs, reservationPaxDTO);

										// Fill PNR Segment information
										pnrSegId = BeanUtils.parseInteger(rs.getInt("PNR_SEG_ID"));
										if (!segmentSet.contains(pnrSegId)) {
											reservationPaxDTO = composeSegments(rs, reservationPaxDTO);
											segmentSet.add(pnrSegId);
										}

										// Update the Set with the PNR Pax number
										paxSet.add(pnrPaxId);
									}

									// Add the pnr number
									pnrSet.add(pnr);

									// Stores the PNR number
									pnrMap.put(pnr, reservationPaxDTO);
								}
							}
						}

						// Returning the values
						bookings = pnrMap.values();

						return new ArrayList(bookings);
					}
				});

		// Loads all passenger credits information if needed
		if (withAllPaxCredits) {

			// Get all passenger Credit
			Map allPassengerCredits = getPassengerCredits(paxSet);

			Iterator<ReservationPaxDTO> itrBookings = collection.iterator();

			ReservationPaxDTO reservationPaxDTO;
			ReservationPaxDetailsDTO reservationPaxDetailsDTO;

			Collection passengerCredit;
			Iterator itrPassenger;

			while (itrBookings.hasNext()) {
				reservationPaxDTO = itrBookings.next();
				itrPassenger = reservationPaxDTO.getPassengers().iterator();

				while (itrPassenger.hasNext()) {
					reservationPaxDetailsDTO = (ReservationPaxDetailsDTO) itrPassenger.next();
					passengerCredit = (Collection) allPassengerCredits.get(reservationPaxDetailsDTO.getPnrPaxId());
					reservationPaxDetailsDTO.setPaxCredit(passengerCredit);
				}
			}
		}

		log.debug("Exit getPassengerCredit");
		return collection;
	}

	/**
	 * Compose Passengers Used by getPassengerCredit
	 * 
	 * @param pnrPaxId
	 * @param rs
	 * @param reservationPaxDTO
	 * @return
	 * @throws SQLException
	 */
	private ReservationPaxDTO composePassengers(Integer pnrPaxId, ResultSet rs, ReservationPaxDTO reservationPaxDTO)
			throws SQLException {

		// Adding pnr passenger information to the reservation
		reservationPaxDTO.addPassenger(getPnrPassengerInformation(pnrPaxId, rs));

		return reservationPaxDTO;
	}

	/**
	 * Compose Segments Used by getPassengerCredit
	 * 
	 * @param rs
	 * @param reservationPaxDTO
	 * @return
	 * @throws SQLException
	 */
	private ReservationPaxDTO composeSegments(ResultSet rs, ReservationPaxDTO reservationPaxDTO) throws SQLException {

		// Adding pnr segment information to the reservation
		reservationPaxDTO.addSegment(getPnrSegmentInformationForCreditSearch(rs));

		return reservationPaxDTO;
	}

	/**
	 * Get all passenger credit information when pax id(s) are given
	 * 
	 * @param paxIds
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map<Integer, Collection<PaxCreditDTO>> getPassengerCredits(Collection paxIds) {
		log.debug("Inside getPassengerCredits");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Iterator iterator = paxIds.iterator();
		Map map = new LinkedHashMap();
		Collection values = new ArrayList();
		String paxId;

		// Composing the sql with arguments
		int i = 1;
		while (iterator.hasNext()) {
			paxId = (String) iterator.next();
			if (i == 1) {
				map.put(" WHERE PNR_PAX_ID = ? ", BeanUtils.isNullHandler(paxId));
			} else {
				map.put(" OR PNR_PAX_ID = ? ", BeanUtils.isNullHandler(paxId));
			}

			i++;
			values.add(paxId);
		}

		// Generating the dynamic sql
		String sql = BeanUtils.sqlGenerator(map, false);

		// Final SQL
		sql = "SELECT PNR_PAX_ID PAX_ID, BALANCE BALANCE, DATE_EXP DATE_OF_EXP FROM T_PAX_CREDIT " + sql
				+ " ORDER BY PNR_PAX_ID ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL number of parameters : " + values.size());
		log.debug("############################################");

		Map<Integer, Collection<PaxCreditDTO>> hashMap = (Map<Integer, Collection<PaxCreditDTO>>) jt.query(sql, values.toArray(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<PaxCreditDTO> paxCredits = new ArrayList<PaxCreditDTO>();
						Set<Integer> paxSet = new HashSet<Integer>();
						Map<Integer, Collection<PaxCreditDTO>> paxMap = new HashMap<Integer, Collection<PaxCreditDTO>>();
						PaxCreditDTO paxCreditDTO;
						Integer pnrPaxId;

						if (rs != null) {
							while (rs.next()) {
								// Pick pnr pax number number
								pnrPaxId = BeanUtils.parseInteger(rs.getInt("PAX_ID"));

								// If pnr pax number exist
								if (paxSet.contains(pnrPaxId)) {
									paxCredits = paxMap.get(pnrPaxId);

									paxCreditDTO = new PaxCreditDTO();
									paxCreditDTO.setPaxId(pnrPaxId);
									paxCreditDTO.setDateOfExpiry(rs.getDate("DATE_OF_EXP"));
									paxCreditDTO.setBalance(rs.getBigDecimal("BALANCE"));

									paxCredits.add(paxCreditDTO);
								} else {
									paxCreditDTO = new PaxCreditDTO();
									paxCreditDTO.setPaxId(pnrPaxId);
									paxCreditDTO.setDateOfExpiry(rs.getDate("DATE_OF_EXP"));
									paxCreditDTO.setBalance(rs.getBigDecimal("BALANCE"));

									paxCredits = new ArrayList();
									paxCredits.add(paxCreditDTO);

									// Add the Passenger Number
									paxSet.add(pnrPaxId);

									// Stores the PNR pax number
									paxMap.put(pnrPaxId, paxCredits);
								}
							}
						}

						return paxMap;
					}
				});

		log.debug("Exit getPassengerCredits");
		return hashMap;
	}

	/**
	 * Return reservation contact information
	 * 
	 * @param pnr
	 * @return object of ReservationContactInfo
	 * @throws CommonsDataAccessException
	 */
	@Override
	public ReservationContactInfo getReservationContactInfo(String pnr) throws CommonsDataAccessException {
		log.debug("Inside getReservationContactInfo");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = " SELECT rCon.CUSTOMER_ID C_ID, rCon.C_TITLE TITLE, rCon.C_FIRST_NAME F_NAME, rCon.C_LAST_NAME L_NAME, "
				+ " rCon.C_STREET_ADDRESS_1 S_ADD_1, rCon.C_STREET_ADDRESS_2 S_ADD_2, rCon.C_CITY CITY, "
				+ " rCon.C_STATE STATE, rCon.C_COUNTRY_CODE C_CODE, rCon.C_ZIP_CODE ZIP_CODE, rCon.C_PHONE_NO P_NO, rCon.C_MOBILE_NO M_NO, "
				+ " rCon.C_FAX FAX, rCon.C_EMAIL EMAIL, rCon.C_NATIONALITY_CODE N_CODE, "
				+ " rCon.E_TITLE E_TITLE, rCon.E_FIRST_NAME E_FIRST_NAME, rCon.E_LAST_NAME E_LAST_NAME, "
				+ " rCon.E_PHONE_NO E_PHONE_NO, rCon.E_EMAIL E_EMAIL, rCon.TAX_REG_NUMBER, rCon.PREFERRED_LANG, rCon.VERSION , rCon.SEND_PROMO_EMAIL SEND_PROMO_EMAIL"
				+ " FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE " + " r.PNR=rCon.PNR AND r.PNR=? ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL argument             : " + pnr);
		log.debug("############################################");

		ReservationContactInfo reservationContactInfo = (ReservationContactInfo) jt.query(sql, new Object[] { pnr },
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						ReservationContactInfo reservationContactInfo = null;

						if (rs != null) {
							if (rs.next()) {
								reservationContactInfo = new ReservationContactInfo();

								reservationContactInfo.setCustomerId(BeanUtils.parseInteger(rs.getInt("C_ID")));
								reservationContactInfo.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
								reservationContactInfo.setFirstName(BeanUtils.nullHandler(rs.getString("F_NAME")));
								reservationContactInfo.setLastName(BeanUtils.nullHandler(rs.getString("L_NAME")));

								reservationContactInfo.setStreetAddress1(BeanUtils.nullHandler(rs.getString("S_ADD_1")));
								reservationContactInfo.setStreetAddress2(BeanUtils.nullHandler(rs.getString("S_ADD_2")));
								reservationContactInfo.setCity(BeanUtils.nullHandler(rs.getString("CITY")));
								reservationContactInfo.setState(BeanUtils.nullHandler(rs.getString("STATE")));
								reservationContactInfo.setNationalityCode(BeanUtils.nullHandler(rs.getString("N_CODE")));
								reservationContactInfo.setCountryCode(BeanUtils.nullHandler(rs.getString("C_CODE")));
								reservationContactInfo.setZipCode(BeanUtils.nullHandler(rs.getString("ZIP_CODE")));
								reservationContactInfo.setTaxRegNo(BeanUtils.nullHandler(rs.getString("TAX_REG_NUMBER")));

								reservationContactInfo.setPhoneNo(BeanUtils.nullHandler(rs.getString("P_NO")));
								reservationContactInfo.setMobileNo(BeanUtils.nullHandler(rs.getString("M_NO")));
								reservationContactInfo.setFax(BeanUtils.nullHandler(rs.getString("FAX")));
								reservationContactInfo.setEmail(BeanUtils.nullHandler(rs.getString("EMAIL")));
								reservationContactInfo.setPreferredLanguage(BeanUtils.nullHandler(rs.getString("PREFERRED_LANG")));
								reservationContactInfo.setVersion(rs.getLong("VERSION"));

								// Emergency Contact Information
								reservationContactInfo.setEmgnTitle(BeanUtils.nullHandler(rs.getString("E_TITLE")));
								reservationContactInfo.setEmgnFirstName(BeanUtils.nullHandler(rs.getString("E_FIRST_NAME")));
								reservationContactInfo.setEmgnLastName(BeanUtils.nullHandler(rs.getString("E_LAST_NAME")));
								reservationContactInfo.setEmgnPhoneNo(BeanUtils.nullHandler(rs.getString("E_PHONE_NO")));
								reservationContactInfo.setEmgnEmail(BeanUtils.nullHandler(rs.getString("E_EMAIL")));
								
								// Send Promo Email Information 
								reservationContactInfo.setSendPromoEmail(rs.getString("SEND_PROMO_EMAIL").equals("Y") ? true : false);
							}
						}

						return reservationContactInfo;
					}
				});

		if (reservationContactInfo == null) {
			throw new CommonsDataAccessException("airreservations.arg.noLongerExist");
		}

		log.debug("Exit getReservationContactInfo");
		return reservationContactInfo;
	}

	@Override
	public ReservationContactInfo getLatestReservationContactInfo(ResContactInfoSearchDTO resContactInfoSearchDTO) {
		log.debug("Inside getLatestReservationContactInfo");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sqlConditions = composeGetLatestResContactInfoSQL(resContactInfoSearchDTO);

		// Final SQL
		String sql = " SELECT * FROM ( "
				+ " SELECT rCon.CUSTOMER_ID C_ID, rCon.C_TITLE TITLE, rCon.C_FIRST_NAME F_NAME, rCon.C_LAST_NAME L_NAME, "
				+ " rCon.C_STREET_ADDRESS_1 S_ADD_1, rCon.C_STREET_ADDRESS_2 S_ADD_2, rCon.C_CITY CITY, "
				+ " rCon.C_STATE STATE, rCon.C_COUNTRY_CODE C_CODE, rCon.C_ZIP_CODE ZIP_CODE, rCon.C_PHONE_NO P_NO, rCon.C_MOBILE_NO M_NO, "
				+ " rCon.C_FAX FAX, rCon.C_EMAIL EMAIL, rCon.C_NATIONALITY_CODE N_CODE, "
				+ " rCon.E_TITLE E_TITLE, rCon.E_FIRST_NAME E_FIRST_NAME, rCon.E_LAST_NAME E_LAST_NAME, "
				+ " rCon.E_PHONE_NO E_PHONE_NO, rCon.E_EMAIL E_EMAIL "
				+ " FROM T_RESERVATION r, T_RESERVATION_CONTACT rCon WHERE r.PNR=rCon.PNR "
				+ (sqlConditions == null ? " " : (" AND " + sqlConditions)) + " ORDER BY r.BOOKING_TIMESTAMP DESC "
				+ " ) WHERE ROWNUM = 1";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		ReservationContactInfo reservationContactInfo = (ReservationContactInfo) jt.query(sql, new Object[] {},
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						ReservationContactInfo reservationContactInfo = null;

						if (rs != null) {
							if (rs.next()) {
								reservationContactInfo = new ReservationContactInfo();

								reservationContactInfo.setCustomerId(BeanUtils.parseInteger(rs.getInt("C_ID")));
								reservationContactInfo.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
								reservationContactInfo.setFirstName(BeanUtils.nullHandler(rs.getString("F_NAME")));
								reservationContactInfo.setLastName(BeanUtils.nullHandler(rs.getString("L_NAME")));

								reservationContactInfo.setStreetAddress1(BeanUtils.nullHandler(rs.getString("S_ADD_1")));
								reservationContactInfo.setStreetAddress2(BeanUtils.nullHandler(rs.getString("S_ADD_2")));
								reservationContactInfo.setCity(BeanUtils.nullHandler(rs.getString("CITY")));
								reservationContactInfo.setState(BeanUtils.nullHandler(rs.getString("STATE")));
								reservationContactInfo.setNationalityCode(BeanUtils.nullHandler(rs.getString("N_CODE")));
								reservationContactInfo.setCountryCode(BeanUtils.nullHandler(rs.getString("C_CODE")));
								reservationContactInfo.setZipCode(BeanUtils.nullHandler(rs.getString("ZIP_CODE")));

								reservationContactInfo.setPhoneNo(BeanUtils.nullHandler(rs.getString("P_NO")));
								reservationContactInfo.setMobileNo(BeanUtils.nullHandler(rs.getString("M_NO")));
								reservationContactInfo.setFax(BeanUtils.nullHandler(rs.getString("FAX")));
								reservationContactInfo.setEmail(BeanUtils.nullHandler(rs.getString("EMAIL")));

								// Emergency Contact Information
								reservationContactInfo.setEmgnTitle(BeanUtils.nullHandler(rs.getString("E_TITLE")));
								reservationContactInfo.setEmgnFirstName(BeanUtils.nullHandler(rs.getString("E_FIRST_NAME")));
								reservationContactInfo.setEmgnLastName(BeanUtils.nullHandler(rs.getString("E_LAST_NAME")));
								reservationContactInfo.setEmgnPhoneNo(BeanUtils.nullHandler(rs.getString("E_PHONE_NO")));
								reservationContactInfo.setEmgnEmail(BeanUtils.nullHandler(rs.getString("E_EMAIL")));
							}
						}

						return reservationContactInfo;
					}
				});

		log.debug("Exit getLatestReservationContactInfo");
		return reservationContactInfo;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String composeGetLatestResContactInfoSQL(ResContactInfoSearchDTO resContactInfoSearchDTO) {
		LinkedList conditions = new LinkedList();

		if (resContactInfoSearchDTO.getFirstName() != null) {
			// case insensitive first name matching
			conditions.add(" rCon.UPPER_C_FIRST_NAME LIKE '"
					+ BeanUtils.wildCardParser(resContactInfoSearchDTO.getFirstName(), null).toUpperCase() + "' ");
		}

		if (resContactInfoSearchDTO.getLastName() != null) {
			// case insensitive last name matching
			conditions.add(" rCon.UPPER_C_LAST_NAME LIKE '"
					+ BeanUtils.wildCardParser(resContactInfoSearchDTO.getLastName(), null).toUpperCase() + "' ");
		}

		if (resContactInfoSearchDTO.getPnr() != null) {
			conditions.add(" r.PNR = '" + resContactInfoSearchDTO.getPnr() + "' ");
		}

		if (resContactInfoSearchDTO.getTelephoneNo() != null) {
			String phoneNumber = resContactInfoSearchDTO.getTelephoneNoForSQL();
			conditions.add(" ( rCon.C_PHONE_NO LIKE '" + phoneNumber + "' OR rCon.C_MOBILE_NO LIKE '" + phoneNumber + "' ) ");
		}

		if (resContactInfoSearchDTO.getEmail() != null) {
			// case insensitive email address matching
			conditions.add(" UPPER(rCon.C_EMAIL) LIKE '"
					+ BeanUtils.wildCardParser(resContactInfoSearchDTO.getEmail(), null).toUpperCase() + "' ");
		}

		String sql = null;
		for (Iterator conditionsIt = conditions.iterator(); conditionsIt.hasNext();) {
			if (sql == null) {
				sql = (String) conditionsIt.next();
			} else {
				sql = sql + " AND " + (String) conditionsIt.next();
			}
		}

		return sql;
	}

	/**
	 * Returns the next pnr number
	 * 
	 * @return
	 */
	@Override
	public String getNextPnrNumber(boolean isGdsReservation) {
		log.debug("Inside getNextPnrNumber");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String pnr;

		log.debug("############################################");
		log.debug(" SQL to excute            : " + pnrGenerationSql);
		log.debug("############################################");


		final String sql = isGdsReservation ? pnrGenerationSqlGds : pnrGenerationSql;


		if (pnrGenerationMethod.equalsIgnoreCase("S")) {
			pnr = (String) jt.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					String pnr = "";

					if (rs != null) {
						if (rs.next()) {
							pnr = BeanUtils.nullHandler(rs.getString("PNR"));
						}
					}

					return pnr;
				}
			});
		} else {
			pnr = (String) jt.execute(new CallableStatementCreator() {

				@Override
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement cs = con.prepareCall(sql);
					cs.registerOutParameter(1, Types.VARCHAR);
					cs.setInt(2, pnrLength);
					return cs;
				}
			}, new CallableStatementCallback() {

				@Override
				public Object doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
					cs.execute();
					String pnrSequence = cs.getString(1);
					return pnrSequence;
				}
			});

		}

		log.debug("Exit getNextPnrNumber");
		return pnr;
	}

	/**
	 * Returns the e Ticket Sequence Number
	 * 
	 * @return
	 */
	@Override
	public String getNextETicketSQID(String strSeq) {
		log.debug("Inside getNextETicketSQID");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = null;

		if (strSeq != null) {
			strSeq.trim();
			sql = " SELECT " + strSeq + ".NEXTVAL ETICKET FROM DUAL ";
		} else {
			sql = " SELECT S_RESERVATION_ETICKET.NEXTVAL ETICKET FROM DUAL ";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String eTicketNo = (String) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String eTicketNo = "";

				if (rs != null) {
					if (rs.next()) {
						eTicketNo = BeanUtils.nullHandler(rs.getString("ETICKET"));
					}
				}

				return eTicketNo;
			}
		});

		log.debug("Exit getNextETicketSQID");
		return eTicketNo;
	}

	/**
	 * save the reservation insurance
	 */
	@Override
	public Integer saveReservationInsurance(ReservationInsurance reservationInsurance) {
		log.debug("Inside saveOrUpdate saveReservationInsurance");

		// Save the reservation
		hibernateSaveOrUpdate(reservationInsurance);

		log.debug("Exit saveOrUpdate saveReservationInsurance");
		return reservationInsurance.getInsuranceId();
	}
	
	public void saveReservationInsurance(List<ReservationInsurance> listReservationInsurance){
		log.debug("Inside saveOrUpdate saveReservationInsurance");
		hibernateSaveOrUpdateAll(listReservationInsurance);
		log.debug("Exit saveOrUpdate saveReservationInsurance");
	}
	/**
	 * Load the Resrvation Insurance
	 */
	@Override
	public ReservationInsurance getReservationInsurance(Integer id) {

		return (ReservationInsurance) get(ReservationInsurance.class, id);
	}

	@Override
	public List<ReservationInsurance> getOnholdReservationInsurance(String pnr) {

		String hql = "select resIns from ReservationInsurance resIns where resIns.state= '"
				+ ReservationInternalConstants.INSURANCE_STATES.OH + "' and resIns.pnr = :pnr";
		return ((List<ReservationInsurance>) getSession().createQuery(hql).setParameter("pnr", pnr).list());	

	}

	@Override
	public ReservationInsurance getQuotedReservationInsurance(Integer id) {

		String hql = "select resIns from ReservationInsurance resIns where resIns.state= '"
				+ ReservationInternalConstants.INSURANCE_STATES.QO + "' and resIns.insuranceId = :insid";
		return ((ReservationInsurance) getSession().createQuery(hql).setParameter("insid", id).uniqueResult());
	}

	@Override
	public BigDecimal getInsuranceMaxValue(String pnr) {
		// Get the max value form ACT and CNX insurance records
		if (pnr != null) {
			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			String sql = "SELECT max(AMOUNT) as AMOUNT FROM T_PNR_INSURANCE a  WHERE a.pnr='" + pnr + "'";
			BigDecimal maxAmount = (BigDecimal) jt.query(sql, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					BigDecimal maxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (rs != null) {
						while (rs.next()) {
							BigDecimal tmpInsAmount = rs.getBigDecimal("AMOUNT");
							if (tmpInsAmount != null) {
								maxAmount = AccelAeroCalculator.add(maxAmount, tmpInsAmount);
							}
							break;
						}
					}
					return maxAmount;
				}
			});
			return maxAmount;
		} else {
			return AccelAeroCalculator.getDefaultBigDecimalZero();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<String> getCarrierCodes() {
		log.debug("Inside getCarrierCodes");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = "SELECT carrier_code, description, status FROM t_carrier a  WHERE status ='ACT' and blockspace_enabled='Y'";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Collection<String> carrierCodes = (Collection<String>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> carrierCodes = new HashSet<String>();

				if (rs != null) {
					while (rs.next()) {
						carrierCodes.add(rs.getString("carrier_code"));
					}
				}

				return carrierCodes;
			}
		});

		log.debug("Exit getCarrierCodes");
		return carrierCodes;
	}

	private String getSubNameQuery(String paxType, ArrayList<String> nameList) {

		StringBuilder subSql = new StringBuilder();

		for (String string : nameList) {
			String paxName = string;
			String title = paxName.split(DLIM)[0];
			String fName = paxName.split(DLIM)[1];
			String lname = paxName.split(DLIM)[2];
			title = (title == null || title.equals("")) ? " a.title is null" : "a.title = " + "'" + title + "'";
			subSql.append("( ");
			subSql.append("a.pax_type_code = " + "'" + paxType.toUpperCase() + "'");
			subSql.append(" AND ");
			subSql.append("a.UPPER_FIRST_NAME = " + "'" + fName.toUpperCase() + "'");
			subSql.append(" AND ");
			subSql.append("a.UPPER_LAST_NAME = " + "'" + lname.toUpperCase() + "'");
			subSql.append(" AND ");
			subSql.append(title);
			subSql.append(" )");
			subSql.append(" OR ");
		}

		return subSql.toString();
	}

	/**
	 * Check weather given names are duplicated in flight segment JIRA AARESAA-1953
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<String> getDuplicateNames(Collection<Integer> flightSegIds, ArrayList<String> adultNameList,
			ArrayList<String> childNameList, String pnr, Collection<Integer> paxIds) {

		StringBuilder sql = new StringBuilder();
		String query = "";
		sql.append("SELECT a.title, a.first_name, a.last_name, a.pnr,fl.flight_number, fs.segment_code ");
		sql.append(" FROM t_pnr_passenger a, t_pnr_segment b,t_flight_segment fs, t_flight fl  WHERE ");
		sql.append(" a.pnr = b.pnr and b.flt_seg_id in ( " + Util.constructINStringForInts(flightSegIds) + ") AND ( ");

		if (adultNameList != null) {
			sql.append(getSubNameQuery("AD", adultNameList));
		}
		if (childNameList != null) {
			sql.append(getSubNameQuery("CH", childNameList));
		}

		if (sql.toString().endsWith(" OR ")) {
			query = sql.substring(0, sql.lastIndexOf(" OR "));
		}

		query += " )";
		query += " and  b.open_rt_confirm_before is null ";
		query += " and  b.flt_seg_id = fs.flt_seg_id and  fs.flight_id = fl.flight_id and b.status <> '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CANCEL + "' ";

		if (pnr != null && !"".equals(pnr)) {
			query += " AND a.pnr <> '" + pnr + "' ";
		} else if (paxIds != null && paxIds.size() > 0) {
			query += "AND a.pnr <> ( SELECT pnr FROM T_PNR_PASSENGER WHERE pnr_pax_id IN ("
					+ Util.constructINStringForInts(paxIds) + ")";
		}

		if (pnr != null && !"".equals(pnr)) {
			query += " AND b.flt_seg_id not in (select unique flt_seg_id from t_pnr_segment ps where  ps.status ='"
				+ ReservationInternalConstants.ReservationSegmentStatus.CANCEL + "' and ps.pnr = '" + pnr + "' ) ";
		}

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Collection<String>) template.query(query, new ResultSetExtractor() {
			Collection<String> dupNameCollection = new ArrayList<String>();
			StringBuilder tempStr = new StringBuilder("");

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {

					tempStr.append(BeanUtils.nullHandler(rs.getString("TITLE")));
					tempStr.append(". ");
					tempStr.append(rs.getString("FIRST_NAME"));
					tempStr.append(" ");
					tempStr.append(rs.getString("LAST_NAME"));
					tempStr.append("(");
					tempStr.append(rs.getString("PNR"));
					tempStr.append(" [");
					tempStr.append(rs.getString("FLIGHT_NUMBER"));
					tempStr.append("-");
					tempStr.append(rs.getString("SEGMENT_CODE"));
					tempStr.append("]");
					tempStr.append(") ,");
					dupNameCollection.add(tempStr.toString());
					tempStr.setLength(0);
				}

				return dupNameCollection;
			}
		});
	}

	/**
	 * @author indika
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CarrierDTO> getCarriers() {
		log.debug("Inside getCarrierCodes");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = "SELECT carrier_code, description, status, c, blockspace_enabled, drysell_enabled  FROM t_carrier a  WHERE status ='ACT'";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Collection<CarrierDTO> carriers = (Collection<CarrierDTO>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<CarrierDTO> carriers = new HashSet<CarrierDTO>();

				if (rs != null) {
					while (rs.next()) {
						CarrierDTO carrierDTO = new CarrierDTO();
						carrierDTO.setCarrierCode(rs.getString("carrier_code"));
						carrierDTO.setDescription(rs.getString("description"));
						String drySellEnabled = rs.getString("drysell_enabled");
						String blockspaceEnabled = rs.getString("blockspace_enabled");
						if (drySellEnabled.equalsIgnoreCase("Y")) {
							carrierDTO.setDrySellEnabled(true);
						}
						if (blockspaceEnabled.equalsIgnoreCase(blockspaceEnabled)) {
							carrierDTO.setBlockSpaceEnabled(true);
						}
						carriers.add(carrierDTO);
					}
				}

				return carriers;
			}
		});

		log.debug("Exit getCarrierCodes");
		return carriers;
	}

	/**
	 * Get Carriers by Airline
	 * 
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<String> getCarrierCodesByAirline(String airlineCode) {
		log.debug("Inside getCarrierCodesByAirline");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = "SELECT carrier_code, description, status FROM t_carrier a  WHERE airline_code ='" + airlineCode + "'";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Collection<String> carriers = (Collection<String>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> carriers = new HashSet<String>();

				if (rs != null) {
					while (rs.next()) {
						carriers.add(rs.getString("carrier_code"));
					}
				}

				return carriers;
			}
		});

		log.debug("Exit getCarrierCodes");
		return carriers;
	}

	@Override
	public PnrFarePassenger getPnrFarePassenger(String pnr, int flightSegmentId, int paxSequence) {
		String sql = "SELECT a.pnr_seg_id, d.pnr_pax_id, c.ppf_id FROM t_pnr_segment a,  "
				+ "t_pnr_pax_fare_segment b , t_pnr_pax_fare c, t_pnr_passenger d "
				+ " WHERE  a.pnr_seg_id  = b.pnr_seg_id AND b.ppf_id = c.ppf_id  AND d.pnr_pax_id = c.pnr_pax_id  AND a.pnr = d.pnr  "
				+ " AND a.status != 'CNX' AND a.flt_seg_id = '" + flightSegmentId + "' AND d.pnr = '" + pnr
				+ "' AND d.pax_sequence = '" + paxSequence + "'";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		PnrFarePassenger pnrFarePax = (PnrFarePassenger) template.queryForObject(sql, new RowMapper() {
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				PnrFarePassenger pnrFarePax = null;
				pnrFarePax = new PnrFarePassenger();
				pnrFarePax.setPnrSegmentId(new Integer(rs.getInt("pnr_seg_id")));
				pnrFarePax.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
				pnrFarePax.setPnrPaxFareId(new Integer(rs.getInt("ppf_id")));
				return pnrFarePax;
			}
		});

		return pnrFarePax;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<AncillaryReminderDetailDTO> getFlightSegmentsToScheduleReminder(Date date, String schedulerType) {
		log.debug(" Inside getFlightListToScheduleReminder() ");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String strDate = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		strDate = dateFormat.format(date);
		String scheluledJobHourDurationPeriod = AppSysParamsUtil.getAncillaryReminderSchedulerTimeDuration();
		int scheluledJobHourDuration = Integer.parseInt(scheluledJobHourDurationPeriod);

		StringBuffer sql = new StringBuffer();
		String query = "";

		sql.append("  SELECT DISTINCT F.FLIGHT_NUMBER AS FLIGHT_NUMBER, FS.FLT_SEG_ID AS FLT_SEG_ID, 	 \n ");
		sql.append("  FS.FLIGHT_ID AS FLIGHT_ID, FS.SEGMENT_CODE AS FLT_SEG_CODE,   					 \n ");
		sql.append("  FSNE.FLT_SEG_NOTIFICATION_EVNT_ID AS FLTSEG_NOTI_ID, FS.EST_TIME_DEPARTURE_LOCAL AS EST_TIME_DEPARTURE_LOCAL,      \n ");
		sql.append("  FS.EST_TIME_DEPARTURE_ZULU - NUMTODSINTERVAL(A.ANCI_NOTIFY_START_CUTOVER,'MINUTE') AS SND_NOTIFICATION_TIME,       \n ");
		sql.append("  FS.EST_TIME_DEPARTURE_ZULU AS EST_TIME_DEPARTURE_ZULU,  SUBSTR(FS.SEGMENT_CODE,0,3) AS ORIGIN,   					 \n ");
		sql.append("  A.ANCI_NOTIFY_START_CUTOVER AS ANCI_NOTIFY_START_CUTOVER,  A.ANCI_NOTIFY_END_CUTOVER AS ANCI_NOTIFY_END_CUTOVER,   \n ");
		sql.append("  A.ONLINE_CHECKIN AS ONLINE_CHECKIN      		\n ");
		sql.append("  FROM  										\n ");
		sql.append("  T_FLIGHT F,									\n ");
		sql.append("  T_AIRPORT A,									\n ");
		sql.append("  T_FLIGHT_SEGMENT FS, 							\n ");
		sql.append("  T_FLIGHT_SEG_NOTIFICATION_EVNT FSNE 			\n ");
		sql.append("  WHERE											\n ");
		sql.append("  F.FLIGHT_ID = FS.FLIGHT_ID					\n ");
		sql.append("  AND FS.FLT_SEG_ID = FSNE.FLT_SEG_ID (+)		\n ");
		sql.append("  AND F.FLIGHT_ID IN (SELECT FL.FLIGHT_ID FROM T_FLIGHT_LEG FL 		\n ");
		sql.append("  					 WHERE FL.route_id IN 							\n ");
		sql.append("  										(SELECT RI.ond_code FROM T_ROUTE_INFO RI 		\n ");
		sql.append("  										WHERE RI.ENABLE_ANCI_REMINDERS= 'Y' AND RI.NO_OF_TRANSITS = 0))			\n ");

		if (schedulerType.equals(ReservationInternalConstants.SchedulerType.MASTER_SCHEDULER)) {
			sql.append("  --- MASTER SCHEDULER CONDITION (IT WILL RUN DAILY MIDNIGHT)									\n ");
			sql.append("  AND (FSNE.ANCI_NOTIFY_STATUS IS NULL OR FSNE.NOTIFICATION_TYPE != '"
					+ ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER + "')  \n");

		} else if (schedulerType.equals(ReservationInternalConstants.SchedulerType.RECOVERY_SCHEDULER)) {
			sql.append("  --- RECOVERY SCHEDULER CONDITION		(IT WILL RUN AFTER EACH ONE HOUR)						\n ");
			sql.append("  AND (FSNE.NO_OF_ATTEMPTS <=5 AND 																	\n ");
			sql.append("      (FSNE.ANCI_NOTIFY_STATUS = 'FAILED' 														\n ");
			sql.append("  		OR (FSNE.ANCI_NOTIFY_STATUS = 'INPROGRESS' AND  to_date('" + strDate
					+ "', 'DD-MM-YYYY HH24:MI:SS') - FSNE.timestamp > 2/24) 	    	\n ");
			sql.append("  		OR (FSNE.ANCI_NOTIFY_STATUS = 'RESCHEDULED' AND  to_date('" + strDate
					+ "', 'DD-MM-YYYY HH24:MI:SS') - FSNE.timestamp > 2/24))) 		\n ");
			sql.append("  AND FSNE.NOTIFICATION_TYPE != '"
					+ ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER + "'  \n");
		}

		sql.append("  AND F.ORIGIN = A.AIRPORT_CODE																	    \n ");

		// if(schedulerType.equals(ReservationInternalConstants.SchedulerType.MASTER_SCHEDULER)){
		sql.append("  AND (FS.EST_TIME_DEPARTURE_ZULU- NUMTODSINTERVAL(A.ANCI_NOTIFY_START_CUTOVER,'MINUTE')		\n ");
		sql.append("  		BETWEEN to_date('" + strDate + "', 'DD-MM-YYYY HH24:MI:SS') AND to_date('" + strDate
				+ "', 'DD-MM-YYYY HH24:MI:SS') + NUMTODSINTERVAL(" + scheluledJobHourDuration + ",'HOUR')								\n ");
		sql.append("  	OR		\n ");
		sql.append("  	FS.EST_TIME_DEPARTURE_ZULU- NUMTODSINTERVAL(A.ANCI_NOTIFY_END_CUTOVER,'MINUTE')				\n ");
		sql.append("  		BETWEEN to_date('" + strDate + "', 'DD-MM-YYYY HH24:MI:SS') AND to_date('" + strDate
				+ "', 'DD-MM-YYYY HH24:MI:SS') + NUMTODSINTERVAL(" + scheluledJobHourDuration + ",'HOUR')								\n ");
		sql.append("    OR		\n ");
		sql.append("    (to_date('"
				+ strDate
				+ "', 'DD-MM-YYYY HH24:MI:SS') BETWEEN FS.EST_TIME_DEPARTURE_ZULU- NUMTODSINTERVAL(A.ANCI_NOTIFY_START_CUTOVER,'MINUTE')			\n ");
		sql.append("    	AND FS.EST_TIME_DEPARTURE_ZULU- NUMTODSINTERVAL(A.ANCI_NOTIFY_END_CUTOVER,'MINUTE'))					\n ");
		sql.append("    ) 																											\n ");
		// }

		sql.append("  AND (F.STATUS ='ACT' OR F.STATUS ='CLS')  	 												\n ");
		sql.append("  AND A.STATUS                   ='ACT'															\n ");
		sql.append("  AND FS.SEGMENT_VALID_FLAG     ='Y'    														\n ");
		sql.append("  ORDER BY FS.EST_TIME_DEPARTURE_ZULU															\n ");

		query = sql.toString();

		log.debug("##############################################################");
		log.debug(" 		GET FLIGHT SEGMENTS LIST TO CREATE SUB JOBS 		 ");
		log.debug(" 		SQL To Execute 			: " + query);
		log.debug(" 		Scheduler Type 			: " + schedulerType);
		log.debug(" 		Param For Query			: " + strDate);
		log.debug("##############################################################");

		Object[] param = {};
		Collection<AncillaryReminderDetailDTO> flightSegmentsList = (Collection<AncillaryReminderDetailDTO>) jt.query(query,
				param, new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<AncillaryReminderDetailDTO> reaminderDetailFlightSegments = new ArrayList<AncillaryReminderDetailDTO>();
						AncillaryReminderDetailDTO reminderDetailDTO;

						if (rs != null) {
							while (rs.next()) {
								reminderDetailDTO = new AncillaryReminderDetailDTO();
								reminderDetailDTO.setFlightId(rs.getInt("FLIGHT_ID"));
								reminderDetailDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
								reminderDetailDTO.setDeparturetimeLocal(new Date(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL")
										.getTime()));
								reminderDetailDTO.setDepartureTimeZulu(new Date(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU")
										.getTime()));
								reminderDetailDTO.setFlightOrigin(rs.getString("ORIGIN"));
								reminderDetailDTO.setFlightSegId(rs.getInt("FLT_SEG_ID"));
								reminderDetailDTO.setSegmentCode(rs.getString("FLT_SEG_CODE"));
								reminderDetailDTO.setAnciNotificationStartCutOverTime(rs.getInt("ANCI_NOTIFY_START_CUTOVER"));
								reminderDetailDTO.setAnciNotificationEndCutOverTime(rs.getInt("ANCI_NOTIFY_END_CUTOVER"));
								reminderDetailDTO.setNotificationReminderTime(new Date(rs.getTimestamp("SND_NOTIFICATION_TIME")
										.getTime()));
								reminderDetailDTO.setFlightSegmentNotificationEventId(rs.getInt("FLTSEG_NOTI_ID"));
								reminderDetailDTO.setAirportOnlineCheckin(rs.getString("ONLINE_CHECKIN"));
								reaminderDetailFlightSegments.add(reminderDetailDTO);
							}
						}

						return reaminderDetailFlightSegments;
					}
				});

		return flightSegmentsList;
	}

	@Override
	public void updatePnrSegmentNotifyStatus(
			Collection<ReservationSegmentNotificationEvent> reservationSegmentNotificationEventCol) {

		log.debug("Inside updatePnrSegmentNotifyStatus");
		// ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
		ReservationSegmentNotificationEvent segmentNotificationEvent;
		ReservationSegmentNotificationEvent existingSegmentNotificationEvent;

		if (reservationSegmentNotificationEventCol != null && reservationSegmentNotificationEventCol.size() > 0) {
			for (ReservationSegmentNotificationEvent reservationSegmentNotificationEvent : reservationSegmentNotificationEventCol) {
				segmentNotificationEvent = reservationSegmentNotificationEvent;
				if (segmentNotificationEvent.getPnrSegmentId() == null
						&& segmentNotificationEvent.getNotifyStatus().equals(
								ReservationInternalConstants.PnrSegNotifyEvent.FAILED)) {
					return;
				}

				if (segmentNotificationEvent.getPnrSegmentMsgEventId() != null
						&& segmentNotificationEvent.getPnrSegmentMsgEventId().intValue() != 0) {

					existingSegmentNotificationEvent = (ReservationSegmentNotificationEvent) get(
							ReservationSegmentNotificationEvent.class, segmentNotificationEvent.getPnrSegmentMsgEventId());

					existingSegmentNotificationEvent.setNotifyStatus(segmentNotificationEvent.getNotifyStatus());
					existingSegmentNotificationEvent.setFailureReason(segmentNotificationEvent.getFailureReason());
					existingSegmentNotificationEvent.setTimeStamp(segmentNotificationEvent.getTimeStamp());

					segmentNotificationEvent = existingSegmentNotificationEvent;
				}
				ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO
						.updateStatusReservationSegmentNotifyEvent(segmentNotificationEvent);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<PnrFlightInfoDTO> getPNRListFromFlightSegment(Integer flightSegmentId) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "";
		log.debug("Inside getPNRListFromFlight()");

		/**
		 * PNRs list which are not notified yet
		 */
		/*
		 * String sql = " SELECT DISTINCT TPS.PNR    " + "  FROM T_FLIGHT_SEGMENT TFS,  " + "  		T_PNR_SEGMENT TPS,  " +
		 * "  		T_RESERVATION TR,  " + "		T_PNR_SEG_NOTIFICATION_EVENT PSNE " +
		 * "  WHERE TFS.FLT_SEG_ID =  TPS.FLT_SEG_ID   " + "  AND TR.PNR = TPS.PNR  " + "  AND TPS.STATUS = 'CNF'  " +
		 * "  AND TR.C_EMAIL IS NOT NULL  " + "  AND TPS.PNR_SEG_ID = PSNE.PNR_SEG_ID (+) " +
		 * "  AND (PSNE.PNR_SEG_ID IS NULL OR PSNE.NOTIFY_STATUS LIKE 'FAILED') " + "  AND TR.ORIGIN_CHANNEL_CODE <> " +
		 * SalesChannelsUtil.SALES_CHANNEL_LCC + "  AND TFS.FLT_SEG_ID = ?  ";
		 */

		sql = "	 SELECT DISTINCT PS.PNR, MIN(FS.EST_TIME_DEPARTURE_LOCAL ) as FIRSTDEPDATE		\n"
				+ "	from T_PNR_SEGMENT PS, 				\n" + "	T_FLIGHT_SEGMENT FS 				\n" + "	WHERE PS.STATUS = 'CNF' 			\n"
				+ "	AND PS.FLT_SEG_ID = FS.FLT_SEG_ID 	\n" + "	and PS.PNR IN (       				\n" + "		SELECT DISTINCT TPS.PNR 		\n"
				+ "	  		FROM T_FLIGHT_SEGMENT TFS, 	\n" + "	  		T_PNR_SEGMENT TPS,  		\n" + "	  		T_RESERVATION TR,   		\n"
				+ "	  		T_RESERVATION_CONTACT TRC,  \n" + "			T_PNR_SEG_NOTIFICATION_EVENT PSNE \n"
				+ "		WHERE TFS.FLT_SEG_ID =  TPS.FLT_SEG_ID    \n" + "		AND TR.PNR = TPS.PNR   \n"
				+ "		AND TR.PNR = TRC.PNR   \n"
				+ "		AND TPS.STATUS = 'CNF'  \n"
				+ "		AND TR.STATUS = 'CNF' \n"
				+ // Consider only confirmed reservations
				"		AND TRC.C_EMAIL IS NOT NULL  \n" + "	    AND TPS.PNR_SEG_ID = PSNE.PNR_SEG_ID (+)  \n"
				+ "	    AND (PSNE.PNR_SEG_ID IS NULL OR (PSNE.NOTIFY_STATUS LIKE 'FAILED' AND PSNE.NOTIFICATION_TYPE ='"
				+ ReservationInternalConstants.NotificationType.ANCI_REMINDER + " '))  \n"
				+ "	    AND TR.ORIGIN_CHANNEL_CODE <> 15 \n" + "	    AND TFS.FLT_SEG_ID = ?  ) \n" + "   GROUP BY PNR	\n";

		log.debug("######################################################");
		log.debug(" 		GET PNR LIST TO SEND REMINDER 			 ");
		log.debug(" 		SQL To Execute 			: " + sql);
		log.debug(" 		Parameter To Execute	: " + flightSegmentId);
		log.debug("######################################################");

		Object[] param = { flightSegmentId };

		Collection<PnrFlightInfoDTO> pnrList = (Collection<PnrFlightInfoDTO>) template.query(sql, param,
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<PnrFlightInfoDTO> pnrs = new ArrayList<PnrFlightInfoDTO>();
						PnrFlightInfoDTO pnrFlightInfoDTO;

						if (rs != null) {
							while (rs.next()) {
								pnrFlightInfoDTO = new PnrFlightInfoDTO();
								pnrFlightInfoDTO.setPnr(rs.getString("PNR"));
								pnrFlightInfoDTO.setFirstSegDepDate(new Date(rs.getTimestamp("FIRSTDEPDATE").getTime()));

								pnrs.add(pnrFlightInfoDTO);
							}
						}
						return pnrs;
					}
				});

		return pnrList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<AncillaryStatusDTO> getPnrAncillaryStatus(String pnr) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(dataSource);
		Collection<AncillaryStatusDTO> ancStatusCollection;

		log.debug(" **** Inside getPnrAncillaryStatus() ******");

		String sql = "	SELECT \n " + " R.PNR AS PNR, \n "
				+ " PS.PNR_SEG_ID AS SEGMENT_ID, \n "
				+ " PS.OND_GROUP_ID AS OND_GROUP_ID, \n"
				+ " FS.FLIGHT_ID AS FLIGHT_ID, \n "
				+ " FS.FLT_SEG_ID AS FLIGHT_SEG_ID, \n "
				+ " FS.SEGMENT_CODE AS SEGMENT, \n "
				+ " PS.GROUND_SEG_ID AS GROUND_SEGMENT, \n "
				+ " PS.STATUS AS SEG_STATUS, \n "
				+ " PS.RETURN_FLAG AS RETURN_FLAG, \n "
				+ " FS.EST_TIME_DEPARTURE_LOCAL AS DEPT_TIME_LOCAL, \n "
				+ " FS.EST_TIME_ARRIVAL_LOCAL  AS ARRIVAL_TIME_LOCAL, \n "
				+ " FS.EST_TIME_DEPARTURE_ZULU AS DEPT_TIME_ZULU, \n "
				+ " FS.EST_TIME_ARRIVAL_ZULU  AS ARRIVAL_TIME_ZULU, \n "
				+ " RC.C_EMAIL AS EMAIL, \n "
				+ " RC.PREFERRED_LANG AS PREFERRED_LANG, \n "
				+ " UPPER(RC.C_TITLE || ' ' || RC.C_FIRST_NAME || ' ' || RC.C_LAST_NAME) AS NAME, \n "
				+ " RC.C_LAST_NAME AS L_NAME, \n "
				+ " F.FLIGHT_NUMBER AS FLIGHT_NUMBER , \n "
				+ " PS.MEAL_SELECTION_STATUS AS MEAL_STATUS, \n "
				+
				// TODO change to BAGGAGE_SELECTION_STATUS
				" PS.MEAL_SELECTION_STATUS AS BAGGAGE_STATUS, \n " + " PS.SEAT_SELECTION_STATUS AS SEAT_STATUS, \n "
				+ " DECODE(PI.STATE, 'SC', 'Y', 'N') AS INS_STATUS, \n " + " PI.POLICY_CODE AS POLICY_CODE, \n "
				+ " psne.pnr_seg_notification_event_id AS PNR_NOTIFY_ID \n " + " FROM \n " + " 		T_PNR_SEGMENT PS, \n "
				+ " 		T_FLIGHT_SEGMENT FS, \n " + " 		T_FLIGHT F, \n " + " 		T_RESERVATION R, \n "
				+ " 		T_RESERVATION_CONTACT RC, \n " + " 		T_PNR_INSURANCE PI, \n "
				+ " 		T_PNR_SEG_NOTIFICATION_EVENT PSNE \n "
				+ " WHERE \n "
				+ " R.PNR = PS.PNR \n "
				+ " AND R.PNR = RC.PNR \n "
				+ " AND R.PNR = PI.PNR (+) \n "
				+ " AND PS.STATUS = 'CNF' \n "
				+ // Exclude canceled segments
				" AND R.STATUS = 'CNF' \n "
				+ // Include only confirmed bookings
				" AND PS.FLT_SEG_ID = FS.FLT_SEG_ID \n "
				+ " AND FS.FLIGHT_ID  = F.FLIGHT_ID \n "
				+ " AND FS.EST_TIME_DEPARTURE_ZULU > SYSDATE \n"
				+ // Include all future segments
				" AND PS.PNR_SEG_ID = PSNE.PNR_SEG_ID (+) \n "
				+ " AND (PSNE.PNR_SEG_ID IS NULL OR (PSNE.NOTIFY_STATUS LIKE 'FAILED' AND PSNE.NOTIFICATION_TYPE = '"
				+ ReservationInternalConstants.NotificationType.ANCI_REMINDER + "')) \n " + // Include only not sent or
																							// failed ones only
				" AND R.PNR = ? \n " + " ORDER BY PS.OND_GROUP_ID, FS.est_time_departure_zulu ";

		Object[] param = { pnr };

		log.debug("######################################################");
		log.debug(" 		GET PNR SEGMENT TO SEND REMINDER 			 ");
		log.debug(" 		SQL To Execute 			: " + sql);
		log.debug(" 		Parameter To Execute	: " + pnr);
		log.debug("######################################################");

		// GEt Seat & Meal Status & INSurance status & Baggage Status
		ancStatusCollection = (Collection<AncillaryStatusDTO>) template.query(sql, param, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AncillaryStatusDTO> ancReminderDTOList = new ArrayList<AncillaryStatusDTO>();
				AncillaryStatusDTO ancillaryReminderDTO;

				if (rs != null) {
					while (rs.next()) {
						ancillaryReminderDTO = new AncillaryStatusDTO();
						ancillaryReminderDTO.setPnr(rs.getString("PNR"));
						ancillaryReminderDTO.setSegmentId(rs.getInt("SEGMENT_ID"));
						ancillaryReminderDTO.setOndGroupId(rs.getString("OND_GROUP_ID"));
						ancillaryReminderDTO.setFlightSegmentId(rs.getInt("FLIGHT_SEG_ID"));
						ancillaryReminderDTO.setFlightId(rs.getInt("FLIGHT_ID"));
						ancillaryReminderDTO.setFlightStatus(rs.getString("SEG_STATUS"));
						ancillaryReminderDTO.setSegment(rs.getString("SEGMENT"));
						ancillaryReminderDTO.setGroundSegId(rs.getString("GROUND_SEGMENT"));
						ancillaryReminderDTO.setDepartureTime(rs.getTimestamp("DEPT_TIME_LOCAL"));
						ancillaryReminderDTO.setArrivalTime(rs.getTimestamp("ARRIVAL_TIME_LOCAL"));
						ancillaryReminderDTO.setDepartureTimeZulu(rs.getTimestamp("DEPT_TIME_ZULU"));
						ancillaryReminderDTO.setArrivalTimeZulu(rs.getTimestamp("ARRIVAL_TIME_ZULU"));
						ancillaryReminderDTO.setFlightNum(rs.getString("FLIGHT_NUMBER"));
						ancillaryReminderDTO.setName(rs.getString("NAME"));
						ancillaryReminderDTO.setLastName(rs.getString("L_NAME"));
						ancillaryReminderDTO.setEmail(rs.getString("EMAIL"));
						ancillaryReminderDTO.setSeatStatus(rs.getString("SEAT_STATUS"));
						ancillaryReminderDTO.setMealStatus(rs.getString("MEAL_STATUS"));
						ancillaryReminderDTO.setBaggageStatus(rs.getString("BAGGAGE_STATUS"));
						ancillaryReminderDTO.setInsuranceStatus(rs.getString("INS_STATUS"));
						ancillaryReminderDTO.setInsurancePolicyCode(rs.getString("POLICY_CODE"));
						ancillaryReminderDTO.setPnrSegNotifyId(rs.getInt("PNR_NOTIFY_ID"));
						ancillaryReminderDTO.setReturnFlag(rs.getString("RETURN_FLAG"));
						ancillaryReminderDTO.setIteneraryLanguage(rs.getString("PREFERRED_LANG"));
						ancReminderDTOList.add(ancillaryReminderDTO);
					}
				}

				return ancReminderDTOList;
			}
		});

		return ancStatusCollection;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<PassengerSeating> getReservationPaxSeatsList(String pnr) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = " SELECT SEAT.PPSAM_SEAT_ID as PPSAM_ID, " + " SEAT.PNR_PAX_ID AS PNR_PAX,  "
				+ " SEAT.PNR_SEG_ID AS PNR_SEG,  " + " SEAT.FLIGHT_AM_SEAT_ID as SEAT_ID, "
				+ " SEAT.CHARGE_AMOUNT as CHARGE_AMOUNT  " + " from SM_T_PNR_PAX_SEG_AM_SEAT SEAT,   " + " T_PNR_PASSENGER PPS "
				+ " where pps.pnr_pax_id = seat.pnr_pax_id    " + " and SEAT.status <> 'CNX'  " + " and pps.pnr = ? ";

		Object[] param = { pnr };

		// Retrieve list of SEATS against given reservation
		List<PassengerSeating> pnrSeatlist = (List<PassengerSeating>) jt.query(sql, param, new ResultSetExtractor() {
			@Override
			public List<PassengerSeating> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PassengerSeating> passengerSeatingList = new ArrayList<PassengerSeating>();

				if (rs != null) {
					while (rs.next()) {
						PassengerSeating passengerSeating = new PassengerSeating();
						passengerSeating.setPaxSeatingid(rs.getInt("PPSAM_ID"));
						passengerSeating.setPnrPaxId(rs.getInt("PNR_PAX"));
						passengerSeating.setPnrSegId(rs.getInt("PNR_SEG"));
						passengerSeating.setChargeAmount(rs.getBigDecimal("CHARGE_AMOUNT"));

						passengerSeatingList.add(passengerSeating);
					}
				}

				return passengerSeatingList;
			}
		});

		return pnrSeatlist;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<PassengerMeal> getReservationPaxMealsList(String pnr) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = " SELECT MEAL.PNR_PAX_SEG_MEAL_ID as SEGMEAL_ID," + " MEAL.PNR_PAX_ID as PAX_PNR,  "
				+ " MEAL.PNR_SEG_ID as PAX_SEG, " + " MEAL.AMOUNT as CHARGE_AMOUNT,  " + " MEAL.MEAL_ID as MEAL_ID     "
				+ " FROM ML_T_PNR_PAX_SEG_MEAL MEAL, T_PNR_PASSENGER PPS " + " WHERE pps.pnr_pax_id = meal.pnr_pax_id    "
				+ " and MEAL.status <> 'CNX'  " + " and pps.pnr =  ? ";

		Object[] param = { pnr };

		// Retrieve list of MEALs against given reservation
		List<PassengerMeal> pnrMeallist = (List<PassengerMeal>) jt.query(sql, param, new ResultSetExtractor() {
			@Override
			public List<PassengerMeal> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PassengerMeal> passengerMealList = new ArrayList<PassengerMeal>();

				if (rs != null) {
					while (rs.next()) {
						PassengerMeal passengerMeal = new PassengerMeal();
						passengerMeal.setPaxMealId(rs.getInt("SEGMEAL_ID"));
						passengerMeal.setPnrPaxId(rs.getInt("PAX_PNR"));
						passengerMeal.setPnrSegId(rs.getInt("PAX_SEG"));
						passengerMeal.setChargeAmount(rs.getBigDecimal("CHARGE_AMOUNT"));
						passengerMeal.setMealId(rs.getInt("MEAL_ID"));

						passengerMealList.add(passengerMeal);
					}
				}

				return passengerMealList;
			}
		});

		return pnrMeallist;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<PassengerBaggage> getReservationPaxBaggagesList(String pnr) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = " SELECT BAGGAGE.PNR_PAX_SEG_BAGGAGE_ID as SEGBAGGAGE_ID," + " BAGGAGE.PNR_PAX_ID as PAX_PNR,  "
				+ " BAGGAGE.PNR_SEG_ID as PAX_SEG, " + " BAGGAGE.AMOUNT as CHARGE_AMOUNT,  "
				+ " BAGGAGE.BAGGAGE_ID as BAGGAGE_ID     " + " FROM BG_T_PNR_PAX_SEG_BAGGAGE BAGGAGE, T_PNR_PASSENGER PPS "
				+ " WHERE PPS.PNR_PAX_ID = BAGGAGE.PNR_PAX_ID    " + " and BAGGAGE.STATUS <> 'CNX'  " + " and PPS.PNR =  ? ";

		Object[] param = { pnr };

		// Retrieve list of BAGGAGEs against given reservation
		List<PassengerBaggage> pnrBaggageList = (List<PassengerBaggage>) jt.query(sql, param, new ResultSetExtractor() {
			@Override
			public List<PassengerBaggage> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PassengerBaggage> passengerBaggageList = new ArrayList<PassengerBaggage>();

				if (rs != null) {
					while (rs.next()) {
						PassengerBaggage passengerBaggage = new PassengerBaggage();
						passengerBaggage.setPaxBaggageId(rs.getInt("SEGBAGGAGE_ID"));
						passengerBaggage.setPnrPaxId(rs.getInt("PAX_PNR"));
						passengerBaggage.setPnrSegmentId(rs.getInt("PAX_SEG"));
						passengerBaggage.setChargeAmount(rs.getBigDecimal("CHARGE_AMOUNT"));
						passengerBaggage.setBaggageId(rs.getInt("BAGGAGE_ID"));

						passengerBaggageList.add(passengerBaggage);
					}
				}

				return passengerBaggageList;
			}
		});

		return pnrBaggageList;
	}

	@Override
	public NotificationDetailInfoDTO getPnrSegListToSendReminder(List<AncillaryStatusDTO> pnrAnciList) throws ModuleException {
		NotificationDetailInfoDTO detailInfoDTO = new NotificationDetailInfoDTO();
		long[] dayDiffArr = new long[2];
		Long daysRemainingInflights;
		Long hourRemainingInflights;
		String pnr = "";
		// String ondCodes = "";

		// LCCClientPnrModesDTO pnrModesDTO;
		// Reservation reservation;

		if (pnrAnciList.size() > 0) {
			pnr = pnrAnciList.get(0).getPnr();
			detailInfoDTO.setPnr(pnr);
			detailInfoDTO.setEmail(pnrAnciList.get(0).getEmail());
			detailInfoDTO.setName(pnrAnciList.get(0).getName());
			detailInfoDTO.setLastName(pnrAnciList.get(0).getLastName());
			detailInfoDTO.setInsurancePolicyNum(pnrAnciList.get(0).getInsurancePolicyCode());
			detailInfoDTO.setInsuranceStatus(pnrAnciList.get(0).getInsuranceStatus());
			detailInfoDTO.setIteneraryLanguage(pnrAnciList.get(0).getIteneraryLanguage());
			// detailInfoDTO.setOrigin(pnrAnciList.get(0).getSegment().substring(0,
			// pnrAnciList.get(0).getSegment().indexOf('/')));
			// detailInfoDTO.setDestination(pnrAnciList.get(0).getSegment().substring(pnrAnciList.get(0).getSegment().lastIndexOf("/")
			// + 1, pnrAnciList.get(0).getSegment().length()));
			AncillaryReminderBL.setSearAndInsuranceAmountToDisplay(detailInfoDTO);
		}

		String mealStatus;
		String baggageStatus;
		String seatStatus;
		String insuStatus;
		int countFulAnciSegment = 0;
		// Get all origin and destination airports of all segments(OND wise) to check bus availability to those airports
		List<String> originList = new ArrayList<String>();
		List<String> destinationList = new ArrayList<String>();
		String origin = "";
		String destination = "";
		String ondGroupId = null;
		boolean isBusExist = false;
		boolean isBusSegment = false;
		boolean isOndSet = false;

		for (AncillaryStatusDTO ancillaryStatusDTO : pnrAnciList) {

			seatStatus = ancillaryStatusDTO.getSeatStatus();
			mealStatus = ancillaryStatusDTO.getMealStatus();
			baggageStatus = ancillaryStatusDTO.getBaggageStatus();
			insuStatus = ancillaryStatusDTO.getInsuranceStatus();
			isBusSegment = isBusSegment(ancillaryStatusDTO.getSegment());
			if (mealStatus.equals(ReservationInternalConstants.SegmentAncillaryStatus.ALL)
					&& seatStatus.equals(ReservationInternalConstants.SegmentAncillaryStatus.ALL)
					&& baggageStatus.equals(ReservationInternalConstants.SegmentAncillaryStatus.ALL) && insuStatus.equals("Y")
					|| isBusSegment) {
				countFulAnciSegment++;
			} else {
				if (!isOndSet) {
					isOndSet = true;
					detailInfoDTO.setOrigin(ancillaryStatusDTO.getSegment().substring(0,
							ancillaryStatusDTO.getSegment().indexOf('/')));
					detailInfoDTO.setDestination(ancillaryStatusDTO.getSegment().substring(
							ancillaryStatusDTO.getSegment().lastIndexOf("/") + 1, ancillaryStatusDTO.getSegment().length()));
					detailInfoDTO.setFlightDepartureTimeLocal(ancillaryStatusDTO.getDepartureTime());
					detailInfoDTO.setFlightDepartureTimeZulu(ancillaryStatusDTO.getDepartureTimeZulu());

					dayDiffArr = AncillaryReminderBL.deprtureRemainingDays(detailInfoDTO.getFlightDepartureTimeZulu(),
							CalendarUtil.getCurrentSystemTimeInZulu());
					daysRemainingInflights = dayDiffArr[0];
					hourRemainingInflights = dayDiffArr[1];

					detailInfoDTO.setDaysRemainingInflights(daysRemainingInflights.intValue());
					detailInfoDTO.setHourRemainingInflights(hourRemainingInflights.intValue());
				}
			}

			FlightSegReminderNotificationDTO flightsegNotifyDTO = new FlightSegReminderNotificationDTO();
			flightsegNotifyDTO.setFlightId(ancillaryStatusDTO.getFlightId());
			flightsegNotifyDTO.setFlightNum(ancillaryStatusDTO.getFlightNum());
			flightsegNotifyDTO.setFlightSegmentId(ancillaryStatusDTO.getFlightSegmentId());
			flightsegNotifyDTO.setFlightStatus(ancillaryStatusDTO.getFlightStatus());
			flightsegNotifyDTO.setSegmentCode(ancillaryStatusDTO.getSegment());
			flightsegNotifyDTO.setSegmentId(ancillaryStatusDTO.getSegmentId());
			flightsegNotifyDTO.setDepartureDateTime(ancillaryStatusDTO.getDepartureTime());
			flightsegNotifyDTO.setArrivalDateTime(ancillaryStatusDTO.getArrivalTime());
			flightsegNotifyDTO.setDepartureDateTimeZulu(ancillaryStatusDTO.getDepartureTimeZulu());
			flightsegNotifyDTO.setArrivalDateTimeZulu(ancillaryStatusDTO.getArrivalTimeZulu());
			flightsegNotifyDTO.setFromAirport(ancillaryStatusDTO.getSegment().substring(0,
					ancillaryStatusDTO.getSegment().indexOf('/')));
			flightsegNotifyDTO.setToAirport(ancillaryStatusDTO.getSegment().substring(
					ancillaryStatusDTO.getSegment().lastIndexOf("/") + 1, ancillaryStatusDTO.getSegment().length()));
			// Skip seat,baggage and meal reminders for bus segments
			if (isBusSegment) {
				flightsegNotifyDTO.setSeatSelectionStatus(ReservationInternalConstants.SegmentAncillaryStatus.ALL);
				flightsegNotifyDTO.setMealSelectionStatus(ReservationInternalConstants.SegmentAncillaryStatus.ALL);
				flightsegNotifyDTO.setBaggageSelectionStatus(ReservationInternalConstants.SegmentAncillaryStatus.ALL);
			} else {
				flightsegNotifyDTO.setSeatSelectionStatus(ancillaryStatusDTO.getSeatStatus());
				flightsegNotifyDTO.setMealSelectionStatus(ancillaryStatusDTO.getMealStatus());
				flightsegNotifyDTO.setBaggageSelectionStatus(ancillaryStatusDTO.getBaggageStatus());
			}
			flightsegNotifyDTO.setReturnFlag(ancillaryStatusDTO.getReturnFlag());
			flightsegNotifyDTO.setPnrSegNotifyId(ancillaryStatusDTO.getPnrSegNotifyId());
			detailInfoDTO.getFlightSegmentsList().add(flightsegNotifyDTO);

			if (AppSysParamsUtil.isGroundServiceEnabled()) {
				if (ondGroupId != null && !ondGroupId.equals(ancillaryStatusDTO.getOndGroupId())) {
					originList.add(origin);
					destinationList.add(destination);

					origin = ancillaryStatusDTO.getSegment().substring(0, 3);
					destination = ancillaryStatusDTO.getSegment().substring(4);
				} else {
					if (ondGroupId != null && ondGroupId.equals(ancillaryStatusDTO.getOndGroupId())) {
						if (destination.equals(ancillaryStatusDTO.getSegment().substring(0, 3))) {
							destination = ancillaryStatusDTO.getSegment().substring(4);
						}
					} else {
						origin = ancillaryStatusDTO.getSegment().substring(0, 3);
						destination = ancillaryStatusDTO.getSegment().substring(4);
					}
				}
				ondGroupId = ancillaryStatusDTO.getOndGroupId();
			}
		}
		originList.add(origin);
		destinationList.add(destination);

		detailInfoDTO.setBusReminderReq("N");

		if (AppSysParamsUtil.isGroundServiceEnabled()) {
			for (AncillaryStatusDTO anciSegment : pnrAnciList) {
				if (anciSegment.getGroundSegId() != null && !anciSegment.getGroundSegId().trim().equals("")) {
					isBusExist = true;
					break;
				}
			}
			if (isBusExist) {
				detailInfoDTO.setBusReminderReq("N");
			} else {
				Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();
				for (String originAirport : originList) {
					if (busConnectingAirports.contains(originAirport)) {
						detailInfoDTO.setBusReminderReq("Y");
						detailInfoDTO.setIsBusAtDeparture("Y");
						detailInfoDTO.setBusConnectingAirport(originAirport);
					}
				}
				for (String destinationAirport : destinationList) {
					if (busConnectingAirports.contains(destinationAirport)) {
						detailInfoDTO.setBusReminderReq("Y");
						detailInfoDTO.setIsBusAtDeparture("N");
						detailInfoDTO.setBusConnectingAirport(destinationAirport);
					}
				}
			}
		}

		if (countFulAnciSegment == pnrAnciList.size() && detailInfoDTO.getBusReminderReq().equals("N")) {
			detailInfoDTO.setReminderRequired(false);
		}

		return detailInfoDTO;
	}

	private boolean isBusSegment(String segmentCode) {
		if (segmentCode != null && !segmentCode.trim().equals("")) {
			Map<String, String> groundStations = CommonsServices.getGlobalConfig().getGroundStationNamesMap();
			if (groundStations.containsKey(segmentCode.substring(0, 3)) || groundStations.containsKey(segmentCode.substring(4))) {
				return true;
			}
		}
		return false;
	}

	@Override
	/**
	 * Returns the e Ticket Sequence Number
	 * 
	 * @return
	 */
	public String getCurrentETicketSQID(String strSeq) {
		log.debug("Inside getNextETicketSQID");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = null;

		if (strSeq != null) {
			strSeq.trim();
			sql = " SELECT " + strSeq + ".CURRVAL ETICKET FROM DUAL ";
		} else {
			sql = " select LAST_NUMBER from all_sequences where sequence_name='S_RESERVATION_ETICKET' ";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String eTicketNo = (String) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String eTicketNo = "";

				if (rs != null) {
					if (rs.next()) {
						eTicketNo = BeanUtils.nullHandler(rs.getString("LAST_NUMBER"));
					}
				}

				return eTicketNo;
			}
		});

		log.debug("Exit getNextETicketSQID");
		return eTicketNo;
	}

	@Override
	public String getLastActualEticketForAgent(String agentCode) {
		log.debug("Inside getNextETicketSQID");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sb = new StringBuilder(
				"select * from( select pa.ETICKET_NUMBER from t_pnr_passenger p,T_PNR_PAX_ADDITIONAL_INFO pa,t_reservation res where p.pnr = res.pnr AND p.PNR_PAX_ID=pa.PNR_PAX_ID AND res.ORIGIN_AGENT_CODE = '");
		sb.append(agentCode);
		sb.append("' and pa.ETICKET_NUMBER is not null ORDER BY BOOKING_TIMESTAMP desc ) where rownum < 2");

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sb.toString());
		log.debug("############################################");

		String eTicketNo = (String) jt.query(sb.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String eTicketNo = "";

				if (rs != null) {
					if (rs.next()) {
						eTicketNo = BeanUtils.nullHandler(rs.getString("ETICKET_NUMBER"));
					}
				}
				return eTicketNo;
			}
		});

		log.debug("Exit getNextETicketSQID");
		return eTicketNo;
	}
	
	@Override
	public Collection<ReservationInsurance> getReservationInsuranceList(String pnr) {

		String hql = "select resIns from ReservationInsurance resIns where resIns.state in (?, ?, ?) and resIns.pnr = ?";
		Object[] param = { ReservationInternalConstants.INSURANCE_STATES.SC.toString(),
				ReservationInternalConstants.INSURANCE_STATES.OH.toString(),
				ReservationInternalConstants.INSURANCE_STATES.FL.toString(), pnr };
		return find(hql, param, ReservationInsurance.class);
		

	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<RakInsuredTraveler> getTravellersForRakInsurancePublishing(int flightSegmentId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT R.PNR, PP.PAX_SEQUENCE, PP.TITLE, PP.FIRST_NAME, PP.LAST_NAME, PP.DATE_OF_BIRTH,"
				+ " PI.ROUND_TRIP, PI.DATE_OF_RETURN, PI.DESTINATION, PI.QUOTED_AMOUNT, PI.TOT_PAX_COUNT "
				+ " FROM T_RESERVATION R, T_PNR_PASSENGER PP, T_PNR_SEGMENT PS, T_PNR_INSURANCE PI, T_FLIGHT F, T_FLIGHT_SEGMENT FS"
				+ " WHERE R.PNR = PP.PNR" + " AND R.PNR = PS.PNR" + " AND R.PNR = PI.PNR" + " AND PS.FLT_SEG_ID = FS.FLT_SEG_ID"
				+ " AND FS.FLIGHT_ID = F.FLIGHT_ID" + " AND PP.PAX_TYPE_CODE <> 'IN'" + " AND PI.STATE = 'SC'"
				+ " AND PI.ORIGIN = F.ORIGIN" + " AND PI.DATE_OF_TRAVEL = FS.EST_TIME_DEPARTURE_LOCAL" + " AND R.STATUS = 'CNF'"
				+ " AND PS.STATUS = 'CNF'" + " AND FS.FLT_SEG_ID = " + flightSegmentId;

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");
		}
		Collection<RakInsuredTraveler> rakTravellers = (Collection<RakInsuredTraveler>) jt.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<RakInsuredTraveler> travellers = new ArrayList<RakInsuredTraveler>();
				RakInsuredTraveler traveller = null;

				if (rs != null) {
					while (rs.next()) {
						traveller = new RakInsuredTraveler();
						traveller.setBookingNo(rs.getString("PNR"));
						traveller.setSequenceNo(rs.getString("PAX_SEQUENCE"));
						traveller.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
						traveller.setFirstName(rs.getString("FIRST_NAME"));
						traveller.setLastName(rs.getString("LAST_NAME"));

						if (rs.getDate("DATE_OF_BIRTH") != null) {
							traveller.setDob(rs.getDate("DATE_OF_BIRTH"));
						} else {
							traveller.setDob(new Date(0));
						}

						traveller.setDestinationAirport(rs.getString("DESTINATION"));
						traveller.setOneWay(rs.getString("ROUND_TRIP").equals("Y") ? false : true);
						if (rs.getDate("DATE_OF_RETURN") != null) {
							traveller.setReturnDate(rs.getDate("DATE_OF_RETURN"));
						}

						traveller.setPremium(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.divide(
								rs.getBigDecimal("QUOTED_AMOUNT"), rs.getInt("TOT_PAX_COUNT"))));
						travellers.add(traveller);
					}
				}
				return travellers;
			}
		});

		return rakTravellers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<RakInsuredTraveler> getTravellersForDailyRakInsurancePublishing(Date bookingDate) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		String date = sdf.format(bookingDate);
		String sql = " SELECT R.PNR, PP.PAX_SEQUENCE, PP.TITLE, PP.FIRST_NAME, PP.LAST_NAME, PP.DATE_OF_BIRTH,"
				+ " PI.ROUND_TRIP, PI.DATE_OF_RETURN, PI.DESTINATION, PI.QUOTED_AMOUNT, PI.TOT_PAX_COUNT "
				+ " FROM T_RESERVATION R, T_PNR_PASSENGER PP, T_PNR_SEGMENT PS, T_PNR_INSURANCE PI, T_FLIGHT F, T_FLIGHT_SEGMENT FS"
				+ " WHERE R.PNR = PP.PNR" + " AND R.PNR = PS.PNR" + " AND R.PNR = PI.PNR" + " AND PS.FLT_SEG_ID = FS.FLT_SEG_ID"
				+ " AND FS.FLIGHT_ID = F.FLIGHT_ID" + " AND PP.PAX_TYPE_CODE <> 'IN'" + " AND PI.STATE = 'SC'"
				+ " AND PI.ORIGIN = F.ORIGIN" + " AND PI.DATE_OF_TRAVEL = FS.EST_TIME_DEPARTURE_LOCAL" + " AND R.STATUS = 'CNF'"
				+ " AND PS.STATUS = 'CNF'" + " AND r1.booking_timestamp " + " BETWEEN TO_DATE ('" + date + " 00:00:00', "
				+ " 'DD-MON-YYYY HH24:mi:ss') " + " AND TO_DATE ('" + date + " 23:59:59', " + " 'DD-MON-YYYY HH24:mi:ss')) ";

		if (log.isDebugEnabled()) {
			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");
		}
		Collection<RakInsuredTraveler> rakTravellers = (Collection<RakInsuredTraveler>) jt.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<RakInsuredTraveler> travellers = new ArrayList<RakInsuredTraveler>();
				RakInsuredTraveler traveller = null;

				if (rs != null) {
					while (rs.next()) {
						traveller = new RakInsuredTraveler();
						traveller.setBookingNo(rs.getString("PNR"));
						traveller.setSequenceNo(rs.getString("PAX_SEQUENCE"));
						traveller.setTitle(BeanUtils.nullHandler(rs.getString("TITLE")));
						traveller.setFirstName(rs.getString("FIRST_NAME"));
						traveller.setLastName(rs.getString("LAST_NAME"));

						if (rs.getDate("DATE_OF_BIRTH") != null) {
							traveller.setDob(rs.getDate("DATE_OF_BIRTH"));
						} else {
							traveller.setDob(new Date(0));
						}

						traveller.setDestinationAirport(rs.getString("DESTINATION"));
						traveller.setOneWay(rs.getString("ROUND_TRIP").equals("Y") ? false : true);
						if (rs.getDate("DATE_OF_RETURN") != null) {
							traveller.setReturnDate(rs.getDate("DATE_OF_RETURN"));
						}

						traveller.setPremium(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.divide(
								rs.getBigDecimal("QUOTED_AMOUNT"), rs.getInt("TOT_PAX_COUNT"))));
						travellers.add(traveller);
					}
				}
				return travellers;
			}
		});

		return rakTravellers;
	}

	@Override
	public void updateTnxGranularityFlag(String pnr, String status, String message, Long version) {
		String sql;
		if (version == null) {
			sql = " update T_RESERVATION set TXN_GRANULARITY_STATUS = '" + status + "', version = (version + 1) "
					+ ", TXN_GRANULARITY_REMARKS='" + message + "' where pnr = '" + pnr + "' ";
		} else {
			sql = " update T_RESERVATION set TXN_GRANULARITY_STATUS = '" + status + "', version = " + (version + 1)
					+ ", TXN_GRANULARITY_REMARKS='" + message + "' where pnr = '" + pnr + "' ";
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		jt.update(sql);
	}

	@Override
	public void updateReservationInsurancePublishStatus(Collection<String> pnrList, int status) {
		String sql;
		// TODO generate strPnrList from pnrList
		String strPnrList = "4343434";
		sql = " update T_PNR_INSURANCE set PUBLISH_STATUS = '" + status + "'" + " where pnr in (" + strPnrList + ") ";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		jt.update(sql);
	}

	@Override
	public void updatePnrsWithTnxGranularityFlag(Collection<String> pnrs, String status) {
		String condition = " PNR IN (" + BeanUtils.constructINString(pnrs) + ")";
		String sql = " update T_RESERVATION SET TXN_GRANULARITY_STATUS = '" + status + "' WHERE " + condition;
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		jt.update(sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getPnrsOfAlreadyTnxGranularityTracked() {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = "SELECT DISTINCT PNR FROM (SELECT C.PNR FROM T_PAX_TXN_OND_BREAKDOWN_SUMMRY A, T_PNR_PASSENGER B, T_RESERVATION C "
				+ "WHERE A.PNR_PAX_ID = B.PNR_PAX_ID AND B.PNR = C.PNR ORDER BY C.BOOKING_TIMESTAMP)";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Collection<String> colPnrs = (Collection<String>) jt.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> colPnrs = new HashSet<String>();

				if (rs != null) {
					while (rs.next()) {
						colPnrs.add(BeanUtils.nullHandler(rs.getString("PNR")));
					}
				}
				return colPnrs;
			}
		});

		return colPnrs;
	}

	public String getReservationPaymentMethod(String paxIds) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = " SELECT DISTINCT pt.payment_mode_id as PAYNEBT_MODE_ID from t_pax_transaction pt "
				+ " where pt.pnr_pax_id in ( " + paxIds + " )";

		Object[] param = {};

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql.toString());
		log.debug("############################################");

		String pnrPaxPaymentId = (String) jt.query(sql, param, new ResultSetExtractor() {
			@Override
			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				String paymentId = "";

				if (rs != null) {
					if (rs.next()) {
						paymentId = BeanUtils.nullHandler(rs.getString("PAYNEBT_MODE_ID"));
					}
				}

				return paymentId;
			}
		});

		return pnrPaxPaymentId;
	}

	@Override
	public void updateReleaseTimeForDummyReservation(String pnr, Date releaseTimeStamp) {
		Object[] args = { releaseTimeStamp, pnr };
		String sql = "UPDATE T_PNR_PASSENGER TPP SET TPP.RELEASE_TIMESTAMP = ? WHERE TPP.PNR = ?";
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		jt.update(sql, args);
	}

	@Override
	public int getOnholdReservationCountByIp(String ipAddress, int salesChannel) {
		Object[] args = { ipAddress, ReservationInternalConstants.ReservationStatus.ON_HOLD, salesChannel,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO) };
		String sql = "SELECT count(pnr) FROM t_reservation r WHERE r.origin_ip=? AND r.status=? AND "
				+ "r.owner_channel_code=? AND r.dummy_booking=?";
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return jt.queryForObject(sql, args, Integer.class);
	}

	@Override
	public int getOnholdReservationCountByFlight(int segId, int salesChannel) {
		Object[] args = { segId, ReservationInternalConstants.ReservationStatus.ON_HOLD, salesChannel,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		String sql = "SELECT count(r.pnr) FROM t_reservation r, t_pnr_segment s, t_flight_segment fs "
				+ "WHERE r.pnr= s.pnr AND s.flt_seg_id=fs.flt_seg_id AND fs.flt_seg_id=? "
				+ "AND r.status=? AND r.owner_channel_code=? AND r.dummy_booking=?";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return jt.queryForObject(sql, args, Integer.class);
	}

	@Override
	public int getOnholdReservationCountByPaxName(ReservationPax reservationPax, int salesChannel) {
		Object[] args = { reservationPax.getTitle(), reservationPax.getFirstName().toUpperCase(),
				reservationPax.getLastName().toUpperCase(), ReservationInternalConstants.ReservationStatus.ON_HOLD, salesChannel,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		String sql = "SELECT count(r.pnr) FROM t_reservation r, t_pnr_passenger p "
				+ "WHERE r.pnr=p.pnr AND p.title=? AND p.upper_first_name=? AND p.upper_last_name=? AND r.status=? AND "
				+ "r.owner_channel_code=? AND r.dummy_booking=?";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return jt.queryForObject(sql, args, Integer.class);

	}

	@Override
	public int getOnholdReservationCountByPaxEmail(String email, int salesChannel) {
		
		if (email != null) {
			email = email.toUpperCase();
		}
			
			Object[] args = { email, ReservationInternalConstants.ReservationStatus.ON_HOLD, salesChannel,
					String.valueOf(ReservationInternalConstants.DummyBooking.NO) };
		
		
		String sql = "SELECT count(r.pnr) FROM t_reservation r, t_reservation_contact c "
				+ "WHERE r.pnr=c.pnr AND upper(c.c_email)=? AND r.status=? AND r.owner_channel_code=? AND r.dummy_booking=?";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return jt.queryForObject(sql, args, Integer.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<OnHoldReleaseTimeDTO> getOnHoldReleaseTime(OnHoldReleaseTimeDTO onHoldReleaseTimeDTO) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Collection<OnHoldReleaseTimeDTO> holdReleaseTimeDTOs = null;
		String ondCode = onHoldReleaseTimeDTO.getOndCode();
		String[] onds = ondCode.split("/");
		String depatureAirportCode = null;
		String agentCode = onHoldReleaseTimeDTO.getAgentCode();
		String cabinClass = onHoldReleaseTimeDTO.getCabinClass();
		String isDomestic = "N";
		int fareRuleId = onHoldReleaseTimeDTO.getFareRuleId();
		if (onHoldReleaseTimeDTO.isDomestic()) {
			isDomestic = "Y";
		}
		if (onds.length > 1) {
			depatureAirportCode = onds[0];
		}
		String departureAll = depatureAirportCode + "/**";
		MODULE module = MODULE.ANY;
		String moduleCode = onHoldReleaseTimeDTO.getModuleCode();
		if (moduleCode != null) {
			if (AppIndicatorEnum.APP_XBE.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.XBE;
			} else if (AppIndicatorEnum.APP_IBE.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.IBE;
			} else if (AppIndicatorEnum.APP_WS.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.API;
			} else if (AppIndicatorEnum.APP_ANDROID.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.ANDROID;
			} else if (AppIndicatorEnum.APP_IOS.toString().equalsIgnoreCase(moduleCode)) {
				module = MODULE.IOS;
			}
		}

		String bookingClass = onHoldReleaseTimeDTO.getBookingClass();
		if (bookingClass == null) {
			bookingClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
		}

		if (agentCode == null) {
			agentCode = OnHoldReleaseTimeDTO.ONHOLD_ANY;
		}

		if (cabinClass == null) {
			cabinClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
		}

		Object[] args = {
				agentCode,
				OnHoldReleaseTimeDTO.ONHOLD_ANY,
				cabinClass,
				OnHoldReleaseTimeDTO.ONHOLD_ANY,
				ondCode,
				departureAll,
				OnHoldReleaseTimeDTO.ONHOLD_ANY,
				bookingClass,
				OnHoldReleaseTimeDTO.ONHOLD_ANY,
				module.toString(),
				OnHoldReleaseTimeDTO.ONHOLD_ANY,
				(onHoldReleaseTimeDTO.getFlightDepartureDate().getTime() - onHoldReleaseTimeDTO.getBookingDate().getTime())
						/ (1000 * 60), isDomestic, fareRuleId };
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ort.AGENT_CODE, ort.CABIN_CLASS_CODE, ort.OND_CODE,ort.BOOKING_CODE,ort.MODULE_CODE, ");
		sql.append(" ort.RELEASE_TIME, ort.REL_TIME_WRT, ort.RANKING, ort.FARE_RULE_ID FROM T_ONHOLD_RELEASE_TIME ort ");
		sql.append(" WHERE (ort.AGENT_CODE =?  OR  ort.AGENT_CODE =?) AND (ort.CABIN_CLASS_CODE =?  OR  ort.CABIN_CLASS_CODE =?) ");
		sql.append(" AND (ort.OND_CODE =?  OR  ort.OND_CODE =? OR ort.OND_CODE =?) AND (ort.BOOKING_CODE = ? OR ort.BOOKING_CODE = ?) ");
		sql.append(" AND (ort.MODULE_CODE = ? OR ort.MODULE_CODE = ?)  AND ? BETWEEN END_CUTOVER AND START_CUTOVER AND ort.IS_DOMESTIC=? ");
		sql.append(" AND (ort.FARE_RULE_ID = ? OR ort.FARE_RULE_ID IS NULL) order by ort.FARE_RULE_ID ");

		holdReleaseTimeDTOs = (Collection<OnHoldReleaseTimeDTO>) jt.query(sql.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<OnHoldReleaseTimeDTO> holdReleaseTimeDTOs = new ArrayList<OnHoldReleaseTimeDTO>();

				if (rs != null) {
					OnHoldReleaseTimeDTO holdReleaseTimeDTO = null;
					String releaseTimeWRT = null;
					while (rs.next()) {
						holdReleaseTimeDTO = new OnHoldReleaseTimeDTO();
						holdReleaseTimeDTO.setAgentCode(rs.getString("AGENT_CODE"));
						holdReleaseTimeDTO.setCabinClass(rs.getString("CABIN_CLASS_CODE"));
						holdReleaseTimeDTO.setOndCode(rs.getString("OND_CODE"));
						holdReleaseTimeDTO.setBookingClass(rs.getString("BOOKING_CODE"));
						holdReleaseTimeDTO.setModuleCode(rs.getString("MODULE_CODE"));
						holdReleaseTimeDTO.setReleaseTime(rs.getLong("RELEASE_TIME"));
						holdReleaseTimeDTO.setRank(rs.getInt("RANKING"));
						releaseTimeWRT = rs.getString("REL_TIME_WRT");
						if ("BKG_DATE".equals(releaseTimeWRT)) {
							holdReleaseTimeDTO.setReleaseTimeFromBookingDate(true);
						}
						holdReleaseTimeDTO.setFareRuleId(rs.getInt("FARE_RULE_ID"));
						holdReleaseTimeDTOs.add(holdReleaseTimeDTO);
					}
				}

				return holdReleaseTimeDTOs;
			}
		});

		return holdReleaseTimeDTOs;
	}

	@Override
	public boolean isReservationTnxGranularityEnable(String pnr) {
		if (log.isDebugEnabled()) {
			log.debug("Inside isReservationTnxGranularityEnable");
		}
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = " SELECT r.ENABLE_TRANSACTION_GRANULARITY FROM T_RESERVATION r WHERE r.PNR = '" + pnr + "'";

		if (log.isDebugEnabled()) {
			log.debug(" SQL to be executed for isReservationTnxGranularityEnable :" + sql);
		}

		boolean tnxGranularityEnable = (Boolean) jt.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				boolean tnxGranularityEnable = false;
				if (rs != null) {
					while (rs.next()) {
						tnxGranularityEnable = BeanUtils.nullHandler(rs.getString("ENABLE_TRANSACTION_GRANULARITY")).equals("Y")
								? true
								: false;
					}
				}
				return tnxGranularityEnable;
			}
		});
		return tnxGranularityEnable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Integer> getUnflownReservationSegmentIds(String pnr, Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String strDate = dateFormat.format(date);

		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		boolean isRequoteEnabled = AppSysParamsUtil.isRequoteEnabled();
		boolean isFlownFromEticket = (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET) ? true : false;

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT ps.pnr_seg_id as pnrSegId ");
		sqlBuilder.append(" FROM t_flight_segment fs, ");
		sqlBuilder.append(" t_pnr_segment ps ");
		sqlBuilder.append(" WHERE ps.flt_seg_id = fs.flt_seg_id ");
		sqlBuilder.append(" AND ps.pnr            ='" + pnr + "' ");
		sqlBuilder.append(" AND ps.status            IN('" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
				+ "','" + ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING + "')");
		// FIXME - Flown should be taken from app parameter
		if (isRequoteEnabled) {
			sqlBuilder.append(" AND PS.PNR_SEG_ID   NOT IN (SELECT PPFS.PNR_SEG_ID ");
			sqlBuilder.append(" FROM  T_RESERVATION R, T_PNR_PASSENGER PP, T_PNR_PAX_FARE PPF , T_PNR_PAX_FARE_SEGMENT PPFS  ");

			if (isFlownFromEticket || AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
				sqlBuilder.append(" , T_PNR_PAX_FARE_SEG_E_TICKET PPFSE ");
			}

			sqlBuilder.append(" WHERE R.PNR = '" + pnr + "'  ");
			sqlBuilder.append(" AND PP.PNR = R.PNR AND PPF.PNR_PAX_ID = PP.PNR_PAX_ID  ");

			if (isFlownFromEticket) {
				sqlBuilder.append(" AND PPFSE.PPFS_ID = PPFS.PPFS_ID  AND PPFS.PPF_ID = PPF.PPF_ID ");
				sqlBuilder.append(" AND PPFSE.STATUS IN ('" + ReservationInternalConstants.PaxFareSegmentTypes.FLOWN + "','"
						+ ReservationInternalConstants.PaxFareSegmentTypes.NO_REC + "'");
				if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()){
					sqlBuilder.append(", '" + EticketStatus.CHECKEDIN.code()+ "','" + EticketStatus.BOARDED.code()+ "'"  );
				}
				sqlBuilder.append( ") ) ");
				
			} else {
				sqlBuilder.append(" AND ( ( PPFS.PAX_STATUS IN ('" + ReservationInternalConstants.PaxFareSegmentTypes.FLOWN + "','"
						+ ReservationInternalConstants.PaxFareSegmentTypes.NO_REC + "') AND PPFS.PPF_ID = PPF.PPF_ID) ");
				if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()){
					sqlBuilder.append(" OR (PPFSE.PPFS_ID = PPFS.PPFS_ID " );
					sqlBuilder.append(" AND PPFSE.STATUS IN ('" + EticketStatus.CHECKEDIN.code()+ "','" 
							+ EticketStatus.BOARDED.code()+ "') AND PPFS.PPF_ID = PPF.PPF_ID) ");
				}
				sqlBuilder.append(" ) )");
			}

		} else {
			sqlBuilder.append(" AND fs.EST_TIME_DEPARTURE_ZULU > to_date('" + strDate + "', 'DD-MM-YYYY HH24:MI:SS') ");
		}

		sqlBuilder.append(" ORDER BY fs.EST_TIME_DEPARTURE_ZULU ");

		Collection<Integer> colPnrSegmentIds = (Collection<Integer>) jt.query(sqlBuilder.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colPnrs = new ArrayList<Integer>();

				if (rs != null) {
					while (rs.next()) {
						colPnrs.add(rs.getInt("pnrSegId"));
					}
				}
				return colPnrs;
			}
		});
		return colPnrSegmentIds;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, Integer> getSSRCount(int flightSegId, String cabinClassCode, String logicalCabinClass) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT st.ssr_id, st.max_qty - ");
		sqlBuilder.append(" (SElECT COUNT(ppss.ppss_id) ");
		sqlBuilder.append(" FROM t_pnr_pax_segment_ssr ppss, t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, t_booking_class bc ");
		sqlBuilder.append(" WHERE ppss.status = 'CNF' AND ppss.ppfs_id= ppfs.ppfs_id ");
		sqlBuilder.append(" AND ppfs.pnr_seg_id=ps.pnr_seg_id AND ppss.ssr_id=st.ssr_id AND ps.flt_seg_id=? ");
		sqlBuilder.append(" AND ppfs.booking_code = bc.booking_code AND (bc.logical_cabin_class_code = '" + logicalCabinClass
				+ "' OR bc.cabin_class_code = '" + cabinClassCode + "' )) avail_count ");
		sqlBuilder.append(" FROM t_flight_segment fs, t_flight f, ");
		sqlBuilder.append(" t_aircraft_model am, t_ssr_template_cabin_qty st ");
		sqlBuilder.append(" WHERE fs.flight_id=f.flight_id AND f.model_number=am.model_number ");
		sqlBuilder.append(" AND am.ssr_template_id= st.ssr_template_id AND fs.flt_seg_id=? ");
		sqlBuilder.append(AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClassCode, logicalCabinClass, "st"));

		Object[] param = { flightSegId, flightSegId };

		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		Map<Integer, Integer> ssrCountMap = (Map<Integer, Integer>) jt.query(sqlBuilder.toString(), param,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Integer> map = new HashMap<Integer, Integer>();

						if (rs != null) {
							while (rs.next()) {
								map.put(rs.getInt("ssr_id"), rs.getInt("avail_count"));
							}
						}

						return map;
					}
				});

		return ssrCountMap;
	}

	private static String getDuration(Date departureDate, Date arrivalDate) {

		NumberFormat nfr = new DecimalFormat("##00");

		Calendar depCal = Calendar.getInstance();
		depCal.setTime(departureDate);

		Calendar arrCal = Calendar.getInstance();
		arrCal.setTime(arrivalDate);

		long diff = arrCal.getTimeInMillis() - depCal.getTimeInMillis();

		long diffMinutes = diff / (60 * 1000);

		String durationHoursStr = nfr.format((diffMinutes / 60)) + ":" + nfr.format((diffMinutes % 60));

		return durationHoursStr;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getOnHoldReservationPnrsForEmailNotification(OnholdReservatoinSearchDTO onHoldReservationSearchDTO) {
		Object[] args = { ReservationInternalConstants.ReservationStatus.ON_HOLD,
				onHoldReservationSearchDTO.getOriginCarrierCode(), onHoldReservationSearchDTO.getOwnerChannelCode(),
				BeanUtils.getTimestamp(onHoldReservationSearchDTO.getBookingTimeStampBeforeStart()),
				BeanUtils.getTimestamp(onHoldReservationSearchDTO.getBookingTimeStampBeforeEnd()),
				onHoldReservationSearchDTO.getOwnerChannelCode(),
				ReservationInternalConstants.OHDAlertTypes.PAYMENT_FAILURES.code() };
		String sql = "SELECT distinct r.pnr as pnr FROM t_reservation r, t_reservation_contact rc,t_temp_payment_tnx tpt where r.status=? AND "
				+ " r.origin_carrier_code=? AND r.owner_channel_code=? AND r.booking_timestamp between ? and ? AND r.PNR = rc.PNR "
				+ " AND rc.c_email is not null AND r.pnr=tpt.pnr(+) AND (tpt.is_offline_payment = 'N' or tpt.is_offline_payment is null) "
				+ " MINUS SELECT oa.pnr as pnr FROM t_onhold_alert oa where oa.sales_channel_code=? and oa.alert_type=?";
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Collection<String>) jt.query(sql, args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String pnr;
				Collection<String> pnrs = new HashSet<String>();
				if (rs != null) {
					while (rs.next()) {
						pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						if (pnr.length() > 0) {
							pnrs.add(pnr);
						}
					}
				}
				return pnrs;
			}
		});
	}

	@Override
	public void saveOnholdAlert(OnholdAlert onholdAlert) {
		hibernateSaveOrUpdate(onholdAlert);
	}

	@Override
	public OnholdAlert getOnholdAlert(String pnr) {
		return (OnholdAlert) get(OnholdAlert.class, pnr);
	}

	@Override
	public void saveOrUpdatePromotionForReservation(Collection<PromotionAttribute> colPromotionAttribute) {
		hibernateSaveOrUpdateAll(colPromotionAttribute);
	}

	@Override
	public void saveAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		hibernateSaveOrUpdate(autoCancellationInfo);
	}

	@Override
	public AutoCancellationInfo getAutoCancellationInfo(Integer autoCancellationId) {
		return (AutoCancellationInfo) get(AutoCancellationInfo.class, autoCancellationId);
	}

	@Override
	public List<Integer> getExpiredAutoCancellationInfoList(Date currentDateTime) {
		String hql = "SELECT a.autoCancellationId FROM AutoCancellationInfo a WHERE a.expireOn <= ? AND a.processedByScheduler= ?";
		Object[] args = new Object[] { currentDateTime, AutoCancellationInfo.NOT_PROCESSED };

		return find(hql, args, Integer.class);

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ReservationLiteDTO> getExpiredInsuranceAvailability(Collection<Integer> cancellationIds, String marketingCarrier) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, r.version, pi.auto_cancellation_id FROM t_reservation r, t_pnr_insurance pi");
		query.append(" WHERE r.pnr=pi.pnr AND r.status=? AND r.ORIGIN_CHANNEL_CODE <> ?");
		query.append(" AND r.DUMMY_BOOKING = ? AND pi.auto_cancellation_id IN (");
		query.append(BeanUtils.constructINStringForInts(cancellationIds) + ")");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.SalesChannel.LCC, String.valueOf(ReservationInternalConstants.DummyBooking.NO),
				ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (List<ReservationLiteDTO>) template.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ReservationLiteDTO> resDtos = null;
				if (rs != null) {
					resDtos = new ArrayList<ReservationLiteDTO>();
					while (rs.next()) {
						String pnr = rs.getString("pnr");
						long version = rs.getLong("version");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");

						ReservationLiteDTO resDto = new ReservationLiteDTO(pnr, version, autoCancellationId);
						resDtos.add(resDto);
					}
				}
				return resDtos;
			}
		});
	}

	public void saveWLReservationPriority(Collection<ReservationWLPriority> reservationWLPriority) {
		hibernateSaveOrUpdateAll(reservationWLPriority);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccInsuranceInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, pi.auto_cancellation_id FROM t_reservation r, t_pnr_insurance pi");
		query.append(" WHERE r.pnr=pi.pnr AND r.status=? AND r.ORIGIN_CHANNEL_CODE = ?");
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.DUMMY_BOOKING = ? AND pi.auto_cancellation_id IN (");
		query.append(BeanUtils.constructINStringForInts(cancellationIds) + ")");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.SalesChannel.LCC, marketingCarrier,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) template.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						autoCnxPNRs.put(pnr, autoCnxId);
					}
				}

				return autoCnxPNRs;
			}
		});
	}

	@Override
	public void updateAutoCancelSchedulerStatus(List<Integer> autoCnxIds, String schedulerStatus) {

		Query query = getSession().createQuery(
				"UPDATE AutoCancellationInfo SET processedByScheduler=:processed WHERE autoCancellationId in (:autoCnxIds)");
		query.setString("processed", schedulerStatus);
		query.setParameterList("autoCnxIds", autoCnxIds);
		query.executeUpdate();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<OnlineCheckInReminderDTO> getDateForOnlineCheckInReminder(Date date) {
		log.debug(" Inside getDateForOnlineCheckInReminder() ");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String strDate = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		strDate = dateFormat.format(date);
		String scheluledJobHourDurationPeriod = AppSysParamsUtil.getOnlineCheckInReminderSchedulerTimeDuration();
		int scheluledJobHourDuration = Integer.parseInt(scheluledJobHourDurationPeriod);

		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT r.pnr,rc.c_email,rc.c_first_name,rc.c_last_name,fs.est_time_departure_local,fs.est_time_departure_local, ");
		sql.append(" SUBSTR(fs.segment_code,1,3) as origin,f.flight_number,rc.preferred_lang ");
		sql.append(" FROM t_reservation r,t_pnr_segment ps,t_flight_segment fs,t_reservation_contact rc,t_flight f ");
		sql.append(" WHERE r.pnr = ps.pnr ");
		sql.append(" AND ps.flt_seg_id = fs.flt_seg_id");
		sql.append(" AND r.pnr = rc.pnr ");
		sql.append(" AND f.flight_id = fs.flight_id ");
		sql.append(" AND r.status = 'CNF' ");
		sql.append(" AND ps.segment_seq = 1");
		sql.append(" AND r.origin_channel_code = 4");
		sql.append(" AND rc.c_email is not null ");
		sql.append(" AND fs.est_time_departure_zulu between sysdate AND sysdate + NUMTODSINTERVAL(" + scheluledJobHourDuration
				+ ",'HOUR')");
		sql.append(" AND SUBSTR(fs.segment_code,1,3) IN ");
		sql.append(" (SELECT airport_code FROM t_airport WHERE online_checkin = 'Y') ");

		log.debug("##############################################################");
		log.debug(" 		GET ONLINE CHECKIN REMINDER DETAILS TO SEND ALERTS	 ");
		log.debug(" 		SQL To Execute 			: " + sql.toString());
		log.debug(" 		Param For Query			: " + strDate);
		log.debug("##############################################################");

		Object[] param = {};
		List<OnlineCheckInReminderDTO> onlineCheckInReminderList = (List<OnlineCheckInReminderDTO>) jt.query(sql.toString(),
				param, new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<OnlineCheckInReminderDTO> onlineCheckInReminder = new ArrayList<OnlineCheckInReminderDTO>();
						OnlineCheckInReminderDTO onlineCheckInReminderDTO;
						if (rs != null) {
							while (rs.next()) {
								onlineCheckInReminderDTO = new OnlineCheckInReminderDTO();
								onlineCheckInReminderDTO.setFirstName(rs.getString("c_first_name"));
								onlineCheckInReminderDTO.setLastName(rs.getString("c_last_name"));
								onlineCheckInReminderDTO.setFlightNo(rs.getString("flight_number"));
								onlineCheckInReminderDTO.setDepartureDate(rs.getDate("est_time_departure_local"));
								onlineCheckInReminderDTO.setOrigin(rs.getString("origin"));
								onlineCheckInReminderDTO.setPnr(rs.getString("pnr"));
								onlineCheckInReminderDTO.setPreferedLanguage(rs.getString("preferred_lang"));
								onlineCheckInReminderDTO.setEmail(rs.getString("c_email"));
								onlineCheckInReminder.add(onlineCheckInReminderDTO);
							}
						}
						return onlineCheckInReminder;
					}
				});

		return onlineCheckInReminderList;
	}

	@Override
	public Integer getWLReservationPriority(Integer flightSegId) {

		String hql = "SELECT MAX(rwlp.priority) FROM ReservationWLPriority rwlp WHERE rwlp.flightSegId = ?";
		Object[] args = new Object[] { flightSegId };

		return BeanUtils.getFirstElement(find(hql, args, Integer.class));

	}

	@Override
	@SuppressWarnings("unchecked")
	public ReservationWLPriority getWaitListedReservationPriority(String pnr, Integer pnrSegId) {
		Collection<ReservationWLPriority> wlPriorities = new ArrayList<ReservationWLPriority>();
		String sql = "SELECT wl.priority FROM t_pnr_wl_priority wl, t_pnr_segment seg WHERE wl.pnr=seg.pnr AND seg.flt_seg_id = wl.flt_seg_id AND seg.pnr_seg_id=? AND wl.pnr= ?";
		Object[] args = new Object[] { pnrSegId, pnr };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		wlPriorities = (List<ReservationWLPriority>) template.query(sql, args, new ResultSetExtractor() {

			public List<ReservationWLPriority> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ReservationWLPriority> wlPriorities = new ArrayList<ReservationWLPriority>();
				ReservationWLPriority wlPriority = null;
				while (rs.next()) {
					wlPriority = new ReservationWLPriority();

					wlPriority.setPriority(rs.getInt("priority"));
					wlPriorities.add(wlPriority);
				}
				return wlPriorities;
			}
		});
		return BeanUtils.getFirstElement(wlPriorities);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationWLPriority> getWaitListedReservations(Integer flightSegId) {
		// String hql =
		// "SELECT rwlp FROM ReservationWLPriority rwlp WHERE rwlp.flightSegId = ? AND rwlp.status= ? ORDER BY rwlp.priority";
		Collection<ReservationWLPriority> wlPriorities = new ArrayList<ReservationWLPriority>();
		String sql = "SELECT wl.pnr_wl_priority_id, wl.pnr, wl.priority, wl.flt_seg_id, wl.status, wl.version, wl.total_pax_adult_child_count, "
				+ "wl.total_pax_infant_count FROM t_pnr_wl_priority wl, t_reservation r WHERE wl.pnr=r.pnr AND wl.flt_seg_id=? AND wl.status= ? AND (r.status= ? OR r.status= ?) ORDER BY r.status, wl.priority";
		Object[] args = new Object[] { flightSegId, ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING,
				ReservationInternalConstants.ReservationStatus.CONFIRMED, ReservationInternalConstants.ReservationStatus.ON_HOLD };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		wlPriorities = (List<ReservationWLPriority>) template.query(sql, args, new ResultSetExtractor() {

			public List<ReservationWLPriority> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ReservationWLPriority> wlPriorities = new ArrayList<ReservationWLPriority>();
				ReservationWLPriority wlPriority = null;
				while (rs.next()) {
					wlPriority = new ReservationWLPriority();
					wlPriority.setPnrWLPriorityId(rs.getInt("pnr_wl_priority_id"));
					wlPriority.setPnr(rs.getString("pnr"));
					wlPriority.setPriority(rs.getInt("priority"));
					wlPriority.setFlightSegId(rs.getInt("flt_seg_id"));
					wlPriority.setVersion(rs.getInt("version"));
					wlPriority.setStatus(rs.getString("status"));
					wlPriority.setTotalPassengerCount(rs.getInt("total_pax_adult_child_count"));
					wlPriority.setInfantCount(rs.getInt("total_pax_infant_count"));
					wlPriorities.add(wlPriority);
				}
				return wlPriorities;
			}
		});
		return wlPriorities;
	}

	@SuppressWarnings("unchecked")
	public List<ReservationWLPriority> getWaitListedPrioritiesForSegment(Integer pnrSegmentId) {
		List<ReservationWLPriority> wlPriorities = null;
		String sql = "SELECT wlp.pnr, wlp.priority, wlp.flt_seg_id FROM t_pnr_wl_priority wlp, t_pnr_segment ps WHERE  wlp.flt_seg_id = ps.flt_seg_id AND ps.pnr_seg_id = ?";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		wlPriorities = (List<ReservationWLPriority>) template.query(sql, new Object[] { pnrSegmentId }, new ResultSetExtractor() {

			public List<ReservationWLPriority> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<ReservationWLPriority> wlPriorities = new ArrayList<ReservationWLPriority>();
				ReservationWLPriority wlPriority = null;
				while (rs.next()) {
					wlPriority = new ReservationWLPriority();
					wlPriority.setPnr(rs.getString("pnr"));
					wlPriority.setPriority(rs.getInt("priority"));
					wlPriority.setFlightSegId(rs.getInt("flt_seg_id"));
					wlPriorities.add(wlPriority);
				}
				return wlPriorities;
			}
		});
		return wlPriorities;
	}

	@SuppressWarnings("unchecked")
	public Collection<ReservationPaxDTO> searchReservations(ReservationSearchDTO searchDTO) {
		Collection<ReservationPaxDTO> reservations = null;

		StringBuilder sb = new StringBuilder();
		sb.append(
				"SELECT r.pnr pnr, p.pnr_pax_id pnr_pax_id, f.flight_number flight_number, fs.est_time_departure_local time_departure, ")
				.append("  fs.est_time_arrival_local time_arrival, fs.segment_code segment_code, p.first_name first_name, p.last_name last_name, ")
				.append("  p.pax_category_code pax_category_code, t.e_ticket_number e_ticket_number ")
				.append("FROM t_pnr_passenger p, t_pax_e_ticket t, t_reservation r, ")
				.append("  t_reservation_contact c, t_pnr_segment s, ").append("  t_flight_segment fs, t_flight f ")
				.append("WHERE p.pnr_pax_id = t.pnr_pax_id AND p.pnr = r.pnr AND r.pnr = s.pnr ")
				.append("  AND s.flt_seg_id = fs.flt_seg_id AND fs.flight_id = f.flight_id AND r.pnr = c.pnr ")
		// .append("  AND r.external_rec_locator is not NULL ")
		;

		if (searchDTO.getFirstName() != null && !searchDTO.getFirstName().isEmpty()) {
			sb.append("   AND LOWER(p.first_name) LIKE '%" + searchDTO.getFirstName().toLowerCase() + "%' ");
		}

		if (searchDTO.getLastName() != null && !searchDTO.getLastName().isEmpty()) {
			sb.append("   AND LOWER(p.last_name) LIKE '%" + searchDTO.getLastName().toLowerCase() + "%' ");
		}

		if (searchDTO.getDepartureDate() != null) {
			sb.append("  AND TO_CHAR(fs.est_time_departure_local , 'dd-mm-yyyy' ) = '"
					+ CalendarUtil.getDateInFormattedString("DD-MM-YYYY", searchDTO.getDepartureDate()) + "' ");
		}

		if (searchDTO.isDepartingTimeInclusive()) {
			sb.append("  AND TO_CHAR(fs.est_time_departure_local , 'hh24-mi' ) = '"
					+ CalendarUtil.getDateInFormattedString("HH-mm", searchDTO.getDepartureDate()) + "' ");
		}

		if (searchDTO.getTelephoneNo() != null) {
			sb.append("  AND (c.c_phone_no LIKE '%" + searchDTO.getTelephoneNo() + "%' OR c.c_mobile_no LIKE '%"
					+ searchDTO.getTelephoneNo() + "%') ");
		}

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		reservations = (Collection<ReservationPaxDTO>) template.query(sb.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<ReservationPaxDTO> results = new ArrayList<ReservationPaxDTO>();

				ReservationPaxDTO paxDTO;
				ReservationSegmentDTO segmentDTO;
				ReservationPaxDetailsDTO paxDetailsDTO;
				ResSegmentEticketInfoDTO eticketInfoDTO;

				while (rs.next()) {
					paxDTO = new ReservationPaxDTO();
					paxDTO.setPnr(rs.getString("pnr"));

					segmentDTO = new ReservationSegmentDTO();
					segmentDTO.setFlightNo(rs.getString("flight_number"));
					segmentDTO.setDepartureDate(rs.getDate("time_departure"));
					segmentDTO.setArrivalDate(rs.getDate("time_arrival"));
					segmentDTO.setSegmentCode(rs.getString("segment_code"));
					paxDTO.addSegment(segmentDTO);

					paxDetailsDTO = new ReservationPaxDetailsDTO();
					paxDetailsDTO.setPnrPaxId(rs.getInt("pnr_pax_id"));
					paxDetailsDTO.setFirstName(rs.getString("first_name"));
					paxDetailsDTO.setLastName(rs.getString("last_name"));

					eticketInfoDTO = new ResSegmentEticketInfoDTO();
					eticketInfoDTO.seteTicketNumber(rs.getLong("e_ticket_number"));
					paxDetailsDTO.addeTicketInfo(eticketInfoDTO);

					paxDTO.addPassenger(paxDetailsDTO);
					results.add(paxDTO);
				}
				return results;
			}
		});

		return reservations;
	}

	@Override
	public Reservation getReservationByPromoCriteria(String inputParam, long promoCriteriaID) {
		String hql = "SELECT r FROM Reservation r WHERE r.promotionId = ? ";
		Object[] params = { Long.valueOf(inputParam) };
		Collection<Reservation> colReservation = find(hql, params, Reservation.class);
		Reservation reservation = null;

		if (colReservation != null && colReservation.size() > 0) {
			reservation = BeanUtils.getFirstElement(colReservation);
			// Load fares

		}

		return reservation;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<OnlineCheckInReminderDTO> getFlightsSegsForOnlineCheckInReminder(Date date, String schedulerType) {

		log.debug(" Inside getDateForOnlineCheckInReminder() ");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String strDate = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		strDate = dateFormat.format(date);
		String scheluledJobHourDurationPeriod = AppSysParamsUtil.getOnlineCheckInReminderSchedulerTimeDuration();
		int scheluledJobHourDuration = Integer.parseInt(scheluledJobHourDurationPeriod);
		int pickingUpStartHours = scheluledJobHourDuration;
		int pickingUpEndHours = scheluledJobHourDuration + 24;

		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT fs.est_time_departure_local,fs.est_time_departure_zulu,SUBSTR(fs.segment_code,1,3) AS origin,f.flight_number, ");
		sql.append(" fs.flt_seg_id,fs.segment_code,fsne.flt_seg_notification_evnt_id,");
		sql.append(" fs.est_time_departure_zulu - NUMTODSINTERVAL(" + scheluledJobHourDuration
				+ ",'HOUR') AS SND_NOTIFICATION_TIME ");
		sql.append(" FROM t_flight_segment fs INNER JOIN t_flight f ON f.flight_id = fs.flight_id");
		sql.append(" LEFT OUTER JOIN t_flight_seg_notification_evnt fsne ON fsne.flt_seg_id = fs.flt_seg_id");
		sql.append(" WHERE f.flight_id  = fs.flight_id ");
		sql.append(" AND(f.status ='ACT' OR f.status ='CLS') ");
		sql.append(" AND FS.SEGMENT_VALID_FLAG ='Y' ");
		sql.append(" AND operation_type_id <> '" + AirScheduleCustomConstants.OperationTypes.BUS_SERVICE + "'");

		if (schedulerType.equals(ReservationInternalConstants.SchedulerType.RECOVERY_SCHEDULER)) {
			sql.append("  AND (fsne.no_of_attempts <=5 AND ");
			sql.append("      (fsne.anci_notify_status = 'FAILED' ");
			sql.append("  		OR (fsne.anci_notify_status = 'INPROGRESS' AND  to_date('" + strDate
					+ "', 'DD-MM-YYYY HH24:MI:SS') - fsne.timestamp > 2/24) ");
			sql.append("  		OR (fsne.anci_notify_status = 'RESCHEDULED' AND  to_date('" + strDate
					+ "', 'DD-MM-YYYY HH24:MI:SS') - fsne.timestamp > 2/24))) ");
			sql.append("  AND fsne.notification_type = '" + ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER
					+ "' ");
		}

		sql.append(" AND fs.est_time_departure_zulu BETWEEN to_date('" + strDate + "', 'DD-MM-YYYY HH24:MI:SS') AND to_date('"
				+ strDate + "', 'DD-MM-YYYY HH24:MI:SS') + NUMTODSINTERVAL(" + pickingUpEndHours + ",'HOUR') ");
		sql.append(" AND SUBSTR(fs.segment_code,1,3) IN (SELECT airport_code FROM t_airport WHERE online_checkin = 'Y' AND status ='ACT') ");
		sql.append(" ORDER BY fs.est_time_departure_zulu ");

		log.debug("##############################################################");
		log.debug(" 		GET ONLINE CHECKIN REMINDER DETAILS TO SEND ALERTS	 ");
		log.debug(" 		SQL To Execute 			: " + sql.toString());
		log.debug(" 		Param For Query			: " + strDate);
		log.debug("##############################################################");

		Object[] param = {};
		List<OnlineCheckInReminderDTO> onlineCheckInReminderList = (List<OnlineCheckInReminderDTO>) jt.query(sql.toString(),
				param, new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<OnlineCheckInReminderDTO> onlineCheckInReminder = new ArrayList<OnlineCheckInReminderDTO>();
						OnlineCheckInReminderDTO onlineCheckInReminderDTO;
						if (rs != null) {
							while (rs.next()) {
								onlineCheckInReminderDTO = new OnlineCheckInReminderDTO();
								onlineCheckInReminderDTO.setFlightNo(rs.getString("flight_number"));
								onlineCheckInReminderDTO.setDepartureDate(rs.getDate("est_time_departure_zulu"));
								onlineCheckInReminderDTO.setOrigin(rs.getString("origin"));
								onlineCheckInReminderDTO.setSegCode(rs.getString("segment_code"));
								onlineCheckInReminderDTO.setFltSegId(rs.getInt("flt_seg_id"));
								onlineCheckInReminderDTO.setSendNotificationTime(new Date(rs
										.getTimestamp("SND_NOTIFICATION_TIME").getTime()));
								onlineCheckInReminderDTO.setFlightSegmentNotificationId(rs.getInt("flt_seg_notification_evnt_id"));
								onlineCheckInReminder.add(onlineCheckInReminderDTO);
							}
						}
						return onlineCheckInReminder;
					}
				});

		return onlineCheckInReminderList;

	}

	@Override
	public List<PNRInfoDTO> getInfoForOnlineCheckinReminder(Integer flightSegId) {

		log.debug(" Inside getInfoForOnlineCheckinReminder() ");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuffer sql = new StringBuffer();

		sql.append(" SELECT rc.c_first_name,rc.c_last_name,rc.c_email,r.pnr,rc.preferred_lang,rc.c_title,psne.pnr_seg_notification_event_id,ps.pnr_seg_id");
		sql.append(" FROM t_reservation r LEFT OUTER JOIN t_reservation_contact rc ON r.pnr=rc.pnr LEFT OUTER JOIN t_pnr_segment ps ON ps.pnr = r.pnr LEFT OUTER JOIN t_flight_segment fs ON ps.flt_seg_id = fs.flt_seg_id LEFT OUTER JOIN t_flight f ON fs.flight_id =f.flight_id");
		sql.append(" LEFT OUTER JOIN t_pnr_seg_notification_event psne ON ps.pnr_seg_id = psne.pnr_seg_id AND psne.notification_type ='"
				+ ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER + "'");
		sql.append(" WHERE  r.status = 'CNF' AND ps.status = 'CNF' AND r.owner_channel_code = 4 AND rc.c_email IS NOT NULL");
		sql.append(" AND fs.flt_seg_id =" + flightSegId);
		sql.append(" AND f.operation_type_id <> '" + AirScheduleCustomConstants.OperationTypes.BUS_SERVICE + "' ");

		sql.append(" AND (psne.pnr_seg_notification_event_id is NULL OR (psne.notify_status = 'FAILED' and psne.notification_type ='"
				+ ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER + "')) ");
		sql.append(" AND fs.est_time_departure_zulu IN (");
		sql.append(" (SELECT MIN(fs1.est_time_departure_zulu) ");
		sql.append(" FROM T_PNR_SEGMENT PS1,T_FLIGHT_SEGMENT FS1 ");
		sql.append(" WHERE ps1.flt_seg_id=fs1.flt_seg_id ");
		sql.append(" AND ps1.PNR = R.PNR ");
		sql.append(" AND ps1.status  ='CNF')) ");

		log.debug("##############################################################");
		log.debug(" 		GET ONLINE CHECKIN REMINDER Info per flightSeg TO SEND ALERTS	 ");
		log.debug(" 		SQL To Execute 			: " + sql.toString());
		log.debug(" 		Param For Query			: " + flightSegId);
		log.debug("##############################################################");

		Object[] param = {};
		List<PNRInfoDTO> pnrInfoDTOList = (List<PNRInfoDTO>) jt.query(sql.toString(), param, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<PNRInfoDTO> pnrInfoDTOs = new ArrayList<PNRInfoDTO>();
				PNRInfoDTO pnrInfoDTO;
				if (rs != null) {
					while (rs.next()) {
						pnrInfoDTO = new PNRInfoDTO();
						pnrInfoDTO.setFirstName(rs.getString("c_first_name"));
						pnrInfoDTO.setLastName(rs.getString("c_last_name"));
						pnrInfoDTO.setPnr(rs.getString("pnr"));
						pnrInfoDTO.setEmail(rs.getString("c_email"));
						pnrInfoDTO.setPrefferdLanguage(rs.getString("preferred_lang"));
						pnrInfoDTO.setTitle(rs.getString("c_title"));
						pnrInfoDTO.setPnrSegId(rs.getInt("pnr_seg_id"));
						pnrInfoDTO.setPnrSegNotificationEventId(rs.getInt("pnr_seg_notification_event_id"));
						pnrInfoDTOs.add(pnrInfoDTO);

					}
				}
				return pnrInfoDTOs;
			}
		});

		return pnrInfoDTOList;

	}

	@SuppressWarnings("unchecked")
	public List<ReservationBasicsDTO> getFlightSegmentPAXDetails(Integer flightSegId, String logicalCabinClass) {

		List<ReservationBasicsDTO> pnrPaxList = new ArrayList();

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT DISTINCT PP.PNR_PAX_ID,PP.TITLE,  PP.FIRST_NAME,  PP.LAST_NAME,  " +
                "PP.PAX_TYPE_CODE,  PP.ADULT_ID, PPAI.ARRIVAL_INTL_FLIGHTNO, PPAI.ARRIVAL_DATE," +
                " PPAI.DEPARTURE_INTL_FLIGHTNO, PPAI.DEPARTURE_DATE, PPAI.GROUP_ID, R.PNR,  R.TOTAL_PAX_COUNT, " +
                " R.TOTAL_PAX_CHILD_COUNT,  R.TOTAL_PAX_INFANT_COUNT, R.STATUS, PS.FLT_SEG_ID, PP.PNR_PAX_ID ");
		sql.append("FROM T_RESERVATION R,  T_PNR_PASSENGER PP ,  T_PNR_SEGMENT PS, T_PNR_PAX_ADDITIONAL_INFO PPAI ");
		sql.append("WHERE PS.FLT_SEG_ID=" + flightSegId + " AND PP.PNR = R.PNR AND PP.PNR = PS.PNR ");
		sql.append("AND PS.PNR = R.PNR AND PP.PNR_PAX_ID  = PPAI.PNR_PAX_ID AND R.PNR IN   (SELECT DISTINCT R.PNR ");
		sql.append("FROM T_PNR_SEGMENT PS ,   T_PNR_PAX_FARE_SEGMENT PPFS,    T_RESERVATION R ,T_BOOKING_CLASS BC ");
		sql.append("WHERE PS.PNR_SEG_ID = PPFS.PNR_SEG_ID  AND R.PNR = PS.PNR ");
		sql.append("AND PS.FLT_SEG_ID = " + flightSegId + "  AND BC.BOOKING_CODE = PPFS.BOOKING_CODE ");
		sql.append("AND R.STATUS <> 'CNX'  AND PS.STATUS = 'CNF' ");
		sql.append("AND PS.OPEN_RT_CONFIRM_BEFORE IS NULL ");
		sql.append("AND BC.LOGICAL_CABIN_CLASS_CODE='" + logicalCabinClass + "' AND BC.BC_TYPE <> 'STB' ) ORDER BY PPAI.ARRIVAL_INTL_FLIGHTNO, PPAI.ARRIVAL_DATE, PPAI.DEPARTURE_INTL_FLIGHTNO, PPAI.DEPARTURE_DATE,  R.PNR");
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		pnrPaxList = (List<ReservationBasicsDTO>) template.query(sql.toString(), new ResultSetExtractor() {

			public List<ReservationBasicsDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {

				Map<String, ReservationBasicsDTO> pnrPaxMap = new HashMap<String, ReservationBasicsDTO>();
				List<String> pnrList = new ArrayList<String>();
				List<ReservationBasicsDTO> pnrPaxList = new ArrayList();
				ReservationPaxDetailsDTO resPaxDTO = null;
				ReservationBasicsDTO reservationBasicsDTO = null;
				while (rs.next()) {
					String pnr = rs.getString("pnr");

					if (pnrPaxMap.get(pnr) == null) {
						reservationBasicsDTO = new ReservationBasicsDTO();
						reservationBasicsDTO.setPnr(pnr);
						reservationBasicsDTO.setAdultCount(rs.getInt("TOTAL_PAX_COUNT"));
						reservationBasicsDTO.setChildCount(rs.getInt("TOTAL_PAX_CHILD_COUNT"));
						reservationBasicsDTO.setInfantCount(rs.getInt("TOTAL_PAX_INFANT_COUNT"));
						reservationBasicsDTO.setStatus(rs.getString("STATUS"));
					} else {
						reservationBasicsDTO = pnrPaxMap.get(pnr);
					}

					resPaxDTO = new ReservationPaxDetailsDTO();
					resPaxDTO.setPnr(pnr);
					resPaxDTO.setPnrPaxId(rs.getInt("PNR_PAX_ID"));
					resPaxDTO.setTitle(rs.getString("TITLE"));
					resPaxDTO.setFirstName(rs.getString("FIRST_NAME"));
					resPaxDTO.setLastName(rs.getString("LAST_NAME"));
					resPaxDTO.setPaxType(rs.getString("PAX_TYPE_CODE"));
					resPaxDTO.setAdultId(rs.getInt("ADULT_ID"));
					resPaxDTO.setArrivalIntlFlightNo(rs.getString("ARRIVAL_INTL_FLIGHTNO"));
					resPaxDTO.setIntlFlightArrivalDate(rs.getTimestamp("ARRIVAL_DATE"));
					resPaxDTO.setDepartureIntlFlightNo(rs.getString("DEPARTURE_INTL_FLIGHTNO"));
					resPaxDTO.setIntlFlightDepartureDate(rs.getTimestamp("DEPARTURE_DATE"));
					resPaxDTO.setPnrPaxGroupId(rs.getString("GROUP_ID"));
					resPaxDTO.setCsFlightNo(rs.getString("FLT_SEG_ID"));

					reservationBasicsDTO.addPassenger(resPaxDTO);

					pnrPaxMap.put(pnr, reservationBasicsDTO);
					if (!pnrList.contains(pnr)) {
						pnrList.add(pnr);
					}
				
				}

				if (pnrPaxMap != null && pnrPaxMap.size() > 0) {
					for (String pnr : pnrList) {
						ReservationBasicsDTO resBasicsDTO = pnrPaxMap.get(pnr);

						pnrPaxList.add(resBasicsDTO);

					}
				}

				return pnrPaxList;
			}
		});
		return pnrPaxList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<PNRGOVResInfoDTO> getResInfoListForFlightSegment(Integer flightSegmentID) {
		log.debug("Getting PNR List for the flight segment ID : " + flightSegmentID);

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT r.PNR, ");
		sql.append("r.origin_agent_code AS or_agent_code,");
		sql.append("r.owner_agent_code  AS ow_agent_code, ");
		sql.append("r.booking_timestamp AS booking_timestamp ");
		sql.append("FROM T_PNR_SEGMENT ps, T_RESERVATION r ");
		sql.append("WHERE ps.flt_seg_id = " + flightSegmentID);
		sql.append(" AND r.status='CNF' ");
		sql.append(" AND ps.status='CNF' ");
		sql.append("AND ps.pnr = r.pnr ");
		sql.append("GROUP BY r.pnr,r.origin_agent_code,r.owner_agent_code,r.booking_timestamp");

		Object[] param = {};

		List<PNRGOVResInfoDTO> pnrListForFlightSegment = (List<PNRGOVResInfoDTO>) jt.query(sql.toString(), param,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<PNRGOVResInfoDTO> resInfoListforFlightSegment = new ArrayList<PNRGOVResInfoDTO>();
						if (rs != null) {
							while (rs.next()) {
								PNRGOVResInfoDTO resInfo = new PNRGOVResInfoDTO();
								resInfo.setPnr(rs.getString("pnr"));
								resInfo.setOwnerAgentCode(rs.getString("ow_agent_code"));
								resInfo.setOriginAgentCode(rs.getString("or_agent_code"));
								resInfo.setBookingTimeStamp(rs.getDate("booking_timestamp"));
								resInfoListforFlightSegment.add(resInfo);
							}
						}
						return resInfoListforFlightSegment;
					}

				});
		return pnrListForFlightSegment;
	}

	public List<GroupPaymentNotificationDTO> getGrooupBookingForReminders() {
		log.debug("Getting PNR List for the flight segment ID : ");
		// String scheluledJobBeforeDayDurationPeriod =
		// AppSysParamsUtil.getOnlineCheckInReminderSchedulerTimeDuration();
		// int scheluledJobDayDuration = Integer.parseInt(scheluledJobBeforeDayDurationPeriod);
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 2);// todo : gayan scheluledJobDayDuration
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String formatDate = format1.format(date);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  gbr.group_booking_request_id,  rc.c_title,  rc.c_first_name,  rc.c_last_name,  rc.c_email,  r.pnr, gbr.target_payment_date ");
		sql.append("FROM t_reservation r LEFT OUTER JOIN t_reservation_contact rc ON r.pnr=rc.pnr inner join t_group_booking_request gbr on ");
		sql.append("gbr.group_booking_request_id=r.group_booking_request_id where gbr.status=8 and gbr.agreed_fare>gbr.payment_amount and ");
		sql.append("gbr.target_payment_date=TO_DATE('" + formatDate + "', 'YYYY-MM-DD')");

		Object[] param = {};
		log.debug("Sql to retrieve date " + sql);
		List<GroupPaymentNotificationDTO> grpPaymentList = (List<GroupPaymentNotificationDTO>) jt.query(sql.toString(), param,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<GroupPaymentNotificationDTO> resInfoPayment = new ArrayList<GroupPaymentNotificationDTO>();
						if (rs != null) {
							while (rs.next()) {
								GroupPaymentNotificationDTO resInfo = new GroupPaymentNotificationDTO();
								resInfo.setGroupBookingRequestID(rs.getLong("group_booking_request_id"));
								resInfo.setTitle(rs.getString("c_title"));
								resInfo.setName(rs.getString("c_first_name"));
								resInfo.setLastName(rs.getString("c_last_name"));
								resInfo.setPnr(rs.getString("pnr"));
								resInfo.setEmail(rs.getString("c_email"));
								resInfo.setTargetPaymentDate(rs.getDate("target_payment_date"));
								resInfoPayment.add(resInfo);
							}
						}
						return resInfoPayment;
					}

				});
		return grpPaymentList;

	}

	public List<ReservationDTO> getImmediateReservations(int customerId, int count) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append(" SELECT pnr, originator_pnr, time_departure_zulu FROM ( SELECT pnr, originator_pnr, MIN(time_departure_zulu) time_departure_zulu FROM ( ")
				.append("   ( SELECT r.pnr pnr, r.originator_pnr originator_pnr, MIN(fs.est_time_departure_zulu) time_departure_zulu ")
				.append("   FROM t_reservation r, t_reservation_contact rc, t_pnr_segment rs, t_flight_segment fs ")
				.append("   WHERE r.pnr = rc.pnr AND r.pnr = rs.pnr AND rs.flt_seg_id = fs.flt_seg_id ")
				.append("       AND rc.customer_id = ? AND r.status != 'CNX' AND rs.status != 'CNX' ")
				.append("   GROUP BY r.pnr, r.originator_pnr HAVING MAX(fs.est_time_departure_zulu) >= SYSDATE ) UNION ")
				.append("   ( SELECT r.pnr pnr, r.originator_pnr originator_pnr, MIN(fs.est_time_departure_zulu) time_departure_zulu ")
				.append("   FROM t_reservation r, t_reservation_contact rc, t_ext_pnr_segment rs, t_ext_flight_segment fs ")
				.append("   WHERE r.pnr = rc.pnr AND r.pnr = rs.pnr AND rs.ext_flt_seg_id = fs.ext_flt_seg_id ")
				.append("       AND rc.customer_id = ? AND r.status != 'CNX' AND rs.status != 'CNX' ")
				.append("   GROUP BY r.pnr, r.originator_pnr HAVING MAX(fs.est_time_departure_zulu) >= SYSDATE ")
				.append("   ) ) GROUP BY pnr, originator_pnr ORDER BY time_departure_zulu ) WHERE ROWNUM <= ? ");

		Object[] args = new Object[3];
		args[0] = customerId;
		args[1] = customerId;
		args[2] = count;

		List<ReservationDTO> reservations = (List<ReservationDTO>) jt.query(queryContent.toString(), args,
				new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						List<ReservationDTO> resList = new ArrayList<ReservationDTO>();
						ReservationDTO res;

						while (resultSet.next()) {
							res = new ReservationDTO();
							res.setPnr(resultSet.getString("pnr"));
							res.setOriginatorPnr(resultSet.getString("originator_pnr"));
							resList.add(res);
						}
						return resList;
					}

				});
		return reservations;
	}

	@Override
	public List<OnHoldBookingInfoDTO> getOnholdPnrsForPaymentReminder(Date toDate) {
		StringBuilder sqlBuilder = new StringBuilder();

		List<Integer> webMobileChannels = new ArrayList<Integer>();
		webMobileChannels.add(ReservationInternalConstants.SalesChannel.WEB);
		webMobileChannels.add(ReservationInternalConstants.SalesChannel.IOS);
		webMobileChannels.add(ReservationInternalConstants.SalesChannel.ANDROID);
		
		sqlBuilder.append(" SELECT DISTINCT pnr,release_timestamp,originator_pnr FROM ");
		sqlBuilder.append(" (");// start of Main
		sqlBuilder.append(" SELECT r.pnr,pp.release_timestamp,r.originator_pnr");
		sqlBuilder.append(" FROM t_reservation r,t_pnr_passenger pp");
		sqlBuilder.append(" WHERE r.pnr = pp.pnr");
		sqlBuilder.append(" AND pp.release_timestamp between ? and ?");
		sqlBuilder.append(" AND r.pnr IN ( ");// start of IN
		// Inner Query
		sqlBuilder.append(" SELECT ri.pnr");
		sqlBuilder.append(" FROM t_reservation ri,t_pnr_passenger ppi");
		sqlBuilder.append(" WHERE ri.pnr = ppi.pnr");
		sqlBuilder.append(" AND ri.status = ?");
		sqlBuilder.append(" AND ppi.release_timestamp between ? and ?");
		sqlBuilder.append(" AND ri.origin_channel_code IN (" + BeanUtils.constructINStringForInts(webMobileChannels) + ") ");
		/**
		 * AARESAA-18772 For interline or Dry, payment is not allowed via APIs(because no ETKT implementation),So I am
		 * also not sending the email for the same.But I have made proficiency in the code to support this when ever
		 * this is allowed Then we just have to remove sqlBuilder.append(" AND r.originator_pnr is null");
		 **/
		sqlBuilder.append(" AND ri.originator_pnr is null");
		sqlBuilder.append(" MINUS");
		// Drop payment failures
		sqlBuilder.append(" (");// start of Minus (
		sqlBuilder.append(" SELECT tpt.pnr FROM t_temp_payment_tnx tpt");
		sqlBuilder.append(" UNION ALL");
		// Drop already reminded PNRs
		sqlBuilder.append(" SELECT oa.pnr FROM t_onhold_alert oa ");
		sqlBuilder.append(" where oa.sales_channel_code IN (" + BeanUtils.constructINStringForInts(webMobileChannels) + ") ");
		sqlBuilder.append(" and oa.alert_type= ? ");
		sqlBuilder.append(" )");// End of Minus )
		sqlBuilder.append(" )");// End of IN
		sqlBuilder.append(" )");// End of Main
		sqlBuilder.append(" ORDER BY release_timestamp");

		Date currentDate = new Date();
		// Reminders are only for IBE bookings
		Object[] args = { BeanUtils.getTimestamp(currentDate), BeanUtils.getTimestamp(toDate),
				ReservationInternalConstants.ReservationStatus.ON_HOLD, BeanUtils.getTimestamp(currentDate),
				BeanUtils.getTimestamp(toDate), ReservationInternalConstants.OHDAlertTypes.PAYMENT_REMINDER.code() };

		if (log.isDebugEnabled()) {
			log.debug("SQL to Retrive Date : - " + sqlBuilder.toString() + " \n Arguments : - " + args.toString());
		}

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (List<OnHoldBookingInfoDTO>) jt.query(sqlBuilder.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<OnHoldBookingInfoDTO> bookingInfo = new ArrayList<OnHoldBookingInfoDTO>();
				if (rs != null) {
					OnHoldBookingInfoDTO infoDTO = null;
					while (rs.next()) {
						infoDTO = new OnHoldBookingInfoDTO();
						infoDTO.setPnr(rs.getString("pnr"));
						infoDTO.setGroupPnr(rs.getString("originator_pnr"));
						infoDTO.setReleaseTime(rs.getDate("release_timestamp"));

						bookingInfo.add(infoDTO);
					}
				}
				return bookingInfo;
			}
		});
	}

	public ArrayList<Reservation> loadReservationsHavingAirportTransfers(List<Integer> fltSegIds) {
		String hql = "SELECT res, rpat FROM Reservation res, ReservationPax rp,ReservationSegment rs, ReservationPaxSegAirportTransfer rpat "
				+ " WHERE rs.flightSegId IN ("
				+ Util.buildIntegerInClauseContent(fltSegIds)
				+ ") AND res.pnr = rp.reservation.pnr AND rs.pnrSegId = rpat.pnrSegId AND rp.pnrPaxId = rpat.pnrPaxId "
				+ " AND rs.status = '" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' ORDER BY res.pnr";

		String pnr = "";
		Map<String, Reservation> resMap = new HashMap<String, Reservation>();
		Collection<Object[]> resultSet = getSession().createQuery(hql).list();
		for (Object[] obj : resultSet) {

			Reservation reservation = null;
			ReservationPaxSegAirportTransfer paxAirportTransfer = null;
			PaxAirportTransferTO apt = new PaxAirportTransferTO();
			for (int i = 0; i < obj.length; i++) {
				if (obj[i] instanceof Reservation) {
					reservation = (Reservation) obj[i];
				}
				if (obj[i] instanceof ReservationPaxSegAirportTransfer) {
					paxAirportTransfer = (ReservationPaxSegAirportTransfer) obj[i];
				}
			}

			Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
			ReservationPax reservationPax;
			while (itReservationPax.hasNext()) {
				reservationPax = itReservationPax.next();
				reservationPax.getPnrPaxFares().isEmpty();
			}
			
			apt.setAirportCode(paxAirportTransfer.getAirportCode());
			apt.setPpfId(paxAirportTransfer.getPpfId());
			apt.setPnrPaxId(paxAirportTransfer.getPnrPaxId());
			apt.setPnrSegId(paxAirportTransfer.getPnrSegId());
			apt.setChargeAmount(paxAirportTransfer.getChargeAmount());
			apt.setPnrPaxSegAptId(paxAirportTransfer.getPnrPaxSegAirportTransferId());
			
			AirportTransferExternalChgDTO externalChgDTO = new AirportTransferExternalChgDTO();
			externalChgDTO.setAmount(paxAirportTransfer.getChargeAmount());
			apt.setChgDTO(externalChgDTO);
			
			if (!pnr.equals(reservation.getPnr())) {
				reservation.setAirportTransfers(new HashSet<PaxAirportTransferTO>());
				reservation.getAirportTransfers().add(apt);
			} else {
				reservation.getAirportTransfers().add(apt);
			}
			resMap.put(reservation.getPnr(), reservation);
			pnr = reservation.getPnr();
		}
		return new ArrayList(resMap.values());
	}

	@Override
	public String isPFSReceivedFromOperatingCarrier(String pnr, String flightNo, Date depDate, String pfsCategory) {

		StringBuilder sqlSb = new StringBuilder();
		if(ReservationInternalConstants.PfsCategories.NO_REC.equals(pfsCategory)){
			sqlSb.append("SELECT  F.CS_OC_CARRIER_CODE AS OC_CARRIER ");
			sqlSb.append("FROM T_FLIGHT_SEGMENT FS,T_FLIGHT F ");
			sqlSb.append("WHERE F.FLIGHT_NUMBER ='" + flightNo + "' ");
			sqlSb.append("AND TO_CHAR(F.DEPARTURE_DATE, 'DD-MON-YYYY')= UPPER('" + CalendarUtil.formatSQLDate(depDate) + "') ");
			sqlSb.append("AND FS.FLIGHT_ID = F.FLIGHT_ID AND F.CS_OC_CARRIER_CODE  IS NOT NULL ");
			sqlSb.append("AND F.CS_OC_FLIGHT_NUMBER IS NOT NULL ");
			
		}else{
			sqlSb.append("SELECT  F.CS_OC_CARRIER_CODE AS OC_CARRIER ");
			sqlSb.append("FROM T_RESERVATION R,T_PNR_SEGMENT PS ,T_FLIGHT_SEGMENT FS,T_FLIGHT F ");
			sqlSb.append("WHERE R.PNR='" + pnr + "' AND PS.PNR = R.PNR ");
			sqlSb.append("AND F.FLIGHT_NUMBER ='" + flightNo + "' AND PS.STATUS='" + ReservationSegmentStatus.CONFIRMED + "' ");
			sqlSb.append("AND TO_CHAR(F.DEPARTURE_DATE, 'DD-MON-YYYY')= UPPER('" + CalendarUtil.formatSQLDate(depDate) + "') ");
			sqlSb.append("AND FS.FLIGHT_ID = F.FLIGHT_ID AND F.CS_OC_CARRIER_CODE  IS NOT NULL ");
			sqlSb.append("AND F.CS_OC_FLIGHT_NUMBER IS NOT NULL AND PS.FLT_SEG_ID = FS.FLT_SEG_ID ");
		}
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String operatingCarrier = (String) jt.query(sqlSb.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						String operatingCarrier = rs.getString("OC_CARRIER");
						if (!StringUtil.isNullOrEmpty(operatingCarrier)) {
							return operatingCarrier;
						} else {
							return "";
						}
					}
				}
				return "";
			}
		});

		return operatingCarrier;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Map<String, String>> getUnflownPNRsForFFID(String ffid) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String strDate = dateFormat.format(new Date());

		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		boolean isFlownFromEticket = (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET) ? true : false;

		StringBuilder sqlBuilder = new StringBuilder();
		
		sqlBuilder
				.append(" SELECT pp.pnr AS pnr,pp.pnr_pax_id AS pnr_pax_id, pp.first_name AS first_name, pp.last_name AS last_name, pp.title AS title");
		sqlBuilder.append(" FROM t_pnr_passenger pp ");
		sqlBuilder.append(" JOIN t_pnr_pax_additional_info ppai ");
		sqlBuilder.append(" ON pp.PNR_PAX_ID = ppai.PNR_PAX_ID ");
		sqlBuilder.append(" AND UPPER(ppai.ffid) ='" + ffid.toUpperCase() + "' ");
		sqlBuilder.append(" JOIN t_flight_segment fs ");
		sqlBuilder.append(" ON fs.EST_TIME_DEPARTURE_ZULU > to_date('" + strDate + "', 'DD-MM-YYYY HH24:MI:SS') ");
		sqlBuilder.append(" JOIN t_pnr_segment ps "); 

		sqlBuilder.append("	ON ps.status IN('" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "','"
				+ ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING + "')");
		sqlBuilder.append("	AND ps.FLT_SEG_ID = fs.FLT_SEG_ID ");
		sqlBuilder.append("	AND ps.pnr = pp.pnr ");
		sqlBuilder.append(" JOIN (SELECT PPFS.PNR_SEG_ID AS PNR_SEG_ID ");
		sqlBuilder.append(" FROM T_RESERVATION R, ");
		sqlBuilder.append(" T_PNR_PASSENGER PP, ");
		sqlBuilder.append("	T_PNR_PAX_FARE PPF, ");
		sqlBuilder.append("	T_PNR_PAX_FARE_SEGMENT PPFS ");
		if (isFlownFromEticket) {
			sqlBuilder.append(" , T_PNR_PAX_FARE_SEG_E_TICKET PPFSE ");
		}
		sqlBuilder.append("	WHERE PP.PNR       = R.PNR ");
		sqlBuilder.append("	AND PPF.PNR_PAX_ID = PP.PNR_PAX_ID ");
		if (isFlownFromEticket) {
			sqlBuilder.append(" AND PPFSE.PPFS_ID = PPFS.PPFS_ID  ");
			sqlBuilder.append(" AND PPFSE.STATUS NOT IN ('" + ReservationInternalConstants.PaxFareSegmentTypes.FLOWN + "','"
					+ ReservationInternalConstants.PaxFareSegmentTypes.NO_REC + "') AND PPFS.PPF_ID = PPF.PPF_ID) ppx ");
		} else {
			sqlBuilder.append(" AND PPFS.PAX_STATUS NOT IN ('" + ReservationInternalConstants.PaxFareSegmentTypes.FLOWN + "','"
					+ ReservationInternalConstants.PaxFareSegmentTypes.NO_REC + "') AND PPFS.PPF_ID = PPF.PPF_ID) ppx ");
		}

		sqlBuilder.append("on ps.PNR_SEG_ID = ppx.PNR_SEG_ID ");
		
		Collection<Map<String, String>> colPnrIds = (Collection<Map<String, String>>) jt.query(sqlBuilder.toString(),
				new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Map<String, String>> colChangedPassenger = new ArrayList<Map<String, String>>();

				if (rs != null) {
					while (rs.next()) {
						Map<String, String> paxMap = new HashMap<String, String>();
						paxMap.put("pnr", rs.getString("pnr"));
						paxMap.put("first_name", rs.getString("first_name"));
						paxMap.put("last_name", rs.getString("last_name"));
						paxMap.put("title", rs.getString("title"));
						paxMap.put("pnr_pax_id", Integer.toString(rs.getInt("pnr_pax_id")));
						colChangedPassenger.add(paxMap);
					}
				}
				return colChangedPassenger;
			}
		});
		return colPnrIds;
	}

	@Override
	public int removeFFIDFromUnflownPNRs(String ffid, Collection<Integer> colPNRPaxIds) {
		String hql = "UPDATE ReservationPaxAdditionalInfo pax SET pax.ffid = NULL where UPPER(pax.ffid)= UPPER(:ffid) and pax.reservationPax.pnrPaxId IN (:list)";
		Query query = getSession().createQuery(hql).setParameterList("list", colPNRPaxIds).setString("ffid", ffid);
		int result = query.executeUpdate();
		return result;

	}
	
	
	@Override
	public void saveLmsBlockCreditInfo(LmsBlockedCredit lmsBlockedCredit) {
		log.debug("Inside saveLmsBlockCreditInfo");		
		hibernateSaveOrUpdate(lmsBlockedCredit);
		log.debug("Exit saveLmsBlockCreditInfo");		
	}
	
	public void updateLmsBlockCreditStatus(Integer tempTnxId, String creditUtilizedStatus, String remarks) {
		if (tempTnxId != null && !StringUtil.isNullOrEmpty(creditUtilizedStatus)) {
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("UPDATE LmsBlockedCredit l SET l.creditUtilizedStatus = :creditStatus ");

			if (!StringUtil.isNullOrEmpty(remarks)) {
				sqlBuilder.append(",l.remarks = :remarks ");
			}

			sqlBuilder.append(" where l.tempTnxId= :tempTnxId");

			Query query = getSession().createQuery(sqlBuilder.toString()).setInteger("tempTnxId", tempTnxId)
					.setString("creditStatus", creditUtilizedStatus);
			if (!StringUtil.isNullOrEmpty(remarks)) {
				query.setString("remarks", remarks);
			}
			query.executeUpdate();
		}
	}
	
	public void updateLmsBlockCreditStatusByID(Integer blockCreditId, String creditUtilizedStatus, String remarks) {
		if (blockCreditId != null && !StringUtil.isNullOrEmpty(creditUtilizedStatus)) {
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("UPDATE LmsBlockedCredit l SET l.creditUtilizedStatus = :creditStatus ");

			if (!StringUtil.isNullOrEmpty(remarks)) {
				sqlBuilder.append(",l.remarks = :remarks ");
			}

			sqlBuilder.append(" where l.lmsBlockedId= :lmsBlockedId");

			Query query = getSession().createQuery(sqlBuilder.toString()).setInteger("lmsBlockedId", blockCreditId)
					.setString("creditStatus", creditUtilizedStatus);
			if (!StringUtil.isNullOrEmpty(remarks)) {
				query.setString("remarks", remarks);
			}
			query.executeUpdate();
		}
	}

	public LmsBlockedCredit getLmsBlockCreditInfoByTmpTnxId(Integer tempTnxId) {
		String hql = "SELECT r FROM LmsBlockedCredit r WHERE r.tempTnxId = ? ";
		Object[] params = { tempTnxId };
		Collection<LmsBlockedCredit> lmsBlockedCredits = find(hql, params, LmsBlockedCredit.class);
		LmsBlockedCredit lmsBlockedCredit = null;

		if (lmsBlockedCredits != null && lmsBlockedCredits.size() > 0) {
			lmsBlockedCredit = BeanUtils.getFirstElement(lmsBlockedCredits);
		}

		return lmsBlockedCredit;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<LmsBlockedCredit> getOldLMSBlockedCreditInfoIds(Date fromDateTime) {

		List<LmsBlockedCredit> list = null;
		try {
			String hql = "select f " + "from LmsBlockedCredit as f " + "where "
					+ "f.paymentTimeStamp < :timeStamp and f.creditUtilizedStatus = :creditStatus";

			Query q = getSession().createQuery(hql);
			if (fromDateTime != null) {
				q.setTimestamp("timeStamp", fromDateTime).setString("creditStatus", LmsBlockedCredit.PENDING);
			}
			list = q.list();
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Integer> getLMSBlockedTnxTempPayIdsByPnr(Collection<String> pnrs) {

		if (pnrs != null && !pnrs.isEmpty()) {
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("SELECT TP.TPT_ID FROM T_LMS_CREDIT_BLOCKED_INFO LC,T_TEMP_PAYMENT_TNX TP ");
			sqlBuilder.append("WHERE TP.TPT_ID=LC.TPT_ID AND TP.PNR IN ( " + BeanUtils.constructINString(pnrs)
					+ " ) AND LC.UTILIZED_STATUS='" + LmsBlockedCredit.PENDING + "'");

			DataSource dataSource = ReservationModuleUtils.getDatasource();
			JdbcTemplate jt = new JdbcTemplate(dataSource);

			Collection<Integer> tempTnxIds = (Collection<Integer>) jt.query(sqlBuilder.toString(), new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Collection<Integer> colTempTnxIds = new ArrayList<Integer>();

					if (rs != null) {
						while (rs.next()) {
							colTempTnxIds.add(rs.getInt("TPT_ID"));
						}
					}
					return colTempTnxIds;
				}
			});
			return tempTnxIds;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ReservationPaxDetailsDTO> getPAXDetails(final Integer flightID, int pageNum) {

		List<ReservationPaxDetailsDTO> PaxDataList = new ArrayList();

		if (pageNum <= 0) {
			pageNum = 1;
		}
		final int fromRowNum = (20 * (pageNum - 1));
		final int toRowNum = 20 * pageNum + 1;

		StringBuilder sqlData = new StringBuilder();

		sqlData = new StringBuilder();
		sqlData.append("SELECT * FROM");
		sqlData.append("(SELECT m.*, rownum r");
		sqlData.append(" FROM (");
		sqlData.append("SELECT PP.PNR, PP.PNR_PAX_ID, PP.TITLE,  PP.FIRST_NAME,  PP.LAST_NAME,  PP.PAX_TYPE_CODE, PPAI.FFID, PS.PNR_SEG_ID, PS.RETURN_FLAG, PS.JOURNEY_SEQ, PS.FLT_SEG_ID, RPV.TO_FLT_SEG_ID ");
		sqlData.append("FROM T_PNR_PASSENGER PP LEFT OUTER JOIN T_REPROTECTED_PAX RPV ON PP.PNR_PAX_ID = RPV.PNR_PAX_ID AND RPV.TO_FLT_SEG_ID IN (SELECT DISTINCT FS.FLT_SEG_ID FROM T_FLIGHT_SEGMENT FS WHERE FS.FLIGHT_ID IN ("
				+ flightID + ")), T_PNR_SEGMENT PS, T_PNR_PAX_ADDITIONAL_INFO PPAI ");
		sqlData.append("WHERE PS.FLT_SEG_ID IN (");
		sqlData.append("SELECT DISTINCT FS.FLT_SEG_ID ");
		sqlData.append("FROM T_FLIGHT_SEGMENT FS ");
		sqlData.append("WHERE FS.FLIGHT_ID IN (" + flightID + ")");
		sqlData.append(") AND PP.PNR = PS.PNR ");
		sqlData.append("AND PP.PNR_PAX_ID = PPAI.PNR_PAX_ID AND PS.STATUS = 'CNF' ");
		sqlData.append(
				"ORDER BY PP.PNR, PPAI.ARRIVAL_INTL_FLIGHTNO, PPAI.ARRIVAL_DATE, PPAI.DEPARTURE_INTL_FLIGHTNO, PPAI.DEPARTURE_DATE");
		sqlData.append("  )  m )");
		sqlData.append(" where r > " + fromRowNum + " and r < " + toRowNum);

		JdbcTemplate templateData = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		PaxDataList = (List<ReservationPaxDetailsDTO>) templateData.query(sqlData.toString(), new ResultSetExtractor() {

			public List<ReservationPaxDetailsDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {

				ReservationPaxDetailsDTO resPaxDTO = null;
				final List<ReservationPaxDetailsDTO> pnrPaxListSegmentWise = new ArrayList();
				while (rs.next()) {
					String pnr = rs.getString("pnr");
					resPaxDTO = new ReservationPaxDetailsDTO();
					resPaxDTO.setPnr(pnr);
					resPaxDTO.setTitle(rs.getString("TITLE"));
					resPaxDTO.setFirstName(rs.getString("FIRST_NAME"));
					resPaxDTO.setLastName(rs.getString("LAST_NAME"));
					resPaxDTO.setPaxType(rs.getString("PAX_TYPE_CODE"));
					resPaxDTO.setPnrSegId(rs.getInt("PNR_SEG_ID"));
					resPaxDTO.setReturnFlag(rs.getString("RETURN_FLAG"));
					resPaxDTO.setJourneySequence(rs.getInt("JOURNEY_SEQ"));
					resPaxDTO.setFlightSegId(rs.getInt("FLT_SEG_ID"));
					// TODO when voucher re protect is available
					 resPaxDTO.setVoucherToFlightSegID(rs.getInt("TO_FLT_SEG_ID"));
					String ffid = rs.getString("FFID");
					if (ffid != null) {
						resPaxDTO.setFFID("Y");
					} else {
						resPaxDTO.setFFID("N");
					}
					pnrPaxListSegmentWise.add(resPaxDTO);

				}

				return pnrPaxListSegmentWise;
			}
		});

		return PaxDataList;
	}

	@SuppressWarnings("unchecked")
	public Integer getPAXDetailsCount(Integer flightID) {

		int recordCount = 0;
		StringBuilder sqlData = new StringBuilder();

		sqlData = new StringBuilder();
		sqlData.append("SELECT COUNT(PP.PNR) AS RECORDCOUNT ");
		sqlData.append("FROM T_PNR_PASSENGER PP ,  T_PNR_SEGMENT PS, T_PNR_PAX_ADDITIONAL_INFO PPAI ");
		sqlData.append("WHERE PS.FLT_SEG_ID IN (");
		sqlData.append("SELECT DISTINCT FS.FLT_SEG_ID ");
		sqlData.append("FROM T_FLIGHT_SEGMENT FS ");
		sqlData.append("WHERE FS.FLIGHT_ID IN (" + flightID + ")");
		sqlData.append(") AND PP.PNR = PS.PNR ");
		sqlData.append("AND PP.PNR_PAX_ID  = PPAI.PNR_PAX_ID AND PS.STATUS = 'CNF' ");
		sqlData.append("ORDER BY PPAI.ARRIVAL_INTL_FLIGHTNO, PPAI.ARRIVAL_DATE, PPAI.DEPARTURE_INTL_FLIGHTNO, PPAI.DEPARTURE_DATE");
		JdbcTemplate templateData = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		recordCount = (Integer) templateData.query(sqlData.toString(), new ResultSetExtractor() {

			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = 0;
				while (rs.next()) {
					count = (Integer) rs.getInt("RECORDCOUNT");
				}
				return count;
			}
		});

		return recordCount;
	}

	@SuppressWarnings("unchecked")
	public List<FlightConnectionDTO> getConnectionDetials(String pnr) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT PS.PNR_SEG_ID, PS.JOURNEY_SEQ, PS.PNR, VPS.FLIGHT_NUMBER, VPS.EST_TIME_DEPARTURE_ZULU,  VPS.EST_TIME_ARRIVAL_ZULU, VPS.SEGMENT_CODE, ");
		sql.append(" VPS.EST_TIME_ARRIVAL_LOCAL, VPS.EST_TIME_DEPARTURE_LOCAL ");
		sql.append("FROM T_PNR_SEGMENT PS INNER JOIN V_PNR_SEGMENT VPS ON PS.PNR = VPS.PNR AND PS.PNR_SEG_ID = VPS.PNR_SEG_ID ");
		sql.append("WHERE PS.PNR = '" + pnr + "' AND PS.STATUS ='CNF' AND PS.PNR_SEG_ID = VPS.PNR_SEG_ID");

		List<FlightConnectionDTO> connectionList = new ArrayList<FlightConnectionDTO>();
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		connectionList = (List<FlightConnectionDTO>) template.query(sql.toString(), new ResultSetExtractor() {
			public List<FlightConnectionDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightConnectionDTO> connectionListTemp = new ArrayList<FlightConnectionDTO>();
				FlightConnectionDTO resFlightConnectionDTO = null;
				while (rs.next()) {
					resFlightConnectionDTO = new FlightConnectionDTO();
					resFlightConnectionDTO.setArrivalTime(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
					resFlightConnectionDTO.setDepartureTime(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
					resFlightConnectionDTO.setLocalArrivalTime(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
					resFlightConnectionDTO.setLocalDepartureTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
					resFlightConnectionDTO.setPnr(rs.getString("PNR"));
					resFlightConnectionDTO.setFligthNumber(rs.getString("FLIGHT_NUMBER"));
					resFlightConnectionDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
					resFlightConnectionDTO.setPnrSegId(rs.getInt("PNR_SEG_ID"));
					resFlightConnectionDTO.setJourney_seq(rs.getInt("JOURNEY_SEQ"));

					connectionListTemp.add(resFlightConnectionDTO);

				}

				return connectionListTemp;
			}
		});
		return connectionList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId){
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT f.flight_number , ");
		sb.append("fs.est_time_departure_local, fs.segment_code, ");
		sb.append("fs.est_time_arrival_local, f.flight_id, fs.flt_seg_id, ");
		sb.append("fs.est_time_arrival_zulu, fs.est_time_departure_zulu ");
		sb.append("FROM t_flight_segment fs ");
		sb.append("INNER JOIN t_flight f ");
		sb.append("ON fs.flight_id      = f.flight_id ");
		sb.append("WHERE fs.flt_seg_id IN ");
		sb.append("(SELECT ptf.flt_seg_id FROM T_PNR_TRANSFER_FLIGHTS ptf WHERE ptf.alert_id = ? )");
		
		String query = sb.toString();
		
		Collection colObj = (Collection) jdbcTemplate.query( query , new Object[]{alertId}, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				List list = new ArrayList();
				SelfReprotectFlightDTO selfReprotectFlightDTO;

				if (rs != null) {
					while (rs.next()) {
						
						selfReprotectFlightDTO = new SelfReprotectFlightDTO();
						
						selfReprotectFlightDTO.setFlightId(rs.getInt("FLIGHT_ID"));
						selfReprotectFlightDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
						selfReprotectFlightDTO.setDepartureDate(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
						selfReprotectFlightDTO.setArrivalDate(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
						selfReprotectFlightDTO.setFltSegId(rs.getString("FLT_SEG_ID"));
						selfReprotectFlightDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
						selfReprotectFlightDTO.setFlightDuration(getDuration(
								rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"),
								rs.getTimestamp("EST_TIME_ARRIVAL_ZULU")));
						
						list.add(selfReprotectFlightDTO);
						
					}
				}

				log.debug("list.size() = " + list.size());
				return list;
			}
		});
		
		return colObj;
		
	}
	
	public boolean hasActionedByIBE(String pnr){
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT alert_id ");
		sb.append("FROM t_alert ");
		sb.append("WHERE content LIKE '%" + pnr + "%' ");
		sb.append("AND ibe_actioned = 'Y'");
		
		String query = sb.toString();
		
		boolean result = (Boolean) jdbcTemplate.query( query , new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				boolean result = false;

				if (rs != null) {
					if(rs.next()){		
						result = true;
					}
				}

				return result;
			}
		});
		
		return result;
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getActiveUserDefinedSSRDetails() {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT SSR_ID,  SSR_CODE FROM T_SSR_INFO WHERE STATUS='ACT' AND USER_DEFINED_SSR='Y'");

		Object[] param = {};

		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		Map<Integer, String> ssrCountMap = (Map<Integer, String>) jt.query(sqlBuilder.toString(), param,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, String> map = new HashMap<Integer, String>();

						if (rs != null) {
							while (rs.next()) {
								map.put(rs.getInt("SSR_ID"), rs.getString("SSR_CODE"));
							}
						}

						return map;
					}
				});

		return ssrCountMap;
	}

	@Override
	public Collection<LmsBlockedCredit> getOldUnconsumedLMSBlockedCreditInfoIds(Date fromDateTime) {
		List<LmsBlockedCredit> list = null;
		try {
			String hql = "select f " + "from LmsBlockedCredit as f " + "where "
					+ "f.paymentTimeStamp < :timeStamp and f.creditUtilizedStatus = :creditStatus";

			Query q = getSession().createQuery(hql);
			if (fromDateTime != null) {
				q.setTimestamp("timeStamp", fromDateTime).setString("creditStatus", LmsBlockedCredit.ISSUE);
			}
			list = q.list();
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getLmsBlockCreditInfoByRewardId(String[] rewardIds) {
		Integer id = null;
		if (rewardIds != null) {

			String hql = "select unique LMS_CREDIT_BLOCKED_INFO_ID from T_LMS_BLOCKED_REWARD_IDS where LMS_REWARD_REF in (:rewardsIds)";

			Query query = getSession().createSQLQuery(hql).addScalar("LMS_CREDIT_BLOCKED_INFO_ID", IntegerType.INSTANCE);
			query.setParameterList("rewardsIds", Arrays.asList(rewardIds));

			List<Integer> idlist = query.list();

			if (idlist != null && !idlist.isEmpty()) {
				id = idlist.get(0);
			}
		}
		return id;
	}

	@Override
	public LmsBlockedCredit getLmsBlockCreditInfoById(Integer lmsCreditBlockedInfoId) {
		String hql = "SELECT r FROM LmsBlockedCredit r WHERE r.lmsBlockedId = ? ";
	
		
		Object[] params = { lmsCreditBlockedInfoId };
		Collection<LmsBlockedCredit> lmsBlockedCredits = find(hql, params, LmsBlockedCredit.class);
		LmsBlockedCredit lmsBlockedCredit = null;

		if (lmsBlockedCredits != null && lmsBlockedCredits.size() > 0) {
			lmsBlockedCredit = BeanUtils.getFirstElement(lmsBlockedCredits);
		}

		return lmsBlockedCredit;
	}
	
	
	/**
	 * Return the collection of PNR for a given flight
	 *
	 * @param flightNumber
	 * @param deptDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getPnrList(String flightNumber, Date deptDate) {

		Date startDate = CalendarUtil.getStartTimeOfDate(deptDate);
		Date endDate = CalendarUtil.getEndTimeOfDate(deptDate);

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT DISTINCT(PS.PNR) FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT PS ");
		sqlBuilder.append("WHERE F.FLIGHT_NUMBER= '" + flightNumber + "' AND F.DEPARTURE_DATE BETWEEN TO_DATE('"
				+ dateFormat.format(startDate) + "','dd-mm-yy HH24:mi:ss') " + " AND TO_DATE('" + dateFormat.format(endDate)
				+ "', 'dd-mm-yy HH24:mi:ss') ");
		sqlBuilder.append("AND FS.FLIGHT_ID = F.FLIGHT_ID AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND PS.STATUS='CNF'");

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Collection<String>) jt.query(sqlBuilder.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> pnrList = new ArrayList<>();
				if (rs != null) {
					while (rs.next()) {
						pnrList.add(rs.getString("PNR"));
					}
				}
				return pnrList;
			}
		});

	}

    @Override
    public void updateReservationContactData(ReservationContactInfo reservationContactInfo) {
        getSession().update(reservationContactInfo);
        getSession().flush();
    }

}
