package com.isa.thinair.airreservation.core.bl.revacc.breakdown;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.PerPaxPriceBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

/**
 * @author Nilindra Fernando
 * @since 07:17 PM 12/19/2008
 */
class TnxBreakDownFactory {

	private static ReservationPaxTnxBreakdown composeReservationPaxTnxBreakdownForPayment(BigDecimal baseAmount, Integer tnxId,
			PayCurrencyDTO payCurrencyDTO, PerPaxPriceBreakdown perPaxPriceBreakdown) throws ModuleException {

		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = createReservationPaxTnxBreakdown(baseAmount, tnxId,
				payCurrencyDTO);
		reservationPaxTnxBreakdown.setTotalPriceInPayCurrency(payCurrencyDTO.getTotalPayCurrencyAmount().negate());

		if (perPaxPriceBreakdown != null) {
			boolean isProrateNeeded = false;
			if (baseAmount.abs().doubleValue() != 0 && perPaxPriceBreakdown.getTotalBasePayAmount() != null) {
				isProrateNeeded = baseAmount.abs().doubleValue() != perPaxPriceBreakdown.getTotalBasePayAmount().doubleValue();
			}
			reservationPaxTnxBreakdown = proratePrices(baseAmount.abs(), payCurrencyDTO.getTotalPayCurrencyAmount().abs(),
					reservationPaxTnxBreakdown, perPaxPriceBreakdown, isProrateNeeded, payCurrencyDTO);
		} else {
			// Transaction is not the first payment, so set the mod amount
			reservationPaxTnxBreakdown.setModAmount(baseAmount);
			reservationPaxTnxBreakdown.setModAmountInPayCurrency(payCurrencyDTO.getTotalPayCurrencyAmount().negate());
		}

		return reservationPaxTnxBreakdown;
	}

	private static ReservationPaxTnxBreakdown proratePrices(BigDecimal actualBasePayAmount, BigDecimal payPriceAmount,
			ReservationPaxTnxBreakdown reservationPaxTnxBreakdown, PerPaxPriceBreakdown perPaxPriceBreakdown,
			boolean isProrateNeeded, PayCurrencyDTO payCurrencyDTO) throws ModuleException {

		if (isProrateNeeded) {
			// In order to handle decimal issues. The following methodology was used.
			// fare = payment * (fare/(fare+tax+sur+other)) and so on
			// sur = payment - fare portion of payment - tax portion of payment - other portion of payment

			BigDecimal totalBaseFareTaxSurOther = AccelAeroCalculator.add(perPaxPriceBreakdown.getFareAmount(),
					perPaxPriceBreakdown.getTaxAmount(), perPaxPriceBreakdown.getSurAmount(),
					perPaxPriceBreakdown.getOtherAmount());

			BigDecimal prorateBaseFare = AccelAeroCalculator.divide(
					AccelAeroCalculator.multiply(actualBasePayAmount, perPaxPriceBreakdown.getFareAmount()),
					totalBaseFareTaxSurOther);

			BigDecimal prorateBaseTax = AccelAeroCalculator.divide(
					AccelAeroCalculator.multiply(actualBasePayAmount, perPaxPriceBreakdown.getTaxAmount()),
					totalBaseFareTaxSurOther);

			BigDecimal prorateBaseOther = AccelAeroCalculator.divide(
					AccelAeroCalculator.multiply(actualBasePayAmount, perPaxPriceBreakdown.getOtherAmount()),
					totalBaseFareTaxSurOther);

			BigDecimal prorateBaseSur = AccelAeroCalculator.subtract(actualBasePayAmount,
					AccelAeroCalculator.add(prorateBaseFare, prorateBaseTax, prorateBaseOther));

			BigDecimal payFareAmount = AccelAeroRounderPolicy.convertAndRound(prorateBaseFare,
					payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
					payCurrencyDTO.getBreakPointValue());
			BigDecimal payTaxAmount = AccelAeroRounderPolicy.convertAndRound(prorateBaseTax,
					payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
					payCurrencyDTO.getBreakPointValue());
			BigDecimal payOtherAmount = AccelAeroRounderPolicy.convertAndRound(prorateBaseOther,
					payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
					payCurrencyDTO.getBreakPointValue());
			BigDecimal paySurAmount = AccelAeroCalculator.subtract(payPriceAmount,
					AccelAeroCalculator.add(payFareAmount, payTaxAmount, payOtherAmount));

			reservationPaxTnxBreakdown.setFareAmount(prorateBaseFare.negate());
			reservationPaxTnxBreakdown.setFareAmountInPayCurrency(payFareAmount.negate());

			reservationPaxTnxBreakdown.setTaxAmount(prorateBaseTax.negate());
			reservationPaxTnxBreakdown.setTaxAmountInPayCurrency(payTaxAmount.negate());

			reservationPaxTnxBreakdown.setSurchargeAmount(prorateBaseSur.negate());
			reservationPaxTnxBreakdown.setSurchargeAmountInPayCurrency(paySurAmount.negate());

			reservationPaxTnxBreakdown.setModAmount(prorateBaseOther.negate());
			reservationPaxTnxBreakdown.setModAmountInPayCurrency(payOtherAmount.negate());
		} else {
			BigDecimal payFareAmount = AccelAeroRounderPolicy.convertAndRound(perPaxPriceBreakdown.getFareAmount(),
					payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
					payCurrencyDTO.getBreakPointValue());
			BigDecimal payTaxAmount = AccelAeroRounderPolicy.convertAndRound(perPaxPriceBreakdown.getTaxAmount(),
					payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
					payCurrencyDTO.getBreakPointValue());
			BigDecimal payOtherAmount = AccelAeroRounderPolicy.convertAndRound(perPaxPriceBreakdown.getOtherAmount(),
					payCurrencyDTO.getPayCurrMultiplyingExchangeRate(), payCurrencyDTO.getBoundaryValue(),
					payCurrencyDTO.getBreakPointValue());
			BigDecimal paySurAmount = AccelAeroCalculator.subtract(payPriceAmount,
					AccelAeroCalculator.add(payFareAmount, payTaxAmount, payOtherAmount));

			reservationPaxTnxBreakdown.setFareAmount(perPaxPriceBreakdown.getFareAmount().negate());
			reservationPaxTnxBreakdown.setFareAmountInPayCurrency(payFareAmount.negate());

			reservationPaxTnxBreakdown.setTaxAmount(perPaxPriceBreakdown.getTaxAmount().negate());
			reservationPaxTnxBreakdown.setTaxAmountInPayCurrency(payTaxAmount.negate());

			reservationPaxTnxBreakdown.setSurchargeAmount(perPaxPriceBreakdown.getSurAmount().negate());
			reservationPaxTnxBreakdown.setSurchargeAmountInPayCurrency(paySurAmount.negate());

			reservationPaxTnxBreakdown.setModAmount(perPaxPriceBreakdown.getOtherAmount().negate());
			reservationPaxTnxBreakdown.setModAmountInPayCurrency(payOtherAmount.negate());
		}

		TnxGranularityUtils.normalizePaymentCurrencyAmounts(payCurrencyDTO, reservationPaxTnxBreakdown, perPaxPriceBreakdown);

		return reservationPaxTnxBreakdown;
	}

	private static ReservationPaxTnxBreakdown createReservationPaxTnxBreakdown(BigDecimal baseAmount, Integer tnxId,
			PayCurrencyDTO payCurrencyDTO) {
		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = new ReservationPaxTnxBreakdown();

		reservationPaxTnxBreakdown.setTxnId(tnxId);
		reservationPaxTnxBreakdown.setPaymentCurrencyCode(payCurrencyDTO.getPayCurrencyCode());
		reservationPaxTnxBreakdown.setTotalPrice(baseAmount);
		reservationPaxTnxBreakdown.setTimestamp(new Date());

		return reservationPaxTnxBreakdown;
	}

	protected static ReservationPaxTnxBreakdown saveReservationPaxTnxBreakdownForPayment(ReservationTnx reservationTnx,
			PayCurrencyDTO payCurrencyDTO, PerPaxPriceBreakdown perPaxPriceBreakdown) throws ModuleException {

		ReservationTnxDAO reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = composeReservationPaxTnxBreakdownForPayment(
				reservationTnx.getAmount(), reservationTnx.getTnxId(), payCurrencyDTO, perPaxPriceBreakdown);
		reservationTnxDao.saveTransaction(reservationPaxTnxBreakdown);

		return reservationPaxTnxBreakdown;
	}

	protected static void saveReservationPaxTnxBreakdownForRefund(ReservationTnx reservationTnx, PayCurrencyDTO payCurrencyDTO)
			throws ModuleException {
		TnxGranularityUtils.updatePayCurrencyDTOPayCurrencyAmount(reservationTnx, payCurrencyDTO);
		ReservationTnxDAO reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;

		BigDecimal amount = reservationTnx.getAmount();
		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = createReservationPaxTnxBreakdown(amount,
				reservationTnx.getTnxId(), payCurrencyDTO);
		reservationPaxTnxBreakdown.setTotalPriceInPayCurrency(payCurrencyDTO.getTotalPayCurrencyAmount());
		reservationPaxTnxBreakdown.setModAmount(amount);
		reservationPaxTnxBreakdown.setModAmountInPayCurrency(payCurrencyDTO.getTotalPayCurrencyAmount());
		reservationTnxDao.saveTransaction(reservationPaxTnxBreakdown);
	}
	
	protected static void saveReservationPaxTnxBreakdownForRefund(ReservationTnx reservationTnx, PayCurrencyDTO payCurrencyDTO,
			PerPaxPriceBreakdown perPaxPriceBreakdown) throws ModuleException {
		TnxGranularityUtils.updatePayCurrencyDTOPayCurrencyAmount(reservationTnx, payCurrencyDTO);
		ReservationTnxDAO reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = composeReservationPaxTnxBreakdownForRefund(
				reservationTnx.getAmount(), reservationTnx.getTnxId(), payCurrencyDTO, perPaxPriceBreakdown);
		reservationTnxDao.saveTransaction(reservationPaxTnxBreakdown);

	}

	private static ReservationPaxTnxBreakdown composeReservationPaxTnxBreakdownForRefund(BigDecimal baseAmount, Integer tnxId,
			PayCurrencyDTO payCurrencyDTO, PerPaxPriceBreakdown perPaxPriceBreakdown) throws ModuleException {

		ReservationPaxTnxBreakdown reservationPaxTnxBreakdown = createReservationPaxTnxBreakdown(baseAmount, tnxId,
				payCurrencyDTO);
		reservationPaxTnxBreakdown.setTotalPriceInPayCurrency(payCurrencyDTO.getTotalPayCurrencyAmount());

		if (perPaxPriceBreakdown != null) {
			boolean isProrateNeeded = baseAmount.abs().doubleValue() != perPaxPriceBreakdown.getTotalBasePayAmount()
					.doubleValue();
			reservationPaxTnxBreakdown = proratePrices(baseAmount.negate(), payCurrencyDTO.getTotalPayCurrencyAmount().negate(),
					reservationPaxTnxBreakdown, perPaxPriceBreakdown, isProrateNeeded, payCurrencyDTO);
		} else {
			// Transaction is not the first payment, so set the mod amount
			reservationPaxTnxBreakdown.setModAmount(baseAmount);
			reservationPaxTnxBreakdown.setModAmountInPayCurrency(payCurrencyDTO.getTotalPayCurrencyAmount());
		}

		return reservationPaxTnxBreakdown;
	}
}
