/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRResultDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRSearchDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * ReservationSSRDAO is the business DAO interface for the sales service apis
 * 
 * @since 2.0
 */
public interface ReservationSSRDAO {

	public Collection<PaxSSRDTO> getReservationSSR(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			String preferredLanguage);

	public Collection<PaxSSRDTO> getReservationSSR(Collection<Integer> pnrPaxIds, boolean isAirportService);

	public Collection<ReservationSSRResultDTO> getReservationSSR(ReservationSSRSearchDTO reservationSSRSearchDTO);

	public Map<Integer, Collection<PaxSSRDetailDTO>> getPNRPaxSSRs(Map<Integer, Integer> mapPnrPaxSegIds, boolean isToSendPNL,
			boolean isToSendEmail, boolean isExcludeSegment);

	public Map<Integer, List<AncillaryDTO>> getPasssengerSSRDetails(Map<Integer, Integer> mapPnrPaxSegIds, DCS_PAX_MSG_GROUP_TYPE msgType);

	public Map<Integer, Collection<PaxSSRDetailDTO>> getPNRPaxSSRs(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			boolean isToSendPNL, boolean isToSendEmail, boolean isExcludeSegment);

	public boolean checkSSRChargeAssignedInReservation(int ssrId) throws CommonsDataAccessException;

	/**
	 * Return expired SSR(s) based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public Map<ReservationLiteDTO, List<PaxSSRDTO>> getExpiredSSRs(Collection<Integer> cancellationIds, String marketingCarrier);

	/**
	 * Return expired lcc reservations due to SSR expire based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccSsrInfo(String marketingCarrier, Collection<Integer> cancellationIds);

	public Map<Integer, List<SSRExternalChargeDTO>> getPasssengerSSRDetails(String pnr);
}
