package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base.BaseObseverActionTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AddAirportMessagePaxTemplate extends BaseObseverActionTemplate<ObserverActionDataContext> {

	public AddAirportMessagePaxTemplate() {
		super();
	}

	@Override
	public void executeTemplateAction(ObserverActionDataContext dataContext) throws ModuleException {
		
		Map<Integer, Set<Integer>> addMap = dataContext.getAddMap();

		// del MAP remove with existing data
		checkForExist(addMap, PNLConstants.AdlActions.A, dataContext.getNewReservation().getPnr());

		List<AirportMessagePassenger> paxList = createUnexistPax(addMap, dataContext.getNewReservation(),
				PNLConstants.AdlActions.A.toString());
		
		paxList.addAll(alreadyExistpaxListToUpdate);
		if(!paxList.isEmpty()){
			auxilliaryDAO.saveAirportMessagePassenger(paxList);
		}
	}

}
