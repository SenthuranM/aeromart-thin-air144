package com.isa.thinair.airreservation.core.bl.pnr;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.AutoCancellationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for removing auto cancellation flag from given PNR
 * 
 * @author rumesh
 * @isa.module.command name="removeAutoCancellation"
 */
public class RemoveAutoCancellation extends DefaultBaseCommand {

	private static Log log = LogFactory.getLog(RemoveAutoCancellation.class);

	@Override
	public ServiceResponce execute() throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Inside Remove auto cancellation");
		}

		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		String oldPnr = (String) this.getParameter(CommandParamNames.OLD_PNR);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);

		// If call from remove pax flow, should take old pnr
		String newPnr = null;
		if (oldPnr != null && !"".equals(oldPnr)) {
			newPnr = pnr;
			pnr = oldPnr;
		}

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		this.validateParams(pnr, credentialsDTO);

		if (AppSysParamsUtil.isAutoCancellationEnabled()) {

			// Loads the reservation object
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())
					&& reservation.getTotalAvailableBalance().doubleValue() <= AccelAeroCalculator.getDefaultBigDecimalZero()
							.doubleValue()) {
				AutoCancellationBO.removeAutoCancellationFlag(reservation);
			}

			// Removing auto cancellation flags from removed PNRs
			if (newPnr != null) {
				pnrModesDTO.setPnr(newPnr);
				reservation = ReservationProxy.getReservation(pnrModesDTO);
				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
					AutoCancellationBO.removeAutoCancellationFlag(reservation);
				}
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("Exit Remove auto cancellation");
		}

		if(this.getParameter(CommandParamNames.BOOKING_TYPE) != null){
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		if (this.getParameter(CommandParamNames.SPLITTED_PAX_IDS) != null) {
			response.addResponceParam(CommandParamNames.SPLITTED_PAX_IDS, this.getParameter(CommandParamNames.SPLITTED_PAX_IDS));
		}
		return response;
	}

	private void validateParams(String pnr, CredentialsDTO credentialsDTO) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Inside validateParams");
		}

		if (pnr == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		if (log.isDebugEnabled()) {
			log.debug("Exit validateParams");
		}
	}
}
