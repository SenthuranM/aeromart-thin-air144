/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;

/**
 * @author udithad
 *
 */
public class OnwardConnectionsInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	protected ReservationAuxilliaryDAO reservationAuxilliaryDAO;
	protected String departureAirportCode;
	protected Integer flightId;

	public OnwardConnectionsInformation(String departureAirportCode,
			Integer flightId) {
		this.departureAirportCode = departureAirportCode;
		this.flightId = flightId;
		reservationAuxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
	}

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> passengerInformations) {
		for (PassengerInformation passengerInformation : passengerInformations) {
			passengerInformation
					.setOnwardconnectionlist(getOnwardConnectionsBy(passengerInformation
							.getPnrPaxId()));
		}
	}

	private ArrayList<OnWardConnectionDTO> getOnwardConnectionsBy(
			Integer passengerId) {
		return this.onwardConnectionsInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformations) {

		for (PassengerInformation passengerInformation : passengerInformations) {
			ArrayList<OnWardConnectionDTO> onwardConnections = (ArrayList<OnWardConnectionDTO>) reservationAuxilliaryDAO
					.getAllOutBoundSequences(
							passengerInformation.getPnr(),
							passengerInformation.getPnrPaxId(),
							departureAirportCode,
							flightId,
							passengerInformation.getPnrStatus()
									.equalsIgnoreCase("CNF")
									& (passengerInformation.getPnlStatus()
											.equalsIgnoreCase("N") | passengerInformation
											.getPnlStatus().equalsIgnoreCase(
													"C")));
			this.onwardConnectionsInformation.put(
					passengerInformation.getPnrPaxId(), onwardConnections);
		}
	}

}
