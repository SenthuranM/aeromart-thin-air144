/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for making a reservation modification
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="recordModification"
 */
public class RecordModification extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(RecordModification.class);

	/**
	 * Execute method of the RecordModification command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Collection<ReservationAudit> colReservationAudit = (Collection<ReservationAudit>) this
				.getParameter(CommandParamNames.RESERVATION_AUDIT_COLLECTION);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		// This will used for pax credit payments for other carriers
		Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) this
				.getParameter(CommandParamNames.PAX_CREDIT_PAYMENTS);
		Boolean isGoShowProcess = (Boolean) this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS);

		if (isGoShowProcess == null) {
			isGoShowProcess = new Boolean(false);
		}

		// Checking params
		this.validateParams(colReservationAudit, credentialsDTO);

		Iterator<ReservationAudit> itColReservationAudit = colReservationAudit.iterator();
		ReservationAudit reservationAudit;

		while (itColReservationAudit.hasNext()) {
			reservationAudit = itColReservationAudit.next();

			ReservationAudit.createReservationAudit(reservationAudit, credentialsDTO, isGoShowProcess);
		}

		// Get the auditor delegate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();

		// Saves the audit collection
		if (colReservationAudit != null && !colReservationAudit.isEmpty()) {
			auditorBD.audit(colReservationAudit);
		}

		// Clear reservation audit. Reason when utilizing pax credit we are recording the audit just after that
		// because if it is initiated from some other carrier this will be the only place to record the audit.
		// But for own airline this will be executed twice. To avoid that removing already logged audits
		colReservationAudit.clear();

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		// Added this to response params for LCC auditing purpose.
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.PAX_CREDIT_PAYMENTS, paxCreditPayments);
		response.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_IDS, this.getParameter(CommandParamNames.FLIGHT_SEGMENT_IDS));
		response.addResponceParam(CommandParamNames.PNR, this.getParameter(CommandParamNames.PNR));

		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap = (Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>>) this
				.getParameter(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP);
		if (updatedFlexibilitiesMap != null && !updatedFlexibilitiesMap.isEmpty()) {
			response.addResponceParam(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP, updatedFlexibilitiesMap);
		}
		if (this.getParameter(CommandParamNames.SPLITTED_PAX_IDS) != null) {
			response.addResponceParam(CommandParamNames.SPLITTED_PAX_IDS, this.getParameter(CommandParamNames.SPLITTED_PAX_IDS));
		}

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param colReservationAudit
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(Collection<ReservationAudit> colReservationAudit, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (colReservationAudit == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

}
