package com.isa.thinair.airreservation.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;

@Local
public interface ReservationAuxilliaryServiceBeanLocal extends ReservationAuxilliaryBD {

}
