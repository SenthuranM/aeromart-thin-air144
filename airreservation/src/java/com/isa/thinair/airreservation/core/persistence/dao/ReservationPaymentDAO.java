/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.ReservationPaxSegmentPaymentTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CreditCardInformationDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PaymentDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaymentModeDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPaymentDTO;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;

/**
 * ReservationPaymentDAO is the business DAO interface for the reservation payment apis
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationPaymentDAO {

	/**
	 * Save tempory payment information
	 * 
	 * @param paymentInfo
	 */
	public void saveTempPaymentInfo(TempPaymentTnx paymentInfo);

	/**
	 * Returns temport payment information
	 * 
	 * @param tnxId
	 *            throws CommonsDataAccessException
	 * @return
	 */
	public TempPaymentTnx getTempPaymentInfo(int tnxId) throws CommonsDataAccessException;

	/**
	 * Returns temporary payments per pnr
	 * 
	 * @param pnr
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr) throws CommonsDataAccessException;

	/**
	 * 
	 * @param pnr
	 * @param status
	 * @param productType
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr, String status, char productType)
			throws CommonsDataAccessException;

	/**
	 * 
	 * @param pnr
	 * @param productType
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr, char productType)
			throws CommonsDataAccessException;

	/**
	 * Save the credit card detail
	 * 
	 * @param creditCardDetail
	 */
	public void saveCreditCardDetail(CreditCardDetail creditCardDetail);

	/**
	 * Return credit card detail information
	 * 
	 * @param ccdId
	 *            throws CommonsDataAccessException
	 * @return
	 */
	public CreditCardDetail getCreditCardDetail(int ccdId) throws CommonsDataAccessException;

	/**
	 * Retruns Collection of CreditCardDetails for Paymernt Brodker Ref id
	 * 
	 * @param paymentBrokerRefId
	 * @return
	 */
	public Collection<CreditCardDetail> getCreditCardDetails(int paymentBrokerRefId);

	/**
	 * Return Pnrs for the credit card information
	 * 
	 * @param no
	 * @param eDate
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<String> getPnrsForCreditCardInfo(String no, String eDate, String ccAuthCode)
			throws CommonsDataAccessException;

	/**
	 * Return credit card detail information
	 * 
	 * @param colTnxIds
	 * @param isOwnTnx
	 *            TODO
	 * @return
	 */
	public Map<Integer, CardPaymentInfo> getCreditCardInfo(Collection<Integer> colTnxIds, boolean isOwnTnx);

	/**
	 * Return Credit card information
	 * 
	 * @param colTnxIds
	 * @param isOwnTnx
	 *            TODO
	 * @return
	 */
	public Map<Integer, CardPaymentInfo> getCreditCardInfoFromRESPCD(Collection<Integer> colTnxIds, boolean isOwnTnx);

	/**
	 * Return TempPayment Details
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 */
	public Collection<TempPaymentTnx> getTempPaymentDetails(int paymentBrokerRefNo);

	/**
	 * Returns credit card information
	 * 
	 * @return collection of CreditCardInformationDTO
	 */
	public Collection<CreditCardInformationDTO> getCreditCardInformation();

	/**
	 * Returns map key --> TnxId(java.lang.Integer) Value --> Authorization Id(java.lang.String)
	 * 
	 * @param colTnxIds
	 * @return
	 */
	public Map<Integer, String> getAuthorizationIdsForCCPayments(Collection<Integer> colTnxIds);

	/**
	 * Save/Update External Payment Transaction Information
	 * 
	 * @param externalPaymentTnx
	 */
	public void saveOrUpdateExternalPayTxInfo(ExternalPaymentTnx externalPaymentTnx);

	/**
	 * Return external payment transactions for the specified criteria.
	 * 
	 * @param criteriaDTO
	 * @return Map<PNR, PNRExtTransactionsTO>
	 */
	public Map<String, PNRExtTransactionsTO> getExtPayTransactions(ExtPayTxCriteriaDTO criteriaDTO);

	/**
	 * Returns Tempory Payment Information
	 * 
	 * @param colTnxId
	 * @return
	 */
	public Collection<TempPaymentTnx> getTempPaymentInfo(Collection<Integer> colTnxId);

	/**
	 * Save Tempory Payment Info All
	 * 
	 * @param colTempPaymentInfo
	 */
	public void saveTempPaymentInfoAll(Collection<TempPaymentTnx> colTempPaymentInfo);

	/**
	 * Returns the Detail PNR Pax Payments
	 * 
	 * @param strPnrPaxIds
	 * @param lstNominalCodes
	 * @return
	 */
	public Map<Integer, Collection<PaymentDetailDTO>> getDetailPNRPaxPayments(Collection<Integer> strPnrPaxIds,
			List<Integer> lstNominalCodes);

	/**
	 * Returns the tempory payment information
	 * 
	 * @param date
	 * @param colStatus
	 * @param colStateHistory
	 * @param colIPGIdentificationDTO
	 * @return
	 */
	public Collection<IPGQueryDTO> getTemporyPaymentInfo(Date date, Collection<String> colStatus,
			Collection<String> colStateHistory, Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO);

	/**
	 * Returns the IPGQueryDTO
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 */
	public IPGQueryDTO getTemporyPaymentInfo(int paymentBrokerRefNo);

	public String getNextCommonRecieptNumber();

	public String getNextUniqueRecieptNumber(String paymentMode);

	public Map<String, PaymentModeDTO> getPaymentModes();

	public void removeReservationPaxOndPayments(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment);

	public void saveReservationPaxOndPayment(Collection<ReservationPaxOndPayment> colReservationPaxOndPayment);

	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPaymentByOndChargeId(Integer pnrPaxId,
			Collection<Long> colPnrPaxOndChgId);

	public Map<Integer, Map<Long, Collection<ReservationPaxOndPayment>>> getPerPaxWiseOndChargeIds(Collection<Integer> pnrPaxIds);

	public Map<Integer, Collection<ReservationPaxOndPayment>> getPerPaxPaymentWiseOndPayments(Integer pnrPaxId,
			Collection<Long> paxTnxIds);

	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPayments(Collection<Long> colPaymentTnxIds);

	public String getActualPaymentMethodNameById(Integer actualPaymentMethodId);

	/**
	 * save or update ReservationPaxExtTnx
	 * 
	 * @param paxExtTnx
	 */
	public void saveReservationPaxExtTnx(ReservationPaxExtTnx paxExtTnx);

	/**
	 * Returns the external payments for a given pax
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	public Collection<ReservationPaxExtTnx> getExternalPaxPayments(Integer pnrPaxId);

	/**
	 * Return external payment transactions for the specified Request UID.
	 * 
	 * @param ReqUID
	 * @return Map<PNR, PNRExtTransactionsTO>
	 */
	public Map<String, PNRExtTransactionsTO> getExtPayTransactionsForReqUID(String ReqUID);

	/**
	 * Retrieves the payment breakdown for Own Reservations for per pax per segment.
	 * 
	 * @param paxSeqLccUniqueTIDPairCollection
	 *            : pax_tnx_id collection for which the breakdown is required.
	 * 
	 * @return : A map of pax segment payment breakdown who's key is a {@link Pair} object with left as pax_tnx_id and
	 *         the right as pax sequence. The value for a given key is a Collection of
	 *         {@link ReservationPaxSegmentPaymentTO} object(s).
	 * 
	 * @see : {@link ReservationPaxSegmentPaymentTO}
	 */
	public Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>
			getReservationPaxSegmentPaymentsForOwnReservation(Collection<Integer> colPaymentTnxIds,
					Collection<Integer> requestingNominalCodes);

	/**
	 * Retrieves the payment breakdown for Group Reservations (i.e Interline and dry) for per pax per segment.
	 * 
	 * @param paxSeqLccUniqueTIDPairCollection
	 *            : The LCC unique transaction ID collection for which the breakdown is required.
	 * 
	 * @return : A map of pax segment payment breakdown who's key is a {@link Pair} object with left as lcc unique tnx
	 *         id and the right as pax sequence. The value for a given key is a Collection of
	 *         {@link ReservationPaxSegmentPaymentTO} object(s).
	 * 
	 * @see : {@link ReservationPaxSegmentPaymentTO}
	 */
	public Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>>
			getReservationPaxSegmentPaymentsForGroupReservation(Collection<String> paxSeqLccUniqueTIDs,
					Collection<Integer> requestingNominalCodes);

	/**
	 * Retrieves the handling charge per transaction per pax for the given set of pax tnx ids.
	 * 
	 * @param transactionIds
	 *            : The pax_tnx_id list
	 * @param handlingChargeCode
	 *            : The handling charge code.
	 * 
	 * @return A map of handling fees where the key is a {@link Pair} object who's left is the pax tnx id and right is
	 *         the pax sequence.
	 */
	public Map<Pair<String, Integer>, BigDecimal> getHandlingChargesForOwnResPaxTnxs(Collection<Integer> transactionIds,
			String handlingChargeCode);

	/**
	 * Retrieves the handling charge per transaction per pax for the given set of LCC Transaction IDs.
	 * 
	 * @param transactionIds
	 *            : The lcc Unique transaction IDs.
	 * @param handlingChargeCode
	 *            : The handling charge code.
	 * 
	 * @return : A map of handling fees where the key is a {@link Pair} object who's left is the Lcc Unique Tnx Id and
	 *         right is the pax sequence.
	 */
	public Map<Pair<String, Integer>, BigDecimal> getHandlingChargesForGroupResPaxTnxs(Collection<String> transactionIds,
			String handlingChargeCode);

	/**
	 * Gets the refundable payment details. Paid payments(positive amount) and refunded payments(negative amount) both.
	 * Current implementation ignores the charge code if it is null. Otherwise it will change the query to include it.
	 * 
	 * @param pnrPaxId
	 *            Particular passengers pnrPaxID
	 * @param chargeType
	 *            Charge type (TAX,SUR etc.)
	 * @param ppfIDs
	 *            passengers Pnr Pax Fare IDs. Irrelevant ids for the passed pnrPaxID will be ignored in the implemented
	 *            query
	 * @param chargeCode
	 *            Charge code of the payment entries to be retrieved.
	 * @return List of {@link ReservationPaxOndPayment}
	 */
	public List<ReservationPaxOndPayment> getRefundablePayment(String pnrPaxId, String chargeType, Collection<String> ppfIDs,
			String chargeCode);

	public List<PNRGOVPaymentDTO> getPaxPaymentDTOs(String pnrPaxID);

	public Collection<Long> getPaxCreditTransactionIds(Collection<Long> colPaymentTnxIds);

	public boolean isPaymentReferenceExist(String paymentReference);

	public Map<String, String> getAgentPayments(String paymentReference);

	public Long getCreditReInstatedTnxIds(Long paymentTnxId);

	public Collection<ReservationPaxOndPayment> getCreditExpiredPaxOndPayments(Long colPaymentTnxId);

	public Map<Long, String> getChargeCodesForPaxOndPayments(Collection<Integer> ppocIds);

	public Integer getPNRPaxMaxPaymentTnxSequence(Integer pnrPaxId);

	public BigDecimal getExternalReferenceGOQUOPayments(String pnr);

	public Integer getPNRMaxPaymentTnxSequence(String pnr);

	public Integer getPNRMaxChargeTnxSequence(String pnr);

	public Integer getPNRPaxMaxChargeTnxSequence(Integer pnrPaxId);
}
