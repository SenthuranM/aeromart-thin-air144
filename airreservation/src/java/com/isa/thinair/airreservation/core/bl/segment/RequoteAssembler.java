package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.ResUniqueIdMapper;
import com.isa.thinair.airproxy.api.utils.ReservationAssemblerConvertUtil;
import com.isa.thinair.airproxy.api.utils.assembler.CommonAssemblerUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IResSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.SortUtil;
import com.isa.thinair.airreservation.core.bl.common.ValidationUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class RequoteAssembler implements IResSegment {

	/**
	 * Input
	 */
	private final RequoteModifyRQ requoteModifyRQ;
	private final CredentialsDTO credentials;
	private TrackInfoDTO trackInfo;

	/**
	 * derived information
	 */
	private Collection<OndFareDTO> ondFares = null;
	private final Set<Integer> cnxPnrSegIds;
	private Set<Integer> newFltSeg;
	private final Map<Integer, ReservationSegment> fltSegIdPnrSegmentMap = new HashMap<Integer, ReservationSegment>();
	private HashSet<ReservationSegment> reservationSegments;
	private boolean pnrSegmentProcessed = false;

	/**
	 * BDs and other assisting data
	 */
	private ReservationQueryBD resQueryBD = null;
	private final ModifyAssitUnit modifyAsst;
	private final FlownAssitUnit flownAsst;
	private boolean validityCalculated;
	private Date ticketValidity;
	private Date lastFareQuotedDate;
	private Collection<Integer> validityApplicableFltSegIds = new ArrayList<Integer>();

	/**
	 * 
	 * @param requoteModifyRQ
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public RequoteAssembler(RequoteModifyRQ requoteModifyRQ, CredentialsDTO credentialsDTO) throws ModuleException {
		this.requoteModifyRQ = requoteModifyRQ;
		this.credentials = credentialsDTO;

		Collection<Integer> newFltSegIds = getNewFlightSegmentIds();
		this.cnxPnrSegIds = ResUniqueIdMapper.getPnrSegIds(requoteModifyRQ.getRemovedSegmentIds());
		this.modifyAsst = new ModifyAssitUnit(cnxPnrSegIds, newFltSegIds, requoteModifyRQ.getPnr());

		boolean isSkipPenalty = ValidationUtils.isSkipPenaltyCalculation(getOndFares(), modifyAsst,
				requoteModifyRQ.getOldFareIdByFltSegIdMap(), requoteModifyRQ.getOndFareTypeByFareIdMap());
		boolean skipPenaltyWithinValidity = requoteModifyRQ.isFQWithinValidity()
				&& AppSysParamsUtil.isApplyPenaltyWithinValidity();

		if (!modifyAsst.isUserCancellation()) {
			skipPenaltyWithinValidity = false;
		}
		
		// skipPenaltyWithinValidity flag is applicable only when add/modify segment,not valid for cancel segment
		if (modifyAsst.isUserCancellation()) {
			skipPenaltyWithinValidity = false;
		}

		this.flownAsst = new FlownAssitUnit(this.modifyAsst, true, getOndFares(), (isSkipPenalty || skipPenaltyWithinValidity),
				requoteModifyRQ.getOndFareTypeByFareIdMap());
		if (requoteModifyRQ.getLastFareQuoteDate() == null) {
			this.lastFareQuotedDate = CalendarUtil.getCurrentSystemTimeInZulu();
		} else {
			this.lastFareQuotedDate = requoteModifyRQ.getLastFareQuoteDate();
		}
	}

	public FlownAssitUnit getFlownAssitUnit() {
		return flownAsst;
	}

	public Collection<Integer> getBCNonChangedExchangedFlightSegIds() throws ModuleException {
		Set<Integer> fltSegIds = new HashSet<Integer>();
		if (this.requoteModifyRQ.hasNewSegments()) {
			for (OndFareDTO ondFare : getOndFares()) {
				for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
					boolean isSame = false;
					for (SegmentSeatDistsDTO seatDist : ondFare.getSegmentSeatDistsDTO()) {
						if (seatDist.getSeatDistribution() != null && seatDist.getSeatDistribution().size() == 1
								&& seatDist.isSameBookingClassAsExisting()) {
							isSame = true;
							break;
						}
					}
					if (isSame) {
						fltSegIds.add(segSeatDist.getFlightSegId());
					}
				}
			}
		}
		return fltSegIds;
	}

	public Collection<Integer> getNewFlightSegmentIds() throws ModuleException {
		if (newFltSeg == null) {
			if (this.requoteModifyRQ.hasNewSegments()) {
				newFltSeg = new HashSet<Integer>();
				for (OndFareDTO ondFare : getOndFares()) {
					newFltSeg.addAll(ondFare.getFlightSegmentIds());
				}
			}
		}
		return newFltSeg;
	}

	private void processPnrSegmentsForNewFltSegments() throws ModuleException {
		if (!pnrSegmentProcessed) {
			int nextSegSequence = modifyAsst.getNextSegmentSequence();
			int nextOndFareGroup = modifyAsst.getNextOndFareGroup();
			int nextReturnFareGroup = modifyAsst.getNextReturnFareGroup();
			int nextJouneryGroup = modifyAsst.getNextJourneyGroup();
			List<OndFareDTO> ondFares = null;
			if (getOndFares() != null) {
				ondFares = new ArrayList<OndFareDTO>(getOndFares());
				Iterator<OndFareDTO> ondFareItr = ondFares.iterator();
				boolean hasFlwnSegments = false;
				while (ondFareItr.hasNext()) {
					if (getFlownAssitUnit().isFlownOnd(ondFareItr.next())) {
						ondFareItr.remove();
						hasFlwnSegments = true;
					}
				}

				if (hasFlwnSegments) {
					SortUtil.sortByOndSequnce(ondFares);
					int ondSequnce = 0;
					for (OndFareDTO ondFare : ondFares) {
						ondFare.setOndSequence(ondSequnce);
						ondSequnce++;
					}
				}
			}

			Map<Integer, String> flightSegIdWiseBaggageOndGrpId = getFlightSegIdWiseBaggageOndGrpIds(modifyAsst.getReservation()
					.getSegmentsView());

			mergeFlightSedIdWiseBaggageOndGroups(flightSegIdWiseBaggageOndGrpId);

			ReservationAssemblerConvertUtil.populateOndSegments(this, ondFares, null, null, nextSegSequence, nextOndFareGroup,
					nextReturnFareGroup, nextJouneryGroup, flightSegIdWiseBaggageOndGrpId, null,
					this.requoteModifyRQ.getCodeShareFlightDTOs());
			pnrSegmentProcessed = true;
		}

	}

	private void mergeFlightSedIdWiseBaggageOndGroups(Map<Integer, String> flightSegIdWiseBaggageOndGrpId) {
		Map<String, String> fltRefWiseBaggageGroups = requoteModifyRQ.getFlightRefWiseOndBaggageGroup();
		if (fltRefWiseBaggageGroups != null && !fltRefWiseBaggageGroups.isEmpty()) {
			for (Entry<String, String> fltRefEntry : fltRefWiseBaggageGroups.entrySet()) {
				String fltRefNum = fltRefEntry.getKey();
				String ondBaggageGroup = fltRefEntry.getValue();
				if (fltRefNum != null && !fltRefNum.equals("")) {
					Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNum);
					if (!flightSegIdWiseBaggageOndGrpId.containsKey(fltSegId)
							|| flightSegIdWiseBaggageOndGrpId.get(fltSegId) == null) {
						flightSegIdWiseBaggageOndGrpId.put(fltSegId, ondBaggageGroup);
					}
				}
			}
		}
	}

	private Map<Integer, String> getFlightSegIdWiseBaggageOndGrpIds(Collection<ReservationSegmentDTO> segmentsView) {
		Map<Integer, String> flightSegIdWiseBaggageOndGrpId = new HashMap<Integer, String>();

		if (segmentsView != null && segmentsView.size() > 0) {
			for (ReservationSegmentDTO reservationSegmentDTO : segmentsView) {
				flightSegIdWiseBaggageOndGrpId.put(reservationSegmentDTO.getFlightSegId(),
						reservationSegmentDTO.getBaggageOndGroupId());
			}
		}

		return flightSegIdWiseBaggageOndGrpId;
	}

	@Override
	public void addGroundStationDataToPNRSegment(Map<Integer, String> groundStationBySegmentMap,
			Map<Integer, Integer> linkedGroundSegmentFlightRefMap) throws ModuleException {
		CommonAssemblerUtil.addGroundStationDataToPNRSegment(groundStationBySegmentMap, linkedGroundSegmentFlightRefMap,
				this.getSegmentObjectsMap());
	}

	@Override
	public void addOndSegment(int ondSequence, int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			Date openRtConfirmBefore, String externalRef, Integer baggageOndGroupId, boolean waitListing, boolean isReturnOnd,
			Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass, String csOcCarrierCode,
			String csOcFlightNumber) {
		if (reservationSegments == null) {
			reservationSegments = new HashSet<ReservationSegment>();
		}

		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(isReturnOnd ? ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE
				: ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setOpenReturnConfirmBeforeZulu(openRtConfirmBefore);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setLastFareQuotedDateZulu(getLastFareQuotedDate());
		pnrSeg.setTicketValidTill(getTicketValidTill(openRtConfirmBefore != null));
		pnrSeg.setTicketValidTill(getTicketValidTillByFltSegId(flightSegId));
		pnrSeg.setJourneySequence(ondSequence);
		pnrSeg.setBaggageOndGroupId(baggageOndGroupId);
		pnrSeg.setBundledFarePeriodId(bundledFarePeriodId);
		if (codeShareFlightNumber != null && codeShareBookingClass != null) {
			pnrSeg.setCodeShareFlightNo(codeShareFlightNumber);
			pnrSeg.setCodeShareBc(codeShareBookingClass);
		}
		pnrSeg.setCsOcCarrierCode(csOcCarrierCode);
		pnrSeg.setCsOcFlightNumber(csOcFlightNumber);
		injectSegmentCredentials(pnrSeg);

		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);

		fltSegIdPnrSegmentMap.put(new Integer(flightSegId), pnrSeg);

		reservationSegments.add(pnrSeg);
	}

	public void injectSegmentCredentials(ReservationSegment reservationSegment) {
		if (reservationSegment != null && this.credentials != null) {
			ReservationApiUtils.captureSegmentStatusChgInfo(reservationSegment, this.credentials);
		}
	}

	public Date getTicketValidTill(boolean openReturn) {
		if (!validityCalculated) {
			if (ondFares != null && (AppSysParamsUtil.isTicketValidityEnabled() || openReturn)) {
				Date arr[] = ReservationApiUtils.calculateValidities(ondFares, null, null, null, validityApplicableFltSegIds);
				this.ticketValidity = arr[0];
			}
			validityCalculated = true;
		}
		return ticketValidity;
	}

	private Date getTicketValidTillByFltSegId(Integer fltSegId) {

		if (fltSegId != null && validityApplicableFltSegIds != null && validityApplicableFltSegIds.size() > 0) {
			if (validityApplicableFltSegIds.contains(fltSegId)) {
				return ticketValidity;
			}
		}

		return null;
	}

	public boolean updateFlownTicketValidity() throws ModuleException {
		return modifyAsst.hasFlownPnrSegments();
	}

	private Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public Collection<Integer> getCnxPnrSegmentIds() {
		return cnxPnrSegIds;
	}

	private ReservationQueryBD getReservationQueryBD() {
		if (resQueryBD == null) {
			resQueryBD = ReservationModuleUtils.getReservationQueryBD();
		}
		return resQueryBD;
	}

	public Collection<OndFareDTO> getOndFares() throws ModuleException {
		if (ondFares == null && requoteModifyRQ.getFareInfo() != null && requoteModifyRQ.getFareInfo().hasOndFares()) {
			ondFares = new ArrayList<OndFareDTO>();
			for (OndRebuildCriteria ondRebuildCriteria : requoteModifyRQ.getFareInfo().getOndRebuildCriterias()) {
				ondFares.addAll(getReservationQueryBD().recreateFareSegCharges(ondRebuildCriteria));
			}
		} else if (ondFares == null
				&& (AppSysParamsUtil.isSkipNonModifiedSegFareONDs() || AppSysParamsUtil.isReQuoteOnlyRequiredONDs())) {
			ondFares = new ArrayList<OndFareDTO>();
		}

		return ondFares;
	}

	public boolean isOpenReturn() throws ModuleException {
		if (ondFares != null) {
			return (OndFareUtil.getOpenReturnPeriods(getOndFares()).getConfirmBefore() != null);
		}
		return false;
	}

	public Float getFareDiscountInfo() {
		if (requoteModifyRQ.getFareInfo() != null && requoteModifyRQ.getFareInfo().getDiscountedFareDetails() != null) {
			return requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getFarePercentage();

		}
		return null;
	}

	public DiscountedFareDetails getFareDiscountDTOInfo() {
		if (requoteModifyRQ.getFareInfo() != null && requoteModifyRQ.getFareInfo().getDiscountedFareDetails() != null) {
			return requoteModifyRQ.getFareInfo().getDiscountedFareDetails();

		}
		return null;
	}

	public String getFareDiscountCode() {
		if (AppSysParamsUtil.enableFareDiscountCodes() && requoteModifyRQ.getFareInfo() != null
				&& requoteModifyRQ.getFareInfo().getDiscountedFareDetails() != null) {
			return requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getCode();

		}
		return null;
	}

	public Reservation getReservation() throws ModuleException {
		return modifyAsst.getReservation();
	}

	public void setTrackInfo(TrackInfoDTO trackInfo) {
		this.trackInfo = trackInfo;
	}

	public CredentialsDTO getCredentials() {
		return credentials;
	}

	public TrackInfoDTO getTrackInfo() {
		return trackInfo;
	}

	public boolean isTriggerBlockSeats() {
		if (requoteModifyRQ.getBlockSeats() == null) {
			return true;
		}
		return false;
	}

	public Collection<TempSegBcAlloc> getBlockSeats() {
		return requoteModifyRQ.getBlockSeats();
	}

	public Collection<ReservationSegment> getExternalSegments() {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<Integer, IPayment> getPassengerPaymentsMap() throws ModuleException {

		Map<Integer, IPayment> pnrPaxIdPaymentMap = new HashMap<Integer, IPayment>();
		for (String uniqueId : requoteModifyRQ.getPassengerPaymentMap().keySet()) {
			pnrPaxIdPaymentMap.put(ResUniqueIdMapper.getPnrPaxId(uniqueId), requoteModifyRQ.getPassengerPaymentMap()
					.get(uniqueId));
		}
		if (pnrPaxIdPaymentMap.isEmpty()) {
			if (!modifyAsst.isUserCancellation() && !modifyAsst.isUserModification()) {
				for (ReservationPax pax : getReservation().getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType()) || requoteModifyRQ.isInfantPaymentSeparated()) {
						PaymentAssembler paymentAssm = new PaymentAssembler();
						paymentAssm.addCashPayment(BigDecimal.ZERO, null, null, null, null);
						pnrPaxIdPaymentMap.put(pax.getPnrPaxId(), paymentAssm);
					}
				}
			}
		}

		return pnrPaxIdPaymentMap;
	}

	public Map<Integer, ReservationSegment> getSegmentObjectsMap() throws ModuleException {
		processPnrSegmentsForNewFltSegments();
		return fltSegIdPnrSegmentMap;
	}

	public Collection<ReservationSegment> getReservationSegments() throws ModuleException {
		processPnrSegmentsForNewFltSegments();
		return reservationSegments;
	}

	public boolean isEnableTransactionGranularity() throws ModuleException {
		return getReservation().isEnableTransactionGranularity();
	}

	public Collection<Integer> getExchangedPnrSegIds() throws ModuleException {
		return modifyAsst.getExchangedPnrSegIds();
	}

	public boolean isUserCancellation() throws ModuleException {
		return modifyAsst.isUserCancellation();
	}

	public boolean isUserModification() throws ModuleException {
		return modifyAsst.isUserModification();
	}
	
	public boolean applyModificationCharge(Reservation reservation) throws ModuleException {
		if (modifyAsst.isUserModification() && AppSysParamsUtil.skipMODChargesForOpenSegmentModification()
				&& modifyAsst.skipMODChargeForOpenSegmentModification(ReservationApiUtils.isOpenReturnBooking(reservation))) {

			return false;
		}
		return modifyAsst.isUserModification();
	}

	public boolean isSameFlightModification() {
		return requoteModifyRQ.isSameFlightModification();
	}

	public boolean isAddOndModification() throws ModuleException {
		return requoteModifyRQ.isAddSegment();
	}

	public String getUserNotes() {
		if (requoteModifyRQ.getFareInfo() != null && requoteModifyRQ.getFareInfo().getDiscountedFareDetails() != null
				&& requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes() != null
				&& !requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes().isEmpty()) {
			if (requoteModifyRQ.getUserNotes() == null) {
				requoteModifyRQ.setUserNotes("FareDiscountNote: "
						+ requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes());
			} else {
				requoteModifyRQ.setUserNotes(requoteModifyRQ.getUserNotes() + ", FareDiscountNote: "
						+ requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes());
			}

		}
		return requoteModifyRQ.getUserNotes();
	}

	public String getEndorsementNotes() {
		if (requoteModifyRQ.getFareInfo() != null && requoteModifyRQ.getFareInfo().getDiscountedFareDetails() != null
				&& requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes() != null) {
			if (requoteModifyRQ.getEndorsementNotes() == null) {
				requoteModifyRQ.setEndorsementNotes("FareDiscountNote: "
						+ requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes());
			} else {
				requoteModifyRQ.setEndorsementNotes(requoteModifyRQ.getEndorsementNotes() + ", FareDiscountNote: "
						+ requoteModifyRQ.getFareInfo().getDiscountedFareDetails().getNotes());
			}
		}
		return requoteModifyRQ.getEndorsementNotes();
	}

	public CustomChargesTO getCustomCharges() {
		if (requoteModifyRQ.getCustomCharges() != null) {
			return requoteModifyRQ.getCustomCharges();
		}
		return new CustomChargesTO();
	}

	public Collection<Integer> getFlownPnrSegIds() throws ModuleException {
		return modifyAsst.getFlownPnrSegIds();
	}

	public Collection<Integer> getValidityApplicableFltSegIds() {
		return validityApplicableFltSegIds;
	}

	public String getUserNoteType() {
		return requoteModifyRQ.getUserNoteType();
	}
}
