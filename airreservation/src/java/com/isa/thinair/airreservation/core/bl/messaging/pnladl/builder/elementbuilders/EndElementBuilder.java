/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders;

import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.ElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.EndElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.UtilizedPassengerContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.base.BaseElementBuilder;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.EndElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.HeaderElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.PnrElementRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.BaseRuleExecutor;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.base.RuleResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.PnrElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants;
import com.isa.thinair.msgbroker.core.util.MessageComposerConstants.PNLADLMessageConstants;

/**
 * @author udithad
 *
 */
public class EndElementBuilder extends BaseElementBuilder {

	private StringBuilder currentLine;
	private StringBuilder messageLine;
	private String currentElement;

	private EndElementContext eEContext;

	@Override
	public void buildElement(ElementContext context) {
		initContextData(context);
		endElementTemplate();
		ammendMessageDataAccordingTo();
	}

	private void endElementTemplate() {
		StringBuilder elementTemplate = new StringBuilder();
		if (eEContext.isMoreElements()) {
			elementTemplate.append(PNLADLMessageConstants.ENDPART + eEContext.getPartNumber());
		} else {
			elementTemplate.append(PNLADLMessageConstants.END);
			elementTemplate.append(eEContext.getMessageType());
		}

		currentElement = elementTemplate.toString();
	}

	private void initContextData(ElementContext context) {
		eEContext = (EndElementContext) context;
		currentLine = eEContext.getCurrentMessageLine();
		messageLine = eEContext.getMessageString();
	}

	private void ammendMessageDataAccordingTo() {
		validateConcatenationElement();
		ammendToBaseLine(currentElement, currentLine, messageLine);
		executeNext();
	}

	private void executeNext() {
		if (nextElementBuilder != null) {
			nextElementBuilder.buildElement(eEContext);
		}
	}

	private int getReduceLengthOfContains(String value, String lastLine, int existingLength, int reduceBy) {
		int ammendedLength = 0;
		if (lastLine.contains(value)) {
			ammendedLength = existingLength;
		} else {
			ammendedLength = existingLength - reduceBy;
		}
		return ammendedLength;
	}

	private void validateConcatenationElement() {
		String message = messageLine.toString();
		String[] separated = message.split("\n");
		String lineSeparator = (String) java.security.AccessController.doPrivileged(new sun.security.action.GetPropertyAction(
				"line.separator"));
		StringBuilder builder = new StringBuilder();
		if (separated != null) {
			String characterAtFirst = String.valueOf(separated[separated.length - 1].charAt(0));
			int separatedArrayLength = 0;
			if (characterAtFirst.equals("-")) {
				String lastline = separated[separated.length - 1];
				if (eEContext.getMessageType().equals(MessageTypes.PAL.toString())
						|| eEContext.getMessageType().equals(MessageTypes.CAL.toString())) {
					separatedArrayLength = separated.length;
				} else {
					separatedArrayLength = getReduceLengthOfContains("000", lastline, separated.length, 1);
				}
			} else if (isPaxModifierElementExist(separated[separated.length - 1])) {

				characterAtFirst = String.valueOf(separated[separated.length - 2].charAt(0));
				if (characterAtFirst.equals("-")) {
					String lastline = separated[separated.length - 2];
					separatedArrayLength = getReduceLengthOfContains("000", lastline, separated.length, 2);
				} else {
					separatedArrayLength = separated.length - 1;
				}
			} else {
				separatedArrayLength = separated.length;
			}

			appendLines(builder, separated, separatedArrayLength, lineSeparator);
			appendToStringBuilder(messageLine, builder);
		}
	}

	private void appendToStringBuilder(StringBuilder message, StringBuilder builder) {
		if (builder != null && !builder.toString().isEmpty()) {
			message.setLength(0);
			message.append(builder.toString());
		}
	}

	private void appendLines(StringBuilder builder, String[] separated, int separatedArrayLength, String lineSeparator) {
		for (int i = 0; i < separatedArrayLength; i++) {
			if (separated[i] != null && !separated[i].toString().isEmpty()) {
				builder.append(separated[i]);
				builder.append(lineSeparator);
			}
		}
	}

	private boolean isPaxModifierElementExist(String lastLine) {
		boolean isExist = false;
		if (lastLine.equals("ADD") || lastLine.equals("DEL") || lastLine.equals("CHG")) {
			isExist = true;
		}
		return isExist;
	}

}
