package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.util.ArrayList;
import java.util.List;

/**
 * Itinerary Document dto
 * 
 * @author mekanayake
 */
public class ItineraryDocumentDTO extends DocumentSnapInDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HeaderDTO headerDTO;

	private List<PassengerDTO> passengrDTOs;

	private List<JourneyDTO> journeyDTOs;

	public ItineraryDocumentDTO() {
		super();
	}

	public ItineraryDocumentDTO(String name, String discription) {
		super(name, discription);
	}

	public HeaderDTO getHeaderDTO() {
		return headerDTO;
	}

	public void setHeaderDTO(HeaderDTO header) {
		this.headerDTO = header;
	}

	public List<PassengerDTO> getPassengrDTOs() {
		if (passengrDTOs == null) {
			passengrDTOs = new ArrayList<PassengerDTO>();
		}
		return passengrDTOs;
	}

	public void setPassengrDTOs(List<PassengerDTO> passengrs) {
		this.passengrDTOs = passengrs;
	}

	public void addPassengreDTO(PassengerDTO passenger) {
		getPassengrDTOs().add(passenger);
	}

	public List<JourneyDTO> getJourneyDTOs() {
		if (journeyDTOs == null) {
			journeyDTOs = new ArrayList<JourneyDTO>();
		}
		return journeyDTOs;
	}

	public void setJourneyDTOs(List<JourneyDTO> jurneyDTOs) {
		this.journeyDTOs = jurneyDTOs;
	}

	public void addJourneyDTO(JourneyDTO jurneyDTO) {
		getJourneyDTOs().add(jurneyDTO);
	}

}
