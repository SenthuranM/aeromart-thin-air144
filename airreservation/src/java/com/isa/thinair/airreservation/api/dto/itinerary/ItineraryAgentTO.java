package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Collection;

public class ItineraryAgentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean isOwner;

	private String carrierCode;

	private String agentName;

	private String agentTelephone;

	private String agentEmail;

	private String ownerChannelId;
	
	private String agentIATANumber;

	private Collection<ItineraryPaymentTO> externalCharges;

	/**
	 * @return the isOwner
	 */
	public boolean getIsOwner() {
		return isOwner;
	}

	/**
	 * @param isOwner
	 *            the isOwner to set
	 */
	public void setIsOwner(boolean isOwner) {
		this.isOwner = isOwner;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the agentTelephone
	 */
	public String getAgentTelephone() {
		return agentTelephone;
	}

	/**
	 * @param agentTelephone
	 *            the agentTelephone to set
	 */
	public void setAgentTelephone(String agentTelephone) {
		this.agentTelephone = agentTelephone;
	}

	/**
	 * @return the agentEmail
	 */
	public String getAgentEmail() {
		return agentEmail;
	}

	/**
	 * @param agentEmail
	 *            the agentEmail to set
	 */
	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}

	/**
	 * @return the externalCharges
	 */
	public Collection<ItineraryPaymentTO> getExternalCharges() {
		return externalCharges;
	}

	/**
	 * @param externalCharges
	 *            the externalcharges to set
	 */
	public void setExternalCharges(Collection<ItineraryPaymentTO> externalCharges) {
		this.externalCharges = externalCharges;
	}

	public String getOwnerChannelId() {
		return ownerChannelId;
	}

	public void setOwnerChannelId(String ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	/**
	 * @return the agentIATANumber
	 */
	public String getAgentIATANumber() {
		return agentIATANumber;
	}

	/**
	 * @param agentIATANumber the agentIATANumber to set
	 */
	public void setAgentIATANumber(String agentIATANumber) {
		this.agentIATANumber = agentIATANumber;
	}

}