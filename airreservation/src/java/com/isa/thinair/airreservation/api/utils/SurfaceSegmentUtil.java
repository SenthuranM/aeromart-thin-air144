package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Utilities related to surface segment handling
 * 
 * @author duminda G
 * @since 25 OCT 2010
 */
public class SurfaceSegmentUtil {
	/**
	 * Retrive surface segments linked to a air segment
	 * 
	 * @param colReservationSegments
	 * @param pnrSegIDs
	 * @return
	 */
	public static Collection<Integer> getLinkedSurfaceSegments(Set<ReservationSegment> colReservationSegments,
			Collection<Integer> pnrSegIDs) {

		Map<Integer, Integer> mapGroundSegIds = ReservationApiUtils.getLinkedGroundSegments(colReservationSegments);
		Collection<Integer> colGroundSegIds = new ArrayList<Integer>();
		for (Integer pnrSegID : pnrSegIDs) {
			if (mapGroundSegIds.containsKey(pnrSegID)) {
				colGroundSegIds.add(mapGroundSegIds.get(pnrSegID));
			}
		}
		return colGroundSegIds;
	}

	/**
	 * Retreive the surface segments
	 * 
	 * @param colReservationSegments
	 * @param pnrSegIDs
	 * @return
	 */
	public static Collection<Integer> getSurfaceSegments(Set<ReservationSegment> colReservationSegments,
			Collection<Integer> pnrSegIDs) {
		Collection<Integer> colResSegment = new ArrayList<Integer>();
		for (ReservationSegment reservationSegment : colReservationSegments) {
			if (reservationSegment.getSubStationShortName() != null && !reservationSegment.getSubStationShortName().isEmpty()) {
				colResSegment.add(reservationSegment.getPnrSegId());
			}
		}
		return colResSegment;
	}

	/**
	 * @param lccClientSegmentAssembler
	 * @return
	 * @throws ModuleException
	 */
	public static boolean hasBusSegment(Collection<LCCClientReservationSegment> lccReservationSegments) throws ModuleException {

		Collection<String> allAirports = new HashSet<String>();

		for (LCCClientReservationSegment lccclientReservationSegment : lccReservationSegments) {
			Collection<String> airports = Arrays.asList(lccclientReservationSegment.getSegmentCode().split("/"));
			allAirports.addAll(airports);
		}

		boolean isSurfaceSegmentsExists = false;

		if (allAirports.size() > 0) {
			isSurfaceSegmentsExists = ReservationModuleUtils.getAirportBD().isContainGroundSegment(allAirports);
		}

		return isSurfaceSegmentsExists;
	}

	/**
	 * @param lccClientSegmentAssembler
	 * @return
	 * @throws ModuleException
	 */
	public static boolean hasFlightSegment(Collection<LCCClientReservationSegment> lccReservationSegments) throws ModuleException {

		for (LCCClientReservationSegment lccclientReservationSegment : lccReservationSegments) {
			if (!isBusSegment(lccclientReservationSegment)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * @param lccClientSegmentAssembler
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isBusSegment(LCCClientReservationSegment lccReservationSegment) throws ModuleException {

		Collection<String> allAirports = new HashSet<String>();

		Collection<String> airports = Arrays.asList(lccReservationSegment.getSegmentCode().split("/"));
		allAirports.addAll(airports);

		return ReservationModuleUtils.getAirportBD().isContainGroundSegment(allAirports);

	}

	public static Map<String, CachedAirportDTO> getCachedAirportMap(
			Collection<LCCClientReservationSegment> colLCCClientReservationSegment) throws ModuleException {

		Map<String, CachedAirportDTO> mapAirportCodeAndCachedAirportDTO = ReservationModuleUtils.getAirportBD()
				.getAllAirportOperatorMap();
		Map<String, CachedAirportDTO> mapSegCodeAndCachedAirportDTO = new HashMap<String, CachedAirportDTO>();

		for (LCCClientReservationSegment lccClientReservationSegment : colLCCClientReservationSegment) {

			Collection<String> airports = Arrays.asList(lccClientReservationSegment.getSegmentCode().split("/"));

			if (airports != null && airports.size() > 0) {
				mapSegCodeAndCachedAirportDTO.put(lccClientReservationSegment.getSegmentCode(),
						getCachedAirportDTO(airports, mapAirportCodeAndCachedAirportDTO));
			}
		}

		return mapSegCodeAndCachedAirportDTO;

	}

	private static CachedAirportDTO getCachedAirportDTO(Collection<String> airports,
			Map<String, CachedAirportDTO> mapCachedAirportDTO) {

		CachedAirportDTO cachedAirportDTO = null;

		for (String airportCode : airports) {
			cachedAirportDTO = mapCachedAirportDTO.get(airportCode);

			if (cachedAirportDTO.isSurfaceSegment()) {
				return cachedAirportDTO;
			}
		}

		return cachedAirportDTO;
	}

}