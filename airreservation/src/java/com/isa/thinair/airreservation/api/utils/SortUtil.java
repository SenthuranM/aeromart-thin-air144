package com.isa.thinair.airreservation.api.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;

/**
 * @author eric
 * 
 */
public class SortUtil {

	public static List<OndFareDTO> sortByOndSequnce(List<OndFareDTO> ondFareDTOs) {
		Collections.sort(ondFareDTOs, new Comparator<OndFareDTO>() {
			@Override
			public int compare(OndFareDTO at1, OndFareDTO at2) {
				return (new Integer(at1.getOndSequence())).compareTo(at2.getOndSequence());
			}
		});
		return ondFareDTOs;
	}

	public static ReservationSegmentDTO[] sortByDepDate(Collection<ReservationSegmentDTO> setReservationSegments) {
		int size = setReservationSegments.size();
		ReservationSegmentDTO[] arrayToSort = setReservationSegments.toArray(new ReservationSegmentDTO[size]);
		Arrays.sort(arrayToSort, new Comparator<ReservationSegmentDTO>() {
			@Override
			public int compare(ReservationSegmentDTO rs1, ReservationSegmentDTO rs2) {
				return rs1.getZuluDepartureDate().compareTo(rs2.getZuluDepartureDate());
			}
		});

		return arrayToSort;
	}
}
