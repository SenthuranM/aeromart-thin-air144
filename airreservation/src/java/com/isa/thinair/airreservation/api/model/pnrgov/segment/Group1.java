package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import java.util.ArrayList;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegmentGroup;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class Group1 extends EDISegmentGroup {

	protected SRC src;
	protected RCI rci;
	protected DAT dat;
	protected ORG org;
	protected ArrayList<ADD> addressCollection = new ArrayList<ADD>();
	protected ArrayList<Group2> group2 = new ArrayList<Group2>();
	protected ArrayList<Group5> group5 = new ArrayList<Group5>();

	public Group1() {
		super("Group1");
	}

	public void setSrc(SRC src) {
		this.src = src;
	}

	public void setRci(RCI rci) {
		this.rci = rci;
	}

	public void setDat(DAT dat) {
		this.dat = dat;
	}

	public void setOrg(ORG org) {
		this.org = org;
	}

	@Override
	public MessageSegment build() {
		addMessageSegmentIfNotEmpty(src);
		addMessageSegmentIfNotEmpty(rci);
		addMessageSegmentIfNotEmpty(dat);
		addMessageSegmentIfNotEmpty(org);
		addMessageSegmentsIfNotEmpty(addressCollection);
		addMessageSegmentsIfNotEmpty(group2);
		addMessageSegmentsIfNotEmpty(group5);

		return this;
	}

	public void addGroup2(Group2 group2) {
		this.group2.add(group2);
	}

	public void addGroup5(Group5 group5) {
		this.group5.add(group5);
	}

	public void addAdressElement(ADD add) {
		this.addressCollection.add(add);
	}

}
