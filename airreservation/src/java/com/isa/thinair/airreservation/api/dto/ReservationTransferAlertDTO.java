/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airreservation.api.model.Reservation;

/**
 * To hold reservation segment data transfer information
 */
public class ReservationTransferAlertDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7309438800530402810L;

	private ReservationSegmentDTO sourceSegment;

	private ReservationSegmentDTO targetSegment;

	private FlightReservationAlertDTO FlightReservationAlertDTO;
	
	private Collection<Reservation> alertingReservations;

	/**
	 * @return Returns the sourceSegment.
	 */
	public ReservationSegmentDTO getSourceSegment() {
		return sourceSegment;
	}

	/**
	 * @param sourceSegment
	 *            The sourceSegment to set.
	 */
	public void setSourceSegment(ReservationSegmentDTO sourceSegment) {
		this.sourceSegment = sourceSegment;
	}

	/**
	 * @return Returns the targetSegment.
	 */
	public ReservationSegmentDTO getTargetSegment() {
		return targetSegment;
	}

	/**
	 * @param targetSegment
	 *            The targetSegment to set.
	 */
	public void setTargetSegment(ReservationSegmentDTO targetSegment) {
		this.targetSegment = targetSegment;
	}

	/**
	 * @return Returns the flightReservationAlertDTO.
	 */
	public FlightReservationAlertDTO getFlightReservationAlertDTO() {
		return FlightReservationAlertDTO;
	}

	/**
	 * @param flightReservationAlertDTO
	 *            The flightReservationAlertDTO to set.
	 */
	public void setFlightReservationAlertDTO(FlightReservationAlertDTO flightReservationAlertDTO) {
		FlightReservationAlertDTO = flightReservationAlertDTO;
	}

	public Collection<Reservation> getAlertingReservations() {
		return alertingReservations;
	}

	public void setAlertingReservations(Collection<Reservation> alertingReservations) {
		this.alertingReservations = alertingReservations;
	}
}
