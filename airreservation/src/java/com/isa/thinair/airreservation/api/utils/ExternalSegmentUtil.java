package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Util for external segment status change
 * 
 * @author malaka
 * 
 */
public class ExternalSegmentUtil {

	public static Map<String, Collection<ExternalPnrSegment>> getConfirmedReservationExternalSegmentInAMap(
			Collection<ExternalPnrSegment> externalSegments) {
		Map<String, Collection<ExternalPnrSegment>> segmentMap = new HashMap<String, Collection<ExternalPnrSegment>>();
		String uniqueSegmentKeyWithOutStatus;

		for (ExternalPnrSegment reservationExternalSegment : externalSegments) {

			if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(reservationExternalSegment
					.getStatus())) {
				uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
				Collection<ExternalPnrSegment> colReservationExternalSegment = segmentMap.get(uniqueSegmentKeyWithOutStatus);

				if (colReservationExternalSegment == null) {
					colReservationExternalSegment = new ArrayList<ExternalPnrSegment>();
					colReservationExternalSegment.add(reservationExternalSegment);
					segmentMap.put(uniqueSegmentKeyWithOutStatus, colReservationExternalSegment);
				} else {
					colReservationExternalSegment.add(reservationExternalSegment);
				}
			}
		}

		return segmentMap;
	}

	public static Map<String, Collection<ExternalPnrSegment>> getAllReservationExternalSegmentInAMap(
			Collection<ExternalPnrSegment> externalSegments) {
		Map<String, Collection<ExternalPnrSegment>> segmentMap = new HashMap<String, Collection<ExternalPnrSegment>>();
		String uniqueSegmentKeyWithOutStatus;

		for (ExternalPnrSegment reservationExternalSegment : externalSegments) {

			uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
			Collection<ExternalPnrSegment> colReservationExternalSegment = segmentMap.get(uniqueSegmentKeyWithOutStatus);

			if (colReservationExternalSegment == null) {
				colReservationExternalSegment = new ArrayList<ExternalPnrSegment>();
				colReservationExternalSegment.add(reservationExternalSegment);
				segmentMap.put(uniqueSegmentKeyWithOutStatus, colReservationExternalSegment);
			} else {
				colReservationExternalSegment.add(reservationExternalSegment);
			}
		}

		return segmentMap;
	}

	/**
	 * Add External Segments
	 * 
	 * @param colExternalSegmentTO
	 * @param reservation
	 * @return
	 */
	public static String addExternalSegments(Collection<ExternalSegmentTO> colExternalSegmentTO, Reservation reservation,
			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap) {

		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;

		Collection<ExternalPnrSegment> colReservationExternalSegment = new ArrayList<ExternalPnrSegment>();
		// int nextSegmentSeq = getNextSegmentSeq(reservation.getExternalReservationSegment());

		for (ExternalSegmentTO externalSegmentTO : colExternalSegmentTO) {

			if (ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
				String uniqueSegmentKeyWithOutStatus = ReservationApiUtils.getUniqueSegmentKey(externalSegmentTO.getFlightNo(),
						externalSegmentTO.getSegmentCode(), externalSegmentTO.getDepartureDate(),
						externalSegmentTO.getCabinClassCode(), null);
				if (serverSegmentMap.containsKey(uniqueSegmentKeyWithOutStatus)) {
					continue;
				}
			}

			Integer extFlightSegRef = FlightRefNumberUtil.getSegmentIdFromFlightRPH(externalSegmentTO.getFlightRefNumber());

			ExternalFlightSegment externalFlightSegment = reservationSegmentDAO.getExternalFlightSegment(
					externalSegmentTO.getOperatingAirline(), extFlightSegRef);

			if (externalFlightSegment == null) {
				externalFlightSegment = new ExternalFlightSegment();
			}

			externalFlightSegment.setExternalFlightSegRef(extFlightSegRef);
			externalFlightSegment.setFlightNumber(externalSegmentTO.getFlightNo());
			externalFlightSegment.setSegmentCode(externalSegmentTO.getSegmentCode());
			externalFlightSegment.setExternalCarrierCode(externalSegmentTO.getOperatingAirline());
			String flightStatus = (externalSegmentTO.getFltSegStatus() == null ? "OPN" : externalSegmentTO.getFltSegStatus());
			externalFlightSegment.setStatus(flightStatus);
			externalFlightSegment.setEstTimeArrivalLocal(externalSegmentTO.getArrivalDate());
			externalFlightSegment.setEstTimeArrivalZulu(externalSegmentTO.getArrivalDateZulu());
			externalFlightSegment.setEstTimeDepatureLocal(externalSegmentTO.getDepartureDate());
			externalFlightSegment.setEstTimeDepatureZulu(externalSegmentTO.getDepartureDateZulu());

			// save or update external flight segment
			reservationSegmentDAO.saveExternalFlightSegment(externalFlightSegment);

			ExternalPnrSegment externalPnrSegment = externalSegmentTO.toExternalPnrSegment();
			externalPnrSegment.setExternalFltSegId(externalFlightSegment.getExternalFlightSegId());
			externalPnrSegment.setSubStatus(externalSegmentTO.getSubStatus());

			colReservationExternalSegment.add(externalPnrSegment);
			// nextSegmentSeq++;

		}

		StringBuilder info = new StringBuilder();
		if (colReservationExternalSegment.size() > 0) {
			for (ExternalPnrSegment reservationExternalSegment : colReservationExternalSegment) {
				// Save the external pnr segment only if it is not present in the reservation.
				if (!isExternalPnrSegmentInReservation(reservation, reservationExternalSegment)
						|| sameDateFlightModification(reservationExternalSegment, colExternalSegmentTO)) {
					reservation.addExternalReservationSegment(reservationExternalSegment);

					if (info.length() == 0) {
						info.append(reservationExternalSegment.toString());
					} else {
						info.append(" , " + reservationExternalSegment.toString());
					}
				}
			}
		}

		return info.toString();
	}

	private static boolean sameDateFlightModification(ExternalPnrSegment reservationExternalSegment,
			Collection<ExternalSegmentTO> colExternalSegmentTOs) {

		String uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
		for (ExternalSegmentTO externalSegmentTO : colExternalSegmentTOs) {
			String uniqueSegmentKey = ReservationApiUtils.getUniqueSegmentKey(externalSegmentTO.getFlightNo(),
					externalSegmentTO.getSegmentCode(), externalSegmentTO.getDepartureDate(),
					externalSegmentTO.getCabinClassCode(), null);
			if (uniqueSegmentKeyWithOutStatus.equals(uniqueSegmentKey)
					&& ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
				return true;
			}
		}
		return false;
	}

	//
	private static boolean isExternalPnrSegmentInReservation(Reservation reservation, ExternalPnrSegment pnrSegment) {
		boolean isExternalPnrSegmentInReservation = false;

		if (reservation != null && pnrSegment != null) {
			for (ExternalPnrSegment exisitingExternalPnrSegment : (Collection<ExternalPnrSegment>) reservation
					.getExternalReservationSegment()) {

				if (pnrSegment.getUniqueSegmentKey().equals(exisitingExternalPnrSegment.getUniqueSegmentKey())
						&& pnrSegment.getStatus().equals(exisitingExternalPnrSegment.getStatus())) {
					isExternalPnrSegmentInReservation = true;
					break;
				}
			}
		}
		return isExternalPnrSegmentInReservation;
	}

	/**
	 * Modify External Segments
	 * 
	 * @param serverSegmentMap
	 * @param clientSegmentMap
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String modifyExternalSegments(Collection<ExternalSegmentTO> colExternalSegmentTO,
			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap) throws ModuleException {

		StringBuilder modifySegInfo = new StringBuilder();

		// sort by segment sequence
		for (ExternalSegmentTO externalSegmentTO : colExternalSegmentTO) {

			if (ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
				String uniqueSegmentKeyWithOutStatus = ReservationApiUtils.getUniqueSegmentKey(externalSegmentTO.getFlightNo(),
						externalSegmentTO.getSegmentCode(), externalSegmentTO.getDepartureDate(),
						externalSegmentTO.getCabinClassCode(), null);

				if (serverSegmentMap.containsKey(uniqueSegmentKeyWithOutStatus)) {
					Collection<ExternalPnrSegment> colReservationExternalSegment = serverSegmentMap
							.get(uniqueSegmentKeyWithOutStatus);
					ExternalPnrSegment reservationExternalSegment = getConfirmedReservationExternalSegment(colReservationExternalSegment);

					if (modifySegInfo.length() == 0) {
						modifySegInfo.append(reservationExternalSegment.toString() + " --> " + externalSegmentTO.toString());
					} else {
						modifySegInfo.append(" , " + reservationExternalSegment.toString() + " --> "
								+ externalSegmentTO.toString());
					}

					reservationExternalSegment.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
					reservationExternalSegment.setStatusModifiedDate(new Date());
					reservationExternalSegment.setSubStatus(externalSegmentTO.getSubStatus());
				}
			}
			// else if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(externalSegmentTO
			// .getStatus())) {
			// String uniqueSegmentKeyWithOutStatus =
			// ReservationApiUtils.getUniqueSegmentKey(externalSegmentTO.getFlightNo(),
			// externalSegmentTO.getSegmentCode(), externalSegmentTO.getDepartureDate(),
			// externalSegmentTO.getCabinClassCode(), null);
			// if (serverSegmentMap.containsKey(uniqueSegmentKeyWithOutStatus)) {
			// Collection<ExternalPnrSegment> colReservationExternalSegment = serverSegmentMap
			// .get(uniqueSegmentKeyWithOutStatus);
			// ExternalPnrSegment reservationExternalSegment =
			// getConfirmedReservationExternalSegment(colReservationExternalSegment);
			//
			// if (modifySegInfo.length() == 0) {
			// modifySegInfo.append(reservationExternalSegment.toString() + " --> " + externalSegmentTO.toString());
			// } else {
			// modifySegInfo.append(" , " + reservationExternalSegment.toString() + " --> "
			// + externalSegmentTO.toString());
			// }
			//
			// reservationExternalSegment.setSegmentSeq(externalSegmentTO.getSegmentSequence());
			// reservationExternalSegment.setExternalPnrSegRef(externalSegmentTO.getExternalPnrSegId());
			// reservationExternalSegment
			// .setStatusModifiedSalesChannelCode(externalSegmentTO.getStatusModifiedChannelCode());
			// reservationExternalSegment.setMarketingAgentCode(externalSegmentTO.getMarketingAgentCode());
			// reservationExternalSegment.setMarketingCarrierCode(externalSegmentTO.getMarketingCarrierCode());
			// reservationExternalSegment.setMarketingStationCode(externalSegmentTO.getMarketingStationCode());
			// reservationExternalSegment.setStatusModifiedDate(externalSegmentTO.getStatusModifiedDate());
			// }
			//
			// }

		}

		return modifySegInfo.toString();
	}

	private static int getNextSegmentSeq(Collection<ExternalPnrSegment> externalSegments) {
		List<Integer> lstSegmentSeq = new ArrayList<Integer>();
		// TODO: Change this logic to return the exact sequence number of the actual segment. For that, we need to
		// either pass the sequence number of actual segment or identify this is from adding existing segments for new
		// booking.
		// Since ext segment seq number does not use for any purpose, this is ok at the moment.
		if (externalSegments != null && externalSegments.size() > 0) {
			for (ExternalPnrSegment reservationExternalSegment : externalSegments) {
				lstSegmentSeq.add(reservationExternalSegment.getSegmentSeq());
			}

			Collections.sort(lstSegmentSeq);
			int i = lstSegmentSeq.get(lstSegmentSeq.size() - 1);
			return ++i;
		} else {
			return new Integer(1);
		}
	}

	/**
	 * Returns the confirmed reservation external segment
	 * 
	 * @param colReservationExternalSegment
	 * @return
	 * @throws ModuleException
	 */
	private static ExternalPnrSegment getConfirmedReservationExternalSegment(
			Collection<ExternalPnrSegment> colReservationExternalSegment) throws ModuleException {

		if (colReservationExternalSegment != null && colReservationExternalSegment.size() > 0) {
			for (ExternalPnrSegment reservationExternalSegment : colReservationExternalSegment) {
				if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(reservationExternalSegment
						.getStatus())) {
					return reservationExternalSegment;
				}
			}
		}

		throw new ModuleException("airreservations.arg.corruptedExternalSegmentsForAmmending");
	}

}
