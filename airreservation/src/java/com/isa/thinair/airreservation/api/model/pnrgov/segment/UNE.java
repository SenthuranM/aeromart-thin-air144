package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.BasicDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class UNE extends EDISegment {

	private String E0060;
	private String E0048;

	public UNE() {
		super(EDISegmentTag.UNE);
	}

	public void setE0060(String e0060) {
		E0060 = e0060;
	}

	public void setE0048(String e0048) {
		E0048 = e0048;
	}

	@Override
	public MessageSegment build() {
		BasicDataElement V0060 = new Numeric(EDI_ELEMENT.E0060, E0060);
		BasicDataElement V0048 = new AlphaNumeric(EDI_ELEMENT.E0048, E0048);

		addEDIDataElement(V0060);
		addEDIDataElement(V0048);

		return this;
	}
}
