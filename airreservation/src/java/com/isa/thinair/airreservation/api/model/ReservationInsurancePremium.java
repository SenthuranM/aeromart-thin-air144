package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;
import com.isa.thinair.commons.core.framework.Persistent;

public class ReservationInsurancePremium extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long premiumId;
	private int fromDays;
	private int toDays;
	private String amount;
	private String currency;
	private Map<InsuranceTypes, BigDecimal> insTypeCharges;

	public static final String LESS_THAN_30_DAYS_ONE_WAY_PREMIUM = "1";
	public static final String LESS_THAN_30_DAYS_ROUND_TRIP_PREMIUM = "2";
	public static final String BETWEEN_31_45_DAYS_ROUND_TRIP_PREMIUM = "3";
	public static final String BETWEEN_46_60_DAYS_ROUND_TRIP_PREMIUM = "4";
	public static final String BETWEEN_61_90_DAYS_ROUND_TRIP_PREMIUM = "5";

	public Long getPremiumId() {
		return premiumId;
	}

	public void setPremiumId(Long premiumId) {
		this.premiumId = premiumId;
	}

	public int getFromDays() {
		return fromDays;
	}

	public void setFromDays(int fromDays) {
		this.fromDays = fromDays;
	}

	public int getToDays() {
		return toDays;
	}

	public void setToDays(int toDays) {
		this.toDays = toDays;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Map<InsuranceTypes, BigDecimal> getInsTypeCharges() {
		return insTypeCharges;
	}

	public void setInsTypeCharges(Map<InsuranceTypes, BigDecimal> insTypeCharges) {
		this.insTypeCharges = insTypeCharges;
	}

}
