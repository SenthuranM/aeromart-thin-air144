package com.isa.thinair.airreservation.api.model.pnrgov;

public abstract class MessageSegment implements EDIGeneratable, EDIBuilder {

	protected String segmentName;

	public MessageSegment (String segmentName) {
		super();
		this.segmentName = segmentName;
	}
	
	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	
	public abstract int getSegmentCount();
}
