package com.isa.thinair.airreservation.api.dto;

/**
 * @author Indika Athauda
 */
public class PaymentModeDTO {

	private String paymentMode;

	public boolean isGenerateReceipt;

	public boolean isGenerateUniqueRecieptforMode;

	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * @param paymentMode
	 *            the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the isGenerateReceipt
	 */
	public boolean isGenerateReceipt() {
		return isGenerateReceipt;
	}

	/**
	 * @param isGenerateReceipt
	 *            the isGenerateReceipt to set
	 */
	public void setGenerateReceipt(boolean isGenerateReceipt) {
		this.isGenerateReceipt = isGenerateReceipt;
	}

	/**
	 * @return the isGenerateUniqueRecieptforMode
	 */
	public boolean isGenerateUniqueRecieptforMode() {
		return isGenerateUniqueRecieptforMode;
	}

	/**
	 * @param isGenerateUniqueRecieptforMode
	 *            the isGenerateUniqueRecieptforMode to set
	 */
	public void setGenerateUniqueRecieptforMode(boolean isGenerateUniqueRecieptforMode) {
		this.isGenerateUniqueRecieptforMode = isGenerateUniqueRecieptforMode;
	}

}
