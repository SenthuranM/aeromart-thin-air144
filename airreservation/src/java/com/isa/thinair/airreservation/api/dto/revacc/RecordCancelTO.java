package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;

/**
 * @author Nilindra Fernando
 */
public class RecordCancelTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1080123528067153872L;

	private String pnrPaxId;

	private BigDecimal creditTotal;

	private BigDecimal chargeAmount;

	private int actionNC;

	private int chargeNC;

	private ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO;

	private CredentialsDTO credentialsDTO;

	private boolean enableTransactionGranularity;

	private BigDecimal modificationPenalty;

	/**
	 * @return the pnrPaxId
	 */
	public String getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the creditTotal
	 */
	public BigDecimal getCreditTotal() {
		return creditTotal;
	}

	/**
	 * @param creditTotal
	 *            the creditTotal to set
	 */
	public void setCreditTotal(BigDecimal creditTotal) {
		this.creditTotal = creditTotal;
	}

	/**
	 * @return the chargeAmount
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            the chargeAmount to set
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the actionNC
	 */
	public int getActionNC() {
		return actionNC;
	}

	/**
	 * @param actionNC
	 *            the actionNC to set
	 */
	public void setActionNC(int actionNC) {
		this.actionNC = actionNC;
	}

	/**
	 * @return the chargeNC
	 */
	public int getChargeNC() {
		return chargeNC;
	}

	/**
	 * @param chargeNC
	 *            the chargeNC to set
	 */
	public void setChargeNC(int chargeNC) {
		this.chargeNC = chargeNC;
	}

	/**
	 * @return the reservationPaxPaymentMetaTO
	 */
	public ReservationPaxPaymentMetaTO getReservationPaxPaymentMetaTO() {
		return reservationPaxPaymentMetaTO;
	}

	/**
	 * @param reservationPaxPaymentMetaTO
	 *            the reservationPaxPaymentMetaTO to set
	 */
	public void setReservationPaxPaymentMetaTO(ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO) {
		this.reservationPaxPaymentMetaTO = reservationPaxPaymentMetaTO;
	}

	/**
	 * @return the credentialsDTO
	 */
	public CredentialsDTO getCredentialsDTO() {
		return credentialsDTO;
	}

	/**
	 * @param credentialsDTO
	 *            the credentialsDTO to set
	 */
	public void setCredentialsDTO(CredentialsDTO credentialsDTO) {
		this.credentialsDTO = credentialsDTO;
	}

	public boolean isEnableTransactionGranularity() {
		return enableTransactionGranularity;
	}

	public void setEnableTransactionGranularity(boolean enableTransactionGranularity) {
		this.enableTransactionGranularity = enableTransactionGranularity;
	}

	public void setModificationPenalty(BigDecimal modificationPenalty) {
		this.modificationPenalty = modificationPenalty;
	}

	public BigDecimal getModificationPenalty() {
		return modificationPenalty;
	}

}
