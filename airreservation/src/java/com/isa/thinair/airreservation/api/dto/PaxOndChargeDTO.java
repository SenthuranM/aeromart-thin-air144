/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * To hold ReservationPaxOndCharge data transfer information
 * 
 * @author Rikaz
 * @since
 */
public class PaxOndChargeDTO implements Serializable {

	private static final long serialVersionUID = 5411176147064274436L;

	private BigDecimal taxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal nonTaxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String taxAppliedCategory;
	
	private String taxDepositStateCode;
	
	private String taxDepositCountryCode;

	public PaxOndChargeDTO() {
		this.taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY;
	}

	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public BigDecimal getNonTaxableAmount() {
		return nonTaxableAmount;
	}

	public void setNonTaxableAmount(BigDecimal nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}

	public String getTaxAppliedCategory() {
		if (StringUtil.isNullOrEmpty(taxAppliedCategory)) {
			taxAppliedCategory = ReservationPaxOndCharge.TAX_APPLIED_FOR_OTHER_CATEGORY;
		}
		return taxAppliedCategory;
	}

	public void setTaxAppliedCategory(String taxAppliedCategory) {
		this.taxAppliedCategory = taxAppliedCategory;
	}

	public String getTaxDepositStateCode() {
		return taxDepositStateCode;
	}

	public void setTaxDepositStateCode(String taxDepositStateCode) {
		this.taxDepositStateCode = taxDepositStateCode;
	}

	public String getTaxDepositCountryCode() {
		return taxDepositCountryCode;
	}

	public void setTaxDepositCountryCode(String taxDepositCountryCode) {
		this.taxDepositCountryCode = taxDepositCountryCode;
	}

}