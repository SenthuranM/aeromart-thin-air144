/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.etl;

import java.util.ArrayList;
import java.util.List;


/**
 * Holds Passenger final sales Meta data transfer information
 * 
 * @author TF
 * @since 1.0
 */
public class ETLMessageDataDTO {
	
	/** Holds the Message Type */
	private String msgType;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the departure day e.g. 02 */
	private String day;

	/** Holds the departure month e.g. DEC */
	private String month;

	/** Holds the from airport */
	private String boardingAirport;	

	private String communicationElement;	
		
	/** Hold the end of Etl */
	private String endElement;
	
	/** Hold the from address of Etl */
	private String fromAddress;
	
	private String seatConfiguration;
	
	/** Holds the flight date 03JAN */
	private String dayMonth;
	
	private List<EtlTotalDestinationElementTO> totalByDestination=new ArrayList<EtlTotalDestinationElementTO>();
	
	private String partElement;
	
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	

	/**
	 * @return the seatConfiguration
	 */
	public String getSeatConfiguration() {
		return seatConfiguration;
	}

	/**
	 * @param seatConfiguration the seatConfiguration to set
	 */
	public void setSeatConfiguration(String seatConfiguration) {
		this.seatConfiguration = seatConfiguration;
	}

	/**
	 * @return Returns the boardingAirport.
	 */
	public String getBoardingAirport() {
		return boardingAirport;
	}

	/**
	 * @param boardingAirport
	 *            The boardingAirport to set.
	 */
	public void setBoardingAirport(String boardingAirport) {
		this.boardingAirport = boardingAirport;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	public String getCommunicationElement() {
		return communicationElement;
	}

	public void setCommunicationElement(String communicationElement) {
		this.communicationElement = communicationElement;
	}

	/**
	 * @return the endElement
	 */
	public String getEndElement() {
		return endElement;
	}

	/**
	 * @param endElement the endElement to set
	 */
	public void setEndElement(String endElement) {
		this.endElement = endElement;
	}

	/**
	 * @return the fromAddress
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress the fromAddress to set
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return the totalByDestination
	 */
	public List<EtlTotalDestinationElementTO> getTotalByDestination() {
		return totalByDestination;
	}

	/**
	 * @param totalByDestination the totalByDestination to set
	 */
	public void setTotalByDestination(List<EtlTotalDestinationElementTO> totalByDestination) {
		this.totalByDestination = totalByDestination;
	}

	public String getDayMonth() {
		return dayMonth;
	}

	public void setDayMonth(String dayMonth) {
		this.dayMonth = dayMonth;
	}

	public String getPartElement() {
		return partElement;
	}

	public void setPartElement(String partElement) {
		this.partElement = partElement;
	}
	
}
