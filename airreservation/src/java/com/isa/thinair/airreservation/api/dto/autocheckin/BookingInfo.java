/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Panchatcharam.S
 *
 */
public class BookingInfo implements Serializable {

	private static final long serialVersionUID = -5653847162815230938L;
	private String id;
	private String language;
	private Collection<PnrPaxInfo> pnrPaxInfo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Collection<PnrPaxInfo> getPnrPaxInfo() {
		return pnrPaxInfo;
	}

	public void setPnrPaxInfo(Collection<PnrPaxInfo> pnrPaxInfo) {
		this.pnrPaxInfo = pnrPaxInfo;
	}

}
