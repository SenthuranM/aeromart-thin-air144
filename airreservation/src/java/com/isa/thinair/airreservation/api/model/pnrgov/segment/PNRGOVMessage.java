package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import java.util.ArrayList;

import com.isa.thinair.airreservation.api.model.pnrgov.EDIFACTBasicMessage;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class PNRGOVMessage extends EDIFACTBasicMessage {

	protected UNB unb;
	protected UNG ung;
	protected UNH unh;
	protected MSG msg;
	protected ORG org;
	protected TVL tvl;
	protected EQN eqn;
	protected ArrayList<Group1> group1 = new ArrayList<Group1>();

	public PNRGOVMessage() {
		super("PNRGOV");
		// TODO Auto-generated constructor stub
	}

	public UNA createUna() {
		UNA una = new UNA();
		return una;
	}

	public void setUnb(UNB unb) {
		this.unb = unb;
	}

	public void setUng(UNG ung) {
		this.ung = ung;
	}

	public void setUnh(UNH unh) {
		this.unh = unh;
	}

	public void setMsg(MSG msg) {
		this.msg = msg;
	}

	public void setOrg(ORG org) {
		this.org = org;
	}

	public void setTvl(TVL tvl) {
		this.tvl = tvl;
	}

	public void setEqn(EQN eqn) {
		this.eqn = eqn;
	}

	public void addGroup1(Group1 group1) {
		this.group1.add(group1);
	}

	public UNT createUnt() {
		UNT unt = new UNT();
		unt.setE0074(String.valueOf(getSegmentCount()));
		unt.setE0062(getUnhRef());
		return unt;
	}

	public UNE createUne() {
		UNE une = new UNE();
		une.setE0060("1");
		une.setE0048(getUngRef());
		return une;
	}

	public UNZ createUnz() {
		UNZ unz = new UNZ();
		unz.setE0036("1");
		unz.setE0020(getUnbRef());
		return unz;
	}

	@Override
	public MessageSegment build() {
		addMessageSegmentIfNotEmpty(createUna());
		addMessageSegmentIfNotEmpty(unb);
		addMessageSegmentIfNotEmpty(ung);
		addMessageSegmentIfNotEmpty(unh);
		addMessageSegmentIfNotEmpty(msg);
		addMessageSegmentIfNotEmpty(org);
		addMessageSegmentIfNotEmpty(tvl);
		addMessageSegmentIfNotEmpty(eqn);
		addMessageSegmentsIfNotEmpty(group1);
		addMessageSegmentIfNotEmpty(createUnt());
		addMessageSegmentIfNotEmpty(createUne());
		addMessageSegmentIfNotEmpty(createUnz());

		return this;
	}

	public UNB getUnb() {
		return unb;
	}

	public UNG getUng() {
		return ung;
	}

	public UNH getUnh() {
		return unh;
	}

	public MSG getMsg() {
		return msg;
	}

	public ORG getOrg() {
		return org;
	}

	public TVL getTvl() {
		return tvl;
	}

	public EQN getEqn() {
		return eqn;
	}

	public ArrayList<Group1> getGroup1() {
		return group1;
	}

}
