package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * To keep track of PnlAdlHistory
 * 
 * @author Isuru
 * @since 1.0
 * @hibernate.class table = "T_PNLADL_PAX_COUNT"
 */
public class PnlAdlPaxCount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -908742433857156251L;

	private Integer id;

	private String cabinClassCode;

	private Integer paxCount;

	private String lastGroupCode;

	private PnlAdlHistory pnlAdlHistory;

	private String logicalCabinClassCode;
	
	private String bookingCode;

	/**
	 * returns the acccId
	 * 
	 * @return Returns the acccId.
	 * @hibernate.id column = "ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNLADL_PAX_COUNT"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 * 
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @hibernate.many-to-one column="HIS_ID" class="com.isa.thinair.airreservation.api.model.PnlAdlHistory"
	 * @return Returns the reservation.
	 */
	public PnlAdlHistory getPnlAdlHistory() {
		return pnlAdlHistory;
	}

	/**
	 * @param pnlAdlHistory
	 *            The pnlAdlHistory to set.
	 */
	public void setPnlAdlHistory(PnlAdlHistory pnlAdlHistory) {
		this.pnlAdlHistory = pnlAdlHistory;
	}

	/**
	 * @return Returns the paxCount.
	 * @hibernate.property column = "PAX_COUNT"
	 */
	public Integer getPaxCount() {
		return paxCount;
	}

	/**
	 * @param paxCount
	 *            The paxCount to set.
	 */
	public void setPaxCount(Integer paxCount) {
		this.paxCount = paxCount;
	}

	/**
	 * @return Returns the paxCount.
	 * @hibernate.property column = "LAST_GROUP_ID"
	 */
	public String getLastGroupCode() {
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

	/**
	 * @return Returns the logicalCabinClassCode.
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 *            The logicalCabinClassCode to set.
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the bookingCode.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}
	
	

}
