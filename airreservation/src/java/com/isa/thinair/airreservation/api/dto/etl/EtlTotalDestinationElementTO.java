/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.etl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashain
 *
 */
public class EtlTotalDestinationElementTO {
	private String destinationAirPort;
	
	private List<EtlCabinClassList>  cabinClassWiseList=new ArrayList<EtlCabinClassList>();

	/**
	 * @return the destinationAirPort
	 */
	public String getDestinationAirPort() {
		return destinationAirPort;
	}

	/**
	 * @param destinationAirPort the destinationAirPort to set
	 */
	public void setDestinationAirPort(String destinationAirPort) {
		this.destinationAirPort = destinationAirPort;
	}

	/**
	 * @return the cabinClassWiseList
	 */
	public List<EtlCabinClassList> getCabinClassWiseList() {
		return cabinClassWiseList;
	}

	/**
	 * @param cabinClassWiseList the cabinClassWiseList to set
	 */
	public void setCabinClassWiseList(List<EtlCabinClassList> cabinClassWiseList) {
		this.cabinClassWiseList = cabinClassWiseList;
	}

	
	
	
}
