/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class FlightInfo implements Serializable {

	private static final long serialVersionUID = 4643573509155265629L;
	private FlightBasicInfo carrierInfo;
	private DepartureInformation departureInformation;

	public FlightBasicInfo getCarrierInfo() {
		return carrierInfo;
	}

	public void setCarrierInfo(FlightBasicInfo carrierInfo) {
		this.carrierInfo = carrierInfo;
	}

	public DepartureInformation getDepartureInformation() {
		return departureInformation;
	}

	public void setDepartureInformation(DepartureInformation departureInformation) {
		this.departureInformation = departureInformation;
	}

}
