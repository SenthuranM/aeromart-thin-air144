/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.FlightReservationSummaryDTO;
import com.isa.thinair.airreservation.api.dto.OtherAirlineSegmentTO;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentTransfer;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Interface to define all the methods required to access the reservation segment details
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface SegmentBD {

	public static final String SERVICE_NAME = "ReservationService";

	/**
	 * Cancel segments
	 * 
	 * @param pnr
	 * @param segmentIds
	 * @param customChargesTO
	 * @param version
	 * @param cnxFlownSegs
	 * @param trackInfoDTO
	 * @param hasExternalCarrierPayments
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce cancelSegments(String pnr, Collection<Integer> segmentIds, CustomChargesTO customChargesTO,
			long version, boolean cnxFlownSegs, TrackInfoDTO trackInfoDTO, boolean hasExternalCarrierPayments, String userNotes,
			boolean applyCancelCharge) throws ModuleException;

	/**
	 * Cancel segments with REQUIRES_NEW transaction
	 * 
	 * @param pnr
	 * @param segmentIds
	 * @param customChargesTO
	 * @param version
	 * @param cnxFlownSegs
	 * @param trackInfoDTO
	 * @param hasExternalCarrierPayments
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce cancelSegmentsWithRequiresNew(String pnr, Collection<Integer> requestedSegmentIds,
			CustomChargesTO customChargesTO, long version, boolean cnxFlownSegs, TrackInfoDTO trackInfoDTO,
			boolean hasExternalCarrierPayments, String userNotes, boolean applyCancelCharge) throws ModuleException;

	/**
	 * Cancel segments applying the modification charge
	 * 
	 * @param pnr
	 * @param requestedSegmentIds
	 * @param newFltSegIds
	 * @param customChargesTO
	 * @param version
	 * @param cnxFlownSegs
	 * @param trackInfoDTO
	 * @param hasExternalCarrierPayments
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce cancelSegmentsApplyingModCharge(String pnr, Collection<Integer> requestedSegmentIds,
			Collection<Integer> newFltSegIds, CustomChargesTO customChargesTO, long version, boolean cnxFlownSegs,
			TrackInfoDTO trackInfoDTO,String userNotes, boolean hasExternalCarrierPayments) throws ModuleException;

	/**
	 * Return Ond Charges for cancellation
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param loadMetaAmounts
	 * @param version
	 * @param hasExternalPayments
	 * @param isVoidOperation
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getOndChargesForCancellation(String pnr, Collection<Integer> pnrSegmentIds,
			boolean loadMetaAmounts, long version, CustomChargesTO customChargesTO, boolean hasExternalPayments,
			boolean isVoidOperation) throws ModuleException;

	/**
	 * Return Ond charges for cancellation applying modification charge
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param newFltSegIds
	 * @param loadMetaAmounts
	 * @param version
	 * @param customChargesTO
	 * @param hasExternalPayments
	 * @param isVoidOperation
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getOndChargesForCancellationApplyingModCharge(String pnr, Collection<Integer> pnrSegmentIds,
			Collection<Integer> newFltSegIds, boolean loadMetaAmounts, long version, CustomChargesTO customChargesTO,
			boolean hasExternalPayments, boolean isVoidOperation) throws ModuleException;

	/**
	 * Return Ond Charges for modification
	 * 
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param newFltSegIds
	 * @param loadMetaAmounts
	 * @param version
	 * @param isVoidOperation
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getOndChargesForModification(String pnr, Collection<Integer> pnrSegmentIds,
			Collection<Integer> newFltSegIds, boolean loadMetaAmounts, boolean enableModifyProrating, long version,
			CustomChargesTO customChargesTO, boolean isVoidOperation) throws ModuleException;

	/**
	 * Return Ond Charges for Requote
	 * 
	 * @param loadMetaAmounts
	 * @param pnr
	 * @param pnrSegmentIds
	 * @param newFltSegIds
	 * @param version
	 * @param customChargesTO
	 * @param hasExternalPayments
	 * @param isForModification
	 * @param showCnxCharge
	 * @param showModCharge
	 * @param ondFares
	 * @param skipPenaltyCharges
	 * @param isVoidOperation
	 * @param isFareAdjustmentValid
	 *            TODO
	 * @param ondFareTypeByFareIdMap
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getOndChargesForRequote(boolean loadMetaAmounts, String pnr,
			Collection<Integer> pnrSegmentIds, Collection<Integer> newFltSegIds, long version, CustomChargesTO customChargesTO,
			boolean hasExternalPayments, boolean isForModification, boolean applyCnxCharge, boolean applyModCharge,
			Collection<OndFareDTO> ondFares, boolean skipPenaltyCharges, boolean isVoidOperation, boolean isFareAdjustmentValid,
			Map<Integer, String> ondFareTypeByFareIdMap, CredentialsDTO credentialsDTO) throws ModuleException;

	/**
	 * Return reservation charges
	 * 
	 * @param pnr
	 * @param loadMetaAmounts
	 * @param includeFlown
	 * @param showCnxCharge
	 * @param version
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getPnrCharges(String pnr, boolean loadMetaAmounts, boolean includeFlown,
			boolean applyCnxCharge, long version) throws ModuleException;

	/**
	 * Return reservation charges
	 * 
	 * @param pnr
	 * @param loadMetaAmounts
	 * @param version
	 * @param hasExternalPayments
	 * @param isVoidOperation
	 * @param autoCancelingPnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PnrChargesDTO> getPnrCharges(String pnr, boolean loadMetaAmounts, long version,
			CustomChargesTO customChargesTO, boolean hasExternalPayments, boolean isVoidOperation,
			List<Integer> autoCancelingPnrSegIds) throws ModuleException;

	/**
	 * Transfer reservation segment
	 * 
	 * @param pnr
	 * @param pnrSegIdAndNewFlgSegId
	 * @param isAlert
	 * @param version
	 * @param salesChannelKey TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce transferReservationSegment(String pnr, Map<Integer, Integer> pnrSegIdAndNewFlgSegId, boolean isAlert,
			boolean noReprotectNoReturn, long version, boolean isSegmentTransferredByNorecProcess,
			List<Integer> reprotectFltSegIds, String visibleIBEonly, String salesChannelKey) throws ModuleException;

	/**
	 * Change segments (FOR CC/AGENT USERS)
	 * 
	 * @param pnr
	 * @param oldSegmentIds
	 * @param iSegment
	 * @param customChargesTO
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 * @param useOtherCrrierPaxCredit
	 * @param hasPreviousOtherCarrierPayments
	 * @param isOpenRetConfSegModification
	 * @param isActualPayment
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce changeSegments(String pnr, Collection<Integer> oldSegmentIds, ISegment iSegment,
			CustomChargesTO customChargesTO, Collection<TempSegBcAlloc> blockKeyIds, Integer paymentTypes, long version,
			TrackInfoDTO trackInfoDTO, Boolean isFlexiSelected, boolean enableECFraudChecking, boolean useOtherCrrierPaxCredit,
			boolean hasPreviousOtherCarrierPayments, boolean isOpenRetConfSegModification, boolean isActualPayment)
			throws ModuleException;

	/**
	 * Change segments (FOR CC/AGENT USERS) with REQUIRES_NEW transaction
	 * 
	 * @param pnr
	 * @param oldSegmentIds
	 * @param iSegment
	 * @param customChargesTO
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 * @param useOtherCrrierPaxCredit
	 * @param hasPreviousOtherCarrierPayments
	 * @param isOpenRetConfSegModification
	 * @param isActualPayment
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce changeSegmentsWithRequiresNew(String pnr, Collection<Integer> oldSegmentIds, ISegment iSegment,
			CustomChargesTO customChargesTO, Collection<TempSegBcAlloc> blockKeyIds, Integer paymentTypes, long version,
			TrackInfoDTO trackInfoDTO, Boolean isFlexiSelected, boolean enableECFraudChecking, boolean useOtherCrrierPaxCredit,
			boolean hasPreviousOtherCarrierPayments, boolean isOpenRetConfSegModification, boolean isActualPayment)
			throws ModuleException;

	public void intermediateRefund(String pnr, ISegment iSegment, TrackInfoDTO trackInfoDTO, String exceptionCode)
			throws ModuleException;

	/**
	 * Add segments
	 * 
	 * @param pnr
	 * @param iSegment
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 * @param useOtherCarrierPaxCredit
	 * @param hasExternalCarrierPayments
	 * @param isActualPayment
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce addSegments(String pnr, ISegment iSegment, Collection<TempSegBcAlloc> blockKeyIds,
			Integer paymentTypes, long version, TrackInfoDTO trackInfoDTO, boolean isFlexiSelected,
			boolean enableECFraudChecking, String selectedPnrSegID,
			Map<Integer, List<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap, boolean useOtherCarrierPaxCredit,
			boolean hasExternalCarrierPayments, boolean isActualPayment) throws ModuleException;

	/**
	 * Return segment view collection
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationSegmentDTO> getSegmentsView(String pnr) throws ModuleException;

	/**
	 * Return reservation segments
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationSegment> getPnrSegments(String pnr) throws ModuleException;

	/**
	 * Return reservations
	 * 
	 * @param flightSegId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Reservation> getPnrsForSegment(int flightSegId) throws ModuleException;

	/**
	 * Return pnr segments
	 * 
	 * @param flightSegIds
	 * @param loadReservation
	 *            TODO
	 * @param loadFares TODO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationSegment> getPnrSegments(List<Integer> flightSegIds, boolean loadReservation, boolean loadFares)
			throws ModuleException;

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws ModuleException
	 */
	public Integer getFlightSegmentId(String flightNumber, Date departureDate, String fromSegmentCode, String toSegmentCode)
			throws ModuleException;

	/**
	 * Returns the no.of confirmed and onhold pnrs for each segment of a flight
	 * 
	 * @param flightIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<FlightReservationSummaryDTO> getFlightReservationsSummary(Collection<Integer> flightIds)
			throws ModuleException;

	/**
	 * Save external segments
	 * 
	 * @param externalFlightSegment
	 * @throws ModuleException
	 */
	public ExternalFlightSegment saveExternalFlightSegment(ExternalFlightSegment externalFlightSegment) throws ModuleException;

	/**
	 * Retrieve external flight segment for a given carrier and externalFlightSegRef
	 * 
	 * @param carrierCode
	 * @param extFlightSegRef
	 * @return
	 * @throws ModuleException
	 */
	public ExternalFlightSegment getExternalFlightSegment(String carrierCode, Integer extFlightSegRef) throws ModuleException;

	/**
	 * Clear segment alerts
	 * 
	 * @param mapPnrSegActions
	 *            key pnr segment id type of integer value action taken of type string
	 * @throws ModuleException
	 */
	public void clearSegmentAlerts(Map<Integer, String> mapPnrSegActions) throws ModuleException;

	/**
	 * Alert Reservations
	 * 
	 * @param reservationAlertDTO
	 * @return
	 */
	public ServiceResponce alertReservations(ReservationAlertDTO reservationAlertDTO) throws ModuleException;

	/**
	 * Clear alerts
	 * 
	 * @param colAlertIDs
	 *            type of integer
	 * @throws ModuleException
	 */
	public void clearAlerts(Collection<Integer> colAlertIDs) throws ModuleException;

	/**
	 * Returns a collection of FlightSegmentDTO
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @param pnr
	 *            TODO
	 * @return
	 */
	public Collection<FlightReconcileDTO> getPnrSegmentsForReconcilation(String flightNumber, Date departureDate,
			String fromSegmentCode, String toSegmentCode, String pnr) throws ModuleException;

	/**
	 * Retunrs the FlightReconcileDTO corresponding to given flightSegmentId
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws ModuleException
	 */
	public FlightReconcileDTO getFlightSegmentsForReconcilation(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws ModuleException;

	/**
	 * Update the reservation segment transfer states
	 * 
	 * @param colReservationSegmentDTO
	 * @param transferPNR
	 * @param processDesc
	 * @param status
	 * @throws ModuleException
	 */
	public void updateReservationSegmentTransfer(Collection<ReservationSegmentDTO> colReservationSegmentDTO, String transferPNR,
			String processDesc, String status) throws ModuleException;

	/**
	 * Process Interlined Segments Transfer
	 * 
	 * @param executionTime
	 * @param interlinedAirLineTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce processInterlinedSegments(Date executionTime, InterlinedAirLineTO interlinedAirLineTO)
			throws ModuleException;

	/**
	 * Returns the reservation transfer segment information
	 * 
	 * @param colPnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, ReservationSegmentTransfer> getReservationTransferSegments(Collection<Integer> colPnrSegIds)
			throws ModuleException;

	/**
	 * Notify reservation segment transfer failures
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public void notifySegmentTransferFailures(Date executionDate) throws ModuleException;

	/**
	 * Add/Change External Segment Information
	 * 
	 * @param pnr
	 * @param colExternalSegmentTO
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce changeExternalSegments(String pnr, Collection<ExternalSegmentTO> colExternalSegmentTO, long version,
			TrackInfoDTO trackInfoDTO) throws ModuleException;
	
	/**
	 * Add/Change External segment for Dummy Reservation
	 * 
	 * @param reservation
	 * @param colExternalSegmentTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce changeExternalSegmentsForDummyReservation(Reservation reservation,
			Collection<ExternalSegmentTO> colExternalSegmentTO) throws ModuleException;

	/**
	 * Expire Open Return Segments
	 * 
	 * @param executionDate
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @throws ModuleException
	 */
	public void expireOpenReturnSegments(Date executionDate, BigDecimal customAdultCancelCharge,
			BigDecimal customChildCancelCharge, BigDecimal customInfantCancelCharge) throws ModuleException;

	/**
	 * Confirms the Open Return Segments
	 * 
	 * @param pnr
	 * @param pnrSegIds
	 * @param iSegment
	 * @param blockKeyIds
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce confirmOpenReturnSegments(String pnr, Collection<Integer> pnrSegIds, ISegment iSegment,
			Collection<TempSegBcAlloc> blockKeyIds, Integer paymentTypes, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public List<ResSegmentEticketInfoDTO> getEticketInformation(List<String> lccUniqueIDs, List<Integer> txnIDs)
			throws ModuleException;

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @param segmentCode
	 * @return
	 * @throws ModuleException
	 */
	public Integer getFlightSegmentIdBySegmentCode(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode, String segmentCode) throws ModuleException;

	/**
	 * Returns the correct flight segment ids
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Integer> getFlightSegmentIds(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws ModuleException;

	/**
	 * Update passenger fare segments
	 * 
	 * @param colPpfsId
	 * @param status
	 */
	public void updatePaxFareSegments(Collection<Integer> colPpfsId, String status) throws ModuleException;

	public Collection<Integer> getFlightSegmentIds(String pnr, boolean isCNFOnly) throws ModuleException;

	public ServiceResponce createAlert(String pnr, Collection<Integer> pnrSegIds, String description,
			LCCClientReservation lccReservation, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public Map<String, BigDecimal> getExistingModificationChargesDue(List<Integer> pnrSegIds) throws ModuleException;

	public void auditFlightTimeChanges(Collection<Integer> flightSegmentIDs, Collection<FlightSegement> updatedSegList,
			String flightNo, String userId) throws ModuleException;

	void cancelAirportTransferRequests(Reservation reservation, Collection<Integer> pnrSegmentIds,
			Map<Integer, Collection<PaxAirportTransferTO>> cancelledSSRMap, CredentialsDTO credentialsDTO);
	
	public ServiceResponce exchangeReservationSegment(String pnr,Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public ServiceResponce changeOtherAirlineSegments(String pnr, Collection<OtherAirlineSegmentTO> colExternalSegmentTO,
			long version, TrackInfoDTO trackInfoDTO) throws ModuleException;

}
