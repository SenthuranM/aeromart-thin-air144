package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class EQN extends EDISegment {

	private String e6350;

	public EQN() {
		super(EDISegmentTag.EQN);
	}

	public void setNumberOfPNRs(String e6350) {
		this.e6350 = e6350;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C523 = new CompoundDataElement("C523", DE_STATUS.M);
		C523.addBasicDataelement(new Numeric(EDI_ELEMENT.E6350, e6350));

		addEDIDataElement(C523);
		return this;
	}

}
