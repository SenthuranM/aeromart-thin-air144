package com.isa.thinair.airreservation.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReprotectedPassenger;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ReprotectedPaxDTO;
import com.isa.thinair.promotion.api.to.ReprotectedPaxSearchrequest;

public interface ReprotectedPassengerBD {
	
	public static final String SERVICE_NAME = "ReservationService";
	
	public void saveAllReprotectedPassengers (Collection<ReprotectedPassenger> reprotectedPassengers);
	
	public List<ReprotectedPassenger> getAllEntriesOfPaxList (List<Integer> pnrPaxIds);	

	public Page<ReprotectedPaxDTO> searchReprotectedPax(int start, int pageSize, ReprotectedPaxSearchrequest reprotectedPaxSearchrequest);

	public int getReprotectedPaxCount (ReprotectedPaxSearchrequest reprotectedPaxSearchrequest);

}
