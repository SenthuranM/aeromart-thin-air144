package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 */
public class PerPaxPriceBreakdown implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7652455522533988749L;

	private BigDecimal totalBasePayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal surAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal otherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public PerPaxPriceBreakdown(BigDecimal fareAmount, BigDecimal taxAmount, BigDecimal surAmount, BigDecimal otherAmount) {
		this.setFareAmount(fareAmount);
		this.setTaxAmount(taxAmount);
		this.setSurAmount(surAmount);
		this.setOtherAmount(otherAmount);
	}

	/**
	 * @return the fareAmount
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            the fareAmount to set
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return the taxAmount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	/**
	 * @param taxAmount
	 *            the taxAmount to set
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the surAmount
	 */
	public BigDecimal getSurAmount() {
		return surAmount;
	}

	/**
	 * @param surAmount
	 *            the surAmount to set
	 */
	public void setSurAmount(BigDecimal surAmount) {
		this.surAmount = surAmount;
	}

	/**
	 * @return the totalBasePayAmount
	 */
	public BigDecimal getTotalBasePayAmount() {
		return totalBasePayAmount;
	}

	/**
	 * @param totalBasePayAmount
	 *            the totalBasePayAmount to set
	 */
	public void setTotalBasePayAmount(BigDecimal totalBasePayAmount) {
		this.totalBasePayAmount = totalBasePayAmount;
	}

	/**
	 * @return the otherAmount
	 */
	public BigDecimal getOtherAmount() {
		return otherAmount;
	}

	/**
	 * @param otherAmount
	 *            the otherAmount to set
	 */
	public void setOtherAmount(BigDecimal otherAmount) {
		this.otherAmount = otherAmount;
	}
}
