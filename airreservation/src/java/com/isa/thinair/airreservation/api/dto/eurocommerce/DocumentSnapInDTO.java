package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

/**
 * DocumentSnapInDTO dto
 * 
 * @author mekanayake
 */
public class DocumentSnapInDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2784368603763071358L;

	private String name;

	private String discription;

	DocumentSnapInDTO(String name, String discription) {
		super();
		this.name = name;
		this.discription = discription;
	}

	public DocumentSnapInDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	@Override
	public String toString() {
		return "DocumentSnapInDTO [discription=" + discription + ", name=" + name + "]";
	}

}
