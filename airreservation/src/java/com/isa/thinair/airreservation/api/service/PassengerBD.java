/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 * @version $Id$
 */

package com.isa.thinair.airreservation.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.dto.ReservationPaxSegmentPaymentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.*;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPassengerDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Interface to define all the methods required to access the reservation passenger details
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface PassengerBD {
	public static final String SERVICE_NAME = "ReservationService";

	/**
	 * Remove passengers
	 * 
	 * @param pnr
	 * @param pnrPaxIds
	 * @param customChargesTO
	 * @param version
	 * @param trackInfoDTO
	 * @param paxExternalCharges
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce removePassengers(String pnr, String newPnr, Collection<Integer> pnrPaxIds,
			CustomChargesTO customChargesTO, long version, TrackInfoDTO trackInfoDTO, String userNotes,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges) throws ModuleException;

	/**
	 * Add Infant
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 * @param autoCancellationInfo
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce addInfant(String pnr, IPassenger iPassenger, Collection<TempSegBcAlloc> blockKeyIds,
			Integer paymentTypes, long version, Map<Integer, Map<Integer, EticketDTO>> paxSegEticket, TrackInfoDTO trackInfoDTO,
			boolean enableECFraudChecking, boolean goshowInfant, boolean otherCarrierCreditUsed,
			AutoCancellationInfo autoCancellationInfo) throws ModuleException;

	/**
	 * Add Infant with REQUIRES_NEW transaction
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param blockKeyIds
	 * @param paymentTypes
	 * @param version
	 * @param paxSegEticket
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce addInfantWithRequiresNew(String pnr, IPassenger iPassenger, Collection<TempSegBcAlloc> blockKeyIds,
			Integer paymentTypes, long version, Map<Integer, Map<Integer, EticketDTO>> paxSegEticket, TrackInfoDTO trackInfoDTO,
			boolean enableECFraudChecking, boolean goshowInfant) throws ModuleException;

	/**
	 * add infant to dummy reservation
	 * 
	 * @param reservation
	 * @param adultSequence
	 * @param iPassenger
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce addInfantForDummyReservation(Reservation reservation, IPassenger iPassenger) throws ModuleException;

	/**
	 * Return passengers
	 * 
	 * @param pnr
	 * @param loadFares
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationPax> getPassengers(String pnr, boolean loadFares) throws ModuleException;

	/**
	 * Return passenger
	 * 
	 * @param pnrPaxId
	 * @param loadFares
	 * @return
	 * @throws ModuleException
	 */
	public ReservationPax getPassenger(int pnrPaxId, boolean loadFares) throws ModuleException;

	/**
	 * Passenger Refund
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param paymentTnxId
	 *            TODO
	 * @param userNotes
	 * @param isCapturePayment
	 *            TODO
	 * @param version
	 * @param trackInfoDTO
	 * @param preferredRefundOrder
	 *            Order the refund should deduct charges. (TAX,SUR etc.)
	 * @param pnrPaxOndChgIds
	 *            PaxOndChgIds related to the particular segment that charge should be deducted from.
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce passengerRefund(String pnr, IPassenger iPassenger, String userNotes, boolean isCapturePayment,
			long version, TrackInfoDTO trackInfoDTO, boolean isAutoRefund, Collection<String> preferredRefundOrder,
			Collection<Long> pnrPaxOndChgIds, boolean removeAgentCommission) throws ModuleException;

	/**
	 * 
	 * Multi pax multi refund. TODO : COmplete documentation and deprecate passengerRefund when dry group refund is
	 * implemented
	 * 
	 * @param isManualRefund
	 */
	public ServiceResponce groupPassengerRefund(String pnr, IPassenger iPassenger, String userNotes, boolean isCapturePayment,
			long version, TrackInfoDTO trackInfoDTO, boolean isAutoRefund, Collection<String> preferredRefundOrder,
			Collection<Long> pnrPaxOndChgIds, boolean removeAgentCommission, boolean isManualRefund) throws ModuleException;

	/**
	 * Adjust credit manually
	 * 
	 * @param pnr
	 * @param pnrPaxFareId
	 * @param chargeRateID
	 * @param amount
	 * @param userNotes
	 * @param version
	 * @param totalPlusAdjByPaxFareId
	 * @param trackInfoDTO
	 * @param serviceTaxRS TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce adjustCreditManual(String pnr, int pnrPaxFareId, Integer chargeRateID, BigDecimal amount,
			String userNotes, long version, Map<Integer, BigDecimal> totalPlusAdjByPaxFareId, TrackInfoDTO trackInfoDTO, ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS)
			throws ModuleException;

	/**
	 * Adjust credit manually for a group
	 * 
	 * @param pnr
	 * @param pnrPaxFareIds
	 * @param chargeRateID
	 * @param amount
	 * @param userNotes
	 * @param version
	 * @param totalPlusAdjByPaxFareId
	 * @param trackInfoDTO
	 * @param serviceTaxRS TODO
	 * @param handlingFeeByPax
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce adjustCreditManual(String pnr, List<Integer> pnrPaxFareIds, Integer chargeRateID, BigDecimal amount,
			String userNotes, long version, Map<Integer, BigDecimal> totalPlusAdjByPaxFareId, TrackInfoDTO trackInfoDTO,
			ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS, Map<Integer, List<ExternalChgDTO>> handlingFeeByPax)
			throws ModuleException;

	/**
	 * Expire Passenger credits
	 * 
	 * @param date
	 * @throws ModuleException
	 */
	public void expirePassengerCredits(Date date) throws ModuleException;

	/**
	 * Return reservation passenger payments
	 * 
	 * @param colPnrPaxId
	 * @param currencyCode
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefunds(Collection<Integer> colPnrPaxId)
			throws ModuleException;

	public Map<Integer, Collection<ReservationTnx>> getPNRPaxPaymentsAndRefundsTnx(Collection<Integer> colPnrPaxId)
			throws ModuleException;

	/**
	 * Return reservation passenger Titles
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, String> getPassengerTitleMap() throws ModuleException;

	/**
	 * Returns Object[]{pnrPaxId, pnrPaxFareId, paxType} corresponding to first adult or child pax with pnrPaxFareId,
	 * paxType linked to first segment.
	 * 
	 * @param pnr
	 * @return Object[]{pnrPaxId, pnrPaxFareId, paxType}
	 */
	public Object[] getFirstPaxPnrPaxFareId(String pnr) throws ModuleException;

	/**
	 * Returns all credits
	 * 
	 * @param pnrPaxId
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, Collection<CreditInfoDTO>> getAllCredits(Collection<Integer> pnrPaxIds) throws ModuleException;

	/**
	 * Extend Credits
	 * 
	 * @param creditId
	 * @param expiryDate
	 * @param note
	 *            TODO
	 * @param pnr
	 *            TODO
	 * @param pnrPaxId
	 *            TODO
	 * @param trackInfoDTO
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce extendCredits(long creditId, Date expiryDate, String note, String pnr, int pnrPaxId,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Reinstate the credit
	 * 
	 * @param pnr
	 * @param pnrPaxId
	 * @param paxTxnId
	 * @param amount
	 * @param expDate
	 * @param note
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reinstateCredit(String pnr, int pnrPaxId, int paxTxnId, BigDecimal amount, Date expDate, String note,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public Map<Integer, Collection<ReservationTnx>> getAllBalanceTransactions(Collection<Integer> pnrPaxIds)
			throws ModuleException;

	/**
	 * Get the ReservationPaxOndPayment form refundable chargers
	 * 
	 * @param pnrPaxId
	 * @param colPnrPaxOndChgId
	 * @return
	 * @throws ModuleException
	 */
	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPaymentByOndChargeId(Integer pnrPaxId,
			Collection<Long> colPnrPaxOndChgId) throws ModuleException;

	/**
	 * Get pax transaction for given pnrPaxId and lccUnique tnx id
	 * 
	 * @param pnrPaxId
	 * @param lccUniqueId
	 * @return
	 */
	public ReservationTnx getLccTransaction(String pnrPaxId, String lccUniqueId);

	/**
	 * Calculate payment amount from the ondFareDto
	 * 
	 * @param colOndFareDTO
	 * @param paxType
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getPayedAmountFromOndFareDto(Collection<OndFareDTO> colOndFareDTO, String paxType) throws ModuleException;

	/**
	 * Return ReservationPaxOndPayments for given transaction ids
	 * 
	 * @param pnrPaxId
	 * @param lccUniqueId
	 * @return
	 */
	public Map<Long, Collection<ReservationPaxOndPayment>> getReservationPaxOndPayments(Collection<Long> colPaymentTnxIds)
			throws ModuleException;

	/**
	 * Retrieves the payment breakdown for Own Reservations and/or Group Reservation for per pax per segment for a given
	 * list(s) of pax-tnx-ids and/or lcc-unique-tnx-ids.
	 * 
	 * Note: You can specify only 1 parameter and make the other null but not both the parameters could be null.
	 * 
	 * @param colPaymentTnxIds
	 *            : The own reservation pax tnx id list.
	 * @param paxSeqLccUniqueTIDPairCollection
	 *            : Group reservation lcc unique tnx id list.
	 * @param requestingNominalCodes
	 *            : The nominal codes for which the transaction breakdown should be retrieved.
	 * 
	 * @return : A map of pax segment payment breakdown who's key is a {@link Pair} object with left as pax_tnx_id or
	 *         lcc-unique-tnx-id and the right as pax sequence. The value for a given key is a Collection of
	 *         {@link ReservationPaxSegmentPaymentTO} object(s).
	 * 
	 * @see : {@link ReservationPaxSegmentPaymentTO}
	 * 
	 * @throws ModuleException
	 *             : If both the tnx id collections are null or if a data access exception occurs.
	 */
	public Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> getReservationPaxSegmentPayments(
			Collection<Integer> colPaymentTnxIds, Collection<String> paxSeqLccUniqueTIDs,
			Collection<Integer> requestingNominalCodes) throws ModuleException;

	/**
	 * Retrieves handling charge per transaction per pax for Own Reservations and/or Group Reservations for a given
	 * list(s) of pax-tnx-ids and/or lcc-unique-tnx-ids.
	 * 
	 * @param transactionIds
	 *            : The list of own pax-tnx-id list for which the handling charges are required.
	 * @param lccUniqueTnxIds
	 *            : The lcc unique tnx ids for which the handling charges are required.
	 * @return : A map of handling fees where the key is a {@link Pair} object who's left as pax_tnx_id or
	 *         lcc-unique-tnx-id and the right as pax sequence
	 * 
	 * @throws ModuleException
	 *             : : If both the tnx id collections are null or if a data access exception occurs.
	 */
	public Map<Pair<String, Integer>, BigDecimal> getHandlingChargesForPaxTnxs(Collection<Integer> transactionIds,
			Collection<String> lccUniqueTnxIds) throws ModuleException;

	/**
	 * Retrieves the passenger Id by the E-Ticket Number of the passenger
	 * 
	 * @param eticketNumber
	 * @return
	 * @throws ModuleException
	 */
	public Integer getPassengerIdByETicketNumber(String eticketNumber) throws ModuleException;

	/**
	 * Retrieves the passenger E Ticket Details by passenger id and flight Id.
	 * 
	 * @param pnrPaxId
	 * @param flightId
	 * @return {@link EticketTO}
	 * @throws ModuleException
	 */
	public EticketTO getPassengerETicketDetail(Integer pnrPaxId, Integer flightId) throws ModuleException;

	public void updatePassengerCoupon(PassengerCouponUpdateRQ paxCouponUpdateRQ, TrackInfoDTO trackInfo, boolean allowExceptions)
			throws ModuleException;

	public void updateGroupPassengerCoupon(List<PassengerCouponUpdateRQ> groupPaxCouponUpdateRQ, TrackInfoDTO trackInfo,
			boolean allowExceptions) throws ModuleException;
	
	public ServiceResponce passengerRefundWithRequiresNew(String pnr, IPassenger iPassenger, String userNotes,
			boolean isCapturePayment, long version, TrackInfoDTO trackInfoDTO, boolean removeAgentCommission)
			throws ModuleException;

	public void auditPaxAutoRefund(String pnr, String paxInfo, BigDecimal amount, String status, String txnDetails,
			CredentialsDTO credentialsDTO, int auditOption, ReservationAdminInfo adminInfo) throws ModuleException;

	public ServiceResponce passengerAutoRefund(String pnr, boolean refundOnlyNSPax, TrackInfoDTO trackInfoDTO,
			boolean isVoidOperation, String ownerChannelId) throws ModuleException;

	public Map<Integer, Collection<ChargeMetaTO>> getPnrPaxIdWiseCurrentCharges(String pnr, long version,
			boolean excludedPnrSegCharges, List<String> excludedPnrSegIds) throws ModuleException;

	public ServiceResponce removePassengersWithRequiresNew(String pnr, String newPnr, Collection<Integer> pnrPaxIds,
			CustomChargesTO customChargesTO, long version, TrackInfoDTO trackInfoDTO, String userNotes,
			Map<Integer, List<ExternalChgDTO>> paxExternalCharges) throws ModuleException;

	public List<PNRGOVPassengerDTO> getPassengersForPNR(String pnr);

	public IbeExitUserDetailsDTO saveIbeExitDetais(IbeExitUserDetailsDTO ibeExitDetails);

	public IbeExitUserDetailsDTO getIbeExitDetais(Integer exitDetailsId);

	public Map<String, Collection<String>> getPassengerTitleTypes(Collection<String> excludingPaxTypes) throws ModuleException;

	public ServiceResponce adjustGDSChargesBulkManual(String pnr,
			Map<Integer, Map<Integer, Map<String, BigDecimal>>> paxWiseAjustments, long version, String userNote,
			Map<Integer, BigDecimal> totalPlusAdjByPaxFareId, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public void markPaymentTransactionNotFirst(Integer pnrPaxID, Integer tnxID, String lccUniqueTnxID) throws ModuleException ;
	
	public ServiceResponce reverseRefundableCharges(String pnr, Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			boolean isNoShoTaxReversal, TrackInfoDTO trackInfoDTO) throws ModuleException;

    public List<ConnectionPaxInfoTO> getConnectionPaxInfo(Date fromDate,Date toDate,int connectionTime, List<String> segments)
            throws ModuleException;

}
