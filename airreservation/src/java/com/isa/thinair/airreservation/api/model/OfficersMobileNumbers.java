package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of OfficersMobileNumbers
 * 
 * @hibernate.class table = "t_officers_mobile_numbers"
 * 
 */

public class OfficersMobileNumbers extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int OfficerId;

	private String OfficerName;

	private String MobNumber;
	
	private String user;
	
	private String emailAddress;
	

	/**
	 * 
	 * @return
	 * @hibernate.id column = "OFFICER_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_OFFICERS_MOBILE_NUMBERS "
	 * 
	 */
	public int getOfficerId() {
		return OfficerId;
	}

	public void setOfficerId(int OfficerId) {
		this.OfficerId = OfficerId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "OFFICER_NAME"
	 * 
	 */

	public String getOfficerName() {
		return OfficerName;
	}

	public void setOfficerName(String officerName) {
		OfficerName = officerName;
	}
	
	/**
	 * 
	 * @return
	 * @hibernate.property column = "MOBILE_NUMBER"
	 * 
	 */

	public String getMobNumber() {
		return MobNumber;
	}

	public void setMobNumber(String mobNumber) {
		MobNumber = mobNumber;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the emailAddress
	 * @hibernate.property column = "EMAIL_ADDRESS"
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
}
