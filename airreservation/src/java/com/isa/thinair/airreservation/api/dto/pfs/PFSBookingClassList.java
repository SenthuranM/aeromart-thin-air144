/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;


/**
 * @author ashain
 *
 */
public class PFSBookingClassList {
	private String paxTravelStatusDisplay;
	private String paxFirstName;
	private String paxLastName;
	private String paxEticket;
	private String paxTitle;
	private String paxStatus;
	private String paxTravelStatus;
	private String paxCoupenNo;
	private String pnr;
	private String baggageDiscription;
	private String seatCode;
	private String ssrDescription;
	private String mealDescription;
	private String bookingClass;
	
	
	/**
	 * @return the paxTravelStatusDisplay
	 */
	public String getPaxTravelStatusDisplay() {
		return paxTravelStatusDisplay;
	}
	/**
	 * @param paxTravelStatusDisplay the paxTravelStatusDisplay to set
	 */
	public void setPaxTravelStatusDisplay(String paxTravelStatusDisplay) {
		this.paxTravelStatusDisplay = paxTravelStatusDisplay;
	}
	/**
	 * @return the paxFirstName
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}
	/**
	 * @param paxFirstName the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}
	/**
	 * @return the paxLastName
	 */
	public String getPaxLastName() {
		return paxLastName;
	}
	/**
	 * @param paxLastName the paxLastName to set
	 */
	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}
	/**
	 * @return the paxEticket
	 */
	public String getPaxEticket() {
		return paxEticket;
	}
	/**
	 * @param paxEticket the paxEticket to set
	 */
	public void setPaxEticket(String paxEticket) {
		this.paxEticket = paxEticket;
	}
	/**
	 * @return the paxTitle
	 */
	public String getPaxTitle() {
		return paxTitle;
	}
	/**
	 * @param paxTitle the paxTitle to set
	 */
	public void setPaxTitle(String paxTitle) {
		this.paxTitle = paxTitle;
	}
	/**
	 * @return the paxStatus
	 */
	public String getPaxStatus() {
		return paxStatus;
	}
	/**
	 * @param paxStatus the paxStatus to set
	 */
	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}
	/**
	 * @return the paxTravelStatus
	 */
	public String getPaxTravelStatus() {
		return paxTravelStatus;
	}
	/**
	 * @param paxTravelStatus the paxTravelStatus to set
	 */
	public void setPaxTravelStatus(String paxTravelStatus) {
		this.paxTravelStatus = paxTravelStatus;
	}
	/**
	 * @return the paxCoupenNo
	 */
	public String getPaxCoupenNo() {
		return paxCoupenNo;
	}
	/**
	 * @param paxCoupenNo the paxCoupenNo to set
	 */
	public void setPaxCoupenNo(String paxCoupenNo) {
		this.paxCoupenNo = paxCoupenNo;
	}
	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}
	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	/**
	 * @return the baggageDiscription
	 */
	public String getBaggageDiscription() {
		return baggageDiscription;
	}
	/**
	 * @param baggageDiscription the baggageDiscription to set
	 */
	public void setBaggageDiscription(String baggageDiscription) {
		this.baggageDiscription = baggageDiscription;
	}
	/**
	 * @return the seatCode
	 */
	public String getSeatCode() {
		return seatCode;
	}
	/**
	 * @param seatCode the seatCode to set
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}
	/**
	 * @return the ssrDescription
	 */
	public String getSsrDescription() {
		return ssrDescription;
	}
	/**
	 * @param ssrDescription the ssrDescription to set
	 */
	public void setSsrDescription(String ssrDescription) {
		this.ssrDescription = ssrDescription;
	}
	/**
	 * @return the mealDescription
	 */
	public String getMealDescription() {
		return mealDescription;
	}
	/**
	 * @param mealDescription the mealDescription to set
	 */
	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}
	
//	/**
//	 * @return the baggageTagLst
//	 */
//	public List<BaggageTagInfo> getBaggageTagLst() {
//		return baggageTagLst;
//	}
//	/**
//	 * @param baggageTagLst the baggageTagLst to set
//	 */
//	public void setBaggageTagLst(List<BaggageTagInfo> baggageTagLst) {
//		this.baggageTagLst = baggageTagLst;
//	}
	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}
	/**
	 * @param bookingClass the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
	
	
}
