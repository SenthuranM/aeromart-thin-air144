/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * To hold Segment Seats information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class SeatsForSegmentDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7982006528067587736L;

	/** Holds the Passenger Fare ID (Which is the ond ID) */
	private int pnrPaxFareId;

	/** Holds the Passenger Fare Segment ID */
	private int pnrPaxFareSegmentId;

	/** Holds the number of adults */
	private int noOfAdults;

	/** Holds the number of infants */
	private int noOfInfants;

	/** Holds whether the seats are onhold or confirmed */
	private boolean isOnHoldSeat;

	/** Holds the flight segment id */
	private int flightSegmentId;

	/** Holds the booking code */
	private String bookingCode;
	
	private boolean waitListedSeat;

	/**
	 * Default constructor is hidden in order to avoid missing information
	 */
	@SuppressWarnings("unused")
	private SeatsForSegmentDTO() {

	}

	/**
	 * Creates seats for a segment data transfer information
	 * 
	 * @param pnrPaxFareId
	 * @param pnrPaxFareSegmentId
	 * @param flightSegmentId
	 * @param bookingCode
	 * @param noOfAdults
	 * @param noOfInfants
	 * @param isOnHoldSeat
	 */
	public SeatsForSegmentDTO(int pnrPaxFareId, int pnrPaxFareSegmentId, int flightSegmentId, String bookingCode, int noOfAdults,
			int noOfInfants, boolean isOnHoldSeat, boolean isWaitListed) {
		this.setPnrPaxFareId(pnrPaxFareId);
		this.setPnrPaxFareSegmentId(pnrPaxFareSegmentId);
		this.setFlightSegmentId(flightSegmentId);
		this.setBookingCode(bookingCode);
		this.setNoOfAdults(noOfAdults);
		this.setNoOfInfants(noOfInfants);
		this.setOnHoldSeat(isOnHoldSeat);
		this.setWaitListedSeat(isWaitListed);
	}

	/**
	 * @return Returns the isOnHoldSeat.
	 */
	public boolean isOnHoldSeat() {
		return isOnHoldSeat;
	}

	/**
	 * @param isOnHoldSeat
	 *            The isOnHoldSeat to set.
	 */
	private void setOnHoldSeat(boolean isOnHoldSeat) {
		this.isOnHoldSeat = isOnHoldSeat;
	}

	/**
	 * @return Returns the noOfAdults.
	 */
	public int getNoOfAdults() {
		return noOfAdults;
	}

	/**
	 * @param noOfAdults
	 *            The noOfAdults to set.
	 */
	private void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	/**
	 * @return Returns the noOfInfants.
	 */
	public int getNoOfInfants() {
		return noOfInfants;
	}

	/**
	 * @param noOfInfants
	 *            The noOfInfants to set.
	 */
	private void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	/**
	 * @return Returns the pnrPaxFareId.
	 */
	public int getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            The pnrPaxFareId to set.
	 */
	private void setPnrPaxFareId(int pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return Returns the bookingCode.
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            The bookingCode to set.
	 */
	private void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return Returns the flightSegmentId.
	 */
	public int getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            The flightSegmentId to set.
	 */
	private void setFlightSegmentId(int flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @return Returns the pnrPaxFareSegmentId.
	 */
	public int getPnrPaxFareSegmentId() {
		return pnrPaxFareSegmentId;
	}

	/**
	 * @param pnrPaxFareSegmentId
	 *            The pnrPaxFareSegmentId to set.
	 */
	private void setPnrPaxFareSegmentId(int pnrPaxFareSegmentId) {
		this.pnrPaxFareSegmentId = pnrPaxFareSegmentId;
	}

	public boolean isWaitListedSeat() {
		return waitListedSeat;
	}

	public void setWaitListedSeat(boolean waitListedSeat) {
		this.waitListedSeat = waitListedSeat;
	}	
}
