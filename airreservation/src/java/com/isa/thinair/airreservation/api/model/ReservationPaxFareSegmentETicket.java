package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author eric
 * @hibernate.class table = "T_PNR_PAX_FARE_SEG_E_TICKET"
 */
public class ReservationPaxFareSegmentETicket extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer paxFareSegemntEticketId;

	private Integer pnrPaxFareSegId;

	private Integer couponNo;
	
	private Integer externalCouponNo;

	private String status;

	/** Holds a instance of reservation pax E ticket */
	private ReservationPaxETicket reservationPaxETicket;

	private String externalEticketNumber;

	private String externalCouponStatus;

	private String externalCouponControl;

	public interface ExternalCouponControl {
		String VALIDATING_CARRIER = "V";
		String MARKETING_CARRIER = "M";
		String OPERATING_CARRIER = "O";
		String GROUND_HANDLER = "G";
	}

	/**
	 * 
	 * @hibernate.id column = "PNR_PAX_FARE_SEG_E_TICKET_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_FARE_SEG_E_TICKET"
	 */
	public Integer getPaxFareSegemntEticketId() {
		return paxFareSegemntEticketId;
	}

	/**
	 * @param paxFareSegemntEticketId
	 */
	public void setPaxFareSegemntEticketId(Integer paxFareSegemntEticketId) {
		this.paxFareSegemntEticketId = paxFareSegemntEticketId;
	}

	/**
	 * @return
	 * @hibernate.property column = "PPFS_ID"
	 */
	public Integer getPnrPaxFareSegId() {
		return pnrPaxFareSegId;
	}

	/**
	 * @param pnrPaxFareSegId
	 */
	public void setPnrPaxFareSegId(Integer pnrPaxFareSegId) {
		this.pnrPaxFareSegId = pnrPaxFareSegId;
	}

	/**
	 * @return Returns the ReservationPaxETicket.
	 * @hibernate.many-to-one column = "PAX_E_TICKET_ID"
	 *                        class="com.isa.thinair.airreservation.api.model.ReservationPaxETicket"
	 *                        cascade="save-update"
	 * 
	 */
	public ReservationPaxETicket getReservationPaxETicket() {
		return reservationPaxETicket;
	}

	/**
	 * @param reservationPaxETicket
	 */
	public void setReservationPaxETicket(ReservationPaxETicket reservationPaxETicket) {
		this.reservationPaxETicket = reservationPaxETicket;
	}

	/**
	 * @return
	 * @hibernate.property column = "STATUS"
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return
	 * @hibernate.property column = "COUPON_NUMBER"
	 */
	public Integer getCouponNo() {
		return couponNo;
	}

	/**
	 * @param couponNo
	 */
	public void setCouponNo(Integer couponNo) {
		this.couponNo = couponNo;
	}

	/**
	 * @hibernate.property column ="EXT_E_TICKET_NUMBER"
	 */
	public String getExternalEticketNumber() {
		return externalEticketNumber;
	}

	public void setExternalEticketNumber(String externalEticketNumber) {
		this.externalEticketNumber = externalEticketNumber;
	}

	/**
	 * @return
	 * @hibernate.property column = "EXT_COUPON_NUMBER"
	 */
	public Integer getExternalCouponNo() {
		return externalCouponNo;
	}

	/**
	 * @param externalCouponNo
	 */
	public void setExternalCouponNo(Integer externalCouponNo) {
		this.externalCouponNo = externalCouponNo;
	}

	/**
	 * @hibernate.property column = "EXT_COUPON_STATUS"
	 */
	public String getExternalCouponStatus() {
		return externalCouponStatus;
	}

	public void setExternalCouponStatus(String externalCouponStatus) {
		this.externalCouponStatus = externalCouponStatus;
	}

	/**
	 * @hibernate.property column = "EXT_COUPON_CONTROL"
	 */
	public String getExternalCouponControl() {
		return externalCouponControl;
	}

	public void setExternalCouponControl(String externalCouponControl) {
		this.externalCouponControl = externalCouponControl;
	}
}
