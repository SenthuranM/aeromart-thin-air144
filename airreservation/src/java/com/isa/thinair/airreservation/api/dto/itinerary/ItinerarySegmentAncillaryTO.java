package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.List;

public class ItinerarySegmentAncillaryTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private ItinerarySegmentTO segment;
	private ItinerarySeatTO seat;
	private List<ItinerarySeatTO> extraSeats;
	private List<ItineraryMealTO> meals;
	private List<ItineraryBaggageTO> baggages;
	private List<ItinerarySSRTO> ssrs;
	private List<ItineraryAirportServiceTO> apss;
	private List<ItineraryAirportTransferTO> apts;
	private boolean insured;

	public ItinerarySegmentTO getSegment() {
		return segment;
	}

	public void setSegment(ItinerarySegmentTO segment) {
		this.segment = segment;
	}

	public ItinerarySeatTO getSeat() {
		return seat;
	}

	public void setSeat(ItinerarySeatTO seat) {
		this.seat = seat;
	}

	public List<ItineraryMealTO> getMeals() {
		return meals;
	}

	public void setMeals(List<ItineraryMealTO> meals) {
		this.meals = meals;
	}

	public List<ItinerarySSRTO> getSsrs() {
		return ssrs;
	}

	public void setSsrs(List<ItinerarySSRTO> ssrs) {
		this.ssrs = ssrs;
	}

	public boolean isInsured() {
		return insured;
	}

	public void setInsured(boolean insured) {
		this.insured = insured;
	}

	/**
	 * @return the baggages
	 */
	public List<ItineraryBaggageTO> getBaggages() {
		return baggages;
	}

	/**
	 * @param baggages
	 *            the baggages to set
	 */
	public void setBaggages(List<ItineraryBaggageTO> baggages) {
		this.baggages = baggages;
	}

	/**
	 * @return the apss
	 */
	public List<ItineraryAirportServiceTO> getApss() {
		return apss;
	}

	/**
	 * @param apss
	 *            the apss to set
	 */
	public void setApss(List<ItineraryAirportServiceTO> apss) {
		this.apss = apss;
	}

	public List<ItinerarySeatTO> getExtraSeats() {
		return extraSeats;
	}

	public void setExtraSeats(List<ItinerarySeatTO> extraSeats) {
		this.extraSeats = extraSeats;
	}

	public List<ItineraryAirportTransferTO> getApts() {
		return apts;
	}

	public void setApts(List<ItineraryAirportTransferTO> apts) {
		this.apts = apts;
	}

}
