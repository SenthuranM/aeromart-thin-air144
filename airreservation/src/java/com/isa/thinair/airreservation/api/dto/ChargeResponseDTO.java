package com.isa.thinair.airreservation.api.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * DTO to response ond Charge amounts
 */

public class ChargeResponseDTO {

	private BigDecimal cancelTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal modifyTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal nameChangeTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal[] refundAmounts;

	private Collection<ChargeMetaTO> refundableChargeMetaTOs = new ArrayList<ChargeMetaTO>();

	private Collection<ChargeMetaTO> nonRefundableChargeMetaTOs = new ArrayList<ChargeMetaTO>();

	private PnrChargeDetailTO cnxChargeDetailDTO;

	private PnrChargeDetailTO modChargeDetailDTO;

	private PnrChargeDetailTO nameChangeChargeDetailDTO;

	public BigDecimal getCancelTotalCharge() {
		return cancelTotalCharge;
	}

	public void setCancelTotalCharge(BigDecimal cancelTotalCharge) {
		this.cancelTotalCharge = cancelTotalCharge;
	}

	public BigDecimal getModifyTotalCharge() {
		return modifyTotalCharge;
	}

	public void setModifyTotalCharge(BigDecimal modifyTotalCharge) {
		this.modifyTotalCharge = modifyTotalCharge;
	}

	public BigDecimal getNameChangeTotalCharge() {
		return nameChangeTotalCharge;
	}

	public void setNameChangeTotalCharge(BigDecimal nameChangeTotalCharge) {
		this.nameChangeTotalCharge = nameChangeTotalCharge;
	}

	public BigDecimal[] getRefundAmounts() {
		return refundAmounts;
	}

	public void setRefundAmounts(BigDecimal[] refundAmounts) {
		this.refundAmounts = refundAmounts;
	}

	public Collection<ChargeMetaTO> getRefundableChargeMetaTOs() {
		return refundableChargeMetaTOs;
	}

	public void setRefundableChargeMetaTOs(Collection<ChargeMetaTO> refundableChargeMetaTOs) {
		this.refundableChargeMetaTOs = refundableChargeMetaTOs;
	}

	public Collection<ChargeMetaTO> getNonRefundableChargeMetaTOs() {
		return nonRefundableChargeMetaTOs;
	}

	public void setNonRefundableChargeMetaTOs(Collection<ChargeMetaTO> nonRefundableChargeMetaTOs) {
		this.nonRefundableChargeMetaTOs = nonRefundableChargeMetaTOs;
	}

	public PnrChargeDetailTO getCnxChargeDetailDTO() {
		return cnxChargeDetailDTO;
	}

	public void setCnxChargeDetailDTO(PnrChargeDetailTO cnxChargeDetailDTO) {
		this.cnxChargeDetailDTO = cnxChargeDetailDTO;
	}

	public PnrChargeDetailTO getModChargeDetailDTO() {
		return modChargeDetailDTO;
	}

	public void setModChargeDetailDTO(PnrChargeDetailTO modChargeDetailDTO) {
		this.modChargeDetailDTO = modChargeDetailDTO;
	}

	public PnrChargeDetailTO getNameChangeChargeDetailDTO() {
		return nameChangeChargeDetailDTO;
	}

	public void setNameChangeChargeDetailDTO(PnrChargeDetailTO nameChangeChargeDetailDTO) {
		this.nameChangeChargeDetailDTO = nameChangeChargeDetailDTO;
	}

}
