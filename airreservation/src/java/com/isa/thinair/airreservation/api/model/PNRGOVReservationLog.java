package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * This will keep track of Pnr Gov Reservation History Log
 * 
 * @author Chinthaka Weerakkody
 * @since 1.0
 * @hibernate.class table = "T_PNRGOV_RES_LOG"
 */
public class PNRGOVReservationLog extends Persistent implements Serializable {

	private int pnrGovResLogId;

	private String pnr;

	private String status;

	private String description;

	private int fltSegId;

	private String inboundOutbound;

	private String countryCode;

	private String airportCode;

	private int timePeriod;

	/**
	 * @return Returns the logID.
	 * 
	 * @hibernate.id column="PNRGOV_RES_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNRGOV_RES_LOG"
	 */
	public int getPnrGovResLogId() {
		return pnrGovResLogId;
	}

	public void setPnrGovResLogId(int pnrGovResLogId) {
		this.pnrGovResLogId = pnrGovResLogId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the fltSegId.
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public int getFltSegId() {
		return fltSegId;
	}

	public void setFltSegId(int fltSegId) {
		this.fltSegId = fltSegId;
	}

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return Returns the airportCode.
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return Returns the timePeriod.
	 * @hibernate.property column = "TIME_PERIOD"
	 */
	public int getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(int timePeriod) {
		this.timePeriod = timePeriod;
	}

	/**
	 * @return Returns the inboundOutbound.
	 * @hibernate.property column = "INBOUND_OUTBOUND"
	 */
	public String getInboundOutbound() {
		return inboundOutbound;
	}

	public void setInboundOutbound(String inboundOutbound) {
		this.inboundOutbound = inboundOutbound;
	}

}
