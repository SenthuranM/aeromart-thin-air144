/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class IdentificationSearchParameters implements Serializable {

	private static final long serialVersionUID = -6738998855435893903L;
	private FlightInfo flightInfo;
	private PassengerInfo passengerInfo;

	public FlightInfo getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(FlightInfo flightInfo) {
		this.flightInfo = flightInfo;
	}

	public PassengerInfo getPassengerInfo() {
		return passengerInfo;
	}

	public void setPassengerInfo(PassengerInfo passengerInfo) {
		this.passengerInfo = passengerInfo;
	}

}
