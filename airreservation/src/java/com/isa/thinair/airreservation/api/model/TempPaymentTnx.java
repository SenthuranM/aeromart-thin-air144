/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of reservation tempory payment transactions
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_TEMP_PAYMENT_TNX"
 */
public class TempPaymentTnx extends Persistent {

	private static final long serialVersionUID = -1151200450895185077L;

	/** Holds the transaction id */
	private Integer tnxId;

	/** Holds the payment time stamp */
	private Date paymentTimeStamp;

	/** Holds the contact person */
	private String contactPerson;

	/** Holds the amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the telephone number */
	private String telephoneNo;

	/** Holds the mobile number */
	private String mobileNo;

	/** Holds the user id */
	private String userId;

	/** Holds the customer id */
	private Integer customerId;

	/** Holds the channel id */
	private Integer salesChannelCode;

	/** Holds the last 4 digit of the credit card number */
	private String last4DigitsCC;

	/** Holds transaction type */
	private String tnxType;

	/** Holds the pnr number */
	private String pnr;

	/** Holds the status of the tempory payment */
	private String status;

	/** Holds the id address of the user */
	private String ipAddress;

	/** Holds the state transition history */
	private String history = "";

	private String comments = "";

	/** Holds the payment currency code */
	private String paymentCurrencyCode;

	/** Holds the fraud check (FC)status */
	private String fraudStatus;

	/** Holds the FC message */
	private String fraudMessage;

	/** Holds the FC referense return from the check system */
	private String fraudRef;

	/** Holds the FC score returned */
	private Integer fraudScore;

	/** Holds the fraudResult code returned */
	private String fraudResultCode;

	/** Holds the currency amount */
	private BigDecimal paymentCurrencyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the email address */
	private String emailAddress;

	/** Holds if payment method is Offline(MTC-Offline) */
	private String isOfflinePayment = "N";

	/** Holds if product is a pnr or a voucher */
	private Character productType = new Character('P');

	/** Holds the customer alias */
	private String alias;

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the contactPerson.
	 * @hibernate.property column = "CONTACT_PERSON"
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson
	 *            The contactPerson to set.
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the last4DigitsCC.
	 * @hibernate.property column = "CC_LAST_4_DIGITS"
	 */
	public String getLast4DigitsCC() {
		return last4DigitsCC;
	}

	/**
	 * @param last4DigitsCC
	 *            The last4DigitsCC to set.
	 */
	public void setLast4DigitsCC(String last4DigitsCC) {
		this.last4DigitsCC = last4DigitsCC;
	}

	/**
	 * @return Returns the paymentTimeStamp.
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getPaymentTimeStamp() {
		return paymentTimeStamp;
	}

	/**
	 * @param paymentTimeStamp
	 *            The paymentTimeStamp to set.
	 */
	public void setPaymentTimeStamp(Date paymentTimeStamp) {
		this.paymentTimeStamp = paymentTimeStamp;
	}

	/**
	 * @return Returns the salesChannelCode.
	 * @hibernate.property column = "CHANNEL_ID"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the tnxId.
	 * @hibernate.id column = "TPT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_TEMP_PAYMENT_TNX"
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return Returns the tnxType.
	 * @hibernate.property column = "DR_CR"
	 */
	public String getTnxType() {
		return tnxType;
	}

	/**
	 * @param tnxType
	 *            The tnxType to set.
	 */
	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the ipAddress.
	 * @hibernate.property column = "IP_ADDRESS"
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            The ipAddress to set.
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return Returns the mobileNo.
	 * @hibernate.property column = "MOBILE_NO"
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            The mobileNo to set.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return Returns the telephoneNo.
	 * @hibernate.property column = "TELEPHONE_NO"
	 */
	public String getTelephoneNo() {
		return telephoneNo;
	}

	/**
	 * @param telephoneNo
	 *            The telephoneNo to set.
	 */
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}

	/**
	 * 
	 * @return State transition history
	 * @hibernate.property column = "STATUS_HISTORY"
	 */
	public String getHistory() {
		return history;
	}

	/**
	 * State transition history. <br>
	 * Please Appent to the String.
	 * 
	 * @param history
	 */
	public void setHistory(String history) {
		this.history = history;
	}

	/**
	 * @return
	 * @hibernate.property column = "COMMENTS"
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the paymentCurrencyAmount
	 * @hibernate.property column = "PAYMENT_CURRENCY_AMOUNT"
	 */
	public BigDecimal getPaymentCurrencyAmount() {
		return paymentCurrencyAmount;
	}

	/**
	 * @param paymentCurrencyAmount
	 *            the paymentCurrencyAmount to set
	 */
	public void setPaymentCurrencyAmount(BigDecimal paymentCurrencyAmount) {
		this.paymentCurrencyAmount = paymentCurrencyAmount;
	}

	/**
	 * @return the paymentCurrencyCode
	 * @hibernate.property column = "PAYMENT_CURRENCY_CODE"
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	/**
	 * @return the fraudStatus
	 * @hibernate.property column = "FRAUD_STATUS"
	 */
	public String getFraudStatus() {
		return fraudStatus;
	}

	/**
	 * @param fraudStatus
	 *            the fraudStatus to set
	 */
	public void setFraudStatus(String fraudStatus) {
		this.fraudStatus = fraudStatus;
	}

	/**
	 * @return the fraudMessage
	 * @hibernate.property column = "FRAUD_MESSAGE"
	 */
	public String getFraudMessage() {
		return fraudMessage;
	}

	/**
	 * @param fraudMessage
	 *            the fraudMessage to set
	 */
	public void setFraudMessage(String fraudMessage) {
		this.fraudMessage = fraudMessage;
	}

	/**
	 * @return the fraudRef
	 * @hibernate.property column = "FRAUD_REFERENCE"
	 */
	public String getFraudRef() {
		return fraudRef;
	}

	/**
	 * @param fraudRef
	 *            the fraudRef to set
	 */
	public void setFraudRef(String fraudRef) {
		this.fraudRef = fraudRef;
	}

	/**
	 * @return the fraudScore
	 * @hibernate.property column = "FRAUD_SCORE"
	 */
	public Integer getFraudScore() {
		return fraudScore;
	}

	/**
	 * @param fraudScore
	 *            the fraudScore to set
	 */
	public void setFraudScore(Integer fraudScore) {
		this.fraudScore = fraudScore;
	}

	/**
	 * @return the fraudResultCode
	 * @hibernate.property column = "FRAUD_RESULTCODE"
	 */
	public String getFraudResultCode() {
		return fraudResultCode;
	}

	/**
	 * @param fraudResultCode
	 *            the fraudResultCode to set
	 */
	public void setFraudResultCode(String fraudResultCode) {
		this.fraudResultCode = fraudResultCode;
	}

	/**
	 * @return the emailAddress
	 * @hibernate.property column = "EMAIL_ADDRESS"
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the emailAddress
	 * @hibernate.property column = "IS_OFFLINE_PAYMENT"
	 */
	public String getIsOfflinePayment() {
		return isOfflinePayment;
	}

	public void setIsOfflinePayment(String isOfflinePayment) {
		this.isOfflinePayment = isOfflinePayment;
	}

	/**
	 * @return the productType
	 * @hibernate.property column = "PRODUCT_TYPE"
	 */
	public Character getProductType() {
		return productType;
	}

	/**
	 * @param productType
	 *            the productType to set
	 */
	public void setProductType(Character productType) {
		this.productType = productType;
	}

	/**
	 * @return the alias
	 * @hibernate.property column = "ALIAS"
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *            the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

}
