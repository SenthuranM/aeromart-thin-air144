/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

/**
 * To hold Reservation data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationDTO implements Serializable {

	private static final long serialVersionUID = 3357062615920761385L;

	/** Holds collection of ReservationSegmentDTO */
	private Collection<ReservationSegmentDTO> segments;

	/** Holds collection of external segments */
	private Collection<ReservationExternalSegmentTO> colExternalSegments;

	/** Holds reservation number */
	private String pnr;

	/** Holds the originator PNR */
	private String originatorPnr;

	/** Holds reservation contact information */
	private ReservationContactInfo reservationContactInfo;

	/** Holds reservation admin information */
	private ReservationAdminInfo reservationAdminInfo;

	/** Holds reservation adult count */
	private Integer adultCount;

	/** Holds reservation infant count */
	private Integer infantCount;

	/** Holds the child count */
	private Integer childCount;

	/** Holds reservation PNR date */
	private Date pnrDate;

	/** Holds the reservation status */
	private String status;

	/** Holds total ticket fare */
	private BigDecimal totalTicketFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds total ticket charge */
	private BigDecimal totalTicketCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total paid amount */
	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total available balance */
	private BigDecimal totalAvailBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total paid amount in specified currency */
	private String totalPaidAmountInPayCurrency;

	/** Holds the total available balance in specified currency */
	private String totalAvailableBalanceInPayCurrency;

	/** Holds whether to show amounts in Pay currency or not */
	private boolean showAmountsInPayCurrency;

	/** Holds the zulu release time stamp */
	private Date zuluReleaseTimeStamp;

	/** Holds the local release time stamp */
	private Date localReleaseTimeStamp;

	/** Holds the base currency code */
	private String baseCurrency;

	/** Holds the specified currency code */
	private String specifiedCurrencyCode;

	/** Holds the specified currency */
	private Currency specifiedCurrency;

	/** Holds currency exchange rate */
	private BigDecimal currencyExchangeRate = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds eTicket Number if exsists */
	// FIXME
	//private String eTicketNumber;

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the segments.
	 */
	public Collection<ReservationSegmentDTO> getSegments() {
		return segments;
	}

	public Collection<ReservationSegmentDTO> getActiveSegments() {
		List<ReservationSegmentDTO> activeSegments = new ArrayList<ReservationSegmentDTO>();
		for (ReservationSegmentDTO seg : getSegments()) {
			if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				activeSegments.add(seg);
			}
		}
		return activeSegments;
	}

	/**
	 * Returns non open return segments
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private Collection<ReservationSegmentDTO> getNonOpenReturnSegments() throws ModuleException {
		List<ReservationSegmentDTO> lstSegments = new ArrayList<ReservationSegmentDTO>();
		if (this.getSegments() != null && this.getSegments().size() > 0) {
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = this.getSegments().iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = itReservationSegmentDTO.next();

				if (!BookingClass.BookingClassType.OPEN_RETURN.equals(reservationSegmentDTO.getBookingType())) {
					lstSegments.add(reservationSegmentDTO);
				}
			}
		}

		return lstSegments;
	}

	/**
	 * Return segments ordered by zulu depature date Used when printing the iternery
	 * 
	 * @return collection of ReservationSegmentDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public Collection<ReservationSegmentDTO> getSegmentsOrderedByZuluDepartureDate() throws ModuleException {
		Collection<ReservationSegmentDTO> colNonOpenReturnSegments = this.getNonOpenReturnSegments();
		if (colNonOpenReturnSegments != null && colNonOpenReturnSegments.size() > 0) {
			List<ReservationSegmentDTO> lstSegments = new ArrayList<ReservationSegmentDTO>(colNonOpenReturnSegments);
			Collections.sort(lstSegments);
			return lstSegments;
		} else {
			return Collections.EMPTY_LIST;
		}
	}

	/**
	 * Returns Segments ordered by zulu departure date which donotes that it should show comments with the fare
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public Collection<ReservationSegmentForItineraryDTO> getSegmentsOrderByZuluDepDateShownWithFareRules() throws ModuleException {
		if (this.getSegments() != null && this.getSegments().size() > 0) {
			List<ReservationSegmentDTO> lstSegWithFareRules = new ArrayList<ReservationSegmentDTO>();
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = this.getSegments().iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();
				if (reservationSegmentDTO.getFareTO().getFareCategoryTO().isShowComments()) {
					lstSegWithFareRules.add(reservationSegmentDTO);
				}
			}
			Collections.sort(lstSegWithFareRules);

			List<ReservationSegmentForItineraryDTO> lstSegItineraryWithFareRules = new ArrayList<ReservationSegmentForItineraryDTO>();
			for (Iterator<ReservationSegmentDTO> itSegment = lstSegWithFareRules.iterator(); itSegment.hasNext();) {
				ReservationSegmentDTO segDTO = (ReservationSegmentDTO) itSegment.next();
				ReservationSegmentForItineraryDTO segmentDTO = new ReservationSegmentForItineraryDTO(segDTO, lstSegWithFareRules);
				lstSegItineraryWithFareRules.add(segmentDTO);
			}
			// return lstSegWithFareRules;
			return lstSegItineraryWithFareRules;
		} else {
			return Collections.EMPTY_LIST;
		}
	}

	/**
	 * @param segments
	 *            The segments to set.
	 */
	private void setSegments(Collection<ReservationSegmentDTO> segments) {
		this.segments = segments;
	}

	/**
	 * Add a segment
	 * 
	 * @param reservationSegmentDTO
	 */
	public void addSegment(ReservationSegmentDTO reservationSegmentDTO) {
		if (this.getSegments() == null) {
			this.setSegments(new ArrayList<ReservationSegmentDTO>());
		}

		this.getSegments().add(reservationSegmentDTO);
	}

	/**
	 * @return Returns the adultCount.
	 */
	public Integer getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount
	 *            The adultCount to set.
	 */
	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return Returns the infantCount.
	 */
	public Integer getInfantCount() {
		return infantCount;
	}

	/**
	 * @param infantCount
	 *            The infantCount to set.
	 */
	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	/**
	 * @return Returns the reservationContactInfo.
	 */
	public ReservationContactInfo getReservationContactInfo() {
		return reservationContactInfo;
	}

	/**
	 * @param reservationContactInfo
	 *            The reservationContactInfo to set.
	 */
	public void setReservationContactInfo(ReservationContactInfo reservationContactInfo) {
		this.reservationContactInfo = reservationContactInfo;
	}

	/**
	 * @return Returns the pnrDate.
	 */
	public Date getPnrDate() {
		return pnrDate;
	}

	/**
	 * @param pnrDate
	 *            The pnrDate to set.
	 */
	public void setPnrDate(Date pnrDate) {
		this.pnrDate = pnrDate;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Returns if this is an itinerary confirmation or not
	 * 
	 * @return
	 */
	public boolean isItineraryConfirmation() {
		if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(this.getStatus())) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Returns the pnr date in string format
	 * 
	 * @return
	 */
	public String getPnrStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getPnrDate(), dateFormat);
	}

	/**
	 * @return Returns the totalTicketCharge.
	 */
	public BigDecimal getTotalTicketCharge() {
		return totalTicketCharge;
	}

	/**
	 * Returns total ticket charge as string
	 * 
	 * @param numberFormat
	 * @param toAgentCurrency
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalTicketChargeAsString(String numberFormat, boolean toAgentCurrency) throws ModuleException {
		if (toAgentCurrency) {
			if (showAmountsInPayCurrency) {
				return "";
			} else {
				return BeanUtils.parseNumberFormat(getAgentTotal(this.getTotalTicketCharge()).doubleValue(), numberFormat);
			}
		} else {
			return BeanUtils.parseNumberFormat(this.getTotalTicketCharge().doubleValue(), numberFormat);
		}
	}

	/**
	 * @param totalTicketCharge
	 *            The totalTicketCharge to set.
	 */
	public void setTotalTicketCharge(BigDecimal totalTicketCharge) {
		this.totalTicketCharge = totalTicketCharge;
	}

	/**
	 * @return Returns the totalTicketFare.
	 */
	public BigDecimal getTotalTicketFare() {
		return totalTicketFare;
	}

	/**
	 * @param totalTicketFare
	 *            The totalTicketFare to set.
	 */
	public void setTotalTicketFare(BigDecimal totalTicketFare) {
		this.totalTicketFare = totalTicketFare;
	}

	/**
	 * Returns total ticket charge as string
	 * 
	 * @param numberFormat
	 * @param toAgentCurrency
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalTicketFareAsString(String numberFormat, boolean toAgentCurrency) throws ModuleException {
		if (toAgentCurrency) {
			if (showAmountsInPayCurrency) {
				return "";
			} else {
				return BeanUtils.parseNumberFormat(getAgentTotal(this.getTotalTicketFare()).doubleValue(), numberFormat);
			}
		} else {
			return BeanUtils.parseNumberFormat(this.getTotalTicketFare().doubleValue(), numberFormat);
		}
	}

	/**
	 * Return total ticket amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalTicketAmount() {
		return AccelAeroCalculator.add(this.getTotalTicketFare(), this.getTotalTicketCharge());
	}

	/**
	 * Return flag segments
	 * 
	 * @param flag
	 * @return
	 */
	private Collection<ReservationSegmentDTO> getFlagSegments(String flag) {
		if (this.getSegments() != null && this.getSegments().size() > 0) {
			Iterator<ReservationSegmentDTO> itReservationSegmentDTO = this.getSegments().iterator();
			ReservationSegmentDTO reservationSegmentDTO;
			Collection<ReservationSegmentDTO> segments = new ArrayList<ReservationSegmentDTO>();

			while (itReservationSegmentDTO.hasNext()) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

				if (reservationSegmentDTO.getReturnFlag().equals(flag)) {
					segments.add(reservationSegmentDTO);
				}
			}
			return segments;
		} else {
			return new ArrayList<ReservationSegmentDTO>();
		}
	}

	/**
	 * Return Departure Segments
	 * 
	 * @return
	 */
	public Collection<ReservationSegmentDTO> getDepartureSegments() {
		return this.getFlagSegments(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
	}

	/**
	 * Return Arrival Segments
	 * 
	 * @return
	 */
	public Collection<ReservationSegmentDTO> getArrivalSegments() {
		return this.getFlagSegments(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE);
	}

	/**
	 * @return Returns the reservationAdminInfo.
	 */
	public ReservationAdminInfo getReservationAdminInfo() {
		return reservationAdminInfo;
	}

	/**
	 * @param reservationAdminInfo
	 *            The reservationAdminInfo to set.
	 */
	public void setReservationAdminInfo(ReservationAdminInfo reservationAdminInfo) {
		this.reservationAdminInfo = reservationAdminInfo;
	}

	/**
	 * @return Returns the totalPaidAmount.
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            The totalPaidAmount to set.
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * Returns total paid amount as string
	 * 
	 * @param toAgentCurrency
	 * @param toBaseCurrency
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalPaidAmountAsString(String numberFormat, boolean toAgentCurrency) throws ModuleException {
		if (toAgentCurrency) {
			if (showAmountsInPayCurrency) {
				if (this.getTotalPaidAmountInPayCurrency() != null) {

					StringTokenizer tk = new StringTokenizer(this.getTotalPaidAmountInPayCurrency(), "+");
					StringBuffer ws = new StringBuffer();
					while (tk.hasMoreTokens()) {
						StringTokenizer tkF = new StringTokenizer(tk.nextToken(), " ");
						if (tkF.countTokens() == 2) {
							ws.append(BeanUtils.parseNumberFormat(Double.parseDouble(tkF.nextToken()), numberFormat) + " "
									+ tkF.nextToken());
							if (tk.hasMoreTokens()) {
								ws.append(" + ");
							}
						}
					}
					return ws.toString();
				} else {
					return this.getTotalPaidAmountInPayCurrency();
				}
			} else {
				return BeanUtils.parseNumberFormat(getAgentTotal(this.getTotalPaidAmount()).doubleValue(), numberFormat);
			}
		} else {
			return BeanUtils.parseNumberFormat(this.getTotalPaidAmount().doubleValue(), numberFormat);
		}
	}

	/**
	 * Returns total available balance as string
	 * 
	 * @param toAgentCurrency
	 * @param toBaseCurrency
	 * @return
	 * @throws ModuleException
	 */
	public String getTotalAvailBalanceAsString(String numberFormat, boolean toAgentCurrency) throws ModuleException {
		if (toAgentCurrency) {
			if (showAmountsInPayCurrency) {
				return this.getTotalAvailableBalanceInPayCurrency();
			} else {
				return BeanUtils.parseNumberFormat(getAgentTotal(this.getTotalAvailBalance()).doubleValue(), numberFormat);
			}
		} else {
			return BeanUtils.parseNumberFormat(this.getTotalAvailBalance().doubleValue(), numberFormat);
		}
	}

	/**
	 * @return Returns the localReleaseTimeStamp.
	 */
	public Date getLocalReleaseTimeStamp() {
		return localReleaseTimeStamp;
	}

	/**
	 * @param localReleaseTimeStamp
	 *            The localReleaseTimeStamp to set.
	 */
	public void setLocalReleaseTimeStamp(Date localReleaseTimeStamp) {
		this.localReleaseTimeStamp = localReleaseTimeStamp;
	}

	/**
	 * @return Returns the zuluReleaseTimeStamp.
	 */
	public Date getZuluReleaseTimeStamp() {
		return zuluReleaseTimeStamp;
	}

	/**
	 * @param zuluReleaseTimeStamp
	 *            The zuluReleaseTimeStamp to set.
	 */
	public void setZuluReleaseTimeStamp(Date zuluReleaseTimeStamp) {
		this.zuluReleaseTimeStamp = zuluReleaseTimeStamp;
	}

	/**
	 * Return the release booking time stamp
	 * 
	 * @param dateFormat
	 * @param displayConversion
	 * @return
	 */
	public String getReleaseTimeStampString(String dateFormat, boolean displayConversion) {
		return ReservationApiUtils.getConvertedDate(this.getZuluReleaseTimeStamp(), this.getLocalReleaseTimeStamp(), dateFormat,
				displayConversion);
	}

	/**
	 * Return if all segments stand by or not
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean isAllSegmentsStandBy() throws ModuleException {
		if (this.getSegments() != null && this.getSegments().size() > 0) {
			ReservationSegmentDTO reservationSegmentDTO;
			boolean isStandby = false;
			int i = 1;

			for (Iterator<ReservationSegmentDTO> itReservationSegmentDTO = this.getSegments().iterator(); itReservationSegmentDTO.hasNext();) {
				reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

				if (i == 1) {
					isStandby = reservationSegmentDTO.isStandBySegment();
				} else {
					isStandby = isStandby && reservationSegmentDTO.isStandBySegment();
				}

				i++;
			}

			return isStandby;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the baseCurrency.
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * @param baseCurrency
	 *            The baseCurrency to set.
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * Is Agent Currency Different from Base Currency
	 * 
	 * @return
	 */
//	private boolean isSpecifiedCurrDiffFromBaseCurr() {
//		if (BeanUtils.nullHandler(this.getSpecifiedCurrencyCode()).length() == 0) {
//			return false;
//		} else if (this.getSpecifiedCurrencyCode().equals(this.getBaseCurrency())) {
//			return false;
//		} else {
//			return true;
//		}
//	}


	/**
	 * Returns Agent total amount
	 * 
	 * @param dblValue
	 * @return
	 * @throws ModuleException
	 */
	private BigDecimal getAgentTotal(BigDecimal dblValue) throws ModuleException {
		return AccelAeroRounderPolicy.convertAndRound(dblValue, this.getCurrencyExchangeRate(), this.getSpecifiedCurrency()
				.getBoundryValue(), this.getSpecifiedCurrency().getBreakPoint());
	}

	/**
	 * @return Returns the childCount.
	 */
	public Integer getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount
	 *            The childCount to set.
	 */
	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return the originatorPnr
	 */
	public String getOriginatorPnr() {
		return originatorPnr;
	}

	/**
	 * @param originatorPnr
	 *            the originatorPnr to set
	 */
	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	/**
	 * @return the showAmountsInPayCurrency
	 */
	public boolean isShowAmountsInPayCurrency() {
		return showAmountsInPayCurrency;
	}

	/**
	 * @param showAmountsInPayCurrency
	 *            the showAmountsInPayCurrency to set
	 */
	public void setShowAmountsInPayCurrency(boolean showAmountsInPayCurrency) {
		this.showAmountsInPayCurrency = showAmountsInPayCurrency;
	}

	/**
	 * @return the specifiedCurrencyCode
	 */
	public String getSpecifiedCurrencyCode() {
		return specifiedCurrencyCode;
	}

	/**
	 * @param specifiedCurrencyCode
	 *            the specifiedCurrencyCode to set
	 */
	public void setSpecifiedCurrencyCode(String specifiedCurrencyCode) {
		this.specifiedCurrencyCode = specifiedCurrencyCode;
	}

	/**
	 * @return the currencyExchangeRate
	 */
	public BigDecimal getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	/**
	 * @param currencyExchangeRate
	 *            the currencyExchangeRate to set
	 */
	public void setCurrencyExchangeRate(BigDecimal currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	/**
	 * @return the specifiedCurrency
	 */
	public Currency getSpecifiedCurrency() {
		return specifiedCurrency;
	}

	/**
	 * @param specifiedCurrency
	 *            the specifiedCurrency to set
	 */
	public void setSpecifiedCurrency(Currency specifiedCurrency) {
		this.specifiedCurrency = specifiedCurrency;
	}

	/**
	 * @return the totalPaidAmountInPayCurrency
	 */
	public String getTotalPaidAmountInPayCurrency() {
		return totalPaidAmountInPayCurrency;
	}

	/**
	 * @param totalPaidAmountInPayCurrency
	 *            the totalPaidAmountInPayCurrency to set
	 */
	public void setTotalPaidAmountInPayCurrency(String totalPaidAmountInPayCurrency) {
		this.totalPaidAmountInPayCurrency = totalPaidAmountInPayCurrency;
	}

	/**
	 * @return Returns the totalAvailableBalanceInPayCurrency.
	 */
	public String getTotalAvailableBalanceInPayCurrency() {
		return totalAvailableBalanceInPayCurrency;
	}

	/**
	 * @param totalAvailableBalanceInPayCurrency
	 *            The totalAvailableBalanceInPayCurrency to set.
	 */
	public void setTotalAvailableBalanceInPayCurrency(String totalAvailableBalanceInPayCurrency) {
		this.totalAvailableBalanceInPayCurrency = totalAvailableBalanceInPayCurrency;
	}

	/**
	 * @return Returns the totalAvailBalance.
	 */
	public BigDecimal getTotalAvailBalance() {
		return totalAvailBalance;
	}

	/**
	 * @param totalAvailBalance
	 *            The totalAvailBalance to set.
	 */
	public void setTotalAvailBalance(BigDecimal totalAvailBalance) {
		this.totalAvailBalance = totalAvailBalance;
	}

	/**
	 * @return the colExternalSegments
	 */
	public Collection<ReservationExternalSegmentTO> getColExternalSegments() {
		return colExternalSegments;
	}

	/**
	 * @param colExternalSegments
	 *            the colExternalSegments to set
	 */
	private void setColExternalSegments(Collection<ReservationExternalSegmentTO> colExternalSegments) {
		this.colExternalSegments = colExternalSegments;
	}

	/**
	 * Add a external segment
	 * 
	 * @param reservationExternalSegmentTO
	 */
	public void addExternalSegment(ReservationExternalSegmentTO reservationExternalSegmentTO) {
		if (this.getColExternalSegments() == null) {
			this.setColExternalSegments(new ArrayList<ReservationExternalSegmentTO>());
		}

		this.getColExternalSegments().add(reservationExternalSegmentTO);
	}
}
