package com.isa.thinair.airreservation.api.dto.adl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ADLDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private FlightInformation flightInfo;
	private Map<String, List<String>> classCodes;
	// Destination-> Class of Service-> Passenger Data
	private Map<String, Map<String, ADLPassengerData>> passengerData;
	private Set<Integer> uniquePnlPaxIds = new HashSet<Integer>();
	private HashMap<String, Integer> destinationWisePaxCount;
	private String lastGroupCode;

	public FlightInformation getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(FlightInformation flightInfo) {
		this.flightInfo = flightInfo;
	}

	public Map<String, Map<String, ADLPassengerData>> getPassengerData() {
		return passengerData;
	}

	public void setPassengerData(Map<String, Map<String, ADLPassengerData>> passengerData) {
		this.passengerData = passengerData;
	}

	public Map<String, List<String>> getClassCodes() {
		return classCodes;
	}

	public void setClassCodes(Map<String, List<String>> classCodes) {
		this.classCodes = classCodes;
	}

	public HashMap<String, Integer> getDestinationWisePaxCount() {
		return destinationWisePaxCount;
	}

	public void setDestinationWisePaxCount(HashMap<String, Integer> destinationWisePaxCount) {
		this.destinationWisePaxCount = destinationWisePaxCount;
	}

	public String getLastGroupCode() {
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

	public Set<Integer> getUniquePnlPaxIds() {
		return uniquePnlPaxIds;
	}

	public void setUniquePnlPaxIds(Set<Integer> uniquePnlPaxIds) {
		this.uniquePnlPaxIds = uniquePnlPaxIds;
	}

}
