package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ItineraryFlightTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int segmentSeq;

	private int ondSeq;

	private String ondFullText;

	private String segmentCode;

	private String flightNo;

	private String status;

	private String displayStatus;

	private Date departureDateTime;

	private Date arrivalDateTime;

	private Date checkInDateTime;

	private Date checkInClosingDateTime;

	private String departureDateInPersian;

	private String arrivalDateInPersian;

	private String departureTimeInPersian;

	private String arrivalTimeInPersian;

	private String checkInDateInPersian;

	private String checkInTimeInPersian;

	private String checkInClosingDateTimeInPersian;

	private String carrierLogoURL;

	private long durationInMillis;

	private Date departureDateTimeZule;

	private Date arrivalDateTimeZulu;

	private String departureDateTimeZuluInPersian;

	private String arrivalDateTimeZuluInPersian;

	private String originAirport;

	private String destinationAirport;

	private boolean openReturnSegment;

	private String openReturnConfirmBeforeLocal;

	private String segmentExpiryLocal;

	private String cabinClassCode;

	private String cabinClassDescription;

	private String logicalCCCode;

	private String logicalCCDescription;

	private String originAptCode;

	private String destinationAptCode;

	private String originAptTerminalCode;

	private String destinationAptTerminalCode;

	private String locale;

	private String flightModelNumber;

	private String flightModelDescription;

	private String flightDuration;

	private String flightDurationInPersian;

	private String flightStopOverDurationInPersian;

	private String remarks;

	private String flightStopOverDuration;

	private String noOfStops;

	private String flightBaggageAllowance;

	private Date ticketValidTill;

	private String ticketExpiryDateInPersian;

	private boolean persianDatesSet;

	private Date standbyTravelValidityEnd;

	private String bundledServiceName;
	
	private String csOcFlightNumber;

	public String getTicketExpiryDateInPersian() {
		return ticketExpiryDateInPersian;
	}

	public void setTicketExpiryDateInPersian(String ticketExpiryDateInPersian) {
		this.ticketExpiryDateInPersian = ticketExpiryDateInPersian;
	}

	public boolean isPersianDatesSet() {
		return persianDatesSet;
	}

	public void setIsPersianDatesSet(boolean usePersianDates) {
		this.persianDatesSet = usePersianDates;
	}

	public String getFlightDurationInPersian() {
		return flightDurationInPersian;
	}

	public void setFlightDurationInPersian(String flightDurationInPersian) {
		this.flightDurationInPersian = flightDurationInPersian;
	}

	public String getFlightStopOverDurationInPersian() {
		return flightStopOverDurationInPersian;
	}

	public void setFlightStopOverDurationInPersian(String flightStopOverDurationInPersian) {
		this.flightStopOverDurationInPersian = flightStopOverDurationInPersian;
	}

	public String getDepartureDateInPersian() {
		return departureDateInPersian;
	}

	public void setDepartureDateInPersian(String departureDateInPersian) {
		this.departureDateInPersian = departureDateInPersian;
	}

	public String getDepartureTimeInPersian() {
		return departureTimeInPersian;
	}

	public void setDepartureTimeInPersian(String departureTimeInPersian) {
		this.departureTimeInPersian = departureTimeInPersian;
	}

	public String getDepartureDateTimeZuluInPersian() {
		return departureDateTimeZuluInPersian;
	}

	public void setDepartureDateTimeZuluInPersian(String departureDateTimeZuleInPersian) {
		this.departureDateTimeZuluInPersian = departureDateTimeZuleInPersian;
	}

	public String getArrivalDateTimeZuluInPersian() {
		return arrivalDateTimeZuluInPersian;
	}

	public void setArrivalDateTimeZuluInPersian(String arrivalDateTimeZuluInPersian) {
		this.arrivalDateTimeZuluInPersian = arrivalDateTimeZuluInPersian;
	}

	public String getArrivalTimeInPersian() {
		return arrivalTimeInPersian;
	}

	public void setArrivalTimeInPersian(String arrivalTimeInPersian) {
		this.arrivalTimeInPersian = arrivalTimeInPersian;
	}

	public String getArrivalDateInPersian() {
		return arrivalDateInPersian;
	}

	public void setArrivalDateInPersian(String arrivalDateInPersian) {
		this.arrivalDateInPersian = arrivalDateInPersian;
	}

	public String getCheckInTimeInPersian() {
		return checkInTimeInPersian;
	}

	public void setCheckInTimeInPersian(String checkInTimeInPersian) {
		this.checkInTimeInPersian = checkInTimeInPersian;
	}

	public void setCheckInDateInPersian(String checkInDateInPersian) {
		this.checkInDateInPersian = checkInDateInPersian;
	}

	public String getCheckInDateInPersian() {
		return checkInDateInPersian;
	}

	public String getCheckInClosingDateTimeInPersian() {
		return checkInClosingDateTimeInPersian;
	}

	public void setCheckInClosingDateTimeInPersian(String checkInClosingDateTimeInPersian) {
		this.checkInClosingDateTimeInPersian = checkInClosingDateTimeInPersian;
	}

	/**
	 * @return the originAirport
	 */
	public String getOriginAirport() {
		return originAirport;
	}

	/**
	 * @param originAirport
	 *            the originAirport to set
	 */
	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	/**
	 * @return the destinationAirport
	 */
	public String getDestinationAirport() {
		return destinationAirport;
	}

	/**
	 * @param destinationAirport
	 *            the destinationAirport to set
	 */
	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getDepartureDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(departureDateTime, dateTimeFormat, locale);
	}

	public String getArrivalDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(arrivalDateTime, dateTimeFormat, locale);
	}

	public String getCheckInDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(checkInDateTime, dateTimeFormat, locale);
	}

	public String getCheckInClosingDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(checkInClosingDateTime, dateTimeFormat, locale);
	}

	/**
	 * @return the ondFullText
	 */
	public String getOndFullText() {
		return ondFullText;
	}

	/**
	 * @param ondFullText
	 *            the ondFullText to set
	 */
	public void setOndFullText(String ondFullText) {
		this.ondFullText = ondFullText;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param departureDateTime
	 *            the departureDateTime to set
	 */
	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	/**
	 * @param arrivalDateTime
	 *            the arrivalDateTime to set
	 */
	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	/**
	 * @param checkInDateTime
	 *            the checkInDateTime to set
	 */
	public void setCheckInDateTime(Date checkInDateTime) {
		this.checkInDateTime = checkInDateTime;
	}

	/**
	 * @return the carrierLogoURL
	 */
	public String getCarrierLogoURL() {
		return carrierLogoURL;
	}

	/**
	 * @param carrierLogoURL
	 *            the carrierLogoURL to set
	 */
	public void setCarrierLogoURL(String carrierLogoURL) {
		this.carrierLogoURL = carrierLogoURL;
	}

	public void setDurationInMillis(long durationInMillis) {
		this.durationInMillis = durationInMillis;
	}

	public long getDurationInMillis() {
		return durationInMillis;
	}

	public String getDuration(String format) {
		return BeanUtils.parseDurationFormat(durationInMillis, format);
	}

	public int getSegmentSeq() {
		return segmentSeq;
	}

	public void setSegmentSeq(int segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	public int getOndSeq() {
		return ondSeq;
	}

	public void setOndSeq(int ondSeq) {
		this.ondSeq = ondSeq;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZule = departureDateTimeZulu;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZule;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public boolean isOpenReturnSegment() {
		return openReturnSegment;
	}

	public void setOpenReturnSegment(boolean openReturnSegment) {
		this.openReturnSegment = openReturnSegment;
	}

	public String getOpenReturnConfirmBeforeLocal() {
		return openReturnConfirmBeforeLocal;
	}

	public void setOpenReturnConfirmBeforeLocal(String openReturnConfirmBeforeLocal) {
		this.openReturnConfirmBeforeLocal = openReturnConfirmBeforeLocal;
	}

	public String getSegmentExpiryLocal() {
		return segmentExpiryLocal;
	}

	public void setSegmentExpiryLocal(String segmentExpiryLocal) {
		this.segmentExpiryLocal = segmentExpiryLocal;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getCabinClassDescription() {
		return cabinClassDescription;
	}

	public void setCabinClassDescription(String cabinClassDescription) {
		this.cabinClassDescription = cabinClassDescription;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getLogicalCCDescription() {
		return logicalCCDescription;
	}

	public void setLogicalCCDescription(String logicalCCDescription) {
		this.logicalCCDescription = logicalCCDescription;
	}

	public String getOriginAptCode() {
		return originAptCode;
	}

	public void setOriginAptCode(String originAptCode) {
		this.originAptCode = originAptCode;
	}

	public String getDestinationAptCode() {
		return destinationAptCode;
	}

	public void setDestinationAptCode(String destinationAptCode) {
		this.destinationAptCode = destinationAptCode;
	}

	/**
	 * @return originAptTerminalCode prefixed with "- " if originAptTerminalCode is not null.
	 */
	public String getOriginAptTerminalCode() {
		/*
		 * Currently only used in the velocity template printing the itinerary. So it's ok to prefix. If it's going to
		 * be used somewhere else remove the prefixing and fix interline_itinerary_body_RT.xml.vm
		 */
		if (originAptTerminalCode != null) {
			originAptTerminalCode = "- " + originAptTerminalCode;
		}
		return originAptTerminalCode;
	}

	public void setOriginAptTerminalCode(String originAptTerminalCode) {
		this.originAptTerminalCode = originAptTerminalCode;
	}

	/**
	 * @return destinationAptTerminalCode prefixed with "- " if destinationAptTerminalCode is not null.
	 */
	public String getDestinationAptTerminalCode() {
		/*
		 * Currently only used in the velocity template printing the itinerary. So it's ok to prefix. If it's going to
		 * be used somewhere else remove the prefixing and fix interline_itinerary_body_RT.xml.vm
		 */
		if (destinationAptTerminalCode != null) {
			destinationAptTerminalCode = "- " + destinationAptTerminalCode;
		}
		return destinationAptTerminalCode;
	}

	public void setDestinationAptTerminalCode(String destinationAptTerminalCode) {
		this.destinationAptTerminalCode = destinationAptTerminalCode;
	}

	public Date getCheckInClosingDateTime() {
		return checkInClosingDateTime;
	}

	public void setCheckInClosingDateTime(Date checkInClosingDateTime) {
		this.checkInClosingDateTime = checkInClosingDateTime;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	public String getFlightModelDescription() {
		return flightModelDescription;
	}

	public void setFlightModelDescription(String flightModelDescription) {
		this.flightModelDescription = flightModelDescription;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFlightStopOverDuration() {
		return flightStopOverDuration;
	}

	public void setFlightStopOverDuration(String flightStopOverDuration) {
		this.flightStopOverDuration = flightStopOverDuration;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getNoOfStops() {
		return noOfStops;
	}

	public void setNoOfStops(String noOfStops) {
		this.noOfStops = noOfStops;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	public String getFlightBaggageAllowance() {
		return flightBaggageAllowance;
	}

	public void setFlightBaggageAllowance(String flightBaggageAllowance) {
		this.flightBaggageAllowance = flightBaggageAllowance;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public String getTicketExpiryDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(ticketValidTill, dateTimeFormat, locale);
	}

	public String getStandbyTravelValidityEnd(String dateTimeFormat) {
		return BeanUtils.nullHandler(BeanUtils.parseDateFormat(standbyTravelValidityEnd, dateTimeFormat, locale));
	}

	public void setStandbyTravelValidityEnd(Date standbyTravelValidityEnd) {
		this.standbyTravelValidityEnd = standbyTravelValidityEnd;
	}

	public boolean showTravelValidity() {
		return (ticketValidTill != null);
	}

	public String getBundledServiceName() {
		return bundledServiceName;
	}

	public void setBundledServiceName(String bundledServiceName) {
		this.bundledServiceName = bundledServiceName;
	}

	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}

	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}
}