/**
 * 	mano
	May 10, 2011 
	2011
 */
package com.isa.thinair.airreservation.api.dto.baggage;


/**
 * @author mano
 * 
 */
public class BaggageUtil {

	public static String makeUniqueIdForModifications(Integer paxId, Integer pnrPaxFareId, Integer pnrSegId) {
		return paxId.toString() + "|" + pnrPaxFareId.toString() + "|" + pnrSegId.toString();
	}

	/**
	 * 
	 * @param reservation
	 * @return
	 */
//	public static Collection<Integer> getPnrPaxIds(Reservation reservation) {
//		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
//		Iterator<ReservationPax> iter = reservation.getPassengers().iterator();
//		while (iter.hasNext()) {
//			ReservationPax resPax = (ReservationPax) iter.next();
//			pnrPaxIds.add(resPax.getPnrPaxId());
//		}
//		return pnrPaxIds;
//	}

	public static String getPnrPaxId(String uniqueId) {
		return uniqueId.substring(0, uniqueId.indexOf("|"));
	}

}
