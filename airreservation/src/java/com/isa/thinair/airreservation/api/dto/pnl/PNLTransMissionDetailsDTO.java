/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.pnl;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author byorn
 */
public class PNLTransMissionDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5695340093762482796L;

	/** Holds the Flight Id * */
	private int flightId;

	/** Holds the Flight Number * */
	private String flightNumber;

	/** Holds the Boarding Airport * */
	private String departureStation;

	/** Holds the Flight Dep Time in ZULU * */
	private Timestamp departureTimeZulu;

	/** Holds theDep Time in LOCAL Tm * */
	private Timestamp departuretimeLocal;

	/** Holds the Flight Seg Id * */
	private int flightSegId;

	/** Holds the Time time gap before flight departs the time for PNL to be sent * */
	/**
	 * Generatly this value is taken from sys params..introduced for THROUGH CHECKIN purpose *
	 */
	private int pnlDepartureGap;

	/** Holds the ADL repeat Interval, THROGH CHECK IN Purpose * */
	private int adlRepeatInterval;

	/** Holds the Gap of the Last ADL to be sent from the flight dep zulu time **/
	private int lastAdlGap;

	private long flightCutoffTime;

	private int adlRepeatIntervalAfterCutoffTime;

	/**
	 * 
	 * @return
	 */
	public String getDepartureStation() {
		return departureStation;
	}

	/**
	 * 
	 * @param departureStation
	 */
	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	/**
	 * 
	 * @return
	 */
	public Timestamp getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	/**
	 * 
	 * @param departureTimeZulu
	 */
	public void setDepartureTimeZulu(Timestamp departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	/**
	 * 
	 * @return
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * 
	 * @param flightId
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	/**
	 * 
	 * @return
	 */
	public int getFlightSegId() {
		return flightSegId;
	}

	/**
	 * 
	 * @param flightSegId
	 */
	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * 
	 * @return
	 */
	public Timestamp getDeparturetimeLocal() {
		return departuretimeLocal;
	}

	/**
	 * 
	 * @param departuretimeLocal
	 */
	public void setDeparturetimeLocal(Timestamp departuretimeLocal) {
		this.departuretimeLocal = departuretimeLocal;
	}

	/**
	 * 
	 * @return
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * 
	 * @param flightNumber
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the adlRepeatInterval.
	 */
	public int getAdlRepeatInterval() {
		return adlRepeatInterval;
	}

	/**
	 * @param adlRepeatInterval
	 *            The adlRepeatInterval to set.
	 */
	public void setAdlRepeatInterval(int adlRepeatInterval) {
		this.adlRepeatInterval = adlRepeatInterval;
	}

	/**
	 * @return Returns the pnlDepartureGap.
	 */
	public int getPnlDepartureGap() {
		return pnlDepartureGap;
	}

	/**
	 * @param pnlDepartureGap
	 *            The pnlDepartureGap to set.
	 */
	public void setPnlDepartureGap(int pnlDepartureGap) {
		this.pnlDepartureGap = pnlDepartureGap;
	}

	/**
	 * @return Returns the lastAdlGap.
	 */
	public int getLastAdlGap() {
		return lastAdlGap;
	}

	/**
	 * @param lastAdlGap
	 *            The lastAdlGap to set.
	 */
	public void setLastAdlGap(int lastAdlGap) {
		this.lastAdlGap = lastAdlGap;
	}

	public int getAdlRepeatIntervalAfterCutoffTime() {
		return adlRepeatIntervalAfterCutoffTime;
	}

	public void setAdlRepeatIntervalAfterCutoffTime(int adlRepeatIntervalAfterCutoffTime) {
		this.adlRepeatIntervalAfterCutoffTime = adlRepeatIntervalAfterCutoffTime;
	}

	public long getFlightCutoffTime() {
		return flightCutoffTime;
	}

	public void setFlightCutoffTime(long flightCutoffTime) {
		this.flightCutoffTime = flightCutoffTime;
	}
}
