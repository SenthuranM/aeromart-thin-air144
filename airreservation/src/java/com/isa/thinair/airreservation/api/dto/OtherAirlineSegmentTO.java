package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class OtherAirlineSegmentTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String flightNumber;

	private String bookingCode;

	private String marketingFlightNumber;

	private String marketingBookingCode;

	private Date departureDateTimeLocal;
	
	private Date arrivalDateTimeLocal;

	private String status;

	private Integer flightSegId;
	
	private String segmentCode;

	public String getFlightNumber() {
		return flightNumber;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public String getMarketingFlightNumber() {
		return marketingFlightNumber;
	}

	public String getMarketingBookingCode() {
		return marketingBookingCode;
	}

	public String getStatus() {
		return status;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public void setMarketingFlightNumber(String marketingFlightNumber) {
		this.marketingFlightNumber = marketingFlightNumber;
	}

	public void setMarketingBookingCode(String marketingBookingCode) {
		this.marketingBookingCode = marketingBookingCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public OtherAirlineSegment toOtherAirlineSegment() {
		OtherAirlineSegment otherAirlineSegment = new OtherAirlineSegment();
		
		otherAirlineSegment.setFlightNumber(this.getFlightNumber());
		otherAirlineSegment.setBookingCode(this.getBookingCode());
		otherAirlineSegment.setMarketingFlightNumber(this.getMarketingFlightNumber());
		otherAirlineSegment.setMarketingBookingCode(this.getMarketingBookingCode());
		otherAirlineSegment.setDepartureDateTimeLocal(this.getDepartureDateTimeLocal());
		otherAirlineSegment.setArrivalDateTimeLocal(this.getArrivalDateTimeLocal());
		otherAirlineSegment.setStatus(this.getStatus());
		otherAirlineSegment.setFlightSegId(this.getFlightSegId());
		if (segmentCode != null) {
			String[] values = segmentCode.split("/");
			otherAirlineSegment.setOrigin(values[0]);
			otherAirlineSegment.setDestination(values[1]);
		}	

		return otherAirlineSegment;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}
	
	public Date getDepartureDate() {
		return CalendarUtil.getStartTimeOfDate(departureDateTimeLocal);
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}
	
	@Override
	public String toString() {
		return "OtherAirlineSegment [flightNumber=" + flightNumber + ", bookingCode=" + bookingCode + ", marketingFlightNumber="
				+ marketingFlightNumber + ", marketingBookingCode=" + marketingBookingCode + ", departureDateTimeLocal="
				+ departureDateTimeLocal + ", arrivalDateTimeLocal=" + arrivalDateTimeLocal + ", status=" + status
				+ ", flightSegId=" + flightSegId + ", segmentCode=" + segmentCode + "]";
	}
}

