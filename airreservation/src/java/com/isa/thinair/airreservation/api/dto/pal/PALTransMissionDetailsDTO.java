package com.isa.thinair.airreservation.api.dto.pal;

import java.io.Serializable;
import java.sql.Timestamp;

public class PALTransMissionDetailsDTO implements Serializable {

	private static final long serialVersionUID = 857658556949977693L;

	private int flightId;

	private String flightNumber;

	private String departureStation;

	private Timestamp departureTimeZulu;

	private Timestamp departuretimeLocal;

	private int flightSegId;

	private int palDepartureGap;

	private int calRepeatInterval;

	private int lastCalGap;

	private long flightCutoffTime;

	private int calRepeatIntervalAfterCutoffTime;

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public Timestamp getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	public void setDepartureTimeZulu(Timestamp departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	public Timestamp getDeparturetimeLocal() {
		return departuretimeLocal;
	}

	public void setDeparturetimeLocal(Timestamp departuretimeLocal) {
		this.departuretimeLocal = departuretimeLocal;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public int getPalDepartureGap() {
		return palDepartureGap;
	}

	public void setPalDepartureGap(int palDepartureGap) {
		this.palDepartureGap = palDepartureGap;
	}

	public int getCalRepeatInterval() {
		return calRepeatInterval;
	}

	public void setCalRepeatInterval(int calRepeatInterval) {
		this.calRepeatInterval = calRepeatInterval;
	}

	public int getLastCalGap() {
		return lastCalGap;
	}

	public void setLastCalGap(int lastCalGap) {
		this.lastCalGap = lastCalGap;
	}

	public long getFlightCutoffTime() {
		return flightCutoffTime;
	}

	public void setFlightCutoffTime(long flightCutoffTime) {
		this.flightCutoffTime = flightCutoffTime;
	}

	public int getCalRepeatIntervalAfterCutoffTime() {
		return calRepeatIntervalAfterCutoffTime;
	}

	public void setCalRepeatIntervalAfterCutoffTime(int calRepeatIntervalAfterCutoffTime) {
		this.calRepeatIntervalAfterCutoffTime = calRepeatIntervalAfterCutoffTime;
	}

}
