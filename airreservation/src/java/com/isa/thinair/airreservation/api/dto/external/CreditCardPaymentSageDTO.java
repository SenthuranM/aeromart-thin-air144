package com.isa.thinair.airreservation.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CreditCardPaymentSageDTO implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	private Date transactionDate;
	private String accountCode;	
	private int cardTypeId;
	private String cardType;
	private String cardTypeName;
	private String AuthorizedCode;
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String currency;
	private String pnr;
	private char transferStatus;
	private Date transferTimeStamp;	
	private BigDecimal amountInPaymentCurrency;
	private String paymentGatewayName;
	private String paymentCurrency;
	
	public BigDecimal getAmountInPaymentCurrency() {
		return amountInPaymentCurrency;
	}
	public void setAmountInPaymentCurrency(BigDecimal amountInPaymentCurrency) {
		this.amountInPaymentCurrency = amountInPaymentCurrency;
	}
	public String getPaymentGatewayName() {
		return paymentGatewayName;
	}
	public void setPaymentGatewayName(String paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}	
	public String getAuthorizedCode() {
		return AuthorizedCode;
	}
	public void setAuthorizedCode(String authorizedCode) {
		AuthorizedCode = authorizedCode;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public char getTransferStatus() {
		return transferStatus;
	}
	public void setTransferStatus(char transferStatus) {
		this.transferStatus = transferStatus;
	}
	public Date getTransferTimeStamp() {
		return transferTimeStamp;
	}
	public void setTransferTimeStamp(Date transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}
	public String getCardTypeName() {
		return cardTypeName;
	}
	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public int getCardTypeId() {
		return cardTypeId;
	}
	public void setCardTypeId(int cardTypeId) {
		this.cardTypeId = cardTypeId;
	}
	public String getPaymentCurrency() {
		return paymentCurrency;
	}
	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}	
	
}
