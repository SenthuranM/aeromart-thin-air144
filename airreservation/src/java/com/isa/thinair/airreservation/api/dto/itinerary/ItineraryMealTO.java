package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItineraryMealTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String mealName;
	private String mealCode;
	private Integer soldMeals;

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public Integer getSoldMeals() {
		return soldMeals;
	}

	public void setSoldMeals(Integer soldMeals) {
		this.soldMeals = soldMeals;
	}
}
