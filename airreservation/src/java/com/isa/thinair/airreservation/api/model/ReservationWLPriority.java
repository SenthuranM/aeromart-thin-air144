package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of WL reservation priority
 * 
 * @hibernate.class table = "T_PNR_WL_PRIORITY"
 */
public class ReservationWLPriority extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private Integer pnrWLPriorityId;
	
	private String pnr;
	
	private Integer flightSegId;
	
	private Integer priority;
	
	private Integer totalPassengerCount;
	
	private Integer infantCount;
	
	private String status;

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}
	
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PRIORITY"
	 */
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * @return Returns the pnrWLPriorityId.
	 * @hibernate.id column = "PNR_WL_PRIORITY_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_WL_PRIORITY"
	 */
	public Integer getPnrWLPriorityId() {
		return pnrWLPriorityId;
	}

	public void setPnrWLPriorityId(Integer pnrWLPriorityId) {
		this.pnrWLPriorityId = pnrWLPriorityId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "TOTAL_PAX_ADULT_CHILD_COUNT"
	 */
	public Integer getTotalPassengerCount() {
		return totalPassengerCount;
	}

	public void setTotalPassengerCount(Integer totalPassengerCount) {
		this.totalPassengerCount = totalPassengerCount;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "TOTAL_PAX_INFANT_COUNT"
	 */
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
	
}
