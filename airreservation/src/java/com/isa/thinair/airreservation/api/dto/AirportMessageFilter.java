package com.isa.thinair.airreservation.api.dto;

import java.util.Date;

public class AirportMessageFilter {

	private String depAirport;
	private String arrAirport;
	private String salesChanel;
	private String stage;
	private Date fromDate;
	private Date toDate;

	private String flightNumber;
	private Date flightDepDate;
	private String flightId;

	private boolean returnFlag;

	/**
	 * @return the depAirport
	 */
	public String getDepAirport() {
		return depAirport;
	}

	/**
	 * @param depAirport
	 *            the depAirport to set
	 */
	public void setDepAirport(String depAirport) {
		this.depAirport = depAirport;
	}

	/**
	 * @return the arrAirport
	 */
	public String getArrAirport() {
		return arrAirport;
	}

	/**
	 * @param arrAirport
	 *            the arrAirport to set
	 */
	public void setArrAirport(String arrAirport) {
		this.arrAirport = arrAirport;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 *            the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 *            the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the salesChanel
	 */
	public String getSalesChanel() {
		return salesChanel;
	}

	/**
	 * @param salesChanel
	 *            the salesChanel to set
	 */
	public void setSalesChanel(String salesChanel) {
		this.salesChanel = salesChanel;
	}

	/**
	 * @return the stage
	 */
	public String getStage() {
		return stage;
	}

	/**
	 * @param stage
	 *            the stage to set
	 */
	public void setStage(String stage) {
		this.stage = stage;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the flightDepDate
	 */
	public Date getFlightDepDate() {
		return flightDepDate;
	}

	/**
	 * @param flightDepDate
	 *            the flightDepDate to set
	 */
	public void setFlightDepDate(Date flightDepDate) {
		this.flightDepDate = flightDepDate;
	}

	/**
	 * @return the returnFlag
	 */
	public boolean isReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            the returnFlag to set
	 */
	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return the flightId
	 */
	public String getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

}
