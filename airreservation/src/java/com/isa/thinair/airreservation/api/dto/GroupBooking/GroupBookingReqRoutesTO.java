package com.isa.thinair.airreservation.api.dto.GroupBooking;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GroupBookingReqRoutesTO implements Serializable {

	private static final long serialVersionUID = 23514212345887L;
	private Integer id;
	private Integer requestID;
	private String departure;
	private String arrival;
	private Date departureDate;
	private Date returningDate;
	private String departureDateStr;
	private String returningDateStr;
	private String ondCode;	
	private String flightNumber;
	private String via;

	private final String DATE_FORMAT = "dd/MM/yyyy";

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getReturningDate() {
		return returningDate;
	}

	public void setReturningDate(Date returningDate) {
		this.returningDate = returningDate;
	}
	
	public Integer getRequestID() {
		return requestID;
	}

	public void setRequestID(Integer requestID) {
		this.requestID = requestID;
	}

	public String getDepartureDateStr() {
		return departureDateStr;
	}

	public void setDepartureDateStr(String departureDateStr) throws ParseException {
		this.departureDateStr = departureDateStr;
		this.departureDate = new SimpleDateFormat(DATE_FORMAT).parse(departureDateStr);
	}
	
	public String getReturningDateStr() {
		return returningDateStr;
	}

	public void setReturningDateStr(String returningDateStr) throws ParseException {
		if (returningDateStr != null) {
			this.returningDateStr = returningDateStr;
			this.returningDate = new SimpleDateFormat(DATE_FORMAT).parse(returningDateStr);
		}
	}

	public String getOndCode() {
		
		if(via == null || via.isEmpty()){
			setOndCode(departure+"/"+arrival);
		}
		else{
			setOndCode(departure+"/"+via+"/"+arrival);
		}
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}
}
