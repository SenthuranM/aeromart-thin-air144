/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * CreditCardSales Sales is the entity class to represent a CreditSales model
 * 
 * @author :byorn
 * @hibernate.class table = "T_CREDITCARD_SALES_V2"
 */
public class CreditCardSalesSage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3968602299542524909L;
	private Integer id;
	private Date dateOfSale;
	private String accountCode;	
	private int cardTypeId;
	private String AuthorizedCode;
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String currency;
	private String pnr;
	private char transferStatus;
	private Date transferTimeStamp;
	private BigDecimal amountInPaymentCurrency;
	private String paymentGatewayName;
	private String paymentCurrency;

	public BigDecimal getAmountInPaymentCurrency() {
		return amountInPaymentCurrency;
	}

	public void setAmountInPaymentCurrency(BigDecimal amountInPaymentCurrency) {
		this.amountInPaymentCurrency = amountInPaymentCurrency;
	}

	public String getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(String paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}

	/**
	 * returns the cardTypeId
	 * 
	 * @return Returns the cardTypeId
	 * @hibernate.property column = "CARD_PAYMENT_MAPPING_CODE"
	 * 
	 */
	public int getCardTypeId() {
		return cardTypeId;		
	}

	/**
	 * sets the cardTypeId
	 * 
	 * @param cardTypeId
	 *            The cardTypeId to set.
	 */
	public void setCardTypeId(int cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	/**
	 * returns the dateOfsales
	 * 
	 * @return Returns the dateOfsales.
	 * @hibernate.property column = "DATE_OF_SALE"
	 */
	public Date getDateOfSale() {
		return dateOfSale;
	}

	/**
	 * sets the dateofsales
	 * 
	 * @param dateOfSales
	 *            The dateOfSales to set.
	 */
	public void setDateOfSale(Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}	

	/**
	 * returns the transferStatus
	 * 
	 * @return Returns the transferStatus
	 * @hibernate.property column = "TRANSFER_STATUS"
	 */
	public char getTransferStatus() {
		return transferStatus;
	}

	/**
	 * sets the transferStatus
	 * 
	 * @param transferStatus
	 *            The transferStatus to set.
	 */
	public void setTransferStatus(char transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * returns the transferTimeStamp
	 * 
	 * @return Returns the transferTimeStamp
	 * @hibernate.property column = "TRANSFER_TIMESTAMP"
	 */
	public Date getTransferTimeStamp() {
		return transferTimeStamp;
	}

	/**
	 * sets the transferTimeStamp
	 * 
	 * @param transferTimeStamp
	 *            ThetransferTimeStamp.
	 */
	public void setTransferTimeStamp(Date transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "CREDITCARD_SALES_V2_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "s_creditcard_sales_v2"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}
    
	/**
	 * returns the accountCode
	 * 
	 * @return Returns the accountCode
	 * @hibernate.property column = "ACCOUNT_CODE"
	 */
	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	/**
	 * returns the AuthorizedCode
	 * 
	 * @return Returns the AuthorizedCode
	 * @hibernate.property column = "AUTH_CODE"
	 */
	public String getAuthorizedCode() {
		return AuthorizedCode;
	}

	public void setAuthorizedCode(String authorizedCode) {
		AuthorizedCode = authorizedCode;
	}
    
	/**
	 * returns the amount
	 * 
	 * @return Returns the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
    
	/**
	 * returns the currency
	 * 
	 * @return Returns the currency
	 * @hibernate.property column = "CURRENCY"
	 */
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
    
	/**
	 * returns the pnr
	 * 
	 * @return Returns the pnr
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}
	
}
