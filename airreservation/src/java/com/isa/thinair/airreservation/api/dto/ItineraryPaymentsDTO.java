/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Holds the itinerary payment specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ItineraryPaymentsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9089217392586776557L;

	/** Holds the passenger specific payments collection of PaymentDetailDTO */
	private Collection<PaymentDetailDTO> colPaymentDetailDTO;

	/** Holds the passenger specific payment amount */
	private BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal paxPaymentAmountInPayCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String paymentCurrencyCode;

	/**
	 * Returns total passenger payment amount as a String format value
	 * 
	 * @param numberFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getPaxPaymentAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(Math.abs(this.getPaxPaymentAmount().doubleValue()), numberFormat);
	}

	/**
	 * @return Returns the colPaymentDetailDTO.
	 */
	public Collection<PaymentDetailDTO> getColPaymentDetailDTO() {
		return colPaymentDetailDTO;
	}

	/**
	 * @param colPaymentDetailDTO
	 *            The colPaymentDetailDTO to set.
	 */
	public void setColPaymentDetailDTO(Collection<PaymentDetailDTO> colPaymentDetailDTO) {
		this.colPaymentDetailDTO = colPaymentDetailDTO;
	}

	public void addPaymentDetailDTO(PaymentDetailDTO paymentDetailDTO) {
		if (this.colPaymentDetailDTO == null) {
			this.colPaymentDetailDTO = new ArrayList<PaymentDetailDTO>();
		}
		this.colPaymentDetailDTO.add(paymentDetailDTO);
	}

	/**
	 * @return Returns the paxPaymentAmount.
	 */
	public BigDecimal getPaxPaymentAmount() {
		return paxPaymentAmount;
	}

	/**
	 * @param paxPaymentAmount
	 *            The paxPaymentAmount to set.
	 */
	public void setPaxPaymentAmount(BigDecimal paxPaymentAmount) {
		this.paxPaymentAmount = paxPaymentAmount;
	}

	/**
	 * @return the paxPaymentAmountInPayCurrency
	 */
	public BigDecimal getPaxPaymentAmountInPayCurrency() {
		return paxPaymentAmountInPayCurrency;
	}

	/**
	 * @param paxPaymentAmountInPayCurrency
	 *            the paxPaymentAmountInPayCurrency to set
	 */
	public void setPaxPaymentAmountInPayCurrency(BigDecimal paxPaymentAmountInPayCurrency) {
		this.paxPaymentAmountInPayCurrency = paxPaymentAmountInPayCurrency;
	}

	public String getPaxPayAmountInPayCurrencyAsString(String numberFormat) throws ModuleException {
		return this.getPaxPaymentAmountInPayCurrency().abs().toString();
	}

	/**
	 * @return the paymentCurrencyCode
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}
}
