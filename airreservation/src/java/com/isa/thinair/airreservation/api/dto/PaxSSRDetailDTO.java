package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * Holds SSR Information to Populate Itinarary \ PNL \ MANIFEST
 * 
 * @author DumindaG
 * @version 1.0
 */
public class PaxSSRDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer ssrId;

	/** Holds IATA standard SSR Code */
	private String ssrCode;

	private String ssrText;

	private String contextId;

	private double chargeAmount;

	private Integer pnrPaxId;

	private char pnlAdlStatus;

	private char emailStatus;

	private String ssrDesc;

	private String status;

	private Integer ssrSubCatID;

	private Integer pNRSegId;
	
	//Use to identify in-flight service & airport service
	private Integer ssrCatId;

	private boolean isNICSentInPNLADL = false;

	/**
	 * @return the ssrSubCatID
	 */
	public Integer getSsrSubCatID() {
		return ssrSubCatID;
	}

	/**
	 * @param ssrSubCatID
	 *            the ssrSubCatID to set
	 */
	public void setSsrSubCatID(Integer ssrSubCatID) {
		this.ssrSubCatID = ssrSubCatID;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public double getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the contextId.
	 */
	public String getContextId() {
		return contextId;
	}

	/**
	 * @param contextId
	 *            The contextId to set.
	 */
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	/**
	 * @return Returns the emailStatus.
	 */
	public char getEmailStatus() {
		return emailStatus;
	}

	/**
	 * @param emailStatus
	 *            The emailStatus to set.
	 */
	public void setEmailStatus(char emailStatus) {
		this.emailStatus = emailStatus;
	}

	/**
	 * @return Returns the pnlAdlStatus.
	 */
	public char getPnlAdlStatus() {
		return pnlAdlStatus;
	}

	/**
	 * @param pnlAdlStatus
	 *            The pnlAdlStatus to set.
	 */
	public void setPnlAdlStatus(char pnlAdlStatus) {
		this.pnlAdlStatus = pnlAdlStatus;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the ssrId.
	 */
	public Integer getSsrId() {
		return ssrId;
	}

	/**
	 * @param ssrId
	 *            The ssrId to set.
	 */
	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		return ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the ssrCode
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            the ssrCode to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrDesc.
	 */
	public String getSsrDesc() {
		return ssrDesc;
	}

	/**
	 * @param ssrDesc
	 *            The ssrDesc to set.
	 */
	public void setSsrDesc(String ssrDesc) {
		this.ssrDesc = ssrDesc;
	}

	public Integer getPNRSegId() {
		return this.pNRSegId;
	}

	public void setPNRSegId(Integer segId) {
		this.pNRSegId = segId;
	}

	/**
	 * @return the ssrCatId
	 */
	public Integer getSsrCatId() {
		return ssrCatId;
	}

	/**
	 * @param ssrCatId the ssrCatId to set
	 */
	public void setSsrCatId(Integer ssrCatId) {
		this.ssrCatId = ssrCatId;
	}

	public boolean isNICSentInPNLADL() {
		return isNICSentInPNLADL;
	}

	public void setNICSentInPNLADL(boolean isNICSentInPNLADL) {
		this.isNICSentInPNLADL = isNICSentInPNLADL;
	}

}
