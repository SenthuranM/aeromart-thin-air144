/**
 * 
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author indika
 * @hibernate.class table = "T_REINSTATED_CREDIT"
 */
public class ReinstatedCredit extends Persistent {

	private static final long serialVersionUID = 8176460991866116979L;

	private int rcid;

	private int paxTxnID;

	private String pnrPaxId;

	private BigDecimal amount;

	private Date datetime;

	private String note;

	/**
	 * @return Returns the note.
	 * 
	 * @hibernate.property column = "NOTE"
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            The note to set.
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return Returns the rcid.
	 * @hibernate.id column = "RC_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_REINSTATED_CREDIT"
	 * 
	 */
	public int getRcid() {
		return rcid;
	}

	/**
	 * @param rcid
	 *            The rcid to set.
	 */
	public void setRcid(int rcid) {
		this.rcid = rcid;
	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the datetime.
	 * @hibernate.property column = "DATETIMESTAMP"
	 */
	public Date getDatetime() {
		return datetime;
	}

	/**
	 * @param datetime
	 *            The datetime to set.
	 */
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	/**
	 * @return Returns the paxTxnID.
	 * @hibernate.property column = "PAX_TXN_ID"
	 */
	public int getPaxTxnID() {
		return paxTxnID;
	}

	/**
	 * @param paxTxnID
	 *            The paxTxnID to set.
	 */
	public void setPaxTxnID(int paxTxnID) {
		this.paxTxnID = paxTxnID;
	}

	/**
	 * @return Returns the pnrpaxId.
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public String getPnrpaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrpaxId
	 *            The pnrpaxId to set.
	 */
	public void setPnrpaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}
}
