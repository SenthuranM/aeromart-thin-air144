package com.isa.thinair.airreservation.api.dto.onlinecheckin;

import java.io.Serializable;
import java.util.Date;

public class OnlineCheckInReminderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pnr;
	private String origin;
	private String flightNo;
	private String firstName;
	private String lastName;
	private String preferedLanguage;
	private Date departureDate;
	private String jobName;
	private String jobGroupName;
	private String email;
	private Date sendNotificationTime;
	private String segCode;
	private Integer fltSegId;
	private Integer flightSegmentNotificationId;
	

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public void setPreferedLanguage(String preferedLanguage) {
		this.preferedLanguage = preferedLanguage;
	}

	public String getPreferedLanguage() {
		return preferedLanguage;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobGroupName(String jobGroupName) {
		this.jobGroupName = jobGroupName;
	}

	public String getJobGroupName() {
		return jobGroupName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setSendNotificationTime(Date sendNotificationTime) {
		this.sendNotificationTime = sendNotificationTime;
	}

	public Date getSendNotificationTime() {
		return sendNotificationTime;
	}


	public void setSegCode(String segCode) {
		this.segCode = segCode;
	}


	public String getSegCode() {
		return segCode;
	}

	public void setFltSegId(Integer fltSegId) {
		this.fltSegId = fltSegId;
	}

	public Integer getFltSegId() {
		return fltSegId;
	}

	public void setFlightSegmentNotificationId(Integer flightSegmentNotificationId) {
		this.flightSegmentNotificationId = flightSegmentNotificationId;
	}

	public Integer getFlightSegmentNotificationId() {
		return flightSegmentNotificationId;
	}

}
