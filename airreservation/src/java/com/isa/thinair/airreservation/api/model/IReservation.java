/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

/**
 * Reservation interface where a client can use to enter main reservation information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface IReservation extends IResSegment {
	/**
	 * Specify the PNR Zulu Release Time Stamp
	 * 
	 * @param pnrZuluReleaseTimeStamp
	 */
	public void setPnrZuluReleaseTimeStamp(Date pnrZuluReleaseTimeStamp);

	/**
	 * Add contact information
	 * 
	 * @param userNotes
	 * @param contactInfo
	 * @param userNoteType
	 */
	public void addContactInfo(String userNotes, ReservationContactInfo contactInfo, String userNoteType);

	/**
	 * Ammend the user notes
	 * 
	 * @param userNotes
	 */
	public void ammendUserNotes(String userNotes);

	/**
	 * Add outgoing segment
	 * 
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param returnOndGroupId
	 * @param originatorRef
	 * @param selectedBundledFarePeriodId
	 */
	public void addOutgoingSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			String originatorRef, Integer selectedBundledFarePeriodId, String codeShareFlightNo, String codeShareBc);

	/**
	 * Add outgoing segment sets the baggageondgroupid for external carrier segments.
	 * 
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param returnOndGroupId
	 * @param originatorRef
	 * @param ondBaggageGrpId
	 * @param selectedBundledFarePeriodId
	 */
	public void addOutgoingSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			String externalRef, Integer ondBaggageGrpId, Integer selectedBundledFarePeriodId, String codeShareFlightNo,
			String codeShareBc);

	/**
	 * Add return segment
	 * 
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param openRtConfirmBefore
	 * @param returnOndGroupId
	 * @param originatorRef
	 * @param selectedBundledFarePeriodId
	 */
	public void addReturnSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Date openRtConfirmBefore,
			Integer returnOndGroupId, String originatorRef, Integer selectedBundledFarePeriodId, String codeShareFlightNo, String codeShareBc);

	/**
	 * Add return segment sets the baggageondgroupid for external carrier segments.
	 * 
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param openRtConfirmBefore
	 * @param returnOndGroupId
	 * @param originatorRef
	 * @param ondBaggageGrpId
	 * @param selectedBundledFarePeriodId
	 */
	public void addReturnSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Date openRtConfirmBefore,
			Integer returnOndGroupId, String externalRef, Integer baggageOndGroupId, Integer selectedBundledFarePeriodId,
			String codeShareFlightNo, String codeShareBc);

	/**
	 * Add a single
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param passportNo
	 *            TODO
	 * @param passportExpiry
	 *            TODO
	 * @param passportIssuedCntry
	 *            TODO
	 * @param employeeId
	 *            TODO
	 * @param dateOfJoin
	 *            TODO
	 * @param idCategory
	 *            TODO
	 * @param paxCategory
	 *            TODO
	 * @param iPayment
	 * @param segmentSSRs
	 * @param segmentSSRs
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param translationlanguage
	 *            TODO
	 * @param titleOl
	 *            TODO
	 * @param eticketNo
	 *            TODO
	 * @throws ModuleException
	 */
	public void addSingle(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String translationlanguage, String titleOl,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException;

	/**
	 * Add a child
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param passportNo
	 *            TODO
	 * @param passportExpiry
	 *            TODO
	 * @param passportIssuedCntry
	 *            TODO
	 * @param employeeId
	 *            TODO
	 * @param dateOfJoin
	 *            TODO
	 * @param idCategory
	 *            TODO
	 * @param paxCategory
	 *            TODO
	 * @param iPayment
	 * @param segmentSSRs
	 * @param segmentSSRs
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param titleOl
	 *            TODO
	 * @param languageCode
	 *            TODO
	 * @param eticketNo
	 *            TODO
	 * @throws ModuleException
	 */
	public void addChild(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String titleOl, String languageCode,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException;

	/**
	 * Add an infant
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @param passportNo
	 *            TODO
	 * @param passportExpiry
	 *            TODO
	 * @param passportIssuedCntry
	 *            TODO
	 * @param employeeId
	 *            TODO
	 * @param dateOfJoin
	 *            TODO
	 * @param idCategory
	 *            TODO
	 * @param paxCategory
	 *            TODO
	 * @param segmentSSRs
	 * @param segmentSSRs
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param languageCode
	 *            TODO
	 * @param autoCnxId
	 * @param eticketNo
	 *            TODO
	 * @throws ModuleException
	 */
	public void addInfant(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int parentSeqId, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String languageCode, Integer autoCnxId,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException;

	/**
	 * Add a Parent
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param infantSeqId
	 * @param passportNo
	 *            TODO
	 * @param passportExpiry
	 *            TODO
	 * @param passportIssuedCntry
	 *            TODO
	 * @param employeeId
	 *            TODO
	 * @param dateOfJoin
	 *            TODO
	 * @param idCategory
	 *            TODO
	 * @param paxCategory
	 *            TODO
	 * @param iPayment
	 * @param segmentSSRs
	 * @param segmentSSRs
	 * @param firstNameOl
	 *            TODO
	 * @param lastNameOl
	 *            TODO
	 * @param titleOl
	 *            TODO
	 * @param languageCode
	 *            TODO
	 * @param eticketNo
	 *            TODO
	 * @throws ModuleException
	 */
	public void addParent(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int infantSeqId, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String titleOl, String languageCode,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException;

	/**
	 * Allows the Add Ond Fare DTO(s)
	 * 
	 * @param colOndFareDTOs
	 * @return
	 */
	public void addOndFareDTOs(Collection<OndFareDTO> colOndFareDTOs);

	/**
	 * Add Tempory Payment Entries
	 * 
	 * @param mapTnxIds
	 */
	public void addTemporyPaymentEntries(Map<Integer, CardPaymentInfo> mapTnxIds);

	/**
	 * Sets Reservation Admin Info.
	 * 
	 * @param reservationAdminInfo
	 */
	public void setReservationAdminInfoTO(ReservationAdminInfoTO reservationAdminInfoTO);

	/**
	 * Add Any External Segment information for the reservation This could be external inbound and outbound connection
	 * information
	 * 
	 * @param segmentSeq
	 * @param externalCarrierCode
	 * @param flightNo
	 * @param segmentCode
	 * @param cabinClassCode
	 * @param departureDate
	 * @param arrivalDate
	 */
	public void addExternalPnrSegment(int segmentSeq, String externalCarrierCode, String flightNo, String segmentCode,
			String cabinClassCode, Date departureDate, Date arrivalDate);

	/**
	 * add a external pnr segments to the reservation
	 * 
	 * @param externalPnrSegment
	 */
	public void addExternalPnrSegment(ExternalPnrSegment externalPnrSegment);

	public void addExternalPnrSegments(Collection<ExternalPnrSegment> externalPnrSegment);
	
	public void addOtherAirlineSegment(String flightNo, String bookingCode, String marketingFlightNo,
			String marketingBookingCode, Date departureDateTimeLocal, Date arrivalDateTimeLocal, String status,
			Integer flightSegId, String segmentCode);

	public void addInsurance(IInsuranceRequest insurance);

	/**
	 * Sets the GDS Originator PNR
	 * 
	 * @param originatorPnr
	 */
	public void setOriginatorPnr(String originatorPnr);

	public void setBookingCategory(String bookingCategory);

	public void setItineraryFareMask(String itineraryFareMask);

	/**
	 * Sets the PNR
	 * 
	 * @param pnr
	 */
	public void setPnr(String pnr);

	public void setLastCurrencyCode(String lastCurrencyCode);

	public void setLastModificationTimestamp(Date lastModTimestamp);

	public void setExtRecordLocator(String extRecLoc);

	public String getExtRecordLocator();

	public void setBookingType(String bookingType);

	/**
	 * set 'Y' if the reservation is dummy
	 * 
	 * @param flag
	 */
	public void setDummyBooking(char flag);

	/**
	 * If set N reservation become readonly. It cannot be modified.
	 * 
	 * @param flag
	 */
	public void setModifiable(char flag);

	/**
	 * @return
	 */
	public int getChargesApplicability();

	/**
	 * @param chargesApplicability
	 */
	public void setChargesApplicability(int chargesApplicability);

	/**
	 * 
	 * @return ssr segment map
	 */
	public Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> getPaxSegmentSSRDTOMaps();

	public void setFareDiscount(float fareDiscountPercentage, String discNotes);

	// public void setTicketValidity(Collection<OndFareDTO> colOndFareDTOs);

	public void setSkipDuplicateCheck(boolean skipDuplicationCheck);

	public void setUsedOtherCarrierCredit(boolean useOtherCarrierCredit);

	public void setFareDiscountCode(String fareDiscountCode);

	public void setAllowFlightSearchAfterCutOffTime(boolean allowFlightSearchAfterCutOffTime);

	public void setFareDiscountInfo(DiscountedFareDetails discountedFareDetails);

	public AutoCancellationInfo getAutoCancellationInfo();

	public void setGDSId(Integer gdsId);

	public void setExternalPos(String externalPos);

	public void setApplicableAgentCommissions(AgentCommissionDetails agentCommissions);

	public void setGroupBookingRequestID(long groupBookingRequestID);

	public void setLoyaltyPaymentInfo(LoyaltyPaymentInfo loyaltyPaymentInfo);
	
	public void setReasonForAllowBlPaxRes(String reasonForAllowBlPaxRes);
	
	public String getReasonForAllowBlPaxRes();

	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo);
	
	public void setCreateResNPayInitInDifferentFlows(boolean isCreateResNPayInitInDifferentFlows);

	public void setOriginCountryOfCall(String originCountry);
}