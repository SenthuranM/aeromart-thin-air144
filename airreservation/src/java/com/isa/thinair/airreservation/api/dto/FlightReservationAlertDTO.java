/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * $Id$
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;

/**
 * To hold flight specific reservation alert data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class FlightReservationAlertDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7141260118322869524L;

	/** Holds flight segment id as the key and segment code as the value */
	@SuppressWarnings("rawtypes")
	private HashMap flightSegments;

	/** Holds alert template id and the hashmap contains template parameters */
	@SuppressWarnings("rawtypes")
	private HashMap alertDetails;

	/** Holds the old flight information */
	private FlightAlertInfoDTO oldFlightAlertInfoDTO;

	/** Holds the new flight information */
	private FlightAlertInfoDTO newFlightAlertInfoDTO;

	/**
	 * Holds the reservation alert type
	 * 
	 * @see com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationAlertTypes
	 */
	private String reservationAlertType;

	/** Holds effected flight segment dto for cancellation */
	private Collection<EffectedFlightSegmentDTO> colEffectedFlightSegmentDTO;

	private Collection<FlightChangeInfo> flightSeatChangesInfo;

	/** Holds customized SMS alert */
	private String smsMessageDesc;

	/**
	 * @return Returns the alertDetails.
	 */
	@SuppressWarnings("rawtypes")
	public HashMap getAlertDetails() {
		return alertDetails;
	}

	/**
	 * @param alertDetails
	 *            The alertDetails to set.
	 */
	@SuppressWarnings("rawtypes")
	public void setAlertDetails(HashMap alertDetails) {
		this.alertDetails = alertDetails;
	}

	/**
	 * @return Returns the flightSegments.
	 */
	@SuppressWarnings("rawtypes")
	public HashMap getFlightSegments() {
		return flightSegments;
	}

	/**
	 * @param flightSegments
	 *            The flightSegments to set.
	 */
	@SuppressWarnings("rawtypes")
	public void setFlightSegments(HashMap flightSegments) {
		this.flightSegments = flightSegments;
	}

	/**
	 * @return Returns the newFlightAlertInfoDTO.
	 */
	public FlightAlertInfoDTO getNewFlightAlertInfoDTO() {
		return newFlightAlertInfoDTO;
	}

	/**
	 * @param newFlightAlertInfoDTO
	 *            The newFlightAlertInfoDTO to set.
	 */
	public void setNewFlightAlertInfoDTO(FlightAlertInfoDTO newFlightAlertInfoDTO) {
		this.newFlightAlertInfoDTO = newFlightAlertInfoDTO;
	}

	/**
	 * @return Returns the oldFlightAlertInfoDTO.
	 */
	public FlightAlertInfoDTO getOldFlightAlertInfoDTO() {
		return oldFlightAlertInfoDTO;
	}

	/**
	 * @param oldFlightAlertInfoDTO
	 *            The oldFlightAlertInfoDTO to set.
	 */
	public void setOldFlightAlertInfoDTO(FlightAlertInfoDTO oldFlightAlertInfoDTO) {
		this.oldFlightAlertInfoDTO = oldFlightAlertInfoDTO;
	}

	/**
	 * @return Returns the reservationAlertType.
	 */
	public String getReservationAlertType() {
		return reservationAlertType;
	}

	/**
	 * @param reservationAlertType
	 *            The reservationAlertType to set.
	 */
	public void setReservationAlertType(String reservationAlertType) {
		this.reservationAlertType = reservationAlertType;
	}

	/**
	 * Return flighr segment ids
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getFlightSegmentIds() {
		return new ArrayList(flightSegments.keySet());
	}

	/**
	 * Return alert template ids
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getAlertTemplateIds() {
		return new ArrayList(alertDetails.keySet());
	}

	/**
	 * @return Returns the colEffectedFlightSegmentDTO.
	 */
	public Collection<EffectedFlightSegmentDTO> getColEffectedFlightSegmentDTO() {
		return colEffectedFlightSegmentDTO;
	}

	/**
	 * @param colEffectedFlightSegmentDTO
	 *            The colEffectedFlightSegmentDTO to set.
	 */
	public void setColEffectedFlightSegmentDTO(Collection<EffectedFlightSegmentDTO> colEffectedFlightSegmentDTO) {
		this.colEffectedFlightSegmentDTO = colEffectedFlightSegmentDTO;
	}

	/**
	 * 
	 * @return
	 */
	public String getSmsMessageDesc() {
		return smsMessageDesc;
	}

	/**
	 * 
	 * @param smsMessageDesc
	 */
	public void setSmsMessageDesc(String smsMessageDesc) {
		this.smsMessageDesc = smsMessageDesc;
	}

	public Collection<FlightChangeInfo> getFlightSeatChangesInfo() {
		return flightSeatChangesInfo;
	}

	public void setFlightSeatChangesInfo(Collection<FlightChangeInfo> flightSeatChangesInfo) {
		this.flightSeatChangesInfo = flightSeatChangesInfo;
	}
}
