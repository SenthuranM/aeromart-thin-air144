package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class TKT extends EDISegment {

	private String e1004;
	private String e1001;
	private String e7240;
	private String e9988;

	public TKT() {
		super(EDISegmentTag.TKT);
	}

	public void setTicketNumber(String e1004) {
		this.e1004 = e1004;
	}

	public void setDocumentType(String e1001) {
		this.e1001 = e1001;
	}

	public void setNumberOfBooklets(String e7240) {
		this.e7240 = e7240;
	}

	public void setDataIndicator(String e9988) {
		this.e9988 = e9988;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C667 = new CompoundDataElement("C667", DE_STATUS.M);
		C667.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E1004, e1004));
		C667.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E1001, e1001));
		C667.addBasicDataelement(new Numeric(EDI_ELEMENT.E7240, e7240));
		C667.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9988, e9988));

		addEDIDataElement(C667);

		return this;
	}

}
