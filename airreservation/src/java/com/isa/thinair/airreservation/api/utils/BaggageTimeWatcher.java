package com.isa.thinair.airreservation.api.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Nilindra Fernando
 */
public class BaggageTimeWatcher {

	public static long getBaggageFinalCutOverTimeInMillis() {
		return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getBaggageFinalCutOverTime());
	}

	public static boolean isWithInFinalCutOver(Date departureDateZulu) {
		Calendar departTime = new GregorianCalendar();
		departTime.setTime(departureDateZulu);
		Calendar currTime = new GregorianCalendar();

		long differnce = departTime.getTimeInMillis() - currTime.getTimeInMillis();
		long cutovertime = getBaggageFinalCutOverTimeInMillis();
		if (differnce > cutovertime) {
			return false;
		} else {
			return true;
		}
	}

	public static long getBaggageCutOverTimeForIBEInMillis(boolean isModifyOperation) {
		if (isModifyOperation) {
			return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getAlterBaggageCutOverTime());
		} else {
			return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getFreshBaggageCutOverTime());
		}
	}

	public static long getBaggageCutOverTimeForXBEInMillis(boolean isModifyOperation, boolean allowTillFinalCutOver,
			boolean isStandBy) {
		if (allowTillFinalCutOver) {
			return getBaggageFinalCutOverTimeInMillis();
		}

		if (isModifyOperation) {
			if (isStandBy) {
				return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getStandByAlterBaggageCutOverTime());
			}
			return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getAlterBaggageCutOverTime());
		} else {
			if (isStandBy) {
				return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getStandByFreshBaggageCutOverTime());
			}
			return AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getFreshBaggageCutOverTime());
		}
	}

}
