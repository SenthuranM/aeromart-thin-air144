/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class FlightLegInfo implements Serializable {

	private static final long serialVersionUID = -788084742287649760L;
	private String RPH;
	private Airport departureAirport;
	private Airport arrivalAirport;
	private FlightBasicInfo operatingAirline;
	private String departureDate;

	public String getRPH() {
		return RPH;
	}

	public void setRPH(String rPH) {
		RPH = rPH;
	}

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public FlightBasicInfo getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(FlightBasicInfo operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

}
