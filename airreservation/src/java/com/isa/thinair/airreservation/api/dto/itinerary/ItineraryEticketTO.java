package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eric
 * 
 */
public class ItineraryEticketTO implements Comparable<ItineraryEticketTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String segmentCode;

	private String eTicketNumber;

	private String flightNo;

	private String departureDateInPersian;

	private boolean persianDatesSet;

	public boolean isPersianDatesSet() {
		return persianDatesSet;
	}

	public void setIsPersianDatesSet(boolean usePersianDates) {
		this.persianDatesSet = usePersianDates;
	}

	public String getDepartureDateInPersian() {
		return departureDateInPersian;
	}

	public void setDepartureDateInPersian(String departureDateInPersian) {
		this.departureDateInPersian = departureDateInPersian;
	}

	private Date departureDate;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(String eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	@Override
	public int compareTo(ItineraryEticketTO o) {

		return this.getDepartureDate().compareTo(o.getDepartureDate());
	}

}
