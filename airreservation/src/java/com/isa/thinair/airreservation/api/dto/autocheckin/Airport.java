/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class Airport implements Serializable {

	private static final long serialVersionUID = -4075468124864475852L;
	private String locationCode;

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

}
