package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class ConnectionPaxInfoTO implements Serializable {

    private Date date;
    private String segmentCode;
    private int totalPax;
    private int connectionPax;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSegmentCode() {
        return segmentCode;
    }

    public void setSegmentCode(String segmentCode) {
        this.segmentCode = segmentCode;
    }

    public int getTotalPax() {
        return totalPax;
    }

    public void setTotalPax(int totalPax) {
        this.totalPax = totalPax;
    }

    public int getConnectionPax() {
        return connectionPax;
    }

    public void setConnectionPax(int connectionPax) {
        this.connectionPax = connectionPax;
    }
}
