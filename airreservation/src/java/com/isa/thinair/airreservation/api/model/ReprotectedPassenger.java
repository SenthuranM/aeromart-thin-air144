package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * ReprotectedPassenger is the entity class to represent a reprotected Passenger model This will keep track of main reprotected Passenger information
 * 
 * @author Chanaka
 * @hibernate.class table = "T_REPROTECTED_PAX" lazy="false"
 */

public class ReprotectedPassenger implements Serializable  {
	
	private static final long serialVersionUID = -1937709272125717558L;
	private Integer reprotectedPaxID;
	private Integer pnrPaxId;
	private String voucherId;
	private Integer fromFlightSegId;
	private Integer toFlightSegId;	
	private Integer pnrSegId;
	
	/**
	 * @hibernate.id column = "REPROTECTED_PAX_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_REPROTECTED_PAX"
	 */
	public Integer getReprotectedPaxID() {
		return reprotectedPaxID;
	}
	/**
	 * @param reprotectedPaxID the reprotectedPaxID to set
	 */
	public void setReprotectedPaxID(Integer reprotectedPaxID) {
		this.reprotectedPaxID = reprotectedPaxID;
	}
	/**
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}
	/**
	 * @param pnrPaxId the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}
	/**
	 * @hibernate.property column = "VOUCHER_ID"
	 */
	public String getVoucherId() {
		return voucherId;
	}
	/**
	 * @param voucherId the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}
	/**
	 * @hibernate.property column = "FROM_FLT_SEG_ID"
	 */
	public Integer getFromFlightSegId() {
		return fromFlightSegId;
	}
	/**
	 * @param fromFlightSegId the fromFlightSegId to set
	 */
	public void setFromFlightSegId(Integer fromFlightSegId) {
		this.fromFlightSegId = fromFlightSegId;
	}
	/**
	 * @hibernate.property column = "TO_FLT_SEG_ID"
	 */
	public Integer getToFlightSegId() {
		return toFlightSegId;
	}
	/**
	 * @param toFlightSegId the toFlightSegId to set
	 */
	public void setToFlightSegId(Integer toFlightSegId) {
		this.toFlightSegId = toFlightSegId;
	}
	/**
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}
	/**
	 * @param pnrSegId the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}
	
}
