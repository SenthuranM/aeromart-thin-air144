package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class DAT extends EDISegment {

	private String e2005;
	private String e9916;
	private String e9918;

	public DAT() {
		super(EDISegmentTag.DAT);
	}

	public void setDateCode(String e2005) {
		this.e2005 = e2005;
	}

	public void setDate(String e9916) {
		this.e9916 = e9916;
	}

	public void setTime(String e9918) {
		this.e9918 = e9918;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C688 = new CompoundDataElement("C688", DE_STATUS.C);
		C688.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E2005, e2005));
		C688.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9916, e9916));
		C688.addBasicDataelement(new Numeric(EDI_ELEMENT.E9918, e9918));

		addEDIDataElement(C688);

		return this;
	}

}
