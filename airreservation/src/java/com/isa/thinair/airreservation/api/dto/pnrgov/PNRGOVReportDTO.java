package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.io.Serializable;

public class PNRGOVReportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String countryName;
	private String fltSegID;
	private String status;
	private String frmDate;
	private String toDate;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getFltSegID() {
		return fltSegID;
	}

	public void setFltSegID(String fltSegID) {
		this.fltSegID = fltSegID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFrmDate() {
		return frmDate;
	}

	public void setFrmDate(String frmDate) {
		this.frmDate = frmDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
