/**
 * 
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * @author panchatcharam.s
 * @hibernate.class table = "T_PNR_PAX_SEG_AUTO_CHKIN_SEAT"
 */

public class ReservationPaxSegAutoCheckinSeat implements Serializable {

	private static final long serialVersionUID = -1400249684475702340L;

	private Integer pnrPaxSegAutoCheckinSeatId;
	private String seatTypePreference;
	private ReservationPaxSegAutoCheckin reservationPaxSegAutoCheckin;

	/**
	 * @hibernate.id column = "PPSACS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_SEG_AUTO_CHKIN_SEAT"
	 */
	public Integer getPnrPaxSegAutoCheckinSeatId() {
		return pnrPaxSegAutoCheckinSeatId;
	}

	/**
	 * @param pnrPaxSegAutoCheckinSeatId
	 *            the pnrPaxSegAutoCheckinSeatId to set
	 */
	public void setPnrPaxSegAutoCheckinSeatId(Integer pnrPaxSegAutoCheckinSeatId) {
		this.pnrPaxSegAutoCheckinSeatId = pnrPaxSegAutoCheckinSeatId;
	}

	/**
	 * @hibernate.property column = "SEAT_TYPE_PREFERENCE"
	 */
	public String getSeatTypePreference() {
		return seatTypePreference;
	}

	/**
	 * @param seatTypePreference
	 *            the seatTypePreference to set
	 * 
	 */
	public void setSeatTypePreference(String seatTypePreference) {
		this.seatTypePreference = seatTypePreference;
	}

	/**
	 * @return the reservationPaxSegAutoCheckin
	 * @hibernate.many-to-one cascade ="all" column="PPAC_ID"
	 *                        class="com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin"
	 */
	public ReservationPaxSegAutoCheckin getReservationPaxSegAutoCheckin() {
		return reservationPaxSegAutoCheckin;
	}

	public void setReservationPaxSegAutoCheckin(ReservationPaxSegAutoCheckin reservationPaxSegAutoCheckin) {
		this.reservationPaxSegAutoCheckin = reservationPaxSegAutoCheckin;
	}
}
