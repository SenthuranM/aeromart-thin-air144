/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of PNL ADL TIMINGS
 * 
 * @author Byorn
 * @since 1.0
 * @hibernate.class table = "T_PNL_ADL_TIMINGS"
 */
public class PnlAdlTiming extends Persistent {

	private static final long serialVersionUID = 5143708459068002707L;

	private Integer id;

	private String airportCode;

	private String flightNumber;

	private int pnlDepGap;

	private int adlRepIntv;

	private Date startingZuluDate;

	private Date endingZuluDate;

	private String status;

	private String applyToAllFlights;

	private int lastAdlGap;
	
	private int adlRepIntvAfterCutoff;

	/**
	 * @return Returns the adlRepIntv.
	 * @hibernate.property column = "ADL_REP_INT"
	 */
	public int getAdlRepIntv() {
		return adlRepIntv;
	}

	/**
	 * @param adlRepIntv
	 *            The adlRepIntv to set.
	 */
	public void setAdlRepIntv(int adlRepIntv) {
		this.adlRepIntv = adlRepIntv;
	}

	/**
	 * @return Returns the airportCode.
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return Returns the endingZuluDate.
	 * @hibernate.property column = "ZULU_END_DATE"
	 */
	public Date getEndingZuluDate() {
		return endingZuluDate;
	}

	/**
	 * @param endingZuluDate
	 *            The endingZuluDate to set.
	 * 
	 */
	public void setEndingZuluDate(Date endingZuluDate) {
		this.endingZuluDate = endingZuluDate;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the pnlDepGap.
	 * @hibernate.property column = "PNL_DEP_GAP"
	 */
	public int getPnlDepGap() {
		return pnlDepGap;
	}

	/**
	 * @param pnlDepGap
	 *            The pnlDepGap to set.
	 */
	public void setPnlDepGap(int pnlDepGap) {
		this.pnlDepGap = pnlDepGap;
	}

	/**
	 * @return Returns the startingZuluDate.
	 * @hibernate.property column = "ZULU_START_DATE"
	 */
	public Date getStartingZuluDate() {
		return startingZuluDate;
	}

	/**
	 * @param startingZuluDate
	 *            The startingZuluDate to set.
	 */
	public void setStartingZuluDate(Date startingZuluDate) {
		this.startingZuluDate = startingZuluDate;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the applyToAllFlights.
	 * @hibernate.property column = "APPLY_TO_ALL"
	 */
	public String getApplyToAllFlights() {
		return applyToAllFlights;
	}

	/**
	 * @param applyToAllFlights
	 *            The applyToAllFlights to set.
	 */
	public void setApplyToAllFlights(String applyToAllFlights) {
		this.applyToAllFlights = applyToAllFlights;
	}

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "TC_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNL_ADL_TIMINGS"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Returns the lastAdlGap.
	 * @hibernate.property column = "LAST_ADL_GAP"
	 */
	public int getLastAdlGap() {
		return lastAdlGap;
	}

	/**
	 * @param lastAdlGap
	 *            The lastAdlGap to set.
	 */
	public void setLastAdlGap(int lastAdlGap) {
		this.lastAdlGap = lastAdlGap;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column="ADL_REP_INT_CUTOFF_TIME"
	 */
	public int getAdlRepIntvAfterCutoff() {
		return adlRepIntvAfterCutoff;
	}

	public void setAdlRepIntvAfterCutoff(int adlRepIntvAfterCutoff) {
		this.adlRepIntvAfterCutoff = adlRepIntvAfterCutoff;
	}
}
