/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.math.BigDecimal;

/**
 * To hold reservation external charges data transfer information
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class SMExternalChgDTO extends ExternalChgDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer paxSequenceNo;

	private Integer flightSeatId;

	private BigDecimal seatCharge;

	private Integer pnrPaxId;

	private Integer pnrPaxFareId;

	private String paxType;

	private Integer pnrSegId;

	private String flightRPH;

	private String seatNumber;

	/**
	 * @return the paxSequenceNo
	 */
	public Integer getPaxSequenceNo() {
		return paxSequenceNo;
	}

	/**
	 * @param paxSequenceNo
	 *            the paxSequenceNo to set
	 */
	public void setPaxSequenceNo(Integer paxSequenceNo) {
		this.paxSequenceNo = paxSequenceNo;
	}

	/**
	 * @return the flightSeatId
	 */
	public Integer getFlightSeatId() {
		return flightSeatId;
	}

	/**
	 * @param flightSeatId
	 *            the flightSeatId to set
	 */
	public void setFlightSeatId(Integer flightSeatId) {
		this.flightSeatId = flightSeatId;
	}

	/**
	 * @return the seatCharge
	 */
	public BigDecimal getSeatCharge() {
		return seatCharge;
	}

	/**
	 * @param seatCharge
	 *            the seatCharge to set
	 */
	public void setSeatCharge(BigDecimal seatCharge) {
		this.seatCharge = seatCharge;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		SMExternalChgDTO clone = new SMExternalChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());

		clone.setPaxSequenceNo(this.getPaxSequenceNo());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setFlightSeatId(this.getFlightSeatId());

		return clone;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrPaxFareId
	 */
	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            the pnrPaxFareId to set
	 */
	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public String getFlightRPH() {
		return flightRPH;
	}

	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	public String getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}
}
