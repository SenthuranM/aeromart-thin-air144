/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.util.Date;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Passenger interface where a client can use to store passenger information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface IPassenger {
	/**
	 * Add an Infant
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @param pnrPaxCatFOIDNumber
	 * @param pnrPaxCatFOIDExpiry
	 * @param pnrPaxCatFOIDIssuedCntry
	 * @param pnrPaxCatEmpId
	 * @param pnrPaxCatDOJ
	 * @param pnrPaxCatIdCat
	 * @param eticketNo
	 * @param paxCategory
	 * @param autoCancellationId
	 * @param nationalIDNo TODO
	 * @param placeOfBirth TODO
	 * @param visaDocNumber TODO
	 * @param visaApplicableCountry TODO
	 * @param visaDocPlaceOfIssue TODO
	 * @param visaDocIssueDate TODO
	 * @param ssrCode
	 * @param ssrRemarks
	 * @param segmentSSRs
	 * @throws ModuleException
	 */
	public void addInfant(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int parentSeqId, String pnrPaxCatFOIDNumber, Date pnrPaxCatFOIDExpiry,
			String pnrPaxCatFOIDIssuedCntry, String pnrPaxCatEmpId, Date pnrPaxCatDOJ, String pnrPaxCatIdCat, String eticketNo,
			String paxCategory, SegmentSSRAssembler segmentSSRAsm, Integer autoCancellationId, String nationalIDNo, String placeOfBirth, String visaDocNumber, String visaApplicableCountry, String visaDocPlaceOfIssue, Date visaDocIssueDate)
			throws ModuleException;

	/**
	 * Add passenger payment NOTE: Pass only parent and adult passenger ids
	 * 
	 * @param pnrPaxId
	 * @param iPayment
	 */
	public void addPassengerPayments(Integer pnrPaxId, IPayment iPayment);

	/**
	 * Sets External Payment Transaction Information
	 * 
	 * @param externalPaymentTnxInfo
	 */
	public void addExternalPaymentTnxInfo(ExternalPaymentTnx externalPaymentTnxInfo);

	/**
	 * Add Tempory Payment Entries
	 * 
	 * @param mapTnxIds
	 */
	public void addTemporyPaymentEntries(Map<Integer, CardPaymentInfo> mapTnxIds);
}
