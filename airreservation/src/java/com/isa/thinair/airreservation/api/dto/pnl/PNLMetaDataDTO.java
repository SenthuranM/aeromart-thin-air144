/**
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.dto.pnl;

import java.io.Serializable;

public class PNLMetaDataDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3476327692919294891L;

	/**
	 * This class will encapsulate the MetaDataInformation for a PNL file DTO convention may be violated due to a
	 * dependency in the template file
	 */
	private String addresspart1;

	private String addresspart2;

	private String originator;

	private String messageIdentifier;

	private String flight;

	private String day;

	private String month;

	private String boardingairport;

	private String transferpoint;

	private boolean istpm;

	private boolean isptm;

	private String partnumber;

	private int count;

	private boolean isPart2;

	private String rbd;

	private boolean rbdEnabled;

	public String getAddresspart1() {
		return addresspart1;
	}

	public void setAddresspart1(String addresspart1) {
		this.addresspart1 = addresspart1;
	}

	public String getAddresspart2() {
		return addresspart2;
	}

	public void setAddresspart2(String addresspart2) {
		this.addresspart2 = addresspart2;
	}

	public String getBoardingairport() {
		return boardingairport;
	}

	public void setBoardingairport(String boardingairport) {
		this.boardingairport = boardingairport;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public boolean isIsptm() {
		return isptm;
	}

	public void setIsptm(boolean isptm) {
		this.isptm = isptm;
	}

	public boolean isIstpm() {
		return istpm;
	}

	public void setIstpm(boolean istpm) {
		this.istpm = istpm;
	}

	public String getMessageIdentifier() {
		return messageIdentifier;
	}

	public void setMessageIdentifier(String messageIdentifier) {
		this.messageIdentifier = messageIdentifier;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getPartnumber() {
		return partnumber;
	}

	public void setPartnumber(String partnumber) {
		this.partnumber = partnumber;
	}

	public String getTransferpoint() {
		return transferpoint;
	}

	public void setTransferpoint(String transferpoint) {
		this.transferpoint = transferpoint;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public boolean isPart2() {
		return isPart2;
	}

	public void setPart2(boolean isPart2) {
		this.isPart2 = isPart2;
	}

	public String getRbd() {
		return rbd;
	}

	public void setRbd(String rbd) {
		this.rbd = rbd;
	}

	public boolean isRbdEnabled() {
		return rbdEnabled;
	}

	public void setRbdEnabled(boolean rbdEnabled) {
		this.rbdEnabled = rbdEnabled;
	}

}
