package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItineraryFinancialTotalsTO implements Serializable {

	private static final long serialVersionUID = 1L;

	// Currency
	private String currency;

	// Financials
	private String totalFare;
	private String totalCharges;
	private String totalPaidAmount;
	private String totalBalance;
	private String creditExpiryDate;
	private String totalTaxes;
	private String totalSurcharges;
	private String totalDue;
	private String totalDiscount;
	private String creditExpiryDateInPersian;
	private boolean isPersianDatesSet;
	private String totalCreditDiscount;

	public boolean isPersianDatesSet() {
		return isPersianDatesSet;
	}

	public void setPersianDatesSet(boolean isPersianDatesSet) {
		this.isPersianDatesSet = isPersianDatesSet;
	}

	public String getCreditExpiryDateInPersian() {
		return creditExpiryDateInPersian;
	}

	public void setCreditExpiryDateInPersian(String creditExpiryDateInPersian) {
		this.creditExpiryDateInPersian = creditExpiryDateInPersian;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the totalFare
	 */
	public String getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            the totalFare to set
	 */
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return the totalCharges
	 */
	public String getTotalCharges() {
		return totalCharges;
	}

	/**
	 * @param totalCharges
	 *            the totalCharges to set
	 */
	public void setTotalCharges(String totalCharges) {
		this.totalCharges = totalCharges;
	}

	/**
	 * @return the totalPaidAmount
	 */
	public String getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            the totalPaidAmount to set
	 */
	public void setTotalPaidAmount(String totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * @return the totalBalance
	 */
	public String getTotalBalance() {
		return totalBalance;
	}

	/**
	 * @param totalBalance
	 *            the totalBalance to set
	 */
	public void setTotalBalance(String totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getCreditExpiryDate() {
		return creditExpiryDate;
	}

	public void setCreditExpiryDate(String creditExpiryDate) {
		this.creditExpiryDate = creditExpiryDate;
	}

	/**
	 * @return The total amount of taxes.
	 */
	public String getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(String totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	/**
	 * Total surcharges = all charges - (taxes + fare). While this is not ideal or intuitive it is a client requirement
	 * 
	 * @return Total surcharges
	 */
	public String getTotalSurcharges() {
		return totalSurcharges;
	}

	public void setTotalSurcharges(String totalSurcharges) {
		this.totalSurcharges = totalSurcharges;
	}

	/**
	 * Total due is calculated by adding surcharges + taxes + fare. Since curcharges include all other charges except
	 * tax.
	 * 
	 * @return the totalDue
	 */
	public String getTotalDue() {
		return totalDue;
	}

	public String getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(String totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	/**
	 * Total due is calculated by adding surcharges + taxes + fare. Since curcharges include all other charges except
	 * tax.
	 * 
	 * @param totalDue
	 *            the totalDue to set
	 */
	public void setTotalDue(String totalDue) {
		this.totalDue = totalDue;
	}

	public String getTotalCreditDiscount() {
		return totalCreditDiscount;
	}

	public void setTotalCreditDiscount(String totalCreditDiscount) {
		this.totalCreditDiscount = totalCreditDiscount;
	}
}