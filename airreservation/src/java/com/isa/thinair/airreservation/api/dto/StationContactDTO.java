package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class StationContactDTO implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	private String stationCode;
	private String stationName;
	private String stationContact;
	private String stationTelephone;
	/**
	 * @param stationCode the stationCode to set
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}
	/**
	 * @return the stationCode
	 */
	public String getStationCode() {
		return stationCode;
	}
	/**
	 * @param stationName the stationName to set
	 */
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	/**
	 * @return the stationName
	 */
	public String getStationName() {
		return stationName;
	}
	/**
	 * @param stationContact the stationContact to set
	 */
	public void setStationContact(String stationContact) {
		this.stationContact = stationContact;
	}
	/**
	 * @return the stationContact
	 */
	public String getStationContact() {
		return stationContact;
	}
	/**
	 * @param stationTelephone the stationTelephone to set
	 */
	public void setStationTelephone(String stationTelephone) {
		this.stationTelephone = stationTelephone;
	}
	/**
	 * @return the stationTelephone
	 */
	public String getStationTelephone() {
		return stationTelephone;
	}
}
