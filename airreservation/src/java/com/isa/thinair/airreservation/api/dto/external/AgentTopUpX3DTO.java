package com.isa.thinair.airreservation.api.dto.external;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AgentTopUpX3DTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer tnxId;
	private Date tnxDate;
	private String agentCode;
	private BigDecimal amount;
	private BigDecimal amountLocal;
	private String correncyCode;
	private String ncDestription;
	private char transferStatus;
	private Date transferTimeStamp;
	private Integer version;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTnxId() {
		return tnxId;
	}

	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	public Date getTnxDate() {
		return tnxDate;
	}

	public void setTnxDate(Date tnxDate) {
		this.tnxDate = tnxDate;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountLocal() {
		return amountLocal;
	}

	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	public String getCorrencyCode() {
		return correncyCode;
	}

	public void setCorrencyCode(String correncyCode) {
		this.correncyCode = correncyCode;
	}

	public String getNcDestription() {
		return ncDestription;
	}

	public void setNcDestription(String ncDestription) {
		this.ncDestription = ncDestription;
	}
	public char getTransferStatus() {
		return transferStatus;
	}
	public void setTransferStatus(char transferStatus) {
		this.transferStatus = transferStatus;
	}
	public Date getTransferTimeStamp() {
		return transferTimeStamp;
	}
	public void setTransferTimeStamp(Date transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
