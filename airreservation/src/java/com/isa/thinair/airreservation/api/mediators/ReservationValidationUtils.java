/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.mediators;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.PNRModifyPreValidationStatesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentTransfer;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.INTERLINE_STATES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.dto.ApplicableBufferTimeDTO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;

/**
 * Holds Reservation Pre Validation itilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationValidationUtils {

	private static final Log log = LogFactory.getLog(ReservationValidationUtils.class);

	/**
	 * Returns whole reservation or segment pre validation states
	 * 
	 * @param reservation
	 * @param modCnxPnrSegIds
	 * @return
	 * @throws ModuleException
	 */
	public static PNRModifyPreValidationStatesTO getReservationOrSegPreValidationStates(Reservation reservation,
			Collection<Integer> modCnxPnrSegIds) throws ModuleException {

		boolean isInterlinedModifiable = false;

		if (modCnxPnrSegIds != null && modCnxPnrSegIds.size() > 0) {
			Collection<ReservationSegmentDTO> selectedSegments = ReservationApiUtils.getReservationSegmentDTOs(
					reservation.getSegmentsView(), modCnxPnrSegIds);

			for (ReservationSegmentDTO reservationSegmentDTO : selectedSegments) {
				isInterlinedModifiable = isInterlinedSegmentModifiable(reservationSegmentDTO, reservation.getSegmentsView());

				if (!isInterlinedModifiable) {
					break;
				}
			}
		} else {
			isInterlinedModifiable = isInterlinedSegmentsModifiable(reservation.getSegmentsView());
		}

		if (isInterlinedModifiable) {
			PNRModifyPreValidationStatesTO statesTO = new PNRModifyPreValidationStatesTO();
			statesTO.setInterlinedModifiable(true);
			statesTO = getBasicReservationOrSegStatuses(reservation, modCnxPnrSegIds, statesTO);
			return statesTO;
		} else {
			return new PNRModifyPreValidationStatesTO();
		}
	}

	/**
	 * Returns interlined segment transferd
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isInterlinedTransferd(Collection<ReservationSegmentDTO> segments) throws ModuleException {
		return isInterlinedSegmentsModifiable(segments);
	}

	/**
	 * Returns the basic pre validation states
	 * 
	 * @param reservation
	 * @param modCnxPnrSegIds
	 * @param statesTO
	 * @return
	 * @throws ModuleException
	 */
	private static PNRModifyPreValidationStatesTO getBasicReservationOrSegStatuses(Reservation reservation,
			Collection<Integer> modCnxPnrSegIds, PNRModifyPreValidationStatesTO statesTO) throws ModuleException {

		Collection<ReservationSegmentDTO> resSegs = reservation.getSegmentsView();

		boolean isInBufferTime = false;
		boolean hasFlownSegs = false;
		boolean isCancelledRes = ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus());
		boolean hasCancelledSegs = isCancelledRes;
		boolean isSegStatusesCheck = (modCnxPnrSegIds != null);
		boolean isRestrictedSegs = false;
		boolean hasFareRuleBuffer = false;
		boolean hasNOSHOWPax = false;
		boolean isSpecialFare = false;
		/* segment modification */
		boolean allowModifyDate = false;
		boolean allowModifyRoute = false;
		if (!isCancelledRes) {
			Date depDateZulu = null;
			Date currentDateZulu = CalendarUtil.getCurrentSystemTimeInZulu();
			for (ReservationSegmentDTO resSeg : resSegs) {
				if ((isSegStatusesCheck && modCnxPnrSegIds.contains(resSeg.getPnrSegId())) || !isSegStatusesCheck) {
					if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())) {
						depDateZulu = resSeg.getZuluDepartureDate();
						Calendar flightClosureTimeZulu = new GregorianCalendar();
						flightClosureTimeZulu.setTime(depDateZulu);
						flightClosureTimeZulu.add(Calendar.MINUTE, -AppSysParamsUtil.getFlightClosureGapInMins());

						if (flightClosureTimeZulu.getTime().before(currentDateZulu)) {
							hasFlownSegs = true;
							hasNOSHOWPax = allPaxHasNOSHOW(reservation.getPassengers(), resSeg.getPnrSegId());
							if (resSeg.getFareTO().getInternationalModifyBufferTimeInMillis() > 0) {
								isSpecialFare = true;
							}
						} else {
							long timeToDeparture = depDateZulu.getTime() - currentDateZulu.getTime();

							ApplicableBufferTimeDTO applicableBufferDTO = FlightUtil.getApplicableModifyBufferTime(resSeg
									.getFareTO().getInternationalModifyBufferTimeInMillis(), resSeg.getFareTO()
									.getDomesticModifyBufferTimeInMillis(), resSeg.getFlightId());

							long modifyBufferTime = applicableBufferDTO.getApplicableBufferTime();
							hasFareRuleBuffer = applicableBufferDTO.isFareRuleBufferUsed();

							Object[] flexiParams = ReservationApiUtils.getFlexiModifyCutOverBuffer(reservation, modCnxPnrSegIds);

							long flexiModifyCutOverBufferInMillis = -1;
							boolean isFlexiModificationAvail = ((Boolean) flexiParams[1]).booleanValue();

							if (isFlexiModificationAvail) {
								flexiModifyCutOverBufferInMillis = ((Long) flexiParams[0]).longValue();
							}

							if (flexiModifyCutOverBufferInMillis != -1 && flexiModifyCutOverBufferInMillis < modifyBufferTime) {
								modifyBufferTime = flexiModifyCutOverBufferInMillis;
							}
							if (timeToDeparture <= modifyBufferTime) {
								isInBufferTime = true;
							}
						}
					} else {
						if (isSegStatusesCheck && modCnxPnrSegIds.contains(resSeg.getPnrSegId())) {
							hasCancelledSegs = true;
						}
					}

					if (resSeg.getFareTO().isFareRestricted()) {
						isRestrictedSegs = true;
					}

					if (resSeg.getFareTO().getIsAllowModifyDate()) {
						allowModifyDate = true;
					}
					if (resSeg.getFareTO().getIsAllowModifyRoute()) {
						allowModifyRoute = true;
					}
				}
			}
		}

		statesTO.setCancelled((isCancelledRes || hasCancelledSegs));
		statesTO.setRestricted(isRestrictedSegs);
		statesTO.setFlown(hasFlownSegs);
		statesTO.setInBufferTime(isInBufferTime);
		statesTO.setHasFareRuleBuffer(hasFareRuleBuffer);
		statesTO.setHasNOSHOWPax(hasNOSHOWPax);
		statesTO.setSpecialFare(isSpecialFare);
		statesTO.setAllowModifySegDate(allowModifyDate);
		statesTO.setAllowModifySegRoute(allowModifyRoute);
		return statesTO;
	}

	private static boolean allPaxHasNOSHOW(Collection<ReservationPax> colReservationPax, Integer pnrSegId) {
		int i = 0;
		for (ReservationPax reservationPax : colReservationPax) {
			if (hasNoSHOWPax(reservationPax, pnrSegId)) {
				i++;
			}
		}

		if (i == colReservationPax.size()) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean hasNoSHOWPax(ReservationPax reservationPax, Integer pnrSegId) {
		Set<ReservationPaxFare> colResPaxFares = reservationPax.getPnrPaxFares();
		for (ReservationPaxFare reservationPaxFare : colResPaxFares) {
			Set<ReservationPaxFareSegment> colPaxFareSegments = reservationPaxFare.getPaxFareSegments();
			for (ReservationPaxFareSegment reservationPaxFareSegment : colPaxFareSegments) {
				if (reservationPaxFareSegment.getPnrSegId().equals(pnrSegId)
						&& reservationPaxFareSegment.getStatus()
								.equals(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Is interlined segments are Modifiable
	 * 
	 * @param colReservationSegmentDTO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isInterlinedSegmentsModifiable(Collection<ReservationSegmentDTO> colReservationSegmentDTO)
			throws ModuleException {
		return isInterlinedSegmentModifiable(null, colReservationSegmentDTO);
	}

	/**
	 * Is the interlined segments modifiable
	 * 
	 * @param states
	 * @param segmentIds
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isInterlinedSegmentsModifiable(Map<Integer, INTERLINE_STATES> states, Collection<Integer> segmentIds)
			throws ModuleException {
		Collection<Integer> notSureSegIds = new ArrayList<Integer>();
		INTERLINE_STATES tmpInterlineStatus;

		for (Integer pnrSegId : segmentIds) {
			tmpInterlineStatus = states.get(pnrSegId);

			if (tmpInterlineStatus == INTERLINE_STATES.FALSE) {
				return false;
			} else if (tmpInterlineStatus == INTERLINE_STATES.NOT_SURE) {
				notSureSegIds.add(pnrSegId);
			}
		}

		if (notSureSegIds.size() > 0) {
			if (SystemPropertyUtil.isManualSegmentTransferEnabled()) {
				SegmentBD segmentBD = ReservationModuleUtils.getSegmentBD();
				Map<Integer, ReservationSegmentTransfer> pnrSegAndExtSegs = segmentBD
						.getReservationTransferSegments(notSureSegIds);

				if (pnrSegAndExtSegs.size() > 0) {
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	/**
	 * Returns the interline segment modifiable status
	 * 
	 * @param flightNumber
	 * @param zuluDepatureDate
	 * @param segmentStatus
	 * @return
	 */
	private static INTERLINE_STATES
			isInterlinedSegmentModifiable(String flightNumber, Date zuluDepatureDate, String segmentStatus) {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		String carrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);

		if (globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE).equals(carrierCode)) {
			return INTERLINE_STATES.TRUE;
		} else {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segmentStatus)) {
				Map<String, InterlinedAirLineTO> interlinedMap = globalConfig.getActiveInterlinedCarriersMap();
				InterlinedAirLineTO interlinedAirLineTO;

				if (interlinedMap.containsKey(carrierCode)) {
					interlinedAirLineTO = (InterlinedAirLineTO) interlinedMap.get(carrierCode);

					Calendar cutOverCal = Calendar.getInstance();
					cutOverCal.setTime(zuluDepatureDate);
					cutOverCal.add(Calendar.MINUTE, -1 * interlinedAirLineTO.getInterlineCutOverInMinutes());

					Calendar currentCal = Calendar.getInstance();

					if (cutOverCal.after(currentCal)) {
						return INTERLINE_STATES.NOT_SURE;
					} else {
						return INTERLINE_STATES.FALSE;
					}
				}
				// If any other carrier codes exists in the marketing airline which is not defined in interline table
				// this will be treated as same as the marketing airline fight(s).
				else {
					return INTERLINE_STATES.TRUE;
				}
			} else {
				return INTERLINE_STATES.TRUE;
			}
		}
	}

	/**
	 * Is interlined segment is Modifiable
	 * 
	 * @param reservationSegmentDTO
	 * @param colReservationSegmentDTO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isInterlinedSegmentModifiable(ReservationSegmentDTO reservationSegmentDTO,
			Collection<ReservationSegmentDTO> colReservationSegmentDTO) throws ModuleException {

		Map<Integer, INTERLINE_STATES> states = new HashMap<Integer, INTERLINE_STATES>();
		Map<Integer, Collection<Integer>> ondGrps = new HashMap<Integer, Collection<Integer>>();
		ReservationSegmentDTO reservationSegmentDTOObj;

		for (Iterator<ReservationSegmentDTO> iter = colReservationSegmentDTO.iterator(); iter.hasNext();) {
			reservationSegmentDTOObj = (ReservationSegmentDTO) iter.next();
			states.put(
					reservationSegmentDTOObj.getPnrSegId(),
					isInterlinedSegmentModifiable(reservationSegmentDTOObj.getFlightNo(),
							reservationSegmentDTOObj.getZuluDepartureDate(), reservationSegmentDTOObj.getStatus()));

			if (ondGrps.containsKey(reservationSegmentDTOObj.getFareGroupId())) {
				Collection<Integer> segmentIds = ondGrps.get(reservationSegmentDTOObj.getFareGroupId());
				segmentIds.add(reservationSegmentDTOObj.getPnrSegId());
			} else {
				Collection<Integer> segmentIds = new ArrayList<Integer>();
				segmentIds.add(reservationSegmentDTOObj.getPnrSegId());

				ondGrps.put(reservationSegmentDTOObj.getFareGroupId(), segmentIds);
			}
		}

		// For a given segment
		if (reservationSegmentDTO != null) {
			INTERLINE_STATES interlineStatus = states.get(reservationSegmentDTO.getPnrSegId());

			if (interlineStatus == INTERLINE_STATES.TRUE || interlineStatus == INTERLINE_STATES.NOT_SURE) {
				return isInterlinedSegmentsModifiable(states, ondGrps.get(reservationSegmentDTO.getFareGroupId()));
			} else if (interlineStatus == INTERLINE_STATES.FALSE) {
				return false;
			} else {
				return true;
			}
			// For the whole reservation
		} else {
			return isInterlinedSegmentsModifiable(states, states.keySet());
		}
	}

	/**
	 * Returns the reservation modifiable till date time
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Date[]> getResModifiableTillDateTime(Reservation reservation) throws ModuleException {

		Map<Integer, Date[]> modifiableTillDateTimes = new HashMap<Integer, Date[]>();

		for (ReservationSegmentDTO resSegDTO : (Collection<ReservationSegmentDTO>) reservation.getSegmentsView()) {
			Date depDateZulu = resSegDTO.getZuluDepartureDate();

			ApplicableBufferTimeDTO applicableBufferDTO = FlightUtil.getApplicableModifyBufferTime(resSegDTO.getFareTO()
					.getInternationalModifyBufferTimeInMillis(), resSegDTO.getFareTO().getDomesticModifyBufferTimeInMillis(),
					resSegDTO.getFlightId());

			long modifyBufferTime = applicableBufferDTO.getApplicableBufferTime();

			long defaultModifyBufferTime = 0;

			List<Integer> pnrSegIdList = new ArrayList<Integer>();
			pnrSegIdList.add(resSegDTO.getPnrSegId());
			Object[] flexiParams = ReservationApiUtils.getFlexiModifyCutOverBuffer(reservation, pnrSegIdList);
			long flexiBufferTime = ((Long) flexiParams[0]).longValue();
			boolean isFlexiModificationAvail = ((Boolean) flexiParams[1]).booleanValue();
			flexiBufferTime = CalendarUtil.millisecs2Minutes(flexiBufferTime);

			// When Flexi fare applied modification time should be concidered according to the Flexi bufer time
			defaultModifyBufferTime = modifyBufferTime;
			if (flexiBufferTime > 0 && flexiBufferTime < modifyBufferTime) {
				modifyBufferTime = flexiBufferTime;
			}

			Calendar segModTillBufferDateTime = new GregorianCalendar();
			segModTillBufferDateTime.setTime(depDateZulu);
			segModTillBufferDateTime.add(Calendar.MINUTE, -CalendarUtil.millisecs2Minutes(modifyBufferTime));

			Calendar segModTillFlgClosureDateTime = new GregorianCalendar();
			segModTillFlgClosureDateTime.setTime(depDateZulu);
			segModTillFlgClosureDateTime.add(Calendar.MINUTE, -AppSysParamsUtil.getFlightClosureGapInMins());

			Calendar defaultSegModTillBufferDateTime = new GregorianCalendar();
			defaultSegModTillBufferDateTime.setTime(depDateZulu);
			defaultSegModTillBufferDateTime.add(Calendar.MINUTE, -CalendarUtil.millisecs2Minutes(defaultModifyBufferTime));

			Calendar segModTillDateTime = new GregorianCalendar();
			segModTillDateTime.setTime(defaultSegModTillBufferDateTime.getTime());

			if (isFlexiModificationAvail) {
				segModTillDateTime.setTime(segModTillBufferDateTime.getTime());
			}

			Date[] dates = new Date[] { segModTillDateTime.getTime(), segModTillFlgClosureDateTime.getTime(),
					segModTillBufferDateTime.getTime() };
			modifiableTillDateTimes.put(resSegDTO.getPnrSegId(), dates);
		}

		return modifiableTillDateTimes;
	}

}