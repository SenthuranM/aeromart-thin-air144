/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Chamindap
 */
public class AgentTotalSaledDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 134907286842949404L;

	private Timestamp invoiceDate;

	private Timestamp startDate;

	private Timestamp endDate;

	private int agentId;

	private String accountId;

	private int processStatus;

	private String agentCode;

	private String invoiceNumber;

	private BigDecimal totalSales = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalSalesLocal = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal settledAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private int billingPeriod;

	private String agentCommissionCode;

	private String currencyCode;

	private BigDecimal baseExchangeRate;

	private int tnxId;

	private String pnr;
	
	private String entityCode;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public int getTnxId() {
		return tnxId;
	}

	public void setTnxId(int tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * The constructor.
	 */
	public AgentTotalSaledDTO() {
		super();
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Timestamp getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Timestamp invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public int getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(int processStatus) {
		this.processStatus = processStatus;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getBillingPeriod() {
		return billingPeriod;
	}

	public void setBillingPeriod(int billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	/**
	 * @return Returns the settledAmount.
	 */
	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	/**
	 * @param settledAmount
	 *            The settledAmount to set.
	 */
	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

	/**
	 * @return Returns the totalSales.
	 */
	public BigDecimal getTotalSales() {
		return totalSales;
	}

	/**
	 * @param totalSales
	 *            The totalSales to set.
	 */
	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}

	public String getAgentCommissionCode() {
		return agentCommissionCode;
	}

	public void setAgentCommissionCode(String agentCommissionCode) {
		this.agentCommissionCode = agentCommissionCode;
	}

	public BigDecimal getBaseExchangeRate() {
		return baseExchangeRate;
	}

	public void setBaseExchangeRate(BigDecimal baseExchangeRate) {
		this.baseExchangeRate = baseExchangeRate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return Returns the totalSalesLocal.
	 */
	public BigDecimal getTotalSalesLocal() {
		return totalSalesLocal;
	}

	/**
	 * @param totalSalesLocal
	 *            The totalSalesLocal to set.
	 */
	public void setTotalSalesLocal(BigDecimal totalSalesLocal) {
		this.totalSalesLocal = totalSalesLocal;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
	
}
