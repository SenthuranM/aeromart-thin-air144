/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;

/**
 * To hold payment data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface PaymentInfo {
	/**
	 * Returns total amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalAmount();

	/**
	 * Set total amount
	 * 
	 * @param totalAmount
	 */
	public void setTotalAmount(BigDecimal totalAmount);

	/**
	 * Returns the PayCurrencyDTO
	 * 
	 * @return
	 */
	public PayCurrencyDTO getPayCurrencyDTO();

	/**
	 * Set the PayCurrencyDTO
	 * 
	 * @param payCurrencyDTO
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO);

	public String getPayCarrier();

	public void setPayCarrier(String payCarrier);
	
	/**
	 * paymentTnxId will be set only when the operation is a refund
	 * 
	 */
	public Integer getPaymentTnxId();
	
	public void setPaymentTnxId(Integer paymentTnxId);
	
}
