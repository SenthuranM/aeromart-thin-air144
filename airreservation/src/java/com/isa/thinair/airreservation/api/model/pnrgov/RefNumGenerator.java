package com.isa.thinair.airreservation.api.model.pnrgov;

import org.apache.commons.lang.RandomStringUtils;

public class RefNumGenerator {
	public String nextSessionID(int sessionIDLength) {
		return RandomStringUtils.randomNumeric(sessionIDLength);
	}
}
