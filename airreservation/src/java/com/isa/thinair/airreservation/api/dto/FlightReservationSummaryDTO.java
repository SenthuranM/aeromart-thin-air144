package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;

public class FlightReservationSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7724555703846478746L;

	private Integer flightId;

	private Collection<FlightSegmentReservationDTO> segmentReservations;

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public Collection<FlightSegmentReservationDTO> getSegmentReservations() {
		return segmentReservations;
	}

	public void setSegmentReservations(Collection<FlightSegmentReservationDTO> segmentReservations) {
		this.segmentReservations = segmentReservations;
	}

}
