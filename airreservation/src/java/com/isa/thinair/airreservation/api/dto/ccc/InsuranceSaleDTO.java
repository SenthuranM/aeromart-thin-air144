package com.isa.thinair.airreservation.api.dto.ccc;

import java.io.Serializable;
import java.math.BigDecimal;

public class InsuranceSaleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7992386502005906609L;

	private String status;

	private String pnr;

	private String natureOfContract;

	private String policyCode;

	private String vendor;

	private String travelDate;

	private String insBookingDate;

	private String departureDate;

	private String returnDate;

	private String countryCode;

	private String contactName;

	private int insType;

	private int paxCount;

	private String insurancePercentage;

	private BigDecimal totalTikectPrice;

	private BigDecimal amount;

	private String personName1;

	private String personName2;

	private String personName3;

	private String personName4;

	private String personName5;

	private String personName6;

	private String personName7;

	private String personName8;

	private String personName9;

	private String personName10;

	private boolean isReturn;

	public static final String CANCEL = "3";

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNatureOfContract() {
		return natureOfContract;
	}

	public void setNatureOfContract(String natureOfContract) {
		this.natureOfContract = natureOfContract;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public int getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

	public BigDecimal getTotalTikectPrice() {
		return totalTikectPrice;
	}

	public void setTotalTikectPrice(BigDecimal totalTikectPrice) {
		this.totalTikectPrice = totalTikectPrice;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPersonName1() {
		return personName1;
	}

	public void setPersonName1(String personName1) {
		this.personName1 = personName1;
	}

	public String getPersonName2() {
		return personName2;
	}

	public void setPersonName2(String personName2) {
		this.personName2 = personName2;
	}

	public String getPersonName3() {
		return personName3;
	}

	public void setPersonName3(String personName3) {
		this.personName3 = personName3;
	}

	public String getPersonName4() {
		return personName4;
	}

	public void setPersonName4(String personName4) {
		this.personName4 = personName4;
	}

	public String getPersonName5() {
		return personName5;
	}

	public void setPersonName5(String personName5) {
		this.personName5 = personName5;
	}

	public String getPersonName6() {
		return personName6;
	}

	public void setPersonName6(String personName6) {
		this.personName6 = personName6;
	}

	public String getPersonName7() {
		return personName7;
	}

	public void setPersonName7(String personName7) {
		this.personName7 = personName7;
	}

	public String getPersonName8() {
		return personName8;
	}

	public void setPersonName8(String personName8) {
		this.personName8 = personName8;
	}

	public String getPersonName9() {
		return personName9;
	}

	public void setPersonName9(String personName9) {
		this.personName9 = personName9;
	}

	public String getPersonName10() {
		return personName10;
	}

	public void setPersonName10(String personName10) {
		this.personName10 = personName10;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPnr() {
		return pnr;
	}

	public String getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}

	public String getInsBookingDate() {
		return insBookingDate;
	}

	public void setInsBookingDate(String insBookingDate) {
		this.insBookingDate = insBookingDate;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public int getInsType() {
		return insType;
	}

	public void setInsType(int insType) {
		this.insType = insType;
	}

	public String getInsurancePercentage() {
		return insurancePercentage;
	}

	public void setInsurancePercentage(String insurancePercentage) {
		this.insurancePercentage = insurancePercentage;
	}

	public boolean isReturn() {
		return isReturn;
	}

	public void setReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}
}
