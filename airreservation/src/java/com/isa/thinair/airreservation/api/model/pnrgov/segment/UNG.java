package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.BasicDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class UNG extends EDISegment {

	private String E0038;
	private String E0040;
	private String E0007_1;
	private String E0044;
	private String E0007_2;
	private String E0017;
	private String E0019;
	private String E0048;
	private String E0051;
	private String E0052;
	private String E0054;

	public UNG() {
		super(EDISegmentTag.UNG);
	}

	public void setFuntionalGroupIdentification(String e0038) {
		E0038 = e0038;
	}

	public void setApplicationSenderIdentification(String e0040) {
		E0040 = e0040;
	}

	public void setSenderIdentificationQualifier(String e0007_1) {
		E0007_1 = e0007_1;
	}

	public void setApplicationRecepientIdentification(String e0044) {
		E0044 = e0044;
	}

	public void setRecepientIdentificationQualifier(String e0007_2) {
		E0007_2 = e0007_2;
	}

	public void setDateOfPreparation(String e0017) {
		E0017 = e0017;
	}

	public void setTimeOfPreparation(String e0019) {
		E0019 = e0019;
	}

	public void setFuntionalGroupReferenceNumber(String e0048) {
		E0048 = e0048;
	}

	public void setControlingAgency(String e0051) {
		E0051 = e0051;
	}

	public void setMessageTypeVersionNumber(String e0052) {
		E0052 = e0052;
	}

	public void setMessageTypeReleaseNumber(String e0054) {
		E0054 = e0054;
	}

	@Override
	public MessageSegment build() {
		BasicDataElement V0038 = new AlphaNumeric(EDI_ELEMENT.E0038, E0038);

		CompoundDataElement S006 = new CompoundDataElement("S006", DE_STATUS.M);
		S006.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0040, E0040));
		S006.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0007, E0007_1));

		CompoundDataElement S007 = new CompoundDataElement("S007", DE_STATUS.M);
		S007.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0044, E0044));
		S007.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0007, E0007_2));

		CompoundDataElement S004 = new CompoundDataElement("S004", DE_STATUS.M);
		S004.addBasicDataelement(new Numeric(EDI_ELEMENT.E0017, E0017));
		S004.addBasicDataelement(new Numeric(EDI_ELEMENT.E0019, E0019));

		BasicDataElement V0048 = new AlphaNumeric(EDI_ELEMENT.E0048, E0048);
		BasicDataElement V0051 = new AlphaNumeric(EDI_ELEMENT.E0051, E0051);

		CompoundDataElement S008 = new CompoundDataElement("S008", DE_STATUS.M);
		S008.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0052, E0052));
		S008.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0054, E0054));

		addEDIDataElement(V0038);
		addEDIDataElement(S006);
		addEDIDataElement(S007);
		addEDIDataElement(S004);
		addEDIDataElement(V0048);
		addEDIDataElement(V0051);
		addEDIDataElement(S008);

		return this;
	}

}
