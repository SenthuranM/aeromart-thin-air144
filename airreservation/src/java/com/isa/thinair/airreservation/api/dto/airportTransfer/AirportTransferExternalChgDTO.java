package com.isa.thinair.airreservation.api.dto.airportTransfer;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

/**
 * To hold reservation external charges data transfer information
 * 
 * @author Manoj Dhanushka
 */
public class AirportTransferExternalChgDTO extends ExternalChgDTO {
	
	private static final long serialVersionUID = -3428414375607766976L;

	private Integer paxSequenceNo;

	private Integer flightAirportTransferId;

	private BigDecimal airportTransferCharge;

	private Integer pnrPaxId;

	private Integer pnrPaxFareId;

	private Integer pnrSegId;

	private Integer airportTransferId;

	private String flightRPH;

	private String airportTransferCode;
	
	private String airportCode;

	private Integer airportTransferChargeID;

	private Integer offeredTemplateID;
	
	/* Holds additional information which is required by airport transfer */
	private String transferDate;

	private String transferAddress;

	private String transferContact;
	
	private String transferType;
	
	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		AirportTransferExternalChgDTO clone = new AirportTransferExternalChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());

		clone.setPaxSequenceNo(this.getPaxSequenceNo());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setFlightAirportTransferId(this.getFlightAirportTransferId());
		clone.setAirportTransferCharge(this.getAirportTransferCharge());
		clone.setPnrPaxId(this.getPnrPaxId());
		clone.setPnrPaxFareId(this.getPnrPaxFareId());
		clone.setPnrSegId(this.getPnrSegId());
		clone.setAirportCode(this.getAirportCode());
		clone.setAirportTransferId(this.getAirportTransferId());
		clone.setFlightRPH(this.getFlightRPH());
		clone.setAirportTransferCode(this.getAirportTransferCode());
		clone.setAirportTransferChargeID(this.getAirportTransferChargeID());
		clone.setOfferedTemplateID(this.getOfferedTemplateID());
		clone.setTransferDate(this.getTransferDate());
		clone.setTransferAddress(this.getTransferAddress());
		clone.setTransferContact(this.getTransferContact());
		
		return clone;
	}

	public Integer getPaxSequenceNo() {
		return paxSequenceNo;
	}

	public Integer getFlightAirportTransferId() {
		return flightAirportTransferId;
	}

	public BigDecimal getAirportTransferCharge() {
		return airportTransferCharge;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public Integer getAirportTransferId() {
		return airportTransferId;
	}

	public String getFlightRPH() {
		return flightRPH;
	}

	public String getAirportTransferCode() {
		return airportTransferCode;
	}

	public Integer getAirportTransferChargeID() {
		return airportTransferChargeID;
	}

	public Integer getOfferedTemplateID() {
		return offeredTemplateID;
	}

	public void setPaxSequenceNo(Integer paxSequenceNo) {
		this.paxSequenceNo = paxSequenceNo;
	}

	public void setFlightAirportTransferId(Integer flightAirportTransferId) {
		this.flightAirportTransferId = flightAirportTransferId;
	}

	public void setAirportTransferCharge(BigDecimal airportTransferCharge) {
		this.airportTransferCharge = airportTransferCharge;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public void setAirportTransferId(Integer airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	public void setAirportTransferCode(String airportTransferCode) {
		this.airportTransferCode = airportTransferCode;
	}

	public void setAirportTransferChargeID(Integer airportTransferChargeID) {
		this.airportTransferChargeID = airportTransferChargeID;
	}

	public void setOfferedTemplateID(Integer offeredTemplateID) {
		this.offeredTemplateID = offeredTemplateID;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public String getTransferContact() {
		return transferContact;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

	public void setTransferContact(String transferContact) {
		this.transferContact = transferContact;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

}
