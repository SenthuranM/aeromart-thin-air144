package com.isa.thinair.airreservation.api.dto.ssr;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

public class SSRExternalChargeDTO extends ExternalChgDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer segmentSequece;

	private Integer sSRId;

	private String sSRText;

	private Integer sSRChargeId;

	private Integer paxID;

	private String flightRPH;

	private String applyOn;

	private String segmentCode;

	/* Holds the airport in which the airport service is available */
	private String airportCode;

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		SSRExternalChargeDTO clone = new SSRExternalChargeDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());

		clone.setSegmentSequece(this.getSegmentSequece());
		clone.setSSRId(this.getSSRId());
		clone.setSSRText(this.getSSRText());
		clone.setSSRChargeId(this.getSSRChargeId());
		clone.setPaxID(this.getPaxID());
		clone.setFlightRPH(this.getFlightRPH());
		clone.setApplyOn(this.getApplyOn());
		clone.setAirportCode(this.getAirportCode());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setSegmentCode(this.getSegmentCode());
		return clone;
	}

	public Integer getSegmentSequece() {
		return this.segmentSequece;
	}

	public void setSegmentSequece(Integer segmentSequene) {
		this.segmentSequece = segmentSequene;
	}

	public Integer getSSRId() {
		return this.sSRId;
	}

	public void setSSRId(Integer id) {
		this.sSRId = id;
	}

	public String getSSRText() {
		return this.sSRText;
	}

	public void setSSRText(String text) {
		this.sSRText = text;
	}

	public Integer getSSRChargeId() {
		return this.sSRChargeId;
	}

	public void setSSRChargeId(Integer chargeId) {
		this.sSRChargeId = chargeId;
	}

	public Integer getPaxID() {
		return this.paxID;
	}

	public void setPaxID(Integer paxID) {
		this.paxID = paxID;
	}

	/**
	 * @return Returns the flightRPH.
	 */
	public String getFlightRPH() {
		return flightRPH;
	}

	/**
	 * @param flightRPH
	 *            The flightSegmentID to set.
	 */
	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	public String getApplyOn() {
		return this.applyOn;
	}

	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
}
