package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ItineraryOndChargeTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ond;
	private List<ItineraryChargeTO> charges;
	private BigDecimal total;
	private boolean isTotalFormatted;

	public ItineraryOndChargeTO() {
		total = AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public String getOnd() {
		return ond;
	}

	public void setOnd(String ond) {
		this.ond = ond;
	}

	public List<ItineraryChargeTO> getCharges() {
		if (charges == null)
			charges = new ArrayList<ItineraryChargeTO>();
		return charges;
	}

	public void addCharge(ItineraryChargeTO charge, BigDecimal amount) {
		getCharges().add(charge);
		total = AccelAeroCalculator.add(total, amount);
	}

	public void setCharges(List<ItineraryChargeTO> charges) {
		this.charges = charges;
	}

	public String getTotal() {
		if (isTotalFormatted()) {
			return AccelAeroCalculator.formatAsDecimal(total);
		} else {
			return total.toString();
		}
	}

	public void setTotal(BigDecimal total, boolean isFormatted) {
		setTotalFormatted(isFormatted);
		this.total = total;
	}

	public boolean isTotalFormatted() {
		return isTotalFormatted;
	}

	public void setTotalFormatted(boolean isTotalFormatted) {
		this.isTotalFormatted = isTotalFormatted;
	}
}
