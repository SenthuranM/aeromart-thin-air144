/**
 * 
 */
package com.isa.thinair.airreservation.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Indika Athauda
 * 
 */
public class RecieptPaxDTO {

	private int totalAdultCount;
	private int totalChildCount;
	private int totalInfantCount;

	// --- Per Pax -----------
	private BigDecimal perAdultFare;
	private BigDecimal perChildFare;
	private BigDecimal perInfantFare;
	/*
	 * Per pax charge, ant per pax total cannot be used statically as different pax might got different charge based on
	 * charge adjustments
	 */

	// --- Pax Type -------------
	private BigDecimal AdultsTotalFare;
	private BigDecimal ChildsTotalFare;
	private BigDecimal InfantsTotalFare;
	private BigDecimal AdultsTotalCharge;
	private BigDecimal ChildsTotalCharge;
	private BigDecimal InfantsTotalCharge;
	private BigDecimal AdultsTotal;
	private BigDecimal ChildsTotal;
	private BigDecimal InfantsTotal;

	// ----- Reservation -----------
	private BigDecimal totalFare;
	private BigDecimal totalCharge;
	private BigDecimal totalInfant;
	private BigDecimal totalReservation;

	public String getPerAdultFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getPerAdultFare().doubleValue(), numberFormat);
	}

	public String getPerChildFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getPerChildFare().doubleValue(), numberFormat);
	}

	public String getPerInfantFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getPerInfantFare().doubleValue(), numberFormat);
	}

	public String getAdultsTotalFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getAdultsTotalFare().doubleValue(), numberFormat);
	}

	public String getChildsTotalFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getChildsTotalFare().doubleValue(), numberFormat);
	}

	public String getInfantsTotalFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getInfantsTotalFare().doubleValue(), numberFormat);
	}

	public String getAdultsTotalChargeAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getAdultsTotalCharge().doubleValue(), numberFormat);
	}

	public String getChildsTotalChargeAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getChildsTotalCharge().doubleValue(), numberFormat);
	}

	public String getInfantsTotalChargeAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getInfantsTotalCharge().doubleValue(), numberFormat);
	}

	public String getAdultsTotalAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getAdultsTotal().doubleValue(), numberFormat);
	}

	public String getChildsTotalAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getChildsTotal().doubleValue(), numberFormat);
	}

	public String getInfantsTotalAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getInfantsTotal().doubleValue(), numberFormat);
	}

	public String getTotalFareAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getTotalFare().doubleValue(), numberFormat);
	}

	public String getTotalChargeAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getTotalCharge().doubleValue(), numberFormat);
	}

	public String getTotalReservationAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getTotalReservation().doubleValue(), numberFormat);
	}

	/**
	 * @return the totalAdultCount
	 */
	public int getTotalAdultCount() {
		return totalAdultCount;
	}

	/**
	 * @param totalAdultCount
	 *            the totalAdultCount to set
	 */
	public void setTotalAdultCount(int totalAdultCount) {
		this.totalAdultCount = totalAdultCount;
	}

	/**
	 * @return the totalChildCount
	 */
	public int getTotalChildCount() {
		return totalChildCount;
	}

	/**
	 * @param totalChildCount
	 *            the totalChildCount to set
	 */
	public void setTotalChildCount(int totalChildCount) {
		this.totalChildCount = totalChildCount;
	}

	/**
	 * @return the totalInfantCount
	 */
	public int getTotalInfantCount() {
		return totalInfantCount;
	}

	/**
	 * @param totalInfantCount
	 *            the totalInfantCount to set
	 */
	public void setTotalInfantCount(int totalInfantCount) {
		this.totalInfantCount = totalInfantCount;
	}

	/**
	 * @return the perAdultFare
	 */
	public BigDecimal getPerAdultFare() {
		return perAdultFare;
	}

	/**
	 * @param perAdultFare
	 *            the perAdultFare to set
	 */
	public void setPerAdultFare(BigDecimal perAdultFare) {
		this.perAdultFare = perAdultFare;
	}

	/**
	 * @return the perChildFare
	 */
	public BigDecimal getPerChildFare() {
		return perChildFare;
	}

	/**
	 * @param perChildFare
	 *            the perChildFare to set
	 */
	public void setPerChildFare(BigDecimal perChildFare) {
		this.perChildFare = perChildFare;
	}

	/**
	 * @return the perInfantFare
	 */
	public BigDecimal getPerInfantFare() {
		return perInfantFare;
	}

	/**
	 * @param perInfantFare
	 *            the perInfantFare to set
	 */
	public void setPerInfantFare(BigDecimal perInfantFare) {
		this.perInfantFare = perInfantFare;
	}

	/**
	 * @return the adultsTotalFare
	 */
	public BigDecimal getAdultsTotalFare() {
		return AdultsTotalFare;
	}

	/**
	 * @param adultsTotalFare
	 *            the adultsTotalFare to set
	 */
	public void setAdultsTotalFare(BigDecimal adultsTotalFare) {
		AdultsTotalFare = adultsTotalFare;
	}

	/**
	 * @return the childsTotalFare
	 */
	public BigDecimal getChildsTotalFare() {
		return ChildsTotalFare;
	}

	/**
	 * @param childsTotalFare
	 *            the childsTotalFare to set
	 */
	public void setChildsTotalFare(BigDecimal childsTotalFare) {
		ChildsTotalFare = childsTotalFare;
	}

	/**
	 * @return the infantsTotalFare
	 */
	public BigDecimal getInfantsTotalFare() {
		return InfantsTotalFare;
	}

	/**
	 * @param infantsTotalFare
	 *            the infantsTotalFare to set
	 */
	public void setInfantsTotalFare(BigDecimal infantsTotalFare) {
		InfantsTotalFare = infantsTotalFare;
	}

	/**
	 * @return the adultsTotalCharge
	 */
	public BigDecimal getAdultsTotalCharge() {
		return AdultsTotalCharge;
	}

	/**
	 * @param adultsTotalCharge
	 *            the adultsTotalCharge to set
	 */
	public void setAdultsTotalCharge(BigDecimal adultsTotalCharge) {
		AdultsTotalCharge = adultsTotalCharge;
	}

	/**
	 * @return the childsTotalCharge
	 */
	public BigDecimal getChildsTotalCharge() {
		return ChildsTotalCharge;
	}

	/**
	 * @param childsTotalCharge
	 *            the childsTotalCharge to set
	 */
	public void setChildsTotalCharge(BigDecimal childsTotalCharge) {
		ChildsTotalCharge = childsTotalCharge;
	}

	/**
	 * @return the infantsTotalCharge
	 */
	public BigDecimal getInfantsTotalCharge() {
		return InfantsTotalCharge;
	}

	/**
	 * @param infantsTotalCharge
	 *            the infantsTotalCharge to set
	 */
	public void setInfantsTotalCharge(BigDecimal infantsTotalCharge) {
		InfantsTotalCharge = infantsTotalCharge;
	}

	/**
	 * @return the adultsTotal
	 */
	public BigDecimal getAdultsTotal() {
		return AdultsTotal;
	}

	/**
	 * @param adultsTotal
	 *            the adultsTotal to set
	 */
	public void setAdultsTotal(BigDecimal adultsTotal) {
		AdultsTotal = adultsTotal;
	}

	/**
	 * @return the childsTotal
	 */
	public BigDecimal getChildsTotal() {
		return ChildsTotal;
	}

	/**
	 * @param childsTotal
	 *            the childsTotal to set
	 */
	public void setChildsTotal(BigDecimal childsTotal) {
		ChildsTotal = childsTotal;
	}

	/**
	 * @return the infantsTotal
	 */
	public BigDecimal getInfantsTotal() {
		return InfantsTotal;
	}

	/**
	 * @param infantsTotal
	 *            the infantsTotal to set
	 */
	public void setInfantsTotal(BigDecimal infantsTotal) {
		InfantsTotal = infantsTotal;
	}

	/**
	 * @return the totalFare
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            the totalFare to set
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return the totalCharge
	 */
	public BigDecimal getTotalCharge() {
		return totalCharge;
	}

	/**
	 * @param totalCharge
	 *            the totalCharge to set
	 */
	public void setTotalCharge(BigDecimal totalCharge) {
		this.totalCharge = totalCharge;
	}

	/**
	 * @return the totalInfant
	 */
	public BigDecimal getTotalInfant() {
		return totalInfant;
	}

	/**
	 * @param totalInfant
	 *            the totalInfant to set
	 */
	public void setTotalInfant(BigDecimal totalInfant) {
		this.totalInfant = totalInfant;
	}

	/**
	 * @return the totalReservation
	 */
	public BigDecimal getTotalReservation() {
		return totalReservation;
	}

	/**
	 * @param totalReservation
	 *            the totalReservation to set
	 */
	public void setTotalReservation(BigDecimal totalReservation) {
		this.totalReservation = totalReservation;
	}

}
