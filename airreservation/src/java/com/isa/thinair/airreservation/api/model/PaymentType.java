/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCardType;

/**
 * @author Lasantha Pambagoda
 */
public class PaymentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int type;

	private PaymentType(int type) {
		this.type = type;
	}

	public String toString() {
		return String.valueOf(this.type);
	}

	public int getTypeValue() {
		return type;
	}

	public boolean equals(PaymentType paymentType) {
		return paymentType.toString().equals(String.valueOf(this.type));
	}

	public static final PaymentType CARD_MASTER = new PaymentType(1);
	public static final PaymentType CARD_VISA = new PaymentType(2);
	public static final PaymentType CARD_AMEX = new PaymentType(3);
	public static final PaymentType CARD_DINERS = new PaymentType(4);
	public static final PaymentType CARD_GENERIC = new PaymentType(5);
	public static final PaymentType CARD_CMI = new PaymentType(6);

	public static final PaymentType CREDIT = new PaymentType(7);
	public static final PaymentType CASH = new PaymentType(8);
	public static final PaymentType ON_ACCOUNT = new PaymentType(9);
	public static final PaymentType CREDIT_BF = new PaymentType(10);
	public static final PaymentType PAYPAL = new PaymentType(11);
	public static final PaymentType BSP = new PaymentType(12);
	public static final PaymentType CARD_DEBIT = new PaymentType(13);
	public static final PaymentType LOYALTY_PAYMENT = new PaymentType(14);
	public static final PaymentType VOUCHER = new PaymentType(15);
	public static final PaymentType OFFLINE = new PaymentType(16);

	/**
	 * Return the payment type description
	 * 
	 * @param paymentType
	 * @return
	 */
	public static String getPaymentTypeDesc(int intPaymentType) {
		String typeDesc = "";

		switch (intPaymentType) {
		case 1:
			typeDesc = "MASTER";
			break;
		case 2:
			typeDesc = "VISA";
			break;
		case 3:
			typeDesc = "AMEX";
			break;
		case 4:
			typeDesc = "DINERS";
			break;
		case 5:
			typeDesc = "GENERIC";
			break;
		case 6:
			typeDesc = "CMI";
			break;
		case 7:
			typeDesc = "CREDIT";
			break;
		case 8:
			typeDesc = "CASH";
			break;
		case 9:
			typeDesc = "ONACCOUNT";
			break;
		case 10:
			typeDesc = "CREDIT_BF";
			break;
		case 11:
			typeDesc = "PAYPAL";
			break;
		case 12:
			typeDesc = "BSP";
			break;
		case 13:
			typeDesc = "DEBIT";
			break;
		case 14:
			typeDesc = "LOYALTY";
			break;
		case 15:
			typeDesc = "VOUCHER";
			break;
		}

		return typeDesc;
	}

	/*
	 * This is used to get the card type in aaservices.
	 */
	public static int getPaymentType(PaymentCardType paymentCardType) {

		switch (paymentCardType) {
		case MASTER:
			return 1;
		case VISA:
			return 2;
		case AMERICAN_EXPRESS:
			return 3;
		case DINERS_CLUB:
			return 4;
		case GENERIC:
			return 5;
		case CMI:
			return 6;
		default:
			return -1;
		}
	}

	/**
	 * Returns the reservation tnx nominal code for the payment
	 * 
	 * @param intPaymentType
	 * @return
	 */
	public static ReservationTnxNominalCode getReservationTnxNominalCodeForPayment(int intPaymentType) {
		ReservationTnxNominalCode reservationTnxNominalCode = null;

		if (PaymentType.CARD_MASTER.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_MASTER;
		} else if (PaymentType.CARD_VISA.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_VISA;
		} else if (PaymentType.CARD_AMEX.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_AMEX;
		} else if (PaymentType.CARD_DINERS.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_DINERS;
		} else if (PaymentType.CARD_GENERIC.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_GENERIC;
		} else if (PaymentType.CARD_CMI.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_CMI;
		} else if (PaymentType.CREDIT.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CREDIT_BF;
		} else if (PaymentType.CASH.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CASH_PAYMENT;
		} else if (PaymentType.ON_ACCOUNT.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.ONACCOUNT_PAYMENT;
		} else if (PaymentType.CREDIT_BF.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CREDIT_BF;
		} else if (PaymentType.PAYPAL.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.PAYMENT_PAYPAL;
		} else if (PaymentType.CARD_DEBIT.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.CARD_PAYMENT_GENERIC_DEBIT;
		} else if (PaymentType.LOYALTY_PAYMENT.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.LOYALTY_PAYMENT;
		} else if (PaymentType.VOUCHER.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.VOUCHER_PAYMENT;
		}

		return reservationTnxNominalCode;
	}

	/**
	 * Returns the reservation tnx nominal code for the payment
	 * 
	 * @param intPaymentType
	 * @return
	 */
	public static ReservationTnxNominalCode getReservationTnxNominalCodeForRefund(int intPaymentType) {
		ReservationTnxNominalCode reservationTnxNominalCode = null;

		if (PaymentType.CARD_MASTER.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_MASTER;
		} else if (PaymentType.CARD_VISA.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_VISA;
		} else if (PaymentType.CARD_AMEX.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_AMEX;
		} else if (PaymentType.CARD_DINERS.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_DINERS;
		} else if (PaymentType.CARD_GENERIC.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_GENERIC;
		} else if (PaymentType.CARD_CMI.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_CMI;
		} else if (PaymentType.CASH.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_CASH;
		} else if (PaymentType.ON_ACCOUNT.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_ONACCOUNT;
		} else if (PaymentType.BSP.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_BSP;
		} else if (PaymentType.PAYPAL.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_PAYPAL;
		} else if (PaymentType.LOYALTY_PAYMENT.getTypeValue() == intPaymentType) {
			reservationTnxNominalCode = ReservationTnxNominalCode.REFUND_LOYALTY_PAYMENT;
		}

		return reservationTnxNominalCode;
	}
}
