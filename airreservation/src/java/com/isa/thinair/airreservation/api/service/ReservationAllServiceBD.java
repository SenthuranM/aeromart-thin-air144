package com.isa.thinair.airreservation.api.service;

public interface ReservationAllServiceBD extends ReservationBD, PassengerBD, ReservationQueryBD, SegmentBD, GroupBookingServiceBD, BlacklistPAXBD, ReprotectedPassengerBD {
}
