/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id $
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.util.Date;

/**
 * Holds the Transaction Credit Payment
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class TnxCreditPayment extends TnxPayment {

	private static final long serialVersionUID = 1L;

	/** Hold the carry forward reservation passenger Id */
	private String pnrPaxId;

	/** Holds the reservationPnr */
	private String pnr;

	/** Holds the agent which relate to the credit payment */
	private String agentCode;

	private Date expiryDate;

	/** holds credit payment related id. This is introduced to facilitate auto reversal functionality */
	private String paxCreditId;

	/**
	 * @return Returns the expiryDate.
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            The expiryDate to set.
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public String getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getPaxCreditId() {
		return paxCreditId;
	}

	public void setPaxCreditId(String paxCreditId) {
		this.paxCreditId = paxCreditId;
	}

}
