package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class MSG extends EDISegment {

	private String e4025;
	private String e1225;

	public MSG() {
		super(EDISegmentTag.MSG);
	}

	public void setBusinessFunction(String e4025) {
		this.e4025 = e4025;
	}

	public void setMessageFunction(String e1225) {
		this.e1225 = e1225;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C302 = new CompoundDataElement("C302", DE_STATUS.M);
		C302.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E4025, e4025));
		C302.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E1225, e1225));

		addEDIDataElement(C302);
		return this;
	}

}
