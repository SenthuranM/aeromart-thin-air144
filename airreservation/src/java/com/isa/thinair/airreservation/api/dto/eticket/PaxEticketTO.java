package com.isa.thinair.airreservation.api.dto.eticket;

import java.io.Serializable;

public class PaxEticketTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int eticketId;
	private int pnrPaxId;
	private String pnr;
	private String name;

	public int getEticketId() {
		return eticketId;
	}

	public void setEticketId(int eticketId) {
		this.eticketId = eticketId;
	}

	public int getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(int pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
