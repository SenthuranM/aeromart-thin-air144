package com.isa.thinair.airreservation.api.dto.baggage;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

/**
 * @author mano
 * @since May 10, 2011
 */
public class PaxBaggageTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer pkey;

	private Integer pnrPaxId;

	private Integer pnrSegId;

	private Integer pnrPaxFareId;

	private Integer selectedFlightBaggageId;

	private String status;

	private ExternalChgDTO chgDTO;

	private Integer baggageId;

	private String baggageName;

	private String paxName;

	private String paxBaggageSegCode;

	private String translatedBaggageDescription;

	private String baggageDescription;

	private Integer baggageChrgId;

	private String baggageOndGroupId;

	private Integer autoCancellationId;

	/**
	 * @return the pkey
	 */
	public Integer getPkey() {
		return pkey;
	}

	/**
	 * @param pkey
	 *            the pkey to set
	 */
	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the pnrPaxFareId
	 */
	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            the pnrPaxFareId to set
	 */
	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return the selectedFlightBaggageId
	 */
	public Integer getSelectedFlightBaggageId() {
		return selectedFlightBaggageId;
	}

	/**
	 * @param selectedFlightBaggageId
	 *            the selectedFlightBaggageId to set
	 */
	public void setSelectedFlightBaggageId(Integer selectedFlightBaggageId) {
		this.selectedFlightBaggageId = selectedFlightBaggageId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the chgDTO
	 */
	public ExternalChgDTO getChgDTO() {
		return chgDTO;
	}

	/**
	 * @param chgDTO
	 *            the chgDTO to set
	 */
	public void setChgDTO(ExternalChgDTO chgDTO) {
		this.chgDTO = chgDTO;
	}

	/**
	 * @return the baggageId
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param baggageId
	 *            the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the baggageName
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param baggageName
	 *            the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	/**
	 * @return the paxName
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            the paxName to set
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return the paxBaggageSegCode
	 */
	public String getPaxBaggageSegCode() {
		return paxBaggageSegCode;
	}

	/**
	 * @param paxBaggageSegCode
	 *            the paxBaggageSegCode to set
	 */
	public void setPaxBaggageSegCode(String paxBaggageSegCode) {
		this.paxBaggageSegCode = paxBaggageSegCode;
	}

	public String getTranslatedBaggageDescription() {
		return translatedBaggageDescription;
	}

	public void setTranslatedBaggageDescription(String translatedBaggageDescription) {
		this.translatedBaggageDescription = translatedBaggageDescription;
	}

	public String getBaggageDescription() {
		return baggageDescription;
	}

	public void setBaggageDescription(String baggageDescription) {
		this.baggageDescription = baggageDescription;
	}

	public Integer getBaggageChrgId() {
		return baggageChrgId;
	}

	public void setBaggageChrgId(Integer baggageChrgId) {
		this.baggageChrgId = baggageChrgId;
	}

	/**
	 * @return the baggageOndGroupId
	 */
	public String getBaggageOndGroupId() {
		return baggageOndGroupId;
	}

	/**
	 * @param baggageOndGroupId
	 *            the baggageOndGroupId to set
	 */
	public void setBaggageOndGroupId(String baggageOndGroupId) {
		this.baggageOndGroupId = baggageOndGroupId;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

}
