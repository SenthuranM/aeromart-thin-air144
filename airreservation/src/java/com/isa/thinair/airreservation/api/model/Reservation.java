/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.StationContactDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Reservation is the entity class to represent a reservation model This will keep track of main reservation information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_RESERVATION"
 */
public class Reservation extends Persistent implements Serializable {

	private static final long serialVersionUID = -3235555318274012591L;

	private static char ENABLE_TRANSACTION_GRANULARITY = 'Y';

	private static char DISABLE_TRANSACTION_GRANULARITY = 'N';

	// Reservation related
	/** Holds PNR Number */
	private String pnr;
	/** Holds the Originator PNR */
	private String originatorPnr;
	/** Holds the gds record locator */
	private String externalRecordLocator;
	/** Holds booking time stamp */
	private Date zuluBookingTimestamp;
	/** Holds the agent station's corresponding local time stamp */
	private Date localBookingTimestamp;
	/** Holds reservation status */
	private String status;

	/** Holds true if the reservation is made void */
	private char isVoidReservation = ReservationInternalConstants.VoidReservation.NO; // No by default

	private char infantPaymentSeparated = ReservationInternalConstants.InfantPaymentRecordedWithInfant.NO; // No by
																											// default

	// Contact and administrator related
	/** Holds contact information */
	private ReservationContactInfo contactInfo;
	/** Holds staff information */
	private ReservationAdminInfo adminInfo;
	/** Holds User Note */
	private String userNote;

	/** Holds User Note Type */
	private String userNoteType;

	// Segments related
	/** Holds a set of ReservationSegment */
	private Set<ReservationSegment> segments;
	/** Holds a Collection of ReservationSegmentDTO (Supports Read Only) */
	private Collection<ReservationSegmentDTO> segmentsView;

	private Set<ExternalPnrSegment> externalReservationSegment;

	private Set<OtherAirlineSegment> otherAirlineSegments;

	private Set<OperatingCarrierRecordLocator> operatingCarrierRecordLocators;

	// Passenger related
	/** Holds a set of ReservationPax */
	private Set<ReservationPax> passengers;

	/** Holds a set of PaxSeatTOs */
	private Collection<PaxSeatTO> seats;

	// Count related
	/** Holds total passenger count */
	private int totalPaxAdultCount;
	/** Holds total child count */
	private int totalPaxChildCount;
	/** Holds total infant count */
	private int totalPaxInfantCount;

	// Ticket Amount related
	/** Holds total ticket fare */
	private BigDecimal totalTicketFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket sur-charge */
	private BigDecimal totalTicketSurCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket tax-charge */
	private BigDecimal totalTicketTaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket cancel charge */
	private BigDecimal totalTicketCancelCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket modification charge */
	private BigDecimal totalTicketModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds total ticket adjustment charge */
	private BigDecimal totalTicketAdjustmentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger paid amount */
	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger available credit or debit amount */
	private BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalTicketModificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalGoquoAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Integer insuranceId;

	/** Holds reservation insurance */
	private List<ReservationInsurance> reservationInsurance;

	/** Holds the last currency code */
	private String lastCurrencyCode;

	/** Holds the last modification time stamp */
	private Date lastModificationTimestamp;

	/** Holds a set of PaxMealTOs */
	private Collection<PaxMealTO> meals;

	/** Holds a set of PaxMealTOs */
	private Collection<PaxAirportTransferTO> airportTransfers;

	/** Holds a set of PaxAutomaticCheckinTOs */
	private Collection<PaxAutomaticCheckinTO> autoCheckins;

	private Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> paxOndFlexibilities;

	/** Holds the flag for dummy reservation */
	private char dummyBooking;

	/** Holds the flag whether the reservation is allowed to be modified or not */
	private char modifiableReservation = ReservationInternalConstants.Modifiable.YES;

	/** Holds a set of PaxBaggageTOs */
	private Collection<PaxBaggageTO> baggages;

	private String endorsementNote;

	private String bookingCategory;

	private String originCountryOfCall;

	private String itineraryFareMaskFlag;

	private Date ticketValidTill;

	private Date minimumStayOver;

	private AutoCancellationInfo autoCancellationInfo;

	/** Holds applied promotion ID */
	private Long promotionId;

	private Integer promotionPaxCount = 0;

	/**
	 * Map containing refundable charge details for this reservation. Key -> pnrPaxID, Value -> collection of
	 * RefundableChargeDetailDTO containing the refundable charge amount data
	 */
	private Map<Integer, Collection<RefundableChargeDetailDTO>> refundableChargeDetails;

	/**
	 * Holds the flag to enable the transaction breakdown for this reservation. Reason to introduce this for reservation
	 * was to skip this for old bookings and disable per reservation tracking which is not affecting other critical
	 * functionality.
	 */
	private char enableTnxGranularity;

	private String fareDiscountCode = null;

	private Integer gdsId;

	private String externalPos;

	private Set<StationContactDTO> stationContactDTOs;

	private Long groupBookingRequestID;

	private Integer nameChangeCount = 0;

	private List<TaxInvoice> taxInvoicesList;

	private char useAeroMartETS = ReservationInternalConstants.UseAccelAeroETS.NO;
	
	/** Holds applied promo code*/
	private String promoCode;

	public Reservation(String pnr, String bookingCategory) {
		this.pnr = pnr;
		this.enableTnxGranularity = AppSysParamsUtil.isEnableTransactionGranularity()
				? ENABLE_TRANSACTION_GRANULARITY
				: DISABLE_TRANSACTION_GRANULARITY;
		this.bookingCategory = bookingCategory;
		this.itineraryFareMaskFlag = "N";
	}

	public Reservation() {
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.id column = "PNR" generator-class = "assigned"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the totalPaxAdultCount.
	 * @hibernate.property column = "TOTAL_PAX_COUNT"
	 */
	public int getTotalPaxAdultCount() {
		return totalPaxAdultCount;
	}

	/**
	 * @param totalPaxAdultCount
	 *            The totalPaxAdultCount to set.
	 */
	public void setTotalPaxAdultCount(int totalPaxAdultCount) {
		this.totalPaxAdultCount = totalPaxAdultCount;
	}

	/**
	 * @return Returns the totalPaxInfantCount.
	 * @hibernate.property column = "TOTAL_PAX_INFANT_COUNT"
	 */
	public int getTotalPaxInfantCount() {
		return totalPaxInfantCount;
	}

	/**
	 * @param totalPaxInfantCount
	 *            The totalPaxInfantCount to set.
	 */
	public void setTotalPaxInfantCount(int totalPaxInfantCount) {
		this.totalPaxInfantCount = totalPaxInfantCount;
	}

	/**
	 * @return Returns the totalTicketFare.
	 * @hibernate.property column = "TOTAL_FARE"
	 */
	public BigDecimal getTotalTicketFare() {
		return totalTicketFare;
	}

	/**
	 * @param totalTicketFare
	 *            The totalTicketFare to set.
	 */
	public void setTotalTicketFare(BigDecimal totalTicketFare) {
		this.totalTicketFare = totalTicketFare;
	}

	/**
	 * @return Returns the adminInfo.
	 * @hibernate.component class="com.isa.thinair.airreservation.api.model.ReservationAdminInfo"
	 */
	public ReservationAdminInfo getAdminInfo() {
		return adminInfo;
	}

	/**
	 * @param adminInfo
	 *            The adminInfo to set.
	 */
	public void setAdminInfo(ReservationAdminInfo adminInfo) {
		this.adminInfo = adminInfo;
	}

	/**
	 * @return Returns the contactInfo.
	 * @hibernate.one-to-one class="com.isa.thinair.airreservation.api.model.ReservationContactInfo" property=PNR
	 *                       constrained="true" cascade="all"
	 */
	public ReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            The contactInfo to set.
	 */
	public void setContactInfo(ReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return Returns the segments.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="SEGMENT_SEQ" inverse="true"
	 * @hibernate.collection-key column="PNR"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationSegment"
	 */
	public Set<ReservationSegment> getSegments() {
		return segments;
	}

	/**
	 * Returns whether or not any cancel segments exists
	 */
	public boolean isAnyCancelSegmentsExists() {
		if (getSegments() != null && getSegments().size() > 0) {
			for (ReservationSegment reservationSegment : getSegments()) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @param segments
	 *            The segments to set.
	 */
	private void setSegments(Set<ReservationSegment> segments) {
		this.segments = segments;
	}

	/**
	 * Add reservation segment
	 * 
	 * @param reservationSegment
	 */
	public void addSegment(ReservationSegment reservationSegment) {
		if (this.getSegments() == null) {
			this.setSegments(new HashSet<ReservationSegment>());
		}

		reservationSegment.setReservation(this);
		this.getSegments().add(reservationSegment);
	}

	public Collection<Integer> getPnrSegIds() {
		Collection<Integer> pnrSegIds = new HashSet<Integer>();

		for (Iterator<ReservationSegment> itReservationSegment = getSegments().iterator(); itReservationSegment.hasNext();) {
			ReservationSegment reservationSegment = (ReservationSegment) itReservationSegment.next();
			pnrSegIds.add(reservationSegment.getPnrSegId());
		}

		return pnrSegIds;
	}

	/**
	 * @return the externalReservationSegment
	 * @return Returns the segments.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="SEGMENT_SEQ" inverse="true"
	 * @hibernate.collection-key column="PNR"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ExternalPnrSegment"
	 */
	public Set<ExternalPnrSegment> getExternalReservationSegment() {
		return externalReservationSegment;
	}

	public void addExternalReservationSegment(ExternalPnrSegment externalPnrSegment) {

		if (this.getExternalReservationSegment() == null) {
			this.setExternalReservationSegment(new HashSet<ExternalPnrSegment>());
		}

		externalPnrSegment.setReservation(this);
		this.getExternalReservationSegment().add(externalPnrSegment);
	}

	/**
	 * @param externalReservationSegment
	 *            the externalReservationSegment to set
	 */
	private void setExternalReservationSegment(Set<ExternalPnrSegment> externalReservationSegment) {
		this.externalReservationSegment = externalReservationSegment;
	}

	/**
	 * @param passengers
	 *            The passengers to set.
	 */
	private void setPassengers(Set<ReservationPax> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return Returns the passengers.
	 * @hibernate.set lazy="false" cascade="all" order-by="PAX_SEQUENCE" inverse="true"
	 * @hibernate.collection-key column="PNR"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPax"
	 */
	public Set<ReservationPax> getPassengers() {
		return passengers;
	}

	public Collection<Integer> getPnrPaxIds() {
		Collection<Integer> pnrPaxIds = new HashSet<Integer>();

		for (Iterator<ReservationPax> itReservationPax = getPassengers().iterator(); itReservationPax.hasNext();) {
			ReservationPax reservationPax = (ReservationPax) itReservationPax.next();
			pnrPaxIds.add(reservationPax.getPnrPaxId());
		}

		return pnrPaxIds;
	}

	/**
	 * Adds a passenger
	 * 
	 * @param reservationPax
	 */
	public void addPassenger(ReservationPax reservationPax) {
		if (this.getPassengers() == null) {
			this.setPassengers(new HashSet<ReservationPax>());
		}

		reservationPax.setReservation(this);
		this.getPassengers().add(reservationPax);
	}

	/**
	 * Remove a passenger
	 * 
	 * @param reservationPax
	 */
	public void removePassenger(ReservationPax reservationPax) {
		if (this.getPassengers() != null) {
			reservationPax.setReservation(null);
			this.getPassengers().remove(reservationPax);
		}
	}

	/**
	 * Remove a segment
	 * 
	 * @param reservationSegment
	 */
	public void removeSegment(ReservationSegment reservationSegment) {
		if (this.getSegments() != null) {
			reservationSegment.setReservation(null);
			this.getSegments().remove(reservationSegment);
		}
	}

	/**
	 * @return Returns the segmentsView.
	 */
	public Collection<ReservationSegmentDTO> getSegmentsView() {
		return segmentsView;
	}

	/**
	 * @param segmentsView
	 *            The segmentsView to set.
	 */
	public void setSegmentsView(Collection<ReservationSegmentDTO> segmentsView) {
		this.segmentsView = segmentsView;
	}

	/**
	 * @return Returns the totalAvailableBalance.
	 */
	public BigDecimal getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	/**
	 * @param totalAvailableBalance
	 *            The totalAvailableBalance to set.
	 */
	public void setTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}

	/**
	 * @return Returns the totalTicketSurCharge.
	 * @hibernate.property column = "TOTAL_SURCHARGES"
	 */
	public BigDecimal getTotalTicketSurCharge() {
		return totalTicketSurCharge;
	}

	/**
	 * @param totalTicketSurCharge
	 *            The totalTicketSurCharge to set.
	 */
	public void setTotalTicketSurCharge(BigDecimal totalTicketSurCharge) {
		this.totalTicketSurCharge = totalTicketSurCharge;
	}

	/**
	 * @return Returns the totalTicketTaxCharge.
	 * @hibernate.property column = "TOTAL_TAX"
	 */
	public BigDecimal getTotalTicketTaxCharge() {
		return totalTicketTaxCharge;
	}

	/**
	 * @param totalTicketTaxCharge
	 *            The totalTicketTaxCharge to set.
	 */
	public void setTotalTicketTaxCharge(BigDecimal totalTicketTaxCharge) {
		this.totalTicketTaxCharge = totalTicketTaxCharge;
	}

	/**
	 * @return Returns the totalTicketAdjustmentCharge.
	 * @hibernate.property column = "TOTAL_ADJ"
	 */
	public BigDecimal getTotalTicketAdjustmentCharge() {
		return totalTicketAdjustmentCharge;
	}

	/**
	 * @param totalTicketAdjustmentCharge
	 *            The totalTicketAdjustmentCharge to set.
	 */
	public void setTotalTicketAdjustmentCharge(BigDecimal totalTicketAdjustmentCharge) {
		this.totalTicketAdjustmentCharge = totalTicketAdjustmentCharge;
	}

	/**
	 * @return Returns the totalTicketCancelCharge.
	 * @hibernate.property column = "TOTAL_CNX"
	 */
	public BigDecimal getTotalTicketCancelCharge() {
		return totalTicketCancelCharge;
	}

	/**
	 * @param totalTicketCancelCharge
	 *            The totalTicketCancelCharge to set.
	 */
	public void setTotalTicketCancelCharge(BigDecimal totalTicketCancelCharge) {
		this.totalTicketCancelCharge = totalTicketCancelCharge;
	}

	/**
	 * @return Returns the totalTicketModificationCharge.
	 * @hibernate.property column = "TOTAL_MOD"
	 */
	public BigDecimal getTotalTicketModificationCharge() {
		return totalTicketModificationCharge;
	}

	/**
	 * @param totalTicketModificationCharge
	 *            The totalTicketModificationCharge to set.
	 */
	public void setTotalTicketModificationCharge(BigDecimal totalTicketModificationCharge) {
		this.totalTicketModificationCharge = totalTicketModificationCharge;
	}

	public void setTotalTicketModificationPenalty(BigDecimal totalTicketModificationPenalty) {
		this.totalTicketModificationPenalty = totalTicketModificationPenalty;
	}

	/**
	 * @return Returns the totalPenaltyCharge
	 * @hibernate.property column = "TOTAL_PEN"
	 */
	public BigDecimal getTotalTicketModificationPenalty() {
		return totalTicketModificationPenalty;
	}

	/**
	 * Return total charge amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalChargeAmount() {
		return AccelAeroCalculator.add(this.getTotalTicketFare(), this.getTotalTicketTaxCharge(), this.getTotalTicketSurCharge(),
				this.getTotalTicketCancelCharge(), this.getTotalTicketModificationCharge(), this.getTotalTicketAdjustmentCharge(),
				this.getTotalTicketModificationPenalty(), this.getTotalDiscount().negate());
	}

	/**
	 * Return total charge amounts
	 * 
	 * @return
	 */
	public BigDecimal[] getTotalChargeAmounts() {
		return new BigDecimal[] { this.getTotalTicketFare(), this.getTotalTicketTaxCharge(), this.getTotalTicketSurCharge(),
				this.getTotalTicketCancelCharge(), this.getTotalTicketModificationCharge(), this.getTotalTicketAdjustmentCharge(),
				this.getTotalTicketModificationPenalty(), this.getTotalDiscount().negate() };
	}

	/**
	 * Set total charge amounts
	 * 
	 * @param totalCharges
	 */
	public void setTotalChargeAmounts(BigDecimal[] totalChargeAmounts) {
		this.setTotalTicketFare(totalChargeAmounts[0]);
		this.setTotalTicketTaxCharge(totalChargeAmounts[1]);
		this.setTotalTicketSurCharge(totalChargeAmounts[2]);
		this.setTotalTicketCancelCharge(totalChargeAmounts[3]);
		this.setTotalTicketModificationCharge(totalChargeAmounts[4]);
		this.setTotalTicketAdjustmentCharge(totalChargeAmounts[5]);
		this.setTotalTicketModificationPenalty(totalChargeAmounts[6]);
		BigDecimal discount = totalChargeAmounts[7];
		if (discount.doubleValue() < 0)
			discount = discount.negate();
		this.setTotalDiscount(discount);
	}

	/**
	 * @return Returns the totalPaidAmount.
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            The totalPaidAmount to set.
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnr()).toHashCode();
	}

	/**
	 * @return Returns the userNote.
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote
	 *            The userNote to set.
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	/**
	 * @return Returns the localBookingTimestamp.
	 */
	public Date getLocalBookingTimestamp() {
		return localBookingTimestamp;
	}

	/**
	 * @param localBookingTimestamp
	 *            The localBookingTimestamp to set.
	 */
	public void setLocalBookingTimestamp(Date localBookingTimestamp) {
		this.localBookingTimestamp = localBookingTimestamp;
	}

	/**
	 * @return Returns the zuluBookingTimestamp.
	 * @hibernate.property column = "BOOKING_TIMESTAMP"
	 */
	public Date getZuluBookingTimestamp() {
		return zuluBookingTimestamp;
	}

	/**
	 * @param zuluBookingTimestamp
	 *            The zuluBookingTimestamp to set.
	 */
	public void setZuluBookingTimestamp(Date zuluBookingTimestamp) {
		this.zuluBookingTimestamp = zuluBookingTimestamp;
	}

	/**
	 * Returns the release time stamp
	 * 
	 * @param dateFormat
	 * @param displayConversion
	 * @return
	 */
	public String getReleaseTimeStampString(String dateFormat, boolean displayConversion) {
		ReservationPax reservationPax = this.getReservationReleaseTimeStamp();

		if (reservationPax != null) {
			return reservationPax.getReleaseTimeStampString(dateFormat, displayConversion);
		} else {
			return "";
		}
	}

	/**
	 * Returns the release time date Object
	 *
	 * @return
	 */
	public Date getZuluReleaseTime() {
		ReservationPax reservationPax = this.getReservationReleaseTimeStamp();
		if (reservationPax != null) {
			return reservationPax.getZuluReleaseTimeStamp();
		}
		return null;
	}

	/**
	 * Return the zulu release time stamp of the reservation
	 * * @return {@link Date}
	 */
	public Date getZuluReleaseTimeStamp() {
		ReservationPax reservationPax = this.getReservationReleaseTimeStamp();

		if (reservationPax != null) {
			return reservationPax.getZuluReleaseTimeStamp();
		}
		return null;
	}

	/**
	 * Returns release time stamps
	 * 
	 * @return Date[0] --> Zulu Release Time Stamp --> Local Release Time Stamp
	 */
	public Date[] getReleaseTimeStamps() {
		ReservationPax reservationPax = this.getReservationReleaseTimeStamp();

		if (reservationPax != null) {
			return new Date[] { reservationPax.getZuluReleaseTimeStamp(), reservationPax.getLocalReleaseTimeStamp() };
		} else {
			return new Date[] { null, null };
		}
	}

	/**
	 * Returns the reservation release time stamp
	 * 
	 * @return
	 */
	private ReservationPax getReservationReleaseTimeStamp() {
		Iterator<ReservationPax> itReservationPax = this.getPassengers().iterator();
		ReservationPax reservationPax;

		// If it an adult or a parent
		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				return reservationPax;
			}
		}

		itReservationPax = this.getPassengers().iterator();
		// No valid adult or parent exist then taking the infant release time stamp
		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (ReservationApiUtils.isInfantType(reservationPax)) {
				return reservationPax;
			}
		}

		// Practically this can not happen.
		// Thus a reservation can not be there with out passengers
		return null;
	}

	/**
	 * @return Returns the totalPaxChildCount.
	 * @hibernate.property column = "TOTAL_PAX_CHILD_COUNT"
	 */
	public int getTotalPaxChildCount() {
		return totalPaxChildCount;
	}

	/**
	 * @param totalPaxChildCount
	 *            The totalPaxChildCount to set.
	 */
	public void setTotalPaxChildCount(int totalPaxChildCount) {
		this.totalPaxChildCount = totalPaxChildCount;
	}

	public Collection<PaxSeatTO> getSeats() {
		return seats;
	}

	public void setSeats(Collection<PaxSeatTO> seats) {
		this.seats = seats;
	}

	public Collection<PaxSeatTO> getSeats(Integer pnrPaxId) {
		Collection<PaxSeatTO> paxSeatsTOs = new ArrayList<PaxSeatTO>();

		if (this.getSeats() != null && this.getSeats().size() > 0) {

			for (Iterator<PaxSeatTO> iterator = this.getSeats().iterator(); iterator.hasNext();) {
				PaxSeatTO paxSeatTO = (PaxSeatTO) iterator.next();
				if (pnrPaxId.equals(paxSeatTO.getPnrPaxId())) {
					paxSeatsTOs.add(paxSeatTO);
				}
			}
		}

		return paxSeatsTOs;
	}

	public List<ReservationInsurance> getReservationInsurance() {
		if (reservationInsurance == null) {
			setReservationInsurance(new ArrayList<ReservationInsurance>());
		}
		return reservationInsurance;
	}

	public void setReservationInsurance(List<ReservationInsurance> reservationInsurance) {
		this.reservationInsurance = reservationInsurance;
	}

	public void addReservationInsurance(ReservationInsurance reservationInsurance) {
		this.getReservationInsurance().add(reservationInsurance);
	}

	/**
	 * @return the insuranceId
	 * @hibernate.property column="INS_ID"
	 */
	public Integer getInsuranceId() {
		return insuranceId;
	}

	/**
	 * @param insuranceId
	 *            the insuranceId to set
	 */
	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}

	/**
	 * @return the originatorPnr
	 * @hibernate.property column = "ORIGINATOR_PNR"
	 */
	public String getOriginatorPnr() {
		return originatorPnr;
	}

	/**
	 * @param originatorPnr
	 *            the originatorPnr to set
	 */
	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	/**
	 * return the gdsRecordLocator
	 * 
	 * @hibernate.property column = "EXTERNAL_REC_LOCATOR"
	 */
	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	/**
	 * @param gdsRecordLocator
	 *            the gdsRecordLocator to set
	 */
	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	/**
	 * @return the externalPos
	 * @hibernate.property column = "EXTERNAL_POS"
	 */
	public String getExternalPos() {
		return externalPos;
	}

	/**
	 * @param externalPos
	 *            the externalPos to set
	 */
	public void setExternalPos(String externalPos) {
		this.externalPos = externalPos;
	}

	/**
	 * @return the lastModificationTimestamp
	 * @hibernate.property column = "OHD_LAST_MODIFICATION"
	 */
	public Date getLastModificationTimestamp() {
		return lastModificationTimestamp;
	}

	/**
	 * @param lastModificationTimestamp
	 *            the lastModificationTimestamp to set
	 */
	public void setLastModificationTimestamp(Date lastModificationTimestamp) {
		this.lastModificationTimestamp = lastModificationTimestamp;
	}

	/**
	 * @return the lastCurrencyCode
	 * @hibernate.property column = "OHD_CURRENCY_CODE"
	 */
	public String getLastCurrencyCode() {
		return lastCurrencyCode;
	}

	/**
	 * @param lastCurrencyCode
	 *            the lastCurrencyCode to set
	 */
	public void setLastCurrencyCode(String lastCurrencyCode) {
		this.lastCurrencyCode = lastCurrencyCode;
	}

	/**
	 * @return the dummyBooking
	 * @hibernate.property column = "DUMMY_BOOKING"
	 */
	public char getDummyBooking() {
		return dummyBooking;
	}

	/**
	 * @param dummyBooking
	 *            the dummyBooking to set
	 */
	public void setDummyBooking(char dummyBooking) {
		this.dummyBooking = dummyBooking;
	}

	/**
	 * Gets the collection of MealTos
	 * 
	 * @return Collection the MealTos
	 */
	public Collection<PaxMealTO> getMeals() {
		return meals;
	}

	/**
	 * Sets the meal TOs
	 * 
	 * @param meals
	 *            the mealTOs
	 */
	public void setMeals(Collection<PaxMealTO> meals) {
		this.meals = meals;
	}

	public Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> getPaxOndFlexibilities() {
		return paxOndFlexibilities;
	}

	public void setPaxOndFlexibilities(Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> paxOndFlexibilities) {
		this.paxOndFlexibilities = paxOndFlexibilities;
	}

	/**
	 * @return the baggages
	 */
	public Collection<PaxBaggageTO> getBaggages() {
		return baggages;
	}

	/**
	 * @param baggages
	 *            the baggages to set
	 */
	public void setBaggages(Collection<PaxBaggageTO> baggages) {
		this.baggages = baggages;
	}

	/**
	 * @return the lastModificationTimestamp
	 * @hibernate.property column = "ENABLE_TRANSACTION_GRANULARITY"
	 */
	public char getEnableTnxGranularity() {
		return enableTnxGranularity;
	}

	public void setEnableTnxGranularity(char enableTnxGranularity) {
		this.enableTnxGranularity = enableTnxGranularity;
	}

	public boolean isEnableTransactionGranularity() {
		if (ENABLE_TRANSACTION_GRANULARITY == getEnableTnxGranularity())
			return true;
		else
			return false;
	}

	public void setEnableTransactionGranularity(boolean enableTransactionGranularity) {
		if (enableTransactionGranularity)
			setEnableTnxGranularity(ENABLE_TRANSACTION_GRANULARITY);
		else
			setEnableTnxGranularity(DISABLE_TRANSACTION_GRANULARITY);
	}

	/**
	 * @return the endorsementNote
	 * @hibernate.property column = "ENDORSEMENTS"
	 */
	public String getEndorsementNote() {
		return endorsementNote;
	}

	/**
	 * @param endorsementNote
	 *            the endorsementNote to set
	 */
	public void setEndorsementNote(String endorsementNote) {
		this.endorsementNote = endorsementNote;
	}

	/**
	 * @return
	 * @hibernate.property column = "BOOKING_CATEGORY_CODE"
	 */
	public String getBookingCategory() {
		return bookingCategory;
	}

	/**
	 * @param bookingCategory
	 */
	public void setBookingCategory(String bookingCategory) {
		this.bookingCategory = bookingCategory;
	}

	/**
	 * @return
	 * @hibernate.property column = "ENABLE_ITINERARY_FARE_MASK"
	 */
	public String getItineraryFareMaskFlag() {
		return itineraryFareMaskFlag;
	}

	/**
	 * @param itineraryFareMaskFlag
	 */
	public void setItineraryFareMaskFlag(String itineraryFareMaskFlag) {
		this.itineraryFareMaskFlag = itineraryFareMaskFlag;
	}

	/**
	 * @return the modifiableReservation
	 * @hibernate.property column = "MODIFIABLE"
	 */
	public char getModifiableReservation() {
		return modifiableReservation;
	}

	/**
	 * @param modifiableReservation
	 *            the modifiableReservation to set
	 */
	public void setModifiableReservation(char modifiableReservation) {
		this.modifiableReservation = modifiableReservation;
	}

	/**
	 * @return the ticketValidTill
	 * @hibernate.property column = "TICKET_VALIDITY"
	 */
	@Deprecated
	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	/**
	 * @param ticketValidTill
	 *            the ticketValidTill to set
	 */
	@Deprecated
	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	/**
	 * @return the minimumStayOver
	 * @hibernate.property column = "MINIMUM_STAY_OVER"
	 */
	@Deprecated
	public Date getMinimumStayOver() {
		return minimumStayOver;
	}

	/**
	 * @param minimumStayOver
	 *            the minimumStayOver to set
	 */
	@Deprecated
	public void setMinimumStayOver(Date minimumStayOver) {
		this.minimumStayOver = minimumStayOver;
	}

	/**
	 * 
	 * @param isVoidReservation
	 */
	public void setIsVoidReservation(char isVoidReservation) {
		this.isVoidReservation = isVoidReservation;
	}

	/**
	 * @hibernate.property column = "VOID_RESERVATION"
	 * @return Y if the reservation is marked Void
	 */
	public char getIsVoidReservation() {
		return isVoidReservation;
	}

	public boolean isVoidReservation() {
		return ReservationInternalConstants.VoidReservation.YES == isVoidReservation;
	}

	public Map<Integer, Collection<RefundableChargeDetailDTO>> getRefundableChargeDetails() {
		return refundableChargeDetails;
	}

	public void setRefundableChargeDetails(Map<Integer, Collection<RefundableChargeDetailDTO>> refundableChargeDetails) {
		this.refundableChargeDetails = refundableChargeDetails;
	}

	/**
	 * @return
	 * @hibernate.property column = "FARE_DISCOUNT_CODE"
	 */
	public String getFareDiscountCode() {
		return fareDiscountCode;
	}

	/**
	 * 
	 * @param fareDiscountCode
	 */
	public void setFareDiscountCode(String fareDiscountCode) {
		this.fareDiscountCode = fareDiscountCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "TOTAL_DISCOUNT"
	 */
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalFareDiscount) {

		if (totalFareDiscount.doubleValue() < 0)
			totalFareDiscount = totalFareDiscount.negate();

		this.totalDiscount = totalFareDiscount;
	}

	/**
	 * @return the gdsId
	 * @hibernate.property column = "GDS_ID"
	 */
	public Integer getGdsId() {
		return gdsId;
	}

	/**
	 * @param gdsId
	 *            the gdsId to set
	 */
	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public AutoCancellationInfo getAutoCancellationInfo() {
		return autoCancellationInfo;
	}

	public void setAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		this.autoCancellationInfo = autoCancellationInfo;
	}

	/**
	 * @param stations
	 *            the stations to set
	 */
	public void setStations(Set<StationContactDTO> stations) {
		this.stationContactDTOs = stations;
	}

	/**
	 * @return the stations
	 */
	public Set<StationContactDTO> getStations() {
		return stationContactDTOs;
	}

	/**
	 * 
	 * @return
	 * 
	 * @hibernate.property column = "PROMO_CRITERIA_ID"
	 */
	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	/**
	 * @return the promotionPaxCount
	 * @hibernate.property column = "PROMO_PAX_COUNT"
	 */
	public Integer getPromotionPaxCount() {
		return promotionPaxCount;
	}

	/**
	 * @param promotionPaxCount
	 *            the promotionPaxCount to set
	 */
	public void setPromotionPaxCount(Integer promotionPaxCount) {
		this.promotionPaxCount = promotionPaxCount;
	}

	/**
	 * @return the groupBookingRequestID
	 * @hibernate.property column = "GROUP_BOOKING_REQUEST_ID"
	 */
	public Long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	/**
	 * @param groupBookingRequestID
	 *            the groupBookingRequestID to set
	 */
	public void setGroupBookingRequestID(Long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	public Collection<PaxAirportTransferTO> getAirportTransfers() {
		return airportTransfers;
	}

	public void setAirportTransfers(Collection<PaxAirportTransferTO> airportTransfers) {
		this.airportTransfers = airportTransfers;
	}

	/**
	 * @return the autoCheckins
	 */
	public Collection<PaxAutomaticCheckinTO> getAutoCheckins() {
		return autoCheckins;
	}

	/**
	 * @param autoCheckins
	 *            the autoCheckins to set
	 */
	public void setAutoCheckins(Collection<PaxAutomaticCheckinTO> autoCheckins) {
		this.autoCheckins = autoCheckins;
	}

	/**
	 * @return the nameChangeCount
	 * @hibernate.property column = "NAME_CHANGE_COUNT"
	 */
	public Integer getNameChangeCount() {
		return nameChangeCount;
	}

	public void setNameChangeCount(Integer nameChangeCount) {
		this.nameChangeCount = nameChangeCount;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PNR"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.OtherAirlineSegment"
	 */
	public Set<OtherAirlineSegment> getOtherAirlineSegments() {
		return otherAirlineSegments;
	}

	public void setOtherAirlineSegments(Set<OtherAirlineSegment> otherAirlineSegments) {
		this.otherAirlineSegments = otherAirlineSegments;
	}

	public void addOtherAirlineSegment(OtherAirlineSegment otherAirlineSegment) {

		if (this.getOtherAirlineSegments() == null) {
			this.setOtherAirlineSegments(new HashSet<OtherAirlineSegment>());
		}
		otherAirlineSegment.setReservation(this);
		this.getOtherAirlineSegments().add(otherAirlineSegment);
	}

	/**
	 * @hibernate.set lazy="false" cascade="save-update" inverse="true"
	 * @hibernate.collection-key column="PNR"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.OperatingCarrierRecordLocator"
	 */
	public Set<OperatingCarrierRecordLocator> getOperatingCarrierRecordLocators() {
		return operatingCarrierRecordLocators;
	}

	public void setOperatingCarrierRecordLocators(Set<OperatingCarrierRecordLocator> operatingCarrierRecordLocators) {
		this.operatingCarrierRecordLocators = operatingCarrierRecordLocators;
	}

	public void addOperatingCarrierRecordLocator(OperatingCarrierRecordLocator operatingCarrierRecordLocator) {

		if (this.getOperatingCarrierRecordLocators() == null) {
			this.setOperatingCarrierRecordLocators(new HashSet<OperatingCarrierRecordLocator>());
		}
		operatingCarrierRecordLocator.setReservation(this);
		this.getOperatingCarrierRecordLocators().add(operatingCarrierRecordLocator);
	}

	/**
	 * @return Returns the userNoteType.
	 */
	public String getUserNoteType() {
		return userNoteType;
	}

	/**
	 * @param userNoteType
	 *            the userNoteType to set
	 */
	public void setUserNoteType(String userNoteType) {
		this.userNoteType = userNoteType;
	}

	public BigDecimal getTotalGoquoAmount() {
		return totalGoquoAmount;
	}

	public void setTotalGoquoAmount(BigDecimal totalGoquoAmount) {
		this.totalGoquoAmount = totalGoquoAmount;
	}

	/**
	 * @hibernate.property column = "INFANT_PAYMENT_SEPARATED"
	 */
	public char getInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public void setInfantPaymentSeparated(char infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}

	public boolean isInfantPaymentRecordedWithInfant() {
		return ReservationInternalConstants.InfantPaymentRecordedWithInfant.YES == infantPaymentSeparated;
	}

	/**
	 * @return the taxInvoicesList
	 */
	public List<TaxInvoice> getTaxInvoicesList() {
		return taxInvoicesList;
	}

	/**
	 * @param taxInvoicesList
	 *            the taxInvoicesList to set
	 */
	public void setTaxInvoicesList(List<TaxInvoice> taxInvoicesList) {
		this.taxInvoicesList = taxInvoicesList;
	}

	/**
	 * @return
	 * @hibernate.property column ="USE_AA_ETS"
	 */
	public char getUseAeroMartETS() {
		return useAeroMartETS;
	}

	public void setUseAeroMartETS(char useAeroMartETS) {
		this.useAeroMartETS = useAeroMartETS;
	}

	public boolean isETRecordedInAeroMartETS() {
		return 'Y' == useAeroMartETS;
	}

	/**
	 * @return
	 * @hibernate.property column ="ORIGIN_COUNTRY_OF_CALL"
	 */
	public String getOriginCountryOfCall() {
		return originCountryOfCall;
	}

	public void setOriginCountryOfCall(String originCountryOfCall) {
		this.originCountryOfCall = originCountryOfCall;
	}
	/**
	 * 
	 * @return
	 * 
	 * @hibernate.property column = "PROMO_CODE"
	 */
	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
}
