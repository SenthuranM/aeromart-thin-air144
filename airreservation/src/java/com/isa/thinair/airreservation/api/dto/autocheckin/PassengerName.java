/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Panchatcharam.S
 *
 */
public class PassengerName implements Serializable {

	private static final long serialVersionUID = -4812851997486764854L;
	private Collection<String> givenName;
	private String surname;
	private String nameTitle;

	public Collection<String> getGivenName() {
		return givenName;
	}

	public void setGivenName(Collection<String> givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNameTitle() {
		return nameTitle;
	}

	public void setNameTitle(String nameTitle) {
		this.nameTitle = nameTitle;
	}

}
