package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class PNLADLEmailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1961998440529410801L;

	private String originatedServer = null;

	private String airportCode = null;

	private String flightNumber = null;

	private Date departureDate = null;

	private String messageType = null;

	private String customMessage = null;

	private StackTraceElement[] stackTraceElements = null;

	private String exceptionDetails = null;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public String getStrDepartureDate() {
		if (getDepartureDate() != null) {
			return getDepartureDate().toString();
		} else {
			return "WAS NULL";
		}
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return Returns the customMessage.
	 */
	public String getCustomMessage() {
		return customMessage;
	}

	/**
	 * @param customMessage
	 *            The customMessage to set.
	 */
	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	/**
	 * @return Returns the stackTraceElements.
	 */
	public StackTraceElement[] getStackTraceElements() {
		return stackTraceElements;
	}

	/**
	 * @param stackTraceElements
	 *            The stackTraceElements to set.
	 */
	public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}

	/**
	 * @return Returns the exceptionDetails.
	 */
	public String getExceptionDetails() {
		return exceptionDetails;
	}

	/**
	 * @param exceptionDetails
	 *            The exceptionDetails to set.
	 */
	public void setExceptionDetails(String exceptionDetails) {
		this.exceptionDetails = exceptionDetails;
	}

	public String getOriginatedServer() {
		return originatedServer;
	}

	public void setOriginatedServer(String originatedServer) {
		this.originatedServer = originatedServer;
	}

}
