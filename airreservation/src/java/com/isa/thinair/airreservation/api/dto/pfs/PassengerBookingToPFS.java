/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashain
 *
 */
public class PassengerBookingToPFS {
	private String bookingClass;
	private int count;
	private String countDisplay;
	private List<PassengerCategoryToPFS> paxCategoryLst = new ArrayList<PassengerCategoryToPFS>();
	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}
	/**
	 * @param bookingClass the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}	
	
	/**
	 * @return the paxCategoryLst
	 */
	public List<PassengerCategoryToPFS> getPaxCategoryLst() {
		return paxCategoryLst;
	}
	/**
	 * @param paxCategoryLst the paxCategoryLst to set
	 */
	public void setPaxCategoryLst(List<PassengerCategoryToPFS> paxCategoryLst) {
		this.paxCategoryLst = paxCategoryLst;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the countDisplay
	 */
	public String getCountDisplay() {
		return countDisplay;
	}
	/**
	 * @param countDisplay the countDisplay to set
	 */
	public void setCountDisplay(String countDisplay) {
		this.countDisplay = countDisplay;
	}
	

	
	
}
