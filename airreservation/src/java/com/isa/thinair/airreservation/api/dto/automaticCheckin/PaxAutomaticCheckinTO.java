package com.isa.thinair.airreservation.api.dto.automaticCheckin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author MOhamed Rizwan
 */
public class PaxAutomaticCheckinTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer pnrPaxSegAutoCheckinId;
	private Integer pnrPaxId;
	private Integer pnrSegId;
	private Integer ppfId;
	private Integer flightAmSeatId;
	private Integer salesChannelCode;
	private String userId;
	private Integer customerId;
	private Integer autoCheckinTemplateId;
	private Date requestTimeStamp;
	private String status;
	private String dcsCheckinStatus;
	private String dcsResponseText;
	private String seatTypePreference;
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String email;
	private Integer noOfAttempts;
	private String pnr;
	private ExternalChgDTO chgDTO;
	private Integer pkey;
	private String seatCode;
	private Integer sitTogetherPnrPaxId;

	public Integer getPnrPaxSegAutoCheckinId() {
		return pnrPaxSegAutoCheckinId;
	}

	public void setPnrPaxSegAutoCheckinId(Integer pnrPaxSegAutoCheckinId) {
		this.pnrPaxSegAutoCheckinId = pnrPaxSegAutoCheckinId;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getPpfId() {
		return ppfId;
	}

	public void setPpfId(Integer ppfId) {
		this.ppfId = ppfId;
	}

	public Integer getFlightAmSeatId() {
		return flightAmSeatId;
	}

	public void setFlightAmSeatId(Integer flightAmSeatId) {
		this.flightAmSeatId = flightAmSeatId;
	}

	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getAutoCheckinTemplateId() {
		return autoCheckinTemplateId;
	}

	public void setAutoCheckinTemplateId(Integer autoCheckinTemplateId) {
		this.autoCheckinTemplateId = autoCheckinTemplateId;
	}

	public Date getRequestTimeStamp() {
		return requestTimeStamp;
	}

	public void setRequestTimeStamp(Date requestTimeStamp) {
		this.requestTimeStamp = requestTimeStamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDcsCheckinStatus() {
		return dcsCheckinStatus;
	}

	public void setDcsCheckinStatus(String dcsCheckinStatus) {
		this.dcsCheckinStatus = dcsCheckinStatus;
	}

	public String getDcsResponseText() {
		return dcsResponseText;
	}

	public void setDcsResponseText(String dcsResponseText) {
		this.dcsResponseText = dcsResponseText;
	}

	public String getSeatTypePreference() {
		return seatTypePreference;
	}

	public void setSeatTypePreference(String seatTypePreference) {
		this.seatTypePreference = seatTypePreference;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getNoOfAttempts() {
		return noOfAttempts;
	}

	public void setNoOfAttempts(Integer noOfAttempts) {
		this.noOfAttempts = noOfAttempts;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the chgDTO
	 */
	public ExternalChgDTO getChgDTO() {
		return chgDTO;
	}

	/**
	 * @param chgDTO
	 *            the chgDTO to set
	 */
	public void setChgDTO(ExternalChgDTO chgDTO) {
		this.chgDTO = chgDTO;
	}

	/**
	 * @return the pkey
	 */
	public Integer getPkey() {
		return pkey;
	}

	/**
	 * @param pkey the pkey to set
	 */
	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}

	/**
	 * @return the seatCode
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode the seatCode to set
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	/**
	 * @return the sitTogetherPnrPaxId
	 */
	public Integer getSitTogetherPnrPaxId() {
		return sitTogetherPnrPaxId;
	}

	/**
	 * @param sitTogetherPnrPaxId
	 *            the sitTogetherPnrPaxId to set
	 */
	public void setSitTogetherPnrPaxId(Integer sitTogetherPnrPaxId) {
		this.sitTogetherPnrPaxId = sitTogetherPnrPaxId;
	}

}
