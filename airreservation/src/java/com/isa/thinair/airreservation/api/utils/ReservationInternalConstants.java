/*
 * ==============================================================================
 * ISA Software License, Version 1.0s
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.utils;

import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * To keep track of Reservation Internal Constants
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public final class ReservationInternalConstants {

	public static final String DLIM = "^";

	/** Denotes Transaction Types */
	public static interface TnxTypes {
		/** Denotes credit transaction type */
		public static final String CREDIT = "CR";

		/** Denotes debit transaction type */
		public static final String DEBIT = "DR";
	}

	/** Denotes Segment Status */
	public static interface ReservationSegmentStatus {
		/** Denotes reservation segment cancellation */
		public static final String CANCEL = "CNX";

		/** Denotes confirmed status */
		public static final String CONFIRMED = "CNF";

		/** Denotes reservation segment status after flight cancellation */
		public static final String FLIGHT_CANCEL = "UN";

		/** Denotes waitlisting status */
		public static final String WAIT_LISTING = "WL";
		
		/** Denotes reservation segment new */
		public static final String NEW_SEGMENT = "NEW";
	}

	/** Denotes Segment Status */
	public static interface ReservationSegmentSubStatus {
		/** Denotes reservation segment cancellation */
		public static final String EXCHANGED = "EX";

	}

	/** Denotes reservation states */
	public static interface ReservationStatus {
		/** Denotes reservation confirmed */
		public static final String CONFIRMED = "CNF";

		/** Denotes reservation cancellation */
		public static final String CANCEL = "CNX";

		/** Denotes reservation on hold */
		public static final String ON_HOLD = "OHD";

		/** Denotes standay reservation */
		public static final String STAND_BY = "STB";

		/** Denotes a Void reservation */
		public static final String VOID = "VOD";
	}

	/** Denotes Reservation passenger states */
	public static interface ReservationPaxStatus {
		/** Denotes reservation passenger on hold status */
		public static final String ON_HOLD = "OHD";

		/** Denotes reservation passenger cancel */
		public static final String CANCEL = "CNX";

		/** Denotes reservation passenger confirmed */
		public static final String CONFIRMED = "CNF";
	}

	/** Denotes Passenger Types */
	public static interface PassengerType {
		/** Denotes adult type */
		public static final String ADULT = PaxTypeTO.ADULT;

		/** Denotes infant type */
		public static final String INFANT = PaxTypeTO.INFANT;

		/** Denotes child type */
		public static final String CHILD = PaxTypeTO.CHILD;

		public static final String PARENT = "PA";
	}

	/** Passport status */
	public static interface PassportStatus {
		/** Passport existing */
		public static final String EXISTING = "EXISTING";

		/** Passport missing */
		public static final String MISSING = "MISSING";

		/** All cases */
		public static final String ANY = "ANY";
	}

	/** DOCS Information status */
	public static interface DOCSInfoStatus {
		/** DOCS Info existing */
		public static final String EXISTING = "EXISTING";

		/** DOCS Info missing */
		public static final String MISSING = "MISSING";

		/** All cases */
		public static final String ANY = "ANY";
	}

	public static interface PassengerConst {
		/** Holds the separate character for saving pspt #, expiry date & issued place */
		public static final String FOID_SEPARATOR = ",";
	}

	/** Denotes reservation segment return types */
	public static interface ReturnTypes {
		/** Holds return flag true status */
		public static final String RETURN_FLAG_TRUE = "Y";

		/** Holds return flag false status */
		public static final String RETURN_FLAG_FALSE = "N";
	}

	/** Denotes if the reservation is dummy of not */
	public static interface DummyBooking {
		/** if it is a dummy booking */
		public static char YES = 'Y';

		/** if it is a actual booking */
		public static char NO = 'N';
	}

	/** Denotes if the reservation is void of not */
	public static interface VoidReservation {
		/** if it is a dummy booking */
		public static char YES = 'Y';

		/** if it is a actual booking */
		public static char NO = 'N';
	}
	
	public static interface UseAccelAeroETS {
		/** if it is a dummy booking */
		public static char YES = 'Y';

		/** if it is a actual booking */
		public static char NO = 'N';
	}

	/** Denotes if the reservation is modifiable of not */
	public static interface Modifiable {
		/** if it is a modifiable booking */
		public static char YES = 'Y';

		/** if it is a unmodifiable booking */
		public static char NO = 'N';
	}
	
	public static interface InfantPaymentRecordedWithInfant {
		/** if it is a modifiable booking */
		public static char YES = 'Y';

		/** if it is a unmodifiable booking */
		public static char NO = 'N';
	}

	/** Denotes Sales channels */
	public static interface SalesChannel {
		/** Denotes AGENT channel */
		public static final int TRAVEL_AGENT = SalesChannelsUtil.SALES_CHANNEL_AGENT;

		/** Denotes Web channel */
		public static final int WEB = SalesChannelsUtil.SALES_CHANNEL_WEB;

		/** Denotes Holidays channel */
		public static final int AA_HOLIDAYS = SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS;

		/** Denotes Dnata channel */
		public static final int DNATA = SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES;

		/** Denotes Interlined channel */
		public static final int INTERLINED = SalesChannelsUtil.SALES_CHANNEL_INTERLINED;

		/** Denotes LCC channel */
		public static final int LCC = SalesChannelsUtil.SALES_CHANNEL_LCC;

		/** Denotes EBI ATM channel */
		public static final int EBIATM = SalesChannelsUtil.SALES_CHANNEL_EBI_ATM;

		/** Denotes EBI CDM channel */
		public static final int EBICDM = SalesChannelsUtil.SALES_CHANNEL_EBI_CDM;

		/** Denotes HahnAir channel */
		public static final int GDS = SalesChannelsUtil.SALES_CHANNEL_GDS;

		public static final int AM_PAY = SalesChannelsUtil.SALES_CHANNEL_GOQUO;
		
		public static final int IOS = SalesChannelsUtil.SALES_CHANNEL_IOS;
		
		public static final int ANDROID = SalesChannelsUtil.SALES_CHANNEL_ANDROID;

	}

	/** Denotes Passenger Titles */
	public static interface PassengerTitle {
		/** Holds the Mr Title */
		public static final String MR = "MR";

		/** Holds the Ms Title */
		public static final String MS = "MS";

		/** Holds the Child Title */
		public static final String CHILD = "CHILD";

		/** Holds Mrs Title */
		public static final String MRS = "MRS";

		/** Holds Master Title */
		public static final String MASTER = "MSTR";

		/** Holds Miss Title */
		public static final String MISS = "MISS";

		/** Holds Doctor Title */
		public static final String DOCTOR = "DR";

		/** Holds Captain Title */
		public static final String CAPTAIN = "CAPT";
	}
	
	/** Titles valid for Infants */
	public static enum INFANT_TITLE_VALUES {
		MSTR("Mstr"), MISS("Miss");
		
		private String value;
		
		private INFANT_TITLE_VALUES(String value){
			this.value = value;
		}
		
		public String getValue(){
			return this.value;
		}
	};

	/** Denotes Charge groups */
	public static interface ChargeGroup {
		/** Holds the fare group */
		public static final String FAR = "FAR";

		/** Holds the tax group */
		public static final String TAX = "TAX";

		/** Holds the surcharge group */
		public static final String SUR = "SUR";

		/** Holds the cancellation group */
		public static final String CNX = "CNX";

		/** Holds the modification group */
		public static final String MOD = "MOD";

		/** Holds the adjustment group */
		public static final String ADJ = "ADJ";

		/** Holds the infant charge group */
		public static final String INF = "INF";

		/** Holds the penalty charge group */
		public static final String PEN = "PEN";

		/** Holds the discount charge group */
		public static final String DISCOUNT = "DIS";

		/**
		 * not a valid charge group for reservation flow. used to hold other category[CNX,MOD,ADJ,INF,PEN,DIS] charges,
		 * to categories when populating tnx-breakdown summary.
		 * */
		public static final String ANY = "ANY";
	}

	public static interface ChargeGroupRatios {
		public static final String SUR_RATIO = "SYTN";
		public static final String TAX_RATIO = "SNTY";
		public static final String SURTAX_RATIO = "SYTY";
		public static final String NO_RATIO = "SNTN";
	}

	/** Donotes Alert Types */
	public static interface AlertTypes {
		/** Holds the alert false status */
		public static final int ALERT_FALSE = 0;

		/** Holds the alert true status */
		public static final int ALERT_TRUE = 1;
	}

	/** Donotes Pnr template names */
	public static interface PnrTemplateNames {
		/** Itinerary */
		public static final String ITINERARY_EMAIL = "itinerary_email";

		/** Fatourati email */
		public static final String FATOURATI_EMAIL = "fatourati_email";

				
		/** automatic-checkin-different-seat */
		public static final String AUTOMATIC_CHECKIN_DIFFERENT_SEAT = "automatic_checkin_different_seat";
		
		/** automatic-checkin-failure*/
		public static final String AUTOMATIC_CHECKIN_FAILURE = "automatic_checkin_failure";
	

		/** Interline Itinerary */
		public static final String INTERLINE_ITINERARY_EMAIL = "interline_itinerary_email";
		
		public static final String EMAIL_NOTIFICAION_AGENT_TOPUP = "email_notification_agent_topup";
		
		/** Medical SSR */
		public static final String MEDICAL_SSR_EMAIL = "medical_ssr_email";

		/** External payment transaction reconciliation status */
		public static final String EXT_PAY_RECON_STATUS_TEMPLATE = "extpay_recon_status";

		/** Segment transfer failures template */
		public static final String SEGMENT_TRANSFER_FAILURES_TEMPLATE = "segment_transfer_failures";

		/** On hold sms template */
		public static final String ONHOLD_SMS_TEMPLATE = "onhold_sms";
		
		public static final String OTP_REDEEM_VOUCHER = "otp_to_redeem_voucher";

		/** web reservation */
		public static final String WEB_RESERVATION = "WEB001";

		public static final String SSR_NOTIFICATION_EMAIL = "ssr_notification_email";

		public static final String AIRPORT_TRANSFER_EMAIL_TEMPLATE = "airport_transfer_email";

		/** Ancillary Reminder Email **/
		public static final String ANCILLARY_REMINDER_EMAIL = "ancillary_reminder_notification_email";

		/** Online CheckIn Reminder Email **/
		public static final String ONLINE_CHECKIN_REMINDER_EMAIL = "online_checkin_reminder_notification_email";

		/** Itinerary */
		public static final String FLIGHT_MANIFEST_EMAIL = "flight_manifest";

		/** SMS template for flight change */
		public static final String FLIGHT_CHANGE_SMS = "flight_change_sms";

		/** Email template for flight change */
		public static final String FLIGHT_CHANGE_EMAIL = "flight_change_email";
		public static final String FLIGHT_SEATCHANGE_EMAIL = "flight_seatchange_email";

		/** General email template for flight */
		public static final String FLIGHT_GENERAL_EMAIL = "flight_general_email";

		/** SMS template for flight cancel */
		public static final String FLIGHT_CANCEL_SMS = "flight_cancel_sms";

		/** SMS template notifing an official about a flight cancel */
		public static final String FLIGHT_CANCEL_SMS_FOR_AN_OFFICIAL = "flight_cancel_sms_for_officer";

		/** SMS template notifing an official about a flight change */
		public static final String FLIGHT_CHANGE_SMS_FOR_AN_OFFICIAL = "flight_change_sms_for_officer";

		/** Email template for flight cancel */
		public static final String FLIGHT_CANCEL_EMAIL = "flight_cancel_email";
		public static final String FLIGHT_CANCEL_EMAIL_FOR_AN_OFFICIAL = "flight_cancel_email_for_officer";
		public static final String SCHEDULE_CANCEL_EMAIL_FOR_AN_OFFICIAL = "schedule_cancel_email_for_officer";

		/** SMS template for flight re-protect */
		public static final String FLIGHT_REPROTECT_SMS = "flight_reprotect_sms";

		/** Email template for flight re-protect */
		public static final String FLIGHT_REPROTECT_EMAIL = "flight_reprotect_email";

		/** Email template for flight re-protect */
		public static final String FLIGHT_REPROTECT_EMAIL_IBE = "flight_reprotect_email_ibe";

		/** Email template for flight re-protect to agents */
		public static final String FLIGHT_REPROTECT_EMAIL_AGENT = "flight_reprotect_email_agent";

		/** SMS template for airport DST change */
		public static final String DST_CHANGE_SMS = "DST_change_sms";

		/** Email template for airport DST change */
		public static final String DST_CHANGE_EMAIL = "DST_change_email";

		/** Email template to use for unconfirmed IBE onhold reservations */
		public static final String IBE_ONHOLD_EMAIL = "IBE_onhold_notification_email";

		/** Email template for reservation modification notify owner agent */
		public static final String RESERVATION_MODIFIED_EMAIL_AGENT = "reservation_modified_email_agent";

		public static final String PROMO_CODE_NOTIFY_EMAIL = "promo_code_notification_email";

		/** Partial payment Reminder Email **/
		public static final String PARTIAL_PAYMENT_REMINDER_EMAIL = "partial_payment_reminder_notification_email";

		/** payment reminders sent to IBE OHD bookings **/
		public static final String IBE_ONHOLD_PAYMENT_REMINDER = "IBE_Onhold_payment_reminder_email";

		public static final String GROUP_BOOKING_EMAIL = "group_booking_email";
		
		/** voucher email sent to passengers **/
		public static final String VOUCHER_EMAIL_TEMPLATE = "voucher_email";
		
		/** promo code email sent to passengers **/
		public static final String PROMO_CODE_EMAIL_TEMPLATE = "promo_code_customer_email";

	}

	/** Donotes Passenger fare segment types */
	public static interface PaxFareSegmentTypes {
		/** Holds the confirmed status */
		public static final String CONFIRMED = "C";

		/** Holds the flown status */
		public static final String FLOWN = "F";

		/** Holds the no shore status */
		public static final String NO_SHORE = "N";

		/** Holds the no record status */
		public static final String NO_REC = "R";

		/** Holds the go shore status */
		public static final String GO_SHORE = "G";

		/** Holds the not boarded status */
		public static final String NOT_BOARDED = "B";

		/** Holds the off loaded status */
		public static final String OFF_LOADED = "O";

		/** Holds the in vol status */
		public static final String IN_VOL = "I";

		/** Holds the ch gcl status */
		public static final String CH_GCL = "V";

		/** Holds the cfmwl status */
		public static final String CF_MWL = "W";

		/** Holds the goshn status */
		public static final String GO_SHN = "S";

		/** Holds the chgsg status */
		public static final String CH_GSG = "D";

		/** Holds the chgfl status */
		public static final String CH_GFL = "F";

		/** Holds the idpad status */
		public static final String ID_PAD = "P";

	}

	/** Donotes Pfs Passenger status types */
	public static interface PfsPaxStatus {
		/** Holds the flown status */
		public static final String FLOWN = "F";

		/** Holds the go shore status */
		public static final String GO_SHORE = "G";

		/** Holds the no shore status */
		public static final String NO_SHORE = "N";

		/** Holds the no rec status */
		public static final String NO_REC = "R";

		/** Holds the off load status */
		public static final String OFF_LD = "O";

		/** Holds the invol status */
		public static final String IN_VOL = "I";

		/** Holds the chgcl status */
		public static final String CH_GCL = "V";

		/** Holds the cfmwl status */
		public static final String CF_MWL = "W";

		/** Holds the goshn status */
		public static final String GO_SHN = "S";

		/** Holds the chgsg status */
		public static final String CH_GSG = "D";

		/** Holds the chgfl status */
		public static final String CH_GFL = "F";

		/** Holds the idpad status */
		public static final String ID_PAD = "P";
		
		public static final String CONFIRMED = "C";
	}

	/**
	 * Holds the values in MSG in the XML schema that will indicate a NoShow passenger. This will only occur if the
	 * passenger is checked in but didn't board the flight for some reason
	 */
	public static interface PfsPaxNoShowIndicators {
		public static final String GATE_NO_SHOW = "GATENSHOW";
	}

	/** Donotes Pfs Categories */
	public static interface PfsCategories {
		/** Holds the go shore status */
		public static final String GO_SHOW = "GOSHO";

		/** Holds the no shore status */
		public static final String NO_SHOW = "NOSHO";

		/** Holds the no rec status */
		public static final String NO_REC = "NOREC";

		/** Holds the off load status */
		public static final String OFF_LK = "OFFLK";

		/** Holds the off load status */
		public static final String OFF_LN = "OFFLN";

		/** Holds the invol status */
		public static final String IN_VOL = "INVOL";

		/** Holds the chgcl status */
		public static final String CH_GCL = "CHGCL";

		/** Holds the cfmwl status */
		public static final String CF_MWL = "CFMWL";

		/** Holds the goshn status */
		public static final String GO_SHN = "GOSHN";

		/** Holds the chgsg status */
		public static final String CH_GSG = "CHGSG";

		/** Holds the chgfl status */
		public static final String CH_GFL = "CHGFL";

		/** Holds the idpad status */
		public static final String ID_PAD = "IDPAD";
	}

	/** Donotes Pfs process status types */
	public static interface PfsProcessStatus {
		/** Holds the process status */
		public static final String PROCESSED = "P";

		/** Holds the no processed status */
		public static final String NOT_PROCESSED = "N";

		/** Holds the error occured status */
		public static final String ERROR_OCCURED = "E";
	}

	/** Donotes Pfs status types */
	public static interface PfsStatus {
		/** Holds the unparsed status - it is not already parsed */
		public static final String UN_PARSED = "U";

		/** Holds the un parsed with errors status */
		public static final String UN_PARSED_WITH_ERRORS = "E";

		/** Holds the parsed status - it is already parsed */
		public static final String PARSED = "P";

		/** Holds the reconcile status - it is reconciled with no errors */
		public static final String RECONCILE_SUCCESS = "R";

		/** Holds the reconcile status - Waiting for PFS process after parsed until ETL processed */
		public static final String WAITING_FOR_ETL_PROCESS = "W";
	}

	/** Donotes reservation alert types */
	public static interface ReservationAlertTypes {

		/** Donotes cancel flight alert type */
		public static final String CANCEL_FLIGHT = "CNXFLT";

		/** Donotes re schedule flight alert type */
		public static final String RESCHEDULE_FLIGHT = "RSHFLT";
	}

	/** Donotes reservation payment modes types */
	public static interface ReservationPaymentModes {

		/** Donotes force confirm type */
		public static final Integer TRIGGER_PNR_FORCE_CONFIRMED = new Integer(1);

		/** Donotes full payment type */
		public static final Integer TRIGGER_FULL_PAYMENT = new Integer(2);

		/** Donotes leave states as it is type */
		public static final Integer TRIGGER_LEAVE_STATES_AS_IT_IS = new Integer(3);

		/** Donotes leave states as it is and do not change the ownership type */
		public static final Integer TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE = new Integer(4);

		public static final Integer TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE = new Integer(5);
	}

	/** Denoting Credit Cards Mappings */
	public static interface CreditCardPaymentMappings {
		/** Master */
		public static final int MASTER = 102607;

		/** Visa */
		public static final int VISA = 102606;

		/** Amex */
		public static final int AMEX = 102609;

		/** Dinars */
		public static final int DINERS = 102608;

		/** Generic */
		public static final int GENERIC = 102610;

		/** CMI */
		public static final int CMI = 102611;
	}

	/*-
	 *  Donotes tempory payment transaction types.
	 *  States with ( ) repeated more than once 
	 * 
	 * 					   |--------> IS
	 * 		|-------> IP --|
	 * 		|			   |--------> IF
	 * 		|-------> II
	 * 	I --|			   |-------> RS
	 *  	|-------> PS --|			  |-------> US
	 * 	  	|			   |-------> RF --| 
	 * 		|			   |			  |-------> UF
	 * 		|			   |------->(II)
	 * 		|			   |			  |------->(IS)
	 * 		|			   |------->(IP)--|
	 * 		|							  |------->(IF)
	 * 		|
	 * 	  	|-------> PF
	 */
	public static interface TempPaymentTnxTypes {
		/**
		 * Donotes Request in the initiated state, contact not made with PGW, Starting state for all requests
		 */
		public static final String INITIATED = "I";

		/**
		 * Payment Success, This is where the request is being received, validated by the PGW and responded back to
		 * AccelAero. All information (Exp date/month, CVV etc) entered is accurate and the PGW has accepted the
		 * payment. Can either proceed to make a reservation successfully (PNR generation) or can be moved to a state
		 * when the PNR creation is failed (RF) due to failure in AccelAero side.
		 */
		public static final String PAYMENT_SUCCESS = "PS";

		/**
		 * Payment Failure, This is where the request is being received, validated by the PGW and responded back to
		 * AccelAero. Any one or more info entered is incorrect thus payment is rejected by the PGW.
		 */
		public static final String PAYMENT_FAILURE = "PF";

		/**
		 * Reservation successful, Reservation created. PNR generated. (Success Scenario)
		 */
		public static final String RESERVATION_SUCCESS = "RS";

		/**
		 * Payment successful, Reservation failed due to failure in AccelAero side (Technical issues, Unavailability of
		 * seats etc.). In environments where Auto reversal is not supported, these items should be monitored in order
		 * for the user to action manual refunds through the client Admin modules provided by the bank. In environments
		 * where auto reversal (refund) is supported, these items will be moved to UF or US.
		 */
		public static final String RESERVATION_FAILURE = "RF";

		/**
		 * Undo operation successful.
		 */
		public static final String UNDO_OPERATION_SUCCESS = "US";

		/**
		 * Undo operation failure, in environments where auto reversal (refund) is supported, these needs to be
		 * monitored as manual intervention required to settle these transactions.
		 */
		public static final String UNDO_OPERATION_FAILURE = "UF";

		/** Holds the initiated refund failed status */
		public static final String INITIATED_REFUND_FAILED = "IF";

		/** Holds the initiated no payment status */
		public static final String INITIATED_NO_PAYMENT = "II";

		/** Holds the initiated payment exist status */
		public static final String INITIATED_PAYMENT_EXIST = "IP";

		/** Holds the initiated refund success status */
		public static final String INITIATED_REFUND_SUCCESS = "IS";

	}

	public static interface TempPaymentTnxConstants {
		public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
		public static final String EXTERNAL_REFERENCE = "EXTERNAL_REFERENCE";
	}

	/**
	 * These states history represents the cases which was capture payment from the customers, but the reservation had
	 * failed
	 */
	public static interface TempPaymentInconsistentStatusHistoryTypes {

		public static final String I_PS_RF = "I->PS->RF";

		public static final String I_PS_PS_RF = "I->PS->PS->RF";

		public static final String I_RF_US = "I->RF->US";

		public static final String I_PF = "I->PF";

	}

	/** Denotes the type of Messages expected to be created from Pop3Client */
	public static interface PopMessageType {
		/** Donotes initiated type */
		public static final String PNL_MESSAGE = "PNL";

		/** Donotes completed type */
		public static final String PFS_MESSAGE = "PFS";
	}

	/** Denotes XAPNL process status types */
	public static interface XAPnlProcessStatus {
		/** Holds the process status */
		public static final String PROCESSED = "P";

		/** Holds the no processed status */
		public static final String NOT_PROCESSED = "N";

		/** Holds the error occured status */
		public static final String ERROR_OCCURED = "E";
	}

	/** Denotes External Pay Transaction Statuses */
	public static interface ExtPayTxStatus {
		/** Holds the unknown status */
		public static final String UNKNOWN = "U";

		/** Holds the initiated status */
		public static final String INITIATED = "I";

		/** Holds the successfuly completed status */
		public static final String SUCCESS = "S";

		/** Holds the failed status */
		public static final String FAILED = "F";

		/** Holds the excess at bank status */
		public static final String BANK_EXCESS = "BE";

		/** Holds the excess at accelaero status */
		public static final String AA_EXCESS = "AE";

		/** Holds the aborted status */
		public static final String ABORTED = "A";

		/** Holds the refunded status */
		public static final String REFUNDED = "R";

		/** Holds the reverted status */
		public static final String REVERTED = "RE";

		/** Holds the bank rolled back status */
		public static final String BANK_ROLLBACKED = "BR";
	}

	/** Denotes external pay transaction statuses */
	public static interface ExtPayTxReconStatus {
		/** Holds the not reconcilled status */
		public static final String NOT_RECONCILED = "N";

		/** Holds the successfully reconcilled status */
		public static final String RECONCILED = "R";

		/** Holds the reconcillation failed status */
		public static final String RECON_FAILED = "F";
	}

	/** Denotes external pay transaction initiated party */
	public static interface ExtPayTxReconSourceType {
		/** Denotes Manual operation */
		public static final String MANUAL = "M";

		/** Denotes Scheduled operation */
		public static final String SCHEDULED_JOB = "S";
	}

	/** Holds Reservation related external charges */
	public static enum EXTERNAL_CHARGES {
		CREDIT_CARD, HANDLING_CHARGE, SEAT_MAP, INSURANCE, NO_SHORE, REFUND, MEAL, AIRPORT_SERVICE, AIRPORT_TRANSFER, REF_ADJ, NON_REF_ADJ, INFLIGHT_SERVICES, FLEXI_CHARGES, BAGGAGE, JN_TAX, PROMOTION_REWARD, ADDITIONAL_SEAT_CHARGE, JN_ANCI, BUNDLED_FARE_CHARGE, JN_OTHER, ANCI_PENALTY, NAME_CHANGE_CHARGE, SERVICE_TAX, BSP_FEE, AUTOMATIC_CHECKIN;
	};

	/** Holds Reservation related charge adjustments */
	public static enum CHARGE_ADJUSTMENTS {
		NAME_CHANGE_CHARGE_INT, NAME_CHANGE_CHARGE_DOM, AUTOMATED_REFUND_TAX
	};

	/** Denotes the reservation segment transfer states which are used by the tempory code share solution */
	public static interface ReservationSegmentTransfer {
		/** Holds the process initiated status */
		public static final String INITIATED = "I";

		/** Holds the successfuly completed status */
		public static final String SUCCESS = "S";

		/** Holds the failed status */
		public static final String FAILED = "F";
	}

	/** Denotes External Segment Status */
	public static interface ReservationExternalSegmentStatus {
		/** Denotes confirmed status */
		public static final String CONFIRMED = "CNF";

		/** Denotes the cancel status */
		public static final String CANCEL = "CNX";
	}
	
	public static interface ReservationOtherAirlineSegmentStatus {
		/** Denotes confirmed status */
		public static final String CONFIRMED = "HK";

		/** Denotes the cancel status */
		public static final String CANCEL = "HX";
	}

	/** Denotes External Segment Status */
	public static interface ReservationExternalFlightStatus {
		/** Denotes confirmed status */
		public static final String OPEN = "OPN";

		/** Denotes the cancel status */
		public static final String CLOSE = "CLS";
	}

	/** Denotes the interline states */
	public enum INTERLINE_STATES {
		TRUE, FALSE, NOT_SURE
	};

	public static interface Gender {
		public static final String MALE = "M";
		public static final String FEMALE = "F";
		public static final String UNDISCLOSED = "U";
	}

	// RQ-Request-If a response was not returned
	// SC-Success
	// FL-Failed from AIG, or Module Exception
	// TO-Time Out, Module Exception code is checked,..wrapping TimeOutException
	// OH-When a quote was made On hold - kiosk
	// CX - canceled
	public enum INSURANCE_STATES {
		RQ, SC, FL, TO, OH, QO, CX
	};

	public static interface InsurancePublishStatus {
		public static final int PUBLISH_SUCCESS = 1;
		public static final int PUBLISH_FAILED = 2;
	}

	public enum ReservationPaxSegmentSSRStatus {
		CONFIRM("CNF"), CANCEL("CNX");

		private final String status;

		private ReservationPaxSegmentSSRStatus(String status) {
			this.status = status;
		}

		public String getCode() {
			return status;
		}
	}

	public static interface PassengerCheckinStatus {
		/** Holds passenger checked in status */
		public static final String CHECKED_IN = "CHK";

		/** Holds passenger to be checked in status */
		public static final String TO_BE_CHECKED_IN = "NCK";

	}

	public static interface PaymentMode {
		public static final String CASH = "CA";
		public static final String ONACCOUNT = "OA";
		public static final String CREDITCARD = "CC";
	}

	public static interface SegmentAncillaryStatus {
		public static final String ALL = "ALL";
		public static final String PARTIAL = "PARTIAL";
		public static final String NONE = "NONE";
		public static final String CANCEL = "CNX";
	}

	public static interface SERVICE_CALLER {
		public static final String SEAT_MAP = "SM";
		public static final String MEAL = "ML";
		public static final String INSURANCE = "INS";
		public static final String AIRPORT_SERVICES = "AS";
		public static final String BAGGAGE = "BG";
	}

	public static interface FlightSegNotifyEvent {
		public static String INPROGRESS = "INPROGRESS";
		public static String FAILED = "FAILED";
	}

	public static interface SchedulerType {
		public static String MASTER_SCHEDULER = "MASTER_SCHEDULER";
		public static String RECOVERY_SCHEDULER = "RECOVERY_SCHEDULER";
	}

	public static interface PnrSegNotifyEvent {
		public static String SENT = "SENT";
		public static String FAILED = "FAILED";
		public static String INVALID = "INVALID";
	}

	public static interface NotificationType {
		public static String ANCI_REMINDER = "ANCI_REMINDER";
		public static String RAK_INSURANCE_PUBLISH = "INS";
		public static String ONLINE_CHECKIN_REMINDER = "ONLINE_CHECKIN_REMINDER";
	}

	public static interface AirportMessageStages {
		public static String SEARCH_RESULT = "SEARCH_RESULT";
		public static String CARD_PAYMENT = "CARD_PAYMENT";
		public static String ITINERARY = "ITINERARY";
	}

	public static interface AirportMessageTravelWay {
		public static String DEP = "DEP";
		public static String ARR = "ARR";
	}

	public static interface AirportMessageSalesChannel {
		public static String XBE = "3";
		public static String IBE = "4";
		public static String API = "12";
		public static String BOTH = "5";
	}
	
	public static interface AirportMessageSuffix {
		public static String API_SUFFIX = "airport_message:";
	}

	public static interface DefaultPaymentGateway {
		public static String YES = "Y";
	}

	public enum InsuranceTypes {
		NONE(0), GENERAL(1), CANCELATION(2), MULTIRISK(3);

		private final Integer code;

		private InsuranceTypes(int code) {
			this.code = new Integer(code);
		}

		public Integer getCode() {
			return this.code;
		}
	}

	public enum PricingBasis {
		Value("V"); // Other types - PercentageOfFare("PF"), PercentageOfTotalPrice("PTF");

		private final String code;

		private PricingBasis(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum FareRulePaxFeeTypes {
		MODIFY("MOD"), CANCEL("CNX"), NOSHOW("NOSHOW");

		private final String code;

		private FareRulePaxFeeTypes(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum InsuranceSalesType {
		DAILY_SALES, DAILY_FLOWN_SALES
	};

	public static interface OnholdAlertStatus {
		public static char SENT = 'Y';

		public static char ATTEMPTED = 'A';

		public static char FALIED = 'N';
	}

	public static interface BankGuranteeNotifyTemplates {
		public static String BANK_GUARANTEE_EXPIRY_NOTIFICATION = "bank_gurantee_expiry";
		public static String CREDIT_LIMIT_UTILIZATION_NOTIFICATION = "credit_limit_utilization";
		public static String BSP_CREDIT_LIMIT_UTILIZATION_NOTIFICATION = "bsp_credit_limit_utilization";

	}

	public static interface AuditActionStatus {
		public static String AUDIT_ACTION_SAVE = "SAVE";

		public static String AUDIT_ACTION_PARSE = "PARSE";

		public static String AUDIT_ACTION_PROCESS = "PROCESS";

		public static String AUDIT_ACTION_DELETE = "DELETE";

		public static String AUDIT_ACTION_SYSTEM_ERROR = "ERROR";

		public static String AUDIT_ACTION_PAX_SAVED = "PAX SAVE";

		public static String AUDIT_ACTION_PAX_DELETED = "PAX DELETE";
	}

	public static interface ResModifyEmailAgentModificationType {
		public static String CANCEL_RESERVATION = "CNX_RESERVATION";
		public static String EXT_ON_HOLD = "EXT_ON_HOLD";
		public static String CNF_WAITLISTED = "CNF_WAITLISTED";

	}

	public static interface ResModifyEmailAgentModificationConst {
		public static String OLD_OHD_TIME = "OLD_OHD_TIME";
		public static String EXT_OHD_TIME = "EXT_OHD_TIME";
	}

	public static interface BookingClassAllocStatus {
		public static final String OPEN = "OPN";
		public static final String CLOSE = "CLS";
	}

	public enum ReservationType {
		WL("WAITLISTING");

		private String description;

		ReservationType(String description) {
			this.description = description;
		}

		public String getDescription() {
			return description;
		}
	}

	public enum AnciTypes {
		MEAL, BAGGAGE, SEAT, SSR
	};
	
	public static interface SsrTypes {
		public static String MEDA = "MEDA";
	};

	public static enum CHAEGE_CODES {
		SM, ML, IN, SSR, BG, FL, HALA, AT, AUTOCHKR, AUTOCHK
	};

	/** Donotes NOSHOW Refund process status types */
	public static interface NSRefundProcessStatus {
		/** Holds the refund processed status */
		public static final String PROCESSED = "P";

		/** Holds the refund not processed status */
		public static final String NOT_PROCESSED = "N";

		/** Holds the refund error occurred status */
		public static final String ERROR_OCCURED = "E";

		/** Holds the refund success status */
		public static final String REFUND_SUCCESS = "R";
	}

	public enum OHDAlertTypes {
		PAYMENT_FAILURES("PAYFAIL"), PAYMENT_REMINDER("PAYREM");

		private final String code;

		private OHDAlertTypes(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}

	}

	public static enum LOYALTY_PRODUCTS {
		SEAT, INSURANCE, MEAL, HALA, BAGGAGE, FARE_BUNDLE, FLIGHT
	};

	public static interface UserNoteType {
		public static final String PUB = "PUB";
		public static final String PVT = "PVT";
	}	

	/** Denotes if the pnr segment booking class inventory nested or not */
	public static interface BCNested {
		/** if segment inventory from nested BC */
		public static String YES = "Y";

		/** if segment inventory not from nested BC */
		public static String NO = "N";
	}

	public static enum DCS_PAX_MSG_GROUP_TYPE {
		PNL_ADL, PAL_CAL,PAL_CAL_AC,PAL_CAL_D
	};
	
	public static interface ServiceTaxIdentificationConstant {
		public static String FARE = "fare";
		public static String ANCI = "anci";
	}

	public static interface AlertActionType {
		public static final String OTHER = "Other";
		public static final String EMAIL = "Email";
		public static final String FAX = "Fax";
		public static final String TELEPHONE = "Telephone";
	}

}
