package com.isa.thinair.airreservation.api.dto.airportTransfer;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Manoj Dhanushka
 */
public interface IPassengerAirportTransfer extends AncillaryAssembler {
	
	public void addPaxSegmentApt(Integer pnrPaxId, Integer pnrSegId, Integer ppfId, BigDecimal chargeAmount,
			Integer airportTransferId, String trnsDate, String trnsAddress, String trnsContact, String pickupType,
			String airportCode, TrackInfoDTO trackInfo) throws ModuleException;

}
