/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Holds the itinerary charges specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ItineraryChargesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1943425878596673166L;

	/** Holds the ond specific charges collection of ChargesDetailDTO */
	private Collection<ChargesDetailDTO> colChargesDetailDTO;

	/** Holds the ond description */
	private String ondDescription;

	/** Holds the ond charge amount */
	private BigDecimal ondChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * Returns ond charge amount as a String format value
	 * 
	 * @param numberFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getOndChargeAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(this.getOndChargeAmount().doubleValue(), numberFormat);
	}

	/**
	 * @return Returns the colChargesDetailDTO.
	 */
	public Collection<ChargesDetailDTO> getColChargesDetailDTO() {
		return colChargesDetailDTO;
	}

	/**
	 * @param colChargesDetailDTO
	 *            The colChargesDetailDTO to set.
	 */
	public void setColChargesDetailDTO(Collection<ChargesDetailDTO> colChargesDetailDTO) {
		this.colChargesDetailDTO = colChargesDetailDTO;
	}

	/**
	 * @return Returns the ondDescription.
	 */
	public String getOndDescription() {
		return ondDescription;
	}

	/**
	 * @param ondDescription
	 *            The ondDescription to set.
	 */
	public void setOndDescription(String ondDescription) {
		this.ondDescription = ondDescription;
	}

	/**
	 * @return Returns the ondChargeAmount.
	 */
	public BigDecimal getOndChargeAmount() {
		return ondChargeAmount;
	}

	/**
	 * @param ondChargeAmount
	 *            The ondChargeAmount to set.
	 */
	public void setOndChargeAmount(BigDecimal ondChargeAmount) {
		this.ondChargeAmount = ondChargeAmount;
	}

}
