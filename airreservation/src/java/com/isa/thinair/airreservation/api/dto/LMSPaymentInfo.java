package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author rumesh
 * 
 */
public class LMSPaymentInfo implements Serializable, PaymentInfo, CustomerPayment {

	private static final long serialVersionUID = 4090712457674450278L;

	private String loyaltyMemberAccountId;

	private String[] rewardIDs;

	/** Hold total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	private PaymentReferenceTO paymentReferenceTO;

	/** Hold the payment carrier code */
	private String payCarrier;

	/** Holds the lcc unique Id for interline or dry booking */
	private String lccUniqueId;

	/**
	 * Holds original paymentTnxId when the operation is a refund
	 */
	private Integer paymentTnxId;

	public String getLoyaltyMemberAccountId() {
		return loyaltyMemberAccountId;
	}

	public void setLoyaltyMemberAccountId(String loyaltyMemberAccountId) {
		this.loyaltyMemberAccountId = loyaltyMemberAccountId;
	}

	public String[] getRewardIDs() {
		return rewardIDs;
	}

	public void setRewardIDs(String[] rewardIDs) {
		this.rewardIDs = rewardIDs;
	}

	@Override
	public String getLccUniqueId() {
		return this.lccUniqueId;
	}

	@Override
	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	@Override
	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	@Override
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public PayCurrencyDTO getPayCurrencyDTO() {
		return this.payCurrencyDTO;
	}

	@Override
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	public PaymentReferenceTO getPaymentReferenceTO() {
		return paymentReferenceTO;
	}

	public void setPaymentReferenceTO(PaymentReferenceTO paymentReferenceTO) {
		this.paymentReferenceTO = paymentReferenceTO;
	}

	@Override
	public String getPayCarrier() {
		return this.payCarrier;
	}

	@Override
	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;
	}

	@Override
	public Integer getPaymentTnxId() {
		return this.paymentTnxId;
	}

	@Override
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;
	}

}
