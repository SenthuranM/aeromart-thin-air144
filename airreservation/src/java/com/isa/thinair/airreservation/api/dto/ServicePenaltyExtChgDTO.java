package com.isa.thinair.airreservation.api.dto;

/**
 * 
 * @author rumesh
 * 
 */
public class ServicePenaltyExtChgDTO extends ExternalChgDTO {
	private static final long serialVersionUID = 8451703768086528370L;

	private String flightRefNumber;

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		ServicePenaltyExtChgDTO clone = new ServicePenaltyExtChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setFlightRefNumber(this.getFlightRefNumber());

		return clone;
	}

}
