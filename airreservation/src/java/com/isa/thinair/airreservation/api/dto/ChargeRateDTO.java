package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.ChargeRateBasis.ChargeBasisEnum;

/**
 * @author eric
 * 
 */
public class ChargeRateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds charge rate id */
	private Integer chgRateId;

	/** Holds the ratio value */
	private BigDecimal ratioValue = BigDecimal.ZERO;

	private BigDecimal chargeValueMin;

	private BigDecimal chargeValueMax;

	private ChargeBasisEnum chargeBasis;

	private int journeyType;

	private boolean valid;
	
	private String cabinClassCode;

	public ChargeRateDTO() {
		super();
	}

	public ChargeRateDTO(int journeyType) {
		super();
		this.journeyType = journeyType;
	}

	public Integer getChgRateId() {
		return chgRateId;
	}

	public void setChgRateId(Integer chgRateId) {
		this.chgRateId = chgRateId;
	}

	public BigDecimal getRatioValue() {
		return ratioValue;
	}

	public void setRatioValue(BigDecimal ratioValue) {
		this.ratioValue = ratioValue;
	}

	public BigDecimal getChargeValueMin() {
		return chargeValueMin;
	}

	public void setChargeValueMin(BigDecimal chargeValueMin) {
		this.chargeValueMin = chargeValueMin;
	}

	public BigDecimal getChargeValueMax() {
		return chargeValueMax;
	}

	public void setChargeValueMax(BigDecimal chargeValueMax) {
		this.chargeValueMax = chargeValueMax;
	}

	public ChargeBasisEnum getChargeBasis() {
		return chargeBasis;
	}

	public void setChargeBasis(ChargeBasisEnum chargeBasis) {
		this.chargeBasis = chargeBasis;
	}

	public int getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(int journeyType) {
		this.journeyType = journeyType;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isRatioInPercentage() {
		return chargeBasis.isPercentage();
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		ChargeRateDTO clone = new ChargeRateDTO();

		clone.setChargeBasis(this.getChargeBasis());
		clone.setChargeValueMax(this.getChargeValueMax());
		clone.setChargeValueMin(this.getChargeValueMin());
		clone.setChgRateId(this.getChgRateId());
		clone.setJourneyType(this.getJourneyType());
		clone.setRatioValue(this.getRatioValue());
		clone.setValid(this.isValid());
		clone.setCabinClassCode(this.getCabinClassCode());

		return clone;
	}

}
