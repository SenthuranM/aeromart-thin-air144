package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

public class ReleaseTimeUtil {

	/**
	 * Returns the flight segment information
	 * 
	 * @param ondFareDTOs
	 * @return
	 */
	public static Collection<FlightSegmentDTO> getFlightDepartureInfo(Collection<OndFareDTO> ondFareDTOs) {
		Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();

		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			flightSegmentDTOs.addAll(ondFareDTO.getSegmentsMap().values());
		}

		return flightSegmentDTOs;
	}

	/**
	 * Returns the flight segment information
	 * 
	 * @param ondFareDTOs
	 * @return
	 */
	public static Collection<FlightSegmentDTO> getFlightDepartureInfo(List<FlightSegmentTO> flightSegmentTOs) {
		Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			flightSegmentDTOs.add(flightSegmentTO.convert());
		}

		return flightSegmentDTOs;
	}

	/**
	 * Returns the release time stamp
	 * 
	 * @param colFlightSegmentDTOs
	 * @param privilegesKeys
	 * @param enforcePrivilegesCheck
	 * @param appIndicator
	 * @return
	 * @throws ModuleException
	 */
	public static Date getReleaseTimeStamp(Collection<FlightSegmentDTO> colFlightSegmentDTOs, Collection<String> privilegesKeys,
			boolean enforcePrivilegesCheck, String agentCode, String appIndicator) throws ModuleException {

		return getReleaseTimeStamp(colFlightSegmentDTOs, privilegesKeys, enforcePrivilegesCheck, agentCode, appIndicator,
				new OnHoldReleaseTimeDTO());
	}

	@SuppressWarnings("unchecked")
	public static Date getReleaseTimeStamp(Collection<FlightSegmentDTO> colFlightSegmentDTOs, Collection<String> privilegesKeys,
			boolean enforcePrivilegesCheck, String agentCode, String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		Map<String, InterlinedAirLineTO> interlinedAirlinesMap = globalConfig.getActiveInterlinedCarriersMap();
		String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		Object[] filteredSegments = getFilteredFlightSegments(colFlightSegmentDTOs, interlinedAirlinesMap.keySet(),
				defaultCarrierCode);

		Collection<FlightSegmentDTO> ownFlightSegmentDTOs = (Collection<FlightSegmentDTO>) filteredSegments[0];
		Collection<FlightSegmentDTO> interlinedFlightSegmentDTOs = (Collection<FlightSegmentDTO>) filteredSegments[1];

		// Ofcause any flight should have it's own flight segments
		if (ownFlightSegmentDTOs.size() > 0) {
			Date ownReleaseTime = getOwnReleaseTime(ownFlightSegmentDTOs, privilegesKeys, enforcePrivilegesCheck, agentCode,
					appIndicator, onHoldReleaseTimeDTO);

			if (ownReleaseTime == null) {
				return null;
			} else {
				if (interlinedFlightSegmentDTOs.size() > 0) {
					return getInterlinedReleaseTime(ownReleaseTime, interlinedFlightSegmentDTOs, interlinedAirlinesMap);
				} else {
					return ownReleaseTime;
				}
			}
			// Remember this is via interlined segments left over case.
		} else if (interlinedFlightSegmentDTOs.size() > 0) {
			return getInterlinedReleaseTime(null, interlinedFlightSegmentDTOs, interlinedAirlinesMap);
		} else {
			return null;
		}
	}

	/**
	 * Returns the filtered flight segments
	 * 
	 * @param colFlightSegmentDTOs
	 * @param interlinedCCCodes
	 * @param defaultCarrierCode
	 * @return
	 */
	private static Object[] getFilteredFlightSegments(Collection<FlightSegmentDTO> colFlightSegmentDTOs,
			Collection<String> interlinedCCCodes, String defaultCarrierCode) {
		Collection<FlightSegmentDTO> ownFlightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		Collection<FlightSegmentDTO> interlinedFlightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		FlightSegmentDTO flightSegmentDTO;
		String carrierCode;

		for (Iterator<FlightSegmentDTO> iter = colFlightSegmentDTOs.iterator(); iter.hasNext();) {
			flightSegmentDTO = (FlightSegmentDTO) iter.next();

			carrierCode = AppSysParamsUtil.extractCarrierCode(flightSegmentDTO.getFlightNumber());

			if (defaultCarrierCode.equals(carrierCode) || !interlinedCCCodes.contains(carrierCode)) {
				ownFlightSegmentDTOs.add(flightSegmentDTO);
			} else {
				interlinedFlightSegmentDTOs.add(flightSegmentDTO);
			}
		}

		return new Object[] { ownFlightSegmentDTOs, interlinedFlightSegmentDTOs };
	}

	/**
	 * Returns the own carrier's release time
	 * 
	 * @param ownFlightSegmentDTOs
	 * @param privilegesKeys
	 * @param enforcePrivilegesCheck
	 * @param appIndicator
	 * @return
	 * @throws ModuleException
	 */
	private static Date getOwnReleaseTime(Collection<FlightSegmentDTO> ownFlightSegmentDTOs, Collection<String> privilegesKeys,
			boolean enforcePrivilegesCheck, String agentCode, String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {
		List<Date> releaseTimes = new ArrayList<Date>();
		FlightSegmentDTO flightSegmentDTO;
		Date tmpReleaseTime;

		if (onHoldReleaseTimeDTO == null || onHoldReleaseTimeDTO.getOndCode() == null) {
			onHoldReleaseTimeDTO = getOnHoldReleaseTimeDTO(ownFlightSegmentDTOs);
		}

		onHoldReleaseTimeDTO.setModuleCode(appIndicator);

		for (Iterator<FlightSegmentDTO> itr = ownFlightSegmentDTOs.iterator(); itr.hasNext();) {
			flightSegmentDTO = (FlightSegmentDTO) itr.next();

			tmpReleaseTime = getReleaseTime(privilegesKeys, flightSegmentDTO.getDepartureDateTimeZulu(), enforcePrivilegesCheck,
					agentCode, appIndicator, onHoldReleaseTimeDTO);

			if (tmpReleaseTime == null) {
				return null;
			} else {
				releaseTimes.add(tmpReleaseTime);
			}
		}

		Collections.sort(releaseTimes);
		return releaseTimes.get(0);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static OnHoldReleaseTimeDTO getOnHoldReleaseTimeDTO(Collection<FlightSegmentDTO> flightDepInfo) {
		OnHoldReleaseTimeDTO holdReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		if (flightDepInfo != null) {
			Collections.sort((ArrayList) flightDepInfo);
			for (FlightSegmentDTO flightsegment : flightDepInfo) {
				// TODO Remove bus segments
				holdReleaseTimeDTO.setOndCode(flightsegment.getSegmentCode());
				holdReleaseTimeDTO.setFlightDepartureDate(flightsegment.getDepartureDateTimeZulu());
				break;
			}
		}
		holdReleaseTimeDTO.setBookingDate(new Date());
		// TODO add other data
		return holdReleaseTimeDTO;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static OnHoldReleaseTimeDTO getOnHoldReleaseTimeDTOFromOndFares(Collection<OndFareDTO> ondFareDTOList)
			throws ModuleException {
		OnHoldReleaseTimeDTO holdReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		FlightSegmentDTO firstDeptFltSeg = null;
		if (ondFareDTOList != null) {
			for (OndFareDTO ondFareDTO : ondFareDTOList) {
				if (ondFareDTO.getSegmentsMap() != null) {
					for (FlightSegmentDTO segment : ondFareDTO.getSegmentsMap().values()) {
						if (firstDeptFltSeg == null) {
							firstDeptFltSeg = segment;
						} else {
							if (firstDeptFltSeg.getDepartureDateTimeZulu().after(segment.getDepartureDateTimeZulu())) {
								firstDeptFltSeg = segment;
							}
						}
					}

				}
			}
		}
		if (firstDeptFltSeg != null) {
			holdReleaseTimeDTO.setBookingDate(new Date());
			holdReleaseTimeDTO.setOndCode(firstDeptFltSeg.getSegmentCode());
			holdReleaseTimeDTO.setFlightDepartureDate(firstDeptFltSeg.getDepartureDateTimeZulu());
			holdReleaseTimeDTO.setDomestic(firstDeptFltSeg.isDomesticFlight());
			String bookingClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
			String cabinClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
			int fareRuleId = -1;
			for (OndFareDTO ondFareDTOTmp : ondFareDTOList) {
				if (ondFareDTOTmp != null && ondFareDTOTmp.getOndCode() != null) {
					if (ondFareDTOTmp.getOndCode().split("/")[0].equalsIgnoreCase(firstDeptFltSeg.getDepartureAirportCode())) {
						bookingClass = ondFareDTOTmp.getFareSummaryDTO().getBookingClassCode();
						fareRuleId = ondFareDTOTmp.getFareSummaryDTO().getFareRuleID();
						cabinClass = ondFareDTOTmp.getCCForChargeQuote();
						break;
					}
				}
			}

			holdReleaseTimeDTO.setBookingClass(bookingClass);
			holdReleaseTimeDTO.setFareRuleId(fareRuleId);
			holdReleaseTimeDTO.setCabinClass(cabinClass);
		}
		return holdReleaseTimeDTO;
	}

	public static long getOnholdBufferTime(String agentCode, String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {
		long ohdBufferInMillis = 0;

		boolean isApplyNewReleaseTime = AppSysParamsUtil.isApplyRouteBookingClassLevelOnholdReleaseTime();
		OnHold ohdType = null;
		boolean isDomesticFlight = false;
		String onHoldDurationInBufferHHMM = "";

		if (onHoldReleaseTimeDTO != null)
			isDomesticFlight = onHoldReleaseTimeDTO.isDomestic();

		if (AppIndicatorEnum.APP_XBE.toString().equals(appIndicator)) {
			ohdType = OnHold.AGENTS;
		} else if (AppIndicatorEnum.APP_IBE.toString().equals(appIndicator)) {
			ohdType = OnHold.IBE;
		} else if (AppIndicatorEnum.APP_WS.toString().equals(appIndicator)) {
			ohdType = OnHold.APIAGENTS;
		} else if (AppIndicatorEnum.APP_ANDROID.toString().equals(appIndicator)) {
			ohdType = OnHold.ANDROID;
		} else if (AppIndicatorEnum.APP_IOS.toString().equals(appIndicator)) {
			ohdType = OnHold.IOS;
		} else {
			ohdType = OnHold.DEFAULT;
		}

		String onHoldDurationHHMM = AppSysParamsUtil.getOnHoldDuration(ohdType);

		if (isApplyNewReleaseTime) {
			Long releaseTimeMM = ReservationModuleUtils.getReservationQueryBD().getOnHoldReleaseTime(onHoldReleaseTimeDTO);

			if (releaseTimeMM != null) {
				long hours = releaseTimeMM / 60;
				long minutes = releaseTimeMM % 60;
				onHoldDurationHHMM = hours + ":" + minutes;
			} else if (isDomesticFlight) {
				onHoldDurationHHMM = "0:0";
			}
		}
		long onholdDurationInMillis = AppSysParamsUtil.getTimeInMillis(onHoldDurationHHMM);

		if (isDomesticFlight) {
			onHoldDurationInBufferHHMM = AppSysParamsUtil.getDomesticOnHoldDurationWithInOnHoldBufferTime(ohdType, agentCode);
		} else {
			onHoldDurationInBufferHHMM = AppSysParamsUtil.getOnHoldDurationWithInOnHoldBufferTime(ohdType, agentCode);
		}

		long onholdDurationInBufInMillis = AppSysParamsUtil.getTimeInMillis(onHoldDurationInBufferHHMM);

		String onHoldBufferStartCutOverTimeHHMM = AppSysParamsUtil.getOnHoldBufferStartCutOver(ohdType);

		long onholdBufferTimeInMills = AppSysParamsUtil.getTimeInMillis(onHoldBufferStartCutOverTimeHHMM);

		boolean hasBuffertimeOhd = onHoldReleaseTimeDTO.isHasBuffertimeOhd();

		String onHoldBufferEndCutOverTimeHHMM = AppSysParamsUtil.getOnHoldBufferEndCutOver(ohdType, agentCode);

		long flightClosureInMillis = AppSysParamsUtil.getTimeInMillis(onHoldBufferEndCutOverTimeHHMM);

		if (hasBuffertimeOhd) {
			ohdBufferInMillis = onholdDurationInBufInMillis + flightClosureInMillis;
		} else {
			ohdBufferInMillis = onholdDurationInMillis + onholdBufferTimeInMills;
		}

		return ohdBufferInMillis;
	}

	/**
	 * Returns Hold Release time calculated considering the flight departure, hold duration, mod buffer duration, flight
	 * closure gap. Returns null when cannot calculate the hold time (i.e. when reservation is not eligible for hold by
	 * user having given privilegesKey set in OnHoldReleaseTimeDTO hasBuffertimeOhd attr)
	 * 
	 * @param appIndicator
	 *            Holds which Application (IBE/XBE/WS) called this method
	 */
	public static Date getReleaseTime(Date departureDateTimeZulu, boolean enforcePrivilegesCheck, String agentCode,
			String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO) throws ModuleException {
		long timeToDepInMillis = getTimeToDepartureInMillis(departureDateTimeZulu);
		boolean isApplyNewReleaseTime = AppSysParamsUtil.isApplyRouteBookingClassLevelOnholdReleaseTime();
		OnHold ohdType = null;
		boolean isDomesticFlight = false;
		String onHoldDurationInBufferHHMM = "";

		if (onHoldReleaseTimeDTO != null)
			isDomesticFlight = onHoldReleaseTimeDTO.isDomestic();

		if (AppIndicatorEnum.APP_XBE.toString().equals(appIndicator)) {
			ohdType = OnHold.AGENTS;
		} else if (AppIndicatorEnum.APP_IBE.toString().equals(appIndicator)) {
			ohdType = OnHold.IBE;
		} else if (AppIndicatorEnum.APP_WS.toString().equals(appIndicator)) {
			ohdType = OnHold.APIAGENTS;
		} else if (AppIndicatorEnum.APP_IBEPAYMENT.toString().equals(appIndicator)) {
			ohdType = OnHold.IBEPAYMENT;
		} else if (AppIndicatorEnum.APP_OFFLINEPAYMENT.toString().equals(appIndicator)) {
			ohdType = OnHold.IBEOFFLINEPAYMENT;
		} else if (AppIndicatorEnum.APP_KIOSK.toString().equals(appIndicator)) {
			ohdType = OnHold.KIOSK;
		} else if (AppIndicatorEnum.APP_MYID.toString().equals(appIndicator)) {
			ohdType = OnHold.MYID;
		} else if (AppIndicatorEnum.APP_ANDROID.toString().equals(appIndicator)) {
			ohdType = OnHold.ANDROID;
		} else if (AppIndicatorEnum.APP_IOS.toString().equals(appIndicator)) {
			ohdType = OnHold.IOS;
		} else {
			ohdType = OnHold.DEFAULT;
		}

		String onHoldDurationHHMM = AppSysParamsUtil.getOnHoldDuration(ohdType); // RES_94 (OLD -- RES_04)

		if (isApplyNewReleaseTime && !ohdType.equals(OnHold.MYID)) {
			Long releaseTimeMM = ReservationModuleUtils.getReservationQueryBD().getOnHoldReleaseTime(onHoldReleaseTimeDTO);

			if (releaseTimeMM != null) {
				long hours = releaseTimeMM / 60;
				long minutes = releaseTimeMM % 60;
				onHoldDurationHHMM = hours + ":" + minutes;
			} else if (isDomesticFlight) {
				onHoldDurationHHMM = "0:0";
			}
		}
		long onholdDurationInMillis = AppSysParamsUtil.getTimeInMillis(onHoldDurationHHMM);

		if (isDomesticFlight) {
			onHoldDurationInBufferHHMM = AppSysParamsUtil.getDomesticOnHoldDurationWithInOnHoldBufferTime(ohdType, agentCode);
		} else {
			onHoldDurationInBufferHHMM = AppSysParamsUtil.getOnHoldDurationWithInOnHoldBufferTime(ohdType, agentCode);
		}

		long onholdDurationInBufInMillis = AppSysParamsUtil.getTimeInMillis(onHoldDurationInBufferHHMM);

		String onHoldBufferStartCutOverTimeHHMM = AppSysParamsUtil.getOnHoldBufferStartCutOver(ohdType); // RES_96
																											// (OLD
																											// ---
																											// RES_46)
																											// /Start
																											// Cut
																											// over
		long onholdBufferTimeInMills = AppSysParamsUtil.getTimeInMillis(onHoldBufferStartCutOverTimeHHMM);

		String onHoldBufferEndCutOverTimeHHMM = AppSysParamsUtil.getOnHoldBufferEndCutOver(ohdType, agentCode); // RES_97
																										// (OLD ---
																										// SCHDSERV_6)
																										// /END Cut
																										// over
		long flightClosureInMillis = AppSysParamsUtil.getTimeInMillis(onHoldBufferEndCutOverTimeHHMM);

		Date dtReleaseTime = null;
		boolean hasBuffertimeOhd = onHoldReleaseTimeDTO.isHasBuffertimeOhd();

		if (timeToDepInMillis >= onholdDurationInMillis + onholdBufferTimeInMills) {
			dtReleaseTime = ReservationApiUtils.calculateReleaseTime(onHoldDurationHHMM);
		} else if (timeToDepInMillis >= onholdBufferTimeInMills + onholdDurationInBufInMillis) {
			dtReleaseTime = calculateBufferReleaseTime(departureDateTimeZulu, ohdType);
			if (hasBuffertimeOhd) {
				dtReleaseTime = BeanUtils.extendByMinutes(dtReleaseTime,
						AppSysParamsUtil.getTotalMinutes(onHoldDurationInBufferHHMM));
			}
		} else if (timeToDepInMillis < onholdBufferTimeInMills + onholdDurationInBufInMillis
				&& timeToDepInMillis >= onholdBufferTimeInMills) {
			if (hasBuffertimeOhd) {
				dtReleaseTime = ReservationApiUtils.calculateReleaseTimeInBufTime(onHoldDurationInBufferHHMM);
			} else {
				dtReleaseTime = calculateBufferReleaseTime(departureDateTimeZulu, ohdType);
			}
		} else if (!enforcePrivilegesCheck || hasBuffertimeOhd) {
			if (timeToDepInMillis >= onholdDurationInBufInMillis + flightClosureInMillis) {
				dtReleaseTime = ReservationApiUtils.calculateReleaseTimeInBufTime(onHoldDurationInBufferHHMM);
			} else if (timeToDepInMillis >= flightClosureInMillis) {
				dtReleaseTime = calculateClosureReleaseTime(departureDateTimeZulu, ohdType, agentCode);
			}
		}

		// TODO get Zulu time - Right now not applicable because of other dependency
		if (dtReleaseTime != null) {
			Date currentDateTime = Calendar.getInstance().getTime();
			if (dtReleaseTime.getTime() <= currentDateTime.getTime())
				dtReleaseTime = null;
		}

		return dtReleaseTime;
	}

	/**
	 * Returns Hold Release time calculated considering the flight departure, hold duration, mod buffer duration, fligth
	 * closure gap & privileges Returns null when cannot calculate the hold time (i.e. when reservation is not eligible
	 * for hold by user having given privilegesKeys)
	 * 
	 * @param enforcePrivilegesCheck
	 *            set true discarding privilegesKeys
	 * @param appIndicator
	 *            Holds which Application (IBE/XBE/WS) called this method
	 */
	public static Date getReleaseTime(Collection<String> privilegesKeys, Date departureDateTimeZulu,
			boolean enforcePrivilegesCheck, String agentCode, String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {
		onHoldReleaseTimeDTO.setHasBuffertimeOhd(AuthorizationsUtil.hasPrivilege(privilegesKeys, "xbe.res.make.onhold.buffer"));
		return getReleaseTime(departureDateTimeZulu, enforcePrivilegesCheck, agentCode, appIndicator, onHoldReleaseTimeDTO);
	}

	public static long getTimeToDepartureInMillis(Date departureDateTimeZulu) {
		long milTimeToDepart = 0;
		Date currentTime = getCurrentZuluDateTime();
		Calendar calDepTime = Calendar.getInstance();
		calDepTime.setTime(departureDateTimeZulu);
		milTimeToDepart = calDepTime.getTimeInMillis() - currentTime.getTime();

		return milTimeToDepart;
	}

	public static Date getCurrentZuluDateTime() {
		return getCurrentDateTime("GMT");
	}

	private static Date getCurrentDateTime(String strTimeZone) {
		TimeZone timeZone = null;
		Calendar cal = Calendar.getInstance();

		if (strTimeZone == null || "".equals(strTimeZone)) {
			timeZone = TimeZone.getDefault();
		} else {
			TimeZone.getTimeZone(strTimeZone);
		}

		cal.setTimeZone(timeZone);

		return cal.getTime();
	}

	/**
	 * Calculates Hold Releasetime considering the modification buffertime
	 */
	private static Date calculateBufferReleaseTime(Date departureDateTimeZulu) {
		Date dtReleaseTime = null;
		String onHoldStartCutOver = AppSysParamsUtil.getOnHoldBufferStartCutOver(OnHold.AGENTS);
		String[] onHoldStartCutOverArr = onHoldStartCutOver.split(":");

		Calendar calReleaseDate = Calendar.getInstance();
		calReleaseDate.setTime(departureDateTimeZulu);
		calReleaseDate.add(Calendar.HOUR, -Integer.parseInt(onHoldStartCutOverArr[0]));
		calReleaseDate.add(Calendar.MINUTE, -Integer.parseInt(onHoldStartCutOverArr[1]));
		calReleaseDate.set(Calendar.SECOND, 0);
		calReleaseDate.set(Calendar.MILLISECOND, 0);
		dtReleaseTime = calReleaseDate.getTime();

		return dtReleaseTime;
	}

	private static Date calculateBufferReleaseTime(Date departureDateTimeZulu, OnHold onHoldApp) {
		Date dtReleaseTime = null;
		String onHoldStartCutOver = AppSysParamsUtil.getOnHoldBufferStartCutOver(onHoldApp);
		String[] onHoldStartCutOverArr = onHoldStartCutOver.split(":");

		Calendar calReleaseDate = Calendar.getInstance();
		calReleaseDate.setTime(departureDateTimeZulu);
		calReleaseDate.add(Calendar.HOUR, -Integer.parseInt(onHoldStartCutOverArr[0]));
		calReleaseDate.add(Calendar.MINUTE, -Integer.parseInt(onHoldStartCutOverArr[1]));
		calReleaseDate.set(Calendar.SECOND, 0);
		calReleaseDate.set(Calendar.MILLISECOND, 0);
		dtReleaseTime = calReleaseDate.getTime();

		return dtReleaseTime;
	}

	/**
	 * Calculates Hold Releasetime considering the flight closure gap
	 */
	public static Date calculateClosureReleaseTime(Date departureDateTimeZulu) {
		Date dtReleaseTime = null;

		String onHoldBufferEndCutOverTimeHours = AppSysParamsUtil.getOnHoldBufferEndCutOver(OnHold.AGENTS, null).split(":")[0];
		String onHoldBufferEndCutOverTimeMinute = AppSysParamsUtil.getOnHoldBufferEndCutOver(OnHold.AGENTS, null).split(":")[1];

		Calendar calReleaseDate = Calendar.getInstance();
		calReleaseDate.setTime(departureDateTimeZulu);
		calReleaseDate.add(Calendar.HOUR, -Integer.parseInt(onHoldBufferEndCutOverTimeHours));
		calReleaseDate.add(Calendar.MINUTE, -Integer.parseInt(onHoldBufferEndCutOverTimeMinute));
		calReleaseDate.set(Calendar.SECOND, 0);
		calReleaseDate.set(Calendar.MILLISECOND, 0);
		dtReleaseTime = calReleaseDate.getTime();

		return dtReleaseTime;
	}

	public static Date calculateClosureReleaseTime(Date departureDateTimeZulu, OnHold onHoldApp, String agentCode) {
		Date dtReleaseTime = null;

		String onHoldBufferEndCutOverTimeHours = AppSysParamsUtil.getOnHoldBufferEndCutOver(onHoldApp, agentCode).split(":")[0];
		String onHoldBufferEndCutOverTimeMinute = AppSysParamsUtil.getOnHoldBufferEndCutOver(onHoldApp, agentCode).split(":")[1];

		Calendar calReleaseDate = Calendar.getInstance();
		calReleaseDate.setTime(departureDateTimeZulu);
		calReleaseDate.add(Calendar.HOUR, -Integer.parseInt(onHoldBufferEndCutOverTimeHours));
		calReleaseDate.add(Calendar.MINUTE, -Integer.parseInt(onHoldBufferEndCutOverTimeMinute));
		calReleaseDate.set(Calendar.SECOND, 0);
		calReleaseDate.set(Calendar.MILLISECOND, 0);
		dtReleaseTime = calReleaseDate.getTime();

		return dtReleaseTime;
	}

	/**
	 * Returns the interlined release time
	 * 
	 * @param ownReleaseTime
	 * @param interlinedFlightSegmentDTOs
	 * @param interlinedAirlinesMap
	 * @return
	 */
	private static Date getInterlinedReleaseTime(Date ownReleaseTime, Collection<FlightSegmentDTO> interlinedFlightSegmentDTOs,
			Map<String, InterlinedAirLineTO> interlinedAirlinesMap) {
		Date maxAllowedReleaseTime = null;
		FlightSegmentDTO flightSegmentDTO;
		String carrierCode;
		InterlinedAirLineTO airLine;

		for (Iterator<FlightSegmentDTO> iter = interlinedFlightSegmentDTOs.iterator(); iter.hasNext();) {
			flightSegmentDTO = (FlightSegmentDTO) iter.next();

			carrierCode = AppSysParamsUtil.extractCarrierCode(flightSegmentDTO.getFlightNumber());
			airLine = (InterlinedAirLineTO) interlinedAirlinesMap.get(carrierCode);

			Date interlineCutoverDateTime = new Date(flightSegmentDTO.getDepartureDateTimeZulu().getTime()
					- (airLine.getInterlineCutOverInMinutes() * 60 * 1000));

			if (ownReleaseTime == null || interlineCutoverDateTime.before(ownReleaseTime)) {
				if (interlineCutoverDateTime.before(new Date())) {
					return null;
				} else {
					if (maxAllowedReleaseTime == null || maxAllowedReleaseTime.after(interlineCutoverDateTime)) {
						maxAllowedReleaseTime = interlineCutoverDateTime;
					}
				}
			} else {
				maxAllowedReleaseTime = ownReleaseTime;
			}
		}

		return maxAllowedReleaseTime;

	}

	/**
	 * Calculates maximum allowed release time when extending hold (implicitly assumes user has buffer time onhold
	 * privilege)
	 * 
	 * @param colFlightSegmentDTOs
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Date getMaxAllowedReleaseTimeZulu(Collection<FlightSegmentDTO> colFlightSegmentDTOs) {

		Date maxAllowedReleaseTime = null;

		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		Map<String, InterlinedAirLineTO> interlinedAirlinesMap = globalConfig.getActiveInterlinedCarriersMap();
		String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		Object[] filteredSegments = getFilteredFlightSegments(colFlightSegmentDTOs, interlinedAirlinesMap.keySet(),
				defaultCarrierCode);

		Date earliestDepartingZuluTime = null;
		Date earliestFltClosureTime = null;
		Date earliestInterlineCutoverTime = null;

		Collection<FlightSegmentDTO> ownFlightSegmentDTOs = (Collection<FlightSegmentDTO>) filteredSegments[0];
		Collection<FlightSegmentDTO> interlinedFlightSegmentDTOs = (Collection<FlightSegmentDTO>) filteredSegments[1];

		if (ownFlightSegmentDTOs.size() > 0) {
			FlightSegmentDTO flightSegmentDTO;
			for (Iterator<FlightSegmentDTO> itr = ownFlightSegmentDTOs.iterator(); itr.hasNext();) {
				flightSegmentDTO = (FlightSegmentDTO) itr.next();
				if (earliestDepartingZuluTime == null
						|| earliestDepartingZuluTime.after(flightSegmentDTO.getDepartureDateTimeZulu())) {
					earliestFltClosureTime = calculateClosureReleaseTime(flightSegmentDTO.getDepartureDateTimeZulu());
					earliestDepartingZuluTime = flightSegmentDTO.getDepartureDateTimeZulu();
				}
			}
			maxAllowedReleaseTime = earliestFltClosureTime;
		}

		if (interlinedFlightSegmentDTOs.size() > 0) {
			earliestInterlineCutoverTime = getInterlinedReleaseTime(null, interlinedFlightSegmentDTOs, interlinedAirlinesMap);

			if (earliestInterlineCutoverTime == null) {
				maxAllowedReleaseTime = null;
			} else if (maxAllowedReleaseTime == null || maxAllowedReleaseTime.after(earliestInterlineCutoverTime)) {
				maxAllowedReleaseTime = earliestInterlineCutoverTime;
			}
		}

		return maxAllowedReleaseTime;
	}

	/**
	 * Returns the confirmed departure segments
	 * 
	 * @param colReservationSegmentDTO
	 * @return
	 */
	public static Collection<FlightSegmentDTO> getConfirmedDepartureSegments(
			Collection<ReservationSegmentDTO> colReservationSegmentDTO) {
		Collection<FlightSegmentDTO> colFlightSegmentDTO = new ArrayList<FlightSegmentDTO>();
		FlightSegmentDTO flightSegmentDTO;
		ReservationSegmentDTO reservationSegmentDTO;

		for (Iterator<ReservationSegmentDTO> iter = colReservationSegmentDTO.iterator(); iter.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) iter.next();

			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
				flightSegmentDTO = new FlightSegmentDTO();
				flightSegmentDTO.setFlightNumber(reservationSegmentDTO.getFlightNo());
				flightSegmentDTO.setDepartureDateTimeZulu(reservationSegmentDTO.getZuluDepartureDate());
				flightSegmentDTO.setSegmentCode(reservationSegmentDTO.getSegmentCode());

				colFlightSegmentDTO.add(flightSegmentDTO);
			}

		}

		return colFlightSegmentDTO;
	}

	/**
	 * Returns the first flight segment's departure datetime in Zulu
	 */
	public static Date getFirstFlightDepartureTimeZulu(Collection<OndFareDTO> ondFareDTOs) {
		Date firstDepZulu = null;
		FlightSegmentDTO flightSegmentDTO = getFirstFlightDepartureFlightSegment(ondFareDTOs);

		if (flightSegmentDTO != null) {
			firstDepZulu = flightSegmentDTO.getDepartureDateTimeZulu();
		}

		return firstDepZulu;
	}

	public static FlightSegmentDTO getFirstFlightDepartureFlightSegment(Collection<OndFareDTO> ondFareDTOs) {
		FlightSegmentDTO fltSeg = null;
		Collection<FlightSegmentDTO> flightSegmentDTOs = null;
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			flightSegmentDTOs = ondFareDTO.getSegmentsMap().values();
			for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
				if (fltSeg == null || fltSeg.getDepartureDateTimeZulu().after(flightSegmentDTO.getDepartureDateTimeZulu())) {
					fltSeg = flightSegmentDTO;
				}
			}
		}
		return fltSeg;
	}

	@SuppressWarnings("unchecked")
	public static Object[] getRestrictedBufferTime(Collection<FlightSegmentDTO> colFlightSegmentDTOs) throws ModuleException {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		Map<String, InterlinedAirLineTO> interlinedAirlinesMap = globalConfig.getActiveInterlinedCarriersMap();
		String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		Object[] filteredSegments = getFilteredFlightSegments(colFlightSegmentDTOs, interlinedAirlinesMap.keySet(),
				defaultCarrierCode);

		Collection<FlightSegmentDTO> ownFlightSegmentDTOs = (Collection<FlightSegmentDTO>) filteredSegments[0];
		Collection<FlightSegmentDTO> interlineFlightSegmentDTOs = (Collection<FlightSegmentDTO>) filteredSegments[1];
		Object[] ohdTimes = null;
		// Ofcause any flight should have it's own flight segments
		if (ownFlightSegmentDTOs.size() > 0) {
			ohdTimes = getRestrictedBuffer(ownFlightSegmentDTOs);
		} else if (interlineFlightSegmentDTOs.size() > 0) {
			ohdTimes = getRestrictedBuffer(interlineFlightSegmentDTOs);
		}
		return ohdTimes;
	}

	private static Object[] getRestrictedBuffer(Collection<FlightSegmentDTO> ownFlightSegmentDTOs) throws ModuleException {
		List<Date> buffStartTime = new ArrayList<Date>();
		List<Date> buffEndTime = new ArrayList<Date>();
		FlightSegmentDTO flightSegmentDTO;
		Date tmpStart;
		Date tmpEnd;

		for (Iterator<FlightSegmentDTO> itr = ownFlightSegmentDTOs.iterator(); itr.hasNext();) {
			flightSegmentDTO = (FlightSegmentDTO) itr.next();
			tmpStart = calculateBufferReleaseTime(flightSegmentDTO.getDepartureDateTimeZulu());
			tmpEnd = calculateClosureReleaseTime(flightSegmentDTO.getDepartureDateTimeZulu());

			if (tmpStart != null) {
				buffStartTime.add(tmpStart);
			}
			if (tmpEnd != null) {
				buffEndTime.add(tmpEnd);
			}
		}

		Collections.sort(buffStartTime);
		Collections.sort(buffEndTime);
		Object[] arrObj = new Object[2];
		arrObj[0] = buffStartTime.get(0);
		arrObj[1] = buffEndTime.get(0);
		return arrObj;
	}
}
