package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.BasicDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class UNZ extends EDISegment {

	private String E0036;
	private String E0020;
	
	public UNZ(){
		super(EDISegmentTag.UNZ);
	}

	public String getE0036() {
		return E0036;
	}

	public void setE0036(String e0036) {
		E0036 = e0036;
	}

	public String getE0020() {
		return E0020;
	}

	public void setE0020(String e0020) {
		E0020 = e0020;
	}

	@Override
	public MessageSegment build() {
		BasicDataElement V0036 = new Numeric(EDI_ELEMENT.E0036, E0036);
		BasicDataElement V0020 = new AlphaNumeric(EDI_ELEMENT.E0020, E0020);
		
		addEDIDataElement(V0036);
		addEDIDataElement(V0020);

		return this;
	}
}
