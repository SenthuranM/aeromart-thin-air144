package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class ReservationLiteDTO implements Serializable {
	private static final long serialVersionUID = -785005878511859610L;

	private String pnr;
	private long version;
	private Integer autoCancellationId;

	public ReservationLiteDTO() {

	}

	public ReservationLiteDTO(String pnr, long version, Integer autoCancallationId) {
		super();
		this.pnr = pnr;
		this.version = version;
		this.autoCancellationId = autoCancallationId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		if (!(obj instanceof ReservationLiteDTO)) {
			return false;
		}
		ReservationLiteDTO other = (ReservationLiteDTO) obj;
		if (pnr == null) {
			if (other.pnr != null) {
				return false;
			}
		} else if (!pnr.equals(other.pnr)) {
			return false;
		}
		return true;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

}
