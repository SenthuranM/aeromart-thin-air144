package com.isa.thinair.airreservation.api.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

import com.isa.thinair.airreservation.api.model.TermsTemplate;
import com.isa.thinair.airreservation.api.model.TermsTemplateAudit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.TermsTemplateAuditDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateDTO;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Class containing utility methods related to {@link TermsTemplate} manipulation.
 * 
 * @author thihara
 * 
 */
public class TermsTemplateUtils {

	/**
	 * Transforms a {@link TermsTemplate} into a {@link TermsTemplateDTO}. Hex to normal conversion is also performed by
	 * thus method.
	 * 
	 * @param termsTemplate
	 *            The {@link TermsTemplate} to be transformed.
	 * @return An instance of {@link TermsTemplateDTO} populated with the passed termsTemplate.
	 */
	public static TermsTemplateDTO toTermsTemplateDTO(TermsTemplate termsTemplate) {
		TermsTemplateDTO tmp = new TermsTemplateDTO();

		tmp.setCarriers(termsTemplate.getCarriers());
		tmp.setLanguage(termsTemplate.getLanguageCode());

		// Translate DB saved template content from hex to normal text
		tmp.setTemplateContent(StringUtil.getUnicode(termsTemplate.getTemplateContent()));

		tmp.setTemplateID(termsTemplate.getTemplateID());
		tmp.setTemplateName(termsTemplate.getTemplateName());
		tmp.setVersion(termsTemplate.getVersion());

		if (termsTemplate.getAuditHistory() != null && !termsTemplate.getAuditHistory().isEmpty()) {
			TermsTemplateAudit latestAudit = new TreeSet<TermsTemplateAudit>(termsTemplate.getAuditHistory()).last();
			tmp.setLastModifiedUser(latestAudit.getUserId());
			tmp.setLastModifiedDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(latestAudit.getModifiedTime()));
		}
		return tmp;
	}

	/**
	 * Transforms a {@link TermsTemplateDTO} to a {@link TermsTemplate}. Normal to hex conversion is also performed by
	 * this method.
	 * 
	 * @param termsTemplateDTO
	 *            The {@link TermsTemplateDTO} to be transformed.
	 * 
	 * @return An instance of {@link TermsTemplate} populated with the data in the passed termsTemplateDTO
	 */
	public static TermsTemplate toTermsTemplate(TermsTemplateDTO termsTemplateDTO) {
		TermsTemplate tmp = new TermsTemplate();

		tmp.setCarriers(termsTemplateDTO.getCarriers());
		tmp.setLanguageCode(termsTemplateDTO.getLanguage());

		// Translate normal text to hex so it can be saved in the database.
		tmp.setTemplateContent(StringUtil.convertToHex(termsTemplateDTO.getTemplateContent()));

		tmp.setTemplateID(termsTemplateDTO.getTemplateID());
		tmp.setTemplateName(termsTemplateDTO.getTemplateName());
		tmp.setVersion(termsTemplateDTO.getVersion());

		return tmp;
	}

	/**
	 * Transforms a Collection of {@link TermsTemplate} into a Collection of {@link TermsTemplateDTO}. This method calls
	 * {@link #toTermsTemplateDTO(TermsTemplate)} underneath.
	 * 
	 * @param termsTemplates
	 *            Collection of {@link TermsTemplate} to be transformed.
	 * @return A transformed Collection of {@link TermsTemplateDTO}s.
	 */
	public static Collection<TermsTemplateDTO> toTermsTemplateDTOCOllection(Collection<TermsTemplate> termsTemplates) {
		ArrayList<TermsTemplateDTO> dtos = new ArrayList<TermsTemplateDTO>();
		for (TermsTemplate template : termsTemplates) {
			dtos.add(toTermsTemplateDTO(template));
		}
		return dtos;
	}

	/**
	 * Transforms a {@link Page} of {@link TermsTemplate} into a {@link Page} of {@link TermsTemplateDTO}. This method
	 * calls {@link #toTermsTemplateDTOCOllection(TermsTemplate)} underneath.
	 * 
	 * @param termsTemplates
	 *            {@link Page} of {@link TermsTemplate} to be transformed.
	 * @return A transformed {@link Page} of {@link TermsTemplateDTO}s.
	 */
	public static Page<TermsTemplateDTO> convertPageToDTO(Page<TermsTemplate> termsTemplatePage) {

		Page<TermsTemplateDTO> dtoPage = new Page<TermsTemplateDTO>(termsTemplatePage.getTotalNoOfRecords(),
				termsTemplatePage.getStartPosition(), termsTemplatePage.getEndPosition(),
				toTermsTemplateDTOCOllection(termsTemplatePage.getPageData()));

		return dtoPage;
	}

	/**
	 * Transform an {@link TermsTemplateAuditDTO} into an {@link TermsTemplateAudit}
	 * 
	 * @param auditDTO
	 *            The {@link TermsTemplateAuditDTO} to be transformed.
	 * 
	 * @return An instance of {@link TermsTemplateAudit} populated with the data in the passed auditDTO
	 */
	public static TermsTemplateAudit toTermsTemplateAudit(TermsTemplateAuditDTO auditDTO) {
		TermsTemplateAudit audit = new TermsTemplateAudit();
		audit.setIpAddress(auditDTO.getIpAddress());
		audit.setModifiedTime(auditDTO.getModifiedTime());
		audit.setUserId(auditDTO.getUserId());
		return audit;
	}
}
