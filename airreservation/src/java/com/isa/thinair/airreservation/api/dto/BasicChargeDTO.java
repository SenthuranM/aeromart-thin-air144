package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.api.dto.ChargeRateBasis.ChargeBasisEnum;

/**
 * @author eric
 * 
 */
public class BasicChargeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Holds charge group code */
	private String chgGrpCode;

	/** Holds the charge code */
	private String chargeCode;

	/** Holds the charge description */
	private String chargeDescription;

	/** If this charge is applies to a parent reside this charge with the infant */
	private boolean isAppliesToInfant;

	private int appliesTo;

	// charge rate information

	/** Holds charge rate id */
	private Integer chgRateId;

	/** Holds the ratio value */
	private BigDecimal ratioValue = BigDecimal.ZERO;
	/** Holds the ratio value in percentage */
	private boolean isRatioValueInPercentage;

	private BigDecimal chargeValueMin;

	private BigDecimal chargeValueMax;

	private ChargeBasisEnum chargeBasis;
	// charge rate level journey type
	private int journeyType = ChargeRateJourneyType.ANY;

	/** The break point value used for individual rounding - applied only to percentage charges */
	private BigDecimal breakPoint;

	/** The boundry value used for individual rounding - applied only to percentage charges */
	private BigDecimal boundryValue;

	/**
	 * @return Returns the chgGrpCode.
	 */
	public String getChgGrpCode() {
		return chgGrpCode;
	}

	/**
	 * @param chgGrpCode
	 *            The chgGrpCode to set.
	 */
	public void setChgGrpCode(String chgGrpCode) {
		this.chgGrpCode = chgGrpCode;
	}

	/**
	 * @return Returns the isRatioValueInPercentage.
	 */
	public boolean isRatioValueInPercentage() {
		return isRatioValueInPercentage;
	}

	/**
	 * @return Returns the chgRateId.
	 */
	public Integer getChgRateId() {
		return chgRateId;
	}

	/**
	 * @param chgRateId
	 *            The chgRateId to set.
	 */
	public void setChgRateId(Integer chgRateId) {
		this.chgRateId = chgRateId;
	}

	/**
	 * @param isRatioValueInPercentage
	 *            The isRatioValueInPercentage to set.
	 */
	public void setRatioValueInPercentage(boolean isRatioValueInPercentage) {
		this.isRatioValueInPercentage = isRatioValueInPercentage;
	}

	/**
	 * @return Returns the ratioValue.
	 */
	public BigDecimal getRatioValue() {
		return ratioValue;
	}

	/**
	 * @param ratioValue
	 *            The ratioValue to set.
	 */
	public void setRatioValue(BigDecimal ratioValue) {
		this.ratioValue = ratioValue;
	}

	/**
	 * 
	 * @return Returns the quotedChargeDTO
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * 
	 * @param chargeCode
	 *            The charge code to set
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return Returns the chargeDescription.
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * @param chargeDescription
	 *            The chargeDescription to set.
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * @return the isAppliesToInfant
	 */
	public boolean isAppliesToInfant() {
		return isAppliesToInfant;
	}

	/**
	 * @param isAppliesToInfant
	 *            the isAppliesToInfant to set
	 */
	public void setAppliesToInfant(boolean isAppliesToInfant) {
		this.isAppliesToInfant = isAppliesToInfant;
	}

	/**
	 * @return
	 */
	public int getAppliesTo() {
		return appliesTo;
	}

	/**
	 * @param appliesTo
	 */
	public void setAppliesTo(int appliesTo) {
		this.appliesTo = appliesTo;
	}

	/**
	 * @return
	 */
	public BigDecimal getChargeValueMin() {
		return chargeValueMin;
	}

	/**
	 * @param chargeValueMin
	 */
	public void setChargeValueMin(BigDecimal chargeValueMin) {
		this.chargeValueMin = chargeValueMin;
	}

	/**
	 * @return
	 */
	public BigDecimal getChargeValueMax() {
		return chargeValueMax;
	}

	/**
	 * @param chargeValueMax
	 */
	public void setChargeValueMax(BigDecimal chargeValueMax) {
		this.chargeValueMax = chargeValueMax;
	}

	/**
	 * @return
	 */
	public ChargeBasisEnum getChargeBasis() {
		return chargeBasis;
	}

	/**
	 * @param chargeBasis
	 */
	public void setChargeBasis(ChargeBasisEnum chargeBasis) {
		this.chargeBasis = chargeBasis;
	}

	/**
	 * @return
	 */
	public int getJourneyType() {
		return journeyType;
	}

	/**
	 * @param journeyType
	 */
	public void setJourneyType(int journeyType) {
		this.journeyType = journeyType;
	}

	/**
	 * @return : the break point value.
	 */
	public BigDecimal getBreakPoint() {
		return breakPoint;
	}

	/**
	 * @param breakPoint
	 *            : The breal point to set.
	 */
	public void setBreakPoint(BigDecimal breakPoint) {
		this.breakPoint = breakPoint;
	}

	/**
	 * @return : The boundry value.
	 */
	public BigDecimal getBoundryValue() {
		return boundryValue;
	}

	/**
	 * @param boundryValue
	 *            : The boundry value to set.
	 */
	public void setBoundryValue(BigDecimal boundryValue) {
		this.boundryValue = boundryValue;
	}

}
