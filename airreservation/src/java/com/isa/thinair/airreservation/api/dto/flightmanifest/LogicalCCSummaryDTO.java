package com.isa.thinair.airreservation.api.dto.flightmanifest;

public class LogicalCCSummaryDTO extends ManifestCountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String logicalCCName;

	private String logicalCCCode;

	public LogicalCCSummaryDTO(String logicalCCName, String logicalCCCode) {
		super();
		this.logicalCCName = logicalCCName;
		this.logicalCCCode = logicalCCCode;
	}

	public String getLogicalCCName() {
		return logicalCCName;
	}

	public void setLogicalCCName(String logicalCCName) {
		this.logicalCCName = logicalCCName;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

}
