/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Byorn
 * 
 * @since 1.0
 * @hibernate.class table = "T_ALERT_ACTIONS"
 */
public class AlertAction extends Persistent {

	private static final long serialVersionUID = -1856028200750260663L;
	private String actionCode;
	private String actionNote;

	/**
	 * @hibernate.id column = "ACTION_CODE" generator-class="assigned"
	 * 
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * @param actionCode
	 *            The actionCode to set.
	 */
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	/**
	 * @return Returns the actionNote.
	 * @hibernate.property column = "ACTION_NOTE"
	 */
	public String getActionNote() {
		return actionNote;
	}

	/**
	 * @param actionNote
	 *            The actionNote to set.
	 */
	public void setActionNote(String actionNote) {
		this.actionNote = actionNote;
	}
}
