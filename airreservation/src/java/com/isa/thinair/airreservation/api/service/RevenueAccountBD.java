/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Lasantha Pambagoda, Byorn
 * @isa.module.bd-intf id="revenueaccount.service"
 */
public interface RevenueAccountBD {

	/**
	 * For a New Booking, adding a segment and adding an infant Note: For Only a Value addition of an account
	 * 
	 * @param pnrPaxId
	 * @param segmentTotal
	 * @param payments
	 * @param isGoShowProcess
	 * @param isActualPayment
	 *            TODO
	 * @param isFirstPayment
	 *            TODO
	 * @param transactionSeq
	 * @param transactionSegmentIds
	 * @param map
	 * @throws ModuleException
	 */
	public ServiceResponce recordPurchase(String pnrPaxId, BigDecimal segmentTotal, BigDecimal totalPaymentAmount,
			Collection<ITnxPayment> payments, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			CredentialsDTO credentialsDTO, String recieptNumber, Collection<CardDetailConfigDTO> cardConfigData,
			boolean enableTransactionGranularity, boolean isGoShowProcess, boolean isActualPayment, boolean isFirstPayment,
			Integer transactionSeq, Map<String, BigDecimal> lmsProductRedeemedAmount, List<TransactionSegment> transactionSegments)
			throws ModuleException;

	/**
	 * For a removal from an Account i.e : Cancelling a Segment or Removing a Passenger
	 * 
	 * @param pnrPaxId
	 * @param creditTotal
	 * @param chargeAmount
	 * @param penaltyAmount
	 * @param actionNC
	 *            AIR_LINE_CANCEL || PAX_CANCEL
	 * @param chargeNC
	 *            CANCEL_CHARGE || MODIFICATION_CHARGE || NO_CHARGE
	 * @throws ModuleException
	 */
	public ServiceResponce recordCancel(String pnrPaxId, BigDecimal creditTotal, BigDecimal chargeAmount,
			BigDecimal penaltyAmount, int actionNC, int chargeNC, ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO,
			CredentialsDTO credentialsDTO, boolean enableTransactionGranularity) throws ModuleException;

	/**
	 * For an operation that Both Debit and Credit should happen together i.e Modifing a Segment
	 * 
	 * @param pnrPaxId
	 * @param cancelTotal
	 * @param addedTotal
	 * @param chargeAmt
	 * @param penaltyAmount
	 * @param actionNC
	 *            -- PAX_CANCEL
	 * @param chargeNC
	 *            -- CANCEL_CHARGE || ALTER_CHARGE
	 * @param receiptNumber
	 * @param isFirstPayment
	 * @param isActualPayment
	 * @param transactionSeq
	 * @param lmsProductRedeemedAmount
	 * @param transactionSegments
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce recordModification(String pnrPaxId, BigDecimal cancelTotal, BigDecimal addedTotal,
			BigDecimal chargeAmt, BigDecimal penaltyAmount, int actionNC, int chargeNC, Collection<ITnxPayment> colPayment,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO, String receiptNumber,
			Collection<CardDetailConfigDTO> cardConfigData, boolean enableTransactionGranularity, boolean isFirstPayment,
			boolean isActualPayment, Integer transactionSeq, Map<String, BigDecimal> lmsProductRedeemedAmount,
			List<TransactionSegment> transactionSegments) throws ModuleException;

	public ServiceResponce adjustCredits(String pnr, String pnrPaxId, BigDecimal amount, String userNotes,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity, Integer transactionSequence) throws ModuleException;

	public ServiceResponce recordTransferInfant(String fromPnrPaxId, String toPnrPaxId, BigDecimal amount,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO,
			boolean enableTransactionGranularity) throws ModuleException;

	public ServiceResponce recordRefund(String pnrPaxId, BigDecimal refundAmount, Collection<ITnxPayment> payments,
			String userNotes, CredentialsDTO credentialsDTO, boolean enableTransactionGranularity,
			Collection<String> prefferedRefundOrder, Collection<Long> pnrPaxOndChgIds, Boolean isManualRefund, String userNoteType)
			throws ModuleException;

	/**
	 * Used to extend/change current expiry date of available credits
	 * 
	 * @param creditId
	 *            creditId for the record in T_PAX_CREDIT which is going to change
	 * @param expiryDate
	 *            new expiry date
	 * @param note
	 * @param pnr
	 * @param pnrPaxId
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce extendCredits(long creditId, Date expiryDate, String note, String pnr, int pnrPaxId,
			CredentialsDTO credentialsDTO) throws ModuleException;

	/**
	 * Reinstate, expired credit
	 * 
	 * @param pnr
	 * @param pnrPaxId
	 * @param paxTxnId
	 * @param amount
	 * @param expDate
	 * @param credentialsDTO
	 * @param note
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reinstateCredit(String pnr, int pnrPaxId, int paxTxnId, BigDecimal amount, Date expDate,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO, String note,
			boolean enableTransactionGranularity) throws ModuleException;

	public BigDecimal getPNRPaxBalance(String pnrPaxId) throws ModuleException;

	public Map<Integer, BigDecimal> getPNRPaxBalances(Collection<Integer> pnrPaxIds) throws ModuleException;

	public Integer getPNRPaxMaxTransactionSeq(Integer pnrPaxId) throws ModuleException;
	
	public ServiceResponce recordHandlingFeeOnCancellation(String pnrPaxId, BigDecimal amount, CredentialsDTO credentialsDTO) throws ModuleException;

}
