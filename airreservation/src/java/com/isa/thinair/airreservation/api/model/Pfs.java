/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of Pfs information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PFS"
 */
public class Pfs extends Persistent {

	private static final long serialVersionUID = -7185867895680222309L;

	/** Holds the pfs parsed id */
	private int ppId;

	/** Hold the date downloaded */
	private Date dateDownloaded;

	/** Hold the from address of Pfs */
	private String fromAddress;

	/** Hold the pfsContent */
	private String pfsContent;

	/** Hold the fromAirport */
	private String fromAirport; //

	/** Hold the proceed status */
	private String processedStatus;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the Departure Date */
	private Date departureDate;	

	/**
	 * @return Returns the dateDownloaded
	 * @hibernate.property column = "DATE_OF_DOWNLOAD"
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the fromAddress.
	 * @hibernate.property column = "FROM_ADDRESS"
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param setFromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return Returns the PfsContent.
	 * @hibernate.property column = "PFS_CONTENT"
	 */
	public String getPfsContent() {
		return pfsContent;
	}

	/**
	 * @param setPfsContent
	 *            The pfsContent to set.
	 */
	public void setPfsContent(String pfsContent) {
		this.pfsContent = pfsContent;
	}

	/**
	 * @return Returns the fromAddress.
	 * @hibernate.property column = "FROM_AIRPORT"
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param setFromAirport
	 *            The From Airport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the proceedStatus.
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * Set the departure date
	 * 
	 * @param departureDate
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the proceedStatus.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * Set the flight number
	 * 
	 * @param flightNumber
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the ppId.
	 * @hibernate.id column = "PFS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PFS"
	 */
	public int getPpId() {
		return ppId;
	}

	/**
	 * @param ppId
	 */
	public void setPpId(int ppId) {
		this.ppId = ppId;
	}

	/**
	 * @return Returns the proceedStatus.
	 * @hibernate.property column = "PROCESSED_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

}
