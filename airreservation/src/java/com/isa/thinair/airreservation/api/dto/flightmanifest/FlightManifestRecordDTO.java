package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FlightManifestRecordDTO implements Serializable, Cloneable, Comparable<FlightManifestRecordDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pnr;

	private String originatorPnr;

	private String passengerTitle;

	private Integer pnrPaxId;

	private String passengerName;

	private String passengerType;

	private Integer adultPassengerID;

	private String ssr;

	private String origin;

	private String segmentCode;

	private String destination;

	private String reservationStatus;

	private String paxStatus;

	private String agency;

	private String bookingClass;

	private String cabinClassCode;

	private String cabinClassName;

	private String logicalCCCode;

	private String logicalCCName;

	private BigDecimal balance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String currency;

	private String credit;

	private String foidNumber;

	private String foidPsptExpiry;

	private String foidPsptIssuedPlace;

	private String paxCategory;

	private String nationality;

	private String ssrText;

	private String seatCode;

	private String eticket;

	private String cc4Digit;

	private String bookingDate;

	private String mealCode;

	private String baggageCode;

	private double baggageWeight;

	private String formatAmt;

	private String halaServices;

	private String releaseTimeStamp;

	private boolean isPossibleMisConnection;

	private String connectionTime;

	private String autoCancelExpireTime;

	private Double inboundConnectionTime;

	private Double outboundConnectionTime;

	public String getPnr() {
		return pnr;
	}

	public String getOriginatorPnr() {
		return originatorPnr;
	}

	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

	public String getPassengerTitle() {
		return passengerTitle;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public Integer getAdultPassengerID() {
		return adultPassengerID;
	}

	public String getSsr() {
		return ssr;
	}

	public String getOrigin() {
		return origin;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public String getDestination() {
		return destination;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public String getAgency() {
		return agency;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public String getCabinClassName() {
		return cabinClassName;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public String getCurrency() {
		return currency;
	}

	public String getCredit() {
		return credit;
	}

	public String getFoidNumber() {
		return foidNumber;
	}

	public String getFoidPsptExpiry() {
		return foidPsptExpiry;
	}

	public String getFoidPsptIssuedPlace() {
		return foidPsptIssuedPlace;
	}

	public String getPaxCategory() {
		return paxCategory;
	}

	public String getNationality() {
		return nationality;
	}

	public String getSsrText() {
		return ssrText;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public String getEticket() {
		return eticket;
	}

	public String getCc4Digit() {
		return cc4Digit;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public String getMealCode() {
		return mealCode;
	}

	public String getFormatAmt() {
		return formatAmt;
	}

	public String getHalaServices() {
		return halaServices;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setPassengerTitle(String passengerTitle) {
		this.passengerTitle = passengerTitle;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public void setAdultPassengerID(Integer adultPassengerID) {
		this.adultPassengerID = adultPassengerID;
	}

	public void setSsr(String ssr) {
		this.ssr = ssr;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public void setCabinClassName(String cabinClassName) {
		this.cabinClassName = cabinClassName;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getLogicalCCName() {
		return logicalCCName;
	}

	public void setLogicalCCName(String logicalCCName) {
		this.logicalCCName = logicalCCName;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}

	public void setFoidPsptExpiry(String foidPsptExpiry) {
		this.foidPsptExpiry = foidPsptExpiry;
	}

	public void setFoidPsptIssuedPlace(String foidPsptIssuedPlace) {
		this.foidPsptIssuedPlace = foidPsptIssuedPlace;
	}

	public void setPaxCategory(String paxCategory) {
		this.paxCategory = paxCategory;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	public void setCc4Digit(String cc4Digit) {
		this.cc4Digit = cc4Digit;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public void setFormatAmt(String formatAmt) {
		this.formatAmt = formatAmt;
	}

	public void setHalaServices(String halaServices) {
		this.halaServices = halaServices;
	}

	public String getReleaseTimeStamp() {
		return releaseTimeStamp;
	}

	public void setReleaseTimeStamp(String releaseTimeStamp) {
		this.releaseTimeStamp = releaseTimeStamp;
	}

	public String getBaggageCode() {
		return baggageCode;
	}

	public void setBaggageCode(String baggageCode) {
		this.baggageCode = baggageCode;
	}

	public double getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(double baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public boolean isPossibleMisConnection() {
		return isPossibleMisConnection;
	}

	public void setPossibleMisConnection(boolean isPossibleMisConnection) {
		this.isPossibleMisConnection = isPossibleMisConnection;
	}

	public String getAutoCancelExpireTime() {
		return autoCancelExpireTime;
	}

	public void setAutoCancelExpireTime(String autoCancelExpireTime) {
		this.autoCancelExpireTime = autoCancelExpireTime;
	}

	@Override
	public int compareTo(FlightManifestRecordDTO o) {
		int originmatch = this.getOrigin().compareTo(o.getOrigin());
		if (originmatch == 0) {
			return this.getDestination().compareTo(o.getDestination());
		} else {
			return originmatch;
		}
	}

	public Object clone() {

		FlightManifestRecordDTO clonedRecord;
		try {
			clonedRecord = (FlightManifestRecordDTO) super.clone();
		} catch (CloneNotSupportedException e) {

			clonedRecord = null;
		}

		return clonedRecord;

	}

	/**
	 * @return the connectionTime
	 */
	public String getConnectionTime() {
		return connectionTime;
	}

	/**
	 * Gets the connection time as number of days. Will set the connectionTime string in the 12h 15min format by
	 * converting the fraction of day being set. (0.09375 = 2h 15min)
	 * 
	 * @param connectionTime
	 *            the connectionTime to set in number of days.
	 */
	public void setConnectionTime(double connectionTime) {
		if (connectionTime == 0d) {
			this.connectionTime = " - ";
		} else {
			BigDecimal connectionTimeInMinutes = new BigDecimal(connectionTime * 24 * 60);
			BigDecimal[] hnm = connectionTimeInMinutes.divideAndRemainder(new BigDecimal("60"));
			this.connectionTime = hnm[0].toBigInteger() + "h. " + hnm[1].toBigInteger() + " min.";
		}
	}

	private String formatConnectionTime(Double connTime) {
		if (connTime == null || connTime == 0d) {
			return " - ";
		} else {
			double inMins = connTime.doubleValue() * 24 * 60;
			int hours = (int) (inMins / 60.0);
			int mins = (int) Math.ceil((inMins - (hours * 60.0)));
			return hours + "h. " + mins + " min.";
		}

	}

	public double getMinimumConnectionTime() {
		if (inboundConnectionTime != null && outboundConnectionTime != null) {
			return Math.min(inboundConnectionTime, outboundConnectionTime);
		} else if (inboundConnectionTime != null) {
			return inboundConnectionTime;
		} else if (outboundConnectionTime != null) {
			return outboundConnectionTime;
		} else {
			return 0;
		}
	}

	public String getInboundConnectionTime() {
		return formatConnectionTime(inboundConnectionTime);
	}

	public void setInboundConnectionTime(Double inboundConnectionTime) {
		this.inboundConnectionTime = inboundConnectionTime;
	}

	public String getOutboundConnectionTime() {
		return formatConnectionTime(outboundConnectionTime);
	}

	public void setOutboundConnectionTime(Double outboundConnectionTime) {
		this.outboundConnectionTime = outboundConnectionTime;
	}

}
