package com.isa.thinair.airreservation.api.dto.GroupBooking;

import java.io.Serializable;
import java.util.Date;

/*
 * This class will use for the full payment notification email 
 */

public class GroupPaymentNotificationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long groupBookingRequestID;

	private String pnr;
	private String name;
	private String lastName;
	private String title;
	private String email;
	private Date targetPaymentDate;

	public long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	public void setGroupBookingRequestID(long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getTargetPaymentDate() {
		return targetPaymentDate;
	}

	public void setTargetPaymentDate(Date targetPaymentDate) {
		this.targetPaymentDate = targetPaymentDate;
	}

}
