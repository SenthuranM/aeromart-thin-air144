package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItineraryAncillaryAvailabilityTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private boolean seatAvailable;
	private boolean mealAvailable;
	private boolean insuranceAvailable;
	private boolean ssrAvailable;
	private boolean baggageAvailable;
	private boolean airportServiceAvailable;
	private boolean airportTransferAvailable;
	private boolean extraSeatAvailable;

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public boolean isMealAvailable() {
		return mealAvailable;
	}

	public void setMealAvailable(boolean mealAvailable) {
		this.mealAvailable = mealAvailable;
	}

	public boolean isInsuranceAvailable() {
		return insuranceAvailable;
	}

	public void setInsuranceAvailable(boolean insuranceAvailable) {
		this.insuranceAvailable = insuranceAvailable;
	}

	public boolean isSsrAvailable() {
		return ssrAvailable;
	}

	public void setSsrAvailable(boolean ssrAvailable) {
		this.ssrAvailable = ssrAvailable;
	}

	public boolean isAncillaryAvailable() {
		return (isMealAvailable() || isSeatAvailable() || isSsrAvailable() || isBaggageAvailable() || isAirportServiceAvailable() || isAirportTransferAvailable());
	}

	/**
	 * @return the baggageAvailable
	 */
	public boolean isBaggageAvailable() {
		return baggageAvailable;
	}

	/**
	 * @param baggageAvailable
	 *            the baggageAvailable to set
	 */
	public void setBaggageAvailable(boolean baggageAvailable) {
		this.baggageAvailable = baggageAvailable;
	}

	/**
	 * @return the airportServiceAvailable
	 */
	public boolean isAirportServiceAvailable() {
		return airportServiceAvailable;
	}

	/**
	 * @param airportServiceAvailable
	 *            the airportServiceAvailable to set
	 */
	public void setAirportServiceAvailable(boolean airportServiceAvailable) {
		this.airportServiceAvailable = airportServiceAvailable;
	}

	public boolean isExtraSeatAvailable() {
		return extraSeatAvailable;
	}

	public void setExtraSeatAvailable(boolean extraSeatAvailable) {
		this.extraSeatAvailable = extraSeatAvailable;
	}

	public boolean isAirportTransferAvailable() {
		return airportTransferAvailable;
	}

	public void setAirportTransferAvailable(boolean airportTransferAvailable) {
		this.airportTransferAvailable = airportTransferAvailable;
	}

}
