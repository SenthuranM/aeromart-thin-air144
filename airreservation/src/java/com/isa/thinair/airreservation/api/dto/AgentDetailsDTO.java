package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class AgentDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8747396102637588269L;

	private String agentCode;

	private String accountCode;

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

}
