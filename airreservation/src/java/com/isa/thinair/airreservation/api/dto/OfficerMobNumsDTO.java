package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;


public class OfficerMobNumsDTO implements Serializable, Comparable<OfficerMobNumsDTO> {

	private static final long serialVersionUID = -1L;
	
	private int officerId;
	
	private String officerName;
	
	private String mobNumber;
	
	private String contactEmail;
	
	private Long version;
	

	@Override
	public int compareTo(OfficerMobNumsDTO o) {
		// TODO Auto-generated method stub
		return 0;
	}


	public int getOfficerId() {
		return officerId;
	}


	public void setOfficerId(int officerId) {
		this.officerId = officerId;
	}


	public String getOfficerName() {
		return officerName;
	}


	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}


	public String getMobNumber() {
		return mobNumber;
	}


	public void setMobNumber(String mobNumber) {
		this.mobNumber = mobNumber;
	}


	public Long getVersion() {
		return version;
	}


	public void setVersion(Long version) {
		this.version = version;
	}


	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}


	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

}