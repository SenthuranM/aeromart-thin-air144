package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.util.Date;

public class PNRGOVTrasmissionDetailsDTO {
	private int fltSegID;
	
	private String flightNo;
	
	private int flightID;
	
	private String departureAirport;
	
	private String arrivalAirport;
	
	private Date departureTimeZulu;

	public int getFltSegID() {
		return fltSegID;
	}

	public void setFltSegID(int fltSegID) {
		this.fltSegID = fltSegID;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public Date getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	public void setDepartureTimeZulu(Date departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	public int getFlightID() {
		return flightID;
	}

	public void setFlightID(int flightID) {
		this.flightID = flightID;
	}

}
