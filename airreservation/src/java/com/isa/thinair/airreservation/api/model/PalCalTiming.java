package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "T_PAL_CAL_TIMINGS"
 * 
 */

public class PalCalTiming extends Persistent {

	private static final long serialVersionUID = -332648005483777126L;

	private Integer palCalTimingId;

	private String airportCode;

	private int palDepGap;

	private int calRepIntv;

	private Date startingZuluDate;

	private Date endingZuluDate;

	private String status;
	
	private int lastCalGap;
	
	private int calRepIntvAfterCutoff;
	
	private String flightNo;
	
	private String applyToAllFlights;



	/**
	 * @return the palCalTimingId
	 * @hibernate.id column = "PAL_CAL_TIMING_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PAL_CAL_TIMINGS"
	 */
	public Integer getPalCalTimingId() {
		return palCalTimingId;
	}

	/**
	 * @param palCalTimingId
	 *            the palCalTimingId to set
	 */
	public void setPalCalTimingId(Integer palCalTimingId) {
		this.palCalTimingId = palCalTimingId;
	}

	/**
	 * @return the airportCode
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the palDepGap
	 * @hibernate.property column = "PAL_DEP_GAP"
	 */
	public int getPalDepGap() {
		return palDepGap;
	}

	/**
	 * @param palDepGap
	 *            the palDepGap to set
	 */
	public void setPalDepGap(int palDepGap) {
		this.palDepGap = palDepGap;
	}

	/**
	 * @return the calRepIntv
	 * @hibernate.property column = "CAL_REP_INT"
	 */
	public int getCalRepIntv() {
		return calRepIntv;
	}

	/**
	 * @param calRepIntv
	 *            the calRepIntv to set
	 */
	public void setCalRepIntv(int calRepIntv) {
		this.calRepIntv = calRepIntv;
	}

	/**
	 * @return the startingZuluDate
	 * @hibernate.property column = "ZULU_START_DATE"
	 */
	public Date getStartingZuluDate() {
		return startingZuluDate;
	}

	/**
	 * @param startingZuluDate
	 *            the startingZuluDate to set
	 */
	public void setStartingZuluDate(Date startingZuluDate) {
		this.startingZuluDate = startingZuluDate;
	}

	/**
	 * @return the endingZuluDate
	 * @hibernate.property column = "ZULU_END_DATE"
	 */
	public Date getEndingZuluDate() {
		return endingZuluDate;
	}

	/**
	 * @param endingZuluDate
	 *            the endingZuluDate to set
	 */
	public void setEndingZuluDate(Date endingZuluDate) {
		this.endingZuluDate = endingZuluDate;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the lastCalGap
	 *  @hibernate.property column = "LAST_CAL_GAP"
	 */
	public int getLastCalGap() {
		return lastCalGap;
	}

	/**
	 * @param lastCalGap the lastCalGap to set
	 */
	public void setLastCalGap(int lastCalGap) {
		this.lastCalGap = lastCalGap;
	}

	/**
	 * @return the calRepIntvAfterCutoff
	 *  @hibernate.property column = "CAL_REP_INT_CUTOFF_TIME"
	 */
	public int getCalRepIntvAfterCutoff() {
		return calRepIntvAfterCutoff;
	}

	/**
	 * @param calRepIntvAfterCutoff the calRepIntvAfterCutoff to set
	 */
	public void setCalRepIntvAfterCutoff(int calRepIntvAfterCutoff) {
		this.calRepIntvAfterCutoff = calRepIntvAfterCutoff;
	}
	
	/**
	 * @return the flightNo
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNo() {
		return flightNo;
	}
		
	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return Returns the applyToAllFlights.
	 * @hibernate.property column = "APPLY_TO_ALL"
	 */
	public String getApplyToAllFlights() {
		return applyToAllFlights;
	}

	/**
	 * @param applyToAllFlights
	 *            The applyToAllFlights to set.
	 */
	public void setApplyToAllFlights(String applyToAllFlights) {
		this.applyToAllFlights = applyToAllFlights;
	}
	
}