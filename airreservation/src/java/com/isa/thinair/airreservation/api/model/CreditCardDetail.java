/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

/**
 * To keep track of Credit Card details which belongs to a <strong> particular payment.<strong>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_RES_PCD"
 */
public class CreditCardDetail extends Persistent implements Serializable {

	private static final long serialVersionUID = -1503041335508147807L;

	/** Holds credit card generation ID */
	private Integer ccdId;

	/** Holds credit card number */
	private String no;

	/** Holds the credit card number first digits */
	private String noFirstDigits;

	/** Holds the credit card number last digits */
	private String noLastDigits;

	/** Holds credit card name */
	private String name;

	/** Holds credit card expiry date (YYMM) format */
	private String eDate;

	/** Holds credit card security code */
	private String securityCode;

	/** Holds credit card address */
	private String address;

	/** Holds credit card type */
	private int type;

	/** Holds the reservation pnr number */
	private Integer pnrPaxId;

	/** Holds the pnr number */
	private String pnr;

	/** Holds the corresponding transaction id */
	private int transactionId;

	/** Holds the corresponding transaction id */
	private int extTransactionId;

	/** Holds the payment broker reference number */
	private int paymentBrokerRefNo;

	/*
	 * Following fields are not stored in the table... Thus used only for data transfer
	 */
	/** Holds the application indicator */
	private AppIndicatorEnum appIndicator;

	/** Holds the transaction mode enum */
	private TnxModeEnum tnxMode;

	/** Holds the old credit card record id */
	private int oldCCRecordId = -1;

	/** Holds whether it was successful */
	private boolean isSuccess;

	/** Holds the operation type Payment, Refund or Reverse */
	private String operationType;

	/** Holds the tempory payment id */
	private int temporyPaymentId;

	/** Holds the authorization id */
	private String authorizationId;

	/** Holds the payment gateway identification parameters */
	private IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	private boolean isOfflinePayment;

	/**
	 * @return Returns the address.
	 * @hibernate.property column = "ADDRESS"
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Returns the ccdId.
	 * @hibernate.id column = "RES_PCD_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_RES_PCD"
	 */
	public Integer getCcdId() {
		return ccdId;
	}

	/**
	 * @param ccdId
	 *            The ccdId to set.
	 */
	public void setCcdId(Integer ccdId) {
		this.ccdId = ccdId;
	}

	/**
	 * @return Returns the cType.
	 * @hibernate.property column = "CARD_TYPE_ID"
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            The cType to set.
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return Returns the eDate.
	 * @hibernate.property column = "E_DATE"
	 */
	public String getEDate() {
		return eDate;
	}

	/**
	 * @param date
	 *            The eDate to set.
	 */
	public void setEDate(String date) {
		eDate = date;
	}

	/**
	 * @return Returns the name.
	 * @hibernate.property column = "NAME"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the noLastDigits.
	 * @hibernate.property column = "NO"
	 */
	public String getNo() {
		return no;
	}

	/**
	 * @param no
	 *            The no to set.
	 */
	public void setNo(String no) {
		this.no = no;
		this.setNoFirstDigits(BeanUtils.getFirst6Digits(no));
		this.setNoLastDigits(BeanUtils.getLast4Digits(no));
	}

	/**
	 * @return Returns the securityCode.
	 * @hibernate.property column = "SEC_CODE"
	 */
	public String getSecurityCode() {
		return securityCode;
	}

	/**
	 * @param securityCode
	 *            The securityCode to set.
	 */
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	/**
	 * @return Returns the isSuccess.
	 */
	public boolean isSuccess() {
		return isSuccess;
	}

	/**
	 * @param isSuccess
	 *            The isSuccess to set.
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	/**
	 * @return Returns the pnrPaxId.
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the transactionId.
	 * @hibernate.property column = "TXN_ID"
	 */
	public int getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            The transactionId to set.
	 */
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the paymentBrokerRefNo.
	 * @hibernate.property column = "TRANSACTION_REF_NO"
	 */
	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	/**
	 * @param paymentBrokerRefNo
	 *            The paymentBrokerRefNo to set.
	 */
	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	/**
	 * @return Returns the appIndicator.
	 */
	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @param appIndicator
	 *            The appIndicator to set.
	 */
	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @return Returns the tnxMode.
	 */
	public TnxModeEnum getTnxMode() {
		return tnxMode;
	}

	/**
	 * @param tnxMode
	 *            The tnxMode to set.
	 */
	public void setTnxMode(TnxModeEnum tnxMode) {
		this.tnxMode = tnxMode;
	}

	/**
	 * @return Returns the oldCCRecordId.
	 */
	public int getOldCCRecordId() {
		return oldCCRecordId;
	}

	/**
	 * @param oldCCRecordId
	 *            The oldCCRecordId to set.
	 */
	public void setOldCCRecordId(int oldCCRecordId) {
		this.oldCCRecordId = oldCCRecordId;
	}

	/**
	 * @return Returns the noFirstDigits.
	 * @hibernate.property column = "FIRST_DIGITS_OF_NUMBER"
	 */
	public String getNoFirstDigits() {
		return noFirstDigits;
	}

	/**
	 * @param noFirstDigits
	 *            The noFirstDigits to set.
	 */
	public void setNoFirstDigits(String noFirstDigits) {
		this.noFirstDigits = noFirstDigits;
	}

	public String getNoLastDigits() {
		return noLastDigits;
	}

	/**
	 * @param noLastDigits
	 *            The noLastDigits to set.
	 */
	public void setNoLastDigits(String noLastDigits) {
		this.noLastDigits = noLastDigits;
	}

	/**
	 * @return Returns the temporyPaymentId.
	 */
	public int getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            The temporyPaymentId to set.
	 */
	public void setTemporyPaymentId(int temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	/**
	 * @return Returns the operationType.
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType
	 *            The operationType to set.
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return Returns the authorizationId.
	 * @hibernate.property column = "AUTHORIZATION_ID"
	 */
	public String getAuthorizationId() {
		return authorizationId;
	}

	/**
	 * @param authorizationId
	 *            The authorizationId to set.
	 */
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

	/**
	 * @return the ipgIdentificationParamsDTO
	 */
	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	/**
	 * @param ipgIdentificationParamsDTO
	 *            the ipgIdentificationParamsDTO to set
	 */
	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * @return Returns the authorizationId.
	 * @hibernate.property column = "EXT_TXN_ID"
	 */
	public int getExtTransactionId() {
		return extTransactionId;
	}

	public void setExtTransactionId(int extTransactionId) {
		this.extTransactionId = extTransactionId;
	}

	public boolean isOfflinePayment() {
		return isOfflinePayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.isOfflinePayment = isOfflinePayment;
	}

}
