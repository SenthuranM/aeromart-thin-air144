package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To Show in the screen the total amount generate/and what is existing in interface tables.
 * 
 * @author isa11
 * 
 */
public class CashSalesStatusDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3069125835017208996L;
	private BigDecimal totAmtGenerated = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal totAmtTransfered = AccelAeroCalculator.getDefaultBigDecimalZero();
	private boolean hasEXDB;

	/** Contatins CashSaleshistoryDTO **/
	private ArrayList<CashSalesHistoryDTO> cashSales;

	/**
	 * @return Returns the amountSatus.
	 */
	public String getAmountSatus() {
		NumberFormat formatter = new DecimalFormat("#,###,###.00");
		if (this.totAmtGenerated != null) {

			if (this.totAmtTransfered != null) {

				if (Math.round(this.totAmtGenerated.doubleValue()) == Math.round(this.totAmtTransfered.doubleValue())) {
					return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
							+ " Is Generated and Transfered ";
				} else {
					if (Math.round(this.totAmtGenerated.doubleValue()) == 0) {
						return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
								+ " Is Generated but NOT Transfered";
					} else {
						return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
								+ " Is Generated but Transfered Valued is :"
								+ formatter.format(this.totAmtTransfered.doubleValue());
					}

				}
			} else {
				if (hasEXDB) {
					return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue())
							+ " Is Generated but Not Transfered";
				}
				return "For the Selected Period " + formatter.format(this.totAmtGenerated.doubleValue()) + " Is Generated.";
			}

		}

		return "No Cash Sales have been generated for the selected Period ";
	}

	/**
	 * @return Returns the cashSales.
	 */
	public ArrayList<CashSalesHistoryDTO> getCashSales() {
		return cashSales;
	}

	/**
	 * @param cashSales
	 *            The cashSales to set.
	 */
	public void setCashSales(ArrayList<CashSalesHistoryDTO> cashSales) {
		this.cashSales = cashSales;
	}

	/**
	 * @return Returns the hasEXDB.
	 */
	public boolean isHasEXDB() {
		return hasEXDB;
	}

	/**
	 * @param hasEXDB
	 *            The hasEXDB to set.
	 */
	public void setHasEXDB(boolean hasEXDB) {
		this.hasEXDB = hasEXDB;
	}

	/**
	 * @return Returns the totAmtGenerated.
	 */
	public BigDecimal getTotAmtGenerated() {
		return totAmtGenerated;
	}

	/**
	 * @param totAmtGenerated
	 *            The totAmtGenerated to set.
	 */
	public void setTotAmtGenerated(BigDecimal totAmtGenerated) {
		this.totAmtGenerated = totAmtGenerated;
	}

	/**
	 * @return Returns the totAmtTransfered.
	 */
	public BigDecimal getTotAmtTransfered() {
		return totAmtTransfered;
	}

	/**
	 * @param totAmtTransfered
	 *            The totAmtTransfered to set.
	 */
	public void setTotAmtTransfered(BigDecimal totAmtTransfered) {
		this.totAmtTransfered = totAmtTransfered;
	}

}
