/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Cash Sales is the entity class to repesent a CashSales model
 * 
 * @author :Isuru
 * @hibernate.class table = "T_CASH_SALES"
 */
public class CashSales implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4675827169556605708L;
	private Integer id;
	private Date dateOfSales;
	private String staff_Id;
	private BigDecimal totalDailySales = AccelAeroCalculator.getDefaultBigDecimalZero();
	private char transferStatus;
	private Date transferTimeStamp;
	private String paymentCurrencyCode;

	/**
	 * returns the dateOfsales
	 * 
	 * @return Returns the dateOfsales.
	 * @hibernate.property column="DATE_OF_SALE"
	 */
	public Date getDateOfSales() {
		return dateOfSales;
	}

	/**
	 * sets the dateofsales
	 * 
	 * @param dateOfSales
	 *            The dateOfSales to set.
	 */
	public void setDateOfSales(Date dateOfSales) {
		this.dateOfSales = dateOfSales;
	}

	/**
	 * returns the staff_Id
	 * 
	 * @return Returns the staff_Id
	 * @hibernate.property column = "STAFF_ID"
	 */
	public String getStaff_Id() {
		return staff_Id;
	}

	/**
	 * sets the staff_Id
	 * 
	 * @param staff_Id
	 *            The staff_Id to set.
	 */
	public void setStaff_Id(String staff_Id) {
		this.staff_Id = staff_Id;
	}

	/**
	 * returns the totalDailySales
	 * 
	 * @return Returns the totalDailySales
	 * 
	 * @hibernate.property column = "TOTAL_DAILY_SALES"
	 */
	public BigDecimal getTotalDailySales() {
		return totalDailySales;
	}

	/**
	 * sets the totalDailySales
	 * 
	 * @param totalDailySales
	 *            The totalDailySales to set.
	 */
	public void setTotalDailySales(BigDecimal totalDailySales) {
		this.totalDailySales = totalDailySales;
	}

	/**
	 * returns the transferStatus
	 * 
	 * @return Returns the transferStatus
	 * @hibernate.property column = "TRANSFER_STATUS"
	 */
	public char getTransferStatus() {
		return transferStatus;
	}

	/**
	 * sets the transferStatus
	 * 
	 * @param transferStatus
	 *            The transferStatus to set.
	 */
	public void setTransferStatus(char transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * returns the transferTimeStamp
	 * 
	 * @return Returns the transferTimeStamp
	 * @hibernate.property column = "TRANSFER_TIMESTAMP"
	 */
	public Date getTransferTimeStamp() {
		return transferTimeStamp;
	}

	/**
	 * sets the transferTimeStamp
	 * 
	 * @param transferTimeStamp
	 *            The transferTimeStamp to set.
	 */
	public void setTransferTimeStamp(Date transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}

	/**
	 * 
	 * @return Returns the id.
	 * @hibernate.id column = "ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CASH_SALES"
	 * 
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}
}
