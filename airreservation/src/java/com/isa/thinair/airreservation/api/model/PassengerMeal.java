/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @hibernate.class table = "ML_T_PNR_PAX_SEG_MEAL"
 */
public class PassengerMeal extends Persistent {

	private static final long serialVersionUID = -7095267913770972771L;
	private Integer paxMealId;
	private Integer pnrPaxId;
	private Integer pnrSegId;
	private Integer flightMealId;
	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String status;
	private Integer salesChannelCode;
	private String userId;
	private Integer customerId;
	private Date timestamp;
	private Integer pPFID;
	private Integer mealId;
	private Integer autoCancellationId;

	/**
	 * @return the paxSeatingid
	 * @hibernate.id column = "PNR_PAX_SEG_MEAL_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="ML_S_PNR_PAX_SEG_MEAL"
	 */
	public Integer getPaxMealId() {
		return paxMealId;
	}

	/**
	 * @param paxSeatingid
	 *            the paxSeatingid to set
	 */
	public void setPaxMealId(Integer paxMealId) {
		this.paxMealId = paxMealId;
	}

	/**
	 * @return the pnrPaxId
	 * @hibernate.property column = "pnr_pax_id"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the flightMealId
	 * @hibernate.property column = "FLIGHT_MEAL_CHARGE_ID"
	 */
	public Integer getFlightMealId() {
		return flightMealId;
	}

	/**
	 * @param flightMealId
	 *            the flightMealId to set
	 */
	public void setFlightMealId(Integer flightMealId) {
		this.flightMealId = flightMealId;
	}

	/**
	 * @return the chargeAmount
	 * @hibernate.property column = "amount"
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            the chargeAmount to set
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "status"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the salesChannelCode
	 * @hibernate.property column = "sales_channel_code"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            the salesChannelCode to set
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "user_id"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column = "customer_id"
	 * @return the customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @hibernate.property column = "pnr_seg_id"
	 * @return
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @hibernate.property column = "timestamp"
	 * @return
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @hibernate.property column = "ppf_id"
	 * @return
	 */
	public Integer getpPFID() {
		return pPFID;
	}

	public void setpPFID(Integer pPFID) {
		this.pPFID = pPFID;
	}

	/**
	 * @hibernate.property column = "meal_id"
	 * @return
	 */
	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	/**
	 * @return
	 * @hibernate.property column = "AUTO_CANCELLATION_ID"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

}
