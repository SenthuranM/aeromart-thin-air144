package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ItineraryFlightOndGroup implements Serializable, Comparable<ItineraryFlightOndGroup> {

	private static final long serialVersionUID = 1L;
	private int ondSeq;
	private List<ItineraryFlightTO> flights;
	private Date firstDeparture;
	private Date lastArrival;

	public ItineraryFlightOndGroup(int ondSeq) {
		this.ondSeq = ondSeq;
		this.flights = new ArrayList<ItineraryFlightTO>();
	}

	public int getOndSeq() {
		return ondSeq;
	}

	public void setOndSeq(int ondSeq) {
		this.ondSeq = ondSeq;
	}

	public List<ItineraryFlightTO> getFlights() {
		return flights;
	}

	public void setFlights(List<ItineraryFlightTO> flights) {
		this.flights = flights;
	}

	public int compareTo(ItineraryFlightOndGroup o) {
		return (this.ondSeq - o.ondSeq);
	}

	public String getDuration(String format) {
		if (firstDeparture != null && lastArrival != null) {
			return BeanUtils.parseDurationFormat(lastArrival.getTime() - firstDeparture.getTime(), format);
		} else {
			return "";
		}
	}

	public void addFlight(ItineraryFlightTO flight) {
		if (firstDeparture == null
				|| (flight.getDepartureDateTimeZulu() != null && firstDeparture.compareTo(flight.getDepartureDateTimeZulu()) > 0)) {
			firstDeparture = flight.getDepartureDateTimeZulu();
		}
		if (lastArrival == null
				|| (flight.getArrivalDateTimeZulu() != null && lastArrival.compareTo(flight.getArrivalDateTimeZulu()) < 0)) {
			lastArrival = flight.getArrivalDateTimeZulu();
		}
		this.getFlights().add(flight);
	}

}
