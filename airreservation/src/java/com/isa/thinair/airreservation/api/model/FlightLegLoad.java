package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Reserved flight load information in leg vice to send for cargo planning.
 * @author vidula
 *
 * @hibernate.class table = "T_FLIGHT_LEG_LOAD"
 */
public class FlightLegLoad extends Persistent {
	
	private static final long serialVersionUID = 166355364246925496L;
	
	private Integer flightLegLoadId;
	private Integer flightLegId;
	private String origin;
	private String destination;
	private Date departureTimeZulu;
	private Date arrivalTimeZulu;
	private Integer maleCount;
	private Integer femaleCount;
	private Integer childCount;
	private Integer infantCount;
	private Double baggageLoad;

	public FlightLegLoad() {
		
	}

	public FlightLegLoad(Integer flightLegId, String origin, String destination, Date departureTimeZulu, Date arrivalTimeZulu,
			Integer maleCount, Integer femaleCount, Integer childCount, Integer infantCount, Double baggageLoad) {
		this.flightLegId = flightLegId;
		this.origin = origin;
		this.destination = destination;
		this.departureTimeZulu = departureTimeZulu;
		this.arrivalTimeZulu = arrivalTimeZulu;
		this.maleCount = maleCount;
		this.femaleCount = femaleCount;
		this.childCount = childCount;
		this.infantCount = infantCount;
		this.baggageLoad = baggageLoad;
	}

	/**
	 * @hibernate.id column = "FLIGHT_LEG_LOAD_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_LEG_LOAD"
	 */
	public Integer getFlightLegLoadId() {
		return flightLegLoadId;
	}

	public void setFlightLegLoadId(Integer flightLoadId) {
		this.flightLegLoadId = flightLoadId;
	}

	/**
	 * @hibernate.property column = "FLIGHT_LEG_ID"
	 */
	public Integer getFlightLegId() {
		return flightLegId;
	}

	public void setFlightLegId(Integer flightLegId) {
		this.flightLegId = flightLegId;
	}
	
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	public void setDepartureTimeZulu(Date departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	public Date getArrivalTimeZulu() {
		return arrivalTimeZulu;
	}

	public void setArrivalTimeZulu(Date arrivalTimeZulu) {
		this.arrivalTimeZulu = arrivalTimeZulu;
	}

	/**
	 * @hibernate.property column = "MALE_COUNT"
	 */
	public Integer getMaleCount() {
		return maleCount;
	}

	public void setMaleCount(Integer maleCount) {
		this.maleCount = maleCount;
	}

	/**
	 * @hibernate.property column = "FEMALE_COUNT"
	 */
	public Integer getFemaleCount() {
		return femaleCount;
	}

	public void setFemaleCount(Integer femaleCount) {
		this.femaleCount = femaleCount;
	}

	/**
	 * @hibernate.property column = "CHILD_COUNT"
	 */
	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	/**
	 * @hibernate.property column = "INFANT_COUNT"
	 */
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	/**
	 * @hibernate.property column = "BAGGAGE_LOAD"
	 */
	public Double getBaggageLoad() {
		return baggageLoad;
	}

	public void setBaggageLoad(Double baggageLoad) {
		this.baggageLoad = baggageLoad;
	}

	@Override
	public String toString() {
		return "FlightLegLoad [flightLegLoadId=" + flightLegLoadId
				+ ", flightLegId=" + flightLegId + ", origin=" + origin
				+ ", destination=" + destination + ", departureTimeZulu="
				+ departureTimeZulu + ", arrivalTimeZulu=" + arrivalTimeZulu
				+ ", maleCount=" + maleCount + ", femaleCount=" + femaleCount
				+ ", childCount=" + childCount + ", infantCount=" + infantCount
				+ ", baggageLoad=" + baggageLoad + "]";
	}

}