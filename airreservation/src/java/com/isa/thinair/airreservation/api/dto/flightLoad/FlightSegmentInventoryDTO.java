package com.isa.thinair.airreservation.api.dto.flightLoad;

import java.io.Serializable;

public class FlightSegmentInventoryDTO implements Serializable {

	private static final long serialVersionUID = 5833695737924456314L;

	private int totalBookings;
	private int adultCount;
	private int childCount;
	private int infantCount;
	private String segmentCode;
	
	public int getTotalBookings() {
		return totalBookings;
	}
	public void setTotalBookings(int totalBookings) {
		this.totalBookings = totalBookings;
	}
	public int getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}
	public int getChildCount() {
		return childCount;
	}
	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}
	public int getInfantCount() {
		return infantCount;
	}
	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}
	public String getSegmentCode() {
		return segmentCode;
	}
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}


}
