package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;

public class PNRGOVPaymentDTO {

	private BigDecimal paymentAmount;

	private String paymentCurrency;

	private int nominalCode;

	private String tnxType;

	private Date tnxDate;

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public int getNominalCode() {
		return nominalCode;
	}

	public void setNominalCode(int nominalCode) {
		this.nominalCode = nominalCode;
	}

	public String getTnxType() {
		return tnxType;
	}

	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	public Date getTnxDate() {
		return tnxDate;
	}

	public void setTnxDate(Date tnxDate) {
		this.tnxDate = tnxDate;
	}

	public String getFormOfPaymentIdentification() {
		if (ReservationTnxNominalCode.CARD_PAYMENT_AMEX.getCode() == this.getNominalCode()
				|| ReservationTnxNominalCode.CARD_PAYMENT_MASTER.getCode() == this.getNominalCode()
				|| ReservationTnxNominalCode.CARD_PAYMENT_VISA.getCode() == this.getNominalCode()
				|| ReservationTnxNominalCode.CARD_PAYMENT_DINERS.getCode() == this.getNominalCode()
				|| ReservationTnxNominalCode.CARD_PAYMENT_GENERIC_DEBIT.getCode() == this.getNominalCode()) {
			return "CC";
		} else {
			return "CA";
		}
	}

}
