/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pnl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

/**
 * @author udithad
 *
 */
public class DestinationFare {

	private String originAirportCode;
	private String destinationAirportCode;
	private Map<String, List<PassengerInformation>> addPassengers;
	private Map<String, List<PassengerInformation>> deletePassengers;
	private Map<String, List<PassengerInformation>> changePassengers;
	private String fareClass;
	private Integer numberOfPreviousPassengers;
	private boolean isOnceUsed;

	public DestinationFare() {
		createNewPassengerMaps();
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	public Map<String, List<PassengerInformation>> getAddPassengers() {
		return addPassengers;
	}

	public void setAddPassengers(
			Map<String, List<PassengerInformation>> addPassengers) {
		this.addPassengers = addPassengers;
	}

	public Map<String, List<PassengerInformation>> getDeletePassengers() {
		return deletePassengers;
	}

	public void setDeletePassengers(
			Map<String, List<PassengerInformation>> deletePassengers) {
		this.deletePassengers = deletePassengers;
	}

	public Map<String, List<PassengerInformation>> getChangePassengers() {
		return changePassengers;
	}

	public void setChangePassengers(
			Map<String, List<PassengerInformation>> changePassengers) {
		this.changePassengers = changePassengers;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public Integer getNumberOfPreviousPassengers() {
		return numberOfPreviousPassengers;
	}

	public void setNumberOfPreviousPassengers(Integer numberOfPreviousPassengers) {
		this.numberOfPreviousPassengers = numberOfPreviousPassengers;
	}

	public void populateAddedPassengersToPnl(PassengerInformation detailsDTO) {
		numberOfPreviousPassengers++;
		addPassengerToList(
				getAvailablePassengerList(detailsDTO.getLastName(),
						this.addPassengers), detailsDTO);
	}

	public void populateDeletedPassengersToPnl(PassengerInformation detailsDTO) {
		numberOfPreviousPassengers--;
		addPassengerToList(
				getAvailablePassengerList(detailsDTO.getLastName(),
						this.deletePassengers), detailsDTO);
	}

	public void populateChangedPassengersToPnl(PassengerInformation detailsDTO) {
		addPassengerToList(
				getAvailablePassengerList(detailsDTO.getLastName(),
						this.changePassengers), detailsDTO);
	}

	private void addPassengerToList(List<PassengerInformation> passengersList,
			PassengerInformation paxDetailsDTO) {
		passengersList.add(paxDetailsDTO);
	}

	private List<PassengerInformation> getAvailablePassengerList(
			String lastName,
			Map<String, List<PassengerInformation>> passengerMap) {
		List<PassengerInformation> passengersList = null;

		if (passengerMap.containsKey(lastName)) {
			passengersList = passengerMap.get(lastName);
		} else {
			passengersList = getNewPaxDetailsDTOList();
			passengerMap.put(lastName, passengersList);
		}
		return passengersList;
	}

	private void createNewPassengerMaps() {
		this.addPassengers = createNewPassengerMap(this.addPassengers);
		this.deletePassengers = createNewPassengerMap(this.deletePassengers);
		this.changePassengers = createNewPassengerMap(this.changePassengers);
	}

	private List<PassengerInformation> getNewPaxDetailsDTOList() {
		return new ArrayList<PassengerInformation>();
	}

	private Map<String, List<PassengerInformation>> createNewPassengerMap(
			Map<String, List<PassengerInformation>> passengerMap) {
		if (passengerMap == null) {
			passengerMap = getNewPassengerMap();
		}
		return passengerMap;
	}

	private Map<String, List<PassengerInformation>> getNewPassengerMap() {
		return new TreeMap<String, List<PassengerInformation>>();
	}

	public Map<String, List<PassengerInformation>> getPassengersMap(
			PassengerStoreTypes passengerStoreTypes) {
		Map<String, List<PassengerInformation>> passengers = null;
		if (PassengerStoreTypes.ADDED.equals(passengerStoreTypes)) {
			passengers = addPassengers;
		} else if (PassengerStoreTypes.DELETED.equals(passengerStoreTypes)) {
			passengers = deletePassengers;
		} else if (PassengerStoreTypes.CHANGED.equals(passengerStoreTypes)) {
			passengers = changePassengers;
		}
		return passengers;
	}

	private List<PassengerInformation> createPassengerList(
			Map<String, List<PassengerInformation>> passengerMap) {
		List<PassengerInformation> passengers = new ArrayList<PassengerInformation>();
		for (Map.Entry<String, List<PassengerInformation>> entry : passengerMap
				.entrySet()) {
			passengers.addAll(entry.getValue());
		}
		return passengers;
	}

	public void removePassenger(PassengerStoreTypes storeTypes,
			PassengerInformation passengerInformation) {
		if (PassengerStoreTypes.ADDED.equals(storeTypes)) {
			remove(addPassengers, passengerInformation);
			remove(addPassengers);
		} else if (PassengerStoreTypes.DELETED.equals(storeTypes)) {
			remove(deletePassengers, passengerInformation);
			remove(deletePassengers);
		} else if (PassengerStoreTypes.CHANGED.equals(storeTypes)) {
			remove(changePassengers, passengerInformation);
			remove(changePassengers);
		}
	}

	public void removeEmptyPassengerLists(PassengerStoreTypes storeTypes) {
		if (PassengerStoreTypes.ADDED.equals(storeTypes)) {
			remove(addPassengers);
		} else if (PassengerStoreTypes.DELETED.equals(storeTypes)) {
			remove(deletePassengers);
		} else if (PassengerStoreTypes.CHANGED.equals(storeTypes)) {
			remove(changePassengers);
		}
		
	}

	private void remove(Map<String, List<PassengerInformation>> passengerMap) {
		Iterator<Map.Entry<String, List<PassengerInformation>>> entries = passengerMap
				.entrySet().iterator();

		for (Iterator<Map.Entry<String, List<PassengerInformation>>> it = passengerMap
				.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, List<PassengerInformation>> entry = it.next();
			List<PassengerInformation> passengerInformations = entry.getValue();
			if (passengerInformations == null) {
				it.remove();
			} else if (passengerInformations != null
					&& passengerInformations.isEmpty()) {
				it.remove();
			}
		}

	}

	private void remove(Map<String, List<PassengerInformation>> passengerMap,
			PassengerInformation passengerInformation) {
		List<PassengerInformation> passengerInformations = null;
		if (passengerMap.containsKey(passengerInformation.getLastName())) {

			passengerInformations = passengerMap.get(passengerInformation
					.getLastName());
			passengerInformations.remove(passengerInformation);
		}
	}

	public int getTotalPassengerCount(){
		int totalAddCount = 0;
		int totalCount = 0;
		int totalDelCount=0;
		int totalChgCount=0;
		for (Map.Entry<String, List<PassengerInformation>> entry : addPassengers
				.entrySet()) {
			if(entry.getValue() != null && !entry.getValue().isEmpty()){
				totalAddCount = totalAddCount + entry.getValue().size() ;
			}
		}
		for (Map.Entry<String, List<PassengerInformation>> entry : deletePassengers
				.entrySet()) {
			if(entry.getValue() != null && !entry.getValue().isEmpty()){
				totalDelCount = totalDelCount + entry.getValue().size() ;
			}
		}
		
		for (Map.Entry<String, List<PassengerInformation>> entry : deletePassengers
				.entrySet()) {
			if(entry.getValue() != null && !entry.getValue().isEmpty()){
				totalChgCount = totalChgCount + entry.getValue().size() ;
			}
		}
		
		if(totalAddCount != 0 || totalDelCount != 0){
			if(totalAddCount > totalDelCount){
				totalCount = totalAddCount-totalDelCount;
			}else{
				totalCount = totalDelCount;
			}
		}else{
			totalCount = totalChgCount ;
		}
		
		return totalCount;
	}
	
	public static enum PassengerStoreTypes {
		ADDED, DELETED, CHANGED
	}

	public boolean isOnceUsed() {
		return isOnceUsed;
	}

	public void setOnceUsed(boolean isOnceUsed) {
		this.isOnceUsed = isOnceUsed;
	}

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

}
