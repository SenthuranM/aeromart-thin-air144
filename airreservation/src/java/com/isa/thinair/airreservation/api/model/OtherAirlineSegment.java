package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
* @hibernate.class table = "T_PNR_OTHER_AIRLINE_SEGMENT"
*/
public class OtherAirlineSegment implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer pnrOtherAirlineSegId;

	private String flightNumber;

	private String bookingCode;

	private String marketingFlightNumber;

	private String marketingBookingCode;

	private Date departureDateTimeLocal;
	
	private Date arrivalDateTimeLocal;

	private String status;

	private Integer flightSegId;
	
	private Reservation reservation;
	
	private String origin;
	
	private String destination;	

	/**
	 * @hibernate.id column = "PNR_OTHER_AIRLINE_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_OTHER_AIRLINE_SEGMENT"
	 */	
	public Integer getPnrOtherAirlineSegId() {
		return pnrOtherAirlineSegId;
	}

	/**
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @hibernate.property column = "MARKETING_FLIGHT_NUMBER"
	 */	
	public String getMarketingFlightNumber() {
		return marketingFlightNumber;
	}

	/**
	 * @hibernate.property column = "MARKETING_BOOKING_CODE"
	 */	
	public String getMarketingBookingCode() {
		return marketingBookingCode;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */	
	public String getStatus() {
		return status;
	}

	/**
	 * @hibernate.property column = "FLT_SEG_ID"
	 */	
	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setPnrOtherAirlineSegId(Integer pnrOtherAirlineSegId) {
		this.pnrOtherAirlineSegId = pnrOtherAirlineSegId;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public void setMarketingFlightNumber(String marketingFlightNumber) {
		this.marketingFlightNumber = marketingFlightNumber;
	}

	public void setMarketingBookingCode(String marketingBookingCode) {
		this.marketingBookingCode = marketingBookingCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @hibernate.many-to-one column="PNR" class="com.isa.thinair.airreservation.api.model.Reservation"
	 */
	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @hibernate.property column = "ORIGIN"
	 */	
	public String getOrigin() {
		return origin;
	}

	/**
	 * @hibernate.property column = "DESTINATION"
	 */	
	public String getDestination() {
		return destination;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public String getUniqueSegmentKey() {
		return ReservationApiUtils.getUniqueSegmentKey(getFlightNumber(), getOrigin() + "/" + getDestination(),
				getDepartureDate(), getBookingCode(), null);
	}

	/**
	 * @hibernate.property column = "EST_TIME_DEPARTURE_LOCAL"
	 */	
	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}
	
	public Date getDepartureDate() {
		return CalendarUtil.getStartTimeOfDate(departureDateTimeLocal);
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	/**
	 * @hibernate.property column = "EST_TIME_ARRIVAL_LOCAL"
	 */	
	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}
	
	@Override
	public String toString() {
		return "OtherAirlineSegment [pnrOtherAirlineSegId=" + pnrOtherAirlineSegId + ", flightNumber=" + flightNumber
				+ ", bookingCode=" + bookingCode + ", marketingFlightNumber=" + marketingFlightNumber + ", marketingBookingCode="
				+ marketingBookingCode + ", departureDateTimeLocal=" + departureDateTimeLocal + ", arrivalDateTimeLocal="
				+ arrivalDateTimeLocal + ", status=" + status + ", flightSegId=" + flightSegId + ", origin=" + origin
				+ ", destination=" + destination + "]";
	}

}
