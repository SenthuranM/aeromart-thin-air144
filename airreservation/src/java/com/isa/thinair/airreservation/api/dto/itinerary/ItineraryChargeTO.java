package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ItineraryChargeTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String segmentCode;

	private String passengerName;

	private Date chargeDate;

	private String chargeDateInPersian;

	private String description;

	private String chargeType;

	private String amount;

	private String currency;

	private String chargeCode;

	private boolean persianDatesSet;

	public boolean isPersianDatesSet() {
		return persianDatesSet;
	}

	public void setIsPersianDatesSet(boolean usePersianDates) {
		this.persianDatesSet = usePersianDates;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	/**
	 * @return the chargeDate
	 */
	public String getChargeDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.chargeDate, dateFormat);
	}

	/**
	 * @param chargeDate
	 *            the chargeDate to set
	 */
	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the chargeCode
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            the chargeCode to set
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeDateInPersian() {
		return chargeDateInPersian;
	}

	public void setChargeDateInPersian(String chargeDateInPersian) {
		this.chargeDateInPersian = chargeDateInPersian;
	}

}