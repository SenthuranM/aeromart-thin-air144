package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CashSalesHistoryDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5612931289617469613L;

	private Date dateOfSale;

	private String staffId;

	private String displayName;

	private BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String transferStatus;

	private Timestamp transferTimeStamp;

	public Date getDateOfSale() {
		return dateOfSale;
	}

	public void setDateOfSale(Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	public Timestamp getTransferTimeStamp() {
		return transferTimeStamp;
	}

	public void setTransferTimeStamp(Timestamp transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return Returns the total.
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            The total to set.
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
