package com.isa.thinair.airreservation.api.dto.airportTransfer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Manoj Dhanushka
 */
public class PaxAirportTransferTO implements Serializable {
	
	private static final long serialVersionUID = 4916628131074044994L;

	private String requestTimeStamp;

	private String contactNo;

	private String address;
	
	private String pickupType;
	
	private Integer pnrPaxId;
	
	private Integer pnrSegId;
	
	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Integer ppfId;
	
	private Integer airportTransferId;
	
	private String airportCode;
	
	private Integer salesChannelCode;
	
	private String userId;
	
	private ExternalChgDTO chgDTO;
	
	private Integer pnrPaxSegAptId;

	private AirportTransferTO provider;

	private String paxName;
	
	private Date bookingTimeStamp;

	public String getRequestTimeStamp() {
		return requestTimeStamp;
	}

	public String getContactNo() {
		return contactNo;
	}

	public String getAddress() {
		return address;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public Integer getPpfId() {
		return ppfId;
	}

	public Integer getAirportTransferId() {
		return airportTransferId;
	}

	public void setRequestTimeStamp(String requestTimeStamp) {
		this.requestTimeStamp = requestTimeStamp;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public void setPpfId(Integer ppfId) {
		this.ppfId = ppfId;
	}

	public void setAirportTransferId(Integer airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public ExternalChgDTO getChgDTO() {
		return chgDTO;
	}

	public void setChgDTO(ExternalChgDTO chgDTO) {
		this.chgDTO = chgDTO;
	}

	public String getPickupType() {
		return pickupType;
	}

	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Integer getPnrPaxSegAptId() {
		return pnrPaxSegAptId;
	}

	public void setPnrPaxSegAptId(Integer pnrPaxSegAptId) {
		this.pnrPaxSegAptId = pnrPaxSegAptId;
	}

	public AirportTransferTO getProvider() {
		return provider;
	}

	public void setProvider(AirportTransferTO provider) {
		this.provider = provider;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public Date getBookingTimeStamp() {
		return bookingTimeStamp;
	}

	public void setBookingTimeStamp(Date bookingTimeStamp) {
		this.bookingTimeStamp = bookingTimeStamp;
	}

}
