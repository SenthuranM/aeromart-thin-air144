package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;


/**
 * class with common properties of BlacklistPAX. 
 * 
 * @author rajiv
 * 
 * @hibernate.class table = "T_BL_PASSENGER"
 *
 */
public class BlacklistPAX extends Tracking{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID field for Blacklist PAX Criteria ID
	 */
	private Long blacklistPAXCriteriaID;

	/**
	 * ID field for first name
	 */
//	private String fName;
	
	/**
	 * ID field for last name
	 */
//	private String lName;
	
	/**
	 * ID field for full name
	 */
	private String fullName;
	
	/**
	 * ID field for passport no
	 */
	private String passportNo;
	
	/**
	 * ID field for nationality
	 */
	private Integer nationality;
	
	/**
	 * ID field for date of birth
	 */
	private Date dateOfBirth;
	
	/**
	 * ID field for date of status
	 */
	private String status;
	
	/**
	 * ID field for remarks
	 */
	private String remarks;
	
	/**
	 * ID field for Blacklist type {Tempory / Permenant}
	 */
	private String blacklistType;
	
	/**
	 * ID field for Effective date from
	 */
	private Date effectiveFrom;
	
	/**
	 * ID field for effective date to
	 */
	private Date effectiveTo;
	
	/**
	 * ID field for Removed reason
	 */
	private String removedReason;
	
	/**
	 * ID field for valid Until date
	 */
	private Date validUntil;
	
	/**
	 * @return Returns the blacklistPAXCriteriaID.
	 * 
	 * @hibernate.id column="BL_PASSENGER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BL_PASSENGER"
	 */
	public Long getBlacklistPAXCriteriaID() {
		return blacklistPAXCriteriaID;
	}
	
	public void setBlacklistPAXCriteriaID(Long blacklistPAXCriteriaID) {
		this.blacklistPAXCriteriaID = blacklistPAXCriteriaID;
	}
	
	/**
	 * @hibernate.property column = "FIRST_NAME"
	 * 
	 * @see #fName
	 */
	/*public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}*/

	/**
	 * @hibernate.property column = "LAST_name"
	 * 
	 * @see #lName
	 */
	/*public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}*/

	/**
	 * @hibernate.property column = "PAX_FULL_NAME"
	 * 
	 * @see #fullName
	 */	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 * 
	 * @see #passportNo
	 */
	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;	
	}

	/**
	 * @return Returns the nationalityCode.
	 * @hibernate.property column = "NATIONALITY_CODE"
	 */
	public Integer getNationality() {
		return nationality;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	/**
	 * @hibernate.property column = "DOB"
	 * 
	 * @see #dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @hibernate.property column = "PAX_STATUS"
	 * 
	 * @see #status
	 */	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "REMARKS"
	 * 
	 * @see #remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @hibernate.property column = "BL_TYPE"
	 * 
	 * @see #blacklistType
	 */
	public String getBlacklistType() {
		return blacklistType;
	}

	public void setBlacklistType(String blacklistType) {
		this.blacklistType = blacklistType;
	}

	/**
	 * @hibernate.property column = "EFFECTIVE_FROM"
	 * 
	 * @see #effectiveFrom
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @hibernate.property column = "EFFECTIVE_TO"
	 * 
	 * @see #effectiveTo
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}


	/**
	 * @hibernate.property column = "REMOVED_REASON"
	 * 
	 * @see #removedReason
	 */
	public String getRemovedReason() {
		return removedReason;
	}

	public void setRemovedReason(String removedReason) {
		this.removedReason = removedReason;
	}

	/**
	 * @hibernate.property column = "VALID_UNTIL"
	 * 
	 * @see #validUntil
	 */
	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}
	
	
}
