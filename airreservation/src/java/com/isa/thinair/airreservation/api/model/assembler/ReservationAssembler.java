/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model.assembler;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.utils.assembler.CommonAssemblerUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.airreservation.api.model.PaxNameTranslations;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

/**
 * ReservationAssembler is the main assembling utility for the Reservation Object
 * 
 * @author Nilindra Fernando / Byorn
 * @since 1.0
 */
public class ReservationAssembler implements Serializable, IReservation {

	private static final long serialVersionUID = 4744971745913803098L;

	/* Holds the Reservation object */
	private Reservation reservation;

	private String bookingType;

	/* Holds the inventory fares */
	private Collection<OndFareDTO> colOndFareDTOs;

	/* Holds segment objects map */
	private Map<Integer, ReservationSegment> segmentObjectsMap;

	/* Holds the Release Time Stamp which is unique for all passengers */
	private Date pnrZuluReleaseTimeStamp;

	/* Holds the default pnr zulu release time stamp */
	private Date defaultPnrZuluReleaseTimeStamp;

	/* Holds the Tempory Payment Transaction Ids */
	private Map<Integer, CardPaymentInfo> mapTnxIds;

	private List<IInsuranceRequest> insuranceRequest;

	/* Holds the pax segment ssrs */
	private Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRMaps;

	private ReservationAdminInfoTO reservationAdminInfoTO;

	private Float fareDiscount;

	private String fareDiscountCode = null;

	/* Holds booking charges applicability level, make , modify or default */
	private int chargesApplicability = ChargeRateOperationType.ANY;

	private boolean skipDuplicationCheck = false;

	private boolean useOtherCarrierCredit;

	private Date ticketValidity;

	private Date minimumStayOver;

	private boolean allowFlightSearchAfterCutOffTime = false;

	private DiscountedFareDetails fareDiscountInfo;

	private AutoCancellationInfo autoCancellationInfo;

	private AgentCommissionDetails agentCommissionInfo;

	private Collection<Integer> validityApplicableFltSegIds = new ArrayList<Integer>();

	private LoyaltyPaymentInfo loyaltyPaymentInfo;

	private String  reasonForAllowBlPaxRes;

	private PayByVoucherInfo payByVoucherInfo;
	
	private boolean isCreateResNPayInitInDifferentFlows;

	/**
	 * Construct Reservation Object Fares is for PASSENGER FARE GENERATION PURPOSES ONLY
	 * 
	 * @throws ModuleException
	 */
	public ReservationAssembler(Collection<OndFareDTO> colOndFareDTOs, String pnr) throws ModuleException {
		setDummyReservation(new Reservation(pnr, BookingCategory.STANDARD.getCode()));
		setColOndFareDTOs(colOndFareDTOs);
		calculateTicketValidity(colOndFareDTOs);
		setSegmentObjectsMap(new HashMap<Integer, ReservationSegment>());
		setTnxIds(new HashMap<Integer, CardPaymentInfo>());
		setDefaultPnrZuluReleaseTimeStamp(new Date());
		setPaxSegmentSSRDTOMaps(new HashMap<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>>());
	}

	/**
	 * Add Reservation Information
	 * @param contactInfo
	 * @param userNote
	 */
	@Override
	public void addContactInfo(String userNote, ReservationContactInfo contactInfo, String userNoteType) {
		getDummyReservation().setUserNote(userNote);
		getDummyReservation().setUserNoteType(userNoteType);
		getDummyReservation().setContactInfo(contactInfo);
		getDummyReservation().setZuluBookingTimestamp(new Date());
	}

	/**
	 * set the total pax count
	 * 
	 * @param adultCount
	 * @param childCount
	 * @param infantCount
	 */
	public void addPaxCount(int adultCount, int childCount, int infantCount) {
		getDummyReservation().setTotalPaxAdultCount(adultCount);
		getDummyReservation().setTotalPaxChildCount(childCount);
		getDummyReservation().setTotalPaxInfantCount(infantCount);
	}

	/**
	 * Ammend the user notes
	 * 
	 * @param userNotes
	 */
	@Override
	public void ammendUserNotes(String userNotes) {
		String newUserNote = BeanUtils.nullHandler(getDummyReservation().getUserNote()) + " " + userNotes;
		getDummyReservation().setUserNote(newUserNote);
	}

	public void ammendEndorsements(String endorsement) {
		String newUserNote = BeanUtils.nullHandler(getDummyReservation().getEndorsementNote()) + " " + endorsement;
		getDummyReservation().setEndorsementNote(newUserNote);
	}

	/**
	 * Adds a outgoing segment
	 * 
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 */
	@Override
	public void addOutgoingSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			String externalRef, Integer baggageOndGroupId, Integer selectedBundledFarePeriodId, String codeShareFlightNo, String codeShareBc) {
		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setExternalReference(externalRef);
		pnrSeg.setLastFareQuotedDateZulu(CalendarUtil.getCurrentSystemTimeInZulu());
		// pnrSeg.setTicketValidTill(getTicketValidTill());
		pnrSeg.setTicketValidTill(getTicketValidTillByFltSegId(flightSegId));
		// use the same segment sequence for journey sequence
		pnrSeg.setJourneySequence(new Integer(segmentSeq));
		// pnrSeg.setMinimumStayOver(getMinimumStayOver());
		pnrSeg.setBaggageOndGroupId(baggageOndGroupId);

		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		pnrSeg.setCodeShareFlightNo(codeShareFlightNo);
		pnrSeg.setCodeShareBc(codeShareBc);
		
		pnrSeg.setBundledFarePeriodId(selectedBundledFarePeriodId);

		getSegmentObjectsMap().put(new Integer(flightSegId), pnrSeg);

		getDummyReservation().addSegment(pnrSeg);
	}

	public void addOndSegment(int ondSequence, int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			Date openRtConfirmBefore, String externalRef, Integer baggageOndGroupId, boolean waitListing, boolean isReturnOnd,
			Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass, String csOcCarrierCode,
			String csOcFlightNumber) {
		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(isReturnOnd ?
				ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE :
				ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
		
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setExternalReference(externalRef);
		pnrSeg.setLastFareQuotedDateZulu(CalendarUtil.getCurrentSystemTimeInZulu());
		// pnrSeg.setTicketValidTill(getTicketValidTill());
		pnrSeg.setTicketValidTill(getTicketValidTillByFltSegId(flightSegId));
		pnrSeg.setOpenReturnConfirmBeforeZulu(openRtConfirmBefore);
		pnrSeg.setJourneySequence(ondSequence);
		pnrSeg.setBaggageOndGroupId(baggageOndGroupId);
		// pnrSeg.setMinimumStayOver(getMinimumStayOver());
		pnrSeg.setBundledFarePeriodId(bundledFarePeriodId);
		if (codeShareFlightNumber != null && codeShareBookingClass != null) {
			pnrSeg.setCodeShareFlightNo(codeShareFlightNumber);
			pnrSeg.setCodeShareBc(codeShareBookingClass);
		}
		pnrSeg.setCsOcCarrierCode(csOcCarrierCode);
		pnrSeg.setCsOcFlightNumber(csOcFlightNumber);

		if (!waitListing) {
			// Making the segment status active purposly
			pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		} else {
			pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING);
		}

		getSegmentObjectsMap().put(new Integer(flightSegId), pnrSeg);

		getDummyReservation().addSegment(pnrSeg);
	}

	/**
	 * Add a return segment
	 * 
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param openRtConfirmBefore
	 */
	@Override
	public void addReturnSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Date openRtConfirmBefore,
			Integer returnOndGroupId, String externalRef, Integer baggageOndGroupId, Integer selectedBundledFarePeriodId,
			String codeShareFlightNo, String codeShareBc) {
		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE);
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setOpenReturnConfirmBeforeZulu(openRtConfirmBefore);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setExternalReference(externalRef);
		pnrSeg.setLastFareQuotedDateZulu(CalendarUtil.getCurrentSystemTimeInZulu());
		// pnrSeg.setTicketValidTill(getTicketValidTill());
		pnrSeg.setTicketValidTill(getTicketValidTillByFltSegId(flightSegId));
		pnrSeg.setJourneySequence(new Integer(segmentSeq));
		// pnrSeg.setMinimumStayOver(getMinimumStayOver());
		pnrSeg.setBaggageOndGroupId(baggageOndGroupId);
		pnrSeg.setBundledFarePeriodId(selectedBundledFarePeriodId);

		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		pnrSeg.setCodeShareFlightNo(codeShareFlightNo);
		pnrSeg.setCodeShareBc(codeShareBc);

		getSegmentObjectsMap().put(new Integer(flightSegId), pnrSeg);

		getDummyReservation().addSegment(pnrSeg);
	}

	@Override
	public void addOutgoingSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			String externalRef, Integer selectedBundledFarePeriodId, String codeShareFlightNo, String codeShareBc) {
		addOutgoingSegment(segmentSeq, flightSegId, ondGroupId, returnOndGroupId, externalRef, null, selectedBundledFarePeriodId,
				codeShareFlightNo, codeShareBc);
	}

	@Override
	public void addReturnSegment(int segmentSeq, int flightSegId, Integer ondGroupId, Date openRtConfirmBefore,
			Integer returnOndGroupId, String externalRef, Integer selectedBundledFarePeriodId, String codeShareFlightNo, String codeShareBc) {
		addReturnSegment(segmentSeq, flightSegId, ondGroupId, openRtConfirmBefore, returnOndGroupId, externalRef, null,
				selectedBundledFarePeriodId, codeShareFlightNo, codeShareBc);
	}

	/**
	 * Add Any External Segment information for the reservation This could be external inbound and outbound connection
	 * information
	 * 
	 * @param segmentSeq
	 * @param flightNo
	 * @param segmentCode
	 * @param cabinClassCode
	 * @param departureDate
	 * @param arrivalDate
	 */
	public void addExternalPnrSegment(int segmentSeq, String externalCarrierCode, String flightNo, String segmentCode,
			String cabinClassCode, Date departureDate, Date arrivalDate) {

		ExternalPnrSegment reservationExternalSegment = new ExternalPnrSegment();

		ExternalFlightSegment externalFlightSegment = new ExternalFlightSegment();
		externalFlightSegment.setFlightNumber(flightNo);
		externalFlightSegment.setSegmentCode(segmentCode);
		externalFlightSegment.setEstTimeDepatureLocal(departureDate);
		externalFlightSegment.setEstTimeArrivalLocal(arrivalDate);
		externalFlightSegment.setExternalCarrierCode(externalCarrierCode);
		externalFlightSegment.setStatus(ReservationInternalConstants.ReservationExternalFlightStatus.OPEN);

		reservationExternalSegment.setExternalFlightSegment(externalFlightSegment);
		reservationExternalSegment.setExternalCarrierCode(externalCarrierCode);
		reservationExternalSegment.setSegmentSeq(segmentSeq);
		reservationExternalSegment.setCabinClassCode(cabinClassCode);
		reservationExternalSegment.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);

		getDummyReservation().addExternalReservationSegment(reservationExternalSegment);
	}
	
	public void addOtherAirlineSegment(String flightNumber, String bookingCode, String marketingFlightNumber,
			String marketingBookingCode, Date departureDateTimeLocal, Date arrivalDateTimeLocal, String status,
			Integer flightSegId, String segmentCode) {
		
		OtherAirlineSegment otherAirlineSegment = new OtherAirlineSegment();
		otherAirlineSegment.setFlightNumber(flightNumber);
		otherAirlineSegment.setBookingCode(bookingCode);
		otherAirlineSegment.setMarketingFlightNumber(marketingFlightNumber);
		otherAirlineSegment.setMarketingBookingCode(marketingBookingCode);
		otherAirlineSegment.setDepartureDateTimeLocal(departureDateTimeLocal);
		otherAirlineSegment.setArrivalDateTimeLocal(arrivalDateTimeLocal);
		otherAirlineSegment.setStatus(status);
		otherAirlineSegment.setFlightSegId(flightSegId);
		if (segmentCode != null) {
			String[] values = segmentCode.split("/");
			otherAirlineSegment.setOrigin(values[0]);
			otherAirlineSegment.setDestination(values[1]);
			getDummyReservation().addOtherAirlineSegment(otherAirlineSegment);
		}		
	}


	/**
	 * add external pnr segment info to the reservation
	 * 
	 * @param externalPnrSegment
	 */
	public void addExternalPnrSegment(ExternalPnrSegment externalPnrSegment) {
		getDummyReservation().addExternalReservationSegment(externalPnrSegment);
	}

	@Override
	public void addExternalPnrSegments(Collection<ExternalPnrSegment> externalPnrSegments) {
		if (externalPnrSegments != null) {
			for (ExternalPnrSegment segment : externalPnrSegments) {
				this.addExternalPnrSegment(segment);
			}
		}
	}

	/**
	 * Injest the segment credentials information
	 * 
	 * @param credentialsDTO
	 */
	public void injectSegmentCredentials(CredentialsDTO credentialsDTO) {
		Collection<ReservationSegment> colReservationSegment = getDummyReservation().getSegments();
		if (colReservationSegment != null && !colReservationSegment.isEmpty()) {
			Iterator<ReservationSegment> itColReservationSegment = colReservationSegment.iterator();
			ReservationSegment reservationSegment;

			while (itColReservationSegment.hasNext()) {
				reservationSegment = itColReservationSegment.next();
				ReservationApiUtils.captureSegmentStatusChgInfo(reservationSegment, credentialsDTO);
				ReservationApiUtils.captureSegmentMarketingInfo(reservationSegment, credentialsDTO);
			}
		}
	}

	/**
	 * Adds an adult
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param iPayment
	 * @throws ModuleException
	 */
	@Override
	public void addSingle(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String translationlanguage, String titleOl,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException {

		ReservationPax passenger = new ReservationPax();

		passenger.setFirstName(firstName);
		passenger.setLastName(lastName);
		passenger.setTitle(title);
		passenger.setDateOfBirth(dateOfBirth);
		passenger.setNationalityCode(nationalityCode);
		passenger.setPaxSequence(new Integer(paxSequence));
		passenger.setPaxType(ReservationInternalConstants.PassengerType.ADULT);
		passenger.setIndexId(null);
		passenger.setFamilyID(new Integer(familyID));

		if (BeanUtils.nullHandler(firstNameOl).length() > 0 && BeanUtils.nullHandler(lastNameOl).length() > 0
				&& BeanUtils.nullHandler(translationlanguage).length() > 0 && BeanUtils.nullHandler(titleOl).length() > 0) {
			PaxNameTranslations paxNameTranslations = new PaxNameTranslations();
			paxNameTranslations.setLanguageCode(translationlanguage);
			paxNameTranslations.setFirstNameOl(firstNameOl);
			paxNameTranslations.setLastNameOl(lastNameOl);
			paxNameTranslations.setTitleOl(titleOl);
			passenger.setPaxNameTranslationList(paxNameTranslations);
		}

		ReservationPaxAdditionalInfo additioInfo = new ReservationPaxAdditionalInfo();

		additioInfo.setPassportNo(paxAdditionalInfoDTO.getPassportNo());
		additioInfo.setPassportExpiry(paxAdditionalInfoDTO.getPassportExpiry());
		additioInfo.setPassportIssuedCntry(paxAdditionalInfoDTO.getPassportIssuedCntry());
		additioInfo.setEmployeeID(paxAdditionalInfoDTO.getEmployeeId());
		additioInfo.setDateOfJoin(paxAdditionalInfoDTO.getDateOfJoin());
		additioInfo.setIdCategory(paxAdditionalInfoDTO.getIdCategory());
		additioInfo.setPlaceOfBirth(paxAdditionalInfoDTO.getPlaceOfBirth());
		additioInfo.setTravelDocumentType(paxAdditionalInfoDTO.getTravelDocumentType());
		additioInfo.setVisaDocNumber(paxAdditionalInfoDTO.getVisaDocNumber());
		additioInfo.setVisaDocPlaceOfIssue(paxAdditionalInfoDTO.getVisaDocPlaceOfIssue());
		additioInfo.setVisaDocIssueDate(paxAdditionalInfoDTO.getVisaDocIssueDate());
		additioInfo.setVisaApplicableCountry(paxAdditionalInfoDTO.getVisaApplicableCountry());
		additioInfo.setFfid(paxAdditionalInfoDTO.getFfid());
		additioInfo.setArrivalIntlFlightNo(arrivalIntlFltNo);
		additioInfo.setIntlFlightArrivalDate(intlFltArrivalDate);
		additioInfo.setDepartureIntlFlightNo(departureIntlFltNo);
		additioInfo.setIntlFlightDepartureDate(intlFltDepartureDate);
		additioInfo.setGroupId(pnrPaxGroupId);
		additioInfo.setNationalIDNo(paxAdditionalInfoDTO.getNationalIDNo());

		passenger.setPaxAdditionalInfo(additioInfo);
		passenger.setPaxCategory(paxCategory);

		passenger.setZuluStartTimeStamp(new Date());

		// This Zulu Release Time stamp will be overridden with actuals later
		// Adding for consistency
		passenger.setZuluReleaseTimeStamp(getDefaultPnrZuluReleaseTimeStamp());

		passenger.setPayment(iPayment);

		// Add the adult
		getDummyReservation().addPassenger(passenger);
		paxSegmentSSRMaps.put(passenger.getPaxSequence(), segmentSSRs.getNewSegmentSSRDTOMap());
	}

	/**
	 * Add a child
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param iPayment
	 * @throws ModuleException
	 */
	@Override
	public void addChild(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String titleOl, String languageCode,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException {

		ReservationPax passenger = new ReservationPax();

		passenger.setFirstName(firstName);
		passenger.setLastName(lastName);
		passenger.setTitle(title);
		passenger.setDateOfBirth(dateOfBirth);
		passenger.setNationalityCode(nationalityCode);
		passenger.setPaxSequence(new Integer(paxSequence));
		passenger.setPaxType(ReservationInternalConstants.PassengerType.CHILD);
		passenger.setIndexId(null);
		passenger.setFamilyID(new Integer(familyID));

		ReservationPaxAdditionalInfo additioInfo = new ReservationPaxAdditionalInfo();

		additioInfo.setPassportNo(paxAdditionalInfoDTO.getPassportNo());
		additioInfo.setPassportExpiry(paxAdditionalInfoDTO.getPassportExpiry());
		additioInfo.setPassportIssuedCntry(paxAdditionalInfoDTO.getPassportIssuedCntry());
		additioInfo.setEmployeeID(paxAdditionalInfoDTO.getEmployeeId());
		additioInfo.setDateOfJoin(paxAdditionalInfoDTO.getDateOfJoin());
		additioInfo.setIdCategory(paxAdditionalInfoDTO.getIdCategory());
		additioInfo.setPlaceOfBirth(paxAdditionalInfoDTO.getPlaceOfBirth());
		additioInfo.setTravelDocumentType(paxAdditionalInfoDTO.getTravelDocumentType());
		additioInfo.setVisaDocNumber(paxAdditionalInfoDTO.getVisaDocNumber());
		additioInfo.setVisaDocPlaceOfIssue(paxAdditionalInfoDTO.getVisaDocPlaceOfIssue());
		additioInfo.setVisaDocIssueDate(paxAdditionalInfoDTO.getVisaDocIssueDate());
		additioInfo.setVisaApplicableCountry(paxAdditionalInfoDTO.getVisaApplicableCountry());
		additioInfo.setFfid(paxAdditionalInfoDTO.getFfid());
		additioInfo.setNationalIDNo(paxAdditionalInfoDTO.getNationalIDNo());
		additioInfo.setArrivalIntlFlightNo(arrivalIntlFltNo);
		additioInfo.setIntlFlightArrivalDate(intlFltArrivalDate);
		additioInfo.setDepartureIntlFlightNo(departureIntlFltNo);
		additioInfo.setIntlFlightDepartureDate(intlFltDepartureDate);
		additioInfo.setGroupId(pnrPaxGroupId);

		passenger.setPaxAdditionalInfo(additioInfo);

		if (BeanUtils.nullHandler(firstNameOl).length() > 0 && BeanUtils.nullHandler(lastNameOl).length() > 0
				&& BeanUtils.nullHandler(languageCode).length() > 0 && BeanUtils.nullHandler(titleOl).length() > 0) {
			PaxNameTranslations paxNameTranslation = new PaxNameTranslations();
			paxNameTranslation.setFirstNameOl(firstNameOl);
			paxNameTranslation.setLastNameOl(lastNameOl);
			paxNameTranslation.setLanguageCode(languageCode);
			paxNameTranslation.setTitleOl(titleOl);
			passenger.setPaxNameTranslationList(paxNameTranslation);
		}

		passenger.setPaxCategory(paxCategory);

		passenger.setZuluStartTimeStamp(new Date());

		// This Zulu Release Time stamp will be overridden with actuals later
		// Adding for consistency
		passenger.setZuluReleaseTimeStamp(getDefaultPnrZuluReleaseTimeStamp());

		passenger.setPayment(iPayment);

		// Add the child
		getDummyReservation().addPassenger(passenger);
		paxSegmentSSRMaps.put(passenger.getPaxSequence(), segmentSSRs.getNewSegmentSSRDTOMap());
	}

	/**
	 * Adds an infant
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @throws ModuleException
	 */
	@Override
	public void addInfant(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int parentSeqId, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String languageCode, Integer autoCnxId,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException {

		ReservationPax passenger = new ReservationPax();
		passenger.setFirstName(firstName);
		passenger.setLastName(lastName);
		passenger.setTitle(title);
		passenger.setDateOfBirth(dateOfBirth);
		passenger.setNationalityCode(nationalityCode);
		passenger.setPaxSequence(new Integer(paxSequence));
		passenger.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
		passenger.setIndexId(new Integer(parentSeqId));
		passenger.setFamilyID(new Integer(familyID));

		ReservationPaxAdditionalInfo additioInfo = new ReservationPaxAdditionalInfo();
		additioInfo.setPassportNo(paxAdditionalInfoDTO.getPassportNo());
		additioInfo.setPassportExpiry(paxAdditionalInfoDTO.getPassportExpiry());
		additioInfo.setPassportIssuedCntry(paxAdditionalInfoDTO.getPassportIssuedCntry());
		additioInfo.setEmployeeID(paxAdditionalInfoDTO.getEmployeeId());
		additioInfo.setDateOfJoin(paxAdditionalInfoDTO.getDateOfJoin());
		additioInfo.setIdCategory(paxAdditionalInfoDTO.getIdCategory());
		additioInfo.setPlaceOfBirth(paxAdditionalInfoDTO.getPlaceOfBirth());
		additioInfo.setTravelDocumentType(paxAdditionalInfoDTO.getTravelDocumentType());
		additioInfo.setVisaDocNumber(paxAdditionalInfoDTO.getVisaDocNumber());
		additioInfo.setVisaDocPlaceOfIssue(paxAdditionalInfoDTO.getVisaDocPlaceOfIssue());
		additioInfo.setVisaDocIssueDate(paxAdditionalInfoDTO.getVisaDocIssueDate());
		additioInfo.setVisaApplicableCountry(paxAdditionalInfoDTO.getVisaApplicableCountry());
		additioInfo.setNationalIDNo(paxAdditionalInfoDTO.getNationalIDNo());
		additioInfo.setArrivalIntlFlightNo(arrivalIntlFltNo);
		additioInfo.setIntlFlightArrivalDate(intlFltArrivalDate);
		additioInfo.setDepartureIntlFlightNo(departureIntlFltNo);
		additioInfo.setIntlFlightDepartureDate(intlFltDepartureDate);
		additioInfo.setGroupId(pnrPaxGroupId);

		passenger.setPaxAdditionalInfo(additioInfo);
		passenger.setPaxCategory(paxCategory);

		if (BeanUtils.nullHandler(firstNameOl).length() > 0 && BeanUtils.nullHandler(lastNameOl).length() > 0
				&& BeanUtils.nullHandler(languageCode).length() > 0) {
			PaxNameTranslations paxNameTranslation = new PaxNameTranslations();
			paxNameTranslation.setFirstNameOl(firstNameOl);
			paxNameTranslation.setLastNameOl(lastNameOl);
			paxNameTranslation.setLanguageCode(languageCode);
			passenger.setPaxNameTranslationList(paxNameTranslation);
		}

		passenger.setZuluStartTimeStamp(new Date());

		// This Zulu Release Time stamp will be overridden with actuals later
		// Adding for consistency
		passenger.setZuluReleaseTimeStamp(getDefaultPnrZuluReleaseTimeStamp());

		passenger.setPayment(iPayment);
		passenger.setAutoCancellationId(autoCnxId);

		// Add the infant
		getDummyReservation().addPassenger(passenger);
		paxSegmentSSRMaps.put(passenger.getPaxSequence(), segmentSSRs.getNewSegmentSSRDTOMap());
	}

	/**
	 * Add a parent
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param infantSeqId
	 * @param iPayment
	 * @param ssrCode
	 * @param ssrRemarks
	 * @throws ModuleException
	 */
	@Override
	public void addParent(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int infantSeqId, PaxAdditionalInfoDTO paxAdditionalInfoDTO, String paxCategory, IPayment iPayment,
			SegmentSSRAssembler segmentSSRs, String firstNameOl, String lastNameOl, String titleOl, String languageCode,
			int familyID, String arrivalIntlFltNo, Date intlFltArrivalDate, String departureIntlFltNo, Date intlFltDepartureDate,
			String pnrPaxGroupId) throws ModuleException {

		ReservationPax passenger = new ReservationPax();
		passenger.setFirstName(firstName);
		passenger.setLastName(lastName);
		passenger.setTitle(title);
		passenger.setDateOfBirth(dateOfBirth);
		passenger.setNationalityCode(nationalityCode);
		passenger.setPaxSequence(new Integer(paxSequence));
		passenger.setPaxType(ReservationInternalConstants.PassengerType.ADULT);
		passenger.setIndexId(new Integer(infantSeqId));
		passenger.setFamilyID(new Integer(familyID));

		ReservationPaxAdditionalInfo additioInfo = new ReservationPaxAdditionalInfo();

		additioInfo.setPassportNo(paxAdditionalInfoDTO.getPassportNo());
		additioInfo.setPassportExpiry(paxAdditionalInfoDTO.getPassportExpiry());
		additioInfo.setPassportIssuedCntry(paxAdditionalInfoDTO.getPassportIssuedCntry());
		additioInfo.setEmployeeID(paxAdditionalInfoDTO.getEmployeeId());
		additioInfo.setDateOfJoin(paxAdditionalInfoDTO.getDateOfJoin());
		additioInfo.setIdCategory(paxAdditionalInfoDTO.getIdCategory());
		additioInfo.setPlaceOfBirth(paxAdditionalInfoDTO.getPlaceOfBirth());
		additioInfo.setTravelDocumentType(paxAdditionalInfoDTO.getTravelDocumentType());
		additioInfo.setVisaDocNumber(paxAdditionalInfoDTO.getVisaDocNumber());
		additioInfo.setVisaDocPlaceOfIssue(paxAdditionalInfoDTO.getVisaDocPlaceOfIssue());
		additioInfo.setVisaDocIssueDate(paxAdditionalInfoDTO.getVisaDocIssueDate());
		additioInfo.setVisaApplicableCountry(paxAdditionalInfoDTO.getVisaApplicableCountry());
		additioInfo.setNationalIDNo(paxAdditionalInfoDTO.getNationalIDNo());
		additioInfo.setArrivalIntlFlightNo(arrivalIntlFltNo);
		additioInfo.setIntlFlightArrivalDate(intlFltArrivalDate);
		additioInfo.setDepartureIntlFlightNo(departureIntlFltNo);
		additioInfo.setIntlFlightDepartureDate(intlFltDepartureDate);
		additioInfo.setFfid(paxAdditionalInfoDTO.getFfid());
		additioInfo.setGroupId(pnrPaxGroupId);

		passenger.setPaxAdditionalInfo(additioInfo);

		if (BeanUtils.nullHandler(firstNameOl).length() > 0 && BeanUtils.nullHandler(lastNameOl).length() > 0
				&& BeanUtils.nullHandler(languageCode).length() > 0 && BeanUtils.nullHandler(titleOl).length() > 0) {
			PaxNameTranslations paxNameTranslations = new PaxNameTranslations();
			paxNameTranslations.setLanguageCode(languageCode);
			paxNameTranslations.setFirstNameOl(firstNameOl);
			paxNameTranslations.setLastNameOl(lastNameOl);
			paxNameTranslations.setTitleOl(titleOl);
			passenger.setPaxNameTranslationList(paxNameTranslations);
		}
		passenger.setPaxCategory(paxCategory);

		passenger.setZuluStartTimeStamp(new Date());

		// This Zulu Release Time stamp will be overridden with actuals later
		// Adding for consistency
		passenger.setZuluReleaseTimeStamp(getDefaultPnrZuluReleaseTimeStamp());

		passenger.setPayment(iPayment);

		// Add the Parent
		getDummyReservation().addPassenger(passenger);
		paxSegmentSSRMaps.put(passenger.getPaxSequence(), segmentSSRs.getNewSegmentSSRDTOMap());
	}

	/**
	 * Return the dummy Reservation
	 * 
	 * @return
	 */
	public Reservation getDummyReservation() {
		return reservation;
	}

	/**
	 * Return the inventory fares
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> getInventoryFares() throws ModuleException {
		return ReservationApiUtils.checkSegmentsCompatibility(getColOndFareDTOs(), getSegmentObjectsMap().keySet(),
				getDummyReservation(), this.skipDuplicationCheck);
	}

	/**
	 * @return Returns the segmentObjectsMap.
	 */
	public Map<Integer, ReservationSegment> getSegmentObjectsMap() {
		return segmentObjectsMap;
	}

	/**
	 * Allows the Add Ond Fare DTO(s)
	 * 
	 * @param colOndFareDTOs
	 * @return
	 */
	@Override
	public void addOndFareDTOs(Collection<OndFareDTO> colOndFareDTOs) {
		getColOndFareDTOs().addAll(colOndFareDTOs);
	}

	/**
	 * Add Tempory Payment Entries
	 */
	@Override
	public void addTemporyPaymentEntries(Map<Integer, CardPaymentInfo> mapTnxIds) {
		if (mapTnxIds != null && mapTnxIds.size() > 0) {
			setTnxIds(mapTnxIds);
			ReservationApiUtils.updateTemporyPaymentStates(getTnxIds());
		}
	}

	@Override
	public void addGroundStationDataToPNRSegment(Map<Integer, String> groundStation,
			Map<Integer, Integer> groundSementFlightByAirFlightMap) throws ModuleException {

		CommonAssemblerUtil.addGroundStationDataToPNRSegment(groundStation, groundSementFlightByAirFlightMap,
				this.getSegmentObjectsMap());

	}

	/**
	 * Returns Tempory payment transaction Ids
	 * 
	 * @return
	 */
	public Map<Integer, CardPaymentInfo> getTnxIds() {
		return mapTnxIds;
	}

	/**
	 * @return Returns the colOndFareDTOs.
	 */
	private Collection<OndFareDTO> getColOndFareDTOs() {
		if (colOndFareDTOs == null) {
			colOndFareDTOs = new ArrayList<OndFareDTO>();
		}
		return colOndFareDTOs;
	}

	/**
	 * @param colOndFareDTOs
	 *            The colOndFareDTOs to set.
	 */
	private void setColOndFareDTOs(Collection<OndFareDTO> colOndFareDTOs) {
		this.colOndFareDTOs = colOndFareDTOs;
	}

	/**
	 * @param mapTnxIds
	 *            The mapTnxIds to set.
	 */
	private void setTnxIds(Map<Integer, CardPaymentInfo> mapTnxIds) {
		this.mapTnxIds = mapTnxIds;
	}

	/**
	 * @param reservation
	 *            The reservation to set.
	 */
	private void setDummyReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @param segmentObjectsMap
	 *            The segmentObjectsMap to set.
	 */
	private void setSegmentObjectsMap(Map<Integer, ReservationSegment> segmentObjectsMap) {
		this.segmentObjectsMap = segmentObjectsMap;
	}

	/**
	 * @return Returns the pnrZuluReleaseTimeStamp.
	 */
	public Date getPnrZuluReleaseTimeStamp() {
		return pnrZuluReleaseTimeStamp;
	}

	/**
	 * @param pnrZuluReleaseTimeStamp
	 *            The pnrZuluReleaseTimeStamp to set.
	 */
	@Override
	public void setPnrZuluReleaseTimeStamp(Date pnrZuluReleaseTimeStamp) {
		this.pnrZuluReleaseTimeStamp = pnrZuluReleaseTimeStamp;
	}

	/**
	 * @return Returns the defaultPnrZuluReleaseTimeStamp.
	 */
	private Date getDefaultPnrZuluReleaseTimeStamp() {
		return defaultPnrZuluReleaseTimeStamp;
	}

	/**
	 * @param defaultPnrZuluReleaseTimeStamp
	 *            The defaultPnrZuluReleaseTimeStamp to set.
	 */
	private void setDefaultPnrZuluReleaseTimeStamp(Date defaultPnrZuluReleaseTimeStamp) {
		this.defaultPnrZuluReleaseTimeStamp = defaultPnrZuluReleaseTimeStamp;
	}

	/** add insurance charge **/
	@Override
	public void addInsurance(IInsuranceRequest insurance) {
		insurance.prepareOriginAndDestinationInfo(this.colOndFareDTOs);
		insurance.prepareFlightSegmentsInfo(this.colOndFareDTOs);
		if(this.insuranceRequest != null){
			this.insuranceRequest.add(insurance);
		}
		else{
			this.insuranceRequest = new ArrayList<IInsuranceRequest>();
			this.insuranceRequest.add(insurance);
		}
		
	}

	/**
	 * @return the request
	 */
	public List<IInsuranceRequest> getInsuranceRequest() {
		return insuranceRequest;
	}

	/**
	 * @param originatorPnr
	 *            the originatorPnr to set
	 */
	@Override
	public void setOriginatorPnr(String originatorPnr) {
		this.reservation.setOriginatorPnr(originatorPnr);
	}

	/**
	 * 
	 * @param ohdCurrencyCopde
	 *            String the currency code
	 */
	@Override
	public void setLastCurrencyCode(String lastCurrencyCode) {
		this.getDummyReservation().setLastCurrencyCode(lastCurrencyCode);
	}

	/**
	 * @param lastModTimestamp
	 *            String the last modification time stamp
	 */
	@Override
	public void setLastModificationTimestamp(Date lastModTimestamp) {
		this.getDummyReservation().setLastModificationTimestamp(lastModTimestamp);
	}

	@Override
	public Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> getPaxSegmentSSRDTOMaps() {
		return paxSegmentSSRMaps;
	}

	public void setPaxSegmentSSRDTOMaps(Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRMaps) {
		this.paxSegmentSSRMaps = paxSegmentSSRMaps;
	}

	@Override
	public void setPnr(String pnr) {
		pnr = BeanUtils.nullHandler(pnr);

		if (pnr.length() > 0) {
			this.getDummyReservation().setPnr(pnr);
		}
	}

	@Override
	public void setExtRecordLocator(String extRecLoc) {
		extRecLoc = BeanUtils.nullHandler(extRecLoc);

		if (extRecLoc.length() > 0) {
			this.getDummyReservation().setExternalRecordLocator(extRecLoc);
		}
	}

	@Override
	public String getExtRecordLocator() {
		return this.getDummyReservation().getExternalRecordLocator();
	}

	@Override
	public void setDummyBooking(char flag) {

		this.getDummyReservation().setDummyBooking(flag);
	}

	@Override
	public void setModifiable(char flag) {
		this.getDummyReservation().setModifiableReservation(flag);
	}

	/**
	 * @return the reservationAdminInfoTO
	 */
	public ReservationAdminInfoTO getReservationAdminInfoTO() {
		return reservationAdminInfoTO;
	}

	/**
	 * @param reservationAdminInfoTO
	 *            the reservationAdminInfoTO to set
	 */
	@Override
	public void setReservationAdminInfoTO(ReservationAdminInfoTO reservationAdminInfoTO) {
		this.reservationAdminInfoTO = reservationAdminInfoTO;
	}

	/**
	 * @return
	 */
	@Override
	public int getChargesApplicability() {
		return chargesApplicability;
	}

	/**
	 * @param chargesApplicability
	 */
	@Override
	public void setChargesApplicability(int chargesApplicability) {
		this.chargesApplicability = chargesApplicability;
	}

	@Override
	public void setFareDiscount(float fareDiscount, String notes) {
		this.fareDiscount = fareDiscount;
		if (notes != null && !"".equals(notes)) {
			ammendUserNotes("FareDiscountNote: " + notes);
			ammendEndorsements("FareDiscountNote: " + notes);
		}
	}

	public Float getFareDiscount() {
		return fareDiscount;
	}

	@Override
	public void setBookingCategory(String bookingCategory) {
		getDummyReservation().setBookingCategory(bookingCategory);
	}

	@Override
	public void setItineraryFareMask(String itineraryFareMask) {
		getDummyReservation().setItineraryFareMaskFlag(itineraryFareMask);
	}

	public void calculateTicketValidity(Collection<OndFareDTO> colOndFareDTOs) throws ModuleException {
		if (colOndFareDTOs != null
				&& (AppSysParamsUtil.isTicketValidityEnabled() || OndFareUtil.getOpenReturnPeriods(colOndFareDTOs)
						.getConfirmBefore() != null)) {
			Date arr[] = ReservationApiUtils.calculateValidities(colOndFareDTOs, null, null, null, validityApplicableFltSegIds);
			setTicketValidTill(arr[0]);
			setMinimumStayOver(arr[1]);
		}
	}

	private void setMinimumStayOver(Date minimumStayOver) {
		this.minimumStayOver = minimumStayOver;
	}

	private void setTicketValidTill(Date ticketValidity) {
		this.ticketValidity = ticketValidity;
	}

	private Date getMinimumStayOver() {
		return minimumStayOver;
	}

	private Date getTicketValidTill() {
		return ticketValidity;
	}

	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	@Override
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	@Override
	public void setSkipDuplicateCheck(boolean skipDuplicationCheck) {
		this.skipDuplicationCheck = skipDuplicationCheck;
	}

	@Override
	public void setUsedOtherCarrierCredit(boolean useOtherCarrierCredit) {
		this.useOtherCarrierCredit = useOtherCarrierCredit;
	}

	public boolean isUsedOtherCarrierCredit() {
		return this.useOtherCarrierCredit;
	}

	public String getFareDiscountCode() {
		return fareDiscountCode;
	}

	@Override
	public void setFareDiscountCode(String fareDiscountCode) {
		this.fareDiscountCode = fareDiscountCode;
		getDummyReservation().setFareDiscountCode(fareDiscountCode);
	}

	public boolean isAllowFlightSearchAfterCutOffTime() {
		return allowFlightSearchAfterCutOffTime;
	}

	@Override
	public void setAllowFlightSearchAfterCutOffTime(boolean allowFlightSearchAfterCutOffTime) {
		this.allowFlightSearchAfterCutOffTime = allowFlightSearchAfterCutOffTime;
	}

	@Override
	public void setFareDiscountInfo(DiscountedFareDetails discountedFareDetails) {
		this.fareDiscountInfo = discountedFareDetails;
	}

	public DiscountedFareDetails getFareDiscountInfo() {
		return fareDiscountInfo;
	}

	public AutoCancellationInfo getAutoCancellationInfo() {
		return autoCancellationInfo;
	}

	public void setAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		this.autoCancellationInfo = autoCancellationInfo;
	}

	public Integer getGDSId() {
		return getDummyReservation().getGdsId();
	}

	@Override
	public void setGDSId(Integer gdsId) {
		getDummyReservation().setGdsId(gdsId);
	}

	public String getExternalPos() {
		return getDummyReservation().getExternalPos();
	}

	@Override
	public void setExternalPos(String externalPos) {
		getDummyReservation().setExternalPos(externalPos);
	}

	@Override
	public void setApplicableAgentCommissions(AgentCommissionDetails agentCommissions) {
		this.agentCommissionInfo = agentCommissions;
	}

	public AgentCommissionDetails getAgentCommissionInfo() {
		return agentCommissionInfo;
	}

	@Override
	public void setGroupBookingRequestID(long groupBookingRequestID) {
		if (groupBookingRequestID > 0) {
			getDummyReservation().setGroupBookingRequestID(groupBookingRequestID);
		}
	}

	public long getGroupBookingRequestID() {
		return getDummyReservation().getGroupBookingRequestID();
	}

	public Collection<Integer> getValidityApplicableFltSegIds() {
		return validityApplicableFltSegIds;
	}

	private Date getTicketValidTillByFltSegId(Integer fltSegId) {

		if (fltSegId != null && validityApplicableFltSegIds != null && validityApplicableFltSegIds.size() > 0) {
			if (validityApplicableFltSegIds.contains(fltSegId)) {
				return ticketValidity;
			}
		}

		return null;
	}

	@Override
	public void setLoyaltyPaymentInfo(LoyaltyPaymentInfo loyaltyPaymentInfo) {
		this.loyaltyPaymentInfo = loyaltyPaymentInfo;
	}

	public LoyaltyPaymentInfo getLoyaltyPaymentInfo() {
		return loyaltyPaymentInfo;
	}

	public String getReasonForAllowBlPaxRes() {
		return reasonForAllowBlPaxRes;
	}

	public void setReasonForAllowBlPaxRes(String reasonForAllowBlPaxRes) {
		this.reasonForAllowBlPaxRes = reasonForAllowBlPaxRes;
	}

	@Override
	public void setPayByVoucherInfo(PayByVoucherInfo payByVoucherInfo) {
		this.payByVoucherInfo = payByVoucherInfo;
	}

	public PayByVoucherInfo getPayByVoucherInfo() {
		return payByVoucherInfo;
	}

	public boolean isCreateResNPayInitInDifferentFlows() {
		return isCreateResNPayInitInDifferentFlows;
	}

	public void setCreateResNPayInitInDifferentFlows(boolean isCreateResNPayInitInDifferentFlows) {
		this.isCreateResNPayInitInDifferentFlows = isCreateResNPayInitInDifferentFlows;
	}

	@Override
	public void setOriginCountryOfCall(String originCountry) {
		getDummyReservation().setOriginCountryOfCall(originCountry);
	}

}
