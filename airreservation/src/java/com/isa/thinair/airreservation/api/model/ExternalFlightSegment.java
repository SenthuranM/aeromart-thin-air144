package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To track the external flight segments
 * 
 * @author mekanayake
 * @hibernate.class table = "T_EXT_FLIGHT_SEGMENT"
 */
public class ExternalFlightSegment extends Persistent implements Serializable, Comparable<ExternalFlightSegment>{

	private static final long serialVersionUID = 283110406659759269L;

	private Integer externalFlightSegId;

	private String externalCarrierCode;

	private String flightNumber;

	private String segmentCode;

	private String status;

	private Date estTimeDepatureLocal;

	private Date estTimeArrivalLocal;

	private Date estTimeDepatureZulu;

	private Date estTimeArrivalZulu;

	private Integer externalFlightSegRef;

	/**
	 * @hibernate.id column = "EXT_FLT_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_EXT_FLIGHT_SEGMENT"
	 * @return the externalFlightSegId
	 */
	public Integer getExternalFlightSegId() {
		return externalFlightSegId;
	}

	/**
	 * @param externalFlightSegId
	 *            the externalFlightSegId to set
	 */
	public void setExternalFlightSegId(Integer externalFlightSegId) {
		this.externalFlightSegId = externalFlightSegId;
	}

	/**
	 * @return the externalCarrierCode
	 * @hibernate.property column = "EXTERNAL_CARRIER_CODE"
	 */
	public String getExternalCarrierCode() {
		return externalCarrierCode;
	}

	/**
	 * @param externalCarrierCode
	 *            the externalCarrierCode to set
	 */
	public void setExternalCarrierCode(String externalCarrierCode) {
		this.externalCarrierCode = externalCarrierCode;
	}

	/**
	 * @return the flightNumber
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the segmentCode
	 * @hibernate.property column = "SEGMENT_CODE"
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the estTimeDepatureLocal
	 * @hibernate.property column = "EST_TIME_DEPARTURE_LOCAL"
	 */
	public Date getEstTimeDepatureLocal() {
		return estTimeDepatureLocal;
	}

	/**
	 * @param estTimeDepatureLocal
	 *            the estTimeDepatureLocal to set
	 */
	public void setEstTimeDepatureLocal(Date estTimeDepatureLocal) {
		this.estTimeDepatureLocal = estTimeDepatureLocal;
	}

	/**
	 * @return the estTimeArrivalLocal
	 * @hibernate.property column = "EST_TIME_ARRIVAL_LOCAL"
	 */
	public Date getEstTimeArrivalLocal() {
		return estTimeArrivalLocal;
	}

	/**
	 * @param estTimeArrivalLocal
	 *            the estTimeArrivalLocal to set
	 */
	public void setEstTimeArrivalLocal(Date estTimeArrivalLocal) {
		this.estTimeArrivalLocal = estTimeArrivalLocal;
	}

	/**
	 * @return the estTimeDepatureZulu
	 * @hibernate.property column = "EST_TIME_DEPARTURE_ZULU"
	 */
	public Date getEstTimeDepatureZulu() {
		return estTimeDepatureZulu;
	}

	/**
	 * @param estTimeDepatureZulu
	 *            the estTimeDepatureZulu to set
	 */
	public void setEstTimeDepatureZulu(Date estTimeDepatureZulu) {
		this.estTimeDepatureZulu = estTimeDepatureZulu;
	}

	/**
	 * @return the estTimeArrivalZulu
	 * @hibernate.property column = "EST_TIME_ARRIVAL_ZULU"
	 */
	public Date getEstTimeArrivalZulu() {
		return estTimeArrivalZulu;
	}

	/**
	 * @param estTimeArrivalZulu
	 *            the estTimeArrivalZulu to set
	 */
	public void setEstTimeArrivalZulu(Date estTimeArrivalZulu) {
		this.estTimeArrivalZulu = estTimeArrivalZulu;
	}

	/**
	 * @return the externalFlightSegRef
	 * @hibernate.property column = "EXT_FLT_SEG_REF"
	 */
	public Integer getExternalFlightSegRef() {
		return externalFlightSegRef;
	}

	/**
	 * @param externalFlightSegRef
	 *            the externalFlightSegRef to set
	 */
	public void setExternalFlightSegRef(Integer externalFlightSegRef) {
		this.externalFlightSegRef = externalFlightSegRef;
	}


	@Override
	public int compareTo(ExternalFlightSegment externalFlightSegment) {
		return this.getEstTimeDepatureZulu().compareTo(externalFlightSegment.getEstTimeDepatureZulu());
	}

}
