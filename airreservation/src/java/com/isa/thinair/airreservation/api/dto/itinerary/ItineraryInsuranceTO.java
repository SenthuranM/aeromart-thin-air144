package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;

public class ItineraryInsuranceTO implements Serializable {

	private static final long serialVersionUID = -4302180586744356161L;

	private String insuranceQuoteRefNumber;

	private String operatingAirline;

	private String policyCode;

	private BigDecimal totalPremium;

	private LCCInsuredJourneyDTO insuredJourneyDTO;

	private String insuranceDescription;

	private boolean showInsuranceType;

	private int insuranceType;

	public String getInsuranceQuoteRefNumber() {
		return insuranceQuoteRefNumber;
	}

	public void setInsuranceQuoteRefNumber(String insuranceQuoteRefNumber) {
		this.insuranceQuoteRefNumber = insuranceQuoteRefNumber;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}

	public BigDecimal getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}

	public LCCInsuredJourneyDTO getInsuredJourneyDTO() {
		return insuredJourneyDTO;
	}

	public void setInsuredJourneyDTO(LCCInsuredJourneyDTO insuredJourneyDTO) {
		this.insuredJourneyDTO = insuredJourneyDTO;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getInsuranceDescription() {
		return insuranceDescription;
	}

	public void setInsuranceDescription(String insuranceDescription) {
		this.insuranceDescription = insuranceDescription;
	}

	public boolean isShowInsuranceType() {
		return showInsuranceType;
	}

	public void setShowInsuranceType(boolean showInsuranceType) {
		this.showInsuranceType = showInsuranceType;
	}

	public int getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(int insuranceType) {
		this.insuranceType = insuranceType;
	}

}
