package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegmentGroup;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class Group4 extends EDISegmentGroup {

	protected FOP fop;

	public Group4() {
		super("Group4");

	}

	public void setFop(FOP fop) {
		this.fop = fop;
	}

	@Override
	public MessageSegment build() {
		addMessageSegmentIfNotEmpty(fop);
		return this;
	}

}
