/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of LCC Passenger wise payment transactions
 * 
 * @author Nilindra Fernando
 */
public class LccPaxPaymentTO extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8714224671602770700L;

	private Long lccPaxTnxId;

	private String groupPnr;

	private int paxSequence;

	private BigDecimal baseAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal paymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String paymentCurrencyCode;

	private LccResPaymentTO lccResPaymentTO;

	/**
	 * @return the lccPaxTnxId on the MC
	 */
	public Long getLccPaxTnxId() {
		return lccPaxTnxId;
	}

	/**
	 * @param lccPaxTnxId
	 *            the lccPaxTnxId to set
	 */
	public void setLccPaxTnxId(Long lccPaxTnxId) {
		this.lccPaxTnxId = lccPaxTnxId;
	}

	/**
	 * @return the groupPnr
	 */
	public String getGroupPnr() {
		return groupPnr;
	}

	/**
	 * @param groupPnr
	 *            the groupPnr to set
	 */
	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	/**
	 * @return the paxSequence
	 */
	public int getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            the paxSequence to set
	 */
	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return the paymentAmount
	 */
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param paymentAmount
	 *            the paymentAmount to set
	 */
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * @return the paymentCurrencyCode
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	/**
	 * @return the baseAmount
	 */
	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	/**
	 * @param baseAmount
	 *            the baseAmount to set
	 */
	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	/**
	 * @return the lccResPaymentTO
	 */
	public LccResPaymentTO getLccResPaymentTO() {
		return lccResPaymentTO;
	}

	/**
	 * @param lccResPaymentTO
	 *            the lccResPaymentTO to set
	 */
	public void setLccResPaymentTO(LccResPaymentTO lccResPaymentTO) {
		this.lccResPaymentTO = lccResPaymentTO;
	}
}