package com.isa.thinair.airreservation.api.dto.flightLoad;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class FlightLoadInfoDTO implements Serializable {

	private static final long serialVersionUID = 4897128015412302163L;
	private String flightNumber;
	private Date departureTimeZulu;
	private String aircraftModel;
	private Map<String, CabinInventoryDTO> cabinInventoryMap;


	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Date getDepartureTimeZulu() {
		return departureTimeZulu;
	}
	public void setDepartureTimeZulu(Date depatureDate) {
		this.departureTimeZulu = depatureDate;
	}
	public String getAircraftModel() {
		return aircraftModel;
	}
	public void setAircraftModel(String aircraftModel) {
		this.aircraftModel = aircraftModel;
	}
	public Map<String, CabinInventoryDTO> getCabinInventoryMap() {
		return cabinInventoryMap;
	}
	public void setCabinInventoryMap(Map<String, CabinInventoryDTO> cabinInventoryMap) {
		this.cabinInventoryMap = cabinInventoryMap;
	}
}
