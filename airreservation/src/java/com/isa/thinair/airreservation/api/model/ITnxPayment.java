/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;

/**
 * @author Lasantha Pambagoda
 * 
 */
public interface ITnxPayment extends Serializable {

	public int getPaymentType();

	public BigDecimal getAmount();

	public PayCurrencyDTO getPayCurrencyDTO();

	public String getPaymentCarrier();

	public void setPaymentCarrier(String paymentCarrier);

	public String getLccUniqueTnxId();

	public void setLccUniqueTnxId(String lccUniqueTnxId);
	
	/**
	 * paymentTnxId will be set only when the transaction is a refund
	 *
	 */
	public Integer getPaymentTnxId();
	
	public void setPaymentTnxId(Integer paymentTnxId);
	
}
