/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.adl.comparator;

import java.util.Comparator;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

/**
 * @author udithad
 *
 */
public class CompPassengerPnr implements Comparator<PassengerInformation> {
    @Override
    public int compare(PassengerInformation arg0, PassengerInformation arg1) {
    
    	int ret=arg0.getPnr().compareTo(arg1.getPnr());
    	
    	return ret;
    }
}
