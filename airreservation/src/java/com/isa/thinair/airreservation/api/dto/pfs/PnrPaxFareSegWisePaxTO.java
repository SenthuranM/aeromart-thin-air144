package com.isa.thinair.airreservation.api.dto.pfs;

import java.io.Serializable;

/**
 * @author Nilindra Fernando
 * @since Feb 16, 2012
 */
public class PnrPaxFareSegWisePaxTO implements Serializable {

	private static final long serialVersionUID = 8166640729838552200L;

	private Integer ppfId;

	private Integer pnrPaxId;

	private String bookingCode;

	private String paxType;

	/**
	 * @return the ppfId
	 */
	public Integer getPpfId() {
		return ppfId;
	}

	/**
	 * @param ppfId
	 *            the ppfId to set
	 */
	public void setPpfId(Integer ppfId) {
		this.ppfId = ppfId;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the bookingCode
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            the bookingCode to set
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

}
