package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Holds the refundable charge amount details
 * 
 * @author Thihara
 */
public class RefundableChargeDetailDTO implements Serializable {

	private static final long serialVersionUID = 8469787935550458581L;

	/**
	 * Whether this information is related to TAX,SUR etc.
	 */
	private String chargeType;

	/**
	 * pnrSegID
	 */
	private String pnrSegID;

	/**
	 * ppfIDs
	 */
	private List<String> ppfIDs;

	/**
	 * Contains the related PnrPaxChgIds for the particular calculation. (Related to the particular segment, used when
	 * refunding in TnxGranularityFactory)
	 */
	private List<Long> pnrPaxOndChgIDs;

	/**
	 * This contains the txnID as key and related total refundable amount as value.
	 */
	private Map<String, BigDecimal> chargeAmountMap;

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getPnrSegID() {
		return pnrSegID;
	}

	public void setPnrSegID(String pnrSegID) {
		this.pnrSegID = pnrSegID;
	}

	public List<Long> getPnrPaxOndChgIDs() {
		return pnrPaxOndChgIDs;
	}

	public void setPnrPaxOndChgIDs(List<Long> pnrPaxOndChgIDs) {
		this.pnrPaxOndChgIDs = pnrPaxOndChgIDs;
	}

	public Map<String, BigDecimal> getChargeAmountMap() {
		return chargeAmountMap;
	}

	public void setChargeAmountMap(Map<String, BigDecimal> chargeAmountMap) {
		this.chargeAmountMap = chargeAmountMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chargeType == null) ? 0 : chargeType.hashCode());
		result = prime * result + ((pnrPaxOndChgIDs == null) ? 0 : pnrPaxOndChgIDs.hashCode());
		result = prime * result + ((pnrSegID == null) ? 0 : pnrSegID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RefundableChargeDetailDTO))
			return false;
		RefundableChargeDetailDTO other = (RefundableChargeDetailDTO) obj;
		if (chargeType == null) {
			if (other.chargeType != null)
				return false;
		} else if (!chargeType.equals(other.chargeType))
			return false;
		if (pnrPaxOndChgIDs == null) {
			if (other.pnrPaxOndChgIDs != null)
				return false;
		} else if (!pnrPaxOndChgIDs.equals(other.pnrPaxOndChgIDs))
			return false;
		if (pnrSegID == null) {
			if (other.pnrSegID != null)
				return false;
		} else if (!pnrSegID.equals(other.pnrSegID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RefundableChargeDetailDTO [chargeType=" + chargeType + ", pnrSegID=" + pnrSegID + ", pnrPaxOndChgIDs="
				+ pnrPaxOndChgIDs + ", chargeAmountMap=" + chargeAmountMap + "]";
	}

	/**
	 * @return the ppfIDs
	 */
	public List<String> getPpfIDs() {
		return ppfIDs;
	}

	/**
	 * @param ppfIDs
	 *            the ppfIDs to set
	 */
	public void setPpfIDs(List<String> ppfIDs) {
		this.ppfIDs = ppfIDs;
	}

}
