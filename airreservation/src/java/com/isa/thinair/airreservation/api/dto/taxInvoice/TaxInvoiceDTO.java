package com.isa.thinair.airreservation.api.dto.taxInvoice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.TaxInvoice;

public class TaxInvoiceDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private TaxInvoice taxInvoice;
	
	private String nameOfTheAirline;
	
	private String airlineOfficeSTAddress1;
	
	private String airlineOfficeSTAddress2;
	
	private String airlineOfficeCity;
	
	private String airlineOfficeCountryCode;
	
	private String gstinForInvoiceState;
	
	private String nameOfTheRecipient;
	
	private String recipientStreetAddress1;
	
	private String recipientStreetAddress2;
	
	private String recipientCity;
	
	private String recipientCountryCode;
	
	private String gstinOfTheRecipient;
	
	private String stateOfTheRecipient;
	
	private String stateCodeOfTheRecipient;
	
	private String accountCodeOfService;
	
	private String descriptionOfService;
	// taxable + non taxable amount
	private BigDecimal totalValueOfService;
	
	private String placeOfSupply;
	
	private String placeOfSupplyState;
	
	private String digitalSignForInvoiceState;
	// tax rate 1 - cgst
	private double taxRate1Percentage;
	// tax rate 2 - sgst
	private double taxRate2Percentage;
	// tax rate 3 - igst
	private double taxRate3Percentage;
	//formatted date
	private String dateOfIssue;
	
	// cgst + sgst || igst
	private BigDecimal totalTaxAmount = BigDecimal.ZERO;
	// for debit note
	private String dateOfIssueOriginalTaxInvoice;

	public TaxInvoice getTaxInvoice() {
		return taxInvoice;
	}

	public void setTaxInvoice(TaxInvoice taxInvoice) {
		this.taxInvoice = taxInvoice;
	}

	public String getNameOfTheAirline() {
		return nameOfTheAirline;
	}

	public void setNameOfTheAirline(String nameOfTheAirline) {
		this.nameOfTheAirline = nameOfTheAirline;
	}

	public String getGstinForInvoiceState() {
		return gstinForInvoiceState;
	}

	public void setGstinForInvoiceState(String gstinForInvoiceState) {
		this.gstinForInvoiceState = gstinForInvoiceState;
	}

	public String getNameOfTheRecipient() {
		return nameOfTheRecipient;
	}

	public void setNameOfTheRecipient(String nameOfTheRecipient) {
		this.nameOfTheRecipient = nameOfTheRecipient;
	}

	public String getGstinOfTheRecipient() {
		return gstinOfTheRecipient;
	}

	public void setGstinOfTheRecipient(String gstinOfTheRecipient) {
		this.gstinOfTheRecipient = gstinOfTheRecipient;
	}

	public String getStateOfTheRecipient() {
		return stateOfTheRecipient;
	}

	public void setStateOfTheRecipient(String stateOfTheRecipient) {
		this.stateOfTheRecipient = stateOfTheRecipient;
	}

	public String getStateCodeOfTheRecipient() {
		return stateCodeOfTheRecipient;
	}

	public void setStateCodeOfTheRecipient(String stateCodeOfTheRecipient) {
		this.stateCodeOfTheRecipient = stateCodeOfTheRecipient;
	}

	public String getAccountCodeOfService() {
		return accountCodeOfService;
	}

	public void setAccountCodeOfService(String accountCodeOfService) {
		this.accountCodeOfService = accountCodeOfService;
	}

	public String getDescriptionOfService() {
		return descriptionOfService;
	}

	public void setDescriptionOfService(String descriptionOfService) {
		this.descriptionOfService = descriptionOfService;
	}

	public BigDecimal getTotalValueOfService() {
		return totalValueOfService;
	}

	public void setTotalValueOfService(BigDecimal totalValueOfService) {
		this.totalValueOfService = totalValueOfService;
	}

	public String getPlaceOfSupply() {
		return placeOfSupply;
	}

	public void setPlaceOfSupply(String placeOfSupply) {
		this.placeOfSupply = placeOfSupply;
	}

	public String getPlaceOfSupplyState() {
		return placeOfSupplyState;
	}

	public void setPlaceOfSupplyState(String placeOfSupplyState) {
		this.placeOfSupplyState = placeOfSupplyState;
	}

	public String getDigitalSignForInvoiceState() {
		return digitalSignForInvoiceState;
	}

	public void setDigitalSignForInvoiceState(String digitalSignForInvoiceState) {
		this.digitalSignForInvoiceState = digitalSignForInvoiceState;
	}

	public String getAirlineOfficeSTAddress1() {
		return airlineOfficeSTAddress1;
	}

	public void setAirlineOfficeSTAddress1(String airlineOfficeSTAddress1) {
		this.airlineOfficeSTAddress1 = airlineOfficeSTAddress1;
	}

	public String getAirlineOfficeSTAddress2() {
		return airlineOfficeSTAddress2;
	}

	public void setAirlineOfficeSTAddress2(String airlineOfficeSTAddress2) {
		this.airlineOfficeSTAddress2 = airlineOfficeSTAddress2;
	}

	public String getAirlineOfficeCity() {
		return airlineOfficeCity;
	}

	public void setAirlineOfficeCity(String airlineOfficeCity) {
		this.airlineOfficeCity = airlineOfficeCity;
	}

	public String getRecipientStreetAddress1() {
		return recipientStreetAddress1;
	}

	public void setRecipientStreetAddress1(String recipientStreetAddress1) {
		this.recipientStreetAddress1 = recipientStreetAddress1;
	}

	public String getRecipientStreetAddress2() {
		return recipientStreetAddress2;
	}

	public void setRecipientStreetAddress2(String recipientStreetAddress2) {
		this.recipientStreetAddress2 = recipientStreetAddress2;
	}

	public String getRecipientCity() {
		return recipientCity;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientCountryCode() {
		return recipientCountryCode;
	}

	public void setRecipientCountryCode(String recipientCountryCode) {
		this.recipientCountryCode = recipientCountryCode;
	}

	public String getAirlineOfficeCountryCode() {
		return airlineOfficeCountryCode;
	}

	public void setAirlineOfficeCountryCode(String airlineOfficeCountryCode) {
		this.airlineOfficeCountryCode = airlineOfficeCountryCode;
	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public double getTaxRate1Percentage() {
		return taxRate1Percentage;
	}

	public void setTaxRate1Percentage(double taxRate1Percentage) {
		this.taxRate1Percentage = taxRate1Percentage;
	}

	public double getTaxRate2Percentage() {
		return taxRate2Percentage;
	}

	public void setTaxRate2Percentage(double taxRate2Percentage) {
		this.taxRate2Percentage = taxRate2Percentage;
	}

	public double getTaxRate3Percentage() {
		return taxRate3Percentage;
	}

	public void setTaxRate3Percentage(double taxRate3Percentage) {
		this.taxRate3Percentage = taxRate3Percentage;
	}

	public String getDateOfIssueOriginalTaxInvoice() {
		return dateOfIssueOriginalTaxInvoice;
	}

	public void setDateOfIssueOriginalTaxInvoice(String dateOfIssueOriginalTaxInvoice) {
		this.dateOfIssueOriginalTaxInvoice = dateOfIssueOriginalTaxInvoice;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
}
