package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItinerarySegmentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String segmentCode;

	private String flightNo;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
}
