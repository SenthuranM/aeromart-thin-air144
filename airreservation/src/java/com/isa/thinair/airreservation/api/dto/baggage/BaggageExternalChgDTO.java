/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.baggage;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

/**
 * @author mano
 * 
 */
public class BaggageExternalChgDTO extends ExternalChgDTO {

	private static final long serialVersionUID = 1L;

	private Integer paxSequenceNo;

	private Integer flightBaggageId;

	private BigDecimal baggageCharge;

	private Integer pnrPaxId;

	private Integer pnrPaxFareId;

	private Integer pnrSegId;

	private Integer baggageId;

	private String flightRPH;

	private String baggageCode;
	
	private String ondBaggageChargeGroupId;

	private String ondBaggageGroupId;
	
	/**
	 * @return the paxSequenceNo
	 */
	public Integer getPaxSequenceNo() {
		return paxSequenceNo;
	}

	/**
	 * @param paxSequenceNo
	 *            the paxSequenceNo to set
	 */
	public void setPaxSequenceNo(Integer paxSequenceNo) {
		this.paxSequenceNo = paxSequenceNo;
	}

	/**
	 * @return the flightBaggageId
	 */
	public Integer getFlightBaggageId() {
		return flightBaggageId;
	}

	/**
	 * @param flightBaggageId
	 *            the flightBaggageId to set
	 */
	public void setFlightBaggageId(Integer flightBaggageId) {
		this.flightBaggageId = flightBaggageId;
	}

	/**
	 * @return the baggageCharge
	 */
	public BigDecimal getBaggageCharge() {
		return baggageCharge;
	}

	/**
	 * @param baggageCharge
	 *            the baggageCharge to set
	 */
	public void setBaggageCharge(BigDecimal baggageCharge) {
		this.baggageCharge = baggageCharge;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrPaxFareId
	 */
	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            the pnrPaxFareId to set
	 */
	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the baggageId
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param baggageId
	 *            the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the flightRPH
	 */
	public String getFlightRPH() {
		return flightRPH;
	}

	/**
	 * @param flightRPH
	 *            the flightRPH to set
	 */
	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		BaggageExternalChgDTO clone = new BaggageExternalChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());

		clone.setPaxSequenceNo(this.getPaxSequenceNo());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setFlightBaggageId(this.getFlightBaggageId());
		clone.setBaggageId(this.getBaggageId());

		return clone;
	}

	/**
	 * @return the baggageCode
	 */
	public String getBaggageCode() {
		return baggageCode;
	}

	/**
	 * @param baggageCode
	 *            the baggageCode to set
	 */
	public void setBaggageCode(String baggageCode) {
		this.baggageCode = baggageCode;
	}

	/**
	 * @return the ondBaggageChargeGroupId
	 */
	public String getOndBaggageChargeGroupId() {
		return ondBaggageChargeGroupId;
	}

	/**
	 * @param ondBaggageChargeGroupId the ondBaggageChargeGroupId to set
	 */
	public void setOndBaggageChargeGroupId(String ondBaggageChargeGroupId) {
		this.ondBaggageChargeGroupId = ondBaggageChargeGroupId;
	}

	/**
	 * @return the ondBaggageGroupId
	 */
	public String getOndBaggageGroupId() {
		return ondBaggageGroupId;
	}

	/**
	 * @param ondBaggageGroupId the ondBaggageGroupId to set
	 */
	public void setOndBaggageGroupId(String ondBaggageGroupId) {
		this.ondBaggageGroupId = ondBaggageGroupId;
	}


}
