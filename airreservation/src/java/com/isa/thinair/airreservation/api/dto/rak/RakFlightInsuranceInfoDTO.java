package com.isa.thinair.airreservation.api.dto.rak;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class RakFlightInsuranceInfoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3634407072405179865L;
	private String recordsNo;
	private String flightNo;
	private String originAirport;
	private Date flightDepartureDate;
	private Date notificationSendingTime;
	private String uploadResponse;

	private Collection<RakInsuredTraveler> travelers;

	public String getRecordsNo() {
		return recordsNo;
	}

	public void setRecordsNo(String recordsNo) {
		this.recordsNo = recordsNo;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public Collection<RakInsuredTraveler> getTravellers() {
		return travelers;
	}

	public void setTravellers(Collection<RakInsuredTraveler> travellers) {
		this.travelers = travellers;
	}

	/**
	 * @return the flightDepartureDate
	 */
	public Date getFlightDepartureDate() {
		return flightDepartureDate;
	}

	/**
	 * @param flightDepartureDate
	 *            the flightDepartureDate to set
	 */
	public void setFlightDepartureDate(Date flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}

	/**
	 * @return the notificationSendingTime
	 */
	public Date getNotificationSendingTime() {
		return notificationSendingTime;
	}

	/**
	 * @param notificationSendingTime
	 *            the notificationSendingTime to set
	 */
	public void setNotificationSendingTime(Date notificationSendingTime) {
		this.notificationSendingTime = notificationSendingTime;
	}

	public String getUploadResponse() {
		return uploadResponse;
	}

	public void setUploadResponse(String uploadResponse) {
		this.uploadResponse = uploadResponse;
	}

}
