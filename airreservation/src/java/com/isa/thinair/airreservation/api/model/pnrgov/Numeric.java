package com.isa.thinair.airreservation.api.model.pnrgov;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;

public class Numeric extends BasicDataElement{

	public Numeric(EDI_ELEMENT element, String data) {
		super(element);
		this.data = data;
	}
	@Override
	public boolean isValidDataFormat() {
		DE_STATUS status = element.getStatus();
		boolean isValid = false;
		switch (status) {
		case C:
			if(StringUtils.isNotEmpty(data)) {
				isValid = StringUtils.isNumeric(data) && isValidLength();
			} else {
				isValid = true;
			}
			break;
		case M:
			isValid = StringUtils.isNumeric(data) && isValidLength();
			break;
		case NA:
			isValid = true;
			break;
		}
		return isValid;
	}

}
