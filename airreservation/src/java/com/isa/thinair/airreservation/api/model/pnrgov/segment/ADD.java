package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumericSpace;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class ADD extends EDISegment {

	private String e3299;
	private String e3042;
	private String e3164;
	private String e3229;
	private String e3228;
	private String e3207;
	private String e3251;
	private String e4440;

	public ADD() {
		super(EDISegmentTag.ADD);
	}

	public void setAddressPurposeCode(String e3299) {
		this.e3299 = e3299;
	}

	public void setStreet(String e3042) {
		this.e3042 = e3042;
	}

	public void setCityName(String e3164) {
		this.e3164 = e3164;
	}

	public void setState(String e3229) {
		this.e3229 = e3229;
	}

	public void setProvince(String e3228) {
		this.e3228 = e3228;
	}

	public void setCountryCode(String e3207) {
		this.e3207 = e3207;
	}

	public void setPostCode(String e3251) {
		this.e3251 = e3251;
	}

	public void setFreeText(String e4440) {
		this.e4440 = e4440;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C031 = new CompoundDataElement("C031", DE_STATUS.C);

		CompoundDataElement C032 = new CompoundDataElement("C032", DE_STATUS.M);
		C032.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3299, e3299));
		C032.addBasicDataelement(new AlphaNumericSpace(EDI_ELEMENT.E3042, e3042));
		C032.addBasicDataelement(new AlphaNumericSpace(EDI_ELEMENT.E3164, e3164));
		C032.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3229, e3229));
		C032.addBasicDataelement(new AlphaNumericSpace(EDI_ELEMENT.E3228, e3228));
		C032.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3207, e3207));
		C032.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3251, e3251));
		C032.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E4440, e4440));

		addEDIDataElement(C031);
		addEDIDataElement(C032);

		return this;
	}

}
