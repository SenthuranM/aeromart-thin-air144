package com.isa.thinair.airreservation.api.dto.airportTransfer;

import java.io.Serializable;

/**
 * 
 * @author Manoj Dhanushka
 *
 */
public class AirportTransferNotificationDetailsDTO implements Serializable {
	
	private static final long serialVersionUID = 4916628131074044994L;

	private String title;
	
	private String firstName;
	
	private String lastName;
	
	private String pnr;
	
	private String eticketNumber;
	
	private String flightNo;
	
	private String segmentCode;
	
	private String cabinClass;
	
	private String pickupType;
	
	private String pickupDate;
	
	private String pickupTime;
	
	private String pickupAddress;
	
	private String pickupContactNo;
	
	private String paxMobileNo;
	
	private String paxPhoneNo;
	
	private String paxEmail;
	
	private String airportCode;
	
	private String providerName;
	
	private String providerEmail;
	
	public String getTitle() {
		return title;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getPnr() {
		return pnr;
	}
	
	public String getEticketNumber() {
		return eticketNumber;
	}
	
	public String getFlightNo() {
		return flightNo;
	}
	
	public String getSegmentCode() {
		return segmentCode;
	}
	
	public String getCabinClass() {
		return cabinClass;
	}
	
	public String getPickupType() {
		return pickupType;
	}
	
	public String getPickupDate() {
		return pickupDate;
	}
	
	public String getPickupTime() {
		return pickupTime;
	}
	
	public String getPickupAddress() {
		return pickupAddress;
	}
	
	public String getPickupContactNo() {
		return pickupContactNo;
	}
	
	public String getPaxMobileNo() {
		return paxMobileNo;
	}
	
	public String getPaxPhoneNo() {
		return paxPhoneNo;
	}
	
	public String getPaxEmail() {
		return paxEmail;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}
	
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
	
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}
	
	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}
	
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	
	public void setPickupTime(String pickupTime) {
		this.pickupTime = pickupTime;
	}
	
	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}
	
	public void setPickupContactNo(String pickupContactNo) {
		this.pickupContactNo = pickupContactNo;
	}
	
	public void setPaxMobileNo(String paxMobileNo) {
		this.paxMobileNo = paxMobileNo;
	}
	
	public void setPaxPhoneNo(String paxPhoneNo) {
		this.paxPhoneNo = paxPhoneNo;
	}
	
	public void setPaxEmail(String paxEmail) {
		this.paxEmail = paxEmail;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getProviderName() {
		return providerName;
	}

	public String getProviderEmail() {
		return providerEmail;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public void setProviderEmail(String providerEmail) {
		this.providerEmail = providerEmail;
	}
}
