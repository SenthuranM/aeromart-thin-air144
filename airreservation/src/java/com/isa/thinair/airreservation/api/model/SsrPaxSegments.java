package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * <h1>SsrPaxSegments</h1>
 * 
 * <p>
 * This can contain the passenger information and their segments. This is used to contain the passenger and his/her
 * segments which selected medical ssr. List from the these objects is used in email content.
 *
 * @author Shiluka Dharmasena
 * @since 2016-09-28
 */
public class SsrPaxSegments implements Serializable {

	private static final long serialVersionUID = 123456654321L;

	private String title;
	private String firstName;
	private String lastName;
	private List<String> segs;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the segs
	 */
	public List<String> getSegs() {
		return segs;
	}

	/**
	 * @param segs
	 *            the segs to set
	 */
	public void setSegs(List<String> segs) {
		this.segs = segs;
	}

}
