package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class FlightManifestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String flightNo;

	private String route;

	private String flightDate;

	private ManifestCountDTO flightLoadStatus;

	private Map<String, SegmentSummaryDTO> segmentSummaries;

	private Map<String, CabinClassSummaryDTO> cabinClassSummaries;

	private Map<String, AirportSummaryDTO> airportSummaries;

	private Collection<FlightManifestRecordDTO> paxRecords;

	private boolean showPaxIndentification;

	private boolean showSsr;

	private boolean showSeatMap;

	private boolean showMeal;

	private boolean showBaggage;

	private boolean showCreditCardDetails;

	private boolean showAirportServices;

	private String manifestLegend;

	private String summaryMode;

	private String summaryPosition;

	private String viewOption;

	private String passengersSummary;

	public String getPassengersSummary() {
		return passengersSummary;
	}

	public void setPassengersSummary(String passengerSummary) {
		this.passengersSummary = passengerSummary;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public String getRoute() {
		return route;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public ManifestCountDTO getFlightLoadStatus() {
		return flightLoadStatus;
	}

	public void setFlightLoadStatus(ManifestCountDTO flightLoadStatus) {
		this.flightLoadStatus = flightLoadStatus;
	}

	public Map<String, SegmentSummaryDTO> getSegmentSummaries() {
		return segmentSummaries;
	}

	public Map<String, CabinClassSummaryDTO> getCabinClassSummaries() {
		return cabinClassSummaries;
	}

	public Map<String, AirportSummaryDTO> getAirportSummaries() {
		return airportSummaries;
	}

	public Collection<FlightManifestRecordDTO> getPaxRecords() {
		return paxRecords;
	}

	public boolean isShowPaxIndentification() {
		return showPaxIndentification;
	}

	public boolean isShowSsr() {
		return showSsr;
	}

	public boolean isShowSeatMap() {
		return showSeatMap;
	}

	public boolean isShowMeal() {
		return showMeal;
	}

	public boolean isShowCreditCardDetails() {
		return showCreditCardDetails;
	}

	public boolean isShowAirportServices() {
		return showAirportServices;
	}

	public String getManifestLegend() {
		return manifestLegend;
	}

	public String getSummaryMode() {
		return summaryMode;
	}

	public String getSummaryPosition() {
		return summaryPosition;
	}

	public String getViewOption() {
		return viewOption;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public void setFlightDate(String date) {
		this.flightDate = date;
	}

	public void setSegmentSummaries(Map<String, SegmentSummaryDTO> segmentSummaries) {
		this.segmentSummaries = segmentSummaries;
	}

	public void setCabinClassSummaries(Map<String, CabinClassSummaryDTO> cabinClassSummaries) {
		this.cabinClassSummaries = cabinClassSummaries;
	}

	public void setAirportSummaries(Map<String, AirportSummaryDTO> airportSummaries) {
		this.airportSummaries = airportSummaries;
	}

	public void setPaxRecords(Collection<FlightManifestRecordDTO> paxRecords) {
		this.paxRecords = paxRecords;
	}

	public void setShowPaxIndentification(boolean showPaxIndentification) {
		this.showPaxIndentification = showPaxIndentification;
	}

	public void setShowSsr(boolean showSsr) {
		this.showSsr = showSsr;
	}

	public void setShowSeatMap(boolean showSeatMap) {
		this.showSeatMap = showSeatMap;
	}

	public void setShowMeal(boolean showMeal) {
		this.showMeal = showMeal;
	}

	public void setShowCreditCardDetails(boolean showCreditCardDetails) {
		this.showCreditCardDetails = showCreditCardDetails;
	}

	public void setShowAirportServices(boolean showAirportServices) {
		this.showAirportServices = showAirportServices;
	}

	public void setManifestLegend(String manifestLegend) {
		this.manifestLegend = manifestLegend;
	}

	public void setSummaryMode(String summaryMode) {
		this.summaryMode = summaryMode;
	}

	public void setSummaryPosition(String summaryPosition) {
		this.summaryPosition = summaryPosition;
	}

	public void setViewOption(String viewOption) {
		this.viewOption = viewOption;
	}

	public boolean isShowBaggage() {
		return showBaggage;
	}

	public void setShowBaggage(boolean showBaggage) {
		this.showBaggage = showBaggage;
	}

}
