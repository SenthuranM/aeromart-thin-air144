package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class TransactionResultDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2615380313348346482L;

	private XMLGregorianCalendar responseDate;

	private String transactionMoniker;

	private String authorizationCode;

	private String responseIndicator;

	private String message;

	private String resultType;

	private boolean processed;

	public XMLGregorianCalendar getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(XMLGregorianCalendar responseDate) {
		this.responseDate = responseDate;
	}

	public String getTransactionMoniker() {
		return transactionMoniker;
	}

	public void setTransactionMoniker(String transactionMoniker) {
		this.transactionMoniker = transactionMoniker;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getResponseIndicator() {
		return responseIndicator;
	}

	public void setResponseIndicator(String responseIndicator) {
		this.responseIndicator = responseIndicator;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

}
