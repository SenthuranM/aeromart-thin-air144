package com.isa.thinair.airreservation.api.dto;

import java.util.Set;

public class CSTypeBRequestDTO {
	
	private TypeBRequestDTO typeBRequestDTO;
	
	private Set<String> csOCCarrirs;

	public TypeBRequestDTO getTypeBRequestDTO() {
		return typeBRequestDTO;
	}

	public Set<String> getCsOCCarrirs() {
		return csOCCarrirs;
	}

	public void setTypeBRequestDTO(TypeBRequestDTO typeBRequestDTO) {
		this.typeBRequestDTO = typeBRequestDTO;
	}

	public void setCsOCCarrirs(Set<String> csOCCarrirs) {
		this.csOCCarrirs = csOCCarrirs;
	}
}
