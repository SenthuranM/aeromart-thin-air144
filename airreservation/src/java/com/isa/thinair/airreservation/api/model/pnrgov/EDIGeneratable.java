package com.isa.thinair.airreservation.api.model.pnrgov;

public interface EDIGeneratable {

	public String getEDIFmtData() throws PNRGOVException;
	
	public boolean isValidDataFormat();
}
