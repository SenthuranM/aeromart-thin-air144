package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Manoj Dhanushka
 * @hibernate.class table = "T_PNR_PAX_SEG_AIRPORT_TRANSFER"
 * 
 */
public class ReservationPaxSegAirportTransfer extends Persistent {

	private static final long serialVersionUID = -3132382786139314900L;

	private Integer pnrPaxSegAirportTransferId;

	private Date requestTimeStamp;

	private String contactNo;

	private String address;
	
	private Integer pnrPaxId;
	
	private Integer pnrSegId;
	
	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Integer salesChannelCode;
	
	private String userId;

	private Integer ppfId;
	
	private Integer airportTransferId;
	
	private String pickupType;
	
	private String airportCode;
	
	private String status;
	
	private Date bookingTimestamp;
	
	/**
	 * @return
	 * @hibernate.id column = "PPSAT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_SEG_AIRPORT_TRANSFER"
	 */
	public Integer getPnrPaxSegAirportTransferId() {
		return pnrPaxSegAirportTransferId;
	}

	public void setPnrPaxSegAirportTransferId(Integer pnrPaxSegAirportTransferId) {
		this.pnrPaxSegAirportTransferId = pnrPaxSegAirportTransferId;
	}

	/**
	 * @hibernate.property column = "REQUEST_TIMESTAMP"
	 */
	public Date getRequestTimeStamp() {
		return requestTimeStamp;
	}

	public void setRequestTimeStamp(Date requestTimeStamp) {
		this.requestTimeStamp = requestTimeStamp;
	}

	/**
	 * @hibernate.property column = "CONTACT_NO"
	 * 
	 */
	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	/**
	 * @hibernate.property column = "ADDRESS"
	 */
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */	
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @hibernate.property column = "USER_ID"
	 */	
	public String getUserId() {
		return userId;
	}

	/**
	 * @hibernate.property column = "PPF_ID"
	 */	
	public Integer getPpfId() {
		return ppfId;
	}

	/**
	 * @hibernate.property column = "APT_ID"
	 */	
	public Integer getAirportTransferId() {
		return airportTransferId;
	}

	/**
	 * @hibernate.property column = "PICKUP_TYPE"
	 */	
	public String getPickupType() {
		return pickupType;
	}

	/**
	 * @hibernate.property column = "AIRPORT_CODE"
	 */	
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */	
	public String getStatus() {
		return status;
	}

	/**
	 * @hibernate.property column = "BOOKING_TIMESTAMP"
	 */
	public Date getBookingTimestamp() {
		return bookingTimestamp;
	}
	
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setPpfId(Integer ppfId) {
		this.ppfId = ppfId;
	}

	public void setAirportTransferId(Integer airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setBookingTimestamp(Date bookingTimestamp) {
		this.bookingTimestamp = bookingTimestamp;
	}
	
}
