package com.isa.thinair.airreservation.api.dto.seatmap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PaxAdjAssembler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<Integer, BigDecimal> balanceMap;
	private Map<Integer, Boolean> creditMap;

	public PaxAdjAssembler() {
		this.balanceMap = new HashMap<>();
		this.creditMap = new HashMap<>();
	}

	public void addCharge(Integer pnrPaxId, BigDecimal amount) {
		balanceMap.put(pnrPaxId, AccelAeroCalculator.add(getPaxCreditAmount(pnrPaxId), amount));
	}

	private BigDecimal getPaxCreditAmount(Integer pnrPaxId) {
		if (!balanceMap.containsKey(pnrPaxId)) {
			balanceMap.put(pnrPaxId, AccelAeroCalculator.getDefaultBigDecimalZero());
		}

		return balanceMap.get(pnrPaxId);
	}

	public void removeCharge(Integer pnrPaxId, BigDecimal amount) {
		balanceMap.put(pnrPaxId, AccelAeroCalculator.subtract(getPaxCreditAmount(pnrPaxId), amount));
		if (amount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			creditMap.put(pnrPaxId, Boolean.TRUE);
		}
	}

	public boolean newTnxSequenceNeededAtPayment(Integer pnrPaxId) {
		if (creditMap.containsKey(pnrPaxId) && creditMap.get(pnrPaxId)) {
			return false;
		} else if (balanceMap.containsKey(pnrPaxId)
				&& balanceMap.get(pnrPaxId).compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
			return true;
		}

		return false;
	}

}
