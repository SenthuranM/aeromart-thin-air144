package com.isa.thinair.airreservation.api.dto.airportTransfer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Manoj Dhanushka
 */
public class AirportTransferAssembler implements Serializable, IPassengerAirportTransfer {

	private static final long serialVersionUID = -3428414375607766976L;

	private Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferInfo = new HashMap<String, Collection<PaxAirportTransferTO>>();
	private Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferTORemove = new HashMap<String, Collection<PaxAirportTransferTO>>();

	private Map<Integer, Integer> flightAirportTransferIdPnrSegId = new HashMap<Integer, Integer>();

	private final boolean isForceConfirmed;

	private PaxAdjAssembler adjAssembler;

	public AirportTransferAssembler(boolean isForceConfirmed) {
		this(null, isForceConfirmed);
	}

	public AirportTransferAssembler(PaxAdjAssembler adjAssembler, boolean isForceConfirmed) {
		this.isForceConfirmed = isForceConfirmed;
		this.adjAssembler = adjAssembler;
	}

	@Override
	public void addPaxSegmentApt(Integer pnrPaxId, Integer pnrSegId, Integer pnrPaxFareId, BigDecimal chargeAmount,
			Integer airportTransferId, String trnsDate, String trnsAddress, String trnsContact, String pickupType,
			String airportCode, TrackInfoDTO trackInfo) throws ModuleException {

		PaxAirportTransferTO paxAirportTransferTO = new PaxAirportTransferTO();
		paxAirportTransferTO.setPnrPaxId(pnrPaxId);
		paxAirportTransferTO.setPnrSegId(pnrSegId);
		paxAirportTransferTO.setPpfId(pnrPaxFareId);
		paxAirportTransferTO.setChargeAmount(chargeAmount);
		paxAirportTransferTO.setAirportTransferId(airportTransferId);
		paxAirportTransferTO.setAirportCode(airportCode);
		paxAirportTransferTO.setRequestTimeStamp(trnsDate);
		paxAirportTransferTO.setContactNo(trnsContact);
		paxAirportTransferTO.setAddress(trnsAddress);
		paxAirportTransferTO.setPickupType(pickupType);
		paxAirportTransferTO.setChgDTO(getExternalChgDTOForApt(chargeAmount, trackInfo));
		paxAirportTransferTO.setBookingTimeStamp(new Date());

		String uniqueId = AirportTransferUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		if (paxAirportTransferInfo.get(uniqueId) == null)
			paxAirportTransferInfo.put(uniqueId, new ArrayList<PaxAirportTransferTO>());
		paxAirportTransferInfo.get(uniqueId).add(paxAirportTransferTO);

		flightAirportTransferIdPnrSegId.put(pnrPaxFareId, pnrSegId);

		if (adjAssembler != null) {
			adjAssembler.addCharge(pnrPaxId, chargeAmount);
		}
	}

	public void removePaxSegmentApt(Integer pnrPaxId, Integer pnrSegId, Integer pnrPaxFareId, BigDecimal chargeAmount,
			String airportCode, TrackInfoDTO trackInfo) throws ModuleException {

		PaxAirportTransferTO paxAirportTransferTO = new PaxAirportTransferTO();
		paxAirportTransferTO.setPnrPaxId(pnrPaxId);
		paxAirportTransferTO.setPnrSegId(pnrSegId);
		paxAirportTransferTO.setAirportCode(airportCode);
		paxAirportTransferTO.setChgDTO(getExternalChgDTOForApt(chargeAmount.negate(), trackInfo));

		String uniqueId = AirportTransferUtil.makeUniqueIdForModifications(pnrPaxId, pnrPaxFareId, pnrSegId);
		if (paxAirportTransferTORemove.get(uniqueId) == null)
			paxAirportTransferTORemove.put(uniqueId, new ArrayList<PaxAirportTransferTO>());
		paxAirportTransferTORemove.get(uniqueId).add(paxAirportTransferTO);

		if (adjAssembler != null) {
			adjAssembler.removeCharge(pnrPaxId, chargeAmount);
		}
	}

	public Map<String, Collection<PaxAirportTransferTO>> getPaxAirportTransferInfo() {
		return paxAirportTransferInfo;
	}

	public Map<String, Collection<PaxAirportTransferTO>> getPaxAirportTransferTORemove() {
		return paxAirportTransferTORemove;
	}

	public void setPaxAirportTransferInfo(Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferInfo) {
		this.paxAirportTransferInfo = paxAirportTransferInfo;
	}

	public void setPaxAirportTransferTORemove(Map<String, Collection<PaxAirportTransferTO>> paxAirportTransferTORemove) {
		this.paxAirportTransferTORemove = paxAirportTransferTORemove;
	}

	public boolean isForceConfirmed() {
		return isForceConfirmed;
	}

	private ExternalChgDTO getExternalChgDTOForApt(BigDecimal charge, TrackInfoDTO trackInfo) throws ModuleException {
		if (charge == null) {
			return null;
		}

		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
		Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils
				.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfo, null);
		ExternalChgDTO externalChgDTO = (ExternalChgDTO) ((ExternalChgDTO) mapExternalChgs.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER))
				.clone();
		externalChgDTO.setAmount(charge);
		return externalChgDTO;
	}
}
