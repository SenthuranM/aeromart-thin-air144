/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;

/**
 * @author ashain
 *
 */
public class PaxPFSTo {
	private String paxFirstName;
	private String paxLastName;
	private String paxTitle;
	private String pnr;
	private int paxCount=1;
	/**
	 * @return the paxFirstName
	 */
	public String getPaxFirstName() {
		return paxFirstName;
	}
	/**
	 * @param paxFirstName the paxFirstName to set
	 */
	public void setPaxFirstName(String paxFirstName) {
		this.paxFirstName = paxFirstName;
	}
	/**
	 * @return the paxLastName
	 */
	public String getPaxLastName() {
		return paxLastName;
	}
	/**
	 * @param paxLastName the paxLastName to set
	 */
	public void setPaxLastName(String paxLastName) {
		this.paxLastName = paxLastName;
	}
	/**
	 * @return the paxTitle
	 */
	public String getPaxTitle() {
		return paxTitle;
	}
	/**
	 * @param paxTitle the paxTitle to set
	 */
	public void setPaxTitle(String paxTitle) {
		this.paxTitle = paxTitle;
	}
	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}
	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	/**
	 * @return the paxCount
	 */
	public int getPaxCount() {
		return paxCount;
	}
	/**
	 * @param paxCount the paxCount to set
	 */
	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

	
	
}
