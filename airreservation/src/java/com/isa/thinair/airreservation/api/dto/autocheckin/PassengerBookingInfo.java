/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class PassengerBookingInfo implements Serializable {

	private static final long serialVersionUID = 3640824782304686816L;
	// This Id holds the Pnr
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
