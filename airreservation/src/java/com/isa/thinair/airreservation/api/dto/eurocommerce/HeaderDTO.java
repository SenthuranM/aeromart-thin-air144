package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;
import java.util.Date;

public class HeaderDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2068060354466576172L;

	private String pnr;

	private String agencyName;

	private boolean ticketLess;

	private Date issueDate;

	private String currencyCode;

	public HeaderDTO(String pnr, String agencyName, boolean ticketLess, Date issueDate, String currencyCode) {
		super();
		this.pnr = pnr;
		this.agencyName = agencyName;
		this.ticketLess = ticketLess;
		this.issueDate = issueDate;
		this.currencyCode = currencyCode;
	}

	public HeaderDTO() {
		super();
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public boolean isTicketLess() {
		return ticketLess;
	}

	public void setTicketLess(boolean ticketLess) {
		this.ticketLess = ticketLess;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
