/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Holds the charges specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 11:33 AM 11/12/2009
 */
public class ChargeMetaTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6732060126265578320L;

	/** Holds the charge date */
	private Date chargeDate;

	/** Holds the ond code */
	private String ondCode;

	/** Holds the passenger id */
	private Integer pnrPaxId;

	/** Holds the passenger type */
	private String paxType;

	/** Holds the charge code */
	private String chargeCode;

	/** Holds the charge amount */
	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the charge group code */
	private String chargeGroupCode;

	/** Holds the charge rate id */
	private Integer chargeRateId;

	/** Holds the charge description */
	private String chargeDescription;

	private String airportTaxCategory;
	
	private BigDecimal discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal adjustmentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();	

	public static ChargeMetaTO createBasicChargeMetaTO(String chargeGroupCode, BigDecimal chargeAmount, Date chargeDate, BigDecimal discountAmount, BigDecimal adjustmentAmount) {
		ChargeMetaTO chargeMetaTO = new ChargeMetaTO();
		chargeMetaTO.setChargeGroupCode(chargeGroupCode);
		chargeMetaTO.setChargeAmount(chargeAmount);
		chargeMetaTO.setChargeDate(chargeDate);
		chargeMetaTO.setDiscountAmount(discountAmount);
		chargeMetaTO.setAdjustmentAmount(adjustmentAmount);

		return chargeMetaTO;
	}

	public static ChargeMetaTO createBasicChargeMetaTO(String chargeGroupCode, BigDecimal chargeAmount, Date chargeDate,
			BigDecimal discountAmount, BigDecimal adjustmentAmount, String chargeCode, Integer chargeRateId) {
		ChargeMetaTO chargeMetaTO = new ChargeMetaTO();
		chargeMetaTO.setChargeGroupCode(chargeGroupCode);
		chargeMetaTO.setChargeAmount(chargeAmount);
		chargeMetaTO.setChargeDate(chargeDate);
		chargeMetaTO.setDiscountAmount(discountAmount);
		chargeMetaTO.setAdjustmentAmount(adjustmentAmount);
		chargeMetaTO.setChargeCode(chargeCode);
		chargeMetaTO.setChargeRateId(chargeRateId);

		return chargeMetaTO;
	}

	public ChargeMetaTO clone() {
		ChargeMetaTO chargeMetaTO = new ChargeMetaTO();
		chargeMetaTO.setChargeAmount(this.getChargeAmount());
		chargeMetaTO.setChargeDate(this.getChargeDate());
		chargeMetaTO.setOndCode(this.getOndCode());
		chargeMetaTO.setPnrPaxId(this.getPnrPaxId());
		chargeMetaTO.setPaxType(this.getPaxType());
		chargeMetaTO.setChargeCode(this.getChargeCode());
		chargeMetaTO.setChargeGroupCode(this.getChargeGroupCode());
		chargeMetaTO.setChargeRateId(this.getChargeRateId());
		chargeMetaTO.setChargeDescription(this.getChargeDescription());
		chargeMetaTO.setDiscountAmount(this.getDiscountAmount());
		chargeMetaTO.setAdjustmentAmount(this.getAdjustmentAmount());

		return chargeMetaTO;
	}

	/**
	 * @return Returns the chargeDate.
	 */
	public Date getChargeDate() {
		return chargeDate;
	}

	/**
	 * @param chargeDate
	 *            The chargeDate to set.
	 */
	public void setChargeDate(Date chargeDate) {
		this.chargeDate = chargeDate;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the chargeGroupCode.
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            The chargeGroupCode to set.
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return Returns the ondCode.
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the chargeCode.
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the chargeRateId.
	 */
	public Integer getChargeRateId() {
		return chargeRateId;
	}

	/**
	 * @param chargeRateId
	 *            The chargeRateId to set.
	 */
	public void setChargeRateId(Integer chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	/**
	 * @return Returns the chargeDescription.
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * @param chargeDescription
	 *            The chargeDescription to set.
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public static Collection<ChargeMetaTO> sum(Collection<ChargeMetaTO>... colChargeMetaTO) {
		Collection<ChargeMetaTO> tmpColChargeMetaTO = new ArrayList<ChargeMetaTO>();

		for (int i = 0; i < colChargeMetaTO.length; i++) {
			tmpColChargeMetaTO.addAll(colChargeMetaTO[i]);
		}

		return tmpColChargeMetaTO;
	}

	public void setAirportTaxCategory(String airportTaxCategory) {
		this.airportTaxCategory = airportTaxCategory;
	}

	public String getAirportTaxCategory() {
		return airportTaxCategory;
	}

	public boolean isAirportTax() {
		return (airportTaxCategory != null);
	}

	@Override
	public String toString() {
		return "ChargeMetaTO [chargeDate=" + chargeDate + ", ondCode=" + ondCode + ", pnrPaxId=" + pnrPaxId + ", paxType="
				+ paxType + ", chargeCode=" + chargeCode + ", chargeAmount=" + chargeAmount + ", chargeGroupCode="
				+ chargeGroupCode + ", chargeRateId=" + chargeRateId + ", chargeDescription=" + chargeDescription
				+ ", airportTaxCategory=" + airportTaxCategory + "]";
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		if (discountAmount == null)
			discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		this.discountAmount = discountAmount;
	}

	public BigDecimal getAdjustmentAmount() {
		return adjustmentAmount;
	}

	public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
		if (discountAmount == null)
			discountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		this.adjustmentAmount = adjustmentAmount;
	}	
}
