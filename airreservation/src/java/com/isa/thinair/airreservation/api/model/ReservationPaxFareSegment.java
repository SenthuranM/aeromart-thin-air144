/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of reservation passenger fare segment information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table="T_PNR_PAX_FARE_SEGMENT"
 */
public class ReservationPaxFareSegment extends Persistent implements Serializable, Comparable<ReservationPaxFareSegment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 915886684413475707L;

	/** Holds the reservation passenger fare segment id */
	private Integer pnrPaxFareSegId;

	/** Holds the booking code */
	private String bookingCode;

	/** Holds the reservation segment */
	private ReservationSegment segment;

	/** Holds the reservation passenger fare */
	private ReservationPaxFare reservationPaxFare;

	/** Holds the passenger flown status default to flown status */
	private String status = ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED;

	/** Holds the pnl transmission status */
	private String pnlStat = PNLConstants.PaxPnlAdlStates.PNL_RES_STAT;

	/** Holds the group id that is shown in the PNL */
	private String groupId;

	/** Holds the reservation passenger fare */
	private Set<ReservationPaxSegmentSSR> reservationPaxSegmentSSRs;

	/** Holds the checkinStatus */
	private String checkinStatus = ReservationInternalConstants.PassengerCheckinStatus.TO_BE_CHECKED_IN;

	/** Holds if segment inventory served from nested booking class or not */
	private String bcNested = ReservationInternalConstants.BCNested.NO;
	
	private String calStat = PNLConstants.PaxPnlAdlStates.PNL_RES_STAT;

	/**
	 * @return Returns the bookingCode.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            The bookingCode to set.
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return Returns the pnrPaxFareSegId.
	 * @hibernate.id column = "PPFS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_FARE_SEGMENT"
	 */
	public Integer getPnrPaxFareSegId() {
		return pnrPaxFareSegId;
	}

	/**
	 * @param pnrPaxFareSegId
	 *            The pnrPaxFareSegId to set.
	 */
	public void setPnrPaxFareSegId(Integer pnrPaxFareSegId) {
		this.pnrPaxFareSegId = pnrPaxFareSegId;
	}

	/**
	 * @return Returns the pnrSegId.
	 */
	public Integer getPnrSegId() {
		Integer segmentId = null;

		if (segment != null) {
			segmentId = segment.getPnrSegId();
		}

		return segmentId;
	}

	/**
	 * @return Returns the segment.
	 * @hibernate.many-to-one column="PNR_SEG_ID" class="com.isa.thinair.airreservation.api.model.ReservationSegment"
	 */
	public ReservationSegment getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            The segment to set.
	 */
	public void setSegment(ReservationSegment segment) {
		this.segment = segment;
	}

	/**
	 * @hibernate.many-to-one column="PPF_ID" class="com.isa.thinair.airreservation.api.model.ReservationPaxFare"
	 * @return Returns the reservationPaxFare.
	 */
	public ReservationPaxFare getReservationPaxFare() {
		return reservationPaxFare;
	}

	/**
	 * @param reservationPaxFare
	 *            The reservationPaxFare to set.
	 */
	public void setReservationPaxFare(ReservationPaxFare reservationPaxFare) {
		this.reservationPaxFare = reservationPaxFare;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "PAX_STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPaxFareSegment) {
			ReservationPaxFareSegment castObject = (ReservationPaxFareSegment) o;
			if (castObject.getPnrPaxFareSegId() == null || this.getPnrPaxFareSegId() == null) {
				return false;
			} else if (castObject.getPnrPaxFareSegId().intValue() == this.getPnrPaxFareSegId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrPaxFareSegId()).toHashCode();
	}

	/**
	 * @return Returns the pnlStat.
	 * @hibernate.property column = "PNL_STAT"
	 */
	public String getPnlStat() {
		return pnlStat;
	}

	/**
	 * @param pnlStat
	 *            The pnlStat to set.
	 */
	public void setPnlStat(String pnlStat) {
		this.pnlStat = pnlStat;
	}

	/**
	 * @return Returns the pnlStat.
	 * @hibernate.property column = "GROUP_ID"
	 */
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return Returns the reservationPaxSegmentSSRs.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PPFS_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR"
	 */
	public Set<ReservationPaxSegmentSSR> getReservationPaxSegmentSSRs() {
		return reservationPaxSegmentSSRs;
	}

	public void setReservationPaxSegmentSSRs(Set<ReservationPaxSegmentSSR> reservationPaxSegmentSSRs) {
		this.reservationPaxSegmentSSRs = reservationPaxSegmentSSRs;
	}

	/**
	 * Add a ReservationPaxFareSegment
	 * 
	 * @param reservationPaxSegmentSSR
	 */
	public void addPaxSegmentSSR(ReservationPaxSegmentSSR reservationPaxSegmentSSR) {
		if (this.getReservationPaxSegmentSSRs() == null) {
			this.setReservationPaxSegmentSSRs(new HashSet<ReservationPaxSegmentSSR>());
		}

		reservationPaxSegmentSSR.setReservationPaxFareSegment(this);
		this.getReservationPaxSegmentSSRs().add(reservationPaxSegmentSSR);
	}

	/**
	 * @return Returns the checkinStatus.
	 * @hibernate.property column = "CHECKIN_STATUS"
	 */
	public String getCheckinStatus() {
		return checkinStatus;
	}

	/**
	 * @param checkinStatus
	 *            The checkinStatus to set.
	 */
	public void setCheckinStatus(String checkinStatus) {
		this.checkinStatus = checkinStatus;
	}

	/**
	 * @return Returns the bcNested.
	 * @hibernate.property column = "IS_NESTED"
	 */
	public String getBcNested() {
		return bcNested;
	}

	public void setBcNested(String bcNested) {
		this.bcNested = bcNested;
	}

	public Object clone() {
		ReservationPaxFareSegment reservationPaxFareSegment = new ReservationPaxFareSegment();
		reservationPaxFareSegment.setBookingCode(this.getBookingCode());
		reservationPaxFareSegment.setCheckinStatus(this.getCheckinStatus());
		reservationPaxFareSegment.setGroupId(this.getGroupId());
		reservationPaxFareSegment.setPnlStat(this.getPnlStat());
		// Note that SSR list is always set to empty in this case.
		reservationPaxFareSegment.setReservationPaxSegmentSSRs(new HashSet<ReservationPaxSegmentSSR>());
		reservationPaxFareSegment.setStatus(this.getStatus());
		reservationPaxFareSegment.setBcNested(this.getBcNested());
		reservationPaxFareSegment.setCalStat(this.getCalStat());
		return reservationPaxFareSegment;
	}
	
	public int compareTo(ReservationPaxFareSegment reservationPaxFareSegment) {
		return this.getPnrPaxFareSegId().compareTo(reservationPaxFareSegment.getPnrPaxFareSegId());
	}

	/**
	 * @return Returns the pnlStat.
	 * @hibernate.property column = "CAL_STAT"
	 */
	public String getCalStat() {
		return calStat;
	}

	public void setCalStat(String calStat) {
		this.calStat = calStat;
	}
}
