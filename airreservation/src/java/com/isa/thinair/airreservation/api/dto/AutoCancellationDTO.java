package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author rumesh
 * 
 */
public class AutoCancellationDTO implements Serializable {
	private static final long serialVersionUID = 302063525686155117L;

	private Date flightDepartureDateTimeInZulu;
	private boolean isDomesticFlight;
	private Date expireOn;
	private String cancellationType;
	private boolean hasBufferTimeCancellation;

	public AutoCancellationDTO() {

	}

	public AutoCancellationDTO(Date flightDepartureDateTimeInZulu, String cancellationType, boolean hasBufferTimeCancellation) {
		super();
		this.flightDepartureDateTimeInZulu = flightDepartureDateTimeInZulu;
		this.cancellationType = cancellationType;
		this.hasBufferTimeCancellation = hasBufferTimeCancellation;
	}

	/**
	 * @return the expireOn
	 */
	public Date getExpireOn() {
		return expireOn;
	}

	/**
	 * @param expireOn
	 *            the expireOn to set
	 */
	public void setExpireOn(Date expireOn) {
		this.expireOn = expireOn;
	}

	/**
	 * @return the cancellationType
	 */
	public String getCancellationType() {
		return cancellationType;
	}

	/**
	 * @param cancellationType
	 *            the cancellationType to set
	 */
	public void setCancellationType(String cancellationType) {
		this.cancellationType = cancellationType;
	}

	/**
	 * @return the hasBufferTimeCancellation
	 */
	public boolean isHasBufferTimeCancellation() {
		return hasBufferTimeCancellation;
	}

	/**
	 * @param hasBufferTimeCancellation
	 *            the hasBufferTimeCancellation to set
	 */
	public void setHasBufferTimeCancellation(boolean hasBufferTimeCancellation) {
		this.hasBufferTimeCancellation = hasBufferTimeCancellation;
	}

	/**
	 * @return the flightDepartureDateTimeInZulu
	 */
	public Date getFlightDepartureDateTimeInZulu() {
		return flightDepartureDateTimeInZulu;
	}

	/**
	 * @param flightDepartureDateTimeInZulu
	 *            the flightDepartureDateTimeInZulu to set
	 */
	public void setFlightDepartureDateTimeInZulu(Date flightDepartureDateTimeInZulu) {
		this.flightDepartureDateTimeInZulu = flightDepartureDateTimeInZulu;
	}

	/**
	 * @return the isDomesticFlight
	 */
	public boolean isDomesticFlight() {
		return isDomesticFlight;
	}

	/**
	 * @param isDomesticFlight
	 *            the isDomesticFlight to set
	 */
	public void setDomesticFlight(boolean isDomesticFlight) {
		this.isDomesticFlight = isDomesticFlight;
	}
}
