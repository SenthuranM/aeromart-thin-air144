/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.adl.comparator;

import java.util.Comparator;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

/**
 * @author udithad
 *
 */
public class CompPassengerFirstName implements Comparator<PassengerInformation> {

	@Override
	public int compare(PassengerInformation o1, PassengerInformation o2) {
		int value=0;
		if(o1.getFirstName() != null && o2.getFirstName() != null){
			value = o1.getFirstName().compareTo(o2.getFirstName());
		}
		return value;
	}

}
