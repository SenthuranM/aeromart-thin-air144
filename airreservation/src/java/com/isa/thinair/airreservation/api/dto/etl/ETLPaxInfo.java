package com.isa.thinair.airreservation.api.dto.etl;

import java.io.Serializable;

public class ETLPaxInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String pnr;
	private String title;
	private String firstName;
	private String lastName;
	private String paxStatus;
	private String bookingClass;
	private String cabinClass;
	private String arrivalAirport;
	private String eticketNumber;
	private Integer coupNumber;
	private String infEticketNumber;
	private Integer infCoupNumber;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String departureAirport) {
		this.arrivalAirport = departureAirport;
	}

	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	public Integer getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(Integer coupNumber) {
		this.coupNumber = coupNumber;
	}

	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	public Integer getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(Integer infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}
}
