package com.isa.thinair.airreservation.api.dto.ssr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.dto.ssr.CommonMedicalSsrParamDTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.SsrPaxSegments;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * <h1>MedicalSsrDTO</h1>
 * 
 * <p>
 * This is used to send email when the medical ssr selected. This takes reservation and paxIdsWithPayments as parameters
 * to create CommonMedicalSsrParamDTO which include the content of the mail.
 *
 * @author Shiluka Dharmasena
 * @since 2016-09-28
 */
public class MedicalSsrDTO implements Serializable {

	private static final long serialVersionUID = 123321456654L;

	private static Log log = LogFactory.getLog(MedicalSsrDTO.class);
	// private Map<Integer, List<LCCClientExternalChgDTO>> paxIdsWithPayments;
	private Map<Integer, List<LCCClientExternalChgDTO>> externalChargesWithPaxIds;
	private boolean sendMedicalSsrEmail;
	private boolean hasBalanceDue;
	// private Reservation reservation;
	private CommonReservationContactInfo contactInfo;
	private String pnr;
	private Collection<LCCClientReservationPax> passengers;

	/**
	 * This constructor is used to initiate reservation and paxIdsWithPayments.
	 * 
	 * @param paxIdsWithPayments
	 * @param reservation
	 */
	// public MedicalSsrDTO(Map<Integer, IPayment> paxIdsWithPayments, Reservation reservation) {
	// this.reservation = reservation;
	// this.paxIdsWithPayments = paxIdsWithPayments;
	// this.sendMedicalSsrEmail = false;
	// this.hasBalanceDue = hasBalanceDue;
	// }

	public MedicalSsrDTO(String pnr, Collection<LCCClientReservationPax> passengers,
			Map<Integer, List<LCCClientExternalChgDTO>> externalChargesWithPaxIds, CommonReservationContactInfo contactInfo,
			boolean hasBalanceDue) {
		this.pnr = pnr;
		this.contactInfo = contactInfo;
		this.externalChargesWithPaxIds = externalChargesWithPaxIds;
		this.sendMedicalSsrEmail = false;
		this.hasBalanceDue = hasBalanceDue;
		this.passengers = passengers;
	}

	/**
	 * This method is used to send email to the contact person if the medical ssr selected.
	 * 
	 * @return Nothing.
	 */
	public void sendEmail() {
		try {
			CommonMedicalSsrParamDTO medicalSsrParam = transformReservationIntoCommonMedicalSsrParam();

			if (this.sendMedicalSsrEmail && !this.hasBalanceDue) {
				ReservationModuleUtils.getReservationBD().sendEmailMedicalSsr(medicalSsrParam);
			}

		} catch (Exception e) {
			log.error("Error while trying to send medical email after payment", e);
		}
	}

	/**
	 * This method is used to create CommonMedicalSsrParamDTO to include the information for the email content. TODO
	 * create separate transformer to transform reservation into CommonMedicalSsrParamDTO.
	 * 
	 * @return the CommonMedicalSsrParamDTO
	 */
	private CommonMedicalSsrParamDTO transformReservationIntoCommonMedicalSsrParam() {
		CommonMedicalSsrParamDTO medicalSsrParam = new CommonMedicalSsrParamDTO();
		medicalSsrParam.setContactInfo(contactInfo);
		medicalSsrParam.setPnr(pnr);
		medicalSsrParam.setMedicalSSREmailLanguage("en");
		// medicalSsrParam.setLinkToFillMedicalSSRInfo(encodeMedicalSsrUrl());
		medicalSsrParam.setPaxWiseMedicalSsrSegments(getPaxWiseMedicalSsrSegments());
		return medicalSsrParam;
	}

	/**
	 * This method is used to create link for the medical ssr email. This link contains the passenger name and the
	 * segments.
	 * 
	 * @param staticLink
	 * @param segmentWiseMedicalSsrPaxList
	 * @return the link of the medical ssr email
	 */
	private String encodeMedicalSsrUrl(String staticLink, Map<ReservationPax, List<String>> segmentWiseMedicalSsrPaxList) {

		final String END_OF_STATIC_URL_CHAR = "?";
		final String PAX_WISE_SEGMENT_IDENTIFIER_CHAR = "%";
		final String SEGMENT_IDENTIFIER_CHAR = "_";

		StringBuilder urlBuilder = new StringBuilder(staticLink);
		urlBuilder.append(END_OF_STATIC_URL_CHAR);

		List<String> paxWiseSegmentList = new ArrayList<String>();

		Iterator iterator = segmentWiseMedicalSsrPaxList.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<ReservationPax, List<String>> paxWiseSegmentPair = (Entry<ReservationPax, List<String>>) iterator.next();
			ReservationPax reservationPax = paxWiseSegmentPair.getKey();
			List<String> segmentList = paxWiseSegmentPair.getValue();

			if (!segmentList.isEmpty()) {
				StringBuilder paxSegmentBuilder = new StringBuilder();
				paxSegmentBuilder.append(reservationPax.getFirstName());
				paxSegmentBuilder.append(SEGMENT_IDENTIFIER_CHAR);
				for (String segment : segmentList) {
					paxSegmentBuilder.append(segment);
					paxSegmentBuilder.append(SEGMENT_IDENTIFIER_CHAR);
				}
				urlBuilder.append(paxSegmentBuilder.toString());
				urlBuilder.append(PAX_WISE_SEGMENT_IDENTIFIER_CHAR);
			} else {
				continue;
			}
		}

		return urlBuilder.toString();
	}

	/**
	 * This method is used to get passengers who selected the medical ssr and their segments.
	 * 
	 * @return the ssr pax with segments list
	 */
	private List<SsrPaxSegments> getPaxWiseMedicalSsrSegments() {

		List<SsrPaxSegments> paxWiseMedicalSsrSegmentsList = new ArrayList<SsrPaxSegments>();

		for (Integer pnrPaxID : externalChargesWithPaxIds.keySet()) {

			SsrPaxSegments ssrPaxSegments = new SsrPaxSegments();

			for (Iterator<LCCClientReservationPax> paxIterator = passengers.iterator(); paxIterator.hasNext();) {
				LCCClientReservationPax pax = paxIterator.next();
				if (pax.getPaxSequence().equals(pnrPaxID)) {
					ssrPaxSegments.setTitle(pax.getTitle());
					ssrPaxSegments.setFirstName(pax.getFirstName());
					ssrPaxSegments.setLastName(pax.getLastName());
				}
			}

			// PaymentAssembler paymentAssembler = (PaymentAssembler) externalChargesWithPaxIds.get(pnrPaxID);
			List<LCCClientExternalChgDTO> externalChgDTOList = externalChargesWithPaxIds.get(pnrPaxID);
			List<String> segmentList = new ArrayList<String>();
			
			for (Iterator<LCCClientExternalChgDTO> externalChgDTOListIterator = externalChgDTOList.iterator(); externalChgDTOListIterator
					.hasNext();) {
				LCCClientExternalChgDTO externalChgDTO = externalChgDTOListIterator.next();
				if (ReservationInternalConstants.SsrTypes.MEDA.equals(externalChgDTO.getCode())) {
					//if (externalChgDTO.getSegmentSequence() == 1) {
						this.sendMedicalSsrEmail = true;
					//}
					segmentList.add(externalChgDTO.getSegmentCode());
				}
			}

			ssrPaxSegments.setSegs(segmentList);

			if (!segmentList.isEmpty()) {
				paxWiseMedicalSsrSegmentsList.add(ssrPaxSegments);
			}
		}

		return paxWiseMedicalSsrSegmentsList;
	}

	/**
	 * This method is used to get minimum segment sequence. This validation is used to escape sending two mails for a
	 * interline reservation.
	 * 
	 * @param reservation
	 * @return minimum segment sequence
	 */
	// private Integer getMinimumSegmentSequence() {
	//
	// ArrayList<Integer> segmentSequenceList = new ArrayList<Integer>();
	//
	// if (reservation.getSegments() != null) {
	// Set<ReservationSegment> segmentSet = reservation.getSegments();
	// for (Iterator<ReservationSegment> segmentIterator = segmentSet.iterator(); segmentIterator.hasNext();) {
	// ReservationSegment segment = segmentIterator.next();
	// segmentSequenceList.add(segment.getSegmentSeq());
	// }
	// }
	//
	// if (reservation.getExternalReservationSegment() != null) {
	// Set<ExternalPnrSegment> externalSegmentSet = reservation.getExternalReservationSegment();
	// for (Iterator<ExternalPnrSegment> externalSegmentIterator = externalSegmentSet.iterator();
	// externalSegmentIterator
	// .hasNext();) {
	// ExternalPnrSegment externalSegment = externalSegmentIterator.next();
	// segmentSequenceList.add(externalSegment.getSegmentSeq());
	// }
	// }
	//
	// if (!segmentSequenceList.isEmpty()) {
	// return Collections.min(segmentSequenceList);
	// } else {
	// return 0;
	// }
	//
	// }

	/**
	 * This method is used to get ssr code when ssr id is given.
	 * 
	 * @param ssrId
	 * @return the ssr code
	 */
	private String getSsrCode(Integer ssrId) {
		String ssrCode = "";

		try {
			com.isa.thinair.airmaster.api.model.SSR ssr = ReservationModuleUtils.getSsrServiceBD().getSSR(ssrId);
			ssrCode = ssr.getSsrCode();
		} catch (ModuleException e) {
			log.error("error in getting ssr code");
		}

		return ssrCode;
	}

	/**
	 * @return the paxIdsWithPayments
	 */
	public Map<Integer, List<LCCClientExternalChgDTO>> getPaxIdsWithPayments() {
		return externalChargesWithPaxIds;
	}

	/**
	 * @param paxIdsWithPayments
	 *            the paxIdsWithPayments to set
	 */
	public void setPaxIdsWithPayments(Map<Integer, List<LCCClientExternalChgDTO>> externalChargesWithPaxIds) {
		this.externalChargesWithPaxIds = externalChargesWithPaxIds;
	}

	/**
	 * @return the sendMedicalSsrEmail
	 */
	public boolean isSendMedicalSsrEmail() {
		return sendMedicalSsrEmail;
	}

	/**
	 * @param sendMedicalSsrEmail
	 *            the sendMedicalSsrEmail to set
	 */
	public void setSendMedicalSsrEmail(boolean sendMedicalSsrEmail) {
		this.sendMedicalSsrEmail = sendMedicalSsrEmail;
	}

	/**
	 * @return the reservation
	 */
	// public Reservation getReservation() {
	// return reservation;
	// }

	/**
	 * @param reservation
	 *            the reservation to set
	 */
	// public void setReservation(Reservation reservation) {
	// this.reservation = reservation;
	// }

}
