package com.isa.thinair.airreservation.api.dto.pfs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PfsXmlDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dateDownloaded;

	private String fromAddress;

	private String fromAirport; //

	private String processedStatus;

	private String flightNumber;

	private Date departureDate;
	
	private List<PfsEntryDTO> entryDTOs;

	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getProcessedStatus() {
		return processedStatus;
	}

	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public List<PfsEntryDTO> getEntryDTOs() {
		if (entryDTOs == null) {
			entryDTOs = new ArrayList<PfsEntryDTO>();
		}
		return entryDTOs;
	}

	

	
}
