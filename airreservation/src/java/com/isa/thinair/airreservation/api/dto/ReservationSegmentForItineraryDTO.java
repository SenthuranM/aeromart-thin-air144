package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ReservationSegmentForItineraryDTO implements Serializable {

	private static final long serialVersionUID = 912246483831789189L;

	private ReservationSegmentDTO reservationSegmentDTO;

	private String rowSpan;

	public ReservationSegmentForItineraryDTO() {
	}

	protected ReservationSegmentForItineraryDTO(ReservationSegmentDTO reservationSegmentDTO, Collection<ReservationSegmentDTO> segments) {
		this.reservationSegmentDTO = reservationSegmentDTO;
		calculateRowSpanForItineraryFareRulesDisplay(reservationSegmentDTO, segments);
	}

	/**
	 * @return Returns the segmentDescription.
	 */
	public String getSegmentDescription() {
		return this.reservationSegmentDTO.getSegmentDescription();
	}

	/**
	 * @return Returns the fareTO.
	 * @throws ModuleException
	 */
	public FareTO getFareTO() throws ModuleException {
		if (this.reservationSegmentDTO.getFareTO() == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}
		return this.reservationSegmentDTO.getFareTO();
	}

	public Date getSegmentExpiryDateLocal() {
		return this.reservationSegmentDTO.getSegmentExpiryDateLocal();
	}

	public String getRowSpan() {
		return this.rowSpan;
	}

	/**
	 * Returns the segment expiry date
	 * 
	 * @param dateFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getSegmentExpiryDate(String dateFormat) {
		String airportCode = this.reservationSegmentDTO.getSegmentDescription().split("/")[0];
		return BeanUtils.parseDateFormat(this.getSegmentExpiryDateLocal(), dateFormat) + " " + airportCode;
	}

	private void calculateRowSpanForItineraryFareRulesDisplay(ReservationSegmentDTO resSegDTO,
			Collection<ReservationSegmentDTO> segments) {
		int rowspanCnt = 0;
		Integer currentOndGroupID = resSegDTO.getFareGroupId();
		int ondGroupID;
		for (ReservationSegmentDTO segment : segments) {
			ondGroupID = segment.getFareGroupId();
			if (currentOndGroupID.equals(ondGroupID)) {
				rowspanCnt++;
			}
		}

		for (ReservationSegmentDTO segment : segments) {
			if (currentOndGroupID.equals(segment.getFareGroupId())) {
				if (resSegDTO.getSegmentSeq().equals(segment.getSegmentSeq())) {
					this.rowSpan = String.valueOf(rowspanCnt);
				} else {
					this.rowSpan = "";
				}
				break; // assign rowpan only for first segment with same ond group id
			}
		}
	}

}
