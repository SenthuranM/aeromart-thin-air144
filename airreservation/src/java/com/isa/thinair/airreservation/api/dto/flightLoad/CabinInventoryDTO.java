package com.isa.thinair.airreservation.api.dto.flightLoad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


public class CabinInventoryDTO implements Serializable {

	private static final long serialVersionUID = 4897128015412302163L;
	private String cabinClassCode;
	private  Collection<FlightSegmentInventoryDTO> flightSegmentInventoryList;
	
	
	public String getCabinClassCode() {
		return cabinClassCode;
	}
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
	public Collection<FlightSegmentInventoryDTO> getFlightSegmentInventoryList() {
		return flightSegmentInventoryList;
	}
	public void setFlightSegmentInventoryList(Collection<FlightSegmentInventoryDTO> flightSegmentInventoryList) {
		this.flightSegmentInventoryList = flightSegmentInventoryList;
	}
	public void addFlightSegmentInventoryList(FlightSegmentInventoryDTO flightSegmentInventory) {
		if (getFlightSegmentInventoryList() == null){
			this.flightSegmentInventoryList = new ArrayList<FlightSegmentInventoryDTO>();
		}
		flightSegmentInventoryList.add(flightSegmentInventory);
	}

	
	
}
