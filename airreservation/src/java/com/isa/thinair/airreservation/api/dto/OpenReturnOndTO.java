package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * @author Nilindra Fernando
 */
public class OpenReturnOndTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8111350275883985994L;

	private String pnr;

	private Integer ondGroupId;

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the ondGroupId
	 */
	public Integer getOndGroupId() {
		return ondGroupId;
	}

	/**
	 * @param ondGroupId
	 *            the ondGroupId to set
	 */
	public void setOndGroupId(Integer ondGroupId) {
		this.ondGroupId = ondGroupId;
	}

}
