/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @version $Id$
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.service;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CarrierDTO;
import com.isa.thinair.airreservation.api.dto.CashSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardInformationDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.UserDetails;
import com.isa.thinair.airreservation.api.dto.autocheckin.CheckinPassengersInfoDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.cesar.FlightLegPaxTypeCountTO;
import com.isa.thinair.airreservation.api.dto.eticket.PaxEticketTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightLoadInfoDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.dto.pal.PALTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVTrasmissionDetailsDTO;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.airreservation.api.model.PNRGOVTxHistoryLog;
import com.isa.thinair.airreservation.api.model.PalCalHistory;
import com.isa.thinair.airreservation.api.model.PalCalTiming;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.TermsTemplateAuditDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateDTO;
import com.isa.thinair.commons.api.dto.TermsTemplateSearchDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

/**
 * Interface to define all the methods required to access the reservation auxilliary details
 * 
 * @author Isuru
 * @since 1.0
 */
public interface ReservationAuxilliaryBD {

	public static final String SERVICE_NAME = "ReservationAuxilliaryService";

	/**
	 * Called by scheduler job that resends failed mails to sita address
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public void checkAndResendPNLADL() throws ModuleException;

	/**
	 * Called by PnlSender
	 * 
	 * @param fid
	 * @param departureairport
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce sendPNL(int fid, String departureairport) throws ModuleException;

	/**
	 * Called by AdlSender
	 * 
	 * @param fid
	 * @param departureairport
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce sendADL(int fid, String departureairport) throws ModuleException;

	// manual pnl
	public void sendPNLAuxiliary(String flightNumber, String departureStation, Date dateOfflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException;
	
	public void sendPNL(String flightNumber, String departureStation, Date dateOfflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException;

	public void resendExistingManualPNLADL(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus, String[] sitaaddresses, String mailserver, String flightNumber,
			Date dateOfFlight) throws ModuleException;

	// manual adl
	public void sendADLAuxiliary(String flightNumber, String departureStation, Date dateOfflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException;

	public Collection<AirportPassengerMessageTxHsitory> getPnlAdlHistory(String flightNumber, Date date, String departureStation)
			throws ModuleException;

	public void transferCashSales(Date date) throws ModuleException;

	public void transferCreditCardSales(Date date) throws ModuleException;

	public void transferAgentTopUp(Date date) throws ModuleException;

	public Collection<CreditCardInformationDTO> getActiveCards() throws ModuleException;

	public ArrayList<UserDetails> getUsersForTravelAgent(String agentName) throws ModuleException;

	/**
	 * 
	 * @param users
	 * @param fromDate
	 * @param toDate
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public CashSalesStatusDTO getCashSalesDetails(String users[], Date fromDate, Date toDate, String agentCode)
			throws ModuleException;

	/**
	 * 
	 * @param cards
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws ModuleException
	 */
	public CreditCardSalesStatusDTO getCreditCardSalesDetails(Integer cards[], Date fromDate, Date toDate) throws ModuleException;

	public void transferManualCreditCardSalesCollection(Collection<Date> dateCollection) throws ModuleException;

	/**
	 * Used from the Manual Screen.
	 * 
	 * @param datecollection
	 * @throws ModuleException
	 */
	public void transferManualCashSalesCollection(Collection<Date> datecollection) throws ModuleException;

	public String getSavedPnlAdl(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException;

	/**
	 * For PNL/ADL Timing Manual Screen SAVE Operation
	 * 
	 * @param timing
	 * @throws ModuleException
	 */
	public void savePnlAdlTiming(PnlAdlTiming timing) throws ModuleException;

	/**
	 * For PNL/ADL Timing Manual Screen Data Search Operation
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param startRec
	 * @param numOfRecs
	 * @return
	 * @throws ModuleException
	 */
	public Page getAllTimings(String flightNumber, String airportCode, int startRec, int numOfRecs) throws ModuleException;

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ArrayList<PNLTransMissionDetailsDTO> getFlightForPnlAdlScheduling() throws ModuleException;

	public int getFlightForPnlAdlDepartureGap(PNLTransMissionDetailsDTO pnlTransMissionDetailsDTO) throws ModuleException;

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ArrayList<MealNotificationDetailsDTO> getFlightForMealNotificationScheduling() throws ModuleException;

	public boolean hasPnlSentHistory(String flightNumber, Date fligthDepZuluDate, String airport) throws ModuleException;

	/**
	 * Called by Schedule Service and Auxiliary Operation for sending PNL ADL Insert to History
	 * 
	 * @param pnlAdlHistory
	 * @throws ModuleException
	 */
	public void insertToHistory(String msgType, HashMap<String, String> sitaMap, int flightId, String depAirport,
			String fileContent, Timestamp now, HashMap<String, Integer> ccPaxCountMap, ModuleException customError,
			String sendingMethodType, String lastGroupCode) throws ModuleException;
	
	public void updatePaxFareSegments(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds,
			String status, Map<String, String> pnrPaxIdSegIdGroupId) throws ModuleException;
	
	public void updatePnlPassengers(Collection<Integer> pnlPaxIds) throws ModuleException;
	
	public void updatePnlPassengerStatus(Collection<Integer> pnrSegIds) throws ModuleException;

	public void saveDCSPnlAdlHistory(PnlAdlHistory pnlAdlHistory) throws ModuleException;

	public Collection<FlightLegPaxTypeCountTO> getFlightLegPaxTypeCount(String flightNumber, String origin, String destination,
			Date departureDateZulu) throws ModuleException;
	
	List<InsuranceResponse> reconcileFailedInsurances(String pnr, List<Integer> insuranceRefs)  throws ModuleException;

	public void reconcileInsurancesForFailures(Date dateFrom) throws ModuleException;

	public void saveSellInfo(List<ReservationInsurance> insuranceResponse) throws ModuleException;

	public ReservationInsurance getInsurance(Integer id) throws ModuleException;

	public Collection<CarrierDTO> getCarriers() throws ModuleException;

	public ResultSet getSalesTransferServiceData(Collection<Date> dateList, Collection<Integer> nominalCodes)
			throws ModuleException;

	public String loadAirportMessagesTranslation(Integer airportMsgId, String lang) throws ModuleException;

	public void sendMealNotificaions(List<Integer> flightIds) throws ModuleException;
	
	public void sendFlightLoadInfo() throws ModuleException;

	public boolean hasFlownSegments(Collection<Integer> pngSegIds) throws ModuleException;

	public Map<Integer, String> getFltSegWiseBkgClasses(Collection<Integer> pnrSegIds) throws ModuleException;

	public Map<Integer, String> getFltSegWiseCabinClasses(Collection<Integer> pnrSegIds) throws ModuleException;

	public Map<Integer, Date> getFltSegWiseLastFQDates(List<Integer> flownPnrSegIds) throws ModuleException;

	public List<PNRGOVTrasmissionDetailsDTO> getFlightForPnrGovScheduling(String airportCode, int timePeriod,
			int pnrGovTrasmissionGap, boolean isOutbound) throws ModuleException;

	public boolean hasPnrGovSentHistory(int fltSegId, String countryCode, String airportCode, boolean isOutbound, int timePeriod)
			throws ModuleException;
	public Page getPNRGOVReports(int startrec, int numofrecs) throws ModuleException;
	
	public Page getPNRGOVReports(int startrec, int numofrecs, List criteria) throws ModuleException;
	
	public PNRGOVTxHistoryLog getHistoryLog(String logID) throws ModuleException;
	
	/**
	 * Retrieves all of the terms template data.
	 * 
	 * @return Collection of {@link TermsTemplateDTO}
	 * @throws ModuleException
	 *             If any of the underlying operations throws an error.
	 */
	public Collection<TermsTemplateDTO> getTermsTemplates() throws ModuleException;
	
	/**
	 * Updates a terms template and the audit data.
	 * 
	 * @param termsTemplateDTO
	 *            New data to be updated.
	 * @param termsAuditDTO
	 *            Audit data to be created with the update.
	 * @throws ModuleException
	 */
	public void updateTermsTemplate(TermsTemplateDTO termsTemplateDTO, TermsTemplateAuditDTO termsAuditDTO)
			throws ModuleException;

	/**
	 * Searches terms templates according to the provided criteria and returns results in a paginated context.
	 * 
	 * @param searchCriteria
	 *            Search parameters to run the query.
	 * @param start
	 *            Result set's start position.
	 * @param size
	 *            Size of the results to be returned.
	 * @return A {@link Page} encapsulating the result data in a paginated context.
	 * @throws ModuleException
	 *             If any of the underlying data access or conversion operations fails.
	 */
	public Page<TermsTemplateDTO> getTermsTemplatePage(TermsTemplateSearchDTO searchCriteria, Integer start, Integer recSize)
			throws ModuleException;

	public void publishPendingFlights(Date startDate, Date endDate) throws ModuleException;
	
	public List<PaxEticketTO> getPaxEtickets(int flightId, String stationCode);

	public void savePalCalTiming(PalCalTiming palCalTiming) throws ModuleException;

	public Page getAllPALCALTimings(String airportCode, int startRec, int numOfRecs, String flightNo) throws ModuleException;

	public void deletePAlCalTiming(PalCalTiming palcalTiming) throws ModuleException;

	public ArrayList<PALTransMissionDetailsDTO> getFlightForPalCalScheduling() throws ModuleException;

	public boolean hasPalSentHistory(String flightNumber, Date flightDepZulu, String airportCode) throws ModuleException;

	public ServiceResponce sendPALMessage(int flightId, String departureairport) throws ModuleException;

	public ServiceResponce sendCALMessage(int flightId, String departureairport) throws ModuleException;

	public void savePALCALTXHistory(PalCalHistory palCalHistory) throws ModuleException;

	public void sendPALMessage(String flightNumber, String departureStation, Date dateofflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException;

	public void sendCALAuxiliary(String flightNumber, String departureStation, Date dateOfflight, String sitaaddresses[],
			String sendingMethod) throws ModuleException;

	public void resendExistingManualPALCAL(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus, String[] sitaaddresses, String mailserver, String flightNumber,
			Date dateOfFlight) throws ModuleException;

	public String getSavedPalCal(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException;

	public Collection<AirportPassengerMessageTxHsitory> getPalCalHistory(String flightNumber, Date date, String departureStation)
			throws ModuleException;
	
	public void checkAndResendPALCAL() throws ModuleException;

	public String loadI18AirportMessagesTranslation(String airportMsgId, String lang) throws ModuleException;

	public Collection<FlightLoadInfoDTO> getFlightLoadInfo(Date departureDateFrom, Date departureDateTo) throws ModuleException;

	public void deriveAutoCheckinSeatMap(int flightId) throws ModuleException;

	public void updateAutomaticCheckin(Collection<Integer> pnrpaxIds, Integer flightId, Integer noOfAttempts, String dcsCheckinStatus,
			String dcsResponseText) throws ModuleException;

	public Collection<CheckinPassengersInfoDTO> getPassengerDetailsforCheckin(Integer flightId, Integer pnrPaxId)
			throws ModuleException;

	public PaxAutomaticCheckinTO getAutoCheckinPassengerDetails(Date departureDate, String flightNumber, String title,
			String firstName, String lastName, String pnr, String seatCode, String seatSelectionStatus) throws ModuleException;

	public void sendAutomaticCheckinDetails(Integer pnrPaxId, Integer flightId, Integer noOfAttempts, String dcsCheckinStatus,
			String dcsResponseText) throws ModuleException;

	public void updateCreiditNoteforPassenger(Integer flightId) throws ModuleException;

	public String getAutoCheckinSeatSelectionStatus(Date departureDate, String flightNumber, String title, String firstName,
			String lastName, String pnr, String checkInSeat) throws ModuleException;

	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId)
			throws ModuleException;


}
