package com.isa.thinair.airreservation.api.model.pnrgov;

public class PNRGOVException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public PNRGOVException (String message) {
		super(message);
	}
	
}
