package com.isa.thinair.airreservation.api.dto.automaticCheckin;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author aravinth.r
 *
 */
public interface IPassengerAutomaticCheckin extends AncillaryAssembler {

	public void addPaxSegmentAutoCheckin(Integer pnrPaxId, Integer flightSeatId, Integer pnrSegId, Integer pnrPaxFareId,
			BigDecimal chargeAmount, Integer autoCheckinId, String seatPref, String email, TrackInfoDTO trackInfo)
			throws ModuleException;
}
