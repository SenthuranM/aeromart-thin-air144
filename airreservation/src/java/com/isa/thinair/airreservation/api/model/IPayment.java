/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

/**
 * Payment interface where a client can use to call payment information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface IPayment {

	/**
	 * Add External Charges
	 * 
	 * @param colExternalCharges
	 */
	public void addExternalCharges(Collection<ExternalChgDTO> colExternalChgDTO);

	/**
	 * Add Card Payment
	 * 
	 * @param cardType
	 * @param eDate
	 *            should be YYMM format
	 * @param number
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param oldCCRecordId
	 * @param ipgIdentificationParamsDTO
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 */
	public void addCardPayment(int cardType, String eDate, String number, String name, String address, String securityCode,
			BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum, Integer oldCCRecordId,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, String authorizationId, Integer paymentBrokerRefNo, Integer paymentTnxId);

	/**
	 * Add Card Payment with Payment Reference
	 * 
	 * @param cardType
	 * @param eDate
	 * @param number
	 * @param name
	 * @param address
	 * @param securityCode
	 * @param amount
	 * @param appIndicatorEnum
	 * @param tnxModeEnum
	 * @param oldCCRecordId
	 * @param ipgIdentificationParamsDTO
	 * @param paymentRef
	 * @param payMode
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param userInputDTO
	 * @param paymentTnxId
	 */
	public void addCardPayment(int cardType, String eDate, String number, String name, String address, String securityCode,
			BigDecimal amount, AppIndicatorEnum appIndicatorEnum, TnxModeEnum tnxModeEnum, Integer oldCCRecordId,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, String paymentRef, Integer payMode, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, String authorizationId, Integer paymentBrokerRefNo,
			UserInputDTO userInputDTO, Integer paymentTnxId);

	/**
	 * Add Agent Credit Payment
	 * 
	 * @param agentCode
	 * @param amount
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 *            TODO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 */
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, Integer paymentTnxId);

	/**
	 * @param agentCode
	 * @param amount
	 * @param paymentCarrier
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 * @param paymentMethod
	 * @param paymentTnxId
	 *            TODO
	 */
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, String paymentMethod, Integer paymentTnxId);

	/**
	 * Add Agent credit payment with reference
	 * 
	 * @param agentCode
	 * @param amount
	 * @param paymentRef
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 */
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentRef, Integer paymentMode,
			String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId);

	/**
	 * @param agentCode
	 * @param amount
	 * @param paymentRef
	 * @param paymentMode
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentMethod
	 * @param paymentTnxId
	 *            TODO
	 */
	public void addAgentCreditPayment(String agentCode, BigDecimal amount, String paymentRef, Integer paymentMode,
			String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, String paymentMethod,
			Integer paymentTnxId);

	/**
	 * add cash payment with PayRef
	 * 
	 * @param amount
	 * @param paymentRef
	 * @param paymentMode
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 *            TODO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 */
	public void addCashPayment(BigDecimal amount, String paymentRef, Integer paymentMode, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId);

	/**
	 * Add cash payment
	 * 
	 * @param amount
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 *            TODO
	 */
	public void addCashPayment(BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId,
			Integer paymentTnxId);

	/**
	 * Add Passenger credit payment
	 * 
	 * @param balance
	 * @param debitPaxId
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 */
	public void addCreditPayment(BigDecimal balance, String debitPaxId, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			String lccUniqueTnxId, Integer paxSequence, Date expiryDate, String pnr, String paxCreditId,
			String creditUtilizingPnr, Integer paymentTnxId);

	/**
	 * Add Loyalty Credit Payment
	 * 
	 * @param loyaltyAgentCode
	 * @param amount
	 * @param loyaltyAccountNo
	 * @param paymentCarrier
	 *            carrier which the payment is made
	 * @param payCurrencyDTO
	 * @param lccUniqueTnxId
	 *            lcc unique tnx id to link the payment tnx across the carriers
	 * @param paymentTnxId
	 */
	public void addLoyaltyCreditPayment(String loyaltyAgentCode, BigDecimal amount, String loyaltyAccountNo,
			String paymentCarrier, PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId);

	public void addLMSPayment(String loyaltyMemberAccountId, String[] rewardIDs, BigDecimal amount, String paymentCarrier,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId);

	public void addVoucherPayment(VoucherDTO voucherDTO, BigDecimal totalWOExtCharges, String carrierCode,
			PayCurrencyDTO payCurrencyDTO, String lccUniqueTnxId, Integer paymentTnxId);
	
	public void addOfflinePayment(BigDecimal amount, String paymentCarrier, PayCurrencyDTO payCurrencyDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO, Integer paymentTnxId);

	public BigDecimal getTotalExternalChargesAmountWithoutConsume();

	/**
	 * Get the toal pax credit
	 * 
	 * @return totalPaxCredit
	 */
	public BigDecimal getTotalPaxCredit();

	public Collection<PaymentInfo> getPayments();

}
