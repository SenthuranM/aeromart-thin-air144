package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author manoji
 * 
 * @hibernate.class table = "T_PNR_PAX_NAMES_TRANSLATIONS"
 */
public class PaxNameTranslations extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ReservationPax reservationPax;
	private Integer pnrPaxNamesId;
	private String languageCode;
	private String firstNameOl;
	private String lastNameOl;
	private String titleOl;

	/**
	 * @return the additionalInfoId
	 * @hibernate.id column = "PNR_PAX_NAMES_TRANLATION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_NAMES_TRANSLATIONS"
	 */

	public Integer getPnrPaxNamesId() {
		return pnrPaxNamesId;
	}

	public void setPnrPaxNamesId(Integer pnrPaxNamesId) {
		this.pnrPaxNamesId = pnrPaxNamesId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "FIRST_NAME_OL"
	 */
	public String getFirstNameOl() {
		return firstNameOl;
	}

	public void setFirstNameOl(String firstNameOl) {
		this.firstNameOl = firstNameOl;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LAST_NAME_OL"
	 */
	public String getLastNameOl() {
		return lastNameOl;
	}

	public void setLastNameOl(String lastNameOl) {
		this.lastNameOl = lastNameOl;
	}

	/**
	 * @hibernate.many-to-one column="PNR_PAX_ID" class="com.isa.thinair.airreservation.api.model.ReservationPax"
	 * @return Returns the reservationPax.
	 */
	public ReservationPax getReservationPax() {
		return reservationPax;
	}

	public void setReservationPax(ReservationPax reservationPax) {
		this.reservationPax = reservationPax;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TITLE_OL"
	 */
	public String getTitleOl() {
		return titleOl;
	}

	public void setTitleOl(String titleOl) {
		this.titleOl = titleOl;
	}
}