/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model.assembler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.AgentCommissionDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.utils.assembler.CommonAssemblerUtil;
import com.isa.thinair.airreservation.api.dto.AutoCancellationDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

/**
 * ReservationAssembler is the main assembling utility for the Reservation Object
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class SegmentAssembler implements Serializable, ISegment {

	private static final long serialVersionUID = 226137902988068005L;

	/* Holds the Reservation segment objects */
	private Collection<ReservationSegment> segments;

	/* Holds the Reservation external segment objects */
	private Collection<ExternalPnrSegment> externalSegments;

	/* Holds the inventory fares */
	private Collection<OndFareDTO> colOndFareDTOs;

	/* Holds segment objects map */
	private Map<Integer, ReservationSegment> segmentObjectsMap;

	/* Holds passenger payments map */
	private Map<Integer, IPayment> passengerPaymentsMap;

	/* Holds the overridden zulu release time stamp */
	private Date zuluReleaseTimeStamp;

	/* Holds the Tempory Payment Transaction Ids */
	private Map<Integer, CardPaymentInfo> mapTnxIds;

	private List<IInsuranceRequest> insuranceRequests;

	/* Holds the last modification time stamp */
	private Date lastModificationTimeStamp;

	/* Holds the last currency code */
	private String lastCurrencyCode;

	private Float fareDiscount;

	private String userNote;

	private String endorsementNotes;

	private Date lastFareQuotedDate;

	/* validity related variables */
	private Date validityExistingFirstDept;

	private Long validityMostRestMaxStayOver;

	private boolean validityCalculated = false;

	private Date ticketValidity;

	private DiscountedFareDetails fareDiscountInfo;

	private AutoCancellationInfo autoCancellationInfo;

	private AgentCommissionDetails agentCommissionInfo;
	
	private Collection<Integer> validityApplicableFltSegIds = new ArrayList<Integer>();

	/**
	 * Construct Reservation Object Fares is for PASSENGER FARE GENERATION PURPOSES ONLY
	 */
	public SegmentAssembler(Collection<OndFareDTO> colOndFareDTOs) {
		validityCalculated = false;
		setColOndFareDTOs(colOndFareDTOs);
		setSegmentObjectsMap(new HashMap<Integer, ReservationSegment>());
		setPassengerPaymentsMap(new HashMap<Integer, IPayment>());
		setTnxIds(new HashMap<Integer, CardPaymentInfo>());
		setInsuranceRequests(new ArrayList<IInsuranceRequest>());
	}

	public SegmentAssembler(Collection<OndFareDTO> ondFareDTOs, Date lastFareQuotedDate) {
		this(ondFareDTOs);
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	/**
	 * Adds the passenger payments
	 * 
	 * @param pnrPaxId
	 * @param iPayment
	 */
	@Override
	public void addPassengerPayments(Integer pnrPaxId, IPayment iPayment) {
		getPassengerPaymentsMap().put(pnrPaxId, iPayment);
	}

	@Override
	public void addOutgoingSegment(int journeySeq, int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			Integer baggageOndGrpId, Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass) {
		if (getDummySegments() == null) {
			setSegments(new HashSet<ReservationSegment>());
		}

		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setLastFareQuotedDateZulu(getLastFareQuotedDate());
		pnrSeg.setTicketValidTill(getTicketValidTill(false));
		pnrSeg.setJourneySequence(journeySeq);
		pnrSeg.setBaggageOndGroupId(baggageOndGrpId);
		pnrSeg.setBundledFarePeriodId(bundledFarePeriodId);
		pnrSeg.setCodeShareBc(codeShareBookingClass);
		pnrSeg.setCodeShareFlightNo(codeShareFlightNumber);

		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);

		getSegmentObjectsMap().put(new Integer(flightSegId), pnrSeg);

		getDummySegments().add(pnrSeg);
	}

	@Override
	public void addReturnSegment(int journeySeq, int segmentSeq, int flightSegId, Integer ondGroupId, Date openRtConfirmBefore,
			Integer returnOndGroupId, Integer baggageOndGrpId, Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass) {
		if (getDummySegments() == null) {
			setSegments(new HashSet<ReservationSegment>());
		}

		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE);
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setOpenReturnConfirmBeforeZulu(openRtConfirmBefore);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setLastFareQuotedDateZulu(getLastFareQuotedDate());
		pnrSeg.setTicketValidTill(getTicketValidTill(openRtConfirmBefore != null));
		pnrSeg.setJourneySequence(journeySeq);
		pnrSeg.setBaggageOndGroupId(baggageOndGrpId);
		pnrSeg.setBundledFarePeriodId(bundledFarePeriodId);
		pnrSeg.setCodeShareBc(codeShareBookingClass);
		pnrSeg.setCodeShareFlightNo(codeShareFlightNumber);

		// Making the segment status active purposly
		pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);

		getSegmentObjectsMap().put(new Integer(flightSegId), pnrSeg);

		getDummySegments().add(pnrSeg);
	}

	/**
	 * Injest the segment credentials information
	 * 
	 * @param credentialsDTO
	 */
	public void injectSegmentCredentials(CredentialsDTO credentialsDTO) {
		Iterator<ReservationSegment> itColReservationSegment = getDummySegments().iterator();
		ReservationSegment reservationSegment;

		while (itColReservationSegment.hasNext()) {
			reservationSegment = (ReservationSegment) itColReservationSegment.next();
			ReservationApiUtils.captureSegmentStatusChgInfo(reservationSegment, credentialsDTO);
		}
	}

	/**
	 * Return the dummy segments
	 * 
	 * @return
	 */
	public Collection<ReservationSegment> getDummySegments() {
		return segments;
	}

	/**
	 * Return the inventory fares
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> getInventoryFares() throws ModuleException {
		return ReservationApiUtils.checkSegmentsCompatibilityOnly(getColOndFareDTOs(), getSegmentObjectsMap().keySet());
	}

	/**
	 * Returns ordered new flight segment ids
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Integer> getOrderedNewFlightSegmentIds() throws ModuleException {
		if (getDummySegments() == null || getDummySegments().size() == 0) {
			throw new ModuleException("airreservations.cancellation.noNewFlgSegIds");
		}

		Map<Integer, Integer> mapSeq;
		Integer flightSegId;
		Integer ondGroupId;
		ReservationSegment reservationSegment;
		Map<Integer, Map<Integer, Integer>> mapOndGroupId = new TreeMap<Integer, Map<Integer, Integer>>();

		for (Iterator<ReservationSegment> itReservationSegment = getDummySegments().iterator(); itReservationSegment.hasNext();) {
			reservationSegment = (ReservationSegment) itReservationSegment.next();

			// Group wise ond group id
			if (mapOndGroupId.containsKey(reservationSegment.getOndGroupId())) {
				mapSeq = (Map<Integer, Integer>) mapOndGroupId.get(reservationSegment.getOndGroupId());
				mapSeq.put(reservationSegment.getSegmentSeq(), reservationSegment.getFlightSegId());
			} else {
				mapSeq = new TreeMap<Integer, Integer>();
				mapSeq.put(reservationSegment.getSegmentSeq(), reservationSegment.getFlightSegId());
				mapOndGroupId.put(reservationSegment.getOndGroupId(), mapSeq);
			}
		}

		Collection<Integer> colOrderedNewFlgSegIds = new ArrayList<Integer>();

		for (Iterator<Integer> itIntegerOndGroupId = mapOndGroupId.keySet().iterator(); itIntegerOndGroupId.hasNext();) {
			ondGroupId = (Integer) itIntegerOndGroupId.next();
			mapSeq = (Map<Integer, Integer>) mapOndGroupId.get(ondGroupId);

			for (Iterator<Integer> itFlightSegId = mapSeq.values().iterator(); itFlightSegId.hasNext();) {
				flightSegId = (Integer) itFlightSegId.next();
				colOrderedNewFlgSegIds.add(flightSegId);
			}
		}

		return colOrderedNewFlgSegIds;
	}

	/**
	 * @return Returns the segmentObjectsMap.
	 */
	public Map<Integer, ReservationSegment> getSegmentObjectsMap() {
		return segmentObjectsMap;
	}

	/**
	 * @return Returns the passengerPaymentsMap.
	 */
	public Map<Integer, IPayment> getPassengerPaymentsMap() {
		return passengerPaymentsMap;
	}

	/**
	 * Over ride the zulu release time stamp
	 */
	public void overrideZuluReleaseTimeStamp(Date zuluReleaseTimeStamp) {
		this.zuluReleaseTimeStamp = zuluReleaseTimeStamp;
	}

	/**
	 * @return Returns the zuluReleaseTimeStamp.
	 */
	public Date getZuluReleaseTimeStamp() {
		return zuluReleaseTimeStamp;
	}

	/**
	 * Add Tempory Payment Entries
	 */
	public void addTemporyPaymentEntries(Map<Integer, CardPaymentInfo> mapTnxIds) {
		if (mapTnxIds != null && mapTnxIds.size() > 0) {
			setTnxIds(mapTnxIds);
			ReservationApiUtils.updateTemporyPaymentStates(getTnxIds());
		}
	}

	/**
	 * Returns Tempory payment transaction Ids
	 * 
	 * @return
	 */
	public Map<Integer, CardPaymentInfo> getTnxIds() {
		return mapTnxIds;
	}

	/**
	 * @param mapTnxIds
	 *            The mapTnxIds to set.
	 */
	private void setTnxIds(Map<Integer, CardPaymentInfo> mapTnxIds) {
		this.mapTnxIds = mapTnxIds;
	}

	/**
	 * Add Any External Segment information for the reservation This could be external inbound and outbound connection
	 * information
	 * 
	 * Note from Malaka : Change the external segment information storing dto from ReservationExternalSegment to
	 * ExternalPnrSegment. But at the time the refactoring is made this method has no usage.
	 * 
	 * @param segmentSeq
	 * @param flightNo
	 * @param segmentCode
	 * @param cabinClassCode
	 * @param departureDate
	 * @param arrivalDate
	 */
	public void addExternalSegment(int segmentSeq, String flightNo, String segmentCode, String cabinClassCode,
			Date departureDate, Date arrivalDate) {
		if (getExternalSegments() == null) {
			setExternalSegments(new HashSet<ExternalPnrSegment>());
		}

		ExternalPnrSegment externalPnrSegment = new ExternalPnrSegment();
		ExternalFlightSegment externalFlightSegment = new ExternalFlightSegment();
		externalFlightSegment.setFlightNumber(flightNo);
		externalFlightSegment.setSegmentCode(segmentCode);
		externalFlightSegment.setEstTimeDepatureLocal(departureDate);
		externalFlightSegment.setEstTimeArrivalLocal(arrivalDate);

		externalPnrSegment.setExternalFlightSegment(externalFlightSegment);
		externalPnrSegment.setSegmentSeq(segmentSeq);
		externalPnrSegment.setCabinClassCode(cabinClassCode);
		externalPnrSegment.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);

		getExternalSegments().add(externalPnrSegment);
	}

	@Override
	public void addGroundStationDataToPNRSegment(Map<Integer, String> groundStation,
			Map<Integer, Integer> groundSementFlightByAirFlightMap) throws ModuleException {

		CommonAssemblerUtil.addGroundStationDataToPNRSegment(groundStation, groundSementFlightByAirFlightMap,
				this.getSegmentObjectsMap());

	}

	/**
	 * @return Returns the colOndFareDTOs.
	 */
	private Collection<OndFareDTO> getColOndFareDTOs() {
		return colOndFareDTOs;
	}

	/**
	 * @param colOndFareDTOs
	 *            The colOndFareDTOs to set.
	 */
	private void setColOndFareDTOs(Collection<OndFareDTO> colOndFareDTOs) {
		this.colOndFareDTOs = colOndFareDTOs;
	}

	/**
	 * @param segments
	 *            The segments to set.
	 */
	private void setSegments(Collection<ReservationSegment> segments) {
		this.segments = segments;
	}

	/**
	 * @param passengerPaymentsMap
	 *            The passengerPaymentsMap to set.
	 */
	private void setPassengerPaymentsMap(Map<Integer, IPayment> passengerPaymentsMap) {
		this.passengerPaymentsMap = passengerPaymentsMap;
	}

	/**
	 * @param segmentObjectsMap
	 *            The segmentObjectsMap to set.
	 */
	private void setSegmentObjectsMap(Map<Integer, ReservationSegment> segmentObjectsMap) {
		this.segmentObjectsMap = segmentObjectsMap;
	}

	/**
	 * @return Returns the externalSegments.
	 */
	public Collection<ExternalPnrSegment> getExternalSegments() {
		return externalSegments;
	}

	/**
	 * @param externalSegments
	 *            The externalSegments to set.
	 */
	private void setExternalSegments(Collection<ExternalPnrSegment> externalSegments) {
		this.externalSegments = externalSegments;
	}

	@Override
	public void setLastCurrencyCode(String lastCurrencyCode) {
		this.lastCurrencyCode = lastCurrencyCode;
	}

	/**
	 * @return the lastCurrencyCode
	 */
	public String getLastCurrencyCode() {
		return lastCurrencyCode;
	}
	
	

	public List<IInsuranceRequest> getInsuranceRequests() {
		return insuranceRequests;
	}

	public void setInsuranceRequests(List<IInsuranceRequest> insuranceRequests) {
		this.insuranceRequests = insuranceRequests;
	}

	// Haider 19Apr09
	/** add insurance charge **/
	@Override
	public void addInsurance(IInsuranceRequest insurance) {
		insurance.prepareOriginAndDestinationInfo(this.colOndFareDTOs);
		getInsuranceRequests().add(insurance);
	}

	/**
	 * @return the lastModificationTimeStamp
	 */
	public Date getLastModificationTimeStamp() {
		return lastModificationTimeStamp;
	}

	/**
	 * @param lastModificationTimeStamp
	 *            the lastModificationTimeStamp to set
	 */
	public void setLastModificationTimeStamp(Date lastModificationTimeStamp) {
		this.lastModificationTimeStamp = lastModificationTimeStamp;
	}

	public void setFareDiscount(float fareDiscount, String notes) {
		this.fareDiscount = fareDiscount;
		if (notes != null && !"".equals(notes)) {
			setUserNote("FareDiscountNote: " + notes);
			setEndorsementNotes("FareDiscountNote: " + notes);
		}
	}

	public Float getFareDiscount() {
		return fareDiscount;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setEndorsementNotes(String endorsementNotes) {
		this.endorsementNotes = endorsementNotes;
	}

	public String getEndorsementNotes() {
		return endorsementNotes;
	}

	public Date getLastFareQuotedDate() {
		if (lastFareQuotedDate != null) {
			return lastFareQuotedDate;
		}
		return CalendarUtil.getCurrentSystemTimeInZulu();
	}

	@Override
	public void setExistingValidityInfo(Date firstDepDate, Long mostRestrMaxStayOver) {
		this.validityExistingFirstDept = firstDepDate;
		this.validityMostRestMaxStayOver = mostRestrMaxStayOver;
		this.validityCalculated = false;
	}

	private Date getTicketValidTill(boolean isOpenReturn) {
		if (!validityCalculated) {
			if (colOndFareDTOs != null && (AppSysParamsUtil.isTicketValidityEnabled() || isOpenReturn)) {
				Date arr[] = ReservationApiUtils.calculateValidities(colOndFareDTOs, null, this.validityMostRestMaxStayOver,
						this.validityExistingFirstDept, this.validityApplicableFltSegIds);
				this.ticketValidity = arr[0];
			}
			validityCalculated = true;
		}
		return ticketValidity;
	}

	@Override
	public void addOndSegment(int ondSequence, int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			Date openRtConfirmBefore, String externalRef, Integer baggageOndGroupId, boolean waitListing, boolean isReturnOnd,
			Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass, String csOcCarrierCode,
			String csOcFlightNumber) {
		if (getDummySegments() == null) {
			setSegments(new HashSet<ReservationSegment>());
		}

		ReservationSegment pnrSeg = new ReservationSegment();

		pnrSeg.setSegmentSeq(new Integer(segmentSeq));
		pnrSeg.setReturnFlag(isReturnOnd
				? ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE
				: ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);
		pnrSeg.setFlightSegId(new Integer(flightSegId));
		pnrSeg.setOndGroupId(ondGroupId);
		pnrSeg.setOpenReturnConfirmBeforeZulu(openRtConfirmBefore);
		pnrSeg.setReturnOndGroupId(returnOndGroupId);
		pnrSeg.setLastFareQuotedDateZulu(getLastFareQuotedDate());
		pnrSeg.setTicketValidTill(getTicketValidTill(openRtConfirmBefore != null));
		pnrSeg.setExternalReference(externalRef);
		pnrSeg.setJourneySequence(ondSequence);
		pnrSeg.setBaggageOndGroupId(baggageOndGroupId);
		pnrSeg.setBundledFarePeriodId(bundledFarePeriodId);
		if (codeShareFlightNumber != null && codeShareBookingClass != null) {
			pnrSeg.setCodeShareFlightNo(codeShareFlightNumber);
			pnrSeg.setCodeShareBc(codeShareBookingClass);
		}
		pnrSeg.setCsOcCarrierCode(csOcCarrierCode);
		pnrSeg.setCsOcFlightNumber(csOcFlightNumber);

		if (!waitListing) {
			// Making the segment status active purposly
			pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED);
		} else {
			pnrSeg.setStatus(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING);
		}

		getSegmentObjectsMap().put(new Integer(flightSegId), pnrSeg);

		getDummySegments().add(pnrSeg);

	}

	@Override
	public void setFareDiscountInfo(DiscountedFareDetails discountedFareDetails) {
		this.fareDiscountInfo = discountedFareDetails;

	}

	public DiscountedFareDetails getFareDiscountInfo() {
		return fareDiscountInfo;
	}

	/**
	 * @return the autoCancellationInfo
	 */
	public AutoCancellationInfo getAutoCancellationInfo() {
		return autoCancellationInfo;
	}

	@Override
	public void addAutoCancellationInfo(AutoCancellationDTO autoCancellationDTO) {
		autoCancellationInfo = new AutoCancellationInfo(autoCancellationDTO.getExpireOn(),
				autoCancellationDTO.getCancellationType());
	}

	@Override
	public void setAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo) {
		this.autoCancellationInfo = autoCancellationInfo;
	}

	@Override
	public void setApplicableAgentCommissions(AgentCommissionDetails agentCommissions) {
		this.agentCommissionInfo = agentCommissions;
	}

	public AgentCommissionDetails getAgentCommissionInfo() {
		return agentCommissionInfo;
	}

	public Collection<Integer> getValidityApplicableFltSegIds() {
		return validityApplicableFltSegIds;
	}
}
