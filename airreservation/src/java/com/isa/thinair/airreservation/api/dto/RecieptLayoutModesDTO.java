/**
 * 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;

/**
 * @author Indika Athauda
 * 
 */
public class RecieptLayoutModesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3181857681758418444L;

	/** Holds the template name */
	private String templateName;

	/** Holds the reservation number */
	private String pnr;

	/** Holds the locale specific information */
	private Locale locale;

	/** Holds whether to load passenger prices */
	private boolean includePassengerPrices;

	/** Holds whether to load detail charges */
	private boolean includeDetailCharges;

	/** Holds whether to load detail payments */
	private boolean includeDetailPayments;

	/** Holds the carrierCode */
	private String carrierCode;

	/** Holds whether to include COS */
	private boolean includeClassOfService;

	/** Hold whether to include carrier thumbnail logo in travel segments */
	private boolean includeThumbnailLogo;

	/** Holds Reciept Numebr */
	private String recieptNumber;

	private String payId;

	private String paymentMode;

	private Date paymentDate;

	private BigDecimal amount;

	private BigDecimal amountPayCurr;

	private String baseCurr;

	private String payCurr;

	/**
	 * @return the amountPayCurr
	 */
	public BigDecimal getAmountPayCurr() {
		return amountPayCurr;
	}

	/**
	 * @param amountPayCurr
	 *            the amountPayCurr to set
	 */
	public void setAmountPayCurr(BigDecimal amountPayCurr) {
		this.amountPayCurr = amountPayCurr;
	}

	/**
	 * @return the baseCurr
	 */
	public String getBaseCurr() {
		return baseCurr;
	}

	/**
	 * @param baseCurr
	 *            the baseCurr to set
	 */
	public void setBaseCurr(String baseCurr) {
		this.baseCurr = baseCurr;
	}

	/**
	 * @return the payCurr
	 */
	public String getPayCurr() {
		return payCurr;
	}

	/**
	 * @param payCurr
	 *            the payCurr to set
	 */
	public void setPayCurr(String payCurr) {
		this.payCurr = payCurr;
	}

	private String actualPaymentMethod;

	public String getPaymentAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(Math.abs(this.getAmount().doubleValue()), numberFormat);
	}

	public String getPaymentCurrAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(Math.abs(this.getAmountPayCurr().doubleValue()), numberFormat);
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getPaymentStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getPaymentDate(), dateFormat);
	}

	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * @param paymentMode
	 *            the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the payId
	 */
	public String getPayId() {
		return payId;
	}

	/**
	 * @param payId
	 *            the payId to set
	 */
	public void setPayId(String payId) {
		this.payId = payId;
	}

	/**
	 * @return the recieptNumber
	 */
	public String getRecieptNumber() {
		return recieptNumber;
	}

	/**
	 * @param recieptNumber
	 *            the recieptNumber to set
	 */
	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @return the includePassengerPrices
	 */
	public boolean isIncludePassengerPrices() {
		return includePassengerPrices;
	}

	/**
	 * @param includePassengerPrices
	 *            the includePassengerPrices to set
	 */
	public void setIncludePassengerPrices(boolean includePassengerPrices) {
		this.includePassengerPrices = includePassengerPrices;
	}

	/**
	 * @return the includeDetailCharges
	 */
	public boolean isIncludeDetailCharges() {
		return includeDetailCharges;
	}

	/**
	 * @param includeDetailCharges
	 *            the includeDetailCharges to set
	 */
	public void setIncludeDetailCharges(boolean includeDetailCharges) {
		this.includeDetailCharges = includeDetailCharges;
	}

	/**
	 * @return the includeDetailPayments
	 */
	public boolean isIncludeDetailPayments() {
		return includeDetailPayments;
	}

	/**
	 * @param includeDetailPayments
	 *            the includeDetailPayments to set
	 */
	public void setIncludeDetailPayments(boolean includeDetailPayments) {
		this.includeDetailPayments = includeDetailPayments;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the includeClassOfService
	 */
	public boolean isIncludeClassOfService() {
		return includeClassOfService;
	}

	/**
	 * @param includeClassOfService
	 *            the includeClassOfService to set
	 */
	public void setIncludeClassOfService(boolean includeClassOfService) {
		this.includeClassOfService = includeClassOfService;
	}

	/**
	 * @return the includeThumbnailLogo
	 */
	public boolean isIncludeThumbnailLogo() {
		return includeThumbnailLogo;
	}

	/**
	 * @param includeThumbnailLogo
	 *            the includeThumbnailLogo to set
	 */
	public void setIncludeThumbnailLogo(boolean includeThumbnailLogo) {
		this.includeThumbnailLogo = includeThumbnailLogo;
	}

	public String getLogoImageName() {
		String logoImageName = "LogoAni.gif";
		if (getCarrierCode() != null) {
			logoImageName = StaticFileNameUtil.getCorrected("LogoAni" + getCarrierCode() + ".gif");
		}
		return logoImageName;
	}

	/**
	 * @return the actualPaymentMethod
	 */
	public String getActualPaymentMethod() {
		return actualPaymentMethod;
	}

	/**
	 * @param actualPaymentMethod
	 *            the actualPaymentMethod to set
	 */
	public void setActualPaymentMethod(String actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

}
