package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To Track the external pnr segment
 * 
 * @author mekanayake
 * @hibernate.class table = "T_EXT_PNR_SEGMENT"
 */
public class ExternalPnrSegment extends Persistent implements Serializable, Comparable<ExternalPnrSegment> {

	private static final long serialVersionUID = -221246473412760424L;

	private Integer externalPnrSegId;

	private Reservation reservation;

	private String externalCarrierCode;

	private Integer externalPnrSegRef;

	private String cabinClassCode;

	private String logicalCabinClassCode;

	private Integer segmentSeq;

	private String returnFlag;

	private String status;

	private Integer externalFltSegId;

	private Date statusModifiedDate;

	private Integer statusModifiedSalesChannelCode;

	private String marketingCarrierCode;

	private String marketingStationCode;

	private String marketingAgentCode;
	
	private String subStatus;

	/**
	 * READONLY view of externalFlightSegment loaded with the reservation quarry Reason to add this as a readonly was
	 * bit unfortunate as xdoclet 1.2.3 is not supporting many to one lazy = false !!!
	 */
	private ExternalFlightSegment externalFlightSegment;

	/**
	 * @hibernate.id column = "EXT_PNR_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_EXT_PNR_SEGMENT"
	 * @return the externalPnrSegId
	 */
	public Integer getExternalPnrSegId() {
		return externalPnrSegId;
	}

	/**
	 * @param externalPnrSegId
	 *            the externalPnrSegId to set
	 */
	public void setExternalPnrSegId(Integer externalPnrSegId) {
		this.externalPnrSegId = externalPnrSegId;
	}

	/**
	 * @hibernate.many-to-one column="PNR" class="com.isa.thinair.airreservation.api.model.Reservation"
	 * @return the reservation
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation
	 *            the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @return the externalCarrierCode
	 * @hibernate.property column = "EXTERNAL_CARRIER_CODE"
	 */
	public String getExternalCarrierCode() {
		return externalCarrierCode;
	}

	/**
	 * @param externalCarrierCode
	 *            the externalCarrierCode to set
	 */
	public void setExternalCarrierCode(String externalCarrierCode) {
		this.externalCarrierCode = externalCarrierCode;
	}

	/**
	 * @return the externalPnrSegRef
	 * @hibernate.property column = "EXT_PNR_SEG_REF"
	 */
	public Integer getExternalPnrSegRef() {
		return externalPnrSegRef;
	}

	/**
	 * @param externalPnrSegRef
	 *            the externalPnrSegRef to set
	 */
	public void setExternalPnrSegRef(Integer externalPnrSegRef) {
		this.externalPnrSegRef = externalPnrSegRef;
	}

	/**
	 * @return the cabinClassCode
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 * 
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return the segmentSeq
	 * @hibernate.property column = "SEGMENT_SEQ"
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            the segmentSeq to set
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	/**
	 * @return the returnFlag
	 * @hibernate.property column = "RETURN_FLAG"
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            the returnFlag to set
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the externalFltSegId
	 * @hibernate.property column = "EXT_FLT_SEG_ID"
	 */
	public Integer getExternalFltSegId() {
		return externalFltSegId;
	}

	/**
	 * @param externalFltSegId
	 *            the externalFltSegId to set
	 */
	public void setExternalFltSegId(Integer externalFltSegId) {
		this.externalFltSegId = externalFltSegId;
	}

	/**
	 * Returns readonly view of ExternalFlightSegment
	 * 
	 * @return the externalFlightSegment
	 */
	public ExternalFlightSegment getExternalFlightSegment() {
		return externalFlightSegment;
	}

	/**
	 * @param externalFlightSegment
	 *            the externalFlightSegment to set
	 */
	public void setExternalFlightSegment(ExternalFlightSegment externalFlightSegment) {
		this.externalFlightSegment = externalFlightSegment;
	}

	/**
	 * @return The statusModifiedDate
	 * @hibernate.property column = "STATUS_MOD_DATE"
	 */
	public Date getStatusModifiedDate() {
		return statusModifiedDate;
	}

	/**
	 * @param statusModifiedDate
	 *            The statusModifiedDate to set.
	 */
	public void setStatusModifiedDate(Date statusModifiedDate) {
		this.statusModifiedDate = statusModifiedDate;
	}

	/**
	 * @return The statusModifiedSalesChannelCode.
	 * @hibernate.property column = "STATUS_MOD_CHANNEL_CODE"
	 */
	public Integer getStatusModifiedSalesChannelCode() {
		return statusModifiedSalesChannelCode;
	}

	/**
	 * @param statusModifiedSalesChannelCode
	 *            : The statusModifiedSalesChannelCode to set.
	 */
	public void setStatusModifiedSalesChannelCode(Integer statusModifiedSalesChannelCode) {
		this.statusModifiedSalesChannelCode = statusModifiedSalesChannelCode;
	}

	/**
	 * @return The marketingCarrierCode.
	 * @hibernate.property column = "MKTING_CARRIER_CODE"
	 */
	public String getMarketingCarrierCode() {
		return marketingCarrierCode;
	}

	/**
	 * @param marketingCarrierCode
	 *            : The marketingCarrierCode to set.
	 */
	public void setMarketingCarrierCode(String marketingCarrierCode) {
		this.marketingCarrierCode = marketingCarrierCode;
	}

	/**
	 * @return The marketingStationCode.
	 * @hibernate.property column = "MKTING_STATION_CODE"
	 */
	public String getMarketingStationCode() {
		return marketingStationCode;
	}

	/**
	 * @param marketingStationCode
	 *            : The marketingStationCode to set.
	 */
	public void setMarketingStationCode(String marketingStationCode) {
		this.marketingStationCode = marketingStationCode;
	}

	/**
	 * @return The marketingAgentCode.
	 * @hibernate.property column = "MKTING_AGENT_CODE"
	 */
	public String getMarketingAgentCode() {
		return marketingAgentCode;
	}

	/**
	 * @param marketingAgentCode
	 *            : The marketingAgentCode to set.
	 */
	public void setMarketingAgentCode(String marketingAgentCode) {
		this.marketingAgentCode = marketingAgentCode;
	}

	/**
	 * Returns the Segment Business wise unique key
	 * 
	 * @return
	 */
	public String getUniqueSegmentKey() {
		ExternalFlightSegment externalFlightSegment = this.getExternalFlightSegment();
		return ReservationApiUtils.getUniqueSegmentKey(externalFlightSegment.getFlightNumber(),
				externalFlightSegment.getSegmentCode(), externalFlightSegment.getEstTimeDepatureLocal(), getCabinClassCode(),
				null);
	}

	/**
	 * return true if both external carrier code and the external pnr segment ref equals
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ExternalPnrSegment) {
			ExternalPnrSegment castObject = (ExternalPnrSegment) o;

			if (castObject.getExternalCarrierCode() == null || this.getExternalCarrierCode() == null) {
				return false;
			} else if (castObject.getExternalPnrSegRef() == null || this.getExternalPnrSegRef() == null) {
				return false;
			} else if (castObject.getExternalCarrierCode() == this.getExternalCarrierCode()
					&& castObject.getExternalPnrSegRef().intValue() == this.getExternalPnrSegRef().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getExternalCarrierCode()).append(getExternalPnrSegRef()).toHashCode();
	}

	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		ExternalFlightSegment externalFlightSegment = this.getExternalFlightSegment();

		if (externalFlightSegment != null) {
			stringBuilder.append(" Flight Number: " + externalFlightSegment.getFlightNumber());
			stringBuilder.append(" Segment Code: " + externalFlightSegment.getSegmentCode());
			stringBuilder.append(" Departure Date: "
					+ BeanUtils.parseDateFormat(externalFlightSegment.getEstTimeDepatureLocal(), "E, dd MMM yyyy HH:mm:ss"));
			stringBuilder.append(" Arrival Date: "
					+ BeanUtils.parseDateFormat(externalFlightSegment.getEstTimeArrivalLocal(), "E, dd MMM yyyy HH:mm:ss"));

		}
		stringBuilder.append(" Cabin Class: " + this.getCabinClassCode());
		stringBuilder.append(" Status : " + this.getStatus());
		stringBuilder.append(" Sub Status : " + this.getSubStatus());

		return stringBuilder.toString();
	}

	public int compareTo(ExternalPnrSegment o) {
		return this.getSegmentSeq().compareTo(o.getSegmentSeq());
	}

	/**
	 * @return The subStatus.
	 * @hibernate.property column = "SUB_STATUS"
	 */
	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
}
