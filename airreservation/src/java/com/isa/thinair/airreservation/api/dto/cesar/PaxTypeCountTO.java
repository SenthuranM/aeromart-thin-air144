package com.isa.thinair.airreservation.api.dto.cesar;

import java.io.Serializable;

public class PaxTypeCountTO implements Serializable {

	private static final long serialVersionUID = 5833695737924456314L;

	private int adultMaleCount;
	private int adultFemaleCount;
	private int childCount;
	private int infantCount;
	private int othersCount;
	private int baggageWeight;

	public int getAdultMaleCount() {
		return adultMaleCount;
	}

	public void setAdultMaleCount(int adultMaleCount) {
		this.adultMaleCount = adultMaleCount;
	}

	public int getAdultFemaleCount() {
		return adultFemaleCount;
	}

	public void setAdultFemaleCount(int adultFemaleCount) {
		this.adultFemaleCount = adultFemaleCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public int getOthersCount() {
		return othersCount;
	}

	public void setOthersCount(int othersCount) {
		this.othersCount = othersCount;
	}

	public int getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(int baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

}
