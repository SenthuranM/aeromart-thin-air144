package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Jagath
 * @since Feb 19, 2013
 * @hibernate.class table = "T_NS_REFUND_PNR" lazy="false"
 */
public class NSRefundReservation extends Persistent implements Serializable {

	private static final long serialVersionUID = -2737205969327935805L;

	private int nsRefundId;

	private String pnr;

	private Date nsProcessedTime;

	private String refundProcessedStatus;

	private Date refundProcessedTime;

	/**
	 * @return the nsRefundId
	 * @hibernate.id column = "NS_REFUND_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_NS_REFUND_PNR"
	 */
	public int getNsRefundId() {
		return nsRefundId;
	}

	/**
	 * @param nsRefundId
	 *            the nsRefundId to set
	 */
	public void setNsRefundId(int nsRefundId) {
		this.nsRefundId = nsRefundId;
	}

	/**
	 * @return the pnr
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the nsProcessedTime
	 * @hibernate.property column = "NS_PROCESSED_TIME"
	 */
	public Date getNsProcessedTime() {
		return nsProcessedTime;
	}

	/**
	 * @param nsProcessedTime
	 *            the nsProcessedTime to set
	 */
	public void setNsProcessedTime(Date nsProcessedTime) {
		this.nsProcessedTime = nsProcessedTime;
	}

	/**
	 * @return the refundProcessedStatus
	 * @hibernate.property column = "REFUND_PROCESSED_STATUS"
	 */
	public String getRefundProcessedStatus() {
		return refundProcessedStatus;
	}

	/**
	 * @param refundProcessedStatus
	 *            the refundProcessedStatus to set
	 */
	public void setRefundProcessedStatus(String refundProcessedStatus) {
		this.refundProcessedStatus = refundProcessedStatus;
	}

	/**
	 * @return the refundProcessedTime
	 * @hibernate.property column = "REFUND_PROCESSED_TIME"
	 */
	public Date getRefundProcessedTime() {
		return refundProcessedTime;
	}

	/**
	 * @param refundProcessedTime
	 *            the refundProcessedTime to set
	 */
	public void setRefundProcessedTime(Date refundProcessedTime) {
		this.refundProcessedTime = refundProcessedTime;
	}

}