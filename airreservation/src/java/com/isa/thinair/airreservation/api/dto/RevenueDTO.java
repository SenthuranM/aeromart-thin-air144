/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold revenue data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class RevenueDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5657455223181534221L;

	/** Holds Passenger Id */
	private Integer pnrPaxId;

	/** Holds the credit total */
	private BigDecimal creditTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the modification or cancellation total */
	private BigDecimal cancelOrModifyTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the added total */
	private BigDecimal addedTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the total charge after cancellation */
	private BigDecimal totalChgBeforeCancellation = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the action nomical code */
	private int actionNC;

	/** Holds the charge nominal code */
	private int chargeNC;

	private Map<Long, ReservationPaxOndCharge> mapRefundableCharges = new HashMap<Long, ReservationPaxOndCharge>();

	private Collection<ReservationPaxOndCharge> addedCharges = new ArrayList<ReservationPaxOndCharge>();

	private BigDecimal modificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();

	/**
	 * @return Returns the creditTotal.
	 */
	public BigDecimal getCreditTotal() {
		return creditTotal;
	}

	/**
	 * @param creditTotal
	 *            The creditTotal to set.
	 */
	public void setCreditTotal(BigDecimal creditTotal) {
		this.creditTotal = creditTotal;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the cancelOrModifyTotal.
	 */
	public BigDecimal getCancelOrModifyTotal() {
		return cancelOrModifyTotal;
	}

	/**
	 * @param cancelOrModifyTotal
	 *            The cancelOrModifyTotal to set.
	 */
	public void setCancelOrModifyTotal(BigDecimal cancelOrModifyTotal) {
		this.cancelOrModifyTotal = cancelOrModifyTotal;
	}

	/**
	 * @return Returns the addedTotal.
	 */
	public BigDecimal getAddedTotal() {
		return addedTotal;
	}

	/**
	 * @param addedTotal
	 *            The addedTotal to set.
	 */
	public void setAddedTotal(BigDecimal addedTotal) {
		this.addedTotal = addedTotal;
	}

	/**
	 * @return Returns the actionNC.
	 */
	public int getActionNC() {
		return actionNC;
	}

	/**
	 * @param actionNC
	 *            The actionNC to set.
	 */
	public void setActionNC(int actionNC) {
		this.actionNC = actionNC;
	}

	/**
	 * @return Returns the chargeNC.
	 */
	public int getChargeNC() {
		return chargeNC;
	}

	/**
	 * @param chargeNC
	 *            The chargeNC to set.
	 */
	public void setChargeNC(int chargeNC) {
		this.chargeNC = chargeNC;
	}

	/**
	 * @return Returns the totalChgBeforeCancellation.
	 */
	public BigDecimal getTotalChgBeforeCancellation() {
		return totalChgBeforeCancellation;
	}

	/**
	 * @param totalChgBeforeCancellation
	 *            The totalChgBeforeCancellation to set.
	 */
	public void setTotalChgBeforeCancellation(BigDecimal totalChgBeforeCancellation) {
		this.totalChgBeforeCancellation = totalChgBeforeCancellation;
	}

	/**
	 * @return the mapRefundableCharges
	 */
	public Map<Long, ReservationPaxOndCharge> getMapRefundableCharges() {
		return mapRefundableCharges;
	}

	/**
	 * @return the addedCharges
	 */
	public Collection<ReservationPaxOndCharge> getAddedCharges() {
		return addedCharges;
	}

	public void setModificationPenalty(BigDecimal modificationPenalty) {
		this.modificationPenalty = modificationPenalty;
	}

	public BigDecimal getModificationPenalty() {
		return modificationPenalty;
	}

}
