/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold agent credit data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class AgentCreditInfo implements PaymentInfo, CustomerPayment, Serializable {

	private static final long serialVersionUID = 2270286901946108917L;

	/** Holds total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the agent code */
	private String agentCode;

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	private PaymentReferenceTO paymentReferenceTO;

	/** Holds the payment carrier code */
	private String payCarrier;

	/** Holds the lcc unique Id for interline or dry booking */
	private String lccUniqueId;
	
	/**
	 * Holds original paymentTnxId when the operation is a refund
	 */
	private Integer paymentTnxId;
	

	public AgentCreditInfo(String agentCode, BigDecimal totalAmount, PayCurrencyDTO payCurrencyDTO,
			PaymentReferenceTO paymentReferenceTO, String payCarrier, String lccUniqueId, Integer paymentTnxId) {
		this.agentCode = agentCode;
		this.totalAmount = totalAmount;
		this.payCurrencyDTO = payCurrencyDTO;
		this.paymentReferenceTO = paymentReferenceTO;
		this.payCarrier = payCarrier;
		this.lccUniqueId = lccUniqueId;
		this.paymentTnxId = paymentTnxId;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the totalAmount.
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            The totalAmount to set.
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the paymentReferenceTO
	 */
	public PaymentReferenceTO getPaymentReferenceTO() {
		return paymentReferenceTO;
	}

	/**
	 * @param paymentReferenceTO
	 *            the paymentReferenceTO to set
	 */
	public void setPaymentReferenceTO(PaymentReferenceTO paymentReferenceTO) {
		this.paymentReferenceTO = paymentReferenceTO;
	}

	/**
	 * @return the payCarrier
	 */
	public String getPayCarrier() {
		return payCarrier;
	}

	/**
	 * @param payCarrier
	 *            the payCarrier to set
	 */
	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;
	}

	/**
	 * @return the lccUniqueId
	 */
	public String getLccUniqueId() {
		return lccUniqueId;
	}

	/**
	 * @param lccUniqueId
	 *            the lccUniqueId to set
	 */
	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	/**
	 * @param paymentTnxId
	 */
	public Integer getPaymentTnxId() {
		return this.paymentTnxId;
	}

	
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;

	}
	
}
