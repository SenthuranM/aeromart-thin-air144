package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ItineraryPaymentTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date paymentDate;

	private String paymentDateInPersian;

	private String paymentType;

	private String authorizationId;

	private String baseAmount;

	private String baseCurrency;

	private String paidAmount;

	private String paidCurrency;

	private String last4DigitsOfCCNo;

	private boolean usePersianDates;
	
	private String paxDisplayName = null;

	public boolean isPersianDatesSet() {
		return usePersianDates;
	}

	public void setIsPersianDatesSet(boolean usePersianDates) {
		this.usePersianDates = usePersianDates;
	}

	public String getPaymentDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(paymentDate, dateTimeFormat);
	}

	/**
	 * @param paymentDate
	 *            the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the authorizationId
	 */
	public String getAuthorizationId() {
		return authorizationId;
	}

	/**
	 * @param authorizationId
	 *            the authorizationId to set
	 */
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

	/**
	 * @return the baseAmount
	 */
	public String getBaseAmount() {
		return baseAmount;
	}

	/**
	 * @param baseAmount
	 *            the baseAmount to set
	 */
	public void setBaseAmount(String baseAmount) {
		this.baseAmount = baseAmount;
	}

	/**
	 * @return the baseCurrency
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * @param baseCurrency
	 *            the baseCurrency to set
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	/**
	 * @return the paidAmount
	 */
	public String getPaidAmount() {
		return paidAmount;
	}

	/**
	 * @param paidAmount
	 *            the paidAmount to set
	 */
	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	/**
	 * @return the paidCurrency
	 */
	public String getPaidCurrency() {
		return paidCurrency;
	}

	/**
	 * @param paidCurrency
	 *            the paidCurrency to set
	 */
	public void setPaidCurrency(String paidCurrency) {
		this.paidCurrency = paidCurrency;
	}

	/**
	 * @return the last4DigitsOfCCNo
	 */
	public String getLast4DigitsOfCCNo() {
		return last4DigitsOfCCNo;
	}

	/**
	 * @param last4DigitsOfCCNo
	 *            the last4DigitsOfCCNo to set
	 */
	public void setLast4DigitsOfCCNo(String last4DigitsOfCCNo) {
		this.last4DigitsOfCCNo = last4DigitsOfCCNo;
	}

	public String getPaymentDateInPersian() {
		return paymentDateInPersian;
	}

	public void setPaymentDateInPersian(String paymentDateInPersian) {
		this.paymentDateInPersian = paymentDateInPersian;
	}

	public String getPaxDisplayName() {
		return paxDisplayName;
	}

	public void setPaxDisplayName(String paxDisplayName) {
		this.paxDisplayName = paxDisplayName;
	}
	
}