package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.isa.thinair.airinventory.api.dto.TransferSeatBcCount;
import com.isa.thinair.airinventory.api.dto.TransferSeatList;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;

public class RollforwardResultsSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5912977946144715490L;
	/** Holds the target flight segment detail */
	private FlightSegmentDTO targetFlightSegmentDetails;
	/** Holds the collection of source flight segment details */
	private Collection<FlightSegmentDTO> sourceFlightSegmentDetails;
	/** Holds a collection of TransferSeatList */
	private Collection<TransferSeatList> sourceFltSegSeatDists;
	/** Holds the collection of Reservation Segment Details */
	private Collection<ReservationSegmentDTO> reservationSegmentDetails;

	/**
	 * @return Returns the sourceFlightSegmentDetails.
	 */
	public Collection<FlightSegmentDTO> getSourceFlightSegmentDetails() {
		return sourceFlightSegmentDetails;
	}

	/**
	 * @param sourceFlightSegmentDetails
	 *            The sourceFlightSegmentDetails to set.
	 */
	public void setSourceFlightSegmentDetails(Collection<FlightSegmentDTO> sourceFlightSegmentDetails) {
		this.sourceFlightSegmentDetails = sourceFlightSegmentDetails;
	}

	/**
	 * @return Returns the sourceFltSegSeatDists.
	 */
	public Collection<TransferSeatList> getSourceFltSegSeatDists() {
		return sourceFltSegSeatDists;
	}

	/**
	 * @param sourceFltSegSeatDists
	 *            The sourceFltSegSeatDists to set.
	 */
	public void setSourceFltSegSeatDists(Collection<TransferSeatList> sourceFltSegSeatDists) {
		this.sourceFltSegSeatDists = sourceFltSegSeatDists;
	}

	/**
	 * @return Returns the targetFlightSegmentDetails.
	 */
	public FlightSegmentDTO getTargetFlightSegmentDetails() {
		return targetFlightSegmentDetails;
	}

	/**
	 * @param targetFlightSegmentDetails
	 *            The targetFlightSegmentDetails to set.
	 */
	public void setTargetFlightSegmentDetails(FlightSegmentDTO targetFlightSegmentDetails) {
		this.targetFlightSegmentDetails = targetFlightSegmentDetails;
	}

	/**
	 * @return Returns the reservationSegmentDetails.
	 */
	public Collection<ReservationSegmentDTO> getReservationSegmentDetails() {
		return reservationSegmentDetails;
	}

	/**
	 * @param reservationSegmentDetails
	 *            The reservationSegmentDetails to set.
	 */
	public void setReservationSegmentDetails(Collection<ReservationSegmentDTO> reservationSegmentDetails) {
		this.reservationSegmentDetails = reservationSegmentDetails;
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public String getSegmentDistributionSummary() {
		StringBuffer content = new StringBuffer();
		HashMap bcMap = new HashMap();
		Iterator itBcMap;
		if (sourceFltSegSeatDists != null) {
			Iterator it = sourceFltSegSeatDists.iterator();
			while (it.hasNext()) {
				TransferSeatList seatList = (TransferSeatList) it.next();
				content.append("\n\r");
				content.append("\nSource Flight Id       = [" + seatList.getSourceFlightId() + "]\nSource Segment Id      = ["
						+ seatList.getSourceSegmentId() + "]\nSource Segment Code    = [" + seatList.getSourceSegmentCode()
						+ "]\nTransfer All Selected  = [" + seatList.isTransferAll() + "]\nTransfer Seat Count    = ["
						+ seatList.getTransferSeatCount() + "]\nConfirmed Infant Count = [" + seatList.getConfirmedInfantCount()
						+ "]\nOnhold Infant Count    = [" + seatList.getOnHoldInfantCount() + "]");
				content.append("\nBooking Code [Booking Code, Seat Sold, Seat Onhold] = ");
				bcMap = seatList.getBcCount();
				if (bcMap != null) {
					itBcMap = bcMap.keySet().iterator();
					while (itBcMap.hasNext()) {
						String bc = (String) itBcMap.next();
						TransferSeatBcCount trBcCount = (TransferSeatBcCount) bcMap.get(bc);
						content.append("\t[" + bc).append("," + trBcCount.getSoldCount())
								.append("," + trBcCount.getOnHoldCount() + "]");
					}
				}
				content.append("\n\r");
			}
		}
		return content.toString();
	}

	/**
	 * 
	 * @return
	 */
	public String getReservationSummary() {
		StringBuffer content = new StringBuffer();
		if (reservationSegmentDetails != null) {
			Iterator<ReservationSegmentDTO> it = reservationSegmentDetails.iterator();
			while (it.hasNext()) {
				ReservationSegmentDTO resSeg = (ReservationSegmentDTO) it.next();
				content.append("\n\r");
				content.append("\nFlight Segment Id = [" + resSeg.getFlightSegId() + "]\nPNR               = [" + resSeg.getPnr()
						+ "]\nPNR Segment Id    = [" + resSeg.getPnrSegId() + "]\nPNR Segment Status= [" + resSeg.getStatus()
						+ "]");
				content.append("\n\r");
			}
		}
		return content.toString();
	}

}
