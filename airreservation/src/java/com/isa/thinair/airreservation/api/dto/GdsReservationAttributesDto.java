package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class GdsReservationAttributesDto implements Serializable {
	private String pnr;
	private String externalRecLocator;
	private String externalPos;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getExternalRecLocator() {
		return externalRecLocator;
	}

	public void setExternalRecLocator(String externalRecLocator) {
		this.externalRecLocator = externalRecLocator;
	}

	public String getExternalPos() {
		return externalPos;
	}

	public void setExternalPos(String externalPos) {
		this.externalPos = externalPos;
	}
}
