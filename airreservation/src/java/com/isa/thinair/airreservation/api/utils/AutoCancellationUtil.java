package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.AutoCancellationDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.INSURANCE_STATES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class AutoCancellationUtil {

	public static AutoCancellationInfo getAutoCancellationInfo(String pnr, Reservation reservation, Date firstDepartureTime,
			Collection<Integer> modifiedSegIds, String cancellationType, boolean hasBufferTimePrivilege,
			boolean getExistingCnxTimeOnly) throws ModuleException {
		AutoCancellationInfo autoCancellationInfo = null;
		if (getExistingCnxTimeOnly) {
			autoCancellationInfo = new AutoCancellationInfo();
		} else {
			AutoCancellationDTO autoCancellationDTO = new AutoCancellationDTO(firstDepartureTime, cancellationType,
					hasBufferTimePrivilege);
			AutoCancellationUtil.calculateAutomaticCancellationTriggerTimeStamp(autoCancellationDTO);

			autoCancellationInfo = new AutoCancellationInfo(autoCancellationDTO.getExpireOn(),
					autoCancellationDTO.getCancellationType());
		}

		if (pnr != null) {
			if (reservation == null) {
				reservation = loadReservation(pnr);
			}
			autoCancellationInfo = overrideAutoCancellationTime(reservation, autoCancellationInfo, modifiedSegIds);
		}

		return autoCancellationInfo;
	}

	/**
	 * Calculate auto cancellation trigger time considering flight departure time, modification buffer time, modify
	 * within buffer privilege.
	 * 
	 * @param autoCancellationDTO
	 * @throws ModuleException
	 */
	private static void calculateAutomaticCancellationTriggerTimeStamp(AutoCancellationDTO autoCancellationDTO)
			throws ModuleException {

		long departTimeInMillis = autoCancellationDTO.getFlightDepartureDateTimeInZulu().getTime();
		long currentSysTimeInMillis = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		long timeToDepInMillis = ReleaseTimeUtil.getTimeToDepartureInMillis(autoCancellationDTO
				.getFlightDepartureDateTimeInZulu());

		long modificationStopBuffer = 0;
		if (autoCancellationDTO.isDomesticFlight()) {
			modificationStopBuffer = AppSysParamsUtil.getDomesticBufferDurationInMillis();
		} else {
			modificationStopBuffer = AppSysParamsUtil.getInternationalBufferDurationInMillis();
		}

		String autoCnxTriggerTimeHHMM = AppSysParamsUtil.getAutoCancellationTriggerTime();
		String autoCnxTriggerTimeDurationInBufferHHMM = AppSysParamsUtil.getAutoCancellationTriggerTimeInBuffer();

		int inBufferCnxInMunites = AppSysParamsUtil.getTotalMinutes(autoCnxTriggerTimeDurationInBufferHHMM);
		long inBufferCnxInMillis = AppSysParamsUtil.getTimeInMillis(autoCnxTriggerTimeDurationInBufferHHMM);
		long inBufferTotal = inBufferCnxInMillis + currentSysTimeInMillis;

		Date autoCnxTriggerTimeStamp = CalendarUtil.getCurrentSystemTimeInZulu();
		if (timeToDepInMillis > modificationStopBuffer) {
			Date triggerTimeStamp = CalendarUtil.getCurrentSystemTimeInZulu();
			if (AppSysParamsUtil.isAutoCancellationTriggerTimeFromFlightDepartureTime()) {
				triggerTimeStamp = BeanUtils.extendByMinutes(autoCancellationDTO.getFlightDepartureDateTimeInZulu(),
						(AppSysParamsUtil.getTotalMinutes(autoCnxTriggerTimeHHMM) * -1));
			} else {
				// else will use modification time to calculate trigger time stamp. So trigger time stamp will be
				// current time + auto cancellation trigger time
				triggerTimeStamp = BeanUtils.extendByMinutes(CalendarUtil.getCurrentSystemTimeInZulu(),
						(AppSysParamsUtil.getTotalMinutes(autoCnxTriggerTimeHHMM)));
			}
			long triggerTimeStampInMillis = triggerTimeStamp.getTime();
			if ((departTimeInMillis - triggerTimeStampInMillis) <= 0
					|| (departTimeInMillis - triggerTimeStampInMillis) > timeToDepInMillis) {
				if (inBufferTotal < departTimeInMillis) {
					autoCnxTriggerTimeStamp = BeanUtils.extendByMinutes(CalendarUtil.getCurrentSystemTimeInZulu(),
							inBufferCnxInMunites);
				} else {
					autoCnxTriggerTimeStamp = CalendarUtil.getCurrentSystemTimeInZulu();
				}
			} else {
				autoCnxTriggerTimeStamp = triggerTimeStamp;
			}
		} else if ((timeToDepInMillis <= modificationStopBuffer) && autoCancellationDTO.isHasBufferTimeCancellation()) {
			if (inBufferTotal < departTimeInMillis) {
				autoCnxTriggerTimeStamp = BeanUtils.extendByMinutes(CalendarUtil.getCurrentSystemTimeInZulu(),
						inBufferCnxInMunites);
			}
		}

		autoCancellationDTO.setExpireOn(autoCnxTriggerTimeStamp);
	}

	/**
	 * This method override auto cancellation trigger time, if expire time is already exists & less than new expire time
	 * 
	 * @param reservation
	 * @param autoCancellationInfo
	 * @param modifiedSegIds
	 */
	private static AutoCancellationInfo overrideAutoCancellationTime(Reservation reservation,
			AutoCancellationInfo autoCancellationInfo, Collection<Integer> modifiedSegIds) {
		if (autoCancellationInfo != null) {
			AutoCancellationInfo existingAutoCnxInfo = getExistingAutoCnxObj(reservation, modifiedSegIds);

			if (existingAutoCnxInfo != null) {
				if (autoCancellationInfo.getExpireOn() != null) {
					if (existingAutoCnxInfo.getExpireOn().before(autoCancellationInfo.getExpireOn())) {
						return existingAutoCnxInfo;
					} else {
						existingAutoCnxInfo.setExpireOn(autoCancellationInfo.getExpireOn());
						return existingAutoCnxInfo;
					}
				} else {
					return existingAutoCnxInfo;
				}
			}

			return autoCancellationInfo;
		}

		return null;
	}

	private static AutoCancellationInfo getExistingAutoCnxObj(Reservation reservation, Collection<Integer> modifiedSegIds) {

		AutoCancellationInfo autoCancellationInfo = reservation.getAutoCancellationInfo();

		if (autoCancellationInfo != null) {
			// Check auto cnx available in existing segments
			Collection<ReservationSegment> existingPnrSegments = reservation.getSegments();
			for (ReservationSegment existingReservationSegment : existingPnrSegments) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
						.equals(existingReservationSegment.getStatus())
						&& existingReservationSegment.getAutoCancellationId() != null) {

					if (modifiedSegIds != null && modifiedSegIds.contains(existingReservationSegment.getPnrSegId())) {
						continue;
					} else {
						return autoCancellationInfo;
					}
				}
			}

			Collection<PaxSSRDTO> paxSSRDTOs = new ArrayList<PaxSSRDTO>();

			// Check auto cnx available in infants
			Collection<ReservationPax> allPaxs = reservation.getPassengers();
			for (ReservationPax reservationPax : allPaxs) {
				if (reservationPax.getPaxSSR() != null && !reservationPax.getPaxSSR().isEmpty()) {
					paxSSRDTOs.addAll(reservationPax.getPaxSSR());
				}

				if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())
						&& ReservationInternalConstants.ReservationPaxStatus.CONFIRMED.equals(reservationPax.getStatus())
						&& reservationPax.getAutoCancellationId() != null) {
					return autoCancellationInfo;
				}
			}

			// Check auto cnx available in existing seats
			if (reservation.getSeats() != null && !reservation.getSeats().isEmpty()) {
				for (PaxSeatTO paxSeatTO : reservation.getSeats()) {
					if (modifiedSegIds != null && modifiedSegIds.contains(paxSeatTO.getPnrSegId())) {
						continue;
					} else if (!ReservationInternalConstants.SegmentAncillaryStatus.CANCEL.equals(paxSeatTO.getStatus())
							&& paxSeatTO.getAutoCancellationId() != null) {
						return autoCancellationInfo;
					}
				}
			}

			// Check auto cnx available in existing meals
			if (reservation.getMeals() != null && !reservation.getMeals().isEmpty()) {
				for (PaxMealTO paxMealTO : reservation.getMeals()) {
					if (modifiedSegIds != null && modifiedSegIds.contains(paxMealTO.getPnrSegId())) {
						continue;
					} else if (!ReservationInternalConstants.SegmentAncillaryStatus.CANCEL.equals(paxMealTO.getStatus())
							&& paxMealTO.getAutoCancellationId() != null) {
						return autoCancellationInfo;
					}
				}
			}

			// Check auto cnx available in existing baggages
			if (reservation.getBaggages() != null && !reservation.getBaggages().isEmpty()) {
				for (PaxBaggageTO paxBaggageTO : reservation.getBaggages()) {
					if (modifiedSegIds != null && modifiedSegIds.contains(paxBaggageTO.getPnrSegId())) {
						continue;
					} else if (!ReservationInternalConstants.SegmentAncillaryStatus.CANCEL.equals(paxBaggageTO.getStatus())
							&& paxBaggageTO.getAutoCancellationId() != null) {
						return autoCancellationInfo;
					}
				}
			}

			// Check auto cnx available in existing SSRs
			if (paxSSRDTOs != null && !paxSSRDTOs.isEmpty()) {
				for (PaxSSRDTO paxSSRDTO : paxSSRDTOs) {
					if (modifiedSegIds != null && modifiedSegIds.contains(paxSSRDTO.getPnrSegId())) {
						continue;
					} else if (!ReservationInternalConstants.SegmentAncillaryStatus.CANCEL.equals(paxSSRDTO.getStatus())
							&& paxSSRDTO.getAutoCancellationId() != null) {
						return autoCancellationInfo;
					}
				}
			}

			// Check auto cnx available in existing insurance
			if (reservation.getReservationInsurance() != null && !reservation.getReservationInsurance().isEmpty()){
				for (ReservationInsurance reservationInsurance : reservation.getReservationInsurance()) {
					if(INSURANCE_STATES.OH.equals(reservationInsurance.getState())&& reservationInsurance.getAutoCancellationId() != null){
						return autoCancellationInfo;
					}
				}
				
			}
					
					
		}

		return null;
	}

	public static Reservation loadReservation(String pnr) throws ModuleException {
		Reservation reservation = null;
		LCCClientPnrModesDTO pnrModesDTO = getDefaultLCCClientPnrModesDTO(pnr);
		reservation = ReservationModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		return reservation;
	}

	private static LCCClientPnrModesDTO getDefaultLCCClientPnrModesDTO(String pnr) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(false);
		pnrModesDTO.setLoadLocalTimes(false);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(false);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		return pnrModesDTO;
	}
}
