package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Gayan
 * 
 * @hibernate.class table="t_user_station"
 */
public class UserStation extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private String userId;

	private String stattionCode;

	/**
	 * @hibernate.id column = "USER_STATION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="s_user_station"
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @hibernate.property column= "USER_ID"
	 * @return requestDate
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column= "STATION_CODE"
	 * @return requestDate
	 */
	public String getStattionCode() {
		return stattionCode;
	}

	public void setStattionCode(String stattionCode) {
		this.stattionCode = stattionCode;
	}

}
