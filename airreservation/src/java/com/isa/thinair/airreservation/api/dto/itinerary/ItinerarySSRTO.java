package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItinerarySSRTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String description;
	private String ssrCode;
	private String text;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
