package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ResContactInfoSearchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8535238681167553910L;

	private static final String PHONENUMBER_SEPERATOR = "-";

	private static final String WILDCARD_CHAR = "*";

	/** Holds PNR Number */
	private String pnr;

	/** Holds first Name */
	private String firstName;

	/** Holds last Name */
	private String lastName;

	/** Holds country code of phone number */
	private String countryCode;

	/** Holds country code of phone number */
	private String areaCode;

	/** Holds phone number with codes */
	private String localPhoneNumber;

	/** Holds email */
	private String email;

	private boolean exactMatchOnly;

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		if (exactMatchOnly) {
			return email;
		} else {
			return email != null ? WILDCARD_CHAR + email + WILDCARD_CHAR : null;
		}
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		if (exactMatchOnly) {
			return firstName;
		} else {
			return firstName != null ? WILDCARD_CHAR + firstName + WILDCARD_CHAR : null;
		}
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		if (exactMatchOnly) {
			return lastName;
		} else {
			return lastName != null ? WILDCARD_CHAR + lastName + WILDCARD_CHAR : null;
		}
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the telephoneNo.
	 */
	public String getTelephoneNo() {
		String phoneNumber = null;
		if (this.localPhoneNumber != null) {
			if (this.areaCode == null || this.areaCode.equals("")) {
				phoneNumber = WILDCARD_CHAR + this.localPhoneNumber;
			} else if (this.countryCode == null || this.countryCode.equals("")) {
				phoneNumber = WILDCARD_CHAR + this.areaCode + PHONENUMBER_SEPERATOR + this.localPhoneNumber;
			} else {
				phoneNumber = WILDCARD_CHAR + this.countryCode + PHONENUMBER_SEPERATOR + this.areaCode + PHONENUMBER_SEPERATOR
						+ this.localPhoneNumber;
			}
		}
		return phoneNumber;
	}

	public String getTelephoneNoForSQL() {
		return BeanUtils.getTelephoneNoForSQL(this.countryCode, this.areaCode, this.localPhoneNumber);
	}

	/**
	 * @param telephoneNo
	 *            The telephoneNo to set.
	 */
	public void setTelephoneNo(String countryCode, String areaCode, String localNumber) {
		this.countryCode = countryCode;
		this.areaCode = areaCode;
		this.localPhoneNumber = localNumber;
	}

	/**
	 * @return Returns the exactMatchOnly.
	 */
	public boolean isExactMatchOnly() {
		return exactMatchOnly;
	}

	/**
	 * @param exactMatchOnly
	 *            The exactMatchOnly to set.
	 */
	public void setExactMatchOnly(boolean exactMatchOnly) {
		this.exactMatchOnly = exactMatchOnly;
	}
}
