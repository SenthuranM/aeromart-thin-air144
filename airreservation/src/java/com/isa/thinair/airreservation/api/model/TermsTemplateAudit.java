package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Class that will keep track of changes done to {@link TermsTemplate} objects. This will enable a detailed version
 * history to be made along with the changed made in each version.
 * 
 * This class is designed for creation and reading only. No updates should be done.
 * 
 * equals and hashCode is overwritten to consider equality inside a set for hibernates use. Any decoupled objects
 * equality checks should be done on the {@link #id} instance. For newly created objects current equals method doesn't
 * guarantee a through equality test (For two identical edits on two different templates). There is absolutely no reason
 * to do this kind of comparisons.
 * 
 * @author thihara
 * @hibernate.class table = "T_TERMS_TEMPLATE_AUDIT"
 * 
 */
public class TermsTemplateAudit implements Serializable, Comparable<TermsTemplateAudit> {

	private static final long serialVersionUID = 113587641354568132L;

	/**
	 * Unique ID of TermsTemplateAudit. This is guaranteed to be unique globally for persisted TermsTemplateAudit
	 * instances.
	 */
	private Integer id;

	/**
	 * User ID of the modifying user
	 */
	private String userId;

	/**
	 * Modification time
	 */
	private Date modifiedTime;

	/**
	 * IP address modification originated from.
	 */
	private String ipAddress;

	/**
	 * Previous template content ({@link TermsTemplate#getTemplateContent()}) of the {@link #owningTemplate}
	 */
	private String previousContent;

	/**
	 * {@link TermsTemplate} this audit instance belongs to.
	 */
	private TermsTemplate owningTemplate;

	/**
	 * @return {@link #id}
	 * 
	 * @hibernate.id column = "TERMS_TEMPLATE_AUDIT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_TERMS_TEMPLATE_AUDIT"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            {@link #id}
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link #userId}
	 * 
	 * @hibernate.property column = "MODIFIED_BY"
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return {@link #modifiedTime}
	 * 
	 * @hibernate.property column = "MODIFIED_DATE"
	 */
	public Date getModifiedTime() {
		return modifiedTime;
	}

	/**
	 * @param {@link #modifiedTime}
	 */
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	/**
	 * @return {@link #ipAddress}
	 * 
	 * @hibernate.property column = "IP_ADDRESS"
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param {@link #ipAddress}
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return {@link #previousContent}
	 * 
	 * @hibernate.property column = "PREV_TERMS_TEMPLATE_CONTENT"
	 */
	public String getPreviousContent() {
		return previousContent;
	}

	/**
	 * @param {@link #previousContent}
	 */
	public void setPreviousContent(String previousContent) {
		this.previousContent = previousContent;
	}

	/**
	 * @return {@link #owningTemplate}
	 * 
	 * @hibernate.many-to-one column="TERMS_TEMPLATE_ID" class="com.isa.thinair.airreservation.api.model.TermsTemplate"
	 */
	public TermsTemplate getOwningTemplate() {
		return owningTemplate;
	}

	/**
	 * @param {@link #owningTemplate}
	 */
	public void setOwningTemplate(TermsTemplate owningTemplate) {
		this.owningTemplate = owningTemplate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + ((modifiedTime == null) ? 0 : modifiedTime.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof TermsTemplateAudit)) {
			return false;
		}
		TermsTemplateAudit other = (TermsTemplateAudit) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null) {
				return false;
			}
		} else if (!ipAddress.equals(other.ipAddress)) {
			return false;
		}
		if (modifiedTime == null) {
			if (other.modifiedTime != null) {
				return false;
			}
		} else if (!modifiedTime.equals(other.modifiedTime)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(TermsTemplateAudit o) {
		return this.modifiedTime.compareTo(o.modifiedTime);
	}

}
