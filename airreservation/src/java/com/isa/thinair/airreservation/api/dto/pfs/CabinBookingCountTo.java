/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;


/**
 * @author ashain
 *
 */
public class CabinBookingCountTo {
	private String cabinClass;
	private String bookingClass;
	private String bookingCountStr;
	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}
	/**
	 * @param cabinClass the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}
	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}
	/**
	 * @param bookingClass the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
	/**
	 * @return the bookingCountStr
	 */
	public String getBookingCountStr() {
		return bookingCountStr;
	}
	/**
	 * @param bookingCountStr the bookingCountStr to set
	 */
	public void setBookingCountStr(String bookingCountStr) {
		this.bookingCountStr = bookingCountStr;
	}
		
	
	
}
