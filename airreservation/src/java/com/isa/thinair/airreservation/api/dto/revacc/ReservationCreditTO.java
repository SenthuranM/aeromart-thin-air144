package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO.ReservationPaxChargeMetaTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxPaymentCredit;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author M.Rikaz
 * @since November 12, 2013
 */
public class ReservationCreditTO implements Serializable {

	private static final long serialVersionUID = -6794291358324461711L;

	private BigDecimal fareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal surchargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal otherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	// private Map<String, Collection<String>> creditConsumedGroupInfoMap = new HashMap<String, Collection<String>>();

	private Map<String, Map<String, BigDecimal>> creditConsumedGroupView = new HashMap<String, Map<String, BigDecimal>>();

	public BigDecimal getTotalCredit() {
		return AccelAeroCalculator.add(fareAmount, taxAmount, surchargeAmount, otherAmount);
	}

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getSurchargeAmount() {
		return surchargeAmount;
	}

	public void setSurchargeAmount(BigDecimal surchargeAmount) {
		this.surchargeAmount = surchargeAmount;
	}

	public BigDecimal getOtherAmount() {
		return otherAmount;
	}

	public void setOtherAmount(BigDecimal otherAmount) {
		this.otherAmount = otherAmount;
	}

	public void processAvailableCredits(Collection<ReservationCredit> colReservationCredit) {

		if (colReservationCredit != null && colReservationCredit.size() > 0) {
			for (ReservationCredit reservationCredit : colReservationCredit) {
				// need to validated at this point generated payment collection is correct /wrong if there is a mismatch
				// need to update the other amount with respective balance
				if (reservationCredit.getBalance().doubleValue() > 0 && reservationCredit.getPayments() != null
						&& reservationCredit.getPayments().size() > 0) {

					addPaymentCredits(reservationCredit.getPayments());

				}

			}

		}

	}

	public void addPaymentCredits(Collection<ReservationPaxPaymentCredit> colPaxPaymentCredit) {

		if (colPaxPaymentCredit != null && colPaxPaymentCredit.size() > 0) {
			for (ReservationPaxPaymentCredit paxPaymentCredit : colPaxPaymentCredit) {

				if (paxPaymentCredit != null) {

					BigDecimal amount = paxPaymentCredit.getAmount();
					String chargeGroupCode = paxPaymentCredit.getChargeGroupCode();
					addPaymentCredit(amount, chargeGroupCode);

				}

			}
		}

	}

	public void addPaymentCreditsOpt(Collection<ReservationPaxChargeMetaTO> colPaxPaymentCredit) {

		if (colPaxPaymentCredit != null && colPaxPaymentCredit.size() > 0) {
			for (ReservationPaxChargeMetaTO paxChargeMetaTO : colPaxPaymentCredit) {

				if (paxChargeMetaTO != null) {

					BigDecimal amount = paxChargeMetaTO.getAmount();
					String chargeGroupCode = paxChargeMetaTO.getChargeGroupCode();
					addPaymentCredit(amount, chargeGroupCode);

				}

			}
		}

	}

	public void addPaymentCredit(BigDecimal amount, String chargeGroupCode) {

		if (amount != null && chargeGroupCode != null) {
			if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeGroupCode)) {
				addFare(amount);
			} else if (ReservationInternalConstants.ChargeGroup.TAX.equals(chargeGroupCode)) {
				addTax(amount);
			} else if (ReservationInternalConstants.ChargeGroup.SUR.equals(chargeGroupCode)) {
				addSurcharge(amount);
			} else {
				addOtherCharges(amount);
			}

			// totalCredit = AccelAeroCalculator.add(totalCredit, amount);
		}

	}

	public void addFare(BigDecimal amount) {
		fareAmount = AccelAeroCalculator.add(fareAmount, amount);

	}

	public void addTax(BigDecimal amount) {
		taxAmount = AccelAeroCalculator.add(taxAmount, amount);

	}

	public void addSurcharge(BigDecimal amount) {
		surchargeAmount = AccelAeroCalculator.add(surchargeAmount, amount);

	}

	public void addOtherCharges(BigDecimal amount) {
		otherAmount = AccelAeroCalculator.add(otherAmount, amount);

	}

	public Map<String, Map<String, BigDecimal>> getCreditConsumedGroupView() {
		return creditConsumedGroupView;
	}

	public void setCreditConsumedGroupView(Map<String, Map<String, BigDecimal>> creditConsumedGroupView) {
		this.creditConsumedGroupView = creditConsumedGroupView;
	}

	public void addCreditConsumedGroupNew(String chargeConsumedTo, String chargeConsumedFrom, BigDecimal consumedAmount) {
		if (!StringUtil.isNullOrEmpty(chargeConsumedTo) && !StringUtil.isNullOrEmpty(chargeConsumedFrom)
				&& consumedAmount != null && consumedAmount.doubleValue() > 0) {

			Map<String, BigDecimal> consumedAmountGroupWise = null;

			if (this.getCreditConsumedGroupView() == null) {
				this.creditConsumedGroupView = new HashMap<String, Map<String, BigDecimal>>();
			}
			consumedAmountGroupWise = this.creditConsumedGroupView.get(chargeConsumedTo);

			if (consumedAmountGroupWise == null) {
				consumedAmountGroupWise = new HashMap<String, BigDecimal>();
			}

			consumedAmountGroupWise.put(chargeConsumedFrom, consumedAmount);

			this.creditConsumedGroupView.put(chargeConsumedTo, consumedAmountGroupWise);
		}

	}

	public Map<String, BigDecimal> getAppliedChargeGroupsNew(String chargeGroup) {

		if (!StringUtil.isNullOrEmpty(chargeGroup)) {
			if (!ChargeGroup.FAR.equals(chargeGroup) && !ChargeGroup.TAX.equals(chargeGroup)
					&& !ChargeGroup.SUR.equals(chargeGroup)) {
				chargeGroup = ChargeGroup.ANY;
			}
			return this.getCreditConsumedGroupView().get(chargeGroup);
		}

		return null;

	}

}
