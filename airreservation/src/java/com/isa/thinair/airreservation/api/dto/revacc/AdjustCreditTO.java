package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;

/**
 * @author Nilindra Fernando
 * @since August 16, 2010
 */
public class AdjustCreditTO implements Serializable, Comparable<AdjustCreditTO>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2195169173159062344L;

	private String pnr;

	private Integer pnrPaxId;

	private BigDecimal amount;

	private String userNotes;

	private CredentialsDTO credentialsDTO;

	private ReservationPaxOndCharge reservationPaxOndCharge;

	private boolean enableTransactionGranularity;

	private Integer originalPaymentTnxID;

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the userNotes
	 */
	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @param userNotes
	 *            the userNotes to set
	 */
	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	/**
	 * @return the credentialsDTO
	 */
	public CredentialsDTO getCredentialsDTO() {
		return credentialsDTO;
	}

	/**
	 * @param credentialsDTO
	 *            the credentialsDTO to set
	 */
	public void setCredentialsDTO(CredentialsDTO credentialsDTO) {
		this.credentialsDTO = credentialsDTO;
	}

	/**
	 * @return the reservationPaxOndCharge
	 */
	public ReservationPaxOndCharge getReservationPaxOndCharge() {
		return reservationPaxOndCharge;
	}

	/**
	 * @param reservationPaxOndCharge
	 *            the reservationPaxOndCharge to set
	 */
	public void setReservationPaxOndCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		this.reservationPaxOndCharge = reservationPaxOndCharge;
	}

	public boolean isEnableTransactionGranularity() {
		return enableTransactionGranularity;
	}

	public void setEnableTransactionGranularity(boolean enableTransactionGranularity) {
		this.enableTransactionGranularity = enableTransactionGranularity;
	}

	public Integer getOriginalPaymentTnxID() {
		return originalPaymentTnxID;
	}

	public void setOriginalPaymentTnxID(Integer originalPaymentTnxID) {
		this.originalPaymentTnxID = originalPaymentTnxID;
	}

	@Override
	public int compareTo(AdjustCreditTO adjustCreditTO) {
		return this.getAmount().compareTo(adjustCreditTO.getAmount());
	}

}
