/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * To hold PNLADLDestinationDTO data transfer information
 * 
 * @author isuru
 * @since 1.0
 */
public class PNLADLDestinationSupportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2174012176355305218L;

	private String destinationStatusCode;

	private String bookingCode;
	
	private String cabinClassCode;

	private ReservationPaxDetailsDTO passenger;

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getDestinationStatusCode() {
		return destinationStatusCode;
	}

	public void setDestinationStatusCode(String destinationStatusCode) {
		this.destinationStatusCode = destinationStatusCode;
	}

	public ReservationPaxDetailsDTO getPassenger() {
		return passenger;
	}

	public void setPassenger(ReservationPaxDetailsDTO passenger) {
		this.passenger = passenger;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
}
