/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;

/**
 * Holds the flight reconcile data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class FlightReconcileDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9204614676138521351L;

	/** Holds the pnr number */
	private String pnr;

	/** Holds the reservation segment id */
	private Integer pnrSegId;

	/** Holds the flight segment id */
	private Integer flightSegId;

	/** Holds the flight id */
	private Integer flightId;

	/** Holds the flight segment code */
	private String segementCode;

	/** Holds the flight departure datetime in departing station's local time */
	private Date departureDateTimeLocal;

	/** holds whether flight is domestic or international */
	private String flightType;

	/** Holds the flight segments departure date time zulu */
	private Date departureDateTimeZulu;

	private String cabinClassCode;
	
	private String flightNumber;
	
	private Date arrivalDateTimeLocal;
	
	private Date arrivalDateTimeZulu;
	/**
	 * @return Returns the flightSegId.
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            The flightSegId to set.
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the pnrSegId.
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the flightId.
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return Returns the segementCode.
	 */
	public String getSegementCode() {
		return segementCode;
	}

	/**
	 * @param segementCode
	 *            The segementCode to set.
	 */
	public void setSegementCode(String segementCode) {
		this.segementCode = segementCode;
	}

	/**
	 * @return
	 */
	public String getFlightType() {
		return flightType;
	}

	/**
	 * @param flightType
	 */
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);
	}

	/**
	 * Overides the to String method
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	/**
	 * Overides the hash code method
	 */
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/**
	 * @return Return the departureDateTimeLocal
	 */
	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}

	/**
	 * @param departureDateTimeLocal
	 */
	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	/**
	 * @return the departureDateTimeZulu
	 */
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	/**
	 * @param departureDateTimeZulu
	 *            the departureDateTimeZulu to set
	 */
	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public boolean isInternationalFlight() {
		return getFlightType().equalsIgnoreCase(FlightType.INTERNATIONL.getType());
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}
	
	
}
