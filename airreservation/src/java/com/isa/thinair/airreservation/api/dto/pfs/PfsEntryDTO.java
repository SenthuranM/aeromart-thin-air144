package com.isa.thinair.airreservation.api.dto.pfs;

import java.io.Serializable;
import java.util.Date;

public class PfsEntryDTO implements Serializable {

	private static final long serialVersionUID = 1L;


	private String flightNumber;

	private Date receivedDate;

	private Date flightDate;

	private String departureAirport;

	private String pnr;

	private String title;

	private String firstName;

	private String lastName;

	private String entryStatus;

	private String cabinClassCode;

	private String arrivalAirport;

	private String errorDescription;

	private String paxCategoryCode;

	private String paxType;
	
	private String isParent;
	
	private String eTicketNo;
	
	private PfsEntryDTO infant;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEntryStatus() {
		return entryStatus;
	}

	public void setEntryStatus(String entryStatus) {
		this.entryStatus = entryStatus;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getIsParent() {
		return isParent;
	}

	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}

	public String geteTicketNo() {
		return eTicketNo;
	}

	public void seteTicketNo(String eTicketNo) {
		this.eTicketNo = eTicketNo;
	}

	public PfsEntryDTO getInfant() {
		return infant;
	}

	public void setInfant(PfsEntryDTO infant) {
		this.infant = infant;
	}
}
