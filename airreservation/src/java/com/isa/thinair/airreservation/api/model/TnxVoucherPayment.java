package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.api.dto.VoucherDTO;

/**
 * @author chethiya
 *
 */
public class TnxVoucherPayment extends TnxPayment {

	private static final long serialVersionUID = 5413890028126827519L;

	VoucherDTO voucherDTO;

	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

}
