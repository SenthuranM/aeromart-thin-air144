package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Represents the external carrier pax transactions of a dry/interline booking TODO This class and ReservationTnx can
 * share a common super class. Need to to that refac
 * 
 * @author mekanayake
 * @hibernate.class table = "T_PAX_EXT_CARRIER_TRANSACTIONS"
 */
public class ReservationPaxExtTnx extends Persistent implements Serializable {

	private static final long serialVersionUID = -8229278377341786777L;

	private Integer paxExternalTnxId;

	private Integer pnrPaxId;

	private String externalCarrierCode;

	private String lccUniqueId;

	/** Holds transaction type */
	private String tnxType;

	/** Holds transaction nominal code (ticket,part..etc) */
	private Integer nominalCode;

	/** Holds transaction amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal amountExtBase = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal amountPayCur = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the pay currency code */
	private String payCurrencyCode;

	/** Holds transaction date */
	private Date dateTime;

	/** Holds sales channel code */
	private Integer salesChannelCode;

	/** Holds payment type */
	private String agentCode;

	/** Holds user id */
	private String userId;

	/** Holds remarks **/
	private String remarks;
	
	private Integer originalExtPaymentTnxID;

	/** Hold credit card payment details for the transaction. This will not be saved by saving this obj **/
	private CreditCardDetail creditCardDetail = null;
	
	
	private String firstPayment;

	/** This holds voucher id if any other payment method refernace can hold then uniquely identif along with nominal code **/
	private String externalReference;

	/**
	 * @hibernate.id column = "PAX_EXT_CARRIER_TXN_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PAX_EXT_CARRIER_TRANSACTIONS"
	 * @return the paxExternalTnxId
	 */
	public Integer getPaxExternalTnxId() {
		return paxExternalTnxId;
	}

	/**
	 * @param paxExternalTnxId
	 *            the paxExternalTnxId to set
	 */
	public void setPaxExternalTnxId(Integer paxExternalTnxId) {
		this.paxExternalTnxId = paxExternalTnxId;
	}

	/**
	 * @return the pnrPaxId
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the externalCarrierCode
	 * @hibernate.property column = "EXT_CARRIER_CODE"
	 */
	public String getExternalCarrierCode() {
		return externalCarrierCode;
	}

	/**
	 * @param externalCarrierCode
	 *            the externalCarrierCode to set
	 */
	public void setExternalCarrierCode(String externalCarrierCode) {
		this.externalCarrierCode = externalCarrierCode;
	}

	/**
	 * @return the lccUniqueId
	 * @hibernate.property column = "LCC_UNIQUE_TXN_ID"
	 */
	public String getLccUniqueId() {
		return lccUniqueId;
	}

	/**
	 * @param lccUniqueId
	 *            the lccUniqueId to set
	 */
	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	/**
	 * @return the tnxType
	 * @hibernate.property column = "DR_CR"
	 */
	public String getTnxType() {
		return tnxType;
	}

	/**
	 * @param tnxType
	 *            the tnxType to set
	 */
	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	/**
	 * @return the nominalCode
	 * @hibernate.property column = "NOMINAL_CODE"
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            the nominalCode to set
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the amountExtBase
	 * @hibernate.property column = "AMOUNT_EXT_BASECUR"
	 */
	public BigDecimal getAmountExtBase() {
		return amountExtBase;
	}

	/**
	 * @param amountExtBase
	 *            the amountExtBase to set
	 */
	public void setAmountExtBase(BigDecimal amountExtBase) {
		this.amountExtBase = amountExtBase;
	}

	/**
	 * @return the amountPayCur
	 * @hibernate.property column = "AMOUNT_PAYCUR"
	 */
	public BigDecimal getAmountPayCur() {
		return amountPayCur;
	}

	/**
	 * @param amountPayCur
	 *            the amountPayCur to set
	 */
	public void setAmountPayCur(BigDecimal amountPayCur) {
		this.amountPayCur = amountPayCur;
	}

	/**
	 * @return the payCurrencyCode
	 * @hibernate.property column = "PAYCUR_CODE"
	 */
	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	/**
	 * @param payCurrencyCode
	 *            the payCurrencyCode to set
	 */
	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	/**
	 * @return the dateTime
	 * @hibernate.property column = "TXN_TIMESTAMP"
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime
	 *            the dateTime to set
	 */
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return the salesChannelCode
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            the salesChannelCode to set
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the remarks
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	/**
	 * @return the originalExtPaymentTnxID
	 * @hibernate.property column = "EXT_PAYMENT_TXN_ID"
	 */
	public Integer getOriginalExtPaymentTnxID() {
		return originalExtPaymentTnxID;
	}

	/**
	 * @param originalExtPaymentTnxID the originalExtPaymentTnxID to set
	 */
	public void setOriginalExtPaymentTnxID(Integer originalExtPaymentTnxID) {
		this.originalExtPaymentTnxID = originalExtPaymentTnxID;
	}
	
	/**
	 * @return the firstPayment
	 * @hibernate.property column = "FIRST_PAYMENT"
	 */
	public String getFirstPayment() {
		return firstPayment;
	}

	public void setFirstPayment(String firstPayment) {
		this.firstPayment = firstPayment;
	}

	public CreditCardDetail getCreditCardDetail() {
		return creditCardDetail;
	}

	public void setCreditCardDetail(CreditCardDetail creditCardDetail) {
		this.creditCardDetail = creditCardDetail;
	}

	/**
	 * @return the firstPayment
	 * @hibernate.property column = "EXT_REFERENCE"
	 */
	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String voucherID) {
		this.externalReference = voucherID;
	}

}
