/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.commons.core.security.UserDST;

/**
 * To hold reservation authentication data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CredentialsDTO implements Serializable {
	
	private static final long serialVersionUID = 5411176147064274436L;

	/** Holds user id */
	private String userId;

	/** Holds the userPasswod . Used for dry user */
	private String password;

	/** Holds customer id */
	private Integer customerId;

	/** Holds sales channel code */
	private Integer salesChannelCode;
	
	/** Holds directbill id */
	private String directBillId;

	/** Holds agent code */
	private String agentCode;

	/** Holds the agent station */
	private String agentStation;

	/** Holds the user dst information */
	private Collection<UserDST> colUserDST;

	/** Holds track informations */
	private TrackInfoDTO trackInfoDTO;

	/** Holds user default carrier code */
	private String defaultCarrierCode;

	/** Holds the Agent Currency Code */
	private String agentCurrencyCode;

	/** Holds the current display name */
	private String displayName;	
	/** Holds actual channel code for lcc bookings eg XBE-3, IBE-4 
	 * Do not use other purpose 
	 **/
	private int marketingBookingChannel;
	
	/**
	 * @return Returns the customerId.
	 */	
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the salesChannelCode.
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the trackInfoDTO.
	 */
	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	/**
	 * @param trackInfoDTO
	 *            The trackInfoDTO to set.
	 */
	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}

	/**
	 * @return Returns the colUserDST.
	 */
	public Collection<UserDST> getColUserDST() {
		return colUserDST;
	}

	/**
	 * @param colUserDST
	 *            The colUserDST to set.
	 */
	public void setColUserDST(Collection<UserDST> colUserDST) {
		this.colUserDST = colUserDST;
	}

	/**
	 * @return Returns the agentStation.
	 */
	public String getAgentStation() {
		return agentStation;
	}

	/**
	 * @param agentStation
	 *            The agentStation to set.
	 */
	public void setAgentStation(String agentStation) {
		this.agentStation = agentStation;
	}

	/**
	 * @return Returns the defaultCarrierCode.
	 */
	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	/**
	 * @param defaultCarrierCode
	 *            The defaultCarrierCode to set.
	 */
	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	/**
	 * @return the agentCurrencyCode
	 */
	public String getAgentCurrencyCode() {
		return agentCurrencyCode;
	}

	/**
	 * @param agentCurrencyCode
	 *            the agentCurrencyCode to set
	 */
	public void setAgentCurrencyCode(String agentCurrencyCode) {
		this.agentCurrencyCode = agentCurrencyCode;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName
	 *            the current display name for the user_id
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDirectBillId() {
		return directBillId;
	}

	public void setDirectBillId(String directBillId) {
		this.directBillId = directBillId;
	}

	public int getMarketingBookingChannel() {
		return marketingBookingChannel;
	}

	public void setMarketingBookingChannel(int marketingBookingChannel) {
		this.marketingBookingChannel = marketingBookingChannel;
	}	
	
}