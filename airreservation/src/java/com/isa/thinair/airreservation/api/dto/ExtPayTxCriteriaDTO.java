package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Specifies criteria for loading external payment transaction information
 * 
 * @author Mohamed Nasly
 */
public class ExtPayTxCriteriaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2441082865674351458L;

	private String pnr = null;

	private String balanceQueryKey = null;

	private List<String> statuses = null;

	private Date startTimestamp = null;

	private Date endTimestamp = null;

	private List<String> reconStatusToInclude = null;

	private boolean includeAbortedManualReconTxns = false;

	private String agentCode = null;

	public String getBalanceQueryKey() {
		return balanceQueryKey;
	}

	public void setBalanceQueryKey(String balanceQueryKey) {
		this.balanceQueryKey = balanceQueryKey;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(Date endTimestamp) {
		this.endTimestamp = endTimestamp;
	}

	public Date getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(Date startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public void addStatus(String status) {
		if (statuses == null) {
			statuses = new ArrayList<String>();
		}
		statuses.add(status);
	}

	public List<String> getStatuses() {
		return this.statuses;
	}

	public List<String> getReconStatusToInclude() {
		return reconStatusToInclude;
	}

	public void addReconStatusToInclude(String reconStatusToInclude) {
		if (this.reconStatusToInclude == null) {
			this.reconStatusToInclude = new ArrayList<String>();
		}
		this.reconStatusToInclude.add(reconStatusToInclude);
	}

	public boolean isIncludeAbortedManualReconTxns() {
		return includeAbortedManualReconTxns;
	}

	public void setIncludeAbortedManualReconTxns(boolean includeAbortedManualReconTxns) {
		this.includeAbortedManualReconTxns = includeAbortedManualReconTxns;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
}
