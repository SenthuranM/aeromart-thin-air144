package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import java.util.ArrayList;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegmentGroup;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class Group5 extends EDISegmentGroup {

	protected TVL tvl;
	protected ArrayList<SSR> ssrCollection = new ArrayList<SSR>();

	public void addSsr(SSR ssr) {
		this.ssrCollection.add(ssr);
	}

	public Group5() {
		super("Group5");
	}

	public void setTvl(TVL tvl) {
		this.tvl = tvl;
	}

	@Override
	public MessageSegment build() {
		addMessageSegmentIfNotEmpty(tvl);
		if (ssrCollection != null && !ssrCollection.isEmpty()) {
			addMessageSegmentsIfNotEmpty(ssrCollection);
		}

		return this;
	}

}
