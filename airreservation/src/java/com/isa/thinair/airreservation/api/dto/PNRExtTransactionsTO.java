package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

/**
 * Encapsualates external payment transactions for a given PNR.
 * 
 * @author Nasly
 */
public class PNRExtTransactionsTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5667614529735348222L;

	private String pnr = null;

	private Collection<ExternalPaymentTnx> extPayTransactions = null;

	public Collection<ExternalPaymentTnx> getExtPayTransactions() {
		return extPayTransactions;
	}

	public void addExtPayTransactions(ExternalPaymentTnx externalPaymentTnx) {
		if (getExtPayTransactions() == null) {
			this.extPayTransactions = new ArrayList<ExternalPaymentTnx>();
		}
		this.extPayTransactions.add(externalPaymentTnx);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * Return false if at least on of the PNR transaction is not reconciled yet.
	 * 
	 * @return
	 */
	public Boolean isPNRTxnsEligibleForManualRecon() {
		boolean isEligibleForRecon = false;
		if (getExtPayTransactions() != null && getExtPayTransactions().size() > 0) {
			ExternalPaymentTnx txn = null;
			for (Iterator<ExternalPaymentTnx> extPayTxIt = getExtPayTransactions().iterator(); extPayTxIt.hasNext();) {
				txn = (ExternalPaymentTnx) extPayTxIt.next();
				if (ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED.equals(txn.getReconcilationStatus())
						|| ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED.equals(txn.getReconcilationStatus())
						|| (ReservationInternalConstants.ExtPayTxStatus.ABORTED.equals(txn.getStatus()) && ReservationInternalConstants.ExtPayTxReconSourceType.MANUAL
								.equals(txn.getReconciliationSource()))) {
					isEligibleForRecon = true;
					break;
				}
			}
		}
		return isEligibleForRecon;
	}

	public StringBuffer getSummary() {
		StringBuffer summmary = new StringBuffer();
		String nl = System.getProperty("line.separator");
		summmary.append("PNR External Transactions Summary ::" + nl);
		summmary.append("pnr =").append(getPnr()).append(nl);
		if (getExtPayTransactions() != null && getExtPayTransactions().size() > 0) {
			Collection<ExternalPaymentTnx> txns = getExtPayTransactions();
			for (ExternalPaymentTnx txn : txns) {
				summmary.append(txn.getSummary());
			}
		}
		summmary.append(nl);

		return summmary;
	}
}
