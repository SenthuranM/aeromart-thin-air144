/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of reservation segment ssrs for a passenger
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNR_PAX_SEGMENT_SSR"
 */
public class ReservationPaxSegmentSSR extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8522882346274794671L;
	public static final String APPLY_ON_DEPARTURE = "D";
	public static final String APPLY_ON_ARRIVAL = "A";
	public static final String APPLY_ON_TRANSIT = "T";
	public static final String APPLY_ON_SEGMENT = "F";

	/** Holds pnr passenger segment ssr id */
	private Integer pnrPaxSegmentSSRId;

	/** Holds context id */
	private Integer contextId;

	/** Holds ssr id */
	private Integer sSRId;

	/** Holds ssr text */
	private String sSRText;

	private BigDecimal chargeAmount;

	/** Holds status */
	private String status;

	/** Holds email status */
	private String emailStatus;

	/** Holds PNL/ADL status */
	private String pNLADLStatus;

	/** Holds application level for a segment[D=departure, A-arrival, T=transit, F-full-segment */
	private String applyOn;

	/** Holds Holds external reference */
	private String externalReference;

	/** Holds airport service applicable airport */
	private String airportCode;

	/** Holds a instance of reservation pax fare segment */
	private ReservationPaxFareSegment reservationPaxFareSegment;

	private Integer autoCancellationId;

	/**
	 * @return Returns the pnrPaxFareId.
	 * @hibernate.id column = "PPSS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_SEGMENT_SSR"
	 */
	public Integer getPnrPaxSegmentSSRId() {
		return pnrPaxSegmentSSRId;
	}

	/**
	 * @param PaxSegmentSSRId
	 *            The PaxSegmentSSRId to set.
	 */
	public void setPnrPaxSegmentSSRId(Integer pnrPaxSegmentSSRId) {
		this.pnrPaxSegmentSSRId = pnrPaxSegmentSSRId;
	}

	/**
	 * @hibernate.property column = "SSR_ID"
	 */
	public Integer getSSRId() {
		return sSRId;
	}

	public void setSSRId(Integer ssrId) {
		sSRId = ssrId;
	}

	/**
	 * @hibernate.property column = "SSR_TEXT"
	 */
	public String getSSRText() {
		return sSRText;
	}

	public void setSSRText(String string) {
		sSRText = string;
	}

	/**
	 * Overrides hash code to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrPaxSegmentSSRId()).toHashCode();
	}

	/**
	 * Overrides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPaxSegmentSSR) {
			ReservationPaxSegmentSSR castObject = (ReservationPaxSegmentSSR) o;
			if (castObject.getPnrPaxSegmentSSRId() == null || this.getPnrPaxSegmentSSRId() == null) {
				return false;
			} else if (castObject.getPnrPaxSegmentSSRId().intValue() == this.getPnrPaxSegmentSSRId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @hibernate.property column = "CONTEXT_ID"
	 */
	public Integer getContextId() {
		return contextId;
	}

	public void setContextId(Integer contextId) {
		this.contextId = contextId;
	}

	/**
	 * @hibernate.property column = "CHARGE_AMOUNT"
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "EMAIL_STATUS"
	 */
	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	/**
	 * @hibernate.property column = "PNL_ADL_STATUS"
	 */
	public String getPNLADLStatus() {
		return this.pNLADLStatus;
	}

	public void setPNLADLStatus(String status) {
		this.pNLADLStatus = status;
	}

	/**
	 * @hibernate.property column = "APPLY_ON"
	 */
	public String getApplyOn() {
		return this.applyOn;
	}

	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	/**
	 * @hibernate.property column = "EXT_REF"
	 */
	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @hibernate.many-to-one column="PPFS_ID"
	 *                        class="com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment"
	 * @return Returns the reservationPaxFareSegment.
	 */
	public ReservationPaxFareSegment getReservationPaxFareSegment() {
		return reservationPaxFareSegment;
	}

	public void setReservationPaxFareSegment(ReservationPaxFareSegment reservationPaxFareSegment) {
		this.reservationPaxFareSegment = reservationPaxFareSegment;
	}

	/**
	 * @return the airportCode
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 * 
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @hibernate.property column = "AUTO_CANCELLATION_ID"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	public Object clone() {
		ReservationPaxSegmentSSR reservationPaxSegmentSSR = new ReservationPaxSegmentSSR();
		reservationPaxSegmentSSR.setApplyOn(this.getApplyOn());
		reservationPaxSegmentSSR.setChargeAmount(this.getChargeAmount());
		reservationPaxSegmentSSR.setContextId(this.getContextId());
		reservationPaxSegmentSSR.setEmailStatus(this.getEmailStatus());
		reservationPaxSegmentSSR.setPNLADLStatus(this.getEmailStatus());
		reservationPaxSegmentSSR.setSSRText(this.getSSRText());
		reservationPaxSegmentSSR.setStatus(this.getStatus());
		reservationPaxSegmentSSR.setAutoCancellationId(this.getAutoCancellationId());
		return reservationPaxSegmentSSR;
	}
}
