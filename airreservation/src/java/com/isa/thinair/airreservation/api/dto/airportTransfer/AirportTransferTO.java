package com.isa.thinair.airreservation.api.dto.airportTransfer;

import java.io.Serializable;

public class AirportTransferTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2306360290016890318L;

	private int airportTransferId;
	
	private String airportCode;
	
	private String providerName;
	
	private String providerAddress;
	
	private String providerNumber;
	
	private String providerEmail;
	
	private String status;

	public int getAirportTransferId() {
		return airportTransferId;
	}

	public void setAirportTransferId(int airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderAddress() {
		return providerAddress;
	}

	public void setProviderAddress(String providerAddress) {
		this.providerAddress = providerAddress;
	}

	public String getProviderNumber() {
		return providerNumber;
	}

	public void setProviderNumber(String providerNumber) {
		this.providerNumber = providerNumber;
	}

	public String getProviderEmail() {
		return providerEmail;
	}

	public void setProviderEmail(String providerEmail) {
		this.providerEmail = providerEmail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
