package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 *  
 * 
 * @author subash
 * 
 * @hibernate.class table = "T_BL_PNR_PASSENGER "
 *
 */
public class BlacklistReservation extends Tracking {

	private static final long serialVersionUID = 1L;
	
	/** Holds blacklist reservation id */
	private Long blacklistReservationId;

	/** Holds blacklist pax id */
	private Integer blacklistPaxId;
	
	/** Holds  reservation pax id */
	private Integer pnrPaxId;
	
	/** Holds reason to allow */
	private String reasonToAllow;
	
	/** Holds is action */
	private String isActioned;
	
	private String reasonForWhitelisted;
	
	/**
	 * @return Returns the blacklistReservationId.
	 * 
	 * @hibernate.id column="BL_PNR_PASSENGER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BL_PNR_PASSENGER"
	 */	
	public Long getBlacklistReservationId() {
		return blacklistReservationId;
	}

	public void setBlacklistReservationId(Long blacklistReservationId) {
		this.blacklistReservationId = blacklistReservationId;
	}
	
	/**
	 * @hibernate.property column = "BL_PASSENGER_ID"
	 * 
	 * @see #blacklistPaxId
	 */
	public Integer getBlacklistPaxId() {
		return blacklistPaxId;
	}

	public void setBlacklistPaxId(Integer blacklistPaxId) {
		this.blacklistPaxId = blacklistPaxId;
	}
	
	/**
	 * @hibernate.property column = "PNR_PAX_ID"
	 * 
	 * @see #pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer reservationPaxId) {
		this.pnrPaxId = reservationPaxId;
	}
	
	/**
	 * @hibernate.property column = "REASON_TO_ALLOW"
	 * 
	 * @see #reasonToAllow
	 */
	public String getReasonToAllow() {
		return reasonToAllow;
	}

	public void setReasonToAllow(String reasonToAllow) {
		this.reasonToAllow = reasonToAllow;
	}
	
	/**
	 * @hibernate.property column = "IS_ACTIONED"
	 * 
	 * @see #isActioned
	 */
	public String getIsActioned() {
		return isActioned;
	}

	public void setIsActioned(String isActioned) {
		this.isActioned = isActioned;
	}
	
	/**
	 * @hibernate.property column = "REASON_TO_CLEAR"
	 * 
	 * @see #reasonForWhitelisted
	 */
	public String getReasonForWhitelisted() {
		return reasonForWhitelisted;
	}

	public void setReasonForWhitelisted(String reasonForWhitelisted) {
		this.reasonForWhitelisted = reasonForWhitelisted;
	}
		
}
