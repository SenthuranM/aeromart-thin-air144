/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold reservation charges data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PnrChargesDTO implements Serializable {

	private static final long serialVersionUID = 2239199009018454490L;

	/** Holds the reservation number */
	private String pnr;

	/** Holds the passenger id */
	private Integer pnrPaxId;

	/** Holds the passenger type */
	private String paxType;

	/** Holds the refundable amounts */
	private BigDecimal[] refundableAmounts = ReservationApiUtils.getChargeTypeAmounts();

	/** Holds the refundable meta to */
	private Collection<ChargeMetaTO> refundableChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

	/** Holds the non refundable meta to */
	private Collection<ChargeMetaTO> nonRefundableChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

	/** Holds the extra fee meta to */
	private Collection<ChargeMetaTO> extraFeeChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

	/** Holds the actual amounts */
	private BigDecimal[] actualAmounts = ReservationApiUtils.getChargeTypeAmounts();

	/** Holds the actual meta to */
	private Collection<ChargeMetaTO> actualChargeMetaAmounts = new ArrayList<ChargeMetaTO>();

	/** Holds the actual cancellation amount */
	private BigDecimal actualCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the identified cancellation amount */
	private BigDecimal identifiedCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the actual modification amount */
	private BigDecimal actualModificationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the identified modification amount */
	private BigDecimal identifiedModificationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds reservation passenger available credit or debit amount */
	private BigDecimal availableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the payment information a collection of ReservationTnx */
	private Collection<ReservationTnx> paymentInfo = new ArrayList<ReservationTnx>();

	/** Holds the this dto is carring a infomation related to parent */
	private Boolean parent = null;

	private PnrChargeDetailTO cnxChargeDetailTO;

	private PnrChargeDetailTO modChargeDetailTO;
	
	private PnrChargeDetailTO nameChangeChargeDetailTO;
	
	private BigDecimal nameChangeChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal modificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal identifiedExtraFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private Collection<ChargeMetaTO> serviceTaxPenaltyCharges = new ArrayList<ChargeMetaTO>();


	/**
	 * @return Returns the actualCancellationAmount.
	 */
	public BigDecimal getActualCancellationAmount() {
		return actualCancellationAmount;
	}

	/**
	 * @param actualCancellationAmount
	 *            The actualCancellationAmount to set.
	 */
	public void setActualCancellationAmount(BigDecimal actualCancellationAmount) {
		this.actualCancellationAmount = actualCancellationAmount;
	}

	/**
	 * @return Returns the actualModificationAmount.
	 */
	public BigDecimal getActualModificationAmount() {
		return actualModificationAmount;
	}

	/**
	 * @param actualModificationAmount
	 *            The actualModificationAmount to set.
	 */
	public void setActualModificationAmount(BigDecimal actualModificationAmount) {
		this.actualModificationAmount = actualModificationAmount;
	}

	/**
	 * @return Returns the identifiedCancellationAmount.
	 */
	public BigDecimal getIdentifiedCancellationAmount() {
		return identifiedCancellationAmount;
	}

	/**
	 * @param identifiedCancellationAmount
	 *            The identifiedCancellationAmount to set.
	 */
	public void setIdentifiedCancellationAmount(BigDecimal identifiedCancellationAmount) {
		this.identifiedCancellationAmount = identifiedCancellationAmount;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the actualAmounts.
	 */
	public BigDecimal[] getActualAmounts() {
		return actualAmounts;
	}

	/**
	 * @param actualAmounts
	 *            The actualAmounts to set.
	 */
	public void setActualAmounts(BigDecimal[] actualAmounts) {
		this.actualAmounts = actualAmounts;
	}

	/**
	 * @return Returns the refundableAmounts.
	 */
	public BigDecimal[] getRefundableAmounts() {
		return refundableAmounts;
	}

	/**
	 * @param refundableAmounts
	 *            The refundableAmounts to set.
	 */
	public void setRefundableAmounts(BigDecimal[] refundableAmounts) {
		this.refundableAmounts = refundableAmounts;
	}

	/**
	 * Return refundable fare
	 * 
	 * @return
	 */
	public BigDecimal getRefundableFare() {
		return this.getRefundableAmounts()[0];
	}

	/**
	 * Return refundable charge
	 * 
	 * @return
	 */
	public BigDecimal getRefundableCharge() {
		return AccelAeroCalculator.subtract(AccelAeroCalculator.add(this.getRefundableAmounts()), getRefundableFare());
	}

	/**
	 * Return the total refundable amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalRefundableAmount() {
		return AccelAeroCalculator.add(this.getRefundableAmounts());
	}

	/**
	 * Returns the total actual amount
	 * 
	 * @return
	 */
	public BigDecimal getTotalActualAmount() {
		return AccelAeroCalculator.add(this.getActualAmounts());
	}

	/**
	 * Return actual fare
	 * 
	 * @return
	 */
	public BigDecimal getActualFare() {
		return this.getActualAmounts()[0];
	}

	/**
	 * Return actual charge
	 * 
	 * @return
	 */
	public BigDecimal getActualCharge() {
		return AccelAeroCalculator.subtract(AccelAeroCalculator.add(this.getActualAmounts()), getActualFare());
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the availableBalance.
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * @param availableBalance
	 *            The availableBalance to set.
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * @return Returns the isInfant.
	 */
	public boolean isInfant() {
		return ReservationInternalConstants.PassengerType.INFANT.equals(getPaxType());
	}

	/**
	 * @return Returns the identifiedModificationAmount.
	 */
	public BigDecimal getIdentifiedModificationAmount() {
		return identifiedModificationAmount;
	}

	/**
	 * @param identifiedModificationAmount
	 *            The identifiedModificationAmount to set.
	 */
	public void setIdentifiedModificationAmount(BigDecimal identifiedModificationAmount) {
		this.identifiedModificationAmount = identifiedModificationAmount;
	}

	/**
	 * @return Returns the paymentInfo.
	 */
	public Collection<ReservationTnx> getPaymentInfo() {
		return paymentInfo;
	}

	/**
	 * Add the payment information
	 * 
	 * @param colPaymentInfoHolder
	 * @param colPaymentInfoSource
	 */
	public void addPaymentInfo(Collection<ReservationTnx> colPaymentInfoHolder, Collection<ReservationTnx> colPaymentInfoSource) {
		this.getPaymentInfo().addAll(colPaymentInfoHolder);
		this.checkAndAddReservationTnxs(colPaymentInfoSource, this.getPaymentInfo());
	}

	/**
	 * Check and add payment info
	 * 
	 * @param colPaymentInfo
	 */
	private void checkAndAddReservationTnxs(Collection<ReservationTnx> colNewReservationTnx,
			Collection<ReservationTnx> colAccumilatorReservationTnx) {
		if (colNewReservationTnx != null && colNewReservationTnx.size() > 0) {
			for (Iterator<ReservationTnx> iterator = colNewReservationTnx.iterator(); iterator.hasNext();) {
				ReservationTnx reservationTnx = (ReservationTnx) iterator.next();

				if (!colAccumilatorReservationTnx.contains(reservationTnx)) {
					colAccumilatorReservationTnx.add(reservationTnx);
				}
			}
		}
	}

	/**
	 * Add refundable charge meta amounts
	 * 
	 * @param colRefundableChargeMetaAmountsHolder
	 * @param colRefundableChargeMetaAmountsSource
	 */
	public void addRefundableChargeMetaAmounts(Collection<ChargeMetaTO> colRefundableChargeMetaAmountsHolder,
			Collection<ChargeMetaTO> colRefundableChargeMetaAmountsSource) {
		this.getRefundableChargeMetaAmounts().addAll(colRefundableChargeMetaAmountsHolder);
		this.getRefundableChargeMetaAmounts().addAll(colRefundableChargeMetaAmountsSource);
	}

	/**
	 * Add non refundable charge meta amounts
	 * 
	 * @param colNonRefundableChargeMetaAmountsHolder
	 * @param colNonRefundableChargeMetaAmountsSource
	 */
	public void addNonRefundableChargeMetaAmounts(Collection<ChargeMetaTO> colNonRefundableChargeMetaAmountsHolder,
			Collection<ChargeMetaTO> colNonRefundableChargeMetaAmountsSource) {
		this.getNonRefundableChargeMetaAmounts().addAll(colNonRefundableChargeMetaAmountsHolder);
		this.getNonRefundableChargeMetaAmounts().addAll(colNonRefundableChargeMetaAmountsSource);
	}

	public void addExtraFeeChargeMetaAmounts(Collection<ChargeMetaTO> colExtraFeeChargeMetaAmountsHolder,
			Collection<ChargeMetaTO> colExtraFeeChargeMetaAmountsSource) {
		this.getExtraFeeChargeMetaAmounts().addAll(colExtraFeeChargeMetaAmountsHolder);
		this.getExtraFeeChargeMetaAmounts().addAll(colExtraFeeChargeMetaAmountsSource);
	}

	/**
	 * Add actual charge meta amounts
	 * 
	 * @param colActualChargeMetaAmountsHolder
	 * @param colActualChargeMetaAmountsSource
	 */
	public void addActualChargeMetaAmounts(Collection<ChargeMetaTO> colActualChargeMetaAmountsHolder,
			Collection<ChargeMetaTO> colActualChargeMetaAmountsSource) {
		this.getActualChargeMetaAmounts().addAll(colActualChargeMetaAmountsHolder);
		this.getActualChargeMetaAmounts().addAll(colActualChargeMetaAmountsSource);
	}

	/**
	 * @param paymentInfo
	 *            The paymentInfo to set.
	 */
	public void setPaymentInfo(Collection<ReservationTnx> paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	/**
	 * Is payment made or not
	 * 
	 * @return
	 */
	public boolean isPaymentMade() {
		if (this.getPaymentInfo().size() == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * For DEBUG purposes.
	 * 
	 * @return
	 */
	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = System.getProperty("line.separator");

		summary.append("PNR Pax Charges Summary" + nl);
		summary.append("PNR						= " + getPnr() + nl);
		summary.append("PNR Pax Id				= " + getPnrPaxId() + nl);
		summary.append("Pax Type				= " + (getPaxType()) + nl);
		summary.append("Actual Amounts [Fare, Tax, Sur, Cnx, Mod, Adj, Total] 		=" + getActualAmounts()[0] + ","
				+ getActualAmounts()[1] + "," + getActualAmounts()[2] + "," + getActualAmounts()[3] + "," + getActualAmounts()[4]
				+ "," + getTotalActualAmount() + nl);
		summary.append("Refundable Amounts [Fare, Tax, Sur, Cnx, Mod, Adj, Total] 	=" + getRefundableAmounts()[0] + ","
				+ getRefundableAmounts()[1] + "," + getRefundableAmounts()[2] + "," + getRefundableAmounts()[3] + ","
				+ getRefundableAmounts()[4] + "," + getTotalRefundableAmount() + nl);
		summary.append("Cancellation Charge [Actual, Identified] 	= " + getActualCancellationAmount() + ","
				+ getIdentifiedCancellationAmount() + nl);
		summary.append("Modification Charge [Actual, Identified] 	= " + getActualModificationAmount() + ","
				+ getIdentifiedModificationAmount() + nl);
		summary.append("Balance Amount			= " + getAvailableBalance() + nl);

		BigDecimal totalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (isPaymentMade()) {
			summary.append("Payment info 			= " + nl);
			for (Iterator<ReservationTnx> txnIt = getPaymentInfo().iterator(); txnIt.hasNext();) {
				ReservationTnx txn = (ReservationTnx) txnIt.next();
				summary.append(txn.getAmount() + ", " + ReservationTnxNominalCode.getDescription(txn.getNominalCode()) + nl);
				totalPayment = AccelAeroCalculator.add(totalPayment, txn.getAmount());
			}
		}
		summary.append("Total Payment Amount 		= " + totalPayment + nl);

		return summary;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * returns whether this dto is belongs to parent
	 * 
	 * @return
	 */
	public Boolean isParent() {
		return parent;
	}

	/**
	 * sets whether this dto is belongs to parent
	 * 
	 * @return
	 */
	public void setParent(Boolean parent) {
		this.parent = parent;
	}

	/**
	 * @return the actualChargeMetaAmounts
	 */
	public Collection<ChargeMetaTO> getActualChargeMetaAmounts() {
		return actualChargeMetaAmounts;
	}

	/**
	 * @param actualChargeMetaAmounts
	 *            the actualChargeMetaAmounts to set
	 */
	public void setActualChargeMetaAmounts(Collection<ChargeMetaTO> actualChargeMetaAmounts) {
		this.actualChargeMetaAmounts = actualChargeMetaAmounts;
	}

	/**
	 * @return the refundableChargeMetaAmounts
	 */
	public Collection<ChargeMetaTO> getRefundableChargeMetaAmounts() {
		return refundableChargeMetaAmounts;
	}

	/**
	 * @param refundableChargeMetaAmounts
	 *            the refundableChargeMetaAmounts to set
	 */
	public void setRefundableChargeMetaAmounts(Collection<ChargeMetaTO> refundableChargeMetaAmounts) {
		this.refundableChargeMetaAmounts = refundableChargeMetaAmounts;
	}

	/**
	 * @return the nonRefundableChargeMetaAmounts
	 */
	public Collection<ChargeMetaTO> getNonRefundableChargeMetaAmounts() {
		return nonRefundableChargeMetaAmounts;
	}

	/**
	 * @param nonRefundableChargeMetaAmounts
	 *            the nonRefundableChargeMetaAmounts to set
	 */
	public void setNonRefundableChargeMetaAmounts(Collection<ChargeMetaTO> nonRefundableChargeMetaAmounts) {
		this.nonRefundableChargeMetaAmounts = nonRefundableChargeMetaAmounts;
	}

	/**
	 * @return the cnxChargeDetailTO
	 */
	public PnrChargeDetailTO getCnxChargeDetailTO() {
		return cnxChargeDetailTO;
	}

	/**
	 * @param cnxChargeDetailTO
	 *            the cnxChargeDetailTO to set
	 */
	public void setCnxChargeDetailTO(PnrChargeDetailTO cnxChargeDetailTO) {
		this.cnxChargeDetailTO = cnxChargeDetailTO;
	}

	/**
	 * @return the modChargeDetailTO
	 */
	public PnrChargeDetailTO getModChargeDetailTO() {
		return modChargeDetailTO;
	}

	/**
	 * @param modChargeDetailTO
	 *            the modChargeDetailTO to set
	 */
	public void setModChargeDetailTO(PnrChargeDetailTO modChargeDetailTO) {
		this.modChargeDetailTO = modChargeDetailTO;
	}

	@Override
	public String toString() {
		return "PnrChargesDTO ["
				+ (pnr != null ? "pnr=" + pnr + ", " : "")
				+ (pnrPaxId != null ? "pnrPaxId=" + pnrPaxId + ", " : "")
				+ (paxType != null ? "paxType=" + paxType + ", " : "")
				+ (refundableAmounts != null ? "refundableAmounts=" + Arrays.toString(refundableAmounts) + ", " : "")
				+ (refundableChargeMetaAmounts != null ? "refundableChargeMetaAmounts=" + refundableChargeMetaAmounts + ", " : "")
				+ (nonRefundableChargeMetaAmounts != null ? "nonRefundableChargeMetaAmounts=" + nonRefundableChargeMetaAmounts
						+ ", " : "")
				+ (actualAmounts != null ? "actualAmounts=" + Arrays.toString(actualAmounts) + ", " : "")
				+ (actualChargeMetaAmounts != null ? "actualChargeMetaAmounts=" + actualChargeMetaAmounts + ", " : "")
				+ (actualCancellationAmount != null ? "actualCancellationAmount=" + actualCancellationAmount + ", " : "")
				+ (identifiedCancellationAmount != null ? "identifiedCancellationAmount=" + identifiedCancellationAmount + ", "
						: "")
				+ (actualModificationAmount != null ? "actualModificationAmount=" + actualModificationAmount + ", " : "")
				+ (identifiedModificationAmount != null ? "identifiedModificationAmount=" + identifiedModificationAmount + ", "
						: "") + (availableBalance != null ? "availableBalance=" + availableBalance + ", " : "")
				+ (paymentInfo != null ? "paymentInfo=" + paymentInfo + ", " : "")
				+ (parent != null ? "parent=" + parent + ", " : "")
				+ (cnxChargeDetailTO != null ? "cnxChargeDetailTO=" + cnxChargeDetailTO + ", " : "")
				+ (modChargeDetailTO != null ? "modChargeDetailTO=" + modChargeDetailTO : "") + "]";
	}

	public void setModificationPenalty(BigDecimal modificationPenalty) {
		this.modificationPenalty = modificationPenalty;
	}

	public BigDecimal getModificationPenalty() {
		return modificationPenalty;
	}

	/**
	 * @return the extraFeeChargeMetaAmounts
	 */
	public Collection<ChargeMetaTO> getExtraFeeChargeMetaAmounts() {
		return extraFeeChargeMetaAmounts;
	}

	/**
	 * @param extraFeeChargeMetaAmounts
	 *            the extraFeeChargeMetaAmounts to set
	 */
	public void setExtraFeeChargeMetaAmounts(Collection<ChargeMetaTO> extraFeeChargeMetaAmounts) {
		this.extraFeeChargeMetaAmounts = extraFeeChargeMetaAmounts;
	}

	public BigDecimal getIdentifiedExtraFeeAmount() {
		return identifiedExtraFeeAmount;
	}

	public void setIdentifiedExtraFeeAmount(BigDecimal identifiedExtraFeeAmount) {
		this.identifiedExtraFeeAmount = identifiedExtraFeeAmount;
	}

	public PnrChargeDetailTO getNameChangeChargeDetailTO() {
		return nameChangeChargeDetailTO;
	}

	public void setNameChangeChargeDetailTO(PnrChargeDetailTO nameChangeChargeDetailTO) {
		this.nameChangeChargeDetailTO = nameChangeChargeDetailTO;
	}

	public BigDecimal getNameChangeChargeAmount() {
		return nameChangeChargeAmount;
	}

	public void setNameChangeChargeAmount(BigDecimal nameChangeChargeAmount) {
		this.nameChangeChargeAmount = nameChangeChargeAmount;
	}

	public Collection<ChargeMetaTO> getServiceTaxPenaltyCharges() {
		return serviceTaxPenaltyCharges;
	}

	public void setServiceTaxPenaltyCharges(Collection<ChargeMetaTO> serviceTaxPenaltyCharges) {
		this.serviceTaxPenaltyCharges = serviceTaxPenaltyCharges;
	}

}
