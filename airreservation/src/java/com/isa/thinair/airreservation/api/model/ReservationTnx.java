/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of Reservation Transactions
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PAX_TRANSACTION"
 */
public class ReservationTnx extends Persistent implements Serializable {

	private static final long serialVersionUID = 5829613759028045053L;

	/** Holds transaction id */
	private Integer tnxId;

	/** Holds reservation passenger id */
	private String pnrPaxId;

	/** Holds transaction amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the transaction amount in payCurrency */
	private BigDecimal payCurrencyAmount;

	/** Holds the pay currency code */
	private String payCurrencyCode;

	/** Holds transaction date */
	private Date dateTime;

	/** Holds transaction type */
	private String tnxType;

	/** Holds transaction nominal code (ticket,part..etc) */
	private Integer nominalCode;

	/** Holds settlement transaction id */
	private Integer settlementTnxId;

	/** Holds sales channel code */
	private Integer salesChannelCode;

	/** Holds user id */
	private String userId;

	/** Holds customer id */
	private Integer customerId;

	/** Holds payment type */
	private String paymentType;

	/** Holds payment type */
	private String agentCode;

	/** Holds remarks **/
	private String remarks;

	/* Holds a tem value. From BL this will be loaded if needed */
	private ReservationPaxTnxBreakdown reservationPaxTnxBreakdown;

	/** Holds a read only view of ReservationPaxOndPayment collections */
	private Collection<ReservationPaxOndPayment> reservationPaxTnxOndBreakdown;

	/**
	 * Holds the external reference relating to payment . Eg loyalty payments. Note : this was used incorrectly to
	 * record lcc_unique_tnx_id and now moved to LCC_UNIQUE_TXN_ID
	 */
	private String externalReference;

	private Integer externalReferenceType;

	private String recieptNo;

	private String paymentCarrier;

	private String lccUniqueId;

	private Integer originalPaymentTnxID;

	private String firstPayment;

	private String segmentMapped = "N";

	/**
	 * For some operations system records 0 payment records, in order to identify those records this flag been used
	 */
	private String dummyPayment;

	/** Holds agent code whose credit shared with logged agent */
	private String creditSharedAgentCode;

	/**
	 * @return Returns the paymentType.
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            The paymentType to set.
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the dateTime.
	 * @hibernate.property column = "TNX_DATE"
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime
	 *            The dateTime to set.
	 */
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return Returns the nominalCode.
	 * @hibernate.property column = "NOMINAL_CODE"
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            The nominalCode to set.
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return Returns the pnrPaxId.
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public String getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(String pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the settlementTnxId.
	 * @hibernate.property column = "SETTLEMENT_TNX__ID"
	 */
	public Integer getSettlementTnxId() {
		return settlementTnxId;
	}

	/**
	 * @param settlementTnxId
	 *            The settlementTnxId to set.
	 */
	public void setSettlementTnxId(Integer settlementTnxId) {
		this.settlementTnxId = settlementTnxId;
	}

	/**
	 * @return Returns the tnxId.
	 * @hibernate.id column = "TXN_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PAX_TRANSACTION"
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return Returns the tnxType.
	 * @hibernate.property column = "DR_CR"
	 */
	public String getTnxType() {
		return tnxType;
	}

	/**
	 * @param tnxType
	 *            The tnxType to set.
	 */
	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the salesChannelCode.
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 * 
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the recieptNo
	 * @hibernate.property column = "RECEIPT_NO"
	 */
	public String getRecieptNo() {
		return recieptNo;
	}

	/**
	 * @param recieptNo
	 *            the recieptNo to set
	 */
	public void setRecieptNo(String recieptNo) {
		this.recieptNo = recieptNo;
	}

	/**
	 * @return Returns the remarks.
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	public String getRemarksRemovingUnwantedCharacters() {
		String tmpRemarks = BeanUtils.nullHandler(getRemarks());

		if (tmpRemarks.length() > 0) {
			tmpRemarks = tmpRemarks.replace("\n", "");
		} else {
			tmpRemarks = null;
		}

		return tmpRemarks;
	}

	/**
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the payCurrencyAmount
	 * @hibernate.property column = "AMOUNT_PAYCUR"
	 */
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	/**
	 * @param payCurrencyAmount
	 *            the payCurrencyAmount to set
	 */
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	/**
	 * @return the payCurrencyCode
	 * @hibernate.property column = "PAYMENT_CURRENCY_CODE"
	 */
	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	/**
	 * @param payCurrencyCode
	 *            the payCurrencyCode to set
	 */
	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	/**
	 * @return the reservationPaxTnxBreakdown
	 */
	public ReservationPaxTnxBreakdown getReservationPaxTnxBreakdown() {
		return reservationPaxTnxBreakdown;
	}

	/**
	 * @param reservationPaxTnxBreakdown
	 *            the reservationPaxTnxBreakdown to set
	 */
	public void setReservationPaxTnxBreakdown(ReservationPaxTnxBreakdown reservationPaxTnxBreakdown) {
		this.reservationPaxTnxBreakdown = reservationPaxTnxBreakdown;
	}

	/**
	 * @return the reservationPaxTnxOndBreakdown
	 */
	@SuppressWarnings("unchecked")
	public Collection<ReservationPaxOndPayment> getReservationPaxTnxOndBreakdown() {
		if (reservationPaxTnxOndBreakdown == null) {
			reservationPaxTnxOndBreakdown = Collections.EMPTY_LIST;
		}
		return Collections.unmodifiableCollection(reservationPaxTnxOndBreakdown);
	}

	/**
	 * @param reservationPaxTnxOndBreakdown
	 *            the reservationPaxTnxOndBreakdown to set
	 */
	public void setReservationPaxTnxOndBreakdown(Collection<ReservationPaxOndPayment> reservationPaxTnxOndBreakdown) {
		this.reservationPaxTnxOndBreakdown = reservationPaxTnxOndBreakdown;
	}

	/**
	 * @return the externalReference
	 * @hibernate.property column = "EXT_REFERENCE"
	 */
	public String getExternalReference() {
		return externalReference;
	}

	/**
	 * @param externalReference
	 *            the externalReference to set
	 */
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return the externalReference
	 * @hibernate.property column = "PAYMENT_MODE_ID"
	 */
	public Integer getExternalReferenceType() {
		return externalReferenceType;
	}

	public void setExternalReferenceType(Integer externalReferenceType) {
		this.externalReferenceType = externalReferenceType;
	}

	/**
	 * @return the paymentCarrier
	 * @hibernate.property column = "PAYMENT_CARRIER_CODE"
	 */
	public String getPaymentCarrier() {
		return paymentCarrier;
	}

	/**
	 * @param paymentCarrier
	 *            the paymentCarrier to set
	 */
	public void setPaymentCarrier(String paymentCarrier) {
		this.paymentCarrier = paymentCarrier;
	}

	/**
	 * @return the lccUniqueId
	 * @hibernate.property column = "LCC_UNIQUE_TXN_ID"
	 */
	public String getLccUniqueId() {
		return lccUniqueId;
	}

	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	/**
	 * @return the originalPaymentTnxID
	 * @hibernate.property column = "PAYMENT_TXN_ID"
	 */
	public Integer getOriginalPaymentTnxID() {
		return originalPaymentTnxID;
	}

	/**
	 * @param originalPaymentTnxID
	 *            the originalPaymentTnxID to set
	 */
	public void setOriginalPaymentTnxID(Integer originalPaymentTnxID) {
		this.originalPaymentTnxID = originalPaymentTnxID;
	}

	/**
	 * @return the firstPayment
	 * @hibernate.property column = "FIRST_PAYMENT"
	 */
	public String getFirstPayment() {
		return firstPayment;
	}

	public void setFirstPayment(String firstPayment) {
		this.firstPayment = firstPayment;
	}

	/**
	 * Override the equals
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationTnx) {
			ReservationTnx castObject = (ReservationTnx) o;
			if (castObject.getTnxId() == null || this.getTnxId() == null) {
				return false;
			} else if (castObject.getTnxId().intValue() == this.getTnxId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * Override by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getTnxId()).toHashCode();
	}

	/**
	 * @return the dummyPaymentRecord
	 * @hibernate.property column = "DUMMY_PAYMENT"
	 */
	public String getDummyPayment() {
		return dummyPayment;
	}

	public void setDummyPayment(String dummyPaymentRecord) {
		this.dummyPayment = dummyPaymentRecord;
	}

	/**
	 * @hibernate.property column = "SEG_MAPPED"
	 */
	public String getSegmentMapped() {
		return segmentMapped;
	}

	public void setSegmentMapped(String segmentMapped) {
		this.segmentMapped = segmentMapped;
	}

	/**
	 * @return
	 * @hibernate.property column = "DEBIT_CREDIT_AGENT"
	 */
	public String getCreditSharedAgentCode() {
		return creditSharedAgentCode;
	}

	/**
	 * 
	 * @param creditSharedAgentCode
	 */
	public void setCreditSharedAgentCode(String creditSharedAgentCode) {
		this.creditSharedAgentCode = creditSharedAgentCode;
	}

}