package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumericSpace;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class SSR extends EDISegment {

	private String e9962;
	private String e4405;
	private String e6060;
	private String e9906;
	private String e7365_1;
	private String e7365_2;
	private String e3225_1;
	private String e3225_2;
	private List<String> e4440List = new ArrayList<String>();
	private List<CompoundDataElement> C332List = new ArrayList<CompoundDataElement>();

	public SSR() {
		super(EDISegmentTag.SSR);
	}

	public void setSpecialRequirementType(String e9962) {
		this.e9962 = e9962;
	}

	public void setStatus(String e4405) {
		this.e4405 = e4405;
	}

	public void setQuantity(String e6060) {
		this.e6060 = e6060;
	}

	public void setCompanyIdentification(String e9906) {
		this.e9906 = e9906;
	}

	public void setBoardCity(String e3225_1) {
		this.e3225_1 = e3225_1;
	}

	public void setOffCity(String e3225_2) {
		this.e3225_2 = e3225_2;
	}

	public void addFreeText(String e4440) {
		this.e4440List.add(e4440);
	}
	
	public void setProcessingIndicatorOne(String e7365_1) {
		this.e7365_1 = e7365_1;
	}

	public void setProcessingIndicatorTwo(String e7365_2) {
		this.e7365_2 = e7365_2;
	}

	public void setSpecialRequirementData(String e9960, String e6411, String e9944, String e9825) {
		CompoundDataElement C332 = new CompoundDataElement("C332", DE_STATUS.C);
		C332.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9960, e9960));
		C332.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E6411, e6411));
		C332.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9944, e9944));
		C332.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9825, e9825));
		C332List.add(C332);
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C334 = new CompoundDataElement("C334", DE_STATUS.M);
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9962, e9962));
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E4405, e4405));
		C334.addBasicDataelement(new Numeric(EDI_ELEMENT.E6060, e6060));
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9906, e9906));
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E7365, e7365_1));
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E7365, e7365_2));
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3225, e3225_1));
		C334.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3225, e3225_2));
		for (String freeText : e4440List) {
			C334.addBasicDataelement(new AlphaNumericSpace(EDI_ELEMENT.E4440, freeText));
		}

		addEDIDataElement(C334);

		for (CompoundDataElement element : C332List) {
			addEDIDataElement(element);
		}

		return this;
	}

}
