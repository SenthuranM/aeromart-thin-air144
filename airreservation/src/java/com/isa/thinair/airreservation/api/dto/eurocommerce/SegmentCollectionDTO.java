package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SegmentCollectionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5772286200598671614L;
	private List<SegmentDTO> segmentDTOs;

	public List<SegmentDTO> getSegmentDTOs() {
		if (segmentDTOs == null) {
			segmentDTOs = new ArrayList<SegmentDTO>();
		}
		return segmentDTOs;
	}

	public void setSegmentDTOs(List<SegmentDTO> segmentDTOs) {
		this.segmentDTOs = segmentDTOs;
	}

	public void addSegmentDTO(SegmentDTO segmentDTO) {
		getSegmentDTOs().add(segmentDTO);
	}

}
