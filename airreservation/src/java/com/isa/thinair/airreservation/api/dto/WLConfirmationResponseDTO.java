package com.isa.thinair.airreservation.api.dto;

public class WLConfirmationResponseDTO {
	
	private Integer confirmedAdultCount;
	private Integer confirmedInfantCount;
	
	public Integer getConfirmedAdultCount() {
		return confirmedAdultCount;
	}
	public void setConfirmedAdultCount(Integer confirmedAdultCount) {
		this.confirmedAdultCount = confirmedAdultCount;
	}
	public Integer getConfirmedInfantCount() {
		return confirmedInfantCount;
	}
	public void setConfirmedInfantCount(Integer confirmedInfantCount) {
		this.confirmedInfantCount = confirmedInfantCount;
	}	

}
