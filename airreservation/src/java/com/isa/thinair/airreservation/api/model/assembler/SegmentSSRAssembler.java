package com.isa.thinair.airreservation.api.model.assembler;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class SegmentSSRAssembler implements AncillaryAssembler, Serializable {

	private static final long serialVersionUID = 1L;

	private Integer paxSeguence = null;
	// Map<segmentSequence, Collection<PaxSegmentSSRDTO>>
	private Map<Integer, Collection<PaxSegmentSSRDTO>> newSegmentSSRDTOMap;
	// Map<paxSegmentSSRId, PaxSegmentSSRDTO>
	private Map<Integer, PaxSegmentSSRDTO> updatedSegmentSSRDTOMap;

	// Map<paxSegmentSSRId, paxSegmentSSRId>
	private Map<Integer, Integer> canceledSegmentSSRIdMap;

	private Map<Integer, String> canceledSegemntSSRCodeMap;

	private IPassenger passengerAssembler;

	private PaxAdjAssembler adjAssembler;

	public SegmentSSRAssembler() {
		newSegmentSSRDTOMap = new HashMap<Integer, Collection<PaxSegmentSSRDTO>>();
		updatedSegmentSSRDTOMap = new HashMap<Integer, PaxSegmentSSRDTO>();
		canceledSegmentSSRIdMap = new HashMap<Integer, Integer>();
		canceledSegemntSSRCodeMap = new HashMap<Integer, String>();
	}

	public SegmentSSRAssembler(PaxAdjAssembler adjAssembler) {
		this();
		this.adjAssembler = adjAssembler;
	}

	public Map<Integer, Collection<PaxSegmentSSRDTO>> getNewSegmentSSRDTOMap() {
		return newSegmentSSRDTOMap;
	}

	public Map<Integer, PaxSegmentSSRDTO> getUpdatedSegmentSSRDTOMap() {
		return this.updatedSegmentSSRDTOMap;
	}

	public void addPaxSegmentSSR(Integer segmentSequence, Integer ssrId, String ssrText, String ssrCode, String description,
			String extRef) {
		addPaxSegmentSSR(segmentSequence, ssrId, ssrText, null, new BigDecimal(0), ssrCode, description, extRef, null, null, null,
				null, null, null);
	}

	public void addPaxSegmentSSR(Integer segmentSequence, Integer ssrId, String ssrText, String extRef) {
		addPaxSegmentSSR(segmentSequence, ssrId, ssrText, null, new BigDecimal(0), null, null, extRef, null, null, null, null,
				null, null);
	}

	public void addPaxSegmentSSR(Integer segmentSequence, Integer ssrId, String ssrText) {
		addPaxSegmentSSR(segmentSequence, ssrId, ssrText, null, new BigDecimal(0), null, null, null, null, null, null, null, null,
				null);
	}

	public void addPaxSegmentSSR(Integer segmentSequence, Integer ssrId, String ssrText, Integer contextId,
			BigDecimal chargeAmount, String ssrCode, String description, String extRef, String airportCode, String applyOn,
			Integer autoCnxId, String trnsDate, String trnsAddress, String trnsContact) {
		PaxSegmentSSRDTO paxSegmentSSRDTO = new PaxSegmentSSRDTO();

		if (segmentSequence == null) {
			segmentSequence = new Integer(-1);
		}

		paxSegmentSSRDTO.setSegmentSequence(segmentSequence);
		paxSegmentSSRDTO.setSSRId(ssrId);
		paxSegmentSSRDTO.setSSRText(ssrText);
		paxSegmentSSRDTO.setContextId(contextId);
		paxSegmentSSRDTO.setChargeAmount(chargeAmount);
		paxSegmentSSRDTO.setDescription(description);
		paxSegmentSSRDTO.setSsrCode(ssrCode);
		paxSegmentSSRDTO.setExternalReference(extRef);
		paxSegmentSSRDTO.setAirportCode(airportCode);
		paxSegmentSSRDTO.setApplyOn(applyOn);
		if (chargeAmount != null
				&& AccelAeroCalculator.isGreaterThan(chargeAmount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			paxSegmentSSRDTO.setAutoCancellationId(autoCnxId);
		}
		paxSegmentSSRDTO.setTransferDate(trnsDate);
		paxSegmentSSRDTO.setTransferAddress(trnsAddress);
		paxSegmentSSRDTO.setTransferContactNo(trnsContact);

		Collection<PaxSegmentSSRDTO> segmentSSRs = (Collection<PaxSegmentSSRDTO>) newSegmentSSRDTOMap.get(segmentSequence);

		if (segmentSSRs == null) {
			segmentSSRs = new ArrayList<PaxSegmentSSRDTO>();
			newSegmentSSRDTOMap.put(segmentSequence, segmentSSRs);
		}

		segmentSSRs.add(paxSegmentSSRDTO);

		if (adjAssembler != null) {
			// adjAssembler.addCharge(pnrPaxId, amount);
			// FIXME
		}
	}

	public void addUpdatedPaxSegmentSSR(Integer paxSegmentSSRId, Integer ssrId, String ssrText) {
		PaxSegmentSSRDTO paxSegmentSSRDTO = new PaxSegmentSSRDTO();

		if (paxSegmentSSRId == null) {
			paxSegmentSSRId = new Integer(-1);
		}

		paxSegmentSSRDTO.setPaxSegmentSSRId(paxSegmentSSRId);
		paxSegmentSSRDTO.setSSRId(ssrId);
		paxSegmentSSRDTO.setSSRText(ssrText);

		updatedSegmentSSRDTOMap.put(paxSegmentSSRId, paxSegmentSSRDTO);

	}

	public Map<Integer, Integer> getCanceledSegmentSSRIdMap() {
		return this.canceledSegmentSSRIdMap;
	}

	public void addCanceledPaxSegmentSSR(Integer paxSegmentSSRId) {
		this.canceledSegmentSSRIdMap.put(paxSegmentSSRId, paxSegmentSSRId);
	}

	public Map<Integer, String> canceledSegemntSSRCodeMap() {
		return this.canceledSegemntSSRCodeMap;
	}

	public void addCanceledSegemntSSRCodeMap(Integer paxSegmentSSRId, String paxSegmentSSRCode) {
		this.canceledSegemntSSRCodeMap.put(paxSegmentSSRId, paxSegmentSSRCode);
	}

	public IPassenger getPassengerAssembler() {
		return this.passengerAssembler;
	}

	public void setPassengerAssembler(IPassenger passengerAssembler) {
		this.passengerAssembler = passengerAssembler;
	}

	public Integer getPaxSeguence() {
		return this.paxSeguence;
	}

	public void setPaxSeguence(Integer paxSeguence) {
		this.paxSeguence = paxSeguence;
	}

}
