/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold cash payment data transfer information
 * 
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CashPaymentInfo implements Serializable, PaymentInfo, CustomerPayment {

	private static final long serialVersionUID = 6306663275024965439L;

	/** Hold total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	private PaymentReferenceTO paymentReferanceTO;

	/** Hold the payment carrier code */
	private String payCarrier;

	/** Holds the lcc unique Id for interline or dry booking */
	private String lccUniqueId;
	
	/**
	 * Holds original paymentTnxId when the operation is a refund
	 */
	private Integer paymentTnxId;
	

	/**
	 * @return the paymentReferanceTO
	 */
	public PaymentReferenceTO getPaymentReferanceTO() {
		return paymentReferanceTO;
	}

	/**
	 * @param paymentReferanceTO
	 *            the paymentReferanceTO to set
	 */
	public void setPaymentReferanceTO(PaymentReferenceTO paymentReferanceTO) {
		this.paymentReferanceTO = paymentReferanceTO;
	}

	/**
	 * @return Returns the totalAmount.
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            The totalAmount to set.
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the payCarrier
	 */
	public String getPayCarrier() {
		return payCarrier;
	}

	/**
	 * @param payCarrier
	 *            the payCarrier to set
	 */
	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;
	}

	/**
	 * @return the lccUniqueId
	 */
	public String getLccUniqueId() {
		return lccUniqueId;
	}

	/**
	 * @param lccUniqueId
	 *            the lccUniqueId to set
	 */
	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}
	
	/**
	 * @param paymentTnxId
	 */
	public Integer getPaymentTnxId() {
		return this.paymentTnxId;
	}

	
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;

	}
	
}
