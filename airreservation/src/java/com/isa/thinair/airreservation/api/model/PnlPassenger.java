/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * To keep track of pnr passenger information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNL_PASSENGER"
 */
public class PnlPassenger implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5604240665737297389L;

	/** Holds the pnl passenger Id */
	private Integer pnlPassengerId;

	/** Holds the pnr pax id */
	private Integer pnrPaxId;

	/** Holds the pnr seg id */
	private Integer pnrSegId;

	/** Holds the pnr */
	private String pnr;

	/** Holds the passenger title */
	private String title;

	/** Holds the passenger first name */
	private String firstName;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the passenger type */
	private String paxType;

	/** Holds the pax status */
	private String status;

	/** Holds the ssr code */
	private String ssrCode;

	/** Holds the ssr text */
	private String ssrText;

	/** Holds the pnl status */
	private String pnlStatus;

	/** Holds the group id */
	private String groupId;

	/** Holds the Booking class code */
	private String bookingCode;

	/** Holds the Booking class type */
	private String bcType;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/** Holds the logical cabin class code */
	private String logicalCCCode;
	
	private Integer flightSegId;

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the pnrPaxId
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrSegId
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return the pnlStatus
	 * @hibernate.property column = "PNL_STAT"
	 */
	public String getPnlStatus() {
		return pnlStatus;
	}

	/**
	 * @param pnlStatus
	 *            the pnlStatus to set
	 */
	public void setPnlStatus(String pnlStatus) {
		this.pnlStatus = pnlStatus;
	}

	/**
	 * @return the ssrCode
	 * @hibernate.property column = "SSR_CODE"
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            the ssrCode to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return the ssrText
	 * @hibernate.property column = "SSR_TEXT"
	 */
	public String getSsrText() {
		return ssrText;
	}

	/**
	 * @param ssrText
	 *            the ssrText to set
	 */
	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the pnlPassengerId.
	 * @hibernate.id column = "PNL_PAX_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNL_PASSENGER"
	 */
	public Integer getPnlPassengerId() {
		return pnlPassengerId;
	}

	/**
	 * @param pnlPassengerId
	 *            The pnlPassengerId to set.
	 */
	public void setPnlPassengerId(Integer pnlPassengerId) {
		this.pnlPassengerId = pnlPassengerId;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "GROUP_ID"
	 */
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return Returns the bookingCode.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            the bookingCode to set
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return Returns the bcType.
	 * @hibernate.property column = "BC_TYPE"
	 */
	public String getBcType() {
		return bcType;
	}

	/**
	 * @param bcType
	 *            the bcType to set
	 */
	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the logicalCCCode.
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return Returns the flightSegId.
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

}
