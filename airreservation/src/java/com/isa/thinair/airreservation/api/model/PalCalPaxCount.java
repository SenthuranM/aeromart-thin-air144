package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * To keep track of PnlAdlHistory
 * 
 * @author Isuru
 * @since 1.0
 * @hibernate.class table = "t_pal_cal_pax_count"
 */
public class PalCalPaxCount implements Serializable{

	private static final long serialVersionUID = 2618994000219175169L;

	private Integer palCalPaxCountId;

	private String cabinClassCode;

	private Integer paxCount;

	private String lastGroupCode;

	private PalCalHistory palCalHistory;

	private String logicalCabinClassCode;
	
	private String bookingCode;

	/**
	 * returns the acccId
	 * 
	 * @return Returns the acccId.
	 * @hibernate.id column = "PAL_CAL_PAX_COUNT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "s_pal_cal_pax_count"
	 */
	public Integer getPalCalPaxCountId() {
		return palCalPaxCountId;
	}

	/**
	 * @param palCalPaxCountId
	 * 
	 */
	public void setPalCalPaxCountId(Integer id) {
		this.palCalPaxCountId = id;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @hibernate.many-to-one column="HIS_ID" class="com.isa.thinair.airreservation.api.model.PalCalHistory"
	 * @return Returns the reservation.
	 */
	public PalCalHistory getPalCalHistory() {
		return palCalHistory;
	}

	/**
	 * @param palCalHistory
	 *            The palCalHistory to set.
	 */
	public void setPalCalHistory(PalCalHistory pnlAdlHistory) {
		this.palCalHistory = pnlAdlHistory;
	}

	/**
	 * @return Returns the paxCount.
	 * @hibernate.property column = "PAX_COUNT"
	 */
	public Integer getPaxCount() {
		return paxCount;
	}

	/**
	 * @param paxCount
	 *            The paxCount to set.
	 */
	public void setPaxCount(Integer paxCount) {
		this.paxCount = paxCount;
	}

	/**
	 * @return Returns the paxCount.
	 * @hibernate.property column = "LAST_GROUP_ID"
	 */
	public String getLastGroupCode() {
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

	/**
	 * @return Returns the logicalCabinClassCode.
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 *            The logicalCabinClassCode to set.
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the bookingCode.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}
	
	

	
}
