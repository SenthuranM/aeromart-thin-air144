package com.isa.thinair.airreservation.api.dto;

/**
 * @author Indika Athauda
 */
public class CarrierDTO {

	private String carrierCode;
	private String description;
	private boolean isDrySellEnabled;
	private boolean isBlockSpaceEnabled;

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the isDrySellEnabled
	 */
	public boolean isDrySellEnabled() {
		return isDrySellEnabled;
	}

	/**
	 * @param isDrySellEnabled
	 *            the isDrySellEnabled to set
	 */
	public void setDrySellEnabled(boolean isDrySellEnabled) {
		this.isDrySellEnabled = isDrySellEnabled;
	}

	/**
	 * @return the isBlockSpaceEnabled
	 */
	public boolean isBlockSpaceEnabled() {
		return isBlockSpaceEnabled;
	}

	/**
	 * @param isBlockSpaceEnabled
	 *            the isBlockSpaceEnabled to set
	 */
	public void setBlockSpaceEnabled(boolean isBlockSpaceEnabled) {
		this.isBlockSpaceEnabled = isBlockSpaceEnabled;
	}

}
