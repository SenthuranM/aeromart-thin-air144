package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

/**
 * @author Byorn
 */
public class PNLADLLogDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6944099439841671746L;

	private String messageType = null; // PNL / ADL

	private String generatedMessageType = null; // PNL / ADL

	private Date processStartedDate = null; // PNL/ADL
											// generation
											// started
											// date
											// time.

	private Date historyRecordInsertedTime = null; // Time that
													// the
													// history
													// records
													// inserted
													// for the
													// sita
													// addresses.

	private String flightID = null;

	private String departureAirPort = null;

	private String flightNumber = null;

	private Date departureDate = null; // Departure
										// date time

	@SuppressWarnings("rawtypes")
	private Collection obtainedSitaAddresses = null; // Sita
														// addresses
														// retrieved
														// (collection
														// of
														// strings)

	private Date sitaAddressesObtainedTime = null; // Sita
													// addresses
													// retrieved
													// date time

	private Date passengerListobtainedTime = null; // Passenger
													// list
													// obtained
													// date time

	private String folderGenPNL = null; // Folder
										// name of
										// Gen PNL

	private Date genFolderTransferTime = null; // Date time
												// of the
												// file
												// transfered
												// to the
												// GENPNL
												// folder

	@SuppressWarnings("rawtypes")
	private Collection pnlADLEmailTxStatusDTOs = null; // PNLADLEmailTxStatusDTO

	private String folderSendPNL = null; // Folder
											// name of
											// Sent PNL

	private Date sendFolderTransferTime = null; // Date time
												// of the
												// file
												// transfered
												// to the
												// SENTPNL
												// folder

	private String exceptionDescription = null;

	private StackTraceElement[] stackTraceElements = null; // Occurred
															// error
															// stack of
															// the
															// exception

	private Date exceptionOccurredTime = null; // Date and
												// time the
												// exception
												// occurred

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm:ss";

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

	private SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm:ss");

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getGeneratedMessageType() {
		return generatedMessageType;
	}

	public void setGeneratedMessageType(String generatedMessageType) {
		this.generatedMessageType = generatedMessageType;
	}

	public boolean isGeneratedMessageTypeDiffer() {
		return !generatedMessageType.equals(messageType);
	}

	public String getProcessStartedDate() {
		if (processStartedDate != null)
			return simpleDateFormat.format(processStartedDate);
		else
			return null;
	}

	public void setProcessStartedDate(Date processStartedDate) {
		this.processStartedDate = processStartedDate;
	}

	public String getDepartureAirPort() {
		return departureAirPort;
	}

	public void setDepartureAirPort(String departureAirPort) {
		this.departureAirPort = departureAirPort;
	}

	public String getDepartureDate() {
		if (departureDate != null)
			return simpleDateFormat.format(departureDate);
		else
			return null;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getExceptionOccurredTime() {
		if (exceptionOccurredTime != null)
			return simpleTimeFormat.format(exceptionOccurredTime);
		else
			return null;
	}

	public void setExceptionOccurredTime(Date exceptionOccurredTime) {
		this.exceptionOccurredTime = exceptionOccurredTime;
	}

	public String getFlightID() {
		return flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFolderGenPNL() {
		return folderGenPNL;
	}

	public void setFolderGenPNL(String folderGenPNL) {
		this.folderGenPNL = folderGenPNL;
	}

	public String getFolderSendPNL() {
		return folderSendPNL;
	}

	public void setFolderSendPNL(String folderSendPNL) {
		this.folderSendPNL = folderSendPNL;
	}

	public String getGenFolderTransferTime() {
		if (genFolderTransferTime != null)
			return simpleTimeFormat.format(genFolderTransferTime);
		else
			return null;
	}

	public void setGenFolderTransferTime(Date genFolderTransferTime) {
		this.genFolderTransferTime = genFolderTransferTime;
	}

	public String getHistoryRecordInsertedTime() {
		if (historyRecordInsertedTime != null)
			return simpleTimeFormat.format(historyRecordInsertedTime);
		else
			return null;
	}

	public void setHistoryRecordInsertedTime(Date historyRecordInsertedTime) {
		this.historyRecordInsertedTime = historyRecordInsertedTime;
	}

	@SuppressWarnings("rawtypes")
	public Collection getObtainedSitaAddresses() {
		return obtainedSitaAddresses;
	}

	@SuppressWarnings("rawtypes")
	public void setObtainedSitaAddresses(Collection obtainedSitaAddresses) {
		this.obtainedSitaAddresses = obtainedSitaAddresses;
	}

	public String getPassengerListobtainedTime() {
		if (passengerListobtainedTime != null)
			return simpleTimeFormat.format(passengerListobtainedTime);
		else
			return null;
	}

	public void setPassengerListobtainedTime(Date passengerListobtainedTime) {
		this.passengerListobtainedTime = passengerListobtainedTime;
	}

	@SuppressWarnings("rawtypes")
	public Collection getPnlADLEmailTxStatusDTOs() {
		return pnlADLEmailTxStatusDTOs;
	}

	@SuppressWarnings("rawtypes")
	public void setPnlADLEmailTxStatusDTOs(Collection pnlADLEmailTxStatusDTOs) {
		this.pnlADLEmailTxStatusDTOs = pnlADLEmailTxStatusDTOs;
	}

	public String getSendFolderTransferTime() {
		if (sendFolderTransferTime != null)
			return simpleTimeFormat.format(sendFolderTransferTime);
		else
			return null;
	}

	public void setSendFolderTransferTime(Date sendFolderTransferTime) {
		this.sendFolderTransferTime = sendFolderTransferTime;
	}

	public String getSitaAddressesObtainedTime() {
		if (sitaAddressesObtainedTime != null)
			return simpleTimeFormat.format(sitaAddressesObtainedTime);
		else
			return null;
	}

	public void setSitaAddressesObtainedTime(Date sitaAddressesObtainedTime) {
		this.sitaAddressesObtainedTime = sitaAddressesObtainedTime;
	}

	@SuppressWarnings("rawtypes")
	public Collection getStackTraceElements() {
		return Arrays.asList(stackTraceElements);
	}

	public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}

	public String getExceptionDescription() {
		return exceptionDescription;
	}

	public void setExceptionDescription(String exceptionDescription) {
		this.exceptionDescription = exceptionDescription;
	}

}
