package com.isa.thinair.airreservation.api.utils;

import java.util.Calendar;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckRequestDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.HeaderDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.ItineraryDocumentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.JourneyDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.NameValueDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.NameValueDocumentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.PassengerDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.SegmentCollectionDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.SegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.core.util.GlobalConfig;

public class CCFraudCheckRequestDTOBuilder {

	private static final String UPDATE_RESERVATION = "UPDATE_RESERVATION";
	private static final String CCRESERVATION = "CCRESERVATION";
	private static final String PAYMENT_MECHOD_CODE = "CC";

	private CCFraudCheckRequestDTO ccFCDTO = new CCFraudCheckRequestDTO();

	private String reservationType = "CCRESERVATION";
	private String externalId;
	private String pnr;
	private CardPaymentInfo ccInfo;
	private Reservation reservation;
	private Collection<OndFareDTO> ondFareDtos;
	private CredentialsDTO credentials;
	private TrackInfoDTO trackInfo;

	private GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setCardPayInfo(CardPaymentInfo cardPayInfo) {
		this.ccInfo = cardPayInfo;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setReservation(Reservation res) {
		this.reservation = res;

	}

	public void setOndFareDtos(Collection<OndFareDTO> ondFareDtos) {
		this.ondFareDtos = ondFareDtos;
	}

	public void setCredentials(CredentialsDTO credentials) {
		this.credentials = credentials;
	}

	public void setTrackInfo(TrackInfoDTO trackInfo) {
		this.trackInfo = trackInfo;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}

	public CCFraudCheckRequestDTO getCcFraudCheckRequestDTO() {
		ccFCDTO.setPaymentmethodCode(PAYMENT_MECHOD_CODE);
		ccFCDTO.setAccountNumber(ccInfo.getNo());
		ccFCDTO.setAmount(ccInfo.getPayCurrencyDTO().getTotalPayCurrencyAmount());
		ccFCDTO.setOriginalCurrencyCode(ccInfo.getPayCurrencyDTO().getPayCurrencyCode());
		ccFCDTO.setExpirationMonth(Integer.parseInt(ccInfo.getEDate().substring(2)));
		ccFCDTO.setExpirationYear((Integer.parseInt("20".concat(ccInfo.getEDate().substring(0, 2))))); // add the 2000
																										// to the year.

		ccFCDTO.setExternalId(externalId);
		ccFCDTO.setExternalRef(pnr);

		ReservationContactInfo contactInfo = reservation.getContactInfo();

		NameValueDocumentDTO globalDoc = new NameValueDocumentDTO("Global", "Global Values");
		globalDoc.addIteamDTOs(new NameValueDTO("AccountHolderName", String.format("%s %s", contactInfo.getFirstName(),
				contactInfo.getLastName())));
		ccFCDTO.addDocumentSnapInDto(globalDoc);

		NameValueDocumentDTO ccDoc = new NameValueDocumentDTO("CC", "Provide CVV Code");
		ccDoc.addIteamDTOs(new NameValueDTO("VerificationCode", ccInfo.getSecurityCode()));// TODO verify
		ccFCDTO.addDocumentSnapInDto(ccDoc);

		NameValueDocumentDTO fraudCheckDoc = new NameValueDocumentDTO("FraudCheck", "Fraud Check Values");
		fraudCheckDoc.addIteamDTOs(new NameValueDTO("Enabled", "true"));
		fraudCheckDoc.addIteamDTOs(new NameValueDTO("EmailAddress", reservation.getContactInfo().getEmail()));
		String phoneNumber = null;
		if (contactInfo.getMobileNo() != null) {
			phoneNumber = contactInfo.getMobileNo();
		} else if (contactInfo.getPhoneNo() != null) {
			phoneNumber = contactInfo.getPhoneNo();
		} else if (contactInfo.getFax() != null) {
			phoneNumber = contactInfo.getFax();
		}
		fraudCheckDoc.addIteamDTOs(new NameValueDTO("PhoneNumber", phoneNumber));
		fraudCheckDoc.addIteamDTOs(new NameValueDTO("IPAddress", trackInfo.getIpAddress()));

		fraudCheckDoc.addIteamDTOs(new NameValueDTO("SessionID", trackInfo.getSessionId()));
		ccFCDTO.addDocumentSnapInDto(fraudCheckDoc);

		NameValueDocumentDTO avsDoc = new NameValueDocumentDTO("AVS", "AVS Data");
		avsDoc.addIteamDTOs(new NameValueDTO("Address1", contactInfo.getStreetAddress1()));
		avsDoc.addIteamDTOs(new NameValueDTO("Address2", contactInfo.getStreetAddress2()));
		avsDoc.addIteamDTOs(new NameValueDTO("City", contactInfo.getCity()));
		avsDoc.addIteamDTOs(new NameValueDTO("State", contactInfo.getState()));
		avsDoc.addIteamDTOs(new NameValueDTO("Country", contactInfo.getCountryCode()));
		ccFCDTO.addDocumentSnapInDto(avsDoc);

		ItineraryDocumentDTO itineraryDocumentDTO = new ItineraryDocumentDTO("ItineraryDetails", "");
		HeaderDTO header = new HeaderDTO(pnr, credentials.getAgentCode(), true, Calendar.getInstance().getTime(), ccInfo
				.getPayCurrencyDTO().getPayCurrencyCode());
		itineraryDocumentDTO.setHeaderDTO(header);

		for (Object paxObject : reservation.getPassengers()) {
			ReservationPax pax = (ReservationPax) paxObject;
			PassengerDTO paxDto = new PassengerDTO(pax.getTitle(), pax.getFirstName(), pax.getLastName(), "", pax.getPaxType());
			itineraryDocumentDTO.addPassengreDTO(paxDto);
		}

		if (reservationType.equals(CCRESERVATION)) {
			for (OndFareDTO ondFareDTO : ondFareDtos) {
				JourneyDTO jDto = new JourneyDTO();
				SegmentCollectionDTO segCollection = new SegmentCollectionDTO();
				for (Object valueObject : ondFareDTO.getSegmentsMap().values()) {
					FlightSegmentDTO fsDTO = (FlightSegmentDTO) valueObject;
					SegmentDTO segmentDTO = new SegmentDTO();
					segmentDTO.setFareClass("Y");// TODO fix this
					segmentDTO.setDepartureAirport(fsDTO.getFromAirport());
					segmentDTO.setArrivalAirport(fsDTO.getToAirport());
					segmentDTO.setDepartureDateTime(fsDTO.getDepartureDateTime());
					segmentDTO.setDepartureDateTimeZulu(fsDTO.getDepartureDateTimeZulu());
					segmentDTO.setArrivalDateTime(fsDTO.getArrivalDateTime());
					segmentDTO.setArrivalDateTimeZulu(fsDTO.getArrivalDateTimeZulu());
					segmentDTO.setCarrier(fsDTO.getCarrierCode());
					segmentDTO.setFlightNumber(fsDTO.getFlightNumber());
					segmentDTO.setDepartureAirportCountry((String) globalConfig.getAirportInfo(fsDTO.getFromAirport(), true)[3]);
					segCollection.addSegmentDTO(segmentDTO);
				}
				jDto.setSegmentCollectionDTO(segCollection);
				itineraryDocumentDTO.addJourneyDTO(jDto);
			}
		} else if (reservationType.equals(UPDATE_RESERVATION)) {
			JourneyDTO jDto = new JourneyDTO();
			SegmentCollectionDTO segCollection = new SegmentCollectionDTO();

			for (Object segmentDtos : reservation.getSegmentsView()) {
				ReservationSegmentDTO resSegmentDto = (ReservationSegmentDTO) segmentDtos;
				SegmentDTO segmentDTO = new SegmentDTO();
				segmentDTO.setFareClass("Y");
				segmentDTO.setDepartureAirport(resSegmentDto.getSegmentCode().split("/")[0]);
				segmentDTO.setArrivalAirport(resSegmentDto.getSegmentCode().split("/")[1]);
				segmentDTO.setDepartureDateTime(resSegmentDto.getDepartureDate());
				segmentDTO.setDepartureDateTimeZulu(resSegmentDto.getZuluDepartureDate());
				segmentDTO.setArrivalDateTime(resSegmentDto.getArrivalDate());
				segmentDTO.setArrivalDateTimeZulu(resSegmentDto.getZuluArrivalDate());
				segmentDTO.setCarrier(resSegmentDto.getCarrierCode());
				segmentDTO.setFlightNumber(resSegmentDto.getFlightNo());
				segmentDTO.setDepartureAirportCountry((String) globalConfig.getAirportInfo(
						resSegmentDto.getSegmentCode().split("/")[0], true)[3]);
				segCollection.addSegmentDTO(segmentDTO);
			}
			jDto.setSegmentCollectionDTO(segCollection);
			itineraryDocumentDTO.addJourneyDTO(jDto);
		}

		ccFCDTO.addDocumentSnapInDto(itineraryDocumentDTO);
		return ccFCDTO;
	}

}
