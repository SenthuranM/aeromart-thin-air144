package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

public class PnrFarePassenger implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pnrPaxId;
	private Integer pnrPaxFareId;
	private Integer pnrSegmentId;

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	public Integer getPnrSegmentId() {
		return pnrSegmentId;
	}

	public void setPnrSegmentId(Integer pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

}
