/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.dto;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Holds the PFS Destination specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PFSDestinationDTO {

	private String arrivalAirport;

	/** Holds a collection of PFSRecordDTO */
	private Collection<PFSRecordDTO> colPfsRecordDTO;

	/**
	 * Add PFSRecordDTO
	 * 
	 * @param pfsRecordDTO
	 */
	public void addPFSRecordDTO(PFSRecordDTO pfsRecordDTO) {
		if (this.getColPfsRecordDTO() == null) {
			this.setColPfsRecordDTO(new ArrayList<PFSRecordDTO>());
		}

		this.getColPfsRecordDTO().add(pfsRecordDTO);
	}

	/**
	 * @return Returns the arrivalAirport.
	 */
	public String getArrivalAirport() {
		return arrivalAirport;
	}

	/**
	 * @param arrivalAirport
	 *            The arrivalAirport to set.
	 */
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	/**
	 * @return Returns the colPfsRecordDTO.
	 */
	public Collection<PFSRecordDTO> getColPfsRecordDTO() {
		return colPfsRecordDTO;
	}

	/**
	 * @param colPfsRecordDTO
	 *            The colPfsRecordDTO to set.
	 */
	private void setColPfsRecordDTO(Collection<PFSRecordDTO> colPfsRecordDTO) {
		this.colPfsRecordDTO = colPfsRecordDTO;
	}

}
