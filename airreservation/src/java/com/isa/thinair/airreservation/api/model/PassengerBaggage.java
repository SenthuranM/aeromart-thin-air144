package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author mano
 * @hibernate.class table = "BG_T_PNR_PAX_SEG_BAGGAGE"
 */
public class PassengerBaggage extends Persistent {

	private static final long serialVersionUID = 8824267270075483897L;

	private Integer paxBaggageId;

	private Integer pnrPaxId;

	private Integer pnrSegmentId;

	private Integer baggageId;

	private String status;

	private Integer pPFID;

	private Integer flightBgChargeId;

	private Integer originSalesChannelCode;

	private String originUserId;

	private Integer originCustomerId;

	private Date timestamp;

	private BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Integer ondChargeId;

	private String ondBaggage;

	private Integer autoCancellationId;

	/**
	 * @return the paxBaggageId
	 * @hibernate.id column = "PNR_PAX_SEG_BAGGAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="BG_S_PNR_PAX_SEG_BAGGAGE"
	 */
	public Integer getPaxBaggageId() {
		return paxBaggageId;
	}

	/**
	 * @param paxBaggageId
	 *            the paxBaggageId to set
	 */
	public void setPaxBaggageId(Integer paxBaggageId) {
		this.paxBaggageId = paxBaggageId;
	}

	/**
	 * @return the pnrPaxId
	 * @hibernate.property column = "pnr_pax_id"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrSegmentId
	 * @hibernate.property column = "pnr_seg_id"
	 */
	public Integer getPnrSegmentId() {
		return pnrSegmentId;
	}

	/**
	 * @param pnrSegmentId
	 *            the pnrSegmentId to set
	 */
	public void setPnrSegmentId(Integer pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	/**
	 * @return the baggageId
	 * @hibernate.property column = "BAGGAGE_ID"
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param baggageId
	 *            the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the pPFID
	 * @hibernate.property column = "ppf_id"
	 */
	public Integer getpPFID() {
		return pPFID;
	}

	/**
	 * @param pPFID
	 *            the pPFID to set
	 */
	public void setpPFID(Integer pPFID) {
		this.pPFID = pPFID;
	}

	/**
	 * @return the flightBgChargeId
	 * @hibernate.property column = "FLT_SEG_BAGGAGE_CHARGE_ID"
	 * 
	 */
	public Integer getFlightBgChargeId() {
		return flightBgChargeId;
	}

	/**
	 * @param flightBgChargeId
	 *            the flightBgChargeId to set
	 */
	public void setFlightBgChargeId(Integer flightBgChargeId) {
		this.flightBgChargeId = flightBgChargeId;
	}

	/**
	 * @return the originSalesChannelCode
	 * @hibernate.property column = "origin_sales_channel_code"
	 */
	public Integer getOriginSalesChannelCode() {
		return originSalesChannelCode;
	}

	/**
	 * @param originSalesChannelCode
	 *            the originSalesChannelCode to set
	 */
	public void setOriginSalesChannelCode(Integer originSalesChannelCode) {
		this.originSalesChannelCode = originSalesChannelCode;
	}

	/**
	 * @return the originUserId
	 * @hibernate.property column = "origin_user_id"
	 */
	public String getOriginUserId() {
		return originUserId;
	}

	/**
	 * @param originUserId
	 *            the originUserId to set
	 */
	public void setOriginUserId(String originUserId) {
		this.originUserId = originUserId;
	}

	/**
	 * @return the originCustomerId
	 * @hibernate.property column = "origin_customer_id"
	 */
	public Integer getOriginCustomerId() {
		return originCustomerId;
	}

	/**
	 * @param originCustomerId
	 *            the originCustomerId to set
	 */
	public void setOriginCustomerId(Integer originCustomerId) {
		this.originCustomerId = originCustomerId;
	}

	/**
	 * @return the chargeAmount
	 * @hibernate.property column = "amount"
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            the chargeAmount to set
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the timestamp
	 * @hibernate.property column = "CREATED_TIME"
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the ondChargeId
	 * @hibernate.property column = "OND_BAGGAGE_CHARGE_ID"
	 */
	public Integer getOndChargeId() {
		return ondChargeId;
	}

	public void setOndChargeId(Integer ondChargeId) {
		this.ondChargeId = ondChargeId;
	}

	/**
	 * @return the ondBaggage
	 * @hibernate.property column = "IS_OND"
	 */
	public String getOndBaggage() {
		return ondBaggage;
	}

	public void setOndBaggage(String ondBaggage) {
		this.ondBaggage = ondBaggage;
	}

	/**
	 * @return
	 * @hibernate.property column = "AUTO_CANCELLATION_ID"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	public boolean isOND() {
		if (getOndBaggage() != null && getOndBaggage().equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
	}

}
