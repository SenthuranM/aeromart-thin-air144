package com.isa.thinair.airreservation.api.dto.GroupBooking;

import java.io.Serializable;
import java.util.Date;

public class GroupBookingRequestHistoryTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long historyID;
	private Long groupBookingRequestID;
	private String groupBookingDetails;
	private Integer previosuStatusID;
	private Integer newStatusID;
	private String addedBy;
	private Date addedDate;

	public Long getHistoryID() {
		return historyID;
	}

	public void setHistoryID(Long historyID) {
		this.historyID = historyID;
	}

	public Long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	public void setGroupBookingRequestID(Long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	public String getGroupBookingDetails() {
		return groupBookingDetails;
	}

	public void setGroupBookingDetails(String groupBookingDetails) {
		this.groupBookingDetails = groupBookingDetails;
	}

	public Integer getPreviosuStatusID() {
		return previosuStatusID;
	}

	public void setPreviosuStatusID(Integer previosuStatusID) {
		this.previosuStatusID = previosuStatusID;
	}

	public Integer getNewStatusID() {
		return newStatusID;
	}

	public void setNewStatusID(Integer newStatusID) {
		this.newStatusID = newStatusID;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}
}
