package com.isa.thinair.airreservation.api.service;



import java.util.Collection;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistReservation;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;


public interface BlacklistPAXBD {

	public static final String SERVICE_NAME = "ReservationService";

	/**
	 * 
	 * Saves Blacklist PAX object
	 * 
	 * @param blacklistPAXCriteriaTO
	 * @return
	 * @throws ModuleException
	 */
	public void saveBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) throws ModuleException;

	/**
	 * Searches across Blacklist PAX criteria.
	 * 
	 * @param blacklistPAXCriteriaSearchTO
	 *            Search data DTO with the necessary data set.
	 * @param start
	 *            Starting position of the query results.
	 * @param size
	 *            Size of the query result set.
	 * @return a {@link Page} with the search results enclosed.
	 * @throws ModuleException
	 *             If the underlying operation fails.
	 */
	public Page<BlacklistPAXCriteriaTO> searchBlacklistPAXCriteria(BlacklistPAXCriteriaSearchTO blacklistPAXCriteriaSearchTO,
			Integer start, Integer size) throws ModuleException;

	/**
	 * Searches across Blacklist PAX Id.
	 * 
	 * @param blacklistPAXCriteriaID
	 *            Search data DTO with the necessary data set.
	 * @return a {@link Page} with the search results enclosed.
	 */
	public boolean isBlacklistPAXAvailable(long blacklistPAXCriteriaID);

	/**
	 * 
	 * update Blacklist PAX object
	 * 
	 * @param blacklistPAXCriteriaTO
	 * @return
	 * @throws ModuleException
	 */
	public long updateBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) throws ModuleException;
	
	
	public Page<BlacklisPaxReservationTO> searchBlacklistPaxResTO(BlacklisPaxReservationTO blPaxResTOSearch, Integer start, Integer size) throws ModuleException;

	public void updateBlacklistReservation(BlacklisPaxReservationTO blacklistResTOSave) throws ModuleException;
	
	/**
	 * Searches across Blacklist PAX Rulecriteria.
	 * 
	 * @param blacklistPAXRuleSearchTO
	 *            Search data DTO with the necessary data set.
	 * @param start
	 *            Starting position of the query results.
	 * @param size
	 *            Size of the query result set.
	 * @return a {@link Page} with the search results enclosed.
	 * @throws ModuleException
	 *             If the underlying operation fails.
	 */
	public Page<BlacklistPAXRuleTO> getBlacklistPAXRules(Integer start, Integer size, BlacklistPAXRuleSearchTO blacklistPAXRuleSearchTO) throws ModuleException;
	
	/**
	 * 
	 * Saves Blacklist PAX object
	 * 
	 * @param blacklistPAXCriteriaTO
	 * @return
	 * @throws ModuleException
	 */
	public void saveBlacklistRule(BlacklistPAXRuleTO blacklistPAXRuleTO) throws ModuleException;
	
	/**
	 * 
	 * check for existing data element combinations with the given
	 * 
	 * @param strDataEle : user selected data elements as a "," separated string
	 * @return
	 * @throws ModuleException
	 */
	public boolean checkRuleAvailability(String strDataEle, Long ruleId) throws ModuleException;
	
	public List<BlacklistPAX> getBalcklistedPaxReservation(List<BlacklisPaxReservationTO> paxList, boolean getOnlyFirstElement) throws ModuleException;
	
	public void saveBlacklistedPnrPassengers(List<BlacklistReservation> blackListPnrPassengerList) throws ModuleException;
	
	public Collection<Integer> getBLackListPaxIdVsPnrPaxId(String pnr) throws ModuleException;
	
	public boolean isBlackListPaxAlreadyInReservations(Integer blacklistedPaxId)  throws ModuleException;
	
	public void deleteBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) throws ModuleException;
}
