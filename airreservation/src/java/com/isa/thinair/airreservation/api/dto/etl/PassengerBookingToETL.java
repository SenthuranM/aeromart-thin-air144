/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.etl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashain
 *
 */
public class PassengerBookingToETL {
	private String bookingClass;
	private int count;
	private String countDisplay;
	private List<ETLPaxTO> paxList = new ArrayList<ETLPaxTO>();
	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}
	/**
	 * @param bookingClass the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
	
	/**
	 * @return the paxList
	 */
	public List<ETLPaxTO> getPaxList() {
		return paxList;
	}
	/**
	 * @param paxList the paxList to set
	 */
	public void setPaxList(List<ETLPaxTO> paxList) {
		this.paxList = paxList;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the countDisplay
	 */
	public String getCountDisplay() {
		return countDisplay;
	}
	/**
	 * @param countDisplay the countDisplay to set
	 */
	public void setCountDisplay(String countDisplay) {
		this.countDisplay = countDisplay;
	}
	

	
	
}
