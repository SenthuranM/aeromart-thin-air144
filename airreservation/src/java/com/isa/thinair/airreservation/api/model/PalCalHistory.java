package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Set;

/**
 * @hibernate.class table = "T_PAL_CAL_TX_HISTORY"
 */

public class PalCalHistory implements Serializable {

	private static final long serialVersionUID = -7484981683475980177L;

	private Integer palCalTxHisId;

	private Integer flightID;

	private String airportCode;

	private String messageType;

	private Timestamp transmissionTimeStamp;

	private String transmissionStatus;

	private String sitaAddress;

	private Blob sitaMessage;

	private String errorDescription;

	private String sentMethod;

	private String email;

	private String userID;

	private Set<PalCalPaxCount> ccPaxCountSet;

	/**
	 * @return the palCalTxHisId
	 * @hibernate.id column = "PAL_CAL_TX_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PAL_CAL_TX_HISTORY"
	 */
	public Integer getPalCalTxHisId() {
		return palCalTxHisId;
	}

	/**
	 * @param palCalTxHisId
	 *            the palCalTxHisId to set
	 */
	public void setPalCalTxHisId(Integer palCalTxHisId) {
		this.palCalTxHisId = palCalTxHisId;
	}

	/**
	 * @return the flightID
	 * @hibernate.property column ="FLIGHT_ID"
	 */
	public Integer getFlightID() {
		return flightID;
	}

	/**
	 * @param flightID
	 *            the flightID to set
	 */
	public void setFlightID(Integer flightID) {
		this.flightID = flightID;
	}

	/**
	 * @return the airportCode
	 * @hibernate.property column ="AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the messageType
	 * @hibernate.property column ="MESSAGE_TYPE"
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 *            the messageType to set
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return the transmissionTimeStamp
	 * @hibernate.property column ="TRANSMISSION_TIMESTAMP"
	 */
	public Timestamp getTransmissionTimeStamp() {
		return transmissionTimeStamp;
	}

	/**
	 * @param transmissionTimeStamp
	 *            the transmissionTimeStamp to set
	 */
	public void setTransmissionTimeStamp(Timestamp transmissionTimeStamp) {
		this.transmissionTimeStamp = transmissionTimeStamp;
	}

	/**
	 * @return the transmissionStatus
	 * @hibernate.property column ="TRANSMISSION_STATUS"
	 */
	public String getTransmissionStatus() {
		return transmissionStatus;
	}

	/**
	 * @param transmissionStatus
	 *            the transmissionStatus to set
	 */
	public void setTransmissionStatus(String transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	/**
	 * @return the sitaAddress
	 * @hibernate.property column ="SITA_ADDRESS"
	 */
	public String getSitaAddress() {
		return sitaAddress;
	}

	/**
	 * @param sitaAddress
	 *            the sitaAddress to set
	 */
	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	/**
	 * @return the sitaMessage
	 * @hibernate.property column ="SENT_MESSAGE"
	 */
	public Blob getSitaMessage() {
		return sitaMessage;
	}

	/**
	 * @param sitaMessage
	 *            the sitaMessage to set
	 * 
	 */
	public void setSitaMessage(Blob sitaMessage) {
		this.sitaMessage = sitaMessage;
	}

	/**
	 * @return the errorDescription
	 * @hibernate.property column ="ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return the sentMethod
	 * @hibernate.property column ="SENT_METHOD"
	 */
	public String getSentMethod() {
		return sentMethod;
	}

	/**
	 * @param sentMethod
	 *            the sentMethod to set
	 */
	public void setSentMethod(String sentMethod) {
		this.sentMethod = sentMethod;
	}

	/**
	 * @return the email
	 * @hibernate.property column ="EMAIL_ID"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the userID
	 * @hibernate.property column ="USER_ID"
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" order-by="PAL_CAL_PAX_COUNT_ID"
	 * @hibernate.collection-key column="HIS_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.PalCalPaxCount"
	 **/
	public Set<PalCalPaxCount> getCcPaxCountSet() {
		return ccPaxCountSet;
	}

	public void setCcPaxCountSet(Set<PalCalPaxCount> ccPaxCountSet) {
		this.ccPaxCountSet = ccPaxCountSet;
	}

}