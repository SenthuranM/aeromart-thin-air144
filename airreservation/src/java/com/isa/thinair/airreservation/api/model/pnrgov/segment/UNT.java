package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.BasicDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class UNT extends EDISegment {

	private String E0074;
	private String E0062;
	
	public UNT() {
		super(EDISegmentTag.UNT);
	}

	public String getE0074() {
		return E0074;
	}

	public void setE0074(String e0074) {
		E0074 = e0074;
	}

	public String getE0062() {
		return E0062;
	}

	public void setE0062(String e0062) {
		E0062 = e0062;
	}

	@Override
	public MessageSegment build() {
		BasicDataElement V0074 = new Numeric(EDI_ELEMENT.E0074, E0074);
		BasicDataElement V0062 = new Numeric(EDI_ELEMENT.E0062, E0062);

		addEDIDataElement(V0074);
		addEDIDataElement(V0062);

		return this;
	}
}
