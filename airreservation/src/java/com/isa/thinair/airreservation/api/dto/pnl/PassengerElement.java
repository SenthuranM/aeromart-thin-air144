/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pnl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;

/**
 * @author udithad
 *
 */
public class PassengerElement {
	private Integer pnlPaxId;
	private Integer pnrPaxId;
	private Integer pnrSegId;
	private String pnr;
	private String title;
	private String firstName;
	private String lastName;
	private String eticketNumber;
	private Integer coupon;
	private String bcType;
	private String classOfService;
	private String rbdClassOfService;
	private String destinationAirportCode;
	private Date dob;
	private String paxType;
	private String nationality;
	private String foidNumber;
	private Date foidExpiry;
	private String foidIssuedCountry;
	private String foidSsrCode;
	private String placeOfBirth; 	
	private String travelDocumentType; 	
	private String visaDocNumber;	
	private String visaDocPlaceOfIssue;	
	private Date visaDocIssueDate; 	
	private String visaApplicableCountry;
	private String ccDigits;
	private String adlAction;
	private String groupId;
	private String gender;
	private List<AncillaryDTO> seats;
	private List<AncillaryDTO> meals;
	private List<AncillaryDTO> ssrs;
	private AncillaryDTO baggages;
	private PassengerInformation infant;
	private InboundConnectionDTO inboundInfo;
	private boolean standByPassenger;
	private boolean waitListedPassenger;
	private String waitListedInfo;
	private ArrayList<OnWardConnectionDTO> onwardconnectionlist;
	private String pnrStatus;
	private String pnlStatus;
	
	private String csFlightNo;	
	private String csBookingClass;	
	private Date csDepatureDateTime;	
	private String csOrginDestination;
	private boolean codeShareFlight;	
	private String externalPnr;
	private boolean isNICSentInPNLADL = false;
	public Integer getPnlPaxId() {
		return pnlPaxId;
	}
	public void setPnlPaxId(Integer pnlPaxId) {
		this.pnlPaxId = pnlPaxId;
	}
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}
	public Integer getPnrSegId() {
		return pnrSegId;
	}
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEticketNumber() {
		return eticketNumber;
	}
	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}
	public Integer getCoupon() {
		return coupon;
	}
	public void setCoupon(Integer coupon) {
		this.coupon = coupon;
	}
	public String getBcType() {
		return bcType;
	}
	public void setBcType(String bcType) {
		this.bcType = bcType;
	}
	public String getClassOfService() {
		return classOfService;
	}
	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}
	public String getRbdClassOfService() {
		return rbdClassOfService;
	}
	public void setRbdClassOfService(String rbdClassOfService) {
		this.rbdClassOfService = rbdClassOfService;
	}
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getPaxType() {
		return paxType;
	}
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getFoidNumber() {
		return foidNumber;
	}
	public void setFoidNumber(String foidNumber) {
		this.foidNumber = foidNumber;
	}
	public Date getFoidExpiry() {
		return foidExpiry;
	}
	public void setFoidExpiry(Date foidExpiry) {
		this.foidExpiry = foidExpiry;
	}
	public String getFoidIssuedCountry() {
		return foidIssuedCountry;
	}
	public void setFoidIssuedCountry(String foidIssuedCountry) {
		this.foidIssuedCountry = foidIssuedCountry;
	}
	public String getFoidSsrCode() {
		return foidSsrCode;
	}
	public void setFoidSsrCode(String foidSsrCode) {
		this.foidSsrCode = foidSsrCode;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getTravelDocumentType() {
		return travelDocumentType;
	}
	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}
	public String getVisaDocNumber() {
		return visaDocNumber;
	}
	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}
	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}
	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}
	public Date getVisaDocIssueDate() {
		return visaDocIssueDate;
	}
	public void setVisaDocIssueDate(Date visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}
	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}
	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}
	public String getCcDigits() {
		return ccDigits;
	}
	public void setCcDigits(String ccDigits) {
		this.ccDigits = ccDigits;
	}
	public String getAdlAction() {
		return adlAction;
	}
	public void setAdlAction(String adlAction) {
		this.adlAction = adlAction;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public List<AncillaryDTO> getSeats() {
		return seats;
	}
	public void setSeats(List<AncillaryDTO> seats) {
		this.seats = seats;
	}
	public List<AncillaryDTO> getMeals() {
		return meals;
	}
	public void setMeals(List<AncillaryDTO> meals) {
		this.meals = meals;
	}
	public List<AncillaryDTO> getSsrs() {
		return ssrs;
	}
	public void setSsrs(List<AncillaryDTO> ssrs) {
		this.ssrs = ssrs;
	}
	public AncillaryDTO getBaggages() {
		return baggages;
	}
	public void setBaggages(AncillaryDTO baggages) {
		this.baggages = baggages;
	}
	public PassengerInformation getInfant() {
		return infant;
	}
	public void setInfant(PassengerInformation infant) {
		this.infant = infant;
	}
	public InboundConnectionDTO getInboundInfo() {
		return inboundInfo;
	}
	public void setInboundInfo(InboundConnectionDTO inboundInfo) {
		this.inboundInfo = inboundInfo;
	}
	public boolean isStandByPassenger() {
		return standByPassenger;
	}
	public void setStandByPassenger(boolean standByPassenger) {
		this.standByPassenger = standByPassenger;
	}
	public boolean isWaitListedPassenger() {
		return waitListedPassenger;
	}
	public void setWaitListedPassenger(boolean waitListedPassenger) {
		this.waitListedPassenger = waitListedPassenger;
	}
	public String getWaitListedInfo() {
		return waitListedInfo;
	}
	public void setWaitListedInfo(String waitListedInfo) {
		this.waitListedInfo = waitListedInfo;
	}
	public ArrayList<OnWardConnectionDTO> getOnwardconnectionlist() {
		return onwardconnectionlist;
	}
	public void setOnwardconnectionlist(
			ArrayList<OnWardConnectionDTO> onwardconnectionlist) {
		this.onwardconnectionlist = onwardconnectionlist;
	}
	public String getPnrStatus() {
		return pnrStatus;
	}
	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}
	public String getPnlStatus() {
		return pnlStatus;
	}
	public void setPnlStatus(String pnlStatus) {
		this.pnlStatus = pnlStatus;
	}
	public String getCsFlightNo() {
		return csFlightNo;
	}
	public void setCsFlightNo(String csFlightNo) {
		this.csFlightNo = csFlightNo;
	}
	public String getCsBookingClass() {
		return csBookingClass;
	}
	public void setCsBookingClass(String csBookingClass) {
		this.csBookingClass = csBookingClass;
	}
	public Date getCsDepatureDateTime() {
		return csDepatureDateTime;
	}
	public void setCsDepatureDateTime(Date csDepatureDateTime) {
		this.csDepatureDateTime = csDepatureDateTime;
	}
	public String getCsOrginDestination() {
		return csOrginDestination;
	}
	public void setCsOrginDestination(String csOrginDestination) {
		this.csOrginDestination = csOrginDestination;
	}
	public boolean isCodeShareFlight() {
		return codeShareFlight;
	}
	public void setCodeShareFlight(boolean codeShareFlight) {
		this.codeShareFlight = codeShareFlight;
	}
	public String getExternalPnr() {
		return externalPnr;
	}
	public void setExternalPnr(String externalPnr) {
		this.externalPnr = externalPnr;
	}
	public boolean isNICSentInPNLADL() {
		return isNICSentInPNLADL;
	}
	public void setNICSentInPNLADL(boolean isNICSentInPNLADL) {
		this.isNICSentInPNLADL = isNICSentInPNLADL;
	}
	
	
	
}
