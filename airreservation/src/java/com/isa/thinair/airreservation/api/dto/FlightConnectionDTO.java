package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class FlightConnectionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fligthNumber;

	private Date arrivalTime;

	private Date departureTime;

	private String segmentCode;

	private String pnr;

	private Integer pnrSegId;

	private Integer journey_seq;
	
	private Date localDepartureTime;
	
	private Date localArrivalTime;
	
	private boolean inbound;
	
	public String getFligthNumber() {
		return fligthNumber;
	}

	public void setFligthNumber(String fligthNumber) {
		this.fligthNumber = fligthNumber;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getJourney_seq() {
		return journey_seq;
	}

	public void setJourney_seq(Integer journey_seq) {
		this.journey_seq = journey_seq;
	}

	public Date getLocalDepartureTime() {
		return localDepartureTime;
	}

	public void setLocalDepartureTime(Date localDepartureTime) {
		this.localDepartureTime = localDepartureTime;
	}

	public Date getLocalArrivalTime() {
		return localArrivalTime;
	}

	public void setLocalArrivalTime(Date localArrivalTime) {
		this.localArrivalTime = localArrivalTime;
	}

	public boolean isInbound() {
		return inbound;
	}

	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

}
