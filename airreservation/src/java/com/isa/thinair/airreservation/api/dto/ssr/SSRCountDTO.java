package com.isa.thinair.airreservation.api.dto.ssr;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author rumesh
 * 
 */

public class SSRCountDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	String cabinclass;
	String logicalCabinClass;
	Map<Integer, Integer> ssrCountMap;

	/**
	 * @return the cabinclass
	 */
	public String getCabinclass() {
		return cabinclass;
	}

	/**
	 * @param cabinclass
	 *            the cabinclass to set
	 */
	public void setCabinclass(String cabinclass) {
		this.cabinclass = cabinclass;
	}

	/**
	 * @return the logicalCabinClass
	 */
	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	/**
	 * @param logicalCabinClass
	 *            the logicalCabinClass to set
	 */
	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	/**
	 * @return the ssrCountMap
	 */
	public Map<Integer, Integer> getSsrCountMap() {
		if (ssrCountMap == null) {
			ssrCountMap = new HashMap<Integer, Integer>();
		}
		return ssrCountMap;
	}

	/**
	 * @param ssrCountMap
	 *            the ssrCountMap to set
	 */
	public void setSsrCountMap(Map<Integer, Integer> ssrCountMap) {
		this.ssrCountMap = ssrCountMap;
	}
}
