package com.isa.thinair.airreservation.api.dto;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;

public class TransferSegmentDTO {
	
	private FlightSegmentDTO sourceFlightSegment;
	
	private FlightSegmentDTO targetFlightSegment; 
	
	private Integer pnrSegId;

	public FlightSegmentDTO getSourceFlightSegment() {
		return sourceFlightSegment;
	}

	public FlightSegmentDTO getTargetFlightSegment() {
		return targetFlightSegment;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setSourceFlightSegment(FlightSegmentDTO sourceFlightSegment) {
		this.sourceFlightSegment = sourceFlightSegment;
	}

	public void setTargetFlightSegment(FlightSegmentDTO targetFlightSegment) {
		this.targetFlightSegment = targetFlightSegment;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}
}
