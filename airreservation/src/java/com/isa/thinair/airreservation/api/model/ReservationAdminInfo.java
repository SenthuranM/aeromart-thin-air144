/*
 * =============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
 * To keep track of Reservation Administrator information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationAdminInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Holds owner channel id */
	private Integer ownerChannelId;

	/** Holds owner agent code */
	private String ownerAgentCode;

	/** Holds origin channel id */
	private Integer originChannelId;

	/** Holds origin agent code */
	private String originAgentCode;

	/** Holds origin user id */
	private String originUserId;

	/** Holds origin sales terminal */
	private String originSalesTerminal;

	/** Holds terminal for last modification */
	private String lastSalesTerminal;

	/** Holds origin ip address */
	private String originIPAddress;

	/** Holds the origin ip number calculated from the ip address */
	private Long originIPNumber;

	/** Holds the origin carrier code */
	private String originCarrierCode;

	/** Holds the dry agent code **/
	private String originDryAgentCode;

	/**
	 * Holds the origin country code. This will be the originAgent's country code for XBE bookings. For IBE this will be
	 * the country code the originIPAddress belongs to.
	 */
	private String originCountryCode;

	/**
	 * @return Returns the originAgentCode.
	 * @hibernate.property column = "ORIGIN_AGENT_CODE"
	 */
	public String getOriginAgentCode() {
		return originAgentCode;
	}

	/**
	 * @param originAgentCode
	 *            The originAgentCode to set.
	 */
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	/**
	 * @return Returns the originChannelId.
	 * @hibernate.property column = "ORIGIN_CHANNEL_CODE"
	 */
	public Integer getOriginChannelId() {
		return originChannelId;
	}

	/**
	 * @param originChannelId
	 *            The originChannelId to set.
	 */
	public void setOriginChannelId(Integer originChannelId) {
		this.originChannelId = originChannelId;
	}

	/**
	 * @return Returns the originUserId.
	 * @hibernate.property column = "ORIGIN_USER_ID"
	 */
	public String getOriginUserId() {
		return originUserId;
	}

	/**
	 * @param originUserId
	 *            The originUserId to set.
	 */
	public void setOriginUserId(String originUserId) {
		this.originUserId = originUserId;
	}

	/**
	 * @return Returns the ownerAgentCode.
	 * @hibernate.property column = "OWNER_AGENT_CODE"
	 */
	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	/**
	 * @param ownerAgentCode
	 *            The ownerAgentCode to set.
	 */
	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	/**
	 * @return Returns the ownerChannelId.
	 * @hibernate.property column = "OWNER_CHANNEL_CODE"
	 */
	public Integer getOwnerChannelId() {
		return ownerChannelId;
	}

	/**
	 * @param ownerChannelId
	 *            The ownerChannelId to set.
	 */
	public void setOwnerChannelId(Integer ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	/**
	 * @return Returns the lastSalesTerminal.
	 * @hibernate.property column = "LAST_SALE_TERMINAL"
	 */
	public String getLastSalesTerminal() {
		return lastSalesTerminal;
	}

	/**
	 * @param lastSalesTerminal
	 *            The lastSalesTerminal to set.
	 */
	public void setLastSalesTerminal(String lastSalesTerminal) {
		this.lastSalesTerminal = lastSalesTerminal;
	}

	/**
	 * @return Returns the originSalesTerminal.
	 * @hibernate.property column = "ORIGIN_SALE_TERMINAL"
	 */
	public String getOriginSalesTerminal() {
		return originSalesTerminal;
	}

	/**
	 * @param originSalesTerminal
	 *            The originSalesTerminal to set.
	 */
	public void setOriginSalesTerminal(String originSalesTerminal) {
		this.originSalesTerminal = originSalesTerminal;
	}

	/**
	 * @hibernate.property column = "ORIGIN_IP"
	 */
	public String getOriginIPAddress() {
		return originIPAddress;
	}

	/**
	 * @param originIPAddress
	 *            The originIPAddress to set.
	 */
	public void setOriginIPAddress(String originIPAddress) {
		this.originIPAddress = originIPAddress;
	}

	/**
	 * @return Returns the IP number calculated from IP address
	 * @hibernate.property column = "ORIGIN_IP_NUMBER"
	 */
	public Long getOriginIPNumber() {
		return originIPNumber;
	}

	/**
	 * @param originIPNumber
	 *            The originIPNumber to set.
	 */
	public void setOriginIPNumber(Long originIPNumber) {
		this.originIPNumber = originIPNumber;
	}

	/**
	 * @return the originCarrierCode
	 * @hibernate.property column = "ORIGIN_CARRIER_CODE"
	 */
	public String getOriginCarrierCode() {
		return originCarrierCode;
	}

	/**
	 * @param originCarrierCode
	 *            the originCarrierCode to set
	 */
	public void setOriginCarrierCode(String originCarrierCode) {
		this.originCarrierCode = originCarrierCode;
	}

	/**
	 * @return the originDryAgentCode
	 * @hibernate.property column = "ORIGIN_DRY_AGENT_CODE"
	 */
	public String getOriginDryAgentCode() {
		return originDryAgentCode;
	}

	/**
	 * @param originDryAgentCode
	 *            the originDryAgentCode to set
	 */
	public void setOriginDryAgentCode(String originDryAgentCode) {
		this.originDryAgentCode = originDryAgentCode;
	}

	/**
	 * @return the originCountryCode
	 */
	public String getOriginCountryCode() {
		return originCountryCode;
	}

	/**
	 * @param originCountryCode
	 *            the originCountryCode to set
	 */
	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}
}