package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.Alpha;
import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.BasicDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class UNB extends EDISegment {

	private String E0001;
	private String E0002;
	private String E0004;
	private String E0007_1;
	private String E0010;
	private String E0007_2;
	private String E0017;
	private String E0019;
	private String E0020;
	private String E0026;
	private String E0029;
	private String E0031;
	private String E0032;
	private String E0035;

	public UNB() {
		super(EDISegmentTag.UNB);
	}

	public void setSyntaxIdentifier(String e0001) {
		E0001 = e0001;
	}

	public void setSyntaxVersionNumber(String e0002) {
		E0002 = e0002;
	}

	public void setCarrierCode(String e0004) {
		E0004 = e0004;
	}

	public void setSenderPartnerIdentification(String e0007_1) {
		E0007_1 = e0007_1;
	}

	public void setRecipientIdentification(String e0010) {
		E0010 = e0010;
	}

	public void setReceiverPartnerIdentification(String e0007_2) {
		E0007_2 = e0007_2;
	}

	public void setDateOfPreparation(String e0017) {
		E0017 = e0017;
	}

	public void setTimeOfPreparation(String e0019) {
		E0019 = e0019;
	}

	public void setInterchangeControlReference(String e0020) {
		E0020 = e0020;
	}

	public void setApplicationReference(String e0026) {
		E0026 = e0026;
	}

	public void setProcessingPriorityCode(String e0029) {
		E0029 = e0029;
	}

	public void setAcknowledgementRequest(String e0031) {
		E0031 = e0031;
	}

	public void setCommunicationsAgreementID(String e0032) {
		E0032 = e0032;
	}

	public void setTestIndicator(String e0035) {
		E0035 = e0035;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement S001 = new CompoundDataElement("S001", DE_STATUS.M);
		S001.addBasicDataelement(new Alpha(EDI_ELEMENT.E0001, E0001));
		S001.addBasicDataelement(new Numeric(EDI_ELEMENT.E0002, E0002));

		CompoundDataElement S002 = new CompoundDataElement("S002", DE_STATUS.M);
		S002.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0004, E0004));
		S002.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0007, E0007_1));

		CompoundDataElement S003 = new CompoundDataElement("S003", DE_STATUS.M);
		S003.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0010, E0010));
		S003.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E0007, E0007_2));

		CompoundDataElement S004 = new CompoundDataElement("S004", DE_STATUS.M);
		S004.addBasicDataelement(new Numeric(EDI_ELEMENT.E0017, E0017));
		S004.addBasicDataelement(new Numeric(EDI_ELEMENT.E0019, E0019));

		BasicDataElement V0020 = new AlphaNumeric(EDI_ELEMENT.E0020, E0020);
		BasicDataElement V0026 = new AlphaNumeric(EDI_ELEMENT.E0026, E0026);
		BasicDataElement V0029 = new Alpha(EDI_ELEMENT.E0029, E0029);
		BasicDataElement V0031 = new Numeric(EDI_ELEMENT.E0031, E0031);
		BasicDataElement V0032 = new AlphaNumeric(EDI_ELEMENT.E0032, E0032);
		BasicDataElement V0035 = new Numeric(EDI_ELEMENT.E0035, E0035);

		addEDIDataElement(S001);
		addEDIDataElement(S002);
		addEDIDataElement(S003);
		addEDIDataElement(S004);
		addEDIDataElement(V0020);
		addEDIDataElement(V0026);
		addEDIDataElement(V0029);
		addEDIDataElement(V0031);
		addEDIDataElement(V0032);
		addEDIDataElement(V0035);

		return this;
	}

}
