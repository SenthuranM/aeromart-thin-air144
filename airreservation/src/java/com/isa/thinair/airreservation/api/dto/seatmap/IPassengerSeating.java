package com.isa.thinair.airreservation.api.dto.seatmap;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface IPassengerSeating extends AncillaryAssembler {

	public void addSeatInfo(Integer pnrPaxId, Integer pnrPaxFareId, Integer flightSeatId, Integer pnrSegId,
			BigDecimal chargeAmount, Integer autoCnxId, TrackInfoDTO trackInfo) throws ModuleException;
}
