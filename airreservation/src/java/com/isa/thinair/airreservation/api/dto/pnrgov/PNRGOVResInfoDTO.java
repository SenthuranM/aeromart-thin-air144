package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.util.Date;

import com.isa.thinair.airtravelagents.api.model.Agent;

public class PNRGOVResInfoDTO {
	
	private String pnr;
	
	private String originAgentCode;
	
	private String ownerAgentCode;
	
	private Agent ownerAgent;
	
	private Date bookingTimeStamp;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getOriginAgentCode() {
		return originAgentCode;
	}

	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	public Date getBookingTimeStamp() {
		return bookingTimeStamp;
	}

	public void setBookingTimeStamp(Date bookingTimeStamp) {
		this.bookingTimeStamp = bookingTimeStamp;
	}

	public Agent getOwnerAgent() {
		return ownerAgent;
	}

	public void setOwnerAgent(Agent ownerAgent) {
		this.ownerAgent = ownerAgent;
	}

}
