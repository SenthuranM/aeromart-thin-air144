package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;

/**
 * @author Nilindra Fernando
 * @since August 6, 2010
 */
public class RefundablesMetaInfoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6830141439220983473L;

	private BigDecimal refundableTotal;

	private BigDecimal cancellationTotal;

	private BigDecimal modificationTotal;

	private Map<Long, ReservationPaxOndCharge> mapRefundablePaxOndChargeMetaInfo = new HashMap<Long, ReservationPaxOndCharge>();

	public void addRefundableCharge(Integer pnrPaxOndChgId, ReservationPaxOndCharge reservationPaxOndCharge) {
		if(pnrPaxOndChgId!=null){
			this.getMapRefundablePaxOndChargeMetaInfo().put(new Long(pnrPaxOndChgId), reservationPaxOndCharge);
		}
	}

	/**
	 * @return the refundableTotal
	 */
	public BigDecimal getRefundableTotal() {
		return refundableTotal;
	}

	/**
	 * @param refundableTotal
	 *            the refundableTotal to set
	 */
	public void setRefundableTotal(BigDecimal refundableTotal) {
		this.refundableTotal = refundableTotal;
	}

	/**
	 * @return the cancellationTotal
	 */
	public BigDecimal getCancellationTotal() {
		return cancellationTotal;
	}

	/**
	 * @param cancellationTotal
	 *            the cancellationTotal to set
	 */
	public void setCancellationTotal(BigDecimal cancellationTotal) {
		this.cancellationTotal = cancellationTotal;
	}

	/**
	 * @return the modificationTotal
	 */
	public BigDecimal getModificationTotal() {
		return modificationTotal;
	}

	/**
	 * @param modificationTotal
	 *            the modificationTotal to set
	 */
	public void setModificationTotal(BigDecimal modificationTotal) {
		this.modificationTotal = modificationTotal;
	}

	/**
	 * @return the mapRefundablePaxOndChargeMetaInfo
	 */
	public Map<Long, ReservationPaxOndCharge> getMapRefundablePaxOndChargeMetaInfo() {
		return mapRefundablePaxOndChargeMetaInfo;
	}

}
