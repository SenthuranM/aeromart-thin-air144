package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.PNRGOVException;

public class UNA extends EDISegment {
	
	public UNA() {
		super(EDISegmentTag.UNA);
	}

	@Override
	public String getEDIFmtData() throws PNRGOVException {
		StringBuffer sb = new StringBuffer(segmentName);
		sb.append(ElementFactory.SUB_ELEMENT_SEPERATOR);
		sb.append(ElementFactory.DATA_ELEMENT_SEPERATOR);
		sb.append(ElementFactory.DECIMAL_NOTATION);
		sb.append(ElementFactory.RELEASE_INDICATOR);
		sb.append(ElementFactory.SPACE);
		sb.append(ElementFactory.SEGMENT_SEPERATOR);
		return sb.toString();
	}

	@Override
	public MessageSegment build() {
		return this;
	}

}
