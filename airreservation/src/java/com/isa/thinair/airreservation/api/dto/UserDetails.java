package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class UserDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4965700612816881187L;

	private String userId;

	private String displayName;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
