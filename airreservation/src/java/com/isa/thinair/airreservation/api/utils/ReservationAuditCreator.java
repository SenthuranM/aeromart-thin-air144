package com.isa.thinair.airreservation.api.utils;

import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;

/**
 * 
 * @author primal Suaris
 * 
 * 
 * 
 */
public class ReservationAuditCreator {

	/**
	 * 
	 * @param pnr
	 * @param customChargesTO
	 *            -
	 * @param credentialsDTO
	 * @return
	 */
	public ReservationAudit createCNXMODChargeOverideAudit(String pnr, CustomChargesTO customChargesTO,
			CredentialsDTO credentialsDTO) {

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CNX_CHARGE_OVERIDE.getCode());

		if (customChargesTO.getCustomAdultCCharge() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OverrideCNX_MODCharge.ADULT_CNX_CHARGE,
					customChargesTO.getCustomAdultCCharge().toString());
		}

		if (customChargesTO.getCustomChildCCharge() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OverrideCNX_MODCharge.CHILD_CNX_CHARGE,
					customChargesTO.getCustomChildCCharge().toString());
		}

		if (customChargesTO.getDefaultCustomAdultCharge() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OverrideCNX_MODCharge.Adult_CNX_DEFAULT_CHARGE,
					customChargesTO.getDefaultCustomAdultCharge().toString());
		}

		if (customChargesTO.getDefaultCustomChildCharge() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OverrideCNX_MODCharge.CHILD_CNX_DEFAULT_CHARGE,
					customChargesTO.getDefaultCustomChildCharge().toString());
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OverrideCNX_MODCharge.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OverrideCNX_MODCharge.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}
		return reservationAudit;
	}
	
	/**
	 * @param currentPaxCoupon
	 * @param newCouponStatus
	 * @param passengerCouponAudit
	 * @return
	 */
	public static ReservationAudit createPassengerCouponUpdateAudit(LccClientPassengerEticketInfoTO currentPaxCoupon,
			String newCouponStatus, String newPaxStatus, PassengerTicketCouponAuditDTO passengerCouponAudit) {

		ReservationAudit reservationAudit = new ReservationAudit();

		reservationAudit.setPnr(passengerCouponAudit.getPnr());
		reservationAudit.setModificationType(AuditTemplateEnum.UPDATED_ETICKET.getCode());
		reservationAudit.setUserNote(passengerCouponAudit.getRemark());
		// contentMap
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PNR_PAX_ID,
				passengerCouponAudit.getPnrPaxId());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.ETICKET_NUMBER,
				currentPaxCoupon.getPaxETicketNo() + " / " + currentPaxCoupon.getCouponNo());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.FLIGHT_NUMBER,
				currentPaxCoupon.getFlightNo());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.SEGMENT_CODE,
				currentPaxCoupon.getSegmentCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.FLIGHT_DEPARTURE_DATE,
				BeanUtils.parseDateFormat(currentPaxCoupon.getDepartureDate(), "E, dd MMM yyyy HH:mm:ss"));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.STATUS, newCouponStatus);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PREVIOUS_STATUS,
				currentPaxCoupon.getPaxETicketStatus());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PAX_STATUS, newPaxStatus);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PREVIOUS_PAX_STATUS,
				currentPaxCoupon.getPaxStatus());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PASSENGER_NAME,
				passengerCouponAudit.getPassengerName());

		return reservationAudit;

	}

}
