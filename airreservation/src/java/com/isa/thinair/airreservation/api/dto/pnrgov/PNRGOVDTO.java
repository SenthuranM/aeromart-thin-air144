package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationContactInfo;

public class PNRGOVDTO {

	public enum SsrTypes {
		INFT, SEAT, DOCS, DOCO
	};

	public enum TransmissionStatus {
		SUBMITTED, ERROR
	};

	private String pnr;

	private List<PNRGOVSegmentDTO> segments;

	private List<PNRGOVPassengerDTO> passengers;

	private ReservationContactInfo contactInfo;

	private PNRGOVResInfoDTO reservationInfo;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<PNRGOVSegmentDTO> getSegments() {
		return segments;
	}

	public void setSegments(List<PNRGOVSegmentDTO> segments) {
		this.segments = segments;
	}

	public List<PNRGOVPassengerDTO> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<PNRGOVPassengerDTO> passengers) {
		this.passengers = passengers;
	}

	public ReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public PNRGOVResInfoDTO getReservationInfo() {
		return reservationInfo;
	}

	public void setReservationInfo(PNRGOVResInfoDTO reservationInfo) {
		this.reservationInfo = reservationInfo;
	}

}
