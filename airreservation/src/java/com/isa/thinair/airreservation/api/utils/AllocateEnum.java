/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

import java.io.Serializable;

/**
 * @author Lasantha
 * 
 */
public class AllocateEnum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5283647646514698157L;
	private int code;

	private AllocateEnum(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(AllocateEnum allocateEnum) {
		return (allocateEnum.getCode() == this.getCode());
	}

	public int getCode() {
		return code;
	}

	public static final AllocateEnum AUTO_FIT = new AllocateEnum(1);
	public static final AllocateEnum OVER_ALL = new AllocateEnum(2);
	public static final AllocateEnum OVER_LIMIT = new AllocateEnum(3);
}
