package com.isa.thinair.airreservation.api.model.pnrgov;

public class ElementFactory {

	public static String DATA_ELEMENT_SEPERATOR = "+";
	public static String SUB_ELEMENT_SEPERATOR	= ":";
	public static String DECIMAL_NOTATION		= ".";
	public static String RELEASE_INDICATOR 		= "?";
	public static String SPACE 					= " ";
	public static String SEGMENT_SEPERATOR 		= "'";
	
	/** data type */
	private enum DT {
		ALPHA, /* alpha */
		ALPHA_NUMERIC, /* alpha numeric */
		NUMERIC; /* numeric */
	}

	public enum DE_STATUS {
		M, /* mandatory */
		C, /* conditional */
		NA; /* not applicable */
	}
	
	public enum EDI_ELEMENT {
		
		E0001("Syntax id", DT.ALPHA, 4, 4, DE_STATUS.M),
		E0002("Syntax version No", DT.NUMERIC, 1, 1, DE_STATUS.M),
		E0004("Interchange sender id", DT.ALPHA_NUMERIC, 1, 35, DE_STATUS.M),
		E0007("Partner identification code", DT.ALPHA_NUMERIC, 1, 4, DE_STATUS.C),
		E0010("Recipient Identification", DT.ALPHA_NUMERIC, 1, 35, DE_STATUS.M),
		E0017("Date of Preparation", DT.NUMERIC, 1, 6, DE_STATUS.M),
		E0019("Time of Preparation", DT.NUMERIC, 1, 4, DE_STATUS.M),
		E0020("Interchange control reference", DT.ALPHA_NUMERIC, 1, 14, DE_STATUS.M),
		E0022("Receipient reference password", DT.ALPHA_NUMERIC, 1, 14, DE_STATUS.C),
		E0025("Receipient reference password qualifier", DT.ALPHA_NUMERIC, 1, 2, DE_STATUS.C),
		E0026("Application reference", DT.ALPHA_NUMERIC, 1, 14, DE_STATUS.C),
		E0029("Processing priority code", DT.ALPHA, 1, 1, DE_STATUS.C),
		E0031("Acknowledgement request", DT.NUMERIC, 1, 1, DE_STATUS.C),
		E0032("Communications Agreement ID", DT.ALPHA_NUMERIC, 1, 35, DE_STATUS.C),
		E0035("Test Indicator", DT.NUMERIC, 1, 1, DE_STATUS.C),
		E0060("Number of Messages", DT.NUMERIC, 1, 6, DE_STATUS.M),
		E0048("Application sender identification", DT.ALPHA_NUMERIC, 1, 14, DE_STATUS.M),
		E0038("Functional group identification", DT.ALPHA_NUMERIC, 6, 6, DE_STATUS.M),
		E0040("Application sender identification", DT.ALPHA_NUMERIC, 1, 35, DE_STATUS.M),
		E0044("Applicatin receipient identification", DT.ALPHA_NUMERIC, 1,35, DE_STATUS.M),
		E0051("Controlling Agency",DT.ALPHA_NUMERIC, 2,2, DE_STATUS.M),
		E0052("Message type version number", DT.ALPHA_NUMERIC, 1,3, DE_STATUS.M),
		E0054("Message type release version number", DT.ALPHA_NUMERIC, 1,3, DE_STATUS.M),
		E0062("Message reference number", DT.ALPHA_NUMERIC, 1,14, DE_STATUS.M),
		E0065("Message type", DT.ALPHA_NUMERIC, 6,6, DE_STATUS.M),
		E0057("Association assigned code", DT.ALPHA_NUMERIC, 1,6, DE_STATUS.C),
		E0068("Common access reference", DT.ALPHA_NUMERIC, 1,35, DE_STATUS.C),
		E0070("Sequence of transfers", DT.NUMERIC, 1,2, DE_STATUS.M),
		E0073("First and last transfer", DT.ALPHA, 1,2, DE_STATUS.C),
		E0074("Number of segments in a message", DT.NUMERIC, 1,10, DE_STATUS.M),
		E0036("Interchange control count", DT.NUMERIC, 1,6, DE_STATUS.M),
		E9906("Company Identification",DT.ALPHA_NUMERIC,2,35,DE_STATUS.M),
		E9956("Reservation Control Number",DT.ALPHA_NUMERIC,1,20,DE_STATUS.M),
		E9958("Reservation Control Type",DT.ALPHA_NUMERIC,1,1,DE_STATUS.C),
		E9916("First Date",DT.NUMERIC,1,35,DE_STATUS.C),
		E9994("Time",DT.NUMERIC,1,9,DE_STATUS.C),
		E2005("Date/Time/Period qualifier",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E9918("First Time",DT.NUMERIC,1,4,DE_STATUS.C),
		E4451("Text subject qualifier",DT.NUMERIC,1,4,DE_STATUS.C),
		E3225("Place/location identification",DT.ALPHA_NUMERIC,1,25,DE_STATUS.C),
		E9900("Travel Agent Identification Details",DT.NUMERIC,8,9,DE_STATUS.C),
		E9902("In house identification",DT.ALPHA_NUMERIC,1,9,DE_STATUS.C),
		E3299("Address purpose code",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E3042("Street and number/P.O. Box",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E3164("City name",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E3229("Country sub-entity identification",DT.ALPHA_NUMERIC,1,9,DE_STATUS.C),
		E3228("Country sub-entity name",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E3207("Country, coded",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E3251("Postcode identification",DT.ALPHA_NUMERIC,1,17,DE_STATUS.C),
		E4440("Free text",DT.ALPHA_NUMERIC,1,70,DE_STATUS.NA),
		E9936("Traveller Surname",DT.ALPHA_NUMERIC,1,70,DE_STATUS.M),
		E6353("Number of Units Qualifier",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E9942("Traveller Given Name",DT.ALPHA_NUMERIC,1,70,DE_STATUS.C),
		E9944("Traveller Reference Number",DT.ALPHA_NUMERIC,1,10,DE_STATUS.C),
		E9946("Traveller Accompanied by Infant Indicator",DT.ALPHA_NUMERIC,1,1,DE_STATUS.C),
		E9962("Special Requirement Type",DT.ALPHA_NUMERIC,1,4,DE_STATUS.M),
		E4405("Status, coded",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E6060("Quantity",DT.NUMERIC,1,15,DE_STATUS.C),
		E7365("Processing Indicator",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E9960("Special Requirement Data",DT.ALPHA_NUMERIC,1,4,DE_STATUS.C),
		E6411("Measure Unit Qualifier",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E9825("Seat Characteristic, coded",DT.ALPHA_NUMERIC,1,2,DE_STATUS.C),
		E1004("Document/message number",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E1001("Document/ message name,coded",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E7240("Total number of items",DT.NUMERIC,1,15,DE_STATUS.C),
		E9988("Data Indicator",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E9888("Form of Payment Identification",DT.ALPHA_NUMERIC,1,10,DE_STATUS.M),
		E5004("Monetary Amount",DT.NUMERIC,1,18,DE_STATUS.C),
		E1154("Reference Number",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E9920("Second Date",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E9922("Second Time",DT.NUMERIC,1,4,DE_STATUS.C),
		E9954("Date Variation",DT.NUMERIC,1,1,DE_STATUS.C),
		E9908("Product Identification",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E4025("Business Function, Coded",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E1225("Message Function, Coded",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C),
		E6350("Message Function, Coded",DT.NUMERIC,1,3,DE_STATUS.C),
		E5025("Monetary Amount Type",DT.ALPHA_NUMERIC,1,3,DE_STATUS.M),
		E1230("Allowance or Charge Number",DT.ALPHA_NUMERIC,1,35,DE_STATUS.C),
		E6345("Currency Code",DT.ALPHA_NUMERIC,1,3,DE_STATUS.C);
		
		private EDI_ELEMENT(String description, DT dataType, int minLength, int maxLength, DE_STATUS status) {
			this.description = description;
			this.dataType = dataType;
			this.minLength = minLength;
			this.maxLength = maxLength;
			this.status = status;
		}
		private String description;
		private DT dataType;
		private int maxLength;
		private int minLength;
		private DE_STATUS status;
		
		public String getDescription() {
			return this.description;
		}
		
		public DT getDataType() {
			return this.dataType;
		}
		
		public int getMaxLength() {
			return this.maxLength;
		}
		
		public int getMinLength() {
			return this.minLength;
		}
		
		public DE_STATUS getStatus() {
			return this.status;
		}
	}
	
	public enum EDISegmentTag {
		UNA, UNB, UNE, UNG, UNH, UNT, UNZ, SRC, RCI, DAT, ORG, ADD, TIF, SSR, TKT, FOP, TVL, MSG, EQN, MON
	}
}
