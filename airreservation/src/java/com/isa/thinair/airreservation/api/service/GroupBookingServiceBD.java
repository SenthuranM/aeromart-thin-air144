package com.isa.thinair.airreservation.api.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestHistoryTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

public interface GroupBookingServiceBD {
	public static final String SERVICE_NAME = "ReservationService";

	public Page<GroupBookingRequestTO> getGroupBookingRequestDetails(GroupBookingRequestTO grpBookingReq, String userCode,
			Integer start,boolean mainRequestsOnly) throws ModuleException;

	public ServiceResponce addGroupBookingRequestDetails(GroupBookingRequestTO grpBookingReq) throws ModuleException;

	public ServiceResponce updateGroupBookingRequest(GroupBookingRequestTO grpBookingReq) throws ModuleException;

	public ServiceResponce updateApproveStatus(GroupBookingRequestTO grpBookingReq, Map<Long, BigDecimal> editedData)
			throws ModuleException;

	public ServiceResponce updateGroupBookingAdminStatus(GroupBookingRequestTO grpBookingReq, Collection<Long> requestIDs)
			throws ModuleException;

	public ServiceResponce updateGroupBookingAgentStatus(GroupBookingRequestTO grpBookingReq) throws ModuleException;

	public ServiceResponce updateGroupBookingRequote(GroupBookingRequestTO grpBookingReq) throws ModuleException;

	public ServiceResponce executeRollforward(long requestID, Set<GroupBookingRequestTO> rollForwardedRequests)
			throws ModuleException;

	public ServiceResponce updateBookingStatus(long requestID, BigDecimal payment, String passengerList,
			ArrayList<String> outFlightRPHList, ArrayList<String> retFlightRPHList) throws ModuleException;

	public GroupBookingRequestTO validateGroupBookingRequest(long requestID, BigDecimal payment, String passengerList,
			ArrayList<String> outFlightRPHList, ArrayList<String> retFlightRPHList, String agentCode) throws ModuleException;

	public ServiceResponce addUserStations(String user, Set<String> stations) throws ModuleException;

	public String getUserStation(String userId) throws ModuleException;

	public ServiceResponce addGroupBookingStations(long groupReqID, Set<String> stations) throws ModuleException;

	public String getGroupBookingStation(long groupReqID) throws ModuleException;

	public Page<GroupBookingRequestTO> getGroupBookingRequestForStations(GroupBookingRequestTO grpBookingReq, String stationCode,
			String stations, Integer start, boolean mainReqestOnly) throws ModuleException;

	public Page<GroupBookingRequestHistoryTO> getGroupBookingRequestHistory(long requestID) throws ModuleException;

	public GroupBookingRequestTO validateModifyResGroupBookingRequest(long requestID, BigDecimal payment, String passengerList,
			ArrayList<String> flightlist, String agentCode) throws ModuleException;

	public ServiceResponce updateGroupBookingStatus(long requestID, BigDecimal totalPayment, String pnr) throws ModuleException;

	public Collection<Reservation> getReservation(long groupBookingReqId) throws ModuleException;

	public String getActualPaymentByPnr(String pnr) throws ModuleException;

}
