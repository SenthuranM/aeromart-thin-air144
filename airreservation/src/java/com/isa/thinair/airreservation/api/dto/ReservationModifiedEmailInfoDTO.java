package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class ReservationModifiedEmailInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pnr;

	private String description;

	private String modifiedBy;

	private Date modifiedDate;

	private Date oldReleaseOnHoldDate;

	private Date newReleaseOnHoldDate;

	private String modificationType;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getOldReleaseOnHoldDate() {
		return oldReleaseOnHoldDate;
	}

	public void setOldReleaseOnHoldDate(Date oldReleaseOnHoldDate) {
		this.oldReleaseOnHoldDate = oldReleaseOnHoldDate;
	}

	public Date getNewReleaseOnHoldDate() {
		return newReleaseOnHoldDate;
	}

	public void setNewReleaseOnHoldDate(Date newReleaseOnHoldDate) {
		this.newReleaseOnHoldDate = newReleaseOnHoldDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModificationType() {
		return modificationType;
	}

	public void setModificationType(String modificationType) {
		this.modificationType = modificationType;
	}

	public String getModifiedStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getModifiedDate(), dateFormat);
	}

}
