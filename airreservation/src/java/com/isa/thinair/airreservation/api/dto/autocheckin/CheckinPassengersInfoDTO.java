/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class CheckinPassengersInfoDTO implements Serializable {

	private static final long serialVersionUID = 8294682325030026816L;
	private String id;
	private String flightId;
	private Integer pnrPaxId;
	private Integer adultId;
	private String carrierInfoCode;
	private String carrierInfoFlightnumber;
	private String dateOfDeparture;
	private String departureLocationCode;
	private String flightLegRPH;
	private String flightLegDepAirportLocationCode;
	private String flightLegAriAirportLocationCode;
	private String flightLegOperatingAirlineCode;
	private String flightLegOperatingAirlineFlightNumber;
	private String flightInfoDateDeparture;
	private String paxSelectedSeat;
	private String paxType;
	private String passportNumber;
	private String passportIssuedCountry;
	private String passportExpiryDate;
	private String passportFullName;
	private Integer visaType;
	private String visaNumber;
	private String visaIssuedCountry;
	private String visaIssuedDate;
	private String dateOfBirth;
	private String countryOfResidence;
	private String placeOfBirth;
	private String destinationCity;
	private String nationality;
	private String travelDocType;
	private String staffId;
	private String dateOfJoin;
	private String visaApplicableCountry;
	private String gender;
	private String passengerGivenname;
	private String passengerSurname;
	private String passengerNametitle;
	private String preferredLanguage;
	private String travelWithInfant;
	private String email;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getAdultId() {
		return adultId;
	}

	public void setAdultId(Integer adultId) {
		this.adultId = adultId;
	}

	public String getCarrierInfoCode() {
		return carrierInfoCode;
	}

	public void setCarrierInfoCode(String carrierInfoCode) {
		this.carrierInfoCode = carrierInfoCode;
	}

	public String getCarrierInfoFlightnumber() {
		return carrierInfoFlightnumber;
	}

	public void setCarrierInfoFlightnumber(String carrierInfoFlightnumber) {
		this.carrierInfoFlightnumber = carrierInfoFlightnumber;
	}

	public String getDateOfDeparture() {
		return dateOfDeparture;
	}

	public void setDateOfDeparture(String dateOfDeparture) {
		this.dateOfDeparture = dateOfDeparture;
	}

	public String getDepartureLocationCode() {
		return departureLocationCode;
	}

	public void setDepartureLocationCode(String departureLocationCode) {
		this.departureLocationCode = departureLocationCode;
	}

	public String getFlightLegRPH() {
		return flightLegRPH;
	}

	public void setFlightLegRPH(String flightLegRPH) {
		this.flightLegRPH = flightLegRPH;
	}

	public String getFlightLegDepAirportLocationCode() {
		return flightLegDepAirportLocationCode;
	}

	public void setFlightLegDepAirportLocationCode(String flightLegDepAirportLocationCode) {
		this.flightLegDepAirportLocationCode = flightLegDepAirportLocationCode;
	}

	public String getFlightLegAriAirportLocationCode() {
		return flightLegAriAirportLocationCode;
	}

	public void setFlightLegAriAirportLocationCode(String flightLegAriAirportLocationCode) {
		this.flightLegAriAirportLocationCode = flightLegAriAirportLocationCode;
	}

	public String getFlightLegOperatingAirlineCode() {
		return flightLegOperatingAirlineCode;
	}

	public void setFlightLegOperatingAirlineCode(String flightLegOperatingAirlineCode) {
		this.flightLegOperatingAirlineCode = flightLegOperatingAirlineCode;
	}

	public String getFlightLegOperatingAirlineFlightNumber() {
		return flightLegOperatingAirlineFlightNumber;
	}

	public void setFlightLegOperatingAirlineFlightNumber(String flightLegOperatingAirlineFlightNumber) {
		this.flightLegOperatingAirlineFlightNumber = flightLegOperatingAirlineFlightNumber;
	}

	public String getFlightInfoDateDeparture() {
		return flightInfoDateDeparture;
	}

	public void setFlightInfoDateDeparture(String flightInfoDateDeparture) {
		this.flightInfoDateDeparture = flightInfoDateDeparture;
	}

	public String getPaxSelectedSeat() {
		return paxSelectedSeat;
	}

	public void setPaxSelectedSeat(String paxSelectedSeat) {
		this.paxSelectedSeat = paxSelectedSeat;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getPassportIssuedCountry() {
		return passportIssuedCountry;
	}

	public void setPassportIssuedCountry(String passportIssuedCountry) {
		this.passportIssuedCountry = passportIssuedCountry;
	}

	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	public String getPassportFullName() {
		return passportFullName;
	}

	public void setPassportFullName(String passportFullName) {
		this.passportFullName = passportFullName;
	}

	public Integer getVisaType() {
		return visaType;
	}

	public void setVisaType(Integer visaType) {
		this.visaType = visaType;
	}

	public String getVisaNumber() {
		return visaNumber;
	}

	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}

	public String getVisaIssuedCountry() {
		return visaIssuedCountry;
	}

	public void setVisaIssuedCountry(String visaIssuedCountry) {
		this.visaIssuedCountry = visaIssuedCountry;
	}

	public String getVisaIssuedDate() {
		return visaIssuedDate;
	}

	public void setVisaIssuedDate(String visaIssuedDate) {
		this.visaIssuedDate = visaIssuedDate;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCountryOfResidence() {
		return countryOfResidence;
	}

	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getTravelDocType() {
		return travelDocType;
	}

	public void setTravelDocType(String travelDocType) {
		this.travelDocType = travelDocType;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(String dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassengerGivenname() {
		return passengerGivenname;
	}

	public void setPassengerGivenname(String passengerGivenname) {
		this.passengerGivenname = passengerGivenname;
	}

	public String getPassengerSurname() {
		return passengerSurname;
	}

	public void setPassengerSurname(String passengerSurname) {
		this.passengerSurname = passengerSurname;
	}

	public String getPassengerNametitle() {
		return passengerNametitle;
	}

	public void setPassengerNametitle(String passengerNametitle) {
		this.passengerNametitle = passengerNametitle;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public String getTravelWithInfant() {
		return travelWithInfant;
	}

	public void setTravelWithInfant(String travelWithInfant) {
		this.travelWithInfant = travelWithInfant;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
