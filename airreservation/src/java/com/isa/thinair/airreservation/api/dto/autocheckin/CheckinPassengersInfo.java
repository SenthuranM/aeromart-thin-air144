package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Panchatcharam.S
 *
 */
public class CheckinPassengersInfo implements Serializable {

	private static final long serialVersionUID = 344201385652009908L;
	private String status;
	private String tenentCode;
	private IdentificationSearchParameters identificationSearchParameters;
	private Collection<FlightInfoDetail> flightInfoDetails;
	private ThroughCheckInDestination throughCheckInDestination;
	// This booking information with pax info and pax additional info
	private Collection<BookingInfo> bookingInfo;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTenentCode() {
		return tenentCode;
	}

	public void setTenentCode(String tenentCode) {
		this.tenentCode = tenentCode;
	}

	public IdentificationSearchParameters getIdentificationSearchParameters() {
		return identificationSearchParameters;
	}

	public void setIdentificationSearchParameters(IdentificationSearchParameters identificationSearchParameters) {
		this.identificationSearchParameters = identificationSearchParameters;
	}

	public Collection<FlightInfoDetail> getFlightInfoDetails() {
		return flightInfoDetails;
	}

	public void setFlightInfoDetails(Collection<FlightInfoDetail> flightInfoDetails) {
		this.flightInfoDetails = flightInfoDetails;
	}

	public ThroughCheckInDestination getThroughCheckInDestination() {
		return throughCheckInDestination;
	}

	public void setThroughCheckInDestination(ThroughCheckInDestination throughCheckInDestination) {
		this.throughCheckInDestination = throughCheckInDestination;
	}

	public Collection<BookingInfo> getBookingInfo() {
		return bookingInfo;
	}

	public void setBookingInfo(Collection<BookingInfo> bookingInfo) {
		this.bookingInfo = bookingInfo;
	}

}
