package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Gayan
 * 
 * @hibernate.class table="t_group_booking_req_routes"
 */
public class GroupBookingReqRoutes extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6765219761980057897L;
	private Integer id;
	private Integer requestID;
	private String departure;
	private String arrival;
	private Date departureDate;
	private Date returningDate;
	private String flightNumber;
	private String via;

	/**
	 * @hibernate.id column="group_booking_req_routes_id" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_GROUP_BOOKING_REQ_ROUTES"
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @hibernate.property column="origin"
	 * 
	 * @return departure
	 */
	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	/**
	 * @hibernate.property column ="destination"
	 * @return
	 */
	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	/**
	 * @hibernate.property column="departure_date"
	 * @return
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @hibernate.property column="returning_date"
	 * @return
	 */
	public Date getReturningDate() {
		return returningDate;
	}

	public void setReturningDate(Date returningDate) {
		this.returningDate = returningDate;
	}

	public Integer getRequestID() {
		return requestID;
	}

	public void setRequestID(Integer requestID) {
		this.requestID = requestID;
	}

	/**
	 * @hibernate.property column= "FLIGHT_NUMBERS"
	 * @return flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	/**
	 * @hibernate.property column= "via_point"
	 * @return via
	 */
	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

}
