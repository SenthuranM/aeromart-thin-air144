package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.dto.ServiceProviderDTO;

/**
 * Using a Dto to populate itinerary depending on the service
 * <p>
 * Common DTO to hold Special and regular passenger SSR.
 * </p>
 * <p>
 * Special SSR includes Meal, Seat, Airport Services, Airport Transfers
 * </p>
 * <p>
 * SSR display and formating is determined by ssrSubCatID
 * </p>
 * 
 * @author Thushara
 * @author Dumindag
 * @since 1.0
 */
public class PaxSSRDTO implements Serializable {

	private static final long serialVersionUID = 4916628131074044994L;
	private String passenger;

	private int paxSeq;

	private String segment;

	private String flightNo;

	private String departureDate;

	private String mealCode;

	private String seatCode;

	private List<String> additionalSeatCodes;

	private Integer pnrSegId;

	private Integer ssrId;

	private String ssrCode;

	private String ssrText;

	private String contextId;

	private double chargeAmount;

	private Integer pnrPaxId;

	private char pnlAdlStatus;

	private char emailStatus;

	private String status;

	private String ssrDesc;

	private String pnr;

	private String contactNo;

	private String bookedTime;

	private String arrivalDate;

	private Integer ssrSubCatID;

	private String externalReference;

	private String ssrTranslatedText;

	private String airportCode;

	private String airportType;

	private String newAirportCode;

	private Integer pnrPaxSegmentSSRId;

	/* airport transfer */
	private String transferDate;

	private String transferAddress;

	private ServiceProviderDTO provider;

	private ExternalChgDTO chgDTO;

	private Collection<PaxSSRDetailDTO> colPaxSSRDetailDTO;

	private Integer autoCancellationId;
	
	private boolean inclideInPalCAl;
	
	private boolean userDefinedSSR;

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getPnr() {
		return pnr;
	}

	public String getBookedTime() {
		return bookedTime;
	}

	public void setBookedTime(String bookedTime) {
		this.bookedTime = bookedTime;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getPassenger() {
		return passenger;
	}

	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}

	public int getPaxSeq() {
		return paxSeq;
	}

	public void setPaxSeq(int paxSeq) {
		this.paxSeq = paxSeq;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the chargeAmount.
	 */
	public double getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the contextId.
	 */
	public String getContextId() {
		return contextId;
	}

	/**
	 * @param contextId
	 *            The contextId to set.
	 */
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	/**
	 * @return Returns the emailStatus.
	 */
	public char getEmailStatus() {
		return emailStatus;
	}

	/**
	 * @param emailStatus
	 *            The emailStatus to set.
	 */
	public void setEmailStatus(char emailStatus) {
		this.emailStatus = emailStatus;
	}

	/**
	 * @return Returns the pnlAdlStatus.
	 */
	public char getPnlAdlStatus() {
		return pnlAdlStatus;
	}

	/**
	 * @param pnlAdlStatus
	 *            The pnlAdlStatus to set.
	 */
	public void setPnlAdlStatus(char pnlAdlStatus) {
		this.pnlAdlStatus = pnlAdlStatus;
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the ssrId.
	 */
	public Integer getSsrId() {
		return ssrId;
	}

	/**
	 * @param ssrId
	 *            The ssrId to set.
	 */
	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		return ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the colPaxSSRDetailDTO.
	 */
	public Collection<PaxSSRDetailDTO> getColPaxSSRDetailDTO() {
		return colPaxSSRDetailDTO;
	}

	/**
	 * @param colPaxSSRDetailDTO
	 *            The colPaxSSRDetailDTO to set.
	 */
	public void setColPaxSSRDetailDTO(Collection<PaxSSRDetailDTO> colPaxSSRDetailDTO) {
		this.colPaxSSRDetailDTO = colPaxSSRDetailDTO;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrDesc.
	 */
	public String getSsrDesc() {
		return ssrDesc;
	}

	/**
	 * @param ssrDesc
	 *            The ssrDesc to set.
	 */
	public void setSsrDesc(String ssrDesc) {
		this.ssrDesc = ssrDesc;
	}

	/**
	 * @return Returns the ssrSubCatID.
	 */
	public Integer getSsrSubCatID() {
		return ssrSubCatID;
	}

	/**
	 * @param ssrSubCatID
	 *            The ssrSubCatID to set.
	 */
	public void setSsrSubCatID(Integer ssrSubCatID) {
		this.ssrSubCatID = ssrSubCatID;
	}

	/**
	 * 
	 * @return
	 */
	public String getExternalReference() {
		return externalReference;
	}

	/**
	 * 
	 * @param externalReference
	 */
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	public String getSsrTranslatedText() {
		return ssrTranslatedText;
	}

	public void setSsrTranslatedText(String ssrTranslatedText) {
		this.ssrTranslatedText = ssrTranslatedText;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the pnrPaxSegmentSSRId
	 */
	public Integer getPnrPaxSegmentSSRId() {
		return pnrPaxSegmentSSRId;
	}

	/**
	 * @param pnrPaxSegmentSSRId
	 *            the pnrPaxSegmentSSRId to set
	 */
	public void setPnrPaxSegmentSSRId(Integer pnrPaxSegmentSSRId) {
		this.pnrPaxSegmentSSRId = pnrPaxSegmentSSRId;
	}

	/**
	 * @return the chgDTO
	 */
	public ExternalChgDTO getChgDTO() {
		return chgDTO;
	}

	/**
	 * @param chgDTO
	 *            the chgDTO to set
	 */
	public void setChgDTO(ExternalChgDTO chgDTO) {
		this.chgDTO = chgDTO;
	}

	/**
	 * @return the airportType
	 */
	public String getAirportType() {
		return airportType;
	}

	/**
	 * @param airportType
	 *            the airportType to set
	 */
	public void setAirportType(String airportType) {
		this.airportType = airportType;
	}

	/**
	 * @return the newAirportCode
	 */
	public String getNewAirportCode() {
		return newAirportCode;
	}

	/**
	 * @param newAirportCode
	 *            the newAirportCode to set
	 */
	public void setNewAirportCode(String newAirportCode) {
		this.newAirportCode = newAirportCode;
	}

	/**
	 * @return the additionalSeatCodes
	 */
	public List<String> getAdditionalSeatCodes() {
		return additionalSeatCodes;
	}

	/**
	 * @param additionalSeatCodes
	 *            the additionalSeatCodes to set
	 */
	public void setAdditionalSeatCodes(List<String> additionalSeatCodes) {
		this.additionalSeatCodes = additionalSeatCodes;
	}
	
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

	public ServiceProviderDTO getProvider() {
		if(provider == null){
			provider = new ServiceProviderDTO();
		}
		return provider;
	}

	public void setProvider(ServiceProviderDTO provider) {
		this.provider = provider;
	}

	public boolean isInclideInPalCAl() {
		return inclideInPalCAl;
	}

	public void setInclideInPalCAl(boolean inclideInPalCAl) {
		this.inclideInPalCAl = inclideInPalCAl;
	}

	public boolean isUserDefinedSSR() {
		return userDefinedSSR;
	}

	public void setUserDefinedSSR(boolean userDefinedSSR) {
		this.userDefinedSSR = userDefinedSSR;
	}

}
