/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Holds Passenger final sales Meta data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PFSMetaDataDTO {

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the departure day e.g. 02 */
	private String day;

	/** Holds the departure month e.g. DEC */
	private String month;

	/** Holds the from airport */
	private String boardingAirport;

	/** Holds the part number */
	private String partNumber;

	/** Holds the Key --> to air port(s) value --> totalNoOfPassengers */
	private Map<String,String> mapDestinationAndNoPax;

	/** Holds the day and month e.g 02DEC */
	private String dayMonth;

	/** Hold the pfsContent */
	private String pfsContent;

	/** Hold the date downloaded */
	private Date dateDownloaded;

	/** Hold the from address of Pfs */
	private String fromAddress;

	/** Holds a collection of PFSDestinationDTO */
	private Collection<PFSDestinationDTO> pfsDestinationDTO;

	/** Holds the real departure date and the timestamp */
	private String actualFlightNumber;

	private Date realDepartureDate;

	private Date realDepartureDateZulu;

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the pfsDestinationDTO.
	 */
	public Collection<PFSDestinationDTO> getPfsDestinationDTO() {
		return pfsDestinationDTO;
	}

	/**
	 * @param pfsDestinationDTO
	 *            The pfsDestinationDTO to set.
	 */
	private void setPfsDestinationDTO(Collection<PFSDestinationDTO> pfsDestinationDTO) {
		this.pfsDestinationDTO = pfsDestinationDTO;
	}

	/**
	 * Add pfs destination dtos
	 * 
	 * @param pfsDestinationDTO
	 */
	public void addPfsDestinationDTO(PFSDestinationDTO pfsDestinationDTO) {
		if (this.getPfsDestinationDTO() == null) {
			this.setPfsDestinationDTO(new ArrayList<PFSDestinationDTO>());
		}

		this.getPfsDestinationDTO().add(pfsDestinationDTO);
	}

	/**
	 * @return Returns the pfsContent.
	 */
	public String getPfsContent() {
		return pfsContent;
	}

	/**
	 * @param pfsContent
	 *            The pfsContent to set.
	 */
	public void setPfsContent(String pfsContent) {
		this.pfsContent = pfsContent;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the fromAddress.
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return Returns the dayMonth.
	 */
	public String getDayMonth() {
		return dayMonth;
	}

	/**
	 * @param dayMonth
	 *            The dayMonth to set.
	 */
	public void setDayMonth(String dayMonth) {
		this.dayMonth = dayMonth;

		this.setMonth(dayMonth.substring(dayMonth.length() - 3, dayMonth.length()));
		this.setDay(dayMonth.substring(0, dayMonth.length() - 3));
	}

	/**
	 * Returns the departure date
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Date getDepartureDate() throws ModuleException {
		Calendar calToday = new GregorianCalendar();
		int year = calToday.get(Calendar.YEAR);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
		String tmpdate = year + "-" + this.getMonth() + "-" + this.getDay();

		try {
			return dateFormat.parse(tmpdate);
		} catch (ParseException e) {
			throw new ModuleException("airreservations.pfs.invalidMonthAndDay");
		}
	}

	/**
	 * @return Returns the boardingAirport.
	 */
	public String getBoardingAirport() {
		return boardingAirport;
	}

	/**
	 * @param boardingAirport
	 *            The boardingAirport to set.
	 */
	public void setBoardingAirport(String boardingAirport) {
		this.boardingAirport = boardingAirport;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the partNumber.
	 */
	public String getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return Returns the realDepartureDate.
	 */
	public Date getRealDepartureDate() {
		return realDepartureDate;
	}

	/**
	 * @param realDepartureDate
	 *            The realDepartureDate to set.
	 */
	public void setRealDepartureDate(Date realDepartureDate) {
		this.realDepartureDate = realDepartureDate;
	}

	/**
	 * Add a Destination and no of passengers
	 * 
	 * @param destination
	 * @param noOfPassengers
	 */
	public void addDestination(String destination, String noOfPassengers) {
		if (this.getMapDestinationAndNoPax() == null) {
			this.setMapDestinationAndNoPax(new HashMap<String,String>());
		}

		this.getMapDestinationAndNoPax().put(destination, noOfPassengers);
	}

	/**
	 * @return Returns the mapDestinationAndNoPax.
	 */
	public Map<String,String> getMapDestinationAndNoPax() {
		return mapDestinationAndNoPax;
	}

	/**
	 * @param mapDestinationAndNoPax
	 *            The mapDestinationAndNoPax to set.
	 */
	private void setMapDestinationAndNoPax(Map<String,String> mapDestinationAndNoPax) {
		this.mapDestinationAndNoPax = mapDestinationAndNoPax;
	}

	public String getActualFlightNumber() {
		return actualFlightNumber;
	}

	public void setActualFlightNumber(String actualFlightNumber) {
		this.actualFlightNumber = actualFlightNumber;
	}

	public Date getRealDepartureDateZulu() {
		return realDepartureDateZulu;
	}

	public void setRealDepartureDateZulu(Date realDepartureDateZulu) {
		this.realDepartureDateZulu = realDepartureDateZulu;
	}

}
