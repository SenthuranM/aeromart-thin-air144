package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @hibernate.class table = "T_EXTERNAL_PAYMENT_TX"
 */
public class ExternalPaymentTnx extends Persistent implements Serializable {

	private static final long serialVersionUID = -5690499812292566511L;

	private Integer extPayTxnId;

	private String externalPayId;

	private Integer internalPayId;

	private String balanceQueryKey;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private String externalPayStatus;

	private Date internalTnxStartTimestamp;

	private Date internalTnxEndTimestamp;

	private Date externalTnxEndTimestamp;

	private String pnr;

	private String userId;

	private String channel;

	private String additionalResults;

	private String remarks;

	private String reconcilationStatus;

	private Date reconciledTimestamp;

	private String reconciliationSource;

	private String manualReconBy;

	private String agentCode;

	private Integer paxCount;

	private String externalReqID;

	/**
	 * @hibernate.id column = "EPT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_EXTERNAL_PAYMENT_TX"
	 */
	public Integer getExtPayTxnId() {
		return extPayTxnId;
	}

	public void setExtPayTxnId(Integer extPayTxnId) {
		this.extPayTxnId = extPayTxnId;
	}

	/**
	 * @hibernate.property column = "ADDITIONAL_RESULTS"
	 */
	public String getAdditionalResults() {
		return additionalResults;
	}

	public void setAdditionalResults(String additionalResults) {
		this.additionalResults = additionalResults;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @hibernate.property column = "BAL_QUERY_KEY"
	 */
	public String getBalanceQueryKey() {
		return balanceQueryKey;
	}

	public void setBalanceQueryKey(String balanceQueryKey) {
		this.balanceQueryKey = balanceQueryKey;
	}

	/**
	 * @hibernate.property column = "EXTERNAL_PAY_ID"
	 */
	public String getExternalPayId() {
		return externalPayId;
	}

	public void setExternalPayId(String externalPayId) {
		this.externalPayId = externalPayId;
	}

	/**
	 * @hibernate.property column = "INTERNAL_PAY_ID"
	 */
	public Integer getInternalPayId() {
		return internalPayId;
	}

	public void setInternalPayId(Integer internalPayId) {
		this.internalPayId = internalPayId;
	}

	/**
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "EXTERNAL_PAY_STATUS"
	 */
	public String getExternalPayStatus() {
		return externalPayStatus;
	}

	public void setExternalPayStatus(String externalPayStatus) {
		this.externalPayStatus = externalPayStatus;
	}

	/**
	 * @hibernate.property column = "EXTERNAL_END_TIME"
	 */
	public Date getExternalTnxEndTimestamp() {
		return externalTnxEndTimestamp;
	}

	public void setExternalTnxEndTimestamp(Date externalTnxEndTimestamp) {
		this.externalTnxEndTimestamp = externalTnxEndTimestamp;
	}

	/**
	 * @hibernate.property column = "INTERNAL_END_TIME"
	 */
	public Date getInternalTnxEndTimestamp() {
		return internalTnxEndTimestamp;
	}

	public void setInternalTnxEndTimestamp(Date internalTnxEndTimestamp) {
		this.internalTnxEndTimestamp = internalTnxEndTimestamp;
	}

	/**
	 * @hibernate.property column = "INTERNAL_START_TIME"
	 */
	public Date getInternalTnxStartTimestamp() {
		return internalTnxStartTimestamp;
	}

	public void setInternalTnxStartTimestamp(Date internalTnxStartTimestamp) {
		this.internalTnxStartTimestamp = internalTnxStartTimestamp;
	}

	/**
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column = "CHANNEL"
	 */
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @hibernate.property column = "RECONCILATION_STATUS"
	 */
	public String getReconcilationStatus() {
		return reconcilationStatus;
	}

	public void setReconcilationStatus(String reconcilationStatus) {
		this.reconcilationStatus = reconcilationStatus;
	}

	/**
	 * @hibernate.property column = "RECONCILIATION_TIME"
	 */
	public Date getReconciledTimestamp() {
		return reconciledTimestamp;
	}

	public void setReconciledTimestamp(Date reconciledTimestamp) {
		this.reconciledTimestamp = reconciledTimestamp;
	}

	/**
	 * @hibernate.property column = "RECONCILIATION_SOURCE"
	 */
	public String getReconciliationSource() {
		return reconciliationSource;
	}

	public void setReconciliationSource(String reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
	}

	/**
	 * @hibernate.property column = "MANUAL_RECON_BY"
	 */
	public String getManualReconBy() {
		return manualReconBy;
	}

	public void setManualReconBy(String manualReconBy) {
		this.manualReconBy = manualReconBy;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @hibernate.property column = "PAX_COUNT"
	 */
	public Integer getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(Integer paxCount) {
		this.paxCount = paxCount;
	}

	/**
	 * @hibernate.property column = "EXTERNAL_REQ_ID"
	 */
	public String getExternalReqID() {
		return externalReqID;
	}

	public void setExternalReqID(String externalReqID) {
		this.externalReqID = externalReqID;
	}

	/**
	 * Status to display in front-end.
	 * 
	 * @return
	 */
	public String getDisplayStatus() {
		String displayStatus = "";
		String reconStatus = "";

		if (ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED.equals(getReconcilationStatus())) {
			reconStatus = "Need Recon";
		} else if (ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED.equals(getReconcilationStatus())) {
			reconStatus = "Recon Failed";
		} else if (ReservationInternalConstants.ExtPayTxReconStatus.RECONCILED.equals(getReconcilationStatus())) {
			reconStatus = "Reconciled";
		}

		if (ReservationInternalConstants.ExtPayTxStatus.UNKNOWN.equals(getStatus())) {
			displayStatus = "Unknown";
		} else if (ReservationInternalConstants.ExtPayTxStatus.INITIATED.equals(getStatus())) {
			displayStatus = "Initiated";
		} else if (ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(getStatus())) {
			displayStatus = "Success";
		} else if (ReservationInternalConstants.ExtPayTxStatus.FAILED.equals(getStatus())) {
			displayStatus = "Failed";
		} else if (ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS.equals(getStatus())) {
			displayStatus = "Bank Excess";
		} else if (ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS.equals(getStatus())) {
			displayStatus = "AA Excess";
		} else if (ReservationInternalConstants.ExtPayTxStatus.ABORTED.equals(getStatus())) {
			displayStatus = "Aborted";
		} else if (ReservationInternalConstants.ExtPayTxStatus.REFUNDED.equals(getStatus())) {
			displayStatus = "Refunded";
		} else if (ReservationInternalConstants.ExtPayTxStatus.REVERTED.equals(getStatus())) {
			displayStatus = "Reverted";
		} else if (ReservationInternalConstants.ExtPayTxStatus.BANK_ROLLBACKED.equals(getStatus())) {
			displayStatus = "Bank Reverted";
		}

		displayStatus += ", " + reconStatus;

		return displayStatus;
	}

	/**
	 * Whether to include the record in the front-end display.
	 * 
	 * @return
	 */
	public boolean isEligibleForDisplay() {
		boolean display = false;
		if (ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(getStatus())
				|| ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS.equals(getStatus())
				|| ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS.equals(getStatus())
				|| ReservationInternalConstants.ExtPayTxStatus.REFUNDED.equals(getStatus())
				|| ReservationInternalConstants.ExtPayTxStatus.REVERTED.equals(getStatus())
				|| ReservationInternalConstants.ExtPayTxStatus.UNKNOWN.equals(getStatus())
				|| ReservationInternalConstants.ExtPayTxReconStatus.RECON_FAILED.equals(getReconcilationStatus())) {
			display = true;
		}
		return display;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = System.getProperty("line.separator");

		summary.append("External Payment Txn Summary ::").append(nl);
		summary.append("ID				= ").append(getExtPayTxnId()).append(nl);
		summary.append("PNR				= ").append(getPnr()).append(nl);
		summary.append("BalQueryKey		= ").append(getBalanceQueryKey()).append(nl);
		summary.append("AA Status		= ").append(getStatus()).append(nl);
		summary.append("Bank Ref		= ").append(getExternalPayId()).append(nl);
		summary.append("Bank Status		= ").append(getExternalPayStatus()).append(nl);
		summary.append("AA Txn ID		= ").append(getInternalPayId()).append(nl);
		summary.append("Amount			= ").append(getAmount()).append(nl);
		summary.append("Channel Code	= ").append(getChannel()).append(nl);
		summary.append("User ID			= ").append(getUserId()).append(nl);
		summary.append("AA Start Time	= ").append(getInternalTnxStartTimestamp()).append(nl);
		summary.append("Bank Txn Time  	= ").append(getExternalTnxEndTimestamp()).append(nl);
		summary.append("AA End Time		= ").append(getInternalTnxEndTimestamp()).append(nl);
		summary.append("Recon Status 	= ").append(getReconcilationStatus()).append(nl);
		summary.append("Remarks			= ").append(getRemarks()).append(nl);
		summary.append("Additional Info = ").append(getAdditionalResults()).append(nl);
		summary.append("Recon Time 		= ").append(getReconciledTimestamp()).append(nl);
		summary.append("Recon Source 	= ").append(getReconciliationSource()).append(nl).append(nl);
		summary.append("Pax Count	 	= ").append(getPaxCount()).append(nl);
		summary.append("External Request ID 	= ").append(getExternalReqID()).append(nl);

		return summary;
	}

}
