/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class AirportPassengerMessageTxHsitory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3702504407235243725L;

	private int flightNumber;

	private String airportCode;

	private String messageType;

	private Timestamp transmissionTimeStamp;

	private String transmissionStatus;

	private String sitaAddress;

	private String email;

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;

	}

	public String getTransmissionStatus() {
		return transmissionStatus;
	}

	public void setTransmissionStatus(String transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public String getSitaAddress() {
		return sitaAddress;
	}

	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	public Timestamp getTransmissionTimeStamp() {
		return transmissionTimeStamp;
	}

	public void setTransmissionTimeStamp(Timestamp transmissionTimeStamp) {
		this.transmissionTimeStamp = transmissionTimeStamp;
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
