package com.isa.thinair.airreservation.api.utils;

import java.util.Collection;

public class AuthorizationsUtil {
	/**
	 * Returns true if the privilegeToCheck to check is included in the privilegesKeys collection.
	 */
	public static boolean hasPrivilege(Collection<String> privilegesKeys, String privilegeToCheck) {
		return (privilegesKeys != null) ? privilegesKeys.contains(privilegeToCheck) : false;
	}

	public static boolean hasPrivileges(Collection<String> privilegesKeys, String... privilegesToCheck) {
		for (String privilegeToCheck : privilegesToCheck) {
			if (!hasPrivilege(privilegesKeys, privilegeToCheck))
				return false;
		}
		return true;
	}

}
