/*
 * =============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * To keep track of Reservation Administrator information
 * 
 * @author Nilindra Fernando
 * @since 10:47 AM 2/11/2010
 */
public class ReservationAdminInfoTO implements Serializable {

	private static final long serialVersionUID = 2786828939173861773L;

	/** Holds origin agent code */
	private String originAgentCode;

	/** Override the origin agent code */
	private boolean overrideOriginAgentCode;

	/** Holds origin user id */
	private String originUserId;

	/** Override the origin user id */
	private boolean overrideOriginUserId;

	/** Holds owner agent code */
	private String ownerAgentCode;

	/** Override the owner agent code */
	private boolean overrideOwnerAgentCode;

	/** Holds owner channel id */
	private Integer ownerChannelId;

	/** Override the owner channel id */
	private boolean overrideOwnerChannelId;

	/** Holds origin sales terminal */
	private String originSalesTerminal;

	/** Holds terminal for last modification */
	private String lastSalesTerminal;

	private int originChannelId;

	private boolean overrideOriginChannelId;

	/**
	 * @return the ownerAgentCode
	 */
	public String getOwnerAgentCode() {
		return ownerAgentCode;
	}

	/**
	 * @param ownerAgentCode
	 *            the ownerAgentCode to set
	 */
	public void setOwnerAgentCode(String ownerAgentCode) {
		this.ownerAgentCode = ownerAgentCode;
	}

	/**
	 * @return the originSalesTerminal
	 */
	public String getOriginSalesTerminal() {
		return originSalesTerminal;
	}

	/**
	 * @param originSalesTerminal
	 *            the originSalesTerminal to set
	 */
	public void setOriginSalesTerminal(String originSalesTerminal) {
		this.originSalesTerminal = originSalesTerminal;
	}

	/**
	 * @return the lastSalesTerminal
	 */
	public String getLastSalesTerminal() {
		return lastSalesTerminal;
	}

	/**
	 * @param lastSalesTerminal
	 *            the lastSalesTerminal to set
	 */
	public void setLastSalesTerminal(String lastSalesTerminal) {
		this.lastSalesTerminal = lastSalesTerminal;
	}

	/**
	 * @return the overrideOwnerAgentCode
	 */
	public boolean isOverrideOwnerAgentCode() {
		return overrideOwnerAgentCode;
	}

	/**
	 * @param overrideOwnerAgentCode
	 *            the overrideOwnerAgentCode to set
	 */
	public void setOverrideOwnerAgentCode(boolean overrideOwnerAgentCode) {
		this.overrideOwnerAgentCode = overrideOwnerAgentCode;
	}

	/**
	 * @return the overrideOwnerChannelId
	 */
	public boolean isOverrideOwnerChannelId() {
		return overrideOwnerChannelId;
	}

	/**
	 * @param overrideOwnerChannelId
	 *            the overrideOwnerChannelId to set
	 */
	public void setOverrideOwnerChannelId(boolean overrideOwnerChannelId) {
		this.overrideOwnerChannelId = overrideOwnerChannelId;
	}

	/**
	 * @return the ownerChannelId
	 */
	public Integer getOwnerChannelId() {
		return ownerChannelId;
	}

	/**
	 * @param ownerChannelId
	 *            the ownerChannelId to set
	 */
	public void setOwnerChannelId(Integer ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	/**
	 * @return the originAgentCode
	 */
	public String getOriginAgentCode() {
		return originAgentCode;
	}

	/**
	 * @param originAgentCode
	 *            the originAgentCode to set
	 */
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	/**
	 * @return the overrideOriginAgentCode
	 */
	public boolean isOverrideOriginAgentCode() {
		return overrideOriginAgentCode;
	}

	/**
	 * @param overrideOriginAgentCode
	 *            the overrideOriginAgentCode to set
	 */
	public void setOverrideOriginAgentCode(boolean overrideOriginAgentCode) {
		this.overrideOriginAgentCode = overrideOriginAgentCode;
	}

	/**
	 * @return the originUserId
	 */
	public String getOriginUserId() {
		return originUserId;
	}

	/**
	 * @param originUserId
	 *            the originUserId to set
	 */
	public void setOriginUserId(String originUserId) {
		this.originUserId = originUserId;
	}

	/**
	 * @return the overrideOriginUserId
	 */
	public boolean isOverrideOriginUserId() {
		return overrideOriginUserId;
	}

	/**
	 * @param overrideOriginUserId
	 *            the overrideOriginUserId to set
	 */
	public void setOverrideOriginUserId(boolean overrideOriginUserId) {
		this.overrideOriginUserId = overrideOriginUserId;
	}

	public int getOriginChannelId() {
		return originChannelId;
	}

	public void setOriginChannelId(int originChannelId) {
		this.originChannelId = originChannelId;
	}

	public boolean isOverrideOriginChannelId() {
		return overrideOriginChannelId;
	}

	public void setOverrideOriginChannelId(boolean overrideOriginChannelId) {
		this.overrideOriginChannelId = overrideOriginChannelId;
	}

}
