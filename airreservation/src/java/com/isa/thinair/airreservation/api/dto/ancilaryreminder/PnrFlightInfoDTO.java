package com.isa.thinair.airreservation.api.dto.ancilaryreminder;

import java.util.Date;

public class PnrFlightInfoDTO {

	private String pnr;
	private Date firstSegDepDate;
	private Date secondSegDepDate;

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the firstSegDepDate
	 */
	public Date getFirstSegDepDate() {
		return firstSegDepDate;
	}

	/**
	 * @param firstSegDepDate
	 *            the firstSegDepDate to set
	 */
	public void setFirstSegDepDate(Date firstSegDepDate) {
		this.firstSegDepDate = firstSegDepDate;
	}

	/**
	 * @return the secondSegDepDate
	 */
	public Date getSecondSegDepDate() {
		return secondSegDepDate;
	}

	/**
	 * @param secondSegDepDate
	 *            the secondSegDepDate to set
	 */
	public void setSecondSegDepDate(Date secondSegDepDate) {
		this.secondSegDepDate = secondSegDepDate;
	}

}
