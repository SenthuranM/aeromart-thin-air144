package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * @author eric
 * 
 */
public class CreateTicketInfoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Holds the payment agent code */
	private String paymentAgent;

	/** Holds the application type */
	private AppIndicatorEnum appIndicator;

	/** Holds the whther bsp payment */
	private boolean isBSPPayment;

	public String getPaymentAgent() {
		return paymentAgent;
	}

	public void setPaymentAgent(String paymentAgent) {
		this.paymentAgent = paymentAgent;
	}

	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	public boolean isBSPPayment() {
		return isBSPPayment;
	}

	public void setBSPPayment(boolean isBSPPayment) {
		this.isBSPPayment = isBSPPayment;
	}

	@Override
	public String toString() {
		return "CreateTicketInfoDTO [paymentAgent=" + paymentAgent + ", appIndicator=" + appIndicator + ", isBSPPayment="
				+ isBSPPayment + "]";
	}
}
