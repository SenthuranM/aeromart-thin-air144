package com.isa.thinair.airreservation.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ServiceTaxExtChgDTO extends ExternalChgDTO {
	private static final long serialVersionUID = 8451703768086528370L;

	private String flightRefNumber;

	private boolean ticketingRevenue;

	private BigDecimal taxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal nonTaxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private String taxDepositStateCode;
	
	private String taxDepositCountryCode;
	
	private boolean isServiceTaxAppliedToCCFee;

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public boolean isTicketingRevenue() {
		return ticketingRevenue;
	}

	public void setTicketingRevenue(boolean ticketingRevenue) {
		this.ticketingRevenue = ticketingRevenue;
	}

	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public BigDecimal getNonTaxableAmount() {
		return nonTaxableAmount;
	}

	public void setNonTaxableAmount(BigDecimal nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}
	
	

	public String getTaxDepositStateCode() {
		return taxDepositStateCode;
	}

	public void setTaxDepositStateCode(String taxDepositStateCode) {
		this.taxDepositStateCode = taxDepositStateCode;
	}

	public String getTaxDepositCountryCode() {
		return taxDepositCountryCode;
	}

	public void setTaxDepositCountryCode(String taxDepositCountryCode) {
		this.taxDepositCountryCode = taxDepositCountryCode;
	}
	
	public boolean isServiceTaxAppliedToCCFee() {
		return isServiceTaxAppliedToCCFee;
	}

	public void setServiceTaxAppliedToCCFee(boolean isServiceTaxAppliedToCCFee) {
		this.isServiceTaxAppliedToCCFee = isServiceTaxAppliedToCCFee;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		ServiceTaxExtChgDTO clone = new ServiceTaxExtChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setFlightRefNumber(this.getFlightRefNumber());
		clone.setTicketingRevenue(this.isTicketingRevenue());
		clone.setTaxableAmount(this.getTaxableAmount());
		clone.setNonTaxableAmount(this.getNonTaxableAmount());
		clone.setTaxDepositCountryCode(this.getTaxDepositCountryCode());
		clone.setTaxDepositStateCode(this.getTaxDepositStateCode());
		clone.setServiceTaxAppliedToCCFee(this.isServiceTaxAppliedToCCFee());

		return clone;
	}

}
