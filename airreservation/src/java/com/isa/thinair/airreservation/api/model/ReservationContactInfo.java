/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 * 
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of Reservation Contact information.
 * 
 * @author Nilindra Fernando, Rumesh Nadeera
 * @since 1.0
 * @hibernate.class table = "T_RESERVATION_CONTACT"
 */
public class ReservationContactInfo extends Persistent implements Serializable {

	private static final long serialVersionUID = 4427800820831634226L;

	/** Holds the customer id */
	private Integer customerId;

	/** Holds the marketing carrier customer id */
	private Integer marketingCarrierCustomerId;

	/** Holds the customer title */
	private String title;

	/** Holds the customer first name */
	private String firstName;

	/** Holds the customer last name */
	private String lastName;

	/** Holds the customer upper first name */
	private String upperFirstName ;

	/** Holds the customer upper last name */
	private String upperLastName ;

	/** Holds the customer phone number */
	private String phoneNo;

	/** Holds the customer mobile number */
	private String mobileNo;

	/** Holds the customer fax number */
	private String fax;

	/** Holds the customer email address */
	private String email;

	/** Holds the customer nationality code */
	private String nationalityCode;

	/** Holds the customer code */
	private String countryCode;

	/** Holds street address field 1 */
	private String streetAddress1;

	/** Holds street address field 2 */
	private String streetAddress2;

	/** Holds city */
	private String city;

	/** Holds state */
	private String state;

	/** Holds Zip Code */
	private String zipCode;

	/** Holds PNR */
	private String pnr;

	/** Holds preferred language */
	private String preferredLanguage;
	
	/** Holds the Tax Registration Number */
	private String taxRegNo;

	/*
	 * Emergency contact information
	 */
	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnPhoneNo;

	private String emgnEmail;
	
	/** Holds value to send promo email */
	private Boolean sendPromoEmail;

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "C_COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the marketingCarrierCustomerId
	 * @hibernate.property column = "MC_CUSTOMER_ID"
	 */
	public Integer getMarketingCarrierCustomerId() {
		return marketingCarrierCustomerId;
	}

	/**
	 * @param marketingCarrierCustomerId
	 *            the marketingCarrierCustomerId to set
	 */
	public void setMarketingCarrierCustomerId(Integer marketingCarrierCustomerId) {
		this.marketingCarrierCustomerId = marketingCarrierCustomerId;
	}

	/**
	 * @return Returns the email.
	 * @hibernate.property column = "C_EMAIL"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the fax.
	 * @hibernate.property column = "C_FAX"
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax
	 *            The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "C_FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;

		 if (!BeanUtils.nullHandler(firstName).equals("")) {
			 this.setUpperFirstName(firstName.toUpperCase());
		 }
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "C_LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;

		 if (!BeanUtils.nullHandler(lastName).equals("")) {
			 this.setUpperLastName(lastName.toUpperCase());
		 }
	}

	/**
	 * @return Returns the mobileNo.
	 * @hibernate.property column = "C_MOBILE_NO"
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            The mobileNo to set.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return Returns the nationalityCode.
	 * @hibernate.property column = "C_NATIONALITY_CODE"
	 */
	public String getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 *            The nationalityCode to set.
	 */
	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return Returns the phoneNo.
	 * @hibernate.property column = "C_PHONE_NO"
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo
	 *            The phoneNo to set.
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return Returns the streetAddress1.
	 * @hibernate.property column = "C_STREET_ADDRESS_1"
	 */
	public String getStreetAddress1() {
		return streetAddress1;
	}

	/**
	 * @param streetAddress1
	 *            The streetAddress1 to set.
	 */
	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	/**
	 * @return Returns the streetAddress2.
	 * @hibernate.property column = "C_STREET_ADDRESS_2"
	 */
	public String getStreetAddress2() {
		return streetAddress2;
	}

	/**
	 * @param streetAddress2
	 *            The streetAddress2 to set.
	 */
	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	/**
	 * @return Returns the state.
	 * @hibernate.property column = "C_STATE"
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zipCode
	 * @hibernate.property column = "C_ZIP_CODE"
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return Returns the city.
	 * @hibernate.property column = "C_CITY"
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "C_TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the upperLastName.
	 * @hibernate.property column = "UPPER_C_LAST_NAME"
	 */
	@SuppressWarnings("unused")
	private String getUpperLastName() {
		return upperLastName;
	}

	/**
	 * @param upperLastName
	 *            The upperLastName to set.
	 */
	private void setUpperLastName(String upperLastName) {
		this.upperLastName = upperLastName;
	}

	/**
	 * @return Returns the upperFirstName.
	 * @hibernate.property column = "UPPER_C_FIRST_NAME"
	 */
	@SuppressWarnings("unused")
	private String getUpperFirstName() {
		return upperFirstName;
	}

	/**
	 * @param upperFirstName
	 *            The upperFirstName to set.
	 */
	private void setUpperFirstName(String upperFirstName) {
		this.upperFirstName = upperFirstName;
	}

	/**
	 * @return the pnr
	 * @hibernate.id column = "PNR" generator-class = "assigned"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the emgnTitle
	 * @hibernate.property column = "E_TITLE"
	 */
	public String getEmgnTitle() {
		return emgnTitle;
	}

	/**
	 * @param emgnTitle
	 *            the emgnTitle to set
	 */
	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	/**
	 * @return the emgnFirstName
	 * @hibernate.property column = "E_FIRST_NAME"
	 */
	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	/**
	 * @param emgnFirstName
	 *            the emgnFirstName to set
	 */
	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	/**
	 * @return the emgnLastName
	 * @hibernate.property column = "E_LAST_NAME"
	 */
	public String getEmgnLastName() {
		return emgnLastName;
	}

	/**
	 * @param emgnLastName
	 *            the emgnLastName to set
	 */
	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	/**
	 * @return the emgnPhoneNo
	 * @hibernate.property column = "E_PHONE_NO"
	 */
	public String getEmgnPhoneNo() {
		return emgnPhoneNo;
	}

	/**
	 * @param emgnPhoneNo
	 *            the emgnPhoneNo to set
	 */
	public void setEmgnPhoneNo(String emgnPhoneNo) {
		this.emgnPhoneNo = emgnPhoneNo;
	}

	/**
	 * @return the emgnEmail
	 * @hibernate.property column = "E_EMAIL"
	 */
	public String getEmgnEmail() {
		return emgnEmail;
	}

	/**
	 * @param emgnEmail
	 *            the emgnEmail to set
	 */
	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}

	/**
	 * @return the preferredLanguage
	 * @hibernate.property column = "PREFERRED_LANG"
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage
	 *            the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	
	/**
	 * @return the tax registration number
	 * @hibernate.property column = "TAX_REG_NUMBER"
	 */
	public String getTaxRegNo() {
		return taxRegNo;
	}

	public void setTaxRegNo(String taxRegNo) {
		this.taxRegNo = taxRegNo;
	}
	
	/**
	 * @return the send promo email check
	 * @hibernate.property column = "SEND_PROMO_EMAIL" type = "yes_no"
	 */	
	public Boolean getSendPromoEmail() {
		return sendPromoEmail;
	}

	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}
	
	public ReservationContactInfo clone() {
		ReservationContactInfo contactInfo = new ReservationContactInfo();
		contactInfo.setCity(this.city);
		contactInfo.setCountryCode(this.countryCode);
		contactInfo.setCustomerId(this.customerId);
		contactInfo.setEmail(this.email);
		contactInfo.setFax(this.fax);
		contactInfo.setFirstName(this.firstName);
		contactInfo.setLastName(this.lastName);
		contactInfo.setMarketingCarrierCustomerId(this.marketingCarrierCustomerId);
		contactInfo.setMobileNo(this.mobileNo);
		contactInfo.setNationalityCode(this.nationalityCode);
		contactInfo.setPhoneNo(this.phoneNo);
		contactInfo.setPreferredLanguage(this.preferredLanguage);
		contactInfo.setState(this.state);
		contactInfo.setStreetAddress1(this.streetAddress1);
		contactInfo.setStreetAddress2(this.streetAddress2);
		contactInfo.setTitle(this.title);
		contactInfo.setVersion(-1);
		contactInfo.setZipCode(this.zipCode);
		contactInfo.setEmgnEmail(this.emgnEmail);
		contactInfo.setEmgnFirstName(this.emgnFirstName);
		contactInfo.setEmgnLastName(this.emgnLastName);
		contactInfo.setEmgnPhoneNo(this.emgnPhoneNo);
		contactInfo.setEmgnTitle(this.emgnTitle);
		contactInfo.setTaxRegNo(this.taxRegNo);
		contactInfo.setSendPromoEmail(this.sendPromoEmail);
		return contactInfo;
	}

	
}
