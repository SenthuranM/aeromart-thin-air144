package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Util to handle the reservation split for dummy and actual bookings in AAservisers
 * 
 * @author malaka
 * 
 */
public class ReservationSplitUtil {

	/**
	 * Updates the new external segment with the new PNR
	 * 
	 * @param oldReservation
	 * @param newReservation
	 * @param isInfantOnlySplit
	 */
	public static void composeNewExternalSegments(Reservation oldReservation, Reservation newReservation,
			boolean isInfantOnlySplit) {
		ReservationSegmentDAO reservationSegmentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
		List<ExternalPnrSegment> externalSegments = reservationSegmentDAO.getExternalSegmentInformationByPnr(oldReservation
				.getPnr());

		for (ExternalPnrSegment reservationExternalSegment : externalSegments) {

			ExternalPnrSegment externalPnrSegment = new ExternalPnrSegment();
			ExternalFlightSegment externalFlightSegment = new ExternalFlightSegment();
			externalFlightSegment.setFlightNumber(reservationExternalSegment.getExternalFlightSegment().getFlightNumber());
			externalFlightSegment.setEstTimeDepatureLocal(reservationExternalSegment.getExternalFlightSegment()
					.getEstTimeDepatureLocal());
			externalFlightSegment.setEstTimeArrivalLocal(reservationExternalSegment.getExternalFlightSegment()
					.getEstTimeArrivalLocal());
			externalFlightSegment.setSegmentCode(reservationExternalSegment.getExternalFlightSegment().getSegmentCode());
			externalPnrSegment.setExternalFlightSegment(externalFlightSegment);

			externalPnrSegment.setStatus(reservationExternalSegment.getStatus());
			externalPnrSegment.setSubStatus(reservationExternalSegment.getSubStatus());

			/*
			 * For only infant splitting we can't take the cabin class or logical cabin class for an infant. Since all
			 * fare information and quotes are done for adults only.
			 */
			externalPnrSegment.setCabinClassCode(isInfantOnlySplit ? "" : reservationExternalSegment.getCabinClassCode());
			externalPnrSegment.setLogicalCabinClassCode(isInfantOnlySplit ? "" : reservationExternalSegment
					.getLogicalCabinClassCode());

			externalPnrSegment.setSegmentSeq(reservationExternalSegment.getSegmentSeq());

			externalPnrSegment.setExternalCarrierCode(reservationExternalSegment.getExternalCarrierCode());
			externalPnrSegment.setExternalFltSegId(reservationExternalSegment.getExternalFltSegId());
			externalPnrSegment.setExternalPnrSegId(reservationExternalSegment.getExternalPnrSegId());

			externalPnrSegment.setStatusModifiedDate(new Date());
			externalPnrSegment.setStatusModifiedSalesChannelCode(reservationExternalSegment.getStatusModifiedSalesChannelCode());
			externalPnrSegment.setMarketingAgentCode(reservationExternalSegment.getMarketingAgentCode());
			externalPnrSegment.setMarketingStationCode(reservationExternalSegment.getMarketingStationCode());
			externalPnrSegment.setMarketingCarrierCode(reservationExternalSegment.getMarketingCarrierCode());

			newReservation.addExternalReservationSegment(externalPnrSegment);
		}
	}

	/**
	 * Find whether an ond is cancelled
	 * 
	 * @param reservationPaxFare
	 * @return
	 */
	public static boolean isOndCancelled(ReservationPaxFare reservationPaxFare) {
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;

		int i = 0;
		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationPaxFareSegment.getSegment()
					.getStatus())) {
				i++;
			}
		}

		if (reservationPaxFare.getPaxFareSegments().size() == i) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check split constraints
	 * 
	 * @param reservation
	 * @param pnrPaxIds
	 * @param version
	 * @throws ModuleException
	 */
	public static void checkSplitConstraints(Reservation reservation, Collection<Integer> pnrPaxIds, Long version)
			throws ModuleException {
		// Checking to see if any concurrent update exist
		ReservationCoreUtils.checkReservationVersionCompatibility(reservation, version.longValue());

		int paxSize = reservation.getPassengers().size();

		// Passenger count can not be less than or equal to the split passenger count
		if (paxSize <= pnrPaxIds.size()) {
			throw new ModuleException("airreservations.arg.invalidSplit");
		}

		Collection<Integer> currentPaxIds = new ArrayList<Integer>();
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();
			currentPaxIds.add(reservationPax.getPnrPaxId());
		}

		// If invalid passenger(s) exist from the input
		if (!currentPaxIds.containsAll(pnrPaxIds)) {
			throw new ModuleException("airreservations.arg.invalidSplit");
		}
	}

	/**
	 * Find whether ond is flown
	 * 
	 * @param reservationPaxFare
	 * @param reservation
	 * @return
	 */
	public static boolean isOndFlown(ReservationPaxFare reservationPaxFare, Reservation reservation) {
		Collection<Integer> colFlownPnrSegmentIds = new ArrayList<Integer>();

		Iterator<ReservationSegmentDTO> itReservationSegmentDTO = reservation.getSegmentsView().iterator();
		ReservationSegmentDTO reservationSegmentDTO;
		Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();

		while (itReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

			// If segment is confirmed
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {

				// Capture flown pnr segment ids
				if (currentDate.compareTo(reservationSegmentDTO.getZuluDepartureDate()) > 0) {
					colFlownPnrSegmentIds.add(reservationSegmentDTO.getPnrSegId());
				}
			}
		}

		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;

		int i = 0;
		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

			// If segment is confirmed and flown
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationPaxFareSegment.getSegment()
					.getStatus())) {
				if (!colFlownPnrSegmentIds.isEmpty()
						&& colFlownPnrSegmentIds.contains(reservationPaxFareSegment.getSegment().getPnrSegId())) {
					i++;
				}
			}
		}

		if (reservationPaxFare.getPaxFareSegments().size() == i) {
			return true;
		} else {
			return false;
		}
	}

	public static void mapGroundSegmentsforNewReservation(Reservation newReservation, Reservation oldReservation) {

		Map<Integer, ReservationSegment> mapOldSegements = getSegmentMapWithPnrSegID(oldReservation);
		Map<Integer, ReservationSegment> mapNewSegements = getSegmentMapWithPnrSegID(newReservation);
		/**
		 * 
		 */
		Map<Integer, Integer> mapOldSegmentSequenceMapping = getOldGroundSegSequenceMapping(mapOldSegements);

		Map<Integer, Integer> mapNewSequenceAndPnrSegID = getewSequenceAndPnrSegID(newReservation);

		Collection<ReservationSegment> colReservationSegments = mapNewSegements.values();

		for (ReservationSegment reservationSegment : colReservationSegments) {
			// If ground segment exists
			if (mapOldSegmentSequenceMapping.containsKey(reservationSegment.getSegmentSeq())) {
				if (mapNewSequenceAndPnrSegID.containsKey(mapOldSegmentSequenceMapping.get(reservationSegment.getSegmentSeq()))) {
					ReservationSegmentDAO reservationSegmentDAO = (ReservationSegmentDAO) ReservationDAOUtils.DAOInstance.RESERVATION_SEGMENT_DAO;
					ReservationSegment newReservationSegment = reservationSegmentDAO.getReservationSegment(reservationSegment
							.getPnrSegId());
					ReservationSegment groundReservationSegment = reservationSegmentDAO
							.getReservationSegment(mapNewSequenceAndPnrSegID.get(mapOldSegmentSequenceMapping
									.get(reservationSegment.getSegmentSeq())));
					newReservationSegment.setGroundSegment(groundReservationSegment);
					reservationSegmentDAO.saveReservationSegment(newReservationSegment);
				}

			}
		}
	}

	private static Map<Integer, Integer> getewSequenceAndPnrSegID(Reservation newReservation) {
		Map<Integer, Integer> mapNewSequenceAndPnrSegID = new HashMap<Integer, Integer>();
		Collection<ReservationSegment> colReservationSegments = newReservation.getSegments();
		for (ReservationSegment reservationSegment : colReservationSegments) {
			mapNewSequenceAndPnrSegID.put(reservationSegment.getSegmentSeq(), reservationSegment.getPnrSegId());
		}
		return mapNewSequenceAndPnrSegID;
	}

	/**
	 * Returns sequnce no + mapped sequence number
	 * 
	 * @param mapOldSegements
	 * @return
	 */
	private static Map<Integer, Integer> getOldGroundSegSequenceMapping(Map<Integer, ReservationSegment> mapOldSegements) {

		Map<Integer, Integer> mapSequenceAndID = new HashMap<Integer, Integer>();
		Collection<ReservationSegment> colReservationSegments = mapOldSegements.values();

		for (ReservationSegment reservationSegment : colReservationSegments) {
			if (reservationSegment != null && reservationSegment.getGroundSegment() != null) {
				mapSequenceAndID.put(reservationSegment.getSegmentSeq(),
						mapOldSegements.get(reservationSegment.getGroundSegment().getPnrSegId()).getSegmentSeq());
			}
		}

		return mapSequenceAndID;
	}

	private static Map<Integer, ReservationSegment> getSegmentMapWithPnrSegID(Reservation newReservation) {
		Map<Integer, ReservationSegment> mapReservationSegments = new HashMap<Integer, ReservationSegment>();
		Collection<ReservationSegment> reservationSegments = newReservation.getSegments();
		for (ReservationSegment reservationSegment : reservationSegments) {
			mapReservationSegments.put(reservationSegment.getPnrSegId(), reservationSegment);
		}

		return mapReservationSegments;
	}

	/**
	 * Removes a parent/infant or adult from reservation and updates it
	 * 
	 * @param reservation
	 * @param reservationPax
	 * @param movingPassengers
	 */
	public static void removePassenger(Reservation reservation, ReservationPax passenger, Set<ReservationPax> movingPassengers)
			throws ModuleException {

		// Remember in here we only get Parents and Adults only
		// Parent
		if (ReservationApiUtils.isParentAfterSave(passenger)) {
			// Removes the Parent
			reservation.removePassenger(passenger);

			// Updates the adult count
			updateAdultCount(reservation, false);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, false);

			Iterator<ReservationPax> itReservationPax = passenger.getInfants().iterator();
			ReservationPax infant;

			while (itReservationPax.hasNext()) {
				infant = (ReservationPax) itReservationPax.next();
				// Remove the infant
				reservation.removePassenger(infant);

				// Updates the infant count
				updateInfantCount(reservation, false);

				// Updates the reservation fare and charges
				updatePnrFareAndChgs(reservation, infant, false);
			}
		}
		// Adult
		else if (ReservationApiUtils.isSingleAfterSave(passenger)) {
			// Removes the adult
			reservation.removePassenger(passenger);

			// Updates the adult count
			updateAdultCount(reservation, false);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, false);
		}
		// Child
		else if (ReservationApiUtils.isChildType(passenger)) {
			// Removes the child
			reservation.removePassenger(passenger);

			// Updates the child count
			updateChildCount(reservation, false);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, false);
		}

		// Adds the new parent
		movingPassengers.add(passenger);
	}

	/**
	 * Adds the parent or/and infant to the reservation and updates it
	 * 
	 * @param reservation
	 * @param passenger
	 * @throws ModuleException
	 */
	public static void addPassenger(Reservation reservation, ReservationPax passenger) throws ModuleException {

		// Parent
		if (ReservationApiUtils.isParentAfterSave(passenger)) {
			// Add the Parent
			reservation.addPassenger(passenger);

			// Updates the adult count
			updateAdultCount(reservation, true);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, true);

			Iterator<ReservationPax> itReservationPax = passenger.getInfants().iterator();
			ReservationPax infant;

			while (itReservationPax.hasNext()) {
				infant = (ReservationPax) itReservationPax.next();

				// Add the infant
				reservation.addPassenger(infant);

				// Updates the infant count
				updateInfantCount(reservation, true);

				// Updates the reservation fare and charges
				updatePnrFareAndChgs(reservation, infant, true);
			}
		}
		// Adult
		else if (ReservationApiUtils.isSingleAfterSave(passenger)) {
			// add the adult
			reservation.addPassenger(passenger);

			// Updates the adult count
			updateAdultCount(reservation, true);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, true);
		}
		// Child
		else if (ReservationApiUtils.isChildType(passenger)) {
			// add the child
			reservation.addPassenger(passenger);

			// Updates the child count
			updateChildCount(reservation, true);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, true);
		}
		// Isolated Infant exist
		else if (ReservationApiUtils.isInfantType(passenger) && passenger.getParent() == null) {
			// Removes the adult
			reservation.addPassenger(passenger);

			// Updates the infant count
			updateInfantCount(reservation, true);

			// Updates the reservation fare and charges
			updatePnrFareAndChgs(reservation, passenger, true);
		}
	}

	/**
	 * Update the Adult count
	 * 
	 * @param reservation
	 * @param increment
	 *            the increment mode - true for increment false for decrement
	 */
	private static void updateAdultCount(Reservation reservation, boolean increment) {
		int adultCount = 0;
		if (increment) {
			adultCount = reservation.getTotalPaxAdultCount() + 1;
		} else {
			adultCount = reservation.getTotalPaxAdultCount() - 1;
		}
		reservation.setTotalPaxAdultCount(adultCount);
	}

	/**
	 * Update the child count
	 * 
	 * @param reservation
	 * @param increment
	 *            the increment mode - true for increment false for decrement
	 */
	private static void updateChildCount(Reservation reservation, boolean increment) {
		int childCount = 0;
		if (increment) {
			childCount = reservation.getTotalPaxChildCount() + 1;
		} else {
			childCount = reservation.getTotalPaxChildCount() - 1;
		}
		reservation.setTotalPaxChildCount(childCount);
	}

	/**
	 * Update the Infant count
	 */
	public static void updateInfantCount(Reservation reservation, boolean increment) {
		int infantCount = 0;
		if (increment) {
			infantCount = reservation.getTotalPaxInfantCount() + 1;
		} else {
			infantCount = reservation.getTotalPaxInfantCount() - 1;
		}
		reservation.setTotalPaxInfantCount(infantCount);
	}

	/**
	 * Updates the reservation status
	 * 
	 * @param reservation
	 */
	public static void updatePnrStatus(Reservation reservation) {
		Iterator<ReservationPax> iterator = reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		int i = 0;

		while (iterator.hasNext()) {
			reservationPax = (ReservationPax) iterator.next();

			if (reservationPax.getStatus().equals(ReservationPaxStatus.ON_HOLD)) {
				reservation.setStatus(ReservationStatus.ON_HOLD);
				break;
			} else if (reservationPax.getStatus().equals(ReservationPaxStatus.CANCEL)) {
				reservation.setStatus(ReservationStatus.CANCEL);
				break;
			} else if (reservationPax.getStatus().equals(ReservationPaxStatus.CONFIRMED)) {
				i++;
			}
		}

		if (reservation.getPassengers().size() == i) {
			reservation.setStatus(ReservationStatus.CONFIRMED);
		}
	}

	/**
	 * Updates the reservation external segment status
	 * 
	 * @param reservation
	 */
	public static void updateExternalSegmentStatus(Reservation reservation) {
		if (reservation.getExternalReservationSegment() != null) {
			for (Iterator<ExternalPnrSegment> iterator = reservation.getExternalReservationSegment().iterator(); iterator
					.hasNext();) {
				ExternalPnrSegment extPnrSeg = (ExternalPnrSegment) iterator.next();
				extPnrSeg.setStatus(ReservationSegmentStatus.CANCEL);
			}
		}
	}

	/**
	 * Updates the reservation charges
	 * 
	 * @param reservation
	 * @param reservationPax
	 * @param accumulate
	 */
	public static void updatePnrFareAndChgs(Reservation reservation, ReservationPax reservationPax, boolean accumulate) {
		// Reservation object total charges will automatically will get updated
		// Since it's object reference
		ReservationCoreUtils.updateTotals(reservation, reservationPax.getTotalChargeAmounts(), accumulate);
	}

	/**
	 * Returns the segment map Creates a segment map with Key - Segment Sequence Value - Pnr Segment Id
	 * 
	 * The main purpose of this method is to create hibernate reference
	 * 
	 * @param oldReservation
	 * @return oldSegment map
	 */
	public static Map<Integer, Integer> getSegmentMap(Reservation oldReservation) {
		Map<Integer, Integer> oldSegmentMap = new HashMap<Integer, Integer>();
		Iterator<ReservationSegment> itReservationSegment = oldReservation.getSegments().iterator();
		ReservationSegment reservationSegment;

		while (itReservationSegment.hasNext()) {
			reservationSegment = (ReservationSegment) itReservationSegment.next();
			oldSegmentMap.put(reservationSegment.getSegmentSeq(), reservationSegment.getPnrSegId());
		}

		return oldSegmentMap;
	}

	/**
	 * Adds the new segment mapping
	 * 
	 * @param passenger
	 * @param newSegmentMap
	 */
	public static void addNewSegmentMappings(ReservationPax passenger, Map<Integer, ReservationSegment> newSegmentMap) {
		// Parent
		if (ReservationApiUtils.isParentAfterSave(passenger)) {
			Iterator<ReservationPax> itInfants = passenger.getInfants().iterator();
			ReservationPax infant;
			while (itInfants.hasNext()) {
				infant = (ReservationPax) itInfants.next();
				mapNewSegments(infant, newSegmentMap);
			}

			mapNewSegments(passenger, newSegmentMap);
		}
		// Adult
		else if (ReservationApiUtils.isSingleAfterSave(passenger)) {
			mapNewSegments(passenger, newSegmentMap);
		}
		// Child
		else if (ReservationApiUtils.isChildType(passenger)) {
			mapNewSegments(passenger, newSegmentMap);
		}
		// Infant
		else if (ReservationApiUtils.isInfantType(passenger)) {
			mapNewSegments(passenger, newSegmentMap);
		}
		
	}

	/**
	 * Map New Segments
	 * 
	 * @param passenger
	 * @param newSegmentMap
	 */
	private static void mapNewSegments(ReservationPax passenger, Map<Integer, ReservationSegment> newSegmentMap) {
		Iterator<ReservationPaxFare> itPnrPaxFares = passenger.getPnrPaxFares().iterator();
		Iterator<ReservationPaxFareSegment> itPnrPaxFareSeg;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itPnrPaxFares.hasNext()) {
			reservationPaxFare = (ReservationPaxFare) itPnrPaxFares.next();
			itPnrPaxFareSeg = reservationPaxFare.getPaxFareSegments().iterator();

			while (itPnrPaxFareSeg.hasNext()) {
				reservationPaxFareSegment = (ReservationPaxFareSegment) itPnrPaxFareSeg.next();

				if (reservationPaxFareSegment.getPnrSegId() != null) {
					reservationPaxFareSegment.setSegment((ReservationSegment) newSegmentMap.get(reservationPaxFareSegment
							.getPnrSegId()));
				}
			}
		}
	}

	/**
	 * Return the first passenger's last Name
	 * 
	 * @param newReservation
	 * @return
	 */
	public static String getFirstPassengerLastName(Reservation newReservation) {
		List<Integer> lstPaxSequence = new ArrayList<Integer>();
		Iterator<ReservationPax> itReservationPax = newReservation.getPassengers().iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				lstPaxSequence.add(reservationPax.getPaxSequence());
			}
		}

		// Sorts based on the passenger sequence
		Collections.sort(lstPaxSequence);

		Iterator<Integer> itLstPaxSequence = lstPaxSequence.iterator();
		Integer paxSequenceId = null;
		if (itLstPaxSequence.hasNext()) {
			paxSequenceId = (Integer) itLstPaxSequence.next();
		}

		itReservationPax = newReservation.getPassengers().iterator();
		String lastName = "";

		if (paxSequenceId != null) {
			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();

				if (!ReservationApiUtils.isInfantType(reservationPax)) {
					if (reservationPax.getPaxSequence().equals(paxSequenceId)) {
						lastName = reservationPax.getLastName();
						break;
					}
				}
			}
		}

		return lastName;
	}

	/**
	 * Remove the infant
	 * 
	 * @param infant
	 * @param movingPassengers
	 */
	public static void moveInfant(ReservationPax infant, Set<ReservationPax> movingPassengers) {
		// Removing the infant from the old reservation
		infant.getReservation().removePassenger(infant);

		// Remove the parent
		ReservationPax parent = infant.getParent();
		infant.removeParent(parent);

		// Add the isolated infant to the new reservation
		movingPassengers.add(infant);
	}

	/**
	 * Returns the Action Code
	 * 
	 * @param isPaxSplit
	 * @return
	 */
	public static int getActionCode(Boolean isPaxSplit) {
		if (isPaxSplit.booleanValue()) {
			return ReservationTnxNominalCode.PAX_CANCEL.getCode();
		} else {
			return ReservationTnxNominalCode.AIR_LINE_CANCEL.getCode();
		}
	}

}
