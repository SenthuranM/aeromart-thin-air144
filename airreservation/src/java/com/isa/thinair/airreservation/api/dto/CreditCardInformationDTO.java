package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class CreditCardInformationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5109595774902520826L;
	private int id;
	private String cardType;
	private int paymentNominalCode;
	private int refundNominalCode;

	/**
	 * @return Returns the refundNominalCode.
	 */
	public int getRefundNominalCode() {
		return refundNominalCode;
	}

	/**
	 * @param refundNominalCode
	 *            The refundNominalCode to set.
	 */
	public void setRefundNominalCode(int refundNominalCode) {
		this.refundNominalCode = refundNominalCode;
	}

	/**
	 * @return Returns the paymentNominalCode.
	 */
	public int getPaymentNominalCode() {
		return paymentNominalCode;
	}

	/**
	 * @param paymentNominalCode
	 *            The paymentNominalCode to set.
	 */
	public void setPaymentNominalCode(int paymentNominalCode) {
		this.paymentNominalCode = paymentNominalCode;
	}

	/**
	 * @return Returns the cardType.
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            The cardType to set.
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}

}
