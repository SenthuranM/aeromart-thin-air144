/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.meal;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

/**
 * To hold reservation external charges data transfer information
 * 
 * @author thushara
 */
public class MealExternalChgDTO extends ExternalChgDTO {

	private static final long serialVersionUID = -3428414375607766976L;

	private Integer paxSequenceNo;

	private Integer flightMealId;

	private BigDecimal mealCharge;

	private Integer pnrPaxId;

	private Integer pnrPaxFareId;

	private Integer pnrSegId;

	private Integer mealId;

	private String flightRPH;

	private String mealCode;

	private Integer mealChargeID;

	private boolean isAnciOffer;

	private Integer offeredTemplateID;

	/**
	 * @return the paxSequenceNo
	 */
	public Integer getPaxSequenceNo() {
		return paxSequenceNo;
	}

	/**
	 * @param paxSequenceNo
	 *            the paxSequenceNo to set
	 */
	public void setPaxSequenceNo(Integer paxSequenceNo) {
		this.paxSequenceNo = paxSequenceNo;
	}

	/**
	 * @return the flightMealId
	 */
	public Integer getFlightMealId() {
		return flightMealId;
	}

	/**
	 * @param flightMealId
	 *            the flightMealId to set
	 */
	public void setFlightMealId(Integer flightMealId) {
		this.flightMealId = flightMealId;
	}

	/**
	 * @return the mealCharge
	 */
	public BigDecimal getMealCharge() {
		return mealCharge;
	}

	/**
	 * @param mealCharge
	 *            the mealCharge to set
	 */
	public void setMealCharge(BigDecimal mealCharge) {
		this.mealCharge = mealCharge;
	}

	/**
	 * Clones the object
	 */
	@Override
	public Object clone() {
		MealExternalChgDTO clone = new MealExternalChgDTO();
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChgGrpCode(this.getChgGrpCode());
		clone.setChgRateId(this.getChgRateId());
		clone.setExternalChargesEnum(this.getExternalChargesEnum());
		clone.setAmount(this.getAmount());
		clone.setRatioValueInPercentage(this.isRatioValueInPercentage());
		clone.setRatioValue(this.getRatioValue());
		clone.setAmountConsumedForPayment(this.isAmountConsumedForPayment());

		clone.setPaxSequenceNo(this.getPaxSequenceNo());
		clone.setFlightSegId(this.getFlightSegId());
		clone.setFlightMealId(this.getFlightMealId());
		clone.setAnciOffer(this.isAnciOffer);
		clone.setMealId(this.getMealId());
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setOfferedTemplateID(this.offeredTemplateID);

		return clone;
	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the pnrPaxFareId
	 */
	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	/**
	 * @param pnrPaxFareId
	 *            the pnrPaxFareId to set
	 */
	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	/**
	 * @return the pnrSegId
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            the pnrSegId to set
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	public String getFlightRPH() {
		return flightRPH;
	}

	public void setFlightRPH(String flightRPH) {
		this.flightRPH = flightRPH;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the mealChargeID
	 */
	public Integer getMealChargeID() {
		return mealChargeID;
	}

	/**
	 * @param mealChargeID
	 *            the mealChargeID to set
	 */
	public void setMealChargeID(Integer mealChargeID) {
		this.mealChargeID = mealChargeID;
	}

	/**
	 * @return the offeredTemplateID
	 */
	public Integer getOfferedTemplateID() {
		return offeredTemplateID;
	}

	/**
	 * @param offeredTemplateID
	 *            the offeredTemplateID to set
	 */
	public void setOfferedTemplateID(Integer offeredTemplateID) {
		this.offeredTemplateID = offeredTemplateID;
	}
}
