/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.adl.comparator;

import java.util.Comparator;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare;

/**
 * @author udithad
 *
 */
public class CompDestinationFareClass implements Comparator<DestinationFare> {
    @Override
    public int compare(DestinationFare arg0, DestinationFare arg1) {
    
    	int ret=arg0.getFareClass().compareTo(arg1.getFareClass());
    	
    	return ret;
    }
}
