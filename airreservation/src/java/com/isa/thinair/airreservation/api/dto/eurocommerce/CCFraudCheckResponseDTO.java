package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

public class CCFraudCheckResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2768323023492196080L;

	private TransactionResultDTO result;

	private FraudStatus status;

	private String message;

	private String ref;

	private int score;

	private String resultCode;

	public TransactionResultDTO getResult() {
		return result;
	}

	public void setResult(TransactionResultDTO result) {
		this.result = result;
	}

	public FraudStatus getStatus() {
		return status;
	}

	public void setStatus(FraudStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	@Override
	public String toString() {
		return "ProcessPaymentResponseDTO [ status=" + status + ",message=" + message + ", ref=" + ref + "]";
	}
}
