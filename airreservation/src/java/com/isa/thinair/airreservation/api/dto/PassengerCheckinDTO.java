package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class PassengerCheckinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pnr;

	private Integer pnrPaxId;

	private String passengerTitle;

	private String passengerName;

	private String origin;

	private String segmentCode;

	private String paxCheckinStatus;

	private String passengerType;

	private String classOfService;

	private String bookingClass;

	private String bcType;

	private String seatCode;

	private String ssr;

	private String ssrText;

	private String ppfsStatus;

	private Integer ppfs_id;

	private boolean isUnpaid;

	private String infantPax;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getPassengerTitle() {
		return passengerTitle;
	}

	public void setPassengerTitle(String passengerTitle) {
		this.passengerTitle = passengerTitle;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getPaxCheckinStatus() {
		return paxCheckinStatus;
	}

	public void setPaxCheckinStatus(String paxCheckinStatus) {
		this.paxCheckinStatus = paxCheckinStatus;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public String getSsr() {
		return ssr;
	}

	public void setSsr(String ssr) {
		this.ssr = ssr;
	}

	public String getSsrText() {
		return ssrText;
	}

	public void setSsrText(String ssrText) {
		this.ssrText = ssrText;
	}

	public String getPpfsStatus() {
		return ppfsStatus;
	}

	public void setPpfsStatus(String ppfsStatus) {
		this.ppfsStatus = ppfsStatus;
	}

	public String getBcType() {
		return bcType;
	}

	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	public Integer getPpfs_id() {
		return ppfs_id;
	}

	public void setPpfs_id(Integer ppfsId) {
		ppfs_id = ppfsId;
	}

	/**
	 * @return the isUnpaid
	 */
	public boolean isUnpaid() {
		return isUnpaid;
	}

	/**
	 * @param isUnpaid
	 *            the isUnpaid to set
	 */
	public void setUnpaid(boolean isUnpaid) {
		this.isUnpaid = isUnpaid;
	}

	public String getInfantPax() {
		return infantPax;
	}

	public void setInfantPax(String infantPax) {
		this.infantPax = infantPax;
	}

}
