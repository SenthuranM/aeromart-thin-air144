package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author eric
 * @hibernate.class table = "T_PAX_E_TICKET" lazy="false"
 */
public class ReservationPaxETicket extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String PURCHASED = "PCH";
	public static final String EXCHANGED = "ECH";

	private Integer paxETicketId;

	/** Holds reservation passenger id */
	private Integer pnrPaxId;

	private String eticketNumber;

	private String eTicketStatus;

	/**
	 * @hibernate.id column = "PAX_E_TICKET_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PAX_E_TICKET"
	 */
	public Integer getPaxETicketId() {
		return paxETicketId;
	}

	/**
	 * @param paxETicketId
	 */
	public void setPaxETicketId(Integer paxETicketId) {
		this.paxETicketId = paxETicketId;
	}

	/**
	 * @return
	 * @hibernate.property column = "E_TICKET_NUMBER"
	 */
	public String getEticketNumber() {
		return eticketNumber;
	}

	/**
	 * @param eticketNumber
	 */
	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	/**
	 * @return
	 * @hibernate.property column ="E_TICKET_STATUS"
	 * 
	 */
	public String geteTicketStatus() {
		return eTicketStatus;
	}

	/**
	 * @param eTicketStatus
	 */
	public void seteTicketStatus(String eTicketStatus) {
		this.eTicketStatus = eTicketStatus;
	}

	/**
	 * @return
	 * @hibernate.property column ="PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}



}
