package com.isa.thinair.airreservation.api.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Reserved flight load information to send for cargo planning.
 * @author vidula
 *
 * @hibernate.class table = "T_FLIGHT_LOAD"
 */
public class FlightLoad extends Persistent {

	private static final long serialVersionUID = 166306193446998236L;
	
	public static final String FLIGHT_LOAD_PUBLISH_STATUS_PENDING = "PENDING";
	public static final String FLIGHT_LOAD_PUBLISH_STATUS_SUCCESS = "SUCCESS";
	public static final String FLIGHT_LOAD_PUBLISH_STATUS_FAILED = "FAILED";
	
	private Integer flightLoadId;
	private Integer flightId;
	private String flightNo;
	private Date departureDate;
	private Integer notificationId;
	private String status;
	private Set<FlightLegLoad> flightLegLoad;
	
	public FlightLoad() {
		
	}
	
	public FlightLoad(Integer flightId, String flightNo, Date departureDate, Integer notificationId) {
		this.flightId = flightId;
		this.flightNo = flightNo;
		this.departureDate = departureDate;
		this.notificationId = notificationId;
		this.status = FLIGHT_LOAD_PUBLISH_STATUS_PENDING;
		this.flightLegLoad = new HashSet<FlightLegLoad>();
	}

	public void addLegLoadInformation(Integer flightLegId, String origin, String destination, Date departureTimeZulu,
			Date arrivalTimeZulu, int maleCount, int femaleCount, int childCount, int infantCount, double baggageLoad) {
		flightLegLoad.add(new FlightLegLoad(flightLegId, origin, destination, departureTimeZulu, arrivalTimeZulu,
				maleCount, femaleCount, childCount, infantCount, baggageLoad));
	}

	/**
	 * @hibernate.id column = "FLIGHT_LOAD_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_LOAD"
	 */
	public Integer getFlightLoadId() {
		return flightLoadId;
	}

	public void setFlightLoadId(Integer flightLoadId) {
		this.flightLoadId = flightLoadId;
	}

	/**
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}
	
	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @hibernate.property column = "NOTIFICATION_ID"
	 */
	public Integer getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}

	/**
	 * @hibernate.property column = "NOTIFICATION_STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.FlightLegLoad"
	 * @hibernate.collection-key column="FLIGHT_LOAD_ID"
	 */
	public Set<FlightLegLoad> getFlightLegLoad() {
		return flightLegLoad;
	}

	public void setFlightLegLoad(Set<FlightLegLoad> flightLegLoad) {
		this.flightLegLoad = flightLegLoad;
	}

	@Override
	public String toString() {
		return "FlightLoad [flightLoadId=" + flightLoadId + ", flightId="
				+ flightId + ", flightNo=" + flightNo + ", departureDate="
				+ departureDate + ", notificationId=" + notificationId
				+ ", status=" + status + ", flightLegLoad=" + flightLegLoad
				+ "]";
	}

}
