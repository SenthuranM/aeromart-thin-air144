/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Holds the payment detail specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaymentDetailDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2196731500430694659L;

	/** Holds the passenger id */
	private Integer pnrPaxId;

	/** Holds the passenger name */
	private String passengerName;

	/** Holds the payment date */
	private Date paymentDate;

	/** Holds the authorization id */
	private String authorizationId;

	/** Holds the charge amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the payment currency code */
	private String payCurrencyCode;

	/** Holds the payment currency amount */
	private BigDecimal payCurrencyAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds the nominal code */
	private Integer nominalCode;

	/** Holds the transaction id */
	private Integer tnxId;

	/** Holds the last 4 digits of credit card number */
	private String last4DigitsOfCCNo;

	private String recieptNumber;

	private String paymentMode;

	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * @param paymentMode
	 *            the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the recieptNumber
	 */
	public String getRecieptNumber() {
		return recieptNumber;
	}

	/**
	 * @param recieptNumber
	 *            the recieptNumber to set
	 */
	public void setRecieptNumber(String recieptNumber) {
		this.recieptNumber = recieptNumber;
	}

	/**
	 * Returns payment amount as a String format value
	 * 
	 * @param numberFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getPaymentAmountAsString(String numberFormat) throws ModuleException {
		return BeanUtils.parseNumberFormat(Math.abs(this.getAmount().doubleValue()), numberFormat);
	}

	/**
	 * @return Returns the authorizationId.
	 */
	public String getAuthorizationId() {
		return authorizationId;
	}

	/**
	 * @param authorizationId
	 *            The authorizationId to set.
	 */
	public void setAuthorizationId(String authorizationId) {
		this.authorizationId = authorizationId;
	}

	/**
	 * @return Returns the passengerName.
	 */
	public String getPassengerName() {
		return passengerName;
	}

	/**
	 * @param passengerName
	 *            The passengerName to set.
	 */
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	/**
	 * @return Returns the paymentDate.
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}

	/**
	 * Returns payment date as string format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getPaymentStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getPaymentDate(), dateFormat);
	}

	/**
	 * @param paymentDate
	 *            The paymentDate to set.
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return Returns the paymentType description
	 */
	public String getPaymentTypeDescription() {
		return ReservationTnxNominalCode.getDescription(this.getNominalCode().intValue());
	}

	/**
	 * @return Returns the pnrPaxId.
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return Returns the nominalCode.
	 */
	public Integer getNominalCode() {
		return nominalCode;
	}

	/**
	 * @param nominalCode
	 *            The nominalCode to set.
	 */
	public void setNominalCode(Integer nominalCode) {
		this.nominalCode = nominalCode;
	}

	/**
	 * @return Returns the tnxId.
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	/**
	 * @param tnxId
	 *            The tnxId to set.
	 */
	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * @return Returns the last4DigitsOfCCNo.
	 */
	public String getLast4DigitsOfCCNo() {
		return last4DigitsOfCCNo;
	}

	/**
	 * @param last4DigitsOfCCNo
	 *            The last4DigitsOfCCNo to set.
	 */
	public void setLast4DigitsOfCCNo(String last4DigitsOfCCNo) {
		this.last4DigitsOfCCNo = last4DigitsOfCCNo;
	}

	/**
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Returns payment amount as a String format value
	 * 
	 * @param numberFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getPaymentCurrencyAmountAsString(String numberFormat) throws ModuleException {
		if (this.getPayCurrencyAmount() != null)
			return this.getPayCurrencyAmount().abs().toString();
		else
			return "";
	}

	/**
	 * @return the payCurrencyCode
	 */
	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	/**
	 * @param payCurrencyCode
	 *            the payCurrencyCode to set
	 */
	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	/**
	 * @return the payCurrencyAmount
	 */
	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	/**
	 * @param payCurrencyAmount
	 *            the payCurrencyAmount to set
	 */
	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}
}
