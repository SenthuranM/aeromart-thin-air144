package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import java.util.ArrayList;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegmentGroup;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class Group3 extends EDISegmentGroup {

	protected TKT tkt;
	protected MON mon;
	protected DAT dat;

	protected ArrayList<Group4> group4 = new ArrayList<Group4>();

	public Group3() {
		super("Group3");
	}

	public void setTkt(TKT tkt) {
		this.tkt = tkt;
	}

	public void setMon(MON mon) {
		this.mon = mon;
	}

	public void setDat(DAT dat) {
		this.dat = dat;
	}

	public void addGroup4(Group4 group4) {
		this.group4.add(group4);
	}

	@Override
	public MessageSegment build() {
		addMessageSegmentIfNotEmpty(tkt);
		addMessageSegmentIfNotEmpty(mon);
		addMessageSegmentIfNotEmpty(dat);
		addMessageSegmentsIfNotEmpty(group4);
		return this;
	}

}
