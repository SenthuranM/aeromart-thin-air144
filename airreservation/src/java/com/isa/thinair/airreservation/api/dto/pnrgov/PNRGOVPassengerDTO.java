package com.isa.thinair.airreservation.api.dto.pnrgov;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class PNRGOVPassengerDTO {
	private Integer pnrPaxId;

	private String title;

	private String firstName;

	private String lastName;

	private String passengerType;

	private Integer adultID;

	private Date dateOfBirth;
	
	private String passengerNationality;

	private int paxReference;

	private Set<PNRGOVPassengerDTO> intants = new HashSet<PNRGOVPassengerDTO>();

	private Set<String> passengerEticket;

	private PNRGOVPaymentDTO passengerPayment;
	
	private String passportNumber;
	
	private Date passportExpiry;
	
	private String passportIssueCountry;
	
	private String placeOfBirth;
	
	private String travelDocumentType;
	
	private String visaDocumentNumber;
	
	private String visaPlaceOfIssue;
	
	private Date visaDateOfIssue;
	
	private String visaApplicableCountry;

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		if (firstName != null && !firstName.equals("")) {
			return firstName.replaceAll("\\s+","");
		}
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPnrGovPassengerType() {
		if (this.passengerType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
			return "A";
		} else if (this.passengerType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
			return "C";
		} else {
			return "IN";
		}
	}

	public String getPassengerType() {
		return this.passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public String getAirImpStandardSurName() {
		String surName = this.getLastName();
		if (surName != null && !surName.equals("")) {
			surName = surName.replaceAll("\\s+","");
		}
		if (surName != null && surName.length() == 1) {
			surName += surName;
		}
		return surName;
	}

	public String getgivenName() {
		String givenName = this.getFirstName() + this.getTitle();
		return givenName;
	}

	public PNRGOVPaymentDTO getPassengerPayment() {
		return passengerPayment;
	}

	public void setPassengerPayment(PNRGOVPaymentDTO passengerPayment) {
		this.passengerPayment = passengerPayment;
	}

	public Set<String> getPassengerEticket() {
		return passengerEticket;
	}

	public void setPassengerEticket(Set<String> passengerEticket) {
		this.passengerEticket = passengerEticket;
	}

	public int getPaxReference() {
		return paxReference;
	}

	public void setPaxReference(int paxReference) {
		this.paxReference = paxReference;
	}

	public Integer getAdultID() {
		return adultID;
	}

	public void setAdultID(Integer adultID) {
		this.adultID = adultID;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Set<PNRGOVPassengerDTO> getIntants() {
		return intants;
	}

	public void addIntant(PNRGOVPassengerDTO intants) {
		this.intants.add(intants);
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Date getPassportExpiry() {
		return passportExpiry;
	}

	public void setPassportExpiry(Date passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	public String getPassportIssueCountry() {
		return passportIssueCountry;
	}

	public void setPassportIssueCountry(String passportIssueCountry) {
		this.passportIssueCountry = passportIssueCountry;
	}

	public String getPassengerNationality() {
		return passengerNationality;
	}

	public void setPassengerNationality(String passengerNationality) {
		this.passengerNationality = passengerNationality;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getTravelDocumentType() {
		return travelDocumentType;
	}

	public void setTravelDocumentType(String travelDocumentType) {
		this.travelDocumentType = travelDocumentType;
	}

	public String getVisaDocumentNumber() {
		return visaDocumentNumber;
	}

	public void setVisaDocumentNumber(String visaDocumentNumber) {
		this.visaDocumentNumber = visaDocumentNumber;
	}

	public String getVisaPlaceOfIssue() {
		return visaPlaceOfIssue;
	}

	public void setVisaPlaceOfIssue(String visaPlaceOfIssue) {
		this.visaPlaceOfIssue = visaPlaceOfIssue;
	}

	public Date getVisaDateOfIssue() {
		return visaDateOfIssue;
	}

	public void setVisaDateOfIssue(Date visaDateOfIssue) {
		this.visaDateOfIssue = visaDateOfIssue;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}
	
}
