package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * class with common properties of BlacklistPAX Rules.
 * 
 * @author rajiv
 * 
 * @hibernate.class table = "T_BL_RULES"
 *
 */
public class BlacklistPAXRule extends Tracking {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ID field for Blacklist PAX rule Id
	 */
	private Long ruleId;

	/**
	 * ID field for rule name
	 */
	private String label;

	/**
	 * check field for firstname
	 */
//	private String firstNameChecked;

	/**
	 * check field for lastname
	 */
//	private String laststNameChecked;

	/**
	 * check field for pax name
	 */	
	private String paxFullNameChecked;
	
	/**
	 * check field for date of birth
	 */
	private String dateOfBirthChecked;

	/**
	 * check field for passport no
	 */
	private String passportNoChecked;

	/**
	 * check field for nationality
	 */
	private String nationalityChecked;

	/**
	 * check field for nationality
	 */
	private String status;

	/**
	 * @return Returns the ruleId.
	 * 
	 * @hibernate.id column="BL_RULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BL_RULES"
	 */
	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * @hibernate.property column = "LABEL"
	 * 
	 * @see #label
	 */
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "FIRST_NAME"
	 * 
	 * @see #firstNameChecked
	 */
	/*public String getFirstNameChecked() {
		return firstNameChecked;
	}

	public void setFirstNameChecked(String firstNameChecked) {
		this.firstNameChecked = firstNameChecked;
	}*/

	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "LAST_NAME"
	 * 
	 * @see #laststNameChecked
	 */
/*	public String getLaststNameChecked() {
		return laststNameChecked;
	}

	public void setLaststNameChecked(String laststNameChecked) {
		this.laststNameChecked = laststNameChecked;
	}*/
	
	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "PAX_NAME"
	 * 
	 * @see #paxFullNameChecked
	 */
	public String getPaxFullNameChecked() {
		return paxFullNameChecked;
	}

	public void setPaxFullNameChecked(String paxFullNameChecked) {
		this.paxFullNameChecked = paxFullNameChecked;
	}

	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "DOB"
	 * 
	 * @see #dateOfBirthChecked
	 */
	public String getDateOfBirthChecked() {
		return dateOfBirthChecked;
	}

	public void setDateOfBirthChecked(String dateOfBirthChecked) {
		this.dateOfBirthChecked = dateOfBirthChecked;
	}

	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 * 
	 * @see #passportNoChecked
	 */
	public String getPassportNoChecked() {
		return passportNoChecked;
	}

	public void setPassportNoChecked(String passportNoChecked) {
		this.passportNoChecked = passportNoChecked;
	}

	/**
	 * @return Returns the whether this is the default terminal. values : {"Y" : Yes, "N" : No}
	 * 
	 * @hibernate.property column = "NATIONALITY"
	 * 
	 * @see #nationalityChecked
	 */
	public String getNationalityChecked() {
		return nationalityChecked;
	}

	public void setNationalityChecked(String nationalityChecked) {
		this.nationalityChecked = nationalityChecked;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * 
	 * @see #status
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
