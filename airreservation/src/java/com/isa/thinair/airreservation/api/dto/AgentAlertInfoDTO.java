package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class AgentAlertInfoDTO implements Serializable, Comparable<AgentAlertInfoDTO> {

	private static final long serialVersionUID = 1L;

	private String pnr;

	private String contactName;

	private String contactEMail;

	private String contactPhoneNo;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEMail() {
		return contactEMail;
	}

	public void setContactEMail(String contactEMail) {
		this.contactEMail = contactEMail;
	}

	public String getContactPhoneNo() {
		return contactPhoneNo;
	}

	public void setContactPhoneNo(String contactPhoneNo) {
		this.contactPhoneNo = contactPhoneNo;
	}

	@Override
	public int compareTo(AgentAlertInfoDTO o) {
		return this.pnr.compareTo(o.getPnr());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentAlertInfoDTO other = (AgentAlertInfoDTO) obj;
		if (pnr == null) {
			if (other.pnr != null)
				return false;
		} else if (!pnr.equals(other.pnr))
			return false;
		return true;
	}

}
