package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author dilan
 *
 * @hibernate.class table = "T_PAX_TRNX_SEGMENT"
 */
public class TransactionSegment extends Persistent implements Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer txnSegId;
	private Integer tnxId;
	private Integer pnrSegId;
	private Integer fltSegId;

	/**
	 * 
	 * @hibernate.id column = "TXN_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PAX_TRNX_SEGMENT"
	 */
	public Integer getTxnSegId() {
		return txnSegId;
	}

	public void setTxnSegId(Integer txnSegId) {
		this.txnSegId = txnSegId;
	}

	/**
	 * 
	 * @hibernate.property column = "TXN_ID"
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}

	/**
	 * 
	 * @hibernate.property column = "PNR_SEG_ID"
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * 
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFltSegId() {
		return fltSegId;
	}

	public void setFltSegId(Integer fltSegId) {
		this.fltSegId = fltSegId;
	}

	public TransactionSegment clone() {
		TransactionSegment clone = new TransactionSegment();
		clone.setFltSegId(this.getFltSegId());
		clone.setTnxId(this.getTnxId());
		clone.setPnrSegId(this.getPnrSegId());
		return clone;
	}

}
