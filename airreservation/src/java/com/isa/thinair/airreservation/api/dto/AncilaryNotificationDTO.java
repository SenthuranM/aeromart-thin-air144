package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRResultDTO;

/***
 * 
 * Common DTO used in ancillary notification emails
 * 
 */
public class AncilaryNotificationDTO implements Serializable {

	private static final long serialVersionUID = -5045915759553089986L;

	private String pnr;
	private Integer sSRCatId;
	private Locale locale;
	private String firstName;
	private String lastName;
	private String carrierCode;
	private String mailSubject;
	private String mailTitle;
	private NotificationType notificationType;
	private Date departureDate = null;

	private Collection<ReservationSSRResultDTO> addedSSRDTOs;
	private Collection<ReservationSSRResultDTO> canceledSSRDTOs;

	public static enum NotificationType {
		ADD, CANCEL
	}

	public String getPnr() {
		return this.pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getSSRCatId() {
		return this.sSRCatId;
	}

	public void setSSRCatId(Integer catId) {
		this.sSRCatId = catId;
	}

	public Locale getLocale() {
		return this.locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCarrierCode() {
		return this.carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getMailSubject() {
		return this.mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailTitle() {
		return this.mailTitle;
	}

	public void setMailTitle(String mailTitle) {
		this.mailTitle = mailTitle;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public Collection<ReservationSSRResultDTO> getAddedSSRDTOs() {
		return this.addedSSRDTOs;
	}

	public void setAddedSSRDTOs(Collection<ReservationSSRResultDTO> addedSSRDTOs) {
		this.addedSSRDTOs = addedSSRDTOs;
	}

	public Collection<ReservationSSRResultDTO> getCanceledSSRDTOs() {
		return this.canceledSSRDTOs;
	}

	public void setCanceledSSRDTOs(Collection<ReservationSSRResultDTO> canceledSSRDTOs) {
		this.canceledSSRDTOs = canceledSSRDTOs;
	}

	public Date getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

}
