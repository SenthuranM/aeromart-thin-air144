package com.isa.thinair.airreservation.api.dto;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;

/**
 * @author Manoj Dhanushka
 */
public class TypeBRequestDTO {
	
	private Reservation reservation;
	
	private Integer gdsNotifyAction;
	
	private Map<Integer, NameDTO> changedPaxNamesMap;
	
	private Collection<Integer> cnxPnrSegIds;
	
	private Collection<Integer> cnxInversePnrSegIds;
	
	private Collection<Integer> newlyAddedFlightSegIds;
	
	private Collection<Integer> exchangedPnrSegIds;
	
	private List<NameDTO> splitPaxNames;
	
	private List<SSRInfantDTO> splitedInfants;
	
	private String newPnr;
	
	private Collection<Integer> addInfantPnrPaxIds;
	
	private Map<Integer, SegmentSSRAssembler> ssrAssemblerMap;
	
	private Map<Integer, ReservationPaxAdditionalInfo> additionalInfoChangedPaxMap;
	
	private Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps;
	
	private Date releaseTimeStamp;
	
	private ReservationContactInfo originalContInfo;
	
	private String userNote;
	
	private FlightSegmentDTO reprotectedSourceFltSeg;
	
	private FlightSegmentDTO reprotectedTargetFltSeg;
	
	private Integer reprotectedPnrSegId;
	
	private List<Integer> transferedSegmentIds;
	
	private String csOCCarrierCode;
	
	private Map<Integer, Map<Integer, EticketDTO>> eTicketInfo;
	
	private Map<Integer, Map<Integer, EticketDTO>> cancelledETicketInfo;
	
	List<TransferSegmentDTO> transferSegmentDTOs;

	/**
	 * @return the reservation
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @return the changedPaxNamesMap
	 */
	public Map<Integer, NameDTO> getChangedPaxNamesMap() {
		return changedPaxNamesMap;
	}

	/**
	 * @param changedPaxNamesMap the changedPaxNamesMap to set
	 */
	public void setChangedPaxNamesMap(Map<Integer, NameDTO> changedPaxNamesMap) {
		this.changedPaxNamesMap = changedPaxNamesMap;
	}

	/**
	 * @return the cnxPnrSegIds
	 */
	public Collection<Integer> getCnxPnrSegIds() {
		return cnxPnrSegIds;
	}

	/**
	 * @param cnxPnrSegIds the cnxPnrSegIds to set
	 */
	public void setCnxPnrSegIds(Collection<Integer> cnxPnrSegIds) {
		this.cnxPnrSegIds = cnxPnrSegIds;
	}

	/**
	 * @return the cnxInversePnrSegIds
	 */
	public Collection<Integer> getCnxInversePnrSegIds() {
		return cnxInversePnrSegIds;
	}

	/**
	 * @param cnxInversePnrSegIds the cnxInversePnrSegIds to set
	 */
	public void setCnxInversePnrSegIds(Collection<Integer> cnxInversePnrSegIds) {
		this.cnxInversePnrSegIds = cnxInversePnrSegIds;
	}

	/**
	 * @return the newlyAddedFlightSegIds
	 */
	public Collection<Integer> getNewlyAddedFlightSegIds() {
		return newlyAddedFlightSegIds;
	}

	/**
	 * @param newlyAddedFlightSegIds the newlyAddedFlightSegIds to set
	 */
	public void setNewlyAddedFlightSegIds(Collection<Integer> newlyAddedFlightSegIds) {
		this.newlyAddedFlightSegIds = newlyAddedFlightSegIds;
	}

	/**
	 * @return the splitPaxNames
	 */
	public List<NameDTO> getSplitPaxNames() {
		return splitPaxNames;
	}

	/**
	 * @param splitPaxNames the splitPaxNames to set
	 */
	public void setSplitPaxNames(List<NameDTO> splitPaxNames) {
		this.splitPaxNames = splitPaxNames;
	}

	/**
	 * @return the gdsNotifyAction
	 */
	public Integer getGdsNotifyAction() {
		return gdsNotifyAction;
	}

	/**
	 * @param gdsNotifyAction the gdsNotifyAction to set
	 */
	public void setGdsNotifyAction(Integer gdsNotifyAction) {
		this.gdsNotifyAction = gdsNotifyAction;
	}

	/**
	 * @return the newPnr
	 */
	public String getNewPnr() {
		return newPnr;
	}

	/**
	 * @param newPnr the newPnr to set
	 */
	public void setNewPnr(String newPnr) {
		this.newPnr = newPnr;
	}

	/**
	 * @return the addInfantPnrPaxIds
	 */
	public Collection<Integer> getAddInfantPnrPaxIds() {
		return addInfantPnrPaxIds;
	}

	/**
	 * @param addInfantPnrPaxIds the addInfantPnrPaxIds to set
	 */
	public void setAddInfantPnrPaxIds(Collection<Integer> addInfantPnrPaxIds) {
		this.addInfantPnrPaxIds = addInfantPnrPaxIds;
	}

	/**
	 * @return the ssrAssemblerMap
	 */
	public Map<Integer, SegmentSSRAssembler> getSsrAssemblerMap() {
		return ssrAssemblerMap;
	}

	/**
	 * @param ssrAssemblerMap the ssrAssemblerMap to set
	 */
	public void setSsrAssemblerMap(Map<Integer, SegmentSSRAssembler> ssrAssemblerMap) {
		this.ssrAssemblerMap = ssrAssemblerMap;
	}

	/**
	 * @return the additionalInfoChangedPaxMap
	 */
	public Map<Integer, ReservationPaxAdditionalInfo> getAdditionalInfoChangedPaxMap() {
		return additionalInfoChangedPaxMap;
	}

	/**
	 * @param additionalInfoChangedPaxMap the additionalInfoChangedPaxMap to set
	 */
	public void setAdditionalInfoChangedPaxMap(Map<Integer, ReservationPaxAdditionalInfo> additionalInfoChangedPaxMap) {
		this.additionalInfoChangedPaxMap = additionalInfoChangedPaxMap;
	}

	/**
	 * @return the releaseTimeStamp
	 */
	public Date getReleaseTimeStamp() {
		return releaseTimeStamp;
	}

	/**
	 * @param releaseTimeStamp the releaseTimeStamp to set
	 */
	public void setReleaseTimeStamp(Date releaseTimeStamp) {
		this.releaseTimeStamp = releaseTimeStamp;
	}

	/**
	 * @return the originalContInfo
	 */
	public ReservationContactInfo getOriginalContInfo() {
		return originalContInfo;
	}

	/**
	 * @param originalContInfo the originalContInfo to set
	 */
	public void setOriginalContInfo(ReservationContactInfo originalContInfo) {
		this.originalContInfo = originalContInfo;
	}

	/**
	 * @return the splitedInfants
	 */
	public List<SSRInfantDTO> getSplitedInfants() {
		return splitedInfants;
	}

	/**
	 * @param splitedInfants the splitedInfants to set
	 */
	public void setSplitedInfants(List<SSRInfantDTO> splitedInfants) {
		this.splitedInfants = splitedInfants;
	}

	/**
	 * @return the userNote
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote the userNote to set
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	/**
	 * @return the reprotectedSourceFltSeg
	 */
	public FlightSegmentDTO getReprotectedSourceFltSeg() {
		return reprotectedSourceFltSeg;
	}

	/**
	 * @param reprotectedSourceFltSeg the reprotectedSourceFltSeg to set
	 */
	public void setReprotectedSourceFltSeg(FlightSegmentDTO reprotectedSourceFltSeg) {
		this.reprotectedSourceFltSeg = reprotectedSourceFltSeg;
	}

	/**
	 * @return the reprotectedTargetFltSeg
	 */
	public FlightSegmentDTO getReprotectedTargetFltSeg() {
		return reprotectedTargetFltSeg;
	}

	/**
	 * @param reprotectedTargetFltSeg the reprotectedTargetFltSeg to set
	 */
	public void setReprotectedTargetFltSeg(FlightSegmentDTO reprotectedTargetFltSeg) {
		this.reprotectedTargetFltSeg = reprotectedTargetFltSeg;
	}

	/**
	 * @return the reprotectedPnrSegId
	 */
	public Integer getReprotectedPnrSegId() {
		return reprotectedPnrSegId;
	}

	/**
	 * @param reprotectedPnrSegId the reprotectedPnrSegId to set
	 */
	public void setReprotectedPnrSegId(Integer reprotectedPnrSegId) {
		this.reprotectedPnrSegId = reprotectedPnrSegId;
	}

	/**
	 * @return the transferedSegmentIds
	 */
	public List<Integer> getTransferedSegmentIds() {
		return transferedSegmentIds;
	}

	/**
	 * @param transferedSegmentIds the transferedSegmentIds to set
	 */
	public void setTransferedSegmentIds(List<Integer> transferedSegmentIds) {
		this.transferedSegmentIds = transferedSegmentIds;
	}

	public String getCsOCCarrierCode() {
		return csOCCarrierCode;
	}

	public void setCsOCCarrierCode(String csOCCarrierCode) {
		this.csOCCarrierCode = csOCCarrierCode;
	}

	public Map<Integer, Map<Integer, EticketDTO>> geteTicketInfo() {
		return eTicketInfo;
	}

	public void seteTicketInfo(Map<Integer, Map<Integer, EticketDTO>> eTicketInfo) {
		this.eTicketInfo = eTicketInfo;
	}

	public Map<Integer, Map<Integer, EticketDTO>> getCancelledETicketInfo() {
		return cancelledETicketInfo;
	}

	public void setCancelledETicketInfo(Map<Integer, Map<Integer, EticketDTO>> cancelledETicketInfo) {
		this.cancelledETicketInfo = cancelledETicketInfo;
	}

	public Collection<Integer> getExchangedPnrSegIds() {
		return exchangedPnrSegIds;
	}

	public void setExchangedPnrSegIds(Collection<Integer> exchangedPnrSegIds) {
		this.exchangedPnrSegIds = exchangedPnrSegIds;
	}

	public Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> getPaxSegmentSSRDTOMaps() {
		return paxSegmentSSRDTOMaps;
	}

	public void setPaxSegmentSSRDTOMaps(Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps) {
		this.paxSegmentSSRDTOMaps = paxSegmentSSRDTOMaps;
	}

	public List<TransferSegmentDTO> getTransferSegmentDTOs() {
		return transferSegmentDTOs;
	}

	public void setTransferSegmentDTOs(List<TransferSegmentDTO> transferSegmentDTOs) {
		this.transferSegmentDTOs = transferSegmentDTOs;
	}
}
