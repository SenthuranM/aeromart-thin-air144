/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of pfs parsed information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PFS_PARSED"
 */
public class PfsPaxEntry extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4670991941975577743L;

	/** Holds the pfs parsed id */
	private int ppId;

	/** Holds the pfs parsed unique id */
	private Integer pfsId;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the received time stamp */
	private Date receivedDate;

	/** Holds the flight date */
	private Date flightDate;

	/** Holds the departure airport */
	private String departureAirport;

	/** Holds the pnr */
	private String pnr;

	/** Holds the passenger title */
	private String title;

	/** Holds the passenger first name */
	private String firstName;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the entry status (go shore, no shore, no rec) */
	private String entryStatus;

	/** Holds the process status (not processed, processed, error) */
	private String processedStatus;

	/** Holds the cabin class */
	private String cabinClassCode;

	private String logicalCabinClassCode;

	private String bookingClassCode;

	/** Holds the arrival airport */
	private String arrivalAirport;

	/** Holds the error description */
	private String errorDescription;

	/** Holds the pax category code as defined in booking class and fare rule */
	private String paxCategoryCode;

	/** Holds the passenger type */
	private String paxType;

	/** indicate wether entry is an Adult Parent or not **/
	private String isParent;

	private Integer parentPpId;

	/** Holds the E-Ticket No */
	private String eTicketNo;

	private boolean checkCreditAutoRefund;
	
	private String externalRecordLocator;
	
	private String marketingFlightElement;
	
	private String codeShareFlightNo;
	
	private String codeShareBc;
	
	private Integer gdsId;
	
	private String extETicketNo;
	/**
	 * @return Returns the departureAirport.
	 * @hibernate.property column = "DEP_STN"
	 */
	public String getDepartureAirport() {
		return departureAirport;
	}

	/**
	 * @param departureAirport
	 *            The departureAirport to set.
	 */
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	/**
	 * @return Returns the entryStatus.
	 * @hibernate.property column = "PAX_STATUS"
	 */
	public String getEntryStatus() {
		return entryStatus;
	}

	/**
	 * @param entryStatus
	 *            The entryStatus to set.
	 */
	public void setEntryStatus(String entryStatus) {
		this.entryStatus = entryStatus;
	}

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the ppId.
	 * @hibernate.id column = "PP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PFS_PARSED"
	 */
	public int getPpId() {
		return ppId;
	}

	/**
	 * @param ppId
	 *            The ppId to set.
	 */
	public void setPpId(int ppId) {
		this.ppId = ppId;
	}

	/**
	 * @return Returns the processedStatus.
	 * @hibernate.property column = "PROC_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 *            The processedStatus to set.
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the receivedDate.
	 * @hibernate.property column = "RECIEVED_DATE"
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * @param receivedDate
	 *            The receivedDate to set.
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the flightDate.
	 * @hibernate.property column = "FLIGHT_DATE"
	 */
	public Date getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            The flightDate to set.
	 */
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @return Returns the cabin class code.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the bookingCode.
	 * @hibernate.property column = "DESTINATION_STATION"
	 */

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	/**
	 * @param the
	 *            arrival airport arrival airport to set.
	 */
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	/**
	 * @return Returns the entryStatus.
	 * @hibernate.property column = "PFS_ID"
	 */
	public Integer getPfsId() {
		return pfsId;
	}

	/**
	 * Set the pfs id
	 * 
	 * @param pfsId
	 */
	public void setPfsId(Integer pfsId) {
		this.pfsId = pfsId;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 * 
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the paxCategoryCode.
	 * @hibernate.property column = "PAX_CATEGORY_CODE"
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	/**
	 * @param paxCategoryCode
	 *            The paxCategoryCode to set.
	 */
	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "IS_PARENT"
	 */
	public String getIsParent() {
		return isParent;
	}

	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PARENT_PPID"
	 */
	public Integer getParentPpId() {
		return parentPpId;
	}

	public void setParentPpId(Integer parentPpId) {
		this.parentPpId = parentPpId;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "ETICKET_NO"
	 */
	public String geteTicketNo() {
		return eTicketNo;
	}

	public void seteTicketNo(String eTicketNo) {
		this.eTicketNo = eTicketNo;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public boolean isCheckCreditAutoRefund() {
		return checkCreditAutoRefund;
	}

	public void setCheckCreditAutoRefund(boolean checkCreditAutoRefund) {
		this.checkCreditAutoRefund = checkCreditAutoRefund;
	}

	/**
	 * @return Returns the externalRecordLocator.
	 * @hibernate.property column = "EXTERNAL_REC_LOCATOR"
	 */
	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	/**
	 * @return Returns the marketingFlightElement.
	 * @hibernate.property column = "CS_FLIGHT_INFO"
	 */
	public String getMarketingFlightElement() {
		return marketingFlightElement;
	}

	public void setMarketingFlightElement(String marketingFlightElement) {
		this.marketingFlightElement = marketingFlightElement;
	}

	/**
	 * @return Returns the codeShareFlightNo.
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	/**
	 * @return Returns the codeShareBc.
	 * @hibernate.property column = "CS_BOOKING_CLASS"
	 */
	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}

	/**
	 * @return the gdsId
	 * @hibernate.property column = "GDS_ID"
	 */
	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * @return extETicketNo
	 * @hibernate.property column = "EXT_E_TICKET_NUMBER"
	 * 
	 */
	public String getExtETicketNo() {
		return extETicketNo;
	}

	public void setExtETicketNo(String extETicketNo) {
		this.extETicketNo = extETicketNo;
	}

}
