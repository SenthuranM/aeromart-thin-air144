package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * IBE Exit Details.
 * 
 * @author Eshan
 * 
 * @hibernate.class table = "T_IBE_USER_EXIT_DETAILS"
 */
public class IbeUserExitDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	public IbeUserExitDetails(Integer ibeExitDetailsId, String searchFlightType, String paxEmail, String paxFlightSearchFrom,
			String paxFlightSearchTo, Date paxFlihtSearchDepartureDate, Date paxFlightSearchArrivalDate,
			Integer paxFlightSearchNoOfInfants, Integer paxFlightSearchNoAdults, Integer paxFlightSearchNoChildren,
			Integer exitStep, Date searchTime, Boolean isClickEmailLink, String title, String firstName, String lastName,
			String nationality, String country, String language, String mobileNumber, String travelMobileNumber) {
		super();
		this.ibeExitDetailsId = ibeExitDetailsId;
		this.searchFlightType = searchFlightType;
		this.paxEmail = paxEmail;
		this.paxFlightSearchFrom = paxFlightSearchFrom;
		this.paxFlightSearchTo = paxFlightSearchTo;
		this.paxFlihtSearchDepartureDate = paxFlihtSearchDepartureDate;
		this.paxFlightSearchArrivalDate = paxFlightSearchArrivalDate;
		this.paxFlightSearchNoOfInfants = paxFlightSearchNoOfInfants;
		this.paxFlightSearchNoAdults = paxFlightSearchNoAdults;
		this.paxFlightSearchNoChildren = paxFlightSearchNoChildren;
		this.exitStep = exitStep;
		this.searchTime = searchTime;
		this.isClickEmailLink = isClickEmailLink;
		this.title = title;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nationality = nationality;
		this.country = country;
		this.language = language;
		this.mobileNumber = mobileNumber;
		this.travelMobileNumber = travelMobileNumber;
	}

	public IbeUserExitDetails() {
	}

	private Integer ibeExitDetailsId;
	private String searchFlightType;
	private String paxEmail;
	private String paxFlightSearchFrom;
	private String paxFlightSearchTo;
	private Date paxFlihtSearchDepartureDate;
	private Date paxFlightSearchArrivalDate;
	private Integer paxFlightSearchNoOfInfants;
	private Integer paxFlightSearchNoAdults;
	private Integer paxFlightSearchNoChildren;
	private Integer exitStep;
	private Date searchTime;
	
	private Boolean isClickEmailLink = Boolean.FALSE;
	private String title;
	private String firstName;
	private String lastName;
	private String nationality;
	private String country;
	private String language;
	private String mobileNumber;
	private String travelMobileNumber;

	/**
	 * @hibernate.id column = "IBE_USER_EXIT_DETAILS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_IBE_USER_EXIT_DETAILS"
	 */
	public Integer getIbeExitDetailsId() {
		return ibeExitDetailsId;
	}

	public void setIbeExitDetailsId(Integer ibeExitDetailsId) {
		this.ibeExitDetailsId = ibeExitDetailsId;
	}

	/**
	 * @hibernate.property column = "PAX_EMAIL"
	 */
	public String getPaxEmail() {
		return paxEmail;
	}

	public void setPaxEmail(String paxEmail) {
		this.paxEmail = paxEmail;
	}

	/**
	 * @hibernate.property column = "PAX_FLT_SEARCH_FROM"
	 */
	public String getPaxFlightSearchFrom() {
		return paxFlightSearchFrom;
	}

	public void setPaxFlightSearchFrom(String paxFlightSearchFrom) {
		this.paxFlightSearchFrom = paxFlightSearchFrom;
	}

	/**
	 * @hibernate.property column = "PAX_FLT_SEARCH_TO"
	 */
	public String getPaxFlightSearchTo() {
		return paxFlightSearchTo;
	}

	public void setPaxFlightSearchTo(String paxFlightSearchTo) {
		this.paxFlightSearchTo = paxFlightSearchTo;
	}

	/**
	 * @hibernate.property column = "PAX_FLT_SEARCH_DEPARTURE_DATE"
	 */
	public Date getPaxFlihtSearchDepartureDate() {
		return paxFlihtSearchDepartureDate;
	}

	public void setPaxFlihtSearchDepartureDate(Date paxFlihtSearchDepartureDate) {
		this.paxFlihtSearchDepartureDate = paxFlihtSearchDepartureDate;
	}

	/**
	 * @hibernate.property column = "PAX_FLIGHT_SEARCH_ARRIVAL_DATE"
	 */
	public Date getPaxFlightSearchArrivalDate() {
		return paxFlightSearchArrivalDate;
	}

	public void setPaxFlightSearchArrivalDate(Date paxFlightSearchArrivalDate) {
		this.paxFlightSearchArrivalDate = paxFlightSearchArrivalDate;
	}

	/**
	 * @hibernate.property column = "PAX_FLIGHT_SEARCH_NO_OF_INF"
	 */
	public Integer getPaxFlightSearchNoOfInfants() {
		return paxFlightSearchNoOfInfants;
	}

	public void setPaxFlightSearchNoOfInfants(Integer paxFlightSearchNoOfInfants) {
		this.paxFlightSearchNoOfInfants = paxFlightSearchNoOfInfants;
	}

	/**
	 * @hibernate.property column = "PAX_FLIGHT_SEARCH_NO_OF_ADT"
	 */
	public Integer getPaxFlightSearchNoAdults() {
		return paxFlightSearchNoAdults;
	}

	public void setPaxFlightSearchNoAdults(Integer paxFlightSearchNoAdults) {
		this.paxFlightSearchNoAdults = paxFlightSearchNoAdults;
	}

	/**
	 * @hibernate.property column = "PAX_FLIGHT_SEARCH_NO_OF_CHD"
	 */
	public Integer getPaxFlightSearchNoChildren() {
		return paxFlightSearchNoChildren;
	}

	public void setPaxFlightSearchNoChildren(Integer paxFlightSearchNoChildren) {
		this.paxFlightSearchNoChildren = paxFlightSearchNoChildren;
	}

	/**
	 * @hibernate.property column = "EXIT_STEP"
	 */
	public Integer getExitStep() {
		return exitStep;
	}

	public void setExitStep(Integer exitStep) {
		this.exitStep = exitStep;
	}

	/**
	 * @hibernate.property column = "SEARCH_TIME"
	 */
	public Date getSearchTime() {
		return searchTime;
	}

	public void setSearchTime(Date searchTime) {
		this.searchTime = searchTime;
	}

	/**
	 * @hibernate.property column = "SEARCH_FLT_TYPE"
	 */
	public String getSearchFlightType() {
		return searchFlightType;
	}

	public void setSearchFlightType(String searchFlightType) {
		this.searchFlightType = searchFlightType;
	}
	
	/**
	 * @return the isClickEmailLink
	 * @hibernate.property column = "IS_CLICK_EMAIL_LINK" type = "yes_no"
	 */
	public Boolean getIsClickEmailLink() {
		return isClickEmailLink;
	}

	public void setIsClickEmailLink(Boolean isClickEmailLink) {
		this.isClickEmailLink = isClickEmailLink;
	}
	
	/**
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @hibernate.property column = "NATIONALITY"
	 */
	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @hibernate.property column = "COUNTRY"
	 */
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @hibernate.property column = "LANG"
	 */
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @hibernate.property column = "MOBILE_NUMBER"
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @hibernate.property column = "TRAVEL_MOBILE_NUMBER"
	 */
	public String getTravelMobileNumber() {
		return travelMobileNumber;
	}

	public void setTravelMobileNumber(String travelMobileNumber) {
		this.travelMobileNumber = travelMobileNumber;
	}

	@Override
	public String toString() {
		return "IbeUserExitDetails [ibeExitDetailsId=" + ibeExitDetailsId + ", searchFlightType=" + searchFlightType
				+ ", paxEmail=" + paxEmail + ", paxFlightSearchFrom=" + paxFlightSearchFrom + ", paxFlightSearchTo="
				+ paxFlightSearchTo + ", paxFlihtSearchDepartureDate=" + paxFlihtSearchDepartureDate
				+ ", paxFlightSearchArrivalDate=" + paxFlightSearchArrivalDate + ", paxFlightSearchNoOfInfants="
				+ paxFlightSearchNoOfInfants + ", paxFlightSearchNoAdults=" + paxFlightSearchNoAdults
				+ ", paxFlightSearchNoChildren=" + paxFlightSearchNoChildren + ", exitStep=" + exitStep + ", searchTime="
				+ searchTime + ", isClickEmailLink=" + isClickEmailLink + ", title=" + title + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", nationality=" + nationality + ", country=" + country + ", language=" + language
				+ ", mobileNumber=" + mobileNumber + ", travelMobileNumber=" + travelMobileNumber + "]";
	}
	
}
