package com.isa.thinair.airreservation.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_ONHOLD_ALERT"
 */

public class OnholdAlert extends Persistent {

	private static final long serialVersionUID = 1L;

	private String pnr;

	private char alertSent;

	private int channelCode;

	private Date sentTimeStamp;

	private String alertType;

	/**
	 * @hibernate.property column = "sales_channel_code"
	 */
	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	/**
	 * @hibernate.id column = "pnr" generator-class = "assigned"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column = "alert_sent"
	 */
	public char getAlertSent() {
		return alertSent;
	}

	public void setAlertSent(char alertSent) {
		this.alertSent = alertSent;
	}

	/**
	 * @hibernate.property column = "sent_timestamp"
	 */

	public Date getSentTimeStamp() {
		return sentTimeStamp;
	}

	public void setSentTimeStamp(Date sentTimeStamp) {
		this.sentTimeStamp = sentTimeStamp;
	}

	/**
	 * @hibernate.property column = "alert_type"
	 */
	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

}
