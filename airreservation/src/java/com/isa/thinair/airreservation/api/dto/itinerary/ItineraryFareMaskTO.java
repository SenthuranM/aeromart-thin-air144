package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItineraryFareMaskTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean passengerDetailsMask;
	private boolean paymentDetailsMask;
	private boolean chargeDetailsMask;

	private boolean fareMask;
	private boolean chargesMask;
	private boolean ballanceMask;
	private boolean paidAmountMask;

	public ItineraryFareMaskTO() {
		this.passengerDetailsMask = true;
		this.paymentDetailsMask = true;
		this.chargeDetailsMask = true;

		this.fareMask = true;
		this.chargesMask = true;
		this.ballanceMask = true;
		this.paidAmountMask = true;
	}

	public ItineraryFareMaskTO(boolean passengerDetailsMask,
			boolean paymentDetailsMask, boolean chargeDetailsMask,
			boolean fareMask, boolean chargesMask, boolean ballanceMask,
			boolean paidAmountMask) {
		this.passengerDetailsMask = true;
		this.paymentDetailsMask = true;
		this.chargeDetailsMask = true;

		this.fareMask = true;
		this.chargesMask = true;
		this.ballanceMask = true;
		this.paidAmountMask = true;
	}

	public boolean isPassengerDetailsMask() {
		return passengerDetailsMask;
	}

	public void setPassengerDetailsMask(boolean passengerDetailsMask) {
		this.passengerDetailsMask = passengerDetailsMask;
	}

	public boolean isPaymentDetailsMask() {
		return paymentDetailsMask;
	}

	public void setPaymentDetailsMask(boolean paymentDetailsMask) {
		this.paymentDetailsMask = paymentDetailsMask;
	}

	public boolean isChargeDetailsMask() {
		return chargeDetailsMask;
	}

	public void setChargeDetailsMask(boolean chargeDetailsMask) {
		this.chargeDetailsMask = chargeDetailsMask;
	}

	public boolean isFareMask() {
		return fareMask;
	}

	public void setFareMask(boolean fareMask) {
		this.fareMask = fareMask;
	}

	public boolean isChargesMask() {
		return chargesMask;
	}

	public void setChargesMask(boolean chargesMask) {
		this.chargesMask = chargesMask;
	}

	public boolean isBallanceMask() {
		return ballanceMask;
	}

	public void setBallanceMask(boolean ballanceMask) {
		this.ballanceMask = ballanceMask;
	}

	public boolean isPaidAmountMask() {
		return paidAmountMask;
	}

	public void setPaidAmountMask(boolean paidAmountMask) {
		this.paidAmountMask = paidAmountMask;
	}

}
