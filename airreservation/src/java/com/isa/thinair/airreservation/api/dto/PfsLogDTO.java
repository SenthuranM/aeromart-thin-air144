/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Byorn
 * @TODO : Need to Improvise on this
 */
public class PfsLogDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 976135161587952254L;

	private String emailTo;

	private String fileName = null;

	// Holds the Exception Description
	private String exceptionDescription = null;

	// Holds the Stack Trace Elements
	private StackTraceElement[] stackTraceElements = null;

	// Holds the PFS ID
	private String pfsId = null;

	// Holds teh PFs Content
	private String pfsContent = null;

	// Holds a collection of String pax details - [not in use still for pfs]
	private Collection<String> paxWithErrors = null;

	// Holds a collection of String pax details with reservations - [not in use
	// still for pfs]
	private Collection<String> paxWithReservations = null;

	// Holds any additional information of the logic. - [i.e to capture any code
	// errors..i have itroduced this]
	private String customDescription = null;

	/**
	 * @return Returns the exceptionDescription.
	 */
	public String getExceptionDescription() {
		return exceptionDescription;
	}

	/**
	 * @param exceptionDescription
	 *            The exceptionDescription to set.
	 */
	public void setExceptionDescription(String exceptionDescription) {
		this.exceptionDescription = exceptionDescription;
	}

	/**
	 * @return Returns the stackTraceElements.
	 */
	public StackTraceElement[] getStackTraceElements() {
		return stackTraceElements;
	}

	/**
	 * @param stackTraceElements
	 *            The stackTraceElements to set.
	 */
	public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}

	/**
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return Returns the emailTo.
	 */
	public String getEmailTo() {
		return emailTo;
	}

	/**
	 * @param emailTo
	 *            The emailTo to set.
	 */
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	/**
	 * @return Returns the paxWithErrors.
	 */
	public Collection<String> getPaxWithErrors() {
		return paxWithErrors;
	}

	/**
	 * @param paxWithErrors
	 *            The paxWithErrors to set.
	 */
	public void setPaxWithErrors(Collection<String> paxWithErrors) {
		this.paxWithErrors = paxWithErrors;
	}

	/**
	 * @return Returns the paxWithReservations.
	 */
	public Collection<String> getPaxWithReservations() {
		return paxWithReservations;
	}

	/**
	 * @param paxWithReservations
	 *            The paxWithReservations to set.
	 */
	public void setPaxWithReservations(Collection<String> paxWithReservations) {
		this.paxWithReservations = paxWithReservations;
	}

	/**
	 * @return Returns the pfsContent.
	 */
	public String getPfsContent() {
		return pfsContent;
	}

	/**
	 * @param pfsContent
	 *            The pfsContent to set.
	 */
	public void setPfsContent(String pfsContent) {
		this.pfsContent = pfsContent;
	}

	/**
	 * @return Returns the pfsId.
	 */
	public String getPfsId() {
		return pfsId;
	}

	/**
	 * @param pfsId
	 *            The pfsId to set.
	 */
	public void setPfsId(String pfsId) {
		this.pfsId = pfsId;
	}

	/**
	 * @return Returns the customDescription.
	 */
	public String getCustomDescription() {
		return customDescription;
	}

	/**
	 * @param customDescription
	 *            The customDescription to set.
	 */
	public void setCustomDescription(String customDescription) {
		this.customDescription = customDescription;
	}

}
