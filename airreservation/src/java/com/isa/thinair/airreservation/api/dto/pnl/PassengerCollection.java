/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pnl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.pnl.DestinationFare.PassengerStoreTypes;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;

/**
 * @author udithad
 *
 */
public class PassengerCollection extends BasePassengerCollection {

	private MessageTypes messageType;
	private PNLMetaDataDTO metaDataDTO;
	private Map<String, List<DestinationFare>> pnlAdlDestinationMap;
	private Map<String, List<String>> rbdFareCabinMap;
	private Map<PassengerStoreTypes,Map<String, Integer>> pnrWisePassengerCount;
	private Map<PassengerStoreTypes,Map<String, Integer>> pnrWisePassengerReductionCount;
	private Map<Integer, String> pnrPaxVsGroupCodes;
	private String lastGroupCode;
	private String[] groupCodes;

	private Map<Integer, Integer> pnrPaxIdvsSegmentIds;
	private Map<String, Integer> fareClassWisePaxCount;
	private List<String> pnrCollection;
	private List<PassengerInformation> passengerInformations;

	public PassengerCollection() {
		pnlAdlDestinationMap = new TreeMap<String, List<DestinationFare>>();
		pnrWisePassengerCount = new HashMap<PassengerStoreTypes,Map<String, Integer>>();
		pnrWisePassengerReductionCount = new HashMap<PassengerStoreTypes,Map<String, Integer>>();

		pnrPaxIdvsSegmentIds = new HashMap<Integer, Integer>();
		pnrPaxVsGroupCodes = new HashMap<Integer,String>();
		fareClassWisePaxCount = new HashMap<String, Integer>();
		pnrCollection = new ArrayList<String>();
	}

	public PNLMetaDataDTO getMetaDataDTO() {
		return metaDataDTO;
	}

	public void setMetaDataDTO(PNLMetaDataDTO metaDataDTO) {
		this.metaDataDTO = metaDataDTO;
	}

	public String getMessageType() {
		String messageType = "";
		if (this.messageType != null) {
			messageType = messageType.toString();
		}
		return messageType;
	}

	public void setMessageType(MessageTypes messageType) {
		this.messageType = messageType;
	}

	public Map<String, List<DestinationFare>> getPnlAdlDestinationMap() {
		return pnlAdlDestinationMap;
	}

	public void setPnlAdlDestinationMap(
			Map<String, List<DestinationFare>> pnlAdlDestinationMap) {
		this.pnlAdlDestinationMap = pnlAdlDestinationMap;
	}

	public Map<String, List<String>> getRbdFareCabinMap() {
		return rbdFareCabinMap;
	}

	public void setRbdFareCabinMap(Map<String, List<String>> rbdFareCabinMap) {
		this.rbdFareCabinMap = rbdFareCabinMap;
	}

	public void populateDestinationElements(DestinationFare destinationDTO) {
		addDestinationDTOToList(
				getDestinationFareList(pnlAdlDestinationMap,
						destinationDTO.getDestinationAirportCode()),
				destinationDTO);
	}

	public Map<PassengerStoreTypes, Map<String, Integer>> getPnrWisePassengerCount() {
		return pnrWisePassengerCount;
	}

	public void setPnrWisePassengerCount(
			Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerCount) {
		this.pnrWisePassengerCount = pnrWisePassengerCount;
	}



	public Map<PassengerStoreTypes, Map<String, Integer>> getPnrWisePassengerReductionCount() {
		return pnrWisePassengerReductionCount;
	}

	public void setPnrWisePassengerReductionCount(
			Map<PassengerStoreTypes, Map<String, Integer>> pnrWisePassengerReductionCount) {
		this.pnrWisePassengerReductionCount = pnrWisePassengerReductionCount;
	}

	public String[] getGroupCodes() {
		return groupCodes;
	}

	public void setGroupCodes(String[] groupCodes) {
		this.groupCodes = groupCodes;
	}

	public String getLastGroupCode() {
		return lastGroupCode;
	}

	public void setLastGroupCode(String lastGroupCode) {
		this.lastGroupCode = lastGroupCode;
	}

	public Map<Integer, Integer> getPnrPaxIdvsSegmentIds() {
		return pnrPaxIdvsSegmentIds;
	}

	public void setPnrPaxIdvsSegmentIds(
			Map<Integer, Integer> pnrPaxIdvsSegmentIds) {
		this.pnrPaxIdvsSegmentIds = pnrPaxIdvsSegmentIds;
	}

	public Map<String, Integer> getFareClassWisePaxCount() {
		return fareClassWisePaxCount;
	}

	public void setFareClassWisePaxCount(
			Map<String, Integer> fareClassWisePaxCount) {
		this.fareClassWisePaxCount = fareClassWisePaxCount;
	}

	public List<String> getPnrCollection() {
		return pnrCollection;
	}

	public void setPnrCollection(List<String> pnrCollection) {
		this.pnrCollection = pnrCollection;
	}

	public List<PassengerInformation> getPassengerInformations() {
		return passengerInformations;
	}

	public void setPassengerInformations(
			List<PassengerInformation> passengerInformations) {
		this.passengerInformations = passengerInformations;
	}

	public Map<Integer, String> getPnrPaxVsGroupCodes() {
		return pnrPaxVsGroupCodes;
	}

	public void setPnrPaxVsGroupCodes(Map<Integer, String> pnrPaxVsGroupCodes) {
		this.pnrPaxVsGroupCodes = pnrPaxVsGroupCodes;
	}

}
