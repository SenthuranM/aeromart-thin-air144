/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * To hold PNLADLDestination data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PNLADLDestinationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2325613566375131743L;
	private String destinationAirportCode;
	private String bookingCode;
	private Collection<ReservationPaxDetailsDTO> passenger;
	private String cabinClassCode;
	private String fareClassCode;
	
	/**
	 * @return Returns the bookingCode.
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            The bookingCode to set.
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return Returns the destinationAirportCode.
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	/**
	 * @param destinationAirportCode
	 *            The destinationAirportCode to set.
	 */
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * @return Returns the passenger.
	 */
	public Collection<ReservationPaxDetailsDTO> getPassenger() {
		return passenger;
	}

	/**
	 * @param passenger
	 *            The passenger to set.
	 */
	public void setPassenger(Collection<ReservationPaxDetailsDTO> passenger) {
		this.passenger = passenger;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getFareClassCode() {
		return fareClassCode;
	}

	public void setFareClassCode(String fareClassCode) {
		this.fareClassCode = fareClassCode;
	}
	
}
