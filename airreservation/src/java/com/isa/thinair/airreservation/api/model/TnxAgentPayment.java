/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;

/**
 * Holds reservation transaction agent payments
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class TnxAgentPayment extends TnxPayment {

	private static final long serialVersionUID = 1L;

	/** Hold the agent code */
	private String agentCode;

	private PaymentReferenceTO paymentReferenceTO;

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the paymentReferenceTO
	 */
	public PaymentReferenceTO getPaymentReferenceTO() {
		return paymentReferenceTO;
	}

	/**
	 * @param paymentReferenceTO
	 *            the paymentReferenceTO to set
	 */
	public void setPaymentReferenceTO(PaymentReferenceTO paymentReferenceTO) {
		this.paymentReferenceTO = paymentReferenceTO;
	}
}
