package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nilindra Fernando
 * @since August 2, 2010
 */
public class ReservationPaymentMetaTO implements Serializable {

	private static final long serialVersionUID = -7033094236186405448L;

	private Map<Integer, ReservationPaxPaymentMetaTO> mapPaxWisePaymentMetaInfo = new HashMap<Integer, ReservationPaxPaymentMetaTO>();

	/**
	 * @return the mapPaxWisePaymentMetaInfo
	 */
	public Map<Integer, ReservationPaxPaymentMetaTO> getMapPaxWisePaymentMetaInfo() {
		return mapPaxWisePaymentMetaInfo;
	}

}
