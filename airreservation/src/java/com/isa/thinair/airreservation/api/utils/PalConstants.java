package com.isa.thinair.airreservation.api.utils;

public interface PalConstants {

	public static interface Status {

		public static final String ACTIVE = "ACT";

		public static final String INACTIVE = "INA";
	}
	
	public static interface AllowPalCal{
		
		public static final String YES = "Y";

		public static final String NO = "N";
	}
	
	/** Holds the values of the sita msg types **/
	public interface MessageTypes {
		public final String PAL = "PAL";
		public final String CAL = "CAL";
	}
	
	/** Holds the values the sending method **/
	public interface SendingMethod {
		public final String SCHEDSERVICE = "SS";
		public final String MANUALLY = "MA";
		public final String RESENDER = "RE";
	}
	
	public interface PALGeneration {
		public final String PAL = "PAL";
		public final String PART_ONE = "PART1";
		public final String RBD = "RBD";

	}

	public interface CALGeneration {
		public final String CAL = "CAL";
		public final String PART_ONE = "PART1";
	}
	
	public static interface PalCalTimings {

		public static final String ACTIVE = "ACT";

		public static final String INACTIVE = "INA";

		public static final String APPLY_TO_ALL_YES = "Y";

		public static final String APPLY_TO_ALL_NO = "N";

	}
}
