package com.isa.thinair.airreservation.api.dto.ancilaryreminder;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nakhtar
 */
public class AncillaryStatusDTO implements Serializable {

	private static final long serialVersionUID = 8024976049131006005L;

	private String pnr;
	private Integer segmentId;
	private Integer flightId;
	private Integer pnrSegNotifyId;
	private Integer flightSegmentId;
	private String flightStatus;
	private String segment;
	private Date departureTime;
	private Date arrivalTime;
	private Date departureTimeZulu;
	private Date arrivalTimeZulu;
	private String name;
	private String lastName;
	private String flightNum;
	private String email;
	private String seatStatus;
	private String mealStatus;
	private String baggageStatus;
	private String InsurancePolicyCode;
	private String insuranceStatus;
	private String iteneraryLanguage;
	private String airportOnlineCheckIn;
	private String returnFlag;
	private String groundSegId;
	private String ondGroupId;

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the segmentId
	 */
	public Integer getSegmentId() {
		return segmentId;
	}

	/**
	 * @param segmentId
	 *            the segmentId to set
	 */
	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	/**
	 * @return the flightStatus
	 */
	public String getFlightStatus() {
		return flightStatus;
	}

	/**
	 * @param flightStatus
	 *            the flightStatus to set
	 */
	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	/**
	 * @return the pnrSegNotifyId
	 */
	public Integer getPnrSegNotifyId() {
		return pnrSegNotifyId;
	}

	/**
	 * @param pnrSegNotifyId
	 *            the pnrSegNotifyId to set
	 */
	public void setPnrSegNotifyId(Integer pnrSegNotifyId) {
		this.pnrSegNotifyId = pnrSegNotifyId;
	}

	/**
	 * @return the segment
	 */
	public String getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            the segment to set
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}

	/**
	 * @return the departureTime
	 */
	public Date getDepartureTime() {
		return departureTime;
	}

	/**
	 * @param departureTime
	 *            the departureTime to set
	 */
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * @return the departureTimeZulu
	 */
	public Date getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	/**
	 * @param departureTimeZulu
	 *            the departureTimeZulu to set
	 */
	public void setDepartureTimeZulu(Date departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	/**
	 * @return the arrivalTimeZulu
	 */
	public Date getArrivalTimeZulu() {
		return arrivalTimeZulu;
	}

	/**
	 * @param arrivalTimeZulu
	 *            the arrivalTimeZulu to set
	 */
	public void setArrivalTimeZulu(Date arrivalTimeZulu) {
		this.arrivalTimeZulu = arrivalTimeZulu;
	}

	/**
	 * @return the seatStatus
	 */
	public String getSeatStatus() {
		return seatStatus;
	}

	/**
	 * @param seatStatus
	 *            the seatStatus to set
	 */
	public void setSeatStatus(String seatStatus) {
		this.seatStatus = seatStatus;
	}

	/**
	 * @return the mealStatus
	 */
	public String getMealStatus() {
		return mealStatus;
	}

	/**
	 * @param mealStatus
	 *            the mealStatus to set
	 */
	public void setMealStatus(String mealStatus) {
		this.mealStatus = mealStatus;
	}

	/**
	 * @return the insuranceStatus
	 */
	public String getInsuranceStatus() {
		return insuranceStatus;
	}

	/**
	 * @param insuranceStatus
	 *            the insuranceStatus to set
	 */
	public void setInsuranceStatus(String insuranceStatus) {
		this.insuranceStatus = insuranceStatus;
	}

	/**
	 * @return the arrivalTime
	 */
	public Date getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * @param arrivalTime
	 *            the arrivalTime to set
	 */
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the flightNum
	 */
	public String getFlightNum() {
		return flightNum;
	}

	/**
	 * @param flightNum
	 *            the flightNum to set
	 */
	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the insurancePolicyCode
	 */
	public String getInsurancePolicyCode() {
		return InsurancePolicyCode;
	}

	/**
	 * @param insurancePolicyCode
	 *            the insurancePolicyCode to set
	 */
	public void setInsurancePolicyCode(String insurancePolicyCode) {
		InsurancePolicyCode = insurancePolicyCode;
	}

	/**
	 * @return the flightId
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the flightSegmentId
	 */
	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            the flightSegmentId to set
	 */
	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @return the iteneraryLanguage
	 */
	public String getIteneraryLanguage() {
		return iteneraryLanguage;
	}

	/**
	 * @param iteneraryLanguage
	 *            the iteneraryLanguage to set
	 */
	public void setIteneraryLanguage(String iteneraryLanguage) {
		this.iteneraryLanguage = iteneraryLanguage;
	}

	/**
	 * @return the airportOnlineCheckIn
	 */
	public String getAirportOnlineCheckIn() {
		return airportOnlineCheckIn;
	}

	/**
	 * @param airportOnlineCheckIn
	 *            the airportOnlineCheckIn to set
	 */
	public void setAirportOnlineCheckIn(String airportOnlineCheckIn) {
		this.airportOnlineCheckIn = airportOnlineCheckIn;
	}

	/**
	 * @return the returnFlag
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            the returnFlag to set
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGroundSegId() {
		return groundSegId;
	}

	public void setGroundSegId(String groundSegId) {
		this.groundSegId = groundSegId;
	}

	public String getOndGroupId() {
		return ondGroupId;
	}

	public void setOndGroupId(String ondGroupId) {
		this.ondGroupId = ondGroupId;
	}

	public String getBaggageStatus() {
		return baggageStatus;
	}

	public void setBaggageStatus(String baggageStatus) {
		this.baggageStatus = baggageStatus;
	}

}
