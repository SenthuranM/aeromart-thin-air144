/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * ReservationExternalSegmentTO is the transfer object of ReservationExternalSegment entity class
 * 
 * @author Nilindra Fernando
 * @since 3:55 PM 10/28/2009
 */
public class ReservationExternalSegmentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4650609993410126037L;

	/** Holds the pnr external segment id */
	private Integer pnrExtSegId;

	/** Holds flight number */
	private String flightNo;

	/** Holds departure date */
	private Date departureDate;

	/** Holds arrival date */
	private Date arrivalDate;

	/** Holds segment code */
	private String segmentCode;

	/** Holds external segment status */
	private String status;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/** Holds the logical cabin class code */
	private String logicalCCCode;

	/** Holds external segment sequence */
	private Integer segmentSeq;

	/** Holds the pnr number */
	private String pnr;

	/**
	 * @return the pnrExtSegId
	 */
	public Integer getPnrExtSegId() {
		return pnrExtSegId;
	}

	/**
	 * @param pnrExtSegId
	 *            the pnrExtSegId to set
	 */
	public void setPnrExtSegId(Integer pnrExtSegId) {
		this.pnrExtSegId = pnrExtSegId;
	}

	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the departureDate
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the arrivalDate
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            the arrivalDate to set
	 */
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the segmentSeq
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            the segmentSeq to set
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}
}
