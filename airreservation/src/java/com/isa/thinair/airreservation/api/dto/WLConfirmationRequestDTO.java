package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class WLConfirmationRequestDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer flightId;
	private Integer flightSegId;	
	private String logicalCabinCalss;
	private String bookingClass;
	private String bookingClassType;
	private String segmentCode;
	private Integer segmentAvailabileCount;
	private Integer segmentAvailableInfantCount;
	private Map<String, Integer> segmentBcAvailableCount = new HashMap<String,Integer>();
	private Integer totalAdultCount;
	private Integer totalInfantCount;
	
	
	public Integer getFlightId() {
		return flightId;
	}
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}
	public Integer getFlightSegId() {
		return flightSegId;
	}
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}
	
	public String getLogicalCabinCalss() {
		return logicalCabinCalss;
	}
	public void setLogicalCabinCalss(String logicalCabinCalss) {
		this.logicalCabinCalss = logicalCabinCalss;
	}
	public String getBookingClass() {
		return bookingClass;
	}
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
	public String getSegmentCode() {
		return segmentCode;
	}
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
	public Integer getSegmentAvailabileCount() {
		return segmentAvailabileCount;
	}
	public void setSegmentAvailabileCount(Integer segmentAvailabileCount) {
		this.segmentAvailabileCount = segmentAvailabileCount;
	}
	public Map<String, Integer> getSegmentBcAvailableCount() {
		return segmentBcAvailableCount;
	}
	public void setSegmentBcAvailableCount(Map<String, Integer> segmentBcAvailableCount) {
		this.segmentBcAvailableCount = segmentBcAvailableCount;
	}
	public Integer getSegmentAvailableInfantCount() {
		return segmentAvailableInfantCount;
	}
	public void setSegmentAvailableInfantCount(Integer segmentAvailableInfantCount) {
		this.segmentAvailableInfantCount = segmentAvailableInfantCount;
	}
	public String getBookingClassType() {
		return bookingClassType;
	}
	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}
	public Integer getTotalAdultCount() {
		return totalAdultCount;
	}
	public void setTotalAdultCount(Integer totalAdultCount) {
		this.totalAdultCount = totalAdultCount;
	}
	public Integer getTotalInfantCount() {
		return totalInfantCount;
	}
	public void setTotalInfantCount(Integer totalInfantCount) {
		this.totalInfantCount = totalInfantCount;
	}
	
	
}
