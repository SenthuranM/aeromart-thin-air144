package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class OnHoldReleaseTimeDTO implements Serializable, Comparable<OnHoldReleaseTimeDTO> {

	private static final long serialVersionUID = -7098299349595908777L;

	private Integer releaseTimeId;

	public static final String ONHOLD_ANY = "ANY";

	private Date bookingDate;

	private Date flightDepartureDate;

	private String ondCode;

	private String bookingClass;

	private boolean isDomestic;

	private String cabinClass;

	private String agentCode;

	private String moduleCode;

	private Long releaseTime;

	private Long startCutoverTime;

	private Long endCutoverTime;

	private Long startTime;

	private Long endTime;

	private Long version;

	private boolean isReleaseTimeFromBookingDate;

	private int rank;

	private String releaseTimeWithRelativeTo;

	/** Holds whether user has "xbe.res.make.onhold.buffer" privilege key */
	private boolean hasBuffertimeOhd;

	private int fareRuleId;

	public enum MODULE {
		IBE, XBE, KIOSK, ANY, API, ANDROID, IOS;

		public static MODULE getEnum(String strEnum) {
			for (MODULE enm : MODULE.values()) {
				if (enm.toString().equalsIgnoreCase(strEnum)) {
					return enm;
				}
			}
			return MODULE.ANY;
		}
	};

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public boolean isDomestic() {
		return isDomestic;
	}

	public void setDomestic(boolean isDomestic) {
		this.isDomestic = isDomestic;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getFlightDepartureDate() {
		return flightDepartureDate;
	}

	public void setFlightDepartureDate(Date flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Long getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(Long releaseTime) {
		this.releaseTime = releaseTime;
	}

	public boolean isReleaseTimeFromBookingDate() {
		return isReleaseTimeFromBookingDate;
	}

	public void setReleaseTimeFromBookingDate(boolean isReleaseTimeFromBookingDate) {
		this.isReleaseTimeFromBookingDate = isReleaseTimeFromBookingDate;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public boolean isHasBuffertimeOhd() {
		return hasBuffertimeOhd;
	}

	public void setHasBuffertimeOhd(boolean hasBuffertimeOhd) {
		this.hasBuffertimeOhd = hasBuffertimeOhd;
	}

	@Override
	public int compareTo(OnHoldReleaseTimeDTO o) {
		int rank1 = this.getRank();
		int rank2 = o.getRank();

		if (rank1 > rank2)
			return 1;
		else if (rank1 < rank2)
			return -1;
		else
			return 0;
	}

	public Long getStartCutoverTime() {
		return startCutoverTime;
	}

	public void setStartCutoverTime(Long startCutoverTime) {
		this.startCutoverTime = startCutoverTime;
	}

	public Long getEndCutoverTime() {
		return endCutoverTime;
	}

	public void setEndCutoverTime(Long endCutoverTime) {
		this.endCutoverTime = endCutoverTime;
	}

	public Integer getReleaseTimeId() {
		return releaseTimeId;
	}

	public void setReleaseTimeId(Integer releaseTimeId) {
		this.releaseTimeId = releaseTimeId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getReleaseTimeWithRelativeTo() {
		return releaseTimeWithRelativeTo;
	}

	public void setReleaseTimeWithRelativeTo(String relTimeWrt) {
		this.releaseTimeWithRelativeTo = relTimeWrt;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

}
