package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class CCSalesHistoryDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2423019478051120863L;

	private Date dateOfsale;

	private int cardTypeId;

	private BigDecimal totalDailySales = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String transferStatus;

	private Timestamp transferTimeStamp;

	private String cardType;

	public int getCardTypeId() {
		return cardTypeId;
	}

	public void setCardTypeId(int cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	public Date getDateOfsale() {
		return dateOfsale;
	}

	public void setDateOfsale(Date dateOfsale) {
		this.dateOfsale = dateOfsale;
	}

	public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	public Timestamp getTransferTimeStamp() {
		return transferTimeStamp;
	}

	public void setTransferTimeStamp(Timestamp transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return Returns the totalDailySales.
	 */
	public BigDecimal getTotalDailySales() {
		return totalDailySales;
	}

	/**
	 * @param totalDailySales
	 *            The totalDailySales to set.
	 */
	public void setTotalDailySales(BigDecimal totalDailySales) {
		this.totalDailySales = totalDailySales;
	}

}
