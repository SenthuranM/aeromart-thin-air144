package com.isa.thinair.airreservation.api.dto.rak;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class InsurancePublisherDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String PUBLISH_FIRST = "FIRST";
	public static final String PUBLISH_LAST = "LAST";
	public static final String INS_PUBLISH_STATE_NOT_SENT = "NOTSENT";
	public static final String INS_PUBLISH_STATE_FIRST_SENT = "RAK_INS_FIRST_UPLOAD_SENT";
	public static final String INS_PUBLISH_STATE_FIRST_SUCCESS = "RAK_INS_FIRST_UPLOAD_SUCCESS";
	public static final String INS_PUBLISH_STATE_FIRST_FAILED = "RAK_INS_FIRST_UPLOAD_FAILED";
	public static final String INS_PUBLISH_STATE_LAST_SENT = "RAK_INS_LAST_UPLOAD_SENT";
	public static final String INS_PUBLISH_STATE_LAST_SUCCESS = "RAK_INS_LAST_UPLOAD_SUCCESS";
	public static final String INS_PUBLISH_STATE_LAST_FAILED = "RAK_INS_LAST_UPLOAD_FAILED";
	public static final String RAK_INS_UPLOAD_ERRONEOUS = "ERRONEOUS_UPLOAD";
	public static final String RAK_INS_UPLOAD_SUCCESS = "UPLOAD_SUCCESS";
	public static final String RAK_INS_UPLOAD_FAILED = "UPLOAD_FAILED";
	public static final String RAK_INS_UPLOAD_WS_ERROR = "WS_CALL_ERROR";
	public static final String RAK_INS_UPLOAD_INVALID_XML = "Invalid xml File.";
	public static final String IGNORE_OPERATION_TYPE_BUS_SERVICE = "Bus Service";

	/** Holds the Flight Id * */
	private Integer flightId;

	/** Holds the Flight Number * */
	private String flightNumber;

	/** Holds the Boarding Airport * */
	private String departureStation;

	/** Holds the Flight Dep Time in ZULU * */
	private Date departureTimeZulu;

	/** Holds theDep Time in LOCAL Tm * */
	private Date departuretimeLocal;

	/** Holds the Flight Seg Id * */
	private Integer flightSegId;

	/** Holds Insurance publishing Start cut over time **/
	private Integer insPublishStartCutOverTime;

	/** Holds Insurance publishing End cut over time **/
	private Integer insPublishEndCutOverTime;

	private String flightOrigin;

	private String segmentCode;

	private Date notificationReminderTime;

	private Integer flightSegmentNotificationEventId;

	/** Holds Airport Online checking Status */
	private String airportOnlineCheckin;

	private String jobName;

	private String JobGroupName;

	private String ancillaryType;

	private String status;

	private String publishType;

	private String insResponse;

	private boolean isRecovery;
	
	private Date bookingDate;

	private Collection<String> pnrList = new ArrayList<String>();
	
	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public Date getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	public void setDepartureTimeZulu(Date departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	public Date getDeparturetimeLocal() {
		return departuretimeLocal;
	}

	public void setDeparturetimeLocal(Date departuretimeLocal) {
		this.departuretimeLocal = departuretimeLocal;
	}

	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Integer getInsPublishStartCutOverTime() {
		return insPublishStartCutOverTime;
	}

	public void setInsPublishStartCutOverTime(Integer insPublishStartCutOverTime) {
		this.insPublishStartCutOverTime = insPublishStartCutOverTime;
	}

	public Integer getInsPublishEndCutOverTime() {
		return insPublishEndCutOverTime;
	}

	public void setInsPublishEndCutOverTime(Integer insPublishEndCutOverTime) {
		this.insPublishEndCutOverTime = insPublishEndCutOverTime;
	}

	public String getFlightOrigin() {
		return flightOrigin;
	}

	public void setFlightOrigin(String flightOrigin) {
		this.flightOrigin = flightOrigin;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getNotificationReminderTime() {
		return notificationReminderTime;
	}

	public void setNotificationReminderTime(Date notificationReminderTime) {
		this.notificationReminderTime = notificationReminderTime;
	}

	public Integer getFlightSegmentNotificationEventId() {
		return flightSegmentNotificationEventId;
	}

	public void setFlightSegmentNotificationEventId(Integer flightSegmentNotificationEventId) {
		this.flightSegmentNotificationEventId = flightSegmentNotificationEventId;
	}

	public String getAirportOnlineCheckin() {
		return airportOnlineCheckin;
	}

	public void setAirportOnlineCheckin(String airportOnlineCheckin) {
		this.airportOnlineCheckin = airportOnlineCheckin;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroupName() {
		return JobGroupName;
	}

	public void setJobGroupName(String jobGroupName) {
		JobGroupName = jobGroupName;
	}

	public String getAncillaryType() {
		return ancillaryType;
	}

	public void setAncillaryType(String ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPublishType() {
		return publishType;
	}

	public void setPublishType(String publishType) {
		this.publishType = publishType;
	}

	public String getInsResponse() {
		return insResponse;
	}

	public void setInsResponse(String insResponse) {
		this.insResponse = insResponse;
	}

	public boolean isRecovery() {
		return isRecovery;
	}

	public void setRecovery(boolean isRecovery) {
		this.isRecovery = isRecovery;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Collection<String> getPnrList() {
		return pnrList;
	}

	public void setPnrList(Collection<String> pnrList) {
		this.pnrList = pnrList;
	}
	
}
