package com.isa.thinair.airreservation.api.dto.ancilaryreminder;

import java.io.Serializable;
import java.util.Date;

public class AncillaryReminderDetailDTO implements Serializable {

	private static final long serialVersionUID = -8481597878150833766L;

	/** Holds the Flight Id * */
	private Integer flightId;

	/** Holds the Flight Number * */
	private String flightNumber;

	/** Holds the Boarding Airport * */
	private String departureStation;

	/** Holds the Flight Dep Time in ZULU * */
	private Date departureTimeZulu;

	/** Holds theDep Time in LOCAL Tm * */
	private Date departuretimeLocal;

	/** Holds the Flight Seg Id * */
	private Integer flightSegId;

	/** Holds Ancillary Reminder Start cut over time **/
	private Integer anciNotificationStartCutOverTime;

	/** Holds Ancillary Reminder End cut over time **/
	private Integer anciNotificationEndCutOverTime;

	private String flightOrigin;

	private String segmentCode;

	private Date notificationReminderTime;

	private Integer flightSegmentNotificationEventId;

	/** Holds Airport Online checking Status */
	private String airportOnlineCheckin;

	private String jobName;

	private String JobGroupName;

	/**
	 * @return the flightId
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the departureStation
	 */
	public String getDepartureStation() {
		return departureStation;
	}

	/**
	 * @param departureStation
	 *            the departureStation to set
	 */
	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	/**
	 * @return the departureTimeZulu
	 */
	public Date getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	/**
	 * @param departureTimeZulu
	 *            the departureTimeZulu to set
	 */
	public void setDepartureTimeZulu(Date departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	/**
	 * @return the departuretimeLocal
	 */
	public Date getDeparturetimeLocal() {
		return departuretimeLocal;
	}

	/**
	 * @param departuretimeLocal
	 *            the departuretimeLocal to set
	 */
	public void setDeparturetimeLocal(Date departuretimeLocal) {
		this.departuretimeLocal = departuretimeLocal;
	}

	/**
	 * @return the flightSegId
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            the flightSegId to set
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @return the anciNotificationStartCutOverTime
	 */
	public int getAnciNotificationStartCutOverTime() {
		return anciNotificationStartCutOverTime;
	}

	/**
	 * @param anciNotificationStartCutOverTime
	 *            the anciNotificationStartCutOverTime to set
	 */
	public void setAnciNotificationStartCutOverTime(Integer anciNotificationStartCutOverTime) {
		this.anciNotificationStartCutOverTime = anciNotificationStartCutOverTime;
	}

	/**
	 * @return the anciNotificationEndCutOverTime
	 */
	public Integer getAnciNotificationEndCutOverTime() {
		return anciNotificationEndCutOverTime;
	}

	/**
	 * @param anciNotificationEndCutOverTime
	 *            the anciNotificationEndCutOverTime to set
	 */
	public void setAnciNotificationEndCutOverTime(Integer anciNotificationEndCutOverTime) {
		this.anciNotificationEndCutOverTime = anciNotificationEndCutOverTime;
	}

	/**
	 * @return the flightOrigin
	 */
	public String getFlightOrigin() {
		return flightOrigin;
	}

	/**
	 * @param flightOrigin
	 *            the flightOrigin to set
	 */
	public void setFlightOrigin(String flightOrigin) {
		this.flightOrigin = flightOrigin;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the notificationReminderTime
	 */
	public Date getNotificationReminderTime() {
		return notificationReminderTime;
	}

	/**
	 * @param notificationReminderTime
	 *            the notificationReminderTime to set
	 */
	public void setNotificationReminderTime(Date notificationReminderTime) {
		this.notificationReminderTime = notificationReminderTime;
	}

	/**
	 * @return the flightSegmentNotificationEventId
	 */
	public Integer getFlightSegmentNotificationEventId() {
		return flightSegmentNotificationEventId;
	}

	/**
	 * @param flightSegmentNotificationEventId
	 *            the flightSegmentNotificationEventId to set
	 */
	public void setFlightSegmentNotificationEventId(Integer flightSegmentNotificationEventId) {
		this.flightSegmentNotificationEventId = flightSegmentNotificationEventId;
	}

	/**
	 * @return the airportOnlineCheckin
	 */
	public String getAirportOnlineCheckin() {
		return airportOnlineCheckin;
	}

	/**
	 * @param airportOnlineCheckin
	 *            the airportOnlineCheckin to set
	 */
	public void setAirportOnlineCheckin(String airportOnlineCheckin) {
		this.airportOnlineCheckin = airportOnlineCheckin;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName
	 *            the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the jobGroupName
	 */
	public String getJobGroupName() {
		return JobGroupName;
	}

	/**
	 * @param jobGroupName
	 *            the jobGroupName to set
	 */
	public void setJobGroupName(String jobGroupName) {
		JobGroupName = jobGroupName;
	}

}
