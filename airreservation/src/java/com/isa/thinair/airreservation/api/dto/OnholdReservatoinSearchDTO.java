package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

public class OnholdReservatoinSearchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date bookingTimeStampBeforeStart;

	private String originCarrierCode;

	private int ownerChannelCode;
	
	private Date bookingTimeStampBeforeEnd;

	public Date getBookingTimeStampBeforeStart() {
		return bookingTimeStampBeforeStart;
	}

	public void setBookingTimeStampBeforeStart(Date bookingTimeStampBefore) {
		this.bookingTimeStampBeforeStart = bookingTimeStampBefore;
	}
	
	public Date getBookingTimeStampBeforeEnd() {
		return bookingTimeStampBeforeEnd;
	}

	public void setBookingTimeStampBeforeEnd(Date bookingTimeStampBeforeEnd) {
		this.bookingTimeStampBeforeEnd = bookingTimeStampBeforeEnd;
	}

	public String getOriginCarrierCode() {
		return originCarrierCode;
	}

	public void setOriginCarrierCode(String originCarrierCode) {
		this.originCarrierCode = originCarrierCode;
	}

	public int getOwnerChannelCode() {
		return ownerChannelCode;
	}

	public void setOwnerChannelCode(int ownerChannelCode) {
		this.ownerChannelCode = ownerChannelCode;
	}

}