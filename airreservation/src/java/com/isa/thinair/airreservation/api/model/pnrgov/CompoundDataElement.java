package com.isa.thinair.airreservation.api.model.pnrgov;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;

public class CompoundDataElement extends EDIDataElement {

	protected List<BasicDataElement> basicDataElements = new ArrayList<BasicDataElement>();

	protected DE_STATUS status;

	public CompoundDataElement(String elementTag, DE_STATUS status) {
		super();
		this.elementTag = elementTag;
		this.status = status;
	}

	@Override
	public String getEDIFmtData() throws PNRGOVException {
		if (!isValidDataFormat()) {
			throw new PNRGOVException("Mandatory data element not found for : " + this);
		}

		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < basicDataElements.size(); i++) {
			EDIDataElement dataElement = basicDataElements.get(i);

			try {
				if (i == 0) {
					stringBuilder.append(dataElement.getEDIFmtData());
				} else {
					stringBuilder.append(ElementFactory.SUB_ELEMENT_SEPERATOR + dataElement.getEDIFmtData());
				}
			} catch (PNRGOVException e) {
				StringBuffer errorMessage = new StringBuffer("[" + elementTag + "]" + e.getMessage());
				throw new PNRGOVException(errorMessage.toString());
			}
		}
		String formattedData = stringBuilder.toString();
		return StringUtils.stripEnd(formattedData, ElementFactory.SUB_ELEMENT_SEPERATOR);
	}

	@Override
	public boolean isValidDataFormat() {
		if (basicDataElements.isEmpty() && status.equals(DE_STATUS.M)) {
			return false;
		}
		return true;
	}
	
	public void addBasicDataelement(BasicDataElement element) {
		if(element != null) {
			basicDataElements.add(element);
		}
	}

}
