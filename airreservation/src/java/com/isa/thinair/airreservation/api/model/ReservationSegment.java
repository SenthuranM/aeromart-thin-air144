/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of reservation segment information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNR_SEGMENT"
 */
public class ReservationSegment extends Persistent implements Serializable {

	private static final long serialVersionUID = -1742077331355336073L;

	/** Holds reservation segment id */
	private Integer pnrSegId;

	/** Holds reservation segment sequence */
	private Integer segmentSeq;

	/** Holds reservation segment return flag */
	private String returnFlag;

	/** Holds reservation segment status */
	private String status;

	/** Holds flight segment id */
	private Integer flightSegId;

	/** Holds the ond group id */
	private Integer ondGroupId;

	/** Holds alert status */
	private int alertFlag;

	/**
	 * The meal re-protect status. value : 1 - There are one or more meals that were not transfered value : 0 - There
	 * are no meals that are not transfered. OR there was no re-protect occurred.
	 */
	private int mealReprotectStatus;

	/**
	 * The baggage re-protect status. value : 1 - There are one or more baggages that were not transfered value : 0 -
	 * There are no baggages that are not transfered. OR there was no re-protect occurred.
	 */
	private int baggageReprotectStatus;

	/**
	 * The seat re-protect status. value : 1 - There are one or more meals that were not transfered value : 0 - There
	 * are no meals that are not transfered. OR there was no re-protect occurred.
	 */
	private int seatReprotectStatus;

	/** Holds the reservation instance */
	private Reservation reservation;

	/** Holds the status modified date */
	private Date statusModifiedDate;

	/** Holds the status modified user id */
	private String statusModifiedUserId;

	/** Holds the status modified customer id */
	private Integer statusModifiedCustomerId;

	/** Holds the status modified sales channel code */
	private Integer statusModifiedSalesChannelCode;

	/** Holds the status modified agent code */
	private String statusModifiedAgentCode;

	/** Holds the booking class Code */
	private String bookingCode;

	/** Holds the booking type */
	private String bookingType;

	/** Holds the cabin class code relavant to the booking code */
	private String cabinClassCode;

	/** Holds the logical cabin class code relevant to the booking code */
	private String logicalCCCode;

	/** Holds the fare TO */
	private FareTO fareTO;

	/** Holds the return group id */
	private Integer returnGroupId;

	/** Holds the open return confirm before */
	private Date openReturnConfirmBeforeZulu;

	/** Holds the return ond group id */
	private Integer returnOndGroupId;

	/** Holds the Marketing Carrier Code */
	private String marketingCarrierCode;

	/** Holds the Marketing Station Code */
	private String marketingStationCode;

	/** Holds the Marketing Agent Code */
	private String marketingAgentCode;

	/** Holds the Marketing User ID */
	private String marketingUserID;

	/** Holds the Marketing User ID */
	private String mealSelectionStatus = "NONE";

	/** Holds the Marketing User ID */
	private String baggageSelectionStatus = "NONE";

	/** Holds the Marketing User ID */
	private String seatSelectionStatus = "NONE";

	/** Holds the Short (Name) Code of the Sub Airport */
	private String subStationShortName;

	/** Holds the Originator Systems Reference (e.g. Amadeus) */
	private String externalReference;

	/** Holds the attached surface segment with parent if it is present */
	private ReservationSegment groundSegment;

	/** Last fare quoted date */
	private Date lastFareQuotedDateZulu;

	/** Holds the baggage ond group id */
	private Integer baggageOndGroupId;

	/** Ticket valid till date */
	private Date ticketValidTill;

	private Integer journeySequence;

	private String subStatus;

	private Integer autoCancellationId;

	/** Holds ReservationSegmentID of modified segment */
	private Integer modifiedFrom;

	private Integer bundledFarePeriodId;
	
	private String codeShareFlightNo;
	
	private String codeShareBc;
	
	private String csOcCarrierCode;
	
	private String csOcFlightNumber;

	/**
	 * @return Returns the marketingUserID.
	 * @hibernate.property column = "MKTING_USER_ID"
	 */
	public String getMarketingUserID() {
		return marketingUserID;
	}

	/**
	 * @param marketingUserID
	 *            The marketingUserID to set.
	 */
	public void setMarketingUserID(String marketingUserID) {
		this.marketingUserID = marketingUserID;
	}

	/**
	 * @return Returns the marketingCarrierCode.
	 * @hibernate.property column = "MKTING_CARRIER_CODE"
	 */
	public String getMarketingCarrierCode() {
		return marketingCarrierCode;
	}

	/**
	 * @param marketingCarrierCode
	 *            The marketingCarrierCode to set.
	 */
	public void setMarketingCarrierCode(String marketingCarrierCode) {
		this.marketingCarrierCode = marketingCarrierCode;
	}

	/**
	 * @return Returns the marketingStationCode.
	 * @hibernate.property column = "MKTING_STATION_CODE"
	 */
	public String getMarketingStationCode() {
		return marketingStationCode;
	}

	/**
	 * @param marketingStationCode
	 *            The marketingStationCode to set.
	 */
	public void setMarketingStationCode(String marketingStationCode) {
		this.marketingStationCode = marketingStationCode;
	}

	/**
	 * @return Returns the marketingAgentCode.
	 * @hibernate.property column = "MKTING_AGENT_CODE"
	 */
	public String getMarketingAgentCode() {
		return marketingAgentCode;
	}

	/**
	 * @param marketingAgentCode
	 *            The marketingAgentCode to set.
	 */
	public void setMarketingAgentCode(String marketingAgentCode) {
		this.marketingAgentCode = marketingAgentCode;
	}

	/**
	 * @return Returns the flightSegId.
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            The flightSegId to set.
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @return Returns the pnrSegId.
	 * @hibernate.id column = "PNR_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_SEGMENT"
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the returnFlag.
	 * @hibernate.property column = "RETURN_FLAG"
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            The returnFlag to set.
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	/**
	 * @return Returns the segmentSeq.
	 * @hibernate.property column = "SEGMENT_SEQ"
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            The segmentSeq to set.
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "SUB_STATUS"
	 */
	public String getSubStatus() {
		return subStatus;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	/**
	 * @return Returns the ondGroupId.
	 * @hibernate.property column = "OND_GROUP_ID"
	 */
	public Integer getOndGroupId() {
		return ondGroupId;
	}

	/**
	 * @param ondGroupId
	 *            The ondGroupId to set.
	 */
	public void setOndGroupId(Integer ondGroupId) {
		this.ondGroupId = ondGroupId;
	}

	/**
	 * @hibernate.many-to-one column="PNR" class="com.isa.thinair.airreservation.api.model.Reservation"
	 * @return Returns the reservation.
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation
	 *            The reservation to set.
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrSegId()).toHashCode();
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	@Override
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationSegment) {
			ReservationSegment castObject = (ReservationSegment) o;
			if (castObject.getPnrSegId() == null || this.getPnrSegId() == null) {
				return false;
			} else if (castObject.getPnrSegId().intValue() == this.getPnrSegId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @return Returns the alertFlag.
	 * @hibernate.property column = "ALERT_FLAG"
	 */
	public int getAlertFlag() {
		return alertFlag;
	}

	/**
	 * @param alertFlag
	 *            The alertFlag to set.
	 */
	public void setAlertFlag(int alertFlag) {
		this.alertFlag = alertFlag;
	}

	/**
	 * @return Returns the statusModifiedAgentCode.
	 * @hibernate.property column = "STATUS_AGENT_CODE"
	 */
	public String getStatusModifiedAgentCode() {
		return statusModifiedAgentCode;
	}

	/**
	 * @param statusModifiedAgentCode
	 *            The statusModifiedAgentCode to set.
	 */
	public void setStatusModifiedAgentCode(String statusModifiedAgentCode) {
		this.statusModifiedAgentCode = statusModifiedAgentCode;
	}

	/**
	 * @return Returns the statusModifiedCustomerId.
	 * @hibernate.property column = "STATUS_MOD_CUSTOMER_ID"
	 */
	public Integer getStatusModifiedCustomerId() {
		return statusModifiedCustomerId;
	}

	/**
	 * @param statusModifiedCustomerId
	 *            The statusModifiedCustomerId to set.
	 */
	public void setStatusModifiedCustomerId(Integer statusModifiedCustomerId) {
		this.statusModifiedCustomerId = statusModifiedCustomerId;
	}

	/**
	 * @return Returns the statusModifiedDate.
	 * @hibernate.property column = "STATUS_MOD_DATE"
	 */
	public Date getStatusModifiedDate() {
		return statusModifiedDate;
	}

	/**
	 * @param statusModifiedDate
	 *            The statusModifiedDate to set.
	 */
	public void setStatusModifiedDate(Date statusModifiedDate) {
		this.statusModifiedDate = statusModifiedDate;
	}

	/**
	 * @return Returns the statusModifiedSalesChannelCode.
	 * @hibernate.property column = "STATUS_MOD_CHANNEL_CODE"
	 */
	public Integer getStatusModifiedSalesChannelCode() {
		return statusModifiedSalesChannelCode;
	}

	/**
	 * @param statusModifiedSalesChannelCode
	 *            The statusModifiedSalesChannelCode to set.
	 */
	public void setStatusModifiedSalesChannelCode(Integer statusModifiedSalesChannelCode) {
		this.statusModifiedSalesChannelCode = statusModifiedSalesChannelCode;
	}

	/**
	 * @return Returns the statusModifiedUserId.
	 * @hibernate.property column = "STATUS_MOD_USER_ID"
	 */
	public String getStatusModifiedUserId() {
		return statusModifiedUserId;
	}

	/**
	 * @param statusModifiedUserId
	 *            The statusModifiedUserId to set.
	 */
	public void setStatusModifiedUserId(String statusModifiedUserId) {
		this.statusModifiedUserId = statusModifiedUserId;
	}

	/**
	 * Use this if really requires or else see @see tag
	 * 
	 * @return Returns the bookingType.
	 * @throws ModuleException
	 * @see ReservationApiUtils.getBookingTypes
	 */
	public String getBookingType() throws ModuleException {
		// Booking Type proxy to improve performance
		if (this.bookingType == null) {
			Map<Integer, BookingClass> mapPnrSegIdAndBookingClass = ReservationApiUtils.getBookingClasses(this.reservation);
			ReservationSegment reservationSegment;
			BookingClass bookingClass;

			for (ReservationSegment reservationSegment2 : this.reservation.getSegments()) {
				reservationSegment = reservationSegment2;
				bookingClass = mapPnrSegIdAndBookingClass.get(reservationSegment.getPnrSegId());

				if (bookingClass != null) {
					reservationSegment.bookingType = bookingClass.getBcType();
					reservationSegment.cabinClassCode = bookingClass.getCabinClassCode();
					reservationSegment.logicalCCCode = bookingClass.getLogicalCCCode();
					reservationSegment.bookingCode = bookingClass.getBookingCode();
				} else {
					reservationSegment.bookingType = "";
					reservationSegment.cabinClassCode = "";
					reservationSegment.logicalCCCode = "";
					reservationSegment.bookingCode = "";
				}
			}

			bookingClass = mapPnrSegIdAndBookingClass.get(this.pnrSegId);

			if (bookingClass != null) {
				return bookingClass.getBcType();
			} else {
				return "";
			}
		} else {
			return this.bookingType;
		}
	}

	/**
	 * @return Returns the logicalCCCode.
	 */
	public String getLogicalCCCode() throws ModuleException {
		// If booking type is loaded logical cabin class code will exist too
		this.getBookingType();
		return this.logicalCCCode;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() throws ModuleException {
		// If booking type is loaded cabin class code will exist too
		this.getBookingType();
		return this.cabinClassCode;
	}

	/**
	 * @return the booking Code.
	 */
	public String getBookingCode() throws ModuleException {
		// If booking type is loaded booking Code will exist too
		this.getBookingType();
		return this.bookingCode;
	}

	/**
	 * Returns the FareTO
	 * 
	 * @param string
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public FareTO getFareTO(String prefferedLanguage) throws ModuleException {
		// FareTO proxy to improve performance
		if (this.fareTO == null) {
			Map<Integer, FareTO> pnrSegIdAndFareTO = ReservationApiUtils.getFareTOs(this.reservation, prefferedLanguage);

			ReservationSegment reservationSegment;
			FareTO fareTO;
			for (ReservationSegment reservationSegment2 : this.reservation.getSegments()) {
				reservationSegment = reservationSegment2;
				fareTO = pnrSegIdAndFareTO.get(reservationSegment.getPnrSegId());

				if (fareTO != null) {
					reservationSegment.fareTO = fareTO;
				} else {
					reservationSegment.fareTO = new FareTO();
				}
			}
			return this.fareTO;
		} else {
			return this.fareTO;
		}
	}

	/**
	 * Use this if really requires
	 * 
	 * @return Returns the bookingType.
	 * @throws ModuleException
	 */
	public Integer getReturnGrpId() throws ModuleException {
		// Booking Type proxy to improve performance
		if (this.returnGroupId == null) {
			boolean isReturnSegmentsExist = ReservationApiUtils.isReturnSegmentExist(this.reservation.getSegments());

			if (isReturnSegmentsExist) {
				Map<Collection<Integer>, Collection<Integer>> mapPnrSegIdsAndInversePnrSegIds = ReservationApiUtils
						.getInverseSegments(this.reservation, true);
				Map<Integer, Collection<Integer>> ondGrpWiseSegments = ReservationApiUtils.getOndGrpWiseSegments(this.reservation
						.getSegments());
				Map<Integer, Integer> pnrSegIdAndReturnGrpId = new HashMap<Integer, Integer>();

				ReservationSegment reservationSegment;
				Collection<Integer> pnrSegIds;
				Collection<Integer> inversePnrSegIds;
				Integer returnGrpId;

				int index = 0;
				for (ReservationSegment reservationSegment2 : this.reservation.getSegments()) {
					reservationSegment = reservationSegment2;
					returnGrpId = pnrSegIdAndReturnGrpId.get(reservationSegment.getPnrSegId());

					if (returnGrpId == null) {
						pnrSegIds = ondGrpWiseSegments.get(reservationSegment.getOndGroupId());
						inversePnrSegIds = mapPnrSegIdsAndInversePnrSegIds.get(pnrSegIds);

						if (pnrSegIds != null && pnrSegIds.size() > 0 && inversePnrSegIds != null && inversePnrSegIds.size() > 0) {
							index++;
							this.updateReturnIds(pnrSegIdAndReturnGrpId, pnrSegIds, index);
							this.updateReturnIds(pnrSegIdAndReturnGrpId, inversePnrSegIds, index);
							reservationSegment.setReturnGroupId(index);
						} else {
							this.updateReturnIds(pnrSegIdAndReturnGrpId, pnrSegIds, -1);
							this.updateReturnIds(pnrSegIdAndReturnGrpId, inversePnrSegIds, -1);
							reservationSegment.setReturnGroupId(-1);
						}
					} else {
						reservationSegment.setReturnGroupId(returnGrpId);
					}
				}
			} else {
				ReservationSegment reservationSegment;
				for (ReservationSegment reservationSegment2 : this.reservation.getSegments()) {
					reservationSegment = reservationSegment2;
					reservationSegment.setReturnGroupId(-1);
				}
			}

			return this.returnGroupId;
		} else {
			return this.returnGroupId;
		}
	}

	/**
	 * Updates the return id(s)
	 * 
	 * @param pnrSegIdAndReturnGrpId
	 * @param pnrSegIds
	 * @param index
	 * @return
	 */
	private void updateReturnIds(Map<Integer, Integer> pnrSegIdAndReturnGrpId, Collection<Integer> pnrSegIds, int index) {
		if (pnrSegIds != null && pnrSegIds.size() > 0) {
			Integer integer;
			for (Integer integer2 : pnrSegIds) {
				integer = integer2;
				pnrSegIdAndReturnGrpId.put(integer, index);
			}
		}
	}

	/**
	 * @param returnGroupId
	 *            the returnGroupId to set
	 */
	private void setReturnGroupId(Integer returnGroupId) {
		this.returnGroupId = returnGroupId;
	}

	/**
	 * @return the openReturnConfirmBeforeZulu
	 * @hibernate.property column = "OPEN_RT_CONFIRM_BEFORE"
	 */
	public Date getOpenReturnConfirmBeforeZulu() {
		return openReturnConfirmBeforeZulu;
	}

	/**
	 * @param openReturnConfirmBeforeZulu
	 *            the openReturnConfirmBeforeZulu to set
	 */
	public void setOpenReturnConfirmBeforeZulu(Date openReturnConfirmBeforeZulu) {
		this.openReturnConfirmBeforeZulu = openReturnConfirmBeforeZulu;
	}

	@Override
	public Object clone() {
		ReservationSegment reservationSegment = new ReservationSegment();
		reservationSegment.setAlertFlag(this.getAlertFlag());
		reservationSegment.setFlightSegId(this.getFlightSegId());
		reservationSegment.setOndGroupId(this.getOndGroupId());
		reservationSegment.setOpenReturnConfirmBeforeZulu(this.getOpenReturnConfirmBeforeZulu());
		reservationSegment.setReservation(this.getReservation());
		reservationSegment.setSegmentSeq(this.getSegmentSeq());
		reservationSegment.setStatus(this.getStatus());
		reservationSegment.setStatusModifiedAgentCode(this.getStatusModifiedAgentCode());
		reservationSegment.setStatusModifiedCustomerId(this.getStatusModifiedCustomerId());
		reservationSegment.setStatusModifiedDate(this.getStatusModifiedDate());
		reservationSegment.setStatusModifiedSalesChannelCode(this.getStatusModifiedSalesChannelCode());
		reservationSegment.setStatusModifiedUserId(this.getStatusModifiedUserId());
		reservationSegment.setReturnFlag(this.getReturnFlag());
		reservationSegment.setLastFareQuotedDateZulu(this.getLastFareQuotedDateZulu());
		reservationSegment.setReturnOndGroupId(this.getReturnOndGroupId());
		reservationSegment.setJourneySequence(this.getJourneySequence());
		reservationSegment.setCodeShareFlightNo(this.getCodeShareFlightNo());
		reservationSegment.setCodeShareBc(this.getCodeShareBc());
		return reservationSegment;
	}

	/**
	 * @return Returns the returnOndGroupId.
	 * @hibernate.property column = "RETURN_GROUP_ID"
	 */
	public Integer getReturnOndGroupId() {
		return returnOndGroupId;
	}

	/**
	 * @param returnOndGroupId
	 *            The returnOndGroupId to set.
	 */
	public void setReturnOndGroupId(Integer returnOndGroupId) {
		this.returnOndGroupId = returnOndGroupId;
	}

	/**
	 * @return the mealSelectionStatus
	 * @hibernate.property column = "MEAL_SELECTION_STATUS"
	 */
	public String getMealSelectionStatus() {
		return mealSelectionStatus;
	}

	/**
	 * @param mealSelectionStatus
	 *            the mealSelectionStatus to set
	 */
	public void setMealSelectionStatus(String mealSelectionStatus) {
		this.mealSelectionStatus = mealSelectionStatus;
	}

	/**
	 * @return the seatSelectionStatus
	 * @hibernate.property column = "SEAT_SELECTION_STATUS"
	 */
	public String getSeatSelectionStatus() {
		return seatSelectionStatus;
	}

	/**
	 * @param seatSelectionStatus
	 *            the seatSelectionStatus to set
	 */
	public void setSeatSelectionStatus(String seatSelectionStatus) {
		this.seatSelectionStatus = seatSelectionStatus;
	}

	/**
	 * @return the subStationShortName
	 * @hibernate.property column = "SUB_STATION_SHORTNAME"
	 */
	public String getSubStationShortName() {
		return subStationShortName;
	}

	public void setSubStationShortName(String subStationShortName) {
		this.subStationShortName = subStationShortName;
	}

	/**
	 * @return the subStationShortName
	 * @hibernate.property column = "EXT_REF"
	 */
	public String getExternalReference() {
		return externalReference;
	}

	/**
	 * @param originatorReference
	 *            the originatorReference to set
	 */
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * Returns the linked surface segment with parent segment
	 * 
	 * @return
	 * @hibernate.many-to-one column="GROUND_SEG_ID" cascade="all"
	 *                        class="com.isa.thinair.airreservation.api.model.ReservationSegment"
	 */
	public ReservationSegment getGroundSegment() {
		return groundSegment;
	}

	public void setGroundSegment(ReservationSegment groundSegment) {
		this.groundSegment = groundSegment;
	}

	/**
	 * @return the seatSelectionStatus
	 * @hibernate.property column = "MEAL_REPROTECT_STATUS"
	 */
	public int getMealReprotectStatus() {
		return mealReprotectStatus;
	}

	/**
	 * @param mealReprotectStatus
	 *            : The meal re-protect status to set.
	 */
	public void setMealReprotectStatus(int mealReprotectStatus) {
		this.mealReprotectStatus = mealReprotectStatus;
	}

	/**
	 * @return the seatSelectionStatus
	 * @hibernate.property column = "SEAT_REPROTECT_STATUS"
	 */
	public int getSeatReprotectStatus() {
		return seatReprotectStatus;
	}

	/**
	 * @param seatReprotectStatus
	 *            The seat re-protect status to set.
	 */
	public void setSeatReprotectStatus(int seatReprotectStatus) {
		this.seatReprotectStatus = seatReprotectStatus;
	}

	/**
	 * @return the baggageReprotectStatus
	 * @hibernate.property column = "BAGGAGE_REPROTECT_STATUS"
	 */
	public int getBaggageReprotectStatus() {
		return baggageReprotectStatus;
	}

	public void setBaggageReprotectStatus(int baggageReprotectStatus) {
		this.baggageReprotectStatus = baggageReprotectStatus;
	}

	/**
	 * @return the baggageSelectionStatus
	 * @hibernate.property column = "BAGGAGE_SELECTION_STATUS"
	 */
	public String getBaggageSelectionStatus() {
		return baggageSelectionStatus;
	}

	public void setBaggageSelectionStatus(String baggageSelectionStatus) {
		this.baggageSelectionStatus = baggageSelectionStatus;
	}

	/**
	 * @return the ticketValidTill
	 * @hibernate.property column = "TICKET_VALID_TILL"
	 */
	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	/**
	 * @param ticketValidTill
	 *            the ticketValidTill to set
	 */
	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	/**
	 * @return the baggageSelectionStatus
	 * @hibernate.property column = "LAST_FQ_DATE"
	 */
	public Date getLastFareQuotedDateZulu() {
		return lastFareQuotedDateZulu;
	}

	public void setLastFareQuotedDateZulu(Date lastFareQuotedDateZulu) {
		this.lastFareQuotedDateZulu = lastFareQuotedDateZulu;
	}

	public void setJourneySequence(Integer journeySequence) {
		this.journeySequence = journeySequence;
	}

	/**
	 * @return the baggageSelectionStatus
	 * @hibernate.property column = "JOURNEY_SEQ"
	 */
	public Integer getJourneySequence() {
		return journeySequence;
	}

	/**
	 * @return the modifiedFrom
	 * @hibernate.property column = "AUTO_CANCELLATION_ID"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	/**
	 * @return the modifiedFrom
	 * @hibernate.property column = "MODIFIED_FROM"
	 */
	public Integer getModifiedFrom() {
		return modifiedFrom;
	}

	/**
	 * @param modifiedFrom
	 *            the modifiedFromId to set
	 */
	public void setModifiedFrom(Integer modifiedFrom) {
		this.modifiedFrom = modifiedFrom;
	}

	/**
	 * @return the baggageOndGroupId
	 * @hibernate.property column = "BAGGAGE_OND_GROUP_ID"
	 */
	public Integer getBaggageOndGroupId() {
		return baggageOndGroupId;
	}

	/**
	 * @param baggageOndGroupId
	 *            the baggageOndGroupId to set
	 */
	public void setBaggageOndGroupId(Integer baggageOndGroupId) {
		this.baggageOndGroupId = baggageOndGroupId;
	}

	/**
	 * @return the bundledFarePeriodId
	 * @hibernate.property column = "BUNDLED_FARE_PERIOD_ID"
	 */
	public Integer getBundledFarePeriodId() {
		return bundledFarePeriodId;
	}

	public void setBundledFarePeriodId(Integer bundleFarePeriodId) {
		this.bundledFarePeriodId = bundleFarePeriodId;
	}

	/**
	 * @return the codeShareFlightNo
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	/**
	 * @return the codeShareBc
	 * @hibernate.property column = "CS_BOOKING_CLASS"
	 */
	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}

	public String getCsOcCarrierCode() {
		return csOcCarrierCode;
	}

	public void setCsOcCarrierCode(String csOcCarrierCode) {
		this.csOcCarrierCode = csOcCarrierCode;
	}

	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}

	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}
}
