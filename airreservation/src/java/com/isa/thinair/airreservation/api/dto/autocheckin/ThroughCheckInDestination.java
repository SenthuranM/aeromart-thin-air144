/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.autocheckin;

import java.io.Serializable;

/**
 * @author Panchatcharam.S
 *
 */
public class ThroughCheckInDestination implements Serializable {

	private static final long serialVersionUID = 3805620898208047739L;
	private String locationCode;

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
}
