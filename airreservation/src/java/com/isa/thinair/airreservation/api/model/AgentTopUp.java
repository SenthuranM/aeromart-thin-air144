/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * TopUp is the entity class to represent a top up transaction model
 * 
 * @author :lsandeepa
 * @hibernate.class table = "T_AGENT_TOP_UP_TRANSFER"
 */
public class AgentTopUp extends Persistent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3968602299542524909L;
	private Integer agentTopUpTranferId;
	private Integer tnxId;
	private Date tnxDate;
	private String agentCode;
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal amountLocal = AccelAeroCalculator.getDefaultBigDecimalZero();
	private String currencyCode;
	private String ncDestription;
	private boolean transferStatus;
	private Date transferTimeStamp;

	/**
	 * @return Returns the agent_topUp_tranfer_id.
	 * @hibernate.id column = "AGENT_TOP_UP_TRANSFER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "s_agent_top_up_transfer"
	 */
	public Integer getAgentTopUpTranferId() {
		return agentTopUpTranferId;
	}

	public void setAgentTopUpTranferId(Integer agentTopUpTranferId) {
		this.agentTopUpTranferId = agentTopUpTranferId;
	}

	/**
	 * returns the tnxId
	 * 
	 * @return Returns the tnxId
	 * @hibernate.property column = "TXN_ID"
	 * 
	 */
	public Integer getTnxId() {
		return tnxId;
	}

	public void setTnxId(Integer tnxId) {
		this.tnxId = tnxId;
	}
	/**
	 * returns the tnxDate
	 * 
	 * @return Returns the tnxDate.
	 * @hibernate.property column = "TNX_DATE"
	 */
	public Date getTnxDate() {
		return tnxDate;
	}

	public void setTnxDate(Date tnxDate) {
		this.tnxDate = tnxDate;
	}

	/**
	 * returns the agentCode
	 * 
	 * @return Returns the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * returns the amount
	 * 
	 * @return Returns the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * returns the amountLocal
	 * 
	 * @return Returns the amountLocal
	 * @hibernate.property column = "AMOUNT_LOCAL"
	 */
	public BigDecimal getAmountLocal() {
		return amountLocal;
	}

	public void setAmountLocal(BigDecimal amountLocal) {
		this.amountLocal = amountLocal;
	}

	/**
	 * returns the correncyCode
	 * 
	 * @return Returns the correncyCode
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * returns the ncDestription
	 * 
	 * @return Returns the ncDestription
	 * @hibernate.property column = "NC_DESCRIPTION"
	 */
	public String getNcDestription() {
		return ncDestription;
	}

	public void setNcDestription(String ncDestription) {
		this.ncDestription = ncDestription;
	}

	/**
	 * returns the transferStatus
	 * 
	 * @return Returns the transferStatus
	 * @hibernate.property column = "TRANSFER_STATUS" type = "yes_no"
	 */
	public boolean getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(boolean transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * returns the transferTimeStamp
	 * 
	 * @return Returns the transferTimeStamp
	 * @hibernate.property column = "TRANSFER_TIMESTAMP"
	 */
	public Date getTransferTimeStamp() {
		return transferTimeStamp;
	}
	public void setTransferTimeStamp(Date transferTimeStamp) {
		this.transferTimeStamp = transferTimeStamp;
	}
}
