/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.util.Date;

/**
 * Holds Passenger final sales data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaxFinalSalesDTO {

	private String departureAirportCode;

	private String arrivalAirportCode;

	private String category;

	private String cabinClassCode;

	private String paxType;

	private String title;

	private String firstName;

	private String lastName;

	private String pnr;

	private String day;

	private String month;

	private Date realFlightDate;

	private String flightNumber;

	private int pfsId;

	private Date dateDownloaded;
	
	private String marketingFlight;
	
	private String externalRecordLocator;
	
	private String codeShareFlightNo;
	
	private String codeShareBc;
	
	private Integer gdsId;
	
	private String bookingClassCode;
	
	private boolean codeShareEmptyPfs;

	/**
	 * @return Returns the arrivalAirportCode.
	 */
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	/**
	 * @param arrivalAirportCode
	 *            The arrivalAirportCode to set.
	 */
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	/**
	 * @return Returns the category.
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            The category to set.
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return Returns the departureAirportCode.
	 */
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	/**
	 * @param departureAirportCode
	 *            The departureAirportCode to set.
	 */
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	/**
	 * @return Returns the firstname.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstname
	 *            The firstname to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastname.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastname
	 *            The lastname to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the pfsId.
	 */
	public int getPfsId() {
		return pfsId;
	}

	/**
	 * @param pfsId
	 *            The pfsId to set.
	 */
	public void setPfsId(int pfsId) {
		this.pfsId = pfsId;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the realFlightDate.
	 */
	public Date getRealFlightDate() {
		return realFlightDate;
	}

	/**
	 * @param realFlightDate
	 *            The realFlightDate to set.
	 */
	public void setRealFlightDate(Date realFlightDate) {
		this.realFlightDate = realFlightDate;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getMarketingFlight() {
		return marketingFlight;
	}

	public void setMarketingFlight(String marketingFlight) {
		this.marketingFlight = marketingFlight;
	}

	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public boolean isCodeShareEmptyPfs() {
		return codeShareEmptyPfs;
	}

	public void setCodeShareEmptyPfs(boolean codeShareEmptyPfs) {
		this.codeShareEmptyPfs = codeShareEmptyPfs;
	}	
	
}
