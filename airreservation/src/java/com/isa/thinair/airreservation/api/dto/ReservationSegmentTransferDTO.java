/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * To hold reservation segment transfer data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationSegmentTransferDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 407052001309475225L;

	/** Holds carrier code */
	private String carrierCode;

	/** Holds the transfer status */
	private String transferStatus;

	/** Holds the executed date */
	private Date executedDate;

	/** Holds the PNR */
	private String pnr;

	/** Holds segment code */
	private String segmentCode;

	/** Holds segment sequence */
	private Integer segmentSeq;

	/** Holds departure date */
	private Date departureDate;

	/** Holds arrival date */
	private Date arrivalDate;

	/** Holds zulu departure date */
	private Date zuluDepartureDate;

	/** Holds zulu arrival date */
	private Date zuluArrivalDate;

	/** Holds flight number */
	private String flightNo;

	/** Holds reservation segment id */
	private Integer pnrSegId;

	/** Holds reservation fare group id */
	private Integer fareGroupId;

	/** Holds the process description */
	private String processDesc;

	/**
	 * @return Returns the arrivalDate.
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            The arrivalDate to set.
	 */
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * @return Returns the carrierCode.
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the departureDate.
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the executedDate.
	 */
	public Date getExecutedDate() {
		return executedDate;
	}

	/**
	 * @param executedDate
	 *            The executedDate to set.
	 */
	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}

	/**
	 * @return Returns the fareGroupId.
	 */
	public Integer getFareGroupId() {
		return fareGroupId;
	}

	/**
	 * @param fareGroupId
	 *            The fareGroupId to set.
	 */
	public void setFareGroupId(Integer fareGroupId) {
		this.fareGroupId = fareGroupId;
	}

	/**
	 * @return Returns the flightNo.
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            The flightNo to set.
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the pnrSegId.
	 */
	public Integer getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the segmentSeq.
	 */
	public Integer getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @param segmentSeq
	 *            The segmentSeq to set.
	 */
	public void setSegmentSeq(Integer segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	/**
	 * @return Returns the transferStatus.
	 */
	public String getTransferStatus() {
		return transferStatus;
	}

	/**
	 * @param transferStatus
	 *            The transferStatus to set.
	 */
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * @return Returns the zuluArrivalDate.
	 */
	public Date getZuluArrivalDate() {
		return zuluArrivalDate;
	}

	/**
	 * @param zuluArrivalDate
	 *            The zuluArrivalDate to set.
	 */
	public void setZuluArrivalDate(Date zuluArrivalDate) {
		this.zuluArrivalDate = zuluArrivalDate;
	}

	/**
	 * @return Returns the zuluDepartureDate.
	 */
	public Date getZuluDepartureDate() {
		return zuluDepartureDate;
	}

	/**
	 * @param zuluDepartureDate
	 *            The zuluDepartureDate to set.
	 */
	public void setZuluDepartureDate(Date zuluDepartureDate) {
		this.zuluDepartureDate = zuluDepartureDate;
	}

	/**
	 * Return arrival date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getArrivalStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getArrivalDate(), dateFormat);
	}

	/**
	 * Return departure date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getDepartureStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getDepartureDate(), dateFormat);
	}

	/**
	 * Return executed date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getExecutedStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getExecutedDate(), dateFormat);
	}

	/**
	 * @return Returns the processDesc.
	 */
	public String getProcessDesc() {
		return processDesc;
	}

	/**
	 * @param processDesc
	 *            The processDesc to set.
	 */
	public void setProcessDesc(String processDesc) {
		this.processDesc = processDesc;
	}
}
