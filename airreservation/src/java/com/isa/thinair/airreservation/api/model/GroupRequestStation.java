package com.isa.thinair.airreservation.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Gayan
 * 
 * @hibernate.class table="t_group_request_station"
 */
public class GroupRequestStation extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long Id;

	private Long groupBookingRequestID;

	private String stationCode;

	/**
	 * @hibernate.id column = "GROUP_REQUEST_STATION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="s_group_request_station"
	 */
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	/**
	 * @hibernate.property column= "GROUP_BOOKING_REQUEST_ID"
	 * @return requestDate
	 */
	public Long getGroupBookingRequestID() {
		return groupBookingRequestID;
	}

	public void setGroupBookingRequestID(Long groupBookingRequestID) {
		this.groupBookingRequestID = groupBookingRequestID;
	}

	/**
	 * @hibernate.property column= "STATION_CODE"
	 * @return requestDate
	 */
	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

}
