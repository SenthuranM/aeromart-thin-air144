package com.isa.thinair.airreservation.api.model;

import java.util.Date;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;

public interface IResSegment {
	/**
	 * Add OND segment
	 * 
	 * @param ondSequence
	 * @param segmentSeq
	 * @param flightSegId
	 * @param ondGroupId
	 * @param returnOndGroupId
	 * @param openRtConfirmBefore
	 * @param externalRef
	 * @param isReturnOnd
	 * @param bundledFarePeriodId
	 */
	public void addOndSegment(int ondSequence, int segmentSeq, int flightSegId, Integer ondGroupId, Integer returnOndGroupId,
			Date openRtConfirmBefore, String externalRef, Integer baggageOndGroupId, boolean waitListing, boolean isReturnOnd,
			Integer bundledFarePeriodId, String codeShareFlightNumber, String codeShareBookingClass, String csOcCarrierCode,
			String csOcFlightNumber);

	/**
	 * 
	 * @param groundStationBySegmentMap
	 * @param linkedGroundSegmentFlightRefMap
	 * @throws ModuleException
	 */
	public void addGroundStationDataToPNRSegment(Map<Integer, String> groundStationBySegmentMap,
			Map<Integer, Integer> linkedGroundSegmentFlightRefMap) throws ModuleException;
}
