/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.model.assembler;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * PassengerAssembler is the main assembling utility for the Passenger Object
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PassengerAssembler implements Serializable, IPassenger {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5739801942268868479L;

	/* Holds the Reservation object */
	private Collection<ReservationPax> passengers;

	/* Holds the inventory fares */
	private Collection<OndFareDTO> colOndFareDTOs;

	/* Holds passenger payments map */
	@SuppressWarnings("rawtypes")
	private Map passengerPaymentsMap;

	/* Hold External Payment Transaction Information */
	private ExternalPaymentTnx externalPaymentTnxInfo;

	/* Holds the Tempory Payment Transaction Ids */
	private Map<Integer, CardPaymentInfo> mapTnxIds;

	/* Holds the pax segment ssrs */
	private Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRMaps;

	/**
	 * Construct Reservation Object Fares is for PASSENGER FARE GENERATION PURPOSES ONLY
	 */
	public PassengerAssembler(Collection<OndFareDTO> colOndFareDTOs) {
		setDummyPassengers(new HashSet<ReservationPax>());
		setColOndFareDTOs(colOndFareDTOs);
		setPassengerPaymentsMap(new HashMap<Integer, IPayment>());
		setTnxIds(new HashMap<Integer, CardPaymentInfo>());
		setPaxSegmentSSRDTOMaps(new HashMap<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>>());
	}

	/**
	 * Add an infant
	 * 
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param dateOfBirth
	 * @param nationalityCode
	 * @param paxSequence
	 * @param parentSeqId
	 * @param pnrPaxCatFOIDNumber
	 * @param ssrCode
	 * @param ssrRemarks
	 * @throws ModuleException
	 */
	public void addInfant(String firstName, String lastName, String title, Date dateOfBirth, Integer nationalityCode,
			int paxSequence, int parentSeqId, String pnrPaxCatFOIDNumber, Date pnrPaxCatFOIDExpiry,
			String pnrPaxCatFOIDIssuedCntry, String pnrPaxCatEmpId, Date pnrPaxCatDOJ, String pnrPaxCatIdCat, String eticketNo,
			String paxCategory, SegmentSSRAssembler segmentSSRAsm, Integer autoCancellationId, String nationalIDNo,
			String placeOfBirth, String visaDocNumber, String visaApplicableCountry, String visaDocPlaceOfIssue,
			Date visaDocIssueDate) throws ModuleException {

		ReservationPax passenger = new ReservationPax();
		passenger.setFirstName(firstName);
		passenger.setLastName(lastName);
		passenger.setTitle(title);
		passenger.setDateOfBirth(dateOfBirth);
		passenger.setNationalityCode(nationalityCode);
		passenger.setPaxSequence(new Integer(paxSequence));

		passenger.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
		passenger.setIndexId(new Integer(parentSeqId));
		passenger.setFamilyID(CommonsConstants.INDIVIDUAL_MEMBER);
		
		ReservationPaxAdditionalInfo addnInfo = new ReservationPaxAdditionalInfo();
		addnInfo.setPassportNo(pnrPaxCatFOIDNumber);
		addnInfo.setPassportExpiry(pnrPaxCatFOIDExpiry);
		addnInfo.setPassportIssuedCntry(pnrPaxCatFOIDIssuedCntry);
		addnInfo.setEmployeeID(pnrPaxCatEmpId);
		addnInfo.setDateOfJoin(pnrPaxCatDOJ);
		addnInfo.setIdCategory(pnrPaxCatIdCat);
		addnInfo.setNationalIDNo(nationalIDNo);
		addnInfo.setVisaApplicableCountry(visaApplicableCountry);
		addnInfo.setVisaDocIssueDate(visaDocIssueDate);
		addnInfo.setVisaDocNumber(visaDocNumber);
		addnInfo.setVisaDocPlaceOfIssue(visaDocPlaceOfIssue);
		addnInfo.setPlaceOfBirth(placeOfBirth);

		passenger.setPaxAdditionalInfo(addnInfo);

		passenger.setPaxCategory(paxCategory);
		passenger.setZuluStartTimeStamp(new Date());

		// This Zulu Release Time stamp will be overridden with parent's release time
		// when this infant is being added to the parent
		// Irrespective of On hold or Confirm this TimeStamp will be written to the database
		// The state is what matters at the end
		passenger.setZuluReleaseTimeStamp(BeanUtils.extendByMinutes(new Date(), AppSysParamsUtil.getOnholdDuration()));

		passenger.setAutoCancellationId(autoCancellationId);

		// Add the infant
		getDummyPassengers().add(passenger);
		if (segmentSSRAsm != null) {
			paxSegmentSSRMaps.put(passenger.getPaxSequence(), segmentSSRAsm.getNewSegmentSSRDTOMap());
		}
	}

	/**
	 * Adds the passenger payments
	 * 
	 * @param pnrPaxId
	 * @param iPayment
	 */
	@SuppressWarnings("unchecked")
	public void addPassengerPayments(Integer pnrPaxId, IPayment iPayment) {
		getPassengerPaymentsMap().put(pnrPaxId, iPayment);
	}

	/**
	 * @return Returns the passengers.
	 */
	public Collection<ReservationPax> getDummyPassengers() {
		return passengers;
	}

	/**
	 * @return Returns the passengerPaymentsMap.
	 */
	@SuppressWarnings("rawtypes")
	public Map getPassengerPaymentsMap() {
		return passengerPaymentsMap;
	}

	/**
	 * @return Returns the inventoryFares.
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> getInventoryFares() throws ModuleException {
		return getColOndFareDTOs();
	}

	/**
	 * @return the External Payment Transaction Info
	 */
	public ExternalPaymentTnx getExternalPaymentTnxInfo() {
		return externalPaymentTnxInfo;
	}

	/**
	 * @param externalPaymentTnxInfo
	 *            External Payment Transaction Info
	 */
	public void addExternalPaymentTnxInfo(ExternalPaymentTnx externalPaymentTnxInfo) {
		this.externalPaymentTnxInfo = externalPaymentTnxInfo;
	}

	/**
	 * Add Tempory Payment Entries
	 */
	public void addTemporyPaymentEntries(Map<Integer, CardPaymentInfo> mapTnxIds) {
		if (mapTnxIds != null && mapTnxIds.size() > 0) {
			setTnxIds(mapTnxIds);
			ReservationApiUtils.updateTemporyPaymentStates(getTnxIds());
		}
	}

	/**
	 * Returns Tempory payment transaction Ids
	 * 
	 * @return
	 */
	public Map<Integer, CardPaymentInfo> getTnxIds() {
		return mapTnxIds;
	}

	/**
	 * @param mapTnxIds
	 *            The mapTnxIds to set.
	 */
	private void setTnxIds(Map<Integer, CardPaymentInfo> mapTnxIds) {
		this.mapTnxIds = mapTnxIds;
	}

	/**
	 * @param passengers
	 *            The passengers to set.
	 */
	private void setDummyPassengers(Collection<ReservationPax> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @param passengerPaymentsMap
	 *            The passengerPaymentsMap to set.
	 */
	@SuppressWarnings("rawtypes")
	private void setPassengerPaymentsMap(Map passengerPaymentsMap) {
		this.passengerPaymentsMap = passengerPaymentsMap;
	}

	/**
	 * @return Returns the colOndFareDTOs.
	 */
	private Collection<OndFareDTO> getColOndFareDTOs() {
		return colOndFareDTOs;
	}

	/**
	 * @param colOndFareDTOs
	 *            The colOndFareDTOs to set.
	 */
	private void setColOndFareDTOs(Collection<OndFareDTO> colOndFareDTOs) {
		this.colOndFareDTOs = colOndFareDTOs;
	}

	public Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> getPaxSegmentSSRDTOMaps() {
		return paxSegmentSSRMaps;
	}

	public void setPaxSegmentSSRDTOMaps(Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRMaps) {
		this.paxSegmentSSRMaps = paxSegmentSSRMaps;
	}

}
