package com.isa.thinair.airreservation.api.model.pnrgov;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;

public abstract class BasicDataElement extends EDIDataElement {

	protected EDI_ELEMENT element;

	protected String data = null;

	public BasicDataElement(EDI_ELEMENT element) {
		super();
		this.element = element;
	}

	@Override
	public String getEDIFmtData() throws PNRGOVException {
		if (!isValidDataFormat()) {
			throw new PNRGOVException("[" + element + "]" + " Invalid data " + "[" + data + "]");
		}

		if (StringUtils.isEmpty(data)) {
			return StringUtils.EMPTY;
		} else {
			return StringUtils.trim(data);
		}

	}

	public boolean isValidLength() {
		int length = data.length();
		if (length < element.getMinLength()) {
			return false;
		}

		if (length > element.getMaxLength()) {
			return false;
		}

		return true;
	}

}
