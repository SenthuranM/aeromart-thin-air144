package com.isa.thinair.airreservation.api.dto.flightmanifest;

public class SegmentSummaryDTO extends ManifestCountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String segmentCode;

	public SegmentSummaryDTO(String segmentCode) {
		super();
		this.segmentCode = segmentCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

}
