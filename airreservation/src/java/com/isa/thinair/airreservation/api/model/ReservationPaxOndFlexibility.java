package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * The model to keep track of reservation fare level (OND) charges.
 * 
 * @author Charith Paranaliyanage
 * @hibernate.class table = "T_PNR_PAX_OND_FLEXIBILITY"
 */
public class ReservationPaxOndFlexibility extends Persistent implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The pnr pax ond flexi id. */
	private Integer pnrPaxOndFlexiId;

	/** The ppf id. */
	private Integer ppfId;

	/** The flexi rule rate id. */
	private Integer flexiRuleRateId;

	/** The flexi rule type id. */
	private Integer flexiRuleTypeId;

	/** The available count. */
	private Integer availableCount;

	/** The utilized count. */
	private Integer utilizedCount;

	/** The status. */
	private String status = STATUS_ACTIVE;

	/** The STATUS active. */
	public static String STATUS_ACTIVE = "ACT";

	/** The STATUS inactive. */
	public static String STATUS_INACTIVE = "INA";

	/**
	 * Gets the pnr pax ond flexi id.
	 * 
	 * @return the pnr pax ond flexi id
	 * @hibernate.id column = "PPONDFLX_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_OND_FLEXIBILITY"
	 */
	public Integer getPnrPaxOndFlexiId() {
		return pnrPaxOndFlexiId;
	}

	/**
	 * Sets the pnr pax ond flexi id.
	 * 
	 * @param pnrPaxOndFlexiId
	 *            the new pnr pax ond flexi id
	 */
	public void setPnrPaxOndFlexiId(Integer pnrPaxOndFlexiId) {
		this.pnrPaxOndFlexiId = pnrPaxOndFlexiId;
	}

	/**
	 * Gets the ppf id.
	 * 
	 * @return the ppf id
	 * @hibernate.property column = "PPF_ID"
	 */
	public Integer getPpfId() {
		return ppfId;
	}

	/**
	 * Sets the ppf id.
	 * 
	 * @param ppfId
	 *            the new ppf id
	 */
	public void setPpfId(Integer ppfId) {
		this.ppfId = ppfId;
	}

	/**
	 * Gets the flexi rule rate id.
	 * 
	 * @return the flexi rule rate id
	 * @hibernate.property column = "FLEXI_RULE_RATE_ID"
	 */
	public Integer getFlexiRuleRateId() {
		return flexiRuleRateId;
	}

	/**
	 * Sets the flexi rule rate id.
	 * 
	 * @param flexiRuleRateId
	 *            the new flexi rule rate id
	 */
	public void setFlexiRuleRateId(Integer flexiRuleRateId) {
		this.flexiRuleRateId = flexiRuleRateId;
	}

	/**
	 * Gets the flexi rule type id.
	 * 
	 * @return the flexi rule type id
	 * @hibernate.property column = "FLEXI_RULE_FLEXI_TYPE_ID"
	 */
	public Integer getFlexiRuleTypeId() {
		return flexiRuleTypeId;
	}

	/**
	 * Sets the flexi rule type id.
	 * 
	 * @param flexiRuleTypeId
	 *            the new flexi rule type id
	 */
	public void setFlexiRuleTypeId(Integer flexiRuleTypeId) {
		this.flexiRuleTypeId = flexiRuleTypeId;
	}

	/**
	 * Gets the available count.
	 * 
	 * @return the available count
	 * @hibernate.property column = "AVAILABLE_COUNT"
	 */
	public Integer getAvailableCount() {
		return availableCount;
	}

	/**
	 * Sets the available count.
	 * 
	 * @param availableCount
	 *            the new available count
	 */
	public void setAvailableCount(Integer availableCount) {
		this.availableCount = availableCount;
	}

	/**
	 * Gets the utilized count.
	 * 
	 * @return the utilized count
	 * @hibernate.property column = "UTILIZED_COUNT"
	 */
	public Integer getUtilizedCount() {
		return utilizedCount;
	}

	/**
	 * Sets the utilized count.
	 * 
	 * @param utilizedCount
	 *            the new utilized count
	 */
	public void setUtilizedCount(Integer utilizedCount) {
		this.utilizedCount = utilizedCount;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}