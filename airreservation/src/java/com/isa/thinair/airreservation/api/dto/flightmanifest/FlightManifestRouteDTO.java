package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.io.Serializable;

/**
 * 
 * @author nafly
 * 
 */
public class FlightManifestRouteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String origin;

	private String destination;

	private boolean isPossibleMissConnection = false;

	private Double inboundConnectionTime = null;

	private Double outboundConnectionTime = null;

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the isPossibleMissConnection
	 */
	public boolean isPossibleMissConnection() {
		return isPossibleMissConnection;
	}

	/**
	 * @param isPossibleMissConnection
	 *            the isPossibleMissConnection to set
	 */
	public void setPossibleMissConnection(boolean isPossibleMissConnection) {
		this.isPossibleMissConnection = isPossibleMissConnection;
	}

	public Double getInboundConnectionTime() {
		return inboundConnectionTime;
	}

	public void setInboundConnectionTime(Double inboundConnectionTime) {
		this.inboundConnectionTime = inboundConnectionTime;
	}

	public Double getOutboundConnectionTime() {
		return outboundConnectionTime;
	}

	public void setOutboundConnectionTime(Double outboundConnectionTime) {
		this.outboundConnectionTime = outboundConnectionTime;
	}

}
