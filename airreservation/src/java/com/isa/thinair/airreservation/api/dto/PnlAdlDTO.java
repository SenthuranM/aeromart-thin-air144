package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class PnlAdlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3771041824850690156L;

	private Integer hisID;

	private Integer flightId;

	private String flightNum;

	private Date flightDepartureDateZulu;

	private Date flightDepartureDateLocal;

	private String sitaAddress;

	private String messageType;

	private Timestamp transmissionTimeStamp;

	private String transmissionStatus;

	private String airportCode;

	private String messageText;

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getSitaAddress() {
		return sitaAddress;
	}

	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	public String getTransmissionStatus() {
		return transmissionStatus;
	}

	public void setTransmissionStatus(String transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public Timestamp getTransmissionTimeStamp() {
		return transmissionTimeStamp;
	}

	public void setTransmissionTimeStamp(Timestamp transmissionTimeStamp) {
		this.transmissionTimeStamp = transmissionTimeStamp;
	}

	/**
	 * @return Returns the flightNum.
	 */
	public String getFlightNum() {
		return flightNum;
	}

	/**
	 * @param flightNum
	 *            The flightNum to set.
	 */
	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	/**
	 * @return Returns the flightDepartureDateZulu.
	 */
	public Date getFlightDepartureDateZulu() {
		return flightDepartureDateZulu;
	}

	/**
	 * @param flightDepartureDateZulu
	 *            The flightDepartureDateZulu to set.
	 */
	public void setFlightDepartureDateZulu(Date flightDepartureDateZulu) {
		this.flightDepartureDateZulu = flightDepartureDateZulu;
	}

	/**
	 * @return Returns the flightId.
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return Returns the messageText.
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * @param messageText
	 *            The messageText to set.
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	/**
	 * @return Returns the hisID.
	 */
	public Integer getHisID() {
		return hisID;
	}

	/**
	 * @param hisID
	 *            The hisID to set.
	 */
	public void setHisID(Integer hisID) {
		this.hisID = hisID;
	}

	/**
	 * @return Returns the flightDepartureDateLocal.
	 */
	public Date getFlightDepartureDateLocal() {
		return flightDepartureDateLocal;
	}

	/**
	 * @param flightDepartureDateLocal
	 *            The flightDepartureDateLocal to set.
	 */
	public void setFlightDepartureDateLocal(Date flightDepartureDateLocal) {
		this.flightDepartureDateLocal = flightDepartureDateLocal;
	}
}
