package com.isa.thinair.airreservation.api.dto.revacc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.UniqueIDGenerator;

/**
 * @author Nilindra Fernando
 */
public class ReservationPaxPaymentMetaTO implements Serializable {

	private static final long serialVersionUID = -6794291358324461711L;

	private Integer pnrPaxId;

	private BigDecimal perPaxBaseTotalPayAmount;

	private Collection<ReservationPaxOndCharge> colPerPaxWiseOndNewCharges = new ArrayList<ReservationPaxOndCharge>();
	// Served as a cache value for colPerPaxWiseOndNewCharges transform
	private Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO;

	// map of pnrPaxOndChgId,ReservationPaxOndCharge
	private Map<Long, ReservationPaxOndCharge> mapPerPaxWiseOndRefundableCharges = new HashMap<Long, ReservationPaxOndCharge>();
	// Served as a cache value for payments created by mapPerPaxWiseOndRefundableCharges
	// <PnrPaxOndChgId,Collection<ReservationPaxOndPayment>>
	private Map<Long, Collection<ReservationPaxOndPayment>> mapPerPaxWiseOndChargePayments;

	private Collection<ReservationPaxOndCharge> colPerPaxWiseOndNewAdjustments = new ArrayList<ReservationPaxOndCharge>();

	public static class ReservationPaxChargeMetaTO implements Serializable {

		private static final long serialVersionUID = -141772925029409954L;

		private String uniqueId;

		private Integer pnrPaxOndChgId;

		private Integer fareId;

		private Integer chargeRateId;

		private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

		private Date zuluChargeDate;

		private String chargeGroupCode;

		private String remarks;

		private String chargeCode;

		public ReservationPaxChargeMetaTO() {
			this.uniqueId = UniqueIDGenerator.generate();
		}

		public int hashCode() {
			return new HashCodeBuilder().append(uniqueId).toHashCode();
		}

		/**
		 * @return the pnrPaxOndChgId
		 */
		public Integer getPnrPaxOndChgId() {
			return pnrPaxOndChgId;
		}

		/**
		 * @param pnrPaxOndChgId
		 *            the pnrPaxOndChgId to set
		 */
		public void setPnrPaxOndChgId(Integer pnrPaxOndChgId) {
			this.pnrPaxOndChgId = pnrPaxOndChgId;
		}

		/**
		 * @return the fareId
		 */
		public Integer getFareId() {
			return fareId;
		}

		/**
		 * @param fareId
		 *            the fareId to set
		 */
		public void setFareId(Integer fareId) {
			this.fareId = fareId;
		}

		/**
		 * @return the chargeRateId
		 */
		public Integer getChargeRateId() {
			return chargeRateId;
		}

		/**
		 * @param chargeRateId
		 *            the chargeRateId to set
		 */
		public void setChargeRateId(Integer chargeRateId) {
			this.chargeRateId = chargeRateId;
		}

		/**
		 * @return the amount
		 */
		public BigDecimal getAmount() {
			return amount;
		}

		/**
		 * @param amount
		 *            the amount to set
		 */
		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		/**
		 * @return the zuluChargeDate
		 */
		public Date getZuluChargeDate() {
			return zuluChargeDate;
		}

		/**
		 * @param zuluChargeDate
		 *            the zuluChargeDate to set
		 */
		public void setZuluChargeDate(Date zuluChargeDate) {
			this.zuluChargeDate = zuluChargeDate;
		}

		/**
		 * @return the chargeGroupCode
		 */
		public String getChargeGroupCode() {
			return chargeGroupCode;
		}

		/**
		 * @param chargeGroupCode
		 *            the chargeGroupCode to set
		 */
		public void setChargeGroupCode(String chargeGroupCode) {
			this.chargeGroupCode = chargeGroupCode;
		}

		public ReservationPaxChargeMetaTO clone() {
			ReservationPaxChargeMetaTO reservationPaxChargeMetaTO = new ReservationPaxChargeMetaTO();
			reservationPaxChargeMetaTO.setPnrPaxOndChgId(this.getPnrPaxOndChgId());
			reservationPaxChargeMetaTO.setFareId(this.getFareId());
			reservationPaxChargeMetaTO.setChargeRateId(this.getChargeRateId());
			reservationPaxChargeMetaTO.setAmount(this.getAmount());
			reservationPaxChargeMetaTO.setZuluChargeDate(this.getZuluChargeDate());
			reservationPaxChargeMetaTO.setChargeGroupCode(this.getChargeGroupCode());
			reservationPaxChargeMetaTO.setRemarks(this.getRemarks());
			reservationPaxChargeMetaTO.setChargeCode(this.getChargeCode());

			return reservationPaxChargeMetaTO;
		}

		/**
		 * @return the remarks
		 */
		public String getRemarks() {
			return remarks;
		}

		/**
		 * @param remarks
		 *            the remarks to set
		 */
		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		public String getChargeCode() {
			return chargeCode;
		}

		public void setChargeCode(String chargeCode) {
			this.chargeCode = chargeCode;
		}

	}

	/**
	 * @return the pnrPaxId
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            the pnrPaxId to set
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @return the perPaxBaseTotalPayAmount
	 */
	public BigDecimal getPerPaxBaseTotalPayAmount() {
		return perPaxBaseTotalPayAmount;
	}

	/**
	 * @param perPaxBaseTotalPayAmount
	 *            the perPaxBaseTotalPayAmount to set
	 */
	public void setPerPaxBaseTotalPayAmount(BigDecimal perPaxBaseTotalPayAmount) {
		this.perPaxBaseTotalPayAmount = perPaxBaseTotalPayAmount;
	}

	/**
	 * @return the mapPerPaxWiseRefundablePaxOndCharges
	 */
	public Map<Long, ReservationPaxOndCharge> getMapPerPaxWiseOndRefundableCharges() {
		return mapPerPaxWiseOndRefundableCharges;
	}

	/**
	 * add refundable chargers
	 * 
	 * @param mapPerPaxWiseOndRefundableCharges
	 */
	public void addMapPerPaxWiseOndRefundableCharges(Map<Long, ReservationPaxOndCharge> mapPerPaxWiseOndRefundableCharges) {
		this.mapPerPaxWiseOndRefundableCharges.putAll(mapPerPaxWiseOndRefundableCharges);
	}

	public void addMapPerPaxWiseOndRefundableCharge(Long pnrPaxOndChgId, ReservationPaxOndCharge reservationPaxOndCharge) {
		this.mapPerPaxWiseOndRefundableCharges.put(pnrPaxOndChgId, reservationPaxOndCharge);
	}

	/**
	 * Return the cache view of the ReservationPaxOndPayment for a given pax and given refundable chargers. Note that
	 * for every refundable charge there has to be a entry linking the payment breakdown and charge. Key :
	 * PnrPaxOndChgId VAlue : Collection<ReservationPaxOndPayment>
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Map<Long, Collection<ReservationPaxOndPayment>> getPaxWiseOndChargePayments() throws ModuleException {
		if (this.mapPerPaxWiseOndChargePayments == null) {
			Collection<Long> ondChargeIds = this.getMapPerPaxWiseOndRefundableCharges().keySet();

			if (ondChargeIds != null && ondChargeIds.size() > 0) {
				this.mapPerPaxWiseOndChargePayments = ReservationModuleUtils.getPassengerBD()
						.getReservationPaxOndPaymentByOndChargeId(this.getPnrPaxId(), ondChargeIds);
			} else {
				this.mapPerPaxWiseOndChargePayments = new HashMap<Long, Collection<ReservationPaxOndPayment>>();
			}
		}

		return this.mapPerPaxWiseOndChargePayments;
	}

	/**
	 * @return the colPerPaxWiseOndNewCharges
	 */
	public Collection<ReservationPaxOndCharge> getColPerPaxWiseOndNewCharges() {
		return colPerPaxWiseOndNewCharges;
	}

	/**
	 * add PerPaxWiseOndNewCharges
	 * 
	 * @param reservationPaxOndCharges
	 */
	public void addColPerPaxWiseOndNewCharges(Collection<ReservationPaxOndCharge> reservationPaxOndCharges) {
		this.colPerPaxWiseOndNewCharges.addAll(reservationPaxOndCharges);
	}

	public void addColPerPaxWiseOndNewCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		this.colPerPaxWiseOndNewCharges.add(reservationPaxOndCharge);
	}

	public void removeColPerPaxWiseOndNewCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		this.colPerPaxWiseOndNewCharges.remove(reservationPaxOndCharge);
	}

	public void removeColPerPaxWiseOndNewChargeAll(Collection<ReservationPaxOndCharge> colReservationPaxOndCharge) {
		this.colPerPaxWiseOndNewCharges.removeAll(colReservationPaxOndCharge);
	}

	public Collection<ReservationPaxChargeMetaTO> getColReservationPaxChargeMetaTOForNewCharges() {

		if (this.colReservationPaxChargeMetaTO == null) {
			this.colReservationPaxChargeMetaTO = new ArrayList<ReservationPaxChargeMetaTO>();

			if (getColPerPaxWiseOndNewCharges() != null && getColPerPaxWiseOndNewCharges().size() > 0) {
				for (ReservationPaxOndCharge reservationPaxOndCharge : getColPerPaxWiseOndNewCharges()) {
					this.colReservationPaxChargeMetaTO.add(reservationPaxOndCharge.transformTOReservationPaxChargeMetaTO());
				}
			}
		}

		return colReservationPaxChargeMetaTO;
	}

	/**
	 * @return the colPerPaxWiseOndNewAdjustments
	 */
	public Collection<ReservationPaxOndCharge> getColPerPaxWiseOndNewAdjustments() {
		return colPerPaxWiseOndNewAdjustments;
	}

	/**
	 * add New adjustments
	 * 
	 * @param reservationPaxOndCharges
	 */
	public void addColPerPaxWiseOndNewAdjustments(Collection<ReservationPaxOndCharge> reservationPaxOndCharges) {
		this.colPerPaxWiseOndNewAdjustments.addAll(reservationPaxOndCharges);
	}

	public void addColPerPaxWiseOndNewAdjustment(ReservationPaxOndCharge reservationPaxOndCharge) {
		this.colPerPaxWiseOndNewAdjustments.add(reservationPaxOndCharge);
	}

	/**
	 * @return the colPerPaxWiseOndNewAdjustments
	 */
	public Collection<ReservationPaxChargeMetaTO> getColPerPaxWiseOndNewAdjustments(boolean isMinuxAdjustments) {
		Collection<ReservationPaxChargeMetaTO> colReservationPaxChargeMetaTO = new ArrayList<ReservationPaxChargeMetaTO>();

		for (ReservationPaxOndCharge reservationPaxOndCharge : this.getColPerPaxWiseOndNewAdjustments()) {
			if (isMinuxAdjustments) {
				if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() < 0) {
					colReservationPaxChargeMetaTO.add(reservationPaxOndCharge.transformTOReservationPaxChargeMetaTO());
				}
			} else {
				if (reservationPaxOndCharge.getEffectiveAmount().doubleValue() > 0) {
					colReservationPaxChargeMetaTO.add(reservationPaxOndCharge.transformTOReservationPaxChargeMetaTO());
				}
			}
		}

		return colReservationPaxChargeMetaTO;
	}

}
