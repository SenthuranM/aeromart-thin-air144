package com.isa.thinair.airreservation.api.dto.GroupBooking;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupBookingRequestTO implements Serializable {

	private static final long serialVersionUID = 23514212345887L;
	private final String DATE_FORMAT = "dd/MM/yyyy";
	private Long requestID;
	private Integer paymentStatus;
	private Date requestDate;
	private Integer adultAmount;
	private Integer childAmount;
	private Integer infantAmount;
	private String paxCount;
	private String agent = "AGENT NAME";

	private Set<GroupBookingReqRoutesTO> groupBookingRoutes;

	private GroupBookingReqRoutesTO firstRouteForDisplay;

	private String oalFare;
	private BigDecimal requestedFare;
	private BigDecimal agreedFare;
	private boolean isAgreedFarePercentage;
	private BigDecimal minPaymentRequired;
	private BigDecimal paymentAmount;
	private Date requestedDate;
	private String craeteUserCode;
	private String approvalUserCode;
	private Date approvalDate;
	private String agentComments;
	private String approvalComments;
	private Integer statusID;
	private Collection<Integer> statusIDs;

	private Date targetPaymentDate;
	private Date fareValidDate;
	private String targetPaymentDateStr;
	private String fareValidDateStr;
	private Long version;

	private String statusName;
	private String pnr;
	private String stationCode;

	private String userId;
	private String agentCode;
	private String requestedAgentCode;
	private String agentName;

	private String onds;

	private Set<GroupBookingRequestTO> subRequests;
	private int subRequestsNum;

	private Long mainRequestID;

	public Long getRequestID() {
		return requestID;
	}

	public void setRequestID(Long requestID) {
		this.requestID = requestID;
	}

	public Integer getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Integer paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Integer getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(Integer childAmount) {
		this.childAmount = childAmount;
	}

	public Integer getAdultAmount() {
		return adultAmount;
	}

	public void setAdultAmount(Integer adultAmount) {
		this.adultAmount = adultAmount;
	}

	public Integer getInfantAmount() {
		return infantAmount;
	}

	public void setInfantAmount(Integer infantAmount) {
		this.infantAmount = infantAmount;
	}

	public String getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(Integer adultAmount, Integer childAmount, Integer infantAmount) {
		this.setAdultAmount(adultAmount);
		this.setChildAmount(childAmount);
		this.setInfantAmount(infantAmount);
		this.paxCount = adultAmount + "/" + childAmount + "/" + infantAmount;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public GroupBookingReqRoutesTO getFirstRouteForDisplay() {
		return groupBookingRoutes == null || groupBookingRoutes.isEmpty() ? null : groupBookingRoutes.iterator().next();
	}

	public void setFirstRouteForDisplay(GroupBookingReqRoutesTO firstRouteForDisplay) {
		this.firstRouteForDisplay = firstRouteForDisplay;
	}

	public Set<GroupBookingReqRoutesTO> getGroupBookingRoutes() {
		return groupBookingRoutes;
	}

	public void setGroupBookingRoutes(Set<GroupBookingReqRoutesTO> groupBookingRoutes) {
		this.groupBookingRoutes = groupBookingRoutes;
	}

	public String getOalFare() {
		return oalFare;
	}

	public void setOalFare(String oalFare) {
		this.oalFare = oalFare;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public String getCraeteUserCode() {
		return craeteUserCode;
	}

	public void setCraeteUserCode(String craeteUserCode) {
		this.craeteUserCode = craeteUserCode;
	}

	public String getApprovalUserCode() {
		return approvalUserCode;
	}

	public void setApprovalUserCode(String approvalUserCode) {
		this.approvalUserCode = approvalUserCode;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getAgentComments() {
		return agentComments;
	}

	public void setAgentComments(String agentComments) {
		this.agentComments = agentComments;
	}

	public String getApprovalComments() {
		return approvalComments;
	}

	public void setApprovalComments(String approvalComments) {
		this.approvalComments = approvalComments;
	}

	public Integer getStatusID() {
		return statusID;
	}

	public void setStatusID(Integer statusID) {
		this.statusID = statusID;
	}

	public Date getTargetPaymentDate() {
		return targetPaymentDate;
	}

	public void setTargetPaymentDate(Date targetPaymentDate) {
		this.targetPaymentDate = targetPaymentDate;
	}

	public Date getFareValidDate() {
		return fareValidDate;
	}

	public void setFareValidDate(Date fareValidDate) {
		this.fareValidDate = fareValidDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(Integer statusID) {
		if (statusID == 0)
			this.statusName = "Pending";
		else if (statusID == 1)
			this.statusName = "Approved";
		else if (statusID == 2)
			this.statusName = "Rejected";
		else if (statusID == 3)
			this.statusName = "Re-Quote";
		else if (statusID == 4)
			this.statusName = "Pending after Re-Quote";
		else if (statusID == 5)
			this.statusName = "Request Release Seat";
		else if (statusID == 6)
			this.statusName = "Withdraw";
		else if (statusID == 7)
			this.statusName = "Booked";

	}

	public BigDecimal getRequestedFare() {
		return requestedFare;
	}

	public void setRequestedFare(BigDecimal requestedFare) {
		this.requestedFare = requestedFare;
	}

	public BigDecimal getAgreedFare() {
		return agreedFare;
	}

	public void setAgreedFare(BigDecimal agreedFare) {
		this.agreedFare = agreedFare;
	}

	public BigDecimal getMinPaymentRequired() {
		return minPaymentRequired;
	}

	public void setMinPaymentRequired(BigDecimal minPaymentRequired) {
		this.minPaymentRequired = minPaymentRequired;
	}

	public String getTargetPaymentDateStr() {
		return targetPaymentDateStr;
	}

	public void setTargetPaymentDateStr(String targetPaymentDateStr) throws ParseException {
		this.targetPaymentDateStr = targetPaymentDateStr;
		this.targetPaymentDate = new SimpleDateFormat(DATE_FORMAT).parse(targetPaymentDateStr);
	}

	public String getFareValidDateStr() {
		return fareValidDateStr;
	}

	public void setFareValidDateStr(String fareValidDateStr) throws ParseException {
		this.fareValidDateStr = fareValidDateStr;
		this.fareValidDate = new SimpleDateFormat(DATE_FORMAT).parse(fareValidDateStr);
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getOnds() {
		return onds;
	}

	public void setOnds(String onds) {
		this.onds = onds;
	}

	/**
	 * @return the subRequests
	 */
	public Set<GroupBookingRequestTO> getSubRequests() {
		if (subRequests == null)
			subRequests = new HashSet<GroupBookingRequestTO>();
		return subRequests;
	}

	/**
	 * @param subRequests
	 *            the subRequests to set
	 */
	public void setSubRequests(Set<GroupBookingRequestTO> subRequests) {
		this.subRequests = subRequests;
	}

	public int getSubRequestsNum() {
		return subRequests.size();
	}

	public void setSubRequestsNum(int subRequestsNum) {
		this.subRequestsNum = subRequestsNum;
	}

	public String getRequestedAgentCode() {
		return requestedAgentCode;
	}

	public void setRequestedAgentCode(String requestedAgentCode) {
		this.requestedAgentCode = requestedAgentCode;
	}

	/**
	 * @return the statusIDs
	 */
	public Collection<Integer> getStatusIDs() {
		return statusIDs;
	}

	/**
	 * @param statusIDs
	 *            the statusIDs to set
	 */
	public void setStatusIDs(String statusIDs) {
		List<Integer> list = new ArrayList<Integer>();
		for (String intVal : statusIDs.split(",")) {
			list.add(Integer.parseInt(intVal));
		}
		this.statusIDs = list;
	}

	/**
	 * @return the mainRequestID
	 */
	public Long getMainRequestID() {
		return mainRequestID;
	}

	/**
	 * @param mainRequestID
	 *            the mainRequestID to set
	 */
	public void setMainRequestID(Long mainRequestID) {
		this.mainRequestID = mainRequestID;
	}

	/**
	 * @return the isAgreedFarePercentage
	 */
	public boolean isAgreedFarePercentage() {
		return isAgreedFarePercentage;
	}

	/**
	 * @param isAgreedFarePercentage
	 *            the isAgreedFarePercentage to set
	 */
	public void setAgreedFarePercentage(boolean isAgreedFarePercentage) {
		this.isAgreedFarePercentage = isAgreedFarePercentage;
	}
}
