package com.isa.thinair.airreservation.api.dto.ancilaryreminder;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class FlightSegReminderNotificationDTO implements Serializable {

	private static final long serialVersionUID = 2080715521400943179L;

	private String flightNum;

	private Integer flightId;

	private String flightNumber;

	private Integer flightSegmentId;

	private String flightStatus;

	private Integer segmentId;
	private String segmentCode;
	private String segmentDetails;
	private String fromAirport;
	private String toAirport;
	private Date checkinTime;
	private Date checkInDate;
	private Date departureDateTime;
	private Date arrivalDateTime;
	private Date departureDateTimeZulu;
	private Date arrivalDateTimeZulu;
	private Integer operationTypeID;
	private String seatSelectionStatus;
	private String mealSelectionStatus;
	private String baggageSelectionStatus;
	private String seatNotificationRequired;
	private String mealNotificationRequired;
	private String baggageNotificationRequired;
	private Integer pnrSegNotifyId;
	private String returnFlag;
	private Date depatureDate = null;

	public String getDepartureDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(departureDateTime, dateTimeFormat);
	}

	public String getArrivalDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(arrivalDateTime, dateTimeFormat);
	}

	public String getCheckInDateTime(String dateTimeFormat) {
		return BeanUtils.parseDateFormat(departureDateTime, dateTimeFormat);
	}

	/**
	 * @return the flightNum
	 */
	public String getFlightNum() {
		return flightNum;
	}

	/**
	 * @param flightNum
	 *            the flightNum to set
	 */
	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public Integer getOperationTypeID() {
		return operationTypeID;
	}

	public void setOperationTypeID(Integer operationTypeID) {
		this.operationTypeID = operationTypeID;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * 
	 * @return Returns the carrier code
	 */
	public String getCarrierCode() {
		return this.flightNumber.substring(0, 2);
	}

	/**
	 * @return Returns the segmentId.
	 */
	public Integer getSegmentId() {
		return segmentId;
	}

	/**
	 * @param segmentId
	 *            The segmentId to set.
	 */
	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	/**
	 * @return the seatSelectionStatus
	 */
	public String getSeatSelectionStatus() {
		return seatSelectionStatus;
	}

	/**
	 * @param seatSelectionStatus
	 *            the seatSelectionStatus to set
	 */
	public void setSeatSelectionStatus(String seatSelectionStatus) {
		this.seatSelectionStatus = seatSelectionStatus;
	}

	/**
	 * @return the mealSelectionStatus
	 */
	public String getMealSelectionStatus() {
		return mealSelectionStatus;
	}

	/**
	 * @param mealSelectionStatus
	 *            the mealSelectionStatus to set
	 */
	public void setMealSelectionStatus(String mealSelectionStatus) {
		this.mealSelectionStatus = mealSelectionStatus;
	}

	/**
	 * @return the seatNotificationRequired
	 */
	public String getSeatNotificationRequired() {
		return seatNotificationRequired;
	}

	/**
	 * @param seatNotificationRequired
	 *            the seatNotificationRequired to set
	 */
	public void setSeatNotificationRequired(String seatNotificationRequired) {
		this.seatNotificationRequired = seatNotificationRequired;
	}

	/**
	 * @return the mealNotificationRequired
	 */
	public String getMealNotificationRequired() {
		return mealNotificationRequired;
	}

	/**
	 * @param mealNotificationRequired
	 *            the mealNotificationRequired to set
	 */
	public void setMealNotificationRequired(String mealNotificationRequired) {
		this.mealNotificationRequired = mealNotificationRequired;
	}

	/**
	 * @return the pnrSegNotifyId
	 */
	public Integer getPnrSegNotifyId() {
		return pnrSegNotifyId;
	}

	/**
	 * @return the flightSegmentId
	 */
	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            the flightSegmentId to set
	 */
	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @param pnrSegNotifyId
	 *            the pnrSegNotifyId to set
	 */
	public void setPnrSegNotifyId(Integer pnrSegNotifyId) {
		this.pnrSegNotifyId = pnrSegNotifyId;
	}

	/**
	 * @return the returnFlag
	 */
	public String getReturnFlag() {
		return returnFlag;
	}

	/**
	 * @param returnFlag
	 *            the returnFlag to set
	 */
	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public Date getDepatureDate() {
		if (depatureDate == null) {
			Calendar calSearchedDate = Calendar.getInstance();
			calSearchedDate.setTime(getDepartureDateTime());
			calSearchedDate.set(Calendar.HOUR, 0);
			calSearchedDate.set(Calendar.MINUTE, 0);
			calSearchedDate.set(Calendar.SECOND, 0);
			calSearchedDate.set(Calendar.MILLISECOND, 0);
			depatureDate = calSearchedDate.getTime();
		}
		return depatureDate;
	}

	/**
	 * @return the checkinTime
	 */
	public Date getCheckinTime() {
		return checkinTime;
	}

	/**
	 * @param checkinTime
	 *            the checkinTime to set
	 */
	public void setCheckinTime(Date checkinTime) {
		this.checkinTime = checkinTime;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	/**
	 * Return the check in date
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Date getCheckInDate() throws ModuleException {
		return checkInDate;
	}

	/**
	 * Return the check in date in the given format
	 * 
	 * @param dateFormat
	 * @return
	 * @throws ModuleException
	 */
	public String getCheckInStringDate(String dateFormat) throws ModuleException {
		return BeanUtils.parseDateFormat(this.getCheckInDate(), dateFormat);
	}

	/**
	 * @return the segmentDetails
	 */
	public String getSegmentDetails() {
		return segmentDetails;
	}

	/**
	 * @param segmentDetails
	 *            the segmentDetails to set
	 */
	public void setSegmentDetails(String segmentDetails) {
		this.segmentDetails = segmentDetails;
	}

	public String getBaggageSelectionStatus() {
		return baggageSelectionStatus;
	}

	public void setBaggageSelectionStatus(String baggageSelectionStatus) {
		this.baggageSelectionStatus = baggageSelectionStatus;
	}

	public String getBaggageNotificationRequired() {
		return baggageNotificationRequired;
	}

	public void setBaggageNotificationRequired(String baggageNotificationRequired) {
		this.baggageNotificationRequired = baggageNotificationRequired;
	}

}
