/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ashain
 *
 */
public class PFSCatagoryByDestination {
	private String destinationAirPort;
	
	private List<CabinCountTo>  cabinClassWiseList=new ArrayList<CabinCountTo>();

	/**
	 * @return the destinationAirPort
	 */
	public String getDestinationAirPort() {
		return destinationAirPort;
	}

	/**
	 * @param destinationAirPort the destinationAirPort to set
	 */
	public void setDestinationAirPort(String destinationAirPort) {
		this.destinationAirPort = destinationAirPort;
	}

	/**
	 * @return the cabinClassWiseList
	 */
	public List<CabinCountTo> getCabinClassWiseList() {
		return cabinClassWiseList;
	}

	/**
	 * @param cabinClassWiseList the cabinClassWiseList to set
	 */
	public void setCabinClassWiseList(List<CabinCountTo> cabinClassWiseList) {
		this.cabinClassWiseList = cabinClassWiseList;
	}

	
	
	
	
}
