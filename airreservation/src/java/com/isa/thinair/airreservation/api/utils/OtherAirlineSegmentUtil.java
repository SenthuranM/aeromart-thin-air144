package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.OtherAirlineSegmentTO;
import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class OtherAirlineSegmentUtil {

	public static Map<String, Collection<OtherAirlineSegment>> getConfirmedReservationOtherAirlineSegmentInAMap(
			Collection<OtherAirlineSegment> externalSegments) {
		Map<String, Collection<OtherAirlineSegment>> segmentMap = new HashMap<String, Collection<OtherAirlineSegment>>();
		String uniqueSegmentKeyWithOutStatus;

		for (OtherAirlineSegment reservationExternalSegment : externalSegments) {

			if (ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED.equals(reservationExternalSegment
					.getStatus())) {
				uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
				Collection<OtherAirlineSegment> colReservationExternalSegment = segmentMap.get(uniqueSegmentKeyWithOutStatus);

				if (colReservationExternalSegment == null) {
					colReservationExternalSegment = new ArrayList<OtherAirlineSegment>();
					colReservationExternalSegment.add(reservationExternalSegment);
					segmentMap.put(uniqueSegmentKeyWithOutStatus, colReservationExternalSegment);
				} else {
					colReservationExternalSegment.add(reservationExternalSegment);
				}
			}
		}

		return segmentMap;
	}

	public static Map<String, Collection<OtherAirlineSegment>> getAllReservationOtherAirlineSegmentInAMap(
			Collection<OtherAirlineSegment> externalSegments) {
		Map<String, Collection<OtherAirlineSegment>> segmentMap = new HashMap<String, Collection<OtherAirlineSegment>>();
		String uniqueSegmentKeyWithOutStatus;

		for (OtherAirlineSegment reservationExternalSegment : externalSegments) {

			uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
			Collection<OtherAirlineSegment> colReservationExternalSegment = segmentMap.get(uniqueSegmentKeyWithOutStatus);

			if (colReservationExternalSegment == null) {
				colReservationExternalSegment = new ArrayList<OtherAirlineSegment>();
				colReservationExternalSegment.add(reservationExternalSegment);
				segmentMap.put(uniqueSegmentKeyWithOutStatus, colReservationExternalSegment);
			} else {
				colReservationExternalSegment.add(reservationExternalSegment);
			}
		}
		return segmentMap;
	}

	public static String addOtherAirlineSegments(Collection<OtherAirlineSegmentTO> colExternalSegmentTO, Reservation reservation,
			Map<String, Collection<OtherAirlineSegment>> allSegmentMap) throws ModuleException {

		Collection<OtherAirlineSegment> colReservationExternalSegment = new ArrayList<OtherAirlineSegment>();

		for (OtherAirlineSegmentTO externalSegmentTO : colExternalSegmentTO) {

			if (ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
				String uniqueSegmentKeyWithOutStatus = ReservationApiUtils.getUniqueSegmentKey(
						externalSegmentTO.getFlightNumber(), externalSegmentTO.getSegmentCode(),
						externalSegmentTO.getDepartureDate(), externalSegmentTO.getBookingCode(), null);
				if (allSegmentMap.containsKey(uniqueSegmentKeyWithOutStatus)) {
					continue;
				}
			}
			OtherAirlineSegment externalPnrSegment = externalSegmentTO.toOtherAirlineSegment();
			colReservationExternalSegment.add(externalPnrSegment);
		}

		StringBuilder info = new StringBuilder();
		if (colReservationExternalSegment.size() > 0) {
			for (OtherAirlineSegment reservationExternalSegment : colReservationExternalSegment) {
				// Save the external pnr segment only if it is not present in the reservation.
				if (!isExternalPnrSegmentInReservation(reservation, reservationExternalSegment)
						|| sameDateFlightModification(reservationExternalSegment, colExternalSegmentTO)) {

					if (reservationExternalSegment.getMarketingFlightNumber() != null) {
						List<FlightSegement> flightSegement = (List<FlightSegement>) ReservationModuleUtils.getFlightBD()
								.getFlightSegmentsForLocalDate(reservationExternalSegment.getOrigin(),
										reservationExternalSegment.getDestination(),
										reservationExternalSegment.getMarketingFlightNumber(),
										reservationExternalSegment.getDepartureDateTimeLocal(), true);

						if (flightSegement != null && flightSegement.size() > 0) {
							reservationExternalSegment.setFlightSegId(flightSegement.get(0).getFltSegId());
						}
					}
					reservation.addOtherAirlineSegment(reservationExternalSegment);

					if (info.length() != 0) {
						info.append(" , ");
					}						
					info.append(" Flight No : " + reservationExternalSegment.getFlightNumber() );
				//	info.append(" | MC Flight No : " + reservationExternalSegment.getMarketingFlightNumber() );
					info.append(" | Segment Code : " + reservationExternalSegment.getOrigin() + "/" + reservationExternalSegment.getDestination());
				//	info.append(" | MC Booking Code : " + reservationExternalSegment.getMarketingBookingCode() );
					info.append(" | Departure Date : " + reservationExternalSegment.getDepartureDateTimeLocal());
					info.append(" | Pax Name : " + reservationExternalSegment.getReservation().getContactInfo().getFirstName() +" "+reservationExternalSegment.getReservation().getContactInfo().getLastName());
				}
			}
		}

		return info.toString();
	}

	private static boolean sameDateFlightModification(OtherAirlineSegment reservationExternalSegment,
			Collection<OtherAirlineSegmentTO> colExternalSegmentTOs) {

		String uniqueSegmentKeyWithOutStatus = reservationExternalSegment.getUniqueSegmentKey();
		for (OtherAirlineSegmentTO externalSegmentTO : colExternalSegmentTOs) {
			String uniqueSegmentKey = ReservationApiUtils.getUniqueSegmentKey(externalSegmentTO.getFlightNumber(),
					externalSegmentTO.getSegmentCode(), externalSegmentTO.getDepartureDate(), externalSegmentTO.getBookingCode(),
					null);
			if (uniqueSegmentKeyWithOutStatus.equals(uniqueSegmentKey)
					&& ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
				return true;
			}
		}
		return false;
	}

	private static boolean isExternalPnrSegmentInReservation(Reservation reservation, OtherAirlineSegment pnrSegment) {
		boolean isExternalPnrSegmentInReservation = false;

		if (reservation != null && pnrSegment != null) {
			for (OtherAirlineSegment exisitingExternalPnrSegment : (Collection<OtherAirlineSegment>) reservation
					.getOtherAirlineSegments()) {

				if (pnrSegment.getUniqueSegmentKey().equals(exisitingExternalPnrSegment.getUniqueSegmentKey())
						&& pnrSegment.getStatus().equals(exisitingExternalPnrSegment.getStatus())) {
					isExternalPnrSegmentInReservation = true;
					break;
				} else if (pnrSegment.getDepartureDate().getYear() == exisitingExternalPnrSegment.getDepartureDate().getYear() + 1) {
					String key = ReservationApiUtils
							.getUniqueSegmentKey(pnrSegment.getFlightNumber(),
									pnrSegment.getOrigin() + "/" + pnrSegment.getDestination(),
									CalendarUtil.add(pnrSegment.getDepartureDate(), -1, 0, 0, 0, 0, 0),
									pnrSegment.getBookingCode(), null);
					if (key.equals(exisitingExternalPnrSegment.getUniqueSegmentKey())
							&& pnrSegment.getStatus().equals(exisitingExternalPnrSegment.getStatus())) {
						isExternalPnrSegmentInReservation = true;
						break;
					}
				}
			}
		}
		return isExternalPnrSegmentInReservation;
	}

	public static String modifyExternalSegments(Collection<OtherAirlineSegmentTO> colExternalSegmentTO,
			Map<String, Collection<OtherAirlineSegment>> cnfSegmentMap) throws ModuleException {

		StringBuilder modifySegInfo = new StringBuilder();

		for (OtherAirlineSegmentTO externalSegmentTO : colExternalSegmentTO) {

			if (ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CANCEL.equals(externalSegmentTO.getStatus())) {
				String uniqueSegmentKeyWithOutStatus = ReservationApiUtils.getUniqueSegmentKey(
						externalSegmentTO.getFlightNumber(), externalSegmentTO.getSegmentCode(),
						externalSegmentTO.getDepartureDate(), externalSegmentTO.getBookingCode(), null);

				if (cnfSegmentMap.containsKey(uniqueSegmentKeyWithOutStatus)) {
					Collection<OtherAirlineSegment> colReservationExternalSegment = cnfSegmentMap
							.get(uniqueSegmentKeyWithOutStatus);
					OtherAirlineSegment reservationExternalSegment = getConfirmedReservationExternalSegment(colReservationExternalSegment);

					if (modifySegInfo.length() == 0) {
						modifySegInfo.append(reservationExternalSegment.toString() + " --> " + externalSegmentTO.toString());
					} else {
						modifySegInfo.append(" , " + reservationExternalSegment.toString() + " --> "
								+ externalSegmentTO.toString());
					}

					reservationExternalSegment
							.setStatus(ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CANCEL);
				}
			}
		}

		return modifySegInfo.toString();
	}

	private static OtherAirlineSegment getConfirmedReservationExternalSegment(
			Collection<OtherAirlineSegment> colReservationExternalSegment) throws ModuleException {

		if (colReservationExternalSegment != null && colReservationExternalSegment.size() > 0) {
			for (OtherAirlineSegment reservationExternalSegment : colReservationExternalSegment) {
				if (ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED.equals(reservationExternalSegment
						.getStatus())) {
					return reservationExternalSegment;
				}
			}
		}

		throw new ModuleException("airreservations.arg.corruptedExternalSegmentsForAmmending");
	}
}
