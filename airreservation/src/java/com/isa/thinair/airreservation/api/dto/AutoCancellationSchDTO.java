package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.baggage.PassengerBaggageAssembler;
import com.isa.thinair.airreservation.api.dto.meal.PassengerMealAssembler;
import com.isa.thinair.airreservation.api.dto.seatmap.PassengerSeatingAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;

/**
 * DTO for keeping segment/infant/ancillary to be expired by scheduler job
 * 
 * @author rumesh
 * 
 */
public class AutoCancellationSchDTO implements Serializable {
	private static final long serialVersionUID = 3384301621271027762L;

	private String pnr;
	private long version;
	private Map<Integer, List<Integer>> ondGroudWiseCancellingSegIds;
	private List<Integer> cancellingPaxIds;
	private PassengerSeatingAssembler cancellingSeats;
	private PassengerMealAssembler cancellingMeals;
	private PassengerBaggageAssembler cancellingBaggages;
	private Map<Integer, SegmentSSRAssembler> cancellingInflightServices;
	private Map<Integer, SegmentSSRAssembler> cancellingAirportService;
	private Integer autoCancellationId;
	

	private boolean insuranceExists;

	public AutoCancellationSchDTO() {

	}

	public AutoCancellationSchDTO(String pnr, long version, Integer autoCancellationId) {
		super();
		this.pnr = pnr;
		this.version = version;
		this.autoCancellationId = autoCancellationId; 
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public List<Integer> getCancellingPaxIds() {
		return cancellingPaxIds;
	}

	public void setCancellingPaxIds(List<Integer> cancellingPaxIds) {
		this.cancellingPaxIds = cancellingPaxIds;
	}

	public PassengerSeatingAssembler getCancellingSeats() {
		return cancellingSeats;
	}

	public void setCancellingSeats(PassengerSeatingAssembler cancellingSeats) {
		this.cancellingSeats = cancellingSeats;
	}

	public PassengerMealAssembler getCancellingMeals() {
		return cancellingMeals;
	}

	public void setCancellingMeals(PassengerMealAssembler cancellingMeals) {
		this.cancellingMeals = cancellingMeals;
	}

	public PassengerBaggageAssembler getCancellingBaggages() {
		return cancellingBaggages;
	}

	public void setCancellingBaggages(PassengerBaggageAssembler cancellingBaggages) {
		this.cancellingBaggages = cancellingBaggages;
	}

	public Map<Integer, SegmentSSRAssembler> getCancellingInflightServices() {
		return cancellingInflightServices;
	}

	public void setCancellingInflightServices(Map<Integer, SegmentSSRAssembler> cancellingInflightServices) {
		this.cancellingInflightServices = cancellingInflightServices;
	}

	public Map<Integer, SegmentSSRAssembler> getCancellingAirportService() {
		return cancellingAirportService;
	}

	public void setCancellingAirportService(Map<Integer, SegmentSSRAssembler> cancellingAirportService) {
		this.cancellingAirportService = cancellingAirportService;
	}

	public boolean isInsuranceExists() {
		return insuranceExists;
	}

	public void setInsuranceExists(boolean insuranceExists) {
		this.insuranceExists = insuranceExists;
	}

	public Map<Integer, List<Integer>> getOndGroudWiseCancellingSegIds() {
		return ondGroudWiseCancellingSegIds;
	}

	public void setOndGroudWiseCancellingSegIds(Map<Integer, List<Integer>> ondGroudWiseCancellingSegIds) {
		this.ondGroudWiseCancellingSegIds = ondGroudWiseCancellingSegIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pnr == null) ? 0 : pnr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		if (!(obj instanceof AutoCancellationSchDTO)) {
			return false;
		}
		AutoCancellationSchDTO other = (AutoCancellationSchDTO) obj;
		if (pnr == null) {
			if (other.pnr != null) {
				return false;
			}
		} else if (!pnr.equals(other.pnr)) {
			return false;
		}
		return true;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}
}
