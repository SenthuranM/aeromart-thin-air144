package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author M.Rikaz
 * @since November 12, 2013
 * @hibernate.class table = "T_PNR_PAX_PAYMENT_CREDITS"
 */
public class ReservationPaxPaymentCredit extends Persistent {

	private static final long serialVersionUID = -3390515359192080307L;

	private Long paxOndPaymentId;

	private Long paxPaymenCreditId;

	private BigDecimal amount;

	private String remarks;

	private String chargeGroupCode;

	private Long originalPaxOndPaymentId;

	private ReservationCredit reservationCredit;

	private String chargeCode;

	/**
	 * @return the paxPaymenCreditId
	 * @hibernate.id column = "PPPC_ID " generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_PAYMENT_CREDITS"
	 */
	public Long getPaxPaymenCreditId() {
		return paxPaymenCreditId;
	}

	public void setPaxPaymenCreditId(Long paxPaymenCreditId) {
		this.paxPaymenCreditId = paxPaymenCreditId;
	}

	/**
	 * @return the paxOndPaymentId
	 * @hibernate.property column = "PPOP_ID"
	 */
	public Long getPaxOndPaymentId() {
		return paxOndPaymentId;
	}

	/**
	 * @param paxOndPaymentId
	 *            the paxOndPaymentId to set
	 */
	public void setPaxOndPaymentId(Long paxOndPaymentId) {
		this.paxOndPaymentId = paxOndPaymentId;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the remarks
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the chargeGroupCode
	 * @hibernate.property column = "CHARGE_GROUP_CODE"
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            the chargeGroupCode to set
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return the originalPaxOndPaymentId
	 * @hibernate.property column = "ORIGINAL_PPOP_ID"
	 */
	public Long getOriginalPaxOndPaymentId() {
		return originalPaxOndPaymentId;
	}

	/**
	 * @param originalPaxOndPaymentId
	 */
	public void setOriginalPaxOndPaymentId(Long originalPaxOndPaymentId) {
		this.originalPaxOndPaymentId = originalPaxOndPaymentId;
	}

	/**
	 * @hibernate.many-to-one column="PAY_ID" class="com.isa.thinair.airreservation.api.model.ReservationCredit"
	 * @return Returns the reservationCredit.
	 */
	public ReservationCredit getReservationCredit() {
		return reservationCredit;
	}

	public void setReservationCredit(ReservationCredit reservationCredit) {
		this.reservationCredit = reservationCredit;
	}

	/**
	 * @return the originalPaxOndPaymentId
	 * @hibernate.property column = "CHARGE_CODE"
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public ReservationPaxPaymentCredit clone() {
		ReservationPaxPaymentCredit paxOndPaymentCredit = new ReservationPaxPaymentCredit();
		paxOndPaymentCredit.setPaxPaymenCreditId(this.getPaxPaymenCreditId());
		paxOndPaymentCredit.setAmount(this.getAmount());
		paxOndPaymentCredit.setRemarks(this.getRemarks());
		paxOndPaymentCredit.setChargeGroupCode(this.getChargeGroupCode());
		paxOndPaymentCredit.setPaxOndPaymentId(this.getPaxOndPaymentId());
		paxOndPaymentCredit.setChargeCode(this.getChargeCode());
		return paxOndPaymentCredit;
	}

	@Override
	public String toString() {
		return "ReservationPaxPaymentCredit [paxOndPaymentId=" + paxOndPaymentId + ", paxPaymenCreditId=" + paxPaymenCreditId
				+ ", amount=" + amount + ", remarks=" + remarks + ", chargeGroupCode=" + chargeGroupCode
				+ ", originalPaxOndPaymentId=" + originalPaxOndPaymentId + ", chargeCode=" + chargeCode + "]";
	}

}
