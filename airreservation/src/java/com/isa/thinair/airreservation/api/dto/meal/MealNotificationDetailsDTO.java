/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.meal;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Indika
 * 
 */
public class MealNotificationDetailsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4989604594957208683L;

	/** Holds the Flight Id * */
	private int flightId;

	/** Holds the Flight Number * */
	private String flightNumber;

	/** Holds the Boarding Airport * */
	private String departureStation;

	/** Holds the Flight Dep Time in ZULU * */
	private Timestamp departureTimeZulu;

	/** Holds theDep Time in LOCAL Tm * */
	private Timestamp departuretimeLocal;

	/** Holds the Flight Seg Id * */
	private int flightSegId;

	/** Holds the Time time gap before flight departs the time for PNL to be sent * */
	private int notifyStartTime;

	/** Holds the ADL repeat Interval, THROGH CHECK IN Purpose * */
	private int notifyFrequency;

	/** Holds the Gap of the Last ADL to be sent from the flight dep zulu time **/
	private int lastNotification;

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public Timestamp getDepartureTimeZulu() {
		return departureTimeZulu;
	}

	public void setDepartureTimeZulu(Timestamp departureTimeZulu) {
		this.departureTimeZulu = departureTimeZulu;
	}

	public Timestamp getDeparturetimeLocal() {
		return departuretimeLocal;
	}

	public void setDeparturetimeLocal(Timestamp departuretimeLocal) {
		this.departuretimeLocal = departuretimeLocal;
	}

	public int getNotifyStartTime() {
		return notifyStartTime;
	}

	public void setNotifyStartTime(int notifyStartTime) {
		this.notifyStartTime = notifyStartTime;
	}

	public int getNotifyFrequency() {
		return notifyFrequency;
	}

	public void setNotifyFrequency(int notifyFrequency) {
		this.notifyFrequency = notifyFrequency;
	}

	public int getLastNotification() {
		return lastNotification;
	}

	public void setLastNotification(int lastNotification) {
		this.lastNotification = lastNotification;
	}
}
