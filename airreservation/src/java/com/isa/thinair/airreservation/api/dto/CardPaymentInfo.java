/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;

/**
 * To hold card payment data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class CardPaymentInfo extends CreditCardDetail implements Serializable, PaymentInfo, CustomerPayment {

	private static final long serialVersionUID = -5333962451408954342L;
	
	private static Log log = LogFactory.getLog(CardPaymentInfo.class);

	/** Holds total amount */
	private BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	/** Holds total currency information */
	private PayCurrencyDTO payCurrencyDTO;

	/** Holds payment Reference details */
	private PaymentReferenceTO paymentReferanceTO;

	/** Holds the payment carrier code */
	private String payCarrier;

	/** Holds the lcc unique Id for interline or dry booking */
	private String lccUniqueId;

	/** Holds the lcc userInputDTO */
	private UserInputDTO userInputDTO;
	
	/**
	 * Holds original paymentTnxId when the operation is a refund
	 */
	private Integer paymentTnxId;
	
	/**
	 * @return the paymentReferanceTO
	 */
	public PaymentReferenceTO getPaymentReferanceTO() {
		return paymentReferanceTO;
	}

	/**
	 * @param paymentReferanceTO
	 *            the paymentReferanceTO to set
	 */
	public void setPaymentReferanceTO(PaymentReferenceTO paymentReferanceTO) {
		this.paymentReferanceTO = paymentReferanceTO;
	}

	/**
	 * @return Returns the totalAmount.
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount
	 *            The totalBaseAmount to set.
	 */
	public void setTotalAmount(BigDecimal totalBaseAmount) {
		this.totalAmount = totalBaseAmount;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the payCarrier
	 */
	public String getPayCarrier() {
		return payCarrier;
	}

	/**
	 * @param payCarrier
	 *            the payCarrier to set
	 */
	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;
	}

	public String getLccUniqueId() {
		return lccUniqueId;
	}

	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	/**
	 * Clones the CardPaymentInfo FIXME add the payCarrier
	 */
	public Object clone() {
		CardPaymentInfo cardPaymentInfo = new CardPaymentInfo();
		cardPaymentInfo.setCcdId(this.getCcdId());
		cardPaymentInfo.setNo(this.getNo());
		cardPaymentInfo.setName(this.getName());
		cardPaymentInfo.setEDate(this.getEDate());
		cardPaymentInfo.setSecurityCode(this.getSecurityCode());
		cardPaymentInfo.setAddress(this.getAddress());
		cardPaymentInfo.setType(this.getType());
		cardPaymentInfo.setPnrPaxId(this.getPnrPaxId());
		cardPaymentInfo.setPnr(this.getPnr());
		cardPaymentInfo.setTransactionId(this.getTransactionId());
		cardPaymentInfo.setPaymentBrokerRefNo(this.getPaymentBrokerRefNo());
		cardPaymentInfo.setAppIndicator(this.getAppIndicator());
		cardPaymentInfo.setTnxMode(this.getTnxMode());
		cardPaymentInfo.setOldCCRecordId(this.getOldCCRecordId());
		cardPaymentInfo.setSuccess(this.isSuccess());
		cardPaymentInfo.setTemporyPaymentId(this.getTemporyPaymentId());
		cardPaymentInfo.setTotalAmount(this.getTotalAmount());
		cardPaymentInfo.setPayCurrencyDTO((PayCurrencyDTO) this.getPayCurrencyDTO().clone());
		cardPaymentInfo.setVersion(this.getVersion());
		cardPaymentInfo.setNoFirstDigits(this.getNoFirstDigits());
		cardPaymentInfo.setNoLastDigits(this.getNoLastDigits());
		cardPaymentInfo.setOperationType(this.getOperationType());
		cardPaymentInfo.setAuthorizationId(this.getAuthorizationId());
		cardPaymentInfo.setIpgIdentificationParamsDTO(this.getIpgIdentificationParamsDTO());
		cardPaymentInfo.setPayCarrier(this.getPayCarrier());
		cardPaymentInfo.setLccUniqueId(this.getLccUniqueId());
		cardPaymentInfo.setUserInputDTO(this.getUserInputDTO());
		cardPaymentInfo.setPaymentTnxId(this.getPaymentTnxId());
		cardPaymentInfo.setOfflinePayment(this.isOfflinePayment());
		return cardPaymentInfo;
	}

	/**
	 * Compare two CardPayment objects
	 * 
	 * @param cardPaymentInfoObj1
	 * @param cardPaymentInfoObj2
	 *            TODO check if we need to match the payCarrier as well ?
	 * @return
	 */
	public static boolean compare(CardPaymentInfo cardPaymentInfoObj1, CardPaymentInfo cardPaymentInfoObj2) {
		String strNoObj1 = BeanUtils.nullHandler(cardPaymentInfoObj1.getNo());
		String strNameObj1 = BeanUtils.nullHandler(cardPaymentInfoObj1.getName());
		String strEDateObj1 = BeanUtils.nullHandler(cardPaymentInfoObj1.getEDate());

		String strNoObj2 = BeanUtils.nullHandler(cardPaymentInfoObj2.getNo());
		String strNameObj2 = BeanUtils.nullHandler(cardPaymentInfoObj2.getName());
		String strEDateObj2 = BeanUtils.nullHandler(cardPaymentInfoObj2.getEDate());

		if (strNoObj1.equals(strNoObj2) && strNameObj1.equals(strNameObj2) && strEDateObj1.equals(strEDateObj2)) {
			return true;
		} else {
			log.error("Credit card details doesn't match:" + BeanUtils.getLast4Digits(strNoObj1) +strNameObj1+ 
					strEDateObj1 + "#" + BeanUtils.getLast4Digits(strNoObj2) + strNameObj2 + strEDateObj2);
			return false;
		}
	}

	public void setUserInputDTO(UserInputDTO userInputDTO) {
		this.userInputDTO = userInputDTO;
	}

	public UserInputDTO getUserInputDTO() {
		return userInputDTO;
	}

	/**
	 * @param paymentTnxId
	 */
	public Integer getPaymentTnxId() {
		return this.paymentTnxId;
	}

	
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;

	}
	
}
