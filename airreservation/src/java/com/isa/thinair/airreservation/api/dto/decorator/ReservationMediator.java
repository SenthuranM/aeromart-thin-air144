/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto.decorator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.util.FarePercentageCalculator;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Mediates with the main Reservation object and provides TO(s) for other modules with out having tight coupling with
 * the other modules TO(s) and the main reservation module
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationMediator implements Serializable {

	private static final long serialVersionUID = 1225009975206759183L;

	/** Holds the reservation object */
	private final Reservation reservation;

	/**
	 * Construct Reservation Object
	 */
	public ReservationMediator(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * Return Ond Fare DTOs This is in order to calculate the charge quote for add an infant
	 * 
	 * @return collection of OndFareDTO
	 */
	@SuppressWarnings("unchecked")
	public Collection<OndFareDTO> getOndFareDTOs(int noOfInfants) throws ModuleException {
		if (this.reservation.getSegmentsView() == null) {
			throw new ModuleException("airreservations.arg.invalidLoad");
		}
		Iterator<ReservationPax> iterator = this.reservation.getPassengers().iterator();
		ReservationPax reservationPax;
		ReservationPaxFare reservationPaxFare;
		ReservationPaxFareSegment reservationPaxFareSegment;
		Iterator<ReservationPaxFare> itPnrPaxFares;
		Iterator<ReservationPaxFareSegment> itPnrPaxFareSeg;
		Iterator<FlightSegmentDTO> itFlightSegmentDTOs;
		OndFareDTO ondFareDTO;
		FareSummaryDTO fareSummaryDTO;
		Collection<OndFareDTO> colOndFareDTO = new ArrayList<OndFareDTO>();
		Collection<ReservationPaxFareSegment> pnrPaxFareSegments;
		Collection<String> ondCodes;
		Collection<FlightSegmentDTO> flightSegmentDTOs;
		FlightSegmentDTO flightSegmentDTO;
		boolean isAllSegmentsCancelled = false;

		Map<Collection<Integer>, Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds = ReservationApiUtils.getInverseSegments(
				reservation, true);
		int ondSequence = 0;
		while (iterator.hasNext()) {
			reservationPax = iterator.next();
			isAllSegmentsCancelled = isAllSegmentsCancelledForReservation(reservationPax);
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				itPnrPaxFares = reservationPax.getPnrPaxFares().iterator();

				while (itPnrPaxFares.hasNext()) {
					reservationPaxFare = itPnrPaxFares.next();

					ondFareDTO = new OndFareDTO();
					fareSummaryDTO = new FareSummaryDTO();
					int fareId = reservationPaxFare.getFareId().intValue();

					BigDecimal currentFareAmount = reservationPaxFare.getTotalFare();

					// AARESAA-8777 - when return journey is served by half-return fares following code doesn't make
					// sense when calculating infant fare to over come following method call commented.
					// BigDecimal totalFareAmount = getTotalFareAmount(reservationPaxFare, fareId, currentFareAmount,
					// mapSetPSegIdsAndSetInvPSegIds);
					BigDecimal totalFareAmount = getOrginalFareAmount(fareId);
					int farePercentage;

					if (totalFareAmount.doubleValue() > 0) {
						farePercentage = AccelAeroCalculator.divide(
								AccelAeroCalculator.multiply(currentFareAmount, new BigDecimal(100)), totalFareAmount).intValue();
					} else if (totalFareAmount.doubleValue() == 0 && currentFareAmount.doubleValue() == 0) {
						farePercentage = getReturnFarePercetange(reservationPaxFare, mapSetPSegIdsAndSetInvPSegIds);
					} else {
						farePercentage = 0;
					}

					fareSummaryDTO.setFareId(fareId);
					fareSummaryDTO.setFarePercentage(farePercentage);
					ondFareDTO.setFareSummaryDTO(fareSummaryDTO);

					itPnrPaxFareSeg = reservationPaxFare.getPaxFareSegments().iterator();
					pnrPaxFareSegments = new ArrayList<ReservationPaxFareSegment>();

					// Capturing the pnr segment ids
					while (itPnrPaxFareSeg.hasNext()) {
						reservationPaxFareSegment = itPnrPaxFareSeg.next();

						if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationPaxFareSegment
								.getSegment().getStatus())
								|| ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING
										.equals(reservationPaxFareSegment.getSegment().getStatus())) {
							pnrPaxFareSegments.add(reservationPaxFareSegment);
						}
						/** Modified by Lalanthi */
						/**
						 * This is to basically get ondFareDTO collection for AddInfant operation. If all the segments
						 * in an Interline reservation is canceled, then only add those ondfareDTO's, else (if there are
						 * both CNF and CNX segments then no need to add dummy data. (Old flow is fine for those cases)
						 */
						else if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationPaxFareSegment
								.getSegment().getStatus())
								&& isInterlinedReservation(this.reservation.getAdminInfo().getOriginChannelId().intValue())) {
							if (isAllSegmentsCancelled) {
								pnrPaxFareSegments.add(reservationPaxFareSegment);// Add the CNX segments also.
							}
						}
					}

					if (pnrPaxFareSegments.size() > 0) {
						// Get the flight Segments
						Object[] object = this.getFlightSegments(pnrPaxFareSegments, noOfInfants);
						Collection<SegmentSeatDistsDTO> seatDistsDTOs = (Collection<SegmentSeatDistsDTO>) object[0];
						SegmentSeatDistsDTO first = seatDistsDTOs.iterator().next();
						// Set the segment seat distribution
						ondFareDTO.setSegmentSeatDistsDTOs(seatDistsDTOs);
						ondFareDTO.setInBoundOnd(first.isBelongToInboundOnd());

						// Set the flight segment information
						flightSegmentDTOs = (Collection<FlightSegmentDTO>) object[1];
						itFlightSegmentDTOs = flightSegmentDTOs.iterator();
						ondCodes = new ArrayList<String>();

						while (itFlightSegmentDTOs.hasNext()) {
							flightSegmentDTO = itFlightSegmentDTOs.next();
							ondCodes.add(flightSegmentDTO.getSegmentCode());
							ondFareDTO.addSegment(flightSegmentDTO.getSegmentId(), flightSegmentDTO);
						}

						// Set the ond code
						ondFareDTO.setOndCode(ReservationApiUtils.getOndCode(ondCodes));
						ondFareDTO.setOndSequence(ondSequence++);
						colOndFareDTO.add(ondFareDTO);
					}
				}

				// Only need to know per passenger only...
				// All passengers will be going in the same route
				break;
			}

		}
		
		colOndFareDTO = sortByFirstDepartureDate(new ArrayList<OndFareDTO>(colOndFareDTO)) ;
		
		return colOndFareDTO;
	}

	private int getReturnFarePercetange(ReservationPaxFare reservationPaxFare, Map mapSetPSegIdsAndSetInvPSegIds)
			throws ModuleException {
		Collection<Integer> currentPnrSegIds = getPnrSegIds(reservationPaxFare);
		Collection<Integer> colSetInversePnrSegIds = ReservationApiUtils.getInversePnrSegIds(mapSetPSegIdsAndSetInvPSegIds,
				currentPnrSegIds);
		if (colSetInversePnrSegIds != null && colSetInversePnrSegIds.size() > 0) {
			return FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE;
		}
		return 0;
	}
	
	private boolean isOpenReturnBooking(ReservationPaxFare reservationPaxFare) {
		for (ReservationPaxFare resPaxFare : reservationPaxFare.getReservationPax().getPnrPaxFares()) {
			for (ReservationPaxFareSegment reservationPaxFareSegment : resPaxFare.getPaxFareSegments()) {
				if (reservationPaxFareSegment.getSegment().getOpenReturnConfirmBeforeZulu() != null) {
					return true;
				}
			}
		}
		return false;
	}

	private BigDecimal getOrginalFareAmount(int fareId) throws ModuleException {
		FareDTO fareDTO = ReservationModuleUtils.getFareBD().getFare(fareId);

		if (fareDTO != null && fareDTO.getFare().getFareAmount() > 0) {
			return AccelAeroCalculator.parseBigDecimal(fareDTO.getFare().getFareAmount());
		}

		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	// when return journey is served by half-return fares following code doesnt make sense when calculating infant fare
	// to over come following method commented - AARESAA-8777

	// private BigDecimal getTotalFareAmount(ReservationPaxFare reservationPaxFare, int fareId, BigDecimal
	// currentFareAmount,
	// Map<Collection<Integer>,Collection<Integer>> mapSetPSegIdsAndSetInvPSegIds) throws ModuleException {
	//
	// Collection<Integer> currentPnrSegIds = getPnrSegIds(reservationPaxFare);
	// Collection<Integer> colSetInversePnrSegIds = ReservationApiUtils
	// .getInversePnrSegIds(mapSetPSegIdsAndSetInvPSegIds, currentPnrSegIds);
	//
	// if (colSetInversePnrSegIds != null && colSetInversePnrSegIds.size() > 0) {
	// BigDecimal inversedFareAmount = getApplicableFareAmount(reservationPaxFare.getReservationPax().getPnrPaxFares(),
	// colSetInversePnrSegIds, fareId);
	//
	// return AccelAeroCalculator.add(currentFareAmount, inversedFareAmount);
	// } else {
	// return currentFareAmount;
	// }
	// }
	//
	// private BigDecimal getApplicableFareAmount(Collection<ReservationPaxFare> pnrPaxFares, Collection<Integer>
	// colSetInversePnrSegIds,
	// int fareId) throws ModuleException {
	// for (ReservationPaxFare reservationPaxFare : pnrPaxFares) {
	// Collection<Integer> matchingPnrSegIds = getPnrSegIds(reservationPaxFare);
	//
	// if (matchingPnrSegIds != null && matchingPnrSegIds.size() > 0) {
	// if (matchingPnrSegIds.size() == colSetInversePnrSegIds.size()
	// && matchingPnrSegIds.containsAll(colSetInversePnrSegIds)) {
	// // if (fareId == reservationPaxFare.getFareId()) {
	// return reservationPaxFare.getTotalFare();
	// // } else {
	// // return AccelAeroCalculator.getDefaultBigDecimalZero();
	// // }
	// }
	// }
	// }
	//
	// throw new ModuleException("airreservation.common.return.fare.does.not.exists");
	// }

	private Collection<Integer> getPnrSegIds(ReservationPaxFare reservationPaxFare) {

		Collection<Integer> pnrSegIds = new HashSet<Integer>();

		for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
			pnrSegIds.add(reservationPaxFareSegment.getPnrSegId());
		}

		return pnrSegIds;
	}

	/**
	 * Return FlightSegmentDTO(s)
	 * 
	 * @param pnrPaxFareSegments
	 * @return a collection of FlightSegmentDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private Object[] getFlightSegments(Collection<ReservationPaxFareSegment> pnrPaxFareSegments, int noOfInfants)
			throws ModuleException {
		ReservationPaxFareSegment reservationPaxFareSegment;
		ReservationSegmentDTO reservationSegmentDTO;
		FlightSegmentDTO flightSegmentDTO;
		SegmentSeatDistsDTO segmentSeatDistsDTO;
		Iterator<ReservationPaxFareSegment> itReservationPaxFareSegment = pnrPaxFareSegments.iterator();
		Iterator<ReservationSegmentDTO> itSegView;
		Integer pnrSegId;
		List<FlightSegmentDTO> flightSegments = new ArrayList<FlightSegmentDTO>();
		List<SegmentSeatDistsDTO> segmentSeatDistsDTOs = new ArrayList<SegmentSeatDistsDTO>();

		while (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = itReservationPaxFareSegment.next();
			pnrSegId = reservationPaxFareSegment.getSegment().getPnrSegId();

			itSegView = this.reservation.getSegmentsView().iterator();

			while (itSegView.hasNext()) {
				reservationSegmentDTO = itSegView.next();

				if (reservationSegmentDTO.getPnrSegId().equals(pnrSegId)) {
					flightSegmentDTO = new FlightSegmentDTO();
					segmentSeatDistsDTO = new SegmentSeatDistsDTO();

					// Composing the flight segment dto
					flightSegmentDTO.setDepartureDateTime(reservationSegmentDTO.getDepartureDate());
					flightSegmentDTO.setArrivalDateTime(reservationSegmentDTO.getArrivalDate());

					flightSegmentDTO.setDepartureDateTimeZulu(reservationSegmentDTO.getZuluDepartureDate());
					flightSegmentDTO.setArrivalDateTimeZulu(reservationSegmentDTO.getZuluArrivalDate());

					flightSegmentDTO.setSegmentId(reservationSegmentDTO.getFlightSegId());
					flightSegmentDTO.setFlightNumber(reservationSegmentDTO.getFlightNo());
					flightSegmentDTO.setFlightId(reservationSegmentDTO.getFlightId());
					flightSegmentDTO.setSegmentCode(reservationSegmentDTO.getSegmentCode());

					// Composing the segment seat dist dto
					SeatDistribution seatDistribution = new SeatDistribution();
					seatDistribution.setBookingClassCode(reservationPaxFareSegment.getBookingCode());
					if (reservationPaxFareSegment.getBcNested() != null
							&& reservationPaxFareSegment.getBcNested().equals(ReservationInternalConstants.BCNested.YES)) {
						seatDistribution.setNestedSeats(true);
					}
					seatDistribution.setBookingClassType(reservationSegmentDTO.getBookingType());
					if (reservationSegmentDTO.getStatus().equals(
							ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						seatDistribution.setWaitListed(true);
					}
					segmentSeatDistsDTO.addSeatDistribution(seatDistribution);

					// TODO : Logical CC Change - test the flow
					segmentSeatDistsDTO.setLogicalCabinClass(reservationSegmentDTO.getLogicalCCCode() != null
							? reservationSegmentDTO.getLogicalCCCode()
							: "Y1");
					segmentSeatDistsDTO.setCabinClassCode(reservationSegmentDTO.getCabinClassCode());
					segmentSeatDistsDTO.setFlightSegId(reservationSegmentDTO.getFlightSegId().intValue());
					segmentSeatDistsDTO.setPnrSegId(reservationSegmentDTO.getPnrSegId().intValue());
					segmentSeatDistsDTO.setFlightId(reservationSegmentDTO.getFlightId().intValue());
					segmentSeatDistsDTO.setSegmentCode(reservationSegmentDTO.getSegmentCode());
					segmentSeatDistsDTO.setNoOfInfantSeats(noOfInfants);

					segmentSeatDistsDTO.setBelongToInboundOnd((reservationSegmentDTO.getReturnFlag().equals("Y")));

					flightSegments.add(flightSegmentDTO);
					segmentSeatDistsDTOs.add(segmentSeatDistsDTO);
					break;
				}
			}
		}

		// Sorts the flight segments by earliest departure date
		Collections.sort(flightSegments);

		return new Object[] { segmentSeatDistsDTOs, flightSegments };
	}

	/***
	 * TODO: check whether this assumption is correct
	 * 
	 * @param pnr
	 * @return
	 */
	private boolean isInterlinedReservation(int salesChannel) {
		if (salesChannel == ReservationInternalConstants.SalesChannel.LCC) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isAllSegmentsCancelledForReservation(ReservationPax passenger) {
		List<Boolean> segmentStatus = new ArrayList<Boolean>();
		int segCount = 0;

		for (ReservationPaxFare paxFare : passenger.getPnrPaxFares()) {
			for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(paxFareSeg.getSegment().getStatus())) {
					segmentStatus.add(new Boolean(true));
				}
				segCount++; // Gets the total segments count
			}
		}
		if (segmentStatus.size() >= segCount) { // All the segments are canceled state
			return true;
		} else {
			return false;
		}
	}	
	
	// sort the OndFareDTO collection by firstDepartureDate and re-arrange the ondSequence
	private List<OndFareDTO> sortByFirstDepartureDate(List<OndFareDTO> colOndFareDTO) {

		if (colOndFareDTO != null && colOndFareDTO.size() > 1) {
			Collections.sort(colOndFareDTO, new Comparator<OndFareDTO>() {
				@Override
				public int compare(OndFareDTO firstObj, OndFareDTO secondObj) {
					if ((firstObj.getFirstDepartureDateZulu().compareTo(secondObj.getFirstDepartureDateZulu()) > 0)
							&& secondObj.getOndSequence() > firstObj.getOndSequence()) {
						int tempOndSeq = firstObj.getOndSequence();
						firstObj.setOndSequence(secondObj.getOndSequence());
						secondObj.setOndSequence(tempOndSeq);
					}
					return firstObj.getFirstDepartureDateZulu().compareTo(secondObj.getFirstDepartureDateZulu());
				}
			});
		}
		return colOndFareDTO;
	}
}
