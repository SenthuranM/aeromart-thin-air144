package com.isa.thinair.airreservation.api.dto;

/**
 * @author Manoj Dhanushka
 */
public class TypeBResponseDTO {
	
	private boolean synchronousCommunication;
	
	private boolean success;

	public boolean isSynchronousCommunication() {
		return synchronousCommunication;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSynchronousCommunication(boolean synchronousCommunication) {
		this.synchronousCommunication = synchronousCommunication;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
