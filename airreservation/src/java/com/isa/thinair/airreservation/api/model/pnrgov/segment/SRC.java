package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class SRC extends EDISegment{

	public SRC() {
		super(EDISegmentTag.SRC);
	}

	@Override
	public MessageSegment build() {
		return this;
	}

}
