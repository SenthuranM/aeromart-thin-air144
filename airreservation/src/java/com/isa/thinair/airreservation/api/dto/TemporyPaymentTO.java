package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Nilindra Fernando
 */
public class TemporyPaymentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8922816806360128952L;

	private String telephoneNo;

	private String mobileNo;

	private BigDecimal amount;

	private BigDecimal paymentCurrencyAmount;

	private String paymentCurrencyCode;

	private String ccNo;

	private String groupPnr;

	private String status;
	
	private String contactFullName;

	private String firstName;

	private String lastName;

	private boolean isCredit;

	private String emailAddress;

	private boolean isOfflinePayment = false;
	
	private Character productType;

	/**
	 * @return the telephoneNo
	 */
	public String getTelephoneNo() {
		return telephoneNo;
	}

	/**
	 * @param telephoneNo
	 *            the telephoneNo to set
	 */
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo
	 *            the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the paymentCurrencyAmount
	 */
	public BigDecimal getPaymentCurrencyAmount() {
		return paymentCurrencyAmount;
	}

	/**
	 * @param paymentCurrencyAmount
	 *            the paymentCurrencyAmount to set
	 */
	public void setPaymentCurrencyAmount(BigDecimal paymentCurrencyAmount) {
		this.paymentCurrencyAmount = paymentCurrencyAmount;
	}

	/**
	 * @return the paymentCurrencyCode
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	/**
	 * @return the groupPnr
	 */
	public String getGroupPnr() {
		return groupPnr;
	}

	/**
	 * @param groupPnr
	 *            the groupPnr to set
	 */
	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the ccNo
	 */
	public String getCcNo() {
		return ccNo;
	}

	/**
	 * @param ccNo
	 *            the ccNo to set
	 */
	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	/**
	 * @return the isCredit
	 */
	public boolean isCredit() {
		return isCredit;
	}

	/**
	 * @param isCredit
	 *            the isCredit to set
	 */
	public void setCredit(boolean isCredit) {
		this.isCredit = isCredit;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isOfflinePayment() {
		return isOfflinePayment;
	}

	public void setOfflinePayment(boolean isOfflinePayment) {
		this.isOfflinePayment = isOfflinePayment;
	}

	/**
	 * @return the productType
	 */
	public Character getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(Character productType) {
		this.productType = productType;
	}

	public String getContactFullName() {
		return contactFullName;
	}

	public void setContactFullName(String contactFullName) {
		this.contactFullName = contactFullName;
	}
	
	
}
