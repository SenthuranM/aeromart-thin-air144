/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Locale;

import com.isa.thinair.commons.core.util.StaticFileNameUtil;

/**
 * To hold itinerary layout specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ItineraryLayoutModesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7254184801800673144L;

	/** Holds the template name */
	private String templateName;

	/** Holds the reservation number */
	private String pnr;

	/** Holds the locale specific information */
	private Locale locale;

	/** Holds whether to load passenger prices */
	private boolean includePassengerPrices;

	/** Holds whether to load detail charges */
	private boolean includeDetailCharges;

	/** Holds whether to load detail payments */
	private boolean includeDetailPayments;

	/** Holds the carrierCode */
	private String carrierCode;

	/** Holds whether to include COS */
	private boolean includeClassOfService;

	/** Holds whether to include Logical Cabin Class */
	private boolean includeLogicalCC;

	/** Hold whether to include carrier thumbnail logo in travel segments */
	private boolean includeThumbnailLogo;

	/**
	 * @return Returns the includeDetailCharges.
	 */
	public boolean isIncludeDetailCharges() {
		return includeDetailCharges;
	}

	/**
	 * @param includeDetailCharges
	 *            The includeDetailCharges to set.
	 */
	public void setIncludeDetailCharges(boolean includeDetailCharges) {
		this.includeDetailCharges = includeDetailCharges;
	}

	/**
	 * @return Returns the includeDetailPayments.
	 */
	public boolean isIncludeDetailPayments() {
		return includeDetailPayments;
	}

	/**
	 * @param includeDetailPayments
	 *            The includeDetailPayments to set.
	 */
	public void setIncludeDetailPayments(boolean includeDetailPayments) {
		this.includeDetailPayments = includeDetailPayments;
	}

	/**
	 * @return Returns the includePassengerPrices.
	 */
	public boolean isIncludePassengerPrices() {
		return includePassengerPrices;
	}

	/**
	 * @param includePassengerPrices
	 *            The includePassengerPrices to set.
	 */
	public void setIncludePassengerPrices(boolean includePassengerPrices) {
		this.includePassengerPrices = includePassengerPrices;
	}

	/**
	 * @return Returns the locale.
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            The locale to set.
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the templateName.
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName
	 *            The templateName to set.
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getLogoImageName() {
		String logoImageName = "LogoAni.gif";
		if (getCarrierCode() != null) {
			logoImageName = "LogoAni" + getCarrierCode() + ".gif";
		}
		return StaticFileNameUtil.getCorrected(logoImageName);
	}

	public boolean isIncludeClassOfService() {
		return includeClassOfService;
	}

	public void setIncludeClassOfService(boolean includeClassOfService) {
		this.includeClassOfService = includeClassOfService;
	}

	public boolean isIncludeLogicalCC() {
		return includeLogicalCC;
	}

	public void setIncludeLogicalCC(boolean includeLogicalCC) {
		this.includeLogicalCC = includeLogicalCC;
	}

	public boolean isIncludeThumbnailLogo() {
		return includeThumbnailLogo;
	}

	public void setIncludeThumbnailLogo(boolean includeThumbnailLogo) {
		this.includeThumbnailLogo = includeThumbnailLogo;
	}
}
