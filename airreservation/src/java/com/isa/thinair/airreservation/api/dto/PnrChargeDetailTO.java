/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To hold reservation charges detail data transfer information {Per PaxType}
 * 
 * @author DumindaG
 * @since 1.0
 */
public class PnrChargeDetailTO implements Serializable {

	private static final long serialVersionUID = 2746487127810342077L;

	private Integer fareID; // if custom charges defined,will be zero

	private String modificationChargeType;

	private String cancellationChargeType;

	private String nameChangeChargeType;

	private BigDecimal minModificationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal maximumModificationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal minCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal maximumCancellationAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal minNameChangeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal maxNameChangeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal chargePercentage = AccelAeroCalculator.getDefaultBigDecimalZero();

	private boolean isCustomCharge;

	public PnrChargeDetailTO() {

	}

	public String getChargeType(boolean isCancelation) {
		if (isCancelation) {
			return this.cancellationChargeType;
		} else {
			return this.modificationChargeType;
		}
	}

	/**
	 * @return the minModificationAmount
	 */
	public BigDecimal getMinModificationAmount() {
		return minModificationAmount;
	}

	/**
	 * @return the maximumModificationAmount
	 */
	public BigDecimal getMaximumModificationAmount() {
		return maximumModificationAmount;
	}

	/**
	 * @return the minCancellationAmount
	 */
	public BigDecimal getMinCancellationAmount() {
		return minCancellationAmount;
	}

	/**
	 * @return the maximumCancellationAmount
	 */
	public BigDecimal getMaximumCancellationAmount() {
		return maximumCancellationAmount;
	}

	/**
	 * @return the chargePercentage
	 */
	public BigDecimal getChargePercentage() {
		return chargePercentage;
	}

	/**
	 * @param minModificationAmount
	 *            the minModificationAmount to set
	 */
	public void setMinModificationAmount(BigDecimal minModificationAmount) {
		this.minModificationAmount = minModificationAmount;
	}

	/**
	 * @param maximumModificationAmount
	 *            the maximumModificationAmount to set
	 */
	public void setMaximumModificationAmount(BigDecimal maximumModificationAmount) {
		this.maximumModificationAmount = maximumModificationAmount;
	}

	/**
	 * @param minCancellationAmount
	 *            the minCancellationAmount to set
	 */
	public void setMinCancellationAmount(BigDecimal minCancellationAmount) {
		this.minCancellationAmount = minCancellationAmount;
	}

	/**
	 * @param maximumCancellationAmount
	 *            the maximumCancellationAmount to set
	 */
	public void setMaximumCancellationAmount(BigDecimal maximumCancellationAmount) {
		this.maximumCancellationAmount = maximumCancellationAmount;
	}

	/**
	 * @param chargePercentage
	 *            the chargePercentage to set
	 */
	public void setChargePercentage(BigDecimal chargePercentage) {
		this.chargePercentage = chargePercentage;
	}

	/**
	 * @return the fareID
	 */
	public Integer getFareID() {
		return fareID;
	}

	/**
	 * @param fareID
	 *            the fareID to set
	 */
	public void setFareID(Integer fareID) {
		this.fareID = fareID;
	}

	/**
	 * @return the modificationChargeType
	 */
	public String getModificationChargeType() {
		return modificationChargeType;
	}

	/**
	 * @param modificationChargeType
	 *            the modificationChargeType to set
	 */
	public void setModificationChargeType(String modificationChargeType) {
		this.modificationChargeType = modificationChargeType;
	}

	/**
	 * @return the cancellationChargeType
	 */
	public String getCancellationChargeType() {
		return cancellationChargeType;
	}

	/**
	 * @param cancellationChargeType
	 *            the cancellationChargeType to set
	 */
	public void setCancellationChargeType(String cancellationChargeType) {
		this.cancellationChargeType = cancellationChargeType;
	}

	/**
	 * @return the isCustomCharge
	 */
	public boolean isCustomCharge() {
		return isCustomCharge;
	}

	/**
	 * @param isCustomCharge
	 *            the isCustomCharge to set
	 */
	public void setCustomCharge(boolean isCustomCharge) {
		this.isCustomCharge = isCustomCharge;
	}

	public String getNameChangeChargeType() {
		return nameChangeChargeType;
	}

	public void setNameChangeChargeType(String nameChangeChargeType) {
		this.nameChangeChargeType = nameChangeChargeType;
	}

	public BigDecimal getMinNameChangeAmount() {
		return minNameChangeAmount;
	}

	public void setMinNameChangeAmount(BigDecimal minNameChangeAmount) {
		this.minNameChangeAmount = minNameChangeAmount;
	}

	public BigDecimal getMaxNameChangeAmount() {
		return maxNameChangeAmount;
	}

	public void setMaxNameChangeAmount(BigDecimal maxNameChangeAmount) {
		this.maxNameChangeAmount = maxNameChangeAmount;
	}

}
