package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class MON extends EDISegment {

	private String e5025;
	private String e1230;
	private String e6345;
	private String e3225;
	
	public void setMonetaryAmountType(String e5025) {
		this.e5025 = e5025;
	}

	public void setTicketAmount(String e1230) {
		this.e1230 = e1230;
	}

	public void setCurrency(String e6345) {
		this.e6345 = e6345;
	}

	public void setLocation(String e3225) {
		this.e3225 = e3225;
	}

	public MON() {
		super(EDISegmentTag.MON);
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C663 = new CompoundDataElement("C663", DE_STATUS.M);
		C663.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E5025, e5025));
		C663.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E1230, e1230));
		C663.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E6345, e6345));
		C663.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3225,e3225));
		
		addEDIDataElement(C663);
		return this;
	}

}
