package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;

/**
 * @author chethiya
 *
 */
public class VoucherPaymentInfo implements Serializable, PaymentInfo, CustomerPayment {

	private static final long serialVersionUID = -1735142865584127015L;

	private VoucherDTO voucherDTO;

	private String lccUniqueId;

	private BigDecimal totalAmount;

	private PayCurrencyDTO payCurrencyDTO;

	private String payCarrier;

	private Integer paymentTnxId;

	@Override
	public String getLccUniqueId() {
		return lccUniqueId;
	}

	@Override
	public void setLccUniqueId(String lccUniqueId) {
		this.lccUniqueId = lccUniqueId;
	}

	@Override
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	@Override
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	@Override
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	@Override
	public String getPayCarrier() {
		return payCarrier;
	}

	@Override
	public void setPayCarrier(String payCarrier) {
		this.payCarrier = payCarrier;
	}

	@Override
	public Integer getPaymentTnxId() {
		return paymentTnxId;
	}

	@Override
	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;
	}

	/**
	 * @return the voucherDTO
	 */
	public VoucherDTO getVoucherDTO() {
		return voucherDTO;
	}

	/**
	 * @param voucherDTO the voucherDTO to set
	 */
	public void setVoucherDTO(VoucherDTO voucherDTO) {
		this.voucherDTO = voucherDTO;
	}

}
