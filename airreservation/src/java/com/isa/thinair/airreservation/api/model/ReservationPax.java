/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * To keep track of reservation passenger information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_PNR_PASSENGER"
 */
public class ReservationPax extends Persistent implements Serializable, Comparable<ReservationPax> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3129029804557199074L;
	// Reservation related
	/** Holds the reservation */
	private Reservation reservation;
	/** Holds reservation passenger id */
	private Integer pnrPaxId;
	/** Holds reservation passenger type */
	private String paxType;
	/** Holds reservation passenger sequence */
	private Integer paxSequence;
	/** Holds reservation zulu passenger initialize time */
	private Date zuluStartTimeStamp;
	/** Holds reservation zulu passenger release time */
	private Date zuluReleaseTimeStamp;
	/** Holds reservation local passenger initialize time */
	private Date localStartTimeStamp;
	/** Holds reservation local passenger release time */
	private Date localReleaseTimeStamp;
	/** Holds reservation passenger status */
	private String status;

	// Contact related
	/** Holds corresponding adult id before persist */
	private Integer indexId;
	/** Holds reservation passenger title */
	private String title;
	/** Holds reservation passenger first name */
	private String firstName;
	/** Holds reservation passenger last name */
	private String lastName;
	/** Holds reservation passenger upper first name */
	private String upperFirstName;
	/** Holds reservation passenger upper last name */
	private String upperLastName;
	/** Holds reservation passenger nationality code */
	private Integer nationalityCode;
	/** Holds reservation passenger date of birth */
	private Date dateOfBirth;

	/** Holds the passenger category */
	private String paxCategory;

	// private ReservationPaxAdditionalInfo paxAdditionalInfo;
	private Set<ReservationPaxAdditionalInfo> paxAdditionalInfoSet;

	private Set<PaxNameTranslations> paxNameTranslations;

	// Charges related
	/** Holds reservation passenger total fare */
	private BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total sur-charge */
	private BigDecimal totalSurCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total tax */
	private BigDecimal totalTaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total cancel charge */
	private BigDecimal totalCancelCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total modification charge */
	private BigDecimal totalModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger total adjustment charge */
	private BigDecimal totalAdjustmentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger paid amount */
	private BigDecimal totalPaidAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	/** Holds reservation passenger available credit or debit amount */
	private BigDecimal totalAvailableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalModificationPenalty = AccelAeroCalculator.getDefaultBigDecimalZero();;

	private BigDecimal totalDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();;

	/** Holds whether this person holds a revenue account */
	private boolean isAccountHolder;

	// /** Holds SSR Code */
	// private String ssrCode;
	// /** Holds SSR Remarks */
	// private String ssrRemarks;

	// Details related
	/** Holds a set of ReservationPaxFare */
	private Set<ReservationPaxFare> pnrPaxFares;

	/** Holds the Parent */
	private ReservationPax parent;

	/** Holds a set of infants */
	private Set<ReservationPax> infants;

	/** Holds passenger payment information */
	private IPayment payment;

	/** Holds a Collection of ChargeDetailDTO (Supports Read Only) */
	private Collection<ChargesDetailDTO> ondChargesView;

	/** Holds a Collection of ReservationTnx (supports read only) */
	private Collection<ReservationTnx> paxPaymentTnxView;

	/** Holds a Collection of ReservationPaxExtTnx (supports read only) */
	private Collection<ReservationPaxExtTnx> externalPaxPaymentTnxView;

	private Collection<PaxSSRDTO> paxSSR;

	/** Holds a set of EticketTOs (supports read only) */
	private Collection<EticketTO> eTickets;

	private Integer autoCancellationId;

	private Integer familyID;
	
	private BigDecimal noShowRefundableAmount;
	
	private boolean isNoShowRefundable;

	/**
	 * @return Returns the dateOfBirth.
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            The dateOfBirth to set.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;

		if (!BeanUtils.nullHandler(firstName).equals("")) {
			this.setUpperFirstName(firstName.toUpperCase());
		}
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;

		if (!BeanUtils.nullHandler(lastName).equals("")) {
			this.setUpperLastName(lastName.toUpperCase());
		}
	}

	/**
	 * @return Returns the nationalityCode.
	 * @hibernate.property column = "NATIONALITY_CODE"
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 *            The nationalityCode to set.
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return Returns the paxSequence.
	 * @hibernate.property column = "PAX_SEQUENCE"
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            The paxSequence to set.
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * Returns the passenger type description
	 * 
	 * @return
	 */
	public String getPaxTypeDescription() {
		return ReservationPax.getPassengerTypeDescription(getPaxType());
	}

	/**
	 * Returns the passenger type description based on the passenger type code
	 * 
	 * @param paxType
	 * @return
	 */
	public static String getPassengerTypeDescription(String paxType) {
		String paxTypeDesc = "";

		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			paxTypeDesc = "ADULT";
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			paxTypeDesc = "CHILD";
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			paxTypeDesc = "INFANT";
		}

		return paxTypeDesc;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the paxAdditionalInfoSet.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PNR_PAX_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo"
	 */
	public Set<ReservationPaxAdditionalInfo> getPaxAdditionalInfoSet() {
		return paxAdditionalInfoSet;
	}

	/**
	 * @param paxAdditionalInfoSet
	 *            the paxAdditionalInfoSet to set
	 */
	public void setPaxAdditionalInfoSet(Set<ReservationPaxAdditionalInfo> paxAdditionalInfoSet) {
		this.paxAdditionalInfoSet = paxAdditionalInfoSet;
	}

	public ReservationPaxAdditionalInfo getPaxAdditionalInfo() {
		ReservationPaxAdditionalInfo r = new ReservationPaxAdditionalInfo();
		if (getPaxAdditionalInfoSet() != null) {
			Iterator<ReservationPaxAdditionalInfo> i = getPaxAdditionalInfoSet().iterator();
			if (i.hasNext()) {
				r = (ReservationPaxAdditionalInfo) i.next();
			}
		}
		return r;
	}

	public void setPaxAdditionalInfo(ReservationPaxAdditionalInfo paxAdditionalInfo) {
		setPaxAdditionalInfoSet(new HashSet<ReservationPaxAdditionalInfo>());
		paxAdditionalInfo.setReservationPax(this);
		this.paxAdditionalInfoSet.add(paxAdditionalInfo);
	}

	/**
	 * @return Returns the totalFare.
	 * @hibernate.property column = "TOTAL_FARE"
	 */
	public BigDecimal getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            The totalFare to set.
	 */
	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	/**
	 * @return Returns the pnrPaxFares.
	 * @hibernate.set lazy="true" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PNR_PAX_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPaxFare"
	 */
	public Set<ReservationPaxFare> getPnrPaxFares() {
		return pnrPaxFares;
	}

	/**
	 * @param infants
	 *            The infants to set.
	 */
	private void setInfants(Set<ReservationPax> infants) {
		this.infants = infants;
	}

	/**
	 * @param pnrPaxFares
	 *            The pnrPaxFares to set.
	 */
	private void setPnrPaxFares(Set<ReservationPaxFare> pnrPaxFares) {
		this.pnrPaxFares = pnrPaxFares;
	}

	/**
	 * Add pnr passenger fare
	 * 
	 * @param pnrPaxFares
	 */
	public void addPnrPaxFare(ReservationPaxFare reservationPaxFare) {
		if (this.getPnrPaxFares() == null) {
			this.setPnrPaxFares(new HashSet<ReservationPaxFare>());
		}

		reservationPaxFare.setReservationPax(this);
		this.getPnrPaxFares().add(reservationPaxFare);
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getPnrPaxId()).toHashCode();
	}

	/**
	 * @return Returns the totalPaidAmount.
	 */
	public BigDecimal getTotalPaidAmount() {
		return totalPaidAmount;
	}

	/**
	 * @param totalPaidAmount
	 *            The totalPaidAmount to set.
	 */
	public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	/**
	 * @return Returns the pnrPaxId.
	 * @hibernate.id column = "PNR_PAX_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PASSENGER"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	/**
	 * @param pnrPaxId
	 *            The pnrPaxId to set.
	 */
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * Holds reservation passenger adult id 1. For a parent this holds infant id 2. For an infant this holds the parent
	 * id 3. For an adult this id will be null
	 * 
	 * @return Returns the adultId.
	 */
	public Integer getAccompaniedPaxId() {
		// Normal Adult
		Integer adultInfantId = null;

		// Parent
		if (infants != null && infants.size() != 0) {
			Iterator<ReservationPax> iterator = infants.iterator();
			ReservationPax reservationPax;

			if (iterator.hasNext()) {
				reservationPax = (ReservationPax) iterator.next();
				adultInfantId = reservationPax.getPnrPaxId();
			}
		}
		// Infant
		else if (parent != null) {
			adultInfantId = parent.getPnrPaxId();
		}

		return adultInfantId;
	}

	public Integer getAccompaniedSequence() {
		// Normal Adult
		Integer accompaniedSeq = null;

		// Parent
		if (infants != null && infants.size() != 0) {
			Iterator<ReservationPax> iterator = infants.iterator();
			ReservationPax reservationPax;

			if (iterator.hasNext()) {
				reservationPax = (ReservationPax) iterator.next();
				accompaniedSeq = reservationPax.getPaxSequence();
			}
		}
		// Infant
		else if (parent != null) {
			accompaniedSeq = parent.getPaxSequence();
		}

		return accompaniedSeq;
	}

	public boolean hasInfant() {
		if (infants != null && infants.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="ADULT_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.ReservationPax"
	 * @return Returns the infants.
	 */
	public Set<ReservationPax> getInfants() {
		return infants;
	}

	/**
	 * Add an infant
	 * 
	 * @param infant
	 */
	public void addInfants(ReservationPax infant) {
		if (this.getInfants() == null) {
			this.setInfants(new HashSet<ReservationPax>());
		}

		infant.setParent(this);
		this.getInfants().add(infant);
	}

	/**
	 * Add a parent
	 * 
	 * @param parent
	 */
	public void addParent(ReservationPax parent) {
		if (parent.getInfants() == null) {
			parent.setInfants(new HashSet<ReservationPax>());
		}

		this.setParent(parent);
		parent.getInfants().add(this);
	}

	/**
	 * Remove a infant
	 * 
	 * @param infant
	 */
	public void removeInfants(ReservationPax infant) {
		if (this.getInfants() != null) {
			infant.setParent(null);
			this.getInfants().remove(infant);
		}
	}

	/**
	 * Remove parent
	 * 
	 * @param parent
	 */
	public void removeParent(ReservationPax parent) {
		if (this.getParent() != null) {
			this.setParent(null);
			parent.getInfants().remove(parent);
		}
	}

	/**
	 * @return Returns the parent.
	 * @hibernate.many-to-one column="ADULT_ID" cascade="all"
	 *                        class="com.isa.thinair.airreservation.api.model.ReservationPax"
	 */
	public ReservationPax getParent() {
		return parent;
	}

	/**
	 * @return Returns the indexId.
	 */
	public Integer getIndexId() {
		return indexId;
	}

	/**
	 * @param indexId
	 *            The indexId to set.
	 */
	public void setIndexId(Integer indexId) {
		this.indexId = indexId;
	}

	/**
	 * @return Returns the totalAvailableBalance.
	 */
	public BigDecimal getTotalAvailableBalance() {
		return totalAvailableBalance;
	}

	/**
	 * @param totalAvailableBalance
	 *            The totalAvailableBalance to set.
	 */
	public void setTotalAvailableBalance(BigDecimal totalAvailableBalance) {
		this.totalAvailableBalance = totalAvailableBalance;
	}

	/**
	 * @return Returns the payment.
	 */
	public IPayment getPayment() {
		return payment;
	}

	/**
	 * @param payment
	 *            The payment to set.
	 */
	public void setPayment(IPayment payment) {
		this.payment = payment;
	}

	/**
	 * @return Returns the totalSurCharge.
	 * @hibernate.property column = "TOTAL_SURCHARGES"
	 */
	public BigDecimal getTotalSurCharge() {
		return totalSurCharge;
	}

	/**
	 * @param totalSurCharge
	 *            The totalSurCharge to set.
	 */
	public void setTotalSurCharge(BigDecimal totalSurCharge) {
		this.totalSurCharge = totalSurCharge;
	}

	/**
	 * @return Returns the totalTaxCharge.
	 * @hibernate.property column = "TOTAL_TAX"
	 */
	public BigDecimal getTotalTaxCharge() {
		return totalTaxCharge;
	}

	/**
	 * @param totalTaxCharge
	 *            The totalTaxCharge to set.
	 */
	public void setTotalTaxCharge(BigDecimal totalTaxCharge) {
		this.totalTaxCharge = totalTaxCharge;
	}

	/**
	 * @return Returns the totalAdjustmentCharge.
	 * @hibernate.property column = "TOTAL_ADJ"
	 */
	public BigDecimal getTotalAdjustmentCharge() {
		return totalAdjustmentCharge;
	}

	/**
	 * @param totalAdjustmentCharge
	 *            The totalAdjustmentCharge to set.
	 */
	public void setTotalAdjustmentCharge(BigDecimal totalAdjustmentCharge) {
		this.totalAdjustmentCharge = totalAdjustmentCharge;
	}

	/**
	 * @return Returns the totalCancelCharge.
	 * @hibernate.property column = "TOTAL_CNX"
	 */
	public BigDecimal getTotalCancelCharge() {
		return totalCancelCharge;
	}

	/**
	 * @param totalCancelCharge
	 *            The totalCancelCharge to set.
	 */
	public void setTotalCancelCharge(BigDecimal totalCancelCharge) {
		this.totalCancelCharge = totalCancelCharge;
	}

	/**
	 * @return Returns the totalModificationCharge.
	 * @hibernate.property column = "TOTAL_MOD"
	 */
	public BigDecimal getTotalModificationCharge() {
		return totalModificationCharge;
	}

	/**
	 * @param totalModificationCharge
	 *            The totalModificationCharge to set.
	 */
	public void setTotalModificationCharge(BigDecimal totalModificationCharge) {
		this.totalModificationCharge = totalModificationCharge;
	}

	/**
	 * @return Returns the totalPenaltyCharge
	 * @hibernate.property column = "TOTAL_PEN"
	 */
	public BigDecimal getTotalModificationPenalty() {
		return totalModificationPenalty;
	}

	public void setTotalModificationPenalty(BigDecimal totalModificationPenalty) {
		this.totalModificationPenalty = totalModificationPenalty;
	}

	/**
	 * @return Returns the familyID.
	 * @hibernate.property column = "FAMILY_ID"
	 */
	public Integer getFamilyID() {
		return familyID;
	}

	/**
	 * @param familyID
	 *            The familyID to set.
	 */
	public void setFamilyID(Integer familyID) {
		this.familyID = familyID;
	}

	/**
	 * Returns the total charges (Parent + Infant) charges
	 * 
	 * @return
	 */
	public BigDecimal getTotalChargeAmount() {
		// If parent exist
		if (ReservationInternalConstants.PassengerType.ADULT.equals(this.getPaxType()) && this.getAccompaniedPaxId() != null) {
			Iterator<ReservationPax> itReservationPax = this.getInfants().iterator();
			ReservationPax infantPax;
			BigDecimal[] parentAndInfantChgs = ReservationApiUtils.getChargeTypeAmounts();

			if (itReservationPax.hasNext()) {
				infantPax = (ReservationPax) itReservationPax.next();
				parentAndInfantChgs = ReservationApiUtils.getUpdatedTotals(infantPax.getTotalChargeAmounts(),
						this.getTotalChargeAmounts(), true);
			}

			return AccelAeroCalculator.add(parentAndInfantChgs);
		}
		// Adult / Child or an Infant
		else {
			return AccelAeroCalculator.add(getTotalFare(), getTotalTaxCharge(), getTotalSurCharge(), getTotalCancelCharge(),
					getTotalModificationCharge(), getTotalAdjustmentCharge(), getTotalModificationPenalty(), getTotalDiscount()
							.negate());
		}
	}
	
	public BigDecimal getTotalChargeAmountWhenInfantHasPayment() {
		return AccelAeroCalculator.add(getTotalFare(), getTotalTaxCharge(), getTotalSurCharge(), getTotalCancelCharge(),
				getTotalModificationCharge(), getTotalAdjustmentCharge(), getTotalModificationPenalty(), getTotalDiscount()
						.negate());		
	}

	/**
	 * Set the total charges amounts
	 * 
	 * @param totalCharges
	 */
	public void setTotalChargeAmounts(BigDecimal[] totalChargeAmounts) {
		this.setTotalFare(totalChargeAmounts[0]);
		this.setTotalTaxCharge(totalChargeAmounts[1]);
		this.setTotalSurCharge(totalChargeAmounts[2]);
		this.setTotalCancelCharge(totalChargeAmounts[3]);
		this.setTotalModificationCharge(totalChargeAmounts[4]);
		this.setTotalAdjustmentCharge(totalChargeAmounts[5]);
		this.setTotalModificationPenalty(totalChargeAmounts[6]);
		this.setTotalDiscount(totalChargeAmounts[7]);
	}

	/**
	 * Return the total charges amounts
	 * 
	 * @return
	 */
	public BigDecimal[] getTotalChargeAmounts() {
		return new BigDecimal[] { this.getTotalFare(), this.getTotalTaxCharge(), this.getTotalSurCharge(),
				this.getTotalCancelCharge(), this.getTotalModificationCharge(), this.getTotalAdjustmentCharge(),
				this.getTotalModificationPenalty(), this.getTotalDiscount().negate() };
	}

	/**
	 * @hibernate.many-to-one column="PNR" class="com.isa.thinair.airreservation.api.model.Reservation"
	 * @return Returns the reservation.
	 */
	public Reservation getReservation() {
		return reservation;
	}

	/**
	 * @param reservation
	 *            The reservation to set.
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * @param parent
	 *            The parent to set.
	 */
	private void setParent(ReservationPax parent) {
		this.parent = parent;
	}

	/**
	 * @return Returns the localReleaseTimeStamp.
	 */
	public Date getLocalReleaseTimeStamp() {
		return localReleaseTimeStamp;
	}

	/**
	 * @param localReleaseTimeStamp
	 *            The localReleaseTimeStamp to set.
	 */
	public void setLocalReleaseTimeStamp(Date localReleaseTimeStamp) {
		this.localReleaseTimeStamp = localReleaseTimeStamp;
	}

	/**
	 * @return Returns the localStartTimeStamp.
	 */
	public Date getLocalStartTimeStamp() {
		return localStartTimeStamp;
	}

	/**
	 * @param localStartTimeStamp
	 *            The localStartTimeStamp to set.
	 */
	public void setLocalStartTimeStamp(Date localStartTimeStamp) {
		this.localStartTimeStamp = localStartTimeStamp;
	}

	/**
	 * @return Returns the zuluReleaseTimeStamp.
	 * @hibernate.property column = "RELEASE_TIMESTAMP"
	 */
	public Date getZuluReleaseTimeStamp() {
		return zuluReleaseTimeStamp;
	}

	/**
	 * @param zuluReleaseTimeStamp
	 *            The zuluReleaseTimeStamp to set.
	 */
	public void setZuluReleaseTimeStamp(Date zuluReleaseTimeStamp) {
		this.zuluReleaseTimeStamp = zuluReleaseTimeStamp;
	}

	/**
	 * @return Returns the zuluStartTimeStamp.
	 * @hibernate.property column = "START_TIMESTAMP"
	 */
	public Date getZuluStartTimeStamp() {
		return zuluStartTimeStamp;
	}

	/**
	 * @param zuluStartTimeStamp
	 *            The zuluStartTimeStamp to set.
	 */
	public void setZuluStartTimeStamp(Date zuluStartTimeStamp) {
		this.zuluStartTimeStamp = zuluStartTimeStamp;
	}

	/**
	 * Return the release booking time stamp
	 * 
	 * @param dateFormat
	 * @param displayConversion
	 * @return
	 */
	protected String getReleaseTimeStampString(String dateFormat, boolean displayConversion) {
		return ReservationApiUtils.getConvertedDate(this.getZuluReleaseTimeStamp(), this.getLocalReleaseTimeStamp(), dateFormat,
				displayConversion);
	}

	/**
	 * @return Returns the isAccountHolder.
	 */
	public boolean isAccountHolder() {
		return isAccountHolder;
	}

	/**
	 * @param isAccountHolder
	 *            The isAccountHolder to set.
	 */
	public void setAccountHolder(boolean isAccountHolder) {
		this.isAccountHolder = isAccountHolder;
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof ReservationPax) {
			ReservationPax castObject = (ReservationPax) o;
			if (castObject.getPnrPaxId() == null || this.getPnrPaxId() == null) {
				return false;
			} else if (castObject.getPnrPaxId().intValue() == this.getPnrPaxId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @param ondChargesView
	 *            The ondChargesView to set.
	 */
	public void setOndChargesView(Collection<ChargesDetailDTO> ondChargesView) {
		this.ondChargesView = ondChargesView;
	}

	/**
	 * @return Returns the ondChargesView.
	 */
	public Collection<ChargesDetailDTO> getOndChargesView() {
		return ondChargesView;
	}

	/**
	 * @return the paxPaymentTnxView
	 */
	@SuppressWarnings("unchecked")
	public Collection<ReservationTnx> getPaxPaymentTnxView() {
		if (paxPaymentTnxView == null) {
			paxPaymentTnxView = Collections.EMPTY_LIST;
		}
		return Collections.unmodifiableCollection(paxPaymentTnxView);
	}

	/**
	 * @param paxPaymentTnxView
	 *            the paxPaymentTnxView to set
	 */
	public void setPaxPaymentTnxView(Collection<ReservationTnx> paxPaymentTnxView) {
		this.paxPaymentTnxView = paxPaymentTnxView;
	}

	/**
	 * @return the externalPaxPaymentTnxView
	 */
	@SuppressWarnings("unchecked")
	public Collection<ReservationPaxExtTnx> getExternalPaxPaymentTnxView() {
		if (externalPaxPaymentTnxView == null) {
			externalPaxPaymentTnxView = Collections.EMPTY_LIST;
		}
		return Collections.unmodifiableCollection(externalPaxPaymentTnxView);
	}

	/**
	 * @param externalPaxPaymentTnxView
	 *            the externalPaxPaymentTnxView to set
	 */
	public void setExternalPaxPaymentTnxView(Collection<ReservationPaxExtTnx> externalPaxPaymentTnxView) {
		this.externalPaxPaymentTnxView = externalPaxPaymentTnxView;
	}

	/**
	 * @return Returns the upperFirstName.
	 * @hibernate.property column = "UPPER_FIRST_NAME"
	 */
	@SuppressWarnings("unused")
	private String getUpperFirstName() {
		return upperFirstName;
	}

	/**
	 * @param upperFirstName
	 *            The upperFirstName to set.
	 */
	private void setUpperFirstName(String upperFirstName) {
		this.upperFirstName = upperFirstName;
	}

	/**
	 * @return Returns the upperLastName.
	 * @hibernate.property column = "UPPER_LAST_NAME"
	 */
	@SuppressWarnings("unused")
	private String getUpperLastName() {
		return upperLastName;
	}

	/**
	 * @param upperLastName
	 *            The upperLastName to set.
	 */
	private void setUpperLastName(String upperLastName) {
		this.upperLastName = upperLastName;
	}

	/**
	 * @return the paxCategory
	 * @hibernate.property column = "PAX_CATEGORY_CODE"
	 */
	public String getPaxCategory() {
		return (paxCategory == null || paxCategory.equals("")) ? AirPricingCustomConstants.BookingPaxType.ANY : paxCategory;
	}

	/**
	 * @param paxCategory
	 *            the paxCategory to set
	 */
	public void setPaxCategory(String paxCategory) {
		this.paxCategory = paxCategory;
	}

	/**
	 * @return Returns the paxSSR.
	 */
	public Collection<PaxSSRDTO> getPaxSSR() {
		return paxSSR;
	}

	/**
	 * @param paxSSR
	 *            The paxSSR to set.
	 */
	public void setPaxSSR(Collection<PaxSSRDTO> paxSSR) {
		this.paxSSR = paxSSR;
	}

	/**
	 * @return
	 */
	public Collection<EticketTO> geteTickets() {
		return eTickets;
	}

	/**
	 * @param eTickets
	 */
	public void seteTickets(Collection<EticketTO> eTickets) {
		this.eTickets = eTickets;
	}

	/**
	 * 
	 * @return
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="PNR_PAX_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airreservation.api.model.PaxNameTranslations"
	 */
	public Set<PaxNameTranslations> getPaxNameTranslations() {
		return paxNameTranslations;
	}

	public void setPaxNameTranslationList(PaxNameTranslations paxNameTranslation) {
		setPaxNameTranslations(new HashSet<PaxNameTranslations>());
		paxNameTranslation.setReservationPax(this);
		this.paxNameTranslations.add(paxNameTranslation);
	}

	public void setPaxNameTranslations(Set<PaxNameTranslations> paxNameTranslations) {
		this.paxNameTranslations = paxNameTranslations;
	}

	public PaxNameTranslations getPaxNameTranslation() {
		PaxNameTranslations paxNameTranslation = null;
		if (getPaxNameTranslations() != null) {
			Iterator<PaxNameTranslations> i = getPaxNameTranslations().iterator();
			if (i.hasNext()) {
				paxNameTranslation = (PaxNameTranslations) i.next();
			}
		}
		return paxNameTranslation;
	}

	/**
	 * @return
	 * @hibernate.property column = "TOTAL_DISCOUNT"
	 */
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {

		if (totalDiscount.doubleValue() < 0)
			totalDiscount = totalDiscount.negate();

		this.totalDiscount = totalDiscount;
	}

	/**
	 * @return
	 * @hibernate.property column = "AUTO_CANCELLATION_ID"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	@Override
	public int compareTo(ReservationPax obj) {
		if (this.getPaxSequence() != null && obj.getPaxSequence() != null) {
			return this.getPaxSequence().compareTo(obj.getPaxSequence());
		}
		return 0;
	}

	public boolean hasVoucherPayment() {
		boolean hasVoucherPayment = false;
		for (PaymentInfo payInfo : payment.getPayments()) {
			if (payInfo instanceof VoucherPaymentInfo) {
				hasVoucherPayment = true;
			}
		}
		return hasVoucherPayment;
	}

	public BigDecimal getNoShowRefundableAmount() {
		return noShowRefundableAmount;
	}

	public void setNoShowRefundableAmount(BigDecimal noShowRefundableAmount) {
		this.noShowRefundableAmount = noShowRefundableAmount;
	}

	public boolean isNoShowRefundable() {
		return isNoShowRefundable;
	}

	public void setNoShowRefundable(boolean isNoShowRefundable) {
		this.isNoShowRefundable = isNoShowRefundable;
	}

}
