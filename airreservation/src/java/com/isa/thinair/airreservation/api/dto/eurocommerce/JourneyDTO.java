package com.isa.thinair.airreservation.api.dto.eurocommerce;

import java.io.Serializable;

public class JourneyDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8586821236456279267L;
	private SegmentCollectionDTO segmentCollectionDTO;

	public SegmentCollectionDTO getSegmentCollectionDTO() {
		return segmentCollectionDTO;
	}

	public void setSegmentCollectionDTO(SegmentCollectionDTO segmentCollectionDTO) {
		this.segmentCollectionDTO = segmentCollectionDTO;
	}

}
