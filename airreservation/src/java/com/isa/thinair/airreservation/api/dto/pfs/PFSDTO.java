package com.isa.thinair.airreservation.api.dto.pfs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PFSDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String flightNumber;
	private Date flightDate;
	private String departureAirport;
	private String arrivalAirport;
	private String gdsCode;	
	private String carrierCode;
	
	private List<PFSPaxInfo> passengerNameList = new ArrayList<PFSPaxInfo>();
	private Map<String,Integer> cabinCapacityMap = new HashMap<String, Integer>();
	


	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public List<PFSPaxInfo> getPassengerNameList() {
		if (passengerNameList == null) {
			passengerNameList = new ArrayList<PFSPaxInfo>();
		}
		return passengerNameList;
	}

	public void addPassengerName(PFSPaxInfo passengerName) {
		getPassengerNameList().add(passengerName);
	}

	public String getGdsCode() {
		return gdsCode;
	}

	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Map<String, Integer> getCabinCapacityMap() {
		return cabinCapacityMap;
	}

	public void setCabinCapacityMap(Map<String, Integer> cabinCapacityMap) {
		this.cabinCapacityMap = cabinCapacityMap;
	}
}
