package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * This entity class will have auto cancellation expire information
 * 
 * @author rumesh
 * @hibernate.class table = "T_AUTO_CANCELLATION" lazy="false"
 */
public class AutoCancellationInfo extends Persistent implements Serializable {

	public enum AUTO_CNX_TYPE {
		SEG_EXP, INF_EXP, ANCI_EXP
	};

	public static final String PROCESSED = "Y";
	public static final String NOT_PROCESSED = "N";

	private static final long serialVersionUID = 7833507435920551762L;

	private Integer autoCancellationId;

	private Date expireOn;

	private String cancellationType;

	private String processedByScheduler = "N";

	public AutoCancellationInfo() {
		super();
	}

	public AutoCancellationInfo(Date expireOn, String cancellationType) {
		super();
		this.expireOn = expireOn;
		this.cancellationType = cancellationType;
	}

	/**
	 * @return
	 * @hibernate.id column = "AUTO_CANCELLATION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_AUTO_CANCELLATION"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "EXPIRE_ON"
	 */
	public Date getExpireOn() {
		return expireOn;
	}

	public void setExpireOn(Date expireOn) {
		this.expireOn = expireOn;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CANCELLATION_TYPE"
	 */
	public String getCancellationType() {
		return cancellationType;
	}

	public void setCancellationType(String cancellationType) {
		this.cancellationType = cancellationType;
	}

	/**
	 * @return the processedByScheduler
	 * @hibernate.property column = "PROCESSED_BY_SCHEDULER"
	 */
	public String getProcessedByScheduler() {
		return processedByScheduler;
	}

	/**
	 * @param processedByScheduler
	 *            the processedByScheduler to set
	 */
	public void setProcessedByScheduler(String processedByScheduler) {
		this.processedByScheduler = processedByScheduler;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((autoCancellationId == null) ? 0 : autoCancellationId.hashCode());
		result = prime * result + ((expireOn == null) ? 0 : expireOn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		if (!(obj instanceof AutoCancellationInfo)) {
			return false;
		}
		AutoCancellationInfo other = (AutoCancellationInfo) obj;
		if (autoCancellationId == null) {
			if (other.autoCancellationId != null) {

			}
		} else if (!autoCancellationId.equals(other.autoCancellationId)) {
			return false;
		}
		if (expireOn == null) {
			if (other.expireOn != null) {
				return false;
			}
		} else if (!expireOn.equals(other.expireOn)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AutoCancellationInfo [autoCancellationId=" + autoCancellationId + ", expireOn=" + expireOn
				+ ", cancellationType=" + cancellationType + ", processedByScheduler=" + processedByScheduler + "]";
	}

}
