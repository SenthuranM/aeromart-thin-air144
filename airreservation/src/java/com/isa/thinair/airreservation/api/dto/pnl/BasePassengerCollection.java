/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pnl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;

/**
 * @author udithad
 *
 */
public class BasePassengerCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void addDestinationDTOToList(
			List<DestinationFare> destinationsList,
			DestinationFare destinationDTO) {
		destinationsList.add(destinationDTO);
	}

	protected List<DestinationFare> getDestinationFareList(
			Map<String, List<DestinationFare>> destinationFareMap,
			String departureAirportCode) {
		List<DestinationFare> destinationsList = null;
		if (destinationFareMap.containsKey(departureAirportCode)) {
			destinationsList = destinationFareMap.get(departureAirportCode);
		} else {
			if (destinationsList == null) {
				destinationsList = getNewDestinationDTOList();
			}
			destinationFareMap.put(departureAirportCode, destinationsList);
		}
		return destinationsList;
	}

	private List<DestinationFare> getNewDestinationDTOList() {
		return new ArrayList<DestinationFare>();
	}

}
