package com.isa.thinair.airreservation.api.utils;

import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.VoucherPaymentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class GiftVoucherUtils {

	private static final Log log = LogFactory.getLog(GiftVoucherUtils.class);

	private GiftVoucherUtils() {
	}

	// In some cases cc fee is not defined for ANY operation Type
	// So in this method first try to get the cc fee for ANY Operation type
	//If it fails again trying to retrieve CC fee for MAKE Operation TYPE
	public static ExternalChgDTO getCCFeeExternalChargeDTOFORVoucherPurchase() {

		Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> externalCharges = null;
		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colExternalCharges = new ArrayList<>();
		colExternalCharges.add(ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD);
		ExternalChgDTO externalChgDTO = new ExternalChgDTO();

		try {
			externalCharges = AirproxyModuleUtils.getReservationBD()
					.getQuotedExternalCharges(colExternalCharges, null, CommonsConstants.ChargeRateOperationType.ANY);

		} catch (ModuleException me) {
			log.info("Credit card fee not defined for any operation type");
		}

		if (externalCharges == null) {
			try {
				log.info("Retrieving credit card fee for make operation type");
				externalCharges = AirproxyModuleUtils.getReservationBD()
						.getQuotedExternalCharges(colExternalCharges, null, CommonsConstants.ChargeRateOperationType.MAKE_ONLY);
			} catch (ModuleException me2) {
				log.error("Error occurred while trying to get credit card fee for voucher payment");
			}
		}
		if (externalCharges != null) {
			externalChgDTO = externalCharges.get(ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD);
		}
		return externalChgDTO;
	}

	// Reason for passing externalChgDto is to make sure charge dto for the selected pgw is passed here/
	// In service-app payment gateway wise charge is considered
	// But existing xbe is using normal cc fee for voucher payment
	public static void updateCCFeeForGiftVouchers(List<VoucherDTO> vouchers, BigDecimal totalValueForVouchersInBase,
			ExternalChgDTO externalChgDTO) {
		if (externalChgDTO.isRatioValueInPercentage()) {
			for (VoucherDTO voucherDTO : vouchers) {
				VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();
				externalChgDTO
						.calculateAmount(AccelAeroCalculator.getTwoScaledBigDecimalFromString(voucherPaymentDTO.getAmount()));
				// This amount is the credit card transaction amount in BASE currency
				voucherPaymentDTO.setCardTxnFeeLocal(externalChgDTO.getAmount().toString());
			}
		} else {
			externalChgDTO.calculateAmount(totalValueForVouchersInBase);
			BigDecimal totalTnxFee = externalChgDTO.getAmount();
			BigDecimal remainingFee = totalTnxFee;
			int index = 1;

			for (VoucherDTO voucherDTO : vouchers) {
				VoucherPaymentDTO voucherPaymentDTO = voucherDTO.getVoucherPaymentDTO();

				BigDecimal tnxFee = AccelAeroCalculator.divide(AccelAeroCalculator
						.multiply(AccelAeroCalculator.getTwoScaledBigDecimalFromString(voucherDTO.getAmountInBase()),
								totalTnxFee), totalValueForVouchersInBase);

				if (index == vouchers.size()) {
					// This amount is the credit card transaction amount in BASE currency
					voucherPaymentDTO.setCardTxnFeeLocal(remainingFee.toString());
				} else {
					// This amount is the credit card transaction amount in BASE currency
					voucherPaymentDTO.setCardTxnFeeLocal(tnxFee.toString());
				}
				remainingFee = AccelAeroCalculator.subtract(remainingFee, tnxFee);
				index++;
			}
		}
	}

	// This method is used with the assumption that payment gateway wise charges are not considered
	// Current XBe is not supporting this functionality
	public static void updateCCFeeForGiftVouchers(List<VoucherDTO> vouchers) {
		updateCCFeeForGiftVouchers(vouchers, getTotalValueOfVouchersInBase(vouchers),
				getCCFeeExternalChargeDTOFORVoucherPurchase());
	}

	public static BigDecimal getTotalValueOfVouchersInBase(List<VoucherDTO> voucherDTOs) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (VoucherDTO voucherDTO : voucherDTOs) {
			total = AccelAeroCalculator
					.add(total, AccelAeroCalculator.getTwoScaledBigDecimalFromString(voucherDTO.getAmountInBase()));
		}
		return total;
	}

	public static BigDecimal getTotalPaymentAmountForVouchersInBase(List<VoucherDTO> vouchers) throws ModuleException {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (VoucherDTO voucherDTO : vouchers) {
			if (voucherDTO.getVoucherPaymentDTO().getCardTxnFeeLocal() != null) {
				total = AccelAeroCalculator.add(total, AccelAeroCalculator
						.add(AccelAeroCalculator.getTwoScaledBigDecimalFromString(voucherDTO.getVoucherPaymentDTO().getAmount()),
								AccelAeroCalculator.getTwoScaledBigDecimalFromString(
										voucherDTO.getVoucherPaymentDTO().getCardTxnFeeLocal())));
			} else {
				total = AccelAeroCalculator.add(total,
						AccelAeroCalculator.getTwoScaledBigDecimalFromString(voucherDTO.getVoucherPaymentDTO().getAmount()));
			}
		}
		return total;
	}
}
