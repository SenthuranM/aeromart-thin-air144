/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * To hold reservation data transfer information
 * 
 * @author M.Rikaz
 * @since 1.0
 */
public class ReservationBasicsDTO implements Serializable {

	private static final long serialVersionUID = -8105558386371192804L;

	/** Holds the pnr */
	private String pnr;

	private int adultCount;

	private int childCount;

	private int infantCount;

	private int totalPaxCount;

	private int totalAdultCount;

	private String status;

	/** Holds collection of ReservationPaxDetailsDTO */
	private Collection<ReservationPaxDetailsDTO> passengers;

    private int missedOnWardConnection;

    private String connectingSector;

	/**
	 * @return Returns the passengers.
	 */
	public Collection<ReservationPaxDetailsDTO> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            The passengers to set.
	 */
	private void setPassengers(Collection<ReservationPaxDetailsDTO> passengers) {
		this.passengers = passengers;
	}

	/**
	 * Add a passenger
	 * 
	 * @param reservationPaxDetailsDTO
	 */
	public void addPassenger(ReservationPaxDetailsDTO reservationPaxDetailsDTO) {
		if (this.getPassengers() == null) {
			this.setPassengers(new ArrayList<ReservationPaxDetailsDTO>());
		}

		this.getPassengers().add(reservationPaxDetailsDTO);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public int getTotalPaxCount() {
		totalPaxCount = adultCount + childCount + infantCount;

		return totalPaxCount;
	}

	public int getTotalAdultCount() {
		totalAdultCount = adultCount + childCount;

		return totalAdultCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    public int getMissedOnWardConnection() {
        return missedOnWardConnection;
    }

    public void setMissedOnWardConnection(int missedOnWardConnection) {
        this.missedOnWardConnection = missedOnWardConnection;
    }

    public String getConnectingSector() {
        return connectingSector;
    }

    public void setConnectingSector(String connectingSector) {
        this.connectingSector = connectingSector;
    }
}
