/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.utils;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Holds bean utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class BeanUtils {

	/** Holds separator all */
	public static final String SEPARATOR_ALL = "/%";

	/** Holds all separator */
	public static final String ALL_SEPARATOR = "%/";

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @param string
	 * @return
	 */
	public static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	public static String maskSpace(String string) {
		string = nullHandler(string);

		if (string.length() == 0) {
			return null;
		} else {
			return string;
		}
	}

	/**
	 * Find out whether the object passed is a null value
	 * 
	 * @param object
	 * @return
	 */
	public static Boolean isNullHandler(Object object) {
		if (object == null) {
			return new Boolean(true);
		} else {
			return new Boolean(false);
		}
	}

	/**
	 * Generates the dynamic SQL
	 * 
	 * @param map
	 *            argument list with isNullHandler values
	 * @param generateConditions
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static String sqlGenerator(Map map, boolean generateConditions) {
		Iterator iterator = map.keySet().iterator();
		StringBuffer sql = null;
		String key = null;
		Boolean value = null;

		while (iterator.hasNext()) {
			key = (String) iterator.next();
			value = (Boolean) map.get(key);

			if (!value.booleanValue()) {
				if (sql == null) {
					if (generateConditions) {
						sql = new StringBuffer().append(" WHERE " + key);
					} else {
						sql = new StringBuffer().append(key);
					}
				} else {
					if (generateConditions) {
						sql = sql.append(" AND " + key);
					} else {
						sql = sql.append(key);
					}
				}
			}
		}

		return nullHandler(sql);
	}

	/**
	 * Performs user wild card characters to sql specific wild cards
	 * 
	 * @param string
	 * @param replaceOptions
	 *            TODO
	 * @return
	 */
	public static String wildCardParser(String string, List<String> replaceOptions) {
		if (string == null || string.equals("")) {
			return "";
		}
		if (replaceOptions == null || replaceOptions.size() == 0) {
			// Replace the * with %
			string = string.replace('*', '%');
			// Replace the ? with _
			string = string.replace('?', '_');

			string = string.replace('-', '%');
		} else {
			if (replaceOptions.contains("*")) {
				string = string.replace('*', '%');
			}
			if (replaceOptions.contains("?")) {
				string = string.replace('?', '_');
			}
			if (replaceOptions.contains("-")) {
				string = string.replace('-', '%');
			}
		}
		return string;
	}

	/**
	 * Extend the given date by given minutes
	 * 
	 * @param date
	 * @param minutes
	 * @return
	 */
	public static Date extendByMinutes(Date date, int minutes) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(GregorianCalendar.MINUTE, minutes);

		return calendar.getTime();
	}

	/**
	 * Parse Integer
	 * 
	 * @param value
	 * @return
	 */
	public static Integer parseInteger(int value) {
		return new Integer(value);
	}

	/**
	 * Get Last 4 Digits of a credit card number
	 * 
	 * @param cNo
	 * @return
	 */
	public static String getLast4Digits(String cNo) {
		if (cNo == null || cNo.length() == 0) {
			return "";
		} else {
			if (cNo.length() >= 4) {
				return cNo.substring(cNo.length() - 4);
			} else {
				return "";
			}
		}
	}

	/**
	 * Get first 6 Digits of a credit card number
	 * 
	 * @param cNo
	 * @return
	 */
	public static String getFirst6Digits(String cNo) {
		if (cNo == null || cNo.length() == 0) {
			return "";
		} else {
			if (cNo.length() >= 6) {
				return cNo.substring(0, 6);
			} else {
				return "";
			}
		}
	}

	/**
	 * Returns the value changes of the origin and the destination objects
	 * 
	 * @param orgin
	 * @param dest
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("rawtypes")
	public static String compare(Object orgin, Object dest) throws ModuleException {
		StringBuffer changes = new StringBuffer();

		try {
			Map oMap = org.apache.commons.beanutils.BeanUtils.describe(orgin);
			Map dMap = org.apache.commons.beanutils.BeanUtils.describe(dest);

			Iterator iterator = oMap.keySet().iterator();
			String element;
			String oValue;
			String dValue;
			// int i = 1;

			while (iterator.hasNext()) {
				element = (String) iterator.next();
				oValue = nullHandler((String) oMap.get(element));
				dValue = nullHandler((String) dMap.get(element));

				if (!oValue.equals(dValue)) {
					changes = changes
							.append(" " + makeFirstLetterCapital(element) + " changed, " + dValue + " (" + oValue + ") ");
					// i++;
				}
			}
		} catch (IllegalAccessException e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		} catch (InvocationTargetException e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		} catch (NoSuchMethodException e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		return changes.toString();
	}

	/**
	 * Compare two column values and amend the the change log with the change name
	 * 
	 * @param oValue
	 * @param dValue
	 * @param changeName
	 * @return
	 */
	public static String compare(Object oValue, Object dValue, String changeName) {
		String log = "";
		oValue = nullHandler(oValue).toUpperCase();
		dValue = nullHandler(dValue).toUpperCase();
		changeName = nullHandler(changeName);

		if (!oValue.equals(dValue)) {
			if (oValue.equals("")) { // Defect AARESAA-1782 fix
				log = " " + changeName + " changed, " + dValue + " ";
			} else {
				log = " " + changeName + " changed, " + dValue + " (" + oValue + ") ";
			}
		}

		return log;
	}

	public static String logRemovedValue(Object remValue, String changeName) {
		return " " + changeName + " removed, " + remValue + " ";
	}

	public static String logAddedValue(Object newValue, String changeName) {
		return " " + changeName + " removed, " + newValue + " ";
	}

	/**
	 * Construct IN string for HQL or SQL String
	 * 
	 * @param codes
	 * @return 'AAA', 'BBB', 'CCC'
	 */
	@SuppressWarnings("rawtypes")
	public static String constructINString(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? "'" + iter.next().toString() + "'" : inStr + "," + "'" + iter.next().toString() + "'";

		}
		return inStr;
	}

	/**
	 * Construct IN string for HQL or SQL String
	 * 
	 * @param codes
	 * @return 1, 2, 3
	 */
	@SuppressWarnings("rawtypes")
	public static String constructINStringForInts(Collection intCodes) {
		String inStr = "";
		Iterator iter = intCodes.iterator();
		while (iter.hasNext()) {
			inStr = inStr.equals("") ? iter.next().toString() : inStr + "," + iter.next().toString();

		}
		return inStr;
	}

	/**
	 * Create a SQL time stamp
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}

	/**
	 * Parse a date for a date format
	 * 
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String parseDateFormat(Date date, String dateFormat, String locale) {
		String formatedDate = null;

		if (date != null) {
			Format formatter = new SimpleDateFormat(dateFormat, new Locale(locale));
			formatedDate = formatter.format(date);
		}

		return formatedDate;
	}

	/**
	 * Parse a date for a date format
	 * 
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String parseDateFormat(Date date, String dateFormat) {
		String formatedDate = null;

		if (date != null) {
			Format formatter = new SimpleDateFormat(dateFormat);
			formatedDate = formatter.format(date);
		}

		return formatedDate;
	}

	/**
	 * Format the duration to specified format format should be compatible to HH:mm:ss:SS variations
	 * 
	 * @param duration
	 * @param durationFormat
	 * @return
	 */
	public static String parseDurationFormat(long duration, String durationFormat) {
		String arr[] = durationFormat.split(":");
		long hours = duration / (60 * 60 * 1000);
		long minutes = ((duration % (60 * 60 * 1000)) / 60000);
		long seconds = (duration - (hours * 3600 * 1000) + (minutes * 60000)) / 1000;
		long miliseconds = (duration - (hours * 3600 * 1000 + (minutes * 6000) + seconds * 1000));
		String output = "";
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].length() > 0) {
				long value = 0;

				switch (arr[i].charAt(0)) {
				case 'H':
					value = hours;
					break;
				case 'm':
					value = minutes;
					break;
				case 's':
					value = seconds;
					break;
				case 'S':
					value = miliseconds;
					break;
				}
				String fmtString = "%";
				if (arr[i].length() > 1) {
					fmtString += ("0" + arr[i].length() + "d");
				} else {
					fmtString += "d";
				}

				if (i > 0) {
					output += ":";
				}

				output += String.format(fmtString, value);
			}
		}
		return output;
	}

	/**
	 * Parse a number for a number format
	 * 
	 * @param number
	 * @param numberFormat
	 * @return
	 * @throws ModuleException
	 */
	public static String parseNumberFormat(double number, String numberFormat) throws ModuleException {
		String formatedNumber = null;

		// If the number format is null taking the default number format
		if (numberFormat == null) {
			NumberFormat formatter = new DecimalFormat(ReservationApiUtils.getDecimalPoints());
			formatedNumber = formatter.format(number);
		} else {
			NumberFormat formatter = new DecimalFormat(numberFormat);
			formatedNumber = formatter.format(number);
		}

		return formatedNumber;
	}

	/**
	 * Make the first letter capital
	 * 
	 * @param element
	 *            the name that needs to format
	 * @return
	 */
	public static String makeFirstLetterCapital(String element) {
		if (element != null && element.length() > 0) {
			element = element.toLowerCase();
			return element.substring(0, 1).toUpperCase() + element.substring(1);
		} else {
			return element;
		}
	}

	/**
	 * Method to remove extra characters other than alphabets in a string Parameter string should be in capitals
	 * 
	 * @param extraChars
	 * @return
	 */
	public static String removeExtraChars(String extraChars) {
		char[] letterArray = extraChars.toCharArray();
		for (int i = 0; i < letterArray.length; i++) {
			int ascii = (int) letterArray[i];
			if (ascii < 65 | ascii > 90) {
				letterArray[i] = ' ';
			}
		}
		String g = new String(letterArray);
		return removeSpaces(g);
	}

	/**
	 * Method to remove spaces in a string
	 * 
	 * @param s
	 * @return
	 */
	public static String removeSpaces(String s) {
		StringTokenizer st = new StringTokenizer(s, " ", false);
		String t = "";
		while (st.hasMoreElements()) {
			t += st.nextElement();
		}
		return t;
	}

	/**
	 * Returns the first element
	 * 
	 * @param <T>
	 * 
	 * @param collection
	 * @return
	 */
	public static <T> T getFirstElement(Collection<T> collection) {
		Iterator<T> itCollection = collection.iterator();
		T element = null;

		if (itCollection.hasNext()) {
			element = itCollection.next();
		}

		return element;
	}

	/**
	 * Returns the first element from map if isKeyset == true, return the first element from key set, if isKeyset ==
	 * false returns the first element from value set
	 * 
	 * @param <T>
	 * @param map
	 * @param isKeyset
	 * @return
	 */
	public static <T> T getFirstElement(Map<T, T> map, boolean isKeyset) {
		Iterator<T> itCollection = null;

		if (isKeyset) {
			itCollection = map.keySet().iterator();
		} else {
			itCollection = map.values().iterator();
		}

		T element = null;

		if (itCollection.hasNext()) {
			element = itCollection.next();
		}

		return element;
	}

	/**
	 * Returns the last element
	 * 
	 * @param collection
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Object getLastElement(Collection collection) {
		if (collection instanceof ArrayList) {
			return ((ArrayList) collection).get(collection.size() - 1);
		} else {
			Object[] list = collection.toArray();
			return list[list.length - 1];
		}
	}

	/**
	 * Return the element of the given index if it exists
	 * 
	 * @param <T>
	 * @param collection
	 * @return
	 */
	public static <T> T getElementByIndex(Collection<T> collection, int index) {
		T element = null;
		if (collection != null && collection instanceof ArrayList && !collection.isEmpty()) {
			List<T> list = (ArrayList<T>) collection;
			element = list.get(index);
		}
		return element;
	}

	/**
	 * Returns the ideal release date/time This will removes the seconds from the given Time Object. There will be no
	 * rounding considered what so ever
	 * 
	 * @param newReleaseDate
	 * @param currentReleaseDate
	 * @return
	 * @throws ParseException
	 */
	public static long getIdealReleaseDate(Date newReleaseDate, Date currentReleaseDate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm");
		return ((Date) formatter.parse(formatter.format(newReleaseDate))).getTime()
				- ((Date) formatter.parse(formatter.format(currentReleaseDate))).getTime();
	}

	/**
	 * Counts the no of decimal points
	 * 
	 * @param boundayValue
	 * @return
	 */
	public static int countNoOfDecimals(BigDecimal boundayValue) {
		int noOfDecimals = 0;

		if (boundayValue != null) {
			String strBoundaryValue = boundayValue.toString();

			if (!strBoundaryValue.isEmpty() && strBoundaryValue.indexOf(".") != -1) {
				String value = nullHandler(strBoundaryValue.substring(strBoundaryValue.indexOf(".") + 1,
						strBoundaryValue.length()));
				noOfDecimals = value.length();
			}
		}

		return noOfDecimals;
	}

	// TODO - Provision for customizing output based on RDBMS
	// A solution pending implemenation
	public static String getTelephoneNoForSQL(String countryCode, String areaCode, String localPhoneNumber) {
		String phoneNumber = null;
		if (localPhoneNumber != null) {
			if (areaCode == null || areaCode.equals("")) {
				// phoneNumber = getFixedLengthString('?', (20 - localPhoneNumber.length())) + localPhoneNumber;
				phoneNumber = "%" + localPhoneNumber;
			} else if (countryCode == null || countryCode.equals("")) {
				// phoneNumber = getCharacterRemovedString(areaCode, '0') + getFixedLengthString('?', 2) +
				// localPhoneNumber;
				// phoneNumber = getFixedLengthString('?', (20 - phoneNumber.length())) + phoneNumber;
				phoneNumber = "%" + areaCode + "%" + localPhoneNumber;
			} else {
				// phoneNumber = countryCode + getFixedLengthString('?', 2) + getCharacterRemovedString(areaCode, '0')
				// + getFixedLengthString('?', 2) + localPhoneNumber;
				// phoneNumber = BeanUtils.getFixedLengthString('?', (20 - phoneNumber.length())) + phoneNumber;
				phoneNumber = "%" + countryCode + "%" + areaCode + "%" + localPhoneNumber;
			}
		}
		return phoneNumber;
	}

	public static String constructSQLPlaceHolderString(int count) {
		StringBuffer sb = null;
		if (count > 1) {
			sb = new StringBuffer();
			for (int i = 0; i < count; ++i) {
				if (i == (count - 1)) {
					sb.append("?");
				} else {
					sb.append("?,");
				}
			}
			return sb.toString();
		} else if (count == 1) {
			return "?";
		} else {
			return null;
		}
	}

	public static boolean isNull(Object obj) {
		if (obj == null || "".equals(obj.toString())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Split a collection to a given size. It's really useful for SQL IN clause generation with 1000 limit in Oracle
	 * 
	 * @param colObjects
	 * @param size
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Collection[] split(Collection colObjects, int size) {
		if (colObjects == null || colObjects.size() == 0) {
			return new Collection[] { new ArrayList() };
		} else if (colObjects.size() <= size) {
			return new Collection[] { colObjects };
		} else {
			Collection objectHolder = new ArrayList();
			Collection<Collection> colHolder = new ArrayList<Collection>();
			colHolder.add(objectHolder);

			int i = 1;
			int j = 1;
			for (Object object : colObjects) {
				objectHolder.add(object);

				if (i == size) {
					if (j != colObjects.size()) {
						objectHolder = new ArrayList();
						colHolder.add(objectHolder);
						i = 0;
					}
				}
				i++;
				j++;
			}

			Collection[] elements = new Collection[colHolder.size()];

			int k = 0;
			for (Collection collection : colHolder) {
				elements[k] = collection;
				k++;
			}

			return elements;
		}
	}

	public static BigDecimal nullToZero(Object object) {
		BigDecimal amount = BigDecimal.ZERO;
		if (object != null && object.toString().trim().length() != 0) {
			amount = new BigDecimal(object.toString().trim());
		}
		return amount;
	}

	public static <T> List<T> toList(Set<T> set) {
		List<T> lstElements = new ArrayList<T>();
		for (T t : set) {
			lstElements.add(t);
		}

		return lstElements;

	}

	/**
	 * Get first given number of characters of a given pax name if it exceed its' max length
	 *
	 * @param name
	 * @param maxlength
	 * @return
	 */
	public static String getFirst20Characters(String name, int maxlength) {
		String userName = name;
		if (userName != null && userName.length() > maxlength) {
			userName = userName.substring(0, maxlength);
		}
		return makeFirstLetterCapital(userName);
	}

}
