package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Sudheera
 * 
 */
public class PaxSegmentSSRDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6441212704077570437L;

	private Integer paxSegmentSSRId;

	private Integer segmentSequence;

	private Integer sSRId;

	private String sSRText;

	private Integer contextId;

	private BigDecimal chargeAmount;

	private String applyOn;

	private String externalReference;

	private String ssrCode;

	private String description;

	private String airportCode;

	private Integer autoCancellationId;

	private String transferDate;

	private String transferAddress;

	private String transferContactNo;
	
	private String transferType;
	
	private String segmentCode;

	public Integer getPaxSegmentSSRId() {
		return this.paxSegmentSSRId;
	}

	public void setPaxSegmentSSRId(Integer paxSegmentSSRId) {
		this.paxSegmentSSRId = paxSegmentSSRId;
	}

	public Integer getSegmentSequence() {
		return this.segmentSequence;
	}

	public void setSegmentSequence(Integer segmentSequence) {
		this.segmentSequence = segmentSequence;
	}

	public Integer getSSRId() {
		return sSRId;
	}

	public void setSSRId(Integer ssrId) {
		sSRId = ssrId;
	}

	public String getSSRText() {
		return sSRText;
	}

	public void setSSRText(String ssrText) {
		sSRText = ssrText;
	}

	public Integer getContextId() {
		return this.contextId;
	}

	public void setContextId(Integer contextId) {
		this.contextId = contextId;
	}

	public BigDecimal getChargeAmount() {
		return this.chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getApplyOn() {
		return this.applyOn;
	}

	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getTransferAddress() {
		return transferAddress;
	}

	public void setTransferAddress(String transferAddress) {
		this.transferAddress = transferAddress;
	}

	public String getTransferContactNo() {
		return transferContactNo;
	}

	public void setTransferContactNo(String transferContactNo) {
		this.transferContactNo = transferContactNo;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
}
