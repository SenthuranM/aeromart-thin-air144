package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of reservation segment Notification Events
 * 
 * @author Naeem Akhtar
 * @since 1.0
 * @hibernate.class table = "T_PNR_SEG_NOTIFICATION_EVENT"
 */
public class ReservationSegmentNotificationEvent extends Persistent implements Serializable {

	private static final long serialVersionUID = 3266068992285167407L;

	private Integer pnrSegmentMsgEventId;
	private Integer pnrSegmentId;
	private String notificationType;
	private String notifyStatus;
	private String failureReason;
	private Date timeStamp;

	/**
	 * @return Returns the flightMsgEventId
	 * @hibernate.id column = "PNR_SEG_NOTIFICATION_EVENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNR_SEG_NOTIFICATION_EVENT"
	 * @return
	 */
	public Integer getPnrSegmentMsgEventId() {
		return pnrSegmentMsgEventId;
	}

	/**
	 * @param flightMsgEventId
	 *            the flightMsgEventId to set
	 */
	public void setPnrSegmentMsgEventId(Integer pnrSegmentMsgEventId) {
		this.pnrSegmentMsgEventId = pnrSegmentMsgEventId;
	}

	/**
	 * @hibernate.property column = "PNR_SEG_ID"
	 * @return
	 */
	public Integer getPnrSegmentId() {
		return pnrSegmentId;
	}

	/**
	 * @param pnrSegmentId
	 *            the pnrSegmentId to set
	 */
	public void setPnrSegmentId(Integer pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	/**
	 * @hibernate.property column = "NOTIFICATION_TYPE"
	 * @return
	 */
	public String getNotificationType() {
		return notificationType;
	}

	/**
	 * @param notificationType
	 *            the notificationType to set
	 */
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @hibernate.property column = "NOTIFY_STATUS"
	 * @return
	 */
	public String getNotifyStatus() {
		return notifyStatus;
	}

	/**
	 * @param notifyStatus
	 *            the notifyStatus to set
	 */
	public void setNotifyStatus(String notifyStatus) {
		this.notifyStatus = notifyStatus;
	}

	/**
	 * @hibernate.property column = "FAILURE_REASON"
	 * @return
	 */
	public String getFailureReason() {
		return failureReason;
	}

	/**
	 * @param failureReason
	 *            the failureReason to set
	 */
	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}

	/**
	 * @hibernate.property column = "TIMESTAMP"
	 * @return
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

}
