package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;

public class ItineraryBaggageTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String baggageName;

	public String getBaggageName() {
		return baggageName;
	}

	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}
}
