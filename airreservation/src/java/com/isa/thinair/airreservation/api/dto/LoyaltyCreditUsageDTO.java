package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To hold Loyalty credit usage data transfer information
 * 
 * @author harshaa
 */
public class LoyaltyCreditUsageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer pnr;

	private String originatorPnr;

	private BigDecimal paidAmount;

	private Date paidDate;

	public Integer getPnr() {
		return pnr;
	}

	public void setPnr(Integer pnr) {
		this.pnr = pnr;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public String getOriginatorPnr() {
		return originatorPnr;
	}

	public void setOriginatorPnr(String originatorPnr) {
		this.originatorPnr = originatorPnr;
	}

}
