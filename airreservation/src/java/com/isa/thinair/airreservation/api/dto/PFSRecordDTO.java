/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

/**
 * Holds Passenger final sales record specific data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PFSRecordDTO {
	/** Holds the category e.g. NOSHO, GOSHO ..etc */
	private String category;

	/** Holds a collection of RecordDTO */
	private Collection<RecordDTO> records;

	/** Holds the number of passengers */
	private int numberOfPassengers;

	/** Holds the cabin class code */
	private String cabinClassCode;

	/**
	 * Add Records DTO
	 * 
	 * @param recordDTO
	 */
	public void addRecordDTO(RecordDTO recordDTO) {
		// If the first name is empty
		// This can be for a Goshore or a Noshore reservation
		if (recordDTO.getFirstNameWithTitle() == null) {
			this.addRecords(recordDTO);
		} else {
			StringTokenizer stringTokenizer = new StringTokenizer(recordDTO.getFirstNameWithTitle(), " ", false);

			if (stringTokenizer.countTokens() != 1) {
				int i = 1;
				while (stringTokenizer.hasMoreElements()) {
					String firstName = (String) stringTokenizer.nextElement();

					if (i == 1) {
						recordDTO.setFirstNameWithTitle(firstName);
						this.addRecords(recordDTO);
					} else {
						RecordDTO copyRecordDTO = (RecordDTO) recordDTO.clone();
						copyRecordDTO.setFirstNameWithTitle(firstName);
						this.addRecords(copyRecordDTO);
					}

					i++;
				}
			} else {
				this.addRecords(recordDTO);
			}
		}
	}

	/**
	 * Add Records
	 * 
	 * @param recordDTO
	 */
	private void addRecords(RecordDTO recordDTO) {
		if (this.getRecords() == null) {
			this.setRecords(new ArrayList<RecordDTO>());
		}

		this.getRecords().add(recordDTO);
	}

	/**
	 * Set the code
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		code = code.replaceAll(" ", "");
		code = code.substring(0, code.indexOf("\n"));

		StringBuffer paxCount = new StringBuffer("");

		for (int i = 0; i < code.length(); i++) {
			try {
				paxCount.append(String.valueOf(Integer.parseInt(String.valueOf(code.charAt(i)))));

			} catch (NumberFormatException e) {
				// Setting the cabin class code
				this.setCabinClassCode(code.substring(paxCount.length()));
				// Setting the number of passengers
				this.setNumberOfPassengers(Integer.parseInt(String.valueOf(paxCount)));
				break;
			}
		}
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	private void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the numberOfPassengers.
	 */
	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	/**
	 * @param numberOfPassengers
	 *            The numberOfPassengers to set.
	 */
	private void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

	/**
	 * @return Returns the category.
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            The category to set.
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return Returns the records.
	 */
	public Collection<RecordDTO> getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            The records to set.
	 */
	public void setRecords(Collection<RecordDTO> records) {
		this.records = records;
	}

}
