package com.isa.thinair.airreservation.api.model.pnrgov;

public interface EDIBuilder {

	public MessageSegment build();
}
