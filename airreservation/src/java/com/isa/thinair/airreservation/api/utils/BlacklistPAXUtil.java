package com.isa.thinair.airreservation.api.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistPAXRule;
import com.isa.thinair.airreservation.api.model.BlacklistReservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;

public class BlacklistPAXUtil {

	public static final String DATA_ELE_FIRST_NAME = "First Name";
	public static final String DATA_ELE_LAST_NAME = "Last Name";
	public static final String DATA_ELE_FULL_NAME = "Full Name";
	public static final String DATA_ELE_DOB = "Date Of Birth";
	public static final String DATA_ELE_PASSPORT = "Passport No";
	public static final String DATA_ELE_NATIONALITY = "Nationality";
	
	public static final String PAX_NAME_SEPARATOR =" ";

	public static BlacklistPAX blacklistPAXTOToBlacklistPAX(BlacklistPAXCriteriaTO blacklistPAXCriteriaTO)
			throws IllegalArgumentException {

		if (blacklistPAXCriteriaTO != null) {
			BlacklistPAX blacklistPAX = new BlacklistPAX();
			blacklistPAX.setBlacklistPAXCriteriaID(blacklistPAXCriteriaTO.getBlacklistPAXCriteriaID());
			blacklistPAX.setBlacklistType(blacklistPAXCriteriaTO.getBlacklistType());
			blacklistPAX.setDateOfBirth(blacklistPAXCriteriaTO.getDateOfBirth());
			blacklistPAX.setEffectiveFrom(blacklistPAXCriteriaTO.getEffectiveFrom());
			blacklistPAX.setEffectiveTo(blacklistPAXCriteriaTO.getEffectiveTo());
			
			blacklistPAX.setFullName(formatFullname(blacklistPAXCriteriaTO.getPaxFullName()));
			
			blacklistPAX.setStatus(blacklistPAXCriteriaTO.getPaxStatus());
			if(!StringUtil.isNullOrEmpty(blacklistPAXCriteriaTO.getNationality())){
				blacklistPAX.setNationality(new Integer(blacklistPAXCriteriaTO.getNationality()));
			}
			blacklistPAX.setPassportNo(blacklistPAXCriteriaTO.getPassportNo());
			blacklistPAX.setRemarks(blacklistPAXCriteriaTO.getRemarks());
			blacklistPAX.setRemovedReason(blacklistPAXCriteriaTO.getRemovedReason());
			blacklistPAX.setValidUntil(blacklistPAXCriteriaTO.getValidUntil());
			blacklistPAX.setVersion(blacklistPAXCriteriaTO.getVersion());

			return blacklistPAX;
		}
		return null;
	}
	
	public static String formatFullname(String name){		
		if(!StringUtil.isNullOrEmpty(name)){
			String formattedName= name.trim().replaceAll(" +", BlacklistPAXUtil.PAX_NAME_SEPARATOR);			
			return formattedName;
		}
		return  null;
	}
	
	public static BlacklistPAXCriteriaTO blacklistPAXToBlacklistPAXTO(BlacklistPAX blacklistPAX) throws ModuleException {

		if (blacklistPAX != null) {
			BlacklistPAXCriteriaTO blacklistPAXCriteriaTO = new BlacklistPAXCriteriaTO();
			blacklistPAXCriteriaTO.setPaxFullName(blacklistPAX.getFullName());
			blacklistPAXCriteriaTO.setPassportNo(blacklistPAX.getPassportNo());
			blacklistPAXCriteriaTO.setBlacklistType(blacklistPAX.getBlacklistType());
			blacklistPAXCriteriaTO.setBlacklistPAXCriteriaID(blacklistPAX.getBlacklistPAXCriteriaID());
			if (blacklistPAX.getNationality() != null) {
				Nationality nationality = ReservationModuleUtils.getCommonMasterBD()
						.getNationality(blacklistPAX.getNationality());
				if (nationality != null)
					blacklistPAXCriteriaTO.setNationality(nationality.getDescription());
			}
			blacklistPAXCriteriaTO.setPaxStatus(blacklistPAX.getStatus());
			blacklistPAXCriteriaTO.setDateOfBirth(blacklistPAX.getDateOfBirth());
			blacklistPAXCriteriaTO.setEffectiveFrom(blacklistPAX.getEffectiveFrom());
			blacklistPAXCriteriaTO.setEffectiveTo(blacklistPAX.getEffectiveTo());
			blacklistPAXCriteriaTO.setRemarks(blacklistPAX.getRemarks());
			blacklistPAXCriteriaTO.setRemovedReason(blacklistPAX.getRemovedReason());
			blacklistPAXCriteriaTO.setValidUntil(blacklistPAX.getValidUntil());
			blacklistPAXCriteriaTO.setVersion(blacklistPAX.getVersion());
			return blacklistPAXCriteriaTO;
		}

		return null;

	}
	
	public static BlacklistPAX merge(BlacklistPAX instsOne, BlacklistPAX instsTwo){
		instsOne.setBlacklistPAXCriteriaID(instsTwo.getBlacklistPAXCriteriaID());
		instsOne.setBlacklistType(instsTwo.getBlacklistType());
		instsOne.setDateOfBirth(instsTwo.getDateOfBirth());
		instsOne.setEffectiveFrom(instsTwo.getEffectiveFrom());
		instsOne.setEffectiveTo(instsTwo.getEffectiveTo());
		instsOne.setFullName(instsTwo.getFullName());
		instsOne.setStatus(instsTwo.getStatus());
		instsOne.setNationality(new Integer(instsTwo.getNationality()));
		instsOne.setPassportNo(instsTwo.getPassportNo());
		instsOne.setRemarks(instsTwo.getRemarks());
		instsOne.setRemovedReason(instsTwo.getRemovedReason());
		instsOne.setValidUntil(instsTwo.getValidUntil());
		instsOne.setVersion(instsTwo.getVersion());
		
		return instsOne;
	}

	public static BlacklistReservation blacklistPaxResTOToBlacklistReservation(BlacklisPaxReservationTO blacklistResTOSave) {
		BlacklistReservation blacklistResSave = new BlacklistReservation();
		blacklistResSave.setBlacklistPaxId(blacklistResTOSave.getBlacklistPaxId());
		blacklistResSave.setBlacklistReservationId(blacklistResTOSave.getBlacklistReservationId());
		blacklistResSave.setPnrPaxId(blacklistResTOSave.getPnrPaxId());
		blacklistResSave.setReasonToAllow(blacklistResTOSave.getReasonToAllow());
		blacklistResSave.setIsActioned(blacklistResTOSave.getIsActioned());
		blacklistResSave.setVersion(blacklistResTOSave.getVersion());
		blacklistResSave.setReasonForWhitelisted(blacklistResTOSave.getReasonForWhitelisted());
		return blacklistResSave;
	}
	
	public static BlacklistPAXRuleTO blacklistPAXRuleToBlacklistPAXRuleTO(BlacklistPAXRule blacklistPAXRule)
			throws ModuleException {

		if (blacklistPAXRule != null) {
			List<String> dataElementList = new ArrayList<String>();
			BlacklistPAXRuleTO blacklistPAXRuleTO = new BlacklistPAXRuleTO();
			blacklistPAXRuleTO.setLabel(blacklistPAXRule.getLabel());
			blacklistPAXRuleTO.setStatus(blacklistPAXRule.getStatus());
						
			blacklistPAXRuleTO.setFullNameChecked(blacklistPAXRule.getPaxFullNameChecked());
			if ("Y".equals(blacklistPAXRule.getPaxFullNameChecked())) {
				dataElementList.add(DATA_ELE_FULL_NAME);
			}						
			blacklistPAXRuleTO.setNationalityChecked(blacklistPAXRule.getNationalityChecked());
			if (blacklistPAXRule.getNationalityChecked().equals("Y")) {
				dataElementList.add(DATA_ELE_NATIONALITY);
			}
			blacklistPAXRuleTO.setPassportNoChecked(blacklistPAXRule.getPassportNoChecked());
			if (blacklistPAXRule.getPassportNoChecked().equals("Y")) {
				dataElementList.add(DATA_ELE_PASSPORT);
			}
			blacklistPAXRuleTO.setDateOfBirthChecked(blacklistPAXRule.getDateOfBirthChecked());
			if (blacklistPAXRule.getDateOfBirthChecked().equals("Y")) {
				dataElementList.add(DATA_ELE_DOB);
			}

			blacklistPAXRuleTO.setVersion(blacklistPAXRule.getVersion());
			blacklistPAXRuleTO.setRuleId(blacklistPAXRule.getRuleId());

			StringBuffer sb = new StringBuffer();
			for (String str : dataElementList) {
				sb.append(str + ",");
			}

			String rules = sb.toString();
			if (rules.length() > 0 && rules.charAt(rules.length() - 1) == ',') {
				rules = rules.substring(0, rules.length() - 1);
			}

			blacklistPAXRuleTO.setStrRules(rules);

			return blacklistPAXRuleTO;
		}

		return null;

	}

	public static BlacklistPAXRule blacklistPAXRuleTOToBlacklistPAXRule(BlacklistPAXRuleTO blacklistPAXRuleTO)
			throws IllegalArgumentException {

		if (blacklistPAXRuleTO != null) {
			BlacklistPAXRule blacklistPAXRule = new BlacklistPAXRule();
			blacklistPAXRule.setRuleId(blacklistPAXRuleTO.getRuleId());
			blacklistPAXRule.setLabel(blacklistPAXRuleTO.getLabel());
			String selectedDataElements = blacklistPAXRuleTO.getStrRules();
			String[] sde = selectedDataElements.split(",");
			List<String> sdeList = new ArrayList(Arrays.asList(sde));
			
			if (sdeList.contains(DATA_ELE_FULL_NAME)) {
				blacklistPAXRule.setPaxFullNameChecked("Y");
			} else {
				blacklistPAXRule.setPaxFullNameChecked("N");
			}
			if (sdeList.contains(DATA_ELE_DOB)) {
				blacklistPAXRule.setDateOfBirthChecked("Y");
			} else {
				blacklistPAXRule.setDateOfBirthChecked("N");
			}
			if (sdeList.contains(DATA_ELE_NATIONALITY)) {
				blacklistPAXRule.setNationalityChecked("Y");
			} else {
				blacklistPAXRule.setNationalityChecked("N");
			}
			if (sdeList.contains(DATA_ELE_PASSPORT)) {
				blacklistPAXRule.setPassportNoChecked("Y");
			} else {
				blacklistPAXRule.setPassportNoChecked("N");
			}
			blacklistPAXRule.setStatus(blacklistPAXRuleTO.getStatus());
			blacklistPAXRule.setVersion(blacklistPAXRuleTO.getVersion());

			return blacklistPAXRule;
		}
		return null;
	}

	public static List<String> getDataElementsList(BlacklistPAXRule blacklistPAXRule) {
		List<String> dls = new ArrayList<String>();
		if (blacklistPAXRule != null) {
			
			if(blacklistPAXRule.getPaxFullNameChecked().equals("Y")){
				dls.add(DATA_ELE_FULL_NAME);
			}
			if (blacklistPAXRule.getDateOfBirthChecked().equals("Y")) {
				dls.add(DATA_ELE_DOB);
			}
			if (blacklistPAXRule.getNationalityChecked().equals("Y")) {
				dls.add(DATA_ELE_NATIONALITY);
			}
			if (blacklistPAXRule.getPassportNoChecked().equals("Y")) {
				dls.add(DATA_ELE_PASSPORT);
			}

			return dls;
		}

		return null;

	}
	
	public static List<BlacklisPaxReservationTO> convertReservationPaxToBlacklisPaxReservationTO(List<ReservationPax> paxList) {
		List<BlacklisPaxReservationTO> paxTOList = new ArrayList<BlacklisPaxReservationTO>();
		for (ReservationPax pax : paxList) {
			BlacklisPaxReservationTO paxTO = new BlacklisPaxReservationTO();

			paxTO.setFullName(pax.getFirstName()+BlacklistPAXUtil.PAX_NAME_SEPARATOR+pax.getLastName());
			
			if (pax.getDateOfBirth() != null)
				paxTO.setDateOfBirth(pax.getDateOfBirth());
			paxTO.setNationalityCode(pax.getNationalityCode());
			paxTO.setPassportNo(pax.getPaxAdditionalInfo().getPassportNo());
			paxTO.setPnrPaxId(pax.getPnrPaxId());

			paxTOList.add(paxTO);
		}

		return paxTOList;
	}
	
	public static BlacklistPAX BlacklisPaxReservationTOToBlacklistPAX (BlacklisPaxReservationTO blacklisPaxReservationTO){
		if (blacklisPaxReservationTO != null) {
			BlacklistPAX blacklistPAX = new BlacklistPAX();
			
			blacklistPAX.setFullName(blacklisPaxReservationTO.getFullName());
			blacklistPAX.setDateOfBirth(blacklisPaxReservationTO.getDateOfBirth());
			blacklistPAX.setNationality(blacklisPaxReservationTO.getNationalityCode());
			blacklistPAX.setPassportNo(blacklisPaxReservationTO.getPassportNo());
			
			return blacklistPAX;
		}
		
		return null;
	}
	
	public static BlacklisPaxReservationTO BlacklistPAXToBlacklisPaxReservationTO(BlacklistPAX blacklistPax) throws ModuleException{
		if (blacklistPax != null) {
			BlacklisPaxReservationTO blacklisPaxReservationTO = new BlacklisPaxReservationTO();
	
			blacklisPaxReservationTO.setFullName(blacklistPax.getFullName());
			blacklisPaxReservationTO.setDateOfBirth(blacklistPax.getDateOfBirth());
			if (blacklistPax.getNationality() != null) {
				Nationality nationality = ReservationModuleUtils.getCommonMasterBD()
						.getNationality(blacklistPax.getNationality());
				if (nationality != null)
					blacklisPaxReservationTO.setNationality(nationality.getDescription());
			}
			blacklisPaxReservationTO.setPassportNo(blacklistPax.getPassportNo());
			
			return blacklisPaxReservationTO;
		}
		
		return null;
	}

	public static BlacklistPAX getMatchingBlacklistPax(List<BlacklistPAX> matchingblacklistPAXs,BlacklisPaxReservationTO paxReservationTO) {
		for(BlacklistPAX bpax : matchingblacklistPAXs){
				
			
			//Don't  Remove below comment 			
			/*if (!StringUtil.isNullOrEmpty(paxReservationTO.getPassportNo()) && paxReservationTO.getDateOfBirth() != null) {
				if( 	paxReservationTO.getfName().equalsIgnoreCase(bpax.getfName()) &&
						paxReservationTO.getlName().equalsIgnoreCase(bpax.getlName()) &&
						paxReservationTO.getNationalityCode()==bpax.getNationality()  &&
						paxReservationTO.getDateOfBirth().equals(bpax.getDateOfBirth()) &&
						paxReservationTO.getPassportNo().equalsIgnoreCase(bpax.getPassportNo())	){
					return bpax;					
				}
				
			}else if (!StringUtil.isNullOrEmpty(paxReservationTO.getPassportNo())) {
				if( 	paxReservationTO.getfName().equalsIgnoreCase(bpax.getfName()) &&
						paxReservationTO.getlName().equalsIgnoreCase(bpax.getlName()) &&
						paxReservationTO.getNationalityCode()==bpax.getNationality()  &&						
					   (paxReservationTO.getPassportNo().equalsIgnoreCase(bpax.getPassportNo()) || StringUtil.isNullOrEmpty(bpax.getPassportNo()))){
					return bpax;
				}
			}else if (paxReservationTO.getDateOfBirth() != null) {
				if( 	paxReservationTO.getfName().equalsIgnoreCase(bpax.getfName()) &&
						paxReservationTO.getlName().equalsIgnoreCase(bpax.getlName()) &&
						paxReservationTO.getNationalityCode()==bpax.getNationality()  &&						
						(paxReservationTO.getDateOfBirth().equals(bpax.getDateOfBirth())|| bpax.getDateOfBirth()==null)){
					return bpax;
				}
			}else{
				if( 	paxReservationTO.getfName().equalsIgnoreCase(bpax.getfName()) &&
						paxReservationTO.getlName().equalsIgnoreCase(bpax.getlName()) &&
						paxReservationTO.getNationalityCode()==bpax.getNationality() ){
					return bpax;
				}
			}*/
		}
		return null;
	}
	
	public static List<BlacklisPaxReservationTO> createPaxTOList(List<LCCClientReservationPax> paxList) {
		List<BlacklisPaxReservationTO> paxTOList = new ArrayList<BlacklisPaxReservationTO>();

		for (LCCClientReservationPax pax : paxList) {
			if (!pax.getFirstName().equals("T B A") && !pax.getLastName().equals("T B A")) {
				BlacklisPaxReservationTO paxTO = new BlacklisPaxReservationTO();
		
				paxTO.setFullName(pax.getFirstName()+BlacklistPAXUtil.PAX_NAME_SEPARATOR+pax.getLastName());
				if (pax.getNationalityCode() != null) {
					paxTO.setNationalityCode(pax.getNationalityCode());
				}
				if (pax.getDateOfBirth() != null) {
					paxTO.setDateOfBirth(pax.getDateOfBirth());
				}
				if (pax.getLccClientAdditionPax() != null && pax.getLccClientAdditionPax().getPassportNo() != null) {
					paxTO.setPassportNo(pax.getLccClientAdditionPax().getPassportNo());
				}

				paxTOList.add(paxTO);
			}
		}
		return paxTOList;
	}
}
