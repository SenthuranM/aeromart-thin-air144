package com.isa.thinair.airreservation.api.dto.eticket;

import java.io.Serializable;
import java.util.Date;

/**
 * To be used in the E ticket update web service
 */
public class ETicketUpdateResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean isNOREC;
	private Integer segmentId;
	private Date departureDate;
	private String flightNo;
	private String usabilityStatus;

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public boolean isNOREC() {
		return isNOREC;
	}

	public void setNOREC(boolean isNOREC) {
		this.isNOREC = isNOREC;
	}

	public Integer getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getUsabilityStatus() {
		return usabilityStatus;
	}

	public void setUsabilityStatus(String usabilityStatus) {
		this.usabilityStatus = usabilityStatus;
	}

}