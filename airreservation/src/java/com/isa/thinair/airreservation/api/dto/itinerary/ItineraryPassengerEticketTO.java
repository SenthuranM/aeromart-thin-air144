package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author eric
 * 
 */
public class ItineraryPassengerEticketTO implements Comparable<ItineraryPassengerEticketTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String passengerDisplayName;
	private String passengerType;
	private Integer paxSequnce;

	private List<ItineraryEticketTO> eTickets;

	public String getPassengerDisplayName() {
		return passengerDisplayName;
	}

	public void setPassengerDisplayName(String passengerDisplayName) {
		this.passengerDisplayName = passengerDisplayName;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public List<ItineraryEticketTO> geteTickets() {
		if (this.eTickets == null) {
			this.eTickets = new ArrayList<ItineraryEticketTO>();
		}
		return eTickets;
	}

	public void seteTickets(List<ItineraryEticketTO> eTickets) {
		this.eTickets = eTickets;
	}

	public int getPaxSequnce() {
		return paxSequnce;
	}

	public void setPaxSequnce(int paxSequnce) {
		this.paxSequnce = paxSequnce;
	}

	@Override
	public int compareTo(ItineraryPassengerEticketTO o) {
		return this.paxSequnce.compareTo(o.getPaxSequnce());
	}

}
