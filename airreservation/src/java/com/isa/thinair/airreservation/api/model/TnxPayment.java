/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Transaction Payment
 * 
 * @author Lasantha Pambagoda
 */
public class TnxPayment implements ITnxPayment {

	private static final long serialVersionUID = 1L;

	private int paymentType;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private PayCurrencyDTO payCurrencyDTO;

	private PaymentReferenceTO paymentReferenceTO;

	/** Holds the payment carrier which the payment is made */
	private String paymentCarrier;
	
	/**
	 * Holds the lcc unique tnx id linking carrier wise payment payments and external payments
	 */
	private String lccUniqueTnxId;
	
	/**
	 * Holds original payment transaction, when the payment type is a refund
	 */
	private Integer paymentTnxId;

	/**
	 * @return the paymentReferanceTO
	 */
	public PaymentReferenceTO getPaymentReferanceTO() {
		return paymentReferenceTO;
	}

	/**
	 * @param paymentReferanceTO
	 *            the paymentReferanceTO to set
	 */
	public void setPaymentReferenceTO(PaymentReferenceTO paymentReferenceTO) {
		this.paymentReferenceTO = paymentReferenceTO;
	}

	/**
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the paymentType.
	 */
	public int getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            The paymentType to set.
	 */
	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the payCurrencyDTO
	 */
	public PayCurrencyDTO getPayCurrencyDTO() {
		return payCurrencyDTO;
	}

	/**
	 * @param payCurrencyDTO
	 *            the payCurrencyDTO to set
	 */
	public void setPayCurrencyDTO(PayCurrencyDTO payCurrencyDTO) {
		this.payCurrencyDTO = payCurrencyDTO;
	}

	/**
	 * @return the paymentCarrier
	 */
	public String getPaymentCarrier() {
		return paymentCarrier;
	}

	/**
	 * @param paymentCarrier
	 *            the paymentCarrier to set
	 */
	public void setPaymentCarrier(String paymentCarrier) {
		this.paymentCarrier = paymentCarrier;
	}

	/**
	 * @return the lccUniqueTnxId
	 */
	public String getLccUniqueTnxId() {
		return lccUniqueTnxId;
	}

	/**
	 * @param lccUniqueTnxId
	 *            the lccUniqueTnxId to set
	 */
	public void setLccUniqueTnxId(String lccUniqueTnxId) {
		this.lccUniqueTnxId = lccUniqueTnxId;
	}


	/**
	 * @param paymentTnxId
	 */
	public Integer getPaymentTnxId() {
		return this.paymentTnxId;
	}

	public void setPaymentTnxId(Integer paymentTnxId) {
		this.paymentTnxId = paymentTnxId;
	}

}
