package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;

/**
* @hibernate.class table = "T_PNR_OPERATING_CARRIER_RLOC"
*/
public class OperatingCarrierRecordLocator implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer pnrOperatingCarrierRLOCId;
	
	//private String pnr;
	
	private int gdsId;
	
	private String operatingCarrierRLOC;
	
	private Reservation reservation;
	
	/**
	 * @hibernate.id column = "PNR_OPERATING_CARRIER_RLOC_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_OPERATING_CARRIER_RLOC"
	 */	
	public Integer getPnrOperatingCarrierRLOCId() {
		return pnrOperatingCarrierRLOCId;
	}
	
//	/**
//	 * @hibernate.property column = "PNR"
//	 */
//	public String getPnr() {
//		return pnr;
//	}

	/**
	 * @hibernate.property column = "GDS_ID"
	 */
	public int getGdsId() {
		return gdsId;
	}

	/**
	 * @hibernate.property column = "OPERATING_CARRIER_RLOC"
	 */
	public String getOperatingCarrierRLOC() {
		return operatingCarrierRLOC;
	}

	public void setPnrOperatingCarrierRLOCId(Integer pnrOperatingCarrierRLOCId) {
		this.pnrOperatingCarrierRLOCId = pnrOperatingCarrierRLOCId;
	}

//	public void setPnr(String pnr) {
//		this.pnr = pnr;
//	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public void setOperatingCarrierRLOC(String operatingCarrierRLOC) {
		this.operatingCarrierRLOC = operatingCarrierRLOC;
	}
	
	/**
	 * @hibernate.many-to-one column="PNR" class="com.isa.thinair.airreservation.api.model.Reservation"
	 */
	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
}
