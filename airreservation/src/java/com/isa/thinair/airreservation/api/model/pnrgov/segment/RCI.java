package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class RCI extends EDISegment {

	private String e9906;
	private String e9956;
	private String e9958;
	private String e9916;
	private String e9994;

	public RCI() {
		super(EDISegmentTag.RCI);
	}

	public void setCompanyIdentification(String e9906) {
		this.e9906 = e9906;
	}

	public void setPNR(String e9956) {
		this.e9956 = e9956;
	}

	public void setReservationControlType(String e9958) {
		this.e9958 = e9958;
	}

	public void setReservationCreatedDate(String e9916) {
		this.e9916 = e9916;
	}

	public void setReservationCreatedTime(String e9994) {
		this.e9994 = e9994;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement c330 = new CompoundDataElement("C330", DE_STATUS.M);
		c330.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9906, e9906));
		c330.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9956, e9956));
		c330.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9958, e9958));
		c330.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9916, e9916));
		c330.addBasicDataelement(new Numeric(EDI_ELEMENT.E9994, e9994));

		addEDIDataElement(c330);
		return this;
	}

}
