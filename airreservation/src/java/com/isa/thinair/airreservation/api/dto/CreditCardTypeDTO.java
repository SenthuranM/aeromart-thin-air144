/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

/**
 * To hold credit card type information
 */
public class CreditCardTypeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3764763097007356900L;

	private Integer cardTypeId;

	private String cardType;

	private Integer paymentNominalCode;

	private Integer refundNominalCode;
	
	private String accountCode;

	/**
	 * @return Returns the cardType.
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            The cardType to set.
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return Returns the cardTypeId.
	 */
	public Integer getCardTypeId() {
		return cardTypeId;
	}

	/**
	 * @param cardTypeId
	 *            The cardTypeId to set.
	 */
	public void setCardTypeId(Integer cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	/**
	 * @return Returns the paymentNominalCode.
	 */
	public Integer getPaymentNominalCode() {
		return paymentNominalCode;
	}

	/**
	 * @param paymentNominalCode
	 *            The paymentNominalCode to set.
	 */
	public void setPaymentNominalCode(Integer paymentNominalCode) {
		this.paymentNominalCode = paymentNominalCode;
	}

	/**
	 * @return Returns the refundNominalCode.
	 */
	public Integer getRefundNominalCode() {
		return refundNominalCode;
	}

	/**
	 * @param refundNominalCode
	 *            The refundNominalCode to set.
	 */
	public void setRefundNominalCode(Integer refundNominalCode) {
		this.refundNominalCode = refundNominalCode;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

}
