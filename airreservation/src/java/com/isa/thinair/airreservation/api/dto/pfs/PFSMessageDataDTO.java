/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.pfs;

import java.util.ArrayList;
import java.util.List;



/**
 * Holds Passenger final sales Meta data transfer information
 * 
 * @author TF
 * @since 1.0
 */
public class PFSMessageDataDTO {
	
	/** Holds the Message Type */
	private String msgType;

	/** Holds the flight number */
	private String flightNumber;
	
	/** Holds the flight date 03JAN */
	private String dayMonth;

	/** Holds the from airport */
	private String boardingAirport;	

	/** Hold the from address of Pfs */
	private String fromAddress;	
	
	private String communicationElement;
	
	private String classCodesRBD;
	
	private String seatConfiguration;
	
	/** Hold the eNumbric By Destination Element Pfs */
	private String numericByDestinationStr;
	
	/** Hold the end of Pfs */
	private String endElement;
	
	private List<PFSCatagoryByDestination> categoryByDestination=new ArrayList<PFSCatagoryByDestination>();
	
	private String partElement;
	
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the fromAddress.
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	

	/**
	 * @return Returns the boardingAirport.
	 */
	public String getBoardingAirport() {
		return boardingAirport;
	}

	/**
	 * @param boardingAirport
	 *            The boardingAirport to set.
	 */
	public void setBoardingAirport(String boardingAirport) {
		this.boardingAirport = boardingAirport;
	}

	public String getCommunicationElement() {
		return communicationElement;
	}

	public void setCommunicationElement(String communicationElement) {
		this.communicationElement = communicationElement;
	}

	/**
	 * @return the endElement
	 */
	public String getEndElement() {
		return endElement;
	}

	/**
	 * @param endElement the endElement to set
	 */
	public void setEndElement(String endElement) {
		this.endElement = endElement;
	}

	
	/**
	 * @return the numericByDestinationStr
	 */
	public String getNumericByDestinationStr() {
		return numericByDestinationStr;
	}

	/**
	 * @param numericByDestinationStr the numericByDestinationStr to set
	 */
	public void setNumericByDestinationStr(String numericByDestinationStr) {
		this.numericByDestinationStr = numericByDestinationStr;
	}

	/**
	 * @return the categoryByDestination
	 */
	public List<PFSCatagoryByDestination> getCategoryByDestination() {
		return categoryByDestination;
	}

	/**
	 * @param categoryByDestination the categoryByDestination to set
	 */
	public void setCategoryByDestination(List<PFSCatagoryByDestination> categoryByDestination) {
		this.categoryByDestination = categoryByDestination;
	}

	/**
	 * @return the seatConfiguration
	 */
	public String getSeatConfiguration() {
		return seatConfiguration;
	}

	/**
	 * @param seatConfiguration the seatConfiguration to set
	 */
	public void setSeatConfiguration(String seatConfiguration) {
		this.seatConfiguration = seatConfiguration;
	}

	/**
	 * @return the classCodesRBD
	 */
	public String getClassCodesRBD() {
		return classCodesRBD;
	}

	/**
	 * @param classCodesRBD the classCodesRBD to set
	 */
	public void setClassCodesRBD(String classCodesRBD) {
		this.classCodesRBD = classCodesRBD;
	}

	public String getDayMonth() {
		return dayMonth;
	}

	public void setDayMonth(String dayMonth) {
		this.dayMonth = dayMonth;
	}

	public String getPartElement() {
		return partElement;
	}

	public void setPartElement(String partElement) {
		this.partElement = partElement;
	}
}
