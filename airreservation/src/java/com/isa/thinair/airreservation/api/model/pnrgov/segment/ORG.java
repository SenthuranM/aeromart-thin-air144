package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class ORG extends EDISegment {

	private String e9906;
	private String e3225_1;
	private String e9900;
	private String e9902_1;

	public ORG() {
		super(EDISegmentTag.ORG);
	}

	public void setCompanyIdentification(String e9906) {
		this.e9906 = e9906;
	}

	public void setSystemLocationIdentification(String e3225_1) {
		this.e3225_1 = e3225_1;
	}

	public void setTravelAgentIdentification(String e9900) {
		this.e9900 = e9900;
	}

	public void setReservationSystemAgentIdentificationCode(String e9902_1) {
		this.e9902_1 = e9902_1;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C336 = new CompoundDataElement("C336", DE_STATUS.M);
		C336.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9906, e9906));
		C336.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E3225, e3225_1));

		CompoundDataElement C300 = new CompoundDataElement("C300", DE_STATUS.C);
		C300.addBasicDataelement(new Numeric(EDI_ELEMENT.E9900, e9900));
		C300.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9902, e9902_1));

		addEDIDataElement(C336);
		addEDIDataElement(C300);
		return this;
	}

}
