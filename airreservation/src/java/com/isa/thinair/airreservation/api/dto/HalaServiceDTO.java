package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

public class HalaServiceDTO implements Serializable {

	private static final long serialVersionUID = -6568457280697902372L;
	private Date departureDate;
	private Date arrivalDate;
	private String strDepLocal;
	private String strArrivLocal;
	private String flightNo;
	private String origin;
	private String destination;
	private Date departureZulu;
	private Date arrivalZulu;
	private String chargeCode;
	private String chargeDescription;
	private int noOfPax;
	private String email;
	private int ssrChargeId;
	private String applicableTime;
	private String applicableAirport;
	private String hubAirPort;

	private String pnr;
	private String segCode;
	private String ssrCode;
	private String ssrDescription;
	private String station;
	private String fullName;
	private String flightSegmentId;
	private String ssrId;
	private Integer ssrCategoryId;
	private Long ppssId;
	
	public String getSsrId() {
		return ssrId;
	}

	public void setSsrId(String ssrId) {
		this.ssrId = ssrId;
	}

	public String getFlightSegmentId() {
		return flightSegmentId;
	}

	public void setFlightSegmentId(String flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getSegCode() {
		return segCode;
	}

	public void setSegCode(String segCode) {
		this.segCode = segCode;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrDescription() {
		return ssrDescription;
	}

	public void setSsrDescription(String ssrDescription) {
		this.ssrDescription = ssrDescription;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	/**
	 * @return the hubAirPort
	 */
	public String getHubAirPort() {
		return hubAirPort;
	}

	/**
	 * @param hubAirPort
	 *            the hubAirPort to set
	 */
	public void setHubAirPort(String hubAirPort) {
		this.hubAirPort = hubAirPort;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getStrDepLocal() {
		return strDepLocal;
	}

	public void setStrDepLocal(String strDepLocal) {
		this.strDepLocal = strDepLocal;
	}

	public String getStrArrivLocal() {
		return strArrivLocal;
	}

	public void setStrArrivLocal(String strArrivLocal) {
		this.strArrivLocal = strArrivLocal;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureZulu() {
		return departureZulu;
	}

	public void setDepartureZulu(Date departureZulu) {
		this.departureZulu = departureZulu;
	}

	public Date getArrivalZulu() {
		return arrivalZulu;
	}

	public void setArrivalZulu(Date arrivalZulu) {
		this.arrivalZulu = arrivalZulu;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeDescription() {
		return chargeDescription;
	}

	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	public int getNoOfPax() {
		return noOfPax;
	}

	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getApplicableAirport() {
		return applicableAirport;
	}

	public void setApplicableAirport(String applicableAirport) {
		this.applicableAirport = applicableAirport;
	}

	public String getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(String applicableTime) {
		this.applicableTime = applicableTime;
	}

	public String getApplyOn() {
		return applyOn;
	}

	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	private String applyOn;

	public int getSsrChargeId() {
		return ssrChargeId;
	}

	public void setSsrChargeId(int ssrChargeId) {
		this.ssrChargeId = ssrChargeId;
	}

	public String getDepartureStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.departureDate, dateFormat);
	}

	public String getArrivalStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.arrivalDate, dateFormat);
	}

	public Integer getSsrCategoryId() {
		return ssrCategoryId;
	}

	public void setSsrCategoryId(Integer ssrCategoryId) {
		this.ssrCategoryId = ssrCategoryId;
	}

	public Long getPpssId() {
		return ppssId;
	}

	public void setPpssId(Long ppssId) {
		this.ppssId = ppssId;
	}

}
