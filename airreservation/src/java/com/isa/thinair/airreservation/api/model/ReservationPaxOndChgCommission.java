package com.isa.thinair.airreservation.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @hibernate.class table="T_PNR_PAX_OND_CHG_COMMISSION"
 * 
 */
public class ReservationPaxOndChgCommission extends Persistent {

	private static final long serialVersionUID = 1L;

	private Integer pnrPaxOndChgComId;

	private ReservationPaxOndCharge resPaxOndCharge;

	private String drOrcr;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Date transctionDate;

	private String agentCode;

	private String userId;
	
	private String status;

	/**
	 * @return Returns the pnrPaxOndChgComId.
	 * @hibernate.id column = "PNR_PAX_OND_CHG_COMMISSION_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_PAX_OND_CHG_COMMISSION"
	 */
	public Integer getPnrPaxOndChgComId() {
		return pnrPaxOndChgComId;
	}

	public void setPnrPaxOndChgComId(Integer pnrPaxOndChgComId) {
		this.pnrPaxOndChgComId = pnrPaxOndChgComId;
	}

	/**
	 * @hibernate.many-to-one column="PFT_ID" class="com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge"
	 * @return Returns the reservationPaxFare.
	 */
	public ReservationPaxOndCharge getResPaxOndCharge() {
		return resPaxOndCharge;
	}

	public void setResPaxOndCharge(ReservationPaxOndCharge resPaxOndCharge) {
		this.resPaxOndCharge = resPaxOndCharge;
	}

	/**
	 * @hibernate.property column = "DR_CR"
	 * @return drOrcr
	 */
	public String getDrOrcr() {
		return drOrcr;
	}

	public void setDrOrcr(String drOrcr) {
		this.drOrcr = drOrcr;
	}

	/**
	 * @hibernate.property column = "AMOUNT"
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @hibernate.property column = "TNX_DATE"
	 * @return
	 */
	public Date getTransctionDate() {
		return transctionDate;
	}

	public void setTransctionDate(Date transctionDate) {
		this.transctionDate = transctionDate;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 * @return
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @hibernate.property column = "USER_ID"
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * @hibernate.property column = "STATUS"
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object clone() {
		try {
			ReservationPaxOndChgCommission cloned = (ReservationPaxOndChgCommission) super.clone();
			cloned.resPaxOndCharge = resPaxOndCharge;
			cloned.agentCode = agentCode;
			cloned.amount = amount;
			cloned.transctionDate = transctionDate;
			cloned.userId = userId;
			cloned.drOrcr = drOrcr;
			cloned.status = status;
			return cloned;
		} catch (CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

}
