/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Isuru
 */
public class CashPaymentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2924290326604063205L;

	private BigDecimal totalSales = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Date dateOfSale;

	private String staffId;

	private String transferStatus;
	
	private String paymentCurrencyCode;

	/**
	 * @return Returns the staffId.
	 */
	public String getStaffId() {
		return staffId;
	}

	/**
	 * @param staffId
	 *            The staffId to set.
	 */
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	/**
	 * @return Returns the dateOfSale.
	 */
	public Date getDateOfSale() {
		return dateOfSale;
	}

	/**
	 * @param dateOfSale
	 *            The dateOfSale to set.
	 */
	public void setDateOfSale(Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	/**
	 * @return Returns the transferStatus.
	 */
	public String getTransferStatus() {
		return transferStatus;
	}

	/**
	 * @param transferStatus
	 *            The transferStatus to set.
	 */
	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	/**
	 * @return Returns the totalSales.
	 */
	public BigDecimal getTotalSales() {
		return totalSales;
	}

	/**
	 * @param totalSales
	 *            The totalSales to set.
	 */
	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}

	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

}
