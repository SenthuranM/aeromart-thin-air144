/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.emirates;

import java.util.Date;

/**
 * Holds Passenger final sales data transfer information
 * 
 * @author Byorn
 * @since 1.0
 */
public class XAPaxNameListDTO {

	private String departureAirportCode;

	private String arrivalAirportCode;

	private String cabinClassCode;

	private String title;

	private String firstName;

	private String lastName;

	private String xaPnr;

	private String day;

	private String month;

	private Date realFlightDate;

	private String flightNumber;

	private int pnlId;

	private Date dateDownloaded;

	private String infantFirstName;

	private String infantLastName;

	private String infantTitle;

	private String ssrCode;

	private String ssrRemarks;

	private String paxType;

	private String inboundInfo;

	private String outboundInfo;

	private String infantSSRCode;

	private String infantSSRText;

	private String errorDescription;

	/**
	 * @return Returns the arrivalAirportCode.
	 */
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	/**
	 * @param arrivalAirportCode
	 *            The arrivalAirportCode to set.
	 */
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	/**
	 * @return Returns the departureAirportCode.
	 */
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	/**
	 * @param departureAirportCode
	 *            The departureAirportCode to set.
	 */
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	/**
	 * @return Returns the firstname.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstname
	 *            The firstname to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastname.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastname
	 *            The lastname to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the pfsId.
	 */
	public int getPnlId() {
		return pnlId;
	}

	/**
	 * @param pfsId
	 *            The pfsId to set.
	 */
	public void setPnlId(int pnlId) {
		this.pnlId = pnlId;
	}

	/**
	 * @return Returns the realFlightDate.
	 */
	public Date getRealFlightDate() {
		return realFlightDate;
	}

	/**
	 * @param realFlightDate
	 *            The realFlightDate to set.
	 */
	public void setRealFlightDate(Date realFlightDate) {
		this.realFlightDate = realFlightDate;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		// if(!this.cabinClassCode.equals(XAParserConstants.CC_CODE)){
		// this.cabinClassCode="XAParserConstants.CC_CODE";
		// setErrorDescription("Cabin Class Code was not Y");
		// }
		return this.cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return the xaPnr
	 */
	public String getXAPnr() {
		if (xaPnr == null) {
			return " ";
		}
		return xaPnr;
	}

	/**
	 * @param xaPnr
	 *            the xaPnr to set
	 */
	public void setXAPnr(String xaPnr) {
		this.xaPnr = xaPnr;
	}

	/**
	 * @return Returns the infantFirstName.
	 */
	public String getInfantFirstName() {
		return infantFirstName;
	}

	/**
	 * @param infantFirstName
	 *            The infantFirstName to set.
	 */
	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	/**
	 * @return Returns the infantLastName.
	 */
	public String getInfantLastName() {
		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return Returns the infantTitle.
	 */
	public String getInfantTitle() {
		return infantTitle;
	}

	/**
	 * @param infantTitle
	 *            The infantTitle to set.
	 */
	public void setInfantTitle(String infantTitle) {
		this.infantTitle = infantTitle;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrRemarks.
	 */
	public String getSsrRemarks() {

		return ssrRemarks;
	}

	/**
	 * @param ssrRemarks
	 *            The ssrRemarks to set.
	 */
	public void setSsrRemarks(String ssrRemarks) {
		this.ssrRemarks = ssrRemarks;
	}

	/**
	 * @return Returns the xaPnr.
	 */
	public String getXaPnr() {
		return xaPnr;
	}

	/**
	 * @param xaPnr
	 *            The xaPnr to set.
	 */
	public void setXaPnr(String xaPnr) {
		this.xaPnr = xaPnr;
	}

	/**
	 * @return Returns the infantSSRCode.
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRText.
	 */
	public String getInfantSSRText() {
		return infantSSRText;
	}

	/**
	 * @param infantSSRText
	 *            The infantSSRText to set.
	 */
	public void setInfantSSRText(String infantSSRText) {
		this.infantSSRText = infantSSRText;
	}

	/**
	 * @return Returns the inboundInfo.
	 */
	public String getInboundInfo() {
		return inboundInfo;
	}

	/**
	 * @param inboundInfo
	 *            The inboundInfo to set.
	 */
	public void setInboundInfo(String inboundInfo) {
		this.inboundInfo = inboundInfo;
	}

	/**
	 * @return Returns the outboundInfo.
	 */
	public String getOutboundInfo() {
		return outboundInfo;
	}

	/**
	 * @param outboundInfo
	 *            The outboundInfo to set.
	 */
	public void setOutboundInfo(String outboundInfo) {
		this.outboundInfo = outboundInfo;
	}

	/**
	 * 
	 * @return
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * 
	 * @param errorDescription
	 */
	public void setErrorDescription(String errorDescription) {
		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

}
