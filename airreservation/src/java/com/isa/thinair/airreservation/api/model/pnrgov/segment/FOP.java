package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import com.isa.thinair.airreservation.api.model.pnrgov.AlphaNumeric;
import com.isa.thinair.airreservation.api.model.pnrgov.CompoundDataElement;
import com.isa.thinair.airreservation.api.model.pnrgov.EDISegment;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.DE_STATUS;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDISegmentTag;
import com.isa.thinair.airreservation.api.model.pnrgov.ElementFactory.EDI_ELEMENT;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;
import com.isa.thinair.airreservation.api.model.pnrgov.Numeric;

public class FOP extends EDISegment {

	private String e9888;
	private String e9988_2;
	private String e5004;
	private String e1154;
	private String e9916;

	public FOP() {
		super(EDISegmentTag.FOP);
	}

	public void setFormOfPaymentIdentification(String e9888) {
		this.e9888 = e9888;
	}

	public void setDataIndicator(String e9988_2) {
		this.e9988_2 = e9988_2;
	}

	public void setMonetaryAmount(String e5004) {
		this.e5004 = e5004;
	}

	public void setReferenceNumber(String e1154) {
		this.e1154 = e1154;
	}

	public void setFirstDate(String e9916) {
		this.e9916 = e9916;
	}

	@Override
	public MessageSegment build() {
		CompoundDataElement C641 = new CompoundDataElement("C641", DE_STATUS.M);
		C641.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9888, e9888));
		C641.addBasicDataelement(new AlphaNumeric(EDI_ELEMENT.E9988, e9988_2));
		C641.addBasicDataelement(new Numeric(EDI_ELEMENT.E5004, e5004));

		addEDIDataElement(C641);

		return this;
	}

}
