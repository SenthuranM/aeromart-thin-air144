package com.isa.thinair.airreservation.api.dto.meal;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;

public class PaxMealTO implements Serializable {

	private static final long serialVersionUID = 4916628131074044994L;
	private Integer pkey;
	private Integer pnrPaxId;
	private Integer pnrSegId;
	private Integer pnrPaxFareId;
	private Integer selectedFlightMealId;
	private String status;
	private String mealCode;
	private ExternalChgDTO chgDTO;
	private Integer mealId;
	private String mealName;
	private String paxName;
	private String paxMealSegCode;
	private String translatedMealName;
	private int allocatedQty = 1;
	private int mealCategoryID;
	private String mealCategoryCode;
	private Integer autoCancellationId;
	private boolean isCategoryRestricted;

	public String getPaxMealSegCode() {
		return paxMealSegCode;
	}

	public void setPaxMealSegCode(String paxMealSegCode) {
		this.paxMealSegCode = paxMealSegCode;
	}

	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public Integer getSelectedFlightMealId() {
		return selectedFlightMealId;
	}

	public void setSelectedFlightMealId(Integer selectedFlightMealId) {
		this.selectedFlightMealId = selectedFlightMealId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ExternalChgDTO getChgDTO() {
		return chgDTO;
	}

	public void setChgDTO(ExternalChgDTO chgDTO) {
		this.chgDTO = chgDTO;
	}

	public Integer getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(Integer pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public Integer getPkey() {
		return pkey;
	}

	public void setPkey(Integer pkey) {
		this.pkey = pkey;
	}

	public Integer getPnrPaxFareId() {
		return pnrPaxFareId;
	}

	public void setPnrPaxFareId(Integer pnrPaxFareId) {
		this.pnrPaxFareId = pnrPaxFareId;
	}

	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	/**
	 * @return Returns the paxName.
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            The paxName to set.
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getTranslatedMealName() {
		return translatedMealName;
	}

	public void setTranslatedMealName(String translatedMealName) {
		this.translatedMealName = translatedMealName;
	}

	public int getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(int allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public int getMealCategoryID() {
		return mealCategoryID;
	}

	public void setMealCategoryID(int mealCategoryID) {
		this.mealCategoryID = mealCategoryID;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	/**
	 * @return the isCategoryRestricted
	 */
	public boolean isCategoryRestricted() {
		return isCategoryRestricted;
	}

	/**
	 * @param isCategoryRestricted the isCategoryRestricted to set
	 */
	public void setCategoryRestricted(boolean isCategoryRestricted) {
		this.isCategoryRestricted = isCategoryRestricted;
	}
	
	

}
