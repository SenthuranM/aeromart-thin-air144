/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditDTO;
import com.isa.thinair.airreservation.api.dto.LoyaltyCreditUsageDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ResContactInfoSearchDTO;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Interface to define all the methods required to access the reservation query details
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationQueryBD {
	public static final String SERVICE_NAME = "ReservationService";

	/**
	 * Return available flights with all fares (For WEB USERS) All flights - fare exist Selected flight - fare exist
	 * 
	 * @param availableFlightSearchDTO
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public AvailableFlightDTO getAvailableFlightsWithAllFares(AvailableFlightSearchDTO availableFlightSearchDTO,
			TrackInfoDTO trackInfo) throws ModuleException;

	/**
	 * Return Quote fare for a selected flight
	 * 
	 * @param availableFlightSearchDTO
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public SelectedFlightDTO getFareQuote(AvailableFlightSearchDTO availableFlightSearchDTO, TrackInfoDTO trackInfo)
			throws ModuleException;

	public SelectedFlightDTO getFareQuoteTnx(AvailableFlightSearchDTO availableFlightSearchDTO, TrackInfoDTO trackInfo)
		throws ModuleException;


	/**
	 * Recreate the OndFareDTO collection form given criteria. This is lighter than reqouting
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> recreateFareSegCharges(OndRebuildCriteria criteria) throws ModuleException;

	/**
	 * Check the inventory availability for a given OndRebuildCriteria
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public boolean checkInventoryAvailability(OndRebuildCriteria criteria) throws ModuleException;

	/**
	 * Return reservation history information
	 * 
	 * @param pnr
	 * @return collection of ReservationModification
	 * @throws ModuleException
	 */
	public Collection<ReservationModificationDTO> getPnrHistory(String pnr) throws ModuleException;

	/**
	 * Search Reservations
	 * 
	 * @param reservationSearchDTO
	 * @return collection of ReservationDTO
	 * @throws ModuleException
	 */
	public Collection<ReservationDTO> getReservations(ReservationSearchDTO reservationSearchDTO) throws ModuleException;

	/**
	 * Search Reservations
	 * 
	 * @param reservationSearchDTO
	 * @return collection of ReservationDTO
	 * @throws ModuleException
	 */
	public String getPnrByExtRecordLocator(String extRLoc, String creationDate, boolean loadFares) throws ModuleException;

	/**
	 * Search Passenger Credit
	 * 
	 * @param reservationSearchDTO
	 * @return collection of ReservationPaxDTO
	 * @throws ModuleException
	 */
	public Collection<ReservationPaxDTO> getPnrPassengerSumCredit(ReservationSearchDTO reservationSearchDTO)
			throws ModuleException;

	/**
	 * Return Early reservations
	 * 
	 * @param customerId
	 * @param date
	 * @param colReservationStates
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationDTO> getEarlyReservations(int customerId, Date date, Collection<String> colReservationStates, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	/**
	 * Return After reservations
	 * 
	 * @param customerId
	 * @param date
	 * @param colReservationStates
	 * @param trackInfoDTO TODO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationDTO> getAfterReservations(int customerId, Date date, Collection<String> colReservationStates, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	/**
	 * Return PNR List of Customer
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	public List<String> getPnrList(int customerId) throws ModuleException;

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PaxCreditDTO> getReservationCredits(int customerId) throws ModuleException;

	/**
	 * Return Pax Credits for PNRs
	 * 
	 * @param pnrList
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PaxCreditDTO> getPaxCreditForPnrs(List<String> pnrList) throws ModuleException;

	/**
	 * Returns the reservation contact information
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public ReservationContactInfo getReservationContactInfo(String pnr) throws ModuleException;

	/**
	 * Return contact infomation of the matching last booking
	 * 
	 * @param resContactInfoSearchDTO
	 *            search critria
	 * @return
	 * @throws ModuleException
	 */
	public ReservationContactInfo getLatestReservationContactInfo(ResContactInfoSearchDTO resContactInfoSearchDTO)
			throws ModuleException;

	/**
	 * Search Reservations for credit card information
	 * 
	 * @param reservationPaymentDTO
	 * @return collection of ReservationDTO
	 * @throws ModuleException
	 */
	public Collection<ReservationPaxDTO> getReservationsForCreditCardInfo(ReservationPaymentDTO reservationPaymentDTO)
			throws ModuleException;

	/**
	 * Return user notes
	 * 
	 * @param pnr
	 * @param isClassifyUN 
	 * @return Collection of ReservationModification
	 * @throws ModuleException
	 */
	public Collection<ReservationModificationDTO> getUserNotes(String pnr, boolean isClassifyUN) throws ModuleException;

	/**
	 * Get flight manifest details
	 * 
	 * @param options
	 *            TODO
	 * 
	 * @return
	 */
	public String viewFlightManifest(FlightManifestOptionsDTO manifestOptions) throws ModuleException;

	/**
	 * Send flight manifest as an email
	 * 
	 */
	public Boolean sendFlightManifestAsEmail(FlightManifestOptionsDTO manifestOptions) throws ModuleException;

	/**
	 * Returns all the external payment transactions for given PNR for front-end display.
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public PNRExtTransactionsTO getPNRExtPayTransactions(String pnr) throws ModuleException;

	/**
	 * Return external payment transactions for the specified criteria.
	 * 
	 * @param criteriaDTO
	 * @return Map<PNR, PNRExtTransactionsTO>
	 * @throws ModuleException
	 */
	public Map<String, PNRExtTransactionsTO> getExtPayTransactions(ExtPayTxCriteriaDTO criteriaDTO) throws ModuleException;

	/**
	 * Returns Terms & Conditions in the specified locale.
	 * 
	 * @param locale
	 * @return
	 */
	public String getTermsNConditions(Locale locale) throws ModuleException;

	/**
	 * Retrieve the terms and conditions for a given language and terms template name.
	 * 
	 * @param templateName
	 *            TermsTemplate name of terms and conditions to be retrieved.
	 * @param locale
	 *            Locale of the terms and conditions with the language set.
	 * @return The relevant terms and conditions for the given name and locale.
	 * @throws ModuleException
	 *             If terms and condition retrieval fails.
	 */
	public String getTermsNConditions(String templateName, Locale locale) throws ModuleException;

	/**
	 * Get Names if exist with given
	 * 
	 * @param flightSegIds
	 * @param adultNameList
	 * @param childNameList
	 * @param pnr
	 * @param paxIds TODO
	 * @return
	 */
	public Collection<String> getDuplicateNameReservations(Collection<Integer> flightSegIds, ArrayList<String> adultNameList,
			ArrayList<String> childNameList, String pnr, Collection<Integer> paxIds) throws ModuleException;

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<LoyaltyCreditDTO> getMashreqCredits(int customerId) throws ModuleException;

	/**
	 * Return reservation credits
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<LoyaltyCreditUsageDTO> getMashreqCreditUsage(int customerId) throws ModuleException;

	/**
	 * Get checkin passenger details
	 * 
	 * @param flightId
	 * @return
	 */
	public Collection<PassengerCheckinDTO> getCheckinPassengers(int flightId, String airportCode, String checkinStatus,
			boolean standBy, boolean goShow) throws ModuleException;

	/**
	 * Get COS for flgiht
	 * 
	 * @param flightId
	 * @return
	 */
	public Collection<String> getCOSForFlight(int flightId) throws ModuleException;

	/**
	 * Update passenger checkin status
	 * 
	 * @param pnrPaxFareSegId
	 * @param checkinStatus
	 * @throws ModuleException
	 */
	public void updatePaxCheckinStatus(ArrayList<Integer> pnrPaxFareSegId, String checkinStatus) throws ModuleException;

	/**
	 * Return the object with pnr pax fare & segment ids
	 * 
	 * @param pnr
	 * @param flightSegmentId
	 * @return
	 * @throws ModuleException
	 */
	public PnrFarePassenger getPnrFarePassenger(String pnr, int flightSegmentId, int paxSequence) throws ModuleException;

	/**
	 * Retrieves the Confirmed (SC) reservation Insurance attached to this pnr)
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public List<ReservationInsurance> getReservationInsuranceByPnr(String pnr) throws ModuleException;

	/**
	 * Return external payment transactions for the specified request UID.
	 * 
	 * @param reqUID
	 * @return Map<PNR, PNRExtTransactionsTO>
	 * @throws ModuleException
	 */
	public Map<String, PNRExtTransactionsTO> getExtPayTransactionsForReqUID(String reqUID) throws ModuleException;

	/**
	 * Get On-holdability of the reservation
	 * 
	 * @param onholdValidationDTO
	 * @return
	 * @throws ModuleException
	 */
	public boolean isReservationOnholdable(ResOnholdValidationDTO onholdValidationDTO) throws ModuleException;

	/**
	 * Get onHold Release time
	 * 
	 * @param onHoldReleaseTimeDTO
	 * @return
	 */
	public Long getOnHoldReleaseTime(OnHoldReleaseTimeDTO onHoldReleaseTimeDTO) throws ModuleException;

	/**
	 * Returns the PNR via the eticket Number
	 * 
	 * @param eticketNumber
	 * @return
	 * @throws ModuleException
	 */
	public String getPNRByEticketNo(String eticketNumber) throws ModuleException;

	/**
	 * Return Available Flights for OHD rollfoward facility
	 * 
	 * @param rollForwardFlightSearch
	 * @return
	 * @throws ModuleException
	 */
	public List<RollForwardFlightAvailRS> getAvaialableFlightForRollForward(RollForwardFlightSearchDTO rollForwardFlightSearch)
			throws ModuleException;

	/**
	 * return segment wise selected bundled fare ID map
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Integer> getSegmentBundledFarePeriodIds(String pnr) throws ModuleException;

	/**
	 * Add user Notes
	 * 
	 * @params userNoteTO
	 * @param trackInfoDTO
	 *            TODO
	 */
	public void addUserNote(UserNoteTO userNoteTO, TrackInfoDTO trackInfoDTO) throws ModuleException;
}
