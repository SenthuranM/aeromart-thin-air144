package com.isa.thinair.airreservation.api.dto.flightmanifest;

import java.util.HashMap;
import java.util.Map;

public class CabinClassSummaryDTO extends ManifestCountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cabinClassName;

	private String cabinClassCode;
	
	private Map<String, LogicalCCSummaryDTO> logicalCCWiseSummary = new HashMap<String, LogicalCCSummaryDTO>();

	public CabinClassSummaryDTO(String cabinClassName, String cabinClassCode, Map<String, LogicalCCSummaryDTO> logicalCCWiseSummary) {
		super();
		this.cabinClassName = cabinClassName;
		this.cabinClassCode = cabinClassCode;
		this.logicalCCWiseSummary = logicalCCWiseSummary;
	}

	public String getCabinClassName() {
		return cabinClassName;
	}

	public void setCabinClassName(String cabinClassName) {
		this.cabinClassName = cabinClassName;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public Map<String, LogicalCCSummaryDTO> getLogicalCCWiseSummary() {
		return logicalCCWiseSummary;
	}

	public void setLogicalCCWiseSummary(Map<String, LogicalCCSummaryDTO> logicalCCWiseSummary) {
		this.logicalCCWiseSummary = logicalCCWiseSummary;
	}
	
}
