/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * To hold reservation passenger data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ReservationPaxDTO extends ReservationDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8525555148867821692L;

	/** Holds collection of ReservationPaxDetailsDTO */
	private Collection<ReservationPaxDetailsDTO> passengers;

	/** the carrier code the pax is belongs to */
	private String carrierCode;

	/**
	 * @return Returns the passengers.
	 */
	public Collection<ReservationPaxDetailsDTO> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            The passengers to set.
	 */
	private void setPassengers(Collection<ReservationPaxDetailsDTO> passengers) {
		this.passengers = passengers;
	}

	/**
	 * Add a passenger
	 * 
	 * @param reservationPaxDetailsDTO
	 */
	public void addPassenger(ReservationPaxDetailsDTO reservationPaxDetailsDTO) {
		if (this.getPassengers() == null) {
			this.setPassengers(new ArrayList<ReservationPaxDetailsDTO>());
		}

		this.getPassengers().add(reservationPaxDetailsDTO);
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
}
