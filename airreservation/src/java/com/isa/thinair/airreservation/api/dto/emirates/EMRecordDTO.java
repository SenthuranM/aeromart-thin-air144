/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto.emirates;

/**
 * Holds the PNL atomic record specific data transfer information
 * 
 * @author Byorn
 * @since 1.0
 */
public class EMRecordDTO {

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the passenger first name with title */
	private String firstNameWithTitle;

	private String ssrCode;

	private String ssrText;

	private String infantName;

	private String infantLastName;

	private String infantSSRCode;

	private String infantSSRText;

	private String errorDescription;

	/**
	 * Add the first Name with title
	 * 
	 * @param fName
	 */
	public void addFName(String fName) {
		if (this.getFirstNameWithTitle() == null) {
			this.setFirstNameWithTitle(fName);
		} else {
			this.setFirstNameWithTitle(this.getFirstNameWithTitle() + " " + fName);
		}
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the title in order to parse
	 * 
	 * @return
	 */
	public String getParseTitle() {
		if (this.getFirstNameWithTitle() == null) {
			return this.getLastName();
		} else {
			return this.getFirstNameWithTitle();
		}
	}

	/**
	 * Clones the RecordDTO object
	 */
	public Object clone() {
		EMRecordDTO recordDTO = new EMRecordDTO();
		recordDTO.setFirstNameWithTitle(this.getFirstNameWithTitle());
		recordDTO.setLastName(this.getLastName());

		return recordDTO;
	}

	/**
	 * @return Returns the firstNameWithTitle.
	 */
	public String getFirstNameWithTitle() {
		return firstNameWithTitle;
	}

	/**
	 * @param firstNameWithTitle
	 *            The firstNameWithTitle to set.
	 */
	public void setFirstNameWithTitle(String firstNameWithTitle) {
		this.firstNameWithTitle = firstNameWithTitle;
	}

	/**
	 * @return Returns the infantName.
	 */
	public String getInfantName() {
		return infantName;
	}

	/**
	 * @param infantName
	 *            The infantName to set.
	 */
	public void setInfantName(String infantName) {
		this.infantName = infantName;
	}

	/**
	 * @return Returns the infantSSRCode.
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRText.
	 */
	public String getInfantSSRText() {
		return infantSSRText;
	}

	/**
	 * @param infantSSRText
	 *            The infantSSRText to set.
	 */
	public void setInfantSSRText(String infantSSRText) {
		this.infantSSRText = infantSSRText;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		if (this.ssrCode == null) {
			this.ssrCode = " ";
		}

		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		if (this.ssrText == null) {
			this.ssrText = "";
		}
		return this.ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {

		this.ssrText = ssrText;
	}

	/**
	 * @return Returns the infantLastName.
	 */
	public String getInfantLastName() {

		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	public String getErrorDescription() {
		if (this.errorDescription == null) {
			this.errorDescription = "";
		}
		return this.errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		if (errorDescription == null) {
			errorDescription = "";
		}

		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}
}
