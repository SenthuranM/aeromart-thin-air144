/**
 * 
 */
package com.isa.thinair.airreservation.api.dto.pfs;

import java.util.List;

/**
 * @author ashain
 *
 */
public class CabinCountTo {
	private String cabinClass;
	private int count;
	private int capacity;
	private String countDisplay;
	private List<PassengerBookingToPFS> paxBookingList;
	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}
	/**
	 * @param cabinClass the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	/**
	 * @return the countDisplay
	 */
	public String getCountDisplay() {
		return countDisplay;
	}
	/**
	 * @param countDisplay the countDisplay to set
	 */
	public void setCountDisplay(String countDisplay) {
		this.countDisplay = countDisplay;
	}
	/**
	 * @return the paxBookingList
	 */
	public List<PassengerBookingToPFS> getPaxBookingList() {
		return paxBookingList;
	}
	/**
	 * @param paxBookingList the paxBookingList to set
	 */
	public void setPaxBookingList(List<PassengerBookingToPFS> paxBookingList) {
		this.paxBookingList = paxBookingList;
	}
	
	
}
