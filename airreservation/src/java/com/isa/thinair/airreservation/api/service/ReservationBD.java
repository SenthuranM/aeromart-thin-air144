/*
 * ==============================================================================

 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.airproxy.api.dto.UserNoteHistoryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.assembler.PreferenceAnalyzer;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.FlightConnectionDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.GdsReservationAttributesDto;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldBookingInfoDTO;
import com.isa.thinair.airreservation.api.dto.OnholdReservatoinSearchDTO;
import com.isa.thinair.airreservation.api.dto.PFSMetaDataDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.RecieptLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.ReservationBasicsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TemporyPaymentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupPaymentNotificationDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferAssembler;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.IPassengerAutomaticCheckin;
import com.isa.thinair.airreservation.api.dto.baggage.IPassengerBaggage;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateRequestDTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateResponseDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.itinerary.ItineraryDTO;
import com.isa.thinair.airreservation.api.dto.meal.IPassengerMeal;
import com.isa.thinair.airreservation.api.dto.onlinecheckin.OnlineCheckInReminderDTO;
import com.isa.thinair.airreservation.api.dto.pfs.CSPaxBookingCount;
import com.isa.thinair.airreservation.api.dto.pfs.PfsXmlDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVResInfoDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVSegmentDTO;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakInsuredTraveler;
import com.isa.thinair.airreservation.api.dto.seatmap.IPassengerSeating;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxAdjAssembler;
import com.isa.thinair.airreservation.api.dto.ssr.CommonMedicalSsrParamDTO;
import com.isa.thinair.airreservation.api.model.AlertAction;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.CodeSharePfs;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.LmsBlockedCredit;
import com.isa.thinair.airreservation.api.model.OfficersMobileNumbers;
import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.model.TaxInvoice;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.AllocateEnum;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceSalesType;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.ConfigOnholdTimeLimitsSearchDTO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.OfficerMobNumsConfigsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.paymentbroker.api.dto.CreditCardPaymentStatusDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

/**
 * Interface to define all the methods required to access the reservation details
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationBD {

	public static final String SERVICE_NAME = "ReservationService";

	/**
	 * Return block records after blocking from the inventory
	 * 
	 * @param colOndFare
	 * @param blockSeatUID
	 *            TODO
	 * @return collection of block Integer ids
	 * @throws ModuleException
	 */
	public Collection<TempSegBcAlloc> blockSeats(Collection<OndFareDTO> colOndFare, Collection<Integer> flightAmSeatIds)
			throws ModuleException;

	/**
	 * Release blocked seats from the inventory
	 * 
	 * @param colBlockIds
	 *            collection of block integer ids
	 * @throws ModuleException
	 */
	public void releaseBlockedSeats(Collection<TempSegBcAlloc> colBlockIds) throws ModuleException;

	public void releaseBlockedSeats(Collection<TempSegBcAlloc> colBlockIds, boolean isFailSilently) throws ModuleException;

	/**
	 * Return a reservation
	 * 
	 * @param pnrModesDTO
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Retrive the parent reservation for a given external locator
	 * 
	 * @param externalLocator
	 * @return
	 * @throws ModuleException
	 */
	public Reservation getCandidateParentReservationFromExtLocator(String externalLocator) throws ModuleException;

	/**
	 * Extend onhold reservation
	 * 
	 * @param pnr
	 * @param minutes
	 * @param version
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	public ServiceResponce extendOnholdReservation(String pnr, int minutes, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	/**
	 * Split reservation
	 * 
	 * @param pnr
	 * @param paxIds
	 * @param version
	 * @param newOriginatorPNR
	 * @param newCarrierPNR
	 * @param userNotes
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce splitReservation(String pnr, Collection<Integer> paxIds, long version, String newOriginatorPNR,
			String newCarrierPNR, TrackInfoDTO trackInfoDTO, String userNotes, String extRecordLocator) throws ModuleException;

	/**
	 * Split reservation for dummy bookings
	 * 
	 * @param oldReservation
	 * @param newPnr
	 * @param paxIds
	 * @param isSplitForRemPax
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce splitDummyReservation(Reservation oldReservation, String newPnr, Collection<Integer> paxIds,
			boolean isSplitForRemPax) throws ModuleException;

	/**
	 * Update reservation
	 * 
	 * @param reservation
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateReservation(Reservation reservation, TrackInfoDTO trackInfoDTO, boolean dupNameCheck,
			boolean applyNameChangeCharge, Collection<String> allowedOperations, String reasonForAllowBlPax)
			throws ModuleException;

	/**
	 * update dummy reservation pax info
	 * 
	 * @param reservation
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateDummyReservation(Reservation reservation, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Cancel reservation
	 * 
	 * @param pnr
	 * @param customChargesTO
	 * @param version
	 * @param cnxFlownSegs
	 * @param utilizeAllRefundables
	 *            TODO
	 * @param trackInfoDTO
	 * @param isVoidReservation
	 * @param userNotes
	 *            TODO
	 * @param gdsNotifyAction
	 * @param paxCharges
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce cancelReservation(String pnr, CustomChargesTO customChargesTO, long version, boolean releaseOnHold,
			boolean cnxFlownSegs, boolean utilizeAllRefundables, TrackInfoDTO trackInfoDTO, boolean isVoidReservation,
			String userNotes, int gdsNotifyAction, Map<Integer, List<ExternalChgDTO>> paxCharges) throws ModuleException;

	/**
	 * Update reservation for payment
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param isForceConfirm
	 * @param version
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 *            flag to enable fraud check or not
	 * @param isPaymentForAcquireCredits
	 *            flag indicating the the caller is AcquireCredits or not.Will used in the confirmReservation
	 * @param otherCarrierCreditUsed
	 *            true iff use other carrier pax credit for balance payment
	 * @param isFirstPayment
	 * @param loyaltyPaymentInfo
	 * @param isGdsSale
	 * @param paxAdj
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateReservationForPayment(String pnr, IPassenger iPassenger, boolean isForceConfirm,
			boolean acceptMorePaymentsThanItsCharges, long version, TrackInfoDTO trackInfoDTO, boolean enableECFraudChecking,
			boolean isPaymentForAcquireCredits, boolean isCapturePayment, boolean otherCarrierCreditUsed, boolean actualPayment,
			boolean isFirstPayment, LoyaltyPaymentInfo loyaltyPaymentInfo, boolean isGdsSale, PaxAdjAssembler paxAdj)
			throws ModuleException;

	/**
	 * Update reservation for split operation
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param version
	 * @param trackInfoDTO
	 * @param enableECFraudChecking
	 *            TODO
	 * @param otherCarrierCreditUsed
	 *            TODO
	 * @param isCapturePayment
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateReservationForSplit(String pnr, IPassenger iPassenger, long version, TrackInfoDTO trackInfoDTO,
			boolean enableECFraudChecking, boolean isPaymentForAcquireCredits, boolean otherCarrierCreditUsed,
			boolean actualPayment, boolean isCapturePayment) throws ModuleException;

	/**
	 * Create onhold reservation
	 * 
	 * @param iReservation
	 * @param blockKeyIds
	 * @param forceConfirm
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createOnHoldReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			boolean forceConfirm, TrackInfoDTO trackInfoDTO, boolean isCapturePayment) throws ModuleException;

	/**
	 * Create onhold reservation through aaService. Note this is a temp solution for the current transaction intergrity
	 * probleam
	 * 
	 * @param iReservation
	 * @param collBlockIDs
	 * @param ondFareDto
	 * @param forceConfirm
	 * @param onholdReservation
	 * @param isCapturePayment
	 * @param userNotes
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createAAServiceReservationMacro(IReservation iReservation, Collection<TempSegBcAlloc> collBlockIDs,
			Collection<OndFareDTO> ondFareDto, boolean forceConfirm, boolean onholdReservation, boolean isCapturePayment,
			String userNotes, TrackInfoDTO trackInfo) throws ModuleException;

	/**
	 * Create pay carrier fulfillments for external payments done for dry or interline booking
	 * 
	 * @param reservation
	 * @param trackInfoDTO
	 * @param adminInfoTO
	 * @param paxWiseExtTnx
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createPayCarrierfulfillReservation(Reservation reservation, TrackInfoDTO trackInfoDTO,
			ReservationAdminInfoTO adminInfoTO, Map<Integer, Collection<ReservationPaxExtTnx>> paxWiseExtTnx,
			String reasonForAllowBlPax) throws ModuleException;

	/**
	 * Create call center reservation
	 * 
	 * @param iReservation
	 * @param trackInfoDTO
	 * @param wsSessionId
	 * @param enableFraud
	 *            flag to enable the CC fraud check TODO move to TrackingInfoDto
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createCCReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			TrackInfoDTO trackInfoDTO, boolean enableFraud, boolean actualPayment) throws ModuleException;

	/**
	 * Create a external reservation which is created from external source such as GDS
	 * 
	 * @param iReservation
	 * @param blockKeyIds
	 * @param eTicketInfo
	 * @param trackInfoDTO
	 * @param enableFraud
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createGDSReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			Map<Integer, Map<Integer, EticketDTO>> eTicketInfo, TrackInfoDTO trackInfoDTO, boolean enableFraud)
			throws ModuleException;

	/**
	 * Create web reservation
	 * 
	 * @param iReservation
	 * @param collBlockID
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createWebReservation(IReservation iReservation, Collection<TempSegBcAlloc> collBlockID,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	public void intermediateRefund(IReservation iReservation, TrackInfoDTO trackInfoDTO, String exceptionCode)
			throws ModuleException;

	/**
	 * Create split reservation
	 * 
	 * @param iReservation
	 * @param blockKeyIds
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createSplitReservation(IReservation iReservation, Collection<TempSegBcAlloc> blockKeyIds,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Return reservation contact information
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public ReservationContactInfo getContactInfo(String pnr) throws ModuleException;

	/**
	 * Update reservation contact information only
	 * 
	 * @param pnr
	 * @param version
	 * @param contactInfo
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateContactInfo(String pnr, long version, ReservationContactInfo contactInfo,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Update dummy reservation contact info
	 * 
	 * @param pnr
	 * @param version
	 * @param contactInfo
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateDummyReservationContactInfo(String pnr, long version, ReservationContactInfo contactInfo,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Update the reservation and pax status
	 * 
	 * @param reservation
	 * @param newReservationStatus
	 * @return
	 */
	public DefaultServiceResponse updateReservatonStatus(Reservation reservation, String newStatus);

	public DefaultServiceResponse updateReservationModifiability(Reservation reservation, char modifiableReservation);

	/**
	 * Transfer reservation ownership
	 * 
	 * @param pnr
	 * @param ownerAgentCode
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce transferOwnerShip(String pnr, String ownerAgentCode, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	/**
	 * Transfer ownership for dummy bookings
	 * 
	 * @param pnr
	 * @param ownerAgentCode
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce transferOwnerShipForDummyRes(String pnr, String ownerAgentCode, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	/**
	 * In order to reprotect reservations once perticular flights are cancelled. pass a collection of TransferSeatDTO
	 * objects.
	 * 
	 * @param transferSeatDTO
	 * @param isAlert
	 * @param overAlloc
	 *            holds allocation mode
	 * @param overAllocSeats
	 * @param fltAltDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce transferSeats(Collection<TransferSeatDTO> transferSeatDTOs, boolean isAlert, AllocateEnum overAlloc,
			int overAllocSeats, FlightAlertDTO fltAltDTO) throws ModuleException;

	/**
	 * Roll forward reservations
	 * 
	 * @param transferSeatDTO
	 * @param isAlert
	 * @param overAlloc
	 * @param overAllocSeats
	 * @param fromDate
	 * @param toDate
	 * @param excludeDays
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce rollForwardReservations(TransferSeatDTO transferSeatDTO, boolean isAlert, AllocateEnum overAlloc,
			int overAllocSeats, Date fromDate, Date toDate, Frequency excludeDays, boolean includeCLSFlights,
			FlightAlertDTO flightAlertDTO) throws ModuleException;

	/**
	 * Expire On hold reservations
	 * 
	 * @param toDate
	 *            release to date
	 * @param customAdultCancelCharge
	 *            pass null to pick system default cancellation charges
	 * @param customChildCancelCharge
	 *            pass null to pick system default cancellation charges
	 * @param customInfantCancelCharge
	 *            pass null to pick system default cancellation charges
	 * @throws ModuleException
	 */
	public void expireOnholdReservations(Date toDate, BigDecimal customAdultCancelCharge, BigDecimal customChildCancelCharge,
			BigDecimal customInfantCancelCharge) throws ModuleException;

	/**
	 * Email Itinerary
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	public void emailItinerary(ItineraryLayoutModesDTO itineraryLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			TrackInfoDTO trackInfoDTO, boolean isCheckIndividual) throws ModuleException;

	/**
	 * Email Interline Itinerary
	 */
	public void emailInterlineItinerary(ItineraryDTO itineraryDTO) throws ModuleException;

	/**
	 * Return the credit card information
	 * 
	 * @param isOwnTnx
	 *            TODO
	 * @param payId
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, CardPaymentInfo> getCreditCardInfo(Collection<Integer> colTnxIds, boolean isOwnTnx)
			throws ModuleException;

	/**
	 * Return credit card information from RESPCD
	 * 
	 * @param colTnxIds
	 * @param isOwnTnx
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, CardPaymentInfo> getCreditCardInfoFromRESPCD(Collection<Integer> colTnxIds, boolean isOwnTnx)
			throws ModuleException;

	/**
	 * To reconcile reservations
	 * 
	 * @param pfsId
	 * @param checkInMethod
	 * @param isConsiderFutureDateValidation
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reconcileReservations(int pfsId, boolean isCheckinProcess, boolean isProcessPFS, String checkInMethod,
			boolean isConsiderFutureDateValidation) throws ModuleException;

	/**
	 * Create go show reservation
	 * 
	 * @param iReservation
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce createGoShoreReservation(IReservation iReservation, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Process Automatic Reconcilation
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce processAutomaticReconcilation() throws ModuleException;

	/**
	 * Process Parsed PFS Reconcilation
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce processParsedPFSReconcilation() throws ModuleException;

	/**
	 * Audit Credit Card Failures
	 * 
	 * @param pnr
	 * @param amount
	 * @param errorFunction
	 * @param errorDescription
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	public void auditCreditCardFailures(String pnr, BigDecimal amount, String errorFunction, String errorDescription,
			TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Return alert actions
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<AlertAction> getAlertActions() throws ModuleException;

	/**
	 * Return Passenger final sales record
	 * 
	 * @param pfsID
	 * @return
	 * @throws ModuleException
	 */
	public Pfs getPFS(int pfsID) throws ModuleException;

	/**
	 * Return paged wise passenger final sales records
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	public Page getPagedPFSData(List<ModuleCriterion> criteria, int startIndex, int noRecs, List<String> orderByFieldList)
			throws ModuleException;

	/**
	 * Save passenger final sales record
	 * 
	 * @param pfs
	 * @throws ModuleException
	 */
	public void savePfsEntry(Pfs pfs) throws ModuleException;

	/**
	 * 
	 * @param pfs
	 * @return ppId
	 * @throws ModuleException
	 */
	public int savePfsEntryForAuditing(Pfs pfs) throws ModuleException;

	/**
	 * Return passenger final sales entries
	 * 
	 * @param pfsId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PfsPaxEntry> getPfsParseEntries(int pfsId) throws ModuleException;

	/**
	 * Save PFS Parse Entry
	 * 
	 * @param pfs
	 * @param pfsParsed
	 * @throws ModuleException
	 */
	public void savePfsParseEntry(Pfs pfs, PfsPaxEntry pfsParsed) throws ModuleException;

	/**
	 * save pfs entry
	 * 
	 * @param pfsParsed
	 * @throws ModuleException
	 */
	public void savePfsParseEntry(PfsPaxEntry pfsParsed) throws ModuleException;

	/**
	 * Delete PFS Parse Entry with PFS status update
	 * 
	 * @param pfs
	 * @param pfsParsed
	 * @throws ModuleException
	 */
	public void deletePfsParseEntry(Pfs pfs, PfsPaxEntry pfsParsed) throws ModuleException;

	/**
	 * Delete PFS Parse Entry
	 * 
	 * @param pfs
	 * @throws ModuleException
	 */
	public void deletePfsEntry(Pfs pfs) throws ModuleException;

	/**
	 * Return Pfs parse entries count
	 * 
	 * @param pfsId
	 * @param processedStatus
	 * @return
	 * @throws ModuleException
	 */
	public int getPfsParseEntryCount(int pfsId, String processedStatus) throws ModuleException;

	/**
	 * Save/Update External Payment Transaction Information
	 * 
	 * @param externalPaymentTnx
	 */
	public void saveOrUpdateExternalPayTxInfo(ExternalPaymentTnx externalPaymentTnx) throws ModuleException;

	/**
	 * Reconciles reservation payments as per the bank payments for transactions for given pnrs.
	 * 
	 * @param pnrExtTransactionsTOs
	 *            collection of PNRExtTransactionsTO
	 * @param onAccAgentCode
	 *            Agent code on which onaccount transactions are recorded
	 * @param isManualRecon
	 *            Specify whether manual or scheduled reconciliation
	 * @param startTimestamp
	 *            Specifies the start timestamp when called for daily transaction reconciliation
	 * @param endTimestamp
	 *            Specifies the end timestamp when called for daily transaction reconciliation
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reconcileExtPayTransactions(Collection<PNRExtTransactionsTO> pnrExtTransactionsTOs,
			boolean isManualRecon, Date startTimestamp, Date endTimestamp, String agencyCode) throws ModuleException;

	/**
	 * Make a tempory payment entry
	 * 
	 * @param pnr
	 *            pass pnr number as null if you don't have a pnr number
	 * @param reservationContactInfo
	 * @param iPayment
	 * @param isCredit
	 *            is true if it is a credit transaction
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, CardPaymentInfo> makeTemporyPaymentEntry(String pnr, ReservationContactInfo reservationContactInfo,
			IPayment iPayment, boolean isCredit, TrackInfoDTO trackInfoDTO, boolean isOfflinePayment,
			boolean isCsOcFlightAvailable) throws ModuleException;

	/**
	 * Update the temp payment with Fraud CC check results
	 * 
	 * @param tnxId
	 * @param result
	 * @return
	 * @throws ModuleException
	 */
	public void updateTempPaymentWithFC(int tnxId, CCFraudCheckResponseDTO result) throws ModuleException;

	/**
	 * Load TempPaymentTnx
	 * 
	 * @param tnxId
	 * @return
	 * @throws ModuleException
	 */
	public TempPaymentTnx loadTempPayment(int tnxId) throws ModuleException;

	/**
	 * Retrieve the temp payments for reservation
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr) throws ModuleException;

	/**
	 * Update the tempory payment entry payment status Payment Successful or Payment Failure can occure
	 * 
	 * @param mapTnxIds
	 * @param isSuccess
	 * @throws ModuleException
	 */
	public void updateTempPaymentEntryPaymentStatus(Map<Integer, CommonCreditCardPaymentInfo> mapTnxIds,
			IPGResponseDTO ipgResponseDTO) throws ModuleException;

	/**
	 * Update the tempory payment entry status Payment Successful or Payment Failure can occur
	 * 
	 * @param tempTxId
	 * @param isSuccess
	 * @throws ModuleException
	 */
	public void updateTempPaymentEntryPaymentStatus(int tempTxId, IPGResponseDTO ipgResponseDTO) throws ModuleException;

	/**
	 * Handle multiple payment responses
	 * 
	 * @param transactionID
	 * @return
	 * @throws ModuleException
	 */
	public boolean processMultiplePaymentResponses(int transactionID) throws ModuleException;

	/**
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public Date getPaymentRequestTime(int paymentBrokerRefNo) throws ModuleException;

	/**
	 * Get amount of the payment
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getTempPaymentAmount(int paymentBrokerRefNo) throws ModuleException;

	/**
	 * Get amount of the payment
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getTempPaymentCurrencyAmount(int paymentBrokerRefNo) throws ModuleException;

	/**
	 * Update the tempory payment entry payment status
	 * 
	 * @param colTnxIds
	 * @param status
	 * @param comment
	 * @param pnr
	 * @param alias
	 * @throws ModuleException
	 */
	public void updateTempPaymentEntry(Collection<Integer> colTnxIds, String status, String comment, String CCLastDigits,
			String pnr, String alias) throws ModuleException;

	public void updateTempPaymentEntryWithActualAmount(Collection<Integer> colTnxIds, String paymentSuccess, String comment,
			String ccLast4Digits, BigDecimal eDirhamFee) throws ModuleException;

	/**
	 * Update the tempory payment entries with payment status for bulk records
	 * 
	 * @param colTnxIds
	 * @param status
	 * @throws ModuleException
	 */
	public void updateTempPaymentEntriesForBulkRecords(Collection<Integer> colTnxIds, String status) throws ModuleException;

	/**
	 * Updates reservation payment after applying adjustment.
	 * 
	 * @param pnr
	 * @param iPassenger
	 * @param isForceConfirm
	 * @param pnrPaxFareId
	 * @param chargeRateID
	 * @param amount
	 * @param userNotes
	 * @param version
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce updateResForPaymentWithSeriveCharge(String pnr, IPassenger iPassenger, boolean isForceConfirm,
			boolean acceptMorePaymentsThanItsCharges, Integer pnrPaxFareId, Integer chargeRateID, BigDecimal amount,
			String userNotes, long version, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Gets a itinerary for print
	 * 
	 * @param itineraryLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	public String getItineraryForPrint(ItineraryLayoutModesDTO itineraryLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			TrackInfoDTO trackInfoDTO, boolean isCheckIndividual) throws ModuleException;

	public String getTaxInvoiceForPrint(List<TaxInvoice> taxInvoicesList, LCCClientReservation reservation)
			throws ModuleException;

	/**
	 * 
	 * @param pnr
	 * @param eticket
	 * @param segCodesOrderedByDep
	 * @return
	 * @throws ModuleException
	 */
	public String getBarcodeImagePathForItinerary(String pnr, List<String> segCodesOrderedByDep) throws ModuleException;

	/**
	 * Gets a reciept for print
	 * 
	 * @param RecioetLayoutModesDTO
	 * @param colSelectedPnrPaxIds
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	public String getRecieptForPrint(RecieptLayoutModesDTO recieptLayoutModesDTO, Collection<Integer> colSelectedPnrPaxIds,
			TrackInfoDTO trackInfoDTO, boolean isCheckIndividual) throws ModuleException;

	/**
	 * Gets a interline itinerary for print
	 * 
	 * @param lccClientReservation
	 * 
	 * @param itineraryDTO
	 * @throws ModuleException
	 */
	public String getInterlineItineraryForPrint(ItineraryDTO itineraryDTO) throws ModuleException;

	/**
	 * Returns the quoted charges information
	 * 
	 * @param colEXTERNAL_CHARGES
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public Map<EXTERNAL_CHARGES, ExternalChgDTO> getQuotedExternalCharges(Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES,
			TrackInfoDTO trackInfoDTO, Integer operationalaMode) throws ModuleException;

	/**
	 * Returns the initiated tempory payment information
	 * 
	 * @param date
	 * @param colIPGIdentificationDTO
	 * @return collection of IPGQueryDTO
	 * @throws ModuleException
	 */
	public Collection<IPGQueryDTO> getInitiatedPaymentInfo(Date date,
			Collection<IPGIdentificationParamsDTO> colIPGIdentificationDTO) throws ModuleException;

	/**
	 * Return the IPGQueryDTO which is tempory payment information
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public IPGQueryDTO getTemporyPaymentInfo(int paymentBrokerRefNo) throws ModuleException;

	/**
	 * 
	 * @param ipassengerSeating
	 * @param pnr
	 * @param userNotes
	 * @param version
	 * @param updatePayment
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce modifySeats(IPassengerSeating ipassengerSeating, String pnr, String userNotes, long version,
			boolean updatePayment, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * 
	 * @param mapTnxIds
	 * @param reservationContactInfo
	 * @param colPaymentInfo
	 * @param credentialsDTO
	 * @param isCredit
	 * @param isOfflinePayment
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Integer> makeTemporyPaymentEntry(Map<Integer, CardPaymentInfo> mapTnxIds,
			ReservationContactInfo reservationContactInfo, Collection<PaymentInfo> colPaymentInfo, CredentialsDTO credentialsDTO,
			boolean isCredit, boolean isOfflinePayment) throws ModuleException;

	/**
	 * 
	 * @throws ModuleException
	 */
	public void downloadPfsMessagesFromMailServer() throws ModuleException;

	/**
	 * 
	 * @param strMsgName
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, PFSMetaDataDTO> parsePfsDocument(String strMsgName) throws ModuleException;

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getValidatedPfsDocuments();

	/**
	 * 
	 * @param criteriaDTO
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, PNRExtTransactionsTO> getExtPayTransactions(ExtPayTxCriteriaDTO criteriaDTO) throws ModuleException;

	/**
	 * 
	 * @param externalPaymentTnx
	 * @param onAccAgentCode
	 * @param isManualRecon
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reconcileExtPayTransaction(ExternalPaymentTnx externalPaymentTnx, String onAccAgentCode,
			boolean isManualRecon) throws ModuleException;

	/**
	 * 
	 * @return
	 */
	public Collection<String> getValidatedXAPnlDocuments();

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<Integer> getFailedXAPnlIds() throws ModuleException;

	/**
	 * 
	 * @param pfs
	 * @throws ModuleException
	 */
	public void savePfs(Pfs pfs) throws ModuleException;

	/**
	 * save and return the id
	 * 
	 * @param pfs
	 * @return
	 * @throws ModuleException
	 */
	public int createPfs(Pfs pfs) throws ModuleException;

	/**
	 * 
	 * @param insuranceRequest
	 * @param pnr
	 * @param iPassenger
	 * @param forceConfirm
	 * @param paymentCompleted
	 * @param insuranceRequest
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce addInsurance(List<IInsuranceRequest> insuranceRequests, String pnr, IPassenger iPassenger,
			boolean forceConfirm, boolean paymentCompleted) throws ModuleException;

	/**
	 * 
	 * @param ipassengerMeal
	 * @param pnr
	 * @param userNotes
	 * @param version
	 * @param updatePayment
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce modifyMeals(IPassengerMeal ipassengerMeal, String pnr, String userNotes, long version,
			boolean updatePayment, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * 
	 * @param pnr
	 * @param version
	 * @return
	 * @throws ModuleException
	 */
	public long removeInsurance(String pnr, long version) throws ModuleException;

	/*
	 * Allows to make the payment for OA, Pax Credit and Credit card without creating the reservation. Implemented for
	 * recording payment for dry/interline bookings
	 * 
	 * Returns Map<Integer, Collection<TnxCreditPayment>> - for actual credit paymens paymentAssembler - for updated
	 * credit card references which is useful for refund operations TODO : we can send credit transactions in
	 * paymentAssembler.
	 */
	public ServiceResponce makePayment(TrackInfoDTO trackInfo, PaymentAssembler paymentAssembler, String pnr,
			ReservationContactInfo contactInfo, Collection<Integer> colTnxIds) throws ModuleException;

	/*
	 * Allows to reverse payment for OA, Pax Credit and Credit card without creating the reservation. Reverse is done by
	 * inserting refund records. Implemented for recording payment for dry/interline bookings
	 */
	public ServiceResponce refundPayment(TrackInfoDTO trackInfo, PaymentAssembler paymentAssembler, String pnr,
			ReservationContactInfo contactInfo, Collection<Integer> tempTnxIds) throws ModuleException;

	/**
	 * 
	 * @param externalReference
	 * @param status
	 * @param productType
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce isExternalReferenceExist(String externalReference, String status, char productType)
			throws ModuleException;

	/**
	 * 
	 * @param externalReference
	 * @param productType
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce isExternalReferenceExist(String externalReference, char productType) throws ModuleException;

	/**
	 * 
	 * @param temporyPaymentTO
	 * @return
	 * @throws ModuleException
	 */
	public TempPaymentTnx recordTemporyPaymentEntry(TemporyPaymentTO temporyPaymentTO) throws ModuleException;

	/**
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Collection<ReservationSegment> getSegments(String pnr) throws ModuleException;

	/**
	 * 
	 * @param pnrPaxIds
	 * @return
	 * @throws ModuleException
	 */
	public Collection<PaxAirportTransferTO> getReservationAirportTransfersByPaxId(Collection<Integer> pnrPaxIds)
			throws ModuleException;

	/**
	 * 
	 * @param pnrPaxId
	 * @return
	 */
	public List<ReservationPaxFare> getReservationPaxFare(Integer pnrPaxId);

	/**
	 * 
	 * @param reservationInsurance
	 * @return
	 */
	public Integer saveReservationInsurance(ReservationInsurance reservationInsurance);

	/**
	 * 
	 * @param pnr
	 * @param b
	 * @return
	 */
	public Reservation getReservation(String pnr, boolean loadFares);

	public Reservation getReservationNonTnx(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * 
	 * @param insuranceRefNumber
	 * @return
	 */
	public ReservationInsurance getReservationInsurance(int insuranceRefNumber);

	/**
	 * 
	 * @param airlineCode
	 * @return
	 */
	public Collection<String> getCarrierCodesByAirline(String airlineCode);

	/**
	 * Return Passenger final sales record
	 * 
	 * @param pfsID
	 * @return
	 * @throws ModuleException
	 */
	public int getPFS(String fromAirport, String flightNumber, Date departureDate) throws ModuleException;

	/**
	 * 
	 * @param reservationSegmentNotificationEventCol
	 * @throws ModuleException
	 */
	public void updatePnrSegmentNotifyStatus(
			Collection<ReservationSegmentNotificationEvent> reservationSegmentNotificationEventCol) throws ModuleException;

	/**
	 * 
	 * @param date
	 * @param schedulerType
	 * @return
	 * @throws ModuleException
	 */
	public List<AncillaryReminderDetailDTO> getFlightSegmentsToScheduleReminder(Date date, String schedulerType)
			throws ModuleException;

	/**
	 * 
	 * @param reminderDetailDTO
	 * @throws ModuleException
	 */
	public void sendAncillaryReminderNotification(AncillaryReminderDetailDTO reminderDetailDTO) throws ModuleException;

	/**
	 * 
	 * @param pnrSegId
	 * @return
	 */
	public Integer getReservationPaxFareId(Integer pnrSegId);

	/**
	 * 
	 * @param strAgentCode
	 * @return
	 * @throws ModuleException
	 */
	public String getCurrentEticketNo(String strAgentCode) throws ModuleException;

	/**
	 * 
	 * @param strAgentCode
	 * @return
	 * @throws ModuleException
	 */
	public String getCurrentTicketNoOnly(String strAgentCode) throws ModuleException;

	/**
	 * Retrives the last eticket for the agent
	 * 
	 * @param strAgentCode
	 * @return
	 * @throws ModuleException
	 */
	public String getLastActualEticket(String strAgentCode) throws ModuleException;

	/**
	 * 
	 * @param paxSSRMap
	 * @param pnr
	 * @param userNotes
	 * @param iPassenger
	 * @param version
	 * @param updatePayment
	 * @param extChargeType
	 * @param trackInfo
	 * @param cancelOnly
	 *            TODO
	 */
	public ServiceResponce modifySSRs(Map<Integer, SegmentSSRAssembler> paxSSRMap, String pnr, String userNotes,
			IPassenger iPassenger, Integer version, boolean updatePayment, EXTERNAL_CHARGES extChargeType, TrackInfoDTO trackInfo,
			boolean cancelOnly) throws ModuleException;

	/**
	 * 
	 * @param paxSSRMap
	 * @param pnr
	 * @param userNotes
	 * @param iPassenger
	 * @param version
	 * @param updatePayment
	 * @param extChargeType
	 * @param trackInfo
	 * @param cancelOnly
	 *            TODO
	 */
	public ServiceResponce modifyAirportTransfers(AirportTransferAssembler paxAirportTransfer, String pnr, String userNotes,
			IPassenger iPassenger, Integer version, boolean updatePayment, EXTERNAL_CHARGES extChargeType, TrackInfoDTO trackInfo,
			boolean cancelOnly) throws ModuleException;

	/**
	 * For Automatic checkin
	 * 
	 * @param ipaxAutoCheckin
	 * @param pnr
	 * @param userNotes
	 * @param version
	 * @param updatePayment
	 * @param trackInfo
	 */
	public ServiceResponce modifyAutoCheckins(IPassengerAutomaticCheckin ipaxAutoCheckin, String pnr, String userNotes,
			Integer version, boolean updatePayment, TrackInfoDTO trackInfo) throws ModuleException;

	/**
	 * 
	 * @param flightSegmentId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<RakInsuredTraveler> getTravellersForRakInsurancePublishing(int flightSegmentId) throws ModuleException;

	/**
	 * 
	 * @param flightSegmentId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<RakInsuredTraveler> getTravellersForDailyRakInsurancePublishing(Date bookingDate) throws ModuleException;

	/**
	 * 
	 * @param insurancePublisherDetailDTO
	 * @throws ModuleException
	 */
	public FlightSegNotificationEvent publishRakInuranceData(InsurancePublisherDetailDTO insurancePublisherDetailDTO)
			throws ModuleException;

	/**
	 * 
	 * @param insurancePublisherDetailDTO
	 * @throws ModuleException
	 */
	public String publishRakInuranceDailyData(InsurancePublisherDetailDTO insurancePublisherDetailDTO) throws ModuleException;

	/**
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public BigDecimal getInsuranceMaxValue(String pnr) throws ModuleException;

	/**
	 * 
	 * @param pnr
	 * @param reservationContactInfo
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public void sendSms(String pnr, ReservationContactInfo reservationContactInfo, CredentialsDTO credentialsDTO)
			throws ModuleException;

	public void updateTnxGranularityFlag(String pnr, String status, String message, Long version) throws ModuleException;

	public void migrateTnxForGranularity(String pnr);

	public String getActualPaymentMethodById(Integer actualPaymentMethodId) throws ModuleException;

	public int getPFSPaxTypeEntryCount(Integer pfsId, String pnr, String paxType) throws ModuleException;

	/**
	 * 
	 * @param originatorPNR
	 *            : The originator pnr - could be empty
	 * @param nextPNRSeqNumber
	 *            : The next pnr sequence number - could be empty.
	 * @return
	 */
	public String getNewPNR(boolean isGdsReservation, String nextPNRSeqNumber);

	/**
	 * 
	 * @throws ModuleException
	 */
	public boolean publishInsuranceSalesData(InsuranceSalesType salesType) throws ModuleException;

	public String getReservationCabinClass(String pnr, Integer pnrSegID) throws ModuleException;

	/**
	 * Updates the Originator PNR Field for the reservation with the given PNR
	 * 
	 * @param pnr
	 *            : The Originator PNR to be set and the PNR of the given reservation.
	 * 
	 * @throws ModuleException
	 */
	public long updateOriginatorPNRForReservation(String pnr) throws ModuleException;

	/**
	 * 
	 * @param ipassengerBaggage
	 * @param pnr
	 * @param userNotes
	 * @param version
	 * @param updatePayment
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce modifyBaggages(IPassengerBaggage ipassengerBaggage, String pnr, String userNotes, long version,
			boolean updatePayment, TrackInfoDTO trackInfoDTO) throws ModuleException;

	/**
	 * Updates release timestamp of a reservation (used for updating dummy reservation, for dry and dry interline
	 * reservations)
	 * 
	 * @param pnr
	 * @param releaseTimeStamp
	 * @throws ModuleException
	 */
	public void updateReleaseTimeForDummyReservation(String pnr, Date releaseTimeStamp) throws ModuleException;

	public boolean isReservationTnxGranularityEnable(String pnr) throws ModuleException;

	public boolean hasPayments(String pnr) throws ModuleException;

	/**
	 * Returns pfs parent entry
	 * 
	 * @param infant
	 *            pfsId
	 * @return PfsPaxEntry
	 * @throws ModuleException
	 */
	public PfsPaxEntry getPfsInfantPaxParent(int infantPPID) throws ModuleException;

	/**
	 * Returns pfs entry
	 * 
	 * @param ppId
	 * @return PfsPaxEntry
	 * @throws ModuleException
	 */
	public PfsPaxEntry getPfsPaxEntry(int ppId) throws ModuleException;

	public boolean checkSSRChargeAssignedInReservation(int ssrId) throws ModuleException;

	public void sendEmailMedicalSsr(CommonMedicalSsrParamDTO medicalSsrDTO) throws ModuleException;

	/**
	 * @param pnr
	 * @param passengerWiseEticketNumbers
	 * @throws ModuleException
	 */
	public void updatePassengerEtickets(String pnr, Map<Integer, Map<Integer, EticketDTO>> passengerWiseEticketNumbers,
			boolean exchangePreviousEtickets) throws ModuleException;

	/**
	 * @param pnr
	 * @param passengerWiseEticketNumbers
	 * @throws ModuleException
	 */
	public void exchangePassengerEtickets(String pnr, List<Integer> oldPnrSegmentIds, List<Integer> newSegmentsSequenceIds)
			throws ModuleException;

	public void exchangeEticketsForGivenPassengers(String pnr, List<Integer> oldPnrSegmentIds,
			List<Integer> newSegmentsSequenceIds, List<Integer> paxSeqsToExchangeET) throws ModuleException;

	/**
	 * @param passengerSequnce
	 * @param segmentSequnce
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbers(Collection<Integer> passengerSequnce,
			List<Integer> segmentSequnce, CreateTicketInfoDTO ticketInfoDto) throws ModuleException;

	public Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbers(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException;

	public String getHistoryForPrint(String pnr, Collection<UserNoteHistoryTO> colUserNoteHistoryTO) throws ModuleException;

	public ETicketUpdateResponseDTO updateETicketStatus(ETicketUpdateRequestDTO eTicketUpdateRequestDTO) throws ModuleException;

	/**
	 * Roll Forward Selected OHD booking for given date range
	 * 
	 * @param sourcePnr
	 * @param availableFlightSearchDTO
	 * @param reservationSegmentDTOs
	 * @param rollForwardingFlights
	 * @param userId
	 * @param trackInfoDTO
	 * @param isDuplicateNameExist
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce rollForwardOnholdBooking(String sourcePnr, AvailableFlightSearchDTO availableFlightSearchDTO,
			List<ReservationSegmentDTO> reservationSegmentDTOs, Map<String, RollForwardFlightRQ> rollForwardingFlights,
			String userId, TrackInfoDTO trackInfoDTO, boolean duplicateNameSkip) throws ModuleException;

	public Object[] reconcilePfsEntry(PfsPaxEntry pfsParsedEntry, Collection<PfsPaxEntry> colErrorPfsParsedEntry,
			CredentialsDTO credentialsDTO, String checkinMethod, Map<Integer, FlightReconcileDTO> allFlightReconcileDTOs, Pfs pfs)
			throws ModuleException;

	public void sendIBEOnholdReservationEmail(OnholdReservatoinSearchDTO onHoldReservationSearchDTO) throws ModuleException;

	public void updateOnHoldAlert(String pnr, char newStatus) throws ModuleException;

	public boolean checkIfPfsAlreadyExists(Date date) throws ModuleException;

	public boolean checkIfPfsAlreadyExists(Date departureDate, String flightNumber, String fromAirport) throws ModuleException;

	public void saveOrUpdate(Collection<ReservationPaxFareSegmentETicket> eTickets) throws ModuleException;

	public ReservationPaxFareSegmentETicket getPaxFareSegmentEticket(String eticketNumber, int coupNumber, String msgType)
			throws ModuleException;

	public String getBookingClassType(String eticketNumber, int coupNumber) throws ModuleException;

	public EticketTO getEticketInfo(String eticketNumber, Integer coupNumber) throws ModuleException;

	public Integer updatePfs(PfsXmlDTO pfsXmlDTO) throws ModuleException;

	public void sendBookingChangesEmailToAgent(Collection<String> pnrCollection, String agentCode, String modificationType,
			Map<String, Map<String, Date>> onHoldTimeMap) throws ModuleException;

	/**
	 * Requote balance calculation query
	 * 
	 * @param balanceQueryTO
	 * @param userPrincipal
	 * @param trackInfo
	 * @return
	 * @throws ModuleException
	 */
	public ReservationBalanceTO getRequoteBalanceSummary(RequoteBalanceQueryRQ balanceQueryTO, UserPrincipal userPrincipal,
			TrackInfoDTO trackInfo) throws ModuleException;

	/**
	 * Modify segments using requote
	 * 
	 * @param requoteModifyRQ
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce requoteModifySegments(RequoteModifyRQ requoteModifyRQ, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public void changeReservationCabinClass(FlightSegement seg, Collection<String> cabinClassesToBeDeleted,
			Map<String, Integer> availableCabinClasses) throws ModuleException;

	public boolean isAllowVoidReservation(Reservation reservation) throws ModuleException;

	public void saveOrUpdateOnholdReleaseTime(OnholdReleaseTime onholdReleaseTime, Map<String, String[]> contMap)
			throws ModuleException;

	public void deleteOnholdReleaseTimeConfig(int releaseTimeId) throws ModuleException;

	public Page getOnholdReleaseTimes(ConfigOnholdTimeLimitsSearchDTO configOnholdTimeLimitsSearchDTO, int start, int recSize)
			throws ModuleException;

	public boolean isOhdRelCfgLegal(OnholdReleaseTime onholdRelTime) throws ModuleException;

	public OnholdReleaseTime getOnholdReleaseTime(int releaseTimeId) throws ModuleException;

	public Integer getAppliedFareDiscountPercentage(String pnr);

	public AutoCancellationInfo getAutoCancellationInfo(String pnr, Reservation reservation, Date firstDepartureTime,
			Collection<Integer> modifiedSegIds, String cancellationType, boolean hasBufferTimePrivilege,
			boolean getExistingCnxTimeOnly) throws ModuleException;

	public AutoCancellationInfo saveAutoCancellationInfo(AutoCancellationInfo autoCancellationInfo, String pnr,
			TrackInfoDTO trackInfo) throws ModuleException;

	/**
	 * 
	 * Auto cancel segments, infant and ancillaries with expired time
	 * 
	 * @param currentDate
	 * @param customAdultCancelCharge
	 * @param customChildCancelCharge
	 * @param customInfantCancelCharge
	 * @throws ModuleException
	 */
	public void executeAutoCancellation(Date currentDate, BigDecimal customAdultCancelCharge, BigDecimal customChildCancelCharge,
			BigDecimal customInfantCancelCharge) throws ModuleException;

	/**
	 * Auto cancellation flag will be removed from reservation, when due amount is settled
	 * 
	 * @param pnr
	 * @param trackInfoDTO
	 * @throws ModuleException
	 */
	public ServiceResponce removeAutoCancellation(String pnr, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public void confirmWaitListedReservations(List<WLConfirmationRequestDTO> wlRequests) throws ModuleException;

	public List<Integer> getExpiredCancellationIDs(Date currentDateTime) throws ModuleException;

	public Map<String, Integer> getAutoCancellableInfo(String marketingCarrier, Collection<Integer> autoCnxIds)
			throws ModuleException;

	public void updateAutoCancelSchedulerStatus(List<Integer> autoCnxIds, String schedulerStatus) throws ModuleException;

	public BigDecimal calculateBalanceToPayAfterAutoCancellation(Map<String, Collection<Integer>> groupWiseSegIds,
			List<Integer> autoCnxInfants, Map<String, CustomChargesTO> groupWiseCustomCharges, String pnr, Long version)
			throws ModuleException;

	public void saveAuditNextSeatPromotionRequest(String pnr, String status, Map<String, String> contentMapForAudit)
			throws ModuleException;

	public void saveAuditFlexiDatePromotionRequest(String pnr, String status, Map<String, Object> contentMapForAudit)
			throws ModuleException;

	public void saveAuditPromotionRequestSubcriptions(PromotionType promotionType, String pnr, String status,
			Map<String, Object> contentMapForAudit, Map<Integer, List<String>> segCodesDirectionWise) throws ModuleException;

	public List<OnlineCheckInReminderDTO> getDateForOnlineCheckInReminder(Date date) throws ModuleException;

	public void sendOnlineCheckInReminder(OnlineCheckInReminderDTO onlineCheckInReminderDTO) throws ModuleException;

	public void deleteAllErrorEntries(Pfs pfs) throws ModuleException;

	public boolean isNOSHOPaxEntry(String eTicketNo, String paxStatus, String segmentCode) throws ModuleException;

	public boolean isFlownOrNoShowPaxEntry(String eTicketNo, String segmentCode) throws ModuleException;

	public Collection<ReservationWLPriority> getWaitListedReservations(Integer flightSegId) throws ModuleException;

	public List<ReservationWLPriority> getWaitListedPrioritiesForSegment(Integer pnrSegmentId) throws ModuleException;

	public void modifyWaitListedPriority(String pnr, Integer priority, Integer fltSegId, long version, TrackInfoDTO trackInfoDTO)
			throws ModuleException;

	public Map<Integer, Collection<Integer>> getPaxwiseFlownSegments(Reservation reservation);

	public Collection<ReservationPaxDTO> searchReservations(ReservationSearchDTO searchDTO);

	/**
	 * Process NOSHOW Auto Refund Process
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce processNSAutoRefundPNRs() throws ModuleException;

	public boolean isReservationAvailable(long promoCriteriaID);

	public List<OnlineCheckInReminderDTO> getFlightSegsForOnlineCheckInReminder(Date date, String schedulerType)
			throws ModuleException;

	public List<ReservationBasicsDTO> getFlightSegmentPAXDetails(Integer flightSegId, String logicalCabinClass)
			throws ModuleException;

	public ServiceResponce updateExternalEticketInfo(List<EticketTO> eticketTOs) throws ModuleException;

	public List<PNRGOVResInfoDTO> getResInfoListForFlightSegment(Integer flightSegmentID);

	public void publishPNRGOV(int flightSegmentID, String contryCode, String airportCode, int timePeriod, String inboundOutbound)
			throws ModuleException;

	public List<PNRGOVSegmentDTO> getSegmentsForPNR(String pnr, String carrierCode);

	public Collection<GroupPaymentNotificationDTO> getGrooupBookingForReminders();

	public void sendTotalPaymentDueReminder();

	public List<ReservationDTO> getImmediateReservations(int customerId, int count) throws ModuleException;

	public boolean applyAgentCommissionForOnd(PreferenceAnalyzer preferenceAnalyzer, String ondCode, List<String> journeyOnds,
			List<List<String>> journeyOndSummary);

	public ServiceTaxContainer getApplicableServiceTax(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			EXTERNAL_CHARGES taxExternalChargeCode) throws ModuleException;

	public ServiceTaxContainer getApplServiceTaxContainer(ReservationSegmentDTO reservationSegmentDTO,
			EXTERNAL_CHARGES taxExternalChargeCode) throws ModuleException;

	public void getPaymentInfo(PaymentAssembler paymentAssembler, String pnr) throws ModuleException;

	public void mergeGdsReservationAttributes(GdsReservationAttributesDto gdsReservationAttributesDto) throws ModuleException;

	void updatePassengerCoupons(Integer flightId, String airport) throws ModuleException;

	public void getPfsFilesViaSitaTex(List<String> messages) throws ModuleException;

	public EticketTO getEticketDetailsByExtETicketNumber(String extEticketNumber, Integer extCoupNumber) throws ModuleException;

	public boolean isCodeSharePNR(String extEticketNumber) throws ModuleException;

	public void cancelAirportTransfers(Collection<Integer> pnrSegIds) throws ModuleException;

	public void cancelAirportTransferRequests(Reservation reservation, Collection<Integer> pnrSegmentIds,
			Map<Integer, Collection<PaxAirportTransferTO>> cancelledSSRMap, CredentialsDTO credentialsDTO);

	public ArrayList<Reservation> loadReservationsHavingAirportTransfers(List<Integer> flightSegIds) throws ModuleException;

	public void sendIBEOnholdPaymentReminderEmail(String pnr, boolean isGroupPnr, boolean isCreateBooking) throws ModuleException;

	public List<OnHoldBookingInfoDTO> getPnrInformationToSendPaymentReminders(Date toDate) throws ModuleException;

	public void sendGroupBookingNotificationMessages(LCCClientReservation ownReservation, String action) throws ModuleException;

	public ServiceResponce updatePaxNames(Map<Integer, NameDTO> changedPaxNamesMap, String pnr, long version, String appIndicator,
			boolean skipDuplicateNameCheck, TrackInfoDTO trackInfoDTO, String reasonForAllowBlPax) throws ModuleException;

	public Page getOfficersMobileNumbers(OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTOint, int startIndex,
			int pageLength) throws ModuleException;

	public Collection<String> getOfficersEmailList() throws ModuleException;

	public ReservationDiscountDTO calculateDiscount(DiscountRQ promotionRQ, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public Map<Integer, Set<ChargesDetailDTO>> getPnrPaxSeqWiseBalanceDueCharges(LCCClientPnrModesDTO pnrModesDTO)
			throws ModuleException;

	public void deleteOfficersMobileNumbersConfig(int OfficerId) throws ModuleException;

	public OfficersMobileNumbers getOfficersMobNums(int releaseTimeId) throws ModuleException;

	public void saveOrUpdateOfficersMobileNumbersConfig(OfficersMobileNumbers OfficersMobNums, Map<String, String[]> contMap)
			throws ModuleException;

	public List<String> getOfficersMobileNumbersList() throws ModuleException;

	public ServiceResponce syncAATotalGroupChargesWithCarrier(GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ, TrackInfoDTO trackInfo)
			throws ModuleException;

	public Collection<Integer> getPaxFareSegmentETicketIDs(Collection<Integer> pnrSegIds) throws ModuleException;

	public Map<Integer, Map<Integer, EticketDTO>> generateIATAETicketNumbersAsSubstitution(Reservation reservation,
			CreateTicketInfoDTO ticketingInfoDto) throws ModuleException;

	public Collection<Reservation> getReservationsByPnr(Collection<String> pnrList, boolean loadFares) throws ModuleException;

	public Collection<Reservation> getReservationsByFlightSegmentId(Integer flightSegId, boolean loadFares)
			throws ModuleException;

	public Collection<CSPaxBookingCount> getCodeSharePaxCountByBookingClass(String csFlightNumber, Date departureDate,
			String carrierCode) throws ModuleException;

	public void saveCodeSharePfsEntry(CodeSharePfs pfs) throws ModuleException;

	public boolean checkCodeSharePfsAlreadySent(Date departureDate, String flightNumber, String fromAirport, String carrierCode)
			throws ModuleException;

	public ServiceResponce detectReservationModification(Reservation reservationBeforeModify) throws ModuleException;

	public Collection<String> removeFFIDFromUnflownPNRs(String ffid, TrackInfoDTO trackInfoDTO) throws ModuleException;

	public Reservation loadReservationByPnrPaxId(int pnrPaxId) throws ModuleException;

	public void invokeTestInvoiceCall(String pnr) throws ModuleException;

	public List<TaxInvoice> getTaxInvoiceFor(List<String> invoiceNumbers) throws ModuleException;

	public List<TaxInvoice> getTaxInvoiceFor(String pnr) throws ModuleException;

	public List<TaxInvoice> getTaxInvoiceForPrintInvoice(String pnr) throws ModuleException;

	public Integer recordTemporyPaymentEntryForOffline(ReservationContactInfo reservationContactInfo,
			OfflinePaymentInfo offlinePaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit, boolean isOfflinePayment,
			String pnr) throws ModuleException;

	public void handleBalanceAmountRequestForOfflinePGW(ReservationContactInfo reservationContactInfo,
			OfflinePaymentInfo offlinePaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit, boolean isOfflinePayment,
			CreditCardPaymentStatusDTO creditCardPaymentStatusDTO, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException;

	public Collection<Integer> makeTemporyPaymentEntry(Map<Integer, CardPaymentInfo> mapTnxIds,
			ReservationContactInfo reservationContactInfo, Collection<PaymentInfo> colPaymentInfo, TrackInfoDTO trackInfo,
			boolean isCredit, boolean isOfflinePayment) throws ModuleException;

	public void saveLmsBlockCreditInfo(LmsBlockedCredit lmsBlockedCredit) throws ModuleException;

	public LmsBlockedCredit getLmsBlockCreditInfoByTmpTnxId(Integer tempTnxId) throws ModuleException;

	public void cancelFailedLMSBlockedCreditPayments() throws ModuleException;

	public void updateLMSCreditUtilizationSuccess(Integer tempTnxId) throws ModuleException;

	public void updateLMSCreditUtilizationFailed(Integer tempTnxId) throws ModuleException;

	public void cancelFailedLMSBlockedCreditPaymentsByTempIds(Collection<Integer> tempTnxIds) throws ModuleException;

	public Collection<Integer> getLMSBlockedTnxTempPayIdsByPnr(Collection<String> pnrs);

	public TempPaymentTnx recordTemporyPaymentEntry(TemporyPaymentTO temporyPaymentTO, TrackInfoDTO trackInfo)
			throws ModuleException;

	public void changeLMSBlockCreditStatusToCancel(String[] rewardIDs, String pnr, String memberId) throws ModuleException;

	public void updateLMSCreditUtilizationSuccess(String[] rewardIDs) throws ModuleException;

	public void clearIssuedUnConsumedLMSCreditPayments() throws ModuleException;

	public LmsBlockedCredit getLmsBlockCreditInfoByRewardId(String[] rewardIDs) throws ModuleException;

	/**
	 * Return the relevant reprotect flights for the given alertId
	 * 
	 * @param alertId
	 * @return reprotect flights for the given alertId
	 * @throws ModuleException
	 */
	public Collection<SelfReprotectFlightDTO> getSelfReprotectFlights(String alertId) throws ModuleException;

	/**
	 * This will check if segment transfer is done using IBE
	 * 
	 * @param pnr
	 * @return true if self reprotection is done
	 * @throws ModuleException
	 */
	public boolean hasActionedByIBE(String pnr) throws ModuleException;

	public List<ReservationPaxDetailsDTO> getPAXDetails(Integer flightId, int page) throws ModuleException;

	public Integer getPAXDetailsCount(Integer flightId) throws ModuleException;

	public List<FlightConnectionDTO> getConnectionDetials(String pnr) throws ModuleException;

	public ExternalChgDTO getHandlingFeeForModification(Integer operationalMode) throws ModuleException;

    public void updateReservationContactData(ReservationContactInfo reservationContactInfo);

}
