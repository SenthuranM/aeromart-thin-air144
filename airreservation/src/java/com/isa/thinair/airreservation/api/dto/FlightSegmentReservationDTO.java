package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;

public class FlightSegmentReservationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9213358885205967157L;

	private String segmentCode;

	private int onholdPnrs;

	private int confirmedPnrs;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getConfirmedPnrs() {
		return confirmedPnrs;
	}

	public void setConfirmedPnrs(int confirmedPnrs) {
		this.confirmedPnrs = confirmedPnrs;
	}

	public int getOnholdPnrs() {
		return onholdPnrs;
	}

	public void setOnholdPnrs(int onholdPnrs) {
		this.onholdPnrs = onholdPnrs;
	}

}
