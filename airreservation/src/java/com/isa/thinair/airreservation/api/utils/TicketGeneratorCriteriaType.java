/**
 * 
 */
package com.isa.thinair.airreservation.api.utils;

public enum TicketGeneratorCriteriaType {

	AGNET_WISE_PAPER_TKT("AWPT", "Agent wise paper etickets"), BSP_TKT("BSPT", "BSP e tickets"), DEF_TKT("DFLT",
			"Default e ticket"), BSP_EMDS("EMDS",
					"EMD Standalone");

	private TicketGeneratorCriteriaType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	private final String code;
	private final String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static TicketGeneratorCriteriaType getPaymentType(String code) {
		for (TicketGeneratorCriteriaType tktType : TicketGeneratorCriteriaType.values()) {
			if (tktType.getCode().equals(code)) {
				return tktType;
			}
		}
		return null;
	}
}