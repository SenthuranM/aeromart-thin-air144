package com.isa.thinair.airreservation.api.utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAirportTransfer;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxSegmentSSRStatus;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class ReservationSSRUtil {
	public static void addPaxSegmentSSRS(Reservation reservation,
			Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps) throws ModuleException {
		if (paxSegmentSSRDTOMaps != null) {
			Set<ReservationPax> passengers = reservation.getPassengers();

			for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
				ReservationPax pax = (ReservationPax) itPax.next();				
				if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap = paxSegmentSSRDTOMaps.get(pax.getPaxSequence());
					if (segmentSSRDTOMap != null) {
						addPaxSegmentSSRs(pax, segmentSSRDTOMap, null);
					}
				}				
			}
		}
	}

	public static void updatePaxSegmentSSRs(Reservation reservation, Map<Integer, SegmentSSRAssembler> paxSSRAssemblerMap,
			boolean isNonStdSSR, Collection<ReservationPaxSegmentSSR> addedPaxSegSSrs) throws ModuleException {
		Set<ReservationPax> passengers = reservation.getPassengers();

		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();
			updatePaxSegmentSSRs(pax, paxSSRAssemblerMap.get(pax.getPaxSequence()), isNonStdSSR, addedPaxSegSSrs);
		}

	}

	public static void updatePaxSegmentSSRs(ReservationPax passenger, SegmentSSRAssembler ssrAssembler, boolean isNonStdSSR,
			Collection<ReservationPaxSegmentSSR> addedPaxSegSSrs) throws ModuleException {
		Map<Integer, Collection<PaxSegmentSSRDTO>> newSegmentSSRDTOMap = null;
		Map<Integer, PaxSegmentSSRDTO> updatedSegmentSSRDTOMap = null;
		Map<Integer, Integer> canceledSegmentSSRIdMap = null;

		Set<ReservationPaxFare> paxFares = passenger.getPnrPaxFares();

		if (ssrAssembler != null) {
			newSegmentSSRDTOMap = ssrAssembler.getNewSegmentSSRDTOMap();
			updatedSegmentSSRDTOMap = ssrAssembler.getUpdatedSegmentSSRDTOMap();
			canceledSegmentSSRIdMap = ssrAssembler.getCanceledSegmentSSRIdMap();

			for (Iterator<ReservationPaxFare> itPaxFares = paxFares.iterator(); itPaxFares.hasNext();) {
				ReservationPaxFare reservationPaxFare = (ReservationPaxFare) itPaxFares.next();

				Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();

				for (Iterator<ReservationPaxFareSegment> itPaxFareSeg = paxFareSegments.iterator(); itPaxFareSeg.hasNext();) {
					ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) itPaxFareSeg.next();

					// add SSRS create booking
					Collection<PaxSegmentSSRDTO> newSegmentSSRDTOs = newSegmentSSRDTOMap.get(Integer.valueOf(-1));

					if (newSegmentSSRDTOs == null) {
						newSegmentSSRDTOs = newSegmentSSRDTOMap.get(reservationPaxFareSegment.getSegment().getSegmentSeq());
					}

					addReservationPaxFareSegmentSSRs(reservationPaxFareSegment, newSegmentSSRDTOs, addedPaxSegSSrs, null);

					// add SSRS modify booking
					PaxSegmentSSRDTO newModSegmentSSRDTO = updatedSegmentSSRDTOMap.get(Integer.valueOf(-1));

					if (newModSegmentSSRDTO == null) {
						newModSegmentSSRDTO = updatedSegmentSSRDTOMap.get(reservationPaxFareSegment.getSegment().getSegmentSeq());
					}

					// addOrUpdatePaxFareSegmentSSRs(reservationPaxFareSegment, newModSegmentSSRDTO, isNonStdSSR);

					// update/cancel SSRs
					Set<ReservationPaxSegmentSSR> segmentSSRs = reservationPaxFareSegment.getReservationPaxSegmentSSRs();

					for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs.hasNext();) {
						ReservationPaxSegmentSSR segmentSSR = (ReservationPaxSegmentSSR) itSegmentSSRs.next();
						// updating SSRs
						PaxSegmentSSRDTO paxSegmentSSRDTO = updatedSegmentSSRDTOMap.get(segmentSSR.getPnrPaxSegmentSSRId());

						if (paxSegmentSSRDTO != null) {
							segmentSSR.setSSRId(paxSegmentSSRDTO.getSSRId());
							segmentSSR.setSSRText(paxSegmentSSRDTO.getSSRText());
							// FIXME: SUDHEERA other properties??
						}

						// canceling SSRs
						Integer segmentSSRId = canceledSegmentSSRIdMap.get(segmentSSR.getPnrPaxSegmentSSRId());

						if (segmentSSRId != null) {
							segmentSSR.setStatus(ReservationPaxSegmentSSRStatus.CANCEL.getCode());
							segmentSSR.setAutoCancellationId(null);
						}

					}
				}
			}
		}
	}

	public static void addPaxSegmentSSRs(ReservationPax passenger, Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap,
			PaxSegmentSSRDTO ssr) throws ModuleException {
		Set<ReservationPaxFare> paxFares = passenger.getPnrPaxFares();
		for (Iterator<ReservationPaxFare> itPaxFares = paxFares.iterator(); itPaxFares.hasNext();) {
			ReservationPaxFare reservationPaxFare = (ReservationPaxFare) itPaxFares.next();

			Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();

			for (Iterator<ReservationPaxFareSegment> itPaxFareSeg = paxFareSegments.iterator(); itPaxFareSeg.hasNext();) {
				ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) itPaxFareSeg.next();
				if (segmentSSRDTOMap != null) {
					Collection<PaxSegmentSSRDTO> segmentSSRs = segmentSSRDTOMap.get(new Integer(-1));

					if (segmentSSRs == null) {
						segmentSSRs = segmentSSRDTOMap.get(reservationPaxFareSegment.getSegment().getSegmentSeq());
					}

					addReservationPaxFareSegmentSSRs(reservationPaxFareSegment, segmentSSRs, null, ssr);
				}
			}
		}
	}

	private static PaxSegmentSSRDTO getGloballyAppliedSSRComment(Set<ReservationPaxFare> paxFares,
			List<SSRExternalChargeDTO> anciDetails, List<String> ssrCodes,
			Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap) {

		PaxSegmentSSRDTO commonSSRDTO = null;
		for (Iterator<ReservationPaxFare> itPaxFares = paxFares.iterator(); itPaxFares.hasNext();) {
			ReservationPaxFare reservationPaxFare = (ReservationPaxFare) itPaxFares.next();

			Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();
			for (Iterator<ReservationPaxFareSegment> itPaxFareSeg = paxFareSegments.iterator(); itPaxFareSeg.hasNext();) {
				ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) itPaxFareSeg.next();

				Collection<PaxSegmentSSRDTO> segmentSSRs = segmentSSRDTOMap.get(new Integer(-1));
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationPaxFareSegment.getSegment()
						.getStatus())) {
					if (segmentSSRs == null) {
						segmentSSRs = segmentSSRDTOMap.get(reservationPaxFareSegment.getSegment().getSegmentSeq());
					}

					if (segmentSSRs != null) {
						for (Iterator<PaxSegmentSSRDTO> itSegSSr = segmentSSRs.iterator(); itSegSSr.hasNext();) {
							PaxSegmentSSRDTO paxSegmentSSRDTO = (PaxSegmentSSRDTO) itSegSSr.next();

							try {
								List<com.isa.thinair.airmaster.api.model.SSR> ssr = ReservationModuleUtils.getSsrServiceBD()
										.getSSR(ssrCodes.get(0));
								if (anciDetails != null) {
									for (SSRExternalChargeDTO ssrDTO : anciDetails) {
										if (paxSegmentSSRDTO.getSegmentCode() != null
												&& paxSegmentSSRDTO.getSegmentCode().equals(ssrDTO.getSegmentCode())) {
											if (ssr.get(0).getSsrId() == paxSegmentSSRDTO.getSSRId()) {
												if (paxSegmentSSRDTO.getSSRId().intValue() != ssrDTO.getSSRId().intValue()
														|| !paxSegmentSSRDTO.getSSRText().equals(ssrDTO.getSSRText())) {

													return paxSegmentSSRDTO;
												}
											}
										}
									}
								} else {
									return null;
								}
							} catch (ModuleException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}
				}
			}
		}

		return commonSSRDTO;
	}

	private static void addReservationPaxFareSegmentSSRs(ReservationPaxFareSegment reservationPaxFareSegment,
			Collection<PaxSegmentSSRDTO> segmentSSRs, Collection<ReservationPaxSegmentSSR> addedPaxSegSSrs, PaxSegmentSSRDTO ssr)
			throws ModuleException {
		if (segmentSSRs != null) {
			for (Iterator<PaxSegmentSSRDTO> itSegSSr = segmentSSRs.iterator(); itSegSSr.hasNext();) {
				PaxSegmentSSRDTO paxSegmentSSRDTO = (PaxSegmentSSRDTO) itSegSSr.next();

				addPaxFareSegmentSSRs(reservationPaxFareSegment, paxSegmentSSRDTO, addedPaxSegSSrs, ssr);
			}
		} else {
			addPaxFareSegmentSSRs(reservationPaxFareSegment, null, addedPaxSegSSrs, ssr);
		}

	}

	private static void addPaxFareSegmentSSRs(ReservationPaxFareSegment reservationPaxFareSegment,
			PaxSegmentSSRDTO paxSegmentSSRDTO, Collection<ReservationPaxSegmentSSR> addedPaxSegSSrs, PaxSegmentSSRDTO ssr)
			throws ModuleException {
		ReservationPaxSegmentSSR paxSegmentSSR = createPNRPaxSegmentSSR(paxSegmentSSRDTO, ssr);
		if (paxSegmentSSR != null) {
			if (addedPaxSegSSrs != null) {
				addedPaxSegSSrs.add(paxSegmentSSR);
			}
			if (ssr != null) {
				if (reservationPaxFareSegment.getReservationPaxSegmentSSRs() != null) {
					reservationPaxFareSegment.setReservationPaxSegmentSSRs(null);
					reservationPaxFareSegment.addPaxSegmentSSR(paxSegmentSSR);
				} else {
					reservationPaxFareSegment.addPaxSegmentSSR(paxSegmentSSR);
				}
			} else {
				reservationPaxFareSegment.addPaxSegmentSSR(paxSegmentSSR);
			}
		}
	}

	private static ReservationPaxSegmentSSR createPNRPaxSegmentSSR(PaxSegmentSSRDTO paxSegmentSSRDTO, PaxSegmentSSRDTO ssr)
			throws ModuleException {
		ReservationPaxSegmentSSR paxSegmentSSR = new ReservationPaxSegmentSSR();
		if (paxSegmentSSRDTO != null) {
			if (ssr != null && paxSegmentSSRDTO.getSSRId().intValue() == ssr.getSSRId().intValue()) {
				paxSegmentSSR.setSSRId(ssr.getSSRId());
				paxSegmentSSR.setSSRText(ssr.getSSRText());
			} else {
				paxSegmentSSR.setSSRId(paxSegmentSSRDTO.getSSRId());
				paxSegmentSSR.setSSRText(paxSegmentSSRDTO.getSSRText());
			}
			paxSegmentSSR.setContextId(paxSegmentSSRDTO.getContextId());
			paxSegmentSSR.setExternalReference(paxSegmentSSRDTO.getExternalReference());
			paxSegmentSSR.setApplyOn(paxSegmentSSRDTO.getApplyOn() == null
					? ReservationPaxSegmentSSR.APPLY_ON_SEGMENT
					: paxSegmentSSRDTO.getApplyOn());

			paxSegmentSSR.setChargeAmount((paxSegmentSSRDTO.getChargeAmount() == null) ? AccelAeroCalculator
					.getDefaultBigDecimalZero() : paxSegmentSSRDTO.getChargeAmount());
			paxSegmentSSR.setEmailStatus(CommonsConstants.NO);
			paxSegmentSSR.setPNLADLStatus(CommonsConstants.NO);

			paxSegmentSSR.setStatus(ReservationInternalConstants.ReservationPaxSegmentSSRStatus.CONFIRM.getCode());
			paxSegmentSSR.setAirportCode(paxSegmentSSRDTO.getAirportCode());
			paxSegmentSSR.setAutoCancellationId(paxSegmentSSRDTO.getAutoCancellationId());
			createPNRPaxSegmentSSRInfo(paxSegmentSSR, paxSegmentSSRDTO);
			return paxSegmentSSR;
		} else {
			if (ssr != null) {
				paxSegmentSSRDTO = new PaxSegmentSSRDTO();
				paxSegmentSSR.setSSRId(ssr.getSSRId());
				paxSegmentSSR.setSSRText(ssr.getSSRText());
				paxSegmentSSR.setApplyOn(paxSegmentSSRDTO.getApplyOn() == null
						? ReservationPaxSegmentSSR.APPLY_ON_SEGMENT
						: paxSegmentSSRDTO.getApplyOn());

				paxSegmentSSR.setChargeAmount((paxSegmentSSRDTO.getChargeAmount() == null) ? AccelAeroCalculator
						.getDefaultBigDecimalZero() : paxSegmentSSRDTO.getChargeAmount());
				paxSegmentSSR.setEmailStatus(CommonsConstants.NO);
				paxSegmentSSR.setPNLADLStatus(CommonsConstants.NO);

				paxSegmentSSR.setStatus(ReservationInternalConstants.ReservationPaxSegmentSSRStatus.CONFIRM.getCode());
				createPNRPaxSegmentSSRInfo(paxSegmentSSR, paxSegmentSSRDTO);
				return paxSegmentSSR;
			}
		}

		return null;
	}

	private static void createPNRPaxSegmentSSRInfo(ReservationPaxSegmentSSR paxSegmentSSR, PaxSegmentSSRDTO paxSegmentSSRDTO)
			throws ModuleException {

		if (isValidPnrPaxSegmentInfo(paxSegmentSSRDTO)) {
			ReservationPaxSegAirportTransfer paxSegmentSsrInfo = new ReservationPaxSegAirportTransfer();
			paxSegmentSsrInfo.setAddress(paxSegmentSSRDTO.getTransferAddress());
			paxSegmentSsrInfo.setContactNo(paxSegmentSSRDTO.getTransferContactNo());
			paxSegmentSsrInfo.setPickupType(paxSegmentSSRDTO.getTransferType());
			paxSegmentSsrInfo.setBookingTimestamp(new Date());
			try {
				Date requestTimeStamp = CalendarUtil.getParsedTime(paxSegmentSSRDTO.getTransferDate(), CalendarUtil.PATTERN8);
				paxSegmentSsrInfo.setRequestTimeStamp(requestTimeStamp);
			} catch (ParseException e) {
				throw new ModuleException("airreservations.airport.tranfer.date.invalid");
			}
		}

	}

	private static boolean isValidPnrPaxSegmentInfo(PaxSegmentSSRDTO paxSegmentSSRDTO) {
		boolean valid = false;
		if (PlatformUtiltiies.isNotEmptyString(paxSegmentSSRDTO.getTransferDate())
				&& PlatformUtiltiies.isNotEmptyString(paxSegmentSSRDTO.getTransferAddress())
				&& PlatformUtiltiies.isNotEmptyString(paxSegmentSSRDTO.getTransferContactNo())) {
			valid = true;
		}
		return valid;
	}

	private static Map<Integer, Collection<ReservationPaxSegmentSSR>> getPaxSegmentSSRMap(ReservationPax reservationPax) {
		Map<Integer, Collection<ReservationPaxSegmentSSR>> mapSegmentSSR = new HashMap<Integer, Collection<ReservationPaxSegmentSSR>>();
		Collection<ReservationPaxFare> colFare = (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares();

		for (Iterator<ReservationPaxFare> itFare = colFare.iterator(); itFare.hasNext();) {
			ReservationPaxFare fare = (ReservationPaxFare) itFare.next();
			Set<ReservationPaxFareSegment> colFareSegment = fare.getPaxFareSegments();

			for (Iterator<ReservationPaxFareSegment> itPaxSegment = colFareSegment.iterator(); itPaxSegment.hasNext();) {
				ReservationPaxFareSegment fareSegment = (ReservationPaxFareSegment) itPaxSegment.next();
				Set<ReservationPaxSegmentSSR> colSegmentSSR = fareSegment.getReservationPaxSegmentSSRs();

				mapSegmentSSR.put(fareSegment.getSegment().getSegmentSeq(), colSegmentSSR);
			}

		}

		return mapSegmentSSR;
	}

	private static Map<Integer, ReservationPaxSegmentSSR> getPaxSegmentSSRByIdMap(ReservationPax reservationPax) {
		Map<Integer, ReservationPaxSegmentSSR> mapSegmentSSR = new HashMap<Integer, ReservationPaxSegmentSSR>();
		Collection<ReservationPaxFare> colFare = (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares();

		for (Iterator<ReservationPaxFare> itFare = colFare.iterator(); itFare.hasNext();) {
			ReservationPaxFare fare = (ReservationPaxFare) itFare.next();
			Set<ReservationPaxFareSegment> colFareSegment = fare.getPaxFareSegments();

			for (Iterator<ReservationPaxFareSegment> itPaxSegment = colFareSegment.iterator(); itPaxSegment.hasNext();) {
				ReservationPaxFareSegment fareSegment = (ReservationPaxFareSegment) itPaxSegment.next();
				Set<ReservationPaxSegmentSSR> segmentSSRs = fareSegment.getReservationPaxSegmentSSRs();

				for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs.hasNext();) {
					ReservationPaxSegmentSSR reservationPaxSegmentSSR = (ReservationPaxSegmentSSR) itSegmentSSRs.next();

					mapSegmentSSR.put(reservationPaxSegmentSSR.getPnrPaxSegmentSSRId(), reservationPaxSegmentSSR);
				}
			}

		}

		return mapSegmentSSR;
	}

	public static String getPaxSSRForAudit(ReservationPax reservationPax) {
		Map<Integer, Collection<ReservationPaxSegmentSSR>> segmentSSRMap = ReservationSSRUtil.getPaxSegmentSSRMap(reservationPax);
		String ssrData = null;

		if (!segmentSSRMap.isEmpty()) {
			Set<Integer> segSeqIds = segmentSSRMap.keySet();

			for (Iterator<Integer> itSegSeqIds = segSeqIds.iterator(); itSegSeqIds.hasNext();) {
				Integer segSeqId = (Integer) itSegSeqIds.next();
				Set<ReservationPaxSegmentSSR> segmentSSRs = (Set<ReservationPaxSegmentSSR>) segmentSSRMap.get(segSeqId);

				if (segmentSSRs != null && !segmentSSRs.isEmpty()) {
					for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs.hasNext();) {
						ReservationPaxSegmentSSR ssr = (ReservationPaxSegmentSSR) itSegmentSSRs.next();

						// This will add in-flight services
						if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(ssr.getStatus())
								&& ssr.getApplyOn().equals(ReservationPaxSegmentSSR.APPLY_ON_SEGMENT)) {

							if (ssrData == null) {
								ssrData = SSRUtil.getSSRCode(ssr.getSSRId());

							} else {
								ssrData += ", " + SSRUtil.getSSRCode(ssr.getSSRId());
								;
							}

							if (ssr.getSSRText() != null && !"".equals(ssr.getSSRText())) {
								ssrData += " [" + AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.SSR_REMARKS + " :"
										+ ssr.getSSRText() + "]";
							}

						} else if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(ssr.getStatus())
								&& (ssr.getApplyOn().equals(ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL)
										|| ssr.getApplyOn().equals(ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE) || ssr
										.getApplyOn().equals(ReservationPaxSegmentSSR.APPLY_ON_TRANSIT))) {
							// This will add airport services
							if (ssrData == null) {
								ssrData = SSRUtil.getSSRCode(ssr.getSSRId());

							} else {
								ssrData += ", " + SSRUtil.getSSRCode(ssr.getSSRId());
								;
							}

							if (ssr.getAirportCode() != null && !"".equals(ssr.getAirportCode())) {
								ssrData += " [" + AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.SSR_REMARKS + " :"
										+ ssr.getAirportCode() + "]";
							}
						}
					}
				}
			}

		}

		return BeanUtils.nullHandler(ssrData);
	}

	public static String[] getPaxSSR(ReservationPax reservationPax) {
		Map<Integer, Collection<ReservationPaxSegmentSSR>> segmentSSRMap = ReservationSSRUtil.getPaxSegmentSSRMap(reservationPax);
		String[] ssrData = new String[2];

		if (!segmentSSRMap.isEmpty()) {
			Set<Integer> segSeqIds = segmentSSRMap.keySet();

			for (Iterator<Integer> itSegSeqIds = segSeqIds.iterator(); itSegSeqIds.hasNext();) {
				Integer segSeqId = (Integer) itSegSeqIds.next();
				Set<ReservationPaxSegmentSSR> segmentSSRs = (Set<ReservationPaxSegmentSSR>) segmentSSRMap.get(segSeqId);

				if (segmentSSRs != null && !segmentSSRs.isEmpty()) {
					for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs.hasNext();) {
						ReservationPaxSegmentSSR ssr = (ReservationPaxSegmentSSR) itSegmentSSRs.next();

						// This will add in-flight services
						if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(ssr.getStatus())
								&& ssr.getApplyOn().equals(ReservationPaxSegmentSSR.APPLY_ON_SEGMENT)) {
							if (ssrData[0] == null) {
								ssrData[0] = SSRUtil.getSSRCode(ssr.getSSRId());
								ssrData[1] = BeanUtils.nullHandler(ssr.getSSRText());
							} else {
								ssrData[1] += " [" + SSRUtil.getSSRCode(ssr.getSSRId()) + " ";
								ssrData[1] += BeanUtils.nullHandler(ssr.getSSRText()) + "]";
							}
						}
					}
				}

				if (ssrData[0] != null) {
					break;
				}
			}

			ssrData[0] = BeanUtils.nullHandler(ssrData[0]);
			ssrData[1] = BeanUtils.nullHandler(ssrData[1]);
		}

		return ssrData;
	}

	/**
	 * 
	 * @param pax
	 * @return
	 */
	public static List<String[]> getPaxSSR(LCCClientReservationPax pax) {
		List<String[]> ssrDetails = new ArrayList<>();

		for (LCCSelectedSegmentAncillaryDTO ancilary : pax.getSelectedAncillaries()) {
			for (LCCSpecialServiceRequestDTO ssr : ancilary.getSpecialServiceRequestDTOs()) {
				if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(ssr.getStatus()) || ssr.isUserDefinedCharge()) {
					String[] ssrData = new String[] { BeanUtils.nullHandler(ssr.getSsrCode()),
							BeanUtils.nullHandler(ssr.getDescription()) };
					ssrDetails.add(ssrData);
				}
			}

		}
		return ssrDetails;
	}

	public static Collection<SSRInfoDTO> getPaxAirportServices(ReservationPax reservationPax) {

		Map<Integer, Collection<ReservationPaxSegmentSSR>> segmentSSRMap = ReservationSSRUtil.getPaxSegmentSSRMap(reservationPax);

		Collection<SSRInfoDTO> ssrInfoDTOList = new ArrayList<SSRInfoDTO>();

		if (!segmentSSRMap.isEmpty()) {
			Set<Integer> segSeqIds = segmentSSRMap.keySet();

			for (Iterator<Integer> itSegSeqIds = segSeqIds.iterator(); itSegSeqIds.hasNext();) {
				Integer segSeqId = (Integer) itSegSeqIds.next();
				Set<ReservationPaxSegmentSSR> segmentSSRs = (Set<ReservationPaxSegmentSSR>) segmentSSRMap.get(segSeqId);

				if (segmentSSRs != null && !segmentSSRs.isEmpty()) {
					for (Iterator<ReservationPaxSegmentSSR> itSegmentSSRs = segmentSSRs.iterator(); itSegmentSSRs.hasNext();) {
						ReservationPaxSegmentSSR ssr = (ReservationPaxSegmentSSR) itSegmentSSRs.next();

						ReservationPaxFareSegment reservationPaxFareSegment = ssr.getReservationPaxFareSegment();

						// This will add airport services only
						if (ReservationPaxSegmentSSRStatus.CONFIRM.getCode().equals(ssr.getStatus())
								&& !BeanUtils.nullHandler(ssr.getAirportCode()).equals("")) {

							SSRInfoDTO ssrInfoDTO = SSRUtil.getSSRInfo(ssr.getSSRId());

							if (ssrInfoDTO != null) {
								ssrInfoDTO.setAirportList(ssr.getAirportCode());
								ssrInfoDTO.setApplyOn(ssr.getApplyOn());
								ssrInfoDTO.setPnrSegId(reservationPaxFareSegment.getSegment().getPnrSegId());

								ssrInfoDTOList.add(ssrInfoDTO);
							}

						}
					}
				}

			}

		}

		return ssrInfoDTOList;
	}

	public static void updatePaxSegmentSSRs(ReservationPax reservationPaxCln, ReservationPax reservationPaxSrv) {
		// Map<Integer, Collection<ReservationPaxSegmentSSR>> paxSegmentSSRMapCln =
		// getPaxSegmentSSRMap(reservationPaxCln);
		Map<Integer, ReservationPaxSegmentSSR> paxSegmentSSRMapSrv = getPaxSegmentSSRByIdMap(reservationPaxSrv);

		Set<ReservationPaxFare> pnrPaxFaresCln = reservationPaxCln.getPnrPaxFares();

		if (pnrPaxFaresCln != null) {
			for (Iterator<ReservationPaxFare> iPaxFares = pnrPaxFaresCln.iterator(); iPaxFares.hasNext();) {
				ReservationPaxFare paxFareCln = (ReservationPaxFare) iPaxFares.next();
				Set<ReservationPaxFareSegment> fareSegmentsCln = paxFareCln.getPaxFareSegments();

				if (fareSegmentsCln != null) {
					for (Iterator<ReservationPaxFareSegment> itFareSegments = fareSegmentsCln.iterator(); itFareSegments
							.hasNext();) {
						ReservationPaxFareSegment fareSegmentCln = (ReservationPaxFareSegment) itFareSegments.next();
						Set<ReservationPaxSegmentSSR> segmentSSRsCln = fareSegmentCln.getReservationPaxSegmentSSRs();
						if(segmentSSRsCln!=null && !segmentSSRsCln.isEmpty()){
							for (Iterator<ReservationPaxSegmentSSR> iterator = segmentSSRsCln.iterator(); iterator.hasNext();) {
								ReservationPaxSegmentSSR segmentSSRCln = (ReservationPaxSegmentSSR) iterator.next();
								Integer pntPaxSegSSRId = segmentSSRCln.getPnrPaxSegmentSSRId();

								if (pntPaxSegSSRId != null) {
									ReservationPaxSegmentSSR segSSRSrv = paxSegmentSSRMapSrv.get(pntPaxSegSSRId);

									if (segmentSSRCln.getSSRId() != null) {
										segSSRSrv.setSSRId(segmentSSRCln.getSSRId());
										segSSRSrv.setSSRText(segmentSSRCln.getSSRText());
										segSSRSrv.setStatus(segmentSSRCln.getStatus());
									} else {
										segSSRSrv.setStatus(ReservationPaxSegmentSSRStatus.CANCEL.getCode());
									}
								} else {
									ReservationPaxFareSegment fareSegmentSrv = getPaxFareSegment(reservationPaxSrv,
											fareSegmentCln.getPnrPaxFareSegId());

									fareSegmentSrv.addPaxSegmentSSR(segmentSSRCln);
								}
							}
						}

					}
				}
			}
		}

	}

	private static ReservationPaxFareSegment getPaxFareSegment(ReservationPax reservationPax, Integer pnrPaxFareSegId) {
		Collection<ReservationPaxFare> colFare = (Collection<ReservationPaxFare>) reservationPax.getPnrPaxFares();

		for (Iterator<ReservationPaxFare> itFare = colFare.iterator(); itFare.hasNext();) {
			ReservationPaxFare fare = (ReservationPaxFare) itFare.next();
			Set<ReservationPaxFareSegment> colFareSegment = fare.getPaxFareSegments();

			for (Iterator<ReservationPaxFareSegment> itPaxSegment = colFareSegment.iterator(); itPaxSegment.hasNext();) {
				ReservationPaxFareSegment fareSegment = (ReservationPaxFareSegment) itPaxSegment.next();

				if (fareSegment.getPnrPaxFareSegId().equals(pnrPaxFareSegId)) {
					return fareSegment;
				}
			}

		}

		return null;
	}

	public static void addSSRsForExternalCharges(ReservationPax reservationPax) throws ModuleException {
		PaymentAssembler payment = (PaymentAssembler) reservationPax.getPayment();
		if (payment != null) {
			Collection<ExternalChgDTO> airportServiceChgDTOs = payment.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
			Collection<ExternalChgDTO> inflightExtChgDTOs = payment.getPerPaxExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);

			Collection<ExternalChgDTO> externalChgDTOs = new ArrayList<ExternalChgDTO>();
			if (airportServiceChgDTOs != null) {
				externalChgDTOs.addAll(airportServiceChgDTOs);
			}

			if (inflightExtChgDTOs != null) {
				externalChgDTOs.addAll(inflightExtChgDTOs);
			}

			if (externalChgDTOs.size() > 0) {
				Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap = createSegmentSSRDTOMap(externalChgDTOs);

				addPaxSegmentSSRs(reservationPax, segmentSSRDTOMap, null);
			}
		}
	}

	public static void addSSRsForExternalCharges(ReservationPax reservationPax, Collection<ExternalChgDTO> externalChgDTOs,
			Map<Integer, List<SSRExternalChargeDTO>> anciMap, Integer salesChannelCode) throws ModuleException {
		Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap = createSegmentSSRDTOMap(externalChgDTOs);

		PaxSegmentSSRDTO ssr = null;

		addPaxSegmentSSRs(reservationPax, segmentSSRDTOMap, null);

		if (salesChannelCode != null
				&& SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY) == salesChannelCode) {
			List<String> ssrList = new ArrayList<String>();
			if (AppSysParamsUtil.applyFQTVInAllSegments()) {
				ssrList.add("FQTV");
			}
			if (ssrList.size() > 0) {
				ssr = getGloballyAppliedSSRComment(reservationPax.getPnrPaxFares(), anciMap.get(reservationPax.getPnrPaxId()),
						ssrList, segmentSSRDTOMap);
			}

			if (ssr != null) {
				addPaxSegmentSSRs(reservationPax, segmentSSRDTOMap, ssr);
			}
		}
	}

	private static Map<Integer, Collection<PaxSegmentSSRDTO>> createSegmentSSRDTOMap(Collection<ExternalChgDTO> externalChgDTOs) {
		Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap = new HashMap<Integer, Collection<PaxSegmentSSRDTO>>();
		for (Iterator<ExternalChgDTO> iterator = externalChgDTOs.iterator(); iterator.hasNext();) {
			SSRExternalChargeDTO ssrExternalChargeDTO = (SSRExternalChargeDTO) iterator.next();
			Integer segSequence = ssrExternalChargeDTO.getSegmentSequece();
			Collection<PaxSegmentSSRDTO> segmentSSRs = segmentSSRDTOMap.get(segSequence);
			PaxSegmentSSRDTO paxSegmentSSRDTO = createPaxSegmentSSRDTO(ssrExternalChargeDTO);

			if (segmentSSRs == null) {
				segmentSSRs = new ArrayList<PaxSegmentSSRDTO>();
				segmentSSRDTOMap.put(segSequence, segmentSSRs);
			}

			segmentSSRs.add(paxSegmentSSRDTO);
		}
		return segmentSSRDTOMap;
	}

	private static PaxSegmentSSRDTO createPaxSegmentSSRDTO(SSRExternalChargeDTO ssrExternalChargeDTO) {
		PaxSegmentSSRDTO segmentSSRDTO = new PaxSegmentSSRDTO();

		segmentSSRDTO.setSegmentSequence(ssrExternalChargeDTO.getSegmentSequece());
		segmentSSRDTO.setSSRId(ssrExternalChargeDTO.getSSRId());
		segmentSSRDTO.setSSRText(ssrExternalChargeDTO.getSSRText() == null ? "" : ssrExternalChargeDTO.getSSRText());
		segmentSSRDTO.setContextId(ssrExternalChargeDTO.getSSRChargeId());
		segmentSSRDTO.setChargeAmount(ssrExternalChargeDTO.getAmount());
		segmentSSRDTO.setApplyOn(ssrExternalChargeDTO.getApplyOn());
		segmentSSRDTO.setAirportCode(ssrExternalChargeDTO.getAirportCode());
		if (ssrExternalChargeDTO.getFlightRPH().indexOf("$") != -1) {
			segmentSSRDTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(ssrExternalChargeDTO.getFlightRPH()));
		} else {
			try {
				FlightSegement flightSeg = ReservationModuleUtils.getFlightBD().getFlightSegment(
						new Integer(ssrExternalChargeDTO.getFlightRPH()));
				segmentSSRDTO.setSegmentCode(flightSeg.getSegmentCode());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ModuleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return segmentSSRDTO;
	}

	public static Collection<ReservationPaxSegmentSSR> cancelPaxSSRs(ReservationPax passenger, Collection<Integer> pnrSegmentIds) {
		Collection<ReservationPaxSegmentSSR> cancelledSSRS = new ArrayList<ReservationPaxSegmentSSR>();
		Set<ReservationPaxFare> paxFares = passenger.getPnrPaxFares();

		for (Iterator<ReservationPaxFare> itPaxFares = paxFares.iterator(); itPaxFares.hasNext();) {
			ReservationPaxFare reservationPaxFare = (ReservationPaxFare) itPaxFares.next();

			Set<ReservationPaxFareSegment> paxFareSegments = reservationPaxFare.getPaxFareSegments();

			for (Iterator<ReservationPaxFareSegment> itPaxFareSeg = paxFareSegments.iterator(); itPaxFareSeg.hasNext();) {
				ReservationPaxFareSegment reservationPaxFareSegment = (ReservationPaxFareSegment) itPaxFareSeg.next();

				if (pnrSegmentIds == null || pnrSegmentIds.contains(reservationPaxFareSegment.getPnrSegId())) {
					Collection<ReservationPaxSegmentSSR> segmentSSRs = reservationPaxFareSegment.getReservationPaxSegmentSSRs();

					for (Iterator<ReservationPaxSegmentSSR> itSSRs = segmentSSRs.iterator(); itSSRs.hasNext();) {
						ReservationPaxSegmentSSR paxSegmentSSR = (ReservationPaxSegmentSSR) itSSRs.next();

						if (!ReservationPaxSegmentSSRStatus.CANCEL.getCode().equals(paxSegmentSSR.getStatus())) {
							paxSegmentSSR.setStatus(ReservationPaxSegmentSSRStatus.CANCEL.getCode());
							cancelledSSRS.add(paxSegmentSSR);
						}

					}
				}

			}
		}

		return cancelledSSRS;
	}

	public static Map<Integer, Collection<ReservationPaxSegmentSSR>> cancelSegmentSSRs(Reservation reservation,
			Collection<Integer> pnrSegmentIds) {
		Map<Integer, Collection<ReservationPaxSegmentSSR>> cancelledSSRMap = new HashMap<Integer, Collection<ReservationPaxSegmentSSR>>();
		Set<ReservationPax> paxs = reservation.getPassengers();

		for (Iterator<ReservationPax> itPaxs = paxs.iterator(); itPaxs.hasNext();) {
			ReservationPax reservationPax = (ReservationPax) itPaxs.next();
			Collection<ReservationPaxSegmentSSR> cancelledSSRs = cancelPaxSSRs(reservationPax, pnrSegmentIds);
			if (cancelledSSRs != null && !cancelledSSRs.isEmpty()) {
				cancelledSSRMap.put(reservationPax.getPnrPaxId(), cancelledSSRs);
			}
		}

		return cancelledSSRMap;

	}

	public static SSRExternalChargeDTO getSSRExternalCharge(TrackInfoDTO trackInfoDTO, EXTERNAL_CHARGES extCharge)
			throws ModuleException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(extCharge);

		Map<EXTERNAL_CHARGES, ExternalChgDTO> mapExternalChgs = ReservationModuleUtils.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, trackInfoDTO, null);

		return (SSRExternalChargeDTO) mapExternalChgs.get(extCharge);
	}

	@SuppressWarnings("unchecked")
	public static void addPaxSegmentSSRs(Reservation reservation, Map<Integer, SegmentSSRAssembler> paxSSRAssemblerMap,
			EXTERNAL_CHARGES extCharge) throws ModuleException {
		Set<ReservationPax> passengers = reservation.getPassengers();

		for (Iterator<ReservationPax> itPax = passengers.iterator(); itPax.hasNext();) {
			ReservationPax pax = (ReservationPax) itPax.next();
			if (!ReservationApiUtils.isInfantType(pax)) {
				SegmentSSRAssembler ssrAssembler = paxSSRAssemblerMap.get(pax.getPaxSequence());

				if (ssrAssembler != null && ssrAssembler.getPassengerAssembler() != null) {
					PassengerAssembler passengerAsm = (PassengerAssembler) ssrAssembler.getPassengerAssembler();
					Map<Integer, IPayment> paxPaymentsMap = passengerAsm.getPassengerPaymentsMap();

					addPaxSegmentSSRs(pax, ssrAssembler, paxPaymentsMap.get(pax.getPnrPaxId()), extCharge);
				}
			}
		}
	}

	private static void addPaxSegmentSSRs(ReservationPax pax, SegmentSSRAssembler ssrAssembler, IPayment iPayment,
			EXTERNAL_CHARGES extCharge) throws ModuleException {
		PaymentAssembler payment = (PaymentAssembler) iPayment;

		if (payment != null) {
			Collection<ExternalChgDTO> externalChgDTOs = payment.getPerPaxExternalCharges(extCharge);
			Map<Integer, Collection<PaxSegmentSSRDTO>> segmentSSRDTOMap = createSegmentSSRDTOMap(externalChgDTOs);

			addPaxSegmentSSRs(pax, segmentSSRDTOMap, null);
		}
	}

	/**
	 * when sub-journey enabled SSR selected segment sequence & final saving segment order is not same. following
	 * function will update the correct segment sequence with picked SSR.
	 * */
	public static Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> updateSSRBookedSegmentSequence(
			Collection<OndFareDTO> colOndFareDTOs, Collection<ReservationSegment> resSegments,
			Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps) {
		boolean isPaxSSRMapUpdated = false;
		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> updatePaxSegmentSSRDTOMaps = new HashMap<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>>();
		if (colOndFareDTOs != null && colOndFareDTOs.size() > 1 && paxSegmentSSRDTOMaps != null
				&& !paxSegmentSSRDTOMaps.isEmpty() && resSegments != null) {
			Map<Integer, Integer> ondSeqByFltSegId = new HashMap<Integer, Integer>();
			Collection<FlightSegmentDTO> flightSegmentColl = ReservationApiUtils.getFlightSegmentsForOnDFare(colOndFareDTOs);
			FlightSegmentDTO[] sortedFlightSegments = ReservationApiUtils.sortFlightSegmentByDepDate(flightSegmentColl);

			int journeySequence = 1;
			for (FlightSegmentDTO fltSegmentDTO : sortedFlightSegments) {
				ondSeqByFltSegId.put(fltSegmentDTO.getSegmentId(), journeySequence);
				journeySequence++;
			}

			if (resSegments.size() > 1) {
				for (ReservationSegment resSeg : resSegments) {
					Integer fltSegId = resSeg.getFlightSegId();
					Integer orderedSeq = ondSeqByFltSegId.get(fltSegId);

					for (Integer paxSeq : paxSegmentSSRDTOMaps.keySet()) {
						if (paxSegmentSSRDTOMaps.get(paxSeq) != null) {
							Map<Integer, Collection<PaxSegmentSSRDTO>> ssrByOnDSeq = paxSegmentSSRDTOMaps.get(paxSeq);

							if (ssrByOnDSeq.get(orderedSeq) != null && !ssrByOnDSeq.get(orderedSeq).isEmpty()) {

								if (updatePaxSegmentSSRDTOMaps.get(paxSeq) == null) {
									updatePaxSegmentSSRDTOMaps.put(paxSeq, new HashMap<Integer, Collection<PaxSegmentSSRDTO>>());
								}

								if (updatePaxSegmentSSRDTOMaps.get(paxSeq).get(orderedSeq) == null) {
									updatePaxSegmentSSRDTOMaps.get(paxSeq).put(resSeg.getSegmentSeq(),
											new ArrayList<PaxSegmentSSRDTO>());
								}
								updatePaxSegmentSSRDTOMaps.get(paxSeq).put(resSeg.getSegmentSeq(), ssrByOnDSeq.get(orderedSeq));
								isPaxSSRMapUpdated = true;
							}

							if (paxSegmentSSRDTOMaps.get(paxSeq).isEmpty()) {
								updatePaxSegmentSSRDTOMaps.put(paxSeq, new HashMap<Integer, Collection<PaxSegmentSSRDTO>>());
							}

						}

					}

				}
			}

		}

		if (isPaxSSRMapUpdated) {
			return updatePaxSegmentSSRDTOMaps;
		} else {
			return paxSegmentSSRDTOMaps;
		}

	}

}
