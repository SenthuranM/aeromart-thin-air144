package com.isa.thinair.airreservation.api.dto.palCal;

import java.io.Serializable;

public class PalCalTimingDTO implements Serializable{
	
	public final static String PAL_CAL_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	public final static String START_DATE_TIME = " 00:00:00";
	public final static String END_DATE_TIME = " 23:59:59";
	public final static String timing_splitter = ":";
	public final static String ALL_FLIGHTS = "ALL FLIGHTS";
	
	private static final long serialVersionUID = -8703894714848791035L;

	private Integer palCalTimingId;

	private String airportCode;

	private String flightNumber;

	private String palDepGap;

	private String calRepIntv;

	private String strStartingZuluDate;

	private String strEndingZuluDate;
	
	private String status;
	
	private long version = -1;
	
	private boolean allowAll;
	
	private String lastCalGap;
	
	private String calRepIntvAfterCutoff;

	public Integer getPalCalTimingId() {
		return palCalTimingId;
	}

	public void setPalCalTimingId(Integer palCalTimingId) {
		this.palCalTimingId = palCalTimingId;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getStrStartingZuluDate() {
		return strStartingZuluDate;
	}

	public void setStrStartingZuluDate(String strStartingZuluDate) {
		this.strStartingZuluDate = strStartingZuluDate;
	}

	public String getStrEndingZuluDate() {
		return strEndingZuluDate;
	}

	public void setStrEndingZuluDate(String strEndingZuluDate) {
		this.strEndingZuluDate = strEndingZuluDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isAllowAll() {
		return allowAll;
	}

	public void setAllowAll(boolean allowAll) {
		this.allowAll = allowAll;
	}

	public String getPalDepGap() {
		return palDepGap;
	}

	public void setPalDepGap(String palDepGap) {
		this.palDepGap = palDepGap;
	}

	public String getCalRepIntv() {
		return calRepIntv;
	}

	public void setCalRepIntv(String calRepIntv) {
		this.calRepIntv = calRepIntv;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getLastCalGap() {
		return lastCalGap;
	}

	public void setLastCalGap(String lastCalGap) {
		this.lastCalGap = lastCalGap;
	}

	public String getCalRepIntvAfterCutoff() {
		return calRepIntvAfterCutoff;
	}

	public void setCalRepIntvAfterCutoff(String calRepIntvAfterCutoff) {
		this.calRepIntvAfterCutoff = calRepIntvAfterCutoff;
	}
	
	
}
