/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Holds the effected flight segment data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class EffectedFlightSegmentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2721772123204866400L;

	/** Holds the actual flight segment id */
	private Integer flightSegmentId;

	/** Holds the actual flight corresponding segment code */
	private String flightSegmentCode;

	/** Holds the all reservation segment id which includes the effected ones too */
	private Collection<Integer> allPnrSegmentIds;

	/** Holds the all reservation segment id */
	private Collection<Integer> actualPnrSegmentIds;

	/**
	 * @return Returns the actualPnrSegmentIds.
	 */
	public Collection<Integer> getActualPnrSegmentIds() {
		return actualPnrSegmentIds;
	}

	/**
	 * @param actualPnrSegmentIds
	 *            The actualPnrSegmentIds to set.
	 */
	private void setActualPnrSegmentIds(Collection<Integer> actualPnrSegmentIds) {
		this.actualPnrSegmentIds = actualPnrSegmentIds;
	}

	/**
	 * @return Returns the allPnrSegmentIds.
	 */
	public Collection<Integer> getAllPnrSegmentIds() {
		return allPnrSegmentIds;
	}

	/**
	 * @param allPnrSegmentIds
	 *            The allPnrSegmentIds to set.
	 */
	private void setAllPnrSegmentIds(Collection<Integer> allPnrSegmentIds) {
		this.allPnrSegmentIds = allPnrSegmentIds;
	}

	/**
	 * Add all pnr segment id
	 * 
	 * @param pnrSegmentIds
	 */
	public void addAllPnrSegmentId(Integer allPnrSegmentId) {
		if (this.getAllPnrSegmentIds() == null) {
			this.setAllPnrSegmentIds(new ArrayList<Integer>());
		}

		this.getAllPnrSegmentIds().add(allPnrSegmentId);
	}

	/**
	 * Add actual pnr segment id
	 * 
	 * @param pnrSegmentIds
	 */
	public void addActualPnrSegmentId(Integer actualPnrSegmentId) {
		if (this.getActualPnrSegmentIds() == null) {
			this.setActualPnrSegmentIds(new ArrayList<Integer>());
		}

		this.getActualPnrSegmentIds().add(actualPnrSegmentId);
	}

	/**
	 * @return Returns the flightSegmentId.
	 */
	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            The flightSegmentId to set.
	 */
	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * Return the effected reservation segment ids
	 * 
	 * @return
	 */
	public Collection<Integer> getEffectedPnrSegmentIds() {
		Collection<Integer> effectedPnrSegIds = new ArrayList<Integer>();

		if (this.getAllPnrSegmentIds() != null && this.getActualPnrSegmentIds() != null) {
			effectedPnrSegIds.addAll(this.getAllPnrSegmentIds());
			effectedPnrSegIds.removeAll(this.getActualPnrSegmentIds());
		}

		return effectedPnrSegIds;
	}

	/**
	 * @return Returns the flightSegmentCode.
	 */
	public String getFlightSegmentCode() {
		return flightSegmentCode;
	}

	/**
	 * @param flightSegmentCode
	 *            The flightSegmentCode to set.
	 */
	public void setFlightSegmentCode(String flightSegmentCode) {
		this.flightSegmentCode = flightSegmentCode;
	}
}
