package com.isa.thinair.airreservation.api.dto.itinerary;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.dto.FlexiRuleFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;

public class ItineraryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	// Options (Used as builders/indicators)
	private String itineraryLanguage;
	private String station;
	private boolean includePaxFinancials;
	private boolean includeTicketCharges;
	private boolean includePaymentDetails;
	private boolean includeTermsAndConditions;
	private boolean includeFlightBaggageAllowance;
	private boolean includePaxCreditExpiryDate;
	private boolean includePaxContactDetails;
	private boolean includeStationContactDetails;
	private boolean showETicketPerPax;
	private boolean showDOB;
	private boolean showFOID;
	private boolean includeFareRules;
	private boolean showExtraChargeDetails;
	private boolean showStandbyTravelValidity;
	private boolean viewFareBasisCode;
	private boolean showBundledService;
	private boolean showChargesInItineraryPassengerDetails = true;
	private boolean showAeromartPayFares;
	// Transfer Objects
	private ItineraryAncillaryAvailabilityTO anciAvailabilityTO;
	private ItineraryInsuranceTO insuranceTO;
	private ItineraryFareMaskTO itineraryFareMaskTO;
	private ItineraryContactInfoTO contactInfo;
	private ItineraryBookingTO booking;
	/** Map (Pax Sequence, Pax) **/
	private List<ItineraryPassengerTO> passengers;
	private ItineraryAgentTO agent;
	private Collection<ItineraryOndChargeTO> ondCharges;
	private List<ItineraryFlightOndGroup> flightOndGroups;

	private List<ItineraryFareRuleTO> fareRules;

	private String appIndicator;
	private TrackInfoDTO trackInfoDTO;

	private String selectedPaxDetails;
	private List<ItineraryFlightOndGroup> openRetFlightOndGroups;

	private List<String> airportMsgs;

	private Collection<ItineraryOndChargeTO> ondTaxesFeesCharges;

	private boolean showCOS;

	private boolean showLogicalCC;

	private boolean includeThumbnailLogo;

	private boolean showCheckInClosingTime;

	private String firstDepartingCarrier;
	// Endorsements
	private boolean includeEndorsements;

	private String endorsements;

	private List<ItineraryPassengerEticketTO> ticketInfo;

	private List<FlexiRuleFlexibilityDTO> flexiRules;

	private boolean showFlightStopOverInfo;

	private boolean isFareMasked;

	private List<Map<String, String>> stationContacts;

	private boolean includeAutoCancellationInfo;

	private boolean showDiscountAmounts;

	private boolean creditDiscount;
	
	private boolean isForced;

	public boolean isShowChargesInItineraryPassengerDetails() {
		return showChargesInItineraryPassengerDetails;
	}

	public void setShowChargesInItineraryPassengerDetails(boolean showChargesInItineraryPassengerDetails) {
		this.showChargesInItineraryPassengerDetails = showChargesInItineraryPassengerDetails;
	}
	/**
	 * @param isForced
	 *            the isForced to set
	 */
	public boolean isForced() {
		return isForced;
	}

	/**
	 * @return the isForced
	 */
	public void setForced(boolean isForced) {
		this.isForced = isForced;
	}

	/**
	 * @return the itineraryLanguage
	 */
	public String getItineraryLanguage() {
		return itineraryLanguage;
	}

	/**
	 * @param itineraryLanguage
	 *            the itineraryLanguage to set
	 */
	public void setItineraryLanguage(String itineraryLanguage) {
		this.itineraryLanguage = itineraryLanguage;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	/**
	 * @return the includePaxFinancials
	 */
	public boolean isIncludePaxFinancials() {
		return includePaxFinancials;
	}

	/**
	 * @param includePaxFinancials
	 *            the includePaxFinancials to set
	 */
	public void setIncludePaxFinancials(boolean includePaxFinancials) {
		this.includePaxFinancials = includePaxFinancials;
	}

	/**
	 * @return the includeTicketCharges
	 */
	public boolean isIncludeTicketCharges() {
		return includeTicketCharges;
	}

	/**
	 * @param includeTicketCharges
	 *            the includeTicketCharges to set
	 */
	public void setIncludeTicketCharges(boolean includeTicketCharges) {
		this.includeTicketCharges = includeTicketCharges;
	}

	/**
	 * @return the includePaymentDetails
	 */
	public boolean isIncludePaymentDetails() {
		return includePaymentDetails;
	}

	/**
	 * @param includePaymentDetails
	 *            the includePaymentDetails to set
	 */
	public void setIncludePaymentDetails(boolean includePaymentDetails) {
		this.includePaymentDetails = includePaymentDetails;
	}

	/**
	 * @return the contactInfo
	 */
	public ItineraryContactInfoTO getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(ItineraryContactInfoTO contactInfo) {
		this.contactInfo = contactInfo;
	}

	/**
	 * @return the booking
	 */
	public ItineraryBookingTO getBooking() {
		return booking;
	}

	/**
	 * @param booking
	 *            the booking to set
	 */
	public void setBooking(ItineraryBookingTO booking) {
		this.booking = booking;
	}

	/**
	 * @return the passengers
	 */
	public List<ItineraryPassengerTO> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	public void setPassengers(List<ItineraryPassengerTO> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the agent
	 */
	public ItineraryAgentTO getAgent() {
		return agent;
	}

	/**
	 * @param agent
	 *            the agent to set
	 */
	public void setAgent(ItineraryAgentTO agent) {
		this.agent = agent;
	}

	/**
	 * @return the charges
	 */
	public Collection<ItineraryOndChargeTO> getOndCharges() {
		return ondCharges;
	}

	/**
	 * @param ondChargeCollection
	 *            the charges to set
	 */
	public void setOndCharges(Collection<ItineraryOndChargeTO> ondChargeCollection) {
		this.ondCharges = ondChargeCollection;
	}

	/**
	 * @return the flights
	 */
	public List<ItineraryFlightOndGroup> getFlightOndGroups() {
		return flightOndGroups;
	}

	/**
	 * @param flightOndGroups
	 *            the flights to set
	 */
	public void setFlightOndGroups(List<ItineraryFlightOndGroup> flightOndGroups) {
		this.flightOndGroups = flightOndGroups;
	}

	public void setFareRules(List<ItineraryFareRuleTO> fareRules) {
		this.fareRules = fareRules;
	}

	public List<ItineraryFareRuleTO> getFareRules() {
		return fareRules;
	}

	public ItineraryAncillaryAvailabilityTO getAnciAvailabilityTO() {
		return anciAvailabilityTO;
	}

	public void setAnciAvailabilityTO(ItineraryAncillaryAvailabilityTO anciAvailabilityTO) {
		this.anciAvailabilityTO = anciAvailabilityTO;
	}

	public ItineraryInsuranceTO getInsuranceTO() {
		return insuranceTO;
	}

	public void setInsuranceTO(ItineraryInsuranceTO insuranceTO) {
		this.insuranceTO = insuranceTO;
	}

	public boolean isShowETicketPerPax() {
		return showETicketPerPax;
	}

	public void setShowETicketPerPax(boolean showETicketPerPax) {
		this.showETicketPerPax = showETicketPerPax;
	}

	public boolean isShowDOB() {
		return showDOB;
	}

	public void setShowDOB(boolean showDOB) {
		this.showDOB = showDOB;
	}

	public boolean isShowFOID() {
		return showFOID;
	}

	public void setShowFOID(boolean showFOID) {
		this.showFOID = showFOID;
	}

	public boolean isIncludeFareRules() {
		return includeFareRules;
	}

	public void setIncludeFareRules(boolean includeFareRules) {
		this.includeFareRules = includeFareRules;
	}

	public void setAppIndicator(String appIndicator) {
		this.appIndicator = appIndicator;
	}

	public String getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @return the trackInfoDTO
	 */
	public TrackInfoDTO getTrackInfoDTO() {
		return trackInfoDTO;
	}

	/**
	 * @param trackInfoDTO
	 *            the trackInfoDTO to set
	 */
	public void setTrackInfoDTO(TrackInfoDTO trackInfoDTO) {
		this.trackInfoDTO = trackInfoDTO;
	}

	/**
	 * @return the setSelectedPaxDetails
	 */
	public String getSelectedPaxDetails() {
		return selectedPaxDetails;
	}

	/**
	 * @param setSelectedPaxDetails
	 *            the setSelectedPaxDetails to set
	 */
	public void setSelectedPaxDetails(String selectedPaxDetails) {
		this.selectedPaxDetails = selectedPaxDetails;
	}

	public List<ItineraryFlightOndGroup> getOpenRetFlightOndGroups() {
		return openRetFlightOndGroups;
	}

	public void setOpenRetFlightOndGroups(List<ItineraryFlightOndGroup> openRetFlightOndGroups) {
		this.openRetFlightOndGroups = openRetFlightOndGroups;
	}

	/**
	 * @return the includeTermsAndConditions
	 */
	public boolean isIncludeTermsAndConditions() {
		return includeTermsAndConditions;
	}

	/**
	 * @param includeTermsAndConditions
	 *            the includeTermsAndConditions to set
	 */
	public void setIncludeTermsAndConditions(boolean includeTermsAndConditions) {
		this.includeTermsAndConditions = includeTermsAndConditions;
	}

	public List<String> getAirportMsgs() {
		return airportMsgs;
	}

	public void setAirportMsgs(List<String> airportMsgs) {
		this.airportMsgs = airportMsgs;
	}

	public Collection<ItineraryOndChargeTO> getOndTaxesFeesCharges() {
		return ondTaxesFeesCharges;
	}

	public void setOndTaxesFeesCharges(Collection<ItineraryOndChargeTO> ondTaxesFeesCharges) {
		this.ondTaxesFeesCharges = ondTaxesFeesCharges;
	}

	public boolean isShowCOS() {
		return showCOS;
	}

	public void setShowCOS(boolean showCOS) {
		this.showCOS = showCOS;
	}

	public boolean isShowLogicalCC() {
		return showLogicalCC;
	}

	public void setShowLogicalCC(boolean showLogicalCC) {
		this.showLogicalCC = showLogicalCC;
	}

	public boolean isIncludeThumbnailLogo() {
		return includeThumbnailLogo;
	}

	public void setIncludeThumbnailLogo(boolean includeThumbnailLogo) {
		this.includeThumbnailLogo = includeThumbnailLogo;
	}

	public boolean isShowCheckInClosingTime() {
		return showCheckInClosingTime;
	}

	public void setShowCheckInClosingTime(boolean showCheckInClosingTime) {
		this.showCheckInClosingTime = showCheckInClosingTime;
	}

	public String getFirstDepartingCarrier() {
		return firstDepartingCarrier;
	}

	public void setFirstDepartingCarrier(String firstDepartingCarrier) {
		this.firstDepartingCarrier = firstDepartingCarrier;
	}

	public List<ItineraryPassengerEticketTO> getTicketInfo() {
		return ticketInfo;
	}

	public void setTicketInfo(List<ItineraryPassengerEticketTO> ticketInfo) {
		this.ticketInfo = ticketInfo;
	}

	/**
	 * @param includeEndorsements
	 *            the includeEndorsements to set
	 */
	public void setIncludeEndorsements(boolean includeEndorsements) {
		this.includeEndorsements = includeEndorsements;
	}

	/**
	 * @return the includeEndorsements
	 */
	public boolean isIncludeEndorsements() {
		return includeEndorsements;
	}

	/**
	 * @param endorsements
	 *            the endorsements to set
	 */
	public void setEndorsements(String endorsements) {
		this.endorsements = endorsements;
	}

	/**
	 * @return the endorsements
	 */
	public String getEndorsements() {
		return endorsements;
	}

	public List<FlexiRuleFlexibilityDTO> getFlexiRules() {
		return flexiRules;
	}

	public void setFlexiRules(List<FlexiRuleFlexibilityDTO> flexiRules) {
		this.flexiRules = flexiRules;
	}

	public boolean isShowFlightStopOverInfo() {
		return showFlightStopOverInfo;
	}

	public void setShowFlightStopOverInfo(boolean showFlightStopOverInfo) {
		this.showFlightStopOverInfo = showFlightStopOverInfo;
	}

	public boolean isIncludeFlightBaggageAllowance() {
		return includeFlightBaggageAllowance;
	}

	public void setIncludeFlightBaggageAllowance(boolean includeFlightBaggageAllowance) {
		this.includeFlightBaggageAllowance = includeFlightBaggageAllowance;
	}

	public boolean isIncludePaxCreditExpiryDate() {
		return includePaxCreditExpiryDate;
	}

	public void setIncludePaxCreditExpiryDate(boolean includePaxCreditExpiryDate) {
		this.includePaxCreditExpiryDate = includePaxCreditExpiryDate;
	}

	public boolean isFareMasked() {
		return isFareMasked;
	}

	public void setFareMasked(boolean isFareMasked) {
		this.isFareMasked = isFareMasked;
	}

	/**
	 * This trigger determines whether ExtraChargeDetails will be printed in the itinerary.
	 * 
	 * @return the showExtraChargeDetails
	 */
	public boolean isShowExtraChargeDetails() {
		return showExtraChargeDetails;
	}

	/**
	 * This trigger determines whether ExtraChargeDetails will be printed in the itinerary.
	 * 
	 * @param showExtraChargeDetails
	 *            the showExtraChargeDetails to set
	 */
	public void setShowExtraChargeDetails(boolean showExtraChargeDetails) {
		this.showExtraChargeDetails = showExtraChargeDetails;
	}

	public List<Map<String, String>> getStationContacts() {
		return stationContacts;
	}

	public void setStationContacts(List<Map<String, String>> stationContacts) {
		this.stationContacts = stationContacts;
	}

	public boolean isIncludePaxContactDetails() {
		return includePaxContactDetails;
	}

	public void setIncludePaxContactDetails(boolean includePaxContactDetails) {
		this.includePaxContactDetails = includePaxContactDetails;
	}

	public boolean isIncludeStationContactDetails() {
		return includeStationContactDetails;
	}

	public void setIncludeStationContactDetails(boolean includeStationContactDetails) {
		this.includeStationContactDetails = includeStationContactDetails;
	}

	public boolean isIncludeAutoCancellationInfo() {
		return includeAutoCancellationInfo;
	}

	public void setIncludeAutoCancellationInfo(boolean includeAutoCancellationInfo) {
		this.includeAutoCancellationInfo = includeAutoCancellationInfo;
	}

	public boolean isShowDiscountAmounts() {
		return showDiscountAmounts;
	}

	public void setShowDiscountAmounts(boolean showDiscountAmounts) {
		this.showDiscountAmounts = showDiscountAmounts;
	}

	public boolean isCreditDiscount() {
		return creditDiscount;
	}

	public void setCreditDiscount(boolean creditDiscount) {
		this.creditDiscount = creditDiscount;
	}
	
	public boolean isShowStandbyTravelValidity() {
		return showStandbyTravelValidity;
	}

	public void setShowStandbyTravelValidity(boolean showStandbyTravelValidity) {
		this.showStandbyTravelValidity = showStandbyTravelValidity;
	}	
	
	public boolean isViewFareBasisCode() {
		return viewFareBasisCode;
	}

	public void setViewFareBasisCode(boolean viewFareBasisCode) {
		this.viewFareBasisCode = viewFareBasisCode;
	}

	public boolean isShowBundledService() {
		return showBundledService;
	}

	public void setShowBundledService(boolean showBundledService) {
		this.showBundledService = showBundledService;
	}

	public ItineraryFareMaskTO getItineraryFareMaskTO() {
		return itineraryFareMaskTO;
	}

	public void setItineraryFareMaskTO(ItineraryFareMaskTO itineraryFareMaskTO) {
		this.itineraryFareMaskTO = itineraryFareMaskTO;
	}

	public boolean isShowAeromartPayFares() {
		return showAeromartPayFares;
	}

	public void setShowAeromartPayFares(boolean showAeromartPayFares) {
		this.showAeromartPayFares = showAeromartPayFares;
	}
	
	
}