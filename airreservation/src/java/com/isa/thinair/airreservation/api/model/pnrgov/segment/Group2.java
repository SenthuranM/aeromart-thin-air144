package com.isa.thinair.airreservation.api.model.pnrgov.segment;

import java.util.ArrayList;

import com.isa.thinair.airreservation.api.model.pnrgov.EDISegmentGroup;
import com.isa.thinair.airreservation.api.model.pnrgov.MessageSegment;

public class Group2 extends EDISegmentGroup {

	protected TIF tif;
	protected ArrayList<SSR> ssrCollection = new ArrayList<SSR>();
	protected ArrayList<Group3> group3 = new ArrayList<Group3>();

	public Group2() {
		super("Group2");
	}

	public void setTif(TIF tif) {
		this.tif = tif;
	}

	public void addSSR(SSR ssr) {
		ssrCollection.add(ssr);
	}

	public void addGroup3(Group3 group3) {
		this.group3.add(group3);
	}

	@Override
	public MessageSegment build() {
		addMessageSegmentIfNotEmpty(tif);
		addMessageSegmentsIfNotEmpty(ssrCollection);
		addMessageSegmentsIfNotEmpty(group3);
		return this;
	}

}
