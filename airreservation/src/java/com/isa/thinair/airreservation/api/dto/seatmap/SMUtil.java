package com.isa.thinair.airreservation.api.dto.seatmap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class SMUtil {

	private static final String SEPERATOR = "|";

	public static String makeUniqueIdForModifications(Integer... inputParams) {
		String key = "";
		for (Integer input : inputParams) {
			key += input.toString() + SEPERATOR;
		}
		return key;
	}

	// get pnr-pax-id
	public static String getPnrPaxId(String uniqueId) {
		return uniqueId.substring(0, uniqueId.indexOf(SEPERATOR));
	}

	// get seat code string for audit
	public static String getSeatAuditString(Collection<String> seatCodes) {
		StringBuilder sb = new StringBuilder();
		for (String string : seatCodes) {
			if (sb.length() < 1) {
				sb.append(string);
			} else {
				sb.append("," + string);
			}
		}
		return sb.toString();
	}

	public static String getPaxTypeCode(ReservationPax reservationPax) {
		if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
			return ReservationInternalConstants.PassengerType.PARENT;
		} else {
			return reservationPax.getPaxType();
		}
	}

	/**
	 * Return collection of PnrPaxIds of the Reservation.
	 * 
	 * @param reservation
	 * @return
	 */
	// public static Collection<Integer> getPnrPaxIds(Reservation reservation) {
	// Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
	// Iterator<ReservationPax> iter = reservation.getPassengers().iterator();
	// while (iter.hasNext()) {
	// ReservationPax resPax = (ReservationPax) iter.next();
	// pnrPaxIds.add(resPax.getPnrPaxId());
	// }
	// return pnrPaxIds;
	// }

	public static Map<Integer, String> adaptToAHashMap(Collection<Integer> flightAmSeatIds) {
		Map<Integer, String> p = new HashMap<Integer, String>();
		for (Integer inte : flightAmSeatIds) {
			p.put(inte, null);
		}

		return p;
	}
}
