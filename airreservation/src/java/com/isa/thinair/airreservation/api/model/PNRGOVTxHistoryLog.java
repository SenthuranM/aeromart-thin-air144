package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * This will keep track of PnrGov Transaction History Log
 * 
 * @author Chinthaka Weerakkody
 * @since 1.0
 * @hibernate.class table = "T_PNRGOV_TX_HISTORY"
 */

public class PNRGOVTxHistoryLog extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer pnrGovTxHistoryID;

	private Integer fltSegID;

	private String status;

	private String description;

	private String userId;

	private Date transmissionTimeStamp;

	private String countryCode;

	private String airportCode;

	private String inboundOutbound;

	private Integer timePeriod;

	/**
	 * @return Returns the pnrGovTxHistoryID.
	 * 
	 * @hibernate.id column="PNRGOV_TX_HISTORY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PNRGOV_TX_HISTORY"
	 */
	public Integer getPnrGovTxHistoryID() {
		return pnrGovTxHistoryID;
	}

	public void setPnrGovTxHistoryID(Integer pnrGovTxHistoryID) {
		this.pnrGovTxHistoryID = pnrGovTxHistoryID;
	}

	/**
	 * @return Returns the fltSegID.
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFltSegID() {
		return fltSegID;
	}

	/**
	 * sets the fltSegID
	 * 
	 * @param fltSegID
	 *            The fltSegID to set.
	 */
	public void setFltSegID(Integer fltSegID) {
		this.fltSegID = fltSegID;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the userId.
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * sets the userId
	 * 
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the transmissionTimeStamp.
	 * @hibernate.property column = "TRANSMISSION_TIMESTAMP"
	 */
	public Date getTransmissionTimeStamp() {
		return transmissionTimeStamp;
	}

	/**
	 * sets the transmissionTimeStamp
	 * 
	 * @param transmissionTimeStamp
	 *            The transmissionTimeStamp to set.
	 */
	public void setTransmissionTimeStamp(Date transmissionTimeStamp) {
		this.transmissionTimeStamp = transmissionTimeStamp;
	}

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * sets the countryCode
	 * 
	 * @param countryCode
	 *            The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return Returns the airportCode.
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * sets the airportCode
	 * 
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return Returns the inboundOutbound.
	 * @hibernate.property column = "INBOUND_OUTBOUND"
	 */
	public String getInboundOutbound() {
		return inboundOutbound;
	}

	/**
	 * sets the inboundOutbound
	 * 
	 * @param inboundOutbound
	 *            The inboundOutbound to set.
	 */
	public void setInboundOutbound(String inboundOutbound) {
		this.inboundOutbound = inboundOutbound;
	}

	/**
	 * @return Returns the timePeriod.
	 * @hibernate.property column = "TIME_PERIOD"
	 */

	public Integer getTimePeriod() {
		return timePeriod;
	}

	/**
	 * sets the timePeriod
	 * 
	 * @param timePeriod
	 *            The timePeriod to set.
	 */
	public void setTimePeriod(Integer timePeriod) {
		this.timePeriod = timePeriod;
	}

}
