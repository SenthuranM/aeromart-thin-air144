/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of reservation segment information
 * 
 * @author Byorn
 * @since 1.0
 * @hibernate.class table = "T_PNR_INSURANCE"
 */
public class ReservationInsurance extends Persistent implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer insuranceId;
	private String messageId;
	private String policyCode;
	private BigDecimal amount;
	private String state;
	private String description;
	private String origin;
	private String destination;
	private Date dateOfTravel;
	private Date dateOfReturn;
	private String roundTrip;
	private Integer totalPaxCount;
	private Date quoteTime;
	private Date sellTime;
	private BigDecimal taxAmount;
	private BigDecimal netAmount;
	private String userId;

	private String quotedCurrencyCode;
	private BigDecimal quotedAmount;
	private BigDecimal quotedTaxAmout;
	private BigDecimal quotedNetAmount;
	private String marketingCarrier;

	private String endDateOffset;
	private String startDateOffset;

	private String pnr;
	/** Lalanthi - Added for RAK insurance and refactored existing AIG flow as well. */

	private Integer insuranceType;
	private BigDecimal insuranceTotalTicketPrice;

	private Integer autoCancellationId;
	private String ssrFeeCode;
	private String planCode;
	private Integer salesChannel;
	private Integer insuranceProductConfigID;

	/**
	 * @return the insuranceId
	 * @hibernate.id column = "INS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_PNR_INSURANCE"
	 */
	public Integer getInsuranceId() {
		return insuranceId;
	}

	/**
	 * @return the messageId
	 * @hibernate.property column = "MSG_ID"
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @return the policyCode
	 * @hibernate.property column = "POLICY_CODE"
	 * 
	 */
	public String getPolicyCode() {
		return policyCode;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @return the state
	 * @hibernate.property column = "STATE"
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the description
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param insuranceId
	 *            the insuranceId to set
	 */
	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}

	/**
	 * @param messageId
	 *            the messageId to set
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * @param policyCode
	 *            the policyCode to set
	 */
	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the origin
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @return the destination
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @return the dateOfTravel
	 * @hibernate.property column = "DATE_OF_TRAVEL"
	 */
	public Date getDateOfTravel() {
		return dateOfTravel;
	}

	/**
	 * @return the dateOfReturn
	 * @hibernate.property column = "DATE_OF_RETURN"
	 */
	public Date getDateOfReturn() {
		return dateOfReturn;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @param dateOfTravel
	 *            the dateOfTravel to set
	 */
	public void setDateOfTravel(Date dateOfTravel) {
		this.dateOfTravel = dateOfTravel;
	}

	/**
	 * @param dateOfReturn
	 *            the dateOfReturn to set
	 */
	public void setDateOfReturn(Date dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}

	/**
	 * @hibernate.property column = "ROUND_TRIP"
	 * @return
	 */
	public String getRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}

	/**
	 * @hibernate.property column = "QUOTE_TIMESTAMP"
	 * @return
	 */
	public Date getQuoteTime() {
		return quoteTime;
	}

	public void setQuoteTime(Date quoteTime) {
		this.quoteTime = quoteTime;
	}

	/**
	 * @hibernate.property column = "SELL_TIMESTAMP"
	 * @return
	 */
	public Date getSellTime() {
		return sellTime;
	}

	public void setSellTime(Date sellTime) {
		this.sellTime = sellTime;
	}

	/**
	 * @hibernate.property column = "TOT_PAX_COUNT"
	 * @return
	 */
	public Integer getTotalPaxCount() {
		return totalPaxCount;
	}

	public void setTotalPaxCount(Integer totalPaxCount) {
		this.totalPaxCount = totalPaxCount;
	}

	/**
	 * @return the taxAmount
	 * @hibernate.property column = "TAX_AMOUNT"
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * @return the netAmount
	 * @hibernate.property column = "NET_AMOUNT"
	 */
	public BigDecimal getNetAmount() {
		return netAmount;
	}

	/**
	 * @param netAmount
	 *            the amount to set
	 */
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the quotedCurrencyCode
	 * @hibernate.property column = "QUOTED_CURRENCY"
	 */
	public String getQuotedCurrencyCode() {
		return quotedCurrencyCode;
	}

	/**
	 * @param quotedCurrencyCode
	 *            the quotedCurrencyCode to set
	 */
	public void setQuotedCurrencyCode(String quotedCurrencyCode) {
		this.quotedCurrencyCode = quotedCurrencyCode;
	}

	/**
	 * @return the quotedAmount
	 * @hibernate.property column = "QUOTED_AMOUNT"
	 */
	public BigDecimal getQuotedAmount() {
		return quotedAmount;
	}

	/**
	 * @param quotedAmount
	 *            the quotedAmount to set
	 */
	public void setQuotedAmount(BigDecimal quotedAmount) {
		this.quotedAmount = quotedAmount;
	}

	/**
	 * @return the quotedTaxAmout
	 * @hibernate.property column = "QUOTED_TAX_AMOUNT"
	 */
	public BigDecimal getQuotedTaxAmout() {
		return quotedTaxAmout;
	}

	/**
	 * @param quotedTaxAmout
	 *            the quotedTaxAmout to set
	 */
	public void setQuotedTaxAmout(BigDecimal quotedTaxAmout) {
		this.quotedTaxAmout = quotedTaxAmout;
	}

	/**
	 * @return the quotedNetAmount
	 * @hibernate.property column = "QUOTED_NET_AMOUNT"
	 */
	public BigDecimal getQuotedNetAmount() {
		return quotedNetAmount;
	}

	/**
	 * @param quotedNetAmount
	 *            the quotedNetAmount to set
	 */
	public void setQuotedNetAmount(BigDecimal quotedNetAmount) {
		this.quotedNetAmount = quotedNetAmount;
	}

	/**
	 * @return the quotedNetAmount
	 * @hibernate.property column = "MARKETING_CARRIER"
	 */
	public String getMarketingCarrier() {
		return marketingCarrier;
	}

	/**
	 * @param marketingCarrier
	 *            the marketingCarrier to set
	 */
	public void setMarketingCarrier(String marketingCarrier) {
		this.marketingCarrier = marketingCarrier;
	}

	/**
	 * @return the quotedNetAmount
	 * @hibernate.property column = "END_DATE_OFFSET"
	 */
	public String getEndDateOffset() {
		return endDateOffset;
	}

	/**
	 * @param endDateOffset
	 *            the endDateOffset to set
	 */
	public void setEndDateOffset(String endDateOffset) {
		this.endDateOffset = endDateOffset;
	}

	/**
	 * @return the quotedNetAmount
	 * @hibernate.property column = "START_DATE_OFFSET"
	 */
	public String getStartDateOffset() {
		return startDateOffset;
	}

	/**
	 * @param startDateOffset
	 *            the startDateOffset to set
	 */
	public void setStartDateOffset(String startDateOffset) {
		this.startDateOffset = startDateOffset;
	}

	/***
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/***
	 * @hibernate.property column = "INS_TYPE"
	 */
	public Integer getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(Integer insuranceType) {
		this.insuranceType = insuranceType;
	}

	/**
	 * @return the insurance total ticket price
	 * @hibernate.property column = "INS_TOTAL_TICKET_PRICE"
	 */
	public BigDecimal getInsuranceTotalTicketPrice() {
		if (insuranceTotalTicketPrice == null) {
			insuranceTotalTicketPrice = BigDecimal.ZERO;
		}
		return insuranceTotalTicketPrice;
	}

	public void setInsuranceTotalTicketPrice(BigDecimal insuranceTotalTicketPrice) {
		this.insuranceTotalTicketPrice = insuranceTotalTicketPrice;
	}

	/**
	 * @return
	 * @hibernate.property column = "AUTO_CANCELLATION_ID"
	 */
	public Integer getAutoCancellationId() {
		return autoCancellationId;
	}

	public void setAutoCancellationId(Integer autoCancellationId) {
		this.autoCancellationId = autoCancellationId;
	}

	/**
	 * @return the ssrFeeCode
	 * @hibernate.property column = "SSR_FEE_CODE"
	 * 
	 */
	public String getSsrFeeCode() {
		return ssrFeeCode;
	}

	public void setSsrFeeCode(String ssrFeeCode) {
		this.ssrFeeCode = ssrFeeCode;
	}

	/**
	 * @return the planCode
	 * @hibernate.property column = "PLAN_CODE"
	 * 
	 */
	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String PlanCode) {
		this.planCode = PlanCode;
	}
	
	

	/**
	 * @return the salesChannel
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannel() {
		return salesChannel;
	}

	/**
	 * @param salesChannel the salesChannel to set
	 */
	public void setSalesChannel(Integer salesChannel) {
		this.salesChannel = salesChannel;
	}
	
	/**
	 * @return the insuranceProductConfigID
	 * @hibernate.property column = "INSURANCE_PRODUCT_CONFIG_ID"
	 * 
	 */
	public Integer getInsuranceProductConfigID() {
		return insuranceProductConfigID;
	}

	public void setInsuranceProductConfigID(Integer insuranceProductConfigID) {
		this.insuranceProductConfigID = insuranceProductConfigID;
	}

	public Object clone() {
		ReservationInsurance inst = new ReservationInsurance();
		inst.messageId = this.messageId == null ? null : this.messageId;
		inst.policyCode = this.policyCode == null ? null : this.policyCode;
		inst.amount = this.amount == null ? null : this.amount;
		inst.state = this.state == null ? null : this.state;
		inst.description = this.description == null ? null : this.description;
		inst.origin = this.origin == null ? null : this.origin;
		inst.destination = this.destination == null ? null : this.destination;
		inst.dateOfTravel = this.dateOfTravel == null ? null : (Date) this.dateOfTravel.clone();
		inst.dateOfReturn = this.dateOfReturn == null ? null : (Date) this.dateOfReturn.clone();
		inst.roundTrip = this.roundTrip == null ? null : new String(this.roundTrip);
		inst.totalPaxCount = this.totalPaxCount == null ? null : new Integer(this.totalPaxCount.intValue());
		inst.quoteTime = this.quoteTime == null ? null : (Date) this.quoteTime.clone();
		inst.sellTime = this.sellTime == null ? null : (Date) this.sellTime.clone();
		inst.taxAmount = this.taxAmount == null ? null : this.taxAmount;
		inst.netAmount = this.netAmount == null ? null : this.netAmount;
		inst.userId = this.userId == null ? null : new String(this.userId);
		inst.quotedCurrencyCode = this.quotedCurrencyCode == null ? null : this.quotedCurrencyCode;
		inst.quotedAmount = this.quotedAmount == null ? null : this.quotedAmount;
		inst.quotedTaxAmout = this.quotedTaxAmout == null ? null : this.quotedTaxAmout;
		inst.quotedNetAmount = this.quotedNetAmount == null ? null : this.quotedNetAmount;
		inst.marketingCarrier = this.marketingCarrier == null ? null : this.marketingCarrier;
		inst.endDateOffset = this.endDateOffset == null ? null : this.endDateOffset;
		inst.startDateOffset = this.startDateOffset == null ? null : this.startDateOffset;
		inst.pnr = this.pnr == null ? null : this.pnr;
		inst.insuranceType = this.insuranceType == null ? null : this.insuranceType;
		inst.insuranceTotalTicketPrice = this.insuranceTotalTicketPrice == null ? null : this.insuranceTotalTicketPrice;
		inst.autoCancellationId = this.autoCancellationId;
		inst.ssrFeeCode = this.ssrFeeCode == null ? null : this.ssrFeeCode;
		inst.planCode = this.planCode == null ? null : this.planCode;
		inst.salesChannel = this.salesChannel == null ? null : new Integer(this.salesChannel.intValue());
		return inst;
	}

	
}
