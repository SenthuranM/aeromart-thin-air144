package com.isa.thinair.airreservation.core.service.bd;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class TestReservationBD {
	
	public static void main(String [] args) throws ModuleException{
		System.setProperty("repository.home", "c:/isaconfig-test");
		ModuleFramework.startup();
		new ClientLoginModule().login("SYSTEM", "password", "c://client_jass_login.config", "jbossClientLogin");
		
//		Map extPayTnxs = getExtPayTnxs();
//		if (extPayTnxs != null && extPayTnxs.containsKey("11678596")){
//			PNRExtTransactionsTO pnrExtTransactionsTO = (PNRExtTransactionsTO) extPayTnxs.get("11678596");
//			System.out.println("results = " + pnrExtTransactionsTO.getExtPayTransactions().size());
//			
//			System.out.println("calling reconcile");
//			reconcile(pnrExtTransactionsTO);
//			System.out.println("completed reconcile");
//		}
//		
//		PNRExtTransactionsTO pnrExtTransactionsTO = getPNRExtTnxs();
//		if (pnrExtTransactionsTO != null && pnrExtTransactionsTO.getExtPayTransactions() != null){
//			System.out.println("tnxs in PNRExtTransactionsTO = " + pnrExtTransactionsTO.getExtPayTransactions().size());
//		}
		
		String pnr = "12846456";
		PNRExtTransactionsTO pnrTxns = new PNRExtTransactionsTO();
		pnrTxns.setPnr(pnr);
		
		ExternalPaymentTnx txn1 = new ExternalPaymentTnx();
		txn1.setPnr(pnr);
		txn1.setBalanceQueryKey("12846456GK16141658968");
		txn1.setAmount(new BigDecimal(940.00));
		txn1.setChannel("9");
		txn1.setExternalPayStatus("BE");
		txn1.setExternalPayId("5555");
		pnrTxns.addExtPayTransactions(txn1);
		
//		ExternalPaymentTnx txn2 = new ExternalPaymentTnx();
//		txn2.setPnr(pnr);
//		txn2.setBalanceQueryKey("11678916GG17165423000");
//		txn2.setAmount(540.00);
//		txn2.setChannel("9");
//		txn2.setExternalPayStatus("I");
//		txn2.setExternalPayId(null);
//		pnrTxns.addExtPayTransactions(txn2);
//		
//		ExternalPaymentTnx txn3 = new ExternalPaymentTnx();
//		txn3.setPnr(pnr);
//		txn3.setBalanceQueryKey("11678914GG17152313718");
//		txn3.setAmount(145.00);
//		txn3.setChannel("9");
//		txn3.setExternalPayStatus("S");
//		txn3.setExternalPayId("2");
//		pnrTxns.addExtPayTransactions(txn3);
		
		reconcile(pnrTxns, false);
	}
	
	private static Map getExtPayTnxs() throws ModuleException{
		ExtPayTxCriteriaDTO criteriaDTO = new ExtPayTxCriteriaDTO();
		criteriaDTO.setPnr("11678596");
		criteriaDTO.addStatus(ReservationInternalConstants.ExtPayTxStatus.SUCCESS);
		criteriaDTO.addReconStatusToInclude(ReservationInternalConstants.ExtPayTxReconStatus.NOT_RECONCILED);
		return ReservationModuleUtils.getReservationQueryBD().getExtPayTransactions(criteriaDTO);
	}
	
	private static PNRExtTransactionsTO getPNRExtTnxs() throws ModuleException{
		return ReservationModuleUtils.getReservationQueryBD().getPNRExtPayTransactions("11678596");	
	}
	
	private static void reconcile(PNRExtTransactionsTO pnrExtTnxs, boolean isManual) throws ModuleException{
		
		Calendar startTimestamp = new GregorianCalendar();
		//startTimestamp.add(Calendar.DAY_OF_MONTH, dayOffset);
		startTimestamp.set(Calendar.HOUR_OF_DAY, 0);
		startTimestamp.set(Calendar.MINUTE, 0);
		startTimestamp.set(Calendar.SECOND, 0);
		startTimestamp.set(Calendar.MILLISECOND, 0);
		
		Calendar endTimestmap = new GregorianCalendar();
		//endTimestmap.add(Calendar.DAY_OF_MONTH, dayOffset);
		endTimestmap.set(Calendar.HOUR_OF_DAY, 23);
		endTimestmap.set(Calendar.MINUTE, 59);
		endTimestmap.set(Calendar.SECOND, 59);
		endTimestmap.set(Calendar.MILLISECOND, 999);
		
		Collection<PNRExtTransactionsTO> col = new ArrayList<PNRExtTransactionsTO>();
		if (pnrExtTnxs != null) col.add(pnrExtTnxs);
		ReservationModuleUtils.getReservationBD().reconcileExtPayTransactions(col, isManual, 
				startTimestamp.getTime(), endTimestmap.getTime(), null);
	}

}
