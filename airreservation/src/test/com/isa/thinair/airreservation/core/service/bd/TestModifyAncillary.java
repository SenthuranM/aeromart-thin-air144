package com.isa.thinair.airreservation.core.service.bd;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airreservation.api.dto.AncillaryAssembler;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ssr.HalaExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CommonsTestcase;


public class TestModifyAncillary extends CommonsTestcase {
	
	private ReservationBD reservationBD;

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		reservationBD = ReservationModuleUtils.getReservationBD();
	}

	public void testModifyAncillary() throws ModuleException {
		//cancelHalaServices();
		addHalaServices();
	}

	private void cancelHalaServices() throws ModuleException {
		String pnr = "17537274";
		Reservation reservation = loadReservation(pnr);
		String userNotes = "This is a user note"  + (new Date());
		long version = reservation.getVersion();
		boolean isForceConfirmed = false;
		Collection<AncillaryAssembler> ancillaryAssemblers = getCancelledHalaServices();
		
		reservationBD.modifyAncillaries(pnr, userNotes, version, null, isForceConfirmed, ancillaryAssemblers, null);
		
	}
	
	private void addHalaServices() throws ModuleException {
		String pnr = "17537274";
		Reservation reservation = loadReservation(pnr);
		String userNotes = "This is a user note "  + (new Date());
		long version = reservation.getVersion();
		IPassenger iPassenger = getIPassenger();
		boolean isForceConfirmed = false;
		Collection<AncillaryAssembler> ancillaryAssemblers = getNewHalaServices(iPassenger);
		
		reservationBD.modifyAncillaries(pnr, userNotes, version, iPassenger, isForceConfirmed, ancillaryAssemblers, null);
		
	}

	private Collection<AncillaryAssembler> getCancelledHalaServices() {
		Collection<AncillaryAssembler> ancillaryAssemblers = new ArrayList<AncillaryAssembler>();
		
		//--- Hala ---
		SegmentSSRAssembler ssrAsm = new SegmentSSRAssembler();
		
		Integer paxSegSSRId = 2140;
		Integer paxSeguence1 = 1;
		ssrAsm.setPaxSeguence(paxSeguence1);
		ancillaryAssemblers.add(ssrAsm);
		//canceled hala service pax segment ssr id
		ssrAsm.addCanceledPaxSegmentSSR(paxSegSSRId);
		
		
		return ancillaryAssemblers;
	}
	
	private Collection<AncillaryAssembler> getNewHalaServices(IPassenger iPassenger) {
		Collection<AncillaryAssembler> ancillaryAssemblers = new ArrayList<AncillaryAssembler>();
		
		//--- Hala ---
		SegmentSSRAssembler ssrAsm = new SegmentSSRAssembler();
		
////		Integer segmentSequence = 1;
////		Integer ssrId = 1;
////		String ssrText = "adding hala service";
////		Integer contextId = 1;
////		double chargeAmount = 100;
//		
		Integer paxSeguence1 = 1;
//
		//Hala ssr detais pick from the iPassenger
		ssrAsm.setPassengerAssembler(iPassenger);
		ssrAsm.setPaxSeguence(paxSeguence1);
		ancillaryAssemblers.add(ssrAsm);

		//SSR data should pick from HalaExternalChargesDTO s
//		//new hala service
//		ssrAsm.addPaxSegmentSSR(segmentSequence, ssrId, ssrText, contextId, AccelAeroCalculator.parseBigDecimal(chargeAmount));
//		
		return ancillaryAssemblers;
	}

	private IPassenger getIPassenger() {
		IPassenger iPassenger = new PassengerAssembler(null);
		IPayment iPayment = new PaymentAssembler();
		Collection<ExternalChgDTO> extCharges = new ArrayList<ExternalChgDTO>();
		
		double totalPaxAmount = 100;
		double ssrCharge = 100;
		Integer flightSegmentId = 191647;
		Integer pnrPaxId = 20093618;
		Integer segmentSequence = 1;
		
		//iPayment.addCashPayment(AccelAeroCalculator.parseBigDecimal(totalPaxAmount));
		
		//add MASS single charge
		HalaExternalChargeDTO halaExChgDTO = new HalaExternalChargeDTO();
		halaExChgDTO.setSSRId(1);
		halaExChgDTO.setChgGrpCode(PricingConstants.ChargeGroups.THIRDPARTY_SURCHARGES);
		
		halaExChgDTO.setChgRateId(new Integer(5634));
		
		halaExChgDTO.setAmount(AccelAeroCalculator.parseBigDecimal(ssrCharge));
		
		halaExChgDTO.setSegmentSequece(segmentSequence);
		
		halaExChgDTO.setFlightSegmentID(flightSegmentId);
		halaExChgDTO.setApplyOn(ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE);
		
		extCharges.add(halaExChgDTO);
		iPayment.addExternalCharges(extCharges);
		
		iPassenger.addPassengerPayments(pnrPaxId, iPayment);
		
		return iPassenger;
	}

	private Reservation loadReservation(String pnrNo) throws ModuleException {
		PnrModesDTO pnrModesDTO = new PnrModesDTO();
        pnrModesDTO.setPnr(pnrNo);
        pnrModesDTO.setLoadSegView(false);

        Reservation reservation = reservationBD.getReservation(pnrModesDTO, null);
        
		return reservation;
	}
	
	
	
	
}
