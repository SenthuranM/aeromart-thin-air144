package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRSearchDTO;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.ssr.ReservationSSRResultDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsTestcase;

public class TestReservationSSRDAOImpl extends CommonsTestcase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testGetReservationSSR() throws ModuleException{
		ReservationSSRSearchDTO reservationSSRSearchDTO = new ReservationSSRSearchDTO();
		Collection<ReservationSSRResultDTO> ssrResultDTOs = ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getReservationSSR(reservationSSRSearchDTO);
		
		for (Iterator<ReservationSSRResultDTO> itSSRResultDTOs = ssrResultDTOs.iterator(); itSSRResultDTOs.hasNext();) {
			ReservationSSRResultDTO reservationSSRResultDTO = (ReservationSSRResultDTO) itSSRResultDTOs.next();
			
			System.out.println(reservationSSRResultDTO.getTimeStamp());
			System.out.println(reservationSSRResultDTO.getPNRNo());
			System.out.println(reservationSSRResultDTO.getTitle());
			System.out.println(reservationSSRResultDTO.getFirstName());
			System.out.println(reservationSSRResultDTO.getLastName());
			System.out.println(reservationSSRResultDTO.getContactNo());
			
			System.out.println(reservationSSRResultDTO.getFlightNo());
			
			System.out.println(reservationSSRResultDTO.getServiceDescription());
			System.out.println(reservationSSRResultDTO.getServiceTime());
			
			
			System.out.println("----");
		}
	}
	
	public void ntestGetPNRPaxSSRs() throws ModuleException{
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		
		pnrPaxIds.add(new Integer(20091554));
		
		Map<Integer, Collection<PaxSSRDetailDTO>> ssrResultDTOMap = ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getPNRPaxSSRs(pnrPaxIds, null, false, false, false);
		Collection<Collection<PaxSSRDetailDTO>> ssrResultDTOs = ssrResultDTOMap.values();
		
		for (Iterator<Collection<PaxSSRDetailDTO>> itSSRResultDTOs = ssrResultDTOs.iterator(); itSSRResultDTOs.hasNext();) {
			Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = (Collection<PaxSSRDetailDTO>) itSSRResultDTOs.next();
			
			for (Iterator<PaxSSRDetailDTO> iterator = paxSSRDetailDTOs.iterator(); iterator.hasNext();) {
				PaxSSRDetailDTO paxSSRDetailDTO = (PaxSSRDetailDTO) iterator.next();
				
				System.out.println(paxSSRDetailDTO.getPnrPaxId());
				System.out.println(paxSSRDetailDTO.getSsrId());
				System.out.println(paxSSRDetailDTO.getSsrText());
				System.out.println(paxSSRDetailDTO.getStatus());
			}
			
			
			System.out.println("----");
		}
	}

	 
}
