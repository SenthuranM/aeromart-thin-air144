package com.isa.thinair.airreservation.core.bl.pnl;

import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsTestcase;

public class TestPNLBL extends CommonsTestcase {
	private ReservationAuxilliaryBD reservationAuxBD;

	protected void setUp() throws Exception {
		super.setUp();
		reservationAuxBD = ReservationModuleUtils.getReservationAuxilliaryBD();
	}

	public void testSendPNL() throws ModuleException {
		int flightId = 189096;
		String airport = "SHJ";
		
		reservationAuxBD.sendPNL(flightId, airport);
	}
}
