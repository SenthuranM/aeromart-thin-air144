package com.isa.thinair.airreservation.core.service.bd;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IFaresChargesAndSeats;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.ssr.HalaExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.TestReservationUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalanderUtil;
import com.isa.thinair.commons.core.util.CommonsTestcase;
import com.isa.thinair.platform.api.ServiceResponce;


public class TestCreateCCReservation extends CommonsTestcase {
	private static final Integer SRI_LANKAN = new Integer(42);
	private static final int ADULT_COUNT = 1;
	private static final int CHILD_COUNT = 0;
	private static final int INFANT_COUNT = 0;
	private static final String FROM_AIRPORT = "CMB";
	private static final String TO_AIRPORT = "MCT";
	private static final String DEPARTURE_DATE = "10/24/2009";
	//private static final String RETURN_DATE = "09/30/2009";
	private static final String RETURN_DATE = null;
	private static final String PNR_NO = "17537215";
	private static final boolean LOAD_FLIGHT_SEGMENT_FROM_PNR = true;
	
	private ReservationQueryBD reservationQueryBD;
	private ChargeBD chargeBD;
	private ReservationBD reservationBD;
	private SegmentBD resSegmentBD;

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		reservationBD = ReservationModuleUtils.getReservationBD();
		reservationQueryBD = ReservationModuleUtils.getReservationQueryBD();
		chargeBD = ReservationModuleUtils.getChargeBD();
		resSegmentBD = ReservationModuleUtils.getSegmentBD();
	}

	// create reservation
	public void ntestCreateReservation() throws ModuleException {
		SelectedFlightDTO selectedFlightDTO = getFlights();

		createReservation(selectedFlightDTO);
	}
	
	// create reservation
	public void ntestAddSegment() throws ModuleException {
		SelectedFlightDTO selectedFlightDTO = getFlights();
		createSegment(selectedFlightDTO);
		
	}
	

	public void testGetSSRCharges() throws ModuleException {
		SSRChargeSearchDTO ssrChargeSearchDTO = new SSRChargeSearchDTO();
		
		ssrChargeSearchDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);
		
		Collection<FlightSegmentDTO> flightSegments = null;
		
		if (LOAD_FLIGHT_SEGMENT_FROM_PNR) {
			Reservation reservation = TestReservationUtil.loadReservation(PNR_NO);
			flightSegments = getFlightSegments(reservation);
			
			ssrChargeSearchDTO.setFromAirpot(FROM_AIRPORT);
			ssrChargeSearchDTO.setToAirpot(TO_AIRPORT);

			ssrChargeSearchDTO.setAdultCount(reservation.getTotalPaxAdultCount());
			ssrChargeSearchDTO.setChildCount(reservation.getTotalPaxChildCount());
			ssrChargeSearchDTO.setInfantCount(reservation.getTotalPaxInfantCount());
			
		} else {
			ssrChargeSearchDTO.setFromAirpot(FROM_AIRPORT);
			ssrChargeSearchDTO.setToAirpot(TO_AIRPORT);
			
			ssrChargeSearchDTO.setAdultCount(ADULT_COUNT);
			ssrChargeSearchDTO.setChildCount(CHILD_COUNT);
			ssrChargeSearchDTO.setInfantCount(INFANT_COUNT);
			
			flightSegments = getFlightSegments();
		}
				
		ssrChargeSearchDTO.setFlightSegments(flightSegments);
		HashMap<String, Collection<SSRChargeDTO>> ssrChargesMap = chargeBD.getSSRCharges(ssrChargeSearchDTO);
		
		Collection<String> airports = ssrChargesMap.keySet();
		
		for (Iterator<String> iterator = airports.iterator(); iterator.hasNext();) {
			String airport =  (String) iterator.next();
			Collection<SSRChargeDTO> ssrCharges = ssrChargesMap.get(airport);
			System.out.println("Airport:" + airport);
			
			for (Iterator<SSRChargeDTO> iterator2 = ssrCharges.iterator(); iterator2.hasNext();) {
				SSRChargeDTO chargeDTO = (SSRChargeDTO) iterator2.next();
				
				System.out.println("SSRChargeCode:" + chargeDTO.getSSRChargeCode());
				System.out.println("SSRCode:" + chargeDTO.getSSRCode());
				
				System.out.println("ONDCode:" + chargeDTO.getONDCode());
				System.out.println("MaximumPaxCount:" + chargeDTO.getMaximumTotalPax());
				System.out.println("");
			}
			
		}
	}

	private Collection<FlightSegmentDTO> getFlightSegments() throws ModuleException {
		SelectedFlightDTO selectedFlightDTO = getFlights();
		return selectedFlightDTO.getFlightSegmentDTOs();
	}
	
	private Collection<FlightSegmentDTO> getFlightSegments(Reservation reservation) throws ModuleException {
		Collection<FlightSegmentDTO> flightSegmentDTOs = ReservationApiUtils.getFlightSegments(reservation);
		return flightSegmentDTOs;
	}

	private SelectedFlightDTO getFlights() throws ModuleException {
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		System.setProperty("user.timezone", "GMT");
		
		availableFlightSearchDTO.setDepartureVariance(0);
		//availableFlightSearchDTO.setArrivalVariance(0);
		availableFlightSearchDTO.setReturnFlag(false);

		availableFlightSearchDTO.setAdultCount(ADULT_COUNT);

		// search flights for the open return inward flight
		availableFlightSearchDTO.setFromAirport(FROM_AIRPORT);
		availableFlightSearchDTO.setToAirport(TO_AIRPORT);
		
		availableFlightSearchDTO.setDepatureDate(getSqlDate(DEPARTURE_DATE));
		
		if (RETURN_DATE != null) {
			availableFlightSearchDTO.setReturnFlag(true);
			availableFlightSearchDTO.setReturnDate(getSqlDate(RETURN_DATE));
		}
		
		availableFlightSearchDTO.setPosAirport(FROM_AIRPORT);
		availableFlightSearchDTO.setAgentCode("SHJ001");
		availableFlightSearchDTO.setChannelCode(3);
		availableFlightSearchDTO.setCabinClassCode("Y");

		// only single segment ONDs should display(No connected flights/No
		// Returns)
		availableFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
        availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);

		availableFlightSearchDTO.setBookingType(BookingClass.BookingClassType.NORMAL);

		//availableFlightSearchDTO.setFirstDepartureDateTimeZulu(depDate);
		//availableFlightSearchDTO.setLastArrivalDateTimeZulu(arrDate);

		availableFlightSearchDTO.setBookingPaxType(AirPricingCustomConstants.BookingPaxType.ANY);
		availableFlightSearchDTO.setFareCategoryType(AirPricingCustomConstants.FareCategoryType.ANY);

		AvailableFlightDTO availableFlightDTO = reservationQueryBD.getAvailableFlightsWithAllFares(availableFlightSearchDTO);
		SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();

		return selectedFlightDTO;
	}

	private void createReservation(SelectedFlightDTO selectedFlightDTO) throws ModuleException {
		IFaresChargesAndSeats faresChargesAndSeats = (IFaresChargesAndSeats) selectedFlightDTO;
		Collection collFareDTO = null;

		if (selectedFlightDTO == null) {
			throw new RuntimeException("No fligts for the selected date");
		}

		if (faresChargesAndSeats.getFareType() != FareTypes.NO_FARE) {
			collFareDTO = faresChargesAndSeats.getFareChargesSeatsSegmentsDTOs();
		} else {
			throw new RuntimeException("No Fares found");
		}

		IReservation reservation = new ReservationAssembler(collFareDTO);

		addPassengers(reservation, collFareDTO, selectedFlightDTO);
		addContactDetails(reservation);
		addSegments(reservation, selectedFlightDTO);
		
		ServiceResponce response = createWEbReservation(reservation);
		//ServiceResponce response = createCCReservation(reservation);
		
		if(response.isSuccess()) {
			String pnrNo = (String) response.getResponseParam(CommandParamNames.PNR);
			System.out.println("pnrNo: " + pnrNo);
		} else {
			throw new RuntimeException("Create Reservation Unit Test faild");
		}
	}
	
	private void createSegment(SelectedFlightDTO selectedFlightDTO) throws ModuleException {
		IFaresChargesAndSeats faresChargesAndSeats = (IFaresChargesAndSeats) selectedFlightDTO;
		Collection collFareDTO = null;

		if (selectedFlightDTO == null) {
			throw new RuntimeException("No fligts for the selected date");
		}

		if (faresChargesAndSeats.getFareType() != FareTypes.NO_FARE) {
			collFareDTO = faresChargesAndSeats.getFareChargesSeatsSegmentsDTOs();
		} else {
			throw new RuntimeException("No Fares found");
		}

		Reservation reservation = TestReservationUtil.loadReservation("17536035");
		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT;
		
		SegmentAssembler iSegmentAsm = new SegmentAssembler(collFareDTO);;
		
		addSegments(iSegmentAsm, selectedFlightDTO, reservation);
		addMODSegPaxPayments(iSegmentAsm, selectedFlightDTO, collFareDTO, reservation);
		
		Collection colBlockKeyIds = reservationBD.blockSeats(collFareDTO,null);
		
		ServiceResponce response = resSegmentBD.addSegments(reservation.getPnr(), iSegmentAsm, colBlockKeyIds,
				intPaymentMode, reservation.getVersion(), null);
		
		if(response.isSuccess()) {
			String pnrNo = (String) response.getResponseParam(CommandParamNames.PNR);
			System.out.println("pnrNo: " + pnrNo);
		} else {
			throw new RuntimeException("Create Reservation Unit Test faild");
		}
	}

	private void addMODSegPaxPayments(SegmentAssembler segmentAsm, SelectedFlightDTO selectedFlightDTO, Collection collFareDTO, Reservation reservation) {
		IPayment paxPayment = new PaymentAssembler();
		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();
		SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
		BigDecimal[] paxCharges = getPaxCharges(collFareDTO);
		BigDecimal adultCharge = paxCharges[0];
		BigDecimal ssrCharge = AccelAeroCalculator.parseBigDecimal(100); //MASS single charge
		FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) selectedFlightDTO.getFlightSegmentDTOs().iterator().next();
		
		paxPayment.addCashPayment(AccelAeroCalculator.add(adultCharge, new BigDecimal(100)));
		
		//add MASS single charge
		int segmentSeq = reservation.getSegments().size() + 1;
		HalaExternalChargeDTO halaExChgDTO = new HalaExternalChargeDTO();
		
		halaExChgDTO.setChgGrpCode(PricingConstants.ChargeGroups.THIRDPARTY_SURCHARGES);
		
		halaExChgDTO.setChgRateId(new Integer(5634));
		halaExChgDTO.setAmount(ssrCharge);
		halaExChgDTO.setSegmentSequece(segmentSeq);
		halaExChgDTO.setFlightSegmentID(flightSegmentDTO.getSegmentId());
		halaExChgDTO.setApplyOn(ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL);
		
		//MASS
		halaExChgDTO.setSSRId(new Integer("3"));
		//MASS single
		halaExChgDTO.setSSRChargeId(new Integer("1"));
		halaExChgDTO.setSSRText("MASS Single");
		halaExChgDTO.setAmountConsumedForPayment(false);
		
		colExternalChgDTO.add(halaExChgDTO);
		
		paxPayment.addExternalCharges(colExternalChgDTO);
		
		Integer pnrPaxId = ((ReservationPax)reservation.getPassengers().iterator().next()).getPnrPaxId();
		segmentAsm.addPassengerPayments(pnrPaxId, paxPayment);
		
	}

	private ServiceResponce createWEbReservation(IReservation reservation) throws ModuleException {
		TrackInfoDTO trackInfoDTO = null;
		ServiceResponce response = reservationBD.createWebReservation(reservation, trackInfoDTO);
		
		return response;
		
	}
	
	private ServiceResponce createCCReservation(IReservation reservation) throws ModuleException {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setIpAddress("1.2.3.4");
		trackInfoDTO.setCarrierCode("G9");
		ServiceResponce response = reservationBD.createWebReservation(reservation, trackInfoDTO);
		
		return response;
		
	}

	private void addSegments(IReservation reservation, SelectedFlightDTO selectedFlightDTO) {
		FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) selectedFlightDTO.getFlightSegmentDTOs().iterator().next();

		reservation.addOutgoingSegment(new Integer(1), flightSegmentDTO.getSegmentId(), new Integer(1));

	}

	private void addSegments(SegmentAssembler segmentAsm, SelectedFlightDTO selectedFlightDTO, Reservation reservation) {
		FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) selectedFlightDTO.getFlightSegmentDTOs().iterator().next();
		int segmentSeq = reservation.getSegments().size() + 1;
		
		segmentAsm.addOutgoingSegment(segmentSeq, flightSegmentDTO.getSegmentId(), segmentSeq);
		
	}
	
	private void addPassengers(IReservation reservation, Collection collFareDTO, SelectedFlightDTO selectedFlightDTO) throws ModuleException {
		PaymentAssembler pax = new PaymentAssembler();
		SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();
		BigDecimal[] paxCharges = getPaxCharges(collFareDTO);
		BigDecimal adultCharge = paxCharges[0];
		BigDecimal ssrCharge = AccelAeroCalculator.parseBigDecimal(100); //MASS single charge
		FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) selectedFlightDTO.getFlightSegmentDTOs().iterator().next();
		
		pax.addCashPayment(adultCharge);
		
		//add MASS single charge
		HalaExternalChargeDTO halaExChgDTO = new HalaExternalChargeDTO();
		
		halaExChgDTO.setChgGrpCode(PricingConstants.ChargeGroups.THIRDPARTY_SURCHARGES);
		
		halaExChgDTO.setChgRateId(new Integer(5634));
		halaExChgDTO.setAmount(ssrCharge);
		halaExChgDTO.setSegmentSequece(new Integer("1"));
		halaExChgDTO.setFlightSegmentID(flightSegmentDTO.getSegmentId());
		halaExChgDTO.setApplyOn(ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL);
		
		//MASS
		halaExChgDTO.setSSRId(new Integer("3"));
		//MASS single
		halaExChgDTO.setSSRChargeId(new Integer("1"));
		halaExChgDTO.setSSRText("MASS Single");
		halaExChgDTO.setAmountConsumedForPayment(false);
		
		colExternalChgDTO.add(halaExChgDTO);
		
		pax.addExternalCharges(colExternalChgDTO);
		segmentSSRs.addPaxSegmentSSR(null, new Integer(8), "Large Bassinet");
		segmentSSRs.addPaxSegmentSSR(null, new Integer(15), "Dr. Mervin Silva");
		reservation.addSingle("Sudheera", "Liyanage", "MR", CalanderUtil.add(new Date(), 50, 0, 0, 0, 0, 0), SRI_LANKAN, 1,  null, pax, segmentSSRs);

	}

	private BigDecimal[] getPaxCharges(Collection collFareDTO) {
		BigDecimal[] paxCharge = new BigDecimal[3];
		BigDecimal bdATotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal bdCTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal bdITotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (Iterator iterator = collFareDTO.iterator(); iterator.hasNext();) {
			OndFareDTO ondFareDTO = (OndFareDTO) iterator.next();

			BigDecimal bdAFare = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare());
			BigDecimal bdCFare = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare());
			BigDecimal bdIFare = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare());

			bdATotal = AccelAeroCalculator.add(bdATotal, bdAFare);
			bdCTotal = AccelAeroCalculator.add(bdCTotal, bdCFare);
			bdITotal = AccelAeroCalculator.add(bdITotal, bdIFare);

			BigDecimal[] dblArrTax = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.TAX));
			BigDecimal bdTotATax = dblArrTax[0];
			BigDecimal bdTotCTax = dblArrTax[2];
			BigDecimal bdTotITax = dblArrTax[1];
			
			bdATotal = AccelAeroCalculator.add(bdATotal, bdTotATax);
			bdCTotal = AccelAeroCalculator.add(bdITotal, bdTotITax);
			bdITotal = AccelAeroCalculator.add(bdCTotal, bdTotCTax);

			BigDecimal[] dblSurcharges = AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.SURCHARGE));
			BigDecimal dblASurChg = dblSurcharges[0];
			BigDecimal dblCSurChg = dblSurcharges[2];
			BigDecimal dblISurChg = AccelAeroCalculator.add(dblSurcharges[1], AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.INFANT_SURCHARGE)[1]));
			
			bdATotal = AccelAeroCalculator.add(bdATotal, dblASurChg);
			bdITotal = AccelAeroCalculator.add(bdITotal, dblISurChg);
			bdCTotal = AccelAeroCalculator.add(bdCTotal, dblCSurChg);
		}

		paxCharge[0] = bdATotal;
		paxCharge[1] = bdCTotal;
		paxCharge[2] = bdITotal;

		return paxCharge;
	}

	private void addContactDetails(IReservation reservation) {
		// ---------------------
		// Get Customer Information
		// ---------------------
		ReservationContactInfo reservationContactInfo = new ReservationContactInfo();

		// String[] arrBuyersInfo =
		// StringUtils.split(request.getParameter("hdnContactInfo"), "^"); moved
		// to line 877
		reservationContactInfo.setTitle("MR");
		reservationContactInfo.setFirstName("Sudheera");
		reservationContactInfo.setLastName("Liyanage");
		reservationContactInfo.setStreetAddress1("No 148");
		reservationContactInfo.setStreetAddress2("Vax st");
		reservationContactInfo.setState("Western");
		reservationContactInfo.setCity("Colombo 2");
		reservationContactInfo.setCountryCode("SL");
		reservationContactInfo.setMobileNo("94-11-111");
		reservationContactInfo.setPhoneNo("94-11-111");
		reservationContactInfo.setFax("94-11-111");
		reservationContactInfo.setEmail("sudheera@jkcsworld.com");
		// reservationContactInfo.setNationalityCode(arrBuyersInfo[11].trim());
		reservation.addContactInfo("Test user notes", reservationContactInfo, "test type");

	}
	
	public static Date getSqlDate(String dateStr) {
		Date date = null;

			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	
			try {
				date = df.parse(dateStr);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}

		return date;
	}
	
	// create reservation
	public void ntestSendItinerary() throws ModuleException {
		ItineraryLayoutModesDTO modesDTO = new ItineraryLayoutModesDTO();
        modesDTO.setPnr("17531561");
        modesDTO.setLocale(Locale.getDefault());
        modesDTO.setIncludePassengerPrices(true);
        
		reservationBD.emailItinerary(modesDTO, null, null, false);
	}
}
