package com.isa.thinair.airreservation.core.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestReservationAuxiliaryBL extends PlatformTestCase{
	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testGetPNLADLTextParts() {
		String crlf = "\n";
		String partStr  = "ENDPART";
		String msgType = "ADL";
		String pnlADLOrgText = msgType + crlf + "ST0000/19JUN CMB PART1" + crlf 
		+ "-SHJ27Y" + crlf
		+ "2ALWIS/MANORIMS/SUNILMR-A6 .L/10002699 .R/2INF THANUJA HIMALI" +crlf
		+ partStr + 1 + crlf + crlf  
		+ msgType + crlf
		+ "ST1111/20JUN SHJ PART2" + crlf
		+ "-DOH76Y" + crlf
		+ "4FERNANDO/CHANDRIKAMS/DEVINICHD/RASIKAMR/SANDEEPCHD" + crlf
		+ "2ALWIS/MANORIMS/SUNILMR-A6 .L/10002699 .R/2INF THANUJA HIMALI" + crlf		
		+ ".O/ST1111Y20DOH" + crlf 
		+ partStr + 2 + crlf + crlf  
		+ msgType + crlf
		+ "ST1111/20JUN SHJ PART3" + crlf
		+ "-DOH76Y" + crlf
		+ "4PERERA/LASHINIMS/PAVITHRAMS/RENUKAKUMARICHD/SHANAKAMR-A6" + crlf
		+ "1WERRWER/WERWERWEMS-C2" + crlf 
		+ "END" + msgType + crlf;
		ReservationAuxiliaryBL reservationAuxiliaryBL = new ReservationAuxiliaryBL();
//		List pnlList = reservationAuxiliaryBL.getPNLADLTextParts(pnlADLOrgText, msgType);
//		System.out.println(pnlList.size());		
	}
	
	
	
//	public void TestSendPNL() throws Exception{
//		PNLMetaDataDTO pnlmetadatadto=new PNLMetaDataDTO();
//		
//		
//		pnlmetadatadto.setBoardingairport("SHJ");
//		pnlmetadatadto.setDay("7");
//		pnlmetadatadto.setFlight("G9505");
//		pnlmetadatadto.setMessageIdentifier("PNL");
//		pnlmetadatadto.setMonth("MAY");
//		pnlmetadatadto.setPartnumber(PNLConstants.PNLTransmission.PART_NUMBER);
//		pnlmetadatadto.setPart2(true);
//		
////	log.debug("At Execution flight number is"+PROP_FlightId +"Departure Station is"+PROP_DepartureStation);		
////		log.info("BEfore Sending the PNL for flight ID"+this.PROP_FlightId + new Date() + "Departure Station is"+PROP_DepartureStation  + "flight number is"+PROP_FLIGHT_NUMBER);
//		ReservationAuxiliaryBL reservationAuxiliaryBL = new ReservationAuxiliaryBL();
//		reservationAuxiliaryBL.sendPNL(5413,"SHJ","",new Timestamp((new Date()).getTime()) , pnlmetadatadto);
//	}
    
    
    public void testPNLFormatter() {
//       String text = "1DONALD/DANIELMR .L/10002706 .R/DEAF HES A GREAT SINGER AND SHOULD BE GIVEN A SPECIAL SEAT HK1-1DONALD/DANIELMR";
       
        String outputPNLString = "";
        //String text = "4PERERA/LASHINIMS/PAVITHRAMS/RENUKAKUMARICHD/SHANAKAMR-A6 .R/2INF VENUJAKUMARA DINUJA .R/DEAF DEAFD cc cc EAFDEAF hjkjk ertyu gfert hjkiu terty ujhg nr d HK1-1PERERA/LASHINIMS .R/BLND BLIN bb bb BB BB BB gggg ggg ghj yuiot ghj fg BB DBLIND HK1-1PERERA/RENUKAKUMARICHD .O/ST1111Y20DOH";
        //String text = "4PERERA/LASHINIMS/PAVITHRAMS/RENUKAKUMARICHD/SHANAKAMR-A6 .R/2INF VENUJAKUMARA DINUJA .R/DEAF DEAFDEAFDEAF HK1-1PERERA/LASHINIMS .R/BLND BLIND12345 kjdgl lkdgjl kdfjl ghghkjdfhgkdh djfghdkhjgdk hgkdjgh kjhk kjdfhkd kjkh  k BLIND HK1-1PERERA/RENUKAKUMARICHD .O/ST1111Y20DOHx";
        //String text = "4PERERA/LASHINIMS/PAVITHRAMS/RENUKAKUMARICHD/SHANAKAMR-A6 .R/2INF VENUJAKUMARA DINUJA .R/DEAF DEAFDEAFDEAF HK1-1PERERA/LASHINIMS .R/BLND BLINDBLIND HK1-1PERERA/RENUKAKUMARICHD .O/ST1111Y20DOH";
        //String text = "1ABCDABCDABCDABCDABCDAB/ABCDABCDABCDABCDABCDABCDABCDABMR .L/10002715 .R/WCHR ABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCDABCD HK1-1ABCDABCDABCDABCDABCDAB/ABCDABCDABCDABCDABCDABCDABCDABCMR";
        //String text = "3HUSAIN/ASGARALIFAZALMR/HAKEEMUDDINFAZALMR/ZAINUDDINFAZALMR-K18 .O/G9201Y27IKA";
        //String text = "2MISBAH/DONYAABDOULFATAHMR/HAGERABDOULFATAHMR-G4 .L/1614377";
        String text = "3HUSAIN/ASGARALIFAZALMR/HAKEEMUDDINFAZALMR/ZAINUDDINFAZALAMR-K18 .L/1614377";
        // Code Starts
          
StringBuffer buf = new StringBuffer();

//            String text = str.nextToken();
          
          System.out.println(">>>>>>>>>>>>>>>>>>>" + text);

            int lines = linesRequired(text);
          System.out.println("Text is"+text);
            System.out.println("................length" + text.length()
                    + "..............");


            
            if (lines > 1) {
                System.out.println("Number of lines are......." + lines);
                String s=null;
                char delimiter=' ';
                boolean end=false;
                int cc=0;
                while(cc<15) 
                {                    
                if(text.length()>64) 
                {
                    s = text.substring(0,64);
                    
                    
//                    delimiter = text.charAt(64);
                    delimiter = text.substring(64).trim().charAt(0);
                }   
                  
                else
                    {
                int hkIndex=    text.indexOf("HK1");
                int dotRIndex=text.indexOf('.');
//                if(text.indexOf("HK")>=0 & dotRIndex<hkIndex) 
//                    {
//                      System.out.println("<<<<<<<<<<<<<<<<<<" + text.trim());
//                        buf.append(text.trim());
//                        buf.append("\n");;
//                    }
//                else if(text.indexOf("HK")>=0)
//                {
//                  System.out.println("<<<<<<<<<<<<<<<<<<" + text.trim());
//                    buf.append(".RN/"+text.trim());
//                    buf.append("\n");;
//                }
                                    
//                else
                {
                      System.out.println("<<<<<<<<<<<<<<<<<<" + text.trim());
                      if (text.trim().indexOf(".")==0 ){
                          buf.append(text.trim());
                          buf.append("\n");;
                      }else if (text.length()<61){
                          buf.append(".RN/");
                          buf.append(text.trim());
                          buf.append("\n");;
                      }else{
                          int dotPlace = text.lastIndexOf(".");
                          int hkPlace = text.lastIndexOf("HK1");
                          int breakIndex = dotPlace;
                          if (dotPlace>0) {
                              breakIndex = dotPlace;
                          }else if (hkPlace > 0) {
                              breakIndex = hkPlace;
                          }
                           
                          if (breakIndex>-1) {
                              //Length >60 and there is a .X some where.
                              //Split at the last dot
                              buf.append(".RN/");
                              buf.append(text.substring(0,breakIndex+1));
                              buf.append("\n");;
                              buf.append(text.substring(breakIndex+1));
                              buf.append("\n");;
                          }else {
                              //Lenth >60 and no .X in the middle                                  
                              buf.append(".RN/");
                              buf.append(text.substring(0,60));
                              buf.append("\n");;
                              if (text.length()>60 ){
                                  buf.append(".RN/");
                                  buf.append(text.substring(60));
                                  buf.append("\n");;
                              }
                         }
                      }
                }
                        end=true;
                    break ;
                    }
                System.out.println("................" + delimiter
                        + "..............");

//                if (delimiter != ' ') {

                    int splitIndex = s.lastIndexOf('.');
                    
                    int dotPlace = s.lastIndexOf(".");
                    int hkPlace = s.lastIndexOf("HK1");
//                    splitIndex = dotPlace;
                    if (dotPlace>0) {
                        splitIndex = dotPlace;
                    }else if (hkPlace > 0) {
                        splitIndex = hkPlace;
                    }else if (delimiter == '.'){
                        splitIndex = 64;
                    }
                
                    
                    
                    
                
                 if(splitIndex<0 )
                    {
                        
                    String part1 = ".RN/"+text.substring(0,60);
                  System.out.println("<<<<<<<<<<<<<<<<<<" + text.trim());
                    buf.append(part1.trim());
                    buf.append("\n");;
                    int len=text.length();
                    text = text.substring(60,len);
                    System.out.println("Text Length is.........."+text.length());
                    }
                    
                    
                    
                    else if(splitIndex==0)
                    {
                    String tmptext=text.substring(0,64);
                    int tmplast=tmptext.lastIndexOf(".");                   
                    
                    if(tmplast==splitIndex)
                    {
                        
                        String part1 = text.substring(0,64);
                      System.out.println("<<<<<<<<<<<<<<<<<<" + part1.trim());
                        buf.append(part1.trim());
                        buf.append("\n");;
                        int len=text.length();
                        text = text.substring(64,len);
                            
                    }
                        
                    
                    }
                    
                    
                    else
                    {
                        if(!isNumber(text.charAt(0)) & splitIndex>0 & text.charAt(0)!='.' )
                        {
                            
                            String part1 =".RN/"+text.substring(0, splitIndex);
                          System.out.println("<<<<<<<<<<<<<<<<<<" + part1.trim());
                            buf.append(part1.trim());
                            buf.append("\n");;
                            int len=text.length();
                            text = text.substring(splitIndex,len);
                            System.out.println("Text Length is.........."+text.length());   
                        }
                        
                        
                        else if(!isNumber(text.charAt(0)) & splitIndex<0 & text.charAt(0)!='.')
                        {
                            String part1 =".RN/"+text.substring(0, 64);
                          System.out.println("<<<<<<<<<<<<<<<<<<" + part1.trim());
                            buf.append(part1.trim());
                            buf.append("\n");;
                            int len=text.length();
                            text = text.substring(64,len);
                            System.out.println("Text Length is.........."+text.length());       
                            
                        }
                        
                        else
                        {
                            
                            String part1 =text.substring(0, splitIndex);
                          System.out.println("<<<<<<<<<<<<<<<<<<" + part1.trim());
                            buf.append(part1.trim());
                            buf.append("\n");;
                            int len=text.length();
                            text = text.substring(splitIndex,len);
                            System.out.println("Text Length is.........."+text.length());
                            
                        }
                        
                        
                    }
//                    }
            cc++;
                }
            }


            else {
                int len = text.length();
                buf.append(text.trim());
                buf.append("\n");;
//                writebuffer.append(text);

                //lineCount++;
                // out.write(text);
                // out.write('\n');

            }
          
          
          System.out.println(buf.toString());
        //      Code Ends
    }
	

    public void insertStringInFile(File inFile, int lineno,
            String lineToBeInserted) throws Exception {
        // temp file
        File outFile = new File("$$$$$$$$.tmp");

        // input
        FileInputStream fis = new FileInputStream(inFile);
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));

        // output
        FileOutputStream fos = new FileOutputStream(outFile);
        PrintWriter out = new PrintWriter(fos);

        String thisLine = "";
        int i = 1;
        while ((thisLine = in.readLine()) != null) {
            if (i == lineno)
                out.println(lineToBeInserted);

            out.println(thisLine);
            i++;
        }
        out.flush();
        out.close();
        in.close();

        inFile.delete();
        outFile.renameTo(inFile);
    }

    public String formatLineAndReturnTruncate(String inputline) {

        String firstpart = inputline.substring(0, 64);
        int last = firstpart.lastIndexOf(' ');

        return null;
    }

    private int linesRequired(String str) {
        int lines;
        int length = str.length();
        if (length % 64 == 0) {
            lines = length / 64;
        } else {
            lines = length / 64 + 1;
        }
        return lines;
    }

    //to check whether a character is   a number

    public boolean isNumber(char c) {
        boolean isNumber = false;
        String str = String.valueOf(c);
        try {
            Integer.parseInt(str);
            isNumber = true;
        } catch (NumberFormatException ne) {
            isNumber = false;
        }
        return isNumber;
    }

    public void testGetFirstNames() throws Exception {
        ReservationPaxDetailsDTO pax1 = new ReservationPaxDetailsDTO();
        pax1.setLastName("OMAR");
        pax1.setFirstName("MONAMOHAMMED");
        pax1.setTitle("MR");
        pax1.setPnr("1614377");
        
        ReservationPaxDetailsDTO pax2 = new ReservationPaxDetailsDTO();
        pax2.setLastName("MISBAH");
        pax2.setFirstName("DONYAABDOULFATAH");
        pax2.setTitle("MR");
        pax2.setPnr("1614377");
        
        ReservationPaxDetailsDTO pax3 = new ReservationPaxDetailsDTO();
        pax3.setLastName("MISBAH");
        pax3.setFirstName("HAGERABDOULFATAH");
        pax3.setTitle("MR");
        pax3.setPnr("1614377");
        
        ReservationPaxDetailsDTO pax4 = new ReservationPaxDetailsDTO();
        pax4.setLastName("MISBAH");
        pax4.setFirstName("MARYAMABDOULFATAH");
        pax4.setTitle("MR");
        pax4.setPnr("1614377");
        
        ReservationPaxDetailsDTO pax5 = new ReservationPaxDetailsDTO();
        pax5.setLastName("MISBAH");
        pax5.setFirstName("MARYAM");
        pax5.setTitle("MS");
        pax5.setPnr("1614377");
        
        ReservationPaxDetailsDTO pax6 = new ReservationPaxDetailsDTO();
        pax6.setLastName("MISBAH");
        pax6.setFirstName("MARYAMAB");
        pax6.setTitle("MR");
        pax6.setPnr("1614377");
        
        ArrayList recordList = new ArrayList();
        recordList.add(pax1);
        recordList.add(pax2);
        recordList.add(pax3);
        recordList.add(pax4);
        recordList.add(pax5);
        recordList.add(pax6);
        
        
        String lastName = "MISBAH";
        
        ArrayList firstNameList = new ArrayList();
        int lastLength = lastName.length();
        int titlelength = 0;
        int nameLength = 0;
        int totalNameLength = 0;
        boolean tourCodeExists = false;
        for (int i = 0; i < recordList.size(); i++) {
            ReservationPaxDetailsDTO pax = (ReservationPaxDetailsDTO) recordList
                    .get(i);
            String last = pax.getLastName();
            if (!last.equalsIgnoreCase(lastName)) {
                tourCodeExists = true;
                break;
            }
        }
        //int sameLastNameCountLength = new Integer(countSameLastName).toString().length();
        int totalPaxPerPnrLength = new Integer(recordList.size()).toString().length();
        int firstNameCount = 0;
        int tourCodeLength = 0;
        if (tourCodeExists) {
            tourCodeLength = 2 + totalPaxPerPnrLength;
        }
        for (int i = 0; i < recordList.size(); i++) {
            ReservationPaxDetailsDTO pax = (ReservationPaxDetailsDTO) recordList
                    .get(i);
            String last = pax.getLastName();
            if (last.equalsIgnoreCase(lastName)) {
                NameDTO ndt = new NameDTO();
        
                String mr = ndt.getTitle();
                if (mr == null || mr.equalsIgnoreCase("")) {
                    ndt.setTitle("Mr");
                }
        
                //titlelength = ndt.getTitle().length();
                nameLength = (pax.getFirstName().length() + 1);
                titlelength = pax.getTitle().length();
                ndt.setFirstname(pax.getFirstName().toUpperCase());
                ndt.setTitle(pax.getTitle());
        
                firstNameCount ++;
                totalNameLength +=  lastLength + nameLength + titlelength + tourCodeLength; 
                if ((new Integer(firstNameCount).toString().length()) + (totalNameLength) <= 64) {
                    firstNameList.add(ndt);
                    tourCodeLength = 0;
                    lastLength = 0;
                    //firstNameCount ++;
                } else {
                    firstNameCount --;
                    totalNameLength = (totalNameLength - nameLength - titlelength);
                }
            }
        }

    }
}