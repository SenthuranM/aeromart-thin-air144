package com.isa.thinair.airreservation.core.bl;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.core.bl.ssr.SSRBL;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsTestcase;


public class TestSSRBL extends CommonsTestcase{
	private ReservationBD reservationBD;

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testSendSSRNotifications() throws ModuleException {
		Reservation reservation = TestReservationUtil.loadReservation("17535443");
		SSRBL.sendSSRAddNotifications(reservation, null, null);
	}
	
	
	

}