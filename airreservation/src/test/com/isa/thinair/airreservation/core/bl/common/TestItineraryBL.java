package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.config.AirReservationConfig;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.util.CommonsTestcase;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestItineraryBL extends CommonsTestcase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testGetItinerary() throws Exception {
		ItineraryLayoutModesDTO itineraryLayoutModesDTO = new ItineraryLayoutModesDTO();
		String pnr = "17531561";
		Locale locale = new Locale("en");
		
		String templateName = "";
		itineraryLayoutModesDTO.setTemplateName(templateName);
		itineraryLayoutModesDTO.setPnr(pnr);
		itineraryLayoutModesDTO.setLocale(locale);

		Collection colSelectedPnrPaxIds = null;
		//colSelectedPnrPaxIds = new ArrayList();
		//colSelectedPnrPaxIds.add(new Integer(""));
		//colSelectedPnrPaxIds.add(new Integer(""));
		
		CredentialsDTO credentialsDTO = new CredentialsDTO();
		credentialsDTO.setSalesChannelCode(new Integer(3));
		Collection colUserDST = new ArrayList();
		UserDST userDST = new UserDST();
		userDST.setGmtOffsetMinutes(300);
		credentialsDTO.setColUserDST(colUserDST);
		
		itineraryLayoutModesDTO.setIncludePassengerPrices(true);
		String itinerary = ItineraryBL.getItineraryForPrint(itineraryLayoutModesDTO,
				colSelectedPnrPaxIds, credentialsDTO,false);
		
		System.out.println(itinerary);
	}
}
