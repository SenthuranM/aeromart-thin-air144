/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;

import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.airreservation.api.dto.FlightReservationSummaryDTO;
import com.isa.thinair.airreservation.api.dto.FlightSegmentReservationDTO;
import com.isa.thinair.airreservation.api.model.ReservationSegment;

/**
 * @author Chamindap
 *
 */
public class TestReservationSegmentDAOImpl extends PlatformTestCase {
	
	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
//	public void testGetPnrSegmentsOverloadedMethod1() {
//	    List flightSegIds = new ArrayList();
//	    System.out.println("Getting PNR Segments for flight seg ids");
//	    flightSegIds.add(new Integer(20656));
//	    flightSegIds.add(new Integer(20666));	    
//		Collection col = getReservationSegmentDAO().getPnrSegments(flightSegIds,null, ReservationInternalConstants.ReservationStatus.CONFIRMED);
//		int count=0;
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			ReservationSegment resSeg = (ReservationSegment) iter.next();
//			System.out.println("----" + count + " Reservation Segment Id :" + resSeg.getPnrSegId());
//			count++;
//		}
//		
//		List pnrSegIds = new ArrayList();
//		System.out.println("Getting PNR Segments for PNR seg ids");
//		pnrSegIds.add(new Integer("9633"));
//		pnrSegIds.add(new Integer("9112"));	    
//		col = getReservationSegmentDAO().getPnrSegments(null,pnrSegIds, ReservationInternalConstants.ReservationStatus.CONFIRMED);
//		count=0;
//		for (Iterator iter = col.iterator(); iter.hasNext();) {
//			ReservationSegment resSeg = (ReservationSegment) iter.next();
//			System.out.println("----" + count + " Reservation Segment Id :" + resSeg.getPnrSegId());
//			count++;
//		}
//	}
	
//	public void testSetReservationAlerts(){		
//		List flightSegIds = new ArrayList();
//	    System.out.println("Getting PNR Segments for flight seg ids");
//	    flightSegIds.add(new Integer(20656));
//	    flightSegIds.add(new Integer(20666));
//	    try {
//	    	getReservationSegmentDAO().setReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_TRUE, flightSegIds,null);			
//		} catch (Exception e) {
//			System.out.println("Error");
//		}	    
//		
//		List pnrSegIds = new ArrayList();
//		System.out.println("Getting PNR Segments for PNR seg ids");
//		pnrSegIds.add(new Integer("9633"));
//		pnrSegIds.add(new Integer("9112"));
//		try {
//	    	getReservationSegmentDAO().setReservationAlerts(ReservationInternalConstants.AlertTypes.ALERT_FALSE, null,pnrSegIds);			
//		} catch (Exception e) {
//			System.out.println("Error");
//		}
//	}


//	public void testGetPnrSegments() {
//        List flightSegIds = new ArrayList();
//        flightSegIds.add(new Integer(20656));
//        flightSegIds.add(new Integer(20658));
//        Collection col = getReservationSegmentDAO().getPnrSegments(flightSegIds);
//	}
//
//    public void testGetFlightReservationsSummary() {
//        Collection flgRes = new ArrayList();
//        HashMap segMap = new HashMap();
//        
//        List flightIds = new ArrayList();
//        flightIds.add(new Integer(100));    
//        flgRes = getReservationSegmentDAO().getFlightReservationsSummary(flightIds);
//        
//        Iterator it = flgRes.iterator();
//        while (it.hasNext()) {
//            FlightReservationSummaryDTO frsDTO = (FlightReservationSummaryDTO) it.next();
//            Collection segRes = frsDTO.getSegmentReservations();
//            Iterator itSegRes = segRes.iterator();
//            while (itSegRes.hasNext()) {
//                FlightSegmentReservationDTO resSegDTO = (FlightSegmentReservationDTO) itSegRes.next();
//            }
//        }
//    }
    
    private ReservationSegmentDAO getReservationSegmentDAO() {
		return null;
        //return ReservationModuleUtils.DAOInstance.RESERVATION_SEGMENT_DAO;        
    }
    
    
    
}