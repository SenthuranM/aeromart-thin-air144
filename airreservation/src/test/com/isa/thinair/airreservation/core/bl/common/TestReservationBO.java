package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

//import com.isa.thinair.airreservation.api.dto.EmailDetailsDTO;
//import com.isa.thinair.airreservation.api.dto.FlightAlertInfoDTO;
//import com.isa.thinair.airreservation.api.dto.FlightSegmentEmailDTO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSegmentDAO;
import com.isa.thinair.messaging.api.util.AlertTemplateEnum;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestReservationBO extends PlatformTestCase {
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
//    public void testGenerateEMails() throws Exception{
//    	List flightSegIds = new ArrayList();
//    	flightSegIds.add(new Integer("9000"));
//    	flightSegIds.add(new Integer("9001"));
//
//    	Collection colAlertTemplateEnum = new HashSet();
//    	colAlertTemplateEnum.add(AlertTemplateEnum.TEMPLATE_CANCELLATION);
//    	colAlertTemplateEnum.add(AlertTemplateEnum.TEMPLATE_CHANGE_DATE_TIME);
//    	
//    	FlightAlertInfoDTO newFlightAlertInfoDTO = new FlightAlertInfoDTO();
//    	newFlightAlertInfoDTO.setDepartureDate(new Date(1,1,1));
//    	newFlightAlertInfoDTO.setDestinationAptCode("New DestAirPort");
//    	newFlightAlertInfoDTO.setFlightId(new Integer(11));
//    	newFlightAlertInfoDTO.setFlightNumber("New Flight No");
//    	newFlightAlertInfoDTO.setLegDetails("New Leg Details are Listed here");
//    	newFlightAlertInfoDTO.setModelNumber("New Model Number");
//    	newFlightAlertInfoDTO.setOriginAptCode("New OrgAirPort");
//    	newFlightAlertInfoDTO.setRoute("New Route");
//    	newFlightAlertInfoDTO.setScheduleId(new Integer(33));
//    	newFlightAlertInfoDTO.setStatus("New Status");
//    	
//    	FlightAlertInfoDTO oldFlightAlertInfoDTO = new FlightAlertInfoDTO();
//    	oldFlightAlertInfoDTO.setDepartureDate(new Date(2,2,2));
//    	oldFlightAlertInfoDTO.setDestinationAptCode("Old DestAirPort");
//    	oldFlightAlertInfoDTO.setFlightId(new Integer(22));
//    	oldFlightAlertInfoDTO.setFlightNumber("Old Flight No");
//    	oldFlightAlertInfoDTO.setLegDetails("Old Leg Details are Listed here");
//    	oldFlightAlertInfoDTO.setModelNumber("Old Model Number");
//    	oldFlightAlertInfoDTO.setOriginAptCode("Old OrgAirPort");
//    	oldFlightAlertInfoDTO.setRoute("Old Route");
//    	oldFlightAlertInfoDTO.setScheduleId(new Integer(44));
//    	oldFlightAlertInfoDTO.setStatus("Old Status");
//    	
//    	flightSegIds = new ArrayList();
//    	flightSegIds.add(new Integer("9100"));
//    	flightSegIds.add(new Integer("9101"));
//    	
//    	FlightSegmentEmailDTO flightSegmentEmailDTO = new FlightSegmentEmailDTO();
//    	flightSegmentEmailDTO.setColAlertTemplateEnum(colAlertTemplateEnum);
//    	flightSegmentEmailDTO.setFlightSegmentIds(flightSegIds);
//    	flightSegmentEmailDTO.setNewFlightAlertInfoDTO(newFlightAlertInfoDTO);
//    	flightSegmentEmailDTO.setOldFlightAlertInfoDTO(oldFlightAlertInfoDTO);
//    	flightSegmentEmailDTO.setTopic("change_flight");
//    	
//    	Collection colFlightSegmentEmailDTO = new HashSet();
//    	colFlightSegmentEmailDTO.add(flightSegmentEmailDTO);
//    	
//    	FlightAlertInfoDTO new2FlightAlertInfoDTO = new FlightAlertInfoDTO();
//    	new2FlightAlertInfoDTO.setDepartureDate(new Date(12,12,12));
//    	new2FlightAlertInfoDTO.setDestinationAptCode("New2 DestAirPort");
//    	new2FlightAlertInfoDTO.setFlightId(new Integer(11));
//    	new2FlightAlertInfoDTO.setFlightNumber("New 2Flight No");
//    	new2FlightAlertInfoDTO.setLegDetails("New2 Leg Details are Listed here");
//    	new2FlightAlertInfoDTO.setModelNumber("New2 Model Number");
//    	new2FlightAlertInfoDTO.setOriginAptCode("New2 OrgAirPort");
//    	new2FlightAlertInfoDTO.setRoute("New2 Route");
//    	new2FlightAlertInfoDTO.setScheduleId(new Integer(233));
//    	new2FlightAlertInfoDTO.setStatus("New2 Status");
//    	
//    	FlightAlertInfoDTO old2FlightAlertInfoDTO = new FlightAlertInfoDTO();
//    	old2FlightAlertInfoDTO.setDepartureDate(new Date(22,2,22));
//    	old2FlightAlertInfoDTO.setDestinationAptCode("Old2 DestAirPort");
//    	old2FlightAlertInfoDTO.setFlightId(new Integer(222));
//    	old2FlightAlertInfoDTO.setFlightNumber("Old2 Flight No");
//    	old2FlightAlertInfoDTO.setLegDetails("Old2 Leg Details are Listed here");
//    	old2FlightAlertInfoDTO.setModelNumber("Old 2Model Number");
//    	old2FlightAlertInfoDTO.setOriginAptCode("Old 2OrgAirPort");
////    	old2FlightAlertInfoDTO.setRoute("Old2 Route");    	
//    	old2FlightAlertInfoDTO.setScheduleId(new Integer(442));
//    	old2FlightAlertInfoDTO.setStatus("Old2 Status");
//    	
//    	colAlertTemplateEnum = new HashSet();
//    	colAlertTemplateEnum.add(AlertTemplateEnum.TEMPLATE_CHANGE_FLIGHT_NUMBER);
//    	colAlertTemplateEnum.add(AlertTemplateEnum.TEMPLATE_CHANGE_ROUTE);
//    	
//    	flightSegmentEmailDTO = new FlightSegmentEmailDTO();
////    	flightSegmentEmailDTO.setColAlertTemplateEnum(colAlertTemplateEnum);
//    	flightSegmentEmailDTO.setFlightSegmentIds(flightSegIds);
//    	flightSegmentEmailDTO.setNewFlightAlertInfoDTO(new2FlightAlertInfoDTO);
//    	flightSegmentEmailDTO.setOldFlightAlertInfoDTO(old2FlightAlertInfoDTO);
//    	flightSegmentEmailDTO.setTopic("change_flight");
//    	
//    	colFlightSegmentEmailDTO.add(flightSegmentEmailDTO);
//    	
//    	Collection emailIds = new HashSet();
//    	emailIds.add("thejaka@jkcsworld.com");
//    	//emailIds.add("chandanak@jkcsworld.com");
//    	
//    	EmailDetailsDTO emailDetailsDTO = new EmailDetailsDTO();
//    	emailDetailsDTO.setEmailContent("Email Content");
//    	emailDetailsDTO.setEmailSubject("Email Subject");
//    	emailDetailsDTO.setEmailTo(emailIds);
//
//    	ReservationBO.generateEMailsForFlightSegments(colFlightSegmentEmailDTO,emailDetailsDTO);
//    }
//    
//    public void testGenerateAlerts() throws Exception{
//    	List pnrSegIds = new ArrayList();
//		pnrSegIds.add(new Integer("9633"));
//		pnrSegIds.add(new Integer("9112"));
//		
//		HashMap hmFlightSegments = new HashMap();    	
//		hmFlightSegments.put(new Integer(20656),"SHJ/DOH");
//		hmFlightSegments.put(new Integer(20666),"CMB/SHJ");
//		
//    	Collection pnrSegs = getReservationSegmentDAO().getPnrSegments(null,pnrSegIds,
//					ReservationInternalConstants.ReservationStatus.CONFIRMED);
//    	HashMap alertDetails = new HashMap();
//    	HashMap parameters = new HashMap();
//    	parameters.put("flightNo","FLIGHTNO_VAL");
//    	parameters.put("departure",new Date(1,1,1));
//    	alertDetails.put(AlertTemplateEnum.TEMPLATE_CANCELLATION,parameters);
//    	
//    	parameters = new HashMap();
//    	parameters.put("oldFlightNo","OLD_FLIGHT_NO");
//    	parameters.put("newFlightNo","NEW_FLIGHT_NO");
//    	parameters.put("departure",new Date(2,2,2));
//    	alertDetails.put(AlertTemplateEnum.TEMPLATE_CHANGE_FLIGHT_NUMBER,parameters);
//    	
//    	ReservationBO.generateAlertsForFlightSegments( pnrSegs,  alertDetails,hmFlightSegments);
//    }

//    public void testRecordPurchase() throws Exception
//    {
//    	
    	
//    	RevenueAccountBD accountBD = new RevenueAccountBDLocalImpl();
////    	ServiceResponce responce = accountBD.recordTransferInfant("1", "2",new Double(100.00),new CredentialsDTO());
////    	
//    	TnxPayment tnxPayment = new TnxPayment();
//    	tnxPayment.setAmount(5000);
//    	tnxPayment.setExpiryDate(new Date());
//    	tnxPayment.setPaymentType(PaymentType.CARD_DINERS.getTypeValue());
//
//    	Collection payments = new ArrayList();
//    	payments.add(tnxPayment);
//    	ServiceResponce serviceResponce= accountBD.recordPurchase("2000", 5000.00, payments, new CredentialsDTO());

    	
    	
    	//ServiceResponce serviceResponce= accountBD.recordCreditAdjust("75", 5000.00, ReservationTnxNominalCode.CREDIT_ADJUST.getCode(), new CredentialsDTO());

    	
	//    	Collection paymentList = new ArrayList();
    	
    	//paymentList.add(tnxPayment);
    	//ITnxPayment payment = (ITnxPayment)itPayments.next();
    	
//    }
    
    private ReservationSegmentDAO getReservationSegmentDAO() {
		return null;
        //return ReservationModuleUtils.DAOInstance.RESERVATION_SEGMENT_DAO;        
        
    }
    
    
 
}
