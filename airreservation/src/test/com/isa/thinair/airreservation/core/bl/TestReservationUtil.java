package com.isa.thinair.airreservation.core.bl;

import com.isa.thinair.airreservation.api.dto.PnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

public class TestReservationUtil {
	static ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
	
	public static Reservation loadReservation(String pnrNo) throws ModuleException {
		PnrModesDTO pnrModesDTO = new PnrModesDTO();
		
		pnrModesDTO.setPnr(pnrNo);
        pnrModesDTO.setLoadFares(true);
        pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
        pnrModesDTO.setLoadSegViewBookingTypes(true);
        pnrModesDTO.setLoadPaxAvaBalance(true);
        pnrModesDTO.setLoadLastUserNote(true);
        pnrModesDTO.setLoadLocalTimes(true);
        pnrModesDTO.setRecordAudit(true);
        pnrModesDTO.setLoadSeatingInfo(true);
        pnrModesDTO.setLoadSegViewReturnGroupId(true);
        pnrModesDTO.setLoadMealInfo(true);
		
		Reservation reservation = reservationBD.getReservation(pnrModesDTO, null);
		
		return reservation;
	}

}
