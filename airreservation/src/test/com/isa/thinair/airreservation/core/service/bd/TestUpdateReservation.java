package com.isa.thinair.airreservation.core.service.bd;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IFaresChargesAndSeats;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.ssr.HalaExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.bl.TestReservationUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalanderUtil;
import com.isa.thinair.commons.core.util.CommonsTestcase;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.xbe.core.util.DateUtil;

public class TestUpdateReservation extends CommonsTestcase {


	private ReservationQueryBD reservationQueryBD;
	private ChargeBD chargeBD;
	private ReservationBD reservationBD;

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		reservationBD = ReservationModuleUtils.getReservationBD();
		reservationQueryBD = ReservationModuleUtils.getReservationQueryBD();
		chargeBD = ReservationModuleUtils.getChargeBD();

	}

	public void testUpdateReservation() throws ModuleException {
		Reservation reservation = TestReservationUtil.loadReservation("17535791");
		Set<ReservationPax> paxs = reservation.getPassengers();
		
		for (Iterator<ReservationPax> itPaxs = paxs.iterator(); itPaxs.hasNext();) {
			ReservationPax pax = (ReservationPax) itPaxs.next();
			
			SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
            String ssrCode = "VIP";
            String ssrText = "Dr. Mervin Silva";
            
			Integer ssrId = SSRUtil.getSSRId(ssrCode);

            
			segmentSSRs.addUpdatedPaxSegmentSSR(null, ssrId, ssrText);

            ReservationSSRUtil.updatePaxSegmentSSRs(pax, segmentSSRs);
            
            break;
		}
		
		ServiceResponce response = updateReservation(reservation);
		
		System.out.println("isSuccess: " + response.isSuccess());
		
	}
	
	
	private ServiceResponce updateReservation(Reservation reservation) throws ModuleException {
		TrackInfoDTO trackInfoDTO = null;

		ServiceResponce response = reservationBD.updateReservation(reservation, trackInfoDTO);
		
		return response;
		
	}

		
	public static Date getSqlDate(String dateStr) {
		Date date = null;

			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	
			try {
				date = df.parse(dateStr);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}

		return date;
	}
}
