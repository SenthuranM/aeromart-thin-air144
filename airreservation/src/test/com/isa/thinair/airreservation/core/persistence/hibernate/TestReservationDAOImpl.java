package com.isa.thinair.airreservation.core.persistence.hibernate;

import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestReservationDAOImpl extends PlatformTestCase {

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/*public void testGetDailyCancellations()
	{
		System.out.println("Before DailyCan");
		getReservationDAO().getDailyCancellations();
		System.out.println("After DailyCan");
		
	}*/
	
	/*public void testStorePfs()
	{
		System.out.println("Before Store Pfs");
		getReservationDAO().storePFS();
		System.out.println("After Store Pfs");
		
		
	}*/
	
	/*public void testReconcileReservations()
	{
		Collection col = null;
		
		 com.isa.thinair.airreservation.api.dto.PaxFinalSalesDTO dto1=new com.isa.thinair.airreservation.api.dto.PaxFinalSalesDTO();
		 dto1.setArrivalAirportCode("SHJ");
		 dto1.setBookingCode("Y");
		 dto1.setCategory("E");
		 dto1.setDate("2005-01009");
		 dto1.setDepartureAirportCode("SHJ");
		 dto1.setFirstName("AHMED");
		 dto1.setFlightNo("234");
		 dto1.setLastName("AH");
		 dto1.setMonth("JAN");
		 dto1.setPnr("123456");
		 dto1.setTitle("MR");

		 col=new ArrayList();
		 col.add(dto1);

		getReservationDAO().reconcileReservations(col);
		
	}*/
	
	/*public void testGetOnwardConnectionList()
	{
		
ArrayList list=		getReservationDAO().getOnwardConnectionList("Tx1000");
	
	for(int i=0;i<list.size();i++)
	{
		
		OnWardConnectionDTO dto=(com.isa.thinair.airreservation.core.bl.pnl.OnWardConnectionDTO)list.get(i);
		//System.out.println(dto.getDeparturetime());
	}
	
	}*/
	
	
	/*
	public void testTransferTempPaymentTxnToIncomplete()
	{
		System.out.println("Before transfer");
		getReservationDAO().transferTempPaymentTxnToIncomplete();
		System.out.println("After transfer");
		
		
	}*/
	
	/*public void testGetcards()
	{
	ArrayList list=	getReservationDAO().getActiveCards();
	System.out.println(list.size());
	for(int i=0;i<list.size();i++)
	{
		System.out.println((String)list.get(i));
	}
	
	}*/
	
	/*public void testPnl() throws Exception
	{
		ReservationAuxiliaryBL bl=new ReservationAuxiliaryBL();
		bl.sendPNL(18,"CMB","",null,null);
	}*/
	private PassengerBD getPassengerBD() {
		PassengerBD passengerBD = null;

		
		
		LookupService lookup = LookupServiceFactory.getInstance();
		// IModule reservation = lookup.getModule("airreservation");
		passengerBD = (PassengerBD) lookup.getServiceBD("airreservation",
				"reservation.service.remote");
		// reservationAuxilliaryBD = (ReservationAuxilliaryBD) reservation
		// .getServiceBD("reservationauxulliary.service.local");

		return passengerBD;
	}

	public void testSetUnusedCredit()
	
	{
		/*ReservationAuxiliaryBL bl=new ReservationAuxiliaryBL();
		bl.acquireExpiredCredit();*/
		
	getPassengerBD();
	}
	/*public void testGetInvoiceHistory()
	{
		
		//geP().getInvoiceHistory("Al Kazim Travel Agency",2005,3,2006,3);
	}*/
	
/*	public void testGetDailyReservations()
	{
		System.out.println("Before DailyRes");
		getReservationDAO().getDailyReservations();
		System.out.println("After DailyRes");
	}*/
	  private ReservationDAO getReservationDAO() {
		return null;
	        //return ReservationModuleUtils.DAOInstance.RESERVATION_DAO;        
	    }
	 
}
