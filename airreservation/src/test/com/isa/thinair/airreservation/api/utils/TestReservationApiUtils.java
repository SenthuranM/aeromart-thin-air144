package com.isa.thinair.airreservation.api.utils;

import com.isa.thinair.airreservation.api.dto.PnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsTestcase;

public class TestReservationApiUtils extends CommonsTestcase {
	private ReservationBD reservationBD;
	
	protected void setUp() throws Exception {
		super.setUp();
		reservationBD = ReservationModuleUtils.getReservationBD();
	}

	public void testMe() throws ModuleException {
		TryGetONDCode();
	}

	private void TryGetONDCode() throws ModuleException {
		String pnrNo = "17537326";
		Reservation reservation = loadReservation(pnrNo);
		String ondCode = ReservationApiUtils.getOndCode(reservation);
		
		System.out.println(ondCode);
	}
	
	private Reservation loadReservation(String pnrNo) throws ModuleException {
		PnrModesDTO pnrModesDTO = new PnrModesDTO();
        pnrModesDTO.setPnr(pnrNo);
        pnrModesDTO.setLoadSegView(true);

        Reservation reservation = reservationBD.getReservation(pnrModesDTO, null);
        
		return reservation;
	}
}
