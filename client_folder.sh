!/bin/bash
SOURCE_CARRIER=W5
TARGET_CARRIER=PK
find ./ -name "$SOURCE_CARRIER" -type d | while read folder; do
  TARGET_FOLDER=${folder/$SOURCE_CARRIER/$TARGET_CARRIER}
  #echo  "Removing $TARGET_FOLDER if exists"
  rm -rf $TARGET_FOLDER
  echo  "Copying $folder to $TARGET_FOLDER recursively"
  cp -rf $folder $TARGET_FOLDER
done
  cp ibe/src/web/js/indexDummy_$SOURCE_CARRIER.jsp ibe/src/web/js/indexDummy_$TARGET_CARRIER.jsp
echo  "Staging for git repository...."
  git add -A
  git add -f airadmin/src/clients/$TARGET_CARRIER/resources/airadmin/airadmin-config.mod.xml
  git add -f aircustomer/src/clients/$TARGET_CARRIER/resources/aircustomer/aircustomer-config.mod.xml
  git add -f airreservation/src/clients/$TARGET_CARRIER/resources/airreservation/airreservation-config.mod.xml
  git add -f airreservation/src/clients/$TARGET_CARRIER/resources/airreservation/externalsystem-config.mod.xml
  git add -f commons/src/clients/$TARGET_CARRIER/resources/commons/globalConfig.mod.xml
  git add -f gdsservices/src/clients/$TARGET_CARRIER/resources/gdsservices/gdsservices-config.mod.xml
  git add -f invoicing/src/clients/$TARGET_CARRIER/resources/invoicing/invoicing-config.mod.xml
  git add -f messagepasser/src/clients/$TARGET_CARRIER/resources/messagepasser/messagepasser-config.mod.xml
  git add -f messaging/src/clients/$TARGET_CARRIER/resources/messaging/messaging-config.mod.xml
  git add -f paymentbroker/src/clients/$TARGET_CARRIER/resources/paymentbroker/paymentbroker-impl.mod.xml
  git add -f scheduler/src/clients/$TARGET_CARRIER/resources/scheduler/scheduler-config.mod.xml
  git add -f scheduler/src/clients/$TARGET_CARRIER/resources/scheduler/scheduler-context.mod.xml
  git add -f webplatform/src/clients/$TARGET_CARRIER/resources/webplatform/webplatform-config.mod.xml
  git add -f webservices/src/clients/$TARGET_CARRIER/resources/webservices/myidtravel/myidtravel-config.mod.xml
  git add -f xbe/src/clients/$TARGET_CARRIER/resources/xbe/xbe-config.mod.xml

echo  "Copying $SOURCE_CARRIER to $TARGET_CARRIER done!!!"
