package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

/**
 * @author eric
 * 
 */
public class BasicFareSummaryDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = -2664388447487446791L;

	private double adultFare;
	private double childFare;
	private double infantFare;

	public double getAdultFare() {
		return adultFare;
	}

	public void setAdultFare(double adultFare) {
		this.adultFare = adultFare;
	}

	public double getChildFare() {
		return childFare;
	}

	public void setChildFare(double childFare) {
		this.childFare = childFare;
	}

	public double getInfantFare() {
		return infantFare;
	}

	public void setInfantFare(double infantFare) {
		this.infantFare = infantFare;
	}

	public double getTotalFare(int adults, int children, int infants) {
		return (adults * adultFare) + (children * childFare) + (infants * infantFare);
	}

}
