/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airinventory.api.model;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author mano
 * @hibernate.class table = "BG_T_FLT_SEG_BAGGAGE_CHARGE"
 * 
 */
public class FlightBaggage extends Tracking {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1389260161045678379L;

	private Integer flightBaggageChargeId;

	private Integer flightSegmentId;

	private Integer baggageChargeId;

	private String cabinClassCode;
	
	private String logicalCCCode;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private Integer soldPieces;

	/**
	 * @return the flightBaggageChargeId
	 * @hibernate.id column = "FLT_SEG_BAGGAGE_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_FLT_SEG_BAGGAGE_CHARGE"
	 * 
	 */
	public Integer getFlightBaggageChargeId() {
		return flightBaggageChargeId;
	}

	/**
	 * @param flightBaggageChargeId
	 *            the flightBaggageChargeId to set
	 */
	public void setFlightBaggageChargeId(Integer flightBaggageChargeId) {
		this.flightBaggageChargeId = flightBaggageChargeId;
	}

	/**
	 * @return the flightSegmentId
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	/**
	 * @param flightSegmentId
	 *            the flightSegmentId to set
	 */
	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @return the baggageChargeId
	 * @hibernate.property column = "BAGGAGE_CHARGE_ID"
	 */
	public Integer getBaggageChargeId() {
		return baggageChargeId;
	}

	/**
	 * @param baggageChargeId
	 *            the baggageChargeId to set
	 */
	public void setBaggageChargeId(Integer baggageChargeId) {
		this.baggageChargeId = baggageChargeId;
	}

	/**
	 * @return the cabinClassCode
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the soldPieces
	 * @hibernate.property column = "SOLD_PIECES"
	 */
	public Integer getSoldPieces() {
		return soldPieces;
	}

	/**
	 * @param soldPieces
	 *            the soldPieces to set
	 */
	public void setSoldPieces(Integer soldPieces) {
		this.soldPieces = soldPieces;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

}
