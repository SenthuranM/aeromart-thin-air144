/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airinventory.api.util.FarePercentageCalculator;
import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.CommonsBusinessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * To hold fare data transfer information
 * 
 * @author Nilindra Fernando/Byorn
 * @since 1.0
 */

public class OndFareDTO implements Serializable, Comparable<OndFareDTO> {

	private static final long serialVersionUID = -8062225917946100220L;

	/** Hold adult, child and infant fare information */
	private FareSummaryDTO fareSummaryDTO;

	/** Holds the ond code */
	private String ondCode = null;

	/** Holds the adult and infant charges collection of QuotedChargeDTO */
	private Collection<QuotedChargeDTO> allCharges;

	/** Holds a collection of SegmentSeatDistsDTO */
	private Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs;

	/**
	 * Segments the ond is made up of key=segmentId, value=FlightSegmentDTO
	 */
	private LinkedHashMap<Integer, FlightSegmentDTO> segmentsMap;

	private boolean isInBoundOnd = false;

	/**
	 * Flag to indicate if the modified ond is belong to inbound. we can't relay on isInBoundOnd for modifications of
	 * inbound
	 */
	private boolean isModifyInbound = false;

	private boolean isOpenReturn;

	/** Holds the adult, Child and infant flexi charges collection of FlexiRuleDTO */
	private Collection<FlexiRuleDTO> allFlexiCharges;

	/**
	 * Type of fare - whether fare type is segment, ond or return. This will represent the fate type of the ondFareDTO.
	 * Not the total journey fareType
	 */
	private int fareType;

	/**
	 * Represent the total journey fare type. This is differ form fareType as it's only represent the OND wise fare type
	 * . Used in return grouping calculation in aaServises as the selectedFlightDto is not available in the
	 * create/modify reservation context
	 */
	private int totalJourneyFareType;

	private int ondSequence;

	private boolean calculateAgentCommission = false;

	private int subJourneyGroup = -1;

	private Integer selectedBundledFarePeriodId;

	private int requestedOndSequence = -1;
	
	private boolean splitOperationForBusInvoked = false;

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	/**
	 * @return Returns the allCharges.
	 */
	public Collection<QuotedChargeDTO> getAllCharges() {
		return allCharges;
	}

	/**
	 * @param allCharges
	 *            The allCharges to set.
	 */
	public void setAllCharges(Collection<QuotedChargeDTO> allCharges) {
		this.allCharges = allCharges;
	}

	/**
	 * Get total charges
	 * 
	 * @return element 0 - total adult charge element 1 - total infant charge element 2 - total child charge for group
	 */
	public double[] getTotalCharges() {
		List<Double> list = ChargeQuoteUtils.calculateTotalCharges(getAllCharges());
		return new double[] { list.get(0), list.get(1), list.get(2) };
	}

	/**
	 * Get total charges for a given charge group
	 * 
	 * @param chargeGroupCode
	 * @return element 0 - total adult charge for group element 1 - total infant charge for group element 2 - total
	 *         child charge for group
	 */
	public double[] getTotalCharges(String chargeGroupCode) {
		return ChargeQuoteUtils.calculateTotalChargeForChargeGroup(getAllCharges(), chargeGroupCode);
	}

	public void setFareSummaryDTO(FareSummaryDTO fareSummaryDTO) {
		this.fareSummaryDTO = fareSummaryDTO;
	}

	public FareSummaryDTO getFareSummaryDTO() {
		return this.fareSummaryDTO;
	}

	/**
	 * @return Returns the fare.
	 */
	public double getAdultFare() {
		return getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT);
	}

	/**
	 * @return Returns the fareId.
	 */
	public int getFareId() {
		return getFareSummaryDTO().getFareId();
	}

	public String getFareRuleDesc() {
		return getFareSummaryDTO().getFareRuleComment();
	}

	/**
	 * @return Returns the fareCategoryType.
	 */
	public String getFareCategoryType() {
		return getFareSummaryDTO().getFareCategoryCode();
	}

	/**
	 * @return Returns the SegmentSeatDistsDTO.
	 */
	public Collection<SegmentSeatDistsDTO> getSegmentSeatDistsDTO() {
		return segmentSeatDistsDTOs;
	}

	public void setSegmentSeatDistsDTOs(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs) {
		this.segmentSeatDistsDTOs = segmentSeatDistsDTOs;
	}

	public void addSegmentSeatDistsDTO(SegmentSeatDistsDTO segmentSeatDistsDTO) {
		if (this.segmentSeatDistsDTOs == null) {
			this.segmentSeatDistsDTOs = new ArrayList<SegmentSeatDistsDTO>();
		}
		this.segmentSeatDistsDTOs.add(segmentSeatDistsDTO);
	}

	public LinkedHashMap<Integer, FlightSegmentDTO> getSegmentsMap() {
		return segmentsMap;
	}

	public FlightSegmentDTO getFlightSegmentDTO(Integer flightSegId) {
		if (this.segmentsMap != null) {
			return this.segmentsMap.get(flightSegId);
		}
		return null;
	}

	public Collection<Integer> getFlightSegmentIds() {
		Collection<Integer> fltSegIds = null;
		if (this.segmentsMap != null) {
			fltSegIds = new HashSet<Integer>();
			for (FlightSegmentDTO segment : this.getSegmentsMap().values()) {
				fltSegIds.add(segment.getSegmentId());
			}
		}
		return fltSegIds;
	}

	public void addSegment(Integer segmentId, FlightSegmentDTO flightSegmentDTO) {
		if (this.segmentsMap == null) {
			segmentsMap = new LinkedHashMap<Integer, FlightSegmentDTO>();
		}
		this.segmentsMap.put(segmentId, flightSegmentDTO);
	}

	void addSegment(LinkedHashMap<Integer, FlightSegmentDTO> segmentsMap) {
		if (this.segmentsMap == null) {
			segmentsMap = new LinkedHashMap<Integer, FlightSegmentDTO>();
		}
		Iterator<Integer> keysIt = segmentsMap.keySet().iterator();
		while (keysIt.hasNext()) {
			Integer segmentId = keysIt.next();
			this.segmentsMap.put(segmentId, segmentsMap.get(segmentId));
		}
	}

	@Deprecated
	public boolean isInBoundOnd() {
		return isInBoundOnd;
	}

	@Deprecated
	public void setInBoundOnd(boolean isInBoundOnd) {
		this.isInBoundOnd = isInBoundOnd;
	}

	/**
	 * @return the isModifyInbound
	 */
	@Deprecated
	public boolean isModifyInbound() {
		return isModifyInbound;
	}

	/**
	 * @param isModifyInbound
	 *            the isModifyInbound to set
	 */
	@Deprecated
	public void setModifyInbound(boolean isModifyInbound) {
		this.isModifyInbound = isModifyInbound;
	}

	/**
	 * Prepares Criteria for charge quoting
	 * 
	 * @param posStation
	 *            Point of Sale station for the reservation
	 * @return QuoteChargesCriteria
	 * @throws ModuleException
	 */
	public QuoteChargesCriteria createCriteriaForChargeQuoting(String posStation, boolean setOndFare) throws ModuleException {
		QuoteChargesCriteria criteria = new QuoteChargesCriteria();
		if (setOndFare) {
			criteria.setFareSummaryDTO(this.getFareSummaryDTO());
		}
		criteria.setChargeCatCodes(null);// include all charge categories
		criteria.setOnd(getOndCode());// charges are quoted for fare's ond, if ondcode is null
		criteria.setPosAirport(posStation);
		criteria.setCabinClassCode(getCCForChargeQuote());
		Collection<FlightSegmentDTO> fltSegDTOs = getSegmentsMap().values();
		if ((fltSegDTOs != null) && (fltSegDTOs.size() > 0)) {
			criteria.setDepatureDate(fltSegDTOs.iterator().next().getDepartureDateTime());
		} else {
			throw new CommonsBusinessException("airpricing.arg.chargequote.effectivedate.invalid", "airpricing.desc");
		}
		return criteria;
	}

	// Haider 09Feb09
	public String getOndCode() throws ModuleException {
		return getOndCode(false);
	}

	/**
	 * It is assumed that the segments are ordered by the departure date
	 * 
	 * @return Returns the ondCode.
	 * @throws ModuleException
	 */
	// Haider 09Feb09 add bHideStopOverSeg param to the fn
	// if bHideStopOverSeg is true then the return string will not contains the stop over segment
	public String getOndCode(boolean bHideStopOverSeg) throws ModuleException {
		// if bHideStopOverSeg is true we need to reurn the ond code without stop over seg
		if (bHideStopOverSeg
				|| (ondCode == null || ondCode.equals("")) && getSegmentsMap() != null && getSegmentsMap().size() > 0) {
			String derivedOndCode = "";
			if (getSegmentsMap() != null && getSegmentsMap().size() > 0) {
				Iterator<FlightSegmentDTO> flightSegmentDTOsIt = getSegmentsMap().values().iterator();
				Date previousSegDepartureDate = null;
				while (flightSegmentDTOsIt.hasNext()) {
					FlightSegmentDTO flightSegmentDTO = flightSegmentDTOsIt.next();
					String segmentCode = flightSegmentDTO.getSegmentCode();
					if (previousSegDepartureDate != null
							&& flightSegmentDTO.getDepartureDateTime().before(previousSegDepartureDate)) {
						// invalid ordering of flight segments
						throw new ModuleException("airinventory.arg.changefare.invalid",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
					previousSegDepartureDate = flightSegmentDTO.getDepartureDateTime();

					String[] airports = StringUtils.split(segmentCode, "/");
					for (int i = 0; i < airports.length; i++) {
						// Haider 09Feb09 remove the stop over segment
						if (bHideStopOverSeg && airports.length > 2 && i > 0 && i < (airports.length - 1)) {
							continue;
						}
						if (derivedOndCode.indexOf(airports[i]) == -1) {
							if (!derivedOndCode.equals("")) {
								derivedOndCode += "/";
							}
							derivedOndCode += airports[i];
						}
					}
				}
				// if bHideStopOverSeg true no need to set the class var
				if (bHideStopOverSeg) {
					return derivedOndCode;
				}
				this.ondCode = derivedOndCode;
			}
		}
		return this.ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	/**
	 * @return the journeyFareType
	 */
	public int getTotalJourneyFareType() {
		return totalJourneyFareType;
	}

	/**
	 * @param journeyFareType
	 *            the journeyFareType to set
	 */
	public void setTotalJourneyFareType(int journeyFareType) {
		this.totalJourneyFareType = journeyFareType;
	}

	public boolean isFlown() {
		if (isFlownOnd()) {
			return true;
		}

		// when re-quote for old segment which are not flown, after modifying the reservation leads to inventory
		// miss-match
		if (!AppSysParamsUtil.isRequoteEnabled()) {
			if (getSegmentsMap() != null && getSegmentsMap().size() > 0) {
				FlightSegmentDTO firstFlightSegmentDTO = getSegmentsMap().values().iterator().next();
				if (firstFlightSegmentDTO.getDepartureDateTimeZulu() != null) {
					if (firstFlightSegmentDTO.getDepartureDateTimeZulu().before(CalendarUtil.getCurrentSystemTimeInZulu())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public boolean isFlownOnd() {
		for (SegmentSeatDistsDTO x : getSegmentSeatDistsDTO()) {
			if (x.isFlownOnd()) {
				return true;
			}
		}
		return false;
	}

	public String getCCForChargeQuote() {
		String cabin = null;
		if (getSegmentSeatDistsDTO() != null) {
			for (SegmentSeatDistsDTO segmentSeatDistsDTO : getSegmentSeatDistsDTO()) {
				cabin = segmentSeatDistsDTO.getCabinClassCode();
			}
		}
		return cabin;
	}

	public String getBCForChargeQuote() {
		String bookingClass = null;
		if (getSegmentSeatDistsDTO() != null) {
			for (SegmentSeatDistsDTO segmentSeatDistsDTO : getSegmentSeatDistsDTO()) {
				if (segmentSeatDistsDTO.getSeatDistribution() != null) {
					for (SeatDistribution seatDistribution : segmentSeatDistsDTO.getSeatDistribution()) {
						bookingClass = seatDistribution.getBookingClassCode();
					}
				} else {
					bookingClass = segmentSeatDistsDTO.getBookingCode();
				}
			}
		}
		return bookingClass;
	}

	public Date getOndDepartureDate() {
		Date ondDepDate = null;
		if (getSegmentsMap() != null) {
			Iterator<FlightSegmentDTO> segmentsIt = getSegmentsMap().values().iterator();
			while (segmentsIt.hasNext()) {
				FlightSegmentDTO flightSegmentDTO = segmentsIt.next();
				if (ondDepDate == null || ondDepDate.after(flightSegmentDTO.getDepartureDateTimeZulu())) {
					ondDepDate = flightSegmentDTO.getDepartureDateTimeZulu();
				}
			}
		}
		return ondDepDate;
	}

	/**
	 * @return Returns the childFare.
	 */
	public Double getChildFare() {
		return this.getFareSummaryDTO().getFareAmount(PaxTypeTO.CHILD);
	}

	/**
	 * @return Returns the infantFare.
	 */
	public Double getInfantFare() {
		return this.getFareSummaryDTO().getFareAmount(PaxTypeTO.INFANT);
	}

	/**
	 * @return Returns the allFlexiCharges.
	 */
	public Collection<FlexiRuleDTO> getAllFlexiCharges() {
		return allFlexiCharges;
	}

	/**
	 * @param allFlexiCharges
	 *            The allFlexiCharges to set.
	 */
	public void setAllFlexiCharges(Collection<FlexiRuleDTO> allFlexiCharges) {
		this.allFlexiCharges = allFlexiCharges;
	}

	/**
	 * Prepares Criteria for flexi quoting
	 * 
	 * @return QuoteChargesCriteria
	 * @throws ModuleException
	 */
	public FlexiQuoteCriteria createCriteriaForFlexiQuoting() throws ModuleException {
		FlexiQuoteCriteria criteria = new FlexiQuoteCriteria();
		criteria.setFareSummaryDTO(this.getFareSummaryDTO());
		criteria.setFareRuleID(this.getFareSummaryDTO().getFareRuleID());
		criteria.setOndCode(getOndCode());// charges are quoted for fare's ond, if ondcode is null
		Collection<FlightSegmentDTO> fltSegDTOs = getSegmentsMap().values();
		if ((fltSegDTOs != null) && (fltSegDTOs.size() > 0)) {
			FlightSegmentDTO flightSegmentDTO = fltSegDTOs.iterator().next();
			criteria.setDepartureDate(flightSegmentDTO.getDepartureDateTime());
			criteria.setDepartureZuluDate(flightSegmentDTO.getDepartureDateTimeZulu());
		} else {
			throw new CommonsBusinessException("airpricing.arg.chargequote.effectivedate.invalid", "airpricing.desc");
		}
		return criteria;
	}

	public Date getFirstDepartureDateZulu() {
		return getSegmentsMap().values().iterator().next().getDepartureDateTimeZulu();
	}

	/**
	 * inject the fare percentage carried by each FareSummaryDTO in the OndFareDTO TODO abstract out
	 * 
	 * @param fareSummaryDTO
	 */
	public void injectFarePercentage(int journeyFareType) {
		int farePercentage = FarePercentageCalculator.getFarePercentage(journeyFareType, this.getFareType());
		fareSummaryDTO.setFarePercentage(farePercentage);
	}

	public StringBuffer getSummary() throws ModuleException {
		StringBuffer summary = new StringBuffer();
		summary.append("\n\r.......................OND Fare DTO summary Begin.......................\n\r");
		summary.append("Ond Code	    	= " + getOndCode() + "\n\r");
		summary.append("Is Inbound OND  	= " + isInBoundOnd() + "\n\r");
		summary.append("Fare Id				= " + getFareId() + "\n\r");
		summary.append("Fare Amount     	= " + getAdultFare() + "\n\r");
		summary.append("Chlid Fare Amount	= " + getChildFare() + "\n\r");
		summary.append("Infant Fare Amount	= " + getInfantFare() + "\n\r");
		summary.append("Fare rule comments 	= " + getFareRuleDesc() + "\n\r");
		summary.append("Fare Type     		= " + getFareType() + "\n\r");
		summary.append("Fare Category   	= " + getFareCategoryType() + "\n\r");
		summary.append("Fare Percentage 	= " + getFareSummaryDTO().getFarePercentage() + "\n\r");
		for (SegmentSeatDistsDTO segSeatDists : getSegmentSeatDistsDTO()) {
			summary.append(segSeatDists.getSummary());
		}
		summary.append("..................................................................\n\r");
		return summary;
	}

	/**
	 * For testing only.
	 */
	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("\t").append("---------------------------OndFareDTO Summary ------------------------------").append("\n");
		sb.append("\t").append("fareType = " + fareType).append("\n");
		sb.append("\t").append("\tisInBoundOnd = " + isInBoundOnd).append("\n");
		sb.append("\t").append("\tisModifyInbound = " + isModifyInbound).append("\n");
		sb.append("\t").append("\tisOpenReturn = " + isOpenReturn).append("\n");

		if (fareSummaryDTO != null) {
			sb.append("\t").append(fareSummaryDTO).append("\n");
		}

		sb.append("\t").append("---------------------------Flight segment ----------------------------------").append("\n");
		if (segmentsMap != null && !segmentsMap.isEmpty()) {
			for (Entry<Integer, FlightSegmentDTO> entry : segmentsMap.entrySet()) {
				sb.append("\t").append(entry.getKey()).append(":").append(entry.getValue()).append("\n");
			}
		}
		sb.append("\t").append("-----------------------------------------------------------------------------").append("\n");

		sb.append("\t").append("---------------------------segmentSeatDistsDTOs -----------------------------").append("\n");
		if (segmentSeatDistsDTOs != null && !segmentSeatDistsDTOs.isEmpty()) {
			for (SegmentSeatDistsDTO seatDistsDTO : segmentSeatDistsDTOs) {
				sb.append("\t").append(seatDistsDTO).append("\n");
			}
		}

		sb.append("\t").append("-----------------------------------------------------------------------------").append("\n");
		sb.append("\t").append("---------------------------------allCharges----------------------------------").append("\n");

		if (allCharges != null && !allCharges.isEmpty()) {
			for (Object o : allCharges) {
				sb.append("\t").append(o).append("\n");
			}
		}

		sb.append("\t").append("-----------------------------------------------------------------------------").append("\n");
		sb.append("\t").append("--------------------------------allFlexiCharges------------------------------").append("\n");

		if (allFlexiCharges != null && !allFlexiCharges.isEmpty()) {
			for (Object o : allFlexiCharges) {
				sb.append("\t").append(o).append("\n");
			}
		}

		sb.append("\t").append("------------------------------OndFareDTO Summary End ------------------------").append("\n");
		return sb.toString();
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void injectFareAdjustment(Map<String, BigDecimal> fareDiffByPaxTypeMap, boolean hasAdult, boolean hasChild,
			boolean hasInfant) throws ModuleException {
		if (fareSummaryDTO != null) {

			BigDecimal adultFareDiff = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal childFareDiff = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal infantFareDiff = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (hasAdult && fareDiffByPaxTypeMap.get(PaxTypeTO.ADULT) != null) {
				adultFareDiff = fareDiffByPaxTypeMap.get(PaxTypeTO.ADULT);
			}

			if (hasChild && fareDiffByPaxTypeMap.get(PaxTypeTO.CHILD) != null) {
				childFareDiff = fareDiffByPaxTypeMap.get(PaxTypeTO.CHILD);
			}

			if (hasInfant && fareDiffByPaxTypeMap.get(PaxTypeTO.INFANT) != null) {
				infantFareDiff = fareDiffByPaxTypeMap.get(PaxTypeTO.INFANT);
			}

			fareSummaryDTO.setEffectiveROEAdjustmentAmount(PaxTypeTO.ADULT, adultFareDiff.doubleValue());
			fareSummaryDTO.setEffectiveROEAdjustmentAmount(PaxTypeTO.CHILD, childFareDiff.doubleValue());
			fareSummaryDTO.setEffectiveROEAdjustmentAmount(PaxTypeTO.INFANT, infantFareDiff.doubleValue());

		}

	}

	public BigDecimal getPaxOndAgentCommission(String paxType) {

		BigDecimal paxOndAgentCommission = AccelAeroCalculator.getDefaultBigDecimalZero();

		String agentCommissionType = this.getFareSummaryDTO().getAgentCommissionType();
		BigDecimal agentCommissionAmount = this.getFareSummaryDTO().getAgentCommissionAmount();

		if (agentCommissionType != null && agentCommissionAmount != null
				&& agentCommissionAmount.compareTo(BigDecimal.ZERO) > 0) {
			double paxBaseFare = 0.00;
			if (paxType.equals(PaxTypeTO.ADULT)) {
				paxBaseFare = getAdultFare();
			} else if (paxType.equals(PaxTypeTO.CHILD)) {
				paxBaseFare = getChildFare();
			} else if (paxType.equals(PaxTypeTO.INFANT)) {
				paxBaseFare = getInfantFare();
			}

			if (paxBaseFare > 0.0) {
				paxOndAgentCommission = calculateCommission(BigDecimal.valueOf(paxBaseFare), agentCommissionType,
						agentCommissionAmount);
			}
		}

		return paxOndAgentCommission;

	}

	private BigDecimal calculateCommission(BigDecimal baseFare, String type, BigDecimal nominator) {
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (type.equals(FareRule.CHARGE_TYPE_V)) {
			amount = nominator;
		} else if (type.equals(FareRule.CHARGE_TYPE_PF)) {
			amount = calculatePercentage(baseFare, nominator);
		}
		return amount;

	}

	private static BigDecimal calculatePercentage(BigDecimal input, BigDecimal percentage) {
		return AccelAeroCalculator.divide(AccelAeroCalculator.multiply(input, percentage), 100);
	}

	public boolean isCalculateAgentCommission() {
		return calculateAgentCommission;
	}

	public void setCalculateAgentCommission(boolean calculateAgentCommission) {
		this.calculateAgentCommission = calculateAgentCommission;
	}

	public boolean isUnTouchedOnd() {
		for (SegmentSeatDistsDTO x : getSegmentSeatDistsDTO()) {
			if (x.isUnTouchedOnd()) {
				return true;
			}
		}
		return false;
	}

	public int getSubJourneyGroup() {
		return subJourneyGroup;
	}

	public void setSubJourneyGroup(int subJourneyGroup) {
		this.subJourneyGroup = subJourneyGroup;
	}

	public double[] getTotalFareAdjustment() {
		double adultCharges = 0;
		double infantCharges = 0;
		double childCharges = 0;

		if (fareSummaryDTO != null && fareSummaryDTO.getEffectiveROEAdjustmentAmountMap() != null
				&& !fareSummaryDTO.getEffectiveROEAdjustmentAmountMap().isEmpty()) {
			adultCharges += fareSummaryDTO.getEffectiveROEAdjustmentAmount(PaxTypeTO.ADULT).doubleValue();
			childCharges += fareSummaryDTO.getEffectiveROEAdjustmentAmount(PaxTypeTO.CHILD).doubleValue();
			infantCharges += fareSummaryDTO.getEffectiveROEAdjustmentAmount(PaxTypeTO.INFANT).doubleValue();
		}

		return new double[] { adultCharges, infantCharges, childCharges };
	}

	@Override
	public int compareTo(OndFareDTO o) {
		return (this.ondSequence - o.getOndSequence());
	}
	
	public Integer getSelectedBundledFarePeriodId() {
		return selectedBundledFarePeriodId;
	}

	public void setSelectedBundledFarePeriodId(Integer selectedBundledFarePeriodId) {
		this.selectedBundledFarePeriodId = selectedBundledFarePeriodId;
	}

	public void injectBalanceDueAdjustment(Map<Integer, BigDecimal> paxWiseDueAdjAmountMap) {
		try {
			if (allCharges == null)
				allCharges = new ArrayList<QuotedChargeDTO>();

			if (allCharges != null && fareSummaryDTO != null) {

				String chargeCode = ChargeCodes.CNX_SEGMENT_SYSTEM_ADJ;

				QuotedChargeDTO quotedChargeDTO = null;

				quotedChargeDTO = AirInventoryModuleUtils.getChargeBD().getCharge(chargeCode, null, fareSummaryDTO, null, false);

				BigDecimal defaultAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, defaultAmount.doubleValue());
				quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, defaultAmount.doubleValue());
				quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.INFANT, defaultAmount.doubleValue());

				if (paxWiseDueAdjAmountMap != null && paxWiseDueAdjAmountMap.size() > 0) {
					for (Integer pnrPaxId : paxWiseDueAdjAmountMap.keySet()) {
						if (paxWiseDueAdjAmountMap.get(pnrPaxId) != null
								&& paxWiseDueAdjAmountMap.get(pnrPaxId).doubleValue() < 0) {
							quotedChargeDTO.setPaxWiseBalanceDueAdj(pnrPaxId, paxWiseDueAdjAmountMap.get(pnrPaxId).doubleValue());
						}

					}
				}

				allCharges.add(quotedChargeDTO);

			}
		} catch (ModuleException e) {
			e.printStackTrace();
		}
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public boolean isSplitOperationForBusInvoked() {
		return splitOperationForBusInvoked;
	}

	public void setSplitOperationForBusInvoked(boolean isSplitOperationForBusInvoked) {
		this.splitOperationForBusInvoked = isSplitOperationForBusInvoked;
	}
	

}
