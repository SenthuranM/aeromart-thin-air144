package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.meal.MealTo;

public class PassengerTo implements Serializable {
	private List<MealTo> meals;
	private String pnr;
	private String title;
	private String firstName;
	private String lastName;
	private String seatCode;

	public PassengerTo() {
		meals = new ArrayList<MealTo>();
	}

	public List<MealTo> getMeals() {
		return meals;
	}

	public void addMeal(MealTo meal) {
		this.meals.add(meal);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}
}
