/**
 * 
 */
package com.isa.thinair.airinventory.api.dto.meal;

import java.io.Serializable;
import java.math.BigDecimal;

public class FlightMealDTO implements Serializable {

	private static final long serialVersionUID = 2607670345222007543L;

	private int flightSegmentID;

	private int templateId;

	private Integer chargeId;

	private Integer flightMealId;

	private Integer mealId;

	private BigDecimal amount;

	private String mealName;

	private int allocatedMeal;

	private int soldMeals;

	private int availableMeals;

	private String mealDescription;

	private String mealCode;

	private String translatedMealTitle;

	private String translatedMealDescription;

	private String mealIATACode;

	private int mealCategoryID;

	private String mealCategoryCode;

	private String translatedMealCategory;
	
	private String cabinClassCode;

	private String logicalCCCode;
	
	private String mealStatus;
	
	private boolean isCategoryRestricted;

	private String popularity;

	public String getTranslatedMealCategory() {
		return translatedMealCategory;
	}

	public void setTranslatedMealCategory(String translatedMealCategory) {
		this.translatedMealCategory = translatedMealCategory;
	}

	// Lalanthi
	private String flightSegmentCode;

	public String getFlightSegmentCode() {
		return flightSegmentCode;
	}

	public void setFlightSegmentCode(String flightSegmentCode) {
		this.flightSegmentCode = flightSegmentCode;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public Integer getFlightMealId() {
		return flightMealId;
	}

	public void setFlightMealId(Integer flightMealId) {
		this.flightMealId = flightMealId;
	}

	public String getMealDescription() {
		return mealDescription;
	}

	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}

	public void setAllocatedMeal(int allocatedMeal) {
		this.allocatedMeal = allocatedMeal;
	}

	public int getAllocatedMeal() {
		return allocatedMeal;
	}

	public int getFlightSegmentID() {
		return flightSegmentID;
	}

	public void setFlightSegmentID(int flightSegmentID) {
		this.flightSegmentID = flightSegmentID;
	}

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public int getSoldMeals() {
		return soldMeals;
	}

	public void setSoldMeals(int soldMeals) {
		this.soldMeals = soldMeals;
	}

	public int getAvailableMeals() {
		return availableMeals;
	}

	public void setAvailableMeals(int availableMeals) {
		this.availableMeals = availableMeals;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getTranslatedMealTitle() {
		return translatedMealTitle;
	}

	public void setTranslatedMealTitle(String translatedMealTitle) {
		this.translatedMealTitle = translatedMealTitle;
	}

	public String getTranslatedMealDescription() {
		return translatedMealDescription;
	}

	public void setTranslatedMealDescription(String translatedMealDescription) {
		this.translatedMealDescription = translatedMealDescription;
	}

	public String getMealIATACode() {
		return mealIATACode;
	}

	public void setMealIATACode(String mealIATACode) {
		this.mealIATACode = mealIATACode;
	}

	public String toString() {
		StringBuilder toStringBuilder = new StringBuilder("[FlightMealDTO - ");
		toStringBuilder.append("flightSegmentID : ").append(flightSegmentID);
		toStringBuilder.append(", templateId : ").append(templateId);
		toStringBuilder.append(", chargeId : ").append(chargeId);
		toStringBuilder.append(", flightMealId : ").append(flightMealId);
		toStringBuilder.append(", mealId : ").append(mealId);
		toStringBuilder.append(", amount : ").append(amount);
		toStringBuilder.append(", mealName : ").append(mealName);
		toStringBuilder.append(", allocatedMeal : ").append(allocatedMeal);
		toStringBuilder.append(", soldMeals : ").append(soldMeals);
		toStringBuilder.append(", availableMeals : ").append(availableMeals);
		toStringBuilder.append(", mealDescription : ").append(mealDescription);
		toStringBuilder.append(", mealCode : ").append(mealCode);
		toStringBuilder.append(", mealCategoryID : ").append(mealCategoryID);
		toStringBuilder.append(", translatedMealTitle : ").append(translatedMealTitle);
		toStringBuilder.append(", translatedMealDescription : ").append(translatedMealDescription);
		toStringBuilder.append(", mealStatus : ").append(mealStatus);
		toStringBuilder.append(", popularity : ").append(popularity);
		toStringBuilder.append("]");
		return toStringBuilder.toString();
	}

	public int getMealCategoryID() {
		return mealCategoryID;
	}

	public void setMealCategoryID(int mealCategoryID) {
		this.mealCategoryID = mealCategoryID;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getMealStatus() {
		return mealStatus;
	}

	public void setMealStatus(String mealStatus) {
		this.mealStatus = mealStatus;
	}

	/**
	 * @return the isCategoryRestricted
	 */
	public boolean isCategoryRestricted() {
		return isCategoryRestricted;
	}

	/**
	 * @param isCategoryRestricted the isCategoryRestricted to set
	 */
	public void setCategoryRestricted(boolean isCategoryRestricted) {
		this.isCategoryRestricted = isCategoryRestricted;
	}


	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + allocatedMeal;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + availableMeals;
		result = prime * result + ((cabinClassCode == null) ? 0 : cabinClassCode.hashCode());
		result = prime * result + ((chargeId == null) ? 0 : chargeId.hashCode());
		result = prime * result + ((flightMealId == null) ? 0 : flightMealId.hashCode());
		result = prime * result + ((flightSegmentCode == null) ? 0 : flightSegmentCode.hashCode());
		result = prime * result + flightSegmentID;
		result = prime * result + (isCategoryRestricted ? 1231 : 1237);
		result = prime * result + ((logicalCCCode == null) ? 0 : logicalCCCode.hashCode());
		result = prime * result + ((mealCategoryCode == null) ? 0 : mealCategoryCode.hashCode());
		result = prime * result + mealCategoryID;
		result = prime * result + ((mealCode == null) ? 0 : mealCode.hashCode());
		result = prime * result + ((mealDescription == null) ? 0 : mealDescription.hashCode());
		result = prime * result + ((mealIATACode == null) ? 0 : mealIATACode.hashCode());
		result = prime * result + ((mealId == null) ? 0 : mealId.hashCode());
		result = prime * result + ((mealName == null) ? 0 : mealName.hashCode());
		result = prime * result + ((mealStatus == null) ? 0 : mealStatus.hashCode());
		result = prime * result + ((popularity == null) ? 0 : popularity.hashCode());
		result = prime * result + soldMeals;
		result = prime * result + templateId;
		result = prime * result + ((translatedMealCategory == null) ? 0 : translatedMealCategory.hashCode());
		result = prime * result + ((translatedMealDescription == null) ? 0 : translatedMealDescription.hashCode());
		result = prime * result + ((translatedMealTitle == null) ? 0 : translatedMealTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightMealDTO other = (FlightMealDTO) obj;
		if (allocatedMeal != other.allocatedMeal)
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (availableMeals != other.availableMeals)
			return false;
		if (cabinClassCode == null) {
			if (other.cabinClassCode != null)
				return false;
		} else if (!cabinClassCode.equals(other.cabinClassCode))
			return false;
		if (chargeId == null) {
			if (other.chargeId != null)
				return false;
		} else if (!chargeId.equals(other.chargeId))
			return false;
		if (flightMealId == null) {
			if (other.flightMealId != null)
				return false;
		} else if (!flightMealId.equals(other.flightMealId))
			return false;
		if (flightSegmentCode == null) {
			if (other.flightSegmentCode != null)
				return false;
		} else if (!flightSegmentCode.equals(other.flightSegmentCode))
			return false;
		if (flightSegmentID != other.flightSegmentID)
			return false;
		if (isCategoryRestricted != other.isCategoryRestricted)
			return false;
		if (logicalCCCode == null) {
			if (other.logicalCCCode != null)
				return false;
		} else if (!logicalCCCode.equals(other.logicalCCCode))
			return false;
		if (mealCategoryCode == null) {
			if (other.mealCategoryCode != null)
				return false;
		} else if (!mealCategoryCode.equals(other.mealCategoryCode))
			return false;
		if (mealCategoryID != other.mealCategoryID)
			return false;
		if (mealCode == null) {
			if (other.mealCode != null)
				return false;
		} else if (!mealCode.equals(other.mealCode))
			return false;
		if (mealDescription == null) {
			if (other.mealDescription != null)
				return false;
		} else if (!mealDescription.equals(other.mealDescription))
			return false;
		if (mealIATACode == null) {
			if (other.mealIATACode != null)
				return false;
		} else if (!mealIATACode.equals(other.mealIATACode))
			return false;
		if (mealId == null) {
			if (other.mealId != null)
				return false;
		} else if (!mealId.equals(other.mealId))
			return false;
		if (mealName == null) {
			if (other.mealName != null)
				return false;
		} else if (!mealName.equals(other.mealName))
			return false;
		if (mealStatus == null) {
			if (other.mealStatus != null)
				return false;
		} else if (!mealStatus.equals(other.mealStatus))
			return false;
		if (popularity == null) {
			if (other.popularity != null)
				return false;
		} else if (!popularity.equals(other.popularity))
			return false;
		if (soldMeals != other.soldMeals)
			return false;
		if (templateId != other.templateId)
			return false;
		if (translatedMealCategory == null) {
			if (other.translatedMealCategory != null)
				return false;
		} else if (!translatedMealCategory.equals(other.translatedMealCategory))
			return false;
		if (translatedMealDescription == null) {
			if (other.translatedMealDescription != null)
				return false;
		} else if (!translatedMealDescription.equals(other.translatedMealDescription))
			return false;
		if (translatedMealTitle == null) {
			if (other.translatedMealTitle != null)
				return false;
		} else if (!translatedMealTitle.equals(other.translatedMealTitle))
			return false;
		return true;
	}
	
	
	
}
