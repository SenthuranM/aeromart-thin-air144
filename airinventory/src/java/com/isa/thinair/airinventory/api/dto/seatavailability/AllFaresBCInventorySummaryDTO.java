package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Captures all fares applicable on set of bc inventories having a given bookingcode
 * 
 * @author Nasly
 */
public class AllFaresBCInventorySummaryDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = 3686284923044628572L;

	/*
	 * Ordered collection of FareSummaryDTO objects, order by the fareAmount in ascending keyed by fareId
	 */
	private LinkedHashMap<Integer, FareSummaryDTO> fareSummaryDTOs;

	/* Collection of FCCSegBCInvSummaryDTOs */
	private FCCSegBCInvSummaryDTO bcInventorySummaryDTO;

	private boolean isBookingCodeCommonForAllSegments = false;

	private boolean hrtApplicableFareExists = false;

	private String ccCode;

	private String logicalCabinclass;

	public enum HRT_INC {
		ONLY, EXCLUDE
	}

	public boolean isBookingCodeCommonForAllSegments() {
		return isBookingCodeCommonForAllSegments;
	}

	public void setBookingCodeCommonForAllSegments(boolean isBookingCodeCommonForAllSegments) {
		this.isBookingCodeCommonForAllSegments = isBookingCodeCommonForAllSegments;
	}

	public FCCSegBCInvSummaryDTO getBcInventorySummaryDTO() {
		return bcInventorySummaryDTO;
	}

	public void setBcInventorySummaryDTO(FCCSegBCInvSummaryDTO bcInventorySummaryDTO) {
		this.bcInventorySummaryDTO = bcInventorySummaryDTO;
	}

	public LinkedHashMap<Integer, FareSummaryDTO> getFareSummaryDTOs() {
		return fareSummaryDTOs;
	}

	public FareSummaryDTO getFareSummaryDTO(Integer fareId) {
		if (this.fareSummaryDTOs != null) {
			return (FareSummaryDTO) this.fareSummaryDTOs.get(fareId);
		}
		return null;
	}

	public void setFareSummaryDTOs(LinkedHashMap<Integer, FareSummaryDTO> fareSummaryDTOs) {
		this.fareSummaryDTOs = fareSummaryDTOs;
	}

	public void addFareSummaryDTO(FareSummaryDTO fare) {
		if (this.fareSummaryDTOs == null) {
			this.fareSummaryDTOs = new LinkedHashMap<Integer, FareSummaryDTO>();
		}
		if (fare != null
				&& ((AppSysParamsUtil.isAllowReturnParticipateInHalfReturn() && fare.isReturnFare()) || fare
						.isHalfReturnFareApplicable())) {
			this.hrtApplicableFareExists = true;
		}
		this.fareSummaryDTOs.put(new Integer(fare.getFareId()), fare);
	}

	public boolean containsFareId(Integer fareId) {
		return getFareSummaryDTOs().containsKey(fareId);
	}

	public void setBCFareIdCommonFlag(Integer fareId) {
		setBookingCodeCommonForAllSegments(true);
		((FareSummaryDTO) getFareSummaryDTOs().get(fareId)).setFareCommonAllSegments(true);
	}

	// ------------------------------------------------------------------
	// Utility methods for the caller
	// ------------------------------------------------------------------

	public LinkedHashMap<Integer, FareSummaryDTO> getFareSummaryDTOsApplicable(Integer fareType) {
		if (fareSummaryDTOsApplicable == null && this.fareSummaryDTOs != null) {
			fareSummaryDTOsApplicable = new LinkedHashMap<Integer, FareSummaryDTO>();
			Iterator<Integer> fareIdsIt = fareSummaryDTOs.keySet().iterator();
			List<Integer> fareIds = new ArrayList<Integer>();
			while (fareIdsIt.hasNext()) {
				FareSummaryDTO fare = fareSummaryDTOs.get(fareIdsIt.next());
				if (fare.isFareCommonAllSegments()
						|| (fareType == FareTypes.OND_FARE && AppSysParamsUtil.showNestedAvailableSeats())) {
					fareSummaryDTOsApplicable.put(new Integer(fare.getFareId()), fare);
				} else if (fareType == FareTypes.RETURN_FARE && AppSysParamsUtil.showNestedAvailableSeats()) {
					fareIds.add(fare.getFareId());
				}
			}
			if (fareIds.size() > 0 && fareSummaryDTOsApplicable.size() == 0) {
				for (Integer fareId : fareIds) {
					FareSummaryDTO fare = fareSummaryDTOs.get(fareId);
					fareSummaryDTOsApplicable.put(new Integer(fare.getFareId()), fare);
				}
			}
		}
		return fareSummaryDTOsApplicable;
	}
	
	public boolean getFareCommonAllSegments(Integer fareType, FCCSegBCInvSummaryDTO bcInvSummary, int paxCount,
			boolean isSelectedSegmentWithSameBC) {
		
		int availableMaxSeats = bcInvSummary.getAvailableMaxSeats();
		int availableMaxNestedSeats = bcInvSummary.getAvailableMaxNestedSeats();
		int availableNestedSeats = bcInvSummary.getAvailableNestedSeats();
		
		if (fareType == FareTypes.RETURN_FARE || fareType == FareTypes.OND_FARE) {
			if (this.fareSummaryDTOs != null) {
				Iterator<Integer> fareIdsIt = fareSummaryDTOs.keySet().iterator();
				while (fareIdsIt.hasNext()) {
					FareSummaryDTO fare = fareSummaryDTOs.get(fareIdsIt.next());
					if (fare.isFareCommonAllSegments()
							&& ((availableMaxSeats > 0) || (availableMaxNestedSeats >0) || (availableNestedSeats == 0 && isSelectedSegmentWithSameBC))) {
						if (AppSysParamsUtil.showNestedAvailableSeats()) {
							if (availableMaxNestedSeats >= paxCount || (availableNestedSeats == 0 && isSelectedSegmentWithSameBC)) {
								return true;
							}
						} else {
							if (availableMaxSeats >= paxCount) {
								return true;
							}
						}
					}
				}
			}
			return false;
		} else if (fareType == FareTypes.HALF_RETURN_FARE) {
			if (this.fareSummaryDTOs != null) {
				Iterator<Integer> fareIdsIt = fareSummaryDTOs.keySet().iterator();
				while (fareIdsIt.hasNext()) {
					FareSummaryDTO fare = fareSummaryDTOs.get(fareIdsIt.next());
					if (isSelectedSegmentWithSameBC) {
						return true;
					}
					if ((availableMaxSeats > 0) || (availableMaxNestedSeats > 0) ) {
						if (AppSysParamsUtil.showNestedAvailableSeats()) {
							if (availableMaxNestedSeats >= paxCount) {
								return true;
							}
						} else {
							if (availableMaxSeats >= paxCount) {
								return true;
							}
						}
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}

	private LinkedHashMap<Integer, FareSummaryDTO> fareSummaryDTOsApplicable = null;

	public AllFaresBCInventorySummaryDTO clone() {
		return clone(null);
	}

	public AllFaresBCInventorySummaryDTO clone(HRT_INC flag) {
		AllFaresBCInventorySummaryDTO clone = new AllFaresBCInventorySummaryDTO();
		clone.setBookingCodeCommonForAllSegments(isBookingCodeCommonForAllSegments());
		clone.setCcCode(getCcCode());
		clone.setLogicalCabinclass(getLogicalCabinclass());
		LinkedHashMap<Integer, FareSummaryDTO> cloneHM = null;
		if (getFareSummaryDTOs() != null) {
			cloneHM = new LinkedHashMap<Integer, FareSummaryDTO>();
			Iterator<Integer> itr = getFareSummaryDTOs().keySet().iterator();
			while (itr.hasNext()) {
				FareSummaryDTO fare = getFareSummaryDTOs().get(itr.next());
				if ((flag == HRT_INC.ONLY && (AppSysParamsUtil.isAllowReturnParticipateInHalfReturn() || fare
						.isHalfReturnFareApplicable()))
						|| (flag == HRT_INC.EXCLUDE && !fare.isHalfReturnFareApplicable())
						|| (flag == null)) {
					FareSummaryDTO fareSummaryDTO = fare.clone();
					cloneHM.put(new Integer(fareSummaryDTO.getFareId()), fareSummaryDTO);
				}
			}
			clone.setFareSummaryDTOs(cloneHM);
		}
		FCCSegBCInvSummaryDTO bcInventorySummaryDTO = (FCCSegBCInvSummaryDTO) getBcInventorySummaryDTO().clone();
		clone.setBcInventorySummaryDTO(bcInventorySummaryDTO);
		return clone;
	}

	public boolean isHRTFareExists() {
		return hrtApplicableFareExists;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getLogicalCabinclass() {
		return logicalCabinclass;
	}

	public void setLogicalCabinclass(String logicalCabinclass) {
		this.logicalCabinclass = logicalCabinclass;
	}

}
