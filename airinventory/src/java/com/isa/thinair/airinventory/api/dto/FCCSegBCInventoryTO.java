package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 
 * @author Nasly
 * 
 */
public class FCCSegBCInventoryTO implements Serializable {

	private static final long serialVersionUID = -4938363787448112483L;

	private int fccsInvId;

	private int fccsbInvId;

	private String bookingClass;

	private boolean standardBookingClass;

	private boolean fixedBookingClass;

	private int nestRank;

	private String allocationType;

	private boolean priorityBucket;

	private int allocation;

	private int available;

	private int soldSeats;

	private int onholdSeats;

	private int cancelledSeats;

	private String status;

	private String statusChangeAction;

	private String optimizationStatus;

	private String thresholdStatus;

	private long version;

	private Collection<Integer> fareTypes;

	private BigDecimal minFare;


	public int getFccsInvId() {
		return fccsInvId;
	}

	public void setFccsInvId(int fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	/**
	 * @return Returns the allocation.
	 */
	public int getAllocation() {
		return allocation;
	}

	/**
	 * @param allocation
	 *            The allocation to set.
	 */
	public void setAllocation(int allocation) {
		this.allocation = allocation;
	}

	/**
	 * @return Returns the allocationType.
	 */
	public String getAllocationType() {
		return allocationType;
	}

	/**
	 * @param allocationType
	 *            The allocationType to set.
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	/**
	 * @return Returns the bookingClass.
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	/**
	 * @param bookingClass
	 *            The bookingClass to set.
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	/**
	 * @return Returns the fccsbInvId.
	 */
	public int getFccsbInvId() {
		return fccsbInvId;
	}

	/**
	 * @param fccsbInvId
	 *            The fccsbInvId to set.
	 */
	public void setFccsbInvId(int fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}

	/**
	 * @return Returns the fixedBookingClass.
	 */
	public boolean isFixedBookingClass() {
		return fixedBookingClass;
	}

	/**
	 * @param fixedBookingClass
	 *            The fixedBookingClass to set.
	 */
	public void setFixedBookingClass(boolean fixedBookingClass) {
		this.fixedBookingClass = fixedBookingClass;
	}

	/**
	 * @return Returns the priorityBucket.
	 */
	public boolean isPriorityBucket() {
		return priorityBucket;
	}

	/**
	 * @param priorityBucket
	 *            The priorityBucket to set.
	 */
	public void setPriorityBucket(boolean priorityBucket) {
		this.priorityBucket = priorityBucket;
	}

	/**
	 * @return Returns the standardBookingClass.
	 */
	public boolean isStandardBookingClass() {
		return standardBookingClass;
	}

	/**
	 * @param standardBookingClass
	 *            The standardBookingClass to set.
	 */
	public void setStandardBookingClass(boolean standardBookingClass) {
		this.standardBookingClass = standardBookingClass;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the statusChangeAction.
	 */
	public String getStatusChangeAction() {
		return statusChangeAction;
	}

	/**
	 * @param statusChangeAction
	 *            The statusChangeAction to set.
	 */
	public void setStatusChangeAction(String statusChangeAction) {
		this.statusChangeAction = statusChangeAction;
	}

	/**
	 * @return Returns the available.
	 */
	public int getAvailable() {
		return available;
	}

	/**
	 * @param available
	 *            The available to set.
	 */
	public void setAvailable(int available) {
		this.available = available;
	}

	/**
	 * @return Returns the nestRank.
	 */
	public int getNestRank() {
		return nestRank;
	}

	/**
	 * @param nestRank
	 *            The nestRank to set.
	 */
	public void setNestRank(int nestRank) {
		this.nestRank = nestRank;
	}

	/**
	 * @return Returns the fareTypes.
	 */
	public Collection<Integer> getFareTypes() {
		return fareTypes;
	}

	public String getFareTypesStr() {
		String fareTypesStr = "";
		if (getFareTypes() != null && getFareTypes().size() > 0) {
			Iterator<Integer> fareTypesIt = getFareTypes().iterator();
			while (fareTypesIt.hasNext()) {
				if (fareTypesStr.equals("")) {
					fareTypesStr += (Integer) fareTypesIt.next();
				} else {
					fareTypesStr += ":" + (Integer) fareTypesIt.next();
				}
			}
		}
		return fareTypesStr;
	}

	/**
	 * @param fareTypes
	 *            The fareTypes to set.
	 */
	public void addFareType(Integer fareType) {
		if (this.fareTypes == null) {
			this.fareTypes = new ArrayList<Integer>();
		}
		this.fareTypes.add(fareType);
	}

	/**
	 * @return Returns the onholdSeats.
	 */
	public int getOnholdSeats() {
		return onholdSeats;
	}

	/**
	 * @param onholdSeats
	 *            The onholdSeats to set.
	 */
	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	/**
	 * @return Returns the soldSeats.
	 */
	public int getSoldSeats() {
		return soldSeats;
	}

	/**
	 * @param soldSeats
	 *            The soldSeats to set.
	 */
	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public String getOptimizationStatus() {
		return optimizationStatus;
	}

	public void setOptimizationStatus(String optimizationStatus) {
		this.optimizationStatus = optimizationStatus;
	}

	public String getThresholdStatus() {
		return thresholdStatus;
	}

	public void setThresholdStatus(String thresholdStatus) {
		this.thresholdStatus = thresholdStatus;
	}

	public int getCancelledSeats() {
		return cancelledSeats;
	}

	public void setCancelledSeats(int cancelledSeats) {
		this.cancelledSeats = cancelledSeats;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public BigDecimal getMinFare() {
		return minFare;
	}

	public void setMinFare(BigDecimal minFare) {
		this.minFare = minFare;
	}

}
