package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlightSegAvailableBCWithFareDTO implements Serializable {

	private static final long serialVersionUID = -4851336432668815783L;

	private int flightId;

	private String segmentCode;

	private Date departureDateTimeLocal;

	private Date arrivalDateTimeLocal;

	private String flightNo;

	private List<String> availableBCsInOrder;

	private HashMap<String, BigDecimal> availableBCsWithFares;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public Date getDepartureDateTimeLocal() {
		return this.departureDateTimeLocal;
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	public Date getArrivalDateTimeLocal() {
		return this.arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public List<String> getAvailableBCsInOrder() {
		if (this.availableBCsInOrder == null)
			this.availableBCsInOrder = new ArrayList<String>();
		return this.availableBCsInOrder;
	}

	public void setAvailableBCsInOrder(List<String> availableBCsInOrder) {
		this.availableBCsInOrder = availableBCsInOrder;
	}

	public void addToAvailableBCsInOrder(String bookingCode) {
		ArrayList<String> aList = (ArrayList<String>) this.getAvailableBCsInOrder();
		aList.add(bookingCode);
	}

	public HashMap<String, BigDecimal> getAvailableBCsWithFares() {
		if (this.availableBCsWithFares == null)
			this.availableBCsWithFares = new HashMap<String, BigDecimal>();
		return this.availableBCsWithFares;
	}

	public void setAvailableBCsWithFares(HashMap<String, BigDecimal> availableBCsWithFares) {
		this.availableBCsWithFares = availableBCsWithFares;
	}

	public void addToAvailableBCsWithFares(String bookingCode, BigDecimal amount) {
		Map<String, BigDecimal> bcFareMap = this.getAvailableBCsWithFares();
		bcFareMap.put(bookingCode, amount);
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof FlightSegAvailableBCWithFareDTO)) {
			return false;
		}
		FlightSegAvailableBCWithFareDTO otherFlightSegAvailableBCWithFareDTO = (FlightSegAvailableBCWithFareDTO) o;
		if (getFlightId() == otherFlightSegAvailableBCWithFareDTO.getFlightId()
				&& getDepartureDateTimeLocal().equals(otherFlightSegAvailableBCWithFareDTO.getDepartureDateTimeLocal())
				&& getArrivalDateTimeLocal().equals(otherFlightSegAvailableBCWithFareDTO.getArrivalDateTimeLocal()))
			return true;

		return false;
	}

	public int hashCode() {
		int hash = 1;
		hash = hash * 63 + (getFlightId() != 0 ? new Integer(getFlightId()).hashCode() : 0);
		return hash;
	}

}
