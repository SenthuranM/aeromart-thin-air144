package com.isa.thinair.airinventory.api.util;

import java.math.BigDecimal;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FareSummaryUtil {

	public static void setOutBoundFareSummaryDTO(FareSummaryDTO fareSummaryDTO, int percentage){
		
		BigDecimal adultFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
		BigDecimal childFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
		BigDecimal infantFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));

		
		double adultFareAmount = AccelAeroCalculator.calculatePercentage(adultFare, percentage).doubleValue();
		fareSummaryDTO.setAdultFare(adultFareAmount);
		if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fareSummaryDTO.getChildFareType())) {
			double childFareAmount = AccelAeroCalculator.calculatePercentage(childFare, percentage).doubleValue();
			fareSummaryDTO.setChildFare(childFareAmount);
		}
		if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fareSummaryDTO.getInfantFareType())) {
			double infantFareAmount = AccelAeroCalculator.calculatePercentage(infantFare, percentage).doubleValue();
			fareSummaryDTO.setInfantFare(infantFareAmount);
		}
	}
	
	public static void setInBoundFareSummaryDTO(FareSummaryDTO fareSummaryDTO, int percentage){
		
		BigDecimal adultFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
		BigDecimal childFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
		BigDecimal infantFare = AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));

		
		double adultFareAmount = AccelAeroCalculator.subtract(adultFare,
				AccelAeroCalculator.calculatePercentage(adultFare, (100 - percentage))).doubleValue();
		fareSummaryDTO.setAdultFare(adultFareAmount);
		if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fareSummaryDTO.getChildFareType())) {
			double childFareAmount = AccelAeroCalculator.subtract(childFare,
					AccelAeroCalculator.calculatePercentage(childFare, (100 - percentage))).doubleValue();
			fareSummaryDTO.setChildFare(childFareAmount);
		}
		if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fareSummaryDTO.getInfantFareType())) {
			double infantFareAmount = AccelAeroCalculator.subtract(infantFare,
					AccelAeroCalculator.calculatePercentage(infantFare, (100 - percentage))).doubleValue();
			fareSummaryDTO.setInfantFare(infantFareAmount);
		}
	}
}
