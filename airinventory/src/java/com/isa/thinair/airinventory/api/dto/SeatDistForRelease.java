package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class SeatDistForRelease implements Serializable {

	private static final long serialVersionUID = 7692039785945670705L;

	private String bookingCode;

	private int soldSeats;

	private int onHoldSeats;

	private String bookingClassType = null;
	
	private boolean waitListed = false;

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int numberOfSeats) {
		this.soldSeats = numberOfSeats;
	}

	public int getOnHoldSeats() {
		return onHoldSeats;
	}

	public void setOnHoldSeats(int onHoldSeats) {
		this.onHoldSeats = onHoldSeats;
	}

	public String getBookingClassType() {
		return bookingClassType;
	}

	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	public boolean isWaitListed() {
		return waitListed;
	}

	public void setWaitListed(boolean waitListed) {
		this.waitListed = waitListed;
	}
	
	
}
