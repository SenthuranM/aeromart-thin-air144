package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

/**
 * This DTO stores (oneway P2P,connection pax count) and (return P2P,connection
 * pax count) based on the fare rule
 * 
 * @author Isuru Samaraweera
 *
 */
public final class FCCBCOnewayReturnPaxCountDTO implements Serializable {
	/**
	 * serialVersionUID to track versions
	 */
	private static final long serialVersionUID = -9085481294735320742L;
	/** oneway p2p paxcount for a booking class */
	private Integer oneWayPaxCount;
	/** oneway connection paxcount for a booking class */
	private Integer oneWayConnectionPaxCount;
	/** return p2p paxcount for a booking class */
	private Integer returnPaxCount;
	/** return connection paxcount for a booking class */
	private Integer returnConnectionPaxCount;
	/** Map of oneway connection pax count for a booking class */
	private Map<String, Integer> oneWayConnectionSegments = Collections.emptyMap();
	/** Map of return connection pax count for a booking class */
	private Map<String, Integer> returnConnectionSegments = Collections.emptyMap();

	public FCCBCOnewayReturnPaxCountDTO(final Integer oneWayPaxCount, final Integer oneWayConnectionPaxCount,
			final Integer returnPaxCount, final Integer returnConnectionPaxCount,
			final Map<String, Integer> oneWayConnectionSegments, final Map<String, Integer> returnConnectionSegments) {

		this.oneWayPaxCount = oneWayPaxCount;
		this.oneWayConnectionPaxCount = oneWayConnectionPaxCount;
		this.returnPaxCount = returnPaxCount;
		this.returnConnectionPaxCount = returnConnectionPaxCount;
		this.oneWayConnectionSegments = oneWayConnectionSegments;
		this.returnConnectionSegments = returnConnectionSegments;
	}

	public Map<String, Integer> getOneWayConnectionSegments() {
		return oneWayConnectionSegments;
	}

	public void setOneWayConnectionSegments(final Map<String, Integer> oneWayConnectionSegments) {
		this.oneWayConnectionSegments = oneWayConnectionSegments;
	}

	public Map<String, Integer> getReturnConnectionSegments() {
		return returnConnectionSegments;
	}

	public void setReturnConnectionSegments(final Map<String, Integer> returnConnectionSegments) {
		this.returnConnectionSegments = returnConnectionSegments;
	}

	public Integer getOneWayPaxCount() {
		return oneWayPaxCount;
	}

	public void setOneWayPaxCount(final Integer oneWayPaxCount) {
		this.oneWayPaxCount = oneWayPaxCount;
	}

	public Integer getOneWayConnectionPaxCount() {
		return oneWayConnectionPaxCount;
	}

	public void setOneWayConnectionPaxCount(final Integer oneWayConnectionPaxCount) {
		this.oneWayConnectionPaxCount = oneWayConnectionPaxCount;
	}

	public Integer getReturnPaxCount() {
		return returnPaxCount;
	}

	public void setReturnPaxCount(final Integer returnPaxCount) {
		this.returnPaxCount = returnPaxCount;
	}

	public Integer getReturnConnectionPaxCount() {
		return returnConnectionPaxCount;
	}

	public void setReturnConnectionPaxCount(Integer returnConnectionPaxCount) {
		this.returnConnectionPaxCount = returnConnectionPaxCount;
	}

	@Override
	public String toString() {
		return "FCCBCOnewayReturnPaxCountDTO [oneWayPaxCount=" + oneWayPaxCount + ", oneWayConnectionPaxCount="
				+ oneWayConnectionPaxCount + ", returnPaxCount=" + returnPaxCount + ", returnConnectionPaxCount="
				+ returnConnectionPaxCount + ", oneWayConnectionSegments=" + oneWayConnectionSegments
				+ ", returnConnectionSegments=" + returnConnectionSegments + "]";
	}
}
