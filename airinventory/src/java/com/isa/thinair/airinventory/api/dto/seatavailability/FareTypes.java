/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on : Dec 20, 2005 6:23:16 PM
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto.seatavailability;

/**
 * Represents Fare Types a fare quote can have
 * 
 * @author MN
 */
public interface FareTypes {

	/**
	 * <p>
	 * other available (will holds full journey has a fare)
	 * </p>
	 * NO_FARE 0
	 */
	public static int OTHER_FARE = -1;
	/**
	 * <p>
	 * no fares available
	 * </p>
	 * NO_FARE 0
	 */
	public static int NO_FARE = 0;

	/**
	 * <p>
	 * return fare
	 * </p>
	 * RETURN_FARE 1
	 */
	public static int RETURN_FARE = 1;

	/**
	 * <p>
	 * ond fare fares for outbound and inbound trip
	 * </p>
	 * OND_FARE 2
	 */
	public static int OND_FARE = 2;

	/**
	 * <p>
	 * ond fare for outbound trip and per flight fares for inbound trip
	 * </p>
	 * OND_FARE_AND_PER_FLIGHT_FARE 3
	 */
	public static int OND_FARE_AND_PER_FLIGHT_FARE = 3;

	/**
	 * <p>
	 * per flight fares for outbound trip and ond fare for inbound trip
	 * </p>
	 * PER_FLIGHT_FARE_AND_OND_FARE 4
	 */
	public static int PER_FLIGHT_FARE_AND_OND_FARE = 4;

	/**
	 * <p>
	 * per flight fares
	 * </p>
	 * PER_FLIGHT_FARE 5
	 */
	public static int PER_FLIGHT_FARE = 5;

	/**
	 * <p>
	 * half return fare
	 * </p>
	 * HALF_RETURN_FARE 6
	 */
	public static int HALF_RETURN_FARE = 6;

}