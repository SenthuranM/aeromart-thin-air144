/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.thinair.airinventory.api.dto.InvDowngradeDTO;
import com.isa.thinair.airinventory.api.dto.TempSegBcAllocDto;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRS;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;

/**
 * @author Nasly
 * @author isuru APIs for Seat Inventory functionalities related to passenger reservations
 */
public interface FlightInventoryResBD {

	public static final String SERVICE_NAME = "FlightInventoryResService";

	/**
	 * For availability search for oneway flights (both single, connected)
	 * 
	 * @param availableFlightsDTO
	 * @param restrictionsDTO
	 * @return
	 */
	public AvailableFlightDTO searchAvailableFlightsSeatAvailability(AvailableFlightDTO availableFlightsDTO,
			AvailableFlightSearchDTO restrictionsDTO) throws ModuleException;

	/**
	 * <p>
	 * Availability search with fare quote for oneway flight, connected flight, oneway return flight, connected return
	 * flight
	 * </p>
	 * 
	 * <p>
	 * <b>Fare Quoting happens as follows</b><br>
	 * <b>Single Oneway flight [SOF]</b><br>
	 * 1. Filter standard cheapest bucket(s) applying nesting<br>
	 * 2. Filter non-standard, non-fixed cheapest bucket <br>
	 * 3. (Non fixed) seats are made available from the cheaper of 1 & 2 (cheapest non fixed buckets)<br>
	 * 4. If there are applicable fixed buckets, cheapest fixed bucket also made available<br>
	 * </p>
	 * 
	 * <p>
	 * <b>Single Return Flight [SRF]</b><br>
	 * 1. Filter stadard bucket with cheapest return fare without applying nesting<br>
	 * 2. Filter non-standard, non-fixed bucket with cheapest return fare<br>
	 * 3. Seats are made available from the cheaper of 1 & 2 <br>
	 * 4. If there are applicable fixed buckets with return fare, cheapest fixed bucket with cheapest return fare also
	 * made available<br>
	 * 
	 * 5. If NO non fixed bucket with return fare available, cheapest non fixed buckets bucket from each <br>
	 * flight segment are seperately filtered out applying SOF<br>
	 * 
	 * 6. If NO fixed bucket with return fare, cheapest fixed buckets bucket from each<br>
	 * flight segment are seperately extracted applying SOF<br>
	 * </p>
	 * 
	 * <p>
	 * <b>Connected Oneway Flight [COF]</b><br>
	 * 1. Filter chepeast common non fixed bucket with through fare<br>
	 * 2. If there are applicable fixed buckets, filter cheapest fixed bucket with through fare (connection fare)<br>
	 * 
	 * 3. If NO non fixed bucket with through fare available, cheapest non fixed buckets bucket from each <br>
	 * flight segment are seperately filtered out applying SOF<br>
	 * 
	 * 4. If NO fixed bucket with through fare available, cheapest fixed buckets bucket from each<br>
	 * flight segment is seperately extracted applying SOF<br>
	 * </p>
	 * 
	 * <p>
	 * <b>Connected Return Flight [CRF]</b><br>
	 * 1. First buckets with cheapested return fare<br>
	 * 2. If not available, then filter buckets with through fare<br>
	 * 3. If that also not available, then filter cheapest bucket from individual flight segments applying SOF<br>
	 * </p>
	 * 
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param skipChargeQuote
	 *            TODO
	 * @return
	 */
	public SelectedFlightDTO searchSelectedFlightsSeatAvailability(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote) throws ModuleException;

	public SelectedFlightDTO searchSelectedFlightsSeatAvailabilityTnx(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote) throws ModuleException;

	public SelectedFlightDTO chargeQuoteSelectedFlight(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException;

	/**
	 * Recreate the ondFareDTO from the RecreateFareSegChargesCriteria criteria. This is lighter than requoting to get
	 * the OndFareDTO Collection.
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> recreateFareSegCharges(OndRebuildCriteria criteria) throws ModuleException;

	/**
	 * Check for the segment and BC inventory availability before payment is done.
	 * 
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public boolean checkInventoryAvailability(OndRebuildCriteria criteria) throws ModuleException;

	/**
	 * Filters out all the buckets with fares for a given segment of a journey [TODO: discuss] Finalize this API with
	 * the callers
	 * 
	 * @param allFaresDTO
	 * @return AllFaresDTO
	 * @throws ModuleException
	 */
	public AllFaresDTO getAllBucketFareCombinationsForSegment(AllFaresDTO allFaresDTO, FareFilteringCriteria criteria,
			AvailableFlightSearchDTO fltSearchDTO) throws ModuleException;

	public ChangeFaresDTO recalculateChargesForFareChange(ChangeFaresDTO changeFaresDTO,
			AvailableFlightSearchDTO availableFlightSearchCriteria) throws ModuleException;

	/**
	 * Quote goshow fare for a given flight segment
	 * 
	 * @param goshowFQCriteria
	 * @return Collection of OndFareDTO
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> quoteGoshowFares(ReconcilePassengersTO goshowFQCriteria) throws ModuleException;

	/**
	 * FareQuote for externally booked reservations such as gds
	 * 
	 * @param goshowFQCriteria
	 * @return
	 * @throws ModuleException
	 */
	public OndFareDTO quoteExternallyBookedReservationFares(ReconcilePassengersTO gdsReconcilePassengersTO)
			throws ModuleException;

	/**
	 * Transfer seats from source flight segments to target flight segment for reproteting operation
	 * 
	 * @param Collection
	 *            of FlightSegmentSeatDistributionDTOs
	 */
	public Map<Integer, String> transferSeats(TransferSeatDTO transferSeatDTO) throws ModuleException;

	/**
	 * TransferReservationSegment operation from xbe.
	 * 
	 * Since the flight is cancelled..release seats has already occured in inventory side. This method will do the
	 * reprotect part in inventory. Transfer seats from source flight segments to target flight segment
	 * 
	 * @param Collection
	 *            of FlightSegmentSeatDistributionDTOs
	 */
	public void transferWithoutRelease(TransferSeatDTO transferSeatDTO) throws ModuleException;

	/**
	 * For cancelling(a.k.a. releasing) sold/onhold seats from bc inventories in a flight segment
	 * 
	 * @param Collection
	 *            of SegmentSeatDistsDTOForRelease
	 */
	public void releaseFlightInventory(Collection<SegmentSeatDistsDTOForRelease> colSegmentSeatDistsDTOForRelease)
			throws ModuleException;

	/**
	 * Onhold seat confirmation
	 * 
	 * @param segmentSeatDistDTOs
	 * @param exchangedSegInvIds
	 *            TODO
	 * @throws ModuleException
	 */
	public void confirmOnholdSeats(Collection<SegmentSeatDistsDTO> segmentSeatDistDTOs, Collection<Integer> exchangedSegInvIds)
			throws ModuleException;

	/**
	 * Get flights which has sold seats, onhold seats or cancelled seats
	 * 
	 * @param flightIds
	 * @return Collection flightIds having one or more reservations or cancelled reservations
	 */
	public Collection<Integer> filterFlightsHavingReservations(List<Integer> flightIds) throws ModuleException;

	public Collection<Integer> filterSegmentsHavingReservations(Collection<Integer> flightSegIds) throws ModuleException;

	/**
	 * Filters out flight segments having sold/onhold seats or having cancelled seats
	 * 
	 * @param flightSegmentsMap
	 *            key=flightId value=collection of segment codes
	 * @return ModuleException key=flightId value=collection of segment codes having reservation
	 * @throws
	 */
	// public HashMap filterFlightSegCombinationsHavingReservations(HashMap flightSegmentsMap)
	// throws ModuleException;

	/**
	 * <p>
	 * The first step in making a reservation is blocking the requested seats - this results in onholding the requested
	 * seats. Information required for blocking are obtained through seat availability search.
	 * </p>
	 * 
	 * <p>
	 * Seats are blocked only if the requested seats from requested booking classes are available, [(to be
	 * incoperated)associated fares have not changed and fare rule matches the purchasing/travelling restrictions];
	 * otherwise an exception is thrown.
	 * </p>
	 * 
	 * <p>
	 * [TODO: Discuss, not implemented] If the seats can be made available for the same fare, but with different seats
	 * distribution (applicable for nested seats), should this allowed? If allowed, return type should include seats
	 * distribution so that the caller have the correct information.
	 * </p>
	 * 
	 * <p>
	 * Blocking seats results in corresponding FCCSegmentInventory updating (+onhold, +infantonhold, -available seats),
	 * FCCSegmentBCInventories updating(+onhold, -available seats), intercepting segments' FCCSegmentInventory updating
	 * (-available seats) and insertion of new record to temporary blocked allocations table.
	 * </p>
	 * 
	 * <p>
	 * [TODO: Change] Rather than maintaining onhold, sold and available at segments, consider accessing these values
	 * through a view. This will enhance record updating efficiency and also increases the data integrity; slightly
	 * reduces data retrieval efficiency
	 * </p>
	 * 
	 * <p>
	 * Logic for checking seat availability when blocking available seats on BC inventory >= requested <br>
	 * [Not incoperated!!!]fare amount is not changed, and not expired <br>
	 * [Note : fare rules restrictions are not verified]the restrictions match with the fare rule (channel, advance
	 * booking days, frequency) [TODO: Discuss] check return restrictions?
	 * </p>
	 * 
	 * <p>
	 * blocks seats for the reservation
	 * </p>
	 * 
	 * @param segmentSeatDistsDTOs
	 *            Collection of SegmentSeatDistsDTO
	 * @param tempQuotedFares
	 *            Collection of tempQuotedFares used to reconstruct the OndFareDTO form block seats.
	 * @return collecection of Integer temp record ids
	 * @param blockSeatUID
	 *            uid for the collection of block seats. Used in OndFareDto reconstruction using block seats.
	 * @param fccSegInventoryAuditDTO
	 *            additional details for inventory auditing
	 * @throws ModuleException
	 */
	public Collection<TempSegBcAlloc> blockSeatsForRes(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs,
			Collection<Integer> blockSeatsForRes) throws ModuleException;

	public Collection<TempSegBcAlloc> adjustSeatsBlocked(List<Integer> blockedSeats,int adjustmentAdult, int adjustmentInfant) throws ModuleException;

	public List<TempSegBcAllocDto> getBlockedSeatsInfo(List<Integer> blockSeatIds);

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */
	public void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedIds) throws ModuleException;

	/**
	 * Move blocked seats to the Sold seats
	 * 
	 * @param blockedIds
	 * @throws ModuleException
	 */

	/**
	 * Move blocked seat to sold/onhold (part sold, part onhold)
	 * 
	 * @param blockedRecords
	 * @param adultSeatsToSell
	 * @throws ModuleException
	 */
	public void partiallyMoveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, int adultSeatsToSell,
			int infantSeatsToSell) throws ModuleException;

	/**
	 * Release onholded seats during blocking and remove temporary blocked seats alloc records
	 * 
	 * @param blockedIds
	 * @param isFailSilenty TODO
	 * @param fccSegInventoryAuditDTO
	 * @throws ModuleException
	 */
	public void releaseBlockedSeats(Collection<TempSegBcAlloc> blockedIds, boolean isFailSilenty) throws ModuleException;

	/**
	 * Release onholded seats during blocking and remove temporary blocked seats alloc records - run in independant
	 * transaction
	 * 
	 * @param blockedIds
	 * @param fccSegInventoryAuditDTO
	 * @throws ModuleException
	 */
	public void releaseBlockedSeatsInIndepedentTx(Collection<TempSegBcAlloc> blockedIds, boolean isFailSilenty)
			throws ModuleException;

	/**
	 * Clean up blocked seats by releasing onholded seats during blocking operations, and remove temporary blocked seats
	 * alloc records
	 * 
	 * Blocked seats records whose release time is less than the current time are deleted
	 * 
	 * @param numberOfMins
	 * @throws ModuleException
	 */
	public void cleanupBlockedSeats(boolean isScheduledService) throws ModuleException;

	/**
	 * Retrieve overall seat availability, unsold fixed quota for agent, unsold fixed for other agents/non-agents
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @param logicalCabinClass
	 * @return
	 * @throws ModuleException
	 */
	public List<Integer> getAvailableNonFixedAndFixedSeatsCounts(int flightId, String segmentCode, String logicalCabinClass)
			throws ModuleException;

	/**
	 * Test method
	 * 
	 * @param seagmentSeatDists
	 * @param adultSeatsToSell
	 * @param infantSeatsToSell
	 * @throws ModuleException
	 */
	public void testBlockAndSeatSeats(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs, int adultSeatsToSell,
			int infantSeatsToSell) throws ModuleException;

	/**
	 * Returns infant specific fare quote information
	 * 
	 * @param ondFareDTOs
	 * @param station
	 * @param override
	 * @return
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> getInfantQuote(Collection<OndFareDTO> ondFareDTOs, String station, boolean override,
			String agentCode) throws ModuleException;

	public void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedIds, Map<Integer, String> flightAmSeatIdPaxTypeMap,
			Collection<Integer> flightMealIds, Collection<Integer> flightBaggageIds) throws ModuleException;

	public void partiallyMoveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, int adultSeatsToSell,
			int infantSeatsToSell, Map<Integer, String> flightAmSeatIdPaxTypeMap, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException;

	public Collection<BestOffersDTO> getDynamicBestOffers(BestOffersSearchDTO bestOffersSearchDTO) throws ModuleException;

	public void removeExpiredBestOffers() throws ModuleException;

	public Map<InvDowngradeDTO, Integer> getAvailableBookingClassInventory(Integer flightSegId,
			Collection<String> availableCabinClasses) throws ModuleException;

	AvailableFlightDTO nonSelectedDatesFareQuoteInIBE(AvailableFlightDTO availableFlightsDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean includeSelectedDate) throws ModuleException;

	public Map<Integer, List<Pair<String, Integer>>> getBcAvailabilityForMyIDTravel(List<Integer> flightSegIds, String agentCode)
			throws ModuleException;

	public void confirmWaitListedSeats(WLConfirmationRequestDTO wlRequest, boolean isConfirmedReservation) throws ModuleException;

	public List<FilteredBucketsDTO> getSingleFlightOneWayCheapestFare(AvailableFlightSegment availableFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException;

	public boolean isValidConnectionRoute(String segmentCode) throws ModuleException;

	public ReservationAudit releaseAncillaryInventory(Collection<Integer> flightAMSeatIds, Collection<Integer> flightMealIds,
			Collection<Integer> flightBaggageIds) throws ModuleException;

	public CalendarSearchRS searchCalendarFares(CalendarSearchRQ calendarSearchRQ) throws ModuleException;
    
	public AASelectedAncillaryRS getSelectedAncillaryDetails(AASelectedAncillaryRQ aaSelectedAncillaryRQ, Map<String, BundledFareDTO> segmentBundledFareMap, TrackInfoDTO trackInfoDTO) throws ModuleException;

}