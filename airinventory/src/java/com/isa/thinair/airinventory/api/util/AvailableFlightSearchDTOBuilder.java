package com.isa.thinair.airinventory.api.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AvailableFlightSearchDTOBuilder {

	private final AvailableFlightSearchDTO searchDTO = new AvailableFlightSearchDTO();

	private Date departureDate;

	private Date returnDate;

	private int departureVariance;

	private int arrivalVariance;

	private boolean usingExactStartTimes = false;

	public void setAdultCount(int adultCount) {
		searchDTO.setAdultCount(adultCount);
	}

	public void setAgentCode(String agentCode) {
		searchDTO.setAgentCode(agentCode);
	}

	public void setAvailabilityRestrictionLevel(int availabilityRestrictionLevel) {
		searchDTO.setAvailabilityRestrictionLevel(availabilityRestrictionLevel);
	}

	public void setBookingClassCode(String bookingClassCode) {
		searchDTO.setBookingClassCode(bookingClassCode);
	}

	public void setBookingPaxType(String bookingPaxType) {
		searchDTO.setBookingPaxType(bookingPaxType);
	}

	public void setBookingType(String bookingType) {
		searchDTO.setBookingType(bookingType);
	}

	public void setCabinClassCode(String cabinClassCode) {
		if (!searchDTO.getCabinClassSelection().containsKey(cabinClassCode)) {
			searchDTO.getCabinClassSelection().put(cabinClassCode, null);
		}
	}

	public void setLogicalCCCode(String logicalCCCode) {
		if (!searchDTO.getLogicalCabinClassSelection().containsKey(logicalCCCode)) {
			searchDTO.getLogicalCabinClassSelection().put(logicalCCCode, null);
		}
	}

	public void setChannelCode(int channelCode) {
		searchDTO.setChannelCode(channelCode);
	}

	public void setChildCount(int childCount) {
		searchDTO.setChildCount(childCount);
	}

	public void setConfirmOpenReturn(boolean isConfirmOpenReturn) {
		searchDTO.setConfirmOpenReturn(isConfirmOpenReturn);
	}

	public void setDepatureDateTimeEnd(Date depatureDateTimeEnd) {
		searchDTO.setDepatureDateTimeEnd(depatureDateTimeEnd);
	}

	public void setDepatureDateTimeStart(Date depatureDateTimeStart) {
		searchDTO.setDepatureDateTimeStart(depatureDateTimeStart);
	}

	public void setEnforceFareCheckForModOnd(Boolean enforceFareCheckForModOnd) {
		searchDTO.setEnforceFareCheckForModOnd(enforceFareCheckForModOnd);
	}

	public void setExistingFlightSegments(Collection<FlightSegmentDTO> existingFlightSegments) {
		searchDTO.setExistingFlightSegments(existingFlightSegments);
	}

	public void setFareCategoryType(String fareCategoryType) {
		searchDTO.setFareCategoryType(fareCategoryType);
	}

	public void setFirstDepartureDateTimeZulu(Date firstDepartureDateTimeZulu) {
		searchDTO.setFirstDepartureDateTimeZulu(firstDepartureDateTimeZulu);
	}

	public void setFlightsPerOndRestriction(int flightsPerOndRestriction) {
		searchDTO.setFlightsPerOndRestriction(flightsPerOndRestriction);
	}

	public void setFromAirport(String fromAirport) {
		searchDTO.setFromAirport(fromAirport);
	}

	public void setInBoundFlights(Collection<Integer> inBoundFlights) {
		searchDTO.setInBoundFlights(inBoundFlights);
	}

	public void setInfantCount(int infantCount) {
		searchDTO.setInfantCount(infantCount);
	}

	public void setLastArrivalDateTimeZulu(Date lastArrivalDateTimeZulu) {
		searchDTO.setLastArrivalDateTimeZulu(lastArrivalDateTimeZulu);
	}

	public void setModifiedFlightSegments(Collection<FlightSegmentDTO> modifiedFlightSegments) {
		searchDTO.setModifiedFlightSegments(modifiedFlightSegments);
	}

	public void setModifiedOndFareId(Integer modifiedOndFareId) {
		searchDTO.setModifiedOndFareId(modifiedOndFareId);
	}

	public void setModifiedOndFareType(Integer modifiedOndFareType) {
		searchDTO.setModifiedOndFareType(modifiedOndFareType);
	}

	public void setModifiedOndPaxFareAmount(BigDecimal modifiedOndPaxFareAmount) {
		searchDTO.setModifiedOndPaxFareAmount(modifiedOndPaxFareAmount);
	}

	public void setOndCode(String ondCode) {
		searchDTO.setOndCode(ondCode);
	}

	public void setOutBoundFlights(Collection<Integer> outBoundFlights) {
		searchDTO.setOutBoundFlights(outBoundFlights);
	}

	public void setPosAirport(String posAirport) {
		searchDTO.setPosAirport(posAirport);
	}

	public void setReturnDateTimeEnd(Date returnDateTimeEnd) {
		searchDTO.setReturnDateTimeEnd(returnDateTimeEnd);
	}

	public void setReturnDateTimeStart(Date returnDateTimeStart) {
		searchDTO.setReturnDateTimeStart(returnDateTimeStart);
	}

	public void setReturnFlag(boolean returnFlag) {
		searchDTO.setReturnFlag(returnFlag);
	}

	public void setReturnValidityPeriodId(Integer returnValidityPeriodId) {
		searchDTO.setReturnValidityPeriodId(returnValidityPeriodId);
	}

	public void setSelectedDepatureDateTimeEnd(Date selectedDepatureDateTimeEnd) {
		searchDTO.setSelectedDepatureDateTimeEnd(selectedDepatureDateTimeEnd);
	}

	public void setSelectedDepatureDateTimeStart(Date selectedDepatureDateTimeStart) {
		searchDTO.setSelectedDepatureDateTimeStart(selectedDepatureDateTimeStart);
	}

	public void setSelectedReturnDateTimeEnd(Date selectedReturnDateTimeEnd) {
		searchDTO.setSelectedReturnDateTimeEnd(selectedReturnDateTimeEnd);
	}

	public void setSelectedReturnDateTimeStart(Date selectedReturnDateTimeStart) {
		searchDTO.setSelectedReturnDateTimeStart(selectedReturnDateTimeStart);
	}

	public void setStayOverMillis(long stayOverMillis) {
		searchDTO.setStayOverMillis(stayOverMillis);
	}

	public void setToAirport(String toAirport) {
		searchDTO.setToAirport(toAirport);
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public void setDepartureVariance(int departureVariance) {
		this.departureVariance = departureVariance;
	}

	public void setArrivalVariance(int arrivalVariance) {
		this.arrivalVariance = arrivalVariance;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setPreferredCurrencyCode(String preferredCurrencyCode) {
		searchDTO.setPreferredCurrencyCode(preferredCurrencyCode);
	}

	public void setOndFlexiQuote(Map<Integer, Boolean> ondFlexiQuote) {
		searchDTO.setOndQuoteFlexi(ondFlexiQuote);
	}

	public void addOriginDestination(OriginDestinationInfoDTO ondInfoDTO) {
		searchDTO.addOriginDestination(ondInfoDTO);
	}

	public boolean isUsingExactStartTimes() {
		return usingExactStartTimes;
	}

	public void setUsingExactStartTimes(boolean usingExactStartTimes) {
		this.usingExactStartTimes = usingExactStartTimes;
	}

	public AvailableFlightSearchDTO getSearchDTO() {
		// fill the depatureDateTimeStart and depatureDateTimeEnd
		if (departureDate != null) {
			Date depatureDateTimeStart;
			Date depatureDateTimeEnd;
			Date selectedDepatureDateTimeStart;
			Date selectedDepatureDateTimeEnd;

			if (!this.isUsingExactStartTimes()) {
				depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(departureDate);
				depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, departureVariance * -1);

				depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(departureDate);
				depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, departureVariance);

				selectedDepatureDateTimeStart = CalendarUtil.getStartTimeOfDate(departureDate);
				selectedDepatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(departureDate);

			} else {
				depatureDateTimeStart = departureDate;
				depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, departureVariance * -1);

				depatureDateTimeEnd = CalendarUtil.addADayInHourMinSecs(departureDate);
				depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, departureVariance);

				selectedDepatureDateTimeStart = CalendarUtil.getStartTimeOfDate(departureDate);
				selectedDepatureDateTimeEnd = CalendarUtil.addADayInHourMinSecs(departureDate);
			}

			searchDTO.setDepatureDateTimeStart(depatureDateTimeStart);
			searchDTO.setDepatureDateTimeEnd(depatureDateTimeEnd);
			searchDTO.setSelectedDepatureDateTimeStart(selectedDepatureDateTimeStart);
			searchDTO.setSelectedDepatureDateTimeEnd(selectedDepatureDateTimeEnd);

		}

		if (returnDate != null) {
			Date returnDateTimeStart;
			Date returnDateTimeEnd;
			Date selectedReturnDateTimeStart;
			Date selectedRetrunDateTimeEnd;

			if (!this.isUsingExactStartTimes()) {
				returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
				returnDateTimeStart = CalendarUtil.getOfssetAddedTime(returnDateTimeStart, arrivalVariance * -1);

				returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
				returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(returnDateTimeEnd, arrivalVariance);

				selectedReturnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
				selectedRetrunDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);

			} else {
				returnDateTimeStart = returnDate;
				returnDateTimeStart = CalendarUtil.getOfssetAddedTime(returnDateTimeStart, arrivalVariance * -1);

				returnDateTimeEnd = CalendarUtil.addADayInHourMinSecs(returnDate);
				returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(returnDateTimeEnd, arrivalVariance);

				selectedReturnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
				selectedRetrunDateTimeEnd = CalendarUtil.addADayInHourMinSecs(returnDate);
			}

			searchDTO.setReturnDateTimeStart(returnDateTimeStart);
			searchDTO.setReturnDateTimeEnd(returnDateTimeEnd);
			searchDTO.setSelectedReturnDateTimeStart(selectedReturnDateTimeStart);
			searchDTO.setSelectedReturnDateTimeEnd(selectedRetrunDateTimeEnd);
		}
		searchDTO.setQuoteFares(true);
		return searchDTO;
	}

	public void setFromAirportSubStation(String fromAirportSubStation) {
		this.searchDTO.setFromAirportSubStation(fromAirportSubStation);
	}

	public void setToAirportSubStation(String toAirportSubStation) {
		this.searchDTO.setToAirportSubStation(toAirportSubStation);
	}

	public void setExcludeConnectionFares(boolean isExcludeConnectionFares) {
		this.searchDTO.setExcludeConnectionFares(isExcludeConnectionFares);
	}

	public void setExcludeReturnFares(boolean isExcludeReturnFares) {
		this.searchDTO.setExcludeReturnFares(isExcludeReturnFares);
	}

}
