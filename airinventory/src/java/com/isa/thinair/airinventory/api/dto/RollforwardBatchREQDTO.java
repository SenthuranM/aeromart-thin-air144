package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.commons.api.constants.DayOfWeek;

public class RollforwardBatchREQDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5724248745483961175L;
	
	private List<RollforwardBatchREQSEGDTO> rollforwardBatchREQSEGDTOList;
	private String requestedData;
	private String batchStatus;
	private String flightNumbers;
	private String strSeatFactorMin;
	private String strSeatFactorMax;
	private String strDeleteTarget;
	private String strOverrideLevel;
	private String strCreateSegAllocs;
	private String strRadioMode;
	private String priority;
	private String bcStatus;
	private String deleteODDBC;
	private String createSegAllocs;
	private Integer curTail;
	private Integer overSell;
	private Integer waitListing;
	private Collection<DayOfWeek> daysCol;
	
	
	public Collection<DayOfWeek> getDaysCol() {
		return daysCol;
	}
	public void setDaysCol(Collection<DayOfWeek> daysCol) {
		this.daysCol = daysCol;
	}
	public String getStrSeatFactorMin() {
		return strSeatFactorMin;
	}
	public void setStrSeatFactorMin(String strSeatFactorMin) {
		this.strSeatFactorMin = strSeatFactorMin;
	}
	public String getStrSeatFactorMax() {
		return strSeatFactorMax;
	}
	public void setStrSeatFactorMax(String strSeatFactorMax) {
		this.strSeatFactorMax = strSeatFactorMax;
	}
	public String getStrDeleteTarget() {
		return strDeleteTarget;
	}
	public void setStrDeleteTarget(String strDeleteTarget) {
		this.strDeleteTarget = strDeleteTarget;
	}
	public String getStrOverrideLevel() {
		return strOverrideLevel;
	}
	public void setStrOverrideLevel(String strOverrideLevel) {
		this.strOverrideLevel = strOverrideLevel;
	}
	public String getStrCreateSegAllocs() {
		return strCreateSegAllocs;
	}
	public void setStrCreateSegAllocs(String strCreateSegAllocs) {
		this.strCreateSegAllocs = strCreateSegAllocs;
	}
	public String getStrRadioMode() {
		return strRadioMode;
	}
	public void setStrRadioMode(String strRadioMode) {
		this.strRadioMode = strRadioMode;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getBcStatus() {
		return bcStatus;
	}
	public void setBcStatus(String bcStatus) {
		this.bcStatus = bcStatus;
	}
	public String getDeleteODDBC() {
		return deleteODDBC;
	}
	public void setDeleteODDBC(String deleteODDBC) {
		this.deleteODDBC = deleteODDBC;
	}
	public String getCreateSegAllocs() {
		return createSegAllocs;
	}
	public void setCreateSegAllocs(String createSegAllocs) {
		this.createSegAllocs = createSegAllocs;
	}
	public Integer getCurTail() {
		return curTail;
	}
	public void setCurTail(Integer curTail) {
		this.curTail = curTail;
	}
	public Integer getOverSell() {
		return overSell;
	}
	public void setOverSell(Integer overSell) {
		this.overSell = overSell;
	}
	public Integer getWaitListing() {
		return waitListing;
	}
	public void setWaitListing(Integer waitListing) {
		this.waitListing = waitListing;
	}
	public List<RollforwardBatchREQSEGDTO> getRollforwardBatchREQSEGDTOList() {
		return rollforwardBatchREQSEGDTOList;
	}
	public void setRollforwardBatchREQSEGDTOList(List<RollforwardBatchREQSEGDTO> rollforwardBatchREQSEGDTOList) {
		this.rollforwardBatchREQSEGDTOList = rollforwardBatchREQSEGDTOList;
	}
	public String getRequestedData() {
		return requestedData;
	}
	public void setRequestedData(String requestedData) {
		this.requestedData = requestedData;
	}
	public String getFlightNumbers() {
		return flightNumbers;
	}
	public void setFlightNumbers(String flightNumbers) {
		this.flightNumbers = flightNumbers;
	}
	public String getBatchStatus() {
		return batchStatus;
	}
	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	
}
