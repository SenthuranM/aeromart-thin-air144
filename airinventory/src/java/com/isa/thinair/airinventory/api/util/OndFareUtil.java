package com.isa.thinair.airinventory.api.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OpenReturnPeriodsTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

public class OndFareUtil {

	private static NumberFormat formatter = null;

	public static String getTruncatedDecimalStr(double doubleAmount) {
		if (formatter == null) {
			String strDecimals = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DECIMALS);
			int d = 3;
			if (strDecimals != null) {
				d = Integer.parseInt(strDecimals);
			}
			String strFormat = "#.";
			for (int i = 0; i < d; i++) {
				strFormat += "#";
			}
			formatter = new DecimalFormat(strFormat);
		}
		return formatter.format(doubleAmount);
	}

	private static boolean isOpenReturnSegmentsExist(Collection<OndFareDTO> collFareDTO) {
		Iterator<OndFareDTO> itCollFareDTO = collFareDTO.iterator();
		OndFareDTO ondFareDTO;

		while (itCollFareDTO.hasNext()) {
			ondFareDTO = (OndFareDTO) itCollFareDTO.next();

			if (ondFareDTO.isInBoundOnd() && ondFareDTO.isOpenReturn()) {
				return true;
			}
		}

		return false;
	}

	private static OndFareDTO getOutBoundOndFareDTO(Collection<OndFareDTO> collFareDTO) {
		Iterator<OndFareDTO> itCollFareDTO = collFareDTO.iterator();
		OndFareDTO ondFareDTO;

		while (itCollFareDTO.hasNext()) {
			ondFareDTO = (OndFareDTO) itCollFareDTO.next();

			if (!ondFareDTO.isInBoundOnd()) {
				return ondFareDTO;
			}
		}

		return null;
	}

	private static OndFareDTO getInBoundOndFareDTO(Collection<OndFareDTO> collFareDTO) {
		Iterator<OndFareDTO> itCollFareDTO = collFareDTO.iterator();
		OndFareDTO ondFareDTO;

		while (itCollFareDTO.hasNext()) {
			ondFareDTO = (OndFareDTO) itCollFareDTO.next();

			if (ondFareDTO.isInBoundOnd()) {
				return ondFareDTO;
			}
		}

		return null;
	}

	public static OpenReturnPeriodsTO getOpenReturnPeriods(Collection<OndFareDTO> collFareDTO) throws ModuleException {
		if (isOpenReturnSegmentsExist(collFareDTO)) {
			OndFareDTO ondFareDTO = getOutBoundOndFareDTO(collFareDTO);

			if (ondFareDTO == null) {
				throw new ModuleException("airinventory.arg.changefare.invalid", AirinventoryCustomConstants.INV_MODULE_CODE);
			}

			Date arrivalDateZulu = null;
			String airportCode = null;

			if (ondFareDTO.getSegmentsMap() != null && ondFareDTO.getSegmentsMap().size() > 0) {
				Iterator<FlightSegmentDTO> flightSegmentDTOsIt = ondFareDTO.getSegmentsMap().values().iterator();
				while (flightSegmentDTOsIt.hasNext()) {
					FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) flightSegmentDTOsIt.next();
					if (arrivalDateZulu != null && flightSegmentDTO.getArrivalDateTimeZulu().before(arrivalDateZulu)) {
						// invalid ordering of flight segments
						throw new ModuleException("airinventory.arg.changefare.invalid",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
					arrivalDateZulu = flightSegmentDTO.getArrivalDateTimeZulu();
					airportCode = getLastAirportCode(flightSegmentDTO.getSegmentCode());
				}
			}

			OpenReturnPeriodsTO openReturnPeriodsTO = calculateOpenReturnPeriods(ondFareDTO.getFareSummaryDTO(), arrivalDateZulu,
					airportCode);

			if (AppSysParamsUtil.isAllowHalfReturnFaresForOpenReturnBookings()) {
				OndFareDTO inBoundOndFareDTO = getInBoundOndFareDTO(collFareDTO);

				if (inBoundOndFareDTO != null) {
					OpenReturnPeriodsTO openReturnPeriodsTOIB = calculateOpenReturnPeriods(inBoundOndFareDTO.getFareSummaryDTO(),
							arrivalDateZulu, airportCode);
					if (openReturnPeriodsTOIB != null) {
						if (openReturnPeriodsTO.getConfirmBefore().compareTo(openReturnPeriodsTOIB.getConfirmBefore()) > 0) {
							openReturnPeriodsTO.setConfirmBefore(openReturnPeriodsTOIB.getConfirmBefore());
						}

						if (openReturnPeriodsTO.getTravelExpiry().compareTo(openReturnPeriodsTOIB.getTravelExpiry()) > 0) {
							openReturnPeriodsTO.setTravelExpiry(openReturnPeriodsTOIB.getTravelExpiry());
						}
					}
				}
			}

			return openReturnPeriodsTO;
		}

		return new OpenReturnPeriodsTO();
	}

	public static OpenReturnPeriodsTO getOpenReturnPeriods(boolean isOpenReturn,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, FareSummaryDTO fareSummaryDTO) throws ModuleException {

		if (isOpenReturn) {
			Date arrivalDateZulu = null;
			String airportCode = null;

			if (availableIBOBFlightSegment.isDirectFlight()) {
				arrivalDateZulu = ((AvailableFlightSegment) availableIBOBFlightSegment).getFlightSegmentDTO()
						.getArrivalDateTimeZulu();
				airportCode = getLastAirportCode(((AvailableFlightSegment) availableIBOBFlightSegment).getFlightSegmentDTO()
						.getSegmentCode());
			} else {
				AvailableConnectedFlight connFlt = (AvailableConnectedFlight) availableIBOBFlightSegment;
				Iterator connFltSegIt = connFlt.getAvailableFlightSegments().iterator();
				AvailableFlightSegment fltSeg;

				while (connFltSegIt.hasNext()) {
					fltSeg = (AvailableFlightSegment) connFltSegIt.next();
					arrivalDateZulu = fltSeg.getFlightSegmentDTO().getArrivalDateTimeZulu();
					airportCode = getLastAirportCode(fltSeg.getFlightSegmentDTO().getSegmentCode());
				}
			}

			return calculateOpenReturnPeriods(fareSummaryDTO, arrivalDateZulu, airportCode);
		}

		return new OpenReturnPeriodsTO();
	}

	private static OpenReturnPeriodsTO calculateOpenReturnPeriods(FareSummaryDTO fareSummaryDTO, Date arrivalDateZulu,
			String airportCode) throws ModuleException {

		OpenReturnPeriodsTO openReturnPeriodsTO = new OpenReturnPeriodsTO();

		if (fareSummaryDTO != null && arrivalDateZulu != null && !BeanUtils.nullHandler(airportCode).isEmpty()) {
			long openRTConfPeriodInMins = fareSummaryDTO.getOpenRTConfPeriodInMins();
			long openRTConfPeriodInMonths = fareSummaryDTO.getOpenRTConfPeriodInMonths();
			long maximumStayOverInMins = fareSummaryDTO.getMaximumStayOverInMins();
			long maximumStayOverInMonths = fareSummaryDTO.getMaximumStayOverInMonths();

			// For an open return booking maximum stay over should not be zero
			Date travelExpiryDate = CalendarUtil.add(arrivalDateZulu, 0, (int) maximumStayOverInMonths, 0, 0,
					(int) maximumStayOverInMins, 0);
			Date confirmBeforeDate;

			// If the user hasn't specified confirm before. This will be same as travel expiry date
			if (openRTConfPeriodInMins == 0 && openRTConfPeriodInMonths == 0) {
				confirmBeforeDate = travelExpiryDate;
			} else {
				confirmBeforeDate = CalendarUtil.add(travelExpiryDate, 0, (int) -openRTConfPeriodInMonths, 0, 0,
						(int) -openRTConfPeriodInMins, 0);
			}

			openReturnPeriodsTO.setAirportCode(airportCode);

			openReturnPeriodsTO.setConfirmBefore(confirmBeforeDate);
			openReturnPeriodsTO.setTravelExpiry(travelExpiryDate);

		}

		return openReturnPeriodsTO;
	}

	public static String[] parseMins(Long totalMinutes) {
		if (totalMinutes != null) {
			long days = totalMinutes / (24 * 60);
			long hours = (totalMinutes % (24 * 60)) / 60;
			long minutes = (totalMinutes % (24 * 60)) % 60;

			return new String[] { String.valueOf(days), String.valueOf(hours), String.valueOf(minutes) };
		} else {
			return new String[] { "", "", "" };
		}
	}

	private static String getLastAirportCode(String segmentCode) {
		segmentCode = BeanUtils.nullHandler(segmentCode);

		if (segmentCode.length() > 0) {
			String[] segments = segmentCode.split("/");
			return segments[segments.length - 1];
		} else {
			return "";
		}
	}

	public static boolean isReturnBooking(Collection<OndFareDTO> ondFareDTOs) throws ModuleException {
		if (ondFareDTOs.size() > 1) {
			List<String> outboundCodes = new ArrayList<String>();
			List<String> inboundCodes = new ArrayList<String>();
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				String code = ondFareDTO.getOndCode();
				if (ondFareDTO.getOndSequence() == 0) {
					outboundCodes.add(code);
				} else if (ondFareDTO.getOndSequence() == 1) {
					inboundCodes.add(code);
				} else {
					return false;
				}
			}
			String outboundCode = ReservationApiUtils.getOndCode(outboundCodes);
			String inboundCode = ReservationApiUtils.getOndCode(inboundCodes);
			if (outboundCode.equals(ReservationApiUtils.getInverseOndCode(inboundCode))) {
				return true;
			}
		}
		return false;
	}

}
