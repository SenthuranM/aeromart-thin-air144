package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class OperationStatusDTO implements Serializable {

	private static final long serialVersionUID = -2943547279099660863L;
	private int status;
	private Exception exception = null;
	private String msg = null;

	public static final int OPERATION_SUCCESS = 0;
	public static final int OPERATION_FAILURE = 1;
	public static final int OPERATION_COMPLETED = 2;

	OperationStatusDTO(int status, String msg, Exception exception) {
		setException(exception);
		setStatus(status);
		setMsg(msg);
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
