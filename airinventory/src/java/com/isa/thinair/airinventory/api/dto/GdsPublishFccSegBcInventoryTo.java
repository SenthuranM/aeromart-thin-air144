package com.isa.thinair.airinventory.api.dto;

public class GdsPublishFccSegBcInventoryTo extends FCCSegBCInventoryTO {

	private int gdsId;
	private String eventStatus;
	private String eventCode;

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
}
