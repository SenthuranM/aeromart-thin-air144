package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;

/**
 * @author Nasly DTO for quoting fares and charges for a journey
 */
public class SelectedFlightDTO implements Serializable {

	private static final long serialVersionUID = 6714900692546062504L;

	private final List<List<AvailableIBOBFlightSegment>> ondWiseSelectedDateAllFlights = new ArrayList<List<AvailableIBOBFlightSegment>>();

	private final List<AvailableIBOBFlightSegment> ondWiseMinimumFareFlights = new ArrayList<AvailableIBOBFlightSegment>();

	/**
	 * Minimum segment fares corresponding to selected minimum fares. This is only applicable if the selected fare's are
	 * return fares.
	 */
	private final List<AvailableIBOBFlightSegment> ondWiseMinimumSegFareFlights = new ArrayList<AvailableIBOBFlightSegment>();
	private final List<List<AvailableIBOBFlightSegment>> ondWiseSelectedDateAllSegFareFlights = new ArrayList<List<AvailableIBOBFlightSegment>>();

	private boolean isReturnFareQuote;

	private boolean isOpenReturn;

	private Date lastFareQuotedDate;

	private ApplicablePromotionDetailsTO promotionDetails;

	public Collection<AvailableIBOBFlightSegment> getOndWiseSelectedDateAllFlights(Integer ondSequence) {
		if (ondSequence >= ondWiseSelectedDateAllFlights.size()) {
			ondWiseSelectedDateAllFlights.add(ondSequence, new ArrayList<AvailableIBOBFlightSegment>());
		}
		return ondWiseSelectedDateAllFlights.get(ondSequence);
	}

	public Collection<AvailableIBOBFlightSegment> getSelectedDateAllFlights() {
		Collection<AvailableIBOBFlightSegment> preferredDateAllFlights = new ArrayList<AvailableIBOBFlightSegment>();
		for (Collection<AvailableIBOBFlightSegment> availableFlightSegments : ondWiseSelectedDateAllFlights) {
			preferredDateAllFlights.addAll(availableFlightSegments);
		}
		return preferredDateAllFlights;
	}

	public List<List<AvailableIBOBFlightSegment>> getOndWiseSelectedDateAllFlights() {
		return ondWiseSelectedDateAllFlights;
	}

	public void setOndWiseSelectedDateAllFlights(Integer ondSequence, List<AvailableIBOBFlightSegment> availableFlightSegment) {
		ondWiseSelectedDateAllFlights.add(ondSequence, availableFlightSegment);
	}

	public List<AvailableIBOBFlightSegment> getSelectedOndFlights() {
		return ondWiseMinimumFareFlights;
	}

	public AvailableIBOBFlightSegment getSelectedOndFlight(Integer ondSequence) {
		if (ondSequence < ondWiseMinimumFareFlights.size()) {
			return ondWiseMinimumFareFlights.get(ondSequence);
		}
		return null;
	}

	/**
	 * Gets the segment fare for the given ond sequence.
	 * 
	 * @param ondSequence
	 *            The ond sequence(index of ondWiseMinimumSegFareFlights) of the required AvailableIBOBFlightSegment.
	 * @return AvailableIBOBFlightSegment of the given ond sequence (index).
	 */
	public AvailableIBOBFlightSegment getSelectedOndSegFlight(Integer ondSequence) {
		if (ondSequence < ondWiseMinimumSegFareFlights.size()) {
			return ondWiseMinimumSegFareFlights.get(ondSequence);
		}
		return null;
	}

	public Collection<AvailableIBOBFlightSegment> getOndWiseSelectedDateAllSegFareFlights(Integer ondSequence) {
		if (ondSequence >= ondWiseSelectedDateAllSegFareFlights.size()) {
			ondWiseSelectedDateAllSegFareFlights.add(ondSequence, new ArrayList<AvailableIBOBFlightSegment>());
		}
		return ondWiseSelectedDateAllSegFareFlights.get(ondSequence);
	}

	public void setSelectedOndFlight(Integer ondSequence, AvailableIBOBFlightSegment availableFlightSegment) {
		if (ondSequence > ondWiseMinimumFareFlights.size()) {
			int i = ondWiseMinimumFareFlights.size();
			while (i < ondSequence) {
				ondWiseMinimumFareFlights.add(i++, null);
			}
		}
		if (ondSequence == ondWiseMinimumFareFlights.size()) {
			ondWiseMinimumFareFlights.add(ondSequence, availableFlightSegment);
		} else {
			ondWiseMinimumFareFlights.set(ondSequence, availableFlightSegment);
		}
		// TODO charith do we need following assignement?
		if (availableFlightSegment != null) {
			availableFlightSegment.setSelectedFlightDTO(this);
		}
	}

	public void setSelectedOndSegFlight(Integer ondSequence, AvailableIBOBFlightSegment availableFlightSegment) {
		ondWiseMinimumSegFareFlights.add(ondSequence, availableFlightSegment);
		if (availableFlightSegment != null) {
			availableFlightSegment.setSelectedFlightDTO(this);
		}
	}

	public List<List<AvailableIBOBFlightSegment>> getOndWiseSelectedDateAllSegFareFlights() {
		return ondWiseSelectedDateAllSegFareFlights;
	}

	public void setOndWiseSelectedDateAllSegFareFlights(Integer ondSequence,
			List<AvailableIBOBFlightSegment> availableFlightSegment) {
		ondWiseSelectedDateAllSegFareFlights.add(ondSequence, availableFlightSegment);
	}

	public boolean isReturnFareQuote() {
		return isReturnFareQuote;
	}

	public void setReturnFareQuote(boolean isReturnFareQuote) {
		this.isReturnFareQuote = isReturnFareQuote;
	}

	public boolean isSeatsAvailable() {
		if (ondWiseMinimumFareFlights.size() == 0) {
			return false;
		}
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : ondWiseMinimumFareFlights) {
			if (availableIBOBFlightSegment.getSelectedLogicalCabinClass() == null) {
				return false;
			}
		}
		return true;
	}

	public Collection<String> getAvailableLogicalCabinClasses(Integer ondSequence) {
		Collection<String> availableLogicalCabinClasses = new ArrayList<String>();
		if (ondSequence < ondWiseMinimumFareFlights.size()) {
			return ondWiseMinimumFareFlights.get(ondSequence).getAvailableLogicalCabinClasses();
		}
		return availableLogicalCabinClasses;
	}

	public void setCabinClassCode(Map<String, List<Integer>> cabinClassSelection) {
		for (AvailableIBOBFlightSegment availableFlight : ondWiseMinimumFareFlights) {
			if (availableFlight.isDirectFlight()) {
				AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableFlight;
				if (availableFlightSegment.getCabinClassCode() == null && availableFlightSegment.getFlightSegmentDTO() != null) {
					availableFlightSegment.setCabinClassCode(FareQuoteUtil.getClassOfserviceFromFlightSegId(cabinClassSelection,
							availableFlightSegment.getFlightSegmentDTO().getSegmentId()));
				}
			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableFlight;
				for (AvailableFlightSegment fltSeg : availableConnectedFlight.getAvailableFlightSegments()) {
					if (fltSeg.getCabinClassCode() == null && fltSeg.getFlightSegmentDTO() != null) {
						fltSeg.setCabinClassCode(FareQuoteUtil.getClassOfserviceFromFlightSegId(cabinClassSelection, fltSeg
								.getFlightSegmentDTO().getSegmentId()));
					}
				}
			}
		}
	}

	public void setNumberOfInfants(int infantSeats) {
		for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
			if (availableFlightSegment.isDirectFlight()) {
				((AvailableFlightSegment) availableFlightSegment).setNumberOfInfantSeats(infantSeats);
			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableFlightSegment;
				for (AvailableFlightSegment fltSeg : availableConnectedFlight.getAvailableFlightSegments()) {
					fltSeg.setNumberOfInfantSeats(infantSeats);
				}
			}
		}
	}

	/**
	 * TODO - implement
	 */

	public void addCharge() {
		throw new UnsupportedOperationException("Adding charge operation is not implemented yet.");
	}

	// ---------------------------------------------------------------------------
	// utility methods for the caller
	// ---------------------------------------------------------------------------

	public Integer getFareType() {
		if (isSeatsAvailable()) {
			int outboundFareType = -1;
			int inboundFareType = -1;

			if (ondWiseMinimumFareFlights.size() > OndSequence.OUT_BOUND) {
				AvailableIBOBFlightSegment availableIBOBFlightSegment = ondWiseMinimumFareFlights.get(OndSequence.OUT_BOUND);
				outboundFareType = availableIBOBFlightSegment.getFareType(availableIBOBFlightSegment
						.getSelectedLogicalCabinClass());
			}
			if (ondWiseMinimumFareFlights.size() > OndSequence.IN_BOUND) {
				AvailableIBOBFlightSegment availableIBOBFlightSegment = ondWiseMinimumFareFlights.get(OndSequence.IN_BOUND);
				inboundFareType = availableIBOBFlightSegment.getFareType(availableIBOBFlightSegment
						.getSelectedLogicalCabinClass());
			}

			if (outboundFareType < 0 && inboundFareType < 0) {
				return FareTypes.NO_FARE;
			} else if (outboundFareType >= 0 && inboundFareType < 0) {
				return outboundFareType;
			} else if (outboundFareType == FareTypes.OND_FARE && inboundFareType == FareTypes.OND_FARE) {
				return FareTypes.OND_FARE;
			} else if (outboundFareType == FareTypes.OND_FARE && inboundFareType == FareTypes.PER_FLIGHT_FARE) {
				return FareTypes.OND_FARE_AND_PER_FLIGHT_FARE;
			} else if (outboundFareType == FareTypes.PER_FLIGHT_FARE && inboundFareType == FareTypes.OND_FARE) {
				return FareTypes.PER_FLIGHT_FARE_AND_OND_FARE;
			} else if (outboundFareType == FareTypes.HALF_RETURN_FARE && inboundFareType == FareTypes.HALF_RETURN_FARE) {
				return FareTypes.HALF_RETURN_FARE;
			} else if (AppSysParamsUtil.isAllowHalfReturnSegmentCombinability() && outboundFareType == FareTypes.HALF_RETURN_FARE
					&& (inboundFareType == FareTypes.OND_FARE || inboundFareType == FareTypes.PER_FLIGHT_FARE)) {
				return FareTypes.HALF_RETURN_FARE;
			} else if (AppSysParamsUtil.isAllowHalfReturnSegmentCombinability()
					&& (outboundFareType == FareTypes.OND_FARE || outboundFareType == FareTypes.PER_FLIGHT_FARE)
					&& inboundFareType == FareTypes.HALF_RETURN_FARE) {
				return FareTypes.HALF_RETURN_FARE;
			} else {
				return FareTypes.PER_FLIGHT_FARE;
			}
		}
		return FareTypes.NO_FARE;
	}

	public Double getFareAmount(String sPaxType) {
		Double fareAmount = null;
		if (isSeatsAvailable()) {
			double amount = 0;
			for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
				amount += availableFlightSegment.getFareAmount(availableFlightSegment.getSelectedLogicalCabinClass(), sPaxType)
						.doubleValue();
			}
			fareAmount = new Double(amount);
		}
		return fareAmount;
	}

	public List<Double> getTotalCharges() {
		List<Double> charges = new ArrayList<Double>();
		if (isSeatsAvailable()) {
			double adultCharges = 0;
			double infantCharges = 0;
			double childCharges = 0;
			for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
				List<Double> outboundJourneyTotCharges = availableFlightSegment.getTotalCharges(availableFlightSegment
						.getSelectedLogicalCabinClass());
				if (outboundJourneyTotCharges != null) {
					adultCharges += outboundJourneyTotCharges.get(0);
					infantCharges += outboundJourneyTotCharges.get(1);
					childCharges += outboundJourneyTotCharges.get(2);
				}
			}
			charges.add(adultCharges);
			charges.add(infantCharges);
			charges.add(childCharges);
		}
		return charges;
	}

	public Collection<OndFareDTO> getOndFareDTOs(boolean getSegFareDTOs) {
		Collection<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();
		if (isSeatsAvailable()) {
			int iteration = 1;
			List<AvailableIBOBFlightSegment> minFareFlights = getAvailableIBOBFlightSegments(getSegFareDTOs);

			for (AvailableIBOBFlightSegment availableFlightSegment : minFareFlights) {
				for (OndFareDTO ondFareDTO : availableFlightSegment.getFareChargesSeatsSegmentsDTOs(availableFlightSegment
						.getSelectedLogicalCabinClass())) {
					ondFareDTO.setInBoundOnd(iteration % 2 == 0);
					ondFareDTO.setModifyInbound(isModifyInboundFlight());
					ondFareDTO.setOpenReturn(this.isOpenReturn());
					if (availableFlightSegment.getSelectedBundledFare() != null
							&& !InventoryAPIUtil.isBusSegmentExists(ondFareDTO.getSegmentsMap().values())) {
						ondFareDTO.setSelectedBundledFarePeriodId(availableFlightSegment.getSelectedBundledFare()
								.getBundledFarePeriodId());
					}
					ondFareDTOs.add(ondFareDTO);
				}
				iteration++;
			}
		}
		return ondFareDTOs;
	}

	public List<AvailableIBOBFlightSegment> getAvailableIBOBFlightSegments(boolean isSegmentFare) {
		return isSegmentFare ? ondWiseMinimumSegFareFlights
                        : ondWiseMinimumFareFlights;
	}

	public Collection<FlightSegmentDTO> getFlightSegmentDTOs() {
		Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
			flightSegmentDTOs.addAll(availableFlightSegment.getFlightSegmentDTOs());
		}
		return flightSegmentDTOs;
	}

	/**
	 * If same charge applies twices or more, it is combined to form a single charge with first charge's charge rate id
	 * and amount is set to sum of all the charges
	 * 
	 * @return Collection of QuotedChargeDTOs
	 */
	private Collection<QuotedChargeDTO> getUnifiedCharges() {
		Collection<QuotedChargeDTO> allCharges = new ArrayList<QuotedChargeDTO>();
		if (getFareType() != FareTypes.NO_FARE) {
			for (OndFareDTO ondFareDTO : getOndFareDTOs(false)) {
				if (ondFareDTO.getAllCharges() != null) {
					allCharges.addAll(ondFareDTO.getAllCharges());
				}
			}
		}
		return ChargeQuoteUtils.getUnifiedCharges(allCharges);
	}

	public Collection<QuotedChargeDTO> getUnifiedCharges(Collection<String> chargeGroupCodes, Collection<String> passengerTypes) {
		if (getUnifiedCharges() != null && getUnifiedCharges().size() > 0) {
			return ChargeQuoteUtils.getUnifiedCharges(getUnifiedCharges(), chargeGroupCodes, passengerTypes);
		}
		return null;
	}

	public SelectedFlightDTO getCleanedInstance() {
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();

		int iteration = 0;
		for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
			selectedFlightDTO.setSelectedOndFlight(iteration, availableFlightSegment.getCleanedInstance());
			iteration++;
		}
		selectedFlightDTO.setReturnFareQuote(isReturnFareQuote());
		selectedFlightDTO.setOpenReturn(isOpenReturn());

		return selectedFlightDTO;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		summary.append(nl + "~~~~~~~~~~~~~~~~~~~Selected Flight Segment(s) Summary~~~~~~~~~~~~~~~~~~~~~" + nl);
		summary.append("Selected Flight Segments Information \n");
		summary.append("\tFare Quote Type       = " + (isReturnFareQuote() ? "Return Fare Quote" : "Oneway Fare Quote") + nl);

		summary.append("\tSeats Available For Channel	= " + isSeatsAvailable() + nl);
		if (getFareType() != FareTypes.NO_FARE) {
			summary.append("\tTotal Adult Fare	= " + getFareAmount(PaxTypeTO.ADULT).doubleValue() + nl);
			summary.append("\tTotal Child Fare	= " + getFareAmount(PaxTypeTO.CHILD).doubleValue() + nl);
			summary.append("\tTotal Infant Fare	= " + getFareAmount(PaxTypeTO.INFANT).doubleValue() + nl);
			summary.append("\tTotal Adult Charges	= " + getTotalCharges().get(0) + nl);
			summary.append("\tTotal Infant Charges	= " + getTotalCharges().get(1) + nl);
			summary.append("\tTotal Child Charges	= " + getTotalCharges().get(2) + nl);
		}

		for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
			summary.append(availableFlightSegment.getSummary());
		}
		return summary;
	}

	public Map<String, Double> getTotalFareMap() {
		Map<String, Double> totalFareMap = new HashMap<String, Double>();
		if (isSeatsAvailable()) {

			double adultFare = 0;
			double childFare = 0;
			double infantFare = 0;
			for (AvailableIBOBFlightSegment availableFlightSegment : ondWiseMinimumFareFlights) {
				Map<String, Double> obTotalFareMap = availableFlightSegment.getTotalFareMap(availableFlightSegment
						.getSelectedLogicalCabinClass());
				adultFare += obTotalFareMap.get(PaxTypeTO.ADULT);
				childFare += obTotalFareMap.get(PaxTypeTO.CHILD);
				infantFare += obTotalFareMap.get(PaxTypeTO.INFANT);
			}
			totalFareMap.put(PaxTypeTO.ADULT, adultFare);
			totalFareMap.put(PaxTypeTO.CHILD, childFare);
			totalFareMap.put(PaxTypeTO.INFANT, infantFare);
		}
		return totalFareMap;
	}

	private Map<String, Double> totalSurchargeMap = null;
	private final Map<String, Double> totalFixedSurchargeMap = null;

	private boolean fqWithinValidity;

	public Map<String, Double> getTotalSurcharge() {
		if (totalSurchargeMap == null) {

			if (AppSysParamsUtil.isRequoteEnabled()) {
				Iterator<AvailableIBOBFlightSegment> availableIBOBFltSegItr = ondWiseMinimumFareFlights.iterator();
				totalSurchargeMap = new HashMap<String, Double>();
				while (availableIBOBFltSegItr.hasNext()) {
					AvailableIBOBFlightSegment availableIBOBFltSeg = availableIBOBFltSegItr.next();
					Map<String, Double> surchargeMap = availableIBOBFltSeg.getTotalSurcharge(availableIBOBFltSeg
							.getSelectedLogicalCabinClass());

					if (surchargeMap != null && !surchargeMap.isEmpty()) {
						if (totalSurchargeMap == null || totalSurchargeMap.isEmpty()) {
							totalSurchargeMap = surchargeMap;
						} else {
							totalSurchargeMap.put(PaxTypeTO.ADULT,
									totalSurchargeMap.get(PaxTypeTO.ADULT) + surchargeMap.get(PaxTypeTO.ADULT));
							totalSurchargeMap.put(PaxTypeTO.CHILD,
									totalSurchargeMap.get(PaxTypeTO.CHILD) + surchargeMap.get(PaxTypeTO.CHILD));
							totalSurchargeMap.put(PaxTypeTO.INFANT,
									totalSurchargeMap.get(PaxTypeTO.INFANT) + surchargeMap.get(PaxTypeTO.INFANT));
						}
					}

				}

			} else {
				if (ondWiseMinimumFareFlights.size() > OndSequence.OUT_BOUND) {
					AvailableIBOBFlightSegment availableOBFlightSegment = ondWiseMinimumFareFlights.get(OndSequence.OUT_BOUND);
					totalSurchargeMap = availableOBFlightSegment.getTotalSurcharge(availableOBFlightSegment
							.getSelectedLogicalCabinClass());
					if (ondWiseMinimumFareFlights.size() > OndSequence.IN_BOUND) {
						AvailableIBOBFlightSegment availableIBFlightSegment = ondWiseMinimumFareFlights.get(OndSequence.IN_BOUND);
						Map<String, Double> ibTotalSurchargeMap = availableIBFlightSegment
								.getTotalSurcharge(availableIBFlightSegment.getSelectedLogicalCabinClass());
						totalSurchargeMap.put(PaxTypeTO.ADULT,
								totalSurchargeMap.get(PaxTypeTO.ADULT) + ibTotalSurchargeMap.get(PaxTypeTO.ADULT));
						totalSurchargeMap.put(PaxTypeTO.CHILD,
								totalSurchargeMap.get(PaxTypeTO.CHILD) + ibTotalSurchargeMap.get(PaxTypeTO.CHILD));
						totalSurchargeMap.put(PaxTypeTO.INFANT,
								totalSurchargeMap.get(PaxTypeTO.INFANT) + ibTotalSurchargeMap.get(PaxTypeTO.INFANT));
					}
				}
			}

		}
		return totalSurchargeMap;
	}

	/*
	 * public Map<String, Double> getTotalFixedSurcharge() { if (totalFixedSurchargeMap == null && getOutboundFlight()
	 * != null) { totalFixedSurchargeMap = getOutboundFlight().getTotalSurcharge(); if (isReturnFareQuote() &&
	 * getInboundFlight() != null) { Map<String, Double> ibTotalSurchargeMap = getInboundFlight().getTotalSurcharge();
	 * totalFixedSurchargeMap.put(PaxTypeTO.ADULT, totalFixedSurchargeMap.get(PaxTypeTO.ADULT) +
	 * ibTotalSurchargeMap.get(PaxTypeTO.ADULT)); totalFixedSurchargeMap.put(PaxTypeTO.CHILD,
	 * totalFixedSurchargeMap.get(PaxTypeTO.CHILD) + ibTotalSurchargeMap.get(PaxTypeTO.CHILD));
	 * totalFixedSurchargeMap.put(PaxTypeTO.INFANT, totalFixedSurchargeMap.get(PaxTypeTO.INFANT) +
	 * ibTotalSurchargeMap.get(PaxTypeTO.INFANT)); } } return totalFixedSurchargeMap; }
	 */

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	public OpenReturnPeriodsTO getOpenReturnPeriods() throws ModuleException {
		AvailableIBOBFlightSegment availableIBOBFlightSegment = getSelectedOndFlight(OndSequence.OUT_BOUND);

		OpenReturnPeriodsTO openReturnPeriodsTO = OndFareUtil.getOpenReturnPeriods(isOpenReturn(), availableIBOBFlightSegment,
				availableIBOBFlightSegment.getFare(availableIBOBFlightSegment.getSelectedLogicalCabinClass()));

		Date confirmBefore = openReturnPeriodsTO.getConfirmBefore();
		Date travelExpiry = openReturnPeriodsTO.getTravelExpiry();

		// when half return fares enabled for open return most restrictive validity should be applied
		if (isOpenReturn && AppSysParamsUtil.isAllowHalfReturnFaresForOpenReturnBookings()) {
			AvailableIBOBFlightSegment availableIBOBFlightSegmentIB = getSelectedOndFlight(OndSequence.IN_BOUND);

			if (availableIBOBFlightSegmentIB != null) {
				OpenReturnPeriodsTO openReturnPeriodsTOIB = OndFareUtil.getOpenReturnPeriods(isOpenReturn(),
						availableIBOBFlightSegment,
						availableIBOBFlightSegmentIB.getFare(availableIBOBFlightSegmentIB.getSelectedLogicalCabinClass()));

				if (openReturnPeriodsTOIB != null) {
					if (confirmBefore.compareTo(openReturnPeriodsTOIB.getConfirmBefore()) > 0) {
						confirmBefore = openReturnPeriodsTOIB.getConfirmBefore();
					}

					if (travelExpiry.compareTo(openReturnPeriodsTOIB.getTravelExpiry()) > 0) {
						travelExpiry = openReturnPeriodsTOIB.getTravelExpiry();
					}
				}
			}

		}
		OpenReturnPeriodsTO openCloneReturnPeriodsTO = new OpenReturnPeriodsTO();
		openCloneReturnPeriodsTO.setConfirmBefore(confirmBefore);
		openCloneReturnPeriodsTO.setTravelExpiry(travelExpiry);

		openCloneReturnPeriodsTO.setAirportCode(BeanUtils.nullHandler(openReturnPeriodsTO.getAirportCode()));

		return openCloneReturnPeriodsTO;
	}

	private boolean isModifyInboundFlight() {
		// used to check in case of Modifying an inbound flight segment of a return booking
		boolean blnFlag = false;
		if (getSelectedOndFlight(OndSequence.OUT_BOUND).isDirectFlight()) {
			blnFlag = ((AvailableFlightSegment) getSelectedOndFlight(OndSequence.OUT_BOUND)).isInboundFlightSegment();
		} else {
			blnFlag = (((AvailableConnectedFlight) getSelectedOndFlight(OndSequence.OUT_BOUND)).getAvailableFlightSegments()
					.iterator().next()).isInboundFlightSegment();
		}
		return blnFlag;
	}

	public void setLastFQDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public void setFQWithinValidity(boolean fqWithinValidity) {
		this.fqWithinValidity = fqWithinValidity;
	}

	public boolean isFQWithinValidity() {
		return fqWithinValidity;
	}

	/**
	 * Gets the ondWiseMinimumSegFareFlights
	 * 
	 * @return the ondWiseMinimumSegFareFlights
	 */
	public List<AvailableIBOBFlightSegment> getOndWiseMinimumSegFareFlights() {
		return ondWiseMinimumSegFareFlights;
	}

	public ApplicablePromotionDetailsTO getPromotionDetails() {
		return promotionDetails;
	}

	public void setPromotionDetails(ApplicablePromotionDetailsTO promotionDetails) {
		this.promotionDetails = promotionDetails;
	}

	public List<BundledFareDTO> getSelectedOndBundledFares() {
		List<BundledFareDTO> selectedBundledFares = null;
		if (ondWiseMinimumFareFlights != null && !ondWiseMinimumFareFlights.isEmpty()) {
			selectedBundledFares = new ArrayList<BundledFareDTO>();
			for (AvailableIBOBFlightSegment minFareFlight : ondWiseMinimumFareFlights) {
				selectedBundledFares.add(minFareFlight.getSelectedBundledFare());
			}
		}
		return selectedBundledFares;
	}
}
