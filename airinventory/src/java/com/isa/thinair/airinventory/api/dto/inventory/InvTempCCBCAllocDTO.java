package com.isa.thinair.airinventory.api.dto.inventory;

import java.io.Serializable;

public class InvTempCCBCAllocDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer invTempID;

	private Integer invTempCabinAllocID;

	private Integer invTmpCabinBCAllocID;

	private String statusBcAlloc;

	private String priorityFlag;

	private Integer allocatedSeats;

	private Integer allocWaitListedSeats;

	private String bookingCode;
	
	private String updateFlag;

	private String standardCode;

	private String cabinClass;

	private String nestRank;

	private String fixFlag;

	public Integer getInvTempCabinAllocID() {
		return invTempCabinAllocID;
	}

	public void setInvTempCabinAllocID(Integer invTempCabinAllocID) {
		this.invTempCabinAllocID = invTempCabinAllocID;
	}

	public Integer getInvTmpCabinBCAllocID() {
		return invTmpCabinBCAllocID;
	}

	public void setInvTmpCabinBCAllocID(Integer invTmpCabinBCAllocID) {
		this.invTmpCabinBCAllocID = invTmpCabinBCAllocID;
	}

	public String getStatusBcAlloc() {
		return statusBcAlloc;
	}

	public void setStatusBcAlloc(String statusBcAlloc) {
		this.statusBcAlloc = statusBcAlloc;
	}

	public String getPriorityFlag() {
		return priorityFlag;
	}


	public void setPriorityFlag(String priorityFlag) {
		this.priorityFlag = priorityFlag;
	}


	public Integer getAllocatedSeats() {
		return allocatedSeats;
	}

	public void setAllocatedSeats(Integer allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	public Integer getAllocWaitListedSeats() {
		return allocWaitListedSeats;
	}

	public void setAllocWaitListedSeats(Integer allocWaitListedSeats) {
		this.allocWaitListedSeats = allocWaitListedSeats;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Integer getInvTempID() {
		return invTempID;
	}

	public void setInvTempID(Integer invTempID) {
		this.invTempID = invTempID;
	}

	public String getStandardCode() {
		return standardCode;
	}

	public void setStandardCode(String standardCode) {
		this.standardCode = standardCode;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getNestRank() {
		return nestRank;
	}

	public void setNestRank(String nestRank) {
		this.nestRank = nestRank;
	}

	public String getFixFlag() {
		return fixFlag;
	}

	public void setFixFlag(String fixFlag) {
		this.fixFlag = fixFlag;
	}

}
