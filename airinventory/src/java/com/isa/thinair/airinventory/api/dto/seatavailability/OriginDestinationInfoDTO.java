package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class OriginDestinationInfoDTO implements Serializable, Comparable<OriginDestinationInfoDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String origin;

	private String destination;

	private Date departureDateTimeStart;

	private Date departureDateTimeEnd;

	private Date preferredDate;

	private Date preferredDateTimeStart;

	private Date preferredDateTimeEnd;

	private Date arrivalDateTimeStart;

	private Date arrivalDateTimeEnd;

	private List<Integer> flightSegmentIds;

	private boolean openOnd;

	private boolean quoteFlexi;

	private String preferredClassOfService;

	private String preferredLogicalCabin;

	private String preferredBookingType;

	private String preferredBookingClass;

	private Integer preferredBundledFarePeriodId;

	private List<Integer> existingFlightSegIds;

	private boolean flownOnd;

	private BigDecimal oldPerPaxFare = BigDecimal.ZERO;

	private FareTO oldPerPaxFareTO;

	private boolean unTouchedOnd = false;

	private List<String> unTouchedResSegList;

	private Map<String, Integer> hubTimeDetailMap;

	private boolean groundSegmentOnly;

	private Map<String, String> segmentBookingClass;

	private boolean hasInwardCxnFlight;

	private boolean hasOutwardCxnFlight;

	private Date inwardCxnFlightArriTime;

	private Date outwardCxnFlightDepTime;

	private boolean isSpecificFlightsAvailability;

	private Date selectedFltFirstDepartureDateTime;

	private Integer requestedOndSequence;

	
	private boolean departureCitySearch;
	
	private boolean arrivalCitySearch;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureDateTimeStart() {
		return departureDateTimeStart;
	}

	public void setDepartureDateTimeStart(Date departureDateTimeStart) {
		this.departureDateTimeStart = departureDateTimeStart;
	}

	public Date getDepartureDateTimeEnd() {
		return departureDateTimeEnd;
	}

	public void setDepartureDateTimeEnd(Date departureDateTimeEnd) {
		this.departureDateTimeEnd = departureDateTimeEnd;
	}

	public Date getPreferredDate() {
		return preferredDate;
	}

	public void setPreferredDate(Date preferredDate) {
		this.preferredDate = preferredDate;
	}

	public Date getPreferredDateTimeStart() {
		return preferredDateTimeStart;
	}

	public void setPreferredDateTimeStart(Date preferredDateTimeStart) {
		this.preferredDateTimeStart = preferredDateTimeStart;
	}

	public Date getPreferredDateTimeEnd() {
		return preferredDateTimeEnd;
	}

	public void setPreferredDateTimeEnd(Date preferredDateTimeEnd) {
		this.preferredDateTimeEnd = preferredDateTimeEnd;
	}

	public Date getArrivalDateTimeStart() {
		return arrivalDateTimeStart;
	}

	public void setArrivalDateTimeStart(Date arrivalDateTimeStart) {
		this.arrivalDateTimeStart = arrivalDateTimeStart;
	}

	public Date getArrivalDateTimeEnd() {
		return arrivalDateTimeEnd;
	}

	public void setArrivalDateTimeEnd(Date arrivalDateTimeEnd) {
		this.arrivalDateTimeEnd = arrivalDateTimeEnd;
	}

	public List<Integer> getFlightSegmentIds() {
		return flightSegmentIds;
	}

	public void setFlightSegmentIds(List<Integer> flightSegmentIds) {
		this.flightSegmentIds = flightSegmentIds;
	}

	public void setOpenOnd(boolean openOnd) {
		this.openOnd = openOnd;
	}

	public boolean isOpenOnd() {
		return openOnd;
	}

	public boolean isQuoteFlexi() {
		return quoteFlexi;
	}

	public void setQuoteFlexi(boolean quoteFlexi) {
		this.quoteFlexi = quoteFlexi;
	}

	public String getPreferredClassOfService() {
		return preferredClassOfService;
	}

	public void setPreferredClassOfService(String preferredClassOfService) {
		this.preferredClassOfService = preferredClassOfService;
	}

	public String getPreferredLogicalCabin() {
		return preferredLogicalCabin;
	}

	public void setPreferredLogicalCabin(String preferredLogicalCabin) {
		this.preferredLogicalCabin = preferredLogicalCabin;
	}

	public void setPreferredBookingClass(String preferredBookingClass) {
		this.preferredBookingClass = preferredBookingClass;
	}

	public void setPreferredBookingType(String preferredBookingType) {
		this.preferredBookingType = preferredBookingType;
	}

	public String getPreferredBookingType() {
		return preferredBookingType;
	}

	public String getPreferredBookingClass() {
		return preferredBookingClass;
	}
	
	public Integer getPreferredBundledFarePeriodId() {
		return preferredBundledFarePeriodId;
	}

	public void setPreferredBundledFarePeriodId(Integer preferredBundleFarePeriodId) {
		this.preferredBundledFarePeriodId = preferredBundleFarePeriodId;
	}

	public void setExistingFlightSegIds(List<Integer> existingFlightSegIds) {
		this.existingFlightSegIds = existingFlightSegIds;
	}

	public List<Integer> getExistingFlightSegIds() {
		return existingFlightSegIds;
	}

	public boolean isExistingFlightSegment(Integer fltSegId) {
		if (existingFlightSegIds != null && existingFlightSegIds.contains(fltSegId)) {
			return true;
		}
		return false;
	}

	public OriginDestinationInfoDTO cloneAndSwapOnD() {
		OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();
		ondInfoDTO.setOrigin(this.getDestination());
		ondInfoDTO.setDestination(this.getOrigin());
		ondInfoDTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoDTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoDTO.setDepartureDateTimeEnd(this.getDepartureDateTimeEnd());
		ondInfoDTO.setArrivalDateTimeStart(this.getArrivalDateTimeStart());
		ondInfoDTO.setArrivalDateTimeEnd(this.getArrivalDateTimeEnd());
		ondInfoDTO.setPreferredDateTimeStart(this.getPreferredDateTimeStart());
		ondInfoDTO.setPreferredDateTimeEnd(this.getPreferredDateTimeEnd());
		ondInfoDTO.setFlightSegmentIds(this.getFlightSegmentIds());

		ondInfoDTO.setPreferredClassOfService(this.getPreferredClassOfService());
		ondInfoDTO.setPreferredLogicalCabin(this.getPreferredLogicalCabin());
		ondInfoDTO.setPreferredBookingClass(this.getPreferredBookingClass());
		ondInfoDTO.setPreferredBookingType(this.getPreferredBookingType());
		ondInfoDTO.setExistingFlightSegIds(this.getExistingFlightSegIds());
		ondInfoDTO.setHubTimeDetailMap(this.getHubTimeDetailMap());
		ondInfoDTO.setUnTouchedResSegList(this.getUnTouchedResSegList());
		ondInfoDTO.setSelectedFltFirstDepartureDateTime(this.getSelectedFltFirstDepartureDateTime());
		ondInfoDTO.setDepartureCitySearch(this.isDepartureCitySearch());
		ondInfoDTO.setArrivalCitySearch(this.isArrivalCitySearch());

		return ondInfoDTO;
	}

	public OriginDestinationInfoDTO clone() {
		OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();
		ondInfoDTO.setOrigin(this.getOrigin());
		ondInfoDTO.setDestination(this.getDestination());
		ondInfoDTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoDTO.setDepartureDateTimeStart(this.getDepartureDateTimeStart());
		ondInfoDTO.setDepartureDateTimeEnd(this.getDepartureDateTimeEnd());
		ondInfoDTO.setArrivalDateTimeStart(this.getArrivalDateTimeStart());
		ondInfoDTO.setArrivalDateTimeEnd(this.getArrivalDateTimeEnd());
		ondInfoDTO.setPreferredDateTimeStart(this.getPreferredDateTimeStart());
		ondInfoDTO.setPreferredDateTimeEnd(this.getPreferredDateTimeEnd());
		ondInfoDTO.setFlightSegmentIds(this.getFlightSegmentIds());

		ondInfoDTO.setPreferredClassOfService(this.getPreferredClassOfService());
		ondInfoDTO.setPreferredLogicalCabin(this.getPreferredLogicalCabin());
		ondInfoDTO.setPreferredBookingClass(this.getPreferredBookingClass());
		ondInfoDTO.setPreferredBookingType(this.getPreferredBookingType());
		ondInfoDTO.setExistingFlightSegIds(this.getExistingFlightSegIds());
		ondInfoDTO.setHubTimeDetailMap(this.getHubTimeDetailMap());
		ondInfoDTO.setOldPerPaxFare(this.getOldPerPaxFare());
		ondInfoDTO.setOldPerPaxFareTO(this.getOldPerPaxFareTO());
		ondInfoDTO.setUnTouchedOnd(this.isUnTouchedOnd());
		ondInfoDTO.setFlownOnd(this.isFlownOnd());
		ondInfoDTO.setQuoteFlexi(this.isQuoteFlexi());
		ondInfoDTO.setUnTouchedResSegList(this.getUnTouchedResSegList());
		ondInfoDTO.setPreferredBundledFarePeriodId(this.getPreferredBundledFarePeriodId());
		ondInfoDTO.setGroundSegmentOnly(this.isGroundSegmentOnly());
		ondInfoDTO.setSegmentBookingClass(this.getSegmentBookingClass());
		ondInfoDTO.setHasInwardCxnFlight(this.hasInwardCxnFlight);
		ondInfoDTO.setHasOutwardCxnFlight(this.hasOutwardCxnFlight);
		ondInfoDTO.setInwardCxnFlightArriTime(this.inwardCxnFlightArriTime);
		ondInfoDTO.setOutwardCxnFlightDepTime(this.outwardCxnFlightDepTime);
		ondInfoDTO.setSelectedFltFirstDepartureDateTime(this.getSelectedFltFirstDepartureDateTime());
		ondInfoDTO.setRequestedOndSequence(this.getRequestedOndSequence());
		ondInfoDTO.setDepartureCitySearch(this.isDepartureCitySearch());
		ondInfoDTO.setArrivalCitySearch(this.isArrivalCitySearch());

		return ondInfoDTO;
	}

	@Override
	public int compareTo(OriginDestinationInfoDTO o) {
		Date comparableOtherDate = o.getDepartureDateTimeStart();
		Date comparableThisDate = this.getDepartureDateTimeStart();

		if ((this.isSpecificFlightsAvailability() || o.isSpecificFlightsAvailability()) && o.getPreferredDate() != null
				&& this.getPreferredDate() != null) {
			comparableOtherDate = o.getPreferredDate();
			comparableThisDate = this.getPreferredDate();
		}

		if (CalendarUtil.isSameDay(comparableOtherDate, comparableThisDate) && o.getSelectedFltFirstDepartureDateTime() != null
				&& this.getSelectedFltFirstDepartureDateTime() != null) {
			comparableOtherDate = o.getSelectedFltFirstDepartureDateTime();
			comparableThisDate = this.getSelectedFltFirstDepartureDateTime();
		}

		return comparableThisDate.compareTo(comparableOtherDate);
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public BigDecimal getOldPerPaxFare() {
		return oldPerPaxFare;
	}

	public void setOldPerPaxFare(BigDecimal oldPerPaxFare) {
		this.oldPerPaxFare = oldPerPaxFare;
	}

	/**
	 * @return the oldPerPaxFareTO
	 */
	public FareTO getOldPerPaxFareTO() {
		return oldPerPaxFareTO;
	}

	/**
	 * @param oldPerPaxFareTO
	 *            the oldPerPaxFareTO to set
	 */
	public void setOldPerPaxFareTO(FareTO oldPerPaxFareTO) {
		this.oldPerPaxFareTO = oldPerPaxFareTO;
	}

	public boolean isUnTouchedOnd() {
		return unTouchedOnd;
	}

	public void setUnTouchedOnd(boolean unTouchedOnd) {
		this.unTouchedOnd = unTouchedOnd;
	}

	public Map<String, Integer> getHubTimeDetailMap() {
		return hubTimeDetailMap;
	}

	public void setHubTimeDetailMap(Map<String, Integer> hubTimeDetailMap) {
		this.hubTimeDetailMap = hubTimeDetailMap;
	}

	public List<String> getUnTouchedResSegList() {
		return unTouchedResSegList;
	}

	public void setUnTouchedResSegList(List<String> unTouchedResSegList) {
		this.unTouchedResSegList = unTouchedResSegList;
	}

	public boolean isGroundSegmentOnly() {
		return groundSegmentOnly;
	}

	public void setGroundSegmentOnly(boolean isGroundSegmentOnly) {
		this.groundSegmentOnly = isGroundSegmentOnly;
	}

	public Map<String, String> getSegmentBookingClass() {
		return segmentBookingClass;
	}

	public void setSegmentBookingClass(Map<String, String> segmentBookingClass) {
		this.segmentBookingClass = segmentBookingClass;
	}

	public boolean isHasInwardCxnFlight() {
		return hasInwardCxnFlight;
	}

	public void setHasInwardCxnFlight(boolean hasInwardCxnFlight) {
		this.hasInwardCxnFlight = hasInwardCxnFlight;
	}

	public boolean isHasOutwardCxnFlight() {
		return hasOutwardCxnFlight;
	}

	public void setHasOutwardCxnFlight(boolean hasOutwardCxnFlight) {
		this.hasOutwardCxnFlight = hasOutwardCxnFlight;
	}

	public Date getInwardCxnFlightArriTime() {
		return inwardCxnFlightArriTime;
	}

	public void setInwardCxnFlightArriTime(Date inwardCxnFlightArriTime) {
		this.inwardCxnFlightArriTime = inwardCxnFlightArriTime;
	}

	public Date getOutwardCxnFlightDepTime() {
		return outwardCxnFlightDepTime;
	}

	public void setOutwardCxnFlightDepTime(Date outwardCxnFlightDepTime) {
		this.outwardCxnFlightDepTime = outwardCxnFlightDepTime;
	}

	public boolean isSpecificFlightsAvailability() {
		return isSpecificFlightsAvailability;
	}

	public void setSpecificFlightsAvailability(boolean isSpecificFlightsAvailability) {
		this.isSpecificFlightsAvailability = isSpecificFlightsAvailability;
	}

	public Date getSelectedFltFirstDepartureDateTime() {
		return selectedFltFirstDepartureDateTime;
	}

	public void setSelectedFltFirstDepartureDateTime(Date selectedFltFirstDepartureDateTime) {
		this.selectedFltFirstDepartureDateTime = selectedFltFirstDepartureDateTime;
	}

	public Integer getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(Integer requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public boolean isDepartureCitySearch() {
		return departureCitySearch;
	}

	public void setDepartureCitySearch(boolean departureCitySearch) {
		this.departureCitySearch = departureCitySearch;
	}

	public boolean isArrivalCitySearch() {
		return arrivalCitySearch;
	}

	public void setArrivalCitySearch(boolean arrivalCitySearch) {
		this.arrivalCitySearch = arrivalCitySearch;
	}
}
