package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightSegWithCCDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String origin;

	private String destination;

	private Date departureDate;

	private Date arrivalDate;

	private Integer segDuration;

	private String flightNumber;

	private String modelNumber;

	private List<String> cabinClassAvailability = new ArrayList<String>();

	private String frequency;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Integer getSegDuration() {
		return segDuration;
	}

	public void setSegDuration(Integer segDuration) {
		this.segDuration = segDuration;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public List<String> getCabinClassAvailability() {
		return cabinClassAvailability;
	}

	public void setCabinClassAvailability(List<String> cabinClassAvailability) {
		this.cabinClassAvailability = cabinClassAvailability;
	}

	public void addCabinClassAvailability(String cabinClass) {
		getCabinClassAvailability().add(cabinClass);
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
}
