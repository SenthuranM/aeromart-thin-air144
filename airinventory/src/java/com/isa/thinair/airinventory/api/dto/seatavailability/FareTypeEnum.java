package com.isa.thinair.airinventory.api.dto.seatavailability;

public enum FareTypeEnum {
	FIXED_ONEWAY, FIXED_HALFRT, NORMAL_ONEWAY, NORMAL_HALFRT
	// need to remove these type , new types are ONEWAY ,HALFRT
	, SELECTED_FARE, SECONDARY_FARE
}
