/**
 * 
 */
package com.isa.thinair.airinventory.api.dto.meal;

import java.io.Serializable;

/**
 * @author Indika
 * 
 */
public class FlightMealClassDTO implements Serializable {

	private static final long serialVersionUID = -6036350422928692599L;

	private String cabinClass;

	private int totalPassengerCount;

	private int totalAdultCount;

	private int totalChildCount;

	private int totalInfantCount;

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public int getTotalPassengerCount() {
		return totalPassengerCount;
	}

	public void setTotalPassengerCount(int totalPassengerCount) {
		this.totalPassengerCount = totalPassengerCount;
	}

	public int getTotalAdultCount() {
		return totalAdultCount;
	}

	public void setTotalAdultCount(int totalAdultCount) {
		this.totalAdultCount = totalAdultCount;
	}

	public int getTotalChildCount() {
		return totalChildCount;
	}

	public void setTotalChildCount(int totalChildCount) {
		this.totalChildCount = totalChildCount;
	}

	public int getTotalInfantCount() {
		return totalInfantCount;
	}

	public void setTotalInfantCount(int totalInfantCount) {
		this.totalInfantCount = totalInfantCount;
	}

}
