package com.isa.thinair.airinventory.api.dto.baggage;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class BaggageRq implements Serializable {

	public enum ReservationFlow {
		CREATE, MODIFY
	}

	private List<FlightSegmentTO> flightSegmentTOs;
	private Map<String, String> classesOfService;
	private Map<String, String> logicalCC;
	private boolean checkCutoverTime;
	private Map<String, String> bookingClasses;
	private String agent;
	private boolean isRequote;
	private ReservationFlow reservationFlow;
	private boolean ownReservation; 
	private int salesChannel;

	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public Map<String, String> getClassesOfService() {
		return classesOfService;
	}

	public void setClassesOfService(Map<String, String> classesOfService) {
		this.classesOfService = classesOfService;
	}

	public boolean isCheckCutoverTime() {
		return checkCutoverTime;
	}

	public void setCheckCutoverTime(boolean checkCutoverTime) {
		this.checkCutoverTime = checkCutoverTime;
	}

	public Map<String, String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Map<String, String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public boolean isRequote() {
		return isRequote;
	}

	public void setRequote(boolean requote) {
		isRequote = requote;
	}

	public ReservationFlow getReservationFlow() {
		return reservationFlow;
	}

	public void setReservationFlow(ReservationFlow reservationFlow) {
		this.reservationFlow = reservationFlow;
	}

	public boolean isOwnReservation() {
		return ownReservation;
	}

	public void setOwnReservation(boolean ownReservation) {
		this.ownReservation = ownReservation;
	}

	public int getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(int salesChannel) {
		this.salesChannel = salesChannel;
	}

	public Map<String, String> getLogicalCC() {
		return logicalCC;
	}

	public void setLogicalCC(Map<String, String> logicalCC) {
		this.logicalCC = logicalCC;
	}
	
}
