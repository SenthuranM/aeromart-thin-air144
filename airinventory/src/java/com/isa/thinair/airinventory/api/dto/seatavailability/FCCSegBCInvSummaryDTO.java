package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

/**
 * Summary of flight cabinclass segment bc inventory
 * 
 * @author Nasly
 */
public class FCCSegBCInvSummaryDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = -550637083228585173L;

	private String bookingCode;

	private int allocatedSeats;

	private boolean standardFlag;

	private boolean fixedFlag;

	private int soldSeats;

	private int onholdSeats;

	private int availableSeats;

	private String bcInvStatus;

	private int availableMaxSeats;
	
	private int availableMaxNestedSeats;

	private String onholdRestricted;

	private boolean bookedFlightCabinBeingSearched;

	private boolean sameBookingClassAsExisting;

	private boolean flownOnd;

	private int nestedRank;

	private int availableNestedSeats;
	
	private int availableHRTNestedSeats;

	private int seatsSoldAquiredByNesting;

	private int seatsSoldNested;

	private int seatsOnHoldAquiredByNesting;

	private int seatsOnHoldNested;

	public int getAllocatedSeats() {
		return allocatedSeats;
	}

	public void setAllocatedSeats(int allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public String getBcInvStatus() {
		return bcInvStatus;
	}

	public void setBcInvStatus(String bcInvStatus) {
		this.bcInvStatus = bcInvStatus;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public boolean isFixedFlag() {
		return fixedFlag;
	}

	public void setFixedFlag(boolean fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public boolean isStandardFlag() {
		return standardFlag;
	}

	public void setStandardFlag(boolean standardFlag) {
		this.standardFlag = standardFlag;
	}

	public int getAvailableMaxSeats() {
		return availableMaxSeats;
	}

	public void setAvailableMaxSeats(int availableMaxSeats) {
		this.availableMaxSeats = availableMaxSeats;
	}

	public int getAvailableMaxNestedSeats() {
		return availableMaxNestedSeats;
	}

	public void setAvailableMaxNestedSeats(int availableMaxNestedSeats) {
		this.availableMaxNestedSeats = availableMaxNestedSeats;
	}

	public String getOnholdRestricted() {
		return onholdRestricted;
	}

	public void setOnholdRestricted(String onholdRestricted) {
		this.onholdRestricted = onholdRestricted;
	}

	public void setBookedFlightCabinSearch(boolean bookedFlightCabinBeingSearched) {
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
	}

	public boolean isBookedFlightCabinBeingSearched() {
		return bookedFlightCabinBeingSearched;
	}

	public void setSameBookingClassAsExisting(boolean sameBookingClassAsExisting) {
		this.sameBookingClassAsExisting = sameBookingClassAsExisting;
	}

	public boolean isSameBookingClassAsExisting() {
		return sameBookingClassAsExisting;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public int getAvailableNestedSeats() {
		return availableNestedSeats;
	}

	public void setAvailableNestedSeats(int availableNestedSeats) {
		this.availableNestedSeats = availableNestedSeats;
	}

	public int getAvailableHRTNestedSeats() {
		return availableHRTNestedSeats;
	}

	public void setAvailableHRTNestedSeats(int availableHRTNestedSeats) {
		this.availableHRTNestedSeats = availableHRTNestedSeats;
	}

	public int getNestedRank() {
		return nestedRank;
	}

	public void setNestedRank(int nestedRank) {
		this.nestedRank = nestedRank;
	}

	public int getSeatsSoldAquiredByNesting() {
		return seatsSoldAquiredByNesting;
	}

	public void setSeatsSoldAquiredByNesting(int seatsSoldAquiredByNesting) {
		this.seatsSoldAquiredByNesting = seatsSoldAquiredByNesting;
	}

	public int getSeatsSoldNested() {
		return seatsSoldNested;
	}

	public void setSeatsSoldNested(int seatsSoldNested) {
		this.seatsSoldNested = seatsSoldNested;
	}

	public int getSeatsOnHoldAquiredByNesting() {
		return seatsOnHoldAquiredByNesting;
	}

	public void setSeatsOnHoldAquiredByNesting(int seatsOnHoldAquiredByNesting) {
		this.seatsOnHoldAquiredByNesting = seatsOnHoldAquiredByNesting;
	}

	public int getSeatsOnHoldNested() {
		return seatsOnHoldNested;
	}

	public void setSeatsOnHoldNested(int seatsOnHoldNested) {
		this.seatsOnHoldNested = seatsOnHoldNested;
	}

	public Integer getActualSeatsSold() {
		return getSoldSeats() + getSeatsSoldAquiredByNesting() - getSeatsSoldNested();
	}

	public Integer getActualSeatsOnHold() {
		return getOnholdSeats() + getSeatsOnHoldAquiredByNesting() - getSeatsOnHoldNested();
	}

	@Override
	public Object clone() {
		FCCSegBCInvSummaryDTO clone = new FCCSegBCInvSummaryDTO();
		clone.setBookingCode(this.getBookingCode());
		clone.setAllocatedSeats(this.getAllocatedSeats());
		clone.setStandardFlag(this.isStandardFlag());
		clone.setFixedFlag(this.isFixedFlag());
		clone.setSoldSeats(this.getSoldSeats());
		clone.setOnholdSeats(this.getOnholdSeats());
		clone.setAvailableSeats(this.getAvailableSeats());
		clone.setBcInvStatus(this.getBcInvStatus());
		clone.setAvailableMaxSeats(this.getAvailableMaxSeats());
		clone.setAvailableMaxNestedSeats(this.getAvailableMaxNestedSeats());
		clone.setOnholdRestricted(this.getOnholdRestricted());
		clone.setSameBookingClassAsExisting(this.isSameBookingClassAsExisting());
		clone.setFlownOnd(this.isFlownOnd());
		clone.setBookedFlightCabinSearch(this.isBookedFlightCabinBeingSearched());
		clone.setAvailableNestedSeats(this.getAvailableNestedSeats());
		clone.setAvailableHRTNestedSeats(this.getAvailableHRTNestedSeats());
		clone.setNestedRank(this.getNestedRank());
		clone.setSeatsSoldAquiredByNesting(this.getSeatsSoldAquiredByNesting());
		clone.setSeatsSoldNested(this.getSeatsSoldNested());
		clone.setSeatsOnHoldAquiredByNesting(this.getSeatsOnHoldAquiredByNesting());
		clone.setSeatsOnHoldNested(this.getSeatsOnHoldNested());

		return clone;
	}

}
