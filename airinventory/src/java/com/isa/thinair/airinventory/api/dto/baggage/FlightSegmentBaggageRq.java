package com.isa.thinair.airinventory.api.dto.baggage;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class FlightSegmentBaggageRq implements Serializable {
	private FlightSegmentTO flightSegment;
	private BigDecimal loadFactor;
	private BigDecimal totalWeight;

	public FlightSegmentTO getFlightSegment() {
		return flightSegment;
	}

	public void setFlightSegment(FlightSegmentTO flightSegment) {
		this.flightSegment = flightSegment;
	}

	public BigDecimal getLoadFactor() {
		return loadFactor;
	}

	public void setLoadFactor(BigDecimal loadFactor) {
		this.loadFactor = loadFactor;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}
}
