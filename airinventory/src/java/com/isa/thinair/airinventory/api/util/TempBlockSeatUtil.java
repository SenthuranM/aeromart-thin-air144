package com.isa.thinair.airinventory.api.util;

import java.util.Collection;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class TempBlockSeatUtil {

	public static boolean isExpired(Collection<TempSegBcAlloc> blockSeats) {
		if (blockSeats != null) {
			for (TempSegBcAlloc tempSBA : blockSeats) {
				if (tempSBA.getTimestamp().compareTo(CalendarUtil.getCurrentSystemTimeInZulu()) <= 0) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isExpired(TempSegBcAlloc tempSBA) {
		if (tempSBA != null && tempSBA.getTimestamp().compareTo(CalendarUtil.getCurrentSystemTimeInZulu()) <= 0) {
			return true;
		}

		return false;
	}
}
