package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.ReturnValidityPeriodTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * Captures criteria for flight availability search or for selected flight flight fare quote.
 * 
 * @author Mohamed Nasly
 */
public class AvailableFlightSearchDTO extends CommonAvailabilitySearchCriteria implements Serializable, Cloneable {

	private static final long serialVersionUID = -1243435364222263353L;

	/**
	 * Hold the availability no restriction at all - will return all 0 available flights information
	 */
	public static final int AVAILABILTY_NO_RESTRICTION = 1;

	/**
	 * Hold the availability restricted - will return only available flights information
	 */
	public static final int AVAILABILTY_RESTRICTED = 2;

	/** Holds single one way (OND) flights information */
	public static final int SINGLE_FLIGHTS_ONLY = 1;

	/** Holds connected (multiple flights) flights information */
	public static final int CONNECTED_FLIGHTS_ONLY = 2;

	/** Holds all single and multiple ond flights information */
	public static final int ALL_SINGLE_CONNECTED_FLIGHTS = 3;

	public enum JourneyType {
		SINGLE_SECTOR, MULTI_SECTOR, ROUNDTRIP, CIRCULAR, OPEN_JAW
	}

	private List<OriginDestinationInfoDTO> originDestinationInfoList = new ArrayList<OriginDestinationInfoDTO>();
	@Deprecated
	private String fromAirport;
	@Deprecated
	private String toAirport;

	private String posAirport;

	// private String agentCode;

	private String ondCode;
	@Deprecated
	private Date depatureDateTimeStart;
	@Deprecated
	private Date depatureDateTimeEnd;
	@Deprecated
	private Date arrivalDateTimeStart;
	@Deprecated
	private Date arrivalDateTimeEnd;
	@Deprecated
	private Date selectedDepatureDateTimeStart;
	@Deprecated
	private Date selectedDepatureDateTimeEnd;
	@Deprecated
	private Date returnDateTimeStart;
	@Deprecated
	private Date returnDateTimeEnd;
	@Deprecated
	private Date selectedReturnDateTimeStart;
	@Deprecated
	private Date selectedReturnDateTimeEnd;

	private boolean searchOnlyInterlineFlights = false;

	private boolean searchInitiatedFromInterline = false;

	private Collection<String> externalBookingClasses = null;

	private boolean xbeLite;

	private boolean isHalfReturnFareQuote = false;

	private boolean isInboundFareQuote = false;

	private Collection<FlightSegmentDTO> inverseSegmentsOfModifiedFlightSegment = null;

	private String inverseOndCode;

	private Integer inverseFareID;

	private Map<Integer, Boolean> ondQuoteFlexi = new HashMap<Integer, Boolean>();

	private Map<Integer, Boolean> ondSelectedFlexi = new HashMap<Integer, Boolean>();

	// for selected flights only
	private long stayOverMillis;

	private BigDecimal actualModifiedOndFareAmt;

	private String preferredCurrencyCode;

	private String travelAgent;

	private boolean quoteFares = false;

	private boolean searchAllBC = false;

	private boolean skipInvCheckForFlown = false;

	private boolean includeHRTFares;

	/**
	 * Holds availabilty restriction level on flights Constants Applied ================= AVAILABILTY_NO_RESTRICTION
	 * AVAILABILTY_RESTRICTED
	 */
	private int availabilityRestrictionLevel = AVAILABILTY_RESTRICTED;

	/**
	 * Holds flights per ond level restriction Constants Applied ================= SINGLE_FLIGHTS_ONLY
	 * CONNECTED_FLIGHTS_ONLY ALL_SINGLE_CONNECTED_FLIGHTS
	 */
	private int flightsPerOndRestriction = ALL_SINGLE_CONNECTED_FLIGHTS;

	/**
	 * Collection<Integer> - outbound flight segmentIds collection Specified for selected flight fare quote only
	 */
	private Collection<Integer> outBoundFlights = new ArrayList<Integer>();

	/**
	 * Collection<Integer> - inbound flight segmentIds collection Specified for selected flight fare quote only
	 */
	private Collection<Integer> inBoundFlights = new ArrayList<Integer>();

	private Integer returnValidityPeriodId;

	private boolean isConfirmOpenReturn;

	private boolean hasInwardCxnFlight;

	private boolean hasOutwardCxnFlight;

	private boolean isInterlineFareQuoted;

	private Date inwardCxnFlightArriTime;

	private Date outwardCxnFlightDepTime;

	/** Hold AccelAero System IBE /XBE */
	private String appIndicator;

	private ExternalFareSummaryDTO externalFareSummaryDTO;
	@Deprecated
	private String fromAirportSubStation;
	@Deprecated
	private String toAirportSubStation;

	// FIXME Quick fix for Amadeus connection flights to give only segment fares
	private boolean isExcludeConnectionFares = false;

	private boolean isExcludeReturnFares = false;

	private boolean isGroundSegmentAvailability = false; // used to search only ground segment when Booking channel is
															// WS

	private Map<Integer, FareIdDetailDTO> flightSegFareDetails;

	private Date lastFareQuotedDate;

	private boolean fqWithinValidity = false;

	private boolean ondProcessed = false;

	private JourneyType journeyType = null;

	private Map<Integer, String> fltSegExistingBkgClasses;

	private Map<Integer, String> fltSegExistingCabinClasses;

	private Map<Integer, Date> fltSegLastFQDates;

	private boolean requoteFlightSearch = false;

	private boolean fixedFareAgentOnly;

	private boolean goshoFareAgentOnly;

	private boolean fqOnLastFQDate;

	private boolean allowFlightSearchAfterCutOffTime;

	private boolean multiCitySearch = false;

	private boolean isModifyBooking = false;

	private Date firstInBoundDeparDate;

	private Map<Integer, Boolean> ondSequenceReturnMap = new HashMap<Integer, Boolean>();

	/** Holds list of charges that should exclude from charge quote by privilege users */
	private List<String> excludedCharges;

	private boolean isOutBoundModifiedToAFutureDepartureDateThanInbound;

	/**
	 * This will check route is eligible for promotion, because interline routes are not eligible for promotion. only
	 * own & dry routes are eligible.
	 */
	private boolean promoApplicable = true;

	/** Holds promotion code entered by user */
	private String promoCode;

	/** Holds bank identification Number(BIN) */
	private Integer bankIdentificationNo;

	private String preferredLanguage;

	private boolean hasPrivAddSeatsInCutover;

	private boolean isAllowTillFinalCutOver;

	private Integer flightSearchGap;

	private Map<Integer, List<Integer>> existRetGropMap = new HashMap<Integer, List<Integer>>();

	private Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> ondChargeInfo;

	private Map<Double, List<Integer>> flightDateFareMap = new HashMap<Double, List<Integer>>();

	private boolean isInboundOutboundChanged;

	private Map<List<Integer>, Integer> totalJourneyOrderedOndSEQMap = new HashMap<List<Integer>, Integer>();

	private boolean isSubJourney = false;

	private boolean allowDoOverbookAfterCutOffTime;

	private boolean allowDoOverbookBeforeCutOffTime;

	private boolean bundledFareApplicable = true;

	private boolean skipSameFareBucketQuote = false;

	private String pnr;

	private boolean flownOnDExist = false;

	private boolean fromNameChange = false;

	private boolean isSourceFlightInCutOffTime = false;

	private boolean fareCalendarSearch = false;

	private boolean isQuoteReturnSegmentDiscount = true;

	private boolean preserveOndOrder = false;
	
	private boolean ondSplitOperationInvoked = false;
	
	private boolean ownSearch = true; //TODO This is property to skip split for dry routes
	
	private String customerId;

	public boolean isGroundSegmentAvailability() {
		return isGroundSegmentAvailability;
	}

	public void setGroundSegmentAvailability(boolean isGroundSegmentAvailability) {
		this.isGroundSegmentAvailability = isGroundSegmentAvailability;
	}

	public String getPosAirport() {
		return posAirport;
	}

	public void setPosAirport(String posAirport) {
		this.posAirport = posAirport;
	}

	@Deprecated
	public String getFromAirport() {
		return fromAirport;
	}

	@Deprecated
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	@Deprecated
	public String getToAirport() {
		return toAirport;
	}

	@Deprecated
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public String getFirstDepartingAirport() {
		processOnds();
		return originDestinationInfoList.get(0).getOrigin();
	}

	public void addOriginDestination(OriginDestinationInfoDTO ondInfoDTO) {
		if (ondInfoDTO != null) {
			originDestinationInfoList.add(ondInfoDTO);
		}
	}

	protected void setOriginDestionationInfoDTOs(List<OriginDestinationInfoDTO> originDestinationInfoList) {
		this.originDestinationInfoList = originDestinationInfoList;
	}

	public List<OriginDestinationInfoDTO> getOriginDestinationInfoDTOs() {
		return originDestinationInfoList;
	}

	public boolean isUntouched(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).isUnTouchedOnd();
		}
		return false;
	}

	public boolean isFlexiQuote(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).isQuoteFlexi();
		}
		return false;
	}

	/*
	 * Return true atleast one OnD isFlexiQuote
	 */
	public boolean isFlexiQuote() {
		for (OriginDestinationInfoDTO ondInfoDTO : getOrderedOriginDestinations()) {
			if (ondInfoDTO.isQuoteFlexi()) {
				return true;
			}
		}
		return false;
	}

	public String getPreferredClassOfService(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).getPreferredClassOfService();
		}
		return null;
	}

	public OriginDestinationInfoDTO getPreferredOriginDestination(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence);
		}
		return null;
	}

	public String getPreferredLogicalCabin(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).getPreferredLogicalCabin();
		}
		return null;
	}

	public String getPreferredBookingClass(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).getPreferredBookingClass();
		}
		return null;
	}

	public String getPreferredBookingType(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).getPreferredBookingType();
		}
		return null;
	}

	public Integer getPreferredBundledServicePeriodId(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).getPreferredBundledFarePeriodId();
		}
		return null;
	}

	public Map<String, String> getOndSegmentBookingClassSelection(int ondSequence) {
		if (getOrderedOriginDestinations().size() > ondSequence) {
			return getOrderedOriginDestinations().get(ondSequence).getSegmentBookingClass();
		}
		return null;
	}

	public List<OriginDestinationInfoDTO> getOrderedOriginDestinations() {
		processOnds();
		return originDestinationInfoList;
	}

	private void processOnds() {
		if (!ondProcessed) {
			ondProcessed = true;
			if (originDestinationInfoList.size() > 0) {
				journeyType = JourneyType.SINGLE_SECTOR;
				boolean isRTApplicable = false;

				if (!isPreserveOndOrder()) {
					Collections.sort(originDestinationInfoList);
				}

				// Collections.sort(originDestinationInfoList, new Comparator<OriginDestinationInfoDTO>() {
				//
				// @Override
				// public int compare(OriginDestinationInfoDTO o1, OriginDestinationInfoDTO o2) {
				// if (o1.getDepartureDateTimeStart().compareTo(o1.getDepartureDateTimeStart()) == 0) {
				// if (o1.getDestination().equals(o2.getOrigin())) {
				// return -1;
				// } else if (o1.getOrigin().equals(o2.getDestination())) {
				// return 1;
				// }
				// return 0;
				// }
				// return o1.getDepartureDateTimeStart().compareTo(o1.getDepartureDateTimeStart());
				// }
				//
				// });
				int countinousSectors = 1;
				Set<String> allNodeSet = new HashSet<String>();
				Map<String, List<String>> onDWiseDestinationMap = new HashMap<String, List<String>>();
				if (originDestinationInfoList.size() >= 2) {
					journeyType = JourneyType.MULTI_SECTOR;
					isRTApplicable = true;
					OriginDestinationInfoDTO prevOnd = null;
					for (OriginDestinationInfoDTO ondInfo : originDestinationInfoList) {

						if (ondInfo.isFlownOnd()) {
							this.flownOnDExist = true;
						}

						if (prevOnd != null) {
							if (!ondInfo.getOrigin().equals(prevOnd.getDestination())) {
								isRTApplicable = false;
								countinousSectors++;
							}
						}
						prevOnd = ondInfo;

						// to identify any sub journey with CIRCULAR/ROUND TRIP
						List<String> destinationList = new ArrayList<String>();
						if (onDWiseDestinationMap.get(ondInfo.getOrigin()) != null) {
							destinationList = onDWiseDestinationMap.get(ondInfo.getOrigin());
						}
						destinationList.add(ondInfo.getDestination());
						allNodeSet.add(ondInfo.getDestination());

						onDWiseDestinationMap.put(ondInfo.getOrigin(), destinationList);

					}
				}

				JourneyDetectionUtil jdu = new JourneyDetectionUtil();
				boolean isCircularJourneyFound = jdu.isCircularJourneyFound(onDWiseDestinationMap, allNodeSet);

				// If continuous journey and journey starts and ends in the same airport
				boolean isJouneryStartAndEndAtSamePoint = (originDestinationInfoList.get(0).getOrigin()
						.equals(originDestinationInfoList.get(originDestinationInfoList.size() - 1).getDestination()));
				boolean isCircularTrip = (isRTApplicable && isJouneryStartAndEndAtSamePoint);

				if (isRTApplicable) {
					if (originDestinationInfoList.size() == 2 && countinousSectors == 1 && isJouneryStartAndEndAtSamePoint) {
						journeyType = JourneyType.ROUNDTRIP;
					} else if (isCircularTrip) {
						journeyType = JourneyType.CIRCULAR;
					}
				}

				// update the journey type if its a valid OPEN_JAW
				if (AppSysParamsUtil.isOpenJawMultiCitySearchEnabled() && !isCircularJourneyFound) {
					processValidOpenJawJorney(isJouneryStartAndEndAtSamePoint);
				}

				if (this.isHalfReturnFareQuote && journeyType == JourneyType.OPEN_JAW) {
					this.isHalfReturnFareQuote = true;
				}

				if (isOpenReturnSearch()) {
					int i = 1;
					for (OriginDestinationInfoDTO ondInfo : originDestinationInfoList) {
						if (journeyType == JourneyType.ROUNDTRIP && i == 2) {
							ondInfo.setOpenOnd(true);
							// from open segment also we need to filter the invalid fares
							if (originDestinationInfoList.get(0).getOldPerPaxFare() != null
									&& originDestinationInfoList.get(0).getOldPerPaxFareTO() != null) {
								ondInfo.setOldPerPaxFare(originDestinationInfoList.get(0).getOldPerPaxFare());
								ondInfo.setOldPerPaxFareTO(originDestinationInfoList.get(0).getOldPerPaxFareTO());
							}

						}
						i++;
					}
				}

			}
		}
	}

	public ClassOfServiceDTO getClassOfServiceDetails(int flightSegId) {
		if (this.originDestinationInfoList != null) {
			for (OriginDestinationInfoDTO ondInfoTo : this.originDestinationInfoList) {
				if (ondInfoTo.getFlightSegmentIds().contains(flightSegId)) {
					return new ClassOfServiceDTO(ondInfoTo.getPreferredClassOfService(), ondInfoTo.getPreferredLogicalCabin(),
							null);
				}
			}
		}
		return null;
	}

	public Map<Integer, String> getOndLogicalCabinClassSelection() {
		Map<Integer, String> ondLogicalCC = new HashMap<Integer, String>();
		if (this.originDestinationInfoList != null) {
			int ondSeq = 0;
			for (OriginDestinationInfoDTO ondInfoTo : getOrderedOriginDestinations()) {
				ondLogicalCC.put(ondSeq++, ondInfoTo.getPreferredLogicalCabin());
			}
		}
		return ondLogicalCC;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	@Deprecated
	// We have change backend to pass immediate previous segment arrival segment. This can be removed with open return
	// changes
	public long getStayOverMillis() {
		return stayOverMillis;
	}

	@Deprecated
	public void setStayOverMillis(long stayOverMillis) {
		this.stayOverMillis = stayOverMillis;
	}

	public String getPreferredCurrencyCode() {
		return preferredCurrencyCode;
	}

	public void setPreferredCurrencyCode(String preferredCurrencyCode) {
		this.preferredCurrencyCode = preferredCurrencyCode;
	}

	/**
	 * Returns Collection<Integer> corresponding to inbound flight segmentIds.
	 * 
	 * @return Returns the inBoundFlights.
	 */
	@Deprecated
	public Collection<Integer> getInBoundFlights() {
		return inBoundFlights;
	}

	/**
	 * Sets Collection<Integer> corresponding to inbound flight segmentIds.
	 * 
	 * @param inBoundFlights
	 *            The inBoundFlights to set.
	 */
	@Deprecated
	public void setInBoundFlights(Collection<Integer> inBoundFlights) {
		this.inBoundFlights = inBoundFlights;
	}

	/**
	 * Returns Collection<Integer> corresponding to outbound flight segmentIds.
	 * 
	 * @return Returns the outBoundFlights.
	 */
	@Deprecated
	public Collection<Integer> getOutBoundFlights() {
		return outBoundFlights;
	}

	/**
	 * Sets Collection<Integer> corresponding to outbound flight segmentIds.
	 * 
	 * @param outBoundFlights
	 *            The outBoundFlights to set.
	 */
	@Deprecated
	public void setOutBoundFlights(Collection<Integer> outBoundFlights) {
		this.outBoundFlights = outBoundFlights;
	}

	/**
	 * @return Returns the availabilityRestrictionLevel.
	 */
	public int getAvailabilityRestrictionLevel() {
		return availabilityRestrictionLevel;
	}

	/**
	 * @param availabilityRestrictionLevel
	 *            The availabilityRestrictionLevel to set.
	 */
	public void setAvailabilityRestrictionLevel(int availabilityRestrictionLevel) {
		this.availabilityRestrictionLevel = availabilityRestrictionLevel;
	}

	/**
	 * @return Returns the flightsPerOndRestriction.
	 */
	public int getFlightsPerOndRestriction() {
		return flightsPerOndRestriction;
	}

	/**
	 * @param flightsPerOndRestriction
	 *            The flightsPerOndRestriction to set.
	 */
	public void setFlightsPerOndRestriction(int flightsPerOndRestriction) {
		this.flightsPerOndRestriction = flightsPerOndRestriction;
	}

	/**
	 * Returns true if the modification is for a date change
	 */
	@Deprecated
	public Boolean isDateChangeModification() {
		Boolean isDateChange = false;

		if (getModifiedFlightSegments() != null && getModifiedFlightSegments().size() > 0) {
			String modifiedOndOrigin = null;
			String modifiedOndDestination = null;

			for (FlightSegmentDTO fltSeg : getModifiedFlightSegments()) {
				if (modifiedOndOrigin == null) {
					modifiedOndOrigin = fltSeg.getFromAirport();
				}
				modifiedOndDestination = fltSeg.getToAirport();
			}

			OriginDestinationInfoDTO ondInfo = getOrderedOriginDestinations().get(0);
			if (ondInfo.getOrigin().equals(modifiedOndOrigin) && ondInfo.getDestination().equals(modifiedOndDestination)
					&& !isReturnFlag()) {
				isDateChange = true;
			}
		}

		return isDateChange;
	}

	/**
	 * Specifies whether or not to exclude segment fares from connection flight availability search.
	 */
	@Deprecated
	public Boolean isExcludeSegFares() {
		Boolean isExcludeSegFares = false;

		if (isDateChangeModification()) {
			if (getModifiedFlightSegments().size() > 1 && getEnforceFareCheckForModOnd()) {
				isExcludeSegFares = true;
			}
		}
		return isExcludeSegFares;
	}

	/**
	 * Specifies whether or not to enforce same or higher fare check for fare quote for modify OnD
	 */
	@Deprecated
	public Boolean isApplyFareCheckForModSeg() {
		Boolean isApply = false;

		if (getEnforceFareCheckForModOnd() && isDateChangeModification()) {
			isApply = true;
		}

		return isApply;
	}

	public boolean isOpenReturnSearch() {
		if (getReturnValidityPeriodId() != null /* && getReturnDate() == null */) {
			return true;
		} else {
			return false;
		}
	}

	public ReturnValidityPeriodTO getReturnValidityPeriodTO() throws ModuleException {
		if (getReturnValidityPeriodId() != null) {
			ReturnValidityPeriodTO returnValidityPeriodTO = CommonsServices.getGlobalConfig().getReturnValidityPeriods()
					.get(getReturnValidityPeriodId());

			if (returnValidityPeriodTO != null) {
				return returnValidityPeriodTO;
			}
		}

		throw new ModuleException("commons.data.return.validity.unit.of.measure.is.empty");
	}

	/**
	 * @return the returnValidityPeriodId
	 */
	public Integer getReturnValidityPeriodId() {
		return returnValidityPeriodId;
	}

	/**
	 * @param returnValidityPeriodId
	 *            the returnValidityPeriodId to set
	 */
	public void setReturnValidityPeriodId(Integer returnValidityPeriodId) {
		this.returnValidityPeriodId = returnValidityPeriodId;
	}

	/**
	 * @return the isConfirmOpenReturn
	 */
	public boolean isConfirmOpenReturn() {
		return isConfirmOpenReturn;
	}

	/**
	 * @param isConfirmOpenReturn
	 *            the isConfirmOpenReturn to set
	 */
	public void setConfirmOpenReturn(boolean isConfirmOpenReturn) {
		this.isConfirmOpenReturn = isConfirmOpenReturn;
	}

	@Deprecated
	public Date getDepatureDateTimeStart() {
		return depatureDateTimeStart;
	}

	@Deprecated
	public void setDepatureDateTimeStart(Date depatureDateTimeStart) {
		this.depatureDateTimeStart = depatureDateTimeStart;
	}

	@Deprecated
	public Date getDepatureDateTimeEnd() {
		return depatureDateTimeEnd;
	}

	@Deprecated
	public void setDepatureDateTimeEnd(Date depatureDateTimeEnd) {
		this.depatureDateTimeEnd = depatureDateTimeEnd;
	}

	@Deprecated
	public Date getReturnDateTimeStart() {
		return returnDateTimeStart;
	}

	@Deprecated
	public void setReturnDateTimeStart(Date returnDateTimeStart) {
		this.returnDateTimeStart = returnDateTimeStart;
	}

	@Deprecated
	public Date getReturnDateTimeEnd() {
		return returnDateTimeEnd;
	}

	@Deprecated
	public void setReturnDateTimeEnd(Date returnDateTimeEnd) {
		this.returnDateTimeEnd = returnDateTimeEnd;
	}

	@Deprecated
	public Date getSelectedDepatureDateTimeStart() {
		return selectedDepatureDateTimeStart;
	}

	@Deprecated
	public void setSelectedDepatureDateTimeStart(Date selectedDepatureDateTimeStart) {
		this.selectedDepatureDateTimeStart = selectedDepatureDateTimeStart;
	}

	@Deprecated
	public Date getSelectedDepatureDateTimeEnd() {
		return selectedDepatureDateTimeEnd;
	}

	@Deprecated
	public void setSelectedDepatureDateTimeEnd(Date selectedDepatureDateTimeEnd) {
		this.selectedDepatureDateTimeEnd = selectedDepatureDateTimeEnd;
	}

	@Deprecated
	public Date getSelectedReturnDateTimeStart() {
		return selectedReturnDateTimeStart;
	}

	@Deprecated
	public void setSelectedReturnDateTimeStart(Date selectedReturnDateTimeStart) {
		this.selectedReturnDateTimeStart = selectedReturnDateTimeStart;
	}

	@Deprecated
	public Date getSelectedReturnDateTimeEnd() {
		return selectedReturnDateTimeEnd;
	}

	@Deprecated
	public void setSelectedReturnDateTimeEnd(Date selectedReturnDateTimeEnd) {
		this.selectedReturnDateTimeEnd = selectedReturnDateTimeEnd;
	}

	public boolean isSearchOnlyInterlineFlights() {
		return searchOnlyInterlineFlights;
	}

	public void setSearchOnlyInterlineFlights(boolean searchOnlyInterlineFlights) {
		this.searchOnlyInterlineFlights = searchOnlyInterlineFlights;
	}

	public Collection<String> getExternalBookingClasses() {
		return externalBookingClasses;
	}

	public void setExternalBookingClasses(Collection<String> externalBookingClasses) {
		this.externalBookingClasses = externalBookingClasses;
	}

	/**
	 * @return the hasInwardCxnFlight
	 */
	public boolean isHasInwardCxnFlight() {
		return hasInwardCxnFlight;
	}

	/**
	 * @param hasInwardCxnFlight
	 *            the hasInwardCxnFlight to set
	 */
	public void setHasInwardCxnFlight(boolean hasInwardCxnFlight) {
		this.hasInwardCxnFlight = hasInwardCxnFlight;
	}

	/**
	 * @return the hasOutwardCxnFlight
	 */
	public boolean isHasOutwardCxnFlight() {
		return hasOutwardCxnFlight;
	}

	/**
	 * @param hasOutwardCxnFlight
	 *            the hasOutwardCxnFlight to set
	 */
	public void setHasOutwardCxnFlight(boolean hasOutwardCxnFlight) {
		this.hasOutwardCxnFlight = hasOutwardCxnFlight;
	}

	/**
	 * @return the isInterlineFareQuoted
	 */
	public boolean isInterlineFareQuoted() {
		return isInterlineFareQuoted;
	}

	/**
	 * @param isInterlineFareQuoted
	 *            the isInterlineFareQuoted to set
	 */
	public void setInterlineFareQuoted(boolean isInterlineFareQuoted) {
		this.isInterlineFareQuoted = isInterlineFareQuoted;
	}

	/**
	 * @return the isHalfReturnFareQuote
	 */
	public boolean isHalfReturnFareQuote() {
		return isHalfReturnFareQuote;
	}

	/**
	 * @param isHalfReturnFareQuote
	 */
	public void setHalfReturnFareQuote(boolean isHalfReturnFareQuote) {
		this.isHalfReturnFareQuote = isHalfReturnFareQuote;
	}

	/**
	 * @return the isInboundHalfReturnFare
	 */
	public boolean isInboundFareQuote() {
		return isInboundFareQuote;
	}

	/**
	 * @param isInboundFareQuote
	 */
	public void setInboundFareQuote(boolean isInboundFareQuote) {
		this.isInboundFareQuote = isInboundFareQuote;
	}

	/**
	 * Returns Collection<FlightSegmentDTO>
	 * 
	 * @return Returns the inverseSegmentsOfModifiedFlightSegment.
	 */
	public Collection<FlightSegmentDTO> getInverseSegmentsOfModifiedFlightSegment() {
		return inverseSegmentsOfModifiedFlightSegment;
	}

	/**
	 * Sets Collection<FlightSegmentDTO>
	 * 
	 * @param inverseSegmentsOfModifiedFlightSegment
	 *            to set.
	 */
	public void setInverseSegmentsOfModifiedFlightSegment(Collection<FlightSegmentDTO> inverseSegmentsOfModifiedFlightSegment) {
		this.inverseSegmentsOfModifiedFlightSegment = inverseSegmentsOfModifiedFlightSegment;
	}

	public String getInverseOndCode() {
		return inverseOndCode;
	}

	public void setInverseOndCode(String inverseOndCode) {
		this.inverseOndCode = inverseOndCode;
	}

	/**
	 * @return the externalFareSummaryDTO
	 */
	public ExternalFareSummaryDTO getExternalFareSummaryDTO() {
		return externalFareSummaryDTO;
	}

	/**
	 * @param externalFareSummaryDTO
	 *            the externalFareSummaryDTO to set
	 */
	public void setExternalFareSummaryDTO(ExternalFareSummaryDTO externalFareSummaryDTO) {
		this.externalFareSummaryDTO = externalFareSummaryDTO;
	}

	public Map<Integer, Boolean> getOndQuoteFlexi() {
		if (ondQuoteFlexi == null) {
			ondQuoteFlexi = new HashMap<Integer, Boolean>();
		}
		return ondQuoteFlexi;
	}

	public void setOndQuoteFlexi(Map<Integer, Boolean> ondQuoteFlexi) {
		this.ondQuoteFlexi = ondQuoteFlexi;
	}

	public Map<Integer, Boolean> getOndSelectedFlexi() {
		if (ondSelectedFlexi == null) {
			ondSelectedFlexi = new HashMap<Integer, Boolean>();
		}
		return ondSelectedFlexi;
	}

	public void setOndSelectedFlexi(Map<Integer, Boolean> ondSelectedFlexi) {
		this.ondSelectedFlexi = ondSelectedFlexi;
	}

	/**
	 * @return the actualModifiedOndFareAmt
	 */
	public BigDecimal getActualModifiedOndFareAmt() {
		return actualModifiedOndFareAmt;
	}

	/**
	 * @param actualModifiedOndFareAmt
	 *            the actualModifiedOndFareAmt to set
	 */
	public void setActualModifiedOndFareAmt(BigDecimal actualModifiedOndFareAmt) {
		this.actualModifiedOndFareAmt = actualModifiedOndFareAmt;
	}

	/**
	 * @return the inwardCxnFlightArriTime
	 */
	public Date getInwardCxnFlightArriTime() {
		return inwardCxnFlightArriTime;
	}

	/**
	 * @param inwardCxnFlightArriTime
	 *            the inwardCxnFlightArriTime to set
	 */
	public void setInwardCxnFlightArriTime(Date inwardCxnFlightArriTime) {
		this.inwardCxnFlightArriTime = inwardCxnFlightArriTime;
	}

	/**
	 * @return the outwardCxnFlightDepTime
	 */
	public Date getOutwardCxnFlightDepTime() {
		return outwardCxnFlightDepTime;
	}

	/**
	 * @param outwardCxnFlightDepTime
	 *            the outwardCxnFlightDepTime to set
	 */
	public void setOutwardCxnFlightDepTime(Date outwardCxnFlightDepTime) {
		this.outwardCxnFlightDepTime = outwardCxnFlightDepTime;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSearchInitiatedFromInterline() {
		return searchInitiatedFromInterline;
	}

	/**
	 * 
	 * @param searchInitiatedFromInterline
	 */
	public void setSearchInitiatedFromInterline(boolean searchInitiatedFromInterline) {
		this.searchInitiatedFromInterline = searchInitiatedFromInterline;
	}

	public Integer getInverseFareID() {
		return inverseFareID;
	}

	public void setInverseFareID(Integer inverseFareID) {
		this.inverseFareID = inverseFareID;
	}

	public String getAppIndicator() {
		return appIndicator;
	}

	public void setAppIndicator(String appIndicator) {
		this.appIndicator = appIndicator;
	}

	/** Clone of current data */
	@Override
	public AvailableFlightSearchDTO clone() {
		AvailableFlightSearchDTO clonedAvailableFlightSearchDTO = new AvailableFlightSearchDTO();
		clonedAvailableFlightSearchDTO.setAdultCount(this.getAdultCount());
		clonedAvailableFlightSearchDTO.setChildCount(this.getChildCount());
		clonedAvailableFlightSearchDTO.setAgentCode(this.getAgentCode());
		clonedAvailableFlightSearchDTO.setFixedAgentFareOnly(this.isFixedAgentFareOnly());
		clonedAvailableFlightSearchDTO.setGoshoFareAgentOnly(this.isGoshoFareAgentOnly());
		clonedAvailableFlightSearchDTO.setAvailabilityRestrictionLevel(this.getAvailabilityRestrictionLevel());
		// clonedAvailableFlightSearchDTO.setBookingClassCode(this.getBookingClassCode());
		clonedAvailableFlightSearchDTO.setChannelCode(this.getChannelCode());
		clonedAvailableFlightSearchDTO.setFlightsPerOndRestriction(this.getFlightsPerOndRestriction());
		// clonedAvailableFlightSearchDTO.setFromAirport(this.getFromAirport());
		clonedAvailableFlightSearchDTO.setInfantCount(this.getInfantCount());
		clonedAvailableFlightSearchDTO.setOndCode(this.getOndCode());
		clonedAvailableFlightSearchDTO.setPosAirport(this.getPosAirport());
		// clonedAvailableFlightSearchDTO.setReturnFlag(this.isReturnFlag());
		// clonedAvailableFlightSearchDTO.setStayOverMillis(this.getStayOverMillis());
		// clonedAvailableFlightSearchDTO.setToAirport(this.getToAirport());
		// clonedAvailableFlightSearchDTO.setBookingType(this.getBookingType());
		clonedAvailableFlightSearchDTO.setBookingPaxType(this.getBookingPaxType());
		clonedAvailableFlightSearchDTO.setFareCategoryType(this.getFareCategoryType());
		clonedAvailableFlightSearchDTO.setReturnValidityPeriodId(this.getReturnValidityPeriodId());
		clonedAvailableFlightSearchDTO.setConfirmOpenReturn(this.isConfirmOpenReturn());
		clonedAvailableFlightSearchDTO.setIncludeHRTFares(this.includeHRTFares);

		if (this.getOriginDestinationInfoDTOs() != null) {
			List<OriginDestinationInfoDTO> clonedOndInfos = new ArrayList<OriginDestinationInfoDTO>();
			for (OriginDestinationInfoDTO ondInfoTo : this.getOriginDestinationInfoDTOs()) {
				if (ondInfoTo != null) {
					clonedOndInfos.add(ondInfoTo.clone());
				} else {
					clonedOndInfos.add(null);
				}
			}
			clonedAvailableFlightSearchDTO.setOriginDestionationInfoDTOs(clonedOndInfos);
		}

		// clonedAvailableFlightSearchDTO.setDepatureDateTimeStart(this.getDepatureDateTimeStart());
		// clonedAvailableFlightSearchDTO.setDepatureDateTimeEnd(this.getDepatureDateTimeEnd());
		// clonedAvailableFlightSearchDTO.setReturnDateTimeStart(this.getDepatureDateTimeStart());
		// clonedAvailableFlightSearchDTO.setReturnDateTimeEnd(this.getDepatureDateTimeEnd());
		// clonedAvailableFlightSearchDTO.setSelectedDepatureDateTimeStart(this.getSelectedDepatureDateTimeStart());
		// clonedAvailableFlightSearchDTO.setSelectedDepatureDateTimeEnd(this.getSelectedDepatureDateTimeEnd());
		// clonedAvailableFlightSearchDTO.setSelectedReturnDateTimeStart(this.getSelectedReturnDateTimeStart());
		// clonedAvailableFlightSearchDTO.setSelectedReturnDateTimeEnd(this.getSelectedReturnDateTimeEnd());

		// clonedAvailableFlightSearchDTO.setFirstDepartureDateTimeZulu(getFirstDepartureDateTimeZulu());
		// clonedAvailableFlightSearchDTO.setLastArrivalDateTimeZulu(getLastArrivalDateTimeZulu());
		//
		clonedAvailableFlightSearchDTO.setModifiedOndFareId(this.getModifiedOndFareId());
		clonedAvailableFlightSearchDTO.setModifiedOndPaxFareAmount(this.getModifiedOndPaxFareAmount());
		clonedAvailableFlightSearchDTO.setModifiedOndFareType(this.getModifiedOndFareType());
		clonedAvailableFlightSearchDTO.setEnforceFareCheckForModOnd(this.getEnforceFareCheckForModOnd());
		clonedAvailableFlightSearchDTO.setModifiedFlightSegments(this.getModifiedFlightSegments());

		clonedAvailableFlightSearchDTO.setHasInwardCxnFlight(isHasInwardCxnFlight());
		clonedAvailableFlightSearchDTO.setHasOutwardCxnFlight(isHasOutwardCxnFlight());
		clonedAvailableFlightSearchDTO.setInterlineFareQuoted(isInterlineFareQuoted());

		clonedAvailableFlightSearchDTO.setHalfReturnFareQuote(this.isHalfReturnFareQuote());
		// clonedAvailableFlightSearchDTO.setModifiedOndReturnGroupId(this.getModifiedOndReturnGroupId());
		clonedAvailableFlightSearchDTO.setInboundFareQuote(this.isInboundFareQuote());
		clonedAvailableFlightSearchDTO.setInverseOndCode(this.getInverseOndCode());
		clonedAvailableFlightSearchDTO.setInverseFareID(this.getInverseFareID());

		// Shallow copy
		// clonedAvailableFlightSearchDTO.setInBoundFlights(this.getInBoundFlights());
		// clonedAvailableFlightSearchDTO.setOutBoundFlights(this.getOutBoundFlights());
		// clonedAvailableFlightSearchDTO.setExistingFlightSegments(this.getExistingFlightSegments());
		// clonedAvailableFlightSearchDTO.setModifiedFlightSegments(this.getModifiedFlightSegments());
		clonedAvailableFlightSearchDTO.setExternalBookingClasses(this.getExternalBookingClasses());
		clonedAvailableFlightSearchDTO.setExternalFareSummaryDTO(this.getExternalFareSummaryDTO());
		clonedAvailableFlightSearchDTO
				.setInverseSegmentsOfModifiedFlightSegment(this.getInverseSegmentsOfModifiedFlightSegment());

		clonedAvailableFlightSearchDTO.setOndQuoteFlexi(this.ondQuoteFlexi);
		clonedAvailableFlightSearchDTO.setOndSelectedFlexi(this.ondSelectedFlexi);
		clonedAvailableFlightSearchDTO.setInwardCxnFlightArriTime(this.getInwardCxnFlightArriTime());
		clonedAvailableFlightSearchDTO.setOutwardCxnFlightDepTime(this.getOutwardCxnFlightDepTime());

		clonedAvailableFlightSearchDTO.setSearchInitiatedFromInterline(this.isSearchInitiatedFromInterline());
		clonedAvailableFlightSearchDTO.setAppIndicator(this.getAppIndicator());

		clonedAvailableFlightSearchDTO.setExcludeConnectionFares(this.isExcludeConnectionFares());
		clonedAvailableFlightSearchDTO.setExcludeReturnFares(this.isExcludeReturnFares());

		clonedAvailableFlightSearchDTO.setFlightSegFareDetails(this.getFlightSegFareDetails());
		// clonedAvailableFlightSearchDTO.setCabinClassSelection(this.getCabinClassSelection());
		// clonedAvailableFlightSearchDTO.setLogicalCabinClassSelection(this.getLogicalCabinClassSelection());
		clonedAvailableFlightSearchDTO.setLastFareQuotedDate(this.getLastFareQuotedDate());
		clonedAvailableFlightSearchDTO.setFQWithinValidity(this.isFQWithinValidity());
		clonedAvailableFlightSearchDTO.setFQOnLastFQDate(this.isFQOnLastFQDate());
		clonedAvailableFlightSearchDTO.setFltSegWiseExtBookingClasses(this.fltSegExistingBkgClasses);
		clonedAvailableFlightSearchDTO.setFltSegWiseExtCabinClasses(this.fltSegExistingCabinClasses);
		clonedAvailableFlightSearchDTO.setFltSegWiseLastFQDates(this.fltSegLastFQDates);
		clonedAvailableFlightSearchDTO.setSearchAllBC(this.searchAllBC);
		clonedAvailableFlightSearchDTO.setSkipInvCheckForFlown(this.skipInvCheckForFlown);
		clonedAvailableFlightSearchDTO.setModifyBooking(this.isModifyBooking);
		clonedAvailableFlightSearchDTO.setExcludedCharges(this.excludedCharges);
		clonedAvailableFlightSearchDTO.setRequoteFlightSearch(this.requoteFlightSearch);
		clonedAvailableFlightSearchDTO.setOndChargeInfo(this.ondChargeInfo);
		clonedAvailableFlightSearchDTO.setTotalJourneyOrderedOndSEQMap(this.totalJourneyOrderedOndSEQMap);
		clonedAvailableFlightSearchDTO.setBundledFareApplicable(this.bundledFareApplicable);
		clonedAvailableFlightSearchDTO.setSkipSameFareBucketQuote(this.skipSameFareBucketQuote);
		clonedAvailableFlightSearchDTO.setAllowDoOverbookAfterCutOffTime(this.isAllowDoOverbookAfterCutOffTime());
		clonedAvailableFlightSearchDTO.setAllowDoOverbookBeforeCutOffTime(this.isAllowDoOverbookBeforeCutOffTime());
		clonedAvailableFlightSearchDTO.setPreferredLanguage(this.preferredLanguage);
		clonedAvailableFlightSearchDTO.setFromNameChange(this.fromNameChange);
		clonedAvailableFlightSearchDTO.setPointOfSale(this.getPointOfSale());
		clonedAvailableFlightSearchDTO.setMultiCitySearch(this.isMultiCitySearch());		

		clonedAvailableFlightSearchDTO.setQuoteReturnSegmentDiscount(this.isQuoteReturnSegmentDiscount());
		clonedAvailableFlightSearchDTO.setFareCalendarSearch(this.isFareCalendarSearch());
		clonedAvailableFlightSearchDTO.setPreserveOndOrder(this.isPreserveOndOrder());

		return clonedAvailableFlightSearchDTO;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		summary.append(nl + "--------Seat Availability/Fare Quote Criteria Summary--------" + nl);
		// summary.append("\t From airport = " + getFromAirport() + nl);
		// summary.append("\t To airport = " + getToAirport() + nl);
		// if (getFromAirportSubStation() != null) {
		// summary.append("\t From airport SubStation = " + getFromAirportSubStation() + nl);
		// }
		// if (getToAirportSubStation() != null) {
		// summary.append("\t To airport SubStation = " + getToAirportSubStation() + nl);
		// }
		summary.append("\t Adult count      = " + getAdultCount() + nl);
		summary.append("\t Child count      = " + getChildCount() + nl);
		summary.append("\t Infant count     = " + getInfantCount() + nl);
		// summary.append("\t Booking Code = " + getBookingClassCode() + nl);
		summary.append("\t OND code         = " + getOndCode() + nl);
		summary.append("\t Sales Channel    = " + getChannelCode() + nl);
		summary.append("\t Agent code       = " + getAgentCode() + nl);
		summary.append("\t Fixed Fare Only  = " + isFixedAgentFareOnly() + nl);
		summary.append("\t POS airport      = " + getPosAirport() + nl);

		// summary.append("\t DepatureDateTimeStart = " + getDepatureDateTimeStart() + nl);
		// summary.append("\t DepatureDateTimeEnd = " + getDepatureDateTimeEnd() + nl);
		// summary.append("\t DepatureDateTimeStart = " + getDepatureDateTimeStart() + nl);
		// summary.append("\t DepatureDateTimeEnd = " + getDepatureDateTimeEnd() + nl);
		// summary.append("\t SelectedDepatureDateTimeStart = " + getSelectedDepatureDateTimeStart() + nl);
		// summary.append("\t SelectedDepatureDateTimeEnd = " + getSelectedDepatureDateTimeEnd() + nl);
		// summary.append("\t SelectedReturnDateTimeStart = " + getSelectedReturnDateTimeStart() + nl);
		// summary.append("\t SelectedReturnDateTimeEnd = " + getSelectedReturnDateTimeEnd() + nl);

		// summary.append("\t Return flag = " + isReturnFlag() + nl);
		// summary.append("\t Stay over Millisecs = " + getStayOverMillis() + nl);

		summary.append("\t Restriction level			= " + getAvailabilityRestrictionLevel() + nl);
		summary.append("\t Flight per ond constraint 	= " + getFlightsPerOndRestriction() + nl);
		// summary.append("\t Booking type = " + getBookingType() + nl);
		summary.append("\t Booking pax type 	= " + getBookingPaxType() + nl);
		summary.append("\t Fare Category type 	= " + getFareCategoryType() + nl);

		// String obFlightSegIdsStr = null;
		// for (Integer flightSegId : getOutBoundFlights()) {
		// if (obFlightSegIdsStr == null) {
		// obFlightSegIdsStr = flightSegId.toString();
		// } else {
		// obFlightSegIdsStr += "," + flightSegId.toString();
		// }
		// }
		//
		// summary.append("\t Outbound fltSegIds = " + obFlightSegIdsStr + nl);
		//
		// String inFlightSegIdsStr = null;
		// for (Integer flightSegId : getInBoundFlights()) {
		// if (inFlightSegIdsStr == null) {
		// inFlightSegIdsStr = flightSegId.toString();
		// } else {
		// inFlightSegIdsStr += "," + flightSegId.toString();
		// }
		// }
		// summary.append("\t Inbound fltSegIds = " + inFlightSegIdsStr + nl);
		// summary.append("For modify/add segment fare quoting" + nl);
		// summary.append("\t First Departure(Zulu) = " + getFirstDepartureDateTimeZulu() + nl);
		// summary.append("\t Last Arrival (Zulu) = " + getLastArrivalDateTimeZulu() + nl);
		// summary.append("\t Modified Ond Pax Fare Id = " + getModifiedOndFareId() + nl);
		// summary.append("\t Modified Ond Pax Fare Amount = " + getModifiedOndPaxFareAmount() + nl);
		// summary.append("\t Modified Ond Fare Type = " + getModifiedOndFareType() + nl);

		String externalBookingClassesStr = null;
		if (getExternalBookingClasses() != null) {
			for (String externalBookingClass : getExternalBookingClasses()) {
				if (externalBookingClassesStr == null) {
					externalBookingClassesStr += externalBookingClass;
				} else {
					externalBookingClassesStr += "," + externalBookingClass;
				}
			}
		}
		summary.append("\t External booking classes = " + externalBookingClassesStr + nl);
		summary.append("\t Is interline fare quoted = " + isInterlineFareQuoted() + nl);

		if (getExternalFareSummaryDTO() != null) {
			summary.append("\t Is interline adult fare amount = " + getExternalFareSummaryDTO().getAdultFareAmount() + nl);
			summary.append("\t Is interline child fare amount = " + getExternalFareSummaryDTO().getChildFareAmount() + nl);
			summary.append("\t Is interline infant fare amount = " + getExternalFareSummaryDTO().getInfantFareAmount() + nl);
		}

		summary.append("\t Has inward cxn flight = " + isHasInwardCxnFlight() + nl);
		summary.append("\t Inward cxn flight Arrival time = " + getInwardCxnFlightArriTime() + nl);
		summary.append("\t Has outward cxn flight = " + getModifiedOndFareType() + nl);
		summary.append("\t outward cxn flight Departure time= " + getOutwardCxnFlightDepTime() + nl);
		if (getExcludedCharges() != null) {
			summary.append("\t Excluded Charges = " + getExcludedCharges() + nl);
		}

		summary.append("------------------------------------------------------------------" + nl);

		return summary;
	}

	@Deprecated
	public String getFromAirportSubStation() {
		return fromAirportSubStation;
	}

	@Deprecated
	public void setFromAirportSubStation(String fromAirportSubStation) {
		this.fromAirportSubStation = fromAirportSubStation;
	}

	@Deprecated
	public String getToAirportSubStation() {
		return toAirportSubStation;
	}

	@Deprecated
	public void setToAirportSubStation(String toAirportSubStation) {
		this.toAirportSubStation = toAirportSubStation;
	}

	public String getTravelAgent() {
		return travelAgent;
	}

	public void setTravelAgent(String travelAgent) {
		this.travelAgent = travelAgent;
	}

	public boolean isExcludeConnectionFares() {
		return isExcludeConnectionFares;
	}

	public void setExcludeConnectionFares(boolean isExcludeConnectionFares) {
		this.isExcludeConnectionFares = isExcludeConnectionFares;
	}

	public boolean isExcludeReturnFares() {
		return isExcludeReturnFares;
	}

	public void setExcludeReturnFares(boolean isExcludeReturnFares) {
		this.isExcludeReturnFares = isExcludeReturnFares;
	}

	@Deprecated
	public Date getArrivalDateTimeStart() {
		return arrivalDateTimeStart;
	}

	@Deprecated
	public void setArrivalDateTimeStart(Date arrivalDateTimeStart) {
		this.arrivalDateTimeStart = arrivalDateTimeStart;
	}

	@Deprecated
	public Date getArrivalDateTimeEnd() {
		return arrivalDateTimeEnd;
	}

	@Deprecated
	public void setArrivalDateTimeEnd(Date arrivalDateTimeEnd) {
		this.arrivalDateTimeEnd = arrivalDateTimeEnd;
	}

	public Map<Integer, FareIdDetailDTO> getFlightSegFareDetails() {
		return flightSegFareDetails;
	}

	public void setFlightSegFareDetails(Map<Integer, FareIdDetailDTO> flightSegFareDetails) {
		this.flightSegFareDetails = flightSegFareDetails;
	}

	public boolean isQuoteFares() {
		return quoteFares;
	}

	public void setQuoteFares(boolean quoteFares) {
		this.quoteFares = quoteFares;
	}

	public void setFQWithinValidity(boolean fqWithinValidity) {
		this.fqWithinValidity = fqWithinValidity;
	}

	public boolean isFQWithinValidity() {
		return fqWithinValidity;
	}

	public void setFQOnLastFQDate(boolean fqOnLastFQDate) {
		this.fqOnLastFQDate = fqOnLastFQDate;
	}

	public boolean isFQOnLastFQDate() {
		return fqOnLastFQDate;
	}

	/**
	 * set the last fare quoted date in zulu
	 * 
	 * @param lastFareQuotedDate
	 */
	public void setLastFareQuotedDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;
	}

	/**
	 * Return the last fare quoted date in zulu
	 * 
	 * @return
	 */
	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public Date getEffectiveLastFQDate(Integer fltSegId) {
		Date lastFQDate = getFltSegLastFQDate(fltSegId);
		if (lastFQDate == null) {
			if (isFQOnLastFQDate()) {
				lastFQDate = getLastFareQuotedDate();
			}
		}
		return lastFQDate;
	}

	public OriginDestinationInfoDTO getOndInfo(Integer ondSeqId) {
		if (getOrderedOriginDestinations() != null && getOrderedOriginDestinations().size() > ondSeqId) {
			return getOrderedOriginDestinations().get(ondSeqId);
		}
		return null;
	}

	public boolean isFirstOnd(Integer ondSeqId) {
		if (ondSeqId != null && ondSeqId == 0) {
			return true;
		}
		return false;
	}

	public boolean isLastOnd(Integer ondSeqId) {
		if (ondSeqId != null && ondSeqId == getOrderedOriginDestinations().size() - 1) {
			return true;
		}
		return false;
	}

	public JourneyType getJourneyType() {
		processOnds();
		return journeyType;
	}

	public void setFltSegWiseExtBookingClasses(Map<Integer, String> fltSegExistingBkgClasses) {
		this.fltSegExistingBkgClasses = fltSegExistingBkgClasses;
	}

	public Map<Integer, String> getFltSegWiseExtBookingClasses() {
		return fltSegExistingBkgClasses;
	}

	public String getBookedFlightBookingClass(Integer fltSegId) {
		if (fltSegExistingBkgClasses != null && fltSegExistingBkgClasses.containsKey(fltSegId)) {
			return fltSegExistingBkgClasses.get(fltSegId);
		}
		return null;
	}

	public void setFltSegWiseExtCabinClasses(Map<Integer, String> fltSegExistingCabinClasses) {
		this.fltSegExistingCabinClasses = fltSegExistingCabinClasses;
	}

	public Map<Integer, String> getFltSegWiseExtCabinClasses() {
		return fltSegExistingCabinClasses;
	}

	public String getBookedFlightCabinClass(Integer fltSegId) {
		if (fltSegExistingCabinClasses != null && fltSegExistingCabinClasses.containsKey(fltSegId)) {
			return fltSegExistingCabinClasses.get(fltSegId);
		}
		return null;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public void setFixedAgentFareOnly(boolean fixedFareAgentOnly) {
		this.fixedFareAgentOnly = fixedFareAgentOnly;
	}

	public boolean isFixedAgentFareOnly() {
		return fixedFareAgentOnly;
	}

	public boolean isSearchAllBC() {
		return searchAllBC;
	}

	public void setSearchAllBC(boolean searchAllBC) {
		this.searchAllBC = searchAllBC;
	}

	public void setFltSegWiseLastFQDates(Map<Integer, Date> fltSegLastFQDates) {
		this.fltSegLastFQDates = fltSegLastFQDates;
	}

	public Map<Integer, Date> getFltSegWiseLastFQDates() {
		return fltSegLastFQDates;
	}

	private Date getFltSegLastFQDate(Integer fltSegId) {
		if (fltSegLastFQDates != null && fltSegId != null && fltSegLastFQDates.containsKey(fltSegId)) {
			return fltSegLastFQDates.get(fltSegId);
		}
		return null;
	}

	public void setAllowFlightSearchAfterCutOffTime(boolean allowFlightSearchAfterCutOffTime) {
		this.allowFlightSearchAfterCutOffTime = allowFlightSearchAfterCutOffTime;
	}

	public boolean isAllowFlightSearchAfterCutOffTime() {
		return allowFlightSearchAfterCutOffTime;
	}

	public boolean isSkipInvCheckForFlown() {
		return skipInvCheckForFlown;
	}

	public void setSkipInvCheckForFlown(boolean skipInvCheckForFlown) {
		this.skipInvCheckForFlown = skipInvCheckForFlown;
	}

	public boolean isMultiCitySearch() {
		return multiCitySearch;
	}

	public void setMultiCitySearch(boolean multiCitySearch) {
		this.multiCitySearch = multiCitySearch;
	}

	public Date getFirstInBoundDate() {
		return firstInBoundDeparDate;
	}

	public void setFirstInBoundDate(Date firstInBoundDate) {
		this.firstInBoundDeparDate = firstInBoundDate;
	}

	public Map<Integer, Boolean> getOndSequenceReturnMap() {
		List<OriginDestinationInfoDTO> orderedOndList = getOrderedOriginDestinations();
		if (orderedOndList != null && orderedOndList.size() > 1) {
			Boolean isReturnOnd = false;
			int countinousSectors = 1;
			OriginDestinationInfoDTO prevOndInfo = null;

			for (int i = 0; i < orderedOndList.size(); i++) {
				OriginDestinationInfoDTO ondInfoDTO = orderedOndList.get(i);
				isReturnOnd = false;
				if (prevOndInfo != null) {
					if (!prevOndInfo.getDestination().equals(ondInfoDTO.getOrigin())) {
						countinousSectors++;
					}
				}

				if (JourneyType.OPEN_JAW == getJourneyType()) {
					if (countinousSectors == 2) {
						isReturnOnd = true;
					} else if (orderedOndList.size() == 2 && countinousSectors == 1 && i == 1) {
						isReturnOnd = true;
					}

				} else if (JourneyType.CIRCULAR == getJourneyType() && countinousSectors == 1
						&& ((orderedOndList.size() - 1) == i)) {
					isReturnOnd = true;
				} else if (JourneyType.ROUNDTRIP == getJourneyType()) {
					if (orderedOndList.size() == 2 && countinousSectors == 1 && i == 1) {
						isReturnOnd = true;
					}
				}

				prevOndInfo = ondInfoDTO;
				ondSequenceReturnMap.put(i, isReturnOnd);
			}

		}

		return ondSequenceReturnMap;
	}

	private void processValidOpenJawJorney(boolean isJouneryStartAndEndAtSamePoint) {
		boolean inBoundAllDomestic = true;
		boolean outBoundAllDomestic = true;
		boolean allDomestic = true;
		boolean isJourneyStartAndEndAtSameCountry = false;
		boolean secondSectorStartAtSamePoint = false;
		boolean isOverlapAirportExist = false;

		if (originDestinationInfoList != null && originDestinationInfoList.size() >= 2) {

			int ondListSize = originDestinationInfoList.size();
			int countinousSectors = 1;
			OriginDestinationInfoDTO prevOnd = null;
			OriginDestinationInfoDTO outBoundLastOnd = null;
			OriginDestinationInfoDTO inBoundFirstOnd = null;
			int count = 1;

			String startCountry = originDestinationInfoList.get(0).getOrigin();
			String endCountry = originDestinationInfoList.get(ondListSize - 1).getDestination();
			Object[] startOndAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(startCountry, false);
			Object[] endOndAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(endCountry, false);

			if (startOndAirportInfo != null && endOndAirportInfo != null) {
				isJourneyStartAndEndAtSameCountry = startOndAirportInfo[3].equals(endOndAirportInfo[3]);
			}
			List<String> connectingPorts = new ArrayList<String>();
			boolean isSameAirportExist = ReservationApiUtils.processFlightsConnectionAirports(originDestinationInfoList,
					connectingPorts);

			if (!isSameAirportExist) {
				for (OriginDestinationInfoDTO ondInfo : originDestinationInfoList) {

					Object[] originAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(ondInfo.getOrigin(), false);
					Object[] destinationOndAirportInfo = CommonsServices.getGlobalConfig()
							.getAirportInfo(ondInfo.getDestination(), false);
					if (originAirportInfo != null && destinationOndAirportInfo != null && originAirportInfo[3] != null
							&& destinationOndAirportInfo[3] != null) {
						if (!originAirportInfo[3].equals(destinationOndAirportInfo[3])) {
							if (countinousSectors == 1) {
								outBoundAllDomestic = false;
							}
							allDomestic = false;
						}

						if (prevOnd != null) {
							if (!ondInfo.getOrigin().equals(prevOnd.getDestination())) {
								countinousSectors++;
								outBoundLastOnd = prevOnd;
								inBoundFirstOnd = ondInfo;
							}
						}
						prevOnd = ondInfo;

						if (connectingPorts.contains(ondInfo.getOrigin()) || connectingPorts.contains(ondInfo.getDestination())) {
							isOverlapAirportExist = true;
						}

						if (countinousSectors == 2) {
							// this is required to set the 1st departure of in-bound when journey type is open jaw.
							if (ondInfo.getPreferredDate() != null && !ondInfo.isSpecificFlightsAvailability()) {
								setFirstInBoundDate(ondInfo.getPreferredDate());
							} else {
								setFirstInBoundDate(ondInfo.getPreferredDateTimeEnd());
							}

							if (!originAirportInfo[3].equals(destinationOndAirportInfo[3])) {
								inBoundAllDomestic = false;
							}

							if (ondInfo.getOrigin().equals(originDestinationInfoList.get(0).getOrigin())) {
								secondSectorStartAtSamePoint = true;
							}

						} else if (ondListSize == 2 && countinousSectors == 1 && count == 2) {
							if (!originAirportInfo[3].equals(destinationOndAirportInfo[3])) {
								inBoundAllDomestic = false;
							}
						}
					}
					count++;
				}
			}

			boolean returningSameCountryArray[] = ReservationApiUtils.isOpenJawReturningFromSameCountry(outBoundLastOnd,
					inBoundFirstOnd);

			if (!isOverlapAirportExist && !allDomestic && !inBoundAllDomestic && !outBoundAllDomestic) {
				if (ondListSize == 2 && countinousSectors == 1 && !isJouneryStartAndEndAtSamePoint
						&& isJourneyStartAndEndAtSameCountry) {
					journeyType = JourneyType.OPEN_JAW;
				} else if (!secondSectorStartAtSamePoint && isJouneryStartAndEndAtSamePoint && countinousSectors == 2
						&& returningSameCountryArray[0]) {
					journeyType = JourneyType.OPEN_JAW;
				} else if (ondListSize == 2 && countinousSectors == 2 && !isJouneryStartAndEndAtSamePoint
						&& isJourneyStartAndEndAtSameCountry && returningSameCountryArray[1]) {
					journeyType = JourneyType.OPEN_JAW;
				}
			}

			if (countinousSectors == 1 && ondListSize == 2) {
				setFirstInBoundDate(originDestinationInfoList.get(1).getPreferredDateTimeEnd());
			}

			if (journeyType != JourneyType.OPEN_JAW) {
				setFirstInBoundDate(null);
			}
		}
	}

	/**
	 * @return the isModifyBooking
	 */
	public boolean isModifyBooking() {
		return isModifyBooking;
	}

	/**
	 * @param isModifyBooking
	 *            the isModifyBooking to set
	 */
	public void setModifyBooking(boolean isModifyBooking) {
		this.isModifyBooking = isModifyBooking;
	}

	/**
	 * Method to check if the return fare segment related to this flight search object is an open return segment.
	 * 
	 * @return true if a return fare segment(inverse segment) exist and it is a open return segment. Otherwise return
	 *         false.
	 */
	public boolean isInverseSegmentOpenReturn() {
		boolean isOpenRT = false;
		if (inverseSegmentsOfModifiedFlightSegment != null) {
			for (FlightSegmentDTO flightSegment : inverseSegmentsOfModifiedFlightSegment) {
				if (flightSegment.isOpenReturnSegment()) {
					isOpenRT = true;
					break;
				}
			}
		}
		return isOpenRT;
	}

	public List<String> getExcludedCharges() {
		return excludedCharges;
	}

	public void setExcludedCharges(List<String> excludedCharges) {
		this.excludedCharges = excludedCharges;
	}

	/**
	 * @return the goshoFareAgentOnly
	 */
	public boolean isGoshoFareAgentOnly() {
		return goshoFareAgentOnly;
	}

	/**
	 * @param goshoFareAgentOnly
	 *            the goshoFareAgentOnly to set
	 */
	public void setGoshoFareAgentOnly(boolean goshoFareAgentOnly) {
		this.goshoFareAgentOnly = goshoFareAgentOnly;
	}

	public boolean isInboundOutboundChanged() {
		return isInboundOutboundChanged;
	}

	public void setInboundOutboundChanged(boolean isInboundOutboundChanged) {
		this.isInboundOutboundChanged = isInboundOutboundChanged;
	}

	public boolean isOutBoundModifiedToAFutureDepartureDateThanInbound() {
		return isOutBoundModifiedToAFutureDepartureDateThanInbound;
	}

	public void setOutBoundModifiedToAFutureDepartureDateThanInbound(boolean isOutBoundModifiedToAFutureDepartureDateThanInbound) {
		this.isOutBoundModifiedToAFutureDepartureDateThanInbound = isOutBoundModifiedToAFutureDepartureDateThanInbound;
	}

	public boolean isPromoApplicable() {
		return promoApplicable;
	}

	public void setPromoApplicable(boolean promoApplicable) {
		this.promoApplicable = promoApplicable;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Integer getBankIdentificationNo() {
		return bankIdentificationNo;
	}

	public void setBankIdentificationNo(Integer bankIdentificationNo) {
		this.bankIdentificationNo = bankIdentificationNo;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public boolean isHasPrivAddSeatsInCutover() {
		return hasPrivAddSeatsInCutover;
	}

	public void setHasPrivAddSeatsInCutover(boolean hasPrivAddSeatsInCutover) {
		this.hasPrivAddSeatsInCutover = hasPrivAddSeatsInCutover;
	}

	public boolean isAllowTillFinalCutOver() {
		return isAllowTillFinalCutOver;
	}

	public void setAllowTillFinalCutOver(boolean isAllowTillFinalCutOver) {
		this.isAllowTillFinalCutOver = isAllowTillFinalCutOver;
	}

	/**
	 * @return the flightSearchGap
	 */
	public Integer getFlightSearchGap() {
		return flightSearchGap;
	}

	/**
	 * @param flightSearchGap
	 *            the flightSearchGap to set
	 */
	public void setFlightSearchGap(Integer flightSearchGap) {
		this.flightSearchGap = flightSearchGap;
	}

	public void setOndChargeInfo(Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> ondChargeInfo) {
		this.ondChargeInfo = ondChargeInfo;
	}

	public Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> getOndChargeInfo() {
		return ondChargeInfo;
	}

	public PaxTypewiseFareONDChargeInfo getPaxTypewiseFareOndChargeInfo(Integer ondSequence, String ondCode) {
		if (ondChargeInfo != null && ondChargeInfo.containsKey(ondSequence)) {
			if (ondChargeInfo.get(ondSequence).containsKey(ondCode)) {
				return ondChargeInfo.get(ondSequence).get(ondCode);
			}
		}
		return new PaxTypewiseFareONDChargeInfo();
	}

	public Map<Double, List<Integer>> getFlightDateFareMap() {
		return flightDateFareMap;
	}

	public void setFlightDateFareMap(Map<Double, List<Integer>> flightDateFareMap) {
		this.flightDateFareMap = flightDateFareMap;
	}

	public Map<Integer, List<Integer>> getExistRetGropMap() {
		return existRetGropMap;
	}

	public void setExistRetGropMap(Map<Integer, List<Integer>> existRetGropMap) {
		this.existRetGropMap = existRetGropMap;
	}

	public AvailableFlightSearchDTO deepCopy(List<OriginDestinationInfoDTO> groupOndList) {
		AvailableFlightSearchDTO deepCopyAvailFltSearchDTO = clone();
		deepCopyAvailFltSearchDTO.setOriginDestionationInfoDTOs(groupOndList);
		return deepCopyAvailFltSearchDTO;
	}

	public boolean isQuoteSameFareBucket() {

		if (!skipSameFareBucketQuote && requoteFlightSearch && AppSysParamsUtil.isReQuoteOnlyRequiredONDs()
				&& originDestinationInfoList != null && originDestinationInfoList.size() > 0) {

			boolean isRetCircularJourney = false;
			boolean isNewOndExist = false;
			boolean flownOndExist = false;
			boolean isRetFareType = false;
			boolean isAllUnTouched = true;
			boolean isNonRetFareType = false;
			boolean isQuoteFullOnd = false;
			boolean isDateModification = false;
			boolean isUnTouchedONDExist = false;
			List<Integer> removeFltSegIdFares = new ArrayList<Integer>();

			if (this.getJourneyType() == JourneyType.CIRCULAR || this.getJourneyType() == JourneyType.ROUNDTRIP
					|| this.getJourneyType() == JourneyType.OPEN_JAW) {
				isRetCircularJourney = true;
			}

			for (OriginDestinationInfoDTO orgDestInfoDTO : originDestinationInfoList) {
				isDateModification = false;

				if (orgDestInfoDTO.isUnTouchedOnd() || orgDestInfoDTO.isFlownOnd()) {
					isUnTouchedONDExist = true;
				}

				if (orgDestInfoDTO.getOldPerPaxFareTO() != null) {
					if (ReservationApiUtils.isRetOrHalfRetFareType(orgDestInfoDTO.getOldPerPaxFareTO().getFareType())) {
						isRetFareType = true;
						if (!isRetCircularJourney) {
							// orgDestInfoDTO.setOldPerPaxFareTO(null);
							removeFltSegIdFares.addAll(orgDestInfoDTO.getExistingFlightSegIds());
						}
					} else {
						isNonRetFareType = true;
					}

				}

				if (orgDestInfoDTO.isFlownOnd()) {
					flownOndExist = true;
				}

				if (isAllUnTouched && !orgDestInfoDTO.isUnTouchedOnd()) {
					isAllUnTouched = false;
				}

				if (!orgDestInfoDTO.isUnTouchedOnd() && orgDestInfoDTO.getOldPerPaxFareTO() != null
						&& orgDestInfoDTO.getOldPerPaxFareTO().getAppliedFlightSegId() != null) {
					isDateModification = true;
				}

				if (!isDateModification && (orgDestInfoDTO.getExistingFlightSegIds() == null
						|| orgDestInfoDTO.getExistingFlightSegIds().isEmpty())) {
					if (!orgDestInfoDTO.isGroundSegmentOnly()) {
						isNewOndExist = true;
					}
				}

			}

			// if ((!flownOndExist && isNewOndExist)) {
			// isQuoteFullOnd = true;
			// for (OriginDestinationInfoDTO orgDestInfoDTO : originDestinationInfoList) {
			// if (orgDestInfoDTO.getOldPerPaxFareTO() != null) {
			// // orgDestInfoDTO.setOldPerPaxFareTO(null);
			// removeFltSegIdFares.addAll(orgDestInfoDTO.getExistingFlightSegIds());
			// }
			// }
			// }

			if (removeFltSegIdFares != null && removeFltSegIdFares.size() > 0 && flightSegFareDetails != null
					&& flightSegFareDetails.size() > 0) {
				flightSegFareDetails.keySet().removeAll(removeFltSegIdFares);
			}

			if ((!isRetCircularJourney && isRetFareType) || (isNewOndExist && (!isUnTouchedONDExist || !flownOndExist))) {
				return false;
			} else {
				return true;
			}

			// if (isRetCircularJourney && isRetFareType && isAllUnTouched) {
			// return true;
			// } else if (isRetFareType && !isAllUnTouched) {
			// return false;
			// } else if (!isRetFareType && isNonRetFareType && !isQuoteFullOnd) {
			// return true;
			// }

		}

		return false;
	}

	public boolean isInBoundOnd(Integer ondSequence) {

		Map<Integer, Boolean> ondSequenceReturnMap = getOndSequenceReturnMap();

		if (ondSequenceReturnMap != null && ondSequenceReturnMap.size() > 1 && ondSequenceReturnMap.get(ondSequence) != null) {
			return ondSequenceReturnMap.get(ondSequence);
		}

		return false;
	}

	public Map<List<Integer>, Integer> getTotalJourneyOrderedOndSEQMap() {
		return totalJourneyOrderedOndSEQMap;
	}

	public void setTotalJourneyOrderedOndSEQMap(Map<List<Integer>, Integer> totalJourneyOrderedOndSEQMap) {
		this.totalJourneyOrderedOndSEQMap = totalJourneyOrderedOndSEQMap;
	}

	public boolean isSubJourney() {
		return isSubJourney;
	}

	public void setSubJourney(boolean isSubJourney) {
		this.isSubJourney = isSubJourney;
	}

	public boolean isAllowDoOverbookAfterCutOffTime() {
		return allowDoOverbookAfterCutOffTime;
	}

	public void setAllowDoOverbookAfterCutOffTime(boolean allowDoOverbookAfterCutOffTime) {
		this.allowDoOverbookAfterCutOffTime = allowDoOverbookAfterCutOffTime;
	}

	public boolean isAllowDoOverbookBeforeCutOffTime() {
		return allowDoOverbookBeforeCutOffTime;
	}

	public void setAllowDoOverbookBeforeCutOffTime(boolean allowDoOverbookBeforeCutOffTime) {
		this.allowDoOverbookBeforeCutOffTime = allowDoOverbookBeforeCutOffTime;
	}

	public boolean isBundledFareApplicable() {
		return bundledFareApplicable;
	}

	public void setBundledFareApplicable(boolean bundledFareApplicable) {
		this.bundledFareApplicable = bundledFareApplicable;
	}

	public boolean isSkipSameFareBucketQuote() {
		return skipSameFareBucketQuote;
	}

	public void setSkipSameFareBucketQuote(boolean skipSameFareBucketQuote) {
		this.skipSameFareBucketQuote = skipSameFareBucketQuote;
	}

	public boolean isReturnOnd(int ondSequence) {
		if ((getJourneyType() == JourneyType.ROUNDTRIP || getJourneyType() == JourneyType.CIRCULAR)
				&& getOriginDestinationInfoDTOs().size() - 1 == ondSequence) {
			return true;
		} else if (getJourneyType() == JourneyType.OPEN_JAW && getOndSequenceReturnMap() != null
				&& getOndSequenceReturnMap().size() > 1 && getOndSequenceReturnMap().get(ondSequence)) {
			return true;
		}
		return false;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isFlownOnDExist() {
		return flownOnDExist;
	}

	public boolean isFromNameChange() {
		return fromNameChange;
	}

	public void setFromNameChange(boolean fromNameChange) {
		this.fromNameChange = fromNameChange;
	}

	public boolean isSourceFlightInCutOffTime() {
		return isSourceFlightInCutOffTime;
	}

	public void setSourceFlightInCutOffTime(boolean isSourceFlightInCutOffTime) {
		this.isSourceFlightInCutOffTime = isSourceFlightInCutOffTime;
	}

	public boolean isFareCalendarSearch() {
		return fareCalendarSearch;
	}

	public void setFareCalendarSearch(boolean fareCalendarSearch) {
		this.fareCalendarSearch = fareCalendarSearch;
	}

	public boolean isQuoteReturnSegmentDiscount() {
		return isQuoteReturnSegmentDiscount;
	}

	public void setQuoteReturnSegmentDiscount(boolean isQuoteReturnSegmentDiscount) {
		this.isQuoteReturnSegmentDiscount = isQuoteReturnSegmentDiscount;
	}

	public void setPreserveOndOrder(boolean preserveOndOrder) {
		this.preserveOndOrder = preserveOndOrder;
	}

	public boolean isPreserveOndOrder() {
		return preserveOndOrder;
	}

	public boolean isOndSplitOperationInvoked() {
		return ondSplitOperationInvoked;
	}

	public void setOndSplitOperationInvoked(boolean ondSplitOperationInvoked) {
		this.ondSplitOperationInvoked = ondSplitOperationInvoked;
	}

	public boolean isOwnSearch() {
		return ownSearch;
	}

	public void setOwnSearch(boolean isOwnSearch) {
		this.ownSearch = isOwnSearch;
	}
	
	
	public boolean isIncludeHRTFares() {
		return includeHRTFares;
	}

	public void setIncludeHRTFares(boolean includeHRTFares) {
		this.includeHRTFares = includeHRTFares;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	
}