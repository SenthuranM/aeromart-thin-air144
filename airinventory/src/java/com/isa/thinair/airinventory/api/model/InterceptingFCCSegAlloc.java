package com.isa.thinair.airinventory.api.model;

/**
 * 
 * @author MN
 * @hibernate.class table = "T_INTERCEPTING_FCC_SEG_ALLOC"
 */
public class InterceptingFCCSegAlloc {

	private int id;

	private int sourceFCCSegInvId;

	private int interceptingFCCSegInvId;

	/**
	 * @hibernate.id column = "IFCCSA_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_INTERCEPTING_FCC_SEG_ALLOC"
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @hibernate.property column = "SOURCE_FCCSA_ID"
	 */
	public int getInterceptingFCCSegInvId() {
		return interceptingFCCSegInvId;
	}

	public void setInterceptingFCCSegInvId(int interceptingFCCSegInvId) {
		this.interceptingFCCSegInvId = interceptingFCCSegInvId;
	}

	/**
	 * @hibernate.property column = "INTETERCEPTING_FCCSA_ID"
	 */
	public int getSourceFCCSegInvId() {
		return sourceFCCSegInvId;
	}

	public void setSourceFCCSegInvId(int sourceFCCSegInvId) {
		this.sourceFCCSegInvId = sourceFCCSegInvId;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof InterceptingFCCSegAlloc)) {
			return false;
		}
		InterceptingFCCSegAlloc interceptingFCCSegAlloc = (InterceptingFCCSegAlloc) object;
		return getSourceFCCSegInvId() == interceptingFCCSegAlloc.getSourceFCCSegInvId()
				&& getInterceptingFCCSegInvId() == interceptingFCCSegAlloc.getInterceptingFCCSegInvId();
	}

	@Override
	public int hashCode() {
		return 63 * getSourceFCCSegInvId() + 31 * getInterceptingFCCSegInvId();
	}
}
