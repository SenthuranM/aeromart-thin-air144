package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.commons.api.dto.CabinClassDTO;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * @author eric
 * 
 */
public class FLCCAllocationDTO implements Serializable, Comparable<FLCCAllocationDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cabinClassCode;

	private String logicalCabinClassCode;

	private int adultAllocation;

	private int infantAllocation;

	private int actualAdultCapacity;

	private int actualInfantCapacity;

	private Integer logicalCCRank;

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public int getAdultAllocation() {
		return adultAllocation;
	}

	public void setAdultAllocation(int adultAllocation) {
		this.adultAllocation = adultAllocation;
	}

	public int getInfantAllocation() {
		return infantAllocation;
	}

	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	public int getActualAdultCapacity() {
		return actualAdultCapacity;
	}

	public void setActualAdultCapacity(int actualAdultCapacity) {
		this.actualAdultCapacity = actualAdultCapacity;
	}

	public int getActualInfantCapacity() {
		return actualInfantCapacity;
	}

	public void setActualInfantCapacity(int actualInfantCapacity) {
		this.actualInfantCapacity = actualInfantCapacity;
	}

	public Integer getLogicalCCRank() {
		return logicalCCRank;
	}

	public void setLogicalCCRank(Integer logicalCCRank) {
		this.logicalCCRank = logicalCCRank;
	}

	@Override
	public int compareTo(FLCCAllocationDTO flccAllocationDTO) {
		Map<String, CabinClassDTO> activeCabinClassDTOs = CommonsServices.getGlobalConfig().getActiveCabinClassDTOMap();
		int comparedAmount = activeCabinClassDTOs.get(cabinClassCode).getRank()
				.compareTo(activeCabinClassDTOs.get(flccAllocationDTO.getCabinClassCode()).getRank());
		if (comparedAmount == 0) {
			return this.getLogicalCCRank().compareTo(flccAllocationDTO.getLogicalCCRank());
		}
		return comparedAmount;
	}
}
