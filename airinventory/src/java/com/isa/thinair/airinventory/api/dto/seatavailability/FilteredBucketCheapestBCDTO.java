package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

/**
 * Holds the filtered cheapest Buckets booking class info
 * 
 * @author M.Rikaz
 */
public class FilteredBucketCheapestBCDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String standardBookingCode;
	private String nonStandardBookingCode;
	private String fixedBookingCode;

	public String getStandardBookingCode() {
		return standardBookingCode;
	}

	public void setStandardBookingCode(String standardBookingCode) {
		this.standardBookingCode = standardBookingCode;
	}

	public String getNonStandardBookingCode() {
		return nonStandardBookingCode;
	}

	public void setNonStandardBookingCode(String nonStandardBookingCode) {
		this.nonStandardBookingCode = nonStandardBookingCode;
	}

	public String getFixedBookingCode() {
		return fixedBookingCode;
	}

	public void setFixedBookingCode(String fixedBookingCode) {
		this.fixedBookingCode = fixedBookingCode;
	}

}
