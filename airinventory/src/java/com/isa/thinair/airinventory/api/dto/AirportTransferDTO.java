package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq.ReservationFlow;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;

public class AirportTransferDTO implements Serializable {

	private List<FlightSegmentTO> flightSegmentTOs;
	private Map<String, String> classesOfService;
	private boolean checkCutoverTime;
	private Map<String, String> bookingClasses;
	private String agent;
	private boolean isRequote;
	private ReservationFlow reservationFlow;
	private boolean ownReservation; 
	private int salesChannel;
}
