package com.isa.thinair.airinventory.api.dto.baggage;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ReservationBaggageTo {

	private Map<Integer, List<FlightBaggageDTO>> baggage;
	private boolean criterionSatisfied;

	public ReservationBaggageTo() {
		baggage = new LinkedHashMap<Integer, List<FlightBaggageDTO>>();
		criterionSatisfied = false;
	}

	public Map<Integer, List<FlightBaggageDTO>> getBaggage() {
		return baggage;
	}

	public void putBaggage(Integer flightSegment, List<FlightBaggageDTO> bg) {
		baggage.put(flightSegment, bg);
	}

	public boolean isCriterionSatisfied() {
		return criterionSatisfied;
	}

	public void setCriterionSatisfied(boolean criterionSatisfied) {
		this.criterionSatisfied = criterionSatisfied;
	}
}
