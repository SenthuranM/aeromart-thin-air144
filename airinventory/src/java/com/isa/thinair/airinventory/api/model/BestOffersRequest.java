package com.isa.thinair.airinventory.api.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @hibernate.class table = "T_BEST_OFFERS_REQUEST"
 * 
 */

public class BestOffersRequest {

	private int bestOffersRequestId;

	private String countryCode;

	private String summaryFlag;

	private String totalQuoteFlag;

	private String origin;

	private String destination;

	private Integer monthOfTravel;

	private Date lastRequestTime;

	private Set<BestOffersResponse> bestOffersResponses;

	/**
	 * @return the bestOffersRequestId
	 * @hibernate.id column = "BEST_OFFERS_REQUEST_ID" generator-class = "assigned"
	 */
	public int getBestOffersRequestId() {
		return this.bestOffersRequestId;
	}

	public void setBestOffersRequestId(int bestOffersRequestId) {
		this.bestOffersRequestId = bestOffersRequestId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "SUMMARY_FLAG"
	 */
	public String getSummaryFlag() {
		return this.summaryFlag;
	}

	public void setSummaryFlag(String summaryFlag) {
		this.summaryFlag = summaryFlag;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return this.origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MONTH_OF_TRAVEL"
	 */
	public Integer getMonthOfTravel() {
		return this.monthOfTravel;
	}

	public void setMonthOfTravel(Integer monthOfTravel) {
		this.monthOfTravel = monthOfTravel;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LAST_REQUEST_TIME"
	 */

	public Date getLastRequestTime() {
		return this.lastRequestTime;
	}

	public void setLastRequestTime(Date lastRequestTime) {
		this.lastRequestTime = lastRequestTime;
	}

	/**
	 * @return Returns the bestOffersResponses.
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="BEST_OFFERS_REQUEST_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airinventory.api.model.BestOffersResponse"
	 */
	public Set<BestOffersResponse> getBestOffersResponses() {
		return bestOffersResponses;
	}

	public void setBestOffersResponses(Set<BestOffersResponse> bestOffersResponses) {
		this.bestOffersResponses = bestOffersResponses;
	}

	/**
	 * Add best Offers Response
	 * 
	 * @param bestOffersResponse
	 */
	public void addBestOffersResponse(BestOffersResponse bestOffersResponse) {
		if (this.getBestOffersResponses() == null) {
			this.setBestOffersResponses(new HashSet<BestOffersResponse>());
		}

		bestOffersResponse.setBestOffersRequestId(this.getBestOffersRequestId());
		this.getBestOffersResponses().add(bestOffersResponse);
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TOTAL_QUOTE_FLAG"
	 */
	public String getTotalQuoteFlag() {
		return totalQuoteFlag;
	}

	public void setTotalQuoteFlag(String totalQuoteFlag) {
		this.totalQuoteFlag = totalQuoteFlag;
	}
}
