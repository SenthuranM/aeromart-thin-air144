package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

public class TransferSeatDTO implements Serializable {

	private static final long serialVersionUID = 1470021406917097939L;

	private int targetFlightId;

	private String targetSegCode;

	private String cabinClassCode;

	private String logicalCCCode;

	private String targetCabinClassCode;
	
	private int targetFlightSegId;	
	
	private boolean cabinClassChanged;	
	
	Collection<TransferSeatDTO> connectingSeatDTOs;

	private boolean directToConnection;

	private boolean skipStandbyBookings;

	/** Holds a collection of TransferSeatList */
	@SuppressWarnings("rawtypes")
	private Collection sourceFltSegSeatDists;

	/**
	 * 
	 * @return
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * 
	 * @param cabinClassCode
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	@SuppressWarnings("rawtypes")
	public Collection getSourceFltSegSeatDists() {
		return sourceFltSegSeatDists;
	}

	@SuppressWarnings("rawtypes")
	public void setSourceFltSegSeatDists(Collection sourceFltSegSeatDists) {
		this.sourceFltSegSeatDists = sourceFltSegSeatDists;
	}

	/**
	 * 
	 * @return
	 */
	public String getTargetSegCode() {
		return targetSegCode;
	}

	/**
	 * 
	 * @param targetSegCode
	 */
	public void setTargetSegCode(String targetSegCode) {
		this.targetSegCode = targetSegCode;
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public int calculateTotalSeatCount() {

		int total = 0;

		if (sourceFltSegSeatDists != null) {

			Iterator itDist = sourceFltSegSeatDists.iterator();
			while (itDist.hasNext()) {
				TransferSeatList transferSeatList = (TransferSeatList) itDist.next();
				total = total + transferSeatList.getTransferSeatCount();
			}
		}

		return total;
	}

	public boolean isCabinClassChanged() {
		return cabinClassChanged;
	}

	public void setCabinClassChanged(boolean cabinClassChanged) {
		this.cabinClassChanged = cabinClassChanged;
	}

	public int getTargetFlightId() {
		return targetFlightId;
	}

	public void setTargetFlightId(int targetFlightId) {
		this.targetFlightId = targetFlightId;
	}

	public int getTargetFlightSegId() {
		return targetFlightSegId;
	}

	public void setTargetFlightSegId(int targetFlightSegId) {
		this.targetFlightSegId = targetFlightSegId;
	}

	public Collection<TransferSeatDTO> getConnectingSeatDTOs() {
		return connectingSeatDTOs;
	}

	public void setConnectingSeatDTOs(Collection<TransferSeatDTO> connectingSeatDTOs) {
		this.connectingSeatDTOs = connectingSeatDTOs;
	}

	public boolean isDirectToConnection() {
		return directToConnection;
	}

	public void setDirectToConnection(boolean directToConnection) {
		this.directToConnection = directToConnection;
	}

	public String getTargetCabinClassCode() {
		return targetCabinClassCode;
	}

	public void setTargetCabinClassCode(String targetCabinClassCode) {
		this.targetCabinClassCode = targetCabinClassCode;
	}

	public boolean isSkipStandbyBookings() {
		return skipStandbyBookings;
	}

	public void setSkipStandbyBookings(boolean skipStandbyBookings) {
		this.skipStandbyBookings = skipStandbyBookings;
	}

	@Override
	public String toString() {
		return "TransferSeatDTO [targetFlightId=" + targetFlightId + ", targetSegCode=" + targetSegCode + ", cabinClassCode="
				+ cabinClassCode + ", logicalCCCode=" + logicalCCCode + ", targetCabinClassCode=" + targetCabinClassCode
				+ ", targetFlightSegId=" + targetFlightSegId + ", cabinClassChanged=" + cabinClassChanged + "]";
	}
	
	
	
}
