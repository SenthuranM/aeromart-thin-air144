/**
 * 
 */
package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * @author Janaka Padukka
 *
 */
public class PaxTypewiseFareONDChargeInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Map<String, FareONDChargeInfo> paxTypewiseFareONDChargeInfo;
	
	public PaxTypewiseFareONDChargeInfo() {
		paxTypewiseFareONDChargeInfo = new HashMap<String, FareONDChargeInfo>();
		paxTypewiseFareONDChargeInfo.put(PaxTypeTO.ADULT, new FareONDChargeInfo());
		paxTypewiseFareONDChargeInfo.put(PaxTypeTO.CHILD, new FareONDChargeInfo());
		paxTypewiseFareONDChargeInfo.put(PaxTypeTO.INFANT, new FareONDChargeInfo());
	}
		
	public FareONDChargeInfo getFareONDChargeInfoForPaxType(String paxType){
		return paxTypewiseFareONDChargeInfo.get(paxType);
	}
	
	
}
