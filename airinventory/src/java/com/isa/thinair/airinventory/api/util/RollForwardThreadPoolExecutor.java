package com.isa.thinair.airinventory.api.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RollForwardThreadPoolExecutor extends ThreadPoolExecutor {

	private final static Log log = LogFactory.getLog(RollForwardThreadPoolExecutor.class);

	public RollForwardThreadPoolExecutor() {
		// core thread, max thread, timeout, timeout units, work queue
		super(Runtime.getRuntime().availableProcessors(), Runtime.getRuntime().availableProcessors(), 10, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>());
	}
	
	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		if (t != null) {
			// Exception Occurred, and handle the exception
			log.error("Error Occured in execution of Thread :: " + t);
		}
	}
	
	@Override
	public void terminated() {
		super.terminated();
	}
}
