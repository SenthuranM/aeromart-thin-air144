package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventStatus;

public class PubAvailStatusUpdateRQ implements Serializable {

	private static final long serialVersionUID = -7532838743382318086L;

	private Integer publishAvailabilityId;

	private PublishInventoryEventStatus publishInventoryEventStatus;

	/**
	 * @return the publishAvailabilityId
	 */
	public Integer getPublishAvailabilityId() {
		return publishAvailabilityId;
	}

	/**
	 * @param publishAvailabilityId
	 *            the publishAvailabilityId to set
	 */
	public void setPublishAvailabilityId(Integer publishAvailabilityId) {
		this.publishAvailabilityId = publishAvailabilityId;
	}

	/**
	 * @return the publishInventoryEventStatus
	 */
	public PublishInventoryEventStatus getPublishInventoryEventStatus() {
		return publishInventoryEventStatus;
	}

	/**
	 * @param publishInventoryEventStatus
	 *            the publishInventoryEventStatus to set
	 */
	public void setPublishInventoryEventStatus(PublishInventoryEventStatus publishInventoryEventStatus) {
		this.publishInventoryEventStatus = publishInventoryEventStatus;
	}
}
