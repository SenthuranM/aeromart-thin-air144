package com.isa.thinair.airinventory.api.dto.meal;

import java.io.Serializable;

public class MealTo implements Serializable {
	private String mealCode;
	private String mealName;
	private int quantity;

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
