package com.isa.thinair.airinventory.api.service;

import java.util.ArrayList;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.dto.inventory.UpdatedInvTempDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author priyantha
 */
public interface InventoryTemplateBD {

	public static final String SERVICE_NAME = "InventoryTemplateService";

	public InvTempDTO addInventoryTemplate(InvTempDTO invTempDTO) throws ModuleException;

	public Page<InvTempDTO> searchInvTemp(int start, int pageSize, String airCraftModel) throws ModuleException;

	public Page<InvTempCCAllocDTO> searchInvTemplCCAlloc(int start, int pageSize, String airCraftModel, boolean editedFlag, int invTempID);

	public ArrayList<InvTempCCBCAllocDTO> getBCDetailsForInvTemp(String ccCode);

	public ArrayList<InvTempCCBCAllocDTO> loadGridBCallocDetails(InvTempDTO invTempDTO, String cabinClassCode, String logicalCabinClassCode);

	public void updateInvTemplate(InvTempDTO invTempDTO, UpdatedInvTempDTO updateList) throws ModuleException;

	public void addInvTemplateCcAlloc(InvTempDTO invTempDTO) throws ModuleException;

	public void saveSplitExistCcAllocs(InvTempDTO invTempDTO) throws ModuleException;

	public void updateInvTempStatus(InvTempDTO invTempDTO) throws ModuleException;

	public ArrayList<InvTempDTO> getInventoryTemplatesForAirCraftModel(String airCraftModelName) throws ModuleException;

	public boolean validateDuplicateTemplate(String airCraftModel, String segment);

}