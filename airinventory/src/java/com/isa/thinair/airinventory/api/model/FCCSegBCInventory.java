/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:58
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Represents Booking Class level seats allocation for a given flight segment, given cabin class
 * 
 * @author : Nasly
 * 
 * @hibernate.class table = "T_FCC_SEG_BC_ALLOC"
 * 
 */
public class FCCSegBCInventory extends Persistent {

	private static final long serialVersionUID = 690996384765972424L;

	public static final String PRIORITY_FLAG_Y = "Y";

	private static final String PRIORITY_FLAG_N = "N";

	/** Eligible statuses */
	public static interface Status {
		/** Open */
		public static final String OPEN = "OPN";

		/** Closed */
		public static final String CLOSED = "CLS";
	}

	/** Denotes the action which set the status */
	public static interface StatusAction {
		/** OPEN statuses */

		/** Denotes status at of initial creation */
		public static final String CREATION = "C";

		/** Automatic REOPENing due to block seat release */
		public static final String BLOCKED_SEAT_RLEASE = "B";

		/** Automatic REOPENing due seat acquisition by release */
		public static final String SEAT_AQUISITION = "R";

		/** Statuses common to OPEN and CLOSED */

		/** Manually CLOSED or Manually OPEN */
		public static final String MANUAL = "M";

		/** CLOSED statuses */

		/** Automatic CLOSURE due to availability ZERO */
		public static final String AVAILABILITY_ZERO = "A";

		/** Automatic CLOSURE due to lower Standard BC closure */
		public static final String LOWERSTANDARD_CLOSURE = "L";

		/** Automatic group CLOSURE */
		public static final String GROUP_CLOSURE = "G";

		/** Closure of connection bucket due to lower ranked segment buckets closure */
		public static final String SEG_BUCKETS_CLOSURE = "S";

		/** Automatic REOPEN/CLOSE due to nested availability check operation */
		public static final String NESTED_SEAT_OPERATION = "N";
	}

	private Integer fccsbInvId;

	private Integer fccsInvId;

	private Integer flightId;

	private Integer seatsAllocated;

	private Integer seatsSold;

	private Integer onHoldSeats;

	private Integer seatsAvailable;

	private Integer seatsCancelled;

	private Integer seatsAcquired;

	private String status;

	private Boolean priorityFlag;

	private String logicalCCCode;

	private String bookingCode;

	private String segmentCode;

	private String statusChangeAction;

	private String thresholdStatus;

	private String optimizationStatus;

	private int allocatedWaitListSeats;

	private int waitListedSeats;

	private Integer seatsAvailableNested;

	private Integer previousSeatsAllocated;

	private Integer previousWLAllocated;
	
	private boolean surchargeApplicability;

	/**
	 * returns the fccsbInvId
	 * 
	 * @return Returns the fccsbInvId.
	 * @hibernate.id column = "FCCSBA_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FCC_SEG_BC_ALLOC_FCCSBA_ID"
	 */
	public Integer getFccsbInvId() {
		return fccsbInvId;
	}

	/**
	 * sets the fccsbInvId
	 * 
	 * @param fccsbInvId
	 *            The fccsbInvId to set.
	 */
	public void setFccsbInvId(Integer fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}

	/**
	 * returns the fccsInvId
	 * 
	 * @return Returns the fccsInvId.
	 * 
	 * @hibernate.property column = "FCCSA_ID"
	 */
	public Integer getfccsInvId() {
		return fccsInvId;
	}

	/**
	 * sets the fccsInvId
	 * 
	 * @param fccsInvId
	 *            The fccsInvId to set.
	 */
	public void setfccsInvId(Integer fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	/**
	 * returns the flightId
	 * 
	 * @return Returns the flightId.
	 * 
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * sets the flightId
	 * 
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * returns the seatsAllocated
	 * 
	 * @return Returns the seatsAllocated.
	 * 
	 * @hibernate.property column = "ALLOCATED_SEATS"
	 */
	public Integer getSeatsAllocated() {
		return seatsAllocated;
	}

	/**
	 * sets the seatsAllocated
	 * 
	 * @param seatsAllocated
	 *            The seatsAllocated to set.
	 */
	public void setSeatsAllocated(Integer seatsAllocated) {
		this.seatsAllocated = seatsAllocated;
	}

	/**
	 * returns the seatsSold
	 * 
	 * @return Returns the seatsSold.
	 * 
	 * @hibernate.property column = "SOLD_SEATS"
	 */
	public Integer getSeatsSold() {
		return seatsSold;
	}

	/**
	 * sets the seatsSold
	 * 
	 * @param seatsSold
	 *            The seatsSold to set.
	 */
	public void setSeatsSold(Integer seatsSold) {
		this.seatsSold = seatsSold;
	}

	/**
	 * returns the onHoldSeats
	 * 
	 * @return Returns the onHoldSeats.
	 * 
	 * @hibernate.property column = "ONHOLD_SEATS"
	 */
	public Integer getOnHoldSeats() {
		return onHoldSeats;
	}

	/**
	 * sets the onHoldSeats
	 * 
	 * @param onHoldSeats
	 *            The onHoldSeats to set.
	 */
	public void setOnHoldSeats(Integer onHoldSeats) {
		this.onHoldSeats = onHoldSeats;
	}

	/**
	 * returns the seatsAvailable
	 * 
	 * @return Returns the seatsAvailable.
	 * 
	 * @hibernate.property column = "AVAILABLE_SEATS"
	 */
	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	/**
	 * sets the seatsAvailable
	 * 
	 * @param seatsAvailable
	 *            The seatsAvailable to set.
	 */
	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	/**
	 * returns the seatsCancelled
	 * 
	 * @return Returns the seatsCancelled.
	 * 
	 * @hibernate.property column = "CANCELLED_SEATS"
	 */
	public Integer getSeatsCancelled() {
		return seatsCancelled;
	}

	/**
	 * sets the seatsCancelled
	 * 
	 * @param seatsCancelled
	 *            The seatsCancelled to set.
	 */
	public void setSeatsCancelled(Integer seatsCancelled) {
		this.seatsCancelled = seatsCancelled;
	}

	/**
	 * returns the seatsAcquired
	 * 
	 * @return Returns the seatsAcquired.
	 * 
	 * @hibernate.property column = "ACQUIRED_SEATS"
	 */
	public Integer getSeatsAcquired() {
		return seatsAcquired;
	}

	/**
	 * sets the seatsAcquired
	 * 
	 * @param seatsAcquired
	 *            The seatsAcquired to set.
	 */
	public void setSeatsAcquired(Integer seatsAcquired) {
		this.seatsAcquired = seatsAcquired;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the priorityFlag
	 * 
	 * @return Returns the priorityFlag.
	 * 
	 * @hibernate.property column = "PRIORITY_FLAG"
	 */
	@SuppressWarnings("unused")
	private String getPriorityFlagChar() {
		if (priorityFlag)
			return PRIORITY_FLAG_Y;
		else
			return PRIORITY_FLAG_N;
	}

	/**
	 * sets the priorityFlag
	 * 
	 * @param priorityFlag
	 *            The priorityFlag to set.
	 */
	@SuppressWarnings("unused")
	private void setPriorityFlagChar(String priorityFlag) {
		if (priorityFlag.equals(PRIORITY_FLAG_Y))
			this.priorityFlag = true;
		else
			this.priorityFlag = false;
	}

	public boolean getPriorityFlag() {
		return priorityFlag;
	}
	
	public Boolean getPriorityFlagWrapper() {
		return priorityFlag;
	}

	public void setPriorityFlag(boolean priorityFlag) {
		this.priorityFlag = priorityFlag;
	}

	/**
	 * returns the logicalCCCode
	 * 
	 * @return Returns the logicalCCCode.
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * sets the logicalCCCode
	 * 
	 * @param logicalCCCode
	 *            The logicalCCCode to set.
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * returns the bookingCode
	 * 
	 * @return Returns the bookingCode.
	 * 
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * sets the bookingCode
	 * 
	 * @param bookingCode
	 *            The bookingCode to set.
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * returns the segmentCode
	 * 
	 * @return Returns the segmentCode.
	 * 
	 * @hibernate.property column = "SEGMENT_CODE"
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * sets the segmentCode
	 * 
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * 
	 * @return Action triggered the status change
	 * 
	 * @hibernate.property column = "STATUS_CHG_ACTION"
	 */
	public String getStatusChangeAction() {
		return statusChangeAction;
	}

	public void setStatusChangeAction(String statusChangeAction) {
		this.statusChangeAction = statusChangeAction;
	}

	/**
	 * @return Optimization Status
	 * 
	 * @hibernate.property column = "OPTIMIZATION_STATUS"
	 */
	public String getOptimizationStatus() {
		return optimizationStatus;
	}

	public void setOptimizationStatus(String optimizationStatus) {
		this.optimizationStatus = optimizationStatus;
	}

	/**
	 * @return Threshold Status
	 * @hibernate.property column = "THRESHOLD_STATUS"
	 */
	public String getThresholdStatus() {
		return thresholdStatus;
	}

	public void setThresholdStatus(String thresholdStatus) {
		this.thresholdStatus = thresholdStatus;
	}

	/**
	 * @return Integer waitListedSeats
	 * @hibernate.property column = "ALLOCATED_WAITLIST_SEATS"
	 */
	public int getAllocatedWaitListSeats() {
		return allocatedWaitListSeats;
	}

	public void setAllocatedWaitListSeats(int allocatedWaitListSeats) {
		this.allocatedWaitListSeats = allocatedWaitListSeats;
	}

	/**
	 * @return Integer waitListedSeats
	 * @hibernate.property column = "WAITLISTED_SEATS"
	 */
	public Integer getWaitListedSeats() {
		return waitListedSeats;
	}

	public void setWaitListedSeats(Integer waitListedSeats) {
		this.waitListedSeats = waitListedSeats;
	}

	// -----------------------------------------------------
	// Utility methods
	// -----------------------------------------------------

	public boolean hasReservations() {
		if (getSeatsSold() > 0 || getOnHoldSeats() > 0) {
			return true;
		}
		return false;
	}

	public Integer getSeatsOnHoldAquiredByNesting() {
		return seatsOnHoldAquiredByNesting;
	}

	public void setSeatsOnHoldAquiredByNesting(Integer seatsOnHoldAquiredByNesting) {
		this.seatsOnHoldAquiredByNesting = seatsOnHoldAquiredByNesting;
	}

	public Integer getSeatsOnHoldNested() {
		return seatsOnHoldNested;
	}

	public void setSeatsOnHoldNested(Integer seatsOnHoldNested) {
		this.seatsOnHoldNested = seatsOnHoldNested;
	}

	public Integer getSeatsSoldAquiredByNesting() {
		return seatsSoldAquiredByNesting;
	}

	public void setSeatsSoldAquiredByNesting(Integer seatsSoldAquiredByNesting) {
		this.seatsSoldAquiredByNesting = seatsSoldAquiredByNesting;
	}

	public Integer getSeatsSoldNested() {
		return seatsSoldNested;
	}

	public void setSeatsSoldNested(Integer seatsSoldNested) {
		this.seatsSoldNested = seatsSoldNested;
	}

	public Integer getActualSeatsSold() {
		return getSeatsSold() + getSeatsSoldAquiredByNesting() - getSeatsSoldNested();
	}

	public Integer getActualSeatsOnHold() {
		return getOnHoldSeats() + getSeatsOnHoldAquiredByNesting() - getSeatsOnHoldNested();
	}

	public Integer getSeatsAvailableNested() {
		return seatsAvailableNested;
	}

	public void setSeatsAvailableNested(Integer seatsAvailableNested) {
		this.seatsAvailableNested = seatsAvailableNested;
	}

	public Integer getPreviousSeatsAllocated() {
		return previousSeatsAllocated;
	}

	public void setPreviousSeatsAllocated(Integer previousSeatsAllocated) {
		this.previousSeatsAllocated = previousSeatsAllocated;
	}

	public Integer getPreviousWLAllocated() {
		return previousWLAllocated;
	}

	public void setPreviousWLAllocated(Integer previousWLAllocated) {
		this.previousWLAllocated = previousWLAllocated;
	}

	public boolean isSurchargeApplicability() {
		return surchargeApplicability;
	}

	public void setSurchargeApplicability(boolean surchargeApplicability) {
		this.surchargeApplicability = surchargeApplicability;
	}

	private Integer seatsSoldAquiredByNesting = 0;

	private Integer seatsSoldNested = 0;

	private Integer seatsOnHoldAquiredByNesting = 0;

	private Integer seatsOnHoldNested = 0;
}
