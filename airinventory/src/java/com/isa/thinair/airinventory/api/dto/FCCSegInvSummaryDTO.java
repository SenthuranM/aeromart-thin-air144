/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

/**
 * Summary of Flight CabinClass Segment Inventory
 * 
 * @author Byorn
 */
public class FCCSegInvSummaryDTO implements Serializable {

	private static final long serialVersionUID = 4768316758946804561L;

	private int flightId;

	private int fccsInvId;

	private String segmentCode;

	private int flightSegmentId;

	private int adultAllocation;

	private int infantAllocation;

	private int adultSold;

	private int infantSold;

	private int adultOnhold;

	private int infantOnhold;

	private int availableSeats;

	private int availableInfantSeats;

	private int seatFactor;

	private String cabinClassCode;

	private String logicalCabinClassCode;

	public FCCSegInvSummaryDTO() {

	}

	public FCCSegInvSummaryDTO(String segmentCode, int adultSold, int infantSold, int adultOnhold, int infantOnhold,
			int flightSegmentId) {
		this.segmentCode = segmentCode;
		this.adultSold = adultSold;
		this.infantSold = infantSold;
		this.adultOnhold = adultOnhold;
		this.infantOnhold = infantOnhold;
		this.flightSegmentId = flightSegmentId;
	}

	public FCCSegInvSummaryDTO(int fccsInvId, String segmentCode, int flightId, String cabinClass, String logicalCabinClass) {
		this.fccsInvId = fccsInvId;
		this.segmentCode = segmentCode;
		this.flightId = flightId;
		this.cabinClassCode = cabinClass;
		this.logicalCabinClassCode = logicalCabinClass;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getFccsInvId() {
		return fccsInvId;
	}

	public void setFccsInvId(int fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	public int getFlightSegmentId() {
		return flightSegmentId;
	}

	public void setFlightSegmentId(int flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}

	/**
	 * @return Returns the adultAllocation.
	 */
	public int getAdultAllocation() {
		return adultAllocation;
	}

	/**
	 * @param adultAllocation
	 *            The adultAllocation to set.
	 */
	public void setAdultAllocation(int adultAllocation) {
		this.adultAllocation = adultAllocation;
	}

	/**
	 * @return Returns the adultOnhold.
	 */
	public int getAdultOnhold() {
		return adultOnhold;
	}

	/**
	 * @param adultOnhold
	 *            The adultOnhold to set.
	 */
	public void setAdultOnhold(int adultOnhold) {
		this.adultOnhold = adultOnhold;
	}

	/**
	 * @return Returns the adultSold.
	 */
	public int getAdultSold() {
		return adultSold;
	}

	/**
	 * @param adultSold
	 *            The adultSold to set.
	 */
	public void setAdultSold(int adultSold) {
		this.adultSold = adultSold;
	}

	/**
	 * @return Returns the infantAllocation.
	 */
	public int getInfantAllocation() {
		return infantAllocation;
	}

	/**
	 * @param infantAllocation
	 *            The infantAllocation to set.
	 */
	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	/**
	 * @return Returns the infantOnhold.
	 */
	public int getInfantOnhold() {
		return infantOnhold;
	}

	/**
	 * @param infantOnhold
	 *            The infantOnhold to set.
	 */
	public void setInfantOnhold(int infantOnhold) {
		this.infantOnhold = infantOnhold;
	}

	/**
	 * @return Returns the infantSold.
	 */
	public int getInfantSold() {
		return infantSold;
	}

	/**
	 * @param infantSold
	 *            The infantSold to set.
	 */
	public void setInfantSold(int infantSold) {
		this.infantSold = infantSold;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getAvailableInfantSeats() {
		return availableInfantSeats;
	}

	public void setAvailableInfantSeats(int availableInfantSeats) {
		this.availableInfantSeats = availableInfantSeats;
	}

	public int getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}

	public int getSeatFactor() {
		return seatFactor;
	}

	public void setSeatFactor(int seatFactor) {
		this.seatFactor = seatFactor;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		String t = "t";
		summary.append(nl + "..................FCC Segment Inventory Information..................." + nl);
		summary.append(t + "FCCSegInvId			= " + getFccsInvId() + nl);
		summary.append(t + "Segment Code		= " + getSegmentCode() + nl);
		summary.append(t + "Adult allocation	= " + getAdultAllocation() + nl);
		summary.append(t + "Infant allocation	= " + getInfantAllocation() + nl);
		summary.append(t + "Available Seats     = " + getAvailableSeats() + nl);
		summary.append(t + "Avail Infant Seats  = " + getAvailableInfantSeats() + nl);
		summary.append(t + "Adult sold			= " + getAdultSold() + nl);
		summary.append(t + "FCCSegInvId			= " + getAdultOnhold() + nl);
		summary.append(t + "Segment Code		= " + getInfantSold() + nl);
		summary.append(t + "Adult allocation	= " + getInfantOnhold() + nl);

		summary.append(nl + "......................................................................" + nl);
		return summary;
	}

}
