package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.FLEXI_CHARGE_PERCENTAGE_BASIS;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

/**
 * Utility methods for manipulating quoted charges
 * 
 * @author Nasly
 */
public class ChargeQuoteUtils {

	/**
	 * Returns a cloned collection of charges with effective charge amount for adult and infant set.
	 * 
	 * @param chargeCollection
	 * @param fareSummaryDTO
	 * @param totalJourneySurcharge
	 * @return
	 */
	public static Collection<QuotedChargeDTO> getChargesWithEffectiveChargeValues(Collection<QuotedChargeDTO> chargeCollection,
			FareSummaryDTO fareSummaryDTO, Integer noOfRequestedAdults, Integer noOfReqestedChildren,
			Map<String, Double> totalJourneyFare, Map<String, Double> totalJourneySurcharge) {
		Collection<QuotedChargeDTO> chargesWithEffectiveChargeAmount = new ArrayList<QuotedChargeDTO>();
		if (chargeCollection != null && chargeCollection.size() > 0) {

			List<QuotedChargeDTO> chargesPFS = new ArrayList<QuotedChargeDTO>();

			double surAdult = 0d;
			double surChild = 0d;
			double surInfant = 0d;

			Double totalADSurcharge = 0d;
			Double totalCHSurcharge = 0d;
			Double toltaINSurcharge = 0d;
			if (totalJourneySurcharge != null) {
				totalADSurcharge = totalJourneySurcharge.get(PaxTypeTO.ADULT);
				totalCHSurcharge = totalJourneySurcharge.get(PaxTypeTO.CHILD);
				toltaINSurcharge = totalJourneySurcharge.get(PaxTypeTO.INFANT);
			}

			Iterator<QuotedChargeDTO> chargesIt = chargeCollection.iterator();
			while (chargesIt.hasNext()) {
				QuotedChargeDTO quotedChargeDTO = chargesIt.next();
				// Need to check whether we need to clone this always. When we do a profile test, this object has
				// highest contribution
				QuotedChargeDTO clonedCharge = quotedChargeDTO.clone();
				if (ChargeGroups.TAX.equals(quotedChargeDTO.getChargeGroupCode()) && quotedChargeDTO.isChargeValueInPercentage()
						&& ChargeRate.CHARGE_BASIS_PFS.equals(quotedChargeDTO.getChargeBasis())) {
					chargesPFS.add(clonedCharge);
				} else {
					if (quotedChargeDTO.isChargeValueInPercentage()) {
						setEffectiveChargeAmount(clonedCharge, PaxTypeTO.ADULT, quotedChargeDTO.getChargeValuePercentage(),
								new Double((getTruncatedDecimalStr(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)))),
								totalJourneyFare.get(PaxTypeTO.ADULT), null, totalADSurcharge);
						setEffectiveChargeAmount(clonedCharge, PaxTypeTO.CHILD, quotedChargeDTO.getChargeValuePercentage(),
								new Double((getTruncatedDecimalStr(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD)))),
								totalJourneyFare.get(PaxTypeTO.CHILD), null, totalCHSurcharge);
						setEffectiveChargeAmount(clonedCharge, PaxTypeTO.INFANT, quotedChargeDTO.getChargeValuePercentage(),
								new Double((getTruncatedDecimalStr(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT)))),
								totalJourneyFare.get(PaxTypeTO.INFANT), null, toltaINSurcharge);
					} else {
						clonedCharge.setEffectiveChargeAmount(PaxTypeTO.ADULT,
								new Double((getTruncatedDecimalStr(quotedChargeDTO.getChargeValuePercentage()))));
						clonedCharge.setEffectiveChargeAmount(PaxTypeTO.CHILD,
								new Double((getTruncatedDecimalStr(quotedChargeDTO.getChargeValuePercentage()))));
						clonedCharge.setEffectiveChargeAmount(PaxTypeTO.INFANT, new Double(
								(getTruncatedDecimalStr(quotedChargeDTO.getChargeValuePercentage()))));
						;
					}
				}

				chargesWithEffectiveChargeAmount.add(clonedCharge);

				if (ChargeGroups.SURCHARGE.equals(quotedChargeDTO.getChargeGroupCode())) {
					if (isChgApplicableForPaxType(quotedChargeDTO, PaxTypeTO.ADULT)) {
						surAdult = surAdult + clonedCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT);
					}
					if (isChgApplicableForPaxType(quotedChargeDTO, PaxTypeTO.CHILD)) {
						surChild = surChild + clonedCharge.getEffectiveChargeAmount(PaxTypeTO.CHILD);
					}
					if (isChgApplicableForPaxType(quotedChargeDTO, PaxTypeTO.INFANT)) {
						surInfant = surInfant + clonedCharge.getEffectiveChargeAmount(PaxTypeTO.INFANT);
					}
				} else if (ChargeGroups.INFANT_SURCHARGE.equals(quotedChargeDTO.getChargeGroupCode())) {
					surInfant = surInfant + clonedCharge.getEffectiveChargeAmount(PaxTypeTO.INFANT);
				}
			}

			if (chargesPFS.size() > 0) {
				for (QuotedChargeDTO chg : chargesPFS) {
					setEffectiveChargeAmount(chg, PaxTypeTO.ADULT, chg.getChargeValuePercentage(), new Double(
							(getTruncatedDecimalStr(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)))),
							totalJourneyFare.get(PaxTypeTO.ADULT), surAdult, totalADSurcharge);
					setEffectiveChargeAmount(chg, PaxTypeTO.CHILD, chg.getChargeValuePercentage(), new Double(
							(getTruncatedDecimalStr(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD)))),
							totalJourneyFare.get(PaxTypeTO.CHILD), surChild, totalCHSurcharge);
					setEffectiveChargeAmount(chg, PaxTypeTO.INFANT, chg.getChargeValuePercentage(), new Double(
							(getTruncatedDecimalStr(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT)))),
							totalJourneyFare.get(PaxTypeTO.INFANT), surInfant, toltaINSurcharge);
				}
			}

			adjustChargesAmoungPassengers(chargesWithEffectiveChargeAmount, noOfRequestedAdults, noOfReqestedChildren);
		}
		return chargesWithEffectiveChargeAmount;
	}

	public static Double getAdjustedEffectiveCharge(Double effectiveCharge, QuotedChargeDTO quotedChargeDTO, int noOfPax) {
		if (Charge.APPLICABLE_TO_OND == quotedChargeDTO.getApplicableTo()) {
			Double effectiveChargeAmount = AccelAeroCalculator.divide(AccelAeroCalculator.parseBigDecimal(effectiveCharge),
					noOfPax).doubleValue();

			if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
				effectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(effectiveChargeAmount),
						quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
			}
			return effectiveChargeAmount;

		} else {
			return effectiveCharge;
		}
	}

	private static void adjustChargesAmoungPassengers(Collection<QuotedChargeDTO> chargesWithEffectiveChargeAmount,
			Integer noOfRequestedAdults, Integer noOfReqestedChildren) {

		QuotedChargeDTO quotedChargeDTO;
		Iterator<QuotedChargeDTO> itChargesWithEffectiveChargeAmount = chargesWithEffectiveChargeAmount.iterator();
		int noOfAdults = noOfRequestedAdults == null ? 0 : noOfRequestedAdults;
		int noOfChildren = noOfReqestedChildren == null ? 0 : noOfReqestedChildren;

		while (itChargesWithEffectiveChargeAmount.hasNext()) {
			quotedChargeDTO = itChargesWithEffectiveChargeAmount.next();
			if (Charge.APPLICABLE_TO_OND == quotedChargeDTO.getApplicableTo()) {

				if (quotedChargeDTO.isChargeValueInPercentage()) {

					if (noOfAdults > 1) {
						Double adultCharge = quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT);
						Double effectiveChargeAmount = AccelAeroCalculator.divide(
								AccelAeroCalculator.parseBigDecimal(adultCharge), noOfAdults).doubleValue();

						if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
							effectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(
									BigDecimal.valueOf(effectiveChargeAmount), quotedChargeDTO.getBoundryValue(),
									quotedChargeDTO.getBreakPoint()).doubleValue();
						}

						quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, effectiveChargeAmount);
					}

					if (noOfChildren > 1) {
						Double childCharge = quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD);
						Double effectiveChargeAmount = AccelAeroCalculator.divide(
								AccelAeroCalculator.parseBigDecimal(childCharge), noOfChildren).doubleValue();

						if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
							effectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(
									BigDecimal.valueOf(effectiveChargeAmount), quotedChargeDTO.getBoundryValue(),
									quotedChargeDTO.getBreakPoint()).doubleValue();
						}

						quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, effectiveChargeAmount);
					}

					quotedChargeDTO.setApplicableTo(Charge.APPLICABLE_TO_ADULT_CHILD);
				} else {

					if ((noOfAdults + noOfChildren) > 1) {
						// Since this is Value charge the charge will be common
						// for adult,child,infant
						Double charge = quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT);
						BigDecimal perCharge = AccelAeroCalculator.divide(AccelAeroCalculator.parseBigDecimal(charge), noOfAdults
								+ noOfChildren);

						quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, perCharge.doubleValue());
						quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, perCharge.doubleValue());
					}

					quotedChargeDTO.setApplicableTo(Charge.APPLICABLE_TO_ADULT_CHILD);
				}
			}
		}
	}

	/**
	 * Returns total charge amounts applicable for adults and infants
	 * 
	 * @param effectiveChargesCollection
	 * @return two element double array element 0 - total adult charge element 1 - total infant charge element 2 - total
	 *         child charge
	 */
	public static List<Double> calculateTotalCharges(Collection<QuotedChargeDTO> effectiveChargesCollection) {
		double adultCharges = 0;
		double infantCharges = 0;
		double childCharges = 0;
		if (effectiveChargesCollection != null) {
			Iterator<QuotedChargeDTO> chargesIt = effectiveChargesCollection.iterator();
			while (chargesIt.hasNext()) {
				QuotedChargeDTO charge = chargesIt.next();
				if (isChgApplicableForPaxType(charge, PaxTypeTO.ADULT)) {
					adultCharges += charge.getEffectiveChargeAmount(PaxTypeTO.ADULT);
				}
				if (isChgApplicableForPaxType(charge, PaxTypeTO.INFANT)) {
					infantCharges += charge.getEffectiveChargeAmount(PaxTypeTO.INFANT);
				}
				if (isChgApplicableForPaxType(charge, PaxTypeTO.CHILD)) {
					childCharges += charge.getEffectiveChargeAmount(PaxTypeTO.CHILD);
				}
			}
		}
		// TODO
		List<Double> totalCharges = new ArrayList<Double>();
		totalCharges.add(adultCharges);
		totalCharges.add(infantCharges);
		totalCharges.add(childCharges);
		return totalCharges;
	}

	public static boolean isChgApplicableForPaxType(QuotedChargeDTO charge, String paxType) {
		if (PaxTypeTO.ADULT.equals(paxType)
				&& (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT || charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD)) {
			return true;
		}
		if (PaxTypeTO.CHILD.equals(paxType)
				&& (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY || charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD)) {
			return true;
		}
		if (PaxTypeTO.INFANT.equals(paxType)
				&& (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
						|| charge.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY || charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT)) {
			return true;
		}
		return false;
	}

	static double[] calculateTotalChargeForChargeGroup(Collection<QuotedChargeDTO> effChargesCol, String chargeGroup) {
		double adultCharges = 0;
		double infantCharges = 0;
		double childCharges = 0;

		if (effChargesCol != null) {
			Iterator<QuotedChargeDTO> chargesIt = effChargesCol.iterator();
			while (chargesIt.hasNext()) {
				QuotedChargeDTO charge = chargesIt.next();
				if (chargeGroup == null || charge.getChargeGroupCode().equals(chargeGroup)) {
					if (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						adultCharges += charge.getEffectiveChargeAmount(PaxTypeTO.ADULT).doubleValue();
					}
					if (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {
						infantCharges += charge.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue();
					}
					if (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						childCharges += charge.getEffectiveChargeAmount(PaxTypeTO.CHILD).doubleValue();
					}
				}
			}
		}
		return new double[] { adultCharges, infantCharges, childCharges };
	}

	public static double[] calculateTotalChargeForChargeGroup(Collection<QuotedChargeDTO> effChargesCol, String chargeGroup,
			boolean skipBundleCharge) {
		double adultCharges = 0;
		double infantCharges = 0;
		double childCharges = 0;

		if (effChargesCol != null) {
			Iterator<QuotedChargeDTO> chargesIt = effChargesCol.iterator();
			while (chargesIt.hasNext()) {
				QuotedChargeDTO charge = chargesIt.next();

				if (skipBundleCharge && charge.isBundledFareCharge()) {
					continue;
				}

				if (chargeGroup == null || charge.getChargeGroupCode().equals(chargeGroup)) {
					if (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						adultCharges += charge.getEffectiveChargeAmount(PaxTypeTO.ADULT).doubleValue();
					}
					if (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {
						infantCharges += charge.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue();
					}
					if (charge.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
							|| charge.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						childCharges += charge.getEffectiveChargeAmount(PaxTypeTO.CHILD).doubleValue();
					}
				}
			}
		}
		return new double[] { adultCharges, infantCharges, childCharges };
	}

	static Collection<QuotedChargeDTO> getUnifiedCharges(Collection<QuotedChargeDTO> allEffectiveChargesCol) {
		HashMap<String, QuotedChargeDTO> unifiedCharges = new HashMap<String, QuotedChargeDTO>();
		if (allEffectiveChargesCol != null && allEffectiveChargesCol.size() > 0) {
			Iterator<QuotedChargeDTO> allEffectiveChargesColIt = allEffectiveChargesCol.iterator();
			while (allEffectiveChargesColIt.hasNext()) {
				QuotedChargeDTO chargeDTO = allEffectiveChargesColIt.next();
				if (unifiedCharges.containsKey(chargeDTO.getChargeCode())) {
					QuotedChargeDTO charge = unifiedCharges.get(chargeDTO.getChargeCode());
					charge.setEffectiveChargeAmount(PaxTypeTO.ADULT, new Double(charge.getEffectiveChargeAmount(PaxTypeTO.ADULT)
							.doubleValue() + chargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT).doubleValue()));
					charge.setEffectiveChargeAmount(PaxTypeTO.CHILD, new Double(charge.getEffectiveChargeAmount(PaxTypeTO.CHILD)
							.doubleValue() + chargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD).doubleValue()));
					charge.setEffectiveChargeAmount(PaxTypeTO.INFANT,
							new Double(charge.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue()
									+ chargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue()));
					// charge.setEffectiveChargeAmountForInfant(new
					// Double(charge.getEffectiveChargeAmountForInfant().doubleValue() +
					// chargeDTO.getEffectiveChargeAmountForInfant().doubleValue()));
				} else {
					unifiedCharges.put(chargeDTO.getChargeCode(), chargeDTO.clone());
				}
			}
		}
		return unifiedCharges.values();
	}

	static Collection<QuotedChargeDTO> getUnifiedCharges(Collection<QuotedChargeDTO> allEffectiveChargesCol,
			String chargeGroupCode, Collection<String> paxTypesCollection) {
		Collection<String> chargeGroupCodes = new ArrayList<String>();
		chargeGroupCodes.add(chargeGroupCode);
		return getUnifiedCharges(allEffectiveChargesCol, chargeGroupCodes, paxTypesCollection);
	}

	static Collection<QuotedChargeDTO> getUnifiedCharges(Collection<QuotedChargeDTO> allEffectiveChargesCol,
			Collection<String> chargeGroupCodes, Collection<String> paxTypesCollection) {
		HashMap<String, QuotedChargeDTO> unifiedCharges = new HashMap<String, QuotedChargeDTO>();
		if (allEffectiveChargesCol != null && allEffectiveChargesCol.size() > 0 && chargeGroupCodes != null
				&& chargeGroupCodes.size() > 0) {
			Iterator<QuotedChargeDTO> allEffectiveChargesColIt = allEffectiveChargesCol.iterator();
			while (allEffectiveChargesColIt.hasNext()) {
				QuotedChargeDTO chargeDTO = allEffectiveChargesColIt.next();
				if (chargeGroupCodes.contains(chargeDTO.getChargeGroupCode())) {
					if (isChargeApplicableForPaxType(chargeDTO.getApplicableTo(), paxTypesCollection)) {
						if (unifiedCharges.containsKey(chargeDTO.getChargeCode())) {
							QuotedChargeDTO charge = unifiedCharges.get(chargeDTO.getChargeCode());
							charge.setEffectiveChargeAmount(PaxTypeTO.ADULT,
									new Double(charge.getEffectiveChargeAmount(PaxTypeTO.ADULT).doubleValue()
											+ chargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT).doubleValue()));
							charge.setEffectiveChargeAmount(PaxTypeTO.CHILD,
									new Double(charge.getEffectiveChargeAmount(PaxTypeTO.CHILD).doubleValue()
											+ chargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD).doubleValue()));
							charge.setEffectiveChargeAmount(PaxTypeTO.INFANT,
									new Double(charge.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue()
											+ chargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue()));
						} else {
							unifiedCharges.put(chargeDTO.getChargeCode(), chargeDTO.clone());
						}
					}
				}
			}
		}
		return unifiedCharges.values();
	}

	private static boolean isChargeApplicableForPaxType(int chargeAppliesTo, Collection<String> paxTypesCollection) {
		boolean isApplicable = false;
		if (paxTypesCollection != null) {
			if (chargeAppliesTo == Charge.APPLICABLE_TO_ALL) {
				isApplicable = true;
			} else if ((chargeAppliesTo == Charge.APPLICABLE_TO_ADULT_ONLY || chargeAppliesTo == Charge.APPLICABLE_TO_ADULT_CHILD || chargeAppliesTo == Charge.APPLICABLE_TO_ADULT_INFANT)
					&& (paxTypesCollection.contains(PaxTypeTO.ADULT))) {
				isApplicable = true;
			} else if ((chargeAppliesTo == Charge.APPLICABLE_TO_INFANT_ONLY || chargeAppliesTo == Charge.APPLICABLE_TO_ADULT_INFANT)
					&& (paxTypesCollection.contains(PaxTypeTO.INFANT))) {
				isApplicable = true;
			} else if ((chargeAppliesTo == Charge.APPLICABLE_TO_CHILD_ONLY || chargeAppliesTo == Charge.APPLICABLE_TO_ADULT_CHILD)
					&& (paxTypesCollection.contains(PaxTypeTO.CHILD))) {
				isApplicable = true;
			}
		} else {
			isApplicable = true;
		}
		return isApplicable;
	}

	private static String getTruncatedDecimalStr(double doubleAmount) {
		return AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.parseBigDecimal(doubleAmount));
	}

	public static Double getEffectiveChargeAmount(QuotedChargeDTO quotedChargeDTO, String paxType, Double fareAmount,
			Double totalFareAmount, Double surchargeAmount, Double totalSurchargeAmount) {

		Double effectiveAmount = null;
		if (quotedChargeDTO.isChargeValueInPercentage()) {
			Double chargePercentage = quotedChargeDTO.getChargeValuePercentage();
			String chargeBasis = quotedChargeDTO.getChargeBasis();
			if (chargeBasis != null
					&& (fareAmount != null || totalFareAmount != null || (fareAmount != null && surchargeAmount != null) || (totalFareAmount != null && totalSurchargeAmount != null))) {
				Double calculatedEffectiveVal = null;

				BigDecimal defineChargeValueMin = quotedChargeDTO.getChargeValueMin();
				BigDecimal defineChargeValueMax = quotedChargeDTO.getChargeValueMax();

				if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PF) && fareAmount != null) {
					calculatedEffectiveVal = new Double(fareAmount * (chargePercentage / 100d));
				} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PTF) && totalFareAmount != null) {
					calculatedEffectiveVal = new Double(totalFareAmount * (chargePercentage / 100d));
				} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PFS) && fareAmount != null && surchargeAmount != null) {
					calculatedEffectiveVal = new Double((fareAmount + surchargeAmount) * (chargePercentage / 100d));

				} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PFS) && fareAmount != null && surchargeAmount != null) {
					calculatedEffectiveVal = new Double((fareAmount + surchargeAmount) * (chargePercentage / 100d));

				} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PTFS) && totalFareAmount != null
						&& totalSurchargeAmount != null) {
					calculatedEffectiveVal = new Double((totalFareAmount + totalSurchargeAmount) * (chargePercentage / 100d));
				}

				if (calculatedEffectiveVal != null) {
					// break point calculations
					boolean chargeRoundingApplied = false;
					if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
						calculatedEffectiveVal = AccelAeroRounderPolicy.getRoundedValue(
								BigDecimal.valueOf(calculatedEffectiveVal), quotedChargeDTO.getBoundryValue(),
								quotedChargeDTO.getBreakPoint()).doubleValue();
						chargeRoundingApplied = true;
					}
					// if min and max value not defined it gets the calculated value
					if ((defineChargeValueMin != null) && (calculatedEffectiveVal < defineChargeValueMin.doubleValue())) {
						calculatedEffectiveVal = defineChargeValueMin.doubleValue();
					} else if ((defineChargeValueMax != null) && (calculatedEffectiveVal > defineChargeValueMax.doubleValue())) {
						calculatedEffectiveVal = defineChargeValueMax.doubleValue();
					}

					String strRoundEnable = AirinventoryUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
					if (!chargeRoundingApplied && strRoundEnable != null && strRoundEnable.equals("Y")) {
						BigDecimal bgeffectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(AccelAeroCalculator
								.parseBigDecimal(calculatedEffectiveVal), new BigDecimal(AirinventoryUtils.getGlobalConfig()
								.getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)), new BigDecimal(AirinventoryUtils
								.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
						calculatedEffectiveVal = bgeffectiveChargeAmount.doubleValue();
					}
					effectiveAmount = calculatedEffectiveVal;
				}
			}
		} else {
			effectiveAmount = new Double(getTruncatedDecimalStr(quotedChargeDTO.getChargeValuePercentage()));
		}
		return effectiveAmount;
	}

	public static void setEffectiveChargeAmount(QuotedChargeDTO quotedChargeDTO, String paxType, Double fareAmount,
			Double chargePercentage, Double totalFareAmount, Double surchargeAmount, Double totalSurchargeAmount) {
		String chargeBasis = quotedChargeDTO.getChargeBasis();

		if (chargeBasis != null
				&& (fareAmount != null || totalFareAmount != null || (fareAmount != null && surchargeAmount != null) || (totalFareAmount != null && totalSurchargeAmount != null))) {
			Double calculatedEffectiveVal = null;

			BigDecimal defineChargeValueMin = quotedChargeDTO.getChargeValueMin();
			BigDecimal defineChargeValueMax = quotedChargeDTO.getChargeValueMax();

			if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PF) && fareAmount != null) {
				if (0d == fareAmount) { // If fare zero, return min value for charge
					quotedChargeDTO.setEffectiveChargeAmount(paxType, ((quotedChargeDTO.getChargeValueMin() != null)
							? quotedChargeDTO.getChargeValueMin()
							: BigDecimal.ZERO).doubleValue());
					return;
				}
				calculatedEffectiveVal = new Double(fareAmount * (chargePercentage / 100d));
			} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PTF) && totalFareAmount != null) {
				if (0d == totalFareAmount) { // If total fare zero, return min value for charge
					quotedChargeDTO.setEffectiveChargeAmount(paxType, ((quotedChargeDTO.getChargeValueMin() != null)
							? quotedChargeDTO.getChargeValueMin()
							: BigDecimal.ZERO).doubleValue());
					return;
				}
				calculatedEffectiveVal = new Double(totalFareAmount * (chargePercentage / 100d));
			} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PFS) && fareAmount != null && surchargeAmount != null) {
				if (0d == (fareAmount + surchargeAmount)) { // If total fare zero, return min value for charge
					quotedChargeDTO.setEffectiveChargeAmount(paxType, ((quotedChargeDTO.getChargeValueMin() != null)
							? quotedChargeDTO.getChargeValueMin()
							: BigDecimal.ZERO).doubleValue());
					return;
				}
				calculatedEffectiveVal = new Double((fareAmount + surchargeAmount) * (chargePercentage / 100d));

			} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PFS) && fareAmount != null && surchargeAmount != null) {
				if (0d == (fareAmount + surchargeAmount)) { // If total fare zero, return min value for charge
					quotedChargeDTO.setEffectiveChargeAmount(paxType, ((quotedChargeDTO.getChargeValueMin() != null)
							? quotedChargeDTO.getChargeValueMin()
							: BigDecimal.ZERO).doubleValue());
					return;
				}
				calculatedEffectiveVal = new Double((fareAmount + surchargeAmount) * (chargePercentage / 100d));

			} else if (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PTFS) && totalFareAmount != null
					&& totalSurchargeAmount != null) {
				if (0d == (totalFareAmount + totalSurchargeAmount)) { // If total fare zero, return min value for charge
					quotedChargeDTO.setEffectiveChargeAmount(paxType, ((quotedChargeDTO.getChargeValueMin() != null)
							? quotedChargeDTO.getChargeValueMin()
							: BigDecimal.ZERO).doubleValue());
					return;
				}
				calculatedEffectiveVal = new Double((totalFareAmount + totalSurchargeAmount) * (chargePercentage / 100d));

			}

			// break point calculations
			boolean chargeRoundingApplied = false;
			if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
				calculatedEffectiveVal = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(calculatedEffectiveVal),
						quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
				chargeRoundingApplied = true;
			}

			if (calculatedEffectiveVal != null) {
				// if min and max value not defined it gets the calculated value
				if ((defineChargeValueMin != null) && (calculatedEffectiveVal < defineChargeValueMin.doubleValue())) {
					calculatedEffectiveVal = defineChargeValueMin.doubleValue();
				} else if ((defineChargeValueMax != null) && (calculatedEffectiveVal > defineChargeValueMax.doubleValue())) {
					calculatedEffectiveVal = defineChargeValueMax.doubleValue();
				}

				String strRoundEnable = AirinventoryUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
				if (!chargeRoundingApplied && strRoundEnable != null && strRoundEnable.equals("Y")
						&& calculatedEffectiveVal != null) {
					BigDecimal bgeffectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(AccelAeroCalculator
							.parseBigDecimal(calculatedEffectiveVal), new BigDecimal(AirinventoryUtils.getGlobalConfig()
							.getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)), new BigDecimal(AirinventoryUtils.getGlobalConfig()
							.getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
					calculatedEffectiveVal = bgeffectiveChargeAmount.doubleValue();
				}
				quotedChargeDTO.setEffectiveChargeAmount(paxType, calculatedEffectiveVal);
			}
		}
	}

	/**
	 * Returns a cloned collection of flexi charges with effective charge amount for adult, child and infant set.
	 * 
	 * @param flexiChargeCollection
	 * @param ondFareDTO
	 * @return flexiChargesWithEffectiveChargeAmount
	 */
	public static Collection<FlexiRuleDTO> getFlexiChargesWithEffectiveChargeValues(
			Collection<FlexiRuleDTO> flexiChargeCollection, FareSummaryDTO fare, Collection<QuotedChargeDTO> charges) {
		Collection<FlexiRuleDTO> flexiChargesWithEffectiveChargeAmount = null;
		if (flexiChargeCollection != null && flexiChargeCollection.size() > 0) {
			flexiChargesWithEffectiveChargeAmount = new ArrayList<FlexiRuleDTO>();

			Iterator<FlexiRuleDTO> chargesIt = flexiChargeCollection.iterator();
			while (chargesIt.hasNext()) {
				FlexiRuleDTO flexiRuleDTO = chargesIt.next();
				FlexiRuleDTO clonedFlexiCharge = flexiRuleDTO.clone();
				// Effective charge for Adult
				if (flexiRuleDTO.getCharge(PaxTypeTO.ADULT).equals(FlexiRuleDTO.VALUE_PERCENTAGE_FLAG_P)) {
					setEffectiveFlexiCharge(clonedFlexiCharge, PaxTypeTO.ADULT, flexiRuleDTO.getCharge(PaxTypeTO.ADULT),
							flexiRuleDTO.getPercentageBasis(PaxTypeTO.ADULT), fare, charges);
				} else {
					clonedFlexiCharge.setEffectiveChargeAmount(PaxTypeTO.ADULT,
							new Double((getTruncatedDecimalStr(flexiRuleDTO.getCharge(PaxTypeTO.ADULT)))));
				}

				// Effective charge for Child
				if (flexiRuleDTO.getCharge(PaxTypeTO.CHILD).equals(FlexiRuleDTO.VALUE_PERCENTAGE_FLAG_P)) {
					setEffectiveFlexiCharge(clonedFlexiCharge, PaxTypeTO.CHILD, flexiRuleDTO.getCharge(PaxTypeTO.CHILD),
							flexiRuleDTO.getPercentageBasis(PaxTypeTO.CHILD), fare, charges);
				} else {
					clonedFlexiCharge.setEffectiveChargeAmount(PaxTypeTO.CHILD,
							new Double((getTruncatedDecimalStr(flexiRuleDTO.getCharge(PaxTypeTO.CHILD)))));
				}

				// Effective charge for Infant
				if (flexiRuleDTO.getCharge(PaxTypeTO.INFANT).equals(FlexiRuleDTO.VALUE_PERCENTAGE_FLAG_P)) {
					setEffectiveFlexiCharge(clonedFlexiCharge, PaxTypeTO.INFANT, flexiRuleDTO.getCharge(PaxTypeTO.INFANT),
							flexiRuleDTO.getPercentageBasis(PaxTypeTO.INFANT), fare, charges);
				} else {
					clonedFlexiCharge.setEffectiveChargeAmount(PaxTypeTO.INFANT,
							new Double((getTruncatedDecimalStr(flexiRuleDTO.getCharge(PaxTypeTO.INFANT)))));
				}

				flexiChargesWithEffectiveChargeAmount.add(clonedFlexiCharge);
			}
		}
		return flexiChargesWithEffectiveChargeAmount;
	}

	@SuppressWarnings("rawtypes")
	private static void setEffectiveFlexiCharge(FlexiRuleDTO flexiRuleDTO, String paxType, Double chargePercentage,
			String percentageBasis, FareSummaryDTO fare, Collection<QuotedChargeDTO> charges) {
		Double effectiveChargeAmount = new Double(0);
		Map<FLEXI_CHARGE_PERCENTAGE_BASIS, String> flexiChargePercentageBasisMap = AirpricingUtils.getAirpricingConfig()
				.getFlexiChargePercentageBasisMap();

		if (flexiChargePercentageBasisMap != null) {
			if (percentageBasis.equals(BeanUtils.nullHandler(flexiChargePercentageBasisMap
					.get(FLEXI_CHARGE_PERCENTAGE_BASIS.FARE_ONLY)))) {
				effectiveChargeAmount = new Double(fare.getFareAmount(paxType) * (chargePercentage / 100f));
			} else if (percentageBasis.equals(BeanUtils.nullHandler(flexiChargePercentageBasisMap
					.get(FLEXI_CHARGE_PERCENTAGE_BASIS.FARE_AND_SURCHARGE)))) {
				double surcharge = 0.0;
				if (charges != null) {
					double[] paxWiseSurcharges = calculateTotalChargeForChargeGroup(charges, ChargeGroups.SURCHARGE);
					if (paxType.equals(PaxTypeTO.ADULT)) {
						surcharge = paxWiseSurcharges[0];
					} else if (paxType.equals(PaxTypeTO.INFANT)) {
						surcharge = paxWiseSurcharges[1]
								+ calculateTotalChargeForChargeGroup(charges, ChargeGroups.INFANT_SURCHARGE)[1];
					} else if (paxType.equals(PaxTypeTO.CHILD)) {
						surcharge = paxWiseSurcharges[2];
					}
				}
				effectiveChargeAmount = new Double((fare.getFareAmount(paxType) + surcharge) * (chargePercentage / 100f));
			} else if (percentageBasis.equals(BeanUtils.nullHandler(flexiChargePercentageBasisMap
					.get(FLEXI_CHARGE_PERCENTAGE_BASIS.TOTAL_PRICE)))) {
				double totalCharge = 0.0;
				if (charges != null) {
					List<Double> paxWiseTotalCharges = calculateTotalCharges(charges);

					if (paxType.equals(PaxTypeTO.ADULT)) {
						totalCharge = paxWiseTotalCharges.get(0);
					} else if (paxType.equals(PaxTypeTO.INFANT)) {
						totalCharge = paxWiseTotalCharges.get(1);
					} else if (paxType.equals(PaxTypeTO.CHILD)) {
						totalCharge = paxWiseTotalCharges.get(2);
					}
				}
				effectiveChargeAmount = new Double((fare.getFareAmount(paxType) + totalCharge) * (chargePercentage / 100f));
			}

			String strRoundEnable = AirinventoryUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
			if (strRoundEnable != null && strRoundEnable.equals("Y")) {
				BigDecimal bgeffectiveChargeAmount = AccelAeroRounderPolicy.getRoundedValue(AccelAeroCalculator
						.parseBigDecimal(effectiveChargeAmount),
						new BigDecimal(AirinventoryUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)),
						new BigDecimal(AirinventoryUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
				effectiveChargeAmount = bgeffectiveChargeAmount.doubleValue();
			}
			flexiRuleDTO.setEffectiveChargeAmount(paxType, effectiveChargeAmount);

		}
	}

	public static Map<String, Double> getTotalSurcharges(Collection<QuotedChargeDTO> charges) {
		Map<String, Double> totalSurMap = new HashMap<String, Double>();
		totalSurMap.put(PaxTypeTO.ADULT, new Double(0));
		totalSurMap.put(PaxTypeTO.CHILD, new Double(0));
		totalSurMap.put(PaxTypeTO.INFANT, new Double(0));
		if (charges != null) {
			for (QuotedChargeDTO chg : charges) {
				if (ChargeGroups.SURCHARGE.equals(chg.getChargeGroupCode())) {
					if (isChgApplicableForPaxType(chg, PaxTypeTO.ADULT)) {
						totalSurMap.put(PaxTypeTO.ADULT,
								totalSurMap.get(PaxTypeTO.ADULT) + chg.getEffectiveChargeAmount(PaxTypeTO.ADULT));
					}
					if (isChgApplicableForPaxType(chg, PaxTypeTO.CHILD)) {
						totalSurMap.put(PaxTypeTO.CHILD,
								totalSurMap.get(PaxTypeTO.CHILD) + chg.getEffectiveChargeAmount(PaxTypeTO.CHILD));
					}
					if (isChgApplicableForPaxType(chg, PaxTypeTO.INFANT)) {
						totalSurMap.put(PaxTypeTO.INFANT,
								totalSurMap.get(PaxTypeTO.INFANT) + chg.getEffectiveChargeAmount(PaxTypeTO.INFANT));
					}
				} else if (ChargeGroups.INFANT_SURCHARGE.equals(chg.getChargeGroupCode())) {
					totalSurMap.put(PaxTypeTO.INFANT,
							totalSurMap.get(PaxTypeTO.INFANT) + chg.getEffectiveChargeAmount(PaxTypeTO.INFANT));
				}
			}
		}
		return totalSurMap;
	}

}
