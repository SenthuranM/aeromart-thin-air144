package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.util.FarePercentageCalculator;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * Abstract out comment methods of both AvailableFlightSegment and AvailableConnectedFlight
 * 
 * @author mekanayake
 */
public abstract class AvailableIBOBFlightSegment
		implements IAvailableFlightSegment, IFaresChargesAndSeats, Comparable<AvailableIBOBFlightSegment> {

	private static final long serialVersionUID = -5140200537689596931L;

	private String selectedLogicalCabinClass;

	private final Map<String, Boolean> seatsAvailable = new HashMap<String, Boolean>();

	// fare can have only one entry for own search and for the dry , interline return searches it can have 2 entries
	private final Map<String, FareSummaryDTO> fare = new HashMap<String, FareSummaryDTO>();

	/** Charges collection without effective amount set */
	private final Map<String, Collection<QuotedChargeDTO>> charges = new HashMap<String, Collection<QuotedChargeDTO>>();

	private Map<String, QuotedChargeDTO> serviceTaxes = new HashMap<>();

	private Map<String, List<String>> serviceTaxExcludedCharges = new HashMap<>();

	/** Charges for external booking classes (IL booking classes )collection */
	private final Map<String, Map<String, List<Double>>> interlineBookingClassWiseCharges = new HashMap<String, Map<String, List<Double>>>();

	private final Map<String, Integer> fareType = new HashMap<String, Integer>();

	private final Map<String, Collection<QuotedChargeDTO>> effectiveCharges = new HashMap<String, Collection<QuotedChargeDTO>>();

	/** Holds total fare for the current instance */
	private final Map<String, Map<String, Double>> totalFareMap = new HashMap<String, Map<String, Double>>();

	/** holds the parent SelectedFlightDTO ref corresponds to the FQ */
	protected SelectedFlightDTO selectedFlightDTO;

	// Fill addional per pax type information
	private final Map<String, Collection<OndFareDTO>> fareChargesSeatsSegmentsDTOs = new HashMap<String, Collection<OndFareDTO>>();

	private final List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();

	private int ondSequence = -1;

	private boolean waitListed = false;

	private boolean unTouchedOnd = false;

	private int subJourneyGroup = -1;
	
	private boolean splitOperationForBusInvoked = false;

	private Map<String, List<BundledFareLiteDTO>> availableBundledFares = new HashMap<String, List<BundledFareLiteDTO>>();

	private BundledFareDTO selectedBundledFare = null;

	@Override
	public boolean equals(Object obj) {
		String thisFltIds = "";
		String objFltIds = "";
		if (getFlightSegmentDTOs() != null) {
			for (FlightSegmentDTO fltSeg : getFlightSegmentDTOs()) {
				thisFltIds += fltSeg.getFlightId();
			}
			AvailableIBOBFlightSegment comObj = (AvailableIBOBFlightSegment) obj;

			if (comObj.getFlightSegmentDTOs() != null) {
				for (FlightSegmentDTO fltSeg : comObj.getFlightSegmentDTOs()) {
					objFltIds += fltSeg.getFlightId();
				}
			}
			return thisFltIds.equals(objFltIds);
		}

		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime + (getFlightSegmentDTOs() == null ? 0 : getFlightSegmentDTOs().hashCode());
		return result;
	}

	protected final Map<String, List<Double>> totalCharges = new HashMap<String, List<Double>>();

	protected Map<String, LinkedHashMap<Integer, SegmentSeatDistsDTO>> segmentSeatDistributionMap = new HashMap<String, LinkedHashMap<Integer, SegmentSeatDistsDTO>>();

	/** Flexi Charges collection without effective amount set */
	private final Map<String, Collection<FlexiRuleDTO>> flexiCharges = new HashMap<String, Collection<FlexiRuleDTO>>();

	private final Map<String, Collection<FlexiRuleDTO>> effectiveFlexiCharges = new HashMap<String, Collection<FlexiRuleDTO>>();

	// TODO charith hope connected flight is served by a single cabin class
	private String cabinClassCode;

	private Map<FarePriceOnd, BigDecimal> minimumFareByPriceOndType;

	public abstract boolean isInboundFlightSegment();

	public abstract void setInboundFlightSegment(boolean isInboundFlightSegment);

	protected Set<String> matchingCombinationKeys = new HashSet<>();

	private Integer requestedOndSequence;

	/**
	 * Inject the fare percentage carried by each FareSummaryDTO
	 * 
	 * @param fareSummaryDTO
	 */
	protected void injectFarePercentage(String logicalCCtype, FareSummaryDTO fareSummaryDTO) {
		SelectedFlightDTO selectedFlightDTO = getSelectedFlightDTO();
		// TODO charith : need to review this
		if (selectedFlightDTO != null) {
			fareSummaryDTO.setFarePercentage(getFarePercentage(selectedFlightDTO.getFareType(), getFareType(logicalCCtype)));
		}
	}

	private int getFarePercentage(int totalJourneyFareType, int ondFareType) {
		return FarePercentageCalculator.getFarePercentage(totalJourneyFareType, ondFareType);
	}

	/**
	 * Return the FareType Map wiht selected Fare type information.
	 * 
	 * @return fareType
	 */
	public Map<String, Integer> getSelectedeFareTypeMap() {
		return fareType;
	}

	@Override
	public void setSeatsAvailable(String logicalCCtype, boolean b) {
		this.seatsAvailable.put(logicalCCtype, b);
	}

	@Override
	/***
	 * TODO charith this is bad. for connection flights if we have to serve by different logical cabin class, we can't
	 * keep this at this point.
	 */
	public boolean isSeatsAvailable(String logicalCCtype) {
		return this.seatsAvailable.get(logicalCCtype) != null && this.seatsAvailable.get(logicalCCtype);
	}

	public void cleanFares() {
		fare.clear();
		fareType.clear();
		selectedLogicalCabinClass = null;
		seatsAvailable.clear();
	}

	@Override
	public FareSummaryDTO getFare(String logicalCCtype) {
		return this.fare.get(logicalCCtype);
	}

	@Override
	public void setFare(String logicalCCtype, FareSummaryDTO fare) {
		this.fare.put(logicalCCtype, fare);
	}

	@Override
	public void removeFare(String logicalCCtype) {
		this.fare.remove(logicalCCtype);
	}

	public void removeFareType(String logicalCCtype) {
		this.fareType.remove(logicalCCtype);
	}

	public Collection<QuotedChargeDTO> getCharges(String logicalCCtype) {
		return charges.get(logicalCCtype);
	}

	public void setCharges(String logicalCCtype, Collection<QuotedChargeDTO> charges) {
		this.charges.put(logicalCCtype, charges);
	}

	public void setInterlineBookingClassCharge(String logicalCabinClass, String externalBookingClass, List<Double> charges) {
		Map<String, List<Double>> bookingClassWiseCharge = interlineBookingClassWiseCharges.get(logicalCabinClass);
		if (bookingClassWiseCharge == null) {
			interlineBookingClassWiseCharges.put(logicalCabinClass, new HashMap<String, List<Double>>());
		}
		interlineBookingClassWiseCharges.get(logicalCabinClass).put(externalBookingClass, charges);
	}

	public List<Double> getChargesForInterlineBookingClass(String logicalCabinClass, String externalBookingClass) {
		if (interlineBookingClassWiseCharges.get(logicalCabinClass) != null) {
			return interlineBookingClassWiseCharges.get(logicalCabinClass).get(externalBookingClass);
		}
		return null;
	}

	@Override
	public Integer getFareType(String logicalCCtype) {
		Integer fType = fareType.get(logicalCCtype);
		return fType == null ? FareTypes.NO_FARE : fType;
	}

	public void setFareType(String logicalCCtype, Integer fareType) {
		this.fareType.put(logicalCCtype, fareType);
	}

	public Collection<QuotedChargeDTO> geteffectiveCharges(String logicalCCtype) {
		return effectiveCharges.get(logicalCCtype);
	}

	void seteffectiveCharges(String logicalCCtype, Collection<QuotedChargeDTO> quotedChargeDTOs) {
		this.effectiveCharges.put(logicalCCtype, quotedChargeDTOs);
	}

	void removeEffectiveCharges(String logicalCCtype) {
		this.effectiveCharges.remove(logicalCCtype);
	}

	@Override
	public Map<String, Double> getTotalFareMap(String logicalCCtype) {
		return totalFareMap.get(logicalCCtype);
	}

	void setTotalFareMap(String logicalCCtype, Map<String, Double> fareMap) {
		this.totalFareMap.put(logicalCCtype, fareMap);
	}

	public abstract void setSelectedFlightDTO(SelectedFlightDTO selectedFlightDTO);

	public SelectedFlightDTO getSelectedFlightDTO() {
		return selectedFlightDTO;
	}

	@Override
	public Collection<OndFareDTO> getFareChargesSeatsSegmentsDTOs(String logicalCCtype) {
		return fareChargesSeatsSegmentsDTOs.get(logicalCCtype);
	}

	void setFareChargesSeatsSegmentsDTOs(String logicalCCtype, Collection<OndFareDTO> ondFareDTOs) {
		fareChargesSeatsSegmentsDTOs.put(logicalCCtype, ondFareDTOs);
	}

	void removeFareChargesSeatsSegmentsDTOs(String logicalCCtype) {
		fareChargesSeatsSegmentsDTOs.remove(logicalCCtype);
	}

	@Override
	public List<FlightSegmentDTO> getFlightSegmentDTOs() {
		return flightSegmentDTOs;
	}

	public void removeTotalCharges(String logicalCCtype) {
		totalCharges.remove(logicalCCtype);
	}

	public Collection<FlexiRuleDTO> getFlexiCharges(String logicalCCtype) {
		return flexiCharges.get(logicalCCtype);
	}

	public void setFlexiCharges(String logicalCCtype, Collection<FlexiRuleDTO> flexiCharges) {
		this.flexiCharges.put(logicalCCtype, flexiCharges);
	}

	public void setEffectiveFlexiCharges(String logicalCCtype, Collection<FlexiRuleDTO> quotedChargeDTOs) {
		if (quotedChargeDTOs != null) {
			Collection<FlexiRuleDTO> clonedFlexiRuleDTOs = new ArrayList<FlexiRuleDTO>();
			if (getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				for (FlexiRuleDTO flexiRuleDTO : quotedChargeDTOs) {
					FlexiRuleDTO clonedFlexiRuleDTO = flexiRuleDTO.clone();
					clonedFlexiRuleDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT,
							clonedFlexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT) / 2);
					clonedFlexiRuleDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD,
							clonedFlexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD) / 2);
					clonedFlexiRuleDTO.setEffectiveChargeAmount(PaxTypeTO.INFANT,
							clonedFlexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT) / 2);
					clonedFlexiRuleDTOs.add(clonedFlexiRuleDTO);
				}

			} else {
				clonedFlexiRuleDTOs.addAll(quotedChargeDTOs);
			}
			this.effectiveFlexiCharges.put(logicalCCtype, clonedFlexiRuleDTOs);
		} else {
			this.effectiveFlexiCharges.put(logicalCCtype, null);
		}
	}

	public Collection<FlexiRuleDTO> getEffectiveFlexiCharges(String logicalCCtype) {
		return effectiveFlexiCharges.get(logicalCCtype);
	}

	public void removeEffectiveFlexiCharges(String logicalCCtype) {
		effectiveFlexiCharges.remove(logicalCCtype);
	}

	public abstract String getOndCode();

	@Override
	public Collection<String> getAvailableLogicalCabinClasses() {
		Set<String> availableLogicalCabinClasses = new HashSet<String>();
		for (String logicalCabinClass : seatsAvailable.keySet()) {
			if (seatsAvailable.get(logicalCabinClass)) {
				availableLogicalCabinClasses.add(logicalCabinClass);
			}
		}
		return availableLogicalCabinClasses;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getSelectedLogicalCabinClass() {
		return selectedLogicalCabinClass;
	}

	public void setSelectedLogicalCabinClass(String selectedLogicalCabinClass) {
		this.selectedLogicalCabinClass = selectedLogicalCabinClass;
	}

	public boolean isWaitListed() {
		return waitListed;
	}

	public void setWaitListed(boolean waitListed) {
		this.waitListed = waitListed;
	}

	@Override
	public int compareTo(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		if (getFlightSegmentDTOs() != null && availableIBOBFlightSegment.getFlightSegmentDTOs() != null) {
			int minDirectFlights = Math.min(getFlightSegmentDTOs().size(),
					availableIBOBFlightSegment.getFlightSegmentDTOs().size());
			Collections.sort(getFlightSegmentDTOs());
			Collections.sort(availableIBOBFlightSegment.getFlightSegmentDTOs());
			for (int i = 0; i < minDirectFlights; i++) {
				if (getFlightSegmentDTOs().get(i).compareTo(availableIBOBFlightSegment.getFlightSegmentDTOs().get(i)) < 0) {
					return -1;
				}
			}
			return 1;
		}
		return -1;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	/**
	 * Shallow copy the AvailableIBOBFlightSegment. Delegating implementation to subclasses since some of the properties
	 * that should be copied exist only in sub classes.
	 */
	public abstract AvailableIBOBFlightSegment getShallowCopy();

	public Date getFirstDepartureDateZulu() {
		if (getFlightSegmentDTOs().size() > 0) {
			return getFlightSegmentDTOs().get(0).getDepartureDateTimeZulu();
		}
		return null;
	}

	public Date getLastArrivalDateZulu() {
		if (getFlightSegmentDTOs().size() > 0) {
			FlightSegmentDTO lastFS = getFlightSegmentDTOs().get(getFlightSegmentDTOs().size() - 1);
			return lastFS.getArrivalDateTimeZulu();
		}
		return null;
	}

	public boolean isUnTouchedOnd() {
		return unTouchedOnd;
	}

	public void setUnTouchedOnd(boolean unTouchedOnd) {
		this.unTouchedOnd = unTouchedOnd;
	}

	public int getSubJourneyGroup() {
		return subJourneyGroup;
	}

	public void setSubJourneyGroup(int subJourneyGroup) {
		this.subJourneyGroup = subJourneyGroup;

	}

	public Map<String, List<BundledFareLiteDTO>> getAvailableBundledFares() {
		if (availableBundledFares == null) {
			availableBundledFares = new HashMap<String, List<BundledFareLiteDTO>>();
		}
		return availableBundledFares;
	}

	public void setAvailableBundledFares(Map<String, List<BundledFareLiteDTO>> availableBundledFares) {
		this.availableBundledFares = availableBundledFares;
	}

	public void addAvailableBundledFares(String logicalCC, List<BundledFareLiteDTO> bundledFares) {
		getAvailableBundledFares().put(logicalCC, bundledFares);
	}

	public BigDecimal getSelectedBundleFareAmountWithTaxes() {
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getSelectedBundledFare() != null) {
			amount = getSelectedBundledFare().getPerPaxBundledFee();
			if (getAvailableBundledFares() != null) {
				outer: for (String logicalCabin : getAvailableBundledFares().keySet()) {
					for (BundledFareLiteDTO bfLite : getAvailableBundledFares().get(logicalCabin)) {
						if (bfLite.getBundleFarePeriodId() == getSelectedBundledFare().getBundledFarePeriodId()) {
							amount = AccelAeroCalculator.add(amount, bfLite.getApplicableTaxPortion());
							break outer;
						}
					}
				}
			}
		}
		return amount;
	}

	public BundledFareDTO getSelectedBundledFare() {
		return selectedBundledFare;
	}

	public void setSelectedBundledFare(BundledFareDTO selectedBundledFare) {
		this.selectedBundledFare = selectedBundledFare;
	}

	/**
	 * Checks if bundled fare is selected & bundled fare offers mentioned service FOC.
	 * 
	 * @param serviceName
	 *            is string value of respective EXTERNAL_CHARGE
	 * @return
	 */
	public boolean isServiceIncludedInBundledFare(String serviceName) {
		if (this.selectedBundledFare != null) {
			return this.selectedBundledFare.isServiceIncluded(serviceName);
		}

		return false;
	}

	public void removeUnmatchedBundledFare() {
		if (selectedBundledFare != null && !this.isFlown()) {
			if (this.isDirectFlight()) {
				FareSummaryDTO fareSummaryDTO = ((AvailableFlightSegment) this).getFare(selectedLogicalCabinClass);
				if (fareSummaryDTO != null && selectedBundledFare.getBookingClasses() != null
						&& !selectedBundledFare.getBookingClasses().isEmpty()
						&& !selectedBundledFare.getBookingClasses().contains(fareSummaryDTO.getBookingClassCode())) {
					this.selectedBundledFare = null;
				}
			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) this;
				FareSummaryDTO fareSummaryDTO = availableConnectedFlight.getFare(selectedLogicalCabinClass);
				if (fareSummaryDTO != null) {
					if (selectedBundledFare.getBookingClasses() != null && !selectedBundledFare.getBookingClasses().isEmpty()
							&& !selectedBundledFare.getBookingClasses().contains(fareSummaryDTO.getBookingClassCode())) {
						this.selectedBundledFare = null;
					}
				} else {
					for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
						if (!availableFlightSegment.isBusSegment()) {
							fareSummaryDTO = availableFlightSegment.getFare(selectedLogicalCabinClass);
							if (fareSummaryDTO != null && selectedBundledFare.getBookingClasses() != null
									&& !selectedBundledFare.getBookingClasses().isEmpty()
									&& !selectedBundledFare.getBookingClasses().contains(fareSummaryDTO.getBookingClassCode())) {
								this.selectedBundledFare = null;
							}
						}
					}
				}
			}
		}
	}

	public boolean isFlown() {
		boolean isFlown = false;
		String selectedLogicalCC = this.getSelectedLogicalCabinClass();
		if (this.isDirectFlight()) {
			Collection<SeatDistribution> seatDistributions = ((AvailableFlightSegment) this)
					.getSeatsAvailability(selectedLogicalCC);
			if (seatDistributions != null) {
				for (SeatDistribution seatDistribution : seatDistributions) {
					if (seatDistribution.isFlownOnd()) {
						isFlown = true;
						break;
					}
				}
			}
		} else {
			for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) this).getAvailableFlightSegments()) {
				Collection<SeatDistribution> seatDistributions = availableFlightSegment.getSeatsAvailability(selectedLogicalCC);
				if (seatDistributions != null) {
					for (SeatDistribution seatDistribution : seatDistributions) {
						if (seatDistribution.isFlownOnd()) {
							isFlown = true;
							break;
						}
					}
				}
			}
		}

		return isFlown;
	}

	public String getSelectedFareChargeRatio(String logicalCCtype) {
		FareSummaryDTO selectedFare = getFare(logicalCCtype);
		if (selectedFare != null && selectedFare.getRatioCategory() != null) {
			return selectedFare.getRatioCategory();
		}
		return null;
	}

	public boolean isAvailableBundledFare(Integer preferredBundledFarePeriodId) {
		for (List<BundledFareLiteDTO> logicalCCWiseBundles : getAvailableBundledFares().values()) {
			for (BundledFareLiteDTO bundle : logicalCCWiseBundles) {
				if (bundle.getBundleFarePeriodId().intValue() == preferredBundledFarePeriodId.intValue()) {
					return true;
				}
			}
		}
		return false;
	}

	public BundledFareLiteDTO getDefaultBundledFare() {
		for (List<BundledFareLiteDTO> logicalCCWiseBundles : getAvailableBundledFares().values()) {
			for (BundledFareLiteDTO bundle : logicalCCWiseBundles) {
				if (bundle.isDefault()) {
					return bundle;
				}
			}
		}
		return null;
	}

	public void setMinimumFareByPriceOnd(FarePriceOnd priceOndType, double totalMinimumFare) {
		getMinimumFareByPriceOnd().put(priceOndType, new BigDecimal(totalMinimumFare));
	}

	public Map<FarePriceOnd, BigDecimal> getMinimumFareByPriceOnd() {
		if (this.minimumFareByPriceOndType == null) {
			this.minimumFareByPriceOndType = new HashMap<FarePriceOnd, BigDecimal>();
		}
		return this.minimumFareByPriceOndType;
	}

	public void addMatchingCombinationKey(String matchingCombinationKey) {
		matchingCombinationKeys.add(matchingCombinationKey);
	}

	public Set<String> getMatchingCombinationKeys() {
		return matchingCombinationKeys;
	}

	public Map<String, List<String>> getServiceTaxExcludedCharges() {
		return serviceTaxExcludedCharges;
	}

	public Map<String, QuotedChargeDTO> getServiceTaxes() {
		return serviceTaxes;
	}

	public void setServiceTaxes(Map<String, QuotedChargeDTO> serviceTaxes) {
		this.serviceTaxes = serviceTaxes;
	}

	public void setServiceTaxExcludedCharges(Map<String, List<String>> serviceTaxExcludedCharges) {
		this.serviceTaxExcludedCharges = serviceTaxExcludedCharges;
	}

	public abstract Collection<QuotedChargeDTO> getAllEffectiveCharges(String logicalCCtype);

	public void setRequestedOndSequence(Integer requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public Integer getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public boolean isSplitOperationForBusInvoked() {
		return splitOperationForBusInvoked;
	}

	public void setSplitOperationForBusInvoked(boolean isSplitOperationForBusInvoked) {
		this.splitOperationForBusInvoked = isSplitOperationForBusInvoked;
	}
	
}
