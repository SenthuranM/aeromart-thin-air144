package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;

public class InvRollForwardCriteriaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static int OVERRIDE_NONE = 0;

	public static int OVERRIDE_ALL = 1;

	public static int OVERRIDE_NOT_HAVING_RESERVATION_ONLY = 2;

	public static int SKIP_SPLITING_ALLOCATIONS = 0;

	public static int COPY_SEG_ALLOCS_WITHOUT_OVERRIDING_SEATS = 1;

	public static int COPY_SEG_ALLOCS_AND_OVERRIDE_SEATS = 2;

	private int sourceFlightId;

	private String sourceCabinClassCode;

	private int bcInventoryOverridingLevel = OVERRIDE_NOT_HAVING_RESERVATION_ONLY;

	private boolean deleteTargetBCInventoriesFirst = false;

	private InvRollForwardFlightsSearchCriteria invRollForwardFlightsSearchCriteria;

	private int rollForwardCurtail = 0;

	private int rollForwardOversell = 0;

	private int rollForwardWaitListing = 0;

	private boolean isRollFwdBCPriority = false;

	private boolean isRollFwdBCStatus = false;

	private boolean isRemoveOddTargetBCs = false;

	private int createSegAllocs = 0;
	
	private Collection<Integer> flights; 

	public boolean isRemoveOddTargetBCs() {
		return isRemoveOddTargetBCs;
	}

	public void setRemoveOddTargetBCs(boolean isRemoveOddTargetBCs) {
		this.isRemoveOddTargetBCs = isRemoveOddTargetBCs;
	}

	public boolean isRollFwdBCPriority() {
		return isRollFwdBCPriority;
	}

	public void setRollFwdBCPriority(boolean isRollFwdBCPriority) {
		this.isRollFwdBCPriority = isRollFwdBCPriority;
	}

	public boolean isRollFwdBCStatus() {
		return isRollFwdBCStatus;
	}

	public void setRollFwdBCStatus(boolean isRollFwdBCStatus) {
		this.isRollFwdBCStatus = isRollFwdBCStatus;
	}

	public int getRollForwardCurtail() {
		return rollForwardCurtail;
	}

	public void setRollForwardCurtail(int rollForwardCurtail) {
		this.rollForwardCurtail = rollForwardCurtail;
	}

	public int getRollForwardOversell() {
		return rollForwardOversell;
	}

	public void setRollForwardOversell(int rollForwardOversell) {
		this.rollForwardOversell = rollForwardOversell;
	}

	public InvRollForwardFlightsSearchCriteria getInvRollForwardFlightsSearchCriteria() {
		return invRollForwardFlightsSearchCriteria;
	}

	public void setInvRollForwardFlightsSearchCriteria(InvRollForwardFlightsSearchCriteria invRollForwardFlightsSearchCriteria) {
		this.invRollForwardFlightsSearchCriteria = invRollForwardFlightsSearchCriteria;
	}

	public int getBcInventoryOverridingLevel() {
		return bcInventoryOverridingLevel;
	}

	public void setBcInventoryOverridingLevel(int bcInventoryOverridingLevel) {
		this.bcInventoryOverridingLevel = bcInventoryOverridingLevel;
	}

	public boolean isDeleteTargetBCInventoriesFirst() {
		return deleteTargetBCInventoriesFirst;
	}

	public void setDeleteTargetBCInventoriesFirst(boolean deleteTargetBCInventoriesFirst) {
		this.deleteTargetBCInventoriesFirst = deleteTargetBCInventoriesFirst;
	}

	// public boolean isRollFixedBCAllocs() {
	// return rollFixedBCAllocs;
	// }
	//
	// public void setRollFixedBCAllocs(boolean rollFixedBCAllocs) {
	// this.rollFixedBCAllocs = rollFixedBCAllocs;
	// }
	//
	// public boolean isRollNonStadardBCAllocs() {
	// return rollNonStadardBCAllocs;
	// }
	//
	// public void setRollNonStadardBCAllocs(boolean rollNonStadardBCAllocs) {
	// this.rollNonStadardBCAllocs = rollNonStadardBCAllocs;
	// }
	//
	// public boolean isRollStandardBCAllocs() {
	// return rollStandardBCAllocs;
	// }
	//
	// public void setRollStandardBCAllocs(boolean rollStandardBCAllocs) {
	// this.rollStandardBCAllocs = rollStandardBCAllocs;
	// }

	public String getSourceCabinClassCode() {
		return sourceCabinClassCode;
	}

	public void setSourceCabinClassCode(String sourceCabinClassCode) {
		this.sourceCabinClassCode = sourceCabinClassCode;
	}

	public int getSourceFlightId() {
		return sourceFlightId;
	}

	public void setSourceFlightId(int sourceFlightId) {
		this.sourceFlightId = sourceFlightId;
	}

	public int getCreateSegAllocs() {
		return createSegAllocs;
	}

	public void setCreateSegAllocs(int createSegAllocs) {
		this.createSegAllocs = createSegAllocs;
	}

	public int getRollForwardWaitListing() {
		return rollForwardWaitListing;
	}

	public void setRollForwardWaitListing(int rollForwardWaitListing) {
		this.rollForwardWaitListing = rollForwardWaitListing;
	}

	public Collection<Integer> getFlights() {
		return flights;
	}

	public void setFlights(Collection<Integer> flights) {
		this.flights = flights;
	}

	@Override
	public String toString() {
		final String BR = "\n";
		StringBuilder strRollFwdCriteria = new StringBuilder(BR);

		strRollFwdCriteria.append(" Source Flight Id is : " + getSourceFlightId()).append(BR);
		strRollFwdCriteria.append(" Source Cabin class is : " + getSourceCabinClassCode()).append(BR);

		if (getInvRollForwardFlightsSearchCriteria() != null) {
			strRollFwdCriteria.append(" Rollforward Search Criteria : " + getInvRollForwardFlightsSearchCriteria().toString())
					.append(BR);
		}

		int overridelevel = this.getBcInventoryOverridingLevel();
		if (overridelevel == 0) {
			strRollFwdCriteria.append(" Selected Override Level : OVERRIDE_NONE ");
		} else if (overridelevel == 1) {
			strRollFwdCriteria.append(" Selected Override Level : OVERRIDE_ALL ");
		} else if (overridelevel == 2) {
			strRollFwdCriteria.append(" Selected Override Level : OVERRIDE_NOT_HAVING_RESERVATION_ONLY ");
		}
		strRollFwdCriteria.append(BR);
		if (this.isDeleteTargetBCInventoriesFirst()) {
			strRollFwdCriteria.append(" Delete Target BC Inventories First : True ");
		} else {
			strRollFwdCriteria.append(" Delete Target BC Inventories First : False ");
		}
		strRollFwdCriteria.append(BR);

		if (this.isRemoveOddTargetBCs()) {
			strRollFwdCriteria.append(" Remove additional booking classes : True ");
		} else {
			strRollFwdCriteria.append(" Remove additional booking classes : False ");
		}
		strRollFwdCriteria.append(BR);
		strRollFwdCriteria.append(" Curtail : ").append(getCurOverWaitState(getRollForwardCurtail()));
		strRollFwdCriteria.append(BR);
		strRollFwdCriteria.append(" Oversell : ").append(getCurOverWaitState(getRollForwardOversell()));
		strRollFwdCriteria.append(BR);
		strRollFwdCriteria.append(" Waitlisting : ").append(getCurOverWaitState(getRollForwardWaitListing()));
		strRollFwdCriteria.append(BR);

		if (this.isRollFwdBCPriority()) {
			strRollFwdCriteria.append(" Copy Priority Flag : True ").append(BR);
		} else {
			strRollFwdCriteria.append(" Copy Priority Flag : false ").append(BR);
		}

		if (this.isRollFwdBCStatus()) {
			strRollFwdCriteria.append(" Copy Status Flag : True ").append(BR);
		} else {
			strRollFwdCriteria.append(" Copy Status Flag : False ").append(BR);
		}

		return strRollFwdCriteria.toString();
	}

	private String getCurOverWaitState(int selected) {
		if (selected == 1) {
			return " If target is zero ";
		} else if (selected == 2) {
			return " Override and update ";
		} else {
			return " Ignore ";
		}
	}

}
