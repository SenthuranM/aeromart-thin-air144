package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogicalCabinFilteredBucketDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String logicalCabinClass;

	private final List<AvailableBCAllocationSummaryDTO> cheapestBuckets = new ArrayList<AvailableBCAllocationSummaryDTO>();

	private final Map<Integer, List<AvailableBCAllocationSummaryDTO>> ondwiseCheapestBuckets = new HashMap<Integer, List<AvailableBCAllocationSummaryDTO>>();

	public LogicalCabinFilteredBucketDTO(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public List<AvailableBCAllocationSummaryDTO> getCheapestBuckets() {
		if (cheapestBuckets.size() == 0 && ondwiseCheapestBuckets.size() > 0) {
			List<Integer> keys = new ArrayList<Integer>(ondwiseCheapestBuckets.keySet());
			Collections.sort(keys);
			for (Integer key : keys) {
				cheapestBuckets.addAll(ondwiseCheapestBuckets.get(key));
			}
		}
		return cheapestBuckets;
	}

	public List<AvailableBCAllocationSummaryDTO> getOndAllocations(int ondSequence) {
		if (ondwiseCheapestBuckets.containsKey(ondSequence)) {
			return ondwiseCheapestBuckets.get(ondSequence);
		}
		return null;
	}

	public void addCheapestAllocations(int ondCount, AvailableBCAllocationSummaryDTO allocation) {
		addCheapestAllocations(ondCount, Arrays.asList(allocation));
	}

	public void addCheapestAllocations(int ondCount, List<AvailableBCAllocationSummaryDTO> allocations) {
		if (!ondwiseCheapestBuckets.containsKey(ondCount)) {
			ondwiseCheapestBuckets.put(ondCount, new ArrayList<AvailableBCAllocationSummaryDTO>());
		}
		ondwiseCheapestBuckets.get(ondCount).addAll(allocations);
	}

	@Override
	public LogicalCabinFilteredBucketDTO clone() {
		LogicalCabinFilteredBucketDTO logicalCabinFilteredBucketDTO = new LogicalCabinFilteredBucketDTO(getLogicalCabinClass());
		for (Integer ondSequence : ondwiseCheapestBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : ondwiseCheapestBuckets.get(ondSequence)) {
				logicalCabinFilteredBucketDTO.addCheapestAllocations(ondSequence, availableBCAllocationSummaryDTO.clone());
			}
		}
		return logicalCabinFilteredBucketDTO;
	}
}
