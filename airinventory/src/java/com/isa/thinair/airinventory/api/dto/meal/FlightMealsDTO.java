package com.isa.thinair.airinventory.api.dto.meal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FlightMealsDTO implements Serializable {

	private static final long serialVersionUID = 3871200662326079316L;
	private Integer flightId;
	private String flightNumber;
	private String segmentCode;
	private Date departureTime;
	private String notifyEmail;

	private Map<String, FlightMealClassDTO> mapCabinClassFlightMealClassDTO;

	private List<FlightMealDTO> flightMealsList;

	/**
	 * @return the flightId
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the flightMealsList
	 */
	public List<FlightMealDTO> getFlightMealsList() {
		return flightMealsList;
	}

	/**
	 * @param flightMealsList
	 *            the flightMealsList to set
	 */
	public void addFlightMeal(FlightMealDTO flightMealDTO) {
		if (this.flightMealsList == null) {
			this.flightMealsList = new ArrayList<FlightMealDTO>();
		}
		this.flightMealsList.add(flightMealDTO);
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Collection<FlightMealClassDTO> getFlightMealClassDTOs() {
		return mapCabinClassFlightMealClassDTO.values();
	}

	public Map<String, FlightMealClassDTO> getMapCabinClassFlightMealClassDTO() {
		return mapCabinClassFlightMealClassDTO;
	}

	public void setMapCabinClassFlightMealClassDTO(Map<String, FlightMealClassDTO> mapCabinClassFlightMealClassDTO) {
		this.mapCabinClassFlightMealClassDTO = mapCabinClassFlightMealClassDTO;
	}

	public String getNotifyEmail() {
		return notifyEmail;
	}

	public void setNotifyEmail(String notifyEmail) {
		this.notifyEmail = notifyEmail;
	}
}
