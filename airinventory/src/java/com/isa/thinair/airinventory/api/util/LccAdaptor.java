package com.isa.thinair.airinventory.api.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAutomaticCheckins;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedAutoCheckin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedBaggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedFlexi;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedInsurance;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedMeal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSelectedAncillaryRS;
import com.isa.connectivity.profiles.maxico.v1.exposed.dto.LCCSuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class LccAdaptor {
    public static AASelectedAncillaryRQ toAASelectedAncillaryRQ(LCCSelectedAncillaryRQ lccSelectedAncillaryRQ)
            throws Exception {
        Map<String, AASelectedAncillaryRQ> aaSelectedAnciMap = new HashMap<String, AASelectedAncillaryRQ>();

        List<SelectedSeat> selectedSeats = lccSelectedAncillaryRQ.getSelectedSeats();
        List<SelectedMeal> selectedMeals = lccSelectedAncillaryRQ.getSelectedMeals();
        List<SelectedBaggage> selectedBaggages = lccSelectedAncillaryRQ.getSelectedBaggages();
        List<SelectedInsurance> selectedInsurances = lccSelectedAncillaryRQ.getSelectedInsurances();
        List<SelectedFlexi> selectedFlexis = lccSelectedAncillaryRQ.getSelectedFlexis();
        List<SelectedSSR> selectedAPServices = lccSelectedAncillaryRQ.getSelectedAirportServices();
        List<SelectedSSR> selectedInflightServices = lccSelectedAncillaryRQ.getSelectedInflightServices();
        List<SelectedSSR> selectedAPTransfer = lccSelectedAncillaryRQ.getSelectedAirportTransfer();
		List<SelectedAutoCheckin> selectedAutoCheckins = lccSelectedAncillaryRQ.getSelectedAutoCheckins();

        AASelectedAncillaryRQ aaSelectedAncillaryRQ = new AASelectedAncillaryRQ();
        for (SelectedSeat selectedSeat : selectedSeats) {
            BookingFlightSegment flightSegment = selectedSeat.getFlightSegment();
            BookingFlightSegment updatedFlightSegment = getBookingFlightSegmentFromRPH(flightSegment);

            if (selectedSeat.getSeatNumber().size() > 0) {
                SelectedSeat updatedSelectedSeat = new SelectedSeat();
                updatedSelectedSeat.setFlightSegment(updatedFlightSegment);
                updatedSelectedSeat.getSeatNumber().addAll(selectedSeat.getSeatNumber());
                updatedSelectedSeat.setOfferedAnciTemplateID(selectedSeat.getOfferedAnciTemplateID());
                aaSelectedAncillaryRQ.getSelectedSeats().add(updatedSelectedSeat);
            }
        }

        for (SelectedMeal selectedMeal : selectedMeals) {
            BookingFlightSegment flightSegment = selectedMeal.getFlightSegment();
            BookingFlightSegment updatedFlightSegment = getBookingFlightSegmentFromRPH(flightSegment);

            if (selectedMeal.getMealCode().size() > 0) {
                SelectedMeal updatedSelectedMeal = new SelectedMeal();
                updatedSelectedMeal.setFlightSegment(updatedFlightSegment);
                updatedSelectedMeal.getMealCode().addAll(selectedMeal.getMealCode());
                updatedSelectedMeal.setOfferedAnciTemplateID(selectedMeal.getOfferedAnciTemplateID());
                aaSelectedAncillaryRQ.getSelectedMeals().add(updatedSelectedMeal);
            }
        }

        for (SelectedBaggage selectedBaggage : selectedBaggages) {
            BookingFlightSegment flightSegment = selectedBaggage.getFlightSegment();
            BookingFlightSegment updatedFlightSegment =
                    getBookingFlightSegmentFromRPH(flightSegment);

            if (selectedBaggage.getBaggageName().size() > 0) {
                SelectedBaggage updatedSelectedBaggage = new SelectedBaggage();
                updatedSelectedBaggage.setFlightSegment(updatedFlightSegment);
                updatedSelectedBaggage.getBaggageName().addAll(selectedBaggage.getBaggageName());
                updatedSelectedBaggage.getBaggageTemplateId().addAll(selectedBaggage.getBaggageTemplateId());
                aaSelectedAncillaryRQ.getSelectedBaggages().add(updatedSelectedBaggage);
            }
        }

        if (selectedInsurances != null && !selectedInsurances.isEmpty()) {
            for (SelectedInsurance selectedInsurance : selectedInsurances) {
                String flightRefNumber = selectedInsurance.getFlightRefNumber();
                if (!selectedInsurance.getInsuranceReferences().isEmpty()) {
                    String flightSegmentId = extractFlightSegmentId(flightRefNumber);
                    SelectedInsurance updatedSelectedInsurance = new SelectedInsurance();
                    updatedSelectedInsurance.setFlightRefNumber(flightSegmentId);
                    updatedSelectedInsurance.getInsuranceReferences().addAll(selectedInsurance.getInsuranceReferences());
                    aaSelectedAncillaryRQ.getSelectedInsurances().add(updatedSelectedInsurance);
                }
            }
        }

        for (SelectedFlexi selectedFlexi : selectedFlexis) {
            String flightRefNumber = selectedFlexi.getFlightRefNumber();

            if (selectedFlexi.getFlexiRateIds() != null && selectedFlexi.getFlexiRateIds().size() > 0) {
                // String flightSegmentId = FlightRefNumber.extractFlightSegmentId(flightRefNumber);
                SelectedFlexi updatedSelectedFlexi = new SelectedFlexi();
                // updatedSelectedFlexi.setFlightRefNumber(flightSegmentId);
                updatedSelectedFlexi.setFlightRefNumber(flightRefNumber);
                updatedSelectedFlexi.getFlexiRateIds().addAll(selectedFlexi.getFlexiRateIds());
                aaSelectedAncillaryRQ.getSelectedFlexis().add(updatedSelectedFlexi);
            }
        }

        if (selectedAPServices != null) {
            for (SelectedSSR selectedAPS : selectedAPServices) {
                BookingFlightSegment flightSegment = selectedAPS.getFlightSegment();
                BookingFlightSegment updatedFlightSegment = getBookingFlightSegmentFromRPH(flightSegment);

                if (selectedAPS.getSsrCode().size() > 0) {
                    SelectedSSR updatedSelectedAPS = new SelectedSSR();
                    updatedSelectedAPS.setFlightSegment(updatedFlightSegment);
                    updatedSelectedAPS.getSsrCode().addAll(selectedAPS.getSsrCode());
                    aaSelectedAncillaryRQ.getSelectedAirportServices().add(updatedSelectedAPS);
                }
            }
        }

        if (selectedInflightServices != null) {
            for (SelectedSSR selectedInflightService : selectedInflightServices) {
                BookingFlightSegment flightSegment = selectedInflightService.getFlightSegment();
                BookingFlightSegment updatedFlightSegment = getBookingFlightSegmentFromRPH(flightSegment);

                if (selectedInflightService.getSsrCode().size() > 0) {
                    SelectedSSR updatedSelectedSSR = new SelectedSSR();
                    updatedSelectedSSR.setFlightSegment(updatedFlightSegment);
                    updatedSelectedSSR.getSsrCode().addAll(selectedInflightService.getSsrCode());
                    aaSelectedAncillaryRQ.getSelectedInflightServices().add(updatedSelectedSSR);
                }
            }
        }

        if (selectedAPTransfer != null) {
            for (SelectedSSR selectedAPT : selectedAPTransfer) {
                BookingFlightSegment flightSegment = selectedAPT.getFlightSegment();
                BookingFlightSegment updatedFlightSegment = getBookingFlightSegmentFromRPH(flightSegment);

                if (selectedAPT.getSsrCode().size() > 0) {
                    SelectedSSR updatedSelectedAPT = new SelectedSSR();
                    updatedSelectedAPT.setFlightSegment(updatedFlightSegment);
                    updatedSelectedAPT.getSsrCode().addAll(selectedAPT.getSsrCode());
                    aaSelectedAncillaryRQ.getSelectedAirportTransfers().add(updatedSelectedAPT);
                }
            }
        }

		if (selectedAutoCheckins != null) {
			for (SelectedAutoCheckin selectedAutoCheckin : selectedAutoCheckins) {
				BookingFlightSegment flightSegment = selectedAutoCheckin.getFlightSegment();
				BookingFlightSegment updatedFlightSegment = getBookingFlightSegmentFromRPH(flightSegment);

				if (selectedAutoCheckin.getAutoCheckinId().size() > 0) {
					SelectedAutoCheckin updatedSelectedAutoCheckin = new SelectedAutoCheckin();
					updatedSelectedAutoCheckin.setFlightSegment(updatedFlightSegment);
					updatedSelectedAutoCheckin.getAutoCheckinId().addAll(selectedAutoCheckin.getAutoCheckinId());
					aaSelectedAncillaryRQ.getSelectedAutoCheckins().add(updatedSelectedAutoCheckin);
				}
			}

		}

        return aaSelectedAncillaryRQ;
    }

    public static LCCSelectedAncillaryRS toLccSelectedAncillaryRS(AASelectedAncillaryRS aaSelectedAncillaryRS)
            throws ModuleException {
        LCCSelectedAncillaryRS selectedAncillaryRS = new LCCSelectedAncillaryRS();
        selectedAncillaryRS.setResponseAttributes(new LCCResponseAttributes());
        selectedAncillaryRS.getResponseAttributes().setSuccess(new LCCSuccess());

        for (FlightSegmentMeals flightSegmentMeals : aaSelectedAncillaryRS.getMeals()) {
            composeAndSetLCCFlightSegmentRPH(flightSegmentMeals.getFlightSegment());
            selectedAncillaryRS.getMeals().add(flightSegmentMeals);
        }

        for (FlightSegmentBaggages flightSegmentBaggages : aaSelectedAncillaryRS.getBaggages()) {
            composeAndSetLCCFlightSegmentRPH(flightSegmentBaggages.getFlightSegment());
            selectedAncillaryRS.getBaggages().add(flightSegmentBaggages);
        }

        for (FlightSegmentSeats flightSegmentSeats : aaSelectedAncillaryRS.getSeats()) {
            composeAndSetLCCFlightSegmentRPH(flightSegmentSeats.getFlightSegment());
            selectedAncillaryRS.getSeats().add(flightSegmentSeats);
        }

        // Insurance quotation will be set only from travel originating carrier. That means atleast in one
        // carrier quotation should not be null.
        if (aaSelectedAncillaryRS.getInsuranceQuotations() != null && !aaSelectedAncillaryRS.getInsuranceQuotations().isEmpty()) {
            selectedAncillaryRS.getInsuranceQuotations().addAll((aaSelectedAncillaryRS.getInsuranceQuotations()));
        }

        for (FlexiQuotation flexiQuotation : aaSelectedAncillaryRS.getFlexiQuotation()) {
            // FlightRefNumber.composeAndSetLCCFlightSegmentRPH(flightSegmentSeats.getFlightSegment(), new
            // Long(-1));
            flexiQuotation.setCarrierCode(AppSysParamsUtil.getCarrierCode());
            selectedAncillaryRS.getFlexiQuotation().add(flexiQuotation);
        }

        for (FlightSegmentAirportServiceRequests fltSegAPS : aaSelectedAncillaryRS.getAirportServices()) {
            composeAndSetLCCFlightSegmentRPH(fltSegAPS.getFlightSegment());
            selectedAncillaryRS.getAirportServices().add(fltSegAPS);
        }

        for (FlightSegmentSpecialServiceRequests fltSegSSR : aaSelectedAncillaryRS.getInflightServices()) {
            composeAndSetLCCFlightSegmentRPH(fltSegSSR.getFlightSegment());
            selectedAncillaryRS.getInflightServices().add(fltSegSSR);
        }
        
        for (FlightSegmentAirportServiceRequests fltSegAPT : aaSelectedAncillaryRS.getAirportTransfers()) {
            composeAndSetLCCFlightSegmentRPH(fltSegAPT.getFlightSegment());
            selectedAncillaryRS.getAirportTransfers().add(fltSegAPT);
        }

		// This is for automatic check-in ancillary
		for (FlightSegmentAutomaticCheckins fltSegAutomaticCheckins : aaSelectedAncillaryRS.getAutomaticCheckins()) {
			composeAndSetLCCFlightSegmentRPH(fltSegAutomaticCheckins.getFlightSegment());
			selectedAncillaryRS.getAutomaticCheckins().add(fltSegAutomaticCheckins);
		}


        return selectedAncillaryRS;
    }


    private static final String DATE_FORMAT_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    private static final String DELIM = "$";

    public static BookingFlightSegment getBookingFlightSegmentFromRPH(BookingFlightSegment flightSegment)
            throws ParseException {
        BookingFlightSegment extractedFlightSegment = new BookingFlightSegment();

        String flightRefNUmber = flightSegment.getFlightRefNumber();
        if (flightRefNUmber == null) {
            return flightSegment;
        }

        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_yyyyMMddHHmmss);
        String[] values = StringUtils.split(flightRefNUmber, DELIM);
        String segmentCode = values[1];

        extractedFlightSegment.setOperatingAirline(values[0]);
        extractedFlightSegment.setSegmentCode(segmentCode);
        extractedFlightSegment.setFlightRefNumber(values[2]);
        // extractedFlightSegment.setFlightRefNumber(flightRefNUmber);
        extractedFlightSegment.setDepatureDateTime(format.parse(values[3]));
        extractedFlightSegment.setDepatureDateTimeZulu(flightSegment.getDepatureDateTimeZulu());
        extractedFlightSegment.setArrivalDateTime(format.parse(values[4]));
        extractedFlightSegment.setCabinClassCode(flightSegment.getCabinClassCode());
        extractedFlightSegment.setLogicalCabinClassCode(flightSegment.getLogicalCabinClassCode());
        extractedFlightSegment.setFlightNumber(flightSegment.getFlightNumber());
        extractedFlightSegment.setAirportCode(flightSegment.getAirportCode());
        extractedFlightSegment.setReturnFlag(flightSegment.getReturnFlag());
        extractedFlightSegment.setBaggageONDGroupId(flightSegment.getBaggageONDGroupId());
        extractedFlightSegment.setBookingFlightRefNumber(flightSegment.getBookingFlightRefNumber());
        extractedFlightSegment.setJourneySeq(flightSegment.getJourneySeq());

        extractedFlightSegment.setAdultCount(flightSegment.getAdultCount());
        extractedFlightSegment.setChildCount(flightSegment.getChildCount());
        extractedFlightSegment.setInfantCount(flightSegment.getInfantCount());

        extractedFlightSegment.setFareBasisCode(flightSegment.getFareBasisCode());
        extractedFlightSegment.setFlexiID(flightSegment.getFlexiID());
        extractedFlightSegment.setParticipatingJourneyReturn(flightSegment.isParticipatingJourneyReturn());
        extractedFlightSegment.setSectorONDCode(flightSegment.getSectorONDCode());
        extractedFlightSegment.setInverseSegmentDeparture(flightSegment.getInverseSegmentDeparture());
        extractedFlightSegment.setBookingClass(flightSegment.getBookingClass());

        extractedFlightSegment.setDepartureAirportCode(segmentCode.substring(0, segmentCode.indexOf("/")));
        extractedFlightSegment.setAirportType(flightSegment.getAirportType());
        extractedFlightSegment.setArrivalAirportCode(segmentCode.substring(segmentCode.lastIndexOf("/") + 1));

        return extractedFlightSegment;
    }

    public static String extractCarrierCode(String flightRefNumber) {
        if (flightRefNumber.indexOf(DELIM) != -1) {
            String[] values = StringUtils.split(flightRefNumber, DELIM);
            flightRefNumber = values[0];
        }
        return flightRefNumber;
    }

    public static String extractFlightSegmentId(String flightRefNumber) {

        if (flightRefNumber.indexOf(DELIM) != -1) {
            String[] values = StringUtils.split(flightRefNumber, DELIM);
            flightRefNumber = values[2];
        }
        return flightRefNumber;
    }

    public static void composeAndSetLCCFlightSegmentRPH(FlightSegment flightSegment) {
        flightSegment.setFlightRefNumber(getFlightSegmentRPH(flightSegment));
    }

    public static String getFlightSegmentRPH(FlightSegment flightSegment) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_yyyyMMddHHmmss);
        String formattedString = flightSegment.getFlightRefNumber();
        if (flightSegment.getFlightRefNumber() != null && flightSegment.getFlightRefNumber().indexOf(DELIM) == -1) {
            String operatingAirlineCode = flightSegment.getOperatingAirline();
            String segmentCode = flightSegment.getSegmentCode();
            String flightRPH = flightSegment.getFlightRefNumber();
            String depatureDateTime = format.format(flightSegment.getDepatureDateTime());
            String arrivalDateTime = format.format(flightSegment.getArrivalDateTime());

            formattedString = operatingAirlineCode + DELIM + segmentCode + DELIM + flightRPH + DELIM + depatureDateTime
                    + DELIM + arrivalDateTime;
        }

        return formattedString;
    }
}
