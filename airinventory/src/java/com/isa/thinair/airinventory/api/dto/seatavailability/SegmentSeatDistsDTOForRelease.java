package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airinventory.api.dto.SeatDistForRelease;

public class SegmentSeatDistsDTOForRelease implements Serializable {

	private static final long serialVersionUID = -661280090217912192L;

	private int flightSegId;

	private Collection<SeatDistForRelease> seatDistributionsForRelease;

	private int soldInfantSeatsForRelease;

	private int onholdInfantSeatsForRelease;

	/** When this DTO is used for releasing infant seats only, logical cabin class must be set */
	private String logicalCCCode;

	/* When seatDistributionsForRelease is empty, this attribute must be set */
	private String bookingClassType = null;

	public SegmentSeatDistsDTOForRelease() {

	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public int getOnholdInfantSeatsForRelease() {
		return onholdInfantSeatsForRelease;
	}

	public void setOnholdInfantSeatsForRelease(int onholdInfantSeatsForRelease) {
		this.onholdInfantSeatsForRelease = onholdInfantSeatsForRelease;
	}

	/**
	 * @return Collection of SeatDistForRelease
	 */
	public Collection<SeatDistForRelease> getSeatDistributionsForRelease() {
		return seatDistributionsForRelease;
	}

	/**
	 * @param seatDistributionsForRelease
	 *            Collection of SeatDistForRelease
	 */
	public void setSeatDistributionsForRelease(Collection<SeatDistForRelease> seatDistributionsForRelease) {
		this.seatDistributionsForRelease = seatDistributionsForRelease;
	}

	public int getSoldInfantSeatsForRelease() {
		return soldInfantSeatsForRelease;
	}

	public void setSoldInfantSeatsForRelease(int soldInfantSeatsForRelease) {
		this.soldInfantSeatsForRelease = soldInfantSeatsForRelease;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public int[] getTotalSoldOnholdAdultSeats() {
		int soldAudultSeats = 0;
		int onholdAdultSeats = 0;
		if (getSeatDistributionsForRelease() != null) {
			Iterator<SeatDistForRelease> seatDistsIt = getSeatDistributionsForRelease().iterator();
			while (seatDistsIt.hasNext()) {
				SeatDistForRelease seatDist = (SeatDistForRelease) seatDistsIt.next();
				soldAudultSeats += seatDist.getSoldSeats();
				onholdAdultSeats += seatDist.getOnHoldSeats();
			}
		}
		return new int[] { soldAudultSeats, onholdAdultSeats };
	}

	public String getBookingCode() {
		String bookingCode = null;
		if (getSeatDistributionsForRelease() != null) {
			Iterator<SeatDistForRelease> seatDistsIt = getSeatDistributionsForRelease().iterator();
			while (seatDistsIt.hasNext()) {
				bookingCode = ((SeatDistForRelease) seatDistsIt.next()).getBookingCode();
			}
		}
		return bookingCode;
	}

	public String getBookingClassType() {
		if (this.bookingClassType == null && getSeatDistributionsForRelease() != null) {
			Iterator<SeatDistForRelease> seatsDistIt = getSeatDistributionsForRelease().iterator();
			while (seatsDistIt.hasNext()) {
				bookingClassType = ((SeatDistForRelease) seatsDistIt.next()).getBookingClassType();
			}
		}
		return bookingClassType;
	}

	/**
	 * @param bookingClassType
	 *            The bookingClassType to set.
	 */
	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

}
