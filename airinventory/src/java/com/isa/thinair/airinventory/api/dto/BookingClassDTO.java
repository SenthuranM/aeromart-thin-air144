/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

import com.isa.thinair.airinventory.api.model.BookingClass;

/**
 * @author MN DTO for save/update booking class Also used in populating the data grid in ManageBooking classes
 */
public class BookingClassDTO implements Serializable {

	private static final long serialVersionUID = -4907949583718718582L;
	private BookingClass bookingClass = null;

	private boolean hasLinkedAgents = false;

	private boolean hasLinkedFareRules = false;

	private boolean hasLinkedInventories = false;

	// attributes only specified from client
	private boolean shiftingNestRankAllowed = false;

	public BookingClassDTO() {

	}

	public BookingClassDTO(BookingClass bc) {
		setBookingClass(bc);
	}

	public BookingClass getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(BookingClass bookingClass) {
		this.bookingClass = bookingClass;
	}

	public boolean isShiftingNestRankAllowed() {
		return shiftingNestRankAllowed;
	}

	public void setShiftingNestRankAllowed(boolean shiftingNestRankAllowed) {
		this.shiftingNestRankAllowed = shiftingNestRankAllowed;
	}

	public boolean isHasLinkedAgents() {
		return hasLinkedAgents;
	}

	public void setHasLinkedAgents(boolean hasLinkedAgents) {
		this.hasLinkedAgents = hasLinkedAgents;
	}

	public boolean isHasLinkedFareRules() {
		return hasLinkedFareRules;
	}

	public void setHasLinkedFareRules(boolean hasLinkedFareRules) {
		this.hasLinkedFareRules = hasLinkedFareRules;
	}

	public boolean isHasLinkedInventories() {
		return hasLinkedInventories;
	}

	public void setHasLinkedInventories(boolean hasLinkedInventories) {
		this.hasLinkedInventories = hasLinkedInventories;
	}
}
