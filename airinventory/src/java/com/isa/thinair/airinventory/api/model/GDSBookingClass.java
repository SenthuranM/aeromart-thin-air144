package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;

/**
* @author Manoj Dhanushka
* 
* @hibernate.class table = "T_BOOKING_CLASS_GDS_MAPPING"
*/
public class GDSBookingClass implements Serializable {
	
	private static final long serialVersionUID = -656709990343286971L;
	
	private int bookingClassGDSMappingId;
	
	private String bookingCode;
	
	private int gdsId;
	
	private String mappedBC;
	
	/**
	 * @return Returns the bookingCode.
	 * 
	 * @hibernate.id column = "BOOKING_CLASS_GDS_MAPPING_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BOOKING_CLASS_GDS_MAPPING"
	 */
	public int getBookingClassGDSMappingId() {
		return bookingClassGDSMappingId;
	}

	/**
	 * @param bookingClassGDSMappingId the bookingClassGDSMappingId to set
	 */
	public void setBookingClassGDSMappingId(int bookingClassGDSMappingId) {
		this.bookingClassGDSMappingId = bookingClassGDSMappingId;
	}


	/**
	 * @return Returns the bookingCode.
	 * 
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode the bookingCode to set
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return the gdsId
	 * 
	 * @hibernate.property column = "GDS_ID"
	 */
	public int getGdsId() {
		return gdsId;
	}

	/**
	 * @param gdsId the gdsId to set
	 */
	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * @return the mappedBC
	 * 
	 * @hibernate.property column = "MAPPED_BC"
	 */
	public String getMappedBC() {
		return mappedBC;
	}

	/**
	 * @param mappedBC the mappedBC to set
	 */
	public void setMappedBC(String mappedBC) {
		this.mappedBC = mappedBC;
	}		

}
