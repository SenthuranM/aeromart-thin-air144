package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

public class BookingClassAlloc implements Serializable {

	private static final long serialVersionUID = 1771004554127622299L;

	private String bookingClassCode;

	private String cabinClassCode;

	private String logicaCabinClassCode;

	private String bookingClassType;

	private boolean overbook;

	private boolean waitListed;

	private int noOfAdults;

	private boolean bookedFlightCabinBeingSearched;

	private boolean sameBookingClassAsExisting;

	private boolean flownOnd;

	private boolean isUntouchedOnd;

	public BookingClassAlloc(String bookingClassCode, String cabinClassCode, String logicaCabinClassCode, int noOfAdults,
			String bookingClassType, boolean overbook, boolean bookedFlightCabinBeingSearched,
			boolean sameBookingClassAsExisting, boolean flownOnd, boolean waitListed, boolean isUntouchedOnd) {
		super();
		this.bookingClassCode = bookingClassCode;
		this.cabinClassCode = cabinClassCode;
		this.logicaCabinClassCode = logicaCabinClassCode;
		this.noOfAdults = noOfAdults;
		this.overbook = overbook;
		this.bookingClassType = bookingClassType;
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
		this.sameBookingClassAsExisting = sameBookingClassAsExisting;
		this.flownOnd = flownOnd;
		this.waitListed = waitListed;
		this.isUntouchedOnd = isUntouchedOnd;
	}

	/**
	 * @return the bCCode
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bCCode
	 *            the bCCode to set
	 */
	public void setbCCode(String bCCode) {
		this.bookingClassCode = bCCode;
	}

	/**
	 * @return the noOfAdults
	 */
	public int getNoOfAdults() {
		return noOfAdults;
	}

	/**
	 * @param noOfAdults
	 *            the noOfAdults to set
	 */
	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public String getLogicaCabinClassCode() {
		return logicaCabinClassCode;
	}

	public String getBookingClassType() {
		return bookingClassType;
	}

	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	public boolean isOverbook() {
		return overbook;
	}

	public boolean isWaitListed() {
		return waitListed;
	}

	public boolean isBookedFlightCabinBeingSearched() {
		return bookedFlightCabinBeingSearched;
	}

	public boolean isSameBookingClassAsExisting() {
		return sameBookingClassAsExisting;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public boolean isUntouchedOnd() {
		return isUntouchedOnd;
	}

}
