/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */

package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * Flight CabinClass Inventory Summary
 * 
 * @author Byorn
 */

public class FCCInventorySummaryDTO implements Serializable {

	private static final long serialVersionUID = -532842061764260532L;

	private int fccInvId;

	private String cabinClassCode;

	private int adultBaseCapacity;

	private int effectiveAdultBaseCapacity;

	private int infantBaseCapacity;

	private List<FCCSegInvSummaryDTO> fccSegInventorySummary;

	public int getFccInvId() {
		return fccInvId;
	}

	public void setFccInvId(int fccInvId) {
		this.fccInvId = fccInvId;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the fccSegInventorySummary.
	 */
	public List<FCCSegInvSummaryDTO> getFccSegInventorySummary() {
		return fccSegInventorySummary;
	}

	/**
	 * @param fccSegInventorySummary
	 *            The fccSegInventorySummary to set.
	 */
	public void setFccSegInventorySummary(List<FCCSegInvSummaryDTO> fccSegInventorySummary) {
		this.fccSegInventorySummary = fccSegInventorySummary;
	}

	/**
	 * @return Returns the adultBaseCapacity.
	 */
	public int getAdultBaseCapacity() {
		return adultBaseCapacity;
	}

	/**
	 * @param adultBaseCapacity
	 *            The adultBaseCapacity to set.
	 */
	public void setAdultBaseCapacity(int adultBaseCapacity) {
		this.adultBaseCapacity = adultBaseCapacity;
	}

	/**
	 * @return Returns the effectiveAdultBaseCapacity. effectiveAdultBaseCapacity = allocated + oversell - curtailed
	 */
	public int getEffectiveAdultBaseCapacity() {
		return effectiveAdultBaseCapacity;
	}

	/**
	 * @param effectiveAdultBaseCapacity
	 *            The effectiveAdultBaseCapacity to set.
	 */
	public void setEffectiveAdultBaseCapacity(int effectiveAdultBaseCapacity) {
		this.effectiveAdultBaseCapacity = effectiveAdultBaseCapacity;
	}

	/**
	 * @return Returns the infantBaseCapacity.
	 */
	public int getInfantBaseCapacity() {
		return infantBaseCapacity;
	}

	/**
	 * @param infantBaseCapacity
	 *            The infantBaseCapacity to set.
	 */
	public void setInfantBaseCapacity(int infantBaseCapacity) {
		this.infantBaseCapacity = infantBaseCapacity;
	}

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		String t = "t";
		summary.append(nl + "----------------------FCC Inventory Information-----------------------" + nl);
		summary.append(t + "FCCSegInvId			= " + getFccInvId() + nl);
		summary.append(t + "Segment Code		= " + getCabinClassCode() + nl);
		summary.append(t + "Adult allocation	= " + getAdultBaseCapacity() + nl);
		summary.append(t + "Infant allocation	= " + getInfantBaseCapacity() + nl);
		summary.append(nl + "----------------------------------------------------------------------" + nl);
		Iterator<FCCSegInvSummaryDTO> fccSegInvIt = getFccSegInventorySummary().iterator();
		while (fccSegInvIt.hasNext()) {
			summary.append(((FCCSegInvSummaryDTO) fccSegInvIt.next()).getSummary());
		}
		return summary;
	}

}
