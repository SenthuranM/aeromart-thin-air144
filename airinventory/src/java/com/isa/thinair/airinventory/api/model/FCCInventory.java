/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:27
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.util.Collection;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool, Thejaka
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_FCC_ALLOC"
 * 
 *                  FCC is the entity class to repesent a FCC model
 * 
 */
public class FCCInventory extends Persistent {

	private static final long serialVersionUID = 2629831176648058866L;

	private Integer fccInvId;

	private int actualCapacity;

	private int infantCapacity;

	private Set<FCCSegInventory> segmentAlloc;

	private String ccCode;

	private int flightId;

	/**
	 * returns the fccInvId
	 * 
	 * @return Returns the fccInvId.
	 * @hibernate.id column = "FCCA_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FCC_ALLOC_FCCA_ID"
	 */
	public Integer getFccInvId() {
		return fccInvId;
	}

	/**
	 * sets the fccInvId
	 * 
	 * @param fccInvId
	 *            The fccInvId to set.
	 * @param fccInvId
	 *            The fccInvId to set.
	 */
	@SuppressWarnings("unused")
	private void setFccInvId(Integer fccInvId) {
		this.fccInvId = fccInvId;
	}

	/**
	 * returns the actualCapacity
	 * 
	 * @return Returns the actualCapacity.
	 * 
	 * @hibernate.property column = "ACTUAL_CAPACITY"
	 */
	public int getActualCapacity() {
		return actualCapacity;
	}

	/**
	 * sets the actualCapacity
	 * 
	 * @param actualCapacity
	 *            The actualCapacity to set.
	 */
	public void setActualCapacity(int actualCapacity) {
		this.actualCapacity = actualCapacity;
	}

	/**
	 * returns the infantCapacity
	 * 
	 * @return Returns the infantCapacity.
	 * 
	 * @hibernate.property column = "INFANT_CAPACITY"
	 */
	public int getInfantCapacity() {
		return infantCapacity;
	}

	/**
	 * sets the infantCapacity
	 * 
	 * @param infantCapacity
	 *            The infantCapacity to set.
	 */
	public void setInfantCapacity(int infantCapacity) {
		this.infantCapacity = infantCapacity;
	}

	/**
	 * returns the flightId
	 * 
	 * @return Returns the flightId.
	 * 
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * sets the flightId
	 * 
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	/**
	 * returns the ccCode
	 * 
	 * @return Returns the ccCode.
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCCCode() {
		return ccCode;
	}

	/**
	 * sets the ccCode
	 * 
	 * @param ccCode
	 *            The ccCode to set.
	 */
	public void setCCCode(String ccCode) {
		this.ccCode = ccCode;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all"
	 * 
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airinventory.api.model.FCCSegInventory"
	 * 
	 * @hibernate.collection-key column="FCCA_ID"
	 */
	public Collection<FCCSegInventory> getSegAlloc() {
		return segmentAlloc;
	}

	/**
   * 
   */
	public void setSegAlloc(Set<FCCSegInventory> segAlloc) {
		this.segmentAlloc = segAlloc;
	}

}
