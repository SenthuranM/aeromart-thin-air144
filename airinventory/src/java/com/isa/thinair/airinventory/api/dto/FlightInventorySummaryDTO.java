package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;

public class FlightInventorySummaryDTO implements Serializable {

	private static final long serialVersionUID = 8412845884428463684L;

	private int flightId;

	private int allocatedAdultSeats;

	private int allocatedInfantSeats;

	private int soldAdultSeats;

	private int soldInfantSeats;

	private int onholdAdultSeats;

	private int onholdInfantSeats;

	private int availableAdultSeats;

	private int availableInfantSeats;

	private int totalFixedSeats;

	private Collection<FCCSegInvSummaryDTO> segmentInventories;

	public FlightInventorySummaryDTO() {

	}

	public FlightInventorySummaryDTO(int flightId, Long allocatedSeats, Long allocatedInfantSeats, Long soldSeats,
			Long soldInfantSeats, Long onholdSeats, Long onholdInfantSeats, Long availableSeats, Long availableInfantSeats,
			Long totalFixedSeats) {
		this.flightId = flightId;
		this.allocatedAdultSeats = allocatedSeats.intValue();
		this.allocatedInfantSeats = allocatedInfantSeats.intValue();
		this.soldAdultSeats = soldSeats.intValue();
		this.soldInfantSeats = soldInfantSeats.intValue();
		this.onholdAdultSeats = onholdSeats.intValue();
		this.onholdInfantSeats = onholdInfantSeats.intValue();
		this.availableAdultSeats = availableSeats.intValue();
		this.availableInfantSeats = availableInfantSeats.intValue();
		this.totalFixedSeats = totalFixedSeats.intValue();
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getAllocatedAdultSeats() {
		return allocatedAdultSeats;
	}

	public void setAllocatedAdultSeats(int allocatedAdultSeats) {
		this.allocatedAdultSeats = allocatedAdultSeats;
	}

	public int getAllocatedInfantSeats() {
		return allocatedInfantSeats;
	}

	public void setAllocatedInfantSeats(int allocatedInfantSeats) {
		this.allocatedInfantSeats = allocatedInfantSeats;
	}

	public int getOnholdAdultSeats() {
		return onholdAdultSeats;
	}

	public void setOnholdAdultSeats(int onholdAdultSeats) {
		this.onholdAdultSeats = onholdAdultSeats;
	}

	public int getOnholdInfantSeats() {
		return onholdInfantSeats;
	}

	public void setOnholdInfantSeats(int onholdInfantSeats) {
		this.onholdInfantSeats = onholdInfantSeats;
	}

	public int getSoldAdultSeats() {
		return soldAdultSeats;
	}

	public void setSoldAdultSeats(int soldAdultSeats) {
		this.soldAdultSeats = soldAdultSeats;
	}

	public int getSoldInfantSeats() {
		return soldInfantSeats;
	}

	public void setSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats = soldInfantSeats;
	}

	public int getAvailableAdultSeats() {
		return availableAdultSeats;
	}

	public void setAvailableAdultSeats(int availableAdultSeats) {
		this.availableAdultSeats = availableAdultSeats;
	}

	public int getAvailableInfantSeats() {
		return availableInfantSeats;
	}

	public void setAvailableInfantSeats(int availableInfantSeats) {
		this.availableInfantSeats = availableInfantSeats;
	}

	public Collection<FCCSegInvSummaryDTO> getSegmentInventories() {
		return segmentInventories;
	}

	public void setSegmentInventories(Collection<FCCSegInvSummaryDTO> segmentInventories) {
		this.segmentInventories = segmentInventories;
	}

	public int getTotalFixedSeats() {
		return totalFixedSeats;
	}

	public void setTotalFixedSeats(int totalFixedSeats) {
		this.totalFixedSeats = totalFixedSeats;
	}
}
