package com.isa.thinair.airinventory.api.dto.rm;

import java.util.Date;

import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;

public class FCCSegInventoryUpdateStatusRMDTO extends FCCSegInventoryUpdateStatusDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8966169168422834238L;

	public FCCSegInventoryUpdateStatusRMDTO(int operationStatus, String msg, Exception exception, String failureLevel,
			Integer fccsInvId, Integer fccsbInvId) {
		super(operationStatus, msg, exception, failureLevel, fccsInvId, fccsbInvId);

	}

	private String cabinClass;

	private String flightNumber;

	private Date depZuluDate;

	private String segmentCode;

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the depZuluDate
	 */
	public Date getDepZuluDate() {
		return depZuluDate;
	}

	/**
	 * @param depZuluDate
	 *            the depZuluDate to set
	 */
	public void setDepZuluDate(Date depZuluDate) {
		this.depZuluDate = depZuluDate;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

}
