package com.isa.thinair.airinventory.api.dto.seatavailability;

public enum FarePriceOnd {
	ONEWAY, RETURN
}
