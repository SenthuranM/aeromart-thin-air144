/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:11:58
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;



import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;


/**
 * @hibernate.class table = "ML_T_FLIGHT_MEAL_CHARGE"
 * 
 */
public class FlightMeal extends Tracking {

	private static final long serialVersionUID = 3730936962182061784L;

	private int flightMealChargeId;

	private Integer flightSegId;

	private Integer mealChargeId;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String status;

	private int allocatedMeals;

	private int soldMeals;

	private int availableMeals;

	private String cabinClassCode;

	private String logicalCCCode;

	private boolean isAnciOfferCreated;

	/**
	 * @return the flightMealChargeId
	 * @hibernate.id column = "FLIGHT_MEAL_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_FLIGHT_MEAL_CHARGE"
	 * 
	 */
	public int getFlightMealChargeId() {
		return flightMealChargeId;
	}

	public void setFlightMealChargeId(int flightMealChargeId) {
		this.flightMealChargeId = flightMealChargeId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_CHARGE_ID"
	 */
	public Integer getMealChargeId() {
		return mealChargeId;
	}

	public void setMealChargeId(Integer mealChargeId) {
		this.mealChargeId = mealChargeId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ALLOCATED_MEALS"
	 */
	public int getAllocatedMeals() {
		return allocatedMeals;
	}

	public void setAllocatedMeals(int allocatedMeals) {
		this.allocatedMeals = allocatedMeals;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "SOLD_MEALS"
	 */
	public int getSoldMeals() {
		return soldMeals;
	}

	public void setSoldMeals(int soldMeals) {
		this.soldMeals = soldMeals;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AVAILABLE_MEALS"
	 */
	public int getAvailableMeals() {
		return availableMeals;
	}

	public void setAvailableMeals(int availableMeals) {
		this.availableMeals = availableMeals;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the isAnciOfferCreated
	 * @hibernate.property column = "IS_ANCI_OFFER_CREATED" type = "yes_no"
	 */
	public boolean isAnciOfferCreated() {
		return isAnciOfferCreated;
	}

	/**
	 * @param isAnciOfferCreated
	 *            the isAnciOfferCreated to set
	 */
	public void setAnciOfferCreated(boolean isAnciOfferCreated) {
		this.isAnciOfferCreated = isAnciOfferCreated;
	}
}
