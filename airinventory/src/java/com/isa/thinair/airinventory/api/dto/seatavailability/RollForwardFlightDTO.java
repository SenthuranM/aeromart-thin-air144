package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author rumesh
 * 
 */
public class RollForwardFlightDTO implements Serializable, Comparable<RollForwardFlightDTO> {
	private static final long serialVersionUID = 1L;

	private String flightRefNo;
	private int flightSegId;
	private String segmentCode;
	private int segmentSeq;
	private Date departureDateTime;
	private Date departureDateTimeZulu;
	private String bookingClass;
	private String cabinClass;
	private int fareId;
	private int fareRuleId;
	private String returnFlag;
	private Integer fareGroupId;
	private Integer returnGroupId;
	private Integer returnOndGroupId;
	private boolean domesticFlight;
	
	/**
	 * Added in order to support onhold roll forward support for open return segments
	 */
	private boolean openReturn;
	
	/**
	 * @return the openReturn
	 */
	public boolean isOpenReturn() {
		return openReturn;
	}

	/**
	 * @param openReturn the openReturn to set
	 */
	public void setOpenReturn(boolean openReturn) {
		this.openReturn = openReturn;
	}

	public String getFlightRefNo() {
		return flightRefNo;
	}

	public void setFlightRefNo(String flightRefNo) {
		this.flightRefNo = flightRefNo;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getSegmentSeq() {
		return segmentSeq;
	}

	public void setSegmentSeq(int segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public int compareTo(RollForwardFlightDTO obj) {
		return this.getDepartureDateTime().compareTo(obj.getDepartureDateTime());
	}

	public Integer getFareGroupId() {
		return fareGroupId;
	}

	public void setFareGroupId(Integer fareGroupId) {
		this.fareGroupId = fareGroupId;
	}

	public Integer getReturnGroupId() {
		return returnGroupId;
	}

	public void setReturnGroupId(Integer returnGroupId) {
		this.returnGroupId = returnGroupId;
	}

	public Integer getReturnOndGroupId() {
		return returnOndGroupId;
	}

	public void setReturnOndGroupId(Integer returnOndGroupId) {
		this.returnOndGroupId = returnOndGroupId;
	}

	public boolean isDomesticFlight() {
		return domesticFlight;
	}

	public void setDomesticFlight(boolean domesticFlight) {
		this.domesticFlight = domesticFlight;
	}
}
