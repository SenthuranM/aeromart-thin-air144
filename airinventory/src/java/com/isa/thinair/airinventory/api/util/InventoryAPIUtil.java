package com.isa.thinair.airinventory.api.util;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;

public class InventoryAPIUtil {
	public static Date getFirstDepartureDate(Collection<FlightSegmentDTO> flightSegmentDTOs) {
		Date depDate = null;

		for (FlightSegmentDTO flightSegmentDTO2 : flightSegmentDTOs) {
			FlightSegmentDTO flightSegmentDTO = flightSegmentDTO2;

			if (depDate == null || depDate.before(flightSegmentDTO.getDepartureDateTimeZulu())) {
				depDate = flightSegmentDTO.getDepartureDateTimeZulu();
			}
		}

		return depDate;
	}

	public static Map<Integer, Integer> getOndWiseSelectedTemplateId(List<BundledFareDTO> bundledFareDTOs, String serviceName) {
		Map<Integer, Integer> ondTemplateId = null;
		if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
			ondTemplateId = new HashMap<Integer, Integer>();
			for (int ondCounter = 0; ondCounter < bundledFareDTOs.size(); ondCounter++) {
				BundledFareDTO bundledFareDTO = bundledFareDTOs.get(ondCounter);
				Integer templateId = null;
				if (bundledFareDTO != null) {
					BundledFareFreeServiceTO freeService = bundledFareDTO.getApplicableService(serviceName);
					if (freeService != null) {
						templateId = freeService.getTemplateId();
					}
				}
				ondTemplateId.put(ondCounter, templateId);
			}
		}

		return ondTemplateId;
	}

	public static Map<Integer, Integer> getOndWiseBundledFareInclusion(List<BundledFareDTO> bundledFareDTOs, String serviceName) {
		Map<Integer, Integer> ondTemplateId = null;
		if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
			ondTemplateId = new HashMap<Integer, Integer>();
			for (int ondCounter = 0; ondCounter < bundledFareDTOs.size(); ondCounter++) {
				BundledFareDTO bundledFareDTO = bundledFareDTOs.get(ondCounter);
				if (bundledFareDTO != null && bundledFareDTO.isServiceIncluded(serviceName)) {
					ondTemplateId.put(ondCounter, bundledFareDTO.getBundledFarePeriodId());
				}
			}
		}

		return ondTemplateId;
	}

	public static Map<Integer, Boolean> getOndWiseServiceMultiSelection(List<BundledFareDTO> bundledFareDTOs, String serviceName) {
		Map<Integer, Boolean> ondTemplateId = null;
		if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
			ondTemplateId = new HashMap<Integer, Boolean>();
			for (int ondCounter = 0; ondCounter < bundledFareDTOs.size(); ondCounter++) {
				BundledFareDTO bundledFareDTO = bundledFareDTOs.get(ondCounter);
				if (bundledFareDTO != null) {
					BundledFareFreeServiceTO freeService = bundledFareDTO.getApplicableService(serviceName);
					if (freeService != null) {
						ondTemplateId.put(ondCounter, freeService.isAllowMultipleSelect());
					}
				}
			}
		}

		return ondTemplateId;
	}

	public static boolean isBusSegment(FlightSegmentDTO flightSegmentDTO) {
		if (flightSegmentDTO.getOperationTypeID() != null
				&& flightSegmentDTO.getOperationTypeID() == AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {
			return true;
		}
		return false;
	}

	public static boolean isBusSegmentExists(Collection<FlightSegmentDTO> flightSegmentDTOs) {
		boolean busSegExists = false;
		if (flightSegmentDTOs != null) {
			for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
				busSegExists |= isBusSegment(flightSegmentDTO);
			}
		}

		return busSegExists;
	}

	public static boolean isValidOndForConnectingSector(List<Integer> flightSegmentIds, String origin, String destination)
			throws ModuleException {

		if (flightSegmentIds != null && flightSegmentIds.size() > 0 && !StringUtil.isNullOrEmpty(origin)
				&& !StringUtil.isNullOrEmpty(destination)) {
			Collection<FlightSegmentDTO> flightSegmentDetails = AirInventoryModuleUtils.getFlightBD().getFlightSegments(
					flightSegmentIds);
			if (flightSegmentDetails != null && flightSegmentDetails.size() > 0) {
				for (FlightSegmentDTO fltSegmentDTO : flightSegmentDetails) {
					String segCode = fltSegmentDTO.getSegmentCode();
					if (!StringUtil.isNullOrEmpty(segCode)) {
						String airports[] = segCode.split("/");
						if (airports[0].equals(origin) && airports[airports.length - 1].equals(destination)) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}
}
