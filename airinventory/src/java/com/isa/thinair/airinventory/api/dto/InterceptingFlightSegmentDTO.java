package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class InterceptingFlightSegmentDTO implements Serializable {

	private static final long serialVersionUID = -4500614496680047141L;
	private int flightId;
	private String interceptingSegCode;

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getInterceptingSegCode() {
		return interceptingSegCode;
	}

	public void setInterceptingSegCode(String interceptingSegCode) {
		this.interceptingSegCode = interceptingSegCode;
	}

}
