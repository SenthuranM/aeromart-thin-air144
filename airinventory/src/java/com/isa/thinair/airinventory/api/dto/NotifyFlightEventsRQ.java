package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class NotifyFlightEventsRQ implements Serializable {

	private static final long serialVersionUID = 2892982280246124846L;
	private Collection<NotifyFlightEventRQ> notifyFlightEventRQCol;

	/**
	 * @return the notifyFlightEventRQCol
	 */
	public Collection<NotifyFlightEventRQ> getNotifyFlightEventRQCol() {
		return notifyFlightEventRQCol;
	}

	/**
	 * @param notifyFlightEventRQCol
	 *            the notifyFlightEventRQCol to set
	 */
	public void setNotifyFlightEventRQCol(Collection<NotifyFlightEventRQ> notifyFlightEventRQCol) {
		this.notifyFlightEventRQCol = notifyFlightEventRQCol;
	}

	public void addNotifyFlightEventRQ(NotifyFlightEventRQ flightEvent) {
		if (this.notifyFlightEventRQCol == null)
			this.notifyFlightEventRQCol = new ArrayList<NotifyFlightEventRQ>();
		this.notifyFlightEventRQCol.add(flightEvent);
	}
}
