package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Captures all the fares, from all the fare types quoted
 * 
 * @author MN
 * 
 */
public class SegmentFaresDTO implements Serializable {

	private static final long serialVersionUID = -2089431762214933198L;
	/* AllFareBCInventorySummaryDTOs collections keyed by fareType */
	private HashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> segmentsFares = new HashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>>();

	public HashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> getSegmentsFares() {
		return segmentsFares;
	}

	public void setSegmentsFares(HashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> segmentsFares) {
		this.segmentsFares = segmentsFares;
	}

	void addSegmentsFares(LinkedHashMap<String, AllFaresBCInventorySummaryDTO> allFaresBCInvetorySummaryDTOs, Integer fareType) {
		this.segmentsFares.put(fareType, allFaresBCInvetorySummaryDTOs);
	}

	LinkedHashMap<String, AllFaresBCInventorySummaryDTO> getSegmentFaresOfSpecifiedFareType(Integer fareType) {
		return segmentsFares.get(fareType);
	}
}
