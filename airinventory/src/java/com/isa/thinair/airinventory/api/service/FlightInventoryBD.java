/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.FCCBCOnewayReturnPaxCountDTO;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryRMTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.FLCCAllocationDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.GdsPublishFccSegBcInventoryTo;
import com.isa.thinair.airinventory.api.dto.InvDowngradeDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollforwardStatusDTO;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.dto.PubAvailStatusUpdateRQ;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateStatusRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.UpdateOptimizedInventoryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.dto.FlightChangeInfo;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.Pair;

/**
 * @author Nasly Flight seat inventory maintenance related functionalities
 */
public interface FlightInventoryBD {

	public static final String SERVICE_NAME = "FlightInventoryService";

	/**
	 * Create FCCInvetory and FCCSegInventory
	 * 
	 * @param createFlightInventoryRQ
	 */
	public void createFlightInventory(CreateFlightInventoryRQ createFlightInventoryRQ) throws ModuleException;

	/**
	 * @param flightNo
	 *            TODO
	 * @param modelNo
	 *            TODO
	 * @param fccSegInvSummaryDTOs
	 * @throws ModuleException
	 */
	public void createFCCSegInventory(Collection<FLCCAllocationDTO> flccAllocationDTOs, Integer flightId, String cabinClass,
			int splitOption) throws ModuleException;

	/**
	 * Retreives seat inventory for given flight/cabin class Collection contains FCCInventoryDTO In case there is an
	 * overlapping flight, the method returns the inventory for the overlapping flight as well (2nd element)
	 * 
	 * @param flightId
	 * @param cos
	 * @return Collection
	 */
	public Collection<FCCInventoryDTO> getFCCInventory(int flightId, String cos, Integer overlappingFlightId, boolean includeCnxFlts)
			throws ModuleException;

	/**
	 * Retrieves booking class specific oneway/Return P2P and Connection pax count
	 * based on the fare rule.
	 * 
	 * @param flightSegmentId
	 * @return Map of bookingcode to <code>FCCBCOnewayReturnPaxCountDTO</code>
	 * @throws ModuleException
	 */
	public Map<String, FCCBCOnewayReturnPaxCountDTO> getFCCBCOnewayReturnPaxCountDTOMap(Integer flightSegmentId)
			throws ModuleException;

	public Collection<FLCCAllocationDTO> getLogicalCabinClassWiseAllocations(int flightId, String cabinClassCode)
			throws ModuleException;

	/**
	 * Check the eligibility of changing aircraft model cabin class capacities
	 * 
	 * @param flightIds
	 * @param aircraftModel
	 * @return boolean
	 * @throws ModuleException
	 */
	public DefaultServiceResponse checkFCCInventoryModelUpdatability(Collection<Integer> flightIds,
			AircraftModel updatedAircraftModel, AircraftModel existingAircraftModel, boolean downGradeToAnyModel,
			boolean isScheduleChange) throws ModuleException;

	public void updateInvForGoshowPassengers(SegmentSeatDistsDTO segmentSeatDistsDTO) throws ModuleException;

	/**
	 * Update FCCInventory and FCCSegInventory when associated aircraftModel capacities changes
	 * 
	 * @param flightIds
	 * @param updatedAircraftModel
	 * @param existingAircraftModel
	 * @param flightSegmentCode
	 * @param flightSegmentDTOs
	 * @throws ModuleException
	 */
	public void updateFCCInventoriesForModelUpdate(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, String flightSegmentCode, boolean downgradeToAnyModel,
			Collection<SegmentDTO> flightSegmentDTOs) throws ModuleException;

	/**
	 * Update inventories when assciated aircraft model is changed
	 * 
	 * @param flightIds
	 * @param updatedAircraftModel
	 * @param existingAircraftModel
	 * @param olFlightIds
	 * @param segmentDTOs
	 * @Param scheduleSegmentCode
	 * @throws ModuleException
	 */
	public void updateFCCInventoriesForModelChange(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, Collection<Integer> olFlightIds, Collection<SegmentDTO> segmentDTOs,
			String scheduleSegmentCode, boolean downgradeToAnyModel) throws ModuleException;

	/**
	 * Update FCCSegInventory along with FCCSegBCInventories belong to the segment
	 * 
	 * @param fccSegInventoryUpdateDTO
	 */
	public FCCSegInventoryUpdateStatusDTO updateFCCSegInventory(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO)
			throws ModuleException;

	/**
	 * API for updating multiple FCCSegmentInventories
	 * 
	 * @param fccSegInventoryUpdateDTOs
	 *            Colletion of FCCSegInventoryUpdateDTO objects
	 */
	public LinkedHashMap<Integer, FCCSegInventoryUpdateStatusDTO> updateFCCSegInventories(
			Collection<FCCSegInventoryUpdateDTO> fccSegInventoryUpdateDTOs) throws ModuleException;

	/**
	 * Rolls flight inventory for the selected flights
	 * 
	 * @param rollForwardCriteriaDTO
	 */
	public InvRollforwardStatusDTO rollFlightInventory(Collection<Integer> flightIds,
			InvRollForwardCriteriaDTO rollForwardCriteriaDTO) throws ModuleException;

	/**
	 * Invalidate flight segment inventory
	 * 
	 * @param flightIds
	 * @param segmentCodes
	 */
	public void invalidateFlightSegmentInventories(Collection<Integer> flightSegIds) throws ModuleException;

	/**
	 * Delete a collection of flight inventories
	 * 
	 * @param flightsIds
	 */
	public void deleteFlightInventories(Collection<Integer> flightsIds) throws ModuleException;

	/**
	 * Retrieve the BC inventories count for a flight (for all cabin classes)
	 * 
	 * @param flightId
	 * @return int bc inventory count
	 * @throws ModuleException
	 */
	public int getFCCSegBCInventoriesCount(int flightId) throws ModuleException;

	/**
	 * Retrieves flight cabin class inventories for optimization
	 * 
	 * @param flightIds
	 * @param optimizeSeatInvCriteriaDTO
	 * @return
	 * @throws ModuleException
	 */
	public Collection<FCCInventoryDTO> getFlightInventoriesForOptimization(Collection<Integer> flightIds,
			OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO) throws ModuleException;

	/**
	 * Returns a HashMap containing values of type FlightInventorySummaryDTO keyed by flightId
	 * 
	 * @param flightIds
	 * @param isReporting
	 *            TODO
	 * @return HashMap of FlightInventorySummaryDTOs
	 * @throws ModuleException
	 */
	public HashMap<Integer, FlightInventorySummaryDTO> getFlightInventoriesSummary(Collection<Integer> flightIds,
			boolean isReporting) throws ModuleException;

	/**
	 * Making an invalid segment, valid
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @throws ModuleException
	 */
	public void makeInvalidFltSegInvsValid(Collection<Integer> flightSegIds) throws ModuleException;

	/**
	 * Updating inventory when overlapping flight is removed
	 * 
	 * @param sourceFlightId
	 * @throws ModuleException
	 */
	public void removeInventoryOverlap(Collection<Integer> flightIds) throws ModuleException;

	/**
	 * Returns the inventory details of given flights segments
	 * 
	 * @param flightIds
	 * @return
	 */
	public Collection<FlightInventorySummaryDTO> getFlightSegmentInventoriesSummary(Collection<Integer> flightIds)
			throws ModuleException;

	public LinkedHashMap<String, List<Object[]>> getFCCSegBCInventories(int flightId, String cabinClassCode,
			boolean excludeInvalidSegments) throws ModuleException;

	/**
	 * Returns BC inventory details for RM optimization.
	 * 
	 * @param segInvIds
	 *            Collection<Integer>
	 * @return Map<Integer, Collection<FCCSegBCInventoryTO>>
	 */
	public Map<Integer, Collection<FCCSegBCInventoryTO>> getInvenotriesForRMAlert(Collection<Integer> segInvIds)
			throws ModuleException;

	/**
	 * Returns flight details for RM optimization.
	 * 
	 * @return Map<Integer,FCCSegBCInventoryRMTO>
	 */
	public Map<Integer, FCCSegBCInventoryRMTO> getFlightsForRMAlert() throws ModuleException;

	/**
	 * Updates Optimization Status in BC Inventory.
	 * 
	 * @param bcInvIds
	 *            Collection<Integer>
	 * @param newOptimizationStatus
	 *            The optimization status to set
	 * 
	 * @return No of bc inventories updated
	 */
	public int updateOptimizationStatus(Collection<Integer> bcInvIds, String newOptimizationStatus) throws ModuleException;

	/**
	 * updateOptimizedInventory
	 * 
	 * @param updateOptimizedInventoryDTO
	 * @throws ModuleException
	 */
	public UpdateOptimizedInventoryDTO updateOptimizedInventory(UpdateOptimizedInventoryDTO updateOptimizedInventoryDTO)
			throws ModuleException;

	/**
	 * 
	 * @param fccSegInventoryUpdateDTOs
	 * @return
	 */
	public Collection<FCCSegInventoryUpdateStatusRMDTO> batchUpdateFCCSegInventory(
			Collection<FCCSegInventoryUpdateRMDTO> fccSegInventoryUpdateDTOs) throws ModuleException;

	public void updateFixedSegmentBCAllocation(Integer fccSegInvID, Integer fccsegBCInvID) throws ModuleException;

	public void releaseFixedAllocations() throws ModuleException;

	public void addInventoryNotes(Integer flightSegmentId, String userNote) throws ModuleException;

	public Collection<InventoryAudit> getInventoryNotes(Integer flightId) throws ModuleException;

	/**
	 * Notify flight events suchs creation, closure, cancellation, etc.
	 * 
	 * @param notifyFlightEventRQ
	 * @throws ModuleException
	 */
	public void notifyFlightEvent(NotifyFlightEventsRQ notifyFlightEventRQ) throws ModuleException;

	/**
	 * Retrieves availability information for publishing.
	 * 
	 * @return Map<Integer, Collection<PublishSegAvailMsgDTO>> ==> <GDSId, Collection of PublishSegAvailMsgDTO>
	 * @throws ModuleException
	 */
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getAvailabilityInfoToPublish() throws ModuleException;

	/**
	 * Retrieves availability information for publishing.BC status update
	 * 
	 * @return Map<Integer, Collection<PublishSegAvailMsgDTO>> ==> <GDSId, Collection of PublishSegAvailMsgDTO>
	 * @throws ModuleException
	 */
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getBCStatusUpdateToPublish() throws ModuleException;

	/**
	 * Do preliminary check for BC status AVS and eliminate ineffective BC status entries
	 * 
	 * @throws ModuleException
	 */
	public void setupBCStatusUpdateToPublish() throws ModuleException;

	/**
	 * Updates Publish Availability Status *
	 * 
	 * @param pubAvailStatusUpdateRQs
	 * @throws ModuleException
	 */
	public void publishAvailabiltyUpdateStatus(Collection<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQs) throws ModuleException;

	/**
	 * This method will prepare a list of meals required by each departing flight for a given station and sends it to
	 * the catering department via an email. The list is prepared certain hours before the departure time of a flight
	 * (this threshold is defined in app. parameter) and sends an email to the catering department (email address is
	 * defined in a parameter)
	 * 
	 * @author lalanthi Date : 14/08/2009
	 * @throws ModuleException
	 */
	public void sendMealNotification() throws ModuleException;

	/**
	 * Returns available seats by cabin class based on search criteria
	 * 
	 * @param flightNumber
	 * @param origin
	 * @param destination
	 * @param departureDateStart
	 * @param departureDateEnd
	 * @return
	 */
	public Map<String, String> getCabinClassAvailability(CabinClassAvailabilityDTO cabinClassAvailabilityDTO);

	/**
	 * Returns the segment detail with the available cabin class for a given sengemtId
	 * 
	 * @param fltSegId
	 * @return
	 */
	public FlightSegWithCCDTO getFlightSegmentsWithCabinClass(Integer fltSegId);

	public Collection<InsurancePublisherDetailDTO> getInsuredFlightData();

	public Collection<FlightChangeInfo> getFlightSeatChangeData(FlightSegement seg, String modelNumber);

	public Collection<InsurancePublisherDetailDTO> getFailedInsuredFlightData();

	public void sendMealsListPerRoute(Object[] mealsCollection) throws ModuleException;

	Object[] getMealsListForStation(List<String> hub, String mealNotificationBeginTime, String mealNotificationEndTime,
			int noofAttempts, List<Integer> flightIds) throws ModuleException;

	public int getMaximumEffectiveReservedSetsCountInInterceptingSegments(Collection<Integer> fccSegInvIds)
			throws ModuleException;

	public LinkedHashMap<String, List<String>> getAllocatedBookingClassesForCabinClasses(Integer flightId);

	public Collection<String> getCabinClassesToBeDeleted(AircraftModel updatedAircraftModel, AircraftModel existingAircraftModel)
			throws ModuleException;

	public void updateFCCSegBCInventories(InvDowngradeDTO inventoryTO, int infantCount, int onHoldInfantCount)
			throws ModuleException;

	public Map<String, Integer> getAvailableCabinClassesInfo();

	public List<TempSegBcAlloc> getTempSegBcAllocs(List<Integer> ids);

	public List<Integer> getBlockedSeatsIds(Collection<Integer> allBlockSeatIds, List<FlightSegmentDTO> flightSegsToRelease)
			throws ModuleException;

	public boolean isExposedToGDS(Integer fccSegBCinventory);

	public FCCSegInventory getFlightSegmentsInventory(Integer fccsegInvId);

	public Collection<FCCSegBCInventory> getFCCSegBCInventories(int fccsaId);

	public void openGoshowBcInventory(FlightSegmentDTO fltSegment);

	public void doAuditOverBookings(Collection<String> fltSegIds, String bookingClass, UserPrincipal principal, String pnr,
			String flightId, String userId, int seatCount);

	public void
			doAuditSeatSold(SegmentSeatDistsDTO segmentSeatDistsDTOs, String flightId, String userId, SeatDistribution seatDis);

	public List<Integer> getInterceptingSegmentIDs(List<Integer> sourceFltSegIds) throws ModuleException;

	public Map<Integer, Set<Integer>> getInterceptingFlightSegmentIds(List<Integer> sourceFltSegIds);

	public void clearCachedFlightIds(Collection<Pair<Integer, String>> alertFlightIds) throws ModuleException;

	public DefaultServiceResponse getAffectedFlights(Collection<Integer> flightIds, AircraftModel updatedAircraftModel)
			throws ModuleException;

	public Map<Integer, BigDecimal> getFlightLoadFactor(List<Integer> flightSegmentIds);

	public void updateOpenReturnFCCSegBCInventories(Integer fccsbInvId, String bookingCode, int adultCount, int onHoldadultCount,
			int infantCount, int onHoldInfantCount) throws ModuleException;

	public FCCSegBCInventory getFCCSegBCInventory(int flightSegId, String bookingCode);

	public FCCSegInventory getFCCSegInventory(int flightSegId, String logicalCabinClassCode);

	public void publishGdsFccSegBcInventory(List<GdsPublishFccSegBcInventoryTo> gdsPublishFccSegBcInventoryTos);
}