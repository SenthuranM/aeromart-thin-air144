package com.isa.thinair.airinventory.api.dto.bundledfare;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

/**
 * Class to hold parameters which define selection criteria for applicable bundled fares for a particular OnD
 * 
 * @author rumesh
 * 
 */
public class ApplicableBundledFareSelectionCriteria implements Serializable {
	private static final long serialVersionUID = -5337853234093411401L;
	
	private static final String ANY_AIRPORT_CODE_MATCHER = "***";

	private String bookingClass;

	private Set<String> bookingClasses;

	private String ondCode;

	private Date firstDepartureFlightDate;

	private Set<String> flightNumbers;

	private String agentCode;

	private String selectedLanguage;
	
	private int channelCode;

	private String pointOfSale;

	public ApplicableBundledFareSelectionCriteria() {

	}

	public ApplicableBundledFareSelectionCriteria(String bookingClass, Set<String> bookingClasses, String ondCode,
			Date firstDepartureFlightDate, Set<String> flightNumbers, String agentCode, String selectedLanguage, int channelCode,
			String pointOfSale) {
		super();
		this.bookingClass = bookingClass;
		this.bookingClasses = bookingClasses;
		this.ondCode = ondCode;
		this.firstDepartureFlightDate = firstDepartureFlightDate;
		this.flightNumbers = flightNumbers;
		this.agentCode = agentCode;
		this.selectedLanguage = (selectedLanguage == null || "".equals(selectedLanguage))
				? Locale.ENGLISH.getLanguage()
				: selectedLanguage;
		this.channelCode = channelCode;
		this.pointOfSale = pointOfSale;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public Date getFirstDepartureFlightDate() {
		return firstDepartureFlightDate;
	}

	public void setFirstDepartureFlightDate(Date firstDepartureFlightDate) {
		this.firstDepartureFlightDate = firstDepartureFlightDate;
	}

	public Set<String> getFlightNumbers() {
		return flightNumbers;
	}

	public void setFlightNumbers(Set<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}
	
	public String getOndCodeForAnyDepAirport() {
		String ondCodeForAnyDepAirport = "";
		if (getOndCode() != null) {
			ondCodeForAnyDepAirport = ANY_AIRPORT_CODE_MATCHER + getOndCode().substring(getOndCode().indexOf("/"));
		}
		return ondCodeForAnyDepAirport;
	}

	public String getOndCodeForAnyArrAirport() {
		String ondCodeForAnyArrAirport = "";
		if (getOndCode() != null) {
			ondCodeForAnyArrAirport = getOndCode().substring(0, getOndCode().length() - 3) + ANY_AIRPORT_CODE_MATCHER;
		}
		return ondCodeForAnyArrAirport;
	}
	
	public String getOndCodeForAnyAirport() {
		String ondCodeForAnyAnyAirport = "";
		if (getOndCode() != null) {
			ondCodeForAnyAnyAirport = ANY_AIRPORT_CODE_MATCHER + getOndCode().substring(getOndCode().indexOf("/"));
			ondCodeForAnyAnyAirport = ondCodeForAnyAnyAirport.substring(0, ondCodeForAnyAnyAirport.length() - 3)
					+ ANY_AIRPORT_CODE_MATCHER;
		}
		return ondCodeForAnyAnyAirport;
	}

	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

}
