package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airmaster.api.model.AircraftModel;

/**
 * Create Flight Inventory Request DTO
 */
public class CreateFlightInventoryRQ implements Serializable {

	private static final long serialVersionUID = 3413272436821616128L;
	private Integer flightId;
	private AircraftModel aircraftModel;
	private Collection<SegmentDTO> segmentDTOs;
	private Integer overlappingFlightId;
	private Collection gdsIdsToPublish;
	private Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
	private Map<String, Map<String, List<InvTempCCAllocDTO>>> ccSegmentWiseInvTemplate;

	/**
	 * @return the aircraftModel
	 */
	public AircraftModel getAircraftModel() {
		return aircraftModel;
	}

	/**
	 * @param aircraftModel
	 *            the aircraftModel to set
	 */
	public void setAircraftModel(AircraftModel aircraftModel) {
		this.aircraftModel = aircraftModel;
	}

	/**
	 * @return the flightId
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the gdsIdsToPublish
	 */
	public Collection<Integer> getGdsIdsToPublish() {
		return gdsIdsToPublish;
	}

	/**
	 * @param gdsIdsToPublish
	 *            the gdsIdsToPublish to set
	 */
	public void setGdsIdsToPublish(Collection<Integer> gdsIdsToPublish) {
		this.gdsIdsToPublish = gdsIdsToPublish;
	}

	/**
	 * @return the overlappingFlightId
	 */
	public Integer getOverlappingFlightId() {
		return overlappingFlightId;
	}

	/**
	 * @param overlappingFlightId
	 *            the overlappingFlightId to set
	 */
	public void setOverlappingFlightId(Integer overlappingFlightId) {
		this.overlappingFlightId = overlappingFlightId;
	}

	/**
	 * @return the segmentDTOs
	 */
	public Collection<SegmentDTO> getSegmentDTOs() {
		return segmentDTOs;
	}

	/**
	 * @param segmentDTOs
	 *            the segmentDTOs to set
	 */
	public void setSegmentDTOs(Collection<SegmentDTO> segmentDTOs) {
		this.segmentDTOs = segmentDTOs;
	}

	/**
	 * @return the defaultLogicalCCs
	 */
	public Map<String, String> getDefaultLogicalCCs() {
		return defaultLogicalCCs;
	}

	/**
	 * @param defaultLogicalCCs
	 *            the defaultLogicalCCs to set
	 */
	public void setDefaultLogicalCCs(Map<String, String> defaultLogicalCCs) {
		this.defaultLogicalCCs = defaultLogicalCCs;
	}

	public Map<String, Map<String, List<InvTempCCAllocDTO>>> getCcSegmentWiseInvTemplate() {
		return ccSegmentWiseInvTemplate;
	}

	public void setCcSegmentWiseInvTemplate(Map<String, Map<String, List<InvTempCCAllocDTO>>> ccSegmentWiseInvTemplate) {
		this.ccSegmentWiseInvTemplate = ccSegmentWiseInvTemplate;
	}

}
