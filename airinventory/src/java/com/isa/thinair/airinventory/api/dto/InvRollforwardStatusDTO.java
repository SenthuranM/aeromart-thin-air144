package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class InvRollforwardStatusDTO extends OperationStatusDTO implements Serializable {

	private static final long serialVersionUID = -4142037270417332165L;

	private Collection<Integer> flightIdsForStatusUpdating = new ArrayList<Integer>();

	private StringBuilder strLog;

	private Map<String, StringBuffer> fltInventoryErrorMap = new HashMap<String, StringBuffer>();

	private LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollforwardStatus = new LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO>();

	private String rollForwardAudit;
	
	private String seatOverrideLogs;
	
	private Boolean noMatchingFlights;
	
	public Boolean getNoMatchingFlights() {
		return noMatchingFlights;
	}

	public void setNoMatchingFlights(Boolean noMatchingFlights) {
		this.noMatchingFlights = noMatchingFlights;
	}

	public String getSeatOverrideLogs() {
		return seatOverrideLogs;
	}

	public void setSeatOverrideLogs(String seatOverrideLogs) {
		this.seatOverrideLogs = seatOverrideLogs;
	}

	public String getRollForwardAudit() {
		return rollForwardAudit;
	}

	public void setRollForwardAudit(String rollForwardAudit) {
		this.rollForwardAudit = rollForwardAudit;
	}

	public InvRollforwardStatusDTO(int status, String msg, Exception exception) {
		super(status, msg, exception);
	}

	public Collection<Integer> getFlightIdsForStatusUpdating() {
		return flightIdsForStatusUpdating;
	}

	public void setFlightIdsForStatusUpdating(Collection<Integer> flightIdsForStatusUpdating) {
		this.flightIdsForStatusUpdating = flightIdsForStatusUpdating;
	}

	public StringBuilder getStrLog() {
		return strLog;
	}

	public void setStrLog(StringBuilder strLog) {
		this.strLog = strLog;
	}

	public Map<String, StringBuffer> getFltInventoryErrorMap() {
		return fltInventoryErrorMap;
	}

	public void setFltInventoryErrorMap(Map<String, StringBuffer> fltInventoryErrorMap) {
		this.fltInventoryErrorMap = fltInventoryErrorMap;
	}

	public LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> getRollforwardStatus() {
		return rollforwardStatus;
	}

	public void setRollforwardStatus(LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollforwardStatus) {
		this.rollforwardStatus = rollforwardStatus;
	}

}
