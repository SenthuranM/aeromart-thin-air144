package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

public class ExternalFareSummaryDTO implements Serializable {

	private static final long serialVersionUID = 7853345407687157525L;
	private String bookingClassCode;
	private double adultFareAmount;
	private double childFareAmount;
	private double infantFareAmount;

	/**
	 * @return the bookingClassCode
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            the bookingClassCode to set
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return the adultFareAmount
	 */
	public double getAdultFareAmount() {
		return adultFareAmount;
	}

	/**
	 * @param adultFareAmount
	 *            the adultFareAmount to set
	 */
	public void setAdultFareAmount(double adultFareAmount) {
		this.adultFareAmount = adultFareAmount;
	}

	/**
	 * @return the childFareAmount
	 */
	public double getChildFareAmount() {
		return childFareAmount;
	}

	/**
	 * @param childFareAmount
	 *            the childFareAmount to set
	 */
	public void setChildFareAmount(double childFareAmount) {
		this.childFareAmount = childFareAmount;
	}

	/**
	 * @return the infantFareAmount
	 */
	public double getInfantFareAmount() {
		return infantFareAmount;
	}

	/**
	 * @param infantFareAmount
	 *            the infantFareAmount to set
	 */
	public void setInfantFareAmount(double infantFareAmount) {
		this.infantFareAmount = infantFareAmount;
	}

	public Object clone() {
		ExternalFareSummaryDTO clone = new ExternalFareSummaryDTO();
		clone.setBookingClassCode(this.getBookingClassCode());
		clone.setAdultFareAmount(this.getAdultFareAmount());
		clone.setChildFareAmount(this.getChildFareAmount());
		clone.setInfantFareAmount(this.getInfantFareAmount());

		return clone;
	}

}
