package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class FCCSegBCInventoriesExistsCriteria implements Serializable {

	private static final long serialVersionUID = 7451543939495553769L;

	private Integer fccSegInvId;

	private String bookingCode;

	private Boolean isGDSTypeOnly;

	public Integer getFccSegInvId() {
		return fccSegInvId;
	}

	public void setFccSegInvId(Integer fccSegInvId) {
		this.fccSegInvId = fccSegInvId;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public Boolean getIsGDSTypeOnly() {
		return isGDSTypeOnly;
	}

	public void setIsGDSTypeOnly(Boolean isGDSTypeOnly) {
		this.isGDSTypeOnly = isGDSTypeOnly;
	}
}
