package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;

/**
 * @author Nasly
 */
public class ChangeFaresDTO implements Serializable {

	private static final long serialVersionUID = 7208324431746972732L;

	/**
	 * Collection of OndFareDTO Represents existing quoted fares, charges and seats
	 */
	private Collection<OndFareDTO> existingFareChargesSeatsSegmentsDTOs = null;

	/**
	 * Collection of SegmentFareDTO Captures information about fare changes
	 */
	private final Collection<SegmentsFareDTO> changedFaresSeatsSegmentsDTOs = new ArrayList<SegmentsFareDTO>();

	private int existingFareType;

	private int changedFareType;

	private String posAirport = null;

	private LinkedHashMap<Integer, SegmentFareDTO> changedSegmentFaresMap = null;

	private LinkedHashMap<Integer, SegmentFareDTO> existingSegmentFaresMap = null;

	private Collection<QuotedChargeDTO> unifiedCharges = null;

	/**
	 * Collection of OndFareDTO with changed fares and new charges
	 */
	private Collection<OndFareDTO> newFareChargesSeatsSegmentsDTOs = null;

	private Map<Integer, FlightSegmentDTO> flightSegmentMap = new HashMap<Integer, FlightSegmentDTO>();

	private boolean processedSegments = false;

	private LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>> ondWiseNewSegmentFaresMap = new LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>>();

	private LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>> requestedOndWiseNewSegmentFaresMap = new LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>>();

	private int infantCount;

	private Date lastFareQuotedDate;

	private boolean fqWithinValidity;

	private ArrayList<Integer> oldFltSegIdList = new ArrayList<Integer>();

	private ApplicablePromotionDetailsTO applicablePromotionDetails;

	private Map<Integer, Boolean> ondSequenceReturnMap = new HashMap<Integer, Boolean>();

	private Map<Integer, Map<String, List<BundledFareLiteDTO>>> availableBundledFares = new HashMap<Integer, Map<String, List<BundledFareLiteDTO>>>();

	private Map<Integer, BundledFareDTO> ondSelectedBundledFares = new HashMap<Integer, BundledFareDTO>();

	public Collection<OndFareDTO> getExistingFareChargesSeatsSegmentsDTOs() {
		return existingFareChargesSeatsSegmentsDTOs;
	}

	public void setExistingFareChargesSeatsSegmentsDTOs(Collection<OndFareDTO> existingFareChargesSeatsSegmentsDTOs) {
		this.existingFareChargesSeatsSegmentsDTOs = existingFareChargesSeatsSegmentsDTOs;
	}

	public void addFlightSegments(Collection<FlightSegmentDTO> flightSegments) {
		if (flightSegments != null) {
			for (FlightSegmentDTO fltSeg : flightSegments) {
				flightSegmentMap.put(fltSeg.getSegmentId(), fltSeg);
			}
		}
	}

	public Collection<SegmentsFareDTO> getChangedFaresSeatsSegmentsDTOs() {
		return changedFaresSeatsSegmentsDTOs;
	}

	public void addChangedFaresSeatsSegmentsDTO(SegmentsFareDTO segmentFareDTO) {
		this.changedFaresSeatsSegmentsDTOs.add(segmentFareDTO);
	}

	public int getChangedFareType() {
		return changedFareType;
	}

	public void setChangedFareType(int changedFareType) {
		this.changedFareType = changedFareType;
	}

	public int getExistingFareType() {
		return existingFareType;
	}

	public void setExistingFareType(int existingFareType) {
		this.existingFareType = existingFareType;
	}

	public Collection<OndFareDTO> getNewFareChargesSeatsSegmentsDTOs() {
		return newFareChargesSeatsSegmentsDTOs;
	}

	public void setNewFareChargesSeatsSegmentsDTOs(Collection<OndFareDTO> newFareChargesSeatsSegmentsDTOs) {
		this.newFareChargesSeatsSegmentsDTOs = newFareChargesSeatsSegmentsDTOs;
	}

	/**
	 * @return Returns the posAirport.
	 */
	public String getPosAirport() {
		return posAirport;
	}

	/**
	 * @param posAirport
	 *            The posAirport to set.
	 */
	public void setPosAirport(String posAirport) {
		this.posAirport = posAirport;
	}

	// ------------------------------------------------
	// Utility Methods
	// ------------------------------------------------
	/**
	 * Collection of OndFareDTO with changed fares and new charges
	 */
	public LinkedHashMap<Integer, SegmentFareDTO> getExistingSegmentFaresMap() throws ModuleException {
		if (this.existingSegmentFaresMap == null && getExistingFareChargesSeatsSegmentsDTOs() != null) {
			//TODO if this set ond wise multicity(sub journey), bus need to be checked
			boolean isSplitOperationForBusInvoked = isSplitOperationInvokedForOnd(
					getExistingFareChargesSeatsSegmentsDTOs());
			
			this.existingSegmentFaresMap = new LinkedHashMap<Integer, SegmentFareDTO>();
			Iterator<OndFareDTO> ondFareDTOIt = getExistingFareChargesSeatsSegmentsDTOs().iterator();
			int ondSeq = 0;
			
			while (ondFareDTOIt.hasNext()) {
				OndFareDTO ondFareDTO = ondFareDTOIt.next();
				Iterator<SegmentSeatDistsDTO> segmentSeatDistDTOIt = ondFareDTO.getSegmentSeatDistsDTO().iterator();
				while (segmentSeatDistDTOIt.hasNext()) {
					SegmentSeatDistsDTO segmentSeatDistsDTO = segmentSeatDistDTOIt.next();
					FlightSegmentDTO flightSegmentDTO = ondFareDTO
							.getFlightSegmentDTO(new Integer(segmentSeatDistsDTO.getFlightSegId()));

					SegmentFareDTO segmentFareDTO = new SegmentFareDTO();
					segmentFareDTO.setOndCode(ondFareDTO.getOndCode());
					segmentFareDTO.setSegmentCode(flightSegmentDTO.getSegmentCode());
					segmentFareDTO.setFlightSegId(flightSegmentDTO.getSegmentId().intValue());
					segmentFareDTO.setFlightSegmentDTO(flightSegmentDTO);

					segmentFareDTO.setSegmentSeatDistsDTO(segmentSeatDistsDTO);
					segmentFareDTO.setSeatDistributions(segmentSeatDistsDTO.getSeatDistribution());

					segmentFareDTO.setFareSummaryDTO(ondFareDTO.getFareSummaryDTO());
					segmentFareDTO.setFareType(ondFareDTO.getFareType());
					segmentFareDTO.setInbound(ondFareDTO.isInBoundOnd());
					if(isSplitOperationForBusInvoked){
						segmentFareDTO.setOndSequence(ondSeq++);
					}else{
						segmentFareDTO.setOndSequence(ondFareDTO.getOndSequence());
					}
					if (ondFareDTO.getRequestedOndSequence() != -1) {
						segmentFareDTO.setRequestedOndSequence(ondFareDTO.getRequestedOndSequence());
					} else {
						segmentFareDTO.setRequestedOndSequence(ondFareDTO.getOndSequence());
					}

					this.existingSegmentFaresMap.put(flightSegmentDTO.getSegmentId(), segmentFareDTO);
				}
			}
		}
		return existingSegmentFaresMap;
	}

	public LinkedHashMap<Integer, SegmentFareDTO> getChangedSegmentFaresMap() throws ModuleException {
		if (changedSegmentFaresMap == null && getChangedFaresSeatsSegmentsDTOs() != null) {
			changedSegmentFaresMap = new LinkedHashMap<Integer, SegmentFareDTO>();
			Iterator<SegmentsFareDTO> changedFaresSeatsSegmentDTOsIt = getChangedFaresSeatsSegmentsDTOs().iterator();
			while (changedFaresSeatsSegmentDTOsIt.hasNext()) {
				SegmentsFareDTO segmentsFareDTO = changedFaresSeatsSegmentDTOsIt.next();

				Iterator<Integer> segmentsIt = segmentsFareDTO.getSegmentRPHMap().keySet().iterator();
				while (segmentsIt.hasNext()) {
					Integer segmentId = (Integer) segmentsIt.next();

					SegmentFareDTO changedSegmentFareDTO = new SegmentFareDTO();
					changedSegmentFareDTO.setOndCode(segmentsFareDTO.getOndCode());
					changedSegmentFareDTO.setOndSequence(segmentsFareDTO.getOndSequence());
					if (segmentsFareDTO.getRequestedOndSequence() != -1) {
						changedSegmentFareDTO.setRequestedOndSequence(segmentsFareDTO.getRequestedOndSequence());
					} else {
						changedSegmentFareDTO.setRequestedOndSequence(segmentsFareDTO.getOndSequence());
					}
					SegmentSeatDistsDTO segmentSeatDistsDTO = null;
					if (hasExistingFareOnds()) {
						SegmentFareDTO existingSegmentFareDTO = getExistingSegmentFaresMap().get(segmentId);

						if (existingSegmentFareDTO == null) {
							// invalid data
							throw new ModuleException("airinventory.arg.changefare.invalid",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}
						changedSegmentFareDTO.setFlightSegId(existingSegmentFareDTO.getFlightSegId());
						changedSegmentFareDTO.setSegmentCode(existingSegmentFareDTO.getSegmentCode());
						changedSegmentFareDTO.setFlightSegmentDTO(existingSegmentFareDTO.getFlightSegmentDTO());

						segmentSeatDistsDTO = (SegmentSeatDistsDTO) existingSegmentFareDTO.getSegmentSeatDistsDTO().clone();
					} else {
						FlightSegmentDTO fltSeg = flightSegmentMap.get(segmentId);
						if (fltSeg == null) {
							// invalid data
							throw new ModuleException("airinventory.arg.changefare.invalid",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}
						changedSegmentFareDTO.setFlightSegId(segmentId);
						changedSegmentFareDTO.setSegmentCode(fltSeg.getSegmentCode());
						changedSegmentFareDTO.setFlightSegmentDTO(fltSeg);

						segmentSeatDistsDTO = new SegmentSeatDistsDTO();
						segmentSeatDistsDTO.setFlightSegId(segmentId);
						segmentSeatDistsDTO.setFlightId(fltSeg.getFlightId());

						segmentSeatDistsDTO.setSegmentCode(fltSeg.getSegmentCode());
						segmentSeatDistsDTO.setNoOfInfantSeats(infantCount);
					}

					// when return fare selected with same BC modification final result leads to inventory miss-match
					updateSegmentSeatDistribution(segmentsFareDTO.getSeatDistributions(), segmentId);

					segmentSeatDistsDTO.setSeatDistributions(segmentsFareDTO.getSeatDistributions());
					segmentSeatDistsDTO.setFixedQuotaSeats(segmentsFareDTO.isFixedQuotaSeats());
					segmentSeatDistsDTO.setCabinClassCode(segmentsFareDTO.getCabinClassCode());
					segmentSeatDistsDTO.setLogicalCabinClass(segmentsFareDTO.getLogicalCabinClassCode());

					changedSegmentFareDTO.setSegmentSeatDistsDTO(segmentSeatDistsDTO);
					changedSegmentFareDTO.setSeatDistributions(segmentSeatDistsDTO.getSeatDistribution());

					changedSegmentFareDTO.setFareSummaryDTO(segmentsFareDTO.getFareSummaryDTO());
					changedSegmentFareDTO.setFareType(segmentsFareDTO.getFareType());
					changedSegmentFareDTO.setInbound(segmentsFareDTO.isInbound());

					changedSegmentFaresMap.put(new Integer(changedSegmentFareDTO.getFlightSegId()), changedSegmentFareDTO);
				}
			}
		}
		return changedSegmentFaresMap;
	}

	public Collection<QuotedChargeDTO> getUnifiedCharges(String chargeGroup) {
		if (getUnifiedCharges() != null && getUnifiedCharges().size() > 0) {
			return ChargeQuoteUtils.getUnifiedCharges(getUnifiedCharges(), chargeGroup, null);
		}
		return null;
	}

	public Collection<QuotedChargeDTO> getUnifiedCharges(Collection<String> chargeGroupCodes) {
		if (getUnifiedCharges() != null && getUnifiedCharges().size() > 0) {
			return ChargeQuoteUtils.getUnifiedCharges(getUnifiedCharges(), chargeGroupCodes, null);
		}
		return null;
	}

	public Collection<QuotedChargeDTO> getUnifiedCharges() {
		if (this.unifiedCharges == null && getChangedFareType() != FareTypes.NO_FARE) {
			Collection<QuotedChargeDTO> allCharges = new ArrayList<QuotedChargeDTO>();

			Iterator<OndFareDTO> ondFareDTOsIt = getNewFareChargesSeatsSegmentsDTOs().iterator();
			while (ondFareDTOsIt.hasNext()) {
				OndFareDTO ondFareDTO = ondFareDTOsIt.next();
				if (ondFareDTO.getAllCharges() != null) {
					allCharges.addAll(ondFareDTO.getAllCharges());
				}
			}
			this.unifiedCharges = ChargeQuoteUtils.getUnifiedCharges(allCharges);
		}
		return this.unifiedCharges;
	}

	public LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>> getRequestedOndWiseNewSegmentFaresMap() {
		if (requestedOndWiseNewSegmentFaresMap == null) {
			requestedOndWiseNewSegmentFaresMap = new LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>>();
		}
		return requestedOndWiseNewSegmentFaresMap;
	}

	public LinkedHashMap<Integer, SegmentFareDTO> getRequestedOndWiseOndNewSegmentFaresMap(int ondSequence) {
		if (!getRequestedOndWiseNewSegmentFaresMap().containsKey(ondSequence)) {
			requestedOndWiseNewSegmentFaresMap.put(ondSequence, new LinkedHashMap<Integer, SegmentFareDTO>());
		}
		return requestedOndWiseNewSegmentFaresMap.get(ondSequence);
	}

	public LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>> getOndWiseNewSegmentFaresMap() {
		if (ondWiseNewSegmentFaresMap == null) {
			ondWiseNewSegmentFaresMap = new LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>>();
		}
		return ondWiseNewSegmentFaresMap;
	}

	private LinkedHashMap<Integer, SegmentFareDTO> getOndNewSegmentFaresMap(int ondSequence) {
		if (!getOndWiseNewSegmentFaresMap().containsKey(ondSequence)) {
			ondWiseNewSegmentFaresMap.put(ondSequence, new LinkedHashMap<Integer, SegmentFareDTO>());
		}
		return ondWiseNewSegmentFaresMap.get(ondSequence);
	}

	public LinkedHashMap<Integer, SegmentFareDTO> getNewSegmentFaresMap(int ondSequence) throws ModuleException {
		processSegments();
		return ondWiseNewSegmentFaresMap.get(ondSequence);
	}

	public Map<Integer, List<Integer>> getOndSegmentIdMap() throws ModuleException {
		processSegments();
		Map<Integer, List<Integer>> map = new HashMap<>();
		for (Integer ondSequence : getOndWiseNewSegmentFaresMap().keySet()) {
			for (SegmentFareDTO segFare : getOndWiseNewSegmentFaresMap().get(ondSequence).values()) {
				if (!map.containsKey(ondSequence)) {
					map.put(ondSequence, new ArrayList<>());
				}
				map.get(ondSequence).add(segFare.getFlightSegId());
			}
		}
		return map;
	}

	private void processSegments() throws ModuleException {
		if (!processedSegments) {
			ondWiseNewSegmentFaresMap = new LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>>();
			requestedOndWiseNewSegmentFaresMap = new LinkedHashMap<Integer, LinkedHashMap<Integer, SegmentFareDTO>>();
			Iterator<SegmentFareDTO> itrSegFare = null;
			if (hasExistingFareOnds()) {
				itrSegFare = getExistingSegmentFaresMap().values().iterator();
			} else {
				itrSegFare = getChangedSegmentFaresMap().values().iterator();
			}
			while (itrSegFare.hasNext()) {
				SegmentFareDTO segFare = itrSegFare.next();
				SegmentFareDTO segFareDTO = segFare;
				if (getChangedSegmentFaresMap().containsKey(segFare.getFlightSegId())) {
					segFareDTO = getChangedSegmentFaresMap().get(segFare.getFlightSegId());
				}

				updateSegmentSeatDistributionForFlown(segFareDTO, segFare);
				if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
					getRequestedOndWiseOndNewSegmentFaresMap(segFare.getRequestedOndSequence()).put(segFare.getFlightSegId(),
							segFareDTO);
				}

				getOndNewSegmentFaresMap(segFare.getOndSequence()).put(segFare.getFlightSegId(), segFareDTO);

			}
			processedSegments = true;
		}
	}

	public boolean hasInboundOnds() throws ModuleException {
		LinkedHashMap<Integer, SegmentFareDTO> segFareMap = getNewSegmentFaresMap(OndSequence.OUT_BOUND);
		if (segFareMap != null && !segFareMap.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public Set<Integer> getSegmentIds(int ondSequence) throws ModuleException {
		LinkedHashMap<Integer, SegmentFareDTO> segFareMap = null;

		if (segFareMap == null || segFareMap.size() == 0) {
			segFareMap = getNewSegmentFaresMap(ondSequence);
		}
		if (segFareMap != null && !segFareMap.isEmpty()) {
			return segFareMap.keySet();
		} else {
			return new HashSet<Integer>();
		}
	}

	public int getSegmentCount(int ondSequence) throws ModuleException {
		Set<Integer> segmentIds = getSegmentIds(ondSequence);
		return segmentIds.size();
	}

	public boolean hasExistingFareOnds() {
		return existingFareChargesSeatsSegmentsDTOs != null && existingFareChargesSeatsSegmentsDTOs.size() > 0;
	}

	public void setInfantSeats(int infantCount) {
		this.infantCount = infantCount;
	}

	public void setLastFQDate(Date lastFareQuotedDate) {
		this.lastFareQuotedDate = lastFareQuotedDate;

	}

	public Date getLastFareQuotedDate() {
		return lastFareQuotedDate;
	}

	public void setFQWithinValidity(boolean fqWithinValidity) {
		this.fqWithinValidity = fqWithinValidity;
	}

	public boolean isFQWithinValidity() {
		return fqWithinValidity;
	}

	public ArrayList<Integer> getOldFltSegIdList() {
		return oldFltSegIdList;
	}

	public void setOldFltSegIdList(ArrayList<Integer> oldFltSegIdList) {
		this.oldFltSegIdList = oldFltSegIdList;
	}

	private void updateSegmentSeatDistribution(Collection<SeatDistribution> seatDistributionColl, Integer fltSegId) {
		if (oldFltSegIdList != null && oldFltSegIdList.size() > 0 && !oldFltSegIdList.contains(fltSegId)) {

			if (seatDistributionColl != null && seatDistributionColl.size() > 0) {

				Iterator<SeatDistribution> seatDistributionsIt = seatDistributionColl.iterator();
				for (int i = 0; seatDistributionsIt.hasNext(); i++) {
					SeatDistribution seatDist = seatDistributionsIt.next();

					if (seatDist.isBookedFlightCabinBeingSearched())
						seatDist.setBookedFlightCabinBeingSearched(false);

					if (seatDist.isSameBookingClassAsExisting())
						seatDist.setSameBookingClassAsExisting(false);

				}
			}
		}
	}

	// this function will update the flown segments flag in modified SegmentFareDTO where this is will checked to skip
	// the inventory updates for flown segment
	private void updateSegmentSeatDistributionForFlown(SegmentFareDTO changeSegFare, SegmentFareDTO existSegFare) {

		if (existSegFare.getSeatDistributions() != null && existSegFare.getSeatDistributions().size() > 0) {
			Iterator<SeatDistribution> existSeatDistItr = existSegFare.getSeatDistributions().iterator();
			while (existSeatDistItr.hasNext()) {
				SeatDistribution existSeatDist = existSeatDistItr.next();
				if (existSeatDist.isFlownOnd()) {
					Iterator<SeatDistribution> changeSeatDistItr = changeSegFare.getSeatDistributions().iterator();
					while (changeSeatDistItr.hasNext()) {
						SeatDistribution changeSeatDist = changeSeatDistItr.next();
						changeSeatDist.setFlownOnd(true);
					}
				}
			}

		}
	}
	
	private boolean isSplitOperationInvokedForOnd(Collection<OndFareDTO> ondFares){
		
		boolean isSplitInvoked = false;
		for(OndFareDTO fareDTO : ondFares){
			if(fareDTO.isSplitOperationForBusInvoked()){
				isSplitInvoked = true;
				break;
			}
		}
		
		return isSplitInvoked;
	}
	
	public ApplicablePromotionDetailsTO getApplicablePromotionDetails() {
		return applicablePromotionDetails;
	}

	public void setApplicablePromotionDetails(ApplicablePromotionDetailsTO applicablePromotionDetails) {
		this.applicablePromotionDetails = applicablePromotionDetails;
	}

	public Map<Integer, Boolean> getOndSequenceReturnMap() {
		return ondSequenceReturnMap;
	}

	public void setOndSequenceReturnMap(Map<Integer, Boolean> ondSequenceReturnMap) {
		this.ondSequenceReturnMap = ondSequenceReturnMap;
	}

	public Map<Integer, Map<String, List<BundledFareLiteDTO>>> getAvailableBundledFares() {
		if (availableBundledFares == null) {
			availableBundledFares = new HashMap<Integer, Map<String, List<BundledFareLiteDTO>>>();
		}
		return availableBundledFares;
	}

	public void setAvailableBundledFares(Map<Integer, Map<String, List<BundledFareLiteDTO>>> availableBundledFares) {
		this.availableBundledFares = availableBundledFares;
	}

	public Map<Integer, BundledFareDTO> getOndSelectedBundledFares() {
		return ondSelectedBundledFares;
	}

	public void setOndSelectedBundledFares(Map<Integer, BundledFareDTO> ondSelectedBundledFares) {
		this.ondSelectedBundledFares = ondSelectedBundledFares;
	}

	public void setProcessSegments(boolean processedSegments) {
		this.processedSegments = processedSegments;
	}
}
