package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class AvailableFlightRollForwardDTO implements Serializable, Comparable<AvailableFlightRollForwardDTO> {
	private static final long serialVersionUID = 1L;

	private int availableAdultSeatForSeg;
	private int availableInfantSeatForSeg;
	private int availableAdultSeatsForBC;
	private String cabinClassCode;
	private String bookingClasscode;
	private int fareId;
	private int fareRuleId;
	private String returnflag;
	private int segmentSeq;
	private FlightSegmentDTO flightSegmentDTO = null;
	private boolean openReturn;
	private String bcAllocStatus;
	private String statusChgAction;	

	public int getAvailableAdultSeatForSeg() {
		return availableAdultSeatForSeg;
	}

	public void setAvailableAdultSeatForSeg(int availableAdultSeatForSeg) {
		this.availableAdultSeatForSeg = availableAdultSeatForSeg;
	}

	public int getAvailableInfantSeatForSeg() {
		return availableInfantSeatForSeg;
	}

	public void setAvailableInfantSeatForSeg(int availableInfantSeatForSeg) {
		this.availableInfantSeatForSeg = availableInfantSeatForSeg;
	}

	public int getAvailableAdultSeatsForBC() {
		return availableAdultSeatsForBC;
	}

	public void setAvailableAdultSeatsForBC(int availableAdultSeatsForBC) {
		this.availableAdultSeatsForBC = availableAdultSeatsForBC;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getBookingClasscode() {
		return bookingClasscode;
	}

	public void setBookingClasscode(String bookingClasscode) {
		this.bookingClasscode = bookingClasscode;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getReturnflag() {
		return returnflag;
	}

	public void setReturnflag(String returnflag) {
		this.returnflag = returnflag;
	}

	public int getSegmentSeq() {
		return segmentSeq;
	}

	public void setSegmentSeq(int segmentSeq) {
		this.segmentSeq = segmentSeq;
	}

	public FlightSegmentDTO getFlightSegmentDTO() {
		return flightSegmentDTO;
	}

	public void setFlightSegmentDTO(FlightSegmentDTO flightSegmentDTO) {
		this.flightSegmentDTO = flightSegmentDTO;
	}

	public int compareTo(AvailableFlightRollForwardDTO obj) {
		return this.getFlightSegmentDTO().getDepartureDateTime().compareTo(obj.getFlightSegmentDTO().getDepartureDateTime());
	}

	/**
	 * @return the openReturn
	 */
	public boolean isOpenReturn() {
		return openReturn;
	}

	/**
	 * @param openReturn
	 *            the openReturn to set
	 */
	public void setOpenReturn(boolean openReturn) {
		this.openReturn = openReturn;
	}

	public String getBcAllocStatus() {
		return bcAllocStatus;
	}

	public void setBcAllocStatus(String bcAllocStatus) {
		this.bcAllocStatus = bcAllocStatus;
	}

	public String getStatusChgAction() {
		return statusChgAction;
	}

	public void setStatusChgAction(String statusChgAction) {
		this.statusChgAction = statusChgAction;
	}

}
