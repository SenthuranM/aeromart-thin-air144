package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;

/**
 * 
 * @hibernate.class table = "ML_T_FLIGHT_MEAL_CHARGE_COS"
 * 
 */
public class FlightMealCharges implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8675561262799408843L;

	private Integer mealChargeCOSId;

	private FlightMeal flightMeal;

	private String cabinClassCode;

	private String logicalCCCode;

	/**
	 * @return the mealChargeCOSId
	 * 
	 * @hibernate.id column="MEAL_CHARGE_COS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_FLIGHT_MEAL_CHARGE_COS"
	 */
	public Integer getMealChargeCOSId() {
		return mealChargeCOSId;
	}

	/**
	 * @param mealChargeCOSId
	 *            the mealChargeCOSId to set
	 */
	public void setMealChargeCOSId(Integer mealChargeCOSId) {
		this.mealChargeCOSId = mealChargeCOSId;
	}

	/**
	 * @return the cabinClassCode
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCCCode
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the flightMeal
	 * @hibernate.many-to-one column="FLIGHT_MEAL_CHARGE_ID" class="com.isa.thinair.airinventory.api.model.FlightMeal"
	 */
	public FlightMeal getFlightMeal() {
		return flightMeal;
	}

	/**
	 * @param flightMeal
	 *            the flightMeal to set
	 */
	public void setFlightMeal(FlightMeal flightMeal) {
		this.flightMeal = flightMeal;
	}
}