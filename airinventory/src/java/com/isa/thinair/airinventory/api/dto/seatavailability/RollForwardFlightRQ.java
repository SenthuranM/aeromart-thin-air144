package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author rumesh
 * 
 */
public class RollForwardFlightRQ implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date onholdReleaseTime;
	private List<RollForwardFlightDTO> rollForwardFlights;

	public Date getOnholdReleaseTime() {
		return onholdReleaseTime;
	}

	public void setOnholdReleaseTime(Date onholdReleaseTime) {
		this.onholdReleaseTime = onholdReleaseTime;
	}

	public List<RollForwardFlightDTO> getRollForwardFlights() {
		return rollForwardFlights;
	}

	public void setRollForwardFlights(List<RollForwardFlightDTO> rollForwardFlights) {
		this.rollForwardFlights = rollForwardFlights;
	}
}
