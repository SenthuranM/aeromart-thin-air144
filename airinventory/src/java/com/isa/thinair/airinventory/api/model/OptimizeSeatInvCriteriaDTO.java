package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class OptimizeSeatInvCriteriaDTO implements Serializable {

	private static final long serialVersionUID = -6296657285685805718L;

	public static final String AVAILABILITY_OPERATOR_GREATER_THAN = ">";

	public static final String AVAILABILITY_OPERATOR_LESS_THAN = "<";

	public static final String AVAILABILITY_OPERATOR_LESS_THAN_OR_EQUAL = "<=";

	public static final String AVAILABILITY_OPERATOR_GREATER_THAN_OR_EQUAL = ">=";

	private Date fromDate;

	private Date toDate;

	private String flightNumber;

	private Collection<String> ondCodes;

	private boolean includeAllBCs;

	private boolean includeStandardBCsOnly;

	private Collection<String> selectedBCs;

	private boolean performAvailableOnSegment;

	private boolean performAvailableOnBC;

	private String availabilityOperator;

	private boolean checkNegativeAvailability;

	private int noOfSeats;

	private String agentCode;

	private String cabinClassCode = null;

	private String availabilityOperatorForBCInventory;

	private int noOfSeatsForBCInventory;

	private Collection<String> applicapableCarrierCodes;

	public OptimizeSeatInvCriteriaDTO() {

	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAvailabilityOperator() {
		return availabilityOperator;
	}

	public void setAvailabilityOperator(String availabilityOperator) {
		this.availabilityOperator = availabilityOperator;
	}

	public boolean isCheckNegativeAvailability() {
		return checkNegativeAvailability;
	}

	public void setCheckNegativeAvailability(boolean checkNegativeAvailability) {
		this.checkNegativeAvailability = checkNegativeAvailability;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public boolean isIncludeAllBCs() {
		return includeAllBCs;
	}

	public void setIncludeAllBCs(boolean includeAllBCs) {
		this.includeAllBCs = includeAllBCs;
	}

	public boolean isIncludeStandardBCsOnly() {
		return includeStandardBCsOnly;
	}

	public void setIncludeStandardBCsOnly(boolean includeStandardBCsOnly) {
		this.includeStandardBCsOnly = includeStandardBCsOnly;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public Collection<String> getOndCodes() {
		return ondCodes;
	}

	public void setOndCodes(Collection<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	public boolean isPerformAvailableOnBC() {
		return performAvailableOnBC;
	}

	public void setPerformAvailableOnBC(boolean performAvailableOnBC) {
		this.performAvailableOnBC = performAvailableOnBC;
	}

	public boolean isPerformAvailableOnSegment() {
		return performAvailableOnSegment;
	}

	public void setPerformAvailableOnSegment(boolean performAvailableOnSegment) {
		this.performAvailableOnSegment = performAvailableOnSegment;
	}

	public Collection<String> getSelectedBCs() {
		return selectedBCs;
	}

	public void setSelectedBCs(Collection<String> selectedBCs) {
		this.selectedBCs = selectedBCs;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getAvailabilityOperatorForBCInventory() {
		return availabilityOperatorForBCInventory;
	}

	public void setAvailabilityOperatorForBCInventory(String availabilityOperatorForBCInventory) {
		this.availabilityOperatorForBCInventory = availabilityOperatorForBCInventory;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public int getNoOfSeatsForBCInventory() {
		return noOfSeatsForBCInventory;
	}

	public void setNoOfSeatsForBCInventory(int noOfSeatsForBCInventory) {
		this.noOfSeatsForBCInventory = noOfSeatsForBCInventory;
	}

	public Collection<String> getApplicapableCarrierCodes() {
		return applicapableCarrierCodes;
	}

	public void setApplicapableCarrierCodes(Collection<String> applicapableCarrierCodes) {
		this.applicapableCarrierCodes = applicapableCarrierCodes;
	}

}
