package com.isa.thinair.airinventory.api.util;

import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;

public abstract class AirinventoryCustomConstants {

	public static final String INV_MODULE_CODE = "airinventory.desc";

	public static final String SINGLE_FLT_SEGMENT_SEAT_AVAIL = "SINGLE_FLT_SEGMENT_SEAT_AVAIL";
	public static final String SINGLE_FLT_SEGMENT_SEAT_AVAILABILITY_FOR_GIVEN_FCCSA_ID = "SINGLE_FLT_SEGMENT_SEAT_AVAILABILITY_FOR_GIVEN_FCCSA_ID";
	public static final String GET_ALL_BOOKING_CLASSES_FARES_SUMMARY_FOR_FLT_LOGICALCABINCLASS = "GET_ALL_BOOKING_CLASSES_FARES_SUMMARY_FOR_FLT_LOGICALCABINCLASS";
	public static final String GET_ONEWAY_BOOKING_CLASSES_FARES_SEATS_FOR_FLT = "GET_ONEWAY_BOOKING_CLASSES_FARES_SEATS_FOR_FLT";
	public static final String GOSHOW_FARE_QUOTE = "GOSHOW_FARE_QUOTE";
	public static final String RETRIEVE_FIGHTS_FOR_RM_OPTIMIZATION = "RETRIEVE_FIGHTS_FOR_RM_OPTIMIZATION";
	public static final String RETRIEVE_INVENTORIES_FOR_RM_OPTIMIZATION = "RETRIEVE_INVENTORIES_FOR_RM_OPTIMIZATION";
	public static final String DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS = "DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS";
	public static final String DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_MODIFIED = "DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_MODIFIED";
	public static final String DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_ENHANCED = "DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_ENHANCED";
	public static final String DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_ENHANCED_MODIFIED = "DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_ENHANCED_MODIFIED";
	public static final String DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS = "DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS";
	public static final String DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS_ENHANCED = "DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS_ENHANCED";
	public static final String MYID_FLIGHT_BC_AVAILABILITY = "MYID_FLIGHT_BC_AVAILABILITY";

	public static final String CHECK_FCC_INVENTORY_ADULT_SEATS_CONSTRAIN = "CHECK_FCC_INVENTORY_ADULT_SEATS_CONSTRAIN";
	public static final String CHECK_FCC_INVENTORY_FIXED_SEATS_CONSTRAIN = "CHECK_FCC_INVENTORY_FIXED_SEATS_CONSTRAIN";

	public static final String RETRIEVE_FCC_SEG_INVENTORY_SUMMARY = "RETRIEVE_FCC_SEG_INVENTORY_SUMMARY";
	// charge quoting
	public static final String[] NO_CHARGE_CAT_CODE_RESTRICTION = new String[] {};

	public static final String[] DEFAULT_CHARGE_GROUPS = { ChargeGroups.TAX, ChargeGroups.SURCHARGE,
			ChargeGroups.INFANT_SURCHARGE };

	public static final String DATABASE_ANY_CHARACTER_MATCHER = "DATABASE_ANY_CHARACTER_MATCHER";

	public static final int DB_OPERATION_CHUNK_SIZE = 100;

	public static final String HALA_NOTIFICATION_MAIL_TEMPLATE = "booked_hala_service_notification";

	public static final String MEAL_NOTIFY_EMAIL_TEMPLATE = "meal_notification";
	
	public static final String INVENTORY_TEMPLATE = "inventory_template";

	// Response Params

	public static interface FlightSeatCharge {
		public static String ASSIGNED = "ASSIGNED";
		public static String NOTASSIGNED = "NOTASSIGNED";
		public static String NEWLYASSIGNED = "NEWLTASSIGNED";
	}

	public static interface FlightMealCharge {
		public static String ASSIGNED = "ASSIGNED";
		public static String NOTASSIGNED = "NOTASSIGNED";
		public static String NEWLYASSIGNED = "NEWLTASSIGNED";
	}

	public static interface FlightBaggageCharge {
		public static String ASSIGNED = "ASSIGNED";
		public static String NOTASSIGNED = "NOTASSIGNED";
		public static String NEWLYASSIGNED = "NEWLTASSIGNED";
	}

	public static AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}

	/** TempSegBcAlloc status **/
	public static interface Seats {
		public static String BLOCKED = "B";
		public static String RELEASED = "R";
		public static String CONSUMED = "C";
		public static String ABORTED = "A";
	}

	public static interface InventoryRMOptimizationStatus {
		public static String OPTIMIZED = "O";
		public static String OPTIMISATION_FAILED = "F";
		public static String OPTIMIZATION_PENDING = "P";
	}

	public static interface InventoryRMThresholdStatus {
		public static String ABOVE_THRESHOLD = "E";
		public static String BELOW_THRESHOLD = "D";
	}

	/** Flight Seat Statuses */
	public static interface FlightSeatStatuses {

		public static final String BLOCKED = "BLK";
		public static final String RESERVED = "RES";
		public static final String VACANT = "VAC";
		public static final String INACTIVE = "INA";
		public static final String ACQUIRED = "ACQ";
		public static final String OHD = "OHD";
		public static final String PAX_CNX = "CNX";
	}

	/** Flight meal Statuses */
	public static interface FlightMealStatuses {

		public static final String INACTIVE = "INA";
		public static final String RESERVED = "RES";
		public static final String PAX_CNX = "CNX";
		public static final String VACANT = "VAC";
		public static final String ACTIVE = "ACT";

	}

	/** Flight Baggage Statuses */
	public static interface FlightBaggageStatuses {

		public static final String INACTIVE = "INA";
		public static final String RESERVED = "RES";
		public static final String PAX_CNX = "CNX";
		public static final String VACANT = "VAC";
		public static final String ACTIVE = "ACT";

	}

	/** Flight Seat Statuses */
	public static interface FlightSeatMap {

		public static final String EXIT_SEAT = "EXIT";
		public static final int LEFT_ROW_GROUP = 1;
	}

	public static enum PublishInventoryEventCode {
		AVS_FLIGHT_CLOSED("FLCL", true), AVS_CANCEL_FLIGHT("FLCX", true),

		AVS_CREATE_FLIGHT("FLCR", false), AVS_PUBLISH_FLIGHT("FLPU", false), AVS_UNPUBLISED_FLIGHT("FLUP", true),

		AVS_AVAIL_BELOW_TRESH("RBBT", false), AVS_RBD_CHANGE_ALLOCATION("RBCA", false), AVS_RBD_AVAILABILITY_CHANGE("RBAV", false), AVS_SEG_AVAILABILITY_CHANGE(
				"SEGA", false), AVS_RBD_SEG_AVAILABILITY_CHANGE("RBSA", false), AVS_RBD_SEG_AVAILABILITY_RESET("RBSR", false), AVS_RBD_CLOSED(
				"RBCL", false), AVS_RBD_REOPEN("RBRO", false), AVS_AVAIL_ABOVE_THRESHOLD("RBAT", false), AVS_FLIGHT_REOPEN(
				"FLRO", false),

		AVS_INACTIVATE_BC("INAC", false), AVS_ACTIVATE_BC("ACTV", false),
		
		AVS_RBD_REQUEST("RBRQ", false);

		private final String code;
		private final boolean isSegmentEvent;

		private PublishInventoryEventCode(String code, boolean isSegmentEvent) {
			this.code = code;
			this.isSegmentEvent = isSegmentEvent;
		}

		public String getCode() {
			return code;
		}

		public boolean isSegmentEvent() {
			return this.isSegmentEvent;
		}

		public static PublishInventoryEventCode getPublishInventoryEventCode(String eventCode) {
			PublishInventoryEventCode eventEnum = null;
			for (PublishInventoryEventCode e : PublishInventoryEventCode.values()) {
				if (e.getCode().equals(eventCode)) {
					eventEnum = e;
					break;
				}
			}
			return eventEnum;
		}
	}

	public static enum PublishInventoryEventStatus {
		NEW_EVENT("NEW"), EVENT_PUBLISHED("PUB"), EVENT_PUBLISH_IGNORED("PIG"), EVENT_PUBLISH_FAILED("PFA");

		private final String code;

		private PublishInventoryEventStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public static enum AvsGenerationFlow {

		NORMAL("NORMAL"), BC_STATUS_UPDATE("BC_STATUS_UPDATE");

		private final String code;

		private AvsGenerationFlow(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public static interface ChargeCodes {
		public static final String FARE_DISCOUNT = "FAR_DISC";
		public static final String DOM_FARE_DISCOUNT = "DOM_DISC";
		public static final String FARE_ADJUSTMENTS = "FAR_ADJ";
		public static final String BUYANDGET_PROMO = "BGF_PROMO";
		public static final String DISCOUNT_PROMO = "DIS_PROMO";
		public static final String FREE_SERVICE_PROMO = "FRE_PROMO";
		public static final String DISCOUNT_PROMO_ADJUSTMENT = "ADJ_PROMO";
		public static final String CABIN_CHANGE_ADJUSTMENT = "CABIN_CHG";
		public static final String JN_FARE_SURCHARGE = "JN";
		public static final String JN_ANCI_TAX = "JNANCI";
		public static final String JN_MOD_CNX_TAX = "JNEXT";
		public static final String JN_HANDLING_CC_ADJ = "JNOTHER";
		public static final String CNX_SEGMENT_SYSTEM_ADJ = "ADJ_CNX";
		public static final String NAME_CHANGE_CHARGE = "NCC";
		public static final String GDS_FARE_GROUP_CHARGE_ADJ = "ADJFARGP";
		public static final String GDS_TAX_GROUP_CHARGE_ADJ = "ADJTAXGP";
		public static final String GDS_SUR_GROUP_CHARGE_ADJ = "ADJSURGP";
		public static final String GDS_OTHER_GROUP_CHARGE_ADJ = "ADJANYGP";
	}

	// use only if the runtime fails to load values from the GDS table
	public static final Integer AVS_AVAIL_THRESHOLD = 9;
	public static final Integer AVS_CLOSURE_THRESHOLD = 4;

	/** AutoCheckin Statuses */
	public static interface AutoCheckinConstants {

		public static final String SIT_TOGETHER = "S";
		public static final String SEAT_WINDOW = "W";
		public static final String SEAT_MIDDLE = "M";
		public static final String SEAT_AISLE = "A";
		public static final String WINDOW_NEXT_SEAT = "M";
		public static final String MIDDLE_NEXT_SEAT = "A";
		public static final String AISLE_NEXT_SEAT = "W";
		
		public static final String COL = "COL";
		public static final String ROW = "ROW";
		public static final String INACTIVE = "INA";
		public static final String ACTIVE = "ACT";
		public static final Integer DEFAULT_ATTEMPT = 0;
		public static final String DCS_STATUS_NS = "NS";
	}

}
