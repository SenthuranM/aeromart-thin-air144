package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class LogicalCabinClassSearchCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String logicalCC;
	private String cabinCLass;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the logicalCC
	 */
	public String getLogicalCC() {
		return logicalCC;
	}

	/**
	 * @param logicalCC
	 *            the logicalCC to set
	 */
	public void setLogicalCC(String logicalCC) {
		this.logicalCC = logicalCC;
	}

	/**
	 * @return the cabinCLass
	 */
	public String getCabinCLass() {
		return cabinCLass;
	}

	/**
	 * @param cabinCLass
	 *            the cabinCLass to set
	 */
	public void setCabinCLass(String cabinCLass) {
		this.cabinCLass = cabinCLass;
	}
}
