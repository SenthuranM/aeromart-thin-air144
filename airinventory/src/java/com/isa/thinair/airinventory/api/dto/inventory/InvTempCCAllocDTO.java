package com.isa.thinair.airinventory.api.dto.inventory;

import java.io.Serializable;
import java.util.ArrayList;

public class InvTempCCAllocDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer invTempID;

	private Integer invTempCabinAllocID;

	private String cabinClassCode;

	private String logicalCabinClassCode;

	private Integer adultAllocation;

	private Integer infantAllocation;

	private ArrayList<InvTempCCBCAllocDTO> invTempCCBCAllocDTOList;

	/**
	 * @return the invTempID
	 */
	public Integer getInvTempID() {
		return invTempID;
	}

	/**
	 * @param invTempID
	 *            the invTempID to set
	 */
	public void setInvTempID(Integer invTempID) {
		this.invTempID = invTempID;
	}


	/**
	 * @return the invTempCabinAllocID
	 */
	public Integer getInvTempCabinAllocID() {
		return invTempCabinAllocID;
	}

	/**
	 * @param invTempCabinAllocID
	 *            the invTempCabinAllocID to set
	 */
	public void setInvTempCabinAllocID(Integer invTempCabinAllocID) {
		this.invTempCabinAllocID = invTempCabinAllocID;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCabinClassCode
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	/**
	 * @param logicalCabinClassCode
	 *            the logicalCabinClassCode to set
	 */
	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return the adultAllocation
	 */
	public Integer getAdultAllocation() {
		return adultAllocation;
	}

	/**
	 * @param adultAllocation
	 *            the adultAllocation to set
	 */
	public void setAdultAllocation(Integer adultAllocation) {
		this.adultAllocation = adultAllocation;
	}

	/**
	 * @return the infantAllocation
	 */
	public Integer getInfantAllocation() {
		return infantAllocation;
	}

	/**
	 * @param infantAllocation
	 *            the infantAllocation to set
	 */
	public void setInfantAllocation(Integer infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	public ArrayList<InvTempCCBCAllocDTO> getInvTempCCBCAllocDTOList() {
		return invTempCCBCAllocDTOList;
	}

	public void setInvTempCCBCAllocDTOList(ArrayList<InvTempCCBCAllocDTO> invTempCCBCAllocDTOList) {
		this.invTempCCBCAllocDTOList = invTempCCBCAllocDTOList;
	}

}
