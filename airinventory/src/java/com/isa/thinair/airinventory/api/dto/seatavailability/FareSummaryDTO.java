package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Holds summarized quoted fare information.
 * 
 * @author Mohamed Nasly
 */
public class FareSummaryDTO extends BasicFareSummaryDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int fareId;

	/** Holds the percentage of the total fare. 100 % for segment fare and lesser for return and half return */
	private Integer farePercentage;

	private String bookingClassCode;

	private int fareRuleID;

	private String fareCategoryCode;

	private String fareRuleCode = "";

	private String fareBasisCode = "";

	private String fareRuleComment = "";

	private String fareRuleCommentSelectedLanguage = "";

	private String childFareType;

	private String infantFareType;

	private long minimumStayOverInMins;

	private long maximumStayOverInMins;

	private long openRTConfPeriodInMins;

	private long minimumStayOverInMonths;

	private long maximumStayOverInMonths;

	private long openRTConfPeriodInMonths;

	// Used for override fare only

	private boolean isFareCommonAllSegments;

	private Collection<Integer> visibleChannels;

	private Collection<String> visibleAgents;

	private boolean AdultApplicability;

	private boolean ChildApplicability;

	private boolean InfantApplicability;

	private Collection<String> visibleChannelNames = null;

	private boolean isAdultFareRefundable;
	private boolean isChildFareRefundable;
	private boolean isInfantFareRefundable;

	private Date timepsatmp;

	private String ondCode;

	private String currenyCode;

	private DepAPFareDTO depAPFareDTO;

	private Integer fareDiscountMin;

	private Integer fareDiscountMax;

	private String agentInstructions = "";

	private String halfReturnFlag;

	private boolean hasFlexi;

	private Integer flexiRuleID;

	private int noOfAvailableSeats = -1;

	private String agentCommissionType;

	private BigDecimal agentCommissionAmount;

	private String ratioCategory;

	private PaxTypewiseFareONDChargeInfo paxTypewiseFareOndChargeInfo;

	private boolean returnFare;
	
	/** Holds the calculated fare adjustment amount PAX Type[AD,CH,IN] wise */
	private Map<String, Double> effectiveROEAdjustmentAmountsMap;

	public FareSummaryDTO() {
		super();
		this.timepsatmp = new Date();
	}

	private FareSummaryDTO(Date timepsatmp) {
		super();
		this.timepsatmp = new Date(timepsatmp.getTime());
	}

	/**
	 * Creates a Basic FareSummaryDTO via FareTO
	 * 
	 * @param fareTO
	 */
	public FareSummaryDTO(FareTO fareTO) {
		this();
		this.setFareId(fareTO.getFareId());
		this.setAdultFare(fareTO.getAdultFareAmount().doubleValue());
		this.setFareCategoryCode(fareTO.getFareCategoryCode());
		this.setBookingClassCode(fareTO.getBookingClassCode());
		this.setFareRuleCode(fareTO.getFareRuleCode());
		this.setFareBasisCode(fareTO.getFareBasisCode());
		this.setFareRuleComment(fareTO.getFareRulesComments());
		this.setFareRuleCommentSelectedLanguage(fareTO.getFareRulesCommentsInSelected());
		this.setAgentInstructions(fareTO.getAgentInstructions());
		this.setMinimumStayOverInMins(fareTO.getMinimumStayOver());
		this.setMaximumStayOverInMins(fareTO.getMaximumStayOver());
		this.setOpenRTConfPeriodInMins(fareTO.getOpenReturnConfirmPeriod());
		this.setMinimumStayOverInMonths(fareTO.getMinimumStayOverMonths());
		this.setMaximumStayOverInMonths(fareTO.getMaximumStayOverMonths());
		this.setOpenRTConfPeriodInMonths(fareTO.getOpenReturnConfirmPeriodMonths());
		this.setChildFare(fareTO.getChildFareRatio().doubleValue());
		this.setChildFareType(fareTO.getChildFareType());
		this.setInfantFare(fareTO.getInfantFareRatio().doubleValue());
		this.setInfantFareType(fareTO.getInfantFareType());
		this.setAdultApplicability(fareTO.isAdultApplyFare());
		this.setChildApplicability(fareTO.isChildApplyFare());
		this.setInfantApplicability(fareTO.isInfantApplyFare());
		this.setFareRuleID(fareTO.getFareRuleID());
		this.setAdultFareRefundable(fareTO.isAdultFareRefundable());
		this.setChildFareRefundable(fareTO.isChildFareRefundable());
		this.setInfantFareRefundable(fareTO.isInfantFareRefundable());
		this.setOndCode(fareTO.getOndCode());
		this.setFareDiscountMin(fareTO.getFareDiscountMin());
		this.setFareDiscountMax(fareTO.getFareDiscountMax());
		this.setAgentCommissionType(fareTO.getAgentCommissionType());
		this.setAgentCommissionAmount(fareTO.getAgentCommissionAmount());
		this.setCurrenyCode(fareTO.getCurrencyCode());
	}

	public Double getFareAmount(String paxType) {
		if (PaxTypeTO.ADULT.equals(paxType)) {
			return getAdultFare();
		} else if (PaxTypeTO.CHILD.equals(paxType)) {
			return this.getEffectiveFareforChild();
		} else if (PaxTypeTO.INFANT.equals(paxType)) {
			return this.getEffectiveFareforInfant();
		} else {
			throw new ModuleRuntimeException("module.runtime.error");// invalid pax type
		}
	}

	public double getPaxFareAmount(String paxType) {
		if (PaxTypeTO.ADULT.equals(paxType)) {
			return getAdultFare();
		} else if (PaxTypeTO.CHILD.equals(paxType)) {
			return this.getEffectiveFareforChild();
		} else if (PaxTypeTO.INFANT.equals(paxType)) {
			return this.getEffectiveFareforInfant();
		} else {
			throw new ModuleRuntimeException("module.runtime.error");// invalid pax type
		}
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	/**
	 * @return the bookingClassCode
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            the bookingClassCode to set
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	/**
	 * @return Returns the childFareType.
	 */
	public String getChildFareType() {
		return childFareType;
	}

	/**
	 * @param childFareType
	 *            The childFareType to set.
	 */
	public void setChildFareType(String childFareType) {
		this.childFareType = childFareType;
	}

	/**
	 * @return Returns the infantFareType.
	 */
	public String getInfantFareType() {
		return infantFareType;
	}

	/**
	 * @param infantFareType
	 *            The infantFareType to set.
	 */
	public void setInfantFareType(String infantFareType) {
		this.infantFareType = infantFareType;
	}

	public boolean isFareCommonAllSegments() {
		return isFareCommonAllSegments;
	}

	public void setFareCommonAllSegments(boolean isFareCommonAllSegments) {
		this.isFareCommonAllSegments = isFareCommonAllSegments;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getFareRuleComment() {
		return fareRuleComment;
	}

	public void setFareRuleComment(String fareRuleComment) {
		this.fareRuleComment = fareRuleComment;
	}

	public Collection<String> getVisibleAgents() {
		if (visibleAgents == null) {
			visibleAgents = new ArrayList<String>();
		}
		return visibleAgents;
	}

	public void addVisibleAgent(String agentCode) {
		if (this.visibleAgents == null) {
			this.visibleAgents = new ArrayList<String>();
		}
		this.visibleAgents.add(agentCode);
	}

	public Collection<Integer> getVisibleChannels() {
		if (visibleChannels == null) {
			visibleChannels = new ArrayList<Integer>();
		}
		return visibleChannels;
	}

	public void addVisibleChannel(Integer visibleChannelCode) {
		if (this.visibleChannels == null) {
			this.visibleChannels = new ArrayList<Integer>();
		}
		this.visibleChannels.add(visibleChannelCode);
	}

	private double getEffectiveFareforChild() {
		if (Fare.VALUE_PERCENTAGE_FLAG_P.equalsIgnoreCase(getChildFareType())) {
			// In Mahan,normally Adult fare will not more than 1000. Just checked to skip issues in test system
			if (AppSysParamsUtil.isRoundupChildAndInfantFaresWhenCalculatingFromAdultFare() && this.getAdultFare() >= 1000
					&& (this.getCurrenyCode() == null || AppSysParamsUtil.getBaseCurrency().equals(this.getCurrenyCode()))) {
				if (AppSysParamsUtil.getRoundUpCHINFareToNearest() != null) {
					int toNearest = AppSysParamsUtil.getRoundUpCHINFareToNearest();
					return roundUpFareValue(this.getAdultFare() * (getChildFare() / 100f), toNearest);
				} else {
					return applyDefaultAccelAeroRounding(this.getAdultFare() * (getChildFare() / 100f));
				}
			} else {
				return applyDefaultAccelAeroRounding(this.getAdultFare() * (getChildFare() / 100f));
			}
		} else {
			return getChildFare();
		}
	}

	private double applyDefaultAccelAeroRounding(double input) {
		return new Double(AccelAeroCalculator.parseBigDecimal(input).toString()).doubleValue();
	}

	private double roundUpFareValue(double input, int toNearest) {

		int roundedUpAmount = new BigDecimal(input).setScale(0, RoundingMode.UP).intValue();
		while ((roundedUpAmount / toNearest) * toNearest != roundedUpAmount) {
			roundedUpAmount += 1;
		}
		return roundedUpAmount;
	}

	private double getEffectiveFareforInfant() {
		if (Fare.VALUE_PERCENTAGE_FLAG_P.equalsIgnoreCase(getInfantFareType())) {
			if (AppSysParamsUtil.isRoundupChildAndInfantFaresWhenCalculatingFromAdultFare() && this.getAdultFare() >= 1000
					&& (this.getCurrenyCode() == null || AppSysParamsUtil.getBaseCurrency().equals(this.getCurrenyCode()))) {
				if (AppSysParamsUtil.getRoundUpCHINFareToNearest() != null) {
					int toNearest = AppSysParamsUtil.getRoundUpCHINFareToNearest();
					return roundUpFareValue(this.getAdultFare() * (getInfantFare() / 100f), toNearest);
				} else {
					return applyDefaultAccelAeroRounding(this.getAdultFare() * (getInfantFare() / 100f));
				}
			} else {
				return applyDefaultAccelAeroRounding(this.getAdultFare() * (getInfantFare() / 100f));
			}
		} else {
			return getInfantFare();
		}

	}

	public Collection<String> getVisibleChannelNames() {
		if (this.visibleChannels != null && this.visibleChannelNames == null) {
			this.visibleChannelNames = new ArrayList<String>();
			Iterator<Integer> setVisibleChannelsIt = visibleChannels.iterator();
			while (setVisibleChannelsIt.hasNext()) {
				visibleChannelNames.add(SalesChannelsUtil.getSalesChannelName(setVisibleChannelsIt.next().intValue()));
			}
		}
		return this.visibleChannelNames;
	}

	public Date getTimepsatmp() {
		return timepsatmp;
	}

	public void setTimepsatmp(Date timepsatmp) {
		this.timepsatmp = timepsatmp;
	}

	@Override
	public FareSummaryDTO clone() {
		FareSummaryDTO clone = new FareSummaryDTO(this.getTimepsatmp());
		clone.setFareId(getFareId());
		clone.setAdultFare(getPaxFareAmount(PaxTypeTO.ADULT));
		clone.setBookingClassCode(getBookingClassCode());
		clone.setFareCategoryCode(getFareCategoryCode());
		clone.setFareCommonAllSegments(isFareCommonAllSegments());
		clone.setFareRuleCode(getFareRuleCode());
		clone.setFareBasisCode(getFareBasisCode());
		clone.setFareRuleComment(getFareRuleComment());
		clone.setFareRuleCommentSelectedLanguage(getFareRuleCommentSelectedLanguage());
		clone.setAgentInstructions(getAgentInstructions());
		clone.setMinimumStayOverInMins(getMinimumStayOverInMins());
		clone.setMaximumStayOverInMins(getMaximumStayOverInMins());
		clone.setOpenRTConfPeriodInMins(getOpenRTConfPeriodInMins());
		clone.setMinimumStayOverInMonths(getMinimumStayOverInMonths());
		clone.setMaximumStayOverInMonths(getMaximumStayOverInMonths());
		clone.setOpenRTConfPeriodInMonths(getOpenRTConfPeriodInMonths());
		clone.setChildFareType(getChildFareType());
		clone.setChildFare(getChildFare());
		clone.setInfantFareType(getInfantFareType());
		clone.setInfantFare(getInfantFare());
		clone.setAdultApplicability(isAdultApplicability());
		clone.setChildApplicability(isChildApplicability());
		clone.setInfantApplicability(isInfantApplicability());
		clone.setFareRuleID(getFareRuleID());
		clone.setAdultFareRefundable(isAdultFareRefundable());
		clone.setChildFareRefundable(isChildFareRefundable());
		clone.setInfantFareRefundable(isInfantFareRefundable());
		clone.setOndCode(getOndCode());
		clone.setCurrenyCode(getCurrenyCode());
		clone.setDepartureAPFare(getDepartureAPFare());
		clone.setFareDiscountMin(getFareDiscountMin());
		clone.setFareDiscountMax(getFareDiscountMax());
		clone.setHalfReturnFlag(getHalfReturnFlag());
		clone.setHasFlexi(isHasFlexi());
		clone.setFlexiRuleID(getFlexiRuleID());
		clone.setNoOfAvailableSeats(getNoOfAvailableSeats());
		clone.setAgentCommissionType(getAgentCommissionType());
		clone.setAgentCommissionAmount(getAgentCommissionAmount());
		if (getVisibleChannels() != null) {
			Iterator<Integer> visibleChannelsIt = getVisibleChannels().iterator();
			while (visibleChannelsIt.hasNext()) {
				clone.addVisibleChannel(visibleChannelsIt.next());
			}
		}
		if (getVisibleAgents() != null) {
			Iterator<String> visibleAgentsIt = getVisibleAgents().iterator();
			while (visibleAgentsIt.hasNext()) {
				clone.addVisibleAgent(visibleAgentsIt.next());
			}
		}
		clone.setPaxTypewiseFareOndChargeInfo(this.ratioCategory, this.paxTypewiseFareOndChargeInfo);
		clone.setReturnFare(this.isReturnFare());
		return clone;
	}

	public boolean isAdultApplicability() {
		return AdultApplicability;
	}

	public void setAdultApplicability(boolean adultApplicability) {
		AdultApplicability = adultApplicability;
	}

	public boolean isChildApplicability() {
		return ChildApplicability;
	}

	public void setChildApplicability(boolean childApplicability) {
		ChildApplicability = childApplicability;
	}

	public boolean isInfantApplicability() {
		return InfantApplicability;
	}

	public void setInfantApplicability(boolean infantApplicability) {
		InfantApplicability = infantApplicability;
	}

	public void setApplicabilityFromFareSummaryDTO(FareSummaryDTO oNewFareSummary) {
		// Setting the fare applicability levels
		this.setAdultApplicability(oNewFareSummary.isAdultApplicability());
		this.setChildApplicability(oNewFareSummary.isChildApplicability());
		this.setInfantApplicability(oNewFareSummary.isInfantApplicability());

		// Setting the fare refundable levels
		this.setAdultFareRefundable(oNewFareSummary.isAdultFareRefundable());
		this.setChildFareRefundable(oNewFareSummary.isChildFareRefundable());
		this.setInfantFareRefundable(oNewFareSummary.isInfantFareRefundable());
	}

	public int getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(int fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public boolean isAdultFareRefundable() {
		return isAdultFareRefundable;
	}

	public void setAdultFareRefundable(boolean isAdultFareRefundable) {
		this.isAdultFareRefundable = isAdultFareRefundable;
	}

	public boolean isChildFareRefundable() {
		return isChildFareRefundable;
	}

	public void setChildFareRefundable(boolean isChildFareRefundable) {
		this.isChildFareRefundable = isChildFareRefundable;
	}

	public boolean isInfantFareRefundable() {
		return isInfantFareRefundable;
	}

	public void setInfantFareRefundable(boolean isInfantFareRefundable) {
		this.isInfantFareRefundable = isInfantFareRefundable;
	}

	/**
	 * @return the minimumStayOverInMins
	 */
	public long getMinimumStayOverInMins() {
		return minimumStayOverInMins;
	}

	/**
	 * @param minimumStayOverInMins
	 *            the minimumStayOverInMins to set
	 */
	public void setMinimumStayOverInMins(long minimumStayOverInMins) {
		this.minimumStayOverInMins = minimumStayOverInMins;
	}

	/**
	 * @return the maximumStayOverInMins
	 */
	public long getMaximumStayOverInMins() {
		return maximumStayOverInMins;
	}

	/**
	 * @param maximumStayOverInMins
	 *            the maximumStayOverInMins to set
	 */
	public void setMaximumStayOverInMins(long maximumStayOverInMins) {
		this.maximumStayOverInMins = maximumStayOverInMins;
	}

	/**
	 * @return the openRTConfPeriodInMins
	 */
	public long getOpenRTConfPeriodInMins() {
		return openRTConfPeriodInMins;
	}

	/**
	 * @param openRTConfPeriodInMins
	 *            the openRTConfPeriodInMins to set
	 */
	public void setOpenRTConfPeriodInMins(long openRTConfPeriodInMins) {
		this.openRTConfPeriodInMins = openRTConfPeriodInMins;
	}

	/**
	 * @return the minimumStayOverInMonths
	 */
	public long getMinimumStayOverInMonths() {
		return minimumStayOverInMonths;
	}

	/**
	 * @param minimumStayOverInMonths
	 *            the minimumStayOverInMonths to set
	 */
	public void setMinimumStayOverInMonths(long minimumStayOverInMonths) {
		this.minimumStayOverInMonths = minimumStayOverInMonths;
	}

	/**
	 * @return the maximumStayOverInMonthsgetFlightRS
	 */
	public long getMaximumStayOverInMonths() {
		return maximumStayOverInMonths;
	}

	/**
	 * @param maximumStayOverInMonths
	 *            the maximumStayOverInMonths to set
	 */
	public void setMaximumStayOverInMonths(long maximumStayOverInMonths) {
		this.maximumStayOverInMonths = maximumStayOverInMonths;
	}

	/**
	 * @return the openRTConfPeriodInMonths
	 */
	public long getOpenRTConfPeriodInMonths() {
		return openRTConfPeriodInMonths;
	}

	/**
	 * @param openRTConfPeriodInMonths
	 *            the openRTConfPeriodInMonths to set
	 */
	public void setOpenRTConfPeriodInMonths(long openRTConfPeriodInMonths) {
		this.openRTConfPeriodInMonths = openRTConfPeriodInMonths;
	}

	/**
	 * @return the ondCode
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * get the percentage of the total fare.
	 * 
	 * @return
	 */
	public Integer getFarePercentage() {
		return farePercentage;
	}

	public void setFarePercentage(Integer farePercentage) {
		this.farePercentage = farePercentage;
	}

	public String getCurrenyCode() {
		return currenyCode;
	}

	public void setCurrenyCode(String currenyCode) {
		this.currenyCode = currenyCode;
	}

	public void setDepartureAPFare(DepAPFareDTO depAPFareDTO) {
		this.depAPFareDTO = depAPFareDTO;
	}

	public DepAPFareDTO getDepartureAPFare() {
		return depAPFareDTO;
	}

	public Integer getFareDiscountMin() {
		return fareDiscountMin;
	}

	public void setFareDiscountMin(Integer fareDiscountMin) {
		this.fareDiscountMin = fareDiscountMin;
	}

	public Integer getFareDiscountMax() {
		return fareDiscountMax;
	}

	public void setFareDiscountMax(Integer fareDiscountMax) {
		this.fareDiscountMax = fareDiscountMax;
	}

	public boolean hasFareDiscount() {
		return (this.fareDiscountMin != null && this.fareDiscountMax != null);
	}

	@Override
	public String toString() {

		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("------------FareSummaryDTO-------------------\n");
		sb.append("\t").append("fareId 					= " + fareId).append("\n");
		sb.append("\t").append("farePercentage 			= " + farePercentage).append("\n");
		sb.append("\t").append("fareAmount 				= " + getAdultFare()).append("\n");
		sb.append("\t").append("isFareCommonAllSegments = " + isFareCommonAllSegments).append("\n");

		sb.append("\t").append("childFare 				= " + getChildFare()).append("\n");
		sb.append("\t").append("childFareType 			= " + childFareType).append("\n");
		sb.append("\t").append("infantFare 				= " + getInfantFare()).append("\n");
		sb.append("\t").append("infantFareType 			= " + infantFareType).append("\n");

		sb.append("\t").append("fareBasisCode 			= " + fareBasisCode).append("\n");
		sb.append("\t").append("fareCategoryCode 		= " + fareCategoryCode).append("\n");
		sb.append("\t").append("fareRuleComment 		= " + fareRuleComment).append("\n");
		sb.append("\t").append("fareRuleID 				= " + fareRuleID).append("\n");
		sb.append("\t").append("ondCode 				= " + ondCode).append("\n");
		sb.append("\t").append("bookingClassCode 		= " + bookingClassCode).append("\n");

		sb.append("\t").append("isAdultFareRefundable 	= " + isAdultFareRefundable).append("\n");
		sb.append("\t").append("isChildFareRefundable 	= " + isChildFareRefundable).append("\n");
		sb.append("\t").append("isInfantFareRefundable 	= " + isInfantFareRefundable).append("\n");

		sb.append("\t").append("AdultApplicability 		= " + AdultApplicability).append("\n");
		sb.append("\t").append("ChildApplicability 		= " + ChildApplicability).append("\n");
		sb.append("\t").append("InfantApplicability 	= " + InfantApplicability).append("\n");

		sb.append("\t").append("fareRuleCode 		= " + fareRuleCode).append("\n");
		sb.append("\t").append("maximumStayOverInMins 	= " + maximumStayOverInMins).append("\n");
		sb.append("\t").append("maximumStayOverInMonths = " + maximumStayOverInMonths).append("\n");
		sb.append("\t").append("minimumStayOverInMins 	= " + minimumStayOverInMins).append("\n");
		sb.append("\t").append("minimumStayOverInMonths = " + minimumStayOverInMonths).append("\n");

		sb.append("\t").append("openRTConfPeriodInMins	= " + openRTConfPeriodInMins).append("\n");
		sb.append("\t").append("openRTConfPeriodInMonths= " + openRTConfPeriodInMonths).append("\n");

		sb.append("\t").append("visibleAgents 			= " + visibleAgents).append("\n");
		sb.append("\t").append("visibleChannels 		= " + visibleChannels).append("\n");
		sb.append("\t").append("visibleChannelNames 	= " + visibleChannelNames).append("\n");
		sb.append("\t").append("----------------------------------------------").append("\n");
		return sb.toString();
	}

	/**
	 * @return the agentInstructions
	 */
	public String getAgentInstructions() {
		return agentInstructions;
	}

	/**
	 * @param agentInstructions
	 *            the agentInstructions to set
	 */
	public void setAgentInstructions(String agentInstructions) {
		this.agentInstructions = agentInstructions;
	}

	public String getHalfReturnFlag() {
		return halfReturnFlag;
	}

	public void setHalfReturnFlag(String halfReturnFlag) {
		this.halfReturnFlag = halfReturnFlag;
	}

	public boolean isHalfReturnFareApplicable() {
		return "Y".equals(getHalfReturnFlag());
	}

	public boolean isHasFlexi() {
		return hasFlexi;
	}

	public void setHasFlexi(boolean hasFlexi) {
		this.hasFlexi = hasFlexi;
	}

	public int getNoOfAvailableSeats() {
		return noOfAvailableSeats;
	}

	public void setNoOfAvailableSeats(int noOfAvailableSeats) {
		this.noOfAvailableSeats = noOfAvailableSeats;
	}

	public String getAgentCommissionType() {
		return agentCommissionType;
	}

	public void setAgentCommissionType(String agentCommissionType) {
		this.agentCommissionType = agentCommissionType;
	}

	public BigDecimal getAgentCommissionAmount() {
		return agentCommissionAmount;
	}

	public void setAgentCommissionAmount(BigDecimal agentCommissionAmount) {
		this.agentCommissionAmount = agentCommissionAmount;
	}

	public Integer getFlexiRuleID() {
		return flexiRuleID;
	}

	public void setFlexiRuleID(Integer flexiRuleID) {
		this.flexiRuleID = flexiRuleID;
	}

	public void setPaxTypewiseFareOndChargeInfo(String ratioCategory, PaxTypewiseFareONDChargeInfo paxTypewiseFareOndChargeInfo) {
		this.paxTypewiseFareOndChargeInfo = paxTypewiseFareOndChargeInfo;
		this.ratioCategory = ratioCategory;
	}

	public double getRatioAdjustedTotalPrice(int adults, int children, int infants) {

		double totalCalculatedAdultFare = paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT)
				.getTotalFare(this.getFareAmount(PaxTypeTO.ADULT), ratioCategory) * adults;
		double totalCalculatedChildFare = paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD)
				.getTotalFare(this.getFareAmount(PaxTypeTO.CHILD), ratioCategory) * children;
		double totalCalculatedInfantFare = paxTypewiseFareOndChargeInfo
				.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT).getTotalFare(this.getFareAmount(PaxTypeTO.INFANT),
						ratioCategory)
				* infants;

		return totalCalculatedAdultFare + totalCalculatedChildFare + totalCalculatedInfantFare;
	}

	public double getRatioAdjustedTotalPriceWithoutServiceTaxes(int adults, int children, int infants) {

		double totalCalculatedAdultFare = paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT)
				.getTotalFareWithoutServiceTaxes(this.getFareAmount(PaxTypeTO.ADULT), ratioCategory) * adults;
		double totalCalculatedChildFare = paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD)
				.getTotalFareWithoutServiceTaxes(this.getFareAmount(PaxTypeTO.CHILD), ratioCategory) * children;
		double totalCalculatedInfantFare = paxTypewiseFareOndChargeInfo
				.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT).getTotalFareWithoutServiceTaxes(this.getFareAmount(PaxTypeTO.INFANT),
						ratioCategory)
				* infants;
		return totalCalculatedAdultFare + totalCalculatedChildFare + totalCalculatedInfantFare;
	}

	public void setReturnFare(boolean returnFlag) {
		this.returnFare = returnFlag;
	}

	public boolean isReturnFare() {
		return this.returnFare;
	}
	
	public void setEffectiveROEAdjustmentAmount(String paxType, Double effectiveChargAmount) {
		if (this.effectiveROEAdjustmentAmountsMap == null) {
			this.effectiveROEAdjustmentAmountsMap = new HashMap<String, Double>();
		}
		if (effectiveChargAmount != null) {
			effectiveChargAmount = Double.parseDouble(AccelAeroCalculator.parseBigDecimal(effectiveChargAmount.doubleValue())
					.toString());
		}
		this.effectiveROEAdjustmentAmountsMap.put(paxType, effectiveChargAmount);
	}

	public Double getEffectiveROEAdjustmentAmount(String paxType) {
		if (this.effectiveROEAdjustmentAmountsMap == null || !this.effectiveROEAdjustmentAmountsMap.containsKey(paxType)) {
			return new Double(0);
		}
		return (Double) this.effectiveROEAdjustmentAmountsMap.get(paxType);
	}

	public Map<String, Double> getEffectiveROEAdjustmentAmountMap() {
		return this.effectiveROEAdjustmentAmountsMap;
	}

	private AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}

	public String getRatioCategory() {
		return this.ratioCategory;
	}

	public String getFareRuleCommentSelectedLanguage() {
		return fareRuleCommentSelectedLanguage;
	}

	public void setFareRuleCommentSelectedLanguage(String fareRuleCommentSelectedLanguage) {
		this.fareRuleCommentSelectedLanguage = fareRuleCommentSelectedLanguage;
	}
}
