package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class RollforwardBatchREQSEGDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1662485960657323223L;
	
	private String fromDate;
	private String toDate;
	private String segmentBatchData;
	private String status;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getSegmentBatchData() {
		return segmentBatchData;
	}
	public void setSegmentBatchData(String segmentBatchData) {
		this.segmentBatchData = segmentBatchData;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
