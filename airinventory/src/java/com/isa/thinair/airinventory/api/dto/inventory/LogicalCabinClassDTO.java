package com.isa.thinair.airinventory.api.dto.inventory;

import java.io.Serializable;

public class LogicalCabinClassDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String logicalCCCode;

	private String cabinClassCode;

	private String status = "INA";

	public LogicalCabinClassDTO() {

	}

	public LogicalCabinClassDTO(String cabinClassCode, String logicalCCCode) {
		this.cabinClassCode = cabinClassCode;
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the logicalCabinClass
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCabinClass
	 *            the logicalCabinClass to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
}