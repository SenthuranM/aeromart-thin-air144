package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;

/**
 * Captures Fares and Agents linked to a booking class
 * 
 * @author MN
 */
public class BCONDFaresAgentsSummaryDTO implements Serializable {

	private static final long serialVersionUID = -864752754523115017L;

	private String bookingCode;

	private String standardCode;

	private String fixedCode;

	/*
	 * OND wise effective fares summaries key=ondCode value=LinkedHashMap of FareSummaryDTOs
	 */
	@SuppressWarnings("rawtypes")
	private LinkedHashMap ondEffectiveFaresMap;

	/*
	 * OND wise effective agents filtered through farerules key=ondCode value=Collection of agentIds
	 */
	@SuppressWarnings("rawtypes")
	private LinkedHashMap ondEffectiveAgentsMap;

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getFixedCode() {
		return fixedCode;
	}

	public void setFixedCode(String fixedCode) {
		this.fixedCode = fixedCode;
	}

	@SuppressWarnings("rawtypes")
	public LinkedHashMap getOndEffectiveAgentsMap() {
		return ondEffectiveAgentsMap;
	}

	@SuppressWarnings("rawtypes")
	public void setOndEffectiveAgentsMap(LinkedHashMap ondEffectiveAgentsMap) {
		this.ondEffectiveAgentsMap = ondEffectiveAgentsMap;
	}

	@SuppressWarnings("rawtypes")
	public LinkedHashMap getOndEffectiveFaresMap() {
		return ondEffectiveFaresMap;
	}

	@SuppressWarnings("rawtypes")
	public void setOndEffectiveFaresMap(LinkedHashMap ondEffectiveFaresMap) {
		this.ondEffectiveFaresMap = ondEffectiveFaresMap;
	}

	public String getStandardCode() {
		return standardCode;
	}

	public void setStandardCode(String standardCode) {
		this.standardCode = standardCode;
	}

	/**
	 * @param ondCode
	 * @return Collection of Integer agentIds
	 */
	@SuppressWarnings("rawtypes")
	public Collection getLinkedAgentIds(String ondCode) {
		if (getOndEffectiveAgentsMap() != null) {
			return (Collection) getOndEffectiveAgentsMap().get(ondCode);
		}
		return null;
	}

	/**
	 * @param ondCode
	 * @return Colleciton of com.isa.thinair.airpricing.api.dto.FareSummaryDTOs
	 */
	@SuppressWarnings("rawtypes")
	public Collection getLinkedFares(String ondCode) {
		if (getOndEffectiveFaresMap() != null) {
			LinkedHashMap effectiveFares = (LinkedHashMap) getOndEffectiveFaresMap().get(ondCode);
			if (effectiveFares != null) {
				return AirinventoryUtils.getSearializableValues(effectiveFares);
			}
		}
		return null;
	}
}
