/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

/**
 * @author Lasantha Pambagoda
 */
public class TransferSeatBcCount implements Serializable {

	private static final long serialVersionUID = 3323166060647387140L;

	private int soldCount;

	private int onHoldCount;

	/**
	 * @return Returns the onHoldCount.
	 */
	public int getOnHoldCount() {
		return onHoldCount;
	}

	/**
	 * @param onHoldCount
	 *            The onHoldCount to set.
	 */
	public void setOnHoldCount(int onHoldCount) {
		this.onHoldCount = onHoldCount;
	}

	/**
	 * @return Returns the soldCount.
	 */
	public int getSoldCount() {
		return soldCount;
	}

	/**
	 * @param soldCount
	 *            The soldCount to set.
	 */
	public void setSoldCount(int soldCount) {
		this.soldCount = soldCount;
	}
}
