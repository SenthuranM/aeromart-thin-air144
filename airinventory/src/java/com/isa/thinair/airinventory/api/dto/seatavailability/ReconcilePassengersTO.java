package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Date;

public class ReconcilePassengersTO implements Serializable {

	private static final long serialVersionUID = -8546523133455238873L;

	private int flightId;

	private int flightSegId;

	private String flightNumber;

	private String segmentCode;

	private int pnrSegmentId;

	private Date departureDateTimeLocal;

	private Date departureDateTimeZulu;
	
	private Date arrivalDateTimeLocal;

	private Date arrivalDateTimeZulu;

	private String cabinClassCode;

	private String bookingClassCode;

	private int noOfAdults;

	private int noOfChildren;

	private String paxCategoryCode;

	private String posStation = null;

	private String agentCode;

	private int fltSegId;

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the bookingClassCode
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            the bookingClassCode to set
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public int getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	/**
	 * @return the departureDateTimeZulu
	 */
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	/**
	 * @param departureDateTimeZulu
	 *            the departureDateTimeZulu to set
	 */
	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public String getPosStation() {
		return posStation;
	}

	public void setPosStation(String posStation) {
		this.posStation = posStation;
	}

	public String getDepartureAirport() {
		String departureAirport = null;
		if (getSegmentCode() != null) {
			departureAirport = getSegmentCode().substring(0, 3);
		}
		return departureAirport;
	}

	/**
	 * @return Returns the paxCategoryCode.
	 */
	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	/**
	 * @param paxCategoryCode
	 *            The paxCategoryCode to set.
	 */
	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	/**
	 * @return Returns the noOfChildren.
	 */
	public int getNoOfChildren() {
		return noOfChildren;
	}

	/**
	 * @param noOfChildren
	 *            The noOfChildren to set.
	 */
	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public int getPnrSegmentId() {
		return pnrSegmentId;
	}

	public void setPnrSegmentId(int pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the fltSegId
	 */
	public int getFltSegId() {
		return fltSegId;
	}

	/**
	 * @param fltSegId
	 *            the fltSegId to set
	 */
	public void setFltSegId(int fltSegId) {
		this.fltSegId = fltSegId;
	}

	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}
}
