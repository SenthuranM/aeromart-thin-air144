/*/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airinventory.api.service;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaSearchTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;



/**
 * @author Raghu
 */
public interface RollForwardBatchREQSEGBD {

	public static final String SERVICE_NAME = "RollForwardBatchREQSEGService";
	
	public List<RollForwardBatchREQSEG> getRollForwardBatchREQSEGs()throws ModuleException;

	public RollForwardBatchREQSEG getRollForwardBatchREQSEG(int id)throws ModuleException;

	public void saveRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG)throws ModuleException;

	public void removeRollForwardBatchREQSEG(int batchId)throws ModuleException;

	public void removeRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG)throws ModuleException;

	public void saveAllRollForwardBatchREQSEG(Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet) throws ModuleException;

	public Page<SearchRollForwardCriteriaTO> search(SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO, int start, int i);
	
	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByBatchId(Integer batchId)throws ModuleException;

	public void updateRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException;

	public void updateRollForwardBatchREQSEGStatus(Integer batchSegId, String flightNumber, Date fromDate, Date toDate, String status, String auditDetails) throws ModuleException;

	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByDateAndFlights(String strFromDate, String strToDate, String flightNumber);

	public void rollForwardBatchSegment(InvRollForwardCriteriaDTO invRollForwardCriteriaDTO, RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException, ParseException;
	
}
