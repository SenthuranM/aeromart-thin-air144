package com.isa.thinair.airinventory.api.dto.inventory;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class UpdatedInvTempDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer invTmpCabinAllocID;

	private Set<InvTempCCBCAllocDTO> deletedInvTempBCAllocs;

	private Set<InvTempCCBCAllocDTO> addedInvTempBCAllocs;

	private Set<InvTempCCBCAllocDTO> editedInvTempBCAllocs;

	public UpdatedInvTempDTO() {
		this.addedInvTempBCAllocs = new HashSet<InvTempCCBCAllocDTO>();
		this.editedInvTempBCAllocs = new HashSet<InvTempCCBCAllocDTO>();
		this.deletedInvTempBCAllocs = new HashSet<InvTempCCBCAllocDTO>();
	}

	public Set<InvTempCCBCAllocDTO> getDeletedInvTempBCAllocs() {
		return deletedInvTempBCAllocs;
	}

	public void setDeletedInvTempBCAllocs(Set<InvTempCCBCAllocDTO> deletedInvTempBCAllocs) {
		this.deletedInvTempBCAllocs = deletedInvTempBCAllocs;
	}

	public Set<InvTempCCBCAllocDTO> getAddedInvTempBCAllocs() {
		return addedInvTempBCAllocs;
	}

	public void setAddedInvTempBCAllocs(Set<InvTempCCBCAllocDTO> addedInvTempBCAllocs) {
		this.addedInvTempBCAllocs = addedInvTempBCAllocs;
	}

	public Set<InvTempCCBCAllocDTO> getEditedInvTempBCAllocs() {
		return editedInvTempBCAllocs;
	}

	public void setEditedInvTempBCAllocs(Set<InvTempCCBCAllocDTO> editedInvTempBCAllocs) {
		this.editedInvTempBCAllocs = editedInvTempBCAllocs;
	}

	public Integer getInvTmpCabinAllocID() {
		return invTmpCabinAllocID;
	}

	public void setInvTmpCabinAllocID(Integer invTmpCabinAllocID) {
		this.invTmpCabinAllocID = invTmpCabinAllocID;
	}
}
