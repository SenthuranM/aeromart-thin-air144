package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airschedules.api.utils.FlightEventCode;

public class NotifyFlightEventRQ implements Serializable {

	private static final long serialVersionUID = -1466987061042609852L;

	private FlightEventCode flightEventCode;

	private Collection<Integer> flightIds;

	private Collection<Integer> gdsIdsAdded;

	private Collection<Integer> gdsIdsRemoved;

	/**
	 * @return the flightEventCode
	 */
	public FlightEventCode getFlightEventCode() {
		return flightEventCode;
	}

	/**
	 * @param flightEventCode
	 *            the flightEventCode to set
	 */
	public void setFlightEventCode(FlightEventCode flightEventCode) {
		this.flightEventCode = flightEventCode;
	}

	/**
	 * @return the flightIds
	 */
	public Collection<Integer> getFlightIds() {
		return flightIds;
	}

	/**
	 * @param flightIds
	 *            the flightIds to set
	 */
	public void setFlightIds(Collection<Integer> flightIds) {
		this.flightIds = flightIds;
	}

	/**
	 * @param flightIds
	 *            the flightIds to set
	 */
	public void addFlightIds(Collection<Integer> flightIds) {
		if (this.flightIds == null)
			this.flightIds = new ArrayList<Integer>();
		this.flightIds.addAll(flightIds);
	}

	public void addFlightId(Integer flightId) {
		if (this.flightIds == null)
			this.flightIds = new ArrayList<Integer>();
		this.flightIds.add(flightId);
	}

	/**
	 * @return the gdsIdsAdded
	 */
	public Collection<Integer> getGdsIdsAdded() {
		return gdsIdsAdded;
	}

	/**
	 * @param gdsIdsAdded
	 *            the gdsIdsAdded to set
	 */
	public void setGdsIdsAdded(Collection<Integer> gdsIdsAdded) {
		this.gdsIdsAdded = gdsIdsAdded;
	}

	/**
	 * @return the gdsIdsRemoved
	 */
	public Collection<Integer> getGdsIdsRemoved() {
		return gdsIdsRemoved;
	}

	/**
	 * @param gdsIdsRemoved
	 *            the gdsIdsRemoved to set
	 */
	public void setGdsIdsRemoved(Collection<Integer> gdsIdsRemoved) {
		this.gdsIdsRemoved = gdsIdsRemoved;
	}

}
