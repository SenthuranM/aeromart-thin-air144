package com.isa.thinair.airinventory.api.dto.seatmap;

import java.io.Serializable;
import java.math.BigDecimal;

public class FlightSeatStatusTO implements Serializable {

	public static final FlightSeatStatusTO FIRST = new FlightSeatStatusTO();
	public static final FlightSeatStatusTO LAST = new FlightSeatStatusTO();

	private Integer flightAmSeatId;
	private String status;
	private String seatCode;
	private String seatType;
	private String logicalCabinClassCode;
	private String cabinClassCode;
	private Integer colId;
	private Integer rowGroupId;
	private FlightSeatStatusTO nextSeat;
	private FlightSeatStatusTO prevSeat;
	private BigDecimal seatCharge;

	public Integer getRowGroupId() {
		return rowGroupId;
	}

	public void setRowGroupId(Integer rowGroupId) {
		this.rowGroupId = rowGroupId;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public Integer getColId() {
		return colId;
	}

	public void setColId(Integer colId) {
		this.colId = colId;
	}

	public Integer getFlightAmSeatId() {
		return flightAmSeatId;
	}

	public void setFlightAmSeatId(Integer flightAmSeatId) {
		this.flightAmSeatId = flightAmSeatId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSeatCode() {
		return seatCode;
	}

	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	public FlightSeatStatusTO getNextSeat() {
		return nextSeat;
	}

	public void setNextSeat(FlightSeatStatusTO nextSeat) {
		this.nextSeat = nextSeat;
	}

	public FlightSeatStatusTO getPrevSeat() {
		return prevSeat;
	}

	public void setPrevSeat(FlightSeatStatusTO prevSeat) {
		this.prevSeat = prevSeat;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
	public void setSeatCharge(BigDecimal seatCharge) {
		this.seatCharge = seatCharge;
	}

	public BigDecimal getSeatCharge() {
		return seatCharge;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

}
