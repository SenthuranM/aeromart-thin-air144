package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlightSegmentTo implements Serializable {

	private static final long serialVersionUID = -8614359155286129866L;
	private String segmentCode;
	private List<PassengerTo> passengers;

	public FlightSegmentTo() {
		this.passengers = new ArrayList<PassengerTo>();
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public List<PassengerTo> getPassengers() {
		return passengers;
	}

	public void addPassenger(PassengerTo passenger) {
		this.passengers.add(passenger);
	}
}
