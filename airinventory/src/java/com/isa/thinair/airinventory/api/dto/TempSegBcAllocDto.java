package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Date;

public class TempSegBcAllocDto implements Serializable {
	private int id;
	private int fltSegId;
	private String segmentCode;
	private String flightNumber;
	private Date departureTime;
	private Date arrivalTime;
	private String bookingClass;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFltSegId() {
		return fltSegId;
	}

	public void setFltSegId(int fltSegId) {
		this.fltSegId = fltSegId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
}
