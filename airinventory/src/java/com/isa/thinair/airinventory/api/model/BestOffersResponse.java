package com.isa.thinair.airinventory.api.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @hibernate.class table = "T_BEST_OFFERS_RESPONSE"
 * 
 */

public class BestOffersResponse {

	private int bestOffersResponseId;

	private int bestOffersRequestId;

	private String origin;

	private String destination;

	private String originAirportName;

	private String destinationAirportName;

	private String flightNos;

	private Date departureDateTimeLocal;

	private BigDecimal fareAmount;

	private BigDecimal totalTax;

	private BigDecimal totalSurcharges;

	private BigDecimal totalAmount;

	private int rank;

	/**
	 * @return the bestOffersResponseId
	 * @hibernate.id column = "BEST_OFFERS_RESPONSE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BEST_OFFERS_RESPONSE"
	 * 
	 */
	public int getBestOffersResponseId() {
		return this.bestOffersResponseId;
	}

	public void setBestOffersResponseId(int bestOffersResponseId) {
		this.bestOffersResponseId = bestOffersResponseId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return this.origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ORIGIN_AIRPORT_NAME"
	 */
	public String getOriginAirportName() {
		return this.originAirportName;
	}

	public void setOriginAirportName(String originAirportName) {
		this.originAirportName = originAirportName;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "DESTINATION_AIRPORT_NAME"
	 */
	public String getDestinationAirportName() {
		return this.destinationAirportName;
	}

	public void setDestinationAirportName(String destinationAirportName) {
		this.destinationAirportName = destinationAirportName;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "FLIGHT_NOS"
	 */
	public String getFlightNos() {
		return this.flightNos;
	}

	public void setFlightNos(String flightNos) {
		this.flightNos = flightNos;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "EST_TIME_DEPARTURE_LOCAL"
	 */
	public Date getDepartureDateTimeLocal() {
		return this.departureDateTimeLocal;
	}

	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "FARE_AMOUNT"
	 */
	public BigDecimal getFareAmount() {
		return this.fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "RANKING"
	 */
	public int getRank() {
		return this.rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "BEST_OFFERS_REQUEST_ID"
	 */
	public int getBestOffersRequestId() {
		return this.bestOffersRequestId;
	}

	public void setBestOffersRequestId(int bestOffersRequestId) {
		this.bestOffersRequestId = bestOffersRequestId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TOTAL_TAX"
	 */
	public BigDecimal getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TOTAL_SURCHARGES"
	 */
	public BigDecimal getTotalSurcharges() {
		return totalSurcharges;
	}

	public void setTotalSurcharges(BigDecimal totalSurcharges) {
		this.totalSurcharges = totalSurcharges;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "TOTAL_AMOUNT"
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

}
