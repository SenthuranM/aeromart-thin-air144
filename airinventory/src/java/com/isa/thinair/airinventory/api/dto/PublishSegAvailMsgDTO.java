package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventCode;

public class PublishSegAvailMsgDTO implements Serializable {

	private static final long serialVersionUID = 8474532429431766692L;

	private Integer publishAvailabilityId;

	private String flightNumber;

	private String originAirport;

	private String destinationAirport;

	private Date departureDateLocal;

	private PublishInventoryEventCode segmentEventCode;

	private Collection<PublishBCAvailMsgDTO> bcAvailabilityDTOs;
	
	private Integer seatsAvailable;

	/**
	 * @return the publishAvailabilityId
	 */
	public Integer getPublishAvailabilityId() {
		return publishAvailabilityId;
	}

	/**
	 * @param publishAvailabilityId
	 *            the publishAvailabilityId to set
	 */
	public void setPublishAvailabilityId(Integer publishAvailabilityId) {
		this.publishAvailabilityId = publishAvailabilityId;
	}

	/**
	 * @return the bcAvailabilityDTOs
	 */
	public Collection<PublishBCAvailMsgDTO> getBcAvailabilityDTOs() {
		return bcAvailabilityDTOs;
	}

	/**
	 * @param bcAvailabilityDTOs
	 *            the bcAvailabilityDTOs to set
	 */
	public void setBcAvailabilityDTOs(Collection<PublishBCAvailMsgDTO> bcAvailabilityDTOs) {
		this.bcAvailabilityDTOs = bcAvailabilityDTOs;
	}

	/**
	 * @return the departureDateLocal
	 */
	public Date getDepartureDateLocal() {
		return departureDateLocal;
	}

	/**
	 * @param departureDateLocal
	 *            the departureDateLocal to set
	 */
	public void setDepartureDateLocal(Date departureDateLocal) {
		this.departureDateLocal = departureDateLocal;
	}

	/**
	 * @return the destinationAirport
	 */
	public String getDestinationAirport() {
		return destinationAirport;
	}

	/**
	 * @param destinationAirport
	 *            the destinationAirport to set
	 */
	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the originAirport
	 */
	public String getOriginAirport() {
		return originAirport;
	}

	/**
	 * @param originAirport
	 *            the originAirport to set
	 */
	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	/**
	 * @return the segmentEventCode
	 */
	public PublishInventoryEventCode getSegmentEventCode() {
		return segmentEventCode;
	}

	/**
	 * @param segmentEventCode
	 *            the segmentEventCode to set
	 */
	public void setSegmentEventCode(PublishInventoryEventCode segmentEventCode) {
		this.segmentEventCode = segmentEventCode;
	}
	
	/**
	 * @return the seatsAvailable
	 */
	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	/**
	 * @param seatsAvailable the seatsAvailable to set
	 */
	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

}
