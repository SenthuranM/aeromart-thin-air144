/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * To hold segment bookings data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class SegmentSeatDistsDTO implements Serializable {

	private static final long serialVersionUID = 7218518103104243866L;

	/* Holds flight segment id */
	private int flightSegId;

	/*
	 * Holds a collection of SeatDistribution A reservation happens from mulitple booking codes only if nesting
	 * applied<br> This seat distribution represents the all the seats break down related to the reservation in concern
	 */
	private Collection<SeatDistribution> seatDistributions;

	/* Holds a flight id (used by inventory) */
	private int flightId;

	/* Holds a flight cabin class (used by inventory) */
	private String cabinClassCode;

	/* Holds a flight logical cabin class (used by inventory) */
	private String logicalCabinClass;

	/* Holds a flight segment code (used by inventory) */
	private String segmentCode;

	private int noOfInfantSeats;

	private boolean isFixedQuotaSeats = false;

	/** Temp variable to hold the OndFareDto.isInoundOnd */
	private boolean isBelongToInboundOnd;

	/**
	 * Reservation segment id corresponds to the flight segment id Used By Add Infant function in reservation
	 */
	private int pnrSegId;

	/**
	 * @return Returns the flightSegId.
	 */
	public int getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            The flightSegId to set.
	 */
	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @return Returns the cabinClassCode.
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	/**
	 * @return Returns the flightId.
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the seatDistribution.
	 */
	public Collection<SeatDistribution> getSeatDistribution() {
		return seatDistributions;
	}

	public Collection<SeatDistribution> getEffectiveSeatDistribution() {
		Collection<SeatDistribution> effectiveSeatDist = null;
		if (seatDistributions != null && seatDistributions.size() > 1) {
			effectiveSeatDist = new ArrayList<SeatDistribution>();
			Iterator<SeatDistribution> seatDistributionsIt = seatDistributions.iterator();
			int seatsCount = 0;
			boolean isOverbook = false;
			boolean isBookedFlightSearch = false;
			boolean isSameBookingClassAsExit = false;
			boolean isFlownOnd = false;
			for (int i = 0; seatDistributionsIt.hasNext(); i++) {
				SeatDistribution seatDist = seatDistributionsIt.next();
				seatsCount += seatDist.getNoOfSeats();
				// make overbook at least one is overbooked
				isOverbook = (isOverbook || seatDist.isOverbook());
				isBookedFlightSearch = (isBookedFlightSearch || seatDist.isBookedFlightCabinBeingSearched());
				isSameBookingClassAsExit = seatDist.isSameBookingClassAsExisting();
				isFlownOnd = (isFlownOnd || seatDist.isFlownOnd());
				if (i == (seatDistributions.size() - 1)) {
					SeatDistribution effSeatDist = new SeatDistribution();
					effSeatDist.setBookingClassCode(seatDist.getBookingClassCode());
					effSeatDist.setBookingClassType(seatDist.getBookingClassType());
					effSeatDist.setOnholdRestricted(seatDist.isOnholdRestricted());
					effSeatDist.setNestedSeats(seatDist.isNestedSeats());
					effSeatDist.setNoOfSeats(seatsCount);
					effSeatDist.setOverbook(isOverbook);
					effSeatDist.setBookedFlightCabinBeingSearched(isBookedFlightSearch);
					effSeatDist.setSameBookingClassAsExisting(isSameBookingClassAsExit);
					effSeatDist.setFlownOnd(isFlownOnd);

					effectiveSeatDist.add(effSeatDist);
				}
			}
		} else {
			effectiveSeatDist = seatDistributions;
		}
		return effectiveSeatDist;
	}

	public void setSeatDistributions(Collection<SeatDistribution> seatDistributions) {
		this.seatDistributions = seatDistributions;
	}

	public void addSeatDistribution(SeatDistribution seatDistribution) {
		if (this.seatDistributions == null) {
			this.seatDistributions = new ArrayList<SeatDistribution>();
		}
		this.seatDistributions.add(seatDistribution);
	}

	public int getNoOfInfantSeats() {
		return noOfInfantSeats;
	}

	public void setNoOfInfantSeats(int noOfInfantSeats) {
		this.noOfInfantSeats = noOfInfantSeats;
	}

	public boolean isFixedQuotaSeats() {
		return isFixedQuotaSeats;
	}

	public void setFixedQuotaSeats(boolean isFixedQuotaSeats) {
		this.isFixedQuotaSeats = isFixedQuotaSeats;
	}

	// -------------------------------------------------------
	// Utility methods
	// -------------------------------------------------------

	public int getTotalAdultSeats() {
		int totalAdultSeats = 0;
		if (getSeatDistribution() != null) {
			Iterator<SeatDistribution> seatsDistIt = getSeatDistribution().iterator();
			while (seatsDistIt.hasNext()) {
				SeatDistribution seatDist = seatsDistIt.next();
				totalAdultSeats += seatDist.getNoOfSeats();
			}
		}
		return totalAdultSeats;
	}

	public String getBookingCode() {
		if (getSeatDistribution() != null && !getSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getSeatDistribution())).getBookingClassCode();
		}
		return null;
	}

	public String getBookingClassType() {
		if (getSeatDistribution() != null && !getSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getSeatDistribution())).getBookingClassType();
		}
		return null;

	}

	public boolean isOverbook() {
		if (getEffectiveSeatDistribution() != null && !getEffectiveSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getEffectiveSeatDistribution())).isOverbook();
		}
		return false;

	}	
	
	public boolean isWaitListed() {
		if (getEffectiveSeatDistribution() != null && !getEffectiveSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getEffectiveSeatDistribution())).isWaitListed();
		}
		return false;

	}	

	public boolean isBookedFlightCabinBeingSearched() {
		if (getEffectiveSeatDistribution() != null && !getEffectiveSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getEffectiveSeatDistribution()))
					.isBookedFlightCabinBeingSearched();
		}
		return false;

	}

	public boolean isSameBookingClassAsExisting() {
		if (getEffectiveSeatDistribution() != null && !getEffectiveSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getEffectiveSeatDistribution())).isSameBookingClassAsExisting();
		}
		return false;

	}

	@Override
	public SegmentSeatDistsDTO clone() {
		SegmentSeatDistsDTO clone = new SegmentSeatDistsDTO();
		clone.setCabinClassCode(getCabinClassCode());
		clone.setLogicalCabinClass(getLogicalCabinClass());
		clone.setFixedQuotaSeats(isFixedQuotaSeats);
		clone.setFlightId(getFlightId());
		clone.setFlightSegId(getFlightSegId());
		clone.setNoOfInfantSeats(getNoOfInfantSeats());

		if (getSeatDistribution() != null) {
			Collection<SeatDistribution> seatDists = new ArrayList<SeatDistribution>();
			Iterator<SeatDistribution> seatDistIt = getSeatDistribution().iterator();
			while (seatDistIt.hasNext()) {
				seatDists.add(seatDistIt.next().clone());
			}
			clone.setSeatDistributions(seatDists);
		}
		clone.setSegmentCode(getSegmentCode());

		return clone;
	}

	// private boolean isNestingFlagSet = false;
	// private boolean isSoldOnHoldSeatsDistSet = false;

	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		summary.append("\n\r.......................Segment Seats Dist.......................\n\r");
		summary.append("FlightId            = " + getFlightId() + "\n\r");
		summary.append("SegmentId           = " + getFlightSegId() + "\n\r");
		summary.append("Seats Distribution  = ");
		Iterator<SeatDistribution> seatDistsIt = getSeatDistribution().iterator();
		while (seatDistsIt.hasNext()) {
			SeatDistribution seatDists = seatDistsIt.next();
			summary.append("[" + seatDists.getBookingClassCode() + "," + seatDists.getNoOfSeats() + "]");
		}
		summary.append("\n\r");
		summary.append("................................................................\n\r");
		return summary;
	}

	/**
	 * @return Returns the pnrSegId.
	 */
	public int getPnrSegId() {
		return pnrSegId;
	}

	/**
	 * @param pnrSegId
	 *            The pnrSegId to set.
	 */
	public void setPnrSegId(int pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public boolean isBelongToInboundOnd() {
		return isBelongToInboundOnd;
	}

	public void setBelongToInboundOnd(boolean isBelongToInboundOnd) {
		this.isBelongToInboundOnd = isBelongToInboundOnd;
	}

	public boolean isFlownOnd() {
		if (getEffectiveSeatDistribution() != null && !getEffectiveSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getEffectiveSeatDistribution())).isFlownOnd();
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("\t").append("-------------------SegmentSeatDistsDTO-------------------").append("\n");
		sb.append("\t").append("flightId 				= " + flightId).append("\n");
		sb.append("\t").append("flightSegId 			= " + flightSegId).append("\n");
		sb.append("\t").append("bookingCode 			= " + getBookingCode()).append("\n");
		sb.append("\t").append("cabinClassCode 			= " + cabinClassCode).append("\n");
		sb.append("\t").append("logicalCabinClass	 	= " + logicalCabinClass).append("\n");
		sb.append("\t").append("isBelongToInboundOnd	= " + isBelongToInboundOnd).append("\n");
		sb.append("\t").append("isFixedQuotaSeats 		= " + isFixedQuotaSeats).append("\n");
		sb.append("\t").append("noOfInfantSeats 		= " + noOfInfantSeats).append("\n");
		sb.append("\t").append("bookingClassType 		= " + getBookingClassType()).append("\n");
		sb.append("\t").append("segmentCode 			= " + segmentCode).append("\n");
		sb.append("\t").append("pnrSegId 				= " + pnrSegId).append("\n").append("\n");
		sb.append("\t").append("----------------seatDistributions---------------").append("\n");
		if (seatDistributions != null && !seatDistributions.isEmpty()) {
			for (SeatDistribution dist : seatDistributions) {
				sb.append("\t").append("\t").append(dist).append("\n");
			}
		}
		return sb.toString();
	}
	
	public boolean isUnTouchedOnd() {
		if (getEffectiveSeatDistribution() != null && !getEffectiveSeatDistribution().isEmpty()) {
			return ((SeatDistribution) BeanUtils.getLastElement(getEffectiveSeatDistribution())).isUnTouchedOnd();
		}
		return false;
	}

}
