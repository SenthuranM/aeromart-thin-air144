package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * @author Nasly
 */
public interface IAvailableFlightSegment extends Serializable {

	public boolean isDirectFlight();

	public FareSummaryDTO getFare(String logicalCCtype);

	public void setFare(String logicalCCtype, FareSummaryDTO fare);

	public void removeFare(String logicalCCtype);

	public boolean isSeatsAvailable(String logicalCCtype);

	public void setSeatsAvailable(String logicalCCtype, boolean b);

	public Collection<FlightSegmentDTO> getFlightSegmentDTOs();

	public AvailableIBOBFlightSegment getCleanedInstance();

	public void addCharge();

	public Map<String, Double> getTotalFareMap(String logicalCCtype);

	public Date getDepartureDate();

	public Date getDepartureDateZulu();

	public Date getArrivalDate();

	public Collection<String> getAvailableLogicalCabinClasses();

	public Set<String> getAvailableExternalLogicalCabinClasses();

	public Map<String, Double> getTotalSurcharge(String logicalCCtype);

	// public Map<String, Double> getTotalSurcharge();

	// public Map<String, Double> getTotalFixedSurcharge();

	public FlightSegmentDTO getFirstFlightSegment();

	public FlightSegmentDTO getLastFlightSegment();

	public String getOriginAirport();

	public String getDestinationAirport();
}
