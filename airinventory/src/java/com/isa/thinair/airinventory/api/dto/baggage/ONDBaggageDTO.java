package com.isa.thinair.airinventory.api.dto.baggage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ONDBaggageDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ondCode;
	private String lastSegCode;
	private Date fromDate;
	private Date toDate;
	private String ondGroupId;
	private boolean isSegmentSearch;
	private List<String> segCodes;
	private List<String> bookingClasses;
	private List<String> flightNumbers;
	private String agent;
	private String salesChannel;
	private String classOfService;
	
	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getLastSegCode() {
		return lastSegCode;
	}

	public void setLastSegCode(String lastSegCode) {
		this.lastSegCode = lastSegCode;
	}

	public List<String> getSegCodes() {
		if (segCodes == null) {
			segCodes = new ArrayList<String>();
		}
		return segCodes;
	}

	public boolean isSegmentSearch() {
		return isSegmentSearch;
	}

	public void setSegmentSearch(boolean isSegmentSearch) {
		this.isSegmentSearch = isSegmentSearch;
	}

	/**
	 * @return the ondGroupId
	 */
	public String getOndGroupId() {
		return ondGroupId;
	}

	/**
	 * @param ondGroupId
	 *            the ondGroupId to set
	 */
	public void setOndGroupId(String ondGroupId) {
		this.ondGroupId = ondGroupId;
	}


	public List<String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(List<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public List<String> getFlightNumbers() {
		return flightNumbers;
	}

	public void setFlightNumbers(List<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String[] getStopOverAirports() {
		return ondCode.split("/");
	}

	public String getOrigin() {
		String[] stopovers = getStopOverAirports();

		if (stopovers.length != 0) {
			return  stopovers[0];
		}

		return null;
	}

	public String getDestination() {
		String[] stopovers = getStopOverAirports();

		if (stopovers.length != 0) {
			return stopovers[stopovers.length - 1];
		}

		return null;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}
}
