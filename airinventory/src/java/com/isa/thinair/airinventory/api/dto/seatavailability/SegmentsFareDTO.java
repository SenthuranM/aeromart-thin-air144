package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

public class SegmentsFareDTO implements Serializable {

	private static final long serialVersionUID = -15379849670850518L;

	private FareSummaryDTO fareSummaryDTO;

	private int fareType;

	private String ondCode;

	private final LinkedHashMap<Integer, String> segmentRPHMap = new LinkedHashMap<Integer, String>();

	private String cabinClassCode;

	private String logicalCabinClassCode;

	Collection<SeatDistribution> seatDistributions = new ArrayList<SeatDistribution>();

	private boolean isFixedQuotaSeats = false;

	private boolean inbound = false;

	private int ondSequence;

	private int requestedOndSequence;

	/**
	 * @return Returns the fareSummaryDTO.
	 */
	public FareSummaryDTO getFareSummaryDTO() {
		return fareSummaryDTO;
	}

	/**
	 * @param fareSummaryDTO
	 *            The fareSummaryDTO to set.
	 */
	public void setFareSummaryDTO(FareSummaryDTO fareSummaryDTO) {
		this.fareSummaryDTO = fareSummaryDTO;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	/**
	 * Return a map of <FlightSegmentId, FlightRefNo> For FlightRPH decomposition refer FlightRefNumberUtil
	 * 
	 * @return
	 */
	public LinkedHashMap<Integer, String> getSegmentRPHMap() {
		return this.segmentRPHMap;
	}

	public void addSegment(Integer flightSegId, String flightRPHNumber) {
		this.segmentRPHMap.put(flightSegId, flightRPHNumber);
	}

	public Collection<SeatDistribution> getSeatDistributions() {
		return seatDistributions;
	}

	public void addSeatDistributions(SeatDistribution seatDistribution) {
		this.seatDistributions.add(seatDistribution);
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the isFixedQuotaSeats.
	 */
	public boolean isFixedQuotaSeats() {
		return isFixedQuotaSeats;
	}

	/**
	 * @param isFixedQuotaSeats
	 *            The isFixedQuotaSeats to set.
	 */
	public void setFixedQuotaSeats(boolean isFixedQuotaSeats) {
		this.isFixedQuotaSeats = isFixedQuotaSeats;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public boolean isInbound() {
		return inbound;
	}

	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

}
