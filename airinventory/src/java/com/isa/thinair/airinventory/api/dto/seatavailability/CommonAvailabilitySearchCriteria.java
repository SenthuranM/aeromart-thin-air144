package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;

class CommonAvailabilitySearchCriteria implements Serializable {

	private static final long serialVersionUID = 3404192255972939924L;

	private int adultCount;

	private int childCount;

	private int infantCount;

	// Cabin class wise selected flight segment IDs. Flight segment IDs list specify only in fare quote.
	private Map<String, List<Integer>> cabinClassSelection;
	// Logical cabin class wise selected flight segment IDs. Flight segment IDs list specify only in fare quote.
	private Map<String, List<Integer>> logicalCabinClassSelection;

	private int channelCode = -1;

	private String agentCode;

	private String selectedAgentCode = null;

	// For selected flights and for change fare only
	private boolean returnFlag = false;

	private String bookingType = BookingClass.BookingClassType.NORMAL;

	private String bookingClassCode;

	/** Used for fare quote during reservation modification */
	// departure date of the first segment
	private Date firstDepartureDateTimeZulu = null;
	// arrival date of the last segment
	private Date lastArrivalDateTimeZulu = null;

	/**
	 * Referred for fare quote during reservation modification.
	 * Collection<com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO>
	 */
	private Collection<FlightSegmentDTO> existingFlightSegments = null;

	/**
	 * Referred for fare quote during reservation modification.
	 * Collection<com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO>
	 */
	private Collection<FlightSegmentDTO> modifiedFlightSegments = null;

	/**
	 * Referred for fare quote during reservation modification. Specifies the fare ID of the modified OnD.
	 */
	private Integer modifiedOndFareId = null;

	/**
	 * Referred for fare quote during reservation modification. Specifies the highest per pax fare amount of the
	 * modified OnD.
	 */
	private BigDecimal modifiedOndPaxFareAmount = null;

	/**
	 * Referred for fare quote during reservation modification. Specifies whether modified OnD has single segment fare,
	 * through fare or return fare.
	 */
	private Integer modifiedOndFareType = null;

	/**
	 * Referred for fare quote during reservation modification. Specified whether or not to enforce same or higher fare
	 * validation for fare quote for modify OnD
	 */
	private Boolean enforceFareCheckForModOnd = false;

	/** Denotes passenger type */
	private String bookingPaxType = AirPricingCustomConstants.BookingPaxType.ANY;

	/** Denotes fare category type */
	private String fareCategoryType = AirPricingCustomConstants.FareCategoryType.ANY;

	/**
	 * Referred for fare quote during reservation modification. Specifies the Return Ond Group ID of the modified OnD.
	 */
	private Integer modifiedOndReturnGroupId = null;

	private String pointOfSale;

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	/**
	 * @return Returns the childCount.
	 */
	public int getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount
	 *            The childCount to set.
	 */
	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the cabinClassSelection
	 */
	@Deprecated
	public Map<String, List<Integer>> getCabinClassSelection() {
		if (this.cabinClassSelection == null) {
			this.cabinClassSelection = new HashMap<String, List<Integer>>();
		}
		return cabinClassSelection;
	}

	/**
	 * @param cabinClassSelection
	 *            the cabinClassSelection to set
	 */
	@Deprecated
	public void setCabinClassSelection(Map<String, List<Integer>> cabinClassSelection) {
		this.cabinClassSelection = cabinClassSelection;
	}

	/**
	 * @return the logicalCabinClassSelection
	 */
	@Deprecated
	public Map<String, List<Integer>> getLogicalCabinClassSelection() {
		if (this.logicalCabinClassSelection == null) {
			this.logicalCabinClassSelection = new HashMap<String, List<Integer>>();
		}
		return logicalCabinClassSelection;
	}

	/**
	 * @param logicalCabinClassSelection
	 *            the logicalCabinClassSelection to set
	 */
	@Deprecated
	public void setLogicalCabinClassSelection(Map<String, List<Integer>> logicalCabinClassSelection) {
		this.logicalCabinClassSelection = logicalCabinClassSelection;
	}

	/**
	 * @return bookingType The booking Type
	 */
	@Deprecated
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            The type of booking
	 */
	@Deprecated
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	/**
	 * @return Returns the firstDepartureDateTime (Zulu).
	 */
	@Deprecated
	public Date getFirstDepartureDateTimeZulu() {
		return firstDepartureDateTimeZulu;
	}

	/**
	 * @param firstDepartureDateTimeZulu
	 *            The firstDepartureDateTime to set (Zulu).
	 */
	@Deprecated
	public void setFirstDepartureDateTimeZulu(Date firstDepartureDateTimeZulu) {
		this.firstDepartureDateTimeZulu = firstDepartureDateTimeZulu;
	}

	/**
	 * @return Returns the lastArrivalDateTime (Zulu).
	 */
	@Deprecated
	public Date getLastArrivalDateTimeZulu() {
		return lastArrivalDateTimeZulu;
	}

	/**
	 * @param lastArrivalDateTimeZulu
	 *            The lastArrivalDateTime to set (Zulu).
	 */
	@Deprecated
	public void setLastArrivalDateTimeZulu(Date lastArrivalDateTimeZulu) {
		this.lastArrivalDateTimeZulu = lastArrivalDateTimeZulu;
	}

	public String getBookingPaxType() {
		return bookingPaxType;
	}

	public void setBookingPaxType(String bookingPaxType) {
		this.bookingPaxType = bookingPaxType;
	}

	/**
	 * Returns fare category type.fare
	 * 
	 * @return fareCategoryType
	 */
	public String getFareCategoryType() {
		return fareCategoryType;
	}

	/**
	 * Set fare category type.<br>
	 * Refer AirPricingCustomConstants.FareCategoryType for applicable types.
	 * 
	 * @param fareCategoryType
	 *            Fare Category Type
	 */
	public void setFareCategoryType(String fareCategoryType) {
		this.fareCategoryType = fareCategoryType;
	}

	@Deprecated
	public Collection<FlightSegmentDTO> getExistingFlightSegments() {
		return existingFlightSegments;
	}

	@Deprecated
	public void setExistingFlightSegments(Collection<FlightSegmentDTO> existingFlightSegments) {
		this.existingFlightSegments = existingFlightSegments;
	}

	@Deprecated
	public Collection<FlightSegmentDTO> getModifiedFlightSegments() {
		return modifiedFlightSegments;
	}

	@Deprecated
	public void setModifiedFlightSegments(Collection<FlightSegmentDTO> modifiedFlightSegments) {
		this.modifiedFlightSegments = modifiedFlightSegments;
	}

	/**
	 * @return the modifiedOndFareId
	 */
	@Deprecated
	public Integer getModifiedOndFareId() {
		return modifiedOndFareId;
	}

	/**
	 * @param modifiedOndFareId
	 *            the modifiedOndFareId to set
	 */
	@Deprecated
	public void setModifiedOndFareId(Integer modifiedOndFareId) {
		this.modifiedOndFareId = modifiedOndFareId;
	}

	/**
	 * @return the modifiedOndFareType
	 */
	public Integer getModifiedOndFareType() {
		return modifiedOndFareType;
	}

	/**
	 * @param modifiedOndFareType
	 *            the modifiedOndFareType to set
	 */
	public void setModifiedOndFareType(Integer modifiedOndFareType) {
		this.modifiedOndFareType = modifiedOndFareType;
	}

	/**
	 * @return the modifiedOndPaxFareAmount
	 */
	public BigDecimal getModifiedOndPaxFareAmount() {
		return modifiedOndPaxFareAmount;
	}

	/**
	 * @param modifiedOndPaxFareAmount
	 *            the modifiedOndPaxFareAmount to set
	 */
	public void setModifiedOndPaxFareAmount(BigDecimal modifiedOndPaxFareAmount) {
		this.modifiedOndPaxFareAmount = modifiedOndPaxFareAmount;
	}

	/**
	 * @return the enforceFareCheckForModOnd
	 */
	public Boolean getEnforceFareCheckForModOnd() {
		return enforceFareCheckForModOnd;
	}

	/**
	 * @param enforceFareCheckForModOnd
	 *            the enforceFareCheckForModOnd to set
	 */
	public void setEnforceFareCheckForModOnd(Boolean enforceFareCheckForModOnd) {
		this.enforceFareCheckForModOnd = enforceFareCheckForModOnd;
	}

	@Deprecated
	public boolean isReturnFlag() {
		return returnFlag;
	}

	@Deprecated
	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	@Deprecated
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	@Deprecated
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return the modifiedOndReturnGroupId
	 */
	@Deprecated
	public Integer getModifiedOndReturnGroupId() {
		return modifiedOndReturnGroupId;
	}

	/**
	 * @param modifiedOndReturnGroupId
	 *            the modifiedOndReturnGroupId to set
	 */
	@Deprecated
	public void setModifiedOndReturnGroupId(Integer modifiedOndReturnGroupId) {
		this.modifiedOndReturnGroupId = modifiedOndReturnGroupId;
	}

	/**
	 * @return the selectedAgentCode
	 */
	public String getSelectedAgentCode() {
		return selectedAgentCode;
	}

	/**
	 * @param selectedAgentCode
	 *            the selectedAgentCode to set
	 */
	public void setSelectedAgentCode(String selectedAgentCode) {
		this.selectedAgentCode = selectedAgentCode;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}
}
