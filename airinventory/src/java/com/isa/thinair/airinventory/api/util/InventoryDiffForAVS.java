package com.isa.thinair.airinventory.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.RetrieveFCCSegBCInvCriteria;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Command for handling discount related operations on Fare
 * 
 * @author Kanarupan
 * 
 * 
 */

public class InventoryDiffForAVS {

	private static Log log = LogFactory.getLog(InventoryDiffForAVS.class);
	private Collection<FCCSegInventoryDTO> initialSnapShotCollection = null;
	private Collection<FCCSegInventoryDTO> finalSnapShotCollection = null;

	private int fccsInvId;

	public InventoryDiffForAVS(int fccsInvId) {
		this.fccsInvId = fccsInvId;

	}

	public boolean takeIntialInvSnapshot() throws ModuleException {

		try {

			boolean isPublishAvailability = AirinventoryUtils.getAirInventoryConfig().isPublishAvailability();

			if (isPublishAvailability) {

				log.debug("Taking Initial SnapShot");

				initialSnapShotCollection = takeInvSnapShot(fccsInvId);

				return true;
			}

		} catch (Exception e) {
			log.error("Error during Inventory Diff intial update", e);

		}

		return false;
	}

	public boolean takeFinalInvSnapshotAndPublishDiff() throws ModuleException {

		try {

			boolean isPublishAvailability = AirinventoryUtils.getAirInventoryConfig().isPublishAvailability();

			if (isPublishAvailability && initialSnapShotCollection != null) {

				log.debug("Taking final snapshot");

				finalSnapShotCollection = takeInvSnapShot(fccsInvId);

				log.debug("Taken final SnapShot and updating the PublishAvailability");

				publishAVSEntries();

				return true;

			}

		} catch (Exception e) {
			log.error("Error during Inventory Diff final update", e);

		}

		return false;
	}

	/** Derive existing snapshot */
	private Collection<FCCSegInventoryDTO> takeInvSnapShot(int fccsInvId) {

		// intercepting segments list
		List<Integer> interceptiongFCCSegInvIdsForAVS = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(fccsInvId,
				true);

		// original and its intercepting segments are checked updates
		interceptiongFCCSegInvIdsForAVS.add(fccsInvId);

		Collection<FCCSegInventory> currentSegInvList = getFlightInventoryDAO().getFCCSegInventories(
				interceptiongFCCSegInvIdsForAVS);

		Collection<FCCSegInventoryDTO> currentSegInvDTOList = new ArrayList<FCCSegInventoryDTO>();

		for (FCCSegInventory currentSegInv : currentSegInvList) {

			FCCSegInventory clonedCurrentSegInventory = new FCCSegInventory();

			FCCSegInventoryDTO currentSegInvDTO = new FCCSegInventoryDTO();

			int availableSeats;

			// fixed BC adjustment only needed for the active Segment
			// intercepting fixed BC are already reflected on the availability
			// get segment specific fixed BC
			int availableFixedSeatCount = getFlightInventoryJDBCDAO().getAvailableFixedSeatCount(currentSegInv.getFccsInvId())
					.intValue();

			int allocatedFixedSeatCount = currentSegInv.getFixedSeats();

			// reduce sold fixed seat
			int fixedSeatAdjustment = Math.min(allocatedFixedSeatCount, availableFixedSeatCount);

			// reduce (own) fixed BC from the segment availability
			availableSeats = Math.max((currentSegInv.getAvailableSeats() - fixedSeatAdjustment), 0);

			// prepare the clone, only required fields are added
			clonedCurrentSegInventory.setAvailableSeats(availableSeats);
			clonedCurrentSegInventory.setFlightSegId(currentSegInv.getFlightSegId());
			clonedCurrentSegInventory.setFlightId(currentSegInv.getFlightId());
			clonedCurrentSegInventory.setFccsInvId(currentSegInv.getFccsInvId());

			// prepare DTO
			currentSegInvDTO.setAvailableSeats(availableSeats);
			currentSegInvDTO.setFlightId(currentSegInv.getFlightId());
			currentSegInvDTO.setFccsInvId(currentSegInv.getFccsInvId());

			// add cloned segment inventory object to DTO
			currentSegInvDTO.setFccSegInventory(clonedCurrentSegInventory);

			// get BC inventories published to some GDS(s)
			RetrieveFCCSegBCInvCriteria retrieveFCCSegBCInvCriteria = new RetrieveFCCSegBCInvCriteria();
			retrieveFCCSegBCInvCriteria.setFccsInvId(currentSegInv.getFccsInvId());
			retrieveFCCSegBCInvCriteria.setIsGDSAllocationsOnly(true);

			Collection<FCCSegBCInventory> gdsFCCSegBCInventories = getFlightInventoryDAO().getFCCSegBCInventories(
					retrieveFCCSegBCInvCriteria);

			Collection<ExtendedFCCSegBCInventory> extendedBCs = new ArrayList<ExtendedFCCSegBCInventory>();

			// per BC inventory published to some GDS
			if (gdsFCCSegBCInventories != null) {

				for (FCCSegBCInventory fccSegBCInventory : gdsFCCSegBCInventories) {

					extendedBCs.add((ExtendedFCCSegBCInventory) getFlightInventoryDAO().getExtendedBCInventory(
							currentSegInv.getFlightSegId(), fccSegBCInventory.getBookingCode()));

				}

				currentSegInvDTO.setFccSegBCInventories(extendedBCs);
			}

			currentSegInvDTOList.add(currentSegInvDTO);
		}

		return currentSegInvDTOList;
	}

	/** Derive existing snapshot */
	private boolean publishAVSEntries() {

		// get the flight published GDS Id
		Flight flight = null;
		try {
			flight = AirInventoryModuleUtils.getFlightBD().getFlight(initialSnapShotCollection.iterator().next().getFlightId());
		} catch (ModuleException e) {

		}


		//only update AVS for published ACTIVE flights (closed, cancelled flight inventory updates aren't sent)
		//no need to consider the CREATE case as this is inventory update
		if (!flight.getStatus().contentEquals(FlightStatusEnum.ACTIVE.getCode())) {
			return false;
		}

		Set<Integer> gDSIds = flight.getGdsIds();

		Map<Integer, FCCSegInventoryDTO> finalSnapShotMap = new HashMap<Integer, FCCSegInventoryDTO>();

		for (FCCSegInventoryDTO fccSegInventoryDTO : finalSnapShotCollection) {
			finalSnapShotMap.put(new Integer(fccSegInventoryDTO.getFccSegInventory().getFlightSegId()), fccSegInventoryDTO);
		}

		for (FCCSegInventoryDTO initialSegInvDTO : initialSnapShotCollection) {
			FCCSegInventoryDTO finalSegInvDTO = finalSnapShotMap.get(initialSegInvDTO.getFccSegInventory().getFlightSegId());

			// per each segment

			if (gDSIds != null && gDSIds.size() > 0) { // is published to some GDS

				if (AirinventoryUtils.getAirInventoryConfig().isPublishAvailability()) {

					Map<Integer, ExtendedFCCSegBCInventory> finalFCCSegBCInvMap = new HashMap<Integer, ExtendedFCCSegBCInventory>();
					Map<Integer, ExtendedFCCSegBCInventory> initialFCCSegBCInvMap = new HashMap<Integer, ExtendedFCCSegBCInventory>();

					// all the BCs added are published to GDS
					Collection<ExtendedFCCSegBCInventory> finalFCCSegBCInventories = finalSegInvDTO.getFccSegBCInventories();
					Collection<ExtendedFCCSegBCInventory> initialFCCSegBCInventories = initialSegInvDTO.getFccSegBCInventories();

					if (finalFCCSegBCInventories != null && finalFCCSegBCInventories.size() > 0) {
						for (ExtendedFCCSegBCInventory extendedFCCSegBCInventory : finalFCCSegBCInventories) {
							finalFCCSegBCInvMap.put(
									new Integer(extendedFCCSegBCInventory.getFccSegBCInventory().getFccsbInvId()),
									extendedFCCSegBCInventory);
						}
					}

					if (initialFCCSegBCInventories != null && initialFCCSegBCInventories.size() > 0) {
						for (ExtendedFCCSegBCInventory extendedFCCSegBCInventory : initialFCCSegBCInventories) {
							initialFCCSegBCInvMap.put(new Integer(extendedFCCSegBCInventory.getFccSegBCInventory()
									.getFccsbInvId()), extendedFCCSegBCInventory);
						}
					}

					boolean hasGDSAllocations = finalFCCSegBCInventories != null && finalFCCSegBCInventories.size() > 0;

					if (hasGDSAllocations) {

						for (Entry<Integer, ExtendedFCCSegBCInventory> finalBCExtendedInv : finalFCCSegBCInvMap.entrySet()) {

							FCCSegBCInventory finalBCInv = finalBCExtendedInv.getValue().getFccSegBCInventory();
							FCCSegBCInventory initialBCInv = null;

							boolean isFreshBCInventory = false;

							if (initialFCCSegBCInvMap.containsKey(finalBCInv.getFccsbInvId())) {
								initialBCInv = initialFCCSegBCInvMap.get(finalBCInv.getFccsbInvId()).getFccSegBCInventory();
							} else {
								isFreshBCInventory = true;
								initialBCInv = finalBCInv;
							}

							Set<GDSBookingClass> gdsBCs = getBookingClassDAO().getBookingClass(
									finalBCExtendedInv.getValue().getFccSegBCInventory().getBookingCode()).getGdsBCs();

							for (GDSBookingClass gdsBookingClass : gdsBCs) {

								if (gDSIds.contains(gdsBookingClass.getGdsId())) {

									getFlightInventoryDAO().saveOrUpdatePubAvailForUpdateCreateAndCancel(
											initialSegInvDTO.getFccSegInventory(), finalSegInvDTO.getFccSegInventory(),
											initialBCInv, finalBCInv, gdsBookingClass.getGdsId(), isFreshBCInventory, false);

								}
							}
						}
					}

					for (Entry<Integer, ExtendedFCCSegBCInventory> initialBCExtendedInv : initialFCCSegBCInvMap.entrySet()) {

						FCCSegBCInventory initialBCInv = initialBCExtendedInv.getValue().getFccSegBCInventory();
						FCCSegBCInventory finalBCInv = null;

						boolean isDeletedBCInv = false;

						if (!finalFCCSegBCInvMap.containsKey(initialBCInv.getFccsbInvId())) {

							isDeletedBCInv = true;
							finalBCInv = initialBCInv;

							Set<GDSBookingClass> gdsBCs = getBookingClassDAO().getBookingClass(
									initialBCExtendedInv.getValue().getFccSegBCInventory().getBookingCode()).getGdsBCs();

							for (GDSBookingClass gdsBookingClass : gdsBCs) {

								if (gDSIds.contains(gdsBookingClass.getGdsId())) {

									getFlightInventoryDAO().saveOrUpdatePubAvailForUpdateCreateAndCancel(
											initialSegInvDTO.getFccSegInventory(), finalSegInvDTO.getFccSegInventory(),
											initialBCInv, finalBCInv, gdsBookingClass.getGdsId(), false, isDeletedBCInv);

								}
							}
						}
					}
				}
			}
		}

		return true;
	}

	private BookingClassDAO getBookingClassDAO() {
		BookingClassDAO bookingClassDAO = (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean(
				"BookingClassDAOImplProxy");
		return bookingClassDAO;
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean(
				"FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	private FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance().getLocalBean(
				"flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

}
