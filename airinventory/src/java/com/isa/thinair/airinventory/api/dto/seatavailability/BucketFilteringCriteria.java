package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.FarePercentageCalculator;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Criteria for filtering buckets with available requested seats
 * 
 * @author MN
 */
public class BucketFilteringCriteria implements Cloneable {

	/* per flight semgent information */
	private int flightId;

	private Date depatureDate;

	private Timestamp depatureTime;

	private Date departureDateTimeZulu;

	private String fromAirport;

	private String toAirport;

	private String segmentCode;

	/* Common for multiple flight segments */
	private String cabinClassCode;

	private String requestedLogicalCabinClassCode;

	/* Common for multiple flight segments */
	private String bookingClassCode;

	private int noOfAdults;

	private int channelCode;

	private String agentCode;

	private int noOfInfants;

	private int noOfChilds;

	private String bookingPaxType = AirPricingCustomConstants.BookingPaxType.ANY;

	private Collection<String> bookingClassTypes = null;

	private String fareCategoryType = AirPricingCustomConstants.FareCategoryType.ANY;

	/* Common for each way of the journey */
	private String ondCode;

	private boolean isInboundFlight = false;

	/* Applicable only for return flights */
	private boolean returnFlag = false;

	private Date arrivalDateTime;

	private Date inboundDate;

	private Date inboundDateZulu;

	private Date sqlInboundTime;

	private Date sqlOutboundTime;

	/* Applicable only for all fares search */
	private boolean retrieveChannelAndAgentInfo = false;

	private boolean excludeNonLowestPublicFare = false;

	private boolean checkAvailability = true;

	/** Applicable only fare quote of modify segment */
	private Boolean enforceFareCheckForModSeg = false;

	private BigDecimal minPaxFareAmount;

	private boolean isOpenReturn;

	private boolean isConfirmOpenReturn;

	private Collection<String> externalBookingClasses = null;

	private ExternalFareSummaryDTO externalFareSummaryDTO = null;

	private boolean isInterlineFareQuoted = false;

	private Integer fareRuleId;

	private Integer fareId;

	private boolean isFlexiQuote = false;

	private Date fareQuoteDate = null;

	private boolean showFlexiFareOnly = false;

	private Long stayOverTime;

	private boolean returnOnd;

	private JourneyType journeyType;

	private boolean bookedFlightCabinBeingSearched;

	private String bookedFlightCabinClass;

	private String bookedFlightBookingClass;

	private boolean fixedFareOnly;

	private boolean goshoFareOnly;

	private boolean searchAllBC;

	private boolean flownOnd;

	private boolean skipInvCheckForFlown;

	private boolean isReturnFareRule;

	// This is to capture whether the original search is for wait listing
	private boolean waitListingSearch = false;

	// This is to search for wait listing inventory availability
	private boolean searchForWaitListingSeatAvailability = false;

	private boolean unTouchedOnd = false;

	private boolean quoteSameFareBucket = false;

	private PaxTypewiseFareONDChargeInfo paxTypewiseFareOndChargeInfo;

	private FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO;

	private boolean isOverbookEnabled;

	private boolean isFlightWithinCutoff = false;

	private String pointOfSale;

	private String preferredLanguage;

	private boolean ignoreStayOverTime;

	private boolean excludeOnewayPublicFaresFromReturnJourneys;

	public BucketFilteringCriteria() {

	}

	public BucketFilteringCriteria(FlightSegmentDTO flightSegmentDTO, AvailableFlightSearchDTO availableFlightSearchDTO,
			String ondCode, Collection<String> externalBookingClasses, PaxTypewiseFareONDChargeInfo chargeRatio,
			int ondSequence) {
		this(flightSegmentDTO, getAvailDTO(flightSegmentDTO, availableFlightSearchDTO, ondCode), externalBookingClasses,
				chargeRatio, ondSequence, availableFlightSearchDTO.getPreferredLanguage());

	}

	private static HRTAvailableFlightSearchDTO getAvailDTO(FlightSegmentDTO flightSegmentDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, String ondCode) {
		HRTAvailableFlightSearchDTO avlDTO = new HRTAvailableFlightSearchDTO(availableFlightSearchDTO);
		avlDTO.setFQOBDepartureDate(flightSegmentDTO.getDepartureDateTime());
		avlDTO.setFQOBDepartureDateZulu(flightSegmentDTO.getDepartureDateTimeZulu());
		avlDTO.setFQOBArrivalDate(flightSegmentDTO.getArrivalDateTime());
		avlDTO.setFQOnd(ondCode);
		return avlDTO;
	}

	public BucketFilteringCriteria(FlightSegmentDTO flightSegmentDTO, HRTAvailableFlightSearchDTO availableFlightSearchDTO,
			Collection<String> externalBookingClasses, PaxTypewiseFareONDChargeInfo fareOndChargeInfo, int ondSequence,
			String preferredLanguage) {
		setFlightId(flightSegmentDTO.getFlightId().intValue());
		setDepatureDateAndTime(availableFlightSearchDTO.getFQOBDepartureDate());
		setDepartureDateTimeZulu(availableFlightSearchDTO.getFQOBDepartureDateZulu());
		setArrivalDateTime(availableFlightSearchDTO.getFQOBArrivalDate());
		setFromAirport(flightSegmentDTO.getFromAirport());
		setToAirport(flightSegmentDTO.getToAirport());
		setSegmentCode(flightSegmentDTO.getSegmentCode());
		setOverbookEnabled(flightSegmentDTO.isOverBookEnabled());
		setFlightWithinCutoff(flightSegmentDTO.isFlightWithinCutoff());
		setPreferredLanguage(preferredLanguage);

		if (availableFlightSearchDTO.getPreferredBundledServicePeriodId(ondSequence) != null
				&& (flightSegmentDTO.getOperationTypeID() != null
						&& flightSegmentDTO.getOperationTypeID() == AirScheduleCustomConstants.OperationTypes.BUS_SERVICE)) {
			setBookingClassCode(null);
		} else {
			Map<String, String> segmentBookingClassSelection = availableFlightSearchDTO
					.getOndSegmentBookingClassSelection(ondSequence);

			if (segmentBookingClassSelection != null
					&& segmentBookingClassSelection.containsKey(flightSegmentDTO.getSegmentCode())) {
				setBookingClassCode(segmentBookingClassSelection.get(flightSegmentDTO.getSegmentCode()));
			} else {
				setBookingClassCode(availableFlightSearchDTO.getPreferredBookingClass(ondSequence));
			}

		}

		boolean isQuoteSameFareBUcket = availableFlightSearchDTO.isQuoteSameFareBucket();

		Collection<String> bookingClassTypes = new ArrayList<String>();
		String bookingType = availableFlightSearchDTO.getPreferredBookingType(ondSequence);
		if (bookingType != null) {
			bookingClassTypes.add(bookingType);
			setWaitListingSearch(bookingType.equals(ReservationInternalConstants.ReservationType.WL.getDescription()));
		}
		if (externalBookingClasses != null && externalBookingClasses.size() > 0) {
			if (!bookingClassTypes.contains(BookingClass.BookingClassType.INTERLINE_FREESELL)) {
				bookingClassTypes.add(BookingClass.BookingClassType.INTERLINE_FREESELL);
			}
			if (!bookingClassTypes.contains(BookingClass.BookingClassType.NORMAL)) {
				bookingClassTypes.add(BookingClass.BookingClassType.NORMAL);
			}
		} else {
			if (bookingClassTypes.contains(BookingClass.BookingClassType.INTERLINE_FREESELL)
					&& !bookingClassTypes.contains(BookingClass.BookingClassType.NORMAL)) {
				bookingClassTypes.add(BookingClass.BookingClassType.NORMAL);
			}
		}

		if (flightSegmentDTO.isFlightWithinCutoff()) {
			if (flightSegmentDTO.isOverBookEnabled() || availableFlightSearchDTO.isAllowDoOverbookAfterCutOffTime()) {
				bookingClassTypes.add(BookingClass.BookingClassType.OVERBOOK);
			} else {
				bookingClassTypes.remove(BookingClass.BookingClassType.OVERBOOK);
				if (!bookingClassTypes.contains(BookingClass.BookingClassType.NORMAL)) {
					bookingClassTypes.add(BookingClass.BookingClassType.NORMAL);
				}
			}
		}
		setBookingClassTypes(bookingClassTypes);

		setBookingPaxType(availableFlightSearchDTO.getBookingPaxType());
		setFareCategoryType(availableFlightSearchDTO.getFareCategoryType());

		setOpenReturn(availableFlightSearchDTO.isOpenReturnSearch());
		setRetrieveChannelAndAgentInfo(true);
		setOndCode(availableFlightSearchDTO.getFQOnd());
		setNoOfAdults(availableFlightSearchDTO.getAdultCount());
		setNoOfInfants(availableFlightSearchDTO.getInfantCount());
		setNoOfChilds(availableFlightSearchDTO.getChildCount());
		setChannelCode(availableFlightSearchDTO.getChannelCode());
		setAgentCode(availableFlightSearchDTO.getAgentCode());
		setFixedFareOnly(availableFlightSearchDTO.isFixedAgentFareOnly());
		setGoshoFareOnly(availableFlightSearchDTO.isGoshoFareAgentOnly());
		setJourneyType(availableFlightSearchDTO.getJourneyType());
		setReturnOnd(availableFlightSearchDTO.isReturnOnd(ondSequence));

		// setReturnFlag(availableFlightSearchDTO.isReturnFlag());
		// setInboundFlight(isInboundFlightSegment && availableFlightSearchDTO.isReturnFlag());
		setConfirmOpenReturn(availableFlightSearchDTO.isConfirmOpenReturn());

		// || (availableFlightSearchDTO.getEnforceFareCheckForModOnd() == true && availableFlightSearchDTO
		// .isHalfReturnFareQuote()));
		// setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());

		setExternalBookingClasses(externalBookingClasses);
		setInterlineFareQuoted(availableFlightSearchDTO.isInterlineFareQuoted());
		setExternalFareSummaryDTO(availableFlightSearchDTO.getExternalFareSummaryDTO());
		setFlexiQuote(availableFlightSearchDTO.isFlexiQuote(ondSequence));
		setCabinClassCode(availableFlightSearchDTO.getPreferredClassOfService(ondSequence));
		setRequestedLogicalCabinClassCode(availableFlightSearchDTO.getPreferredLogicalCabin(ondSequence));
		setFareQuoteDate(availableFlightSearchDTO.getEffectiveLastFQDate(flightSegmentDTO.getSegmentId()));

		// Set Fare Rule ID, Fare ID for OHD roll forward
		// Override cabin class code & booking class code if exists in roll forward custom DTO
		Map<Integer, FareIdDetailDTO> fltFareDetails = availableFlightSearchDTO.getFlightSegFareDetails();
		if ((isQuoteSameFareBUcket || !AppSysParamsUtil.isReQuoteOnlyRequiredONDs()) && fltFareDetails != null
				&& !fltFareDetails.isEmpty()) {
			FareIdDetailDTO fareIdDetailDTO = fltFareDetails.get(flightSegmentDTO.getSegmentId());
			if (fareIdDetailDTO != null) {
				if (isQuoteSameFareBUcket && availableFlightSearchDTO.isUnTouchedOnd(ondSequence)) {
					setFareRuleId(fareIdDetailDTO.getFareRuleId());
					setFareId(fareIdDetailDTO.getFareId());
				} else if (!AppSysParamsUtil.isReQuoteOnlyRequiredONDs()) {
					setFareRuleId(fareIdDetailDTO.getFareRuleId());
					setFareId(fareIdDetailDTO.getFareId());
				}

				if (fareIdDetailDTO.getCabinClassCode() != null && !"".equals(fareIdDetailDTO.getCabinClassCode())) {
					setCabinClassCode(fareIdDetailDTO.getCabinClassCode());
				}
				if (fareIdDetailDTO.getBookingClassCode() != null && !"".equals(fareIdDetailDTO.getBookingClassCode())) {
					setBookingClassCode(fareIdDetailDTO.getBookingClassCode());
				}
			}
		}

		if (availableFlightSearchDTO.getFQIBDepartureDateZulu() != null) {
			setStayOverTime(availableFlightSearchDTO.getFQIBDepartureDateZulu().getTime()
					- availableFlightSearchDTO.getFQOBDepartureDateZulu().getTime());
		}
		if (availableFlightSearchDTO.getFQIBDepartureDate() != null) {
			setInboundDate(availableFlightSearchDTO.getFQIBDepartureDate());
			setInboundDateZulu(availableFlightSearchDTO.getFQIBDepartureDateZulu());
		}

		if (availableFlightSearchDTO.getEnforceFareCheckForModOnd()) {
			if (availableFlightSearchDTO.getChannelCode() == 4 && availableFlightSearchDTO.getModifiedFlightSegments() != null
					&& !availableFlightSearchDTO.getModifiedFlightSegments().isEmpty()
					&& availableFlightSearchDTO.getModifiedFlightSegments().size() > 0
					&& flightSegmentDTO.getSegmentCode()
							.equals(((FlightSegmentDTO) availableFlightSearchDTO.getModifiedFlightSegments().toArray()[0])
									.getSegmentCode())) {
				setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
				if (availableFlightSearchDTO.getModifiedOndFareType() == 2 && !isReturnFareSearch()) {
					// get the prorated amount of actual fare amount for the modified seg
					BigDecimal ondFareAmount = AccelAeroCalculator
							.divide(AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
									FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
					setMinPaxFareAmount(ondFareAmount);
				} else {
					setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());
				}
			} else if (availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null) {
				setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
				if (availableFlightSearchDTO.getModifiedOndFareType() != null
						&& availableFlightSearchDTO.getModifiedOndFareType() == 2 && !isReturnFareSearch()
						&& !journeyType.equals(JourneyType.OPEN_JAW)) {
					// get the prorated amount of actual fare amount for the modified seg
					BigDecimal ondFareAmount = AccelAeroCalculator
							.divide(AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
									FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
					setMinPaxFareAmount(ondFareAmount);
				} else if (availableFlightSearchDTO.getModifiedOndFareType() != null
						&& availableFlightSearchDTO.getModifiedOndFareType() != 2 && isReturnFareSearch()) {
					// get the prorated full amount of actual fare amount for the modified seg
					BigDecimal fullFareAmount = AccelAeroCalculator.divide(
							AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(), 100),
							FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE);
					setMinPaxFareAmount(fullFareAmount);
				} else {
					setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());
				}
			} else if (FareQuoteUtil.isConnectionFareServed(availableFlightSearchDTO.getOriginDestinationInfoDTOs())) {
				setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
				if (availableFlightSearchDTO.getJourneyType() == JourneyType.SINGLE_SECTOR) {
					OriginDestinationInfoDTO originDestinationInfoDTO = availableFlightSearchDTO.getOriginDestinationInfoDTOs()
							.iterator().next();
					if (availableFlightSearchDTO.getFQOnd().startsWith(originDestinationInfoDTO.getOrigin())
							&& availableFlightSearchDTO.getFQOnd().endsWith(originDestinationInfoDTO.getDestination())) {
						setMinPaxFareAmount(originDestinationInfoDTO.getOldPerPaxFare());
					} else {
						setMinPaxFareAmount(AccelAeroCalculator.divide(originDestinationInfoDTO.getOldPerPaxFare(),
								originDestinationInfoDTO.getFlightSegmentIds().size()));
					}
				} else {
					// TODO
				}
			}
		}

		if (availableFlightSearchDTO.isSameFlightSearched(ondSequence, flightSegmentDTO.getSegmentId())) {
			String bookedCC = availableFlightSearchDTO.getBookedFlightCabinClass(flightSegmentDTO.getSegmentId());
			this.bookedFlightCabinBeingSearched = availableFlightSearchDTO.isSameCabinClassSearched(ondSequence, bookedCC);
			this.bookedFlightCabinClass = bookedCC;
		}

		if (!availableFlightSearchDTO.isFromNameChange() || availableFlightSearchDTO.isFlownOnd(ondSequence)) {
			this.bookedFlightBookingClass = availableFlightSearchDTO.getBookedFlightBookingClass(flightSegmentDTO.getSegmentId());
		}

		this.searchAllBC = availableFlightSearchDTO.isSearchAllBC();

		setFlownOnd(availableFlightSearchDTO.isFlownOnd(ondSequence));
		setUnTouchedOnd(availableFlightSearchDTO.isUnTouchedOnd(ondSequence));
		skipInvCheckForFlown = availableFlightSearchDTO.isSkipInvCheckForFlown();
		setPaxTypewiseFareOndChargeInfo(fareOndChargeInfo);
		if (isQuoteSameFareBUcket && this.unTouchedOnd) {
			setQuoteSameFareBucket(true);
		}

		if (availableFlightSearchDTO.isOpenReturnSearch() && availableFlightSearchDTO.getOutBoundCheapestBucketBCDTO() != null) {
			setOutBoundCheapestBucketBCDTO(availableFlightSearchDTO.getOutBoundCheapestBucketBCDTO());
		}

		setPointOfSale(availableFlightSearchDTO.getPointOfSale());
	}

	private void setFixedFareOnly(boolean fixedFareOnly) {
		this.fixedFareOnly = fixedFareOnly;
	}

	public boolean isFixedFareOnly() {
		return fixedFareOnly;
	}

	public BucketFilteringCriteria(FlightSegmentDTO selectedFlightSegmentDTO, FareFilteringCriteria criteria,
			HRTAvailableFlightSearchDTO availableFlightSearchDTO, Integer selectedOndSeq, boolean isOpenReturnSearch) {
		this.setFlightId(selectedFlightSegmentDTO.getFlightId().intValue());
		this.setFromAirport(selectedFlightSegmentDTO.getFromAirport());
		this.setToAirport(selectedFlightSegmentDTO.getToAirport());
		this.setDepatureDateAndTime(selectedFlightSegmentDTO.getDepartureDateTime());
		this.setDepartureDateTimeZulu(selectedFlightSegmentDTO.getDepartureDateTimeZulu());
		this.setArrivalDateTime(selectedFlightSegmentDTO.getArrivalDateTime());
		this.setSegmentCode(selectedFlightSegmentDTO.getSegmentCode());
		// common information
		this.setAgentCode(criteria.getAgentCode());
		this.setFixedFareOnly(availableFlightSearchDTO.isFixedAgentFareOnly());
		this.setGoshoFareOnly(availableFlightSearchDTO.isGoshoFareAgentOnly());
		this.setCabinClassCode(
				availableFlightSearchDTO.getOrderedOriginDestinations().get(selectedOndSeq).getPreferredClassOfService());
		this.setRequestedLogicalCabinClassCode(
				availableFlightSearchDTO.getOrderedOriginDestinations().get(selectedOndSeq).getPreferredLogicalCabin());

		// selFltSegFilterCriteria.setBookingClassType(criteria.getBookingType());
		Collection<String> bookingClassTypes = new ArrayList<String>();
		bookingClassTypes.add(criteria.getBookingType());

		this.setBookingClassTypes(bookingClassTypes);
		this.setChannelCode(criteria.getChannelCode());
		this.setExcludeNonLowestPublicFare(criteria.isExcludeNonLowestPublicFares());
		this.setNoOfAdults(criteria.getNumberOfAdults());
		this.setNoOfChilds(criteria.getNumberOfChilds());
		this.setNoOfInfants(criteria.getNumberOfInfants());
		this.setBookingPaxType(criteria.getBookingPaxType());
		this.setFareCategoryType(criteria.getFareCategoryType());

		// if (criteria.getAgentCode() != null || criteria.isExcludeNonLowestPublicFares()) {
		// this.setCheckAvailability(true);
		// }

		this.setJourneyType(availableFlightSearchDTO.getJourneyType());
		this.setReturnOnd(availableFlightSearchDTO.isReturnOnd(selectedOndSeq));
		this.setOndCode(selectedFlightSegmentDTO.getSegmentCode());
		this.setFlexiQuote(availableFlightSearchDTO.isFlexiQuote(selectedOndSeq));

		if (criteria.getOndShowFaresWithFlexi() != null && criteria.getOndShowFaresWithFlexi().containsKey(selectedOndSeq)) {
			this.setShowFlexiFareOnly(criteria.getOndShowFaresWithFlexi().get(selectedOndSeq));
		}

		this.setRetrieveChannelAndAgentInfo(true);
		this.setOpenReturn(isOpenReturnSearch);

		if (availableFlightSearchDTO.getEnforceFareCheckForModOnd()
				&& availableFlightSearchDTO.getModifiedFlightSegments() != null
				&& !availableFlightSearchDTO.getModifiedFlightSegments().isEmpty()
				&& selectedFlightSegmentDTO.getSegmentCode()
						.equals(((FlightSegmentDTO) availableFlightSearchDTO.getModifiedFlightSegments().toArray()[0])
								.getSegmentCode())) {
			this.setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
			this.setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());
		}

		if (availableFlightSearchDTO.getEnforceFareCheckForModOnd()) {
			if (availableFlightSearchDTO.getChannelCode() == 4 && availableFlightSearchDTO.getModifiedFlightSegments() != null
					&& !availableFlightSearchDTO.getModifiedFlightSegments().isEmpty()
					&& availableFlightSearchDTO.getModifiedFlightSegments().size() > 0
					&& selectedFlightSegmentDTO.getSegmentCode()
							.equals(((FlightSegmentDTO) availableFlightSearchDTO.getModifiedFlightSegments().toArray()[0])
									.getSegmentCode())) {
				setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
				if (availableFlightSearchDTO.getModifiedOndFareType() == 2 && !isReturnFareSearch()) {
					// get the prorated amount of actual fare amount for the modified seg
					BigDecimal ondFareAmount = AccelAeroCalculator
							.divide(AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
									FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
					setMinPaxFareAmount(ondFareAmount);
				} else {
					setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());
				}
			} else if (availableFlightSearchDTO.getModifiedOndPaxFareAmount() != null) {
				JourneyType journeyType = availableFlightSearchDTO.getJourneyType();
				setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
				if (availableFlightSearchDTO.getModifiedOndFareType() != null
						&& availableFlightSearchDTO.getModifiedOndFareType() == 2 && !isReturnFareSearch()
						&& !journeyType.equals(JourneyType.OPEN_JAW)) {
					// get the prorated amount of actual fare amount for the modified seg
					BigDecimal ondFareAmount = AccelAeroCalculator
							.divide(AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(),
									FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE), 100);
					setMinPaxFareAmount(ondFareAmount);
				} else if (availableFlightSearchDTO.getModifiedOndFareType() != null
						&& availableFlightSearchDTO.getModifiedOndFareType() != 2 && isReturnFareSearch()) {
					// get the prorated full amount of actual fare amount for the modified seg
					BigDecimal fullFareAmount = AccelAeroCalculator.divide(
							AccelAeroCalculator.multiply(availableFlightSearchDTO.getModifiedOndPaxFareAmount(), 100),
							FarePercentageCalculator.RETURN_AND_OPEN_RETURN_FARE_PERCENTAGE);
					setMinPaxFareAmount(fullFareAmount);
				} else {
					setMinPaxFareAmount(availableFlightSearchDTO.getModifiedOndPaxFareAmount());
				}
			} else if (FareQuoteUtil.isConnectionFareServed(availableFlightSearchDTO.getOriginDestinationInfoDTOs())) {
				setEnforceFareCheckForModSeg(availableFlightSearchDTO.getEnforceFareCheckForModOnd());
				if (availableFlightSearchDTO.getJourneyType() == JourneyType.SINGLE_SECTOR) {
					OriginDestinationInfoDTO originDestinationInfoDTO = availableFlightSearchDTO.getOriginDestinationInfoDTOs()
							.iterator().next();
					if (availableFlightSearchDTO.getFQOnd() != null
							&& availableFlightSearchDTO.getFQOnd().startsWith(originDestinationInfoDTO.getOrigin())
							&& availableFlightSearchDTO.getFQOnd().endsWith(originDestinationInfoDTO.getDestination())) {
						setMinPaxFareAmount(originDestinationInfoDTO.getOldPerPaxFare());
					} else {
						setMinPaxFareAmount(AccelAeroCalculator.divide(originDestinationInfoDTO.getOldPerPaxFare(),
								originDestinationInfoDTO.getFlightSegmentIds().size()));
					}
				} else {
					// TODO
				}
			}
		}

		// set last quoted date
		this.setFareQuoteDate(availableFlightSearchDTO.getEffectiveLastFQDate(selectedFlightSegmentDTO.getSegmentId()));

		if (!availableFlightSearchDTO.isFromNameChange() || availableFlightSearchDTO.isFlownOnd(selectedOndSeq)) {
			if (availableFlightSearchDTO.isSameFlightSearched(selectedOndSeq, selectedFlightSegmentDTO.getSegmentId())) {
				String bookedCC = availableFlightSearchDTO.getBookedFlightCabinClass(selectedFlightSegmentDTO.getSegmentId());
				this.bookedFlightCabinBeingSearched = availableFlightSearchDTO.isSameCabinClassSearched(selectedOndSeq, bookedCC);
				this.bookedFlightCabinClass = bookedCC;
			}
			this.bookedFlightBookingClass = availableFlightSearchDTO
					.getBookedFlightBookingClass(selectedFlightSegmentDTO.getSegmentId());
		}

		this.searchAllBC = availableFlightSearchDTO.isSearchAllBC();

		setFlownOnd(availableFlightSearchDTO.isFlownOnd(selectedOndSeq));
		setUnTouchedOnd(availableFlightSearchDTO.isUnTouchedOnd(selectedOndSeq));
		if (availableFlightSearchDTO.isQuoteSameFareBucket() && this.unTouchedOnd) {
			setQuoteSameFareBucket(true);
		}

		setPointOfSale(availableFlightSearchDTO.getPointOfSale());

		skipInvCheckForFlown = availableFlightSearchDTO.isSkipInvCheckForFlown();
		setExcludeOnewayPublicFaresFromReturnJourneys(criteria.isExcludeOnewayPublicFaresFromReturnJourneys());

	}

	public boolean isReturnOnd() {
		return returnOnd;
	}

	public boolean isReturnFareJourney() {
		if (journeyType == JourneyType.ROUNDTRIP || journeyType == JourneyType.CIRCULAR || journeyType == JourneyType.OPEN_JAW) {
			return true;
		}
		return false;
	}

	public void setReturnOnd(boolean returnOnd) {
		this.returnOnd = returnOnd;
	}

	public void setJourneyType(JourneyType journeyType) {
		this.journeyType = journeyType;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	/**
	 * stayover time
	 */
	public void setStayOverTime(Long stayOverTime) {
		this.stayOverTime = stayOverTime;
	}

	public Long getStayOverTime() {
		return stayOverTime;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getCabinClassCode() {
		if (searchAllBC) {
			return null;
		}
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public int getChannelCode() {
		return channelCode;
	}

	/**
	 * @return the bookingClassTypes
	 */
	public Collection<String> getBookingClassTypes() {
		return bookingClassTypes;
	}

	public boolean hasBookingClassType(String bcType) {
		if (getBookingClassTypes() != null) {
			for (String bookingClassType : getBookingClassTypes()) {
				if (bookingClassType.equals(bcType)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param bookingClassTypes
	 *            the bookingClassTypes to set
	 */
	public void setBookingClassTypes(Collection<String> bookingClassTypes) {
		this.bookingClassTypes = bookingClassTypes;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public long getAdvancedBookingDays() {
		long advBookingDays = Math.abs(departureDateTimeZulu.getTime() - getFareQuoteDate().getTime());
		return advBookingDays;
	}

	public java.sql.Date getSQLInBoundDate() {
		return new java.sql.Date(inboundDate.getTime());
	}

	private void prepareSqlInboundTime(Date inboundTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(inboundTime);
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONDAY, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.SECOND, 0); // This is because fare rule times are only upto minutes
		this.sqlInboundTime = new Timestamp(cal.getTimeInMillis());
	}

	private void prepareSqlOutboundTime(Date outboundTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(outboundTime);
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONDAY, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.SECOND, 0); // This is because fare rule times are only upto minutes
		this.sqlOutboundTime = new Timestamp(cal.getTimeInMillis());
	}

	public java.sql.Date getDepatureDate() {
		return new java.sql.Date(depatureDate.getTime());
	}

	public Date getOutboundDate() {
		if (depatureDate != null) {
			prepareSqlOutboundTime(depatureDate);
		}
		return depatureDate;
	}

	private void setDepatureDate(Date depatureDateTime) {
		// Calendar cal = Calendar.getInstance();
		// cal.setTime(depatureDateTime);
		// cal.set(Calendar.HOUR, 0);
		// cal.set(Calendar.MINUTE, 0);
		// cal.set(Calendar.SECOND, 0);
		// cal.set(Calendar.MILLISECOND, 0);
		// cal.set(Calendar.AM_PM, Calendar.AM);
		this.depatureDate = depatureDateTime;
	}

	public Timestamp getDepatureTime() {
		return depatureTime;
	}

	private void setDepatureTime(Date depatureDateTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(depatureDateTime);
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONDAY, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		this.depatureTime = new Timestamp(cal.getTimeInMillis());
	}

	public void setDepatureDateAndTime(Date depatureDateTime) {
		setDepatureDate(depatureDateTime);
		setDepatureTime(depatureDateTime);
	}

	public Date getInboundDate() {
		return inboundDate;
	}

	public void setInboundDate(Date inboundDate) {
		// Calendar cal = Calendar.getInstance();
		// cal.setTime(returnDateTime);
		// cal.set(Calendar.HOUR, 0);
		// cal.set(Calendar.MINUTE, 0);
		// cal.set(Calendar.SECOND, 0);
		// cal.set(Calendar.MILLISECOND, 0);
		// cal.set(Calendar.AM_PM, Calendar.AM);
		if (inboundDate != null) {
			prepareSqlInboundTime(inboundDate);
		}
		this.inboundDate = inboundDate;

	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public int getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	@Deprecated
	public boolean isReturnFlag() {
		return returnFlag;
	}

	@Deprecated
	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	// TODO : Initially used for half return. Since return flag has no meaning in segment fare search, we use this
	// boolean to identify return search. Need to revisit usages
	@Deprecated
	public boolean isInboundFlight() {
		return isInboundFlight;
	}

	@Deprecated
	public void setInboundFlight(boolean isInboundFlight) {
		this.isInboundFlight = isInboundFlight;
	}

	public boolean isRetrieveChannelAndAgentInfo() {
		return retrieveChannelAndAgentInfo;
	}

	public void setRetrieveChannelAndAgentInfo(boolean retrieveChannelAndAgentInfo) {
		this.retrieveChannelAndAgentInfo = retrieveChannelAndAgentInfo;
	}

	public String getBookingPaxType() {
		return bookingPaxType;
	}

	public void setBookingPaxType(String bookingPaxType) {
		this.bookingPaxType = bookingPaxType;
	}

	public String getFareCategoryType() {
		return fareCategoryType;
	}

	public void setFareCategoryType(String fareCategoryType) {
		this.fareCategoryType = fareCategoryType;
	}

	public boolean isExcludeNonLowestPublicFare() {
		return excludeNonLowestPublicFare;
	}

	public void setExcludeNonLowestPublicFare(boolean excludeNonLowestPublicFare) {
		this.excludeNonLowestPublicFare = excludeNonLowestPublicFare;
	}

	public boolean isCheckAvailability() {
		return checkAvailability;
	}

	public void setCheckAvailability(boolean checkAvailability) {
		this.checkAvailability = checkAvailability;
	}

	public int getNoOfChilds() {
		return noOfChilds;
	}

	public boolean isSameBookedFltCabinBeingSearched() {
		return bookedFlightCabinBeingSearched;
	}

	public boolean hasBookedFlightCabinClass() {
		return (bookedFlightCabinClass != null);
	}

	public boolean isSameBookedFltCabinBeingSearched(String ccCode) {
		if (ccCode != null) {
			return (ccCode.equals(this.bookedFlightCabinClass));
		}
		return false;
	}

	public void setBookedFltCabinBeingSearched(boolean bookedFlightCabinBeingSearched) {
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
	}

	public int getNoOfActualRequiredSeats() {
		return getNoOfAdults() + getNoOfChilds();
	}

	public void setNoOfChilds(int noOfChilds) {
		this.noOfChilds = noOfChilds;
	}

	@Override
	public BucketFilteringCriteria clone() {
		BucketFilteringCriteria clone = new BucketFilteringCriteria();
		clone.setFlightId(getFlightId());
		clone.setFromAirport(getFromAirport());
		clone.setToAirport(getToAirport());
		clone.setSegmentCode(getSegmentCode());
		clone.setDepatureDate(getDepatureDate());
		clone.setDepatureTime(getDepatureTime());
		clone.setSqlInboundTime(getInboundDate());
		clone.setDepartureDateTimeZulu(getDepartureDateTimeZulu());
		clone.setArrivalDateTime(getArrivalDateTime());
		clone.setInboundDate(getInboundDate());
		clone.setInboundDateZulu(getInboundDateZulu());
		clone.setFareQuoteDate(getFareQuoteDate());

		clone.setCabinClassCode(this.cabinClassCode);
		clone.setRequestedLogicalCabinClassCode(this.requestedLogicalCabinClassCode);
		clone.setBookingClassCode(getBookingClassCode());
		clone.setIgnoreStayOverTime(isIgnoreStayOverTime());

		Collection<String> clonedBcTypes = null;
		if (getBookingClassTypes() != null) {
			clonedBcTypes = new ArrayList<String>();
			for (String bcType : getBookingClassTypes()) {
				clonedBcTypes.add(bcType);
			}
		}
		clone.setBookingClassTypes(clonedBcTypes);

		clone.setChannelCode(getChannelCode());
		clone.setAgentCode(getAgentCode());
		clone.setFixedFareOnly(isFixedFareOnly());
		clone.setGoshoFareOnly(isGoshoFareOnly());
		clone.setBookingPaxType(getBookingPaxType());
		clone.setFareCategoryType(getFareCategoryType());
		clone.setExcludeNonLowestPublicFare(isExcludeNonLowestPublicFare());
		clone.setCheckAvailability(isCheckAvailability());

		clone.setNoOfAdults(getNoOfAdults());
		clone.setNoOfInfants(getNoOfInfants());
		clone.setNoOfChilds(getNoOfChilds());

		clone.setOndCode(getOndCode());

		// clone.setReturnFlag(isReturnFlag());
		// clone.setInboundFlight(isInboundFlight());
		clone.setOpenReturn(isOpenReturn());
		clone.setConfirmOpenReturn(isConfirmOpenReturn());
		clone.setRetrieveChannelAndAgentInfo(isRetrieveChannelAndAgentInfo());

		clone.setEnforceFareCheckForModSeg(getEnforceFareCheckForModSeg());
		clone.setMinPaxFareAmount(getMinPaxFareAmount());

		// shallow copy
		clone.setExternalFareSummaryDTO(this.getExternalFareSummaryDTO());
		clone.setInterlineFareQuoted(this.isInterlineFareQuoted());
		clone.setFlexiQuote(this.isFlexiQuote);
		clone.setShowFlexiFareOnly(this.showFlexiFareOnly);

		clone.setJourneyType(this.journeyType);
		clone.setReturnOnd(isReturnOnd());
		clone.setSameBookedFlightCabinBeingSearched(this.isSameBookedFltCabinBeingSearched());
		clone.setBookedFlightBookingClass(this.getBookedFlightBookingClass());
		clone.setStayOverTime(this.getStayOverTime());
		clone.setSearchAllBC(this.searchAllBC);
		clone.setFlownOnd(this.isFlownOnd());
		clone.setSkipInvCheckForFlown(this.skipInvCheckForFlown);
		clone.setBookedFlightCabinClass(this.bookedFlightCabinClass);
		clone.setReturnFareRule(this.isReturnFareRule());
		clone.setPaxTypewiseFareOndChargeInfo(this.getPaxTypewiseFareOndChargeInfo());
		clone.setUnTouchedOnd(this.unTouchedOnd);
		clone.setPointOfSale(this.pointOfSale);
		clone.setExcludeOnewayPublicFaresFromReturnJourneys(this.isExcludeOnewayPublicFaresFromReturnJourneys());

		return clone;
	}

	private void setSkipInvCheckForFlown(boolean skipInvCheckForFlown) {
		this.skipInvCheckForFlown = skipInvCheckForFlown;
	}

	public void setBookedFlightCabinClass(String bookedFlightCabinClass) {
		this.bookedFlightCabinClass = bookedFlightCabinClass;
	}

	public String getBookedFlightCabinClass() {
		return bookedFlightCabinClass;
	}

	/**
	 * @return Returns the departureDateTimeZulu.
	 */
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	/**
	 * @param departureDateTimeZulu
	 *            The departureDateTimeZulu to set.
	 */
	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	/**
	 * @return the enforceFareCheckForModSeg
	 */
	public Boolean getEnforceFareCheckForModSeg() {
		return enforceFareCheckForModSeg;
	}

	/**
	 * @param enforceFareCheckForModSeg
	 *            the enforceFareCheckForModSeg to set
	 */
	public void setEnforceFareCheckForModSeg(Boolean enforceFareCheckForModSeg) {
		this.enforceFareCheckForModSeg = enforceFareCheckForModSeg;
	}

	/**
	 * @return the minPaxFareAmount
	 */
	public BigDecimal getMinPaxFareAmount() {
		return minPaxFareAmount;
	}

	/**
	 * @param minPaxFareAmount
	 *            the minPaxFareAmount to set
	 */
	public void setMinPaxFareAmount(BigDecimal minPaxFareAmount) {
		this.minPaxFareAmount = minPaxFareAmount;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	/**
	 * @return the isConfirmOpenReturn
	 */
	public boolean isConfirmOpenReturn() {
		return isConfirmOpenReturn;
	}

	/**
	 * @param isConfirmOpenReturn
	 *            the isConfirmOpenReturn to set
	 */
	public void setConfirmOpenReturn(boolean isConfirmOpenReturn) {
		this.isConfirmOpenReturn = isConfirmOpenReturn;
	}

	/**
	 * @return the arrivalDateTime
	 */
	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	/**
	 * @param arrivalDateTime
	 *            the arrivalDateTime to set
	 */
	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	/**
	 * @return the externalBookingClasses
	 */
	public Collection<String> getExternalBookingClasses() {
		return externalBookingClasses;
	}

	/**
	 * @param externalBookingClasses
	 *            the arrivalDateTime to set
	 */
	public void setExternalBookingClasses(Collection<String> externalBookingClasses) {
		this.externalBookingClasses = externalBookingClasses;
	}

	/**
	 * @return the externalFareSummaryDTO
	 */
	public ExternalFareSummaryDTO getExternalFareSummaryDTO() {
		return externalFareSummaryDTO;
	}

	/**
	 * @param externalFareSummaryDTO
	 *            the externalFareSummaryDTO to set
	 */
	public void setExternalFareSummaryDTO(ExternalFareSummaryDTO externalFareSummaryDTO) {
		this.externalFareSummaryDTO = externalFareSummaryDTO;
	}

	/**
	 * @return the isInterlineFareQuote
	 */
	public boolean isInterlineFareQuoted() {
		return isInterlineFareQuoted;
	}

	/**
	 * @param isInterlineFareQuote
	 *            the isInterlineFareQuote to set
	 */
	public void setInterlineFareQuoted(boolean isInterlineFareQuoted) {
		this.isInterlineFareQuoted = isInterlineFareQuoted;
	}

	/**
	 * @return the fareRuleId
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            the fareRuleId to set
	 */
	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return the fareId
	 */
	public Integer getFareId() {
		return fareId;
	}

	/**
	 * @param fareId
	 *            the fareId to set
	 */
	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	public String getRequestedLogicalCabinClassCode() {
		if (searchAllBC) {
			return null;
		}
		return requestedLogicalCabinClassCode;
	}

	public void setRequestedLogicalCabinClassCode(String requestedLogicalCabinClassCode) {
		this.requestedLogicalCabinClassCode = requestedLogicalCabinClassCode;
	}

	public boolean isFlexiQuote() {
		return isFlexiQuote;
	}

	public void setFlexiQuote(boolean isFlexiQuote) {
		this.isFlexiQuote = isFlexiQuote;
	}

	/**
	 * REturn the fare quote date in zulu
	 * 
	 * @return
	 */
	public Date getFareQuoteDate() {
		if (fareQuoteDate == null) {
			return CalendarUtil.getCurrentSystemTimeInZulu();
		}
		return fareQuoteDate;
	}

	/**
	 * Set the fare quote date in zulu
	 * 
	 * @param fareQuoteDate
	 */
	public void setFareQuoteDate(Date fareQuoteDate) {
		this.fareQuoteDate = fareQuoteDate;
	}

	public boolean isShowFlexiFareOnly() {
		return showFlexiFareOnly;
	}

	public void setShowFlexiFareOnly(boolean showFlexiFareOnly) {
		this.showFlexiFareOnly = showFlexiFareOnly;
	}

	public boolean hasStayOverTime() {
		return (stayOverTime != null);
	}

	public boolean isReturnFareSearch() {
		if (isReturnFareJourney() && getInboundDate() != null) {
			return true;
		}
		return false;
	}

	public void setSameBookedFlightCabinBeingSearched(boolean bookedFlightCabinBeingSearched) {
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
	}

	public String getBookedFlightBookingClass() {
		return bookedFlightBookingClass;
	}

	public boolean hasBookedFlightBookingClass() {
		return (bookedFlightBookingClass != null);
	}

	public void setBookedFlightBookingClass(String bookedFlightBookingClass) {
		this.bookedFlightBookingClass = bookedFlightBookingClass;
	}

	private void setSearchAllBC(boolean searchAllBC) {
		this.searchAllBC = searchAllBC;
	}

	public boolean skipInvCheckForFlown() {
		return isFlownOnd() && skipInvCheckForFlown;
	}

	public boolean isWaitListingSearch() {
		return waitListingSearch;
	}

	public void setWaitListingSearch(boolean waitListingSearch) {
		this.waitListingSearch = waitListingSearch;
	}

	public boolean isSearchForWaitListingSeatAvailability() {
		return searchForWaitListingSeatAvailability;
	}

	public void setSearchForWaitListingSeatAvailability(boolean searchForWaitListingSeatAvailability) {
		this.searchForWaitListingSeatAvailability = searchForWaitListingSeatAvailability;
	}

	/**
	 * @return the isReturnFareRule
	 */
	public boolean isReturnFareRule() {
		return isReturnFareRule;
	}

	/**
	 * @param isReturnFareRule
	 *            the isReturnFareRule to set
	 */
	public void setReturnFareRule(boolean isReturnFareRule) {
		this.isReturnFareRule = isReturnFareRule;
	}

	/**
	 * @return the goshoFareOnly
	 */
	public boolean isGoshoFareOnly() {
		return goshoFareOnly;
	}

	/**
	 * @param goshoFareOnly
	 *            the goshoFareOnly to set
	 */
	public void setGoshoFareOnly(boolean goshoFareOnly) {
		this.goshoFareOnly = goshoFareOnly;
	}

	public boolean isUnTouchedOnd() {
		return unTouchedOnd;
	}

	public void setUnTouchedOnd(boolean unTouchedOnd) {
		this.unTouchedOnd = unTouchedOnd;
	}

	private void setPaxTypewiseFareOndChargeInfo(PaxTypewiseFareONDChargeInfo fareOndChargeInfo) {
		this.paxTypewiseFareOndChargeInfo = fareOndChargeInfo;
	}

	public PaxTypewiseFareONDChargeInfo getPaxTypewiseFareOndChargeInfo() {
		return paxTypewiseFareOndChargeInfo;
	}

	public double getTotalPercentageFactor(String ratioCategory) {
		if (paxTypewiseFareOndChargeInfo != null) {
			return paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT)
					.getTotalPercentageFactor(ratioCategory);
		}
		return 1;
	}

	public double getTotalValueFactor(String ratioCategory) {
		if (paxTypewiseFareOndChargeInfo != null) {
			return paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT)
					.getTotalValueFactor(ratioCategory);
		}
		return 0;
	}

	public double getTotalValueFactorForReturn(String ratioCategory) {
		if (paxTypewiseFareOndChargeInfo != null) {
			return paxTypewiseFareOndChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT)
					.getTotalValueFactorForReturn(ratioCategory);
		}
		return 0;
	}

	public Date getInboundDateZulu() {
		return inboundDateZulu;
	}

	public void setInboundDateZulu(Date inboundDateZulu) {
		this.inboundDateZulu = inboundDateZulu;
	}

	public boolean isQuoteSameFareBucket() {
		return quoteSameFareBucket;
	}

	public void setQuoteSameFareBucket(boolean quoteSameFareBucket) {
		this.quoteSameFareBucket = quoteSameFareBucket;
	}

	public FilteredBucketCheapestBCDTO getOutBoundCheapestBucketBCDTO() {
		return outBoundCheapestBucketBCDTO;
	}

	public void setOutBoundCheapestBucketBCDTO(FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO) {
		this.outBoundCheapestBucketBCDTO = outBoundCheapestBucketBCDTO;
	}

	public Date getSqlInboundTime() {
		return sqlInboundTime;
	}

	public void setSqlInboundTime(Date sqlInboundTime) {
		this.sqlInboundTime = sqlInboundTime;
	}

	public Date getSqlOutboundTime() {
		return sqlOutboundTime;
	}

	public void setSqlOutboundTime(Date sqlOutboundTime) {
		this.sqlOutboundTime = sqlOutboundTime;
	}

	public boolean isOverbookEnabled() {
		return isOverbookEnabled;
	}

	public void setOverbookEnabled(boolean isOverbookEnabled) {
		this.isOverbookEnabled = isOverbookEnabled;
	}

	public boolean isFlightWithinCutoff() {
		return isFlightWithinCutoff;
	}

	public void setFlightWithinCutoff(boolean isFlightWithinCutoff) {
		this.isFlightWithinCutoff = isFlightWithinCutoff;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public boolean isIgnoreStayOverTime() {
		return ignoreStayOverTime;
	}

	public void setIgnoreStayOverTime(boolean ignoreStayOverTime) {
		this.ignoreStayOverTime = ignoreStayOverTime;
	}

	public boolean isExcludeOnewayPublicFaresFromReturnJourneys() {
		return excludeOnewayPublicFaresFromReturnJourneys;
	}

	public void setExcludeOnewayPublicFaresFromReturnJourneys(boolean excludeOnewayPublicFaresFromReturnJourneys) {
		this.excludeOnewayPublicFaresFromReturnJourneys = excludeOnewayPublicFaresFromReturnJourneys;
	}

}
