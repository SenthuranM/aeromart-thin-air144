package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class FareTypeDTO implements Serializable {

	private static final long serialVersionUID = 4116803937081439506L;
	private boolean seatAvailable;
	private double adultFare;
	private double childFare;
	private double infantFare;

	public void setFare(FareSummaryDTO fare) {
		adultFare = fare.getFareAmount(PaxTypeTO.ADULT);
		childFare = fare.getFareAmount(PaxTypeTO.CHILD);
		infantFare = fare.getFareAmount(PaxTypeTO.INFANT);
	}

	public double getAdultFare() {
		return adultFare;
	}

	public double getTotalFare(int adults, int children, int infants) {
		return (adults * adultFare) + (children * childFare) + (infants * infantFare);
	}

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

}
