/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.dto.inventory.BCDetailDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * APIs for Booking Class maintenance related functionlities
 */
public interface BookingClassBD {

	public static final String SERVICE_NAME = "BookingClassService";

	/**
	 * Retrieves a specific BookingClass
	 * 
	 * @param bookingCode
	 * @return BookingClass
	 */
	public BookingClass getBookingClass(String bookingCode) throws ModuleException;

	public BookingClass getLightWeightBookingClass(String bookingCode) throws ModuleException;

	public BookingClassDTO getBookingClassDTO(String bookingCode) throws ModuleException;

	public Page getBookingClassDTOs(SearchBCsCriteria criteria) throws ModuleException;

	public Collection<String> getStandbyBookingClasses() throws ModuleException;

	public Collection<Object[]> getBookingCodesWithFlags(String cabinClassCode) throws ModuleException;

	public Collection<Object[]> getBookingCodesWithFlagsForLogicalCC(Collection<String> logicalCCCodes) throws ModuleException;

	/**
	 * Retrieve booking classes for specified bookingcodes
	 * 
	 * @param bookingCodes
	 * @return HashMap keyed by bookingCode
	 * @throws ModuleException
	 */
	public HashMap<String, BookingClass> getBookingClassesMap(Collection<String> bookingCodes) throws ModuleException;

	/**
	 * Persist a new booking class
	 * 
	 * @param bookingClassDTO
	 * @throws ModuleException
	 */
	public void createBookingClass(BookingClassDTO bookingClassDTO) throws ModuleException;

	/**
	 * Update existing booking class
	 * 
	 * @param bookingClassDTO
	 * @throws ModuleException
	 */
	public void updateBookingClass(BookingClassDTO bookingClassDTO) throws ModuleException;

	/**
	 * Delete a booking class from database
	 * 
	 * @param bookingClassCode
	 * @throws ModuleException
	 */
	public void deleteBookingClass(String bookingClassCode) throws ModuleException;

	/**
	 * Retrieve the booking class codes for given gds ids
	 * 
	 * @param colBookingCodes
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Collection<String>> getBookingClassCodes(Collection<Integer> gdsIds) throws ModuleException;

	/**
	 * Retrieve the booking class codes for given gds id
	 * 
	 * @param gdsId
	 * @return
	 * @throws ModuleException
	 */
	public Collection<String> getSingleCharBookingClassesForGDS(int gdsId) throws ModuleException;

	public String getCabinClassForBookingClass(String bookingClass) throws ModuleException;

	public List<BCDetailDTO> getBCsForGrouping() throws ModuleException;

	public void updateBCGroupIds(List<String> bcCodes, int groupId) throws ModuleException;

	public Object[] getMinimumPublicFare(int flightId, String segmentCode, String logicalCabinClassCode);
	
	public Collection<BookingClass> getBookingClasses();
	
}
