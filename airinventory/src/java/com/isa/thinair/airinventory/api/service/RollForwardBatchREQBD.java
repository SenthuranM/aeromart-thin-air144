/*/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airinventory.api.service;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQ;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.commons.api.exception.ModuleException;



/**
 * @author Raghu
 */
public interface RollForwardBatchREQBD {

	public static final String SERVICE_NAME = "RollForwardBatchREQService";
	
	public List<RollForwardBatchREQ> getRollForwardBatchREQs()throws ModuleException;

	public RollForwardBatchREQ getRollForwardBatchREQ(int id)throws ModuleException;

	public Collection<RollForwardBatchREQSEG> saveRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException;
	
	public void updateRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException;

	public void removeRollForwardBatchREQ(int batchId)throws ModuleException;

	public void removeRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ)throws ModuleException;
	
	public void updateRollForwardBatchREQStatus(Integer batchId, String status)throws ModuleException;
	
	public Integer doAdvanceRollForward(List<InvRollForwardCriteriaDTO> criteriaDTOList) throws ModuleException;

	public void doRollForwardLogic(InvRollForwardCriteriaDTO invRollForwardCriteriaDTO) throws ModuleException, ParseException;

}
