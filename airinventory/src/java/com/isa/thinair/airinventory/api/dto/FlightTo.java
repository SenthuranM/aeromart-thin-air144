package com.isa.thinair.airinventory.api.dto;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightTo implements Serializable {
	private String flightNumber;
	private Date departureDate;
	private List<FlightSegmentTo> flightSegments;

	public FlightTo() {
		flightSegments = new ArrayList<FlightSegmentTo>();
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public List<FlightSegmentTo> getFlightSegments() {
		return flightSegments;
	}

	public void addFlightSegment(FlightSegmentTo flightSegment) {
		this.flightSegments.add(flightSegment);
	}
}
