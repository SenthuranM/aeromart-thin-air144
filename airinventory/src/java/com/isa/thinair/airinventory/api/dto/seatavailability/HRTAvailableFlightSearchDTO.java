package com.isa.thinair.airinventory.api.dto.seatavailability;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

import java.util.Date;

public class HRTAvailableFlightSearchDTO extends AvailableFlightSearchDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date fQOBDepartureDate;
	private Date fQOBArrivalDate;
	private Date fQIBDepartureDate;
	private String fQOnd;
	private Date fQOBDepartureDateZulu;
	private Date fQIBDepartureDateZulu;
	private boolean ignoreStayOverTime;

	/**
	 * for OPEN_RETURN Search filter I/B fare with respective O/B selected minimum fare.
	 */
	private FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO;
	private boolean excludeOnewayPublicFaresFromReturnJourneys;

	public HRTAvailableFlightSearchDTO(AvailableFlightSearchDTO avlSearchDTO) {
		copyAvailSeachData(avlSearchDTO);
	}

	public AvailableFlightSearchDTO clone() {
		HRTAvailableFlightSearchDTO avlDTO = new HRTAvailableFlightSearchDTO(super.clone());
		avlDTO.setFQOBDepartureDate(this.fQOBDepartureDate);
		avlDTO.setFQOBArrivalDate(this.fQOBArrivalDate);
		avlDTO.setFQIBDepartureDate(this.fQIBDepartureDate);
		avlDTO.setFQOnd(this.fQOnd);
		avlDTO.setFQOBDepartureDateZulu(this.fQOBDepartureDateZulu);
		avlDTO.setFQIBDepartureDateZulu(this.fQIBDepartureDateZulu);

		return avlDTO;
	}

	// TODO try this with reflections
	private void copyAvailSeachData(AvailableFlightSearchDTO avlSearchDTO) {
		this.setAdultCount(avlSearchDTO.getAdultCount());
		this.setChildCount(avlSearchDTO.getChildCount());
		this.setAgentCode(avlSearchDTO.getAgentCode());
		this.setFixedAgentFareOnly(avlSearchDTO.isFixedAgentFareOnly());
		this.setGoshoFareAgentOnly(avlSearchDTO.isGoshoFareAgentOnly());
		this.setAvailabilityRestrictionLevel(avlSearchDTO.getAvailabilityRestrictionLevel());
		// this.setBookingClassCode(avlSearchDTO.getBookingClassCode());
		this.setChannelCode(avlSearchDTO.getChannelCode());
		this.setFlightsPerOndRestriction(avlSearchDTO.getFlightsPerOndRestriction());
		this.setInfantCount(avlSearchDTO.getInfantCount());
		this.setOndCode(avlSearchDTO.getOndCode());
		this.setPosAirport(avlSearchDTO.getPosAirport());
		// this.setReturnFlag(avlSearchDTO.isReturnFlag());
		// this.setStayOverMillis(avlSearchDTO.getStayOverMillis());
		// this.setBookingType(avlSearchDTO.getBookingType());
		this.setBookingPaxType(avlSearchDTO.getBookingPaxType());
		this.setFareCategoryType(avlSearchDTO.getFareCategoryType());
		this.setReturnValidityPeriodId(avlSearchDTO.getReturnValidityPeriodId());
		this.setConfirmOpenReturn(avlSearchDTO.isConfirmOpenReturn());

		// this.setFirstDepartureDateTimeZulu(getFirstDepartureDateTimeZulu());
		// this.setLastArrivalDateTimeZulu(getLastArrivalDateTimeZulu());

		this.setModifiedOndFareId(avlSearchDTO.getModifiedOndFareId());
		this.setModifiedOndPaxFareAmount(avlSearchDTO.getModifiedOndPaxFareAmount());
		this.setModifiedOndFareType(avlSearchDTO.getModifiedOndFareType());
		this.setEnforceFareCheckForModOnd(avlSearchDTO.getEnforceFareCheckForModOnd());
		this.setModifiedFlightSegments(avlSearchDTO.getModifiedFlightSegments());

		this.setHasInwardCxnFlight(isHasInwardCxnFlight());
		this.setHasOutwardCxnFlight(isHasOutwardCxnFlight());
		this.setHalfReturnFareQuote(avlSearchDTO.isHalfReturnFareQuote());
		// this.setModifiedOndReturnGroupId(avlSearchDTO.getModifiedOndReturnGroupId());
		this.setInboundFareQuote(avlSearchDTO.isInboundFareQuote());
		this.setInverseOndCode(avlSearchDTO.getInverseOndCode());
		this.setInverseFareID(avlSearchDTO.getInverseFareID());

		// Shallow copy
		// this.setInBoundFlights(avlSearchDTO.getInBoundFlights());
		// this.setOutBoundFlights(avlSearchDTO.getOutBoundFlights());
		// this.setExistingFlightSegments(avlSearchDTO.getExistingFlightSegments());
		// this.setModifiedFlightSegments(avlSearchDTO.getModifiedFlightSegments());
		this.setExternalBookingClasses(avlSearchDTO.getExternalBookingClasses());
		this.setExternalFareSummaryDTO(avlSearchDTO.getExternalFareSummaryDTO());
		this.setInverseSegmentsOfModifiedFlightSegment(avlSearchDTO.getInverseSegmentsOfModifiedFlightSegment());
		this.setOndQuoteFlexi(avlSearchDTO.getOndQuoteFlexi());
		this.setInwardCxnFlightArriTime(avlSearchDTO.getInwardCxnFlightArriTime());
		this.setOutwardCxnFlightDepTime(avlSearchDTO.getOutwardCxnFlightDepTime());

		this.setOriginDestionationInfoDTOs(avlSearchDTO.getOriginDestinationInfoDTOs());

		this.setSearchInitiatedFromInterline(avlSearchDTO.isSearchInitiatedFromInterline());
		this.setAppIndicator(avlSearchDTO.getAppIndicator());

		this.setExcludeConnectionFares(avlSearchDTO.isExcludeConnectionFares());
		this.setExcludeReturnFares(avlSearchDTO.isExcludeReturnFares());

		this.setFlightSegFareDetails(avlSearchDTO.getFlightSegFareDetails());
		// this.setCabinClassSelection(avlSearchDTO.getCabinClassSelection());
		// this.setLogicalCabinClassSelection(avlSearchDTO.getLogicalCabinClassSelection());
		this.setLastFareQuotedDate(avlSearchDTO.getLastFareQuotedDate());
		this.setFQWithinValidity(avlSearchDTO.isFQWithinValidity());
		this.setFQOnLastFQDate(avlSearchDTO.isFQOnLastFQDate());
		this.setFltSegWiseExtBookingClasses(avlSearchDTO.getFltSegWiseExtBookingClasses());
		this.setFltSegWiseExtCabinClasses(avlSearchDTO.getFltSegWiseExtCabinClasses());
		this.setFltSegWiseLastFQDates(avlSearchDTO.getFltSegWiseLastFQDates());
		this.setSearchAllBC(avlSearchDTO.isSearchAllBC());
		this.setSkipInvCheckForFlown(avlSearchDTO.isSkipInvCheckForFlown());
		this.setInterlineFareQuoted(avlSearchDTO.isInterlineFareQuoted());
		this.setRequoteFlightSearch(avlSearchDTO.isRequoteFlightSearch());
		this.setOndChargeInfo(avlSearchDTO.getOndChargeInfo());
		this.setSkipSameFareBucketQuote(avlSearchDTO.isSkipSameFareBucketQuote());
		this.setFromNameChange(avlSearchDTO.isFromNameChange());
		this.setPointOfSale(avlSearchDTO.getPointOfSale());
		this.setAllowDoOverbookAfterCutOffTime(avlSearchDTO.isAllowDoOverbookAfterCutOffTime());

		this.setQuoteReturnSegmentDiscount(avlSearchDTO.isQuoteReturnSegmentDiscount());
		this.setFareCalendarSearch(avlSearchDTO.isFareCalendarSearch());
		this.setPreserveOndOrder(avlSearchDTO.isPreserveOndOrder());
		this.setIgnoreStayOverTime(AppSysParamsUtil.showHalfReturnFaresInIBECalendar() && avlSearchDTO.isFareCalendarSearch()
				&& avlSearchDTO.isIncludeHRTFares());
	}

	public Date getFQOBDepartureDate() {
		return fQOBDepartureDate;
	}

	public void setFQOBDepartureDate(Date fQOBDepartureDate) {
		this.fQOBDepartureDate = fQOBDepartureDate;
	}

	public Date getFQOBArrivalDate() {
		return fQOBArrivalDate;
	}

	public void setFQOBArrivalDate(Date fQOBArrivalDate) {
		this.fQOBArrivalDate = fQOBArrivalDate;
	}

	public Date getFQIBDepartureDate() {
		return fQIBDepartureDate;
	}

	public void setFQIBDepartureDate(Date fQIBDepartureDate) {
		this.fQIBDepartureDate = fQIBDepartureDate;
	}

	public String getFQOnd() {
		return fQOnd;
	}

	public void setFQOnd(String fQOnd) {
		this.fQOnd = fQOnd;
	}

	public void setFQOBDepartureDateZulu(Date fQOBDepartureDateZulu) {
		this.fQOBDepartureDateZulu = fQOBDepartureDateZulu;
	}

	public Date getFQOBDepartureDateZulu() {
		return fQOBDepartureDateZulu;
	}

	public void setFQIBDepartureDateZulu(Date fQIBDepartureDateZulu) {
		this.fQIBDepartureDateZulu = fQIBDepartureDateZulu;
	}

	public Date getFQIBDepartureDateZulu() {
		return fQIBDepartureDateZulu;
	}

	public boolean isSameFlightSearched(int ondSequence, int fltSegId) {
		return getOndInfo(ondSequence).isExistingFlightSegment(fltSegId);
	}

	public boolean isSameCabinClassSearched(int ondSequence, String cabinClassCode) {
		if (getOndInfo(ondSequence).getPreferredClassOfService() != null) {
			return getOndInfo(ondSequence).getPreferredClassOfService().equals(cabinClassCode);
		}
		return false;
	}

	public boolean isFlownOnd(int ondSequence) {
		if (getOndInfo(ondSequence) != null) {
			return getOndInfo(ondSequence).isFlownOnd();
		}
		return false;
	}

	public boolean isUnTouchedOnd(int ondSequence) {
		if (getOndInfo(ondSequence) != null) {
			return getOndInfo(ondSequence).isUnTouchedOnd();
		}
		return false;
	}

	public FilteredBucketCheapestBCDTO getOutBoundCheapestBucketBCDTO() {
		return outBoundCheapestBucketBCDTO;
	}

	public void setOutBoundCheapestBucketBCDTO(FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO) {
		this.outBoundCheapestBucketBCDTO = outBoundCheapestBucketBCDTO;
	}

	public boolean isIgnoreStayOverTime() {
		return ignoreStayOverTime;
	}

	public void setIgnoreStayOverTime(boolean ignoreStayOverTime) {
		this.ignoreStayOverTime = ignoreStayOverTime;
	}

	public boolean isExcludeOnewayPublicFaresFromReturnJourneys() {
		return excludeOnewayPublicFaresFromReturnJourneys;
	}

	public void setExcludeOnewayPublicFaresFromReturnJourneys(boolean excludeOnewayPublicFaresFromReturnJourneys) {
		this.excludeOnewayPublicFaresFromReturnJourneys = excludeOnewayPublicFaresFromReturnJourneys;
	}

}
