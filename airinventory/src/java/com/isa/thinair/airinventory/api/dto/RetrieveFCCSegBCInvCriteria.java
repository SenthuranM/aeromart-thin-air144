package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class RetrieveFCCSegBCInvCriteria implements Serializable {

	private static final long serialVersionUID = 3611852382928600643L;

	private Integer fccsInvId;

	private Boolean isGDSAllocationsOnly;

	/**
	 * @return Returns the fccsInvId.
	 */
	public Integer getFccsInvId() {
		return fccsInvId;
	}

	/**
	 * @param fccsInvId
	 *            The fccsInvId to set.
	 */
	public void setFccsInvId(Integer fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	/**
	 * @return Returns the isGDSAllocationsOnly.
	 */
	public Boolean getIsGDSAllocationsOnly() {
		return isGDSAllocationsOnly;
	}

	/**
	 * @param isGDSAllocationsOnly
	 *            The isGDSAllocationsOnly to set.
	 */
	public void setIsGDSAllocationsOnly(Boolean isGDSAllocationsOnly) {
		this.isGDSAllocationsOnly = isGDSAllocationsOnly;
	}

}
