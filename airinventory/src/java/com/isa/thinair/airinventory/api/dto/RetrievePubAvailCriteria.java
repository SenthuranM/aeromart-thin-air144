package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventStatus;

public class RetrievePubAvailCriteria implements Serializable {

	private static final long serialVersionUID = -1338965506133927930L;

	private Integer segInvId;

	private Collection<PublishInventoryEventStatus> statusCollection;

	public Integer getSegInvId() {
		return segInvId;
	}

	public void setSegInvId(Integer segInvId) {
		this.segInvId = segInvId;
	}

	public Collection<PublishInventoryEventStatus> getStatusCollection() {
		return statusCollection;
	}

	public void setStatusCollection(Collection<PublishInventoryEventStatus> statusCollection) {
		this.statusCollection = statusCollection;
	}

}
