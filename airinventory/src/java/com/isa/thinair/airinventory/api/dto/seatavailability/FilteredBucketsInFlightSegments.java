package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.util.List;

/**
 * 
 * @author MN
 */
public class FilteredBucketsInFlightSegments {
	private List<FilteredBucketsDTO> filteredBuckets;
	private boolean excludeStandardBuckets = false;
	private boolean excludeNonStandardBuckets = false;
	private boolean excludeFixedBuckets = false;

	public boolean isExcludeFixedBuckets() {
		return excludeFixedBuckets;
	}

	public void setExcludeFixedBuckets(boolean excludeFixedBuckets) {
		this.excludeFixedBuckets = excludeFixedBuckets;
	}

	public boolean isExcludeNonStandardBuckets() {
		return excludeNonStandardBuckets;
	}

	public void setExcludeNonStandardBuckets(boolean excludeNonStandardBuckets) {
		this.excludeNonStandardBuckets = excludeNonStandardBuckets;
	}

	public boolean isExcludeStandardBuckets() {
		return excludeStandardBuckets;
	}

	public void setExcludeStandardBuckets(boolean excludeStandardBuckets) {
		this.excludeStandardBuckets = excludeStandardBuckets;
	}

	public List<FilteredBucketsDTO> getFilteredBuckets() {
		return filteredBuckets;
	}

	public void setFilteredBuckets(List<FilteredBucketsDTO> filteredBuckets) {
		this.filteredBuckets = filteredBuckets;
	}
}
