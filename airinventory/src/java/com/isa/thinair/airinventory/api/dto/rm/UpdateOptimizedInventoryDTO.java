package com.isa.thinair.airinventory.api.dto.rm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Sent From RM to update the Seat Inventory Accordingly UpdateOptimizedInventoryDTO
 * 
 * @author byorn
 * 
 */
public class UpdateOptimizedInventoryDTO implements Serializable {

	private static final long serialVersionUID = 3240472564905268189L;

	private String flightNumber;

	private String segmentCode;

	private Date depatureDate;

	private String cabinClass;

	private Date messageDate;

	/** Holds the collection of Booking Classes in which seats need to be optimized **/
	private Collection<InventorySeatMovementDTO> inventorySeatMovements;

	/** @see: Constants **/
	private String processingStatus;

	private long optimisationHeaderId;

	private Collection<String> bcsInSeatMovemants;

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the depatureDate
	 */
	public Date getDepatureDate() {
		return depatureDate;
	}

	/**
	 * @param depatureDate
	 *            the depatureDate to set
	 */
	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the inventorySeatMovements
	 */
	public Collection<InventorySeatMovementDTO> getInventorySeatMovements() {
		return inventorySeatMovements;
	}

	/**
	 * @param inventorySeatMovements
	 *            the inventorySeatMovements to set
	 */
	public void setInventorySeatMovements(Collection<InventorySeatMovementDTO> inventorySeatMovements) {
		this.inventorySeatMovements = inventorySeatMovements;
	}

	/**
	 * @return the messageDate
	 */
	public Date getMessageDate() {
		return messageDate;
	}

	/**
	 * @param messageDate
	 *            the messageDate to set
	 */
	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	/**
	 * @return the optimisationHeaderId
	 */
	public long getOptimisationHeaderId() {
		return optimisationHeaderId;
	}

	/**
	 * @param optimisationHeaderId
	 *            the optimisationHeaderId to set
	 */
	public void setOptimisationHeaderId(long optimisationHeaderId) {
		this.optimisationHeaderId = optimisationHeaderId;
	}

	/**
	 * @return the processingStatus
	 */
	public String getProcessingStatus() {
		return processingStatus;
	}

	/**
	 * @param processingStatus
	 *            the processingStatus to set
	 */
	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the bcsInSeatMovemants
	 */
	public Collection<String> getBcsInSeatMovemants() {
		if (this.bcsInSeatMovemants == null) {
			this.bcsInSeatMovemants = new ArrayList<String>();
		}

		Collection<InventorySeatMovementDTO> inventorySeatMovementDTOs = getInventorySeatMovements();
		for (InventorySeatMovementDTO inventorySeatMovementDTO : inventorySeatMovementDTOs) {
			this.bcsInSeatMovemants.add(inventorySeatMovementDTO.getFromBookingClass());
			this.bcsInSeatMovemants.add(inventorySeatMovementDTO.getToBookingClass());

		}
		return bcsInSeatMovemants;
	}

}
