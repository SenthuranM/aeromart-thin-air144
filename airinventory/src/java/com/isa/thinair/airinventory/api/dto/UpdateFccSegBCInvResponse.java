package com.isa.thinair.airinventory.api.dto;

import java.util.Collection;

public class UpdateFccSegBCInvResponse {

	/** Number of records updated */
	private int updateCount;

	/** FCCSegBCInventory identifier on which seat sold/onhold */
	private Integer fccsbInvId;

	/** FCCSegBCInventoryTOs of the buckets get closed */
	private Collection<FCCSegBCInventoryTO> closedFccsbInvs;

	/**
	 * @return Returns the updateCount.
	 */
	public int getUpdateCount() {
		return updateCount;
	}

	/**
	 * @param updateCount
	 *            The updateCount to set.
	 */
	public void setUpdateCount(int updateCount) {
		this.updateCount = updateCount;
	}

	/**
	 * @return Returns the closedFccsbInvs.
	 */
	public Collection<FCCSegBCInventoryTO> getClosedFccsbInvs() {
		return closedFccsbInvs;
	}

	/**
	 * @param closedFccsbInvs
	 *            The closedFccsbInvs to set.
	 */
	public void setClosedFccsbInvs(Collection<FCCSegBCInventoryTO> closedFccsbInvs) {
		this.closedFccsbInvs = closedFccsbInvs;
	}

	/**
	 * @return Returns the fccsbInvId.
	 */
	public Integer getFccsbInvId() {
		return fccsbInvId;
	}

	/**
	 * @param fccsbInvId
	 *            The fccsbInvId to set.
	 */
	public void setFccsbInvId(Integer fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}
}
