package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

public class LightBCAvailability implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String bookingCode;
	private int availability;
	private int nestedAvailability;

	public LightBCAvailability(String bookingCode, int availability, int nestedAvailability) {
		this.bookingCode = bookingCode;
		this.availability = availability;
		this.nestedAvailability = nestedAvailability;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public int getAvailability() {
		return availability;
	}

	public void setAvailability(int availability) {
		this.availability = availability;
	}

	public int getNestedAvailability() {
		return nestedAvailability;
	}

	public void setNestedAvailability(int nestedAvailability) {
		this.nestedAvailability = nestedAvailability;
	}

}
