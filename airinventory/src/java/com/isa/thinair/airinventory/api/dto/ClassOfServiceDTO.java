package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class ClassOfServiceDTO implements Serializable {

	private static final long serialVersionUID = -2347721030007719795L;

	private String cabinClassCode;
	private String logicalCCCode;
	private String segmentCode;

	public ClassOfServiceDTO() {

	}

	public ClassOfServiceDTO(String cabinClassCode, String logicalCCCode, String segmentCode) {
		this.cabinClassCode = cabinClassCode;
		this.logicalCCCode = logicalCCCode;
		this.segmentCode = segmentCode;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

}
