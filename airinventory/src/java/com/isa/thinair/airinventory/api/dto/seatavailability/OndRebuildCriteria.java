package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * Criteria for reliably rebuild the Collection<OndFareDTO>
 * 
 * @author mekanayake
 * 
 */
public class OndRebuildCriteria implements Serializable {

	private static final long serialVersionUID = -2305839175198571807L;

	/** Total fare of the journey . For charge calculation */
	private Map<String, Double> totalJourneyFare;

	/** pax count */
	private int totlaAdults;
	private int totalChildrens;
	private int totalInfants;

	private boolean hasBlockSeats;

	private String bookingType;

	private Collection<OnDFareSegChargesRebuildCriteria> ondCriteria;

	private Map<String, Double> totalSurcharges;

	private boolean ignoreFareValueDecimals;

	private Map<Integer, Map<String, Double>> totalSurChargeByJourneyMap;

	private boolean allowOverbookAfterCutoffTime;

	/**
	 * @return the totalJourneyFare
	 */
	public Map<String, Double> getTotalJourneyFare() {
		return totalJourneyFare;
	}

	/**
	 * @param totalJourneyFare
	 *            the totalJourneyFare to set
	 */
	public void setTotalJourneyFare(Map<String, Double> totalJourneyFare) {
		this.totalJourneyFare = totalJourneyFare;
	}

	/**
	 * @return the totlaAdults
	 */
	public int getTotlaAdults() {
		return totlaAdults;
	}

	/**
	 * @param totlaAdults
	 *            the totlaAdults to set
	 */
	public void setTotlaAdults(int totlaAdults) {
		this.totlaAdults = totlaAdults;
	}

	/**
	 * @return the totalChildrens
	 */
	public int getTotalChildrens() {
		return totalChildrens;
	}

	/**
	 * @param totalChildrens
	 *            the totalChildrens to set
	 */
	public void setTotalChildrens(int totalChildrens) {
		this.totalChildrens = totalChildrens;
	}

	/**
	 * @return the totalInfants
	 */
	public int getTotalInfants() {
		return totalInfants;
	}

	/**
	 * @param totalInfants
	 *            the totalInfants to set
	 */
	public void setTotalInfants(int totalInfants) {
		this.totalInfants = totalInfants;
	}

	/**
	 * @return the isBlockSeats
	 */
	public boolean hasBlockSeats() {
		return hasBlockSeats;
	}

	/**
	 * @param hasBlockSeats
	 *            the isBlockSeats to set
	 */
	public void setBlockSeats(boolean hasBlockSeats) {
		this.hasBlockSeats = hasBlockSeats;
	}

	/**
	 * @return the ondCriteria
	 */
	public Collection<OnDFareSegChargesRebuildCriteria> getOndCriteria() {
		if (ondCriteria == null) {
			ondCriteria = new ArrayList<OnDFareSegChargesRebuildCriteria>();
		}
		return ondCriteria;
	}

	/**
	 * @param ondCriteria
	 *            the ondCriteria to set
	 */
	public void setOndCriteria(Collection<OnDFareSegChargesRebuildCriteria> ondCriteria) {
		this.ondCriteria = ondCriteria;
	}

	public void addOndCriteria(OnDFareSegChargesRebuildCriteria ondCriteria) {
		getOndCriteria().add(ondCriteria);
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public void setTotalSurchage(Map<String, Double> totalSurcharges) {
		this.totalSurcharges = totalSurcharges;
	}

	public Map<String, Double> getTotalSurcharge() {
		return totalSurcharges;
	}

	public boolean isIgnoreFareValueDecimals() {
		return ignoreFareValueDecimals;
	}

	public void setIgnoreFareValueDecimals(boolean ignoreFareValueDecimals) {
		this.ignoreFareValueDecimals = ignoreFareValueDecimals;
	}

	public Map<Integer, Map<String, Double>> getTotalSurChargeByJourneyMap() {
		if (totalSurChargeByJourneyMap == null) {
			totalSurChargeByJourneyMap = new HashMap<Integer, Map<String, Double>>();
		}

		return totalSurChargeByJourneyMap;
	}

	public void addTotalSurChargeByJourneyMap(Integer subJourney, Map<String, Double> surChargeMap) {
		getTotalSurChargeByJourneyMap().put(subJourney, surChargeMap);
	}

	public Map<String, Double> getSubJourneyTotalSurCharge(Integer subJourney) {
		if (subJourney != null) {
			if (getTotalSurChargeByJourneyMap().get(subJourney) != null) {
				return getTotalSurChargeByJourneyMap().get(subJourney);
			}

			Map<String, Double> surChargeMap = new HashMap<String, Double>();
			surChargeMap.put(PaxTypeTO.ADULT, new Double(0));
			surChargeMap.put(PaxTypeTO.CHILD, new Double(0));
			surChargeMap.put(PaxTypeTO.INFANT, new Double(0));

			return surChargeMap;
		} else {
			return getTotalSurcharge();
		}
	}

	public int getOndFareSegCount(int ondSequence) {
		int fareSegOndCount = 0;
		if (!getOndCriteria().isEmpty()) {
			for (OnDFareSegChargesRebuildCriteria ondFareCriteria : getOndCriteria()) {
				if (ondFareCriteria.getOndSequence() == ondSequence && ondFareCriteria.getBundledFareDTO() != null) {
					fareSegOndCount += ondFareCriteria.getFlightSegIds().size();
				}
			}
		} else {
			fareSegOndCount = 1;
		}
		return fareSegOndCount;
	}

	public boolean isAllowOverbookAfterCutoffTime() {
		return allowOverbookAfterCutoffTime;
	}

	public void setAllowOverbookAfterCutoffTime(boolean allowOverbookAfterCutoffTime) {
		this.allowOverbookAfterCutoffTime = allowOverbookAfterCutoffTime;
	}
}
