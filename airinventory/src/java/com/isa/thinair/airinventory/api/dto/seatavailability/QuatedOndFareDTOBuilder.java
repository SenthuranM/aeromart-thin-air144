package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.SegBCAllocDTOBuilder.DIRECTION;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class QuatedOndFareDTOBuilder implements Serializable {

	private static final long serialVersionUID = 1919374142839493428L;

	private OndFareDTO ondFareDTO = new OndFareDTO();

	private Integer tqofId;

	private Integer fareID;

	private Integer farePercentage;

	private String blockSeatUID;

	private Collection<SegBCAllocDTOBuilder> tempSegBCAllocDTOs;

	/**
	 * @return the tqofId
	 */
	public Integer getTqofId() {
		return tqofId;
	}

	/**
	 * @param tqofId
	 *            the tqofId to set
	 */
	public void setTqofId(Integer tqofId) {
		this.tqofId = tqofId;
	}

	/**
	 * @return the fareID
	 */
	public Integer getFareID() {
		return fareID;
	}

	/**
	 * @param fareID
	 *            the fareID to set
	 */
	public void setFareID(Integer fareID) {
		this.fareID = fareID;
	}

	/**
	 * @return the farePercentage
	 */
	public Integer getFarePercentage() {
		return farePercentage;
	}

	public BigDecimal getEffectiveFareRatio() {
		return AccelAeroCalculator.divide(BigDecimal.valueOf(getFarePercentage()), BigDecimal.valueOf(100));
	}

	/**
	 * @param farePercentage
	 *            the farePercentage to set
	 */
	public void setFarePercentage(Integer farePercentage) {
		this.farePercentage = farePercentage;
	}

	/**
	 * @return the ondFareType
	 */
	public Integer getOndFareType() {
		return this.ondFareDTO.getFareType();
	}

	/**
	 * @param ondFareType
	 *            the ondFareType to set
	 */
	public void setOndFareType(Integer ondFareType) {
		this.ondFareDTO.setFareType(ondFareType);
	}

	/**
	 * @return the blockSeatUID
	 */
	public String getBlockSeatUID() {
		return blockSeatUID;
	}

	/**
	 * @param blockSeatUID
	 *            the blockSeatUID to set
	 */
	public void setBlockSeatUID(String blockSeatUID) {
		this.blockSeatUID = blockSeatUID;
	}

	/**
	 * @return the tempSegBCAllocDTOs
	 */
	public Collection<SegBCAllocDTOBuilder> getTempSegBCAllocDTOs() {
		if (tempSegBCAllocDTOs == null) {
			tempSegBCAllocDTOs = new ArrayList<SegBCAllocDTOBuilder>();
		}
		return tempSegBCAllocDTOs;
	}

	/**
	 * @param tempSegBCAllocDTOs
	 *            the tempSegBCAllocDTOs to set
	 */
	public void setTempSegBCAllocDTOs(Collection<SegBCAllocDTOBuilder> tempSegBCAllocDTOs) {
		this.tempSegBCAllocDTOs = tempSegBCAllocDTOs;
	}

	/**
	 * @return the segmentsMap
	 */
	public LinkedHashMap<Integer, FlightSegmentDTO> getSegmentsMap() {
		return this.ondFareDTO.getSegmentsMap();
	}

	/**
	 * @param segmentsMap
	 *            the segmentsMap to set
	 */
	public void setSegmentsMap(LinkedHashMap<Integer, FlightSegmentDTO> segmentsMap) {
		this.ondFareDTO.addSegment(segmentsMap);
	}

	public void addSegment(Integer segmentId, FlightSegmentDTO flightSegmentDTO) {
		this.ondFareDTO.addSegment(segmentId, flightSegmentDTO);
	}

	/**
	 * Setting the open return flag. This will true only if a journey been a open return and direction id inbound
	 * 
	 * @param isOpenReturn
	 * @throws ModuleException
	 */
	public void setOpenReturn(boolean isOpenReturn) throws ModuleException {
		if (isInboundOnd()) {
			this.ondFareDTO.setOpenReturn(isOpenReturn);
		}
	}

	/**
	 * @return the fareSummaryDTO
	 */
	public FareSummaryDTO getFareSummaryDTO() {
		return ondFareDTO.getFareSummaryDTO();
	}

	public void populateFareSummaryDTO(Map<Integer,FareTO> faresTOs) throws ModuleException {
		FareTO fareTO = (FareTO) faresTOs.get(this.fareID);

		if (fareTO == null) {
			throw new ModuleException("no.fare.for.given.fare.id");
		}

		// adjust the adult fare amount per OND.
		BigDecimal anadjustedAdultFare = fareTO.getAdultFareAmount();
		fareTO.setAdultFareAmount(AccelAeroCalculator.multiply(anadjustedAdultFare, getEffectiveFareRatio()));
		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO(fareTO);
		fareSummaryDTO.setFarePercentage(this.farePercentage);
		this.ondFareDTO.setFareSummaryDTO(fareSummaryDTO);
	}

	public String getOndCode() throws ModuleException {
		return this.ondFareDTO.getOndCode();
	}

	public boolean isInboundOnd() throws ModuleException {
		if (this.tempSegBCAllocDTOs != null && !this.tempSegBCAllocDTOs.isEmpty()) {
			DIRECTION direction = this.tempSegBCAllocDTOs.iterator().next().getDirection();
			return direction.isInbound();
		} else {
			throw new ModuleException("no.TempSegBCAlloc.included");
		}
	}

	/**
	 * This will construct a OndFareDTO with following 1) Fare 2) segmentSeatDistsDTOs 3) segmentsMap 4) inbound flag 5)
	 * open return flag NOTE : The OndFareDTO will not contain the charges of flexiChargers
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public OndFareDTO buildOndFareDTO() throws ModuleException {
		ondFareDTO.setInBoundOnd(isInboundOnd());
		ondFareDTO.setSegmentSeatDistsDTOs(createSegmentSeatDistsDTOs());
		return this.ondFareDTO;
	}

	private Collection<SegmentSeatDistsDTO> createSegmentSeatDistsDTOs() {
		Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = new ArrayList<SegmentSeatDistsDTO>();
		if (tempSegBCAllocDTOs != null && !tempSegBCAllocDTOs.isEmpty()) {
			for (SegBCAllocDTOBuilder tempSegBCAllocBuilder : tempSegBCAllocDTOs) {
				SegmentSeatDistsDTO dto = tempSegBCAllocBuilder.buildSegmentSeatDistsDTO();
				segmentSeatDistsDTOs.add(dto);
			}
		}
		return segmentSeatDistsDTOs;
	}

}
