package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;

public class InvDowngradeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int fccsbInvId;

	private String bookingClass;

	private int totalFixedSeatsInSegment;

	private int totalAllocatedSeatsInSegment;

	private String cabinClass;

	private int soldSeats;

	private int onholdSeats;

	private int soldInfant;

	private int onholdInfant;

	private int infantAllocation;

	public int getFccsbInvId() {
		return fccsbInvId;
	}

	public void setFccsbInvId(int fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public int getTotalFixedSeatsInSegment() {
		return totalFixedSeatsInSegment;
	}

	public void setTotalFixedSeatsInSegment(int totalFixedSeatsInSegment) {
		this.totalFixedSeatsInSegment = totalFixedSeatsInSegment;
	}

	public int getTotalAllocatedSeatsInSegment() {
		return totalAllocatedSeatsInSegment;
	}

	public void setTotalAllocatedSeatsInSegment(int totalAllocatedSeatsInSegment) {
		this.totalAllocatedSeatsInSegment = totalAllocatedSeatsInSegment;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public int getSoldInfant() {
		return soldInfant;
	}

	public void setSoldInfant(int soldInfant) {
		this.soldInfant = soldInfant;
	}

	public int getOnholdInfant() {
		return onholdInfant;
	}

	public void setOnholdInfant(int onholdInfant) {
		this.onholdInfant = onholdInfant;
	}

	public int getInfantAllocation() {
		return infantAllocation;
	}

	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

}
