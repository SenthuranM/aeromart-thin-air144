package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * DTO for capturing flights for seat availability search
 * 
 * @author MN
 */
public class AvailableFlightDTO implements Serializable {

	private static final long serialVersionUID = -7387641354320359496L;

	private final Map<Integer, List<AvailableIBOBFlightSegment>> availableOndFlights = new HashMap<Integer, List<AvailableIBOBFlightSegment>>();

	private SelectedFlightDTO selectedFlight;

	private Date calculatedOpenReturnDate;

	private boolean onHoldEnabled;

	private String bookingType;

	public List<AvailableIBOBFlightSegment> getAvailableOndFlights(Integer ondSequence) {
		if (!availableOndFlights.containsKey(ondSequence)) {
			availableOndFlights.put(ondSequence, new ArrayList<AvailableIBOBFlightSegment>());
		}
		return availableOndFlights.get(ondSequence);
	}

	public void setAvailableOndFlights(Integer ondSequence, List<AvailableIBOBFlightSegment> availableFlightSegments) {
		availableOndFlights.put(ondSequence, availableFlightSegments);
	}

	public Set<Integer> getOndSequenceIds() {
		return availableOndFlights.keySet();
	}

	public Collection<AvailableIBOBFlightSegment> getAllAvailableOndFlights() {
		Collection<AvailableIBOBFlightSegment> availableAllOndFlights = new ArrayList<AvailableIBOBFlightSegment>();
		for (Collection<AvailableIBOBFlightSegment> availableFlights : availableOndFlights.values()) {
			availableAllOndFlights.addAll(availableFlights);
		}
		return availableAllOndFlights;
	}

	public SelectedFlightDTO getSelectedFlight() {
		return selectedFlight;
	}

	public void setSelectedFlight(SelectedFlightDTO selectedFlight) {
		this.selectedFlight = selectedFlight;
	}

	/**
	 * @return the calculatedOpenReturnDate
	 */
	public Date getCalculatedOpenReturnDate() {
		return calculatedOpenReturnDate;
	}

	/**
	 * @param calculatedOpenReturnDate
	 *            the calculatedOpenReturnDate to set
	 */
	public void setCalculatedOpenReturnDate(Date calculatedOpenReturnDate) {
		this.calculatedOpenReturnDate = calculatedOpenReturnDate;
	}

	/**
	 * @return the onHoldEnabled
	 */
	@Deprecated
	public boolean isOnHoldEnabled() {
		return onHoldEnabled;
	}

	/**
	 * @param onHoldEnabled
	 *            the onHoldEnabled to set
	 */
	@Deprecated
	public void setOnHoldEnabled(boolean onHoldEnabled) {
		this.onHoldEnabled = onHoldEnabled;
	}

	/**
	 * @return the bookingType
	 */
	@Deprecated
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	@Deprecated
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
}
