package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * Represents a connected flight having two or more flights
 * 
 * @author Nasly
 */
public class AvailableConnectedFlight extends AvailableIBOBFlightSegment {

	private static final long serialVersionUID = 6137767139082518436L;

	private final Map<String, Boolean> effectiveFaresSet = new HashMap<String, Boolean>();

	// If the connected flight comprise of flights flying the segments A1/A2, A2/A3,...,A(n-1)/An
	// then the ondCode represents A1/A2/A3.../A(n-1)/An - i.e. origin and destination along
	// with via points
	private String ondCode = null;

	private List<AvailableFlightSegment> availableFlightSegments = new ArrayList<AvailableFlightSegment>();

	public List<AvailableFlightSegment> getAvailableFlightSegments() {
		return availableFlightSegments;
	}

	public void setAvailableFlightSegments(List<AvailableFlightSegment> availableFlightSegments) {
		this.availableFlightSegments = availableFlightSegments;
	}

	public void addAvailableFlightSegment(AvailableFlightSegment availableFlightSegment) {
		this.availableFlightSegments.add(availableFlightSegment);
	}

	@Override
	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	@Override
	public void setSelectedFlightDTO(SelectedFlightDTO selectedFlightDTO) {
		this.selectedFlightDTO = selectedFlightDTO;
		Collection<AvailableFlightSegment> segmentsForConnection = this.getAvailableFlightSegments();
		if (segmentsForConnection != null && !segmentsForConnection.isEmpty()) {
			for (AvailableFlightSegment availableFlightSegment : segmentsForConnection) {
				availableFlightSegment.setSelectedFlightDTO(selectedFlightDTO);
			}
		}
	}

	// ---------------------------------------------------------------------------
	// utility methods for the caller
	// ---------------------------------------------------------------------------

	private boolean isSameFareType(Integer firstFareType, Integer secondFareType) {
		if (firstFareType == 0 && secondFareType != null) {
			return true;
		}
		if (firstFareType.equals(secondFareType)) {
			return true;
		}
		return false;
	}

	@Override
	public Integer getFareType(String logicalCCtype) {

		if (super.getFareType(logicalCCtype) == null || super.getFareType(logicalCCtype) == 0) {
			Integer fareType = 0;
			for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
				if (availableFlightSegment.isSeatsAvailable(logicalCCtype)
						&& isSameFareType(fareType, availableFlightSegment.getFareType(logicalCCtype))) {
					fareType = availableFlightSegment.getFareType(logicalCCtype);
				}
			}
			setFareType(logicalCCtype, fareType);
		}
		return super.getFareType(logicalCCtype);
	}

	@Override
	public Double getFareAmount(String logicalCCtype, String sPaxTypeTO) {
		if (isSeatsAvailable(logicalCCtype)) {
			if (getFareType(logicalCCtype) == FareTypes.OND_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				return getFare(logicalCCtype).getFareAmount(sPaxTypeTO);
			} else if (getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE) {
				double amount = 0;
				Iterator<AvailableFlightSegment> flightSegmentsIt = getAvailableFlightSegments().iterator();
				while (flightSegmentsIt.hasNext()) {
					AvailableFlightSegment availableFlightSegment = flightSegmentsIt.next();
					Double fareAmount = availableFlightSegment.getFareAmount(logicalCCtype, sPaxTypeTO);
					if (fareAmount != null) {
						amount += fareAmount;
					}
				}
				return amount;
			}
		}
		// decide whether to throw an exception
		return null;
	}

	@Override
	public List<Double> getTotalCharges(String logicalCCtype) {
		if (isSeatsAvailable(logicalCCtype) && this.totalCharges.get(logicalCCtype) == null) {

			if (getFareType(logicalCCtype) == FareTypes.OND_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				setEffectiveFares();
				if (getCharges(logicalCCtype) != null) {
					this.totalCharges.put(logicalCCtype,
							ChargeQuoteUtils.calculateTotalCharges(getEffectiveCharges(logicalCCtype)));
					return this.totalCharges.get(logicalCCtype);
				} else {
					// for oneway availability search, charge quote is not done
					List<Double> defaultArray = new ArrayList<Double>();
					defaultArray.add(new Double(0));
					defaultArray.add(new Double(0));
					defaultArray.add(new Double(0));
					this.totalCharges.put(logicalCCtype, defaultArray);
					return this.totalCharges.get(logicalCCtype);
				}
			} else if (getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE) {
				double adultCharges = 0;
				double infantCharges = 0;
				double childCharges = 0;
				Iterator<AvailableFlightSegment> flightSegmentsIt = getAvailableFlightSegments().iterator();
				while (flightSegmentsIt.hasNext()) {
					AvailableFlightSegment availableFlightSegment = flightSegmentsIt.next();
					availableFlightSegment.setAvailableConnectedFlight(this);
					List<Double> chargesForFlightSeg = availableFlightSegment.getTotalCharges(logicalCCtype);
					if (chargesForFlightSeg != null) {
						adultCharges += availableFlightSegment.getTotalCharges(logicalCCtype).get(0);
						infantCharges += availableFlightSegment.getTotalCharges(logicalCCtype).get(1);
						childCharges += availableFlightSegment.getTotalCharges(logicalCCtype).get(2);
					} else {
						// for oneway availability search, charge quote is not done
						List<Double> defaultArray = new ArrayList<Double>();
						defaultArray.add(new Double(0));
						defaultArray.add(new Double(0));
						defaultArray.add(new Double(0));
						this.totalCharges.put(logicalCCtype, defaultArray);
						return this.totalCharges.get(logicalCCtype);
					}
				}
				List<Double> defaultArray = new ArrayList<Double>();
				defaultArray.add(adultCharges);
				defaultArray.add(infantCharges);
				defaultArray.add(childCharges);
				this.totalCharges.put(logicalCCtype, defaultArray);
				return this.totalCharges.get(logicalCCtype);
			}
		}
		return this.totalCharges.get(logicalCCtype);
	}

	private Collection<QuotedChargeDTO> getEffectiveCharges(String logicalCCtype) {
		if (super.geteffectiveCharges(logicalCCtype) == null && getFare(logicalCCtype) != null) {
			super.seteffectiveCharges(logicalCCtype, getEffectiveCharges(logicalCCtype, false));
		}
		return super.geteffectiveCharges(logicalCCtype);
	}

	private Collection<QuotedChargeDTO> getEffectiveCharges(String logicalCCtype, boolean excludeTotalSurcharge) {
		// pass total journey fare for charge calculation
		Map<String, Double> totalJourneyFare = null;
		Map<String, Double> totalSurcharge = null;
		if (this.getSelectedFlightDTO() != null && this.getSelectedFlightDTO().isSeatsAvailable()) {
			totalJourneyFare = this.getSelectedFlightDTO().getTotalFareMap();
		} else {
			totalJourneyFare = this.getTotalFareMap(logicalCCtype);
		}
		if (!excludeTotalSurcharge) {
			if (this.getSelectedFlightDTO() != null) {
				totalSurcharge = getSelectedFlightDTO().getTotalSurcharge();
			} else {
				totalSurcharge = getTotalSurcharge(logicalCCtype);
			}
		}
		AvailableFlightSegment availableFlightSegment = getAvailableFlightSegments().iterator().next();
		return ChargeQuoteUtils.getChargesWithEffectiveChargeValues(getCharges(logicalCCtype), getFare(logicalCCtype),
				availableFlightSegment.getRequestedNoOfAdults(), availableFlightSegment.getRequestedNoOfChildren(),
				totalJourneyFare, totalSurcharge);
	}

	@Override
	public Collection<OndFareDTO> getFareChargesSeatsSegmentsDTOs(String logicalCCtype) {
		if (isSeatsAvailable(logicalCCtype) && super.getFareChargesSeatsSegmentsDTOs(logicalCCtype) == null) {
			Collection<OndFareDTO> fareChargesSeatsSegments = new ArrayList<OndFareDTO>();
			if (getFareType(logicalCCtype) == FareTypes.OND_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				setEffectiveFares();
				OndFareDTO fareChargesSeatsSegmentsDTO = new OndFareDTO();
				FareSummaryDTO fareSummaryDTO = this.getFare(logicalCCtype).clone();
				injectFarePercentage(logicalCCtype, fareSummaryDTO);
				fareChargesSeatsSegmentsDTO.setFareSummaryDTO(fareSummaryDTO);
				fareChargesSeatsSegmentsDTO.setFareType(getFareType(logicalCCtype));
				fareChargesSeatsSegmentsDTO.setAllCharges(getEffectiveCharges(logicalCCtype));
				fareChargesSeatsSegmentsDTO.setInBoundOnd(isInboundFlightSegment());
				fareChargesSeatsSegmentsDTO.setOndSequence(this.getOndSequence());
				if (this.getRequestedOndSequence() != null) {
					fareChargesSeatsSegmentsDTO.setRequestedOndSequence(this.getRequestedOndSequence());
				} else {
					fareChargesSeatsSegmentsDTO.setRequestedOndSequence(this.getOndSequence());
				}
				fareChargesSeatsSegmentsDTO
						.setOpenReturn(this.getSelectedFlightDTO() != null ? this.getSelectedFlightDTO().isOpenReturn() : false);

				for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
					fareChargesSeatsSegmentsDTO.addSegment(availableFlightSegment.getFlightSegmentDTO().getSegmentId(),
							availableFlightSegment.getFlightSegmentDTO());
				}
				fareChargesSeatsSegmentsDTO.setSegmentSeatDistsDTOs(
						AirinventoryUtils.getSearializableValues(this.getSegmentSeatDistributionMap(logicalCCtype)));
				fareChargesSeatsSegmentsDTO.setAllFlexiCharges(getEffectiveFlexiCharges(logicalCCtype));
				fareChargesSeatsSegmentsDTO.setSubJourneyGroup(this.getSubJourneyGroup());
				fareChargesSeatsSegments.add(fareChargesSeatsSegmentsDTO);
			} else if (getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE) {
				for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
					availableFlightSegment.setAvailableConnectedFlight(this);
					availableFlightSegment.setSubJourneyGroup(this.getSubJourneyGroup());
					if (availableFlightSegment.getFareChargesSeatsSegmentsDTOs(logicalCCtype) != null) {
						fareChargesSeatsSegments.addAll(availableFlightSegment.getFareChargesSeatsSegmentsDTOs(logicalCCtype));
					}
				}
			}
			setFareChargesSeatsSegmentsDTOs(logicalCCtype, fareChargesSeatsSegments);
		}
		return super.getFareChargesSeatsSegmentsDTOs(logicalCCtype);
	}

	@Override
	public LinkedHashMap<Integer, SegmentSeatDistsDTO> getSegmentSeatDistributionMap(String logicalCCtype) {
		if (isSeatsAvailable(logicalCCtype) && segmentSeatDistributionMap.get(logicalCCtype) == null) {
			segmentSeatDistributionMap.put(logicalCCtype, new LinkedHashMap<Integer, SegmentSeatDistsDTO>());
			for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
				Integer segmentId = availableFlightSegment.getFlightSegmentDTO().getSegmentId();
				SegmentSeatDistsDTO segmentSeatDistsDTO = availableFlightSegment.getSegmentSeatDistributionDTO(logicalCCtype,
						segmentId);
				if (segmentSeatDistsDTO != null) {
					segmentSeatDistributionMap.get(logicalCCtype).put(segmentId, segmentSeatDistsDTO);
				}
			}
		}
		return this.segmentSeatDistributionMap.get(logicalCCtype);
	}

	private void setEffectiveFares(String logicalCCtype) {
		if (getFareType(logicalCCtype) == FareTypes.OND_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
			double fltSegFareAmount = getFareAmount(logicalCCtype, PaxTypeTO.ADULT).doubleValue()
					* (1f / getAvailableFlightSegments().size());
			double fltSegFareAmountForChild = getFareAmount(logicalCCtype, PaxTypeTO.CHILD).doubleValue()
					* (1f / getAvailableFlightSegments().size());
			double fltSegFareAmountForInfant = getFareAmount(logicalCCtype, PaxTypeTO.INFANT).doubleValue()
					* (1f / getAvailableFlightSegments().size());

			for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
				FareSummaryDTO fltSegFareSummaryDTO = getFare(logicalCCtype).clone();
				fltSegFareSummaryDTO.setAdultFare(fltSegFareAmount);// TODO introduce a effective fare amount
				if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fltSegFareSummaryDTO.getChildFareType())) {
					fltSegFareSummaryDTO.setChildFare(fltSegFareAmountForChild);
				}
				if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fltSegFareSummaryDTO.getInfantFareType())) {
					fltSegFareSummaryDTO.setInfantFare(fltSegFareAmountForInfant);
				}
				availableFlightSegment.setFare(logicalCCtype, fltSegFareSummaryDTO);
			}
		}
	}

	private void setEffectiveFares() {
		for (String availableLogicalCabinClass : getAvailableLogicalCabinClasses()) {
			if (effectiveFaresSet.get(availableLogicalCabinClass) == null || !effectiveFaresSet.get(availableLogicalCabinClass)) {
				setEffectiveFares(availableLogicalCabinClass);
				effectiveFaresSet.put(availableLogicalCabinClass, true);
			}
		}
	}

	@Override
	public boolean isInboundFlightSegment() {
		boolean isInboundFlightSegment = false;
		// get the first value form the AvailableFlightSegments.
		Collection<AvailableFlightSegment> segmentsForConnection = this.getAvailableFlightSegments();
		for (AvailableFlightSegment availableFlightSegment : segmentsForConnection) {
			isInboundFlightSegment = availableFlightSegment.isInboundFlightSegment();
			break;
		}
		return isInboundFlightSegment;
	}

	@Override
	public void setInboundFlightSegment(boolean isInboundFlightSegment) {
		Collection<AvailableFlightSegment> segmentsForConnection = this.getAvailableFlightSegments();
		for (AvailableFlightSegment availableFlightSegment : segmentsForConnection) {
			availableFlightSegment.setInboundFlightSegment(isInboundFlightSegment);
		}
	}

	@Override
	public List<FlightSegmentDTO> getFlightSegmentDTOs() {
		List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
			flightSegmentDTOs.addAll(availableFlightSegment.getFlightSegmentDTOs());
		}
		return flightSegmentDTOs;
	}

	@Override
	public AvailableConnectedFlight getCleanedInstance() {
		AvailableConnectedFlight availableConnectedFlight = new AvailableConnectedFlight();
		List<AvailableFlightSegment> availableFltSegs = new ArrayList<AvailableFlightSegment>();

		for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
			availableFltSegs.add(availableFlightSegment.getCleanedInstance());
		}

		availableConnectedFlight.setAvailableFlightSegments(availableFltSegs);
		availableConnectedFlight.setOndCode(getOndCode());
		availableConnectedFlight.setOndSequence(getOndSequence());
		availableConnectedFlight.setRequestedOndSequence(getRequestedOndSequence());
		availableConnectedFlight.setSplitOperationForBusInvoked(isSplitOperationForBusInvoked());

		return availableConnectedFlight;
	}

	public FlightSegmentDTO getFirstFlightSegment() {
		return getFlightSegmentDTOs().iterator().next();
	}

	public FlightSegmentDTO getLastFlightSegment() {
		FlightSegmentDTO flightSegmentDTO = null;
		for (Iterator<FlightSegmentDTO> flightSegmentDTOIt = this.getFlightSegmentDTOs().iterator(); flightSegmentDTOIt
				.hasNext();) {
			flightSegmentDTO = flightSegmentDTOIt.next();
		}
		return flightSegmentDTO;
	}

	/**
	 * TODO - implement
	 */
	@Override
	public void addCharge() {
		throw new UnsupportedOperationException("Adding charge operation is not implemented yet.");
	}

	/**
	 * Returns true if atleast one non-marketing carrier segment present in the connection
	 * 
	 * @param marketingCarrierCode
	 * @return
	 */
	public boolean hasNonSystemCarriersSegments(Collection<String> systemCarriers) {
		boolean hasNonSystemCarrierSegs = false;
		if (systemCarriers != null) {
			FlightSegmentDTO flightSegmentDTO = null;
			for (Iterator<FlightSegmentDTO> flightSegmentDTOIt = this.getFlightSegmentDTOs().iterator(); flightSegmentDTOIt
					.hasNext();) {
				flightSegmentDTO = flightSegmentDTOIt.next();
				if (!systemCarriers.contains(flightSegmentDTO.getCarrierCode())) {
					hasNonSystemCarrierSegs = true;
					break;
				}
			}
		}

		return hasNonSystemCarrierSegs;
	}

	@Override
	public Map<String, Double> getTotalFareMap(String logicalCCtype) {
		if (isSeatsAvailable(logicalCCtype) && super.getTotalFareMap(logicalCCtype) == null) {
			setTotalFareMap(logicalCCtype, new HashMap<String, Double>());
			if (getFareType(logicalCCtype) == FareTypes.OND_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.ADULT, getFare(logicalCCtype).getFareAmount(PaxTypeTO.ADULT));
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.CHILD, getFare(logicalCCtype).getFareAmount(PaxTypeTO.CHILD));
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.INFANT,
						getFare(logicalCCtype).getFareAmount(PaxTypeTO.INFANT));
			} else if (getFareType(logicalCCtype) == FareTypes.PER_FLIGHT_FARE) { // Segment Fares
				Iterator<AvailableFlightSegment> flightSegmentsIt = getAvailableFlightSegments().iterator();
				Double adultFare = 0d;
				Double childFare = 0d;
				Double infantFare = 0d;
				while (flightSegmentsIt.hasNext()) {
					AvailableFlightSegment availableFlightSegment = flightSegmentsIt.next();
					if (availableFlightSegment.getFare(logicalCCtype) != null) {
						adultFare = adultFare + availableFlightSegment.getFare(logicalCCtype).getFareAmount(PaxTypeTO.ADULT);
						childFare = childFare + availableFlightSegment.getFare(logicalCCtype).getFareAmount(PaxTypeTO.CHILD);
						infantFare = infantFare + availableFlightSegment.getFare(logicalCCtype).getFareAmount(PaxTypeTO.INFANT);
					} else {
						adultFare = -1d;
						childFare = -1d;
						infantFare = -1d;
						break;
					}
				}
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.ADULT, adultFare);
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.CHILD, childFare);
				super.getTotalFareMap(logicalCCtype).put(PaxTypeTO.INFANT, infantFare);
			}
		}
		return super.getTotalFareMap(logicalCCtype);
	}

	@Override
	public Collection<FlexiRuleDTO> getEffectiveFlexiCharges(String logicalCCtype) {
		if (super.getEffectiveFlexiCharges(logicalCCtype) == null && getFare(logicalCCtype) != null) {
			setEffectiveFlexiCharges(logicalCCtype, ChargeQuoteUtils.getFlexiChargesWithEffectiveChargeValues(
					getFlexiCharges(logicalCCtype), this.getFare(logicalCCtype), this.getEffectiveCharges(logicalCCtype)));
		}
		return super.getEffectiveFlexiCharges(logicalCCtype);
	}

	@Override
	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r";
		String t = "\t";
		summary.append(nl + nl + "==============Available Connected Flight Segments Summary=================" + nl);
		summary.append("Connected Flight Segments Information " + nl);
		summary.append(t + "Connected Flights OND                   = " + getOndCode() + nl);

		for (String logicalCabinClass : getAvailableLogicalCabinClasses()) {
			summary.append(t + "Seats Available For Channel	= " + isSeatsAvailable(logicalCabinClass) + nl);
			if (getFareType(logicalCabinClass) != FareTypes.NO_FARE) {
				summary.append(
						t + "Total Adult Fare	= " + getFareAmount(logicalCabinClass, PaxTypeTO.ADULT).doubleValue() + nl);
				summary.append(
						t + "Total Child Fare	= " + getFareAmount(logicalCabinClass, PaxTypeTO.CHILD).doubleValue() + nl);
				summary.append(
						t + "Total Infant Fare	= " + getFareAmount(logicalCabinClass, PaxTypeTO.INFANT).doubleValue() + nl);
				if (getEffectiveCharges(logicalCabinClass) != null) {
					summary.append(t + "Total Adult Charges	= " + getTotalCharges(logicalCabinClass).get(0) + nl);
					summary.append(t + "Total Infant Charges	= " + getTotalCharges(logicalCabinClass).get(1) + nl);
					summary.append(t + "Total Child Charges	= " + getTotalCharges(logicalCabinClass).get(2) + nl);
				}
			}
			if (isSeatsAvailable(logicalCabinClass)) {
				if (getFareType(logicalCabinClass) == FareTypes.OND_FARE
						|| getFareType(logicalCabinClass) == FareTypes.HALF_RETURN_FARE) {
					summary.append("Fare quoted on the connected flight segments' OND " + nl);
					summary.append(t + "FareId	= " + getFare(logicalCabinClass).getFareId() + nl);
					summary.append(t + "Adult Fare Amount	= " + getFare(logicalCabinClass).getFareAmount(PaxTypeTO.ADULT) + nl);
					summary.append(t + "Child Fare Amount	= " + getFare(logicalCabinClass).getFareAmount(PaxTypeTO.CHILD) + nl);
					summary.append(
							t + "Infant Fare Amount  = " + getFare(logicalCabinClass).getFareAmount(PaxTypeTO.INFANT) + nl);

					if (getEffectiveCharges(logicalCabinClass) != null) {
						summary.append("Charges quoted on the connected flight segments' OND " + nl);
						summary.append(
								"Charges break down \n\t[chargeCode,chargeRateId,chargeCatCode,chargeGroup,chargeValuePercentage,"
										+ "isInPercentage,adultEffCharge,childEffCharge,infEffCharge,appliesTo]" + nl);
						Iterator<QuotedChargeDTO> effectiveChargesIt = getEffectiveCharges(logicalCabinClass).iterator();
						while (effectiveChargesIt.hasNext()) {
							QuotedChargeDTO charge = effectiveChargesIt.next();
							summary.append(t + "[" + charge.getChargeCode()).append(",");
							summary.append(charge.getChargeRateId()).append(",");
							summary.append(charge.getChargeCategoryCode()).append(",");
							summary.append(charge.getChargeGroupCode()).append(",");
							summary.append(charge.getChargeValuePercentage()).append(",");
							summary.append(charge.isChargeValueInPercentage()).append(",");
							summary.append(charge.getEffectiveChargeAmount(PaxTypeTO.ADULT)).append(",");
							summary.append(charge.getEffectiveChargeAmount(PaxTypeTO.CHILD)).append(",");
							summary.append(charge.getEffectiveChargeAmount(PaxTypeTO.INFANT)).append(",");
							summary.append(charge.getApplicableTo()).append("]" + nl);
						}
					} else {
						summary.append("No Charge quoted on the connected flight segments' OND " + nl);
					}
				}
			}
		}
		return summary;
	}

	@Override
	public boolean isDirectFlight() {
		return false;
	}

	@Override
	public Date getDepartureDate() {
		return getAvailableFlightSegments().iterator().next().getFlightSegmentDTO().getDepartureDateTime();
	}

	@Override
	public Date getDepartureDateZulu() {
		return getAvailableFlightSegments().iterator().next().getFlightSegmentDTO().getDepartureDateTimeZulu();
	}

	@Override
	public Date getArrivalDate() {
		if (!getAvailableFlightSegments().isEmpty()) {
			return getAvailableFlightSegments().get(getAvailableFlightSegments().size() - 1).getFlightSegmentDTO()
					.getArrivalDateTime();
		}
		return null;
	}

	@Override
	public String getCabinClassCode() {
		if (super.getCabinClassCode() == null && !getAvailableFlightSegments().isEmpty()) {
			return getAvailableFlightSegments().iterator().next().getCabinClassCode();
		}

		return super.getCabinClassCode();
	}

	@Override
	public String getSelectedLogicalCabinClass() {
		if (super.getSelectedLogicalCabinClass() == null) {
			String selectedLogicalCabinClass = null;
			for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
				selectedLogicalCabinClass = getCommonLogicalCabinClass(availableFlightSegment.getSelectedLogicalCabinClass(),
						selectedLogicalCabinClass);
				if (selectedLogicalCabinClass == null) {
					return null;
				}
			}
			return selectedLogicalCabinClass;
		}
		return super.getSelectedLogicalCabinClass();
	}

	private String getCommonLogicalCabinClass(String firstLogicalCabinClass, String secondLogicalCabinClass) {
		if (firstLogicalCabinClass != null && secondLogicalCabinClass == null) {
			return firstLogicalCabinClass;
		}
		if (firstLogicalCabinClass != null && secondLogicalCabinClass != null
				&& firstLogicalCabinClass.equals(secondLogicalCabinClass)) {
			return firstLogicalCabinClass;
		}
		return null;
	}

	private Map<String, Double> totalSurchargeMap = null;

	@Override
	public Map<String, Double> getTotalSurcharge(String logicalCCtype) {
		if (totalSurchargeMap == null) {
			if (getFareType(logicalCCtype) == FareTypes.OND_FARE || getFareType(logicalCCtype) == FareTypes.HALF_RETURN_FARE) {
				Collection<QuotedChargeDTO> currentEffChgs = getEffectiveCharges(logicalCCtype, true);
				totalSurchargeMap = ChargeQuoteUtils.getTotalSurcharges(currentEffChgs);
				seteffectiveCharges(logicalCCtype, null);
			} else {
				Iterator<AvailableFlightSegment> flightSegmentsIt = getAvailableFlightSegments().iterator();
				Double adultSur = 0d;
				Double childSur = 0d;
				Double infantSur = 0d;
				while (flightSegmentsIt.hasNext()) {
					AvailableFlightSegment availableFlightSegment = flightSegmentsIt.next();
					Map<String, Double> segSurchgMap = availableFlightSegment.getTotalSurcharge(logicalCCtype);
					adultSur = adultSur + segSurchgMap.get(PaxTypeTO.ADULT);
					childSur = childSur + segSurchgMap.get(PaxTypeTO.CHILD);
					infantSur = infantSur + segSurchgMap.get(PaxTypeTO.INFANT);
				}
				this.totalSurchargeMap = new HashMap<String, Double>();
				this.totalSurchargeMap.put(PaxTypeTO.ADULT, adultSur);
				this.totalSurchargeMap.put(PaxTypeTO.CHILD, childSur);
				this.totalSurchargeMap.put(PaxTypeTO.INFANT, infantSur);
			}
		}
		return totalSurchargeMap;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public Set<String> getAvailableExternalLogicalCabinClasses() {
		Set<String> availableExtLogicalCabinClasses = new HashSet<String>();
		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
			availableExtLogicalCabinClasses.retainAll(availableFlightSegment.getAvailableExternalLogicalCabinClasses());
		}
		return availableExtLogicalCabinClasses;
	}

	/**
	 * Shallowly copy itself. Currently copies FlightSegmentDTOs, CalibnClassCode and OndSequence.
	 */
	@Override
	public AvailableIBOBFlightSegment getShallowCopy() {
		AvailableIBOBFlightSegment seg = new AvailableConnectedFlight();
		seg.getFlightSegmentDTOs().addAll(this.getFlightSegmentDTOs());
		seg.setCabinClassCode(getCabinClassCode());
		seg.setOndSequence(getOndSequence());
		seg.setRequestedOndSequence(getRequestedOndSequence());
		return seg;
	}

	@Override
	public Collection<QuotedChargeDTO> getAllEffectiveCharges(String logicalCCType) {
		Collection<QuotedChargeDTO> charges = super.geteffectiveCharges(logicalCCType);
		if (CollectionUtils.isEmpty(charges) && !CollectionUtils.isEmpty(availableFlightSegments)) {
			charges = new ArrayList<>();
			for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
				if (!CollectionUtils.isEmpty(availableFlightSegment.geteffectiveCharges(logicalCCType))) {
					charges.addAll(availableFlightSegment.geteffectiveCharges(logicalCCType));
				}
			}
		}
		return charges;
	}

	public void cleanFares() {
		super.cleanFares();
		for (AvailableFlightSegment availableFlightSegment : getAvailableFlightSegments()) {
			availableFlightSegment.cleanFares();
		}
	}

	public void addMatchingCombinationKey(String matchingCombinationKey) {
		matchingCombinationKeys.add(matchingCombinationKey);
		for (AvailableFlightSegment afs : getAvailableFlightSegments()) {
			afs.addMatchingCombinationKey(matchingCombinationKey);
		}
	}

	public Set<String> getMatchingCombinationKeys() {
		return matchingCombinationKeys;
	}

	@Override
	public String getOriginAirport() {
		return getFirstFlightSegment().getDepartureAirportCode();
	}

	@Override
	public String getDestinationAirport() {
		return getLastFlightSegment().getArrivalAirportCode();
	}

}
