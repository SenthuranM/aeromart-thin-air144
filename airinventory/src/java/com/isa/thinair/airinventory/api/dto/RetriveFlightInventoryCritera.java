package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class RetriveFlightInventoryCritera implements Serializable {

	private static final long serialVersionUID = -4483006079959454108L;

	private int flightId;

	private String cabinClassCode;

	private Integer overlappingFlightId;

	private Collection<Integer> flightIds;

	private boolean excludeInvalidSegmentInventories;

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public Integer getOverlappingFlightId() {
		return overlappingFlightId;
	}

	public void setOverlappingFlightId(Integer overlappingFlightId) {
		this.overlappingFlightId = overlappingFlightId;
	}

	public void setFlightIds(Collection<Integer> flightIds) {
		this.flightIds = flightIds;
	}

	public Collection<Integer> getFlightIds() {
		if (this.flightIds != null) {
			return this.flightIds;
		}

		Collection <Integer>fltIds = new ArrayList<Integer>();
		fltIds.add(new Integer(getFlightId()));
		if (getOverlappingFlightId() != null) {
			fltIds.add(overlappingFlightId);
		}
		return fltIds;
	}

	public boolean isExcludeInvalidSegmentInventories() {
		return excludeInvalidSegmentInventories;
	}

	public void setExcludeInvalidSegmentInventories(boolean excludeInvalidSegmentInventories) {
		this.excludeInvalidSegmentInventories = excludeInvalidSegmentInventories;
	}
}
