package com.isa.thinair.airinventory.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Represents seats nested in flight cabinclass seg bc inventory
 * 
 * @author : MN
 * @hibernate.class table = "T_FCC_SEG_BC_NESTING"
 */
public class FCCSegBCInvNesting extends Persistent {

	private static final long serialVersionUID = 6362329814409066844L;

	private Integer fccsbInvNestingId;

	private Integer fromFCCSegBCInvId;

	private Integer toFCCSegBCInvId;

	private int soldSeats;

	private int onholdSeats;

	private String temporary = "N";

	/**
	 * @hibernate.id column = "FCCSBAN_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FCC_SEG_BC_NESTING"
	 */
	public Integer getFccsbInvNestingId() {
		return fccsbInvNestingId;
	}

	public void setFccsbInvNestingId(Integer fccsbInvNestingId) {
		this.fccsbInvNestingId = fccsbInvNestingId;
	}

	/**
	 * @hibernate.property column = "FROM_FCCSBA_ID"
	 */
	public Integer getFromFCCSegBCInvId() {
		return fromFCCSegBCInvId;
	}

	public void setFromFCCSegBCInvId(Integer fromFCCSegBCInvId) {
		this.fromFCCSegBCInvId = fromFCCSegBCInvId;
	}

	/**
	 * @hibernate.property column = "TO_FCCSBA_ID"
	 */
	public Integer getToFCCSegBCInvId() {
		return toFCCSegBCInvId;
	}

	public void setToFCCSegBCInvId(Integer toFCCSegBCInvId) {
		this.toFCCSegBCInvId = toFCCSegBCInvId;
	}

	/**
	 * @hibernate.property column = "ONHOLD_SEATS"
	 */
	public int getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(int onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	/**
	 * @hibernate.property column = "SOLD_SEATS"
	 */
	public int getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(int soldSeats) {
		this.soldSeats = soldSeats;
	}

	/**
	 * @hibernate.property column = "IS_TEMP"
	 */
	public String getTemporary() {
		return temporary;
	}

	public void setTemporary(String temporary) {
		this.temporary = temporary;
	}

}
