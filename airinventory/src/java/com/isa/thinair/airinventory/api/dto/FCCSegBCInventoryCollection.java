package com.isa.thinair.airinventory.api.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;

/**
 * 
 * @author Nasly
 * 
 */
public class FCCSegBCInventoryCollection {

	private final Log log = LogFactory.getLog(getClass());

	private LinkedHashMap<Integer, FCCSegBCInventoryTO> standardBuckets;
	private LinkedHashMap<Integer, FCCSegBCInventoryTO> nonStandardBuckets;
	private LinkedHashMap<Integer, FCCSegBCInventoryTO> fixedBuckets;

	public FCCSegBCInventoryCollection() {
	}

	public void addBucket(FCCSegBCInventoryTO bucket) {
		if (bucket.isStandardBookingClass()) {
			if (standardBuckets == null) {
				standardBuckets = new LinkedHashMap<Integer, FCCSegBCInventoryTO>();
			}
			standardBuckets.put(new Integer(bucket.getFccsbInvId()), bucket);
		} else if (!bucket.isFixedBookingClass()) {
			if (nonStandardBuckets == null) {
				nonStandardBuckets = new LinkedHashMap<Integer, FCCSegBCInventoryTO>();
			}
			nonStandardBuckets.put(new Integer(bucket.getFccsbInvId()), bucket);
		} else if (bucket.isFixedBookingClass()) {
			if (fixedBuckets == null) {
				fixedBuckets = new LinkedHashMap<Integer, FCCSegBCInventoryTO>();
			}
			fixedBuckets.put(new Integer(bucket.getFccsbInvId()), bucket);
		}
	}

	/**
	 * Return the bucket if exists, otherwise returns null
	 * 
	 * @param fccsbInvId
	 * @param isStandard
	 * @param isFixed
	 * @return
	 */
	public FCCSegBCInventoryTO getBucket(Integer fccsbInvId, boolean isStandard, boolean isFixed) {
		FCCSegBCInventoryTO bucket = null;
		if (isStandard && standardBuckets != null) {
			bucket = (FCCSegBCInventoryTO) standardBuckets.get(fccsbInvId);
		} else if (!isFixed && nonStandardBuckets != null) {
			bucket = (FCCSegBCInventoryTO) nonStandardBuckets.get(fccsbInvId);
		} else if (isFixed && fixedBuckets != null) {
			bucket = (FCCSegBCInventoryTO) fixedBuckets.get(fccsbInvId);
		}

		return bucket;
	}

	/**
	 * The logic assumes standard buckets are in the ascending order of the nest rank
	 */
	public boolean isBucketEligibleForReopen(int fccsbaId) {
		boolean isEligible = false;

		if (standardBuckets != null) {
			Iterator<FCCSegBCInventoryTO> bucketIt = standardBuckets.values().iterator();
			for (int i = 0; bucketIt.hasNext(); ++i) {
				FCCSegBCInventoryTO bucket = (FCCSegBCInventoryTO) bucketIt.next();
				if (log.isDebugEnabled())
					doDebug("Start processing the bucket", bucket);
				if (bucket.getFccsbInvId() == fccsbaId) {
					if (i == 0) {
						if (log.isDebugEnabled())
							doDebug("Requested bucket is lowest rank std bucket, eligible for reopening", bucket);
						isEligible = true;
					}
					break;
				} else if ((bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN)
						&& bucket.getAllocationType().equals(BookingClass.AllocationType.SEGMENT) && bucket.getFareTypes() != null)
						|| (bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN)
								&& bucket.getAllocationType().equals(BookingClass.AllocationType.COMBINED)
								&& bucket.getFareTypes() != null && bucket.getFareTypes().contains(
								new Integer(FareTypes.PER_FLIGHT_FARE)))) {
					// has open, lower ranked, segment fare bucket(s)
					if (log.isDebugEnabled())
						doDebug("Has lower ranked open segment buckets, eligible for reopening", bucket);
					isEligible = true;
					break;
				}
			}
		}

		return isEligible;
	}

	/**
	 * Returns buckets eligible for close The logic assumes standard buckets are in the ascending order of the nest rank
	 * Buckets not having segments fares are only eligible for closing
	 */
	public Collection<FCCSegBCInventoryTO> getBucketsEligbleForClosing() {
		Collection<FCCSegBCInventoryTO> buckets = null;
		if (standardBuckets != null) {
			Collection<FCCSegBCInventoryTO> bucketsToClose = null;
			boolean hasLowerRankedClosedStdBuckets = false;
			boolean hasHigherRankedOpenStdBuckets = false;
			boolean bucketHasSegmentFare = false;
			boolean bucketHasConnectionFare = false;
			FCCSegBCInventoryTO bucket = null;
			Iterator<FCCSegBCInventoryTO> bcInventoryIterator = standardBuckets.values().iterator();
			while (bcInventoryIterator.hasNext()) {
				bucket = (FCCSegBCInventoryTO) bcInventoryIterator.next();

				if (log.isDebugEnabled())
					doDebug("Start processing the bucket", bucket);

				// initialize
				bucketHasSegmentFare = false;
				bucketHasConnectionFare = false;

				// segment/combined buckets not having fare and not having sold/onhold seats are ignored
				if (bucket.getFareTypes() == null
						&& (bucket.getAllocationType().equals(BookingClass.AllocationType.SEGMENT) || bucket.getAllocationType()
								.equals(BookingClass.AllocationType.COMBINED))
						&& (bucket.getSoldSeats() + bucket.getOnholdSeats() == 0)) {
					if (log.isDebugEnabled())
						doDebug("Skipping the bucket as no fare attached", bucket);
					continue;
				}

				// segment fare, open bucket
				if ((bucket.getAllocationType().equals(BookingClass.AllocationType.SEGMENT)
						&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN) && bucket.getFareTypes() != null)
						|| (bucketHasSegmentFare = (bucket.getAllocationType().equals(BookingClass.AllocationType.COMBINED)
								&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN) && bucket.getFareTypes() != null && bucket
								.getFareTypes().contains(new Integer(FareTypes.PER_FLIGHT_FARE))))) {
					if (bucketsToClose == null) {
						if (log.isDebugEnabled())
							doDebug("Aborting, lower ranked open segment fare buckets found", bucket);
						break;// has lower ranked open segment buckets or combined buckets having segment fare,
								// no connection/return bucket is eligible for closing
					} else {// open connection/return bucket already found
						if (log.isDebugEnabled())
							doDebug("Higher ranked open segment fare bucket(s) found", bucket);
						hasHigherRankedOpenStdBuckets = true;
					}
				}

				// segment fare, closed bucket; segment/combined buckets not having fare but with sold/onhold seats are
				// treated as closed segment bucket
				if ((bucket.getAllocationType().equals(BookingClass.AllocationType.SEGMENT) && (bucket.getStatus().equals(
						FCCSegBCInventory.Status.CLOSED) || bucket.getFareTypes() == null))
						|| (!bucketHasSegmentFare && (bucketHasSegmentFare = (bucket.getAllocationType().equals(
								BookingClass.AllocationType.COMBINED) && ((bucket.getStatus().equals(
								FCCSegBCInventory.Status.CLOSED)
								&& bucket.getFareTypes() != null && bucket.getFareTypes().contains(
								new Integer(FareTypes.PER_FLIGHT_FARE))) || bucket.getFareTypes() == null))))) {
					if (bucketsToClose == null) {
						if (log.isDebugEnabled())
							doDebug("Lower ranked closed segment fare bucket(s) found", bucket);
						hasLowerRankedClosedStdBuckets = true;
					}
				}

				if (getAirInventoryConfig().isApplyConnectionBucketsClosure()) {
					// connection fare, open, but not manually open bucket
					if ((bucket.getAllocationType().equals(BookingClass.AllocationType.CONNECTION)
							&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN) && !bucket.getStatusChangeAction()
							.equals(FCCSegBCInventory.StatusAction.MANUAL))
							|| (bucketHasConnectionFare = (!bucketHasSegmentFare
									&& bucket.getAllocationType().equals(BookingClass.AllocationType.COMBINED)
									&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN)
									&& !bucket.getStatusChangeAction().equals(FCCSegBCInventory.StatusAction.MANUAL) && (bucket
									.getFareTypes() != null && bucket.getFareTypes().contains(new Integer(FareTypes.OND_FARE)))))) {
						if (!hasLowerRankedClosedStdBuckets) {
							if (log.isDebugEnabled())
								doDebug("Aborting, no lower ranked closed segment fare bucket(s) found", bucket);
							break;// has no lower ranked segment fare buckets
									// so no connection bucket is eligible for closing
						} else {
							if (log.isDebugEnabled())
								doDebug("Tentatively adding connection fare bucket for closing", bucket);
							if (bucketsToClose == null) {
								bucketsToClose = new ArrayList<FCCSegBCInventoryTO>();
							}
							bucketsToClose.add(bucket);
						}
					}
				}

				if (getAirInventoryConfig().isApplyReturnBucketsClosure()) {
					// return, open, but not manually open bucket

					if ((bucket.getAllocationType().equals(BookingClass.AllocationType.RETURN)
							&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN) && !bucket.getStatusChangeAction()
							.equals(FCCSegBCInventory.StatusAction.MANUAL))
							|| (!bucketHasSegmentFare && !bucketHasConnectionFare
									&& bucket.getAllocationType().equals(BookingClass.AllocationType.COMBINED)
									&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN)
									&& !bucket.getStatusChangeAction().equals(FCCSegBCInventory.StatusAction.MANUAL) && (bucket
									.getFareTypes() != null && bucket.getFareTypes().contains(new Integer(FareTypes.RETURN_FARE))))) {
						if (!hasLowerRankedClosedStdBuckets) {
							if (log.isDebugEnabled())
								doDebug("Aborting, no lower ranked closed segment fare bucket(s) found", bucket);
							break;// has no lower ranked segment fare bucket
									// so no return bucket is eligible for closing
						} else {
							if (log.isDebugEnabled())
								doDebug("Tentatively adding return fare bucket for closing", bucket);
							if (bucketsToClose == null) {
								bucketsToClose = new ArrayList<FCCSegBCInventoryTO>();
							}
							bucketsToClose.add(bucket);
						}
					}
				}

				if (hasHigherRankedOpenStdBuckets) {

					if (buckets == null) {
						buckets = new ArrayList<FCCSegBCInventoryTO>();
					}
					buckets.addAll(bucketsToClose);

					if (log.isDebugEnabled()) {
						if (bucketsToClose != null) {
							Iterator<FCCSegBCInventoryTO> bucketsToCloseIt = bucketsToClose.iterator();
							while (bucketsToCloseIt.hasNext()) {
								doDebug("Finalizing bucket eligible for closing", bucketsToCloseIt.next());
							}
						}
					}

					bucketsToClose = null;
					hasHigherRankedOpenStdBuckets = false;
					break;// no more bucket is eligible for clousure as lower ranked open segment bucket already exists
				}
			}
		}
		return buckets;
	}

	private void doDebug(String msg, FCCSegBCInventoryTO bucket) {
		log.debug(msg + " [fccsbInvId=" + bucket.getFccsbInvId() + ",bc=" + bucket.getBookingClass() + ",allocType="
				+ bucket.getAllocationType() + ",fareTypes=" + bucket.getFareTypesStr() + ",status=" + bucket.getStatus()
				+ ",statusChgAction=" + bucket.getStatusChangeAction() + ",sold:onhold=" + bucket.getSoldSeats() + ":"
				+ bucket.getOnholdSeats() + ",nestRank=" + bucket.getNestRank() + "]");
	}

	private AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}
}
