package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroupRatios;
import org.springframework.util.CollectionUtils;

public class FareONDChargeInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// a - % of fare for sur
	// b - % of total fare for surcharges
	// c - % of fare for taxes
	// d - % of total fare for taxes
	// e - % of surchages
	// f - % of total surcharges
	// g - % of fare + surcharges
	// h - % of total fare + total surcharges
	private double percentFare_Sur = 0;
	private double percentTotalFare_Sur = 0;
	private double percentFare_Tax = 0;
	private double percentTotalFare_Tax = 0;
	private double percentSurchages = 0;
	private double percentTotalSurcharges = 0;
	private double percentFareSurcharges = 0;
	private double percentTotalFareSurcharges = 0;
	private double taxValue = 0;
	private double surchargeValue = 0;
	private double totalJourneyValueSurcharge = 0;
	private double percentServiceTax= 0;

	private Map<String, FareONDChargeInfo> serviceTaxInfoMap;

	public void addSurchargePF(double value) {
		percentFare_Sur += value;
	}

	public void addSurchargePTF(double value) {
		percentTotalFare_Sur += value;
	}

	public void addTaxPF(double value) {
		percentFare_Tax += value;
	}

	public void addTaxPTF(double value) {
		percentTotalFare_Tax += value;
	}

	public void addPS(double value) {
		percentSurchages += value;
	}

	public void addPTS(double value) {
		percentTotalSurcharges += value;
	}

	public void addPFS(double value) {
		percentFareSurcharges += value;
	}

	public void addPTFS(double value) {
		percentTotalFareSurcharges += value;
	}

	public void addTaxValue(double value) {
		taxValue += value;
	}

	public void addSurchargeValue(double value) {
		surchargeValue += value;
	}

	public void setTotalJourneyValueSurcharge(double value) {
		totalJourneyValueSurcharge = value;
	}

	public double getSurchargeValue() {
		return surchargeValue;
	}

	public void setPercentServiceTax(double percentServiceTax) {
		this.percentServiceTax = percentServiceTax;
	}

	public Map<String, FareONDChargeInfo> getServiceTaxInfoMap() {
		if (serviceTaxInfoMap == null) {
			serviceTaxInfoMap = new HashMap<>();
		}
		return serviceTaxInfoMap;
	}

	public double getTotalPercentageFactor(String ratioCategory) {
		if (ChargeGroupRatios.SUR_RATIO.equals(ratioCategory)) {
			return (1 + percentFare_Sur / 100.0 + percentTotalFare_Sur / 100);
		} else if (ChargeGroupRatios.TAX_RATIO.equals(ratioCategory)) {
			return (1 + (percentFare_Sur / 100.0 + percentTotalFare_Sur / 100.0)
					* (percentSurchages / 100.0 + percentFareSurcharges / 100.0) + percentFare_Tax / 100.0 + percentTotalFare_Tax
					/ 100.0 + percentFareSurcharges / 100.0 + percentTotalFareSurcharges / 100.0);
		} else if (ChargeGroupRatios.SURTAX_RATIO.equals(ratioCategory)) {
			return (1 + (percentFare_Sur / 100.0 + percentTotalFare_Sur / 100.0)
					* (percentSurchages / 100.0 + percentFareSurcharges / 100.0 + 1) + percentFare_Tax / 100.0
					+ percentTotalFare_Tax / 100.0 + percentFareSurcharges / 100.0 + percentTotalFareSurcharges / 100.0);
		} else {
			return 1;
		}
	}

	public double getTotalValueFactorForReturn(String ratioCategory) {
		if (ChargeGroupRatios.SUR_RATIO.equals(ratioCategory)) {
			return totalJourneyValueSurcharge;
		} else if (ChargeGroupRatios.TAX_RATIO.equals(ratioCategory)) {
			return taxValue;
		} else if (ChargeGroupRatios.SURTAX_RATIO.equals(ratioCategory)) {
			return ((percentTotalSurcharges / 100.0 + percentTotalFareSurcharges / 100.0) * totalJourneyValueSurcharge
					+ (percentSurchages / 100.0 + percentFareSurcharges / 100.0) * surchargeValue + totalJourneyValueSurcharge + taxValue);
		} else {
			return 0;
		}
	}

	public double getTotalValueFactor(String ratioCategory) {
		if (ChargeGroupRatios.SUR_RATIO.equals(ratioCategory)) {
			return surchargeValue;
		} else if (ChargeGroupRatios.TAX_RATIO.equals(ratioCategory)) {
			return taxValue;
		} else if (ChargeGroupRatios.SURTAX_RATIO.equals(ratioCategory)) {
			return ((percentTotalSurcharges / 100.0 + percentTotalFareSurcharges / 100.0) * totalJourneyValueSurcharge
					+ (percentSurchages / 100.0 + percentFareSurcharges / 100.0 + 1) * surchargeValue + taxValue);
		} else {
			return 0;
		}
	}

	public double getTotalFareWithoutServiceTaxes(double fare, String ratioCategory) {
		return getTotal(fare, ratioCategory, false);
	}

	public double getTotalFare(double fare, String ratioCategory) {
		return getTotal(fare, ratioCategory, false) + getServiceTaxAmount(fare, ratioCategory);

	}

	private double getTotal(double fare, String ratioCategory, boolean isServiceTaxCalculation) {
		double serviceTaxPercentage = isServiceTaxCalculation ? (this.percentServiceTax / 100.0) : 1;
		double surcharge = fare * (percentFare_Sur / 100.0 + percentTotalFare_Sur / 100.0) * serviceTaxPercentage + surchargeValue;

		double tax = fare
				* (percentFare_Tax / 100.0 + percentTotalFare_Tax / 100.0 + percentFareSurcharges / 100.0 + percentTotalFareSurcharges / 100.0)
				* serviceTaxPercentage
				+ taxValue;

		if (ChargeGroupRatios.SURTAX_RATIO.equals(ratioCategory)) {
			tax += (surcharge * (percentSurchages / 100.0 + percentFareSurcharges / 100.0) * serviceTaxPercentage + totalJourneyValueSurcharge
					* (percentTotalSurcharges / 100.0 + percentTotalFareSurcharges / 100.0) * serviceTaxPercentage);
		}

		double total = fare * serviceTaxPercentage;

		if (ChargeGroupRatios.SUR_RATIO.equals(ratioCategory) || ChargeGroupRatios.SURTAX_RATIO.equals(ratioCategory)) {
			total += surcharge;
		}

		if (ChargeGroupRatios.TAX_RATIO.equals(ratioCategory) || ChargeGroupRatios.SURTAX_RATIO.equals(ratioCategory)) {
			total += tax;
		}
		return total;
	}

	private double getServiceTaxAmount(double fare, String ratioCategory) {
		double serviceTaxAmount = 0.0;
		if (!CollectionUtils.isEmpty(serviceTaxInfoMap)) {
			for (FareONDChargeInfo serviceTax : serviceTaxInfoMap.values()) {
				if (serviceTax.percentServiceTax > 0) {
					serviceTaxAmount += serviceTax.getTotal(fare, ratioCategory, true);
				} else {
					serviceTaxAmount += serviceTax.taxValue;
				}
			}
		}
		return serviceTaxAmount;
	}
}