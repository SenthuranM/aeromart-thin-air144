package com.isa.thinair.airinventory.api.dto.bundledfare;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.constants.BundledFareConstants;

/**
 * DTO to transfer minimum data required to display available bundled fare options
 * 
 * @author rumesh
 * 
 */
public class BundledFareLiteDTO implements Serializable {
	private static final long serialVersionUID = 4349650606116482721L;

	private Integer bundleFarePeriodId;

	private String bundledFareName;

	private String description;

	private Set<String> bookingClasses;

	private boolean flexiIncluded;

	private BigDecimal perPaxBundledFee = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal applicableTaxPortion = AccelAeroCalculator.getDefaultBigDecimalZero();

	private Set<String> freeServices;

	private Map<String, String> segmentBookingClass;

	private String imageUrl;

	private Integer priority;

	private boolean isDefault;

	private BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO;
	
	private Map<String, String> imagesUrlMap;

	public Integer getBundleFarePeriodId() {
		return bundleFarePeriodId;
	}

	public void setBundleFarePeriodId(Integer bundleFarePeriodId) {
		this.bundleFarePeriodId = bundleFarePeriodId;
	}

	public String getBundledFareName() {
		return bundledFareName;
	}

	public void setBundledFareName(String bundledFareName) {
		this.bundledFareName = bundledFareName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Set<String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public boolean isFlexiIncluded() {
		return flexiIncluded;
	}

	public void setFlexiIncluded(boolean flexiIncluded) {
		this.flexiIncluded = flexiIncluded;
	}

	public BigDecimal getPerPaxBundledFee() {
		return perPaxBundledFee;
	}

	public void setPerPaxBundledFee(BigDecimal perPaxBundledFee) {
		this.perPaxBundledFee = perPaxBundledFee;
	}

	public BigDecimal getApplicableTaxPortion() {
		return applicableTaxPortion;
	}

	public void setApplicableTaxPortion(BigDecimal applicableTaxPortion) {
		this.applicableTaxPortion = applicableTaxPortion;
	}

	public Set<String> getFreeServices() {
		return freeServices;
	}

	public void setFreeServices(Set<String> freeServices) {
		this.freeServices = freeServices;
	}

	public Map<String, String> getSegmentBookingClass() {
		return segmentBookingClass;
	}

	public void setSegmentBookingClass(Map<String, String> segmentBookingClass) {
		this.segmentBookingClass = segmentBookingClass;
	}

	public Map<String, String> getImagesUrlMap() {
		return imagesUrlMap;
	}

	public void setImagesUrlMap(Map<String, String> imagesUrlMap) {
		this.imagesUrlMap = imagesUrlMap;
	}

	public void setImageUrl(String prefLanguage) {

		String url = "";
		prefLanguage = (prefLanguage == null || "".equals(prefLanguage)) ? Locale.ENGLISH.getLanguage() : prefLanguage;
		if (this.imagesUrlMap != null && !imagesUrlMap.isEmpty()) {
			if (imagesUrlMap.containsKey(prefLanguage)) {
				url = imagesUrlMap.get(prefLanguage);
			}
		}

		if (StringUtil.isNullOrEmpty(url)) {
			// This fallback needs to be completely removed,once the image url migration is fully completed.
			String strImgpath = AppSysParamsUtil.getImageUploadPath();
			StringBuilder imageUrl = new StringBuilder(strImgpath).append(BundledFareConstants.IMG_PREFIX)
					.append(this.getBundleFarePeriodId()).append("_").append(prefLanguage)
					.append(BundledFareConstants.IMG_SUFFIX);
			url = imageUrl.toString();
		}

		this.imageUrl = url;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public BundleFareDescriptionTemplateDTO getBundleFareDescriptionTemplateDTO() {
		return bundleFareDescriptionTemplateDTO;
	}

	public void setBundleFareDescriptionTemplateDTO(BundleFareDescriptionTemplateDTO bundleFareDescriptionTemplateDTO) {
		this.bundleFareDescriptionTemplateDTO = bundleFareDescriptionTemplateDTO;
	}
}