package com.isa.thinair.airinventory.api.dto.inventory;

import java.io.Serializable;
import java.util.ArrayList;

public class InvTempDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer invTempID;

	private String status;

	private String airCraftModel;

	private String segment;

	private ArrayList<InvTempCCAllocDTO> invTempCCAllocDTOList;

	public Integer getInvTempID() {
		return invTempID;
	}


	public void setInvTempID(Integer invTempID) {
		this.invTempID = invTempID;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getAirCraftModel() {
		return airCraftModel;
	}

	public void setAirCraftModel(String airCraftModel) {
		this.airCraftModel = airCraftModel;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}


	public ArrayList<InvTempCCAllocDTO> getInvTempCCAllocDTOList() {
		return invTempCCAllocDTOList;
	}

	public void setInvTempCCAllocDTOList(ArrayList<InvTempCCAllocDTO> invTempCCAllocDTOList) {
		this.invTempCCAllocDTOList = invTempCCAllocDTOList;
	}

}
