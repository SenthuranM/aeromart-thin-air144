package com.isa.thinair.airinventory.api.service;

import java.util.Map;

import com.isa.thinair.airinventory.api.dto.inventory.LogicalCabinClassDTO;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface LogicalCabinClassBD {
	public static final String SERVICE_NAME = "LogicalCabinClassService";

	public Page<LogicalCabinClass> getLogicalCabinClass(LogicalCabinClassDTO searchParams, int startRec, int noRecs)
			throws ModuleException;

	public void saveTemplate(LogicalCabinClass logicalCabinClass, String chkNestShift) throws ModuleException;

	public int deleteLogicalCabinClass(LogicalCabinClass logicalCabinClass) throws ModuleException;

	public String getDefaultLogicalCabinClass(String cabinClass) throws ModuleException;

	public LogicalCabinClass getLogicalCabinClass(String logicalCCCode) throws ModuleException;

	public String getCabinClass(String logicalCCCode) throws ModuleException;

	public Map<String, LogicalCabinClass> getAllLogicalCabinClasses() throws ModuleException;

	public Map<String, LogicalCabinClass> getLogicalCabinClasses(String cabinClass) throws ModuleException;

	/**
	 * Retrieves the translated messages for {@link LogicalCabinClass}
	 * 
	 * @return A Map in the format of <String,Map<String,String>>. First map has the i18n_message_key as the key and a
	 *         Map<String,String> as the value. Second map has the message language as the key and the translated
	 *         message content as the value. (<i18n_message_key,<locale,message_content>>)
	 * @throws ModuleException
	 *             If any error occurs during operation
	 */
	public Map<String, Map<String, String>> getTranslatedMessages() throws ModuleException;

	/**
	 * Saves the translations passed.
	 * 
	 * @param translations
	 *            The translations to be saved in the format of <String,Map<String,String>>. First map has the
	 *            i18n_message_key as the key and a Map<String,String> as the value. Second map has the message language
	 *            as the key and the translated message content as the value.
	 *            (<i18n_message_key,<locale,message_content>>)
	 * @throws ModuleException
	 *             If any error occurs during operation
	 */
	public void saveTranslations(Map<String, Map<String, String>> translations) throws ModuleException;

}