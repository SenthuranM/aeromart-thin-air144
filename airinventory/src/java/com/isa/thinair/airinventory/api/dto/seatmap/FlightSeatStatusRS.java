package com.isa.thinair.airinventory.api.dto.seatmap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class FlightSeatStatusRS implements Serializable {

	private Map<String, FlightSeatStatusTO> seatStatusBySeatCode;
	// Holds reference to the 1st seat of each seat-row
	private Map<Integer, FlightSeatStatusTO> seatStatusByColId;

	public FlightSeatStatusRS() {
		seatStatusBySeatCode = new HashMap<String, FlightSeatStatusTO>();
		seatStatusByColId = new HashMap<Integer, FlightSeatStatusTO>();
	}

	public void addFlightSeatStatusTO(FlightSeatStatusTO flightSeatStatusTO) {
		seatStatusBySeatCode.put(flightSeatStatusTO.getSeatCode(), flightSeatStatusTO);
		if (!seatStatusByColId.containsKey(flightSeatStatusTO.getColId())) {
			seatStatusByColId.put(flightSeatStatusTO.getColId(), flightSeatStatusTO);
		}

	}

	public FlightSeatStatusTO getSeatStatus(String seatCode) {
		return seatStatusBySeatCode.get(seatCode);
	}

	public FlightSeatStatusTO getSeatStatus(int colId) {
		return seatStatusByColId.get(colId);
	}
}
