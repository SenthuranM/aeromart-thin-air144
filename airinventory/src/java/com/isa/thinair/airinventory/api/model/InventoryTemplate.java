/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 04, 2015 11:04:58
 * ===============================================================================
 */
package com.isa.thinair.airinventory.api.model;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_INV_TEMPLATE"
 * @author Priyantha
 * 
 */
public class InventoryTemplate extends Persistent implements Serializable {

	private static final long serialVersionUID = 710583538579751308L;

	private Integer invTempID;

	private String status;

	private String airCraftModel;

	private String segment;

	Set<InvTempCabinAlloc> invTempCabinAllocs;

	/**
	 * @return Returns the invTempID
	 * 
	 * @hibernate.id column = "INV_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_INV_TEMPLATE"
	 */

	public Integer getInvTempID() {
		return invTempID;
	}

	/**
	 * @param invTempID
	 * 
	 */
	public void setInvTempID(Integer invTempID) {
		this.invTempID = invTempID;
	}

	/**
	 *
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 *
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 *
	 * @return
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getAirCraftModel() {
		return airCraftModel;
	}

	/**
	 *
	 * @param airCraftModel
	 */
	public void setAirCraftModel(String airCraftModel) {
		this.airCraftModel = airCraftModel;
	}

	/**
	 *
	 * @return
	 * @hibernate.property column = "SEGMENT_CODE"
	 */
	public String getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 * 
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}

	/**
	 * @return
	 * 
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * 
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airinventory.api.model.InvTempCabinAlloc"
	 * 
	 * @hibernate.collection-key column="INV_TEMPLATE_ID"
	 */

	public Set<InvTempCabinAlloc> getInvTempCabinAllocs() {
		return invTempCabinAllocs;
	}

	/**
	 * @param invTempCabinAllocs
	 *            the invTempCabinAllocs to set
	 */
	public void setInvTempCabinAllocs(Set<InvTempCabinAlloc> invTempCabinAllocs) {
		this.invTempCabinAllocs = invTempCabinAllocs;
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getInvTempID()).toHashCode();
	}

}
