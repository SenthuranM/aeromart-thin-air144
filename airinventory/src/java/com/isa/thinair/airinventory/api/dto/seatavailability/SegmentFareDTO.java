package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.Collection;

public class SegmentFareDTO implements Serializable {

	private static final long serialVersionUID = -7432249164969752954L;

	private FareSummaryDTO fareSummaryDTO;

	private int fareType;

	private String ondCode;

	private int flightSegId;

	private String segmentCode;

	private Collection<SeatDistribution> seatDistributions;

	private FlightSegmentDTO flightSegmentDTO;

	private SegmentSeatDistsDTO segmentSeatDistsDTO;

	private boolean inbound;

	private int ondSequence;

	private int requestedOndSequence;

	/**
	 * @return Returns the fareSummaryDTO.
	 */
	public FareSummaryDTO getFareSummaryDTO() {
		return fareSummaryDTO;
	}

	/**
	 * @param fareSummaryDTO
	 *            The fareSummaryDTO to set.
	 */
	public void setFareSummaryDTO(FareSummaryDTO fareSummaryDTO) {
		this.fareSummaryDTO = fareSummaryDTO;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public Collection<SeatDistribution> getSeatDistributions() {
		return seatDistributions;
	}

	public void setSeatDistributions(Collection<SeatDistribution> seatDistributions) {
		this.seatDistributions = seatDistributions;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public FlightSegmentDTO getFlightSegmentDTO() {
		return flightSegmentDTO;
	}

	public void setFlightSegmentDTO(FlightSegmentDTO flightSegmentDTO) {
		this.flightSegmentDTO = flightSegmentDTO;
	}

	public SegmentSeatDistsDTO getSegmentSeatDistsDTO() {
		return segmentSeatDistsDTO;
	}

	public void setSegmentSeatDistsDTO(SegmentSeatDistsDTO segmentSeatDistsDTO) {
		this.segmentSeatDistsDTO = segmentSeatDistsDTO;
	}

	/**
	 * @return Returns the fareType.
	 */
	public int getFareType() {
		return fareType;
	}

	/**
	 * @param fareType
	 *            The fareType to set.
	 */
	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	public boolean isInbound() {
		return inbound;
	}

	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}
}
