package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;

public class SegmentDTO implements Serializable {

	private static final long serialVersionUID = -1090386192257071097L;
	private int segmentId;
	private String segmentCode;
	private boolean isInvalidSegment;
	private int flightId;
	private Collection<InterceptingFlightSegmentDTO> interceptingFlightSeg;

	public int getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(int segmentId) {
		this.segmentId = segmentId;
	}

	public Collection<InterceptingFlightSegmentDTO> getInterceptingFlightSeg() {
		return interceptingFlightSeg;
	}

	public void setInterceptingFlightSeg(Collection<InterceptingFlightSegmentDTO> interceptingFlightSeg) {
		this.interceptingFlightSeg = interceptingFlightSeg;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public boolean isInvalidSegment() {
		return isInvalidSegment;
	}

	public void setInvalidSegment(boolean isInvalidSegment) {
		this.isInvalidSegment = isInvalidSegment;
	}
	
	/**
	 * @return the flightId
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId the flightId to set
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}
}
