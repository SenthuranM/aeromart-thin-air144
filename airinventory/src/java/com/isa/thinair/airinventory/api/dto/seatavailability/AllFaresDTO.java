package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO.HRT_INC;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * DTO for quoting all the charges applicable for a segment of a journey
 * 
 * @author MN
 */
public class AllFaresDTO implements Serializable {

	private static final long serialVersionUID = 4497855024828032781L;

	/* key = segmentId, value = FlightSegmentDTO */
	@Deprecated
	private LinkedHashMap<Integer, FlightSegmentDTO> inboundFlightSegmentDTOs = null;

	/* key = segmentId, value = FlightSegmentDTO */
	@Deprecated
	private LinkedHashMap<Integer, FlightSegmentDTO> outboundFlightSegmentDTOs = null;

	/* key = ondSequence, value = FlightSegmentDTO */
	private LinkedHashMap<Integer, List<FlightSegmentDTO>> allFlightSegmentDTOs = null;

	/* Segment on which view all fares is based on */
	private Integer selectedSegmentId = null;

	/* key=segmentid value=SegmentFaresDTO */
	private LinkedHashMap<Integer, SegmentFaresDTO> segmentsFaresDTOMap = new LinkedHashMap<Integer, SegmentFaresDTO>();

	private HashMap<Integer, String> ondCodeFareTypeMap = new HashMap<Integer, String>();

	@Deprecated
	public LinkedHashMap<Integer, FlightSegmentDTO> getInboundFlightSegmentDTOs() {
		return inboundFlightSegmentDTOs;
	}

	@Deprecated
	public void setInboundFlightSegmentDTOs(LinkedHashMap<Integer, FlightSegmentDTO> inboundFlightSegmentDTOs) {
		this.inboundFlightSegmentDTOs = inboundFlightSegmentDTOs;
	}

	@Deprecated
	public void addInboundFlightSegmentDTO(FlightSegmentDTO flightSegmentDTO) {
		if (this.inboundFlightSegmentDTOs == null) {
			this.inboundFlightSegmentDTOs = new LinkedHashMap<Integer, FlightSegmentDTO>();
		}
		inboundFlightSegmentDTOs.put(flightSegmentDTO.getSegmentId(), flightSegmentDTO);
	}

	@Deprecated
	public LinkedHashMap<Integer, FlightSegmentDTO> getOutboundFlightSegmentDTOs() {
		return outboundFlightSegmentDTOs;
	}

	@Deprecated
	public void setOutboundFlightSegmentDTOs(LinkedHashMap<Integer, FlightSegmentDTO> outboundFlightSegmentDTOs) {
		this.outboundFlightSegmentDTOs = outboundFlightSegmentDTOs;
	}

	@Deprecated
	public void addOutboundFlightSegmentDTO(FlightSegmentDTO flightSegmentDTO) {
		if (this.outboundFlightSegmentDTOs == null) {
			this.outboundFlightSegmentDTOs = new LinkedHashMap<Integer, FlightSegmentDTO>();
		}
		outboundFlightSegmentDTOs.put(flightSegmentDTO.getSegmentId(), flightSegmentDTO);
	}

	public Map<Integer, List<FlightSegmentDTO>> getAllFlightSegmentDTOs() {
		if (allFlightSegmentDTOs == null) {
			allFlightSegmentDTOs = new LinkedHashMap<Integer, List<FlightSegmentDTO>>();
		}
		return allFlightSegmentDTOs;
	}

	public void setAllFlightSegmentDTOs(LinkedHashMap<Integer, List<FlightSegmentDTO>> allFlightSegmentDTOs) {
		this.allFlightSegmentDTOs = allFlightSegmentDTOs;
	}

	public void addAllFlightSegmentDTO(Integer sequence, FlightSegmentDTO flightSegmentDTO) {
		if (this.allFlightSegmentDTOs == null) {
			this.allFlightSegmentDTOs = new LinkedHashMap<Integer, List<FlightSegmentDTO>>();
		}

		if (!this.getAllFlightSegmentDTOs().containsKey(sequence)) {
			this.allFlightSegmentDTOs.put(sequence, new ArrayList<FlightSegmentDTO>());
		}

		this.allFlightSegmentDTOs.get(sequence).add(flightSegmentDTO);
	}

	public Collection<Integer> getOndFlightSegIds(int ondSequence) {
		Collection<Integer> fltSegIds = new ArrayList<Integer>();
		List<FlightSegmentDTO> fltSegList = getAllFlightSegmentDTOs().get(ondSequence);

		if (fltSegList != null && !fltSegList.isEmpty()) {
			for (FlightSegmentDTO flightSegmentDTO : fltSegList) {
				fltSegIds.add(flightSegmentDTO.getSegmentId());
			}
		}

		return fltSegIds;
	}

	public Integer getSelectedSegmentId() {
		return selectedSegmentId;
	}

	public void setSelectedSegmentId(Integer selectedSegmentId) {
		this.selectedSegmentId = selectedSegmentId;
	}

	public LinkedHashMap<Integer, SegmentFaresDTO> getSegmentsFaresDTOMap() {
		if (segmentsFaresDTOMap == null) {
			segmentsFaresDTOMap = new LinkedHashMap<Integer, SegmentFaresDTO>();
		}

		return segmentsFaresDTOMap;
	}

	public SegmentFaresDTO getSegmentFaresDTO(Integer segmentId) {
		if (!segmentsFaresDTOMap.containsKey(segmentId)) {
			segmentsFaresDTOMap.put(segmentId, new SegmentFaresDTO());
		}
		return segmentsFaresDTOMap.get(segmentId);
	}

	// public LinkedHashMap getSegmentFaresOfGivenType(Integer segmentId, Integer fareType){
	// return getSegmentFaresDTO(segmentId).getSegmentFaresOfSpecifiedFareType(fareType);
	// }

	public void setSegmentsFaresDTOMap(LinkedHashMap<Integer, SegmentFaresDTO> segmentsFaresDTOMap) {
		this.segmentsFaresDTOMap = segmentsFaresDTOMap;
	}

	private void addSegmentFaresOfSpecifiedType(Integer segmentId, Integer fareType,
			LinkedHashMap<String, AllFaresBCInventorySummaryDTO> allFaresBCInvetorySummaryDTOs) {
		getSegmentFaresDTO(segmentId).addSegmentsFares(allFaresBCInvetorySummaryDTOs, fareType);
	}

	public
			void
			addSegmentFaresOfSpecifiedType(
					Integer fareType,
					LinkedHashMap<Integer, LinkedHashMap<String, AllFaresBCInventorySummaryDTO>> allBucketOndFaresCombinationsForSegments) {
		Iterator<Integer> segmentsIt = allBucketOndFaresCombinationsForSegments.keySet().iterator();
		while (segmentsIt.hasNext()) {
			Integer segmentId = segmentsIt.next();
			addSegmentFaresOfSpecifiedType(segmentId, fareType, allBucketOndFaresCombinationsForSegments.get(segmentId));
		}

	}

	public void addOndCodeForFareType(Integer fareType, String ondCode) {
		ondCodeFareTypeMap.put(fareType, ondCode);
	}

	public String getOndCodeForFareType(Integer fareType) {
		return ondCodeFareTypeMap.get(fareType);
	}

	public HashMap<Integer, String> getOndCodeFareTypeMap() {
		return ondCodeFareTypeMap;
	}

	public void setOndCodeFareTypeMap(HashMap<Integer, String> ondCodeFareTypeMap) {
		this.ondCodeFareTypeMap = ondCodeFareTypeMap;
	}

	// ------------------------------------------------------------------------------------
	// Utility methods for the caller
	// ------------------------------------------------------------------------------------

	/**
	 * Returns a LinkedHashMap AllFaresBCInventorySummaryDTO objects keyed by bookingCode
	 */
	public LinkedHashMap<String, AllFaresBCInventorySummaryDTO> getSegmentFaresOfGivenType(Integer segmentId, Integer fareType) {
		LinkedHashMap<String, AllFaresBCInventorySummaryDTO> allFaresForSegment = getSegmentFaresDTO(segmentId)
				.getSegmentFaresOfSpecifiedFareType(fareType);

		if (allFaresForSegment != null && fareType.intValue() != FareTypes.PER_FLIGHT_FARE) {
			LinkedHashMap<String, AllFaresBCInventorySummaryDTO> faresForSegment = new LinkedHashMap<String, AllFaresBCInventorySummaryDTO>();
			Iterator<String> bcsForSegmentIt = allFaresForSegment.keySet().iterator();
			while (bcsForSegmentIt.hasNext()) {
				String bookingCode = bcsForSegmentIt.next();
				AllFaresBCInventorySummaryDTO bcInv = allFaresForSegment.get(bookingCode);
				if (fareType == FareTypes.HALF_RETURN_FARE) {
					if (AppSysParamsUtil.isAllowReturnParticipateInHalfReturn() || bcInv.isHRTFareExists()) {
						faresForSegment.put(bookingCode, bcInv.clone(HRT_INC.ONLY));
					}
				} else {
					if (bcInv.isBookingCodeCommonForAllSegments() || AppSysParamsUtil.showNestedAvailableSeats()) {
						faresForSegment.put(bookingCode, bcInv);
					}
				}
			}
			return faresForSegment;
		}

		return allFaresForSegment;
	}

	/**
	 * Returns true if atleast one non-marketing carrier segment present in the journey
	 * 
	 * @param marketingCarrierCode
	 * @return
	 */
	public boolean hasNonMarketingCarrierSegments(Collection<String> systemCarriers) {
		boolean hasNonMarketingCarrierSegs = false;

		if (this.allFlightSegmentDTOs != null && this.allFlightSegmentDTOs.size() > 0) {
			for (Entry<Integer, List<FlightSegmentDTO>> ondEntry : this.allFlightSegmentDTOs.entrySet()) {
				List<FlightSegmentDTO> flightSegList = ondEntry.getValue();
				if (flightSegList != null && flightSegList.size() > 0) {
					for (FlightSegmentDTO flightSegmentDTO : flightSegList) {
						if (!systemCarriers.contains(flightSegmentDTO.getCarrierCode())) {
							hasNonMarketingCarrierSegs = true;
							break;
						}
					}
				}
			}
		}

		return hasNonMarketingCarrierSegs;
	}

	/**
	 * Returns true if the change fare is for a journey having single segment.
	 * 
	 * @return
	 */
	public boolean isSingleSegmentFareQuote() {
		if (allFlightSegmentDTOs != null && allFlightSegmentDTOs.size() == 1
				&& allFlightSegmentDTOs.get(OndSequence.OUT_BOUND).size() == 1) {
			return true;
		}
		return false;
	}

	public List<FlightSegmentDTO> getAllFlightSegmentDTOList() {
		List<FlightSegmentDTO> allFlightSegments = new ArrayList<FlightSegmentDTO>();
		Collection<List<FlightSegmentDTO>> colValues = getAllFlightSegmentDTOs().values();

		if (colValues != null && !colValues.isEmpty()) {
			for (List<FlightSegmentDTO> lstFlightSegs : colValues) {
				if (lstFlightSegs != null && !lstFlightSegs.isEmpty()) {
					allFlightSegments.addAll(lstFlightSegs);
				}
			}
		}

		return allFlightSegments;
	}

	public FlightSegmentDTO getFlightSegmentDTO(int flightSegId) {
		List<FlightSegmentDTO> allFlightSegments = getAllFlightSegmentDTOList();

		if (!allFlightSegments.isEmpty()) {
			for (FlightSegmentDTO allFltSegDto : allFlightSegments) {
				if (allFltSegDto.getSegmentId() == flightSegId) {
					return allFltSegDto;
				}
			}
		}

		return new FlightSegmentDTO();
	}
}
