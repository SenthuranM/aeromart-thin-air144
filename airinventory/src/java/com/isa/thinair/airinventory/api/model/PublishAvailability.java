package com.isa.thinair.airinventory.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_PUBLISH_AVAILABILITY"
 */
public class PublishAvailability extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer pubSeatInvId;
	private Integer fccsaId;
	private Integer segAvailableSeats;

	private Integer fccsbaId;
	private Integer bcAvailableSeats;
	private String bcStatus;

	private Date eventTimestamp;
	private String eventStatus;
	private Date publishedTimestamp;
	private String eventCode;
	private int gdsID;
	private String bookingCode;
	private Integer flightSegId;

	/**
	 * @return the pubSeatInvId
	 * @hibernate.id column = "PUBLISH_AVAILABILITY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PUBLISH_AVAILABILITY"
	 */
	public Integer getPubSeatInvId() {
		return pubSeatInvId;
	}

	/**
	 * @param pubSeatInvId
	 *            the pubSeatInvId to set
	 */
	public void setPubSeatInvId(Integer pubSeatInvId) {
		this.pubSeatInvId = pubSeatInvId;
	}

	/**
	 * @return the eventCode
	 * @hibernate.property column = "EVENT_CODE"
	 */
	public String getEventCode() {
		return eventCode;
	}

	/**
	 * @param eventCode
	 *            the eventCode to set
	 */
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	/**
	 * @return the eventStatus
	 * @hibernate.property column = "EVENT_STATUS"
	 */
	public String getEventStatus() {
		return eventStatus;
	}

	/**
	 * @param eventStatus
	 *            the eventStatus to set
	 */
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	/**
	 * @return the eventTimestamp
	 * @hibernate.property column = "EVENT_TIMESTAMP"
	 */
	public Date getEventTimestamp() {
		return eventTimestamp;
	}

	/**
	 * @param eventTimestamp
	 *            the eventTimestamp to set
	 */
	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	/**
	 * @return the fccsbaId
	 * @hibernate.property column = "FCCSBA_ID"
	 */
	public Integer getFccsbaId() {
		return fccsbaId;
	}

	/**
	 * @param fccsbaId
	 *            the fccsbaId to set
	 */
	public void setFccsbaId(Integer fccsbaId) {
		this.fccsbaId = fccsbaId;
	}

	/**
	 * @return the publishedTimestamp
	 * @hibernate.property column = "PUBLISHED_TIMESTAMP"
	 */
	public Date getPublishedTimestamp() {
		return publishedTimestamp;
	}

	/**
	 * @param publishedTimestamp
	 *            the publishedTimestamp to set
	 */
	public void setPublishedTimestamp(Date publishedTimestamp) {
		this.publishedTimestamp = publishedTimestamp;
	}

	/**
	 * @return the segAvailableSeats
	 * @hibernate.property column = "SEG_AVAILABLE_SEATS"
	 */
	public Integer getSegAvailableSeats() {
		return segAvailableSeats;
	}

	/**
	 * @param segAvailableSeats
	 *            the segAvailableSeats to set
	 */
	public void setSegAvailableSeats(Integer segAvailableSeats) {
		this.segAvailableSeats = segAvailableSeats;
	}

	/**
	 * @return the bcAvailableSeats
	 * @hibernate.property column = "BC_AVAILABLE_SEATS"
	 */
	public Integer getBcAvailableSeats() {
		return bcAvailableSeats;
	}

	/**
	 * @param bcAvailableSeats
	 *            the bcAvailableSeats to set
	 */
	public void setBcAvailableSeats(Integer bcAvailableSeats) {
		this.bcAvailableSeats = bcAvailableSeats;
	}

	/**
	 * @return the bcStatus
	 * @hibernate.property column = "BC_STATUS"
	 */
	public String getBcStatus() {
		return bcStatus;
	}

	/**
	 * @param bcStatus
	 *            the bcStatus to set
	 */
	public void setBcStatus(String bcStatus) {
		this.bcStatus = bcStatus;
	}

	/**
	 * @return the fccsaId
	 * @hibernate.property column = "FCCSA_ID"
	 */
	public Integer getFccsaId() {
		return fccsaId;
	}

	/**
	 * @param fccsaId
	 *            the fccsaId to set
	 */
	public void setFccsaId(Integer fccsaId) {
		this.fccsaId = fccsaId;
	}

	/**
	 * @return the gdsID
	 * @hibernate.property column = "GDS_ID"
	 */
	public int getGdsID() {
		return gdsID;
	}

	public void setGdsID(int gdsID) {
		this.gdsID = gdsID;
	}

	/**
	 * @return the bookingCode
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return the flightSegId
	 * @hibernate.property column = "FLT_SEG_ID"
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

}
