package com.isa.thinair.airinventory.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.bl.EventPublisherBL;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airmaster.api.util.EventConstants;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.dto.BasicFlightDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.EventAuditor;

/**
 * 
 * 1. Keep track of moved flight segment inventories which are required to publications
 * 
 * 2. Delegate the Composed Business Objects to Event publication BL
 *
 * Current Intent : Publish data which are required to redis cache eviction
 *
 */
public class InventoryEventPublisher {

	private static final String OPERATION_CODE = "INVENTORY_EVENT";

	private boolean eventPublishable = false;
	Set<Integer> outboundFccSegInvIds = null;
	Set<Integer> inboundFccSegInvIds = null;

	public InventoryEventPublisher() {
		eventPublishable = AppSysParamsUtil.isEnableEventPublication();
		outboundFccSegInvIds = new HashSet<Integer>();
		inboundFccSegInvIds = new HashSet<Integer>();
	}

	public void filterAndAdd(String bookingClassType, boolean inbound, Integer fccSegInvId) {
		if (eventPublishable && bookingClassType != null && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
			if (inbound) {
				inboundFccSegInvIds.add(fccSegInvId);
			} else {
				outboundFccSegInvIds.add(fccSegInvId);
			}
		}
	}

	public void add(Integer fccSegInvId) {
		if (eventPublishable) {
			outboundFccSegInvIds.add(fccSegInvId);
		}
	}

	public void publishEvents() {
		try {
			if (eventPublishable) {
				List<BasicFlightDTO> publishableFlights = getPublishableFlights();

				if (!CollectionUtils.isEmpty(publishableFlights)) {
					EventPublisherBL.publishFlights(publishableFlights);
				}

			}
		} catch (Exception ex) {
			EventAuditor.audit(OPERATION_CODE, ex);
		}

	}

	private List<BasicFlightDTO> getPublishableFlights() throws ModuleException {
		List<BasicFlightDTO> publishableFlights = new ArrayList<BasicFlightDTO>();

		if (!CollectionUtils.isEmpty(outboundFccSegInvIds)) {
			List<Integer> flightIds = getFlightIdsByFccsInvIds(outboundFccSegInvIds);
			Collection<BasicFlightDTO> outboundFlights = getPublishableFlights(flightIds);
			if (!CollectionUtils.isEmpty(outboundFlights)) {
				publishableFlights.addAll(outboundFlights);
			}

		}

		if (!CollectionUtils.isEmpty(inboundFccSegInvIds)) {
			List<Integer> flightIds = getFlightIdsByFccsInvIds(inboundFccSegInvIds);
			Collection<BasicFlightDTO> inboundFlights = getPublishableFlights(flightIds);
			if (!CollectionUtils.isEmpty(inboundFlights)) {
				publishableFlights.addAll(inboundFlights);
			}
		}

		return publishableFlights;
	}

	private List<Integer> getFlightIdsByFccsInvIds(Set<Integer> fccsInvIds) {
		List<Integer> flightIds = null;
		Collection<FCCSegInventory> fccSegInventories = getFlightInventoryDAO().getFCCSegInventories(fccsInvIds);
		if (!CollectionUtils.isEmpty(fccSegInventories)) {
			flightIds = fccSegInventories.stream().map(FCCSegInventory::getFlightId).collect(Collectors.toList());
		}
		return flightIds;
	}

	private static Collection<BasicFlightDTO> getPublishableFlights(List<Integer> flightIds) throws ModuleException {
		Collection<BasicFlightDTO> publishableFlights = new ArrayList<BasicFlightDTO>();

		if (flightIds != null) {
			Collection<BasicFlightDTO> basicFlightDTOs = AirInventoryModuleUtils.getFlightBD().getBasicFlightDTOs(flightIds);
			publishableFlights.addAll(basicFlightDTOs);

			if (basicFlightDTOs.size() > 1) {
				BasicFlightDTO compositeFlight = getCompositeFlight(basicFlightDTOs);
				publishableFlights.add(compositeFlight);
			}
		}

		return publishableFlights;
	}

	private static BasicFlightDTO getCompositeFlight(Collection<BasicFlightDTO> basicFlightDTOs) {

		BasicFlightDTO firstFlight = BeanUtils.getFirstElement(basicFlightDTOs);
		BasicFlightDTO lastFlight = (BasicFlightDTO) BeanUtils.getLastElement(basicFlightDTOs);

		String journeyOrigin = firstFlight.getOrigin();
		String journeyDestination = lastFlight.getDestination();
		Date journeyStartDate = firstFlight.getLocalDepartureDate();

		String journeyFlightNumbers = firstFlight.getFlightNumber() + EventConstants.Delimeters.SEPARATOR
				+ lastFlight.getFlightNumber();

		BasicFlightDTO compositeFlight = new BasicFlightDTO();
		compositeFlight.setOrigin(journeyOrigin);
		compositeFlight.setDestination(journeyDestination);
		compositeFlight.setLocalDepartureDate(journeyStartDate);
		compositeFlight.setFlightNumber(journeyFlightNumbers);
		return compositeFlight;
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		return (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryDAOImplProxy");
	}

}
