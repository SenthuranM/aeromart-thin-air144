package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

public class CodeshareMcFlightDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String csMcCarrierCode;
	
	private String csMcFlightNumber;

	public String getCsMcCarrierCode() {
		return csMcCarrierCode;
	}

	public String getCsMcFlightNumber() {
		return csMcFlightNumber;
	}

	public void setCsMcCarrierCode(String csMcCarrierCode) {
		this.csMcCarrierCode = csMcCarrierCode;
	}

	public void setCsMcFlightNumber(String csMcFlightNumber) {
		this.csMcFlightNumber = csMcFlightNumber;
	}

}
