package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;

/**
 * Used for retrieving a flight cabin class inventory.
 * 
 * @author : MN
 */
public class FCCInventoryDTO implements Serializable {

	private static final long serialVersionUID = 8948559995745799911L;

	private int fccaInvId;

	private int flightId;

	private String cabinClassCode;

	private int capacity;

	private int infantCapacity;

	private String origin;

	private String destination;

	private LinkedHashMap<Integer, FCCSegInventoryDTO> fccSegInventoryDTOsMap;

	private Collection<FCCSegInventoryDTO> fccSegInventoryDTOs;

	public FCCInventoryDTO() {

	}

	public int getFccaInvId() {
		return fccaInvId;
	}

	public void setFccaInvId(int fccaInvId) {
		this.fccaInvId = fccaInvId;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	@SuppressWarnings("unchecked")
	public Collection<FCCSegInventoryDTO> getFccSegInventoryDTOs() {
		if (this.fccSegInventoryDTOs == null && getFccSegInventoryDTOsMap() != null) {
			setFccSegInventoryDTOs(AirinventoryUtils.getSearializableValues(getFccSegInventoryDTOsMap()));
		}
		return fccSegInventoryDTOs;
	}

	public void setFccSegInventoryDTOs(Collection<FCCSegInventoryDTO> fccSegInventoryDTOs) {
		this.fccSegInventoryDTOs = fccSegInventoryDTOs;
	}

	public LinkedHashMap<Integer, FCCSegInventoryDTO> getFccSegInventoryDTOsMap() {
		return fccSegInventoryDTOsMap;
	}

	public void setFccSegInventoryDTOsMap(LinkedHashMap<Integer, FCCSegInventoryDTO> fccSegInventoryDTOsMap) {
		this.fccSegInventoryDTOsMap = fccSegInventoryDTOsMap;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getInfantCapacity() {
		return infantCapacity;
	}

	public void setInfantCapacity(int infantCapacity) {
		this.infantCapacity = infantCapacity;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
