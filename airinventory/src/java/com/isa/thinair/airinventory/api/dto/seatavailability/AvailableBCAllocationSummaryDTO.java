package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;

import com.isa.thinair.commons.api.dto.PaxTypeTO;

/**
 * For extracting data retrieved in availability search
 * 
 * @author MN
 */
public class AvailableBCAllocationSummaryDTO implements Serializable {

	private static final long serialVersionUID = -8176243492729337647L;

	private String bookingCode;

	private boolean isStandardBC;

	private int nestRank;

	private boolean isFixedBC;

	private int noOfSeats;

	private String bookingClassType;

	private boolean isOnholdRestricted;

	private FareSummaryDTO fareSummaryDTO;

	private boolean isNestedBc = false;

	private boolean halfReturnApplicable;

	private boolean overbook;

	private boolean bookedFlightCabinBeingSearched;

	private boolean sameBookingClassAsExisting;

	private boolean flownOnd;

	private boolean isSearchedBC;

	private boolean unTouchedOnd;

	private String segmentCode;

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public boolean isFixedBC() {
		return isFixedBC;
	}

	public boolean isStandardBC() {
		return isStandardBC;
	}

	public int getNestRank() {
		return nestRank;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public void setFareSummaryDTO(FareSummaryDTO fareSummaryDTO) {
		this.fareSummaryDTO = fareSummaryDTO;
	}

	public FareSummaryDTO getFareSummaryDTO() {
		return fareSummaryDTO;
	}

	public void setFixedBC(boolean isFixedBC) {
		this.isFixedBC = isFixedBC;
	}

	public void setStandardBC(boolean isStandardBC) {
		this.isStandardBC = isStandardBC;
	}

	public void setNestRank(int nestRank) {
		this.nestRank = nestRank;
	}

	public String getBookingClassType() {
		return bookingClassType;
	}

	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	public StringBuffer getBucketSummary() {
		StringBuffer summary = new StringBuffer();
		summary.append("bookingcode=" + bookingCode).append("\n");
		summary.append("bc type=" + bookingClassType).append("\n");
		summary.append("standardFlag=" + isStandardBC).append("\n");
		summary.append("fixedFlag=" + isFixedBC).append("\n");
		summary.append("halfReturnApplicable=" + halfReturnApplicable).append("\n");
		summary.append("isOverbook=" + isOverbook()).append("\n");
		summary.append("isBookedFlightBeingSearched=" + isBookedFlightCabinBeingSearched()).append("\n");
		summary.append("isSameBookingClassAsExisting=" + isSameBookingClassAsExisting()).append("\n");
		summary.append("flownOnd=" + isFlownOnd()).append("\n");
		summary.append("unTouchedOnd=" + isUnTouchedOnd()).append("\n");
		if (fareSummaryDTO != null) {
			summary.append("fareId=" + fareSummaryDTO.getFareId()).append("\n");
			summary.append(" aduldFareAmount=" + fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)).append("\n");
			summary.append(" childFareAmount=" + fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD)).append("\n");
			summary.append(" infantFareAmount=" + fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT)).append("\n");
		}
		return summary;
	}

	public boolean isOnholdRestricted() {
		return isOnholdRestricted;
	}

	public void setOnholdRestricted(boolean isOnholdRestricted) {
		this.isOnholdRestricted = isOnholdRestricted;
	}

	/**
	 * @return the isSearchedBC
	 */
	public boolean isSearchedBC() {
		return isSearchedBC;
	}

	/**
	 * @param isSearchedBC
	 *            the isSearchedBC to set
	 */
	public void setSearchedBC(boolean isSearchedBC) {
		this.isSearchedBC = isSearchedBC;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime + (bookingCode == null ? 0 : bookingCode.hashCode());
		result *= prime + (fareSummaryDTO == null ? 0 : String.valueOf(fareSummaryDTO.getFareId()).hashCode());
		return result;
	}

	@Override
	public boolean equals(Object object) {
		if (getClass() != object.getClass() || fareSummaryDTO == null || bookingCode == null) {
			return false;
		}
		AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO = (AvailableBCAllocationSummaryDTO) object;
		if (bookingCode.equals(availableBCAllocationSummaryDTO.getBookingCode())
				&& fareSummaryDTO.getFareId() == availableBCAllocationSummaryDTO.getFareSummaryDTO().getFareId()) {
			return true;
		}
		return false;
	}

	public boolean isNestedBc() {
		return isNestedBc;
	}

	public void setNestedBc(boolean isNestedBc) {
		this.isNestedBc = isNestedBc;
	}

	public void setHalfReturnApplicable(boolean halfReturnApplicable) {
		this.halfReturnApplicable = halfReturnApplicable;
	}

	public boolean isHalfReturnApplicable() {
		return halfReturnApplicable;
	}

	@Override
	protected AvailableBCAllocationSummaryDTO clone() {
		AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO = new AvailableBCAllocationSummaryDTO();
		availableBCAllocationSummaryDTO.setBookingClassType(bookingClassType);
		availableBCAllocationSummaryDTO.setBookingCode(bookingCode);
		availableBCAllocationSummaryDTO.setFareSummaryDTO(fareSummaryDTO.clone());
		availableBCAllocationSummaryDTO.setFixedBC(isFixedBC);
		availableBCAllocationSummaryDTO.setNestedBc(isNestedBc);
		availableBCAllocationSummaryDTO.setNestRank(nestRank);
		availableBCAllocationSummaryDTO.setNoOfSeats(noOfSeats);
		availableBCAllocationSummaryDTO.setOnholdRestricted(isOnholdRestricted);
		availableBCAllocationSummaryDTO.setStandardBC(isStandardBC);
		availableBCAllocationSummaryDTO.setHalfReturnApplicable(halfReturnApplicable);
		availableBCAllocationSummaryDTO.setOverbook(this.isOverbook());
		availableBCAllocationSummaryDTO.setBookedFlightCabinSearch(isBookedFlightCabinBeingSearched());
		availableBCAllocationSummaryDTO.setSameBookingClassAsExisting(isSameBookingClassAsExisting());
		availableBCAllocationSummaryDTO.setFlownOnd(isFlownOnd());
		availableBCAllocationSummaryDTO.setUnTouchedOnd(isUnTouchedOnd());
		availableBCAllocationSummaryDTO.setSegmentCode(segmentCode);
		return availableBCAllocationSummaryDTO;
	}

	public void setOverbook(boolean overbook) {
		this.overbook = overbook;
	}

	public boolean isOverbook() {
		return overbook;
	}

	public void setBookedFlightCabinSearch(boolean bookedFlightCabinBeingSearched) {
		this.bookedFlightCabinBeingSearched = bookedFlightCabinBeingSearched;
	}

	public boolean isBookedFlightCabinBeingSearched() {
		return bookedFlightCabinBeingSearched;
	}

	public void setSameBookingClassAsExisting(boolean sameBookingClassAsExisting) {
		this.sameBookingClassAsExisting = sameBookingClassAsExisting;
	}

	public boolean isSameBookingClassAsExisting() {
		return sameBookingClassAsExisting;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public boolean isUnTouchedOnd() {
		return unTouchedOnd;
	}

	public void setUnTouchedOnd(boolean unTouchedOnd) {
		this.unTouchedOnd = unTouchedOnd;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
}
