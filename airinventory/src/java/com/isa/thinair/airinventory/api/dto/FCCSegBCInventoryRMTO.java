package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Carries data needed for RM optimization alerting.
 * 
 * @author Nasly
 * 
 */
public class FCCSegBCInventoryRMTO implements Serializable {

	private static final long serialVersionUID = -9012277110565813565L;

	private Integer fccsaId;

	private String cabinClassCode;

	private String flightNumber;

	private Date departureDateZulu;

	private String segmentCode;

	private BigDecimal minimumFare;

	private long version;

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public void setDepartureDateZulu(Date departureDateLocal) {
		this.departureDateZulu = departureDateLocal;
	}

	public Integer getFccsaId() {
		return fccsaId;
	}

	public void setFccsaId(Integer fccsaId) {
		this.fccsaId = fccsaId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public BigDecimal getMinimumFare() {
		return minimumFare;
	}

	public void setMinimumFare(BigDecimal minimumFare) {
		this.minimumFare = minimumFare;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
}
