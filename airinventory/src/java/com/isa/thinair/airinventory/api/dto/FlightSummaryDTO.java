package com.isa.thinair.airinventory.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.meal.FlightMealClassDTO;

public class FlightSummaryDTO implements Serializable {

	private static final long serialVersionUID = -1587347768234800956L;
	private Integer flightId;
	private String flightNumber;
	private Date departureDateTimeLocal;
	private Date arrivalDateTimeLocal;
	private Date departureDateTimeZulu;
	private Date arrivalDateTimeZulu;
	private String origin;
	private String destination;

	private List<FlightMealClassDTO> flightMealClassDTOs;

	private Map<String, FlightMealClassDTO> mapCabinClassFlightMealClassDTO;

	/**
	 * @return the flightId
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the departureDateTimeLocal
	 */
	public Date getDepartureDateTimeLocal() {
		return departureDateTimeLocal;
	}

	/**
	 * @param departureDateTimeLocal
	 *            the departureDateTimeLocal to set
	 */
	public void setDepartureDateTimeLocal(Date departureDateTimeLocal) {
		this.departureDateTimeLocal = departureDateTimeLocal;
	}

	/**
	 * @return the arrivalDateTimeLocal
	 */
	public Date getArrivalDateTimeLocal() {
		return arrivalDateTimeLocal;
	}

	/**
	 * @param arrivalDateTimeLocal
	 *            the arrivalDateTimeLocal to set
	 */
	public void setArrivalDateTimeLocal(Date arrivalDateTimeLocal) {
		this.arrivalDateTimeLocal = arrivalDateTimeLocal;
	}

	/**
	 * @return the departureDateTimeZulu
	 */
	public Date getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	/**
	 * @param departureDateTimeZulu
	 *            the departureDateTimeZulu to set
	 */
	public void setDepartureDateTimeZulu(Date departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	/**
	 * @return the arrivalDateTimeZulu
	 */
	public Date getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	/**
	 * @param arrivalDateTimeZulu
	 *            the arrivalDateTimeZulu to set
	 */
	public void setArrivalDateTimeZulu(Date arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	public List<FlightMealClassDTO> getFlightMealClassDTOs() {
		return flightMealClassDTOs;
	}

	public void setFlightMealClassDTOs(List<FlightMealClassDTO> flightMealClassDTOs) {
		this.flightMealClassDTOs = flightMealClassDTOs;
	}

	public Map<String, FlightMealClassDTO> getMapCabinClassFlightMealClassDTO() {
		return mapCabinClassFlightMealClassDTO;
	}

	public void setMapCabinClassFlightMealClassDTO(Map<String, FlightMealClassDTO> mapCabinClassFlightMealClassDTO) {
		this.mapCabinClassFlightMealClassDTO = mapCabinClassFlightMealClassDTO;
	}
}
