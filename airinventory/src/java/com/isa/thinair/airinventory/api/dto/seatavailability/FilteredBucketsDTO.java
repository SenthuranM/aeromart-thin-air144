package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;

/**
 * Used for storing fare/inventory information in availability search
 * 
 * @author MN
 */
public class FilteredBucketsDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestStandardBuckets = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseExternalBCAvailability = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestNonStandardBuckets = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestFixedBuckets = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestStandardBucketsForHRT = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestFixedBucketsForHRT = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();
	// Following will be set after minimum fare calculation
	private String checpestFareLogicalCabinClass;
	private final Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestBCAllocationSummaryDTOs = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();

	public void addBucket(String logicalCabinClass, AvailableBCAllocationSummaryDTO bcAllocationSummaryDTO, boolean standardFlag,
			boolean fixedFlag, boolean cheapest, boolean halfRTFlag) {
		if (standardFlag) {
			if (!halfRTFlag) {
				if (logicalCCWiseCheapestStandardBuckets.get(logicalCabinClass) == null) {
					logicalCCWiseCheapestStandardBuckets.put(logicalCabinClass, new ArrayList<AvailableBCAllocationSummaryDTO>());
				}
				logicalCCWiseCheapestStandardBuckets.get(logicalCabinClass).add(bcAllocationSummaryDTO);
			} else {
				if (logicalCCWiseCheapestStandardBucketsForHRT.get(logicalCabinClass) == null) {
					logicalCCWiseCheapestStandardBucketsForHRT.put(logicalCabinClass, new ArrayList<AvailableBCAllocationSummaryDTO>());
				}
				logicalCCWiseCheapestStandardBucketsForHRT.get(logicalCabinClass).add(bcAllocationSummaryDTO);
			}
		} else if (fixedFlag) {
			if (!halfRTFlag) {
				if (logicalCCWiseCheapestFixedBuckets.get(logicalCabinClass) == null) {
					logicalCCWiseCheapestFixedBuckets.put(logicalCabinClass, new ArrayList<AvailableBCAllocationSummaryDTO>());
				}
				logicalCCWiseCheapestFixedBuckets.get(logicalCabinClass).add(bcAllocationSummaryDTO);
			} else {
				if (logicalCCWiseCheapestFixedBucketsForHRT.get(logicalCabinClass) == null) {
					logicalCCWiseCheapestFixedBucketsForHRT.put(logicalCabinClass, new ArrayList<AvailableBCAllocationSummaryDTO>());
				}
				logicalCCWiseCheapestFixedBucketsForHRT.get(logicalCabinClass).add(bcAllocationSummaryDTO);				
			}
		} else if (cheapest) {
			if (logicalCCWiseCheapestBCAllocationSummaryDTOs.get(logicalCabinClass) == null) {
				logicalCCWiseCheapestBCAllocationSummaryDTOs.put(logicalCabinClass,
						new ArrayList<AvailableBCAllocationSummaryDTO>());
			}
			logicalCCWiseCheapestBCAllocationSummaryDTOs.get(logicalCabinClass).add(bcAllocationSummaryDTO);
		} else {
			if (logicalCCWiseCheapestNonStandardBuckets.get(logicalCabinClass) == null) {
				logicalCCWiseCheapestNonStandardBuckets.put(logicalCabinClass, new ArrayList<AvailableBCAllocationSummaryDTO>());
			}
			logicalCCWiseCheapestNonStandardBuckets.get(logicalCabinClass).add(bcAllocationSummaryDTO);
		}
	}

	public List<AvailableBCAllocationSummaryDTO> getBucket(String logicalCabinClass, boolean standardFlag, boolean fixedFlag, boolean halfRTFlag) {
		List<AvailableBCAllocationSummaryDTO> allocationSummaryDTOs;
		if (standardFlag) {
			if (!halfRTFlag) {
				allocationSummaryDTOs = logicalCCWiseCheapestStandardBuckets.get(logicalCabinClass);
			} else {
				allocationSummaryDTOs = logicalCCWiseCheapestStandardBucketsForHRT.get(logicalCabinClass);
			}
		} else if (fixedFlag) {
			if (!halfRTFlag) {
				allocationSummaryDTOs = logicalCCWiseCheapestFixedBuckets.get(logicalCabinClass);
			} else {
				allocationSummaryDTOs = logicalCCWiseCheapestFixedBucketsForHRT.get(logicalCabinClass);
			}
		} else {
			allocationSummaryDTOs = logicalCCWiseCheapestNonStandardBuckets.get(logicalCabinClass);
		}
		if (!AirInventoryDataExtractUtil.isCollectionNullOrEmpty(allocationSummaryDTOs)) {
			return allocationSummaryDTOs;
		} else {
			return new ArrayList<AvailableBCAllocationSummaryDTO>();
		}
	}

	public AvailableBCAllocationSummaryDTO getBucket(String bookingClass, String logicalCabinClass, boolean standardFlag,
			boolean fixedFlag, int fareId) {
		List<AvailableBCAllocationSummaryDTO> allocationSummaryDTOs = getBucket(logicalCabinClass, standardFlag, fixedFlag, false);
		for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : allocationSummaryDTOs) {
			if (availableBCAllocationSummaryDTO.getBookingCode().equals(bookingClass) && fareId == availableBCAllocationSummaryDTO.getFareSummaryDTO().getFareId()) {
				return availableBCAllocationSummaryDTO;
			}
		}
		return null;
	}

	public FilteredBucketsDTO getOnlyHalfReturnBucketsDTO() {
		FilteredBucketsDTO hrtBucket = this.cloneWithCopyingHRTtoRT();
		hrtBucket.reduceToHalfReturnBuckets();
		return hrtBucket;
	}
	
	public FilteredBucketsDTO getOnlyReturnBucketsDTO() {
		FilteredBucketsDTO rtBucket = this.cloneWithoutCopyingHRT();
		return rtBucket;
	}

	private boolean reduceToHalfReturnBuckets() {
		List<Map<String, List<AvailableBCAllocationSummaryDTO>>> bucketsList = new ArrayList<Map<String, List<AvailableBCAllocationSummaryDTO>>>();
		bucketsList.add(logicalCCWiseCheapestStandardBuckets);
		bucketsList.add(logicalCCWiseExternalBCAvailability);
		bucketsList.add(logicalCCWiseCheapestNonStandardBuckets);
		bucketsList.add(logicalCCWiseCheapestFixedBuckets);
		boolean hasBuckets = false;
		for (Map<String, List<AvailableBCAllocationSummaryDTO>> bcTypeBuckets : bucketsList) {
			Iterator<Entry<String, List<AvailableBCAllocationSummaryDTO>>> bcTypeBucketItr = bcTypeBuckets.entrySet().iterator();
			while (bcTypeBucketItr.hasNext()) {
				Entry<String, List<AvailableBCAllocationSummaryDTO>> logiCCAllocItr = bcTypeBucketItr.next();
				Iterator<AvailableBCAllocationSummaryDTO> allocItr = logiCCAllocItr.getValue().iterator();
				while (allocItr.hasNext()) {
					if (!allocItr.next().isHalfReturnApplicable()) {
						allocItr.remove();
					}
				}
				if (logiCCAllocItr.getValue().size() == 0) {
					bcTypeBucketItr.remove();
				} else {
					hasBuckets = true;
				}
			}
		}
		return hasBuckets;
	}

	public Collection<AvailableBCAllocationSummaryDTO> getExternalBookingClassAvailability(String logicalCabinClass) {
		return logicalCCWiseExternalBCAvailability.get(logicalCabinClass);
	}

	public Collection<String> getExternalBookingClassAvailability() {
		if (logicalCCWiseExternalBCAvailability != null) {
			return logicalCCWiseExternalBCAvailability.keySet();
		}
		return new HashSet<String>();
	}

	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseCheapestStandardBuckets() {
		return logicalCCWiseCheapestStandardBuckets;
	}
	
	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseCheapestStandardBucketsForHRT() {
		return logicalCCWiseCheapestStandardBucketsForHRT;
	}

	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseExternalBCAvailability() {
		return logicalCCWiseExternalBCAvailability;
	}

	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseCheapestNonStandardBuckets() {
		return logicalCCWiseCheapestNonStandardBuckets;
	}

	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseCheapestFixedBuckets() {
		return logicalCCWiseCheapestFixedBuckets;
	}
	
	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseCheapestFixedBucketsForHRT() {
		return logicalCCWiseCheapestFixedBucketsForHRT;
	}

	public void addExternalBookingClassAvailability(String logicalCabinClass,
			AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO) {
		if (logicalCCWiseExternalBCAvailability.get(logicalCabinClass) == null) {
			logicalCCWiseExternalBCAvailability.put(logicalCabinClass, new ArrayList<AvailableBCAllocationSummaryDTO>());
		}
		logicalCCWiseExternalBCAvailability.get(logicalCabinClass).add(availableBCAllocationSummaryDTO);
	}

	public boolean hasFares(String logicalCabinClass, Integer fareId, boolean stdFlag, boolean fixedFlag) {
		AvailableBCAllocationSummaryDTO bucket = null;
		if (stdFlag) {
			List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs = logicalCCWiseCheapestStandardBuckets
					.get(logicalCabinClass);
			if (AirInventoryDataExtractUtil.isCollectionNullOrEmpty(availableBCAllocationSummaryDTOs)) {
				bucket = availableBCAllocationSummaryDTOs.get(availableBCAllocationSummaryDTOs.size() - 1);
			}
		} else if (fixedFlag) {
			List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs = logicalCCWiseCheapestFixedBuckets
					.get(logicalCabinClass);
			if (AirInventoryDataExtractUtil.isCollectionNullOrEmpty(availableBCAllocationSummaryDTOs)) {
				bucket = availableBCAllocationSummaryDTOs.get(availableBCAllocationSummaryDTOs.size() - 1);
			}
		} else {
			List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs = logicalCCWiseCheapestNonStandardBuckets
					.get(logicalCabinClass);
			if (AirInventoryDataExtractUtil.isCollectionNullOrEmpty(availableBCAllocationSummaryDTOs)) {
				bucket = availableBCAllocationSummaryDTOs.get(availableBCAllocationSummaryDTOs.size() - 1);
			}
		}

		if (bucket != null && bucket.getFareSummaryDTO().getFareId() == fareId.intValue()) {
			return true;
		} else {
			return false;
		}
	}

	public Set<String> getAvailableLogicalCabinClasses() {
		Set<String> availableLogicalCabinClasses = new HashSet<String>();
		availableLogicalCabinClasses.addAll(logicalCCWiseCheapestStandardBuckets.keySet());
		availableLogicalCabinClasses.addAll(logicalCCWiseCheapestNonStandardBuckets.keySet());
		availableLogicalCabinClasses.addAll(logicalCCWiseCheapestFixedBuckets.keySet());
		availableLogicalCabinClasses.addAll(logicalCCWiseExternalBCAvailability.keySet());
		return availableLogicalCabinClasses;
	}

	public String getChecpestFareLogicalCabinClass() {
		return checpestFareLogicalCabinClass;
	}

	public void setChecpestFareLogicalCabinClass(String checpestFareLogicalCabinClass) {
		this.checpestFareLogicalCabinClass = checpestFareLogicalCabinClass;
	}

	public Map<String, List<AvailableBCAllocationSummaryDTO>> getLogicalCCWiseCheapestBCAllocationSummaryDTOs() {
		return logicalCCWiseCheapestBCAllocationSummaryDTOs;
	}

	public List<AvailableBCAllocationSummaryDTO> getCheapestBCAllocationSummaryDTOs() {
		List<AvailableBCAllocationSummaryDTO> cheapestBCAllocationSummaryDTOs = logicalCCWiseCheapestBCAllocationSummaryDTOs
				.get(checpestFareLogicalCabinClass);
		if (!AirInventoryDataExtractUtil.isCollectionNullOrEmpty(cheapestBCAllocationSummaryDTOs)) {
			return cheapestBCAllocationSummaryDTOs;
		}
		return new ArrayList<AvailableBCAllocationSummaryDTO>();
	}

	@Override
	public FilteredBucketsDTO clone() {
		FilteredBucketsDTO filteredBucketsDTO = new FilteredBucketsDTO();
		filteredBucketsDTO.setChecpestFareLogicalCabinClass(checpestFareLogicalCabinClass);
		for (String availableLogicalCabinClass : logicalCCWiseCheapestBCAllocationSummaryDTOs.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestBCAllocationSummaryDTOs
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, false,
						true, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseExternalBCAvailability.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseExternalBCAvailability
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addExternalBookingClassAvailability(availableLogicalCabinClass,
						availableBCAllocationSummaryDTO.clone());
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestFixedBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestFixedBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, true,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestFixedBucketsForHRT.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestFixedBucketsForHRT
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, true,
						false, true);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestNonStandardBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestNonStandardBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, false,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestStandardBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestStandardBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), true, false,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestStandardBucketsForHRT.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestStandardBucketsForHRT
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), true, false,
						false, true);
			}
		}
		return filteredBucketsDTO;
	}
	
	public FilteredBucketsDTO cloneWithCopyingHRTtoRT() {
		FilteredBucketsDTO filteredBucketsDTO = new FilteredBucketsDTO();
		filteredBucketsDTO.setChecpestFareLogicalCabinClass(checpestFareLogicalCabinClass);
		for (String availableLogicalCabinClass : logicalCCWiseCheapestBCAllocationSummaryDTOs.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestBCAllocationSummaryDTOs
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, false,
						true, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseExternalBCAvailability.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseExternalBCAvailability
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addExternalBookingClassAvailability(availableLogicalCabinClass,
						availableBCAllocationSummaryDTO.clone());
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestFixedBucketsForHRT.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestFixedBucketsForHRT
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, true,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestNonStandardBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestNonStandardBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, false,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestStandardBucketsForHRT.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestStandardBucketsForHRT
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), true, false,
						false, false);
			}
		}
		return filteredBucketsDTO;
	}
	
	public FilteredBucketsDTO cloneWithoutCopyingHRT() {
		FilteredBucketsDTO filteredBucketsDTO = new FilteredBucketsDTO();
		filteredBucketsDTO.setChecpestFareLogicalCabinClass(checpestFareLogicalCabinClass);
		for (String availableLogicalCabinClass : logicalCCWiseCheapestBCAllocationSummaryDTOs.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestBCAllocationSummaryDTOs
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, false,
						true, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseExternalBCAvailability.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseExternalBCAvailability
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addExternalBookingClassAvailability(availableLogicalCabinClass,
						availableBCAllocationSummaryDTO.clone());
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestFixedBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestFixedBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, true,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestNonStandardBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestNonStandardBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), false, false,
						false, false);
			}
		}
		for (String availableLogicalCabinClass : logicalCCWiseCheapestStandardBuckets.keySet()) {
			for (AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO : logicalCCWiseCheapestStandardBuckets
					.get(availableLogicalCabinClass)) {
				filteredBucketsDTO.addBucket(availableLogicalCabinClass, availableBCAllocationSummaryDTO.clone(), true, false,
						false, false);
			}
		}
		return filteredBucketsDTO;
	}

}
