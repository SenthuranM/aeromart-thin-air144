package com.isa.thinair.airinventory.api.dto.seatavailability;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;

/**
 * DTO used to capture all the information needed to reliably recreate the ondFareDTO
 * 
 * @author mekanayake
 */
public class OnDFareSegChargesRebuildCriteria implements Serializable {

	private static final long serialVersionUID = -2203145340576004887L;

	/**
	 * For fare rebuild
	 */
	private Integer fareID;
	private Integer farePercentage;

	/** Ond wise FareType . This will not indicate the fare type of the total journey */
	private Integer fareType;

	/**
	 * direction indicators
	 */
	private boolean isInbound;

	// for half return modification of inbound
	private boolean isModifyInbound;

	/** Open return indicator */
	private boolean isOpenReturn;

	/**
	 * Segments the ond is made up of key=fsegmentId, value=Collection of BC with no of allocations. Order of the
	 * fsegmentId : "departure date zulu" Order of BookingClassAllocs : "Nest Rank"
	 */
	private LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc;

	/**
	 * For rebuilding chargers
	 */
	private Collection<Integer> chargeRateIds;

	/**
	 * For rebuilding flexi
	 */
	private Collection<Integer> flexiChargeIds;

	private Collection<QuotedChargeDTO> charges;

	private FareTO fareTO;

	private int ondSequence;

	/** Flag to calculate agent commission or not **/
	private boolean calculateAgentCommission;

	private int subJourneyGroup = -1;

	private Map<String, Double> totalSubJourneyFare;

	private BundledFareDTO bundledFareDTO;

	private Map<String, BigDecimal> overridePaxFares;

	private int requestedOndSequence;
	
	private boolean splitOperationForBusInvoked;

	/**
	 * @return the fareID
	 */
	public Integer getFareID() {
		return fareID;
	}

	/**
	 * @param fareID
	 *            the fareID to set
	 */
	public void setFareID(Integer fareID) {
		this.fareID = fareID;
	}

	/**
	 * @return the farePercentage
	 */
	public Integer getFarePercentage() {
		return farePercentage;
	}

	/**
	 * @param farePercentage
	 *            the farePercentage to set
	 */
	public void setFarePercentage(Integer farePercentage) {
		this.farePercentage = farePercentage;
	}

	/**
	 * @return the fareType
	 */
	public Integer getFareType() {
		return fareType;
	}

	/**
	 * @param fareType
	 *            the fareType to set
	 */
	public void setFareType(Integer fareType) {
		this.fareType = fareType;
	}

	/**
	 * @return the isOpenReturn
	 */
	public boolean isOpenReturn() {
		return isOpenReturn;
	}

	/**
	 * @param isOpenReturn
	 *            the isOpenReturn to set
	 */
	public void setOpenReturn(boolean isOpenReturn) {
		this.isOpenReturn = isOpenReturn;
	}

	/**
	 * @return the isInbound
	 */
	public boolean isInbound() {
		return isInbound;
	}

	/**
	 * @param isInbound
	 *            the isInbound to set
	 */
	public void setInbound(boolean isInbound) {
		this.isInbound = isInbound;
	}

	/**
	 * @return the isModifyInbound
	 */
	public boolean isModifyInbound() {
		return isModifyInbound;
	}

	/**
	 * @param isModifyInbound
	 *            the isModifyInbound to set
	 */
	public void setModifyInbound(boolean isModifyInbound) {
		this.isModifyInbound = isModifyInbound;
	}

	/**
	 * @return the fsegBCAlloc
	 */
	public LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> getFsegBCAlloc() {
		return fsegBCAlloc;
	}

	/**
	 * @param fsegBCAlloc
	 *            the fsegBCAlloc to set
	 */
	public void setFsegBCAlloc(LinkedHashMap<Integer, LinkedList<BookingClassAlloc>> fsegBCAlloc) {
		this.fsegBCAlloc = fsegBCAlloc;
	}

	public Collection<Integer> getFlightSegIds() {
		if (fsegBCAlloc != null) {
			return fsegBCAlloc.keySet();
		}
		return Collections.emptyList();
	}

	/**
	 * @return the chargeRateIds
	 */
	public Collection<Integer> getChargeRateIds() {
		return chargeRateIds;
	}

	/**
	 * @param chargeRateIds
	 *            the chargeRateIds to set
	 */
	public void setChargeRateIds(Collection<Integer> chargeRateIds) {
		this.chargeRateIds = chargeRateIds;
	}

	/**
	 * @return the flexiChargersId
	 */
	public Collection<Integer> getFlexiChargeIds() {
		return flexiChargeIds;
	}

	/**
	 * @param flexiChargeIds
	 *            the flexiChargersId to set
	 */
	public void setFlexiChargeIds(Collection<Integer> flexiChargeIds) {
		this.flexiChargeIds = flexiChargeIds;
	}

	public void setCharges(Collection<QuotedChargeDTO> charges) {
		this.charges = charges;
	}

	public void setFareTO(FareTO fareTO) {
		this.fareTO = fareTO;
	}

	public Collection<QuotedChargeDTO> getCharges() {
		return charges;
	}

	public FareTO getFareTO() {
		return fareTO;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public boolean isCalculateAgentCommission() {
		return calculateAgentCommission;
	}

	public void setCalculateAgentCommission(boolean calculateAgentCommission) {
		this.calculateAgentCommission = calculateAgentCommission;
	}

	public int getSubJourneyGroup() {
		return subJourneyGroup;
	}

	public void setSubJourneyGroup(int subJourneyGroup) {
		this.subJourneyGroup = subJourneyGroup;
	}

	public Map<String, Double> getTotalSubJourneyFare() {
		if (totalSubJourneyFare == null) {
			totalSubJourneyFare = new HashMap<String, Double>();
		}

		return totalSubJourneyFare;
	}

	public void setTotalSubJourneyFare(Map<String, Double> totalSubJourneyFare) {
		this.totalSubJourneyFare = totalSubJourneyFare;
	}

	public BundledFareDTO getBundledFareDTO() {
		return bundledFareDTO;
	}

	public void setBundledFareDTO(BundledFareDTO bundledFareDTO) {
		this.bundledFareDTO = bundledFareDTO;
	}

	public void setOverridePaxFares(Map<String, BigDecimal> overridePaxFares) {
		this.overridePaxFares = overridePaxFares;
	}

	public Map<String, BigDecimal> getOverridePaxFares() {
		return overridePaxFares;
	}

	public int getRequestedOndSequence() {
		return requestedOndSequence;
	}

	public void setRequestedOndSequence(int requestedOndSequence) {
		this.requestedOndSequence = requestedOndSequence;
	}

	public boolean isSplitOperationForBusInvoked() {
		return splitOperationForBusInvoked;
	}

	public void setSplitOperationForBusInvoked(boolean isSplitOperationForBusInvoked) {
		this.splitOperationForBusInvoked = isSplitOperationForBusInvoked;
	}

	
	
}
