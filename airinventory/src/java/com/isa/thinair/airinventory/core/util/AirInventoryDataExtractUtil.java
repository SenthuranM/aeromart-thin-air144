package com.isa.thinair.airinventory.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.ExternalFareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketCheapestBCDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.InvTempCabinAlloc;
import com.isa.thinair.airinventory.api.model.InvTempCabinBCAlloc;
import com.isa.thinair.airinventory.api.model.InventoryTemplate;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class AirInventoryDataExtractUtil {
	private final static Log log = LogFactory.getLog(AirInventoryDataExtractUtil.class);

	public static FilteredBucketsDTO extractFilteredBucketsDTO(ResultSet resultSet,
			BucketFilteringCriteria bucketFilteringCriteria, boolean allowNesting) throws SQLException, DataAccessException {
		FilteredBucketsDTO filteredBucketsDTO = new FilteredBucketsDTO();
		int cummulativeStandardSeatsCount = 0;
		int cummulativeStandardSeatsCountForHRT = 0;
		int overbookedSeatsCount = 0;
		int overbookedSeatsCountForHRT = 0;
		Map<String, Boolean> logicalCCWiseCheapestStandardBCFound = new HashMap<String, Boolean>();
		Map<String, Boolean> logicalCCWiseCheapestStandardBCFoundForHRT = new HashMap<String, Boolean>();
		Map<String, Boolean> logicalCCWiseCheapestNonStandardBCFound = new HashMap<String, Boolean>();
		Map<String, Boolean> logicalCCWiseCheapestFixedBCFound = new HashMap<String, Boolean>();
		Map<String, Boolean> logicalCCWiseCheapestFixedBCFoundForHRT = new HashMap<String, Boolean>();
		String lastBookingCode = "";
		String lastLogicalCabinClass = "";
		boolean isExternalBookingClass = false;
		boolean skipNormalSearchDuringIntlnAvailSearch = false;
		int requestedSeats = bucketFilteringCriteria.getNoOfActualRequiredSeats();
		Collection<AvailableBCAllocationSummaryDTO> eligibleStandardBookingClasses = new ArrayList<AvailableBCAllocationSummaryDTO>();
		Collection<AvailableBCAllocationSummaryDTO> eligibleStandardBookingClassesForHRT = new ArrayList<AvailableBCAllocationSummaryDTO>();

		// OPEN_RETURN O/B cheapest bucket booking class info
		FilteredBucketCheapestBCDTO outBoundCheapestBucketBCDTO = bucketFilteringCriteria.getOutBoundCheapestBucketBCDTO();

		if (log.isDebugEnabled()) {
			if (bucketFilteringCriteria.isInterlineFareQuoted()) {
				log.debug("Available results extraction interline fare quote [requested bookingClassCode="
						+ bucketFilteringCriteria.getBookingClassCode() + "]");
			}
		}

		boolean showNestedSeat = false;

		// nesting will be allowed only when bc Type is Normal
		if (BookingClassUtil.isOnlyNormalOrOverbookBcType(bucketFilteringCriteria.getBookingClassTypes())) {
			showNestedSeat = AppSysParamsUtil.showNestedAvailableSeats();
		}

		while (resultSet.next()) {
			String logicalCabinClass = resultSet.getString("logical_cabin_class");
			String bookingCode = resultSet.getString("booking_code");
			String bcType = resultSet.getString("bc_type");

			if (!lastLogicalCabinClass.equals(logicalCabinClass)) {
				lastLogicalCabinClass = logicalCabinClass;
				eligibleStandardBookingClasses.clear();
				cummulativeStandardSeatsCount = 0;
				cummulativeStandardSeatsCountForHRT = 0;
				overbookedSeatsCount = 0;
				overbookedSeatsCountForHRT = 0;
			}
			// skip own airline fare extract, if interline booking class type, only when interline AVAILABILITY SEARCH
			// when intetline SELECTED FLIGHT FARE QUOTE is done for selected booking class, then it goes through normal
			// fare extraction
			skipNormalSearchDuringIntlnAvailSearch = BookingClass.BookingClassType.INTERLINE_FREESELL.equals(bcType)
					&& !bucketFilteringCriteria.isInterlineFareQuoted();

			isExternalBookingClass = bucketFilteringCriteria.getExternalBookingClasses() != null
					&& bucketFilteringCriteria.getExternalBookingClasses().contains(bookingCode);

			if (log.isDebugEnabled()) {
				if (skipNormalSearchDuringIntlnAvailSearch) {
					log.debug("Available results extraction interline availability search. Excluding booking class from own "
							+ "airline results [bookingClassCode=" + bookingCode + "]");
				}
				if (isExternalBookingClass) {
					log.debug("Available results extraction. Booking class availability is requested by interline search "
							+ "[bookingClassCode=" + bookingCode + "]");
				}
			}

			boolean validOpenReturnIBStandardBookingCode = isValidBookingCodeForOpenReturn(
					bucketFilteringCriteria.isOpenReturn(), outBoundCheapestBucketBCDTO, false, true, false, bookingCode);
			boolean validOpenReturnIBNonStandardBookingCode = isValidBookingCodeForOpenReturn(
					bucketFilteringCriteria.isOpenReturn(), outBoundCheapestBucketBCDTO, false, false, true, bookingCode);
			boolean validOpenReturnIBFixedBookingCode = isValidBookingCodeForOpenReturn(bucketFilteringCriteria.isOpenReturn(),
					outBoundCheapestBucketBCDTO, true, false, false, bookingCode);

			if (!lastBookingCode.equals(bookingCode)) {
				// We may have several fares attached to same BC. But can take the first one which is the minimum fare
				boolean standardFlag = resultSet.getString("standard_flag").equals("Y") ? true : false;
				if (allowNesting && standardFlag) {
					lastBookingCode = bookingCode;
				}
				boolean fixedFlag = resultSet.getString("fixed_flag").equals("Y") ? true : false;
				boolean halfRTFlag = resultSet.getString("halfrt_flag").equals("Y") ? true : false;
				int availableSeatsAllocation = resultSet.getInt("available_seats");
				int availableNonFixedSeats = resultSet.getInt("available_non_fixed_seats");
				int availableNestedSeatsAllocation = resultSet.getInt("nested_available_seats");
				int effectiveUsedSeats = 0;
				int effectiveUsedSeatsForHRT = 0;
				boolean isBCHasNestedSeatAvailability = false;
				boolean isBCHasNestedSeatAvailabilityForHRT = false;
				boolean isNestingAvailability = false;
				boolean isConsiderBookingClass = true;

				if (showNestedSeat && bucketFilteringCriteria.getBookingClassCode() != null
						&& !bookingCode.equals(bucketFilteringCriteria.getBookingClassCode())) {
					isNestingAvailability = true;
					// when nesting with selected booking class fare should be picked from selected booking class only
					isConsiderBookingClass = false;
				}

				boolean bookedBookingClass = (bucketFilteringCriteria.isSameBookedFltCabinBeingSearched() && bookingCode != null && bookingCode
						.equals(bucketFilteringCriteria.getBookedFlightBookingClass()));
				if ((!getBooleanValue(logicalCCWiseCheapestStandardBCFound.get(logicalCabinClass)) || !allowNesting || isNestingAvailability)
						&& standardFlag && !skipNormalSearchDuringIntlnAvailSearch && validOpenReturnIBStandardBookingCode) {
					boolean isEligible = false;
					boolean bookingClassConsiderable = false;
					boolean bookingClassSeatsConsidered = false;

					if (!bucketFilteringCriteria.isInterlineFareQuoted() && allowNesting) {
						if (cummulativeStandardSeatsCount < requestedSeats) {
							cummulativeStandardSeatsCount += availableSeatsAllocation;
							bookingClassSeatsConsidered = true;
						}

						// when nesting is requested from selected booking class
						if (showNestedSeat && bookingCode.equals(bucketFilteringCriteria.getBookingClassCode())
								&& (availableSeatsAllocation < requestedSeats)
								&& availableNestedSeatsAllocation >= requestedSeats) {
							isBCHasNestedSeatAvailability = true;
						}

						if (isConsiderBookingClass
								&& isBcConsiderabale(cummulativeStandardSeatsCount, availableNonFixedSeats, requestedSeats,
										bucketFilteringCriteria, bcType, bookedBookingClass, isBCHasNestedSeatAvailability)) {
							bookingClassConsiderable = true;

							effectiveUsedSeats = Math.min(cummulativeStandardSeatsCount, availableNonFixedSeats);
							effectiveUsedSeats = Math.min(effectiveUsedSeats, requestedSeats);

							if (isBCHasNestedSeatAvailability) {
								effectiveUsedSeats = requestedSeats;
							}

						}
						isEligible = true;
					} else {
						// No nesting when interline booking class is defined as a standard class
						if (isBcConsiderabale(availableSeatsAllocation, availableNonFixedSeats, requestedSeats,
								bucketFilteringCriteria, bcType, bookedBookingClass, false)) {
							cummulativeStandardSeatsCount += availableSeatsAllocation;
							bookingClassConsiderable = true;
							bookingClassSeatsConsidered = true;
							isEligible = true;
							effectiveUsedSeats = Math.min(cummulativeStandardSeatsCount, availableNonFixedSeats);
							effectiveUsedSeats = Math.min(effectiveUsedSeats, requestedSeats);
						}
					}
					if (isEligible) {
						int noOfSeatsFromBC = 0;
						if (bookingClassSeatsConsidered) {
							noOfSeatsFromBC = (requestedSeats - cummulativeStandardSeatsCount) > 0 ? availableSeatsAllocation
									: (requestedSeats - (cummulativeStandardSeatsCount - availableSeatsAllocation));
							if (bucketFilteringCriteria.isSearchForWaitListingSeatAvailability()) {
								noOfSeatsFromBC = requestedSeats;
							} else if (noOfSeatsFromBC < requestedSeats
									&& AirinventoryUtils.isOverbookingWithBookingClass(bucketFilteringCriteria)) {
								if (isConsiderBookingClass) {
									overbookedSeatsCount = requestedSeats - noOfSeatsFromBC;
									noOfSeatsFromBC = requestedSeats;
								} else if (noOfSeatsFromBC > 0) {
									for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClasses) {
										if (allocationSummaryDTO.isSearchedBC()) {
											allocationSummaryDTO.setNoOfSeats(allocationSummaryDTO.getNoOfSeats()
													- noOfSeatsFromBC);
											overbookedSeatsCount = overbookedSeatsCount - noOfSeatsFromBC;
										}
									}
								}
							} else if (noOfSeatsFromBC < requestedSeats
									&& bucketFilteringCriteria.isSameBookedFltCabinBeingSearched()
									&& bucketFilteringCriteria.getBookedFlightBookingClass() != null
									&& bucketFilteringCriteria.getBookedFlightBookingClass().equals(bookingCode)) {
								if (isConsiderBookingClass) {
									noOfSeatsFromBC = requestedSeats;
									eligibleStandardBookingClasses.clear();
								}
							}
						}
						if (noOfSeatsFromBC > 0) {
							AvailableBCAllocationSummaryDTO bcAllocationSummaryDTO = createAvailableBCAllocationSummaryDTO(
									resultSet, noOfSeatsFromBC, bucketFilteringCriteria, bookingClassConsiderable
											&& (bucketFilteringCriteria.isSearchForWaitListingSeatAvailability() ? false
													: (((effectiveUsedSeats - requestedSeats) < 0) || overbookedSeatsCount > 0)),
									isConsiderBookingClass);
							eligibleStandardBookingClasses.add(bcAllocationSummaryDTO);

							if (bookingClassConsiderable) {

								// if nesting is from selected BC then all the fare buckets are not ready yet, therefore
								// we
								// skip at this stage
								if (!isBCHasNestedSeatAvailability) {
									for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClasses) {
										filteredBucketsDTO.addBucket(logicalCabinClass, allocationSummaryDTO, standardFlag,
												fixedFlag, false, false);
									}

									cummulativeStandardSeatsCount = 0;
									eligibleStandardBookingClasses.clear();
								}
								logicalCCWiseCheapestStandardBCFound.put(logicalCabinClass, true);

							} else if (!allowNesting) {
								eligibleStandardBookingClasses.clear();
							} else if (allowNesting) {
								bcAllocationSummaryDTO.setNestedBc(true);

								if (isNestingAvailability && (cummulativeStandardSeatsCount >= requestedSeats)) {
									// if nesting is from selected BC then all the fare buckets are ready at this stage
									eligibleStandardBookingClasses = sortStandardBookingClasses(eligibleStandardBookingClasses);

									for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClasses) {
										filteredBucketsDTO.addBucket(logicalCabinClass, allocationSummaryDTO, standardFlag,
												fixedFlag, false, false);
									}
									cummulativeStandardSeatsCount = 0;
									eligibleStandardBookingClasses.clear();
								}
							}
						}
					}

					if (bucketFilteringCriteria.isInterlineFareQuoted()) {
						continue;
					}
				} else if ((!getBooleanValue(logicalCCWiseCheapestFixedBCFound.get(logicalCabinClass)) || !allowNesting)
						&& fixedFlag && !skipNormalSearchDuringIntlnAvailSearch && validOpenReturnIBFixedBookingCode) {
					// Filter cheapest fixed bucket It is assumed that buckets are ordered in ascending fare amount
					// we pass non fixed seat count as same as fixed bc allocation. actually we need to check only
					// allocation is sufficient for fixed allocation only. Passing this is okay as it will used only to
					// calculate minimum allocation
					if (isBcConsiderabale(availableSeatsAllocation, availableSeatsAllocation, requestedSeats,
							bucketFilteringCriteria, bcType, bookedBookingClass, false)) {
						AvailableBCAllocationSummaryDTO bcAllocationSummaryDTO = createAvailableBCAllocationSummaryDTO(resultSet,
								requestedSeats, bucketFilteringCriteria,
								(availableNonFixedSeats + availableSeatsAllocation - requestedSeats) < 0, isConsiderBookingClass);
						filteredBucketsDTO.addBucket(logicalCabinClass, bcAllocationSummaryDTO, standardFlag, fixedFlag, false,
								false);
						logicalCCWiseCheapestFixedBCFound.put(logicalCabinClass, true);
					}
					if (bucketFilteringCriteria.isInterlineFareQuoted()) {
						continue;
					}
				} else if (!standardFlag && !fixedFlag && !skipNormalSearchDuringIntlnAvailSearch
						&& validOpenReturnIBNonStandardBookingCode) {
					if ((!getBooleanValue(logicalCCWiseCheapestNonStandardBCFound.get(logicalCabinClass)) || !allowNesting)
							&& !skipNormalSearchDuringIntlnAvailSearch) {
						// Filter cheapest non standard bucket. It is assumed that buckets are ordered
						// in ascending fare amount
						if (isBcConsiderabale(availableSeatsAllocation, availableNonFixedSeats, requestedSeats,
								bucketFilteringCriteria, bcType, bookedBookingClass, false)) {
							AvailableBCAllocationSummaryDTO bcAllocationSummaryDTO = createAvailableBCAllocationSummaryDTO(
									resultSet, requestedSeats, bucketFilteringCriteria,
									(availableNonFixedSeats - requestedSeats) < 0, isConsiderBookingClass);
							filteredBucketsDTO.addBucket(logicalCabinClass, bcAllocationSummaryDTO, standardFlag, fixedFlag,
									false, false);
							logicalCCWiseCheapestNonStandardBCFound.put(logicalCabinClass, true);
						}
					}
					if (bucketFilteringCriteria.isInterlineFareQuoted()) {
						continue;
					}
				}
				if (bucketFilteringCriteria.isReturnFareSearch()
						&& (!getBooleanValue(logicalCCWiseCheapestStandardBCFoundForHRT.get(logicalCabinClass)) || !allowNesting || isNestingAvailability)
						&& standardFlag && !skipNormalSearchDuringIntlnAvailSearch && validOpenReturnIBStandardBookingCode) {

					boolean isEligible = false;
					boolean bookingClassConsiderableForHRT = false;
					boolean bookingClassSeatsConsideredForHRT = false;

					if (!bucketFilteringCriteria.isInterlineFareQuoted() && allowNesting) {
						if (bucketFilteringCriteria.isReturnFareSearch() && halfRTFlag) {
							if (cummulativeStandardSeatsCountForHRT < requestedSeats) {
								cummulativeStandardSeatsCountForHRT += availableSeatsAllocation;
								bookingClassSeatsConsideredForHRT = true;
							}
						}
						// when nesting is requested from selected booking class
						if (showNestedSeat && bookingCode.equals(bucketFilteringCriteria.getBookingClassCode())
								&& (availableSeatsAllocation < requestedSeats)
								&& availableNestedSeatsAllocation >= requestedSeats) {
							isBCHasNestedSeatAvailabilityForHRT = true;
						}

						if (isConsiderBookingClass
								&& isBcConsiderabale(cummulativeStandardSeatsCountForHRT, availableNonFixedSeats, requestedSeats,
										bucketFilteringCriteria, bcType, bookedBookingClass, isBCHasNestedSeatAvailability)) {
							bookingClassConsiderableForHRT = true;

							effectiveUsedSeatsForHRT = Math.min(cummulativeStandardSeatsCountForHRT, availableNonFixedSeats);
							effectiveUsedSeatsForHRT = Math.min(effectiveUsedSeatsForHRT, requestedSeats);

							if (isBCHasNestedSeatAvailabilityForHRT) {
								effectiveUsedSeatsForHRT = requestedSeats;
							}

						}
						isEligible = true;
					} else {
						// No nesting when interline booking class is defined as a standard class
						if (isBcConsiderabale(availableSeatsAllocation, availableNonFixedSeats, requestedSeats,
								bucketFilteringCriteria, bcType, bookedBookingClass, false)) {
							cummulativeStandardSeatsCountForHRT += availableSeatsAllocation;
							bookingClassConsiderableForHRT = true;
							bookingClassSeatsConsideredForHRT = true;
							isEligible = true;
							effectiveUsedSeatsForHRT = Math.min(cummulativeStandardSeatsCountForHRT, availableNonFixedSeats);
							effectiveUsedSeatsForHRT = Math.min(effectiveUsedSeatsForHRT, requestedSeats);
						}
					}
					if (isEligible) {
						int noOfSeatsFromBCForHRT = 0;
						AvailableBCAllocationSummaryDTO bcAllocationSummaryDTOForHRT = null;
						if (bookingClassSeatsConsideredForHRT) {
							noOfSeatsFromBCForHRT = (requestedSeats - cummulativeStandardSeatsCountForHRT) > 0 ? availableSeatsAllocation
									: (requestedSeats - (cummulativeStandardSeatsCountForHRT - availableSeatsAllocation));

							if (bucketFilteringCriteria.isSearchForWaitListingSeatAvailability()) {
								noOfSeatsFromBCForHRT = requestedSeats;
							} else if (noOfSeatsFromBCForHRT < requestedSeats
									&& AirinventoryUtils.isOverbookingWithBookingClass(bucketFilteringCriteria)) {
								if (isConsiderBookingClass) {
									overbookedSeatsCountForHRT = requestedSeats - noOfSeatsFromBCForHRT;
									noOfSeatsFromBCForHRT = requestedSeats;
								} else if (noOfSeatsFromBCForHRT > 0) {
									for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClassesForHRT) {
										if (allocationSummaryDTO.isSearchedBC()) {
											allocationSummaryDTO.setNoOfSeats(allocationSummaryDTO.getNoOfSeats()
													- noOfSeatsFromBCForHRT);
											overbookedSeatsCountForHRT = overbookedSeatsCountForHRT - noOfSeatsFromBCForHRT;
										}
									}
								}
							} else if (noOfSeatsFromBCForHRT < requestedSeats
									&& bucketFilteringCriteria.isSameBookedFltCabinBeingSearched()
									&& bucketFilteringCriteria.getBookedFlightBookingClass() != null
									&& bucketFilteringCriteria.getBookedFlightBookingClass().equals(bookingCode)) {
								if (isConsiderBookingClass) {
									noOfSeatsFromBCForHRT = requestedSeats;
									eligibleStandardBookingClassesForHRT.clear();
								}
							}
						}

						if (noOfSeatsFromBCForHRT > 0) {
							bcAllocationSummaryDTOForHRT = createAvailableBCAllocationSummaryDTO(resultSet,
									noOfSeatsFromBCForHRT, bucketFilteringCriteria, bookingClassConsiderableForHRT
											&& (bucketFilteringCriteria.isSearchForWaitListingSeatAvailability() ? false
													: ((effectiveUsedSeatsForHRT - requestedSeats) < 0)), isConsiderBookingClass);
							eligibleStandardBookingClassesForHRT.add(bcAllocationSummaryDTOForHRT);

							if (bookingClassConsiderableForHRT) {
								if (!isBCHasNestedSeatAvailabilityForHRT) {
									for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClassesForHRT) {
										filteredBucketsDTO.addBucket(logicalCabinClass, allocationSummaryDTO, standardFlag,
												fixedFlag, false, true);
									}

									cummulativeStandardSeatsCountForHRT = 0;
									eligibleStandardBookingClassesForHRT.clear();
								}
								logicalCCWiseCheapestStandardBCFoundForHRT.put(logicalCabinClass, true);

							} else if (!allowNesting) {
								eligibleStandardBookingClassesForHRT.clear();
							} else if (allowNesting) {
								if (bcAllocationSummaryDTOForHRT != null) {
									bcAllocationSummaryDTOForHRT.setNestedBc(true);
								}
								if (isNestingAvailability && (cummulativeStandardSeatsCountForHRT >= requestedSeats)) {
									// if nesting is from selected BC then all the fare buckets are ready at this stage
									eligibleStandardBookingClassesForHRT = sortStandardBookingClasses(eligibleStandardBookingClassesForHRT);

									for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClassesForHRT) {
										filteredBucketsDTO.addBucket(logicalCabinClass, allocationSummaryDTO, standardFlag,
												fixedFlag, false, true);
									}
									cummulativeStandardSeatsCountForHRT = 0;
									eligibleStandardBookingClassesForHRT.clear();
								}
							}
						}
					}
					if (bucketFilteringCriteria.isInterlineFareQuoted()) {
						continue;
					}
				} else if (bucketFilteringCriteria.isReturnFareSearch()
						&& (!getBooleanValue(logicalCCWiseCheapestFixedBCFoundForHRT.get(logicalCabinClass)) || !allowNesting)
						&& fixedFlag && !skipNormalSearchDuringIntlnAvailSearch && validOpenReturnIBFixedBookingCode
						&& halfRTFlag) {

					if (isBcConsiderabale(availableSeatsAllocation, availableSeatsAllocation, requestedSeats,
							bucketFilteringCriteria, bcType, bookedBookingClass, false)) {
						AvailableBCAllocationSummaryDTO bcAllocationSummaryDTO = createAvailableBCAllocationSummaryDTO(resultSet,
								requestedSeats, bucketFilteringCriteria, (availableNonFixedSeats - requestedSeats) < 0,
								isConsiderBookingClass);
						filteredBucketsDTO.addBucket(logicalCabinClass, bcAllocationSummaryDTO, standardFlag, fixedFlag, false,
								true);
						logicalCCWiseCheapestFixedBCFoundForHRT.put(logicalCabinClass, true);
					}
					if (bucketFilteringCriteria.isInterlineFareQuoted()) {
						continue;
					}
				}
				if (skipNormalSearchDuringIntlnAvailSearch && isExternalBookingClass) {
					// add to the interline booking class list
					if (isBcConsiderabale(availableSeatsAllocation, availableNonFixedSeats, requestedSeats,
							bucketFilteringCriteria, bcType, bookedBookingClass, false)) {
						filteredBucketsDTO.addExternalBookingClassAvailability(
								logicalCabinClass,
								createAvailableBCAllocationSummaryDTO(resultSet, availableSeatsAllocation,
										bucketFilteringCriteria, (availableNonFixedSeats - requestedSeats) < 0,
										isConsiderBookingClass));
					}
				}
			}
		}
		if (overbookedSeatsCount > 0 && allowNesting && cummulativeStandardSeatsCount > 0) {
			Collection<AvailableBCAllocationSummaryDTO> eligibleStandardBCs = filteredBucketsDTO.getBucket(lastLogicalCabinClass,
					true, false, false);

			boolean selectedBCExist = false;
			// This part can execute only if user select standard booking class and do the over booking
			if (eligibleStandardBCs != null && eligibleStandardBCs.size() == 1) {
				AvailableBCAllocationSummaryDTO selectedStandardBCs = eligibleStandardBCs.iterator().next();
				if (selectedStandardBCs != null) {
					selectedBCExist = true;
					for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClasses) {
						int numberOfSeats = selectedStandardBCs.getNoOfSeats() - allocationSummaryDTO.getNoOfSeats();
						if (numberOfSeats > 0) {
							selectedStandardBCs.setNoOfSeats(numberOfSeats);
							eligibleStandardBCs.add(allocationSummaryDTO);
						}
					}
				}
			}
			if (selectedBCExist) {
				// if nesting is from selected BC then all the fare buckets are ready at this stage
				eligibleStandardBCs = sortStandardBookingClasses(eligibleStandardBCs);
				filteredBucketsDTO.getBucket(lastLogicalCabinClass, true, false, false).clear();
			} else {
				for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClasses) {
					if (allocationSummaryDTO.getNoOfSeats() > 0) {
						eligibleStandardBCs.add(allocationSummaryDTO);
					}
				}
				// if nesting is from selected BC then all the fare buckets are ready at this stage
				eligibleStandardBCs = sortStandardBookingClasses(eligibleStandardBCs);
			}

			for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBCs) {
				filteredBucketsDTO.addBucket(lastLogicalCabinClass, allocationSummaryDTO, true, allocationSummaryDTO.isFixedBC(),
						false, false);
			}
			cummulativeStandardSeatsCount = 0;
			eligibleStandardBookingClasses.clear();
			eligibleStandardBCs.clear();
		}
		if (overbookedSeatsCountForHRT > 0 && allowNesting && cummulativeStandardSeatsCountForHRT > 0) {
			Collection<AvailableBCAllocationSummaryDTO> eligibleStandardBCs = filteredBucketsDTO.getBucket(lastLogicalCabinClass,
					true, false, true);

			boolean selectedBCExist = false;
			// This part can execute only if user select standard booking class and do the over booking
			if (eligibleStandardBCs != null && eligibleStandardBCs.size() == 1) {
				AvailableBCAllocationSummaryDTO selectedStandardBCs = eligibleStandardBCs.iterator().next();
				if (selectedStandardBCs != null) {
					selectedBCExist = true;
					for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClassesForHRT) {
						int numberOfSeats = selectedStandardBCs.getNoOfSeats() - allocationSummaryDTO.getNoOfSeats();
						if (numberOfSeats > 0) {
							selectedStandardBCs.setNoOfSeats(numberOfSeats);
							eligibleStandardBCs.add(allocationSummaryDTO);
						}
					}
				}
			}

			if (selectedBCExist) {
				// if nesting is from selected BC then all the fare buckets are ready at this stage
				eligibleStandardBCs = sortStandardBookingClasses(eligibleStandardBCs);
				filteredBucketsDTO.getBucket(lastLogicalCabinClass, true, false, true).clear();
			} else {
				for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBookingClassesForHRT) {
					if (allocationSummaryDTO.getNoOfSeats() > 0) {
						eligibleStandardBCs.add(allocationSummaryDTO);
					}
				}
				// if nesting is from selected BC then all the fare buckets are ready at this stage
				eligibleStandardBCs = sortStandardBookingClasses(eligibleStandardBCs);
			}
			for (AvailableBCAllocationSummaryDTO allocationSummaryDTO : eligibleStandardBCs) {
				filteredBucketsDTO.addBucket(lastLogicalCabinClass, allocationSummaryDTO, true, allocationSummaryDTO.isFixedBC(),
						false, true);
			}
			cummulativeStandardSeatsCountForHRT = 0;
			eligibleStandardBookingClassesForHRT.clear();
			eligibleStandardBCs.clear();
		}
		return filteredBucketsDTO;
	}

	public static AvailableBCAllocationSummaryDTO createAvailableBCAllocationSummaryDTO(ResultSet resultSet, int noOfSeats,
			BucketFilteringCriteria bucketFilteringCriteria, boolean isOverbook, boolean isSearchedBC) throws SQLException {
		AvailableBCAllocationSummaryDTO bcAllocSummaryDTO = new AvailableBCAllocationSummaryDTO();
		bcAllocSummaryDTO.setBookingCode(resultSet.getString("booking_code"));
		bcAllocSummaryDTO.setBookingClassType(resultSet.getString("bc_type"));
		bcAllocSummaryDTO.setOnholdRestricted("Y".equals(resultSet.getString("onhold_flag")) ? true : false);
		bcAllocSummaryDTO.setStandardBC(resultSet.getString("standard_flag").equals("Y") ? true : false);
		bcAllocSummaryDTO.setFixedBC(resultSet.getString("fixed_flag").equals("Y") ? true : false);
		bcAllocSummaryDTO.setNestRank(resultSet.getInt("nest_rank"));
		bcAllocSummaryDTO.setNoOfSeats(noOfSeats);
		bcAllocSummaryDTO.setHalfReturnApplicable(resultSet.getString("halfrt_flag").equals("Y") ? true : false);
		bcAllocSummaryDTO.setOverbook(isOverbook);
		bcAllocSummaryDTO.setSearchedBC(isSearchedBC);
		bcAllocSummaryDTO.setBookedFlightCabinSearch(bucketFilteringCriteria.isSameBookedFltCabinBeingSearched());
		bcAllocSummaryDTO.setSegmentCode(bucketFilteringCriteria.getSegmentCode());
		bcAllocSummaryDTO.setFlownOnd(bucketFilteringCriteria.isFlownOnd());
		bcAllocSummaryDTO.setUnTouchedOnd(bucketFilteringCriteria.isUnTouchedOnd());
		if (bucketFilteringCriteria.isSameBookedFltCabinBeingSearched()) {
			bcAllocSummaryDTO.setSameBookingClassAsExisting(bcAllocSummaryDTO.getBookingCode().equals(
					bucketFilteringCriteria.getBookedFlightBookingClass()));
		}

		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
		fareSummaryDTO.setFareId(resultSet.getInt("fare_id"));
		fareSummaryDTO.setFareRuleID(resultSet.getInt("fare_rule_id"));
		fareSummaryDTO.setHasFlexi(resultSet.getInt("flexi_rule_id") != 0);
		fareSummaryDTO.setFlexiRuleID(resultSet.getInt("flexi_rule_id") == 0 ? null : resultSet.getInt("flexi_rule_id"));
		ExternalFareSummaryDTO externalFareSummaryDTO = bucketFilteringCriteria.getExternalFareSummaryDTO();
		if (externalFareSummaryDTO == null
				|| !externalFareSummaryDTO.getBookingClassCode().equals(bcAllocSummaryDTO.getBookingCode())) {
			Double adultFareAmount = resultSet.getDouble("fare_amount");
			Double childFareAmount = resultSet.getDouble("child_fare");
			Double infantFareAmount = resultSet.getDouble("infant_fare");
			String childFareType = resultSet.getString("child_fare_type");
			String infantFareType = resultSet.getString("infant_fare_type");
			String currecyCode = resultSet.getString("currency_code");
			if (bucketFilteringCriteria.isReturnFareSearch()) {
				adultFareAmount = adultFareAmount / 2.0;
				if (childFareType.equals(Fare.VALUE_PERCENTAGE_FLAG_V)) {
					childFareAmount = childFareAmount / 2.0;
				}
				if (infantFareType.equals(Fare.VALUE_PERCENTAGE_FLAG_V)) {
					infantFareAmount = infantFareAmount / 2.0;
				}
			}
			fareSummaryDTO.setAdultFare(adultFareAmount);
			fareSummaryDTO.setChildFareType(childFareType);
			fareSummaryDTO.setChildFare(childFareAmount);
			fareSummaryDTO.setInfantFareType(infantFareType);
			fareSummaryDTO.setInfantFare(infantFareAmount);
			fareSummaryDTO.setCurrenyCode(currecyCode);
		} else {
			// Overriding fare amount for inter line fare quote for selected booking class
			fareSummaryDTO.setAdultFare(externalFareSummaryDTO.getAdultFareAmount());
			fareSummaryDTO.setChildFare(externalFareSummaryDTO.getChildFareAmount());
			fareSummaryDTO.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_V); // FIXME
			fareSummaryDTO.setInfantFare(externalFareSummaryDTO.getInfantFareAmount());
			fareSummaryDTO.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_V); // FIXME
		}

		if (bucketFilteringCriteria.getNoOfAdults() > 0) {
			boolean fareApplicability = BeanUtils.nullHandler(resultSet.getString("AD_APPLICABILITY")).equals("Y") ? true : false;
			boolean isFareRefundable = BeanUtils.nullHandler(resultSet.getString("AD_REFUND_APPLICABILITY"))
					.equalsIgnoreCase("Y") ? true : false;
			fareSummaryDTO.setAdultApplicability(fareApplicability);
			fareSummaryDTO.setAdultFareRefundable(isFareRefundable);
		}
		if (bucketFilteringCriteria.getNoOfChilds() > 0) {
			boolean fareApplicability = BeanUtils.nullHandler(resultSet.getString("CH_APPLICABILITY")).equals("Y") ? true : false;
			boolean isFareRefundable = BeanUtils.nullHandler(resultSet.getString("CH_REFUND_APPLICABILITY"))
					.equalsIgnoreCase("Y") ? true : false;
			fareSummaryDTO.setChildApplicability(fareApplicability);
			fareSummaryDTO.setChildFareRefundable(isFareRefundable);
		}
		if (bucketFilteringCriteria.getNoOfInfants() > 0) {
			boolean fareApplicability = BeanUtils.nullHandler(resultSet.getString("IN_APPLICABILITY")).equals("Y") ? true : false;
			boolean isFareRefundable = BeanUtils.nullHandler(resultSet.getString("IN_REFUND_APPLICABILITY"))
					.equalsIgnoreCase("Y") ? true : false;
			fareSummaryDTO.setInfantApplicability(fareApplicability);
			fareSummaryDTO.setInfantFareRefundable(isFareRefundable);
		}

		fareSummaryDTO.setBookingClassCode(resultSet.getString("booking_code"));
		fareSummaryDTO.setOndCode(resultSet.getString("ond_code"));
		fareSummaryDTO.setFareRuleCode(resultSet.getString("fare_rule_code"));
		fareSummaryDTO.setFareBasisCode(resultSet.getString("fare_basis_code"));
		fareSummaryDTO.setFareRuleComment(resultSet.getString("rules_comments"));
		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			String commentInSelectedLocale = StringUtil.clobToString(resultSet.getClob("comment_in_selected"));
			fareSummaryDTO.setFareRuleCommentSelectedLanguage(StringUtil.getUnicode(commentInSelectedLocale));
		}
		fareSummaryDTO.setAgentInstructions(resultSet.getString("agent_instructions"));
		fareSummaryDTO.setMinimumStayOverInMins(resultSet.getLong("minimum_stay_over_mins"));
		fareSummaryDTO.setMaximumStayOverInMins(resultSet.getLong("maximum_stay_over_mins"));
		fareSummaryDTO.setOpenRTConfPeriodInMins(resultSet.getLong("openrt_conf_period_mins"));
		fareSummaryDTO.setMinimumStayOverInMonths(resultSet.getLong("minimum_stay_over_months"));
		fareSummaryDTO.setMaximumStayOverInMonths(resultSet.getLong("maximum_stay_over_months"));
		fareSummaryDTO.setOpenRTConfPeriodInMonths(resultSet.getLong("openrt_conf_stay_over_months"));
		fareSummaryDTO.setFareCategoryCode(resultSet.getString("fare_cat_id"));
		fareSummaryDTO.setNoOfAvailableSeats(resultSet.getInt("available_seats"));
		if (resultSet.getString("fare_discount_min") != null && resultSet.getString("fare_discount_max") != null) {
			fareSummaryDTO.setFareDiscountMin(resultSet.getInt("fare_discount_min"));
			fareSummaryDTO.setFareDiscountMax(resultSet.getInt("fare_discount_max"));
		}

		fareSummaryDTO.setAgentCommissionType(resultSet.getString("agent_commission_type"));
		fareSummaryDTO.setAgentCommissionAmount(resultSet.getBigDecimal("agent_commission_amount"));
		fareSummaryDTO.setPaxTypewiseFareOndChargeInfo(resultSet.getString("ratio_cat"), bucketFilteringCriteria.getPaxTypewiseFareOndChargeInfo());
		if (bucketFilteringCriteria.isRetrieveChannelAndAgentInfo()) {
			fareSummaryDTO.addVisibleChannel(resultSet.getInt("sales_channel_code"));
		}
		bcAllocSummaryDTO.setFareSummaryDTO(fareSummaryDTO);
		return bcAllocSummaryDTO;
	}

	public static double getFareAmount(List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs,
			BucketFilteringCriteria bucketFilteringCriteria) {
		if (availableBCAllocationSummaryDTOs != null && !availableBCAllocationSummaryDTOs.isEmpty()) {
			return getFareAmount(availableBCAllocationSummaryDTOs.get(availableBCAllocationSummaryDTOs.size() - 1)
					.getFareSummaryDTO(), bucketFilteringCriteria.getNoOfAdults(), bucketFilteringCriteria.getNoOfChilds(),
					bucketFilteringCriteria.getNoOfInfants());
		}
		return -1;
	}

	public static double getFareAmount(AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO,
			BucketFilteringCriteria bucketFilteringCriteria) {
		if (availableBCAllocationSummaryDTO != null) {
			return getFareAmount(availableBCAllocationSummaryDTO.getFareSummaryDTO(), bucketFilteringCriteria.getNoOfAdults(),
					bucketFilteringCriteria.getNoOfChilds(), bucketFilteringCriteria.getNoOfInfants());
		}
		return -1;
	}

	public static double getFareAmount(AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO) {
		if (availableBCAllocationSummaryDTO != null) {
			return getFareAmount(availableBCAllocationSummaryDTO.getFareSummaryDTO(), availableFlightSearchDTO.getAdultCount(),
					availableFlightSearchDTO.getChildCount(), availableFlightSearchDTO.getInfantCount());
		}
		return -1;
	}

	public static double getFareAmount(List<AvailableIBOBFlightSegment> availableIBOBFlightSegments,
			AvailableFlightSearchDTO availableFlightSearchDTO) {
		double totalFare = 0;
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
			String selectedLogicalCabinClass = availableIBOBFlightSegment.getSelectedLogicalCabinClass();
			if (selectedLogicalCabinClass != null && availableIBOBFlightSegment.getFare(selectedLogicalCabinClass) != null) {
				FareSummaryDTO fareSummaryDTO = availableIBOBFlightSegment.getFare(selectedLogicalCabinClass);
				totalFare += AirInventoryDataExtractUtil.getFareAmount(fareSummaryDTO, availableFlightSearchDTO);
			} else if (selectedLogicalCabinClass != null && !availableIBOBFlightSegment.isDirectFlight()) {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;
				for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
					FareSummaryDTO fareSummaryDTO = availableFlightSegment.getFare(selectedLogicalCabinClass);
					totalFare += AirInventoryDataExtractUtil.getFareAmount(fareSummaryDTO, availableFlightSearchDTO);
				}
			} else {
				return -1.0;
			}
		}
		return totalFare;
	}

	public static double getFareAmount(FareSummaryDTO fareSummaryDTO, AvailableFlightSearchDTO availableFlightSearchDTO) {
		if (fareSummaryDTO != null) {
			return getFareAmount(fareSummaryDTO, availableFlightSearchDTO.getAdultCount(),
					availableFlightSearchDTO.getChildCount(), availableFlightSearchDTO.getInfantCount());
		}
		return -1;
	}

	private static double getFareAmount(FareSummaryDTO fareSummaryDTO, int numberOfAdults, int numberOfChildren,
			int numberOfInfant) {
		if (fareSummaryDTO != null) {
			return fareSummaryDTO.getRatioAdjustedTotalPrice(numberOfAdults, numberOfChildren, numberOfInfant);
		}
		return -1;
	}

	public static boolean isCollectionNullOrEmpty(Collection<?> availableBCAllocationSummaryDTOs) {
		if (availableBCAllocationSummaryDTOs == null || availableBCAllocationSummaryDTOs.isEmpty()) {
			return true;
		}
		return false;
	}

	private static boolean getBooleanValue(Boolean seatsFound) {
		if (seatsFound != null) {
			return seatsFound;
		}
		return false;
	}

	public static List<SeatDistribution> populateSeatDistributions(List<AvailableBCAllocationSummaryDTO> bcAllocationSummaryDTOs,
			boolean waitListed) {
		List<SeatDistribution> seatDistributions = new ArrayList<SeatDistribution>();
		for (AvailableBCAllocationSummaryDTO bcAllocationSummaryDTO : bcAllocationSummaryDTOs) {
			SeatDistribution seatDistribution = new SeatDistribution();
			seatDistribution.setBookingClassCode(bcAllocationSummaryDTO.getBookingCode());
			seatDistribution.setBookingClassType(bcAllocationSummaryDTO.getBookingClassType());
			seatDistribution.setOnholdRestricted(bcAllocationSummaryDTO.isOnholdRestricted());
			seatDistribution.setNoOfSeats(bcAllocationSummaryDTO.getNoOfSeats());
			seatDistribution.setOverbook(bcAllocationSummaryDTO.isOverbook());
			seatDistribution.setFixedBC(bcAllocationSummaryDTO.isFixedBC());
			seatDistribution.setWaitListed(waitListed);
			seatDistribution.setBookedFlightCabinBeingSearched(bcAllocationSummaryDTO.isBookedFlightCabinBeingSearched());
			seatDistribution.setSameBookingClassAsExisting(bcAllocationSummaryDTO.isSameBookingClassAsExisting());
			seatDistribution.setFlownOnd(bcAllocationSummaryDTO.isFlownOnd());
			seatDistribution.setUnTouchedOnd(bcAllocationSummaryDTO.isUnTouchedOnd());
			seatDistributions.add(seatDistribution);
		}
		return seatDistributions;
	}

	public static AvailableBCAllocationSummaryDTO getLastElement(
			List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs) {
		if (!availableBCAllocationSummaryDTOs.isEmpty()) {
			return availableBCAllocationSummaryDTOs.get(availableBCAllocationSummaryDTOs.size() - 1);
		}
		return null;
	}

	public static AvailableBCAllocationSummaryDTO getChapestElement(
			List<AvailableBCAllocationSummaryDTO> availableBCAllocationSummaryDTOs) {
		AvailableBCAllocationSummaryDTO availableBCAllocationSummaryDTO = null;
		if (!availableBCAllocationSummaryDTOs.isEmpty()) {
			for (AvailableBCAllocationSummaryDTO availBCSummaryDto : availableBCAllocationSummaryDTOs) {
				if (availableBCAllocationSummaryDTO == null) {
					availableBCAllocationSummaryDTO = availBCSummaryDto;
				} else if (availBCSummaryDto.getFareSummaryDTO().getAdultFare() < availableBCAllocationSummaryDTO
						.getFareSummaryDTO().getAdultFare()) {
					availableBCAllocationSummaryDTO = availBCSummaryDto;
				}
			}
		}
		return availableBCAllocationSummaryDTO;
	}

	public static FilteredBucketsDTO getLastFilteredBucketsDTO(List<FilteredBucketsDTO> filteredBucketsDTOs) {
		if (!filteredBucketsDTOs.isEmpty()) {
			return filteredBucketsDTOs.get(filteredBucketsDTOs.size() - 1);
		}
		return null;
	}

	public static boolean isFirstFareLess(double firstFare, double secondFare) {
		if (secondFare < -1) {
			secondFare = -1;
		}
		if ((firstFare > -1 && secondFare > -1 && firstFare < secondFare) || (secondFare == -1 && firstFare > -1)) {
			return true;
		}
		return false;
	}

	public static boolean isFirstFareLessOrEqual(double firstFare, double secondFare) {
		if (secondFare < -1) {
			secondFare = -1;
		}
		if ((firstFare > -1 && secondFare > -1 && firstFare <= secondFare) || (secondFare == -1 && firstFare > -1)) {
			return true;
		}
		return false;
	}

	public static boolean isFirstFareLessOrEqual(double base, double... fares) {
		if (base > -1) {
			for (double fare : fares) {
				if (fare > -1 && base > fare) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public static boolean isFirstFareLess(double base, double... fares) {
		if (base > -1) {
			for (double fare : fares) {
				if (fare > -1 && base >= fare) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public static double getTotalFare(double... fares) {
		double totalFare = 0;
		for (double fare : fares) {
			if (fare < 0) {
				return -1;
			}
			totalFare += fare;
		}
		return totalFare;
	}

	public static List<FilteredBucketsDTO> getOndFlightWiseFilteredBucketsDTOs(
			AvailableIBOBFlightSegment availableIBOBFlightSegment, boolean returnBuckets, boolean halfReturnBucketsOnly)
			throws ModuleException {
		return getOndFlightWiseFilteredBucketsDTOs(Arrays.asList(availableIBOBFlightSegment), returnBuckets,
				halfReturnBucketsOnly);
	}

	public static List<FilteredBucketsDTO> getOndFlightWiseFilteredBucketsDTOs(
			List<AvailableIBOBFlightSegment> availableIBOBFlightSegments, boolean returnBuckets, boolean halfReturnBucketsOnly)
			throws ModuleException {
		List<FilteredBucketsDTO> ondFlightWiseFilteredBucketsDTOs = new ArrayList<FilteredBucketsDTO>();
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
			String ondCode = availableIBOBFlightSegment.getOndCode();
			if (availableIBOBFlightSegment.isDirectFlight()) {
				AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableIBOBFlightSegment;
				if (!returnBuckets) {
					ondFlightWiseFilteredBucketsDTOs.add(availableFlightSegment.getOndFilteredBucketsDTOsForConnections(ondCode));
				} else {
					if (halfReturnBucketsOnly && !AppSysParamsUtil.isAllowReturnParticipateInHalfReturn()) {
						ondFlightWiseFilteredBucketsDTOs.add(roundingWithoutLossForReturns(availableFlightSegment
								.getOndFilteredBucketsDTOsForReturns(ondCode).getOnlyHalfReturnBucketsDTO(),
								availableIBOBFlightSegment.isInboundFlightSegment()));
					} else {
						ondFlightWiseFilteredBucketsDTOs.add(roundingWithoutLossForReturns(availableFlightSegment
								.getOndFilteredBucketsDTOsForReturns(ondCode).getOnlyReturnBucketsDTO(),
								availableIBOBFlightSegment.isInboundFlightSegment()));
					}
				}
			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;
				for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
					if (!returnBuckets) {
						ondFlightWiseFilteredBucketsDTOs.add(availableFlightSegment
								.getOndFilteredBucketsDTOsForConnections(ondCode));
					} else {
						if (halfReturnBucketsOnly && !AppSysParamsUtil.isAllowReturnParticipateInHalfReturn()) {
							ondFlightWiseFilteredBucketsDTOs.add(roundingWithoutLossForReturns(availableFlightSegment
									.getOndFilteredBucketsDTOsForReturns(ondCode).getOnlyHalfReturnBucketsDTO(),
									availableIBOBFlightSegment.isInboundFlightSegment()));
						} else {
							ondFlightWiseFilteredBucketsDTOs.add(roundingWithoutLossForReturns(availableFlightSegment
									.getOndFilteredBucketsDTOsForReturns(ondCode).getOnlyReturnBucketsDTO(),
									availableIBOBFlightSegment.isInboundFlightSegment()));
						}
					}
				}
			}
		}
		return ondFlightWiseFilteredBucketsDTOs;
	}

	private static FilteredBucketsDTO roundingWithoutLossForReturns(FilteredBucketsDTO filteredBucketsDTO,
			boolean isInboundSegment) {
		if (filteredBucketsDTO != null) {
			Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestStandardBuckets = filteredBucketsDTO
					.getLogicalCCWiseCheapestStandardBuckets();
			Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseExternalBCAvailability = filteredBucketsDTO
					.getLogicalCCWiseExternalBCAvailability();
			Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestNonStandardBuckets = filteredBucketsDTO
					.getLogicalCCWiseCheapestNonStandardBuckets();
			Map<String, List<AvailableBCAllocationSummaryDTO>> logicalCCWiseCheapestFixedBuckets = filteredBucketsDTO
					.getLogicalCCWiseCheapestFixedBuckets();

			if (logicalCCWiseCheapestStandardBuckets != null && !logicalCCWiseCheapestStandardBuckets.isEmpty()) {
				roundingWitoutLossForReturns(logicalCCWiseCheapestStandardBuckets, isInboundSegment);
			}
			if (logicalCCWiseExternalBCAvailability != null && !logicalCCWiseExternalBCAvailability.isEmpty()) {
				roundingWitoutLossForReturns(logicalCCWiseExternalBCAvailability, isInboundSegment);
			}
			if (logicalCCWiseCheapestNonStandardBuckets != null && !logicalCCWiseCheapestNonStandardBuckets.isEmpty()) {
				roundingWitoutLossForReturns(logicalCCWiseCheapestNonStandardBuckets, isInboundSegment);
			}
			if (logicalCCWiseCheapestFixedBuckets != null && !logicalCCWiseCheapestFixedBuckets.isEmpty()) {
				roundingWitoutLossForReturns(logicalCCWiseCheapestFixedBuckets, isInboundSegment);
			}
		}
		return filteredBucketsDTO;
	}

	private static void roundingWitoutLossForReturns(Map<String, List<AvailableBCAllocationSummaryDTO>> mapBcAllocSummary,
			boolean isInboundSegment) {
		for (Entry<String, List<AvailableBCAllocationSummaryDTO>> entry : mapBcAllocSummary.entrySet()) {
			List<AvailableBCAllocationSummaryDTO> lstBcAllocSummary = entry.getValue();
			if (lstBcAllocSummary != null && !lstBcAllocSummary.isEmpty()) {
				for (AvailableBCAllocationSummaryDTO availAllocSummary : lstBcAllocSummary) {
					FareSummaryDTO fareSummaryDTO = availAllocSummary.getFareSummaryDTO();
					if (fareSummaryDTO != null) {
						double adultFare = fareSummaryDTO.getAdultFare();
						double childFare = fareSummaryDTO.getChildFare();
						double infantFare = fareSummaryDTO.getInfantFare();
						if (isInboundSegment) {
							fareSummaryDTO.setAdultFare(AccelAeroCalculator.subtract(
									AccelAeroCalculator.parseBigDecimal(adultFare * 2),
									AccelAeroCalculator.parseBigDecimal(adultFare)).doubleValue());
							fareSummaryDTO.setChildFare(AccelAeroCalculator.subtract(
									AccelAeroCalculator.parseBigDecimal(childFare * 2),
									AccelAeroCalculator.parseBigDecimal(childFare)).doubleValue());
							fareSummaryDTO.setInfantFare(AccelAeroCalculator.subtract(
									AccelAeroCalculator.parseBigDecimal(infantFare * 2),
									AccelAeroCalculator.parseBigDecimal(infantFare)).doubleValue());
						} else {
							fareSummaryDTO.setAdultFare(AccelAeroCalculator.parseBigDecimal(adultFare).doubleValue());
							fareSummaryDTO.setChildFare(AccelAeroCalculator.parseBigDecimal(childFare).doubleValue());
							fareSummaryDTO.setInfantFare(AccelAeroCalculator.parseBigDecimal(infantFare).doubleValue());
						}
					}
				}
			}
		}
	}

	private static boolean isBcConsiderabale(int bookingClassSeatCount, int availableNonFixedSeats, int requestedSeats,
			BucketFilteringCriteria bucketFilteringCriteria, String bcType, boolean bookedBookingClass,
			boolean isBCHasNestedSeatAvailability) {
		if (Math.min(bookingClassSeatCount, availableNonFixedSeats) >= requestedSeats
				|| AirinventoryUtils.isOverbookingWithBookingClass(bucketFilteringCriteria)
				|| (bookingClassSeatCount >= requestedSeats && BookingClassUtil
						.byPassSegInvUpdateForOverbook(bucketFilteringCriteria.getBookingClassTypes()))
				|| (bookingClassSeatCount >= requestedSeats && bucketFilteringCriteria.isSameBookedFltCabinBeingSearched())
				|| (BookingClassUtil.isStandbyBookingClassType(bcType) && bookingClassSeatCount >= requestedSeats)
				|| bookedBookingClass || bucketFilteringCriteria.isSearchForWaitListingSeatAvailability()
				|| isBCHasNestedSeatAvailability
				|| (!bucketFilteringCriteria.isCheckAvailability() && bucketFilteringCriteria.skipInvCheckForFlown())) {
			return true;
		}
		return false;
	}

	private static Collection<AvailableBCAllocationSummaryDTO> sortStandardBookingClasses(
			Collection<AvailableBCAllocationSummaryDTO> eligibleStandardBookingClasses) {

		ArrayList<AvailableBCAllocationSummaryDTO> list = new ArrayList<AvailableBCAllocationSummaryDTO>(
				eligibleStandardBookingClasses);

		Collections.sort(list, new Comparator<AvailableBCAllocationSummaryDTO>() {
			@Override
			public int compare(AvailableBCAllocationSummaryDTO list1, AvailableBCAllocationSummaryDTO list2) {

				return new Integer(list1.getNestRank()).compareTo(new Integer(list2.getNestRank()));
			}
		});
		return list;
	}

	private static boolean isValidBookingCodeForOpenReturn(boolean isOpenReturn,
			FilteredBucketCheapestBCDTO obCheapestBucketBCDTO, boolean fixed, boolean standard, boolean nonStandard,
			String bookingCode) {

		if (isOpenReturn && obCheapestBucketBCDTO != null && bookingCode != null) {

			if (fixed && obCheapestBucketBCDTO.getFixedBookingCode() != null
					&& !obCheapestBucketBCDTO.getFixedBookingCode().equals(bookingCode)) {
				return false;
			}

			if (standard && obCheapestBucketBCDTO.getStandardBookingCode() != null
					&& !obCheapestBucketBCDTO.getStandardBookingCode().equals(bookingCode)) {
				return false;
			}

			if (nonStandard && obCheapestBucketBCDTO.getNonStandardBookingCode() != null
					&& !obCheapestBucketBCDTO.getNonStandardBookingCode().equals(bookingCode)) {
				return false;
			}

		}

		return true;
	}

	public static ArrayList<InvTempDTO> extractInventoryTemplateData(Collection<InventoryTemplate> inventoryTemplates) {
		ArrayList<InvTempDTO> invTempDTOList = new ArrayList<InvTempDTO>();
		for (InventoryTemplate inventoryTemplate : inventoryTemplates) {
			InvTempDTO invTempDTO = new InvTempDTO();

			invTempDTO.setAirCraftModel(inventoryTemplate.getAirCraftModel());
			invTempDTO.setInvTempID(inventoryTemplate.getInvTempID());
			invTempDTO.setSegment(inventoryTemplate.getSegment());
			invTempDTO.setStatus(inventoryTemplate.getStatus());

			ArrayList<InvTempCCAllocDTO> invTempCCAllocs = new ArrayList<InvTempCCAllocDTO>();

			for (InvTempCabinAlloc invTempCabinAlloc : inventoryTemplate.getInvTempCabinAllocs()) {
				InvTempCCAllocDTO invTempCCAllocDTO = new InvTempCCAllocDTO();
				invTempCCAllocDTO.setCabinClassCode(invTempCabinAlloc.getCabinClassCode());
				invTempCCAllocDTO.setLogicalCabinClassCode(invTempCabinAlloc.getLogicalCabinClassCode());
				invTempCCAllocDTO.setAdultAllocation(invTempCabinAlloc.getAdultAllocation());
				invTempCCAllocDTO.setInfantAllocation(invTempCabinAlloc.getInfantAllocation());
				invTempCCAllocDTO.setInvTempCabinAllocID(invTempCabinAlloc.getInvTempCabinAllocID());

				ArrayList<InvTempCCBCAllocDTO> invTempCCBCAllocDTOs = new ArrayList<InvTempCCBCAllocDTO>();

				for (InvTempCabinBCAlloc invTempCabinBCAlloc : invTempCabinAlloc.getInvTempCabinBCAllocs()) {
					InvTempCCBCAllocDTO invTempCCBCAllocDTO = new InvTempCCBCAllocDTO();
					invTempCCBCAllocDTO.setBookingCode(invTempCabinBCAlloc.getBookingCode());
					invTempCCBCAllocDTO.setAllocatedSeats(invTempCabinBCAlloc.getAllocatedSeats());
					invTempCCBCAllocDTO.setPriorityFlag(invTempCabinBCAlloc.getPriorityFlag());
					invTempCCBCAllocDTO.setStatusBcAlloc(invTempCabinBCAlloc.getStatusBcAlloc());
					invTempCCBCAllocDTO.setAllocWaitListedSeats(invTempCabinBCAlloc.getAllocWaitListedSeats());
					invTempCCBCAllocDTO.setInvTmpCabinBCAllocID(invTempCabinBCAlloc.getInvTmpCabinBCAllocID());

					invTempCCBCAllocDTOs.add(invTempCCBCAllocDTO);
				}
				invTempCCAllocDTO.setInvTempCCBCAllocDTOList(invTempCCBCAllocDTOs);
				invTempCCAllocs.add(invTempCCAllocDTO);
			}

			invTempDTO.setInvTempCCAllocDTOList(invTempCCAllocs);
			invTempDTOList.add(invTempDTO);
		}
		return invTempDTOList;
	}

}
