package com.isa.thinair.airinventory.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.bl.BaggageBusinessLayer;
import com.isa.thinair.airinventory.core.bl.BaggageONDRulesMatcher;
import com.isa.thinair.airinventory.core.bl.BaggageSelector;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageJDBCDAO;
import com.isa.thinair.airinventory.core.service.bd.BaggageBusinessDelegateImpl;
import com.isa.thinair.airinventory.core.service.bd.BaggageBusinessDelegateLocalImpl;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author mano
 */
@Stateless
@RemoteBinding(jndiBinding = "BaggageService.remote")
@LocalBinding(jndiBinding = "BaggageService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class BaggageServiceBean extends PlatformBaseSessionBean implements BaggageBusinessDelegateImpl,
		BaggageBusinessDelegateLocalImpl {

	BaggageBusinessLayer flightBaggageBL;

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightBaggageDTO>> getBaggages(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			boolean requoteSegExchanged, boolean skipCutoverValidation) throws ModuleException {
		return getFlightBaggageBL().getBaggagesWithTranslations(flightSegIdWiseCos, null, skipCutoverValidation);
	}

	@Deprecated
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightBaggageDTO>> getONDBaggages(List<FlightSegmentTO> flightSegmentTOs, boolean checkCutOver,
			boolean isRequote) throws ModuleException {
		return BaggageONDRulesMatcher.getONDBaggages(flightSegmentTOs, checkCutOver, isRequote);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FlightBaggageDTO> getBaggageFromTemplate(Integer baggageTemplateID, String selectedLanguage, String cabinClassCode)
			throws ModuleException {
		return baggageTemplateID == null ? new ArrayList<FlightBaggageDTO>() : selectedLanguage.equals("en")
				? getBaggageJDBCDAO().getBaggageByTemplateID(baggageTemplateID, cabinClassCode)
				: getBaggageJDBCDAO().getBaggageByTemplateIDWithTranslations(baggageTemplateID, selectedLanguage, cabinClassCode);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightBaggageDTO>> getBaggage(BaggageRq baggageRq) throws ModuleException {
		return new BaggageSelector().selectBaggage(baggageRq).getBaggage();
	}

	@Override
	public Map<Integer, List<FlightBaggageDTO>> getBaggage(List<FlightSegmentTO> flightSegmentTOs, Integer baggageChargeId)
			throws ModuleException {
		return getFlightBaggageBL().getBaggage(flightSegmentTOs, baggageChargeId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightBaggageDTO>> getBaggagesWithTranslations(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String selectedLanguage) throws ModuleException {
		return getFlightBaggageBL().getBaggagesWithTranslations(flightSegIdWiseCos, selectedLanguage, false);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyBaggage(Collection<Integer> flightBaggageIds, Collection<Integer> flightAMSeatIdsToCancell)
			throws ModuleException {

		if (flightAMSeatIdsToCancell != null && flightAMSeatIdsToCancell.size() > 0) {
			vacateBaggages(flightAMSeatIdsToCancell);
		}

		if (flightBaggageIds != null && flightBaggageIds.size() > 0) {
			getFlightBaggageBL().updateFlightBaggages(flightBaggageIds,
					AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void vacateBaggages(Collection<Integer> flightBaggageIds) throws ModuleException {
		getFlightBaggageBL().updateFlightBaggages(flightBaggageIds, AirinventoryCustomConstants.FlightBaggageStatuses.VACANT);
	}

	/**
	 * 
	 * 
	 * @param flightSegID
	 * @return collection of templateIDs
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FlightBaggageDTO> getBaggageTempleateIds(int flightId) throws ModuleException {
		return getBaggageJDBCDAO().getTempleateIds(flightId);
	}

	/**
	 * @return the flightBaggageBL
	 */
	private BaggageBusinessLayer getFlightBaggageBL() {

		if (flightBaggageBL == null) {
			flightBaggageBL = new BaggageBusinessLayer();
		}
		return flightBaggageBL;
	}

	private BaggageJDBCDAO getBaggageJDBCDAO() {
		return (BaggageJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("baggageJDBCDAO");
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer getNextBaggageOndGroupId() {
		return getBaggageJDBCDAO().getNextBaggageOndGroupId();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce assignFlightBaggageChargesRollFoward(int sourceFlightId, Collection<Integer> destinationFlightIds,
			ArrayList<String> lstSelectedSeg, String classOfService) throws ModuleException {
		return getFlightBaggageBL().assignFlightBaggageChargesRollFoward(sourceFlightId, destinationFlightIds,
				getUserPrincipal(), lstSelectedSeg, classOfService);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce assignFlightBaggageCharges(Collection<FlightBaggageDTO> flightBaggageDTOs, Integer chargeId,
			boolean isFromEdit, String classOfService) throws ModuleException {
		return getFlightBaggageBL().assignFlightBaggageCharges(flightBaggageDTOs, getUserPrincipal(), chargeId, isFromEdit,
				classOfService);
	}

	/**
	 * Get a collection of FlightBaggageDtos for given seg id and baggage ids
	 * 
	 * @param flightSegId
	 * @param baggageIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Collection<FlightBaggageDTO> getAvailableBaggageDtos(int flightSegId, Collection<Integer> baggageIds)
			throws ModuleException {
		return getBaggageJDBCDAO().getAvailbleBaggageDTOs(flightSegId, baggageIds);
	}

	@Override
	public Collection<FlightBaggageDTO> getSoldBaggages(int flightSegID) throws ModuleException {
		return getBaggageJDBCDAO().getSoldBaggages(flightSegID);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFlightBaggages(Collection<Integer> flightIds) throws ModuleException {
		getFlightBaggageBL().deleteFlightBaggages(flightIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, BigDecimal> getTotalBaggageWeight(Map<Integer, Set<Integer>> interceptingFlightSegIds) {
		return getBaggageJDBCDAO().getTotalBaggageWeight(interceptingFlightSegIds);
	}
}
