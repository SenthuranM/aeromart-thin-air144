package com.isa.thinair.airinventory.core.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;

public class OndFilteredBucket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<Integer, List<FilteredBucketsDTO>> ondFlightWiseFilteredBucketsDTOs = new HashMap<Integer, List<FilteredBucketsDTO>>();
	private List<FilteredBucketsDTO> allBucketList = new ArrayList<FilteredBucketsDTO>();

	public void addFilteredBucket(FilteredBucketsDTO filteredBucket, boolean inboundFlightSegment) {
		int ondSequence = OndSequence.OUT_BOUND;
		if (inboundFlightSegment) {
			ondSequence = OndSequence.IN_BOUND;
		}

		addFilteredBucket(filteredBucket, ondSequence);
	}
	
	public void addFilteredBucket(FilteredBucketsDTO filteredBucket, int ondSequence) {

		if (!ondFlightWiseFilteredBucketsDTOs.containsKey(ondSequence)) {
			ondFlightWiseFilteredBucketsDTOs.put(ondSequence, new ArrayList<FilteredBucketsDTO>());
		}

		ondFlightWiseFilteredBucketsDTOs.get(ondSequence).add(filteredBucket);
		allBucketList.add(filteredBucket);
	}

	public FilteredBucketsDTO getFirstFilteredBucket() {
		if (allBucketList.size() == 0) {
			return null;
		}
		return allBucketList.get(0);
	}

	public List<FilteredBucketsDTO> getAllFilteredBuckets() {
		return allBucketList;
	}

	public Map<Integer, List<FilteredBucketsDTO>> getOndwiseFilteredBuckets() {
		return ondFlightWiseFilteredBucketsDTOs;
	}

	public int getFilteredBucketCount() {
		return allBucketList.size();
	}
}
