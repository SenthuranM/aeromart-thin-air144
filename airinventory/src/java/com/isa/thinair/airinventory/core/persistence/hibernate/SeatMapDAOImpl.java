/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusRS;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author indika
 * @isa.module.dao-impl dao-name="SeatMapDAOImpl"
 */
public class SeatMapDAOImpl extends PlatformBaseHibernateDaoSupport implements SeatMapDAO {

	public Collection<FlightSeat> getFilghtSeats(int flightSegID) throws ModuleException {
		String hql = "FROM FlightSeat WHERE flightSegId=" + flightSegID + " ";
		return find(hql, FlightSeat.class);
	}

	@Override
	public Collection<FlightSeat> getSelectedFlightSeats(int flightSegID) throws ModuleException {
		String hql = "FROM FlightSeat WHERE flightSegId=" + flightSegID + " AND status IN('RES','OHD','BLK')";
		return find(hql, FlightSeat.class);
	}

	public Collection<FlightSeat> getFilghtSeatData(Collection<Integer> flightSegIDs, Collection<Integer> seatIds)
			throws ModuleException {
		String hql = "";
		if (flightSegIDs.size() > 1000) {
			hql = "FROM FlightSeat WHERE " + Util.buildIntegerInClauseContent("flightSegId", new ArrayList(flightSegIDs))
					+ " AND seatId IN (" + Util.buildIntegerInClauseContent(seatIds) + ")";
		} else {
			hql = "FROM FlightSeat WHERE flightSegId IN (" + Util.buildIntegerInClauseContent(flightSegIDs) + ") AND seatId IN ("
					+ Util.buildIntegerInClauseContent(seatIds) + ")";
		}
		return find(hql, FlightSeat.class);
	}

	public void deleteExistingSeatFares(ArrayList<Integer> seatIdList, Integer flightSegId) throws ModuleException {
		String hqlUpdate = "delete from FlightSeat where flightSegId = :flightSegId and seatId IN ("
				+ Util.buildIntegerInClauseContent(seatIdList) + ")";
		getSession().createQuery(hqlUpdate).setInteger("flightSegId", flightSegId).executeUpdate();
	}

	public void saveSeatFares(Collection<FlightSeat> seatFares) throws ModuleException {
		hibernateSaveOrUpdateAll(seatFares);
	}

	public void deleteSeatFares(Collection<FlightSeat> seatFares) throws ModuleException {
		deleteAll(seatFares);
	}

	@SuppressWarnings("unchecked")
	public Map<String, FlightSeatStatusTO> getFlightSeatStatusDetails(Integer flightSegID, String seatCode)
			throws ModuleException {

		StringBuilder query = new StringBuilder();

		query.append(
				"SELECT fas.flight_am_seat_id,fas.status, ams.seat_code, ams.seat_type,fas.logical_cabin_class_code,ams.col_id,ams.row_grp_id,")
				.append("ams.cabin_class_code,lpad(ams.seat_code, 5 , '0') seat_code_1 ")
				.append("FROM sm_t_flight_am_seat fas,sm_t_aircraft_model_seats ams ").append("WHERE fas.flt_seg_id='")
				.append(flightSegID).append("' AND fas.am_seat_id = ams.am_seat_id ");
		if (seatCode != null) {
			query.append("AND ams.seat_code='").append(seatCode).append("' ");
		}
		query.append("ORDER BY seat_code_1");

		JdbcTemplate template = new JdbcTemplate(AirinventoryUtils.getDatasource());

		Map<String, FlightSeatStatusTO> mapFlightSeats = template.query(query.toString(),
				new ResultSetExtractor<Map<String, FlightSeatStatusTO>>() {
					public Map<String, FlightSeatStatusTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, FlightSeatStatusTO> mapFlightSeatsStatus = new LinkedHashMap<String, FlightSeatStatusTO>();

						FlightSeatStatusTO flightSeatStatus;
						String seatCode;
						while (rs.next()) {
							flightSeatStatus = new FlightSeatStatusTO();
							seatCode = rs.getString("seat_code");
							flightSeatStatus.setFlightAmSeatId(new Integer(rs.getInt("flight_am_seat_id")));
							flightSeatStatus.setSeatCode(rs.getString("seat_code"));
							flightSeatStatus.setStatus(rs.getString("status"));
							flightSeatStatus.setSeatType(rs.getString("seat_type"));
							flightSeatStatus.setColId(new Integer(rs.getInt("col_id")));
							flightSeatStatus.setRowGroupId(new Integer(rs.getInt("row_grp_id")));
							flightSeatStatus.setLogicalCabinClassCode(rs.getString("logical_cabin_class_code"));
							flightSeatStatus.setCabinClassCode(rs.getString("cabin_class_code"));
							mapFlightSeatsStatus.put(seatCode, flightSeatStatus);

						}
						return mapFlightSeatsStatus;
					}
				});
		return mapFlightSeats;

	}

	public List<FlightSeat> getFlightSeatsInaRowChunck(Integer flightSegID, String cabinClass, String logicalCC, Integer rowGrp,
			Integer colId) {

		List<FlightSeat> flightSeatStatusTOs = new ArrayList<FlightSeat>();

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append(" SELECT fas.flight_am_seat_id , fas.status, fas.pax_type, ams.seat_code, ams.seat_type, fas.logical_cabin_class_code, ");
		queryContent.append(" ams.col_id, ams.row_grp_id, ams.cabin_class_code, lpad(ams.seat_code, 5 , '0') seat_code_1, ");
		queryContent.append(" ams.row_grp_id, ams.col_grp_id, ams.col_id ");
		queryContent.append(" FROM sm_t_flight_am_seat fas, sm_t_aircraft_model_seats ams ");
		queryContent.append(" WHERE fas.flt_seg_id = :flightSegID ");
		queryContent.append("  AND ams.cabin_class_code = :cabinClass ");
		queryContent.append("  AND fas.logical_cabin_class_code = :logicalCC ");
		queryContent.append("  AND ams.row_grp_id = :rowGrp ");
		queryContent.append("  AND ams.col_id = :colId ");
		queryContent.append("  AND fas.am_seat_id    = ams.am_seat_id ");
		queryContent.append(" ORDER BY seat_code_1 ");

		String str = queryContent.toString();
		SQLQuery query = getSession().createSQLQuery(str);
		query.setParameter("flightSegID", flightSegID);
		query.setParameter("cabinClass", cabinClass);
		query.setParameter("logicalCC", logicalCC);
		query.setParameter("rowGrp", rowGrp);
		query.setParameter("colId", colId);

		query.addScalar("flight_am_seat_id", IntegerType.INSTANCE);
		query.addScalar("status", StringType.INSTANCE);
		query.addScalar("seat_code", StringType.INSTANCE);
		query.addScalar("pax_type", StringType.INSTANCE);

		List<Object[]> results = query.list();

		if (results != null && !results.isEmpty()) {
			for (Object[] single : results) {
				FlightSeat flightSeat = new FlightSeat();
				flightSeat.setFlightAmSeatId((Integer) single[0]);
				flightSeat.setStatus((String) single[1]);
				flightSeat.setPaxType((single[2] == null) ? null : (String) single[3]);
				flightSeat.setSeatCode((String) single[3]);
				flightSeatStatusTOs.add(flightSeat);
			}

		}

		return flightSeatStatusTOs;

	}

	public FlightSeatStatusRS getFlightSeatStatuses(int flightSegmentId) {
		StringBuilder queryContent = new StringBuilder();

		queryContent
				.append("	SELECT fms.flight_am_seat_id flight_am_seat_id , fms.status status , ams.seat_code seat_code ,	")
				.append("		fms.logical_cabin_class_code logical_cabin_class_code , ams.col_id col_id , ams.row_grp_id row_grp_id ,	")
				.append("		lpad ( ams.seat_code , 4 , '0' ) seat_code_norm	")
				.append("	    FROM sm_t_flight_am_seat fms , sm_t_aircraft_model_seats ams	")
				.append("	    WHERE fms.am_seat_id = ams.am_seat_id AND fms.flt_seg_id = :flightSegId	")
				.append("	    ORDER BY seat_code_norm	");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("flightSegId", flightSegmentId);
		query.addScalar("flight_am_seat_id", IntegerType.INSTANCE);
		query.addScalar("status", StringType.INSTANCE);
		query.addScalar("seat_code", StringType.INSTANCE);
		query.addScalar("logical_cabin_class_code", StringType.INSTANCE);
		query.addScalar("col_id", IntegerType.INSTANCE);
		query.addScalar("row_grp_id", IntegerType.INSTANCE);
		query.addScalar("seat_code_norm", StringType.INSTANCE);
		List<Object[]> results = query.list();

		FlightSeatStatusRS resp = new FlightSeatStatusRS();
		FlightSeatStatusTO previous = FlightSeatStatusTO.FIRST;
		FlightSeatStatusTO current;

		for (Object[] single : results) {
			current = new FlightSeatStatusTO();
			current.setFlightAmSeatId((Integer) single[0]);
			current.setStatus((String) single[1]);
			current.setSeatCode((String) single[2]);
			current.setLogicalCabinClassCode((String) single[3]);
			current.setColId((Integer) single[4]);
			current.setRowGroupId((Integer) single[5]);

			current.setPrevSeat(previous);
			previous.setNextSeat(current);
			resp.addFlightSeatStatusTO(current);

			previous = current;
		}

		previous.setNextSeat(FlightSeatStatusTO.LAST);
		return resp;
	}

	@Override
	public Collection<SeatDTO> getFlightSeatasSeatDTOs(int flightSegID) throws ModuleException {
		
		StringBuilder sb = new StringBuilder();
		sb.append("select fas.am_seat_id am_seat_id, fas.ams_charge_id ams_charge_id, ")
		.append("  fas.status status, amc.charge_amount charge_amount")
		.append("  from sm_t_am_seat_charge amc inner join sm_t_flight_am_seat fas")
		.append("  on amc.ams_charge_id = fas.ams_charge_id where fas.flt_seg_id = :flightSegID");
		 
		SQLQuery query = getSession().createSQLQuery(sb.toString());
		query.setParameter("flightSegID", flightSegID);
		query.addScalar("am_seat_id", IntegerType.INSTANCE);
		query.addScalar("ams_charge_id", IntegerType.INSTANCE);
		query.addScalar("status", StringType.INSTANCE);
		query.addScalar("charge_amount", BigDecimalType.INSTANCE);		
		
		Collection<SeatDTO> seatDTOs = new ArrayList<SeatDTO>();
		List<Object[]> results = query.list();
		for(Object[] obj : results){
			SeatDTO seatDto = new SeatDTO();
			seatDto.setSeatID((Integer)obj[0]);
			seatDto.setChargeID((Integer)obj[1]);
			seatDto.setStatus((String)obj[2]);
			seatDto.setChargeAmount((BigDecimal)obj[3]);
			
			seatDTOs.add(seatDto);
		}
		 
		return seatDTOs;
	}

}
