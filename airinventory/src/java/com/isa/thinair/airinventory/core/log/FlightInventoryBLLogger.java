package com.isa.thinair.airinventory.core.log;

import org.apache.commons.logging.Log;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;

/**
 * Logs FlightInventoryBL events
 */
public class FlightInventoryBLLogger {

	public static void logCreateFlightSegInventory(Log log, CreateFlightInventoryRQ createFlightInventoryRQ,
			SegmentDTO segmentDTO, String cabinClassCode) {
		if (log.isDebugEnabled()) {
			log.debug("--------------------Creating FCCSegInventory--------------------\n\r" + "\tflightId             = "
					+ createFlightInventoryRQ.getFlightId() + "\n\r" + "\tcabinClass Code      = "
					+ cabinClassCode + "\n\r"
					+ "\tsegment Code         = "
					+ segmentDTO.getSegmentCode() + "\n\r" + "\tIsInvalidSegment     = " + segmentDTO.isInvalidSegment() + "\n\r");
		}
	}
}
