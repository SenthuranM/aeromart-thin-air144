/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.airinventory.api.model.PublishAvailability;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CriteriaParserForHibernate;

/**
 * Hibernate implementation for Booking Class manipulation calls.
 * 
 * @author Thejaka
 * @author Nasly
 * 
 * @isa.module.dao-impl dao-name="BookingClassDAOImpl"
 */
public class BookingClassDAOImpl extends PlatformBaseHibernateDaoSupport implements BookingClassDAO {
	private final Log log = LogFactory.getLog(getClass());

	@Override
	public BookingClass getBookingClass(String code) {
		BookingClass bookingClass = (BookingClass) get(BookingClass.class, code);
		return bookingClass;
	}

	/**
	 * Introduced this to reduce loading unnecessary column data and execution unnecessary db calls to child tables.
	 * 
	 * @param code
	 * @return
	 */
	@Override
	public BookingClass getLightWeightBookingClass(String code) {
		String hql = "SELECT new com.isa.thinair.airinventory.api.model.BookingClass(bc.bookingCode, bc.cabinClassCode,"
				+ " bc.fixedFlagChar, bc.bcType, bc.onHoldChar, bc.status, bc.logicalCCCode) from  BookingClass bc WHERE bc.bookingCode = ? ";
		List<BookingClass> colBookingClass = find(hql, code, BookingClass.class);
		if (colBookingClass.size() > 0) {
			return colBookingClass.get(0);
		}
		return null;
	}

	@Override
	public Map<String, Collection<String>> getBookingClassChargeGroups(Collection<String> bookingClasses,
			Collection<String> busBookingClasses) {
		String sql = "select bcg.BOOKING_CODE, bcg.CHARGE_GROUP_CODE from T_BC_CHARGE_GROUP bcg where bcg.BOOKING_CODE in ("
				+ BeanUtils.constructINString(bookingClasses) + ")";

		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

		final Collection<String> finalBusBCs = busBookingClasses;

		Map<String, Collection<String>> bookingClassWiseChargeGroupCodes = jdbcTemplate.query(sql,
				new ResultSetExtractor<Map<String, Collection<String>>>() {
					@Override
					public Map<String, Collection<String>> extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<String, Collection<String>> bookingClassWiseChargeGroupCodes = new HashMap<String, Collection<String>>();
						if (rs != null) {
							while (rs.next()) {
								String bookingClassCode = rs.getString("BOOKING_CODE");
								if (bookingClassWiseChargeGroupCodes.get(bookingClassCode) == null) {
									bookingClassWiseChargeGroupCodes.put(bookingClassCode, new HashSet<String>());
									if (!AppSysParamsUtil.isConsiderBCConfigForInfantSurcharge()
											|| (rs.getString("CHARGE_GROUP_CODE").equals(ChargeGroups.SURCHARGE) && AppSysParamsUtil
													.isConsiderBCConfigForInfantSurcharge())) {
										if (finalBusBCs != null && !finalBusBCs.contains(bookingClassCode)) {
											bookingClassWiseChargeGroupCodes.get(bookingClassCode).add(
													ChargeGroups.INFANT_SURCHARGE);
										}
									}
								}
								bookingClassWiseChargeGroupCodes.get(bookingClassCode).add(rs.getString("CHARGE_GROUP_CODE"));
							}
						}
						return bookingClassWiseChargeGroupCodes;
					}
				});
		if (bookingClassWiseChargeGroupCodes.keySet().size() != bookingClasses.size()) {
			for (String bookingClass : bookingClasses) {
				if (!bookingClassWiseChargeGroupCodes.containsKey(bookingClass)
						&& (!AppSysParamsUtil.isConsiderBCConfigForInfantSurcharge() || (bookingClassWiseChargeGroupCodes.get(
								bookingClass).contains(ChargeGroups.SURCHARGE) && AppSysParamsUtil
								.isConsiderBCConfigForInfantSurcharge()))) {

					if (finalBusBCs != null && !finalBusBCs.contains(bookingClass)) {
						bookingClassWiseChargeGroupCodes.put(bookingClass, new HashSet<String>());
						bookingClassWiseChargeGroupCodes.get(bookingClass).add(ChargeGroups.INFANT_SURCHARGE);
					}
				}
			}
		}
		return bookingClassWiseChargeGroupCodes;
	}

	@Override
	public Collection<Object[]> getBookingCodesWithFlags(String cabinClassCode) {

		String hql = "SELECT bookingCode, standardCodeChar, fixedFlagChar, bcType, allocationType  " + "FROM BookingClass "
				+ "WHERE cabinClassCode='" + cabinClassCode + "' " + "AND pax_type NOT IN ('" + BookingClass.PassengerType.GOSHOW
				+ "','" + BookingClass.PassengerType.INTERLINE + "','" + BookingClass.BookingClassType.OPEN_RETURN + "') "
				+ "AND status = '" + BookingClass.Status.ACTIVE + "' " + "AND bc_type != '"
				+ BookingClass.BookingClassType.OPEN_RETURN + "'" + "ORDER BY bookingCode";
		return find(hql, Object[].class);
	}

	@Override
	public Collection<String> getStandbyBookingClasses() {
		String hql = "SELECT bookingCode FROM BookingClass WHERE status = '" + BookingClass.Status.ACTIVE + "' "
				+ "AND bc_type = '" + BookingClass.BookingClassType.STANDBY + "'";
		return find(hql, String.class);
	}

	@Override
	public Collection<Object[]> getBookingCodesWithFlagsForLogicalCC(Collection<String> logicalCCCodes,
			Collection<String> bcListWithinFlightCutoffTime) {
		String hql = "SELECT bookingCode, standardCodeChar, fixedFlagChar, bcType, allocationType,"
				+ "logicalCCCode FROM BookingClass WHERE logicalCCCode in (" + BeanUtils.constructINString(logicalCCCodes)
				+ ") AND pax_type NOT IN ('" + BookingClass.PassengerType.GOSHOW + "','" + BookingClass.PassengerType.INTERLINE
				+ "','" + BookingClass.BookingClassType.OPEN_RETURN + "') " + "AND status = '" + BookingClass.Status.ACTIVE
				+ "' " + "AND bc_type != '" + BookingClass.BookingClassType.OPEN_RETURN + "'" + " AND bookingCode NOT IN ("
				+ BeanUtils.constructINString(bcListWithinFlightCutoffTime) + ") ORDER BY bookingCode";
		return find(hql, Object[].class);
	}

	public List<BookingClass> getAllGoshowBookingClasses() {
		String hql = "FROM BookingClass " + " WHERE paxType = '" + BookingClass.PassengerType.GOSHOW + "' and status='ACT' ";
		return find(hql, BookingClass.class);
	}

	@Override
	public BookingClass getGoShowBookingClass(String cabinclassCode, String paxCategoryCode) {

		Collection<String> withInCutOffTimeBCList = AirinventoryUtils.getAirInventoryConfig()
				.getGoshowBCListForAgentBookingsWithinCutoffTime();

		String hql = "FROM BookingClass " + " WHERE paxType = '" + BookingClass.PassengerType.GOSHOW + "' "
				+ " AND cabinClassCode = '" + cabinclassCode + "'" + " AND paxCategoryCode = '" + paxCategoryCode + "'";

		// goshowBCListForAgentBookingsWithinCutoffTime defined BC should not be considered
		if (withInCutOffTimeBCList != null && withInCutOffTimeBCList.size() > 0) {
			hql += " AND bookingCode NOT IN  (" + BeanUtils.constructINString(withInCutOffTimeBCList) + ")";
		}

		Collection<BookingClass> colBookingClass = find(hql, BookingClass.class);

		if (colBookingClass.size() > 1) {
			throw new CommonsDataAccessException("airinventory.masterdata.goshowfare.moreThanOneBCdefined",
					AirinventoryCustomConstants.INV_MODULE_CODE);
		} else {
			Iterator<BookingClass> itColBookingClass = colBookingClass.iterator();
			BookingClass bookingClass = null;

			if (itColBookingClass.hasNext()) {
				bookingClass = itColBookingClass.next();
			}

			return bookingClass;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public BookingClass getOpenReturnBookingClass(String logicalCabinClassCode) {
		String hql = "FROM BookingClass " + " WHERE bcType = '" + BookingClass.BookingClassType.OPEN_RETURN + "' "
				+ " AND logicalCCCode = '" + logicalCabinClassCode + "'";

		Collection<BookingClass> colBookingClass = find(hql, BookingClass.class);

		if (colBookingClass.size() > 1) {
			throw new CommonsDataAccessException("airinventory.logic.bc.openreturn.invalid",
					AirinventoryCustomConstants.INV_MODULE_CODE);
		} else {
			Iterator<BookingClass> itColBookingClass = colBookingClass.iterator();
			BookingClass bookingClass = null;

			if (itColBookingClass.hasNext()) {
				bookingClass = itColBookingClass.next();
			}

			return bookingClass;
		}
	}

	public void saveBookingClass(BookingClass bookingClass) {
		log.debug("Begin saving BookingClassDAOImpl.saveBookingClass(): bookingCode = " + bookingClass.getBookingCode());
		try {
			hibernateSave(bookingClass);
		} catch (Exception e) {
			log.error("Error in saving booking class code, ", e);
		}
		log.debug("End saving BookingClassDAOImpl.saveBookingClass(): bookingCode = " + bookingClass.getBookingCode());
	}

	@Override
	public void createBookingClass(BookingClassDTO bookingClassDTO) {

		BookingClass bc = bookingClassDTO.getBookingClass();
		log.debug("Begin BookingClassDAOImpl.createBookingClass(): bookingCode = " + bc.getBookingCode());

		checkBookingClassValidity(bc);

		if (bc.getStandardCode()) {
			// Nest rank must be unique within cabin class. A booking class can
			// be created with an existing nest rank after incrementing
			// the existing nest ranks by one
			String hqlNestRankCount = "Select count(BC.nestRank) " + "from BookingClass as BC "
					+ "where 	BC.nestRank = :nestRank " + "and BC.logicalCCCode = :logicalCCCode ";

			Long count = (Long) getSession().createQuery(hqlNestRankCount).setInteger("nestRank", bc.getNestRank().intValue())
					.setString("logicalCCCode", bc.getLogicalCCCode()).uniqueResult();

			if (count.intValue() > 0 && !bookingClassDTO.isShiftingNestRankAllowed()) {
				throw new CommonsDataAccessException("airinventory.logic.dao.bc.nestrank.duplicate",
						AirinventoryConstants.MODULE_NAME);
			}

			if (count.intValue() > 0) {
				// increase the nestranks of the other booking classes whose
				// nest rank is greater than or equal to new one's nest rank
				log.warn("Shifting nest ranks");
				String hqlUpdate = "update BookingClass set nestRank = nestRank + 1 " + "where 	nestRank >= :oldnestRank "
						+ "and logicalCCCode = :logicalCCCode";
				getSession().createQuery(hqlUpdate).setInteger("oldnestRank", bc.getNestRank().intValue())
						.setString("logicalCCCode", bc.getLogicalCCCode()).executeUpdate();
			}
		}

		hibernateSave(bc);
		log.debug("End BookingClassDAOImpl.createBookingClass(): bookingCode = " + bc.getBookingCode());
	}

	@Override
	public void updateBookingClass(BookingClassDTO bookingClassDTO) throws ModuleException {
		BookingClass bc = bookingClassDTO.getBookingClass();
		log.debug("Begin BookingClassDAOImpl.updateBookingClass(): bookingCode = " + bc.getBookingCode());

		this.checkBookingClassValidity(bc);
		this.isBookingClassUpdatable(bc);

		boolean statusUpdated = isStatusUpdated(bc.getBookingCode(), bc.getStatus());

		if (bc.getStandardCode()) {
			String hqlNestRankCount = "Select count(BC.nestRank) " + "from BookingClass as BC "
					+ "where 	BC.nestRank = :nestRank " + "and BC.logicalCCCode = :logicalCCCode "
					+ "and BC.bookingCode NOT IN  (:bookingCode)";

			Long count = (Long) getSession().createQuery(hqlNestRankCount).setInteger("nestRank", bc.getNestRank().intValue())
					.setString("logicalCCCode", bc.getLogicalCCCode()).setString("bookingCode", bc.getBookingCode())
					.uniqueResult();

			if ((count.intValue() > 0) && !bookingClassDTO.isShiftingNestRankAllowed()) {
				throw new CommonsDataAccessException("airinventory.logic.dao.bc.nestrank.duplicate",
						AirinventoryConstants.MODULE_NAME);
			}

			if (count.intValue() > 0) {
				// increase the nestranks of the other booking classes whose
				// nest rank is greater than or equal to the nest rank of
				// record being updated
				String hqlUpdate = "update BookingClass set nestRank = nestRank + 1 " + "where 	nestRank >= :oldnestRank "
						+ "and logicalCCCode = :logicalCCCode";
				getSession().createQuery(hqlUpdate).setInteger("oldnestRank", bc.getNestRank().intValue())
						.setString("logicalCCCode", bc.getLogicalCCCode()).executeUpdate();
			}
		}

		update(bc);

		log.debug("End BookingClassDAOImpl.updateBookingClass(): bookingCode = " + bc.getBookingCode());

		if (statusUpdated && isGDSPublishedBC(bc.getBookingCode())) {
			publishBCStatusUpdate(bc);
		}

	}

	/**
	 * @param modifiedBC
	 */
	private void isBookingClassUpdatable(BookingClass modifiedBC) {

		boolean[] bookingClassLinks = this.getBookingClassLinks(modifiedBC.getBookingCode());

		if (bookingClassLinks[0] || bookingClassLinks[1] || bookingClassLinks[2]) {

			BookingClass originalBookingClass = getBookingClass(modifiedBC.getBookingCode());
			boolean invalidUpdate = false;
			if (originalBookingClass.getFixedFlag() != modifiedBC.getFixedFlag()) {
				invalidUpdate = true;
			}
			if (originalBookingClass.getStandardCode() != modifiedBC.getStandardCode()) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getAllocationType().equals(modifiedBC.getAllocationType())) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getBcType().equals(modifiedBC.getBcType())) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getCabinClassCode().equals(modifiedBC.getCabinClassCode())) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getLogicalCCCode().equals(modifiedBC.getLogicalCCCode())) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getPaxCategoryCode().equals(modifiedBC.getPaxCategoryCode())) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getPaxType().equals(modifiedBC.getPaxType())) {
				invalidUpdate = true;
			}
			if (!originalBookingClass.getFareCategoryCode().equals(modifiedBC.getFareCategoryCode())) {
				invalidUpdate = true;
			}
			if (originalBookingClass.getGdsBCs() != null && modifiedBC.getGdsBCs() != null
					&& originalBookingClass.getGdsBCs().size() != modifiedBC.getGdsBCs().size()) {
				invalidUpdate = true;
			} else {
				modifiedBC.setGdsBCs(originalBookingClass.getGdsBCs());
			}
			if (invalidUpdate) {
				throw new CommonsDataAccessException("airinventory.logic.dao.bc.linkedtoinventor",
						AirinventoryConstants.MODULE_NAME);
			}
			this.getSession().evict(originalBookingClass);
		}
	}

	/**
	 * @param bookingClassCode
	 * @return
	 */
	private boolean[] getBookingClassLinks(String bookingClassCode) {

		Set<String> bookingClassCodes = new HashSet<String>();
		bookingClassCodes.add(bookingClassCode);
		// check whether if fare rules, agents linked to booking class
		HashMap<String, boolean[]> fareRulesAgentsMap = getBookingClassJDBCDAO().getFareRuleAgentCountsLinkedToBCs(
				bookingClassCodes);
		boolean[] bookingClassLinks = new boolean[] { false, false, false };
		if (fareRulesAgentsMap != null && !fareRulesAgentsMap.isEmpty() && fareRulesAgentsMap.get(bookingClassCode) != null) {
			boolean[] fareRuleAndAgent = fareRulesAgentsMap.get(bookingClassCode);
			bookingClassLinks[0] = fareRuleAndAgent[0];
			bookingClassLinks[1] = fareRuleAndAgent[1];
		}
		// check whether if booking class is allocated for inventories
		List<String> allocatedBCs = getBookingClassJDBCDAO().getBookingClassesLinkedToInv(bookingClassCodes);

		if (allocatedBCs != null && !allocatedBCs.isEmpty()) {
			bookingClassLinks[1] = true;
		}
		return bookingClassLinks;
	}

	@Override
	public void updateBCGroupIds(List<String> bcCodes, int groupId) {
		String hql = "update BookingClass set groupId=:groupId where bookingCode IN (:bcCodes)";
		getSession().createQuery(hql).setInteger("groupId", groupId).setParameterList("bcCodes", bcCodes).executeUpdate();
	}

	@Override
	public void deleteBookingClass(String bookingClassCode) {
		log.warn("Begin BookingClassDAOImpl.deleteBookingClass(): bookingCode = " + bookingClassCode);

		boolean[] bookingClassLinks = this.getBookingClassLinks(bookingClassCode);

		if (bookingClassLinks[0]) {
			throw new CommonsDataAccessException("airinventory.logic.dao.bc.linkedtofares", AirinventoryConstants.MODULE_NAME);
		}
		if (bookingClassLinks[1]) {
			throw new CommonsDataAccessException("airinventory.logic.dao.bc.linkedtoAgents", AirinventoryConstants.MODULE_NAME);
		}
		if (bookingClassLinks[2]) {
			throw new CommonsDataAccessException("airinventory.logic.dao.bc.linkedtoinventor", AirinventoryConstants.MODULE_NAME);
		}

		BookingClass bookingClass = (BookingClass) get(BookingClass.class, bookingClassCode);

		delete(bookingClass);

		log.warn("End BookingClassDAOImpl.deleteBookingClass(): bookingCode = " + bookingClassCode);
	}

	private boolean checkBookingClassValidity(BookingClass bc) {
		// only non-standard booking classes can carry fixed flag
		if (bc.getStandardCode() && bc.getFixedFlag()) {
			throw new CommonsDataAccessException("airinventory.logic.dao.bc.fixedflag.invalidattributes",
					AirinventoryConstants.MODULE_NAME);
		}

		// Standard booking classes must carry a nest rank
		if (bc.getStandardCode() && bc.getNestRank() == null) {
			throw new CommonsDataAccessException("airinventory.logic.dao.bc.nestrank.null", AirinventoryConstants.MODULE_NAME);
		}

		// Only one GOSHOW booking class per cabin class is allowed
		if (bc.getPaxType().equals(BookingClass.PassengerType.GOSHOW)) {
			Long goshowCount = (Long) getSession()
					.createQuery(
							"Select count(BC.paxType) " + "from BookingClass as BC " + "where 	BC.paxType =:paxType "
									+ "and BC.cabinClassCode =:cabinClassCode").setString("paxType", bc.getPaxType())
					.setString("cabinClassCode", bc.getCabinClassCode()).uniqueResult();

			if (goshowCount.intValue() > 0) {
				throw new CommonsDataAccessException("airinventory.logic.dao.bc.goshow.duplicate",
						AirinventoryConstants.MODULE_NAME);
			}
		}
		return true;
	}

	@Override
	public BookingClassDTO getBookingClassDTO(String bookingCode) throws ModuleException {
		SearchBCsCriteria criteria = new SearchBCsCriteria();
		criteria.setStartIndex(0);
		criteria.setPageSize(1);
		criteria.setBookingCode(bookingCode);

		Page<BookingClassDTO> pagedData = getBookingClassDTOs(criteria);
		if (pagedData.getPageData() != null && pagedData.getPageData().size() == 1) {
			return (BookingClassDTO) pagedData.getPageData().iterator().next();
		}
		return null;
	}

	@Override
	// @SuppressWarnings({ "rawtypes", "unchecked" })
			public
			Page<BookingClassDTO> getBookingClassDTOs(SearchBCsCriteria criteria) throws ModuleException {
		List<ModuleCriterion> hibernateCriteria = new ArrayList<ModuleCriterion>();
		if (criteria.getClassOfService() != null) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("cabinClassCode");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getClassOfService());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		if (criteria.getLogicalCCCode() != null) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("logicalCCCode");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getLogicalCCCode());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		if (criteria.getStandardFlage() != null && !criteria.getStandardFlage().equals("%")) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("standardCodeChar");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getStandardFlage());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		if (criteria.getFixedFlag() != null && !criteria.getFixedFlag().equals("%")) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("fixedFlagChar");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getFixedFlag());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		if (criteria.getBookingCode() != null && !criteria.getBookingCode().equals("%")) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("bookingCode");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getBookingCode());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		if (criteria.getAllocationType() != null && !criteria.getAllocationType().equals("")) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("allocationType");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getAllocationType());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		if (criteria.getStatus() != null && !criteria.getStatus().equals("")) {
			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("status");
			criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
			List<String> values = new ArrayList<String>();
			values.add(criteria.getStatus());
			criterion.setValue(values);

			hibernateCriteria.add(criterion);
		}

		List<String> orderByList = new ArrayList<String>();
		orderByList.add("bookingCode");

		@SuppressWarnings("unchecked")
		Page page = getPagedData(hibernateCriteria, criteria.getStartIndex(), criteria.getPageSize(), BookingClass.class,
				orderByList);

		if (page != null && page.getPageData() != null && page.getPageData().size() > 0) {
			LinkedHashMap<String, BookingClassDTO> bcDTOs = new LinkedHashMap<String, BookingClassDTO>();
			Iterator<BookingClass> bcIt = page.getPageData().iterator();
			while (bcIt.hasNext()) {
				BookingClass bc = bcIt.next();
				bcDTOs.put(bc.getBookingCode(), new BookingClassDTO(bc));
			}

			// set if farerules, agents linked to each booking class
			HashMap<?, ?> fareRulesAgentsMap = getBookingClassJDBCDAO().getFareRuleAgentCountsLinkedToBCs(bcDTOs.keySet());

			String bookingCode = null;
			boolean[] fareAg = null;
			BookingClassDTO bcDTO = null;

			if (fareRulesAgentsMap != null) {
				Iterator<?> fareRulesAgentsIt = fareRulesAgentsMap.keySet().iterator();
				while (fareRulesAgentsIt.hasNext()) {
					bookingCode = (String) fareRulesAgentsIt.next();
					fareAg = (boolean[]) fareRulesAgentsMap.get(bookingCode);
					bcDTO = (BookingClassDTO) bcDTOs.get(bookingCode);
					bcDTO.setHasLinkedFareRules(fareAg[0]);
					bcDTO.setHasLinkedAgents(fareAg[1]);
				}
			}

			// set if inventories linked to booking class
			Iterator<?> bcsLinkedToInvIt = getBookingClassJDBCDAO().getBookingClassesLinkedToInv(bcDTOs.keySet()).iterator();
			while (bcsLinkedToInvIt.hasNext()) {
				((BookingClassDTO) bcDTOs.get(bcsLinkedToInvIt.next())).setHasLinkedInventories(true);
			}

			Collection<BookingClassDTO> colBCDTOs = new ArrayList<BookingClassDTO>();
			Iterator<BookingClassDTO> bcDTOsIt = bcDTOs.values().iterator();
			while (bcDTOsIt.hasNext()) {
				colBCDTOs.add(bcDTOsIt.next());
			}

			page.setPageData(colBCDTOs);
		}
		return page;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<BookingClass> getBookingClasses(Collection<ModuleCriterion> criteria) {
		return CriteriaParserForHibernate
				.parse((List<ModuleCriterion>) criteria, getSession().createCriteria(BookingClass.class)).list();
	}

	@Override
	public HashMap<String, BookingClass> getBookingClassesMap(Collection<String> bookingCodes) {
		HashMap<String, BookingClass> bookingClassesMap = null;
		if (bookingCodes != null && bookingCodes.size() > 0) {
			Collection<ModuleCriterion> criteria = new ArrayList<ModuleCriterion>();

			ModuleCriterion criterion = new ModuleCriterion();
			criterion.setFieldName("bookingCode");
			criterion.setCondition(ModuleCriterion.CONDITION_IN);
			criterion.setValue(new ArrayList<String>(bookingCodes));
			criteria.add(criterion);

			Collection<BookingClass> bookingClasses = getBookingClasses(criteria);

			if (bookingClasses != null && bookingClasses.size() > 0) {
				bookingClassesMap = new HashMap<String, BookingClass>();
				Iterator<BookingClass> bookingClassesIt = bookingClasses.iterator();
				while (bookingClassesIt.hasNext()) {
					BookingClass bc = bookingClassesIt.next();
					bookingClassesMap.put(bc.getBookingCode(), bc);
				}
			}
		}
		return bookingClassesMap;
	}

	private BookingClassJDBCDAO getBookingClassJDBCDAO() {
		return (BookingClassJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("bookingClassJdbcDAO");
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<BookingClass> getFixedBookingClassesToRelease() {

		String hql = " FROM BookingClass " + " WHERE fixedFlagChar = '" + BookingClass.FIXED_FLAG_Y + "' " + " AND status = '"
				+ BookingClass.Status.ACTIVE + "'" + " AND releaseFlag = '" + BookingClass.ReleaseFlag.ACTIVE + "'"
				+ " AND releaseCutover is not null ";

		return find(hql, BookingClass.class);

	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<BookingClass>> getGDSBookingClasses(Collection<Integer> gdsIds) {
		Map<Integer, Collection<BookingClass>> gdsBookingClasses = null;

		if (gdsIds == null || gdsIds.size() == 0) {
			return null;
		}

		Collection<BookingClass> bookingClassesList = getSession()
				.createQuery(
						"SELECT distinct BC FROM BookingClass AS BC, GDSBookingClass AS GDSBC "
								+ "  WHERE BC.bookingCode = GDSBC.bookingCode " + "and GDSBC.gdsId IN (:gdsIds) ")
				.setParameterList("gdsIds", gdsIds).list();

		if (bookingClassesList != null) {
			gdsBookingClasses = new HashMap<Integer, Collection<BookingClass>>();

			for (int gdsId : gdsIds) {
				gdsBookingClasses.put(gdsId, new ArrayList<BookingClass>()); // java for-each loop is 'empty' save ->
																				// safe usages
			}
			for (BookingClass bookingClass : bookingClassesList) {
				Set<GDSBookingClass> gdsBCs = bookingClass.getGdsBCs();
				for (GDSBookingClass gdsBookingClass : gdsBCs) {
					if (gdsBookingClasses.containsKey(gdsBookingClass.getGdsId())) {
						gdsBookingClasses.get(gdsBookingClass.getGdsId()).add(bookingClass);
					}
				}
			}
		}

		return gdsBookingClasses;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<String> getSingleCharBookingClassesForGDS(int gdsId) {

		String sql = "select bcgds.mapped_bc from T_BOOKING_CLASS_GDS_MAPPING bcgds" + " where bcgds.gds_id = " + gdsId;

		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

		Collection<String> gdsBookingClasses = (Collection<String>) jdbcTemplate.query(sql, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<String> gdsBookingClasses = new ArrayList<String>();
				String singleCharBC;
				if (rs != null) {
					while (rs.next()) {
						singleCharBC = rs.getString("mapped_bc");
						gdsBookingClasses.add(singleCharBC);
					}
				}
				return gdsBookingClasses;
			}
		});
		return gdsBookingClasses;
	}

	@Override
	public String getCabinClassForBookingClass(String bookingClass) {
		String sql = "select bc.cabin_class_code from T_BOOKING_CLASS bc where bc.booking_code= '" + bookingClass + "'";

		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

		String cabinClass = jdbcTemplate.query(sql, new ResultSetExtractor<String>() {
			@Override
			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						return rs.getString("cabin_class_code");
					}
				}
				return null;
			}
		});
		return cabinClass;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getInactiveGdsBookingClasse() {
		String inactiveGdsBCsHql = "Select GBC.mappedBC from BookingClass as BC, GDSBookingClass as GBC where BC.status IN (:status) and GBC.bookingCode=BC.bookingCode";

		List<String> inactiveGdsBookingClasses = (List<String>) getSession().createQuery(inactiveGdsBCsHql)
				.setString("status", "INA").list();

		return inactiveGdsBookingClasses;

	}

	public Collection<BookingClass> getGoshowBCListForAgentBookingsWithinCutoffTime(Collection<String> bookingCodes) {
		String hql = "SELECT bc FROM BookingClass bc WHERE bc.bookingCode IN (" + BeanUtils.constructINString(bookingCodes) + ")";
		return find(hql, BookingClass.class);
	}

	public String getBookingClassType(String bookingCode) {
		String hql = "SELECT bc.bcType " + " FROM  BookingClass bc " + " WHERE bc.bookingCode = '" + bookingCode + "' ";
		List<String> bcTypes = find(hql, String.class);
		if (bcTypes != null && !bcTypes.isEmpty()) {
			return bcTypes.get(0);
		} else {
			return null;
		}
	}

	public String getBookingClassChargeFlags(String bookingCode) {
		String sql = "select bc.charge_flags from T_BOOKING_CLASS bc where bc.booking_code= '" + bookingCode + "'";

		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

		String chargeFlags = (String) jdbcTemplate.query(sql, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						return rs.getString("charge_flags");
					}
				}
				return null;
			}
		});
		return chargeFlags;
	}

	private boolean isGDSPublishedBC(String bookingCode) throws ModuleException {

		String gdsCountHql = "Select count(GDSBC.bookingCode) " + "from GDSBookingClass as GDSBC "
				+ "where GDSBC.bookingCode IN (:bookingCode)";
		Long numberOfPublishedGDS = (Long) getSession().createQuery(gdsCountHql).setString("bookingCode", bookingCode)
				.uniqueResult();

		if (numberOfPublishedGDS == null) {
			return false;

		}
		return numberOfPublishedGDS.longValue() > 0;

	}

	// TODO: hack due to t_publish_availability t_gds foreign key constraint
	private int getOneGdsId(String bookingCode) throws ModuleException {

		String gdsListHql = "Select GDSBC.gdsId " + "from GDSBookingClass as GDSBC "
				+ "where GDSBC.bookingCode IN (:bookingCode)";
		@SuppressWarnings("unchecked")
		List<Integer> publishedGdsList = (List<Integer>) getSession().createQuery(gdsListHql)
				.setString("bookingCode", bookingCode).list();

		if (publishedGdsList.isEmpty()) {
			throw new ModuleException("gdsservices.booking.class.status.publish.invalid.state");
		}

		return publishedGdsList.get(0).intValue();

	}

	private void publishBCStatusUpdate(BookingClass bookingClass) throws ModuleException {

		String statusChange = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_ACTIVATE_BC.getCode();

		// status updated to inactive (from active)
		if (BookingClass.Status.INACTIVE.contentEquals(bookingClass.getStatus())) {
			statusChange = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_INACTIVATE_BC.getCode();
		}

		PublishAvailability publishAvailability = new PublishAvailability();
		publishAvailability.setEventCode(statusChange);
		publishAvailability.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT.getCode());
		publishAvailability.setEventTimestamp(new Date());
		publishAvailability.setBookingCode(bookingClass.getBookingCode());
		publishAvailability.setSegAvailableSeats(0);
		publishAvailability.setGdsID(getOneGdsId(bookingClass.getBookingCode()));

		publishAvailability.setFccsaId(null);
		publishAvailability.setFccsbaId(null);
		publishAvailability.setBcAvailableSeats(0);
		publishAvailability.setBcStatus(null);
		publishAvailability.setFlightSegId(null);

		AirInventoryModuleUtils.getFlightInventoryDAO().saveOrUpdatePublishAvailability(publishAvailability);

	}

	private boolean isStatusUpdated(String bookingCode, String updatedStatus) {

		String bcStatusHql = "Select BC.status " + "from BookingClass as BC " + "where BC.bookingCode IN (:bookingCode)";
		String currentStatus = (String) getSession().createQuery(bcStatusHql).setString("bookingCode", bookingCode)
				.uniqueResult();

		return !currentStatus.contentEquals(updatedStatus);

	}
	
	@Override
	public Collection<BookingClass> getBookingClasses() {
		Collection<BookingClass> bookingClasses = getSession().createCriteria(BookingClass.class).list();
		return bookingClasses;
	}

}
