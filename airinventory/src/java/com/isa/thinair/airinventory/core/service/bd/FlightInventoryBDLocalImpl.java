package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;

/**
 * 
 * @author MN
 */
@Local
public interface FlightInventoryBDLocalImpl extends FlightInventoryBD {

}
