package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.InventoryTemplateBD;

/**
 * 
 * @author priyantha
 */
@Local
public interface InventoryTemplateBDLocalImpl extends InventoryTemplateBD {

}
