/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.model.FlightBaggage;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author mano
 *
 */

/**
 * @isa.module.dao-impl dao-name="BaggageDAOImpl"
 */
public class BaggageDAOImpl extends PlatformBaseHibernateDaoSupport implements BaggageDAO {


	public Collection<FlightBaggage> getFlightBaggages(Collection<Integer> flightBaggageIds) throws ModuleException {

		return find(
				"from FlightBaggage fb where fb.flightBaggageChargeId in (" + Util.buildIntegerInClauseContent(flightBaggageIds)
						+ ")", FlightBaggage.class);

	}

	public void saveFlightBaggage(FlightBaggage flightBaggage) throws ModuleException {
		hibernateSaveOrUpdate(flightBaggage);
	}

	public void saveFlightBaggage(Collection<FlightBaggage> flightBaggages) throws ModuleException {
		hibernateSaveOrUpdateAll(flightBaggages);
	}
	

	public Collection<FlightBaggage> getFlightBaggages(int flightSegId) throws ModuleException {
		return find("from FlightBaggage fb where fb.flightSegmentId in (" + flightSegId + ")", FlightBaggage.class);
	}

	public Collection<FlightBaggage> getFlightBaggages(int flightSegId, String cabinClass) throws ModuleException {
		return find(
				"from FlightBaggage fb where fb.flightSegmentId in (" + flightSegId + ") and (fb.logicalCCCode IN ("
						+ "select lcc.logicalCCCode from LogicalCabinClass lcc where lcc.cabinClassCode = '" + cabinClass
						+ "') OR fb.cabinClassCode ='" + cabinClass + "') AND fb.status = 'ACT' ", FlightBaggage.class);
	}

	public Collection<FlightBaggage> getFlightBaggages(int flightSegId, int chargeId, String cabinClass) throws ModuleException {
		return find(
				"from FlightBaggage fb where fb.flightSegmentId in (" + flightSegId + ") and fb.baggageChargeId in (" + chargeId
						+ ")" + getCabinClassCondition(cabinClass), FlightBaggage.class);
	}

	private String getCabinClassCondition(String cabinClass) {
		return (cabinClass != null && !"".equals(cabinClass)) ? " and fb.cabinClassCode ='" + cabinClass + "' " : "";
	}

	@Override
	public void deleteFlightBaggages(Collection<Integer> flightIds) throws ModuleException {
		String query = "delete FlightBaggage where " + Util.getReplaceStringForIN("flightSegmentId", flightIds);
		getSession().createQuery(query).executeUpdate();
	}

	@Override
	public void saveFlightBaggageMultiList(Collection<List<FlightBaggage>> flightBaggagesList)throws ModuleException {
		for (List<FlightBaggage> list : flightBaggagesList) {
			hibernateSaveOrUpdateAll(list);
		}
	}

}
