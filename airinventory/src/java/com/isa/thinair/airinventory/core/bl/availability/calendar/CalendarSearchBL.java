package com.isa.thinair.airinventory.core.bl.availability.calendar;

import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRQ;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRS;
import com.isa.thinair.airschedules.api.dto.CalendarFlightRS;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

public class CalendarSearchBL {

	public CalendarSearchRS getAvailableMinimumFares(CalendarSearchRQ searchRQ) throws ModuleException {

		ServiceResponce response = AirInventoryModuleUtils.getFlightBD().searchAvailableFlights(searchRQ);
		CalendarFlightRS availFlightRS = (CalendarFlightRS) response
				.getResponseParam(ResponceCodes.AVAILABILITY_FLIGHT_CALENDAR_SEARCH_SUCCESSFULL);
		CalendarFaresRQ calFareRQ = new CalendarFaresRQ(searchRQ, availFlightRS.getOndWiseDistinctOndCodes());
		CalendarFaresRS calFares = AirInventoryModuleUtils.getFareBD().getApplicableFares(calFareRQ);

		CalendarONDProcessor calFareProcessor = new CalendarONDProcessor(searchRQ, calFares, availFlightRS);
		return calFareProcessor.getCalendarResults();
	}
}