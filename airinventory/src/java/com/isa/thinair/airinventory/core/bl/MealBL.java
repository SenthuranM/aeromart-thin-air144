/**
 * 
 */
package com.isa.thinair.airinventory.core.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.FlightMeal;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.persistence.dao.MealDAO;
import com.isa.thinair.airinventory.core.persistence.dao.MealJDBCDAO;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;

public class MealBL implements Serializable {

	private static final long serialVersionUID = 6129380760557596136L;

	private MealDAO mealDAO = null;

	private static final Log log = LogFactory.getLog(MealBL.class);

	public MealBL() {
		if (mealDAO == null) {
			this.mealDAO = AirInventoryModuleUtils.getMealDAO();
		}
	}

	public void updateFlightMeals(Collection<Integer> flightMealIds, String status, UserPrincipal userPrincipal,
			ReservationAudit reservationAudit) throws ModuleException {

		Collection<FlightMeal> dbflightmeals = mealDAO.getFlightMeals(flightMealIds);
		if (dbflightmeals == null || dbflightmeals.size() < 1) {
			throw new ModuleException("airinventory.meals.notfound");
		}
		Integer selFltMId = null;
		List<Integer> vacatedMealChargeIds = new ArrayList<Integer>();
		for (Integer integer : flightMealIds) {
			selFltMId = integer;
			for (FlightMeal fltMeal : dbflightmeals) {
				if (selFltMId.intValue() == fltMeal.getFlightMealChargeId()) {
					if (status.equals(AirinventoryCustomConstants.FlightMealStatuses.RESERVED)) {
						fltMeal.setSoldMeals(fltMeal.getSoldMeals() + 1);
						fltMeal.setAvailableMeals(fltMeal.getAvailableMeals() - 1);
						fltMeal.setUserDetails(userPrincipal);
					} else if (status.equals(AirinventoryCustomConstants.FlightMealStatuses.VACANT)) {
						fltMeal.setSoldMeals(fltMeal.getSoldMeals() - 1);
						fltMeal.setAvailableMeals(fltMeal.getAvailableMeals() + 1);
						fltMeal.setUserDetails(userPrincipal);
						vacatedMealChargeIds.add(fltMeal.getMealChargeId());
					}
					mealDAO.saveFlightMeal(fltMeal);
				}
			}
		}

		if (reservationAudit != null && vacatedMealChargeIds.size() > 0) {
			String vacatedMealCodes = "";
			Collection<Meal> vacatedMeals = AirInventoryModuleUtils.getCommonMasterBD()
					.getMealsByMealCharge(vacatedMealChargeIds);

			for (Meal meal : vacatedMeals) {
				vacatedMealCodes += meal.getMealCode() + ", ";
			}

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.CanceledSegment.VACATED_MEALS, vacatedMealCodes);
		}

	}

	public Map<Integer, List<FlightMealDTO>> getMeals(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS,
			Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, boolean skipCutoverValidation, PnrFarePassenger pnrFarePax,
			Integer posSalesChannelCode)
			throws ModuleException {
		Map<Integer, List<FlightMealDTO>> flightSegMealMap = new HashMap<Integer, List<FlightMealDTO>>();
		FlightBD flightBd = AirInventoryModuleUtils.getFlightBD();
		Collection<FlightSegmentDTO> colSegs = flightBd.getFlightSegments(flightSegIdWiseCOS.keySet());
		String strCutOver = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.MEAL_CUTOVERTIME);

		if (strCutOver == null || strCutOver.trim().equals("")) {
			strCutOver = "24";
		}

		for (FlightSegmentDTO fltSeg : colSegs) {
			Integer flightSegId = fltSeg.getSegmentId();
			log.debug("flightSegID for Meal selection " + flightSegId);

			ClassOfServiceDTO cosDTO = flightSegIdWiseCOS.get(flightSegId);

			String cabinClass = null;
			String logcialCabinClass = null;
			if (cosDTO != null) {
				cabinClass = cosDTO.getCabinClassCode();
				logcialCabinClass = cosDTO.getLogicalCCCode();
			}

			Calendar departTime = new GregorianCalendar();
			departTime.setTime(fltSeg.getDepartureDateTimeZulu());
			Calendar currTime = new GregorianCalendar();

			long differnce = departTime.getTimeInMillis() - currTime.getTimeInMillis();
			long cutovertime = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
			if ((differnce > cutovertime) || skipCutoverValidation) {
				List<FlightMealDTO> meals = getMealJDBCDAO().getMealDTOs(flightSegId, cabinClass, logcialCabinClass,
						(flightSegIdWiseSelectedMeals == null ? null : flightSegIdWiseSelectedMeals.get(flightSegId)), pnrFarePax,
						posSalesChannelCode);
				flightSegMealMap.put(flightSegId, meals);
			}
		}
		return flightSegMealMap;
	}

	public Map<Integer, List<FlightMealDTO>> getMealsWithTranslations(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS,
			String selectedLanguage, Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, Integer posSalesChannelCode)
			throws ModuleException {
		Map<Integer, List<FlightMealDTO>> flightSegMealMap = new HashMap<Integer, List<FlightMealDTO>>();

		FlightBD flightBd = AirInventoryModuleUtils.getFlightBD();
		Collection<FlightSegmentDTO> colSegs = flightBd.getFlightSegments(flightSegIdWiseCOS.keySet());

		for (FlightSegmentDTO fltSeg : colSegs) {
			Integer flightSegId = fltSeg.getSegmentId();
			log.debug("flightSegID for Meal selection " + flightSegId);
			Calendar departTime = new GregorianCalendar();
			departTime.setTime(fltSeg.getDepartureDateTimeZulu());
			Calendar currTime = new GregorianCalendar();

			ClassOfServiceDTO cosDTO = flightSegIdWiseCOS.get(flightSegId);

			String cabinClass = null;
			String logcialCabinClass = null;
			if (cosDTO != null) {
				cabinClass = cosDTO.getCabinClassCode();
				logcialCabinClass = cosDTO.getLogicalCCCode();
			}

			long differnce = departTime.getTimeInMillis() - currTime.getTimeInMillis();
			long cutovertime = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
			if (differnce > cutovertime) {
				List<FlightMealDTO> meals = getMealJDBCDAO().getMealDTOsWithTranslations(flightSegId, cabinClass,
						logcialCabinClass, selectedLanguage,
						(flightSegIdWiseSelectedMeals == null ? null : flightSegIdWiseSelectedMeals.get(flightSegId)));			
				
				// Check for empty meal map - meaning there may not be translations - then call the meal method without
				// selected lang
				if (meals == null || meals.size() == 0) {
					meals = getMealJDBCDAO().getMealDTOs(flightSegId, cabinClass, null,
							(flightSegIdWiseSelectedMeals == null ? null : flightSegIdWiseSelectedMeals.get(flightSegId)), null,
							posSalesChannelCode);
				}

				flightSegMealMap.put(flightSegId, meals);
			}
		}
		return flightSegMealMap;
	}	

	/**
	 * @param flightMealDTOs
	 * @param userPrincipal
	 * @param isFromEdit
	 * @return ServiceResponce
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce assignFlightMealCharges(Collection<FlightMealDTO> flightMealDTOs, UserPrincipal userPrincipal,
			Integer chargeId, boolean isFromEdit) throws ModuleException {

		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();
		Map<Integer, Collection<Integer>> mapFlightMeals = new HashMap<Integer, Collection<Integer>>();
		Map<Integer, Set<String>> mapTemplateCcCode = new HashMap<Integer, Set<String>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIds = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsFailed = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsNewlyAssigned = new HashMap<String, Collection<Integer>>();

		for (FlightMealDTO flightMealDTO : flightMealDTOs) {
			if (mapFlightMeals.containsKey(flightMealDTO.getTemplateId())) {
				Collection<Integer> flightSegIDs = mapFlightMeals.get(flightMealDTO.getTemplateId());
				String cabinClsCode = flightMealDTO.getCabinClassCode();
				String logicalCCCode = flightMealDTO.getLogicalCCCode();
				if (!mapFlightMeals.get(flightMealDTO.getTemplateId()).contains(flightMealDTO.getFlightSegmentID())) {
					flightSegIDs.add(flightMealDTO.getFlightSegmentID());
					mapFlightMeals.put(flightMealDTO.getTemplateId(), flightSegIDs);
				}
				if (cabinClsCode != null && !cabinClsCode.equals("")) {
					mapTemplateCcCode.get(flightMealDTO.getTemplateId()).add(cabinClsCode);
				} else {
					mapTemplateCcCode.get(flightMealDTO.getTemplateId()).add("_" + logicalCCCode);
				}
			} else {
				Collection<Integer> flightSegIDs = new ArrayList<Integer>();
				String cabinClsCode = flightMealDTO.getCabinClassCode();
				String logicalCCCode = flightMealDTO.getLogicalCCCode();
				flightSegIDs.add(flightMealDTO.getFlightSegmentID());
				mapFlightMeals.put(flightMealDTO.getTemplateId(), flightSegIDs);
				Set<String> ccSet = new HashSet<String>();
				if (cabinClsCode != null && !cabinClsCode.equals("")) {
					ccSet.add(cabinClsCode);
				} else {
					ccSet.add("_" + logicalCCCode);
				}

				mapTemplateCcCode.put(flightMealDTO.getTemplateId(), ccSet);
			}
		}

		for (Integer integer : mapFlightMeals.keySet()) {
			Integer templateID = integer;
			Collection<Integer> colFlightSegIDs = mapFlightMeals.get(templateID);
			ServiceResponce serRes = assignFlightMealCharges(colFlightSegIDs, templateID, userPrincipal, mapTemplateCcCode,
					chargeId, isFromEdit);
			arrAssigned.addAll((Collection<Integer>) serRes
					.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED));
			arrNotAssigned.addAll((Collection<Integer>) serRes
					.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED));
			arrNewlyAssigned.addAll((Collection<Integer>) serRes
					.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED));
			if (arrAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIds.put(Integer.toString(templateID),
						(Collection<Integer>) serRes.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED));
			}
			if (arrNotAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(templateID),
						(Collection<Integer>) serRes.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED));
			}
			if (arrNewlyAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsNewlyAssigned
						.put(Integer.toString(templateID), (Collection<Integer>) serRes
								.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED));
			}
		}
		Map<String, Map> map = new HashMap();
		map.put(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED, mapAuditTemplateIDcolSegIds);
		map.put(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED, mapAuditTemplateIDcolSegIdsFailed);
		map.put(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED, mapAuditTemplateIDcolSegIdsNewlyAssigned);
		AuditAirinventory.doAuditMealAssignMealCharges(map, userPrincipal);
		serviceResponse.setSuccess(true);
		serviceResponse.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED, arrAssigned);
		serviceResponse.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED, arrNotAssigned);
		serviceResponse.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return serviceResponse;
	}

	/**
	 * @author Harsha Udayapriya
	 * @param templateID
	 * @param userPrincipal
	 * @param isFromEdit
	 * @param flightMealDTOs
	 * @param templateCcCode
	 * @return ServiceResponce
	 * @throws ModuleException
	 */
	private ServiceResponce assignFlightMealCharges(Collection<Integer> colFlightSegmentIDs, Integer templateID,
			UserPrincipal userPrincipal, Map<Integer, Set<String>> mapTemplateCcCode, Integer chargeId, boolean isFromEdit)
			throws ModuleException {
		DefaultServiceResponse res = new DefaultServiceResponse(false);
		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();

		// blank template
		if (templateID == null || templateID == 0) {
			for (Integer flightSegID : colFlightSegmentIDs) {

				/*
				 * Collection exstFlightMeal = null; if (chargeId == null) exstFlightMeal =
				 * getMealDAO().getFlightMeals(flightSegID); else exstFlightMeal =
				 * getMealDAO().getFlightMeals(flightSegID, chargeId.intValue());
				 */

				Collection<FlightMeal> exstFlightMeal = getMealDAO().getFlightMeals(flightSegID, null);

				if (exstFlightMeal != null && !exstFlightMeal.isEmpty()) {
					Collection<FlightMeal> flightMeals = new ArrayList<FlightMeal>();
					for (FlightMeal flightMeal : exstFlightMeal) {
					//	flightMeal.setMealChargeId(null);
						flightMeal.setUserDetails(userPrincipal);
						if (flightMeal.getCabinClassCode() == null && flightMeal.getLogicalCCCode() == null) {
							flightMeal.setCabinClassCode("Y");// FIXME if tamplate
							// id is 0 have to
							// assign a CC code
						}
						flightMeals.add(flightMeal);
						flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
					}
					getMealDAO().saveFlightMeal(flightMeals);
					arrAssigned.add(flightSegID);
				}
			}
			res.setSuccess(true);
			res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED, arrAssigned);
			res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED, arrNotAssigned);
			res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED, arrNewlyAssigned);
			return res;
		}

		Set<String> ccSet = mapTemplateCcCode.get(templateID);
		Collection<MealCharge> mealCharges = null;
		if (chargeId == null) {
			mealCharges = getMealCharges(templateID);
		} else {
			mealCharges = getMealChargeForTemplate(templateID, chargeId);
		}
		Map<Integer, String> mealChargeCCMap = prepareMealChargeCabinClassMap(mealCharges, ccSet);

		for (String classOfService : ccSet) {
			if (mealCharges.isEmpty()) {
				throw new ModuleException("airinventory.logic.bl.flightmealcharge.failed.countmismatch",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			}

			if (mealChargeCCMap.isEmpty() && !isFromEdit) {
				throw new ModuleException("airinventory.logic.bl.flightmealcharge.failed.nomeals",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			}

			Collection<MealCharge> colCOSMealCharges = getCOSmealCharges(mealCharges, classOfService);

			for (Integer flightSegID : colFlightSegmentIDs) {
				boolean isEmptyMealChargeTemplate = false;
				boolean hasSoldMeal = false;
				boolean hasSoldMLEmptyTemp = false;
				Map<Integer, Integer> mapMealIdSoldCnt = null;

				Collection<FlightMeal> exFltMealsCharges = null;
				if (chargeId == null) {
					exFltMealsCharges = getMealDAO().getFlightMeals(flightSegID, classOfService);
				} else {
					exFltMealsCharges = getMealDAO().getFlightMeals(flightSegID, chargeId.intValue(), classOfService);
				}

				// Collection exFltMealsCharges = getMealDAO().getFlightMeals(flightSegID);

				if (exFltMealsCharges != null && !exFltMealsCharges.isEmpty()) {
					Map<Integer, FlightMeal> soldMealMapBycharge = new HashMap<Integer, FlightMeal>();
					Map<Integer, MealCharge> soldMealMapByMeal = new HashMap<Integer, MealCharge>();
					List<Integer> soldMealChargeList = new ArrayList<Integer>();
					Collection<MealCharge> soldMealCharges = null;

					for (FlightMeal flightMeal : exFltMealsCharges) {
						if (flightMeal.getMealChargeId() == null) {
							isEmptyMealChargeTemplate = true;
							if (flightMeal.getSoldMeals() > 0) {
								hasSoldMLEmptyTemp = true;// has sold meals, but
								// existing seg has no
								// template
							}
						} else {
							if (flightMeal.getSoldMeals() > 0) {
								soldMealMapBycharge.put(flightMeal.getMealChargeId(), flightMeal);
								soldMealChargeList.add(flightMeal.getMealChargeId());
								hasSoldMeal = true; // assign true, when having at
								// least one sold meals in
								// existing set
							}
						}
						// hasMealSold = false;
					}

					if (soldMealChargeList.size() > 0) {
						soldMealCharges = getChargeBD().getMealCharges(soldMealChargeList);
						if (soldMealCharges != null) {
							for (MealCharge soldMealCharge : soldMealCharges) {
								soldMealMapByMeal.put(soldMealCharge.getMealId(), soldMealCharge);
							}
						}
					}

					if (hasSoldMLEmptyTemp) {
						mapMealIdSoldCnt = getSoldMealCountMap(flightSegID);
					}

					if (isEmptyMealChargeTemplate) {
						for (FlightMeal exFlightMeal : exFltMealsCharges) {
							exFlightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
						}
						getMealDAO().saveFlightMeal(exFltMealsCharges);

						Collection<FlightMeal> flightMeals = new ArrayList<FlightMeal>();
						int cntSoldMeals = 0;
						int cntAvailableMeals = 0;
						int cntAllocatedMeals = 0;
						for (MealCharge mealCharge : colCOSMealCharges) {
							String ccString = (isFromEdit) ? classOfService : mealChargeCCMap.get(mealCharge.getChargeId());
							if (ccString == null) {
								continue;
							}

							// Skip adding meals in existing template
							if (soldMealMapByMeal.containsKey(mealCharge.getMealId())) {
								continue;
							}

							FlightMeal flightMeal = new FlightMeal();
							flightMeal.setFlightSegId(flightSegID);
							flightMeal.setMealChargeId(mealCharge.getChargeId());
							if (ccString.startsWith("_")) {
								flightMeal.setLogicalCCCode(ccString.substring(1));
							} else {
								flightMeal.setCabinClassCode(ccString);
							}
							flightMeal.setUserDetails(userPrincipal);
							cntAllocatedMeals = mealCharge.getAllocatedMeal();
							flightMeal.setAllocatedMeals(cntAllocatedMeals);

							if (mapMealIdSoldCnt != null && mapMealIdSoldCnt.containsKey(mealCharge.getMealId())) {
								cntSoldMeals = mapMealIdSoldCnt.get(mealCharge.getMealId());
								cntAvailableMeals = cntAllocatedMeals - cntSoldMeals;
								flightMeal.setSoldMeals(cntSoldMeals);
								flightMeal.setAvailableMeals(cntAvailableMeals);
							} else {
								flightMeal.setAvailableMeals(cntAllocatedMeals);
								flightMeal.setSoldMeals(0);
							}
							if (mealCharge.getStatus().equals(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE)) {
								flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE);
							} else {
								flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
							}
							flightMeals.add(flightMeal);

						}
						getMealDAO().saveFlightMeal(flightMeals);
						arrAssigned.add(flightSegID);
						// continue;
					}

					Map<Integer, FlightMeal> mapExFlightMeals = getExFlightMeals(exFltMealsCharges, soldMealMapBycharge);

					// If meals were not sold, assign the new charges
					if (!isEmptyMealChargeTemplate && !hasSoldMeal) {
						// Load
						Collection<FlightMeal> fightMeals = new ArrayList<FlightMeal>();
						Collection<FlightMeal> staleFlightMeals = new ArrayList<FlightMeal>();
						for (MealCharge mealCharge : colCOSMealCharges) {
							String ccString = (isFromEdit) ? classOfService : mealChargeCCMap.get(mealCharge.getChargeId());
							if (ccString == null) {
								continue;
							}

							FlightMeal flightMeal = null;
							if (mapExFlightMeals.containsKey(mealCharge.getChargeId())) {
								flightMeal = mapExFlightMeals.get(mealCharge.getChargeId());
								flightMeal.setUserDetails(userPrincipal);
								// remove existing flight meal from the map
								mapExFlightMeals.remove(mealCharge.getChargeId());
							} else {
								flightMeal = new FlightMeal();
								flightMeal.setUserDetails(userPrincipal);
							}

							flightMeal.setFlightSegId(flightSegID);
							flightMeal.setMealChargeId(mealCharge.getChargeId());
							if (ccString.startsWith("_")) {
								flightMeal.setLogicalCCCode(ccString.substring(1));
							} else {
								flightMeal.setCabinClassCode(ccString);
							}

							if (mealCharge.getStatus().equals(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE)) {
								flightMeal.setAllocatedMeals(mealCharge.getAllocatedMeal());
								flightMeal.setSoldMeals(0);
								flightMeal.setAvailableMeals(mealCharge.getAllocatedMeal());
								flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE);
							} else if (mealCharge.getStatus().equals("INA")) {
								flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
								flightMeal.setAllocatedMeals(0);
								flightMeal.setAvailableMeals(0);
								flightMeal.setSoldMeals(0);
								flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
							}
							fightMeals.add(flightMeal);
						}
						if (!mapExFlightMeals.isEmpty()) {
							Collection<FlightMeal> colFlightMeals = mapExFlightMeals.values();
							for (FlightMeal staleFlightMeal : colFlightMeals) {
								staleFlightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
								staleFlightMeals.add(staleFlightMeal);
							}
						}
						// inactivate stale flight meals
						getMealDAO().saveFlightMeal(staleFlightMeals);
						// save new flight meals
						getMealDAO().saveFlightMeal(fightMeals);
						arrAssigned.add(flightSegID);
					} else if (hasSoldMeal) {
						// Meals Sold, override the template.
						Collection<FlightMeal> flightMeals = new ArrayList<FlightMeal>();
						Collection<FlightMeal> staleFlightMeals = new ArrayList<FlightMeal>();

						for (MealCharge mealCharge : colCOSMealCharges) {
							String ccString = (isFromEdit) ? classOfService : mealChargeCCMap.get(mealCharge.getChargeId());

							if (ccString == null) {
								continue;
							}

							FlightMeal flightMeal = null;
							int cntSoldMeals = 0;
							int cntAvailableMeals = 0;
							int cntAllocatedMeals = mealCharge.getAllocatedMeal();

							if (mapExFlightMeals.containsKey(mealCharge.getChargeId())) {
								flightMeal = mapExFlightMeals.get(mealCharge.getChargeId());
								flightMeal.setUserDetails(userPrincipal);
								// remove existing flight meal from the map
								mapExFlightMeals.remove(mealCharge.getChargeId());
							} else {
								flightMeal = new FlightMeal();
								flightMeal.setUserDetails(userPrincipal);
								flightMeal.setFlightSegId(flightSegID);
							}

							flightMeal.setMealChargeId(mealCharge.getChargeId());
							if (ccString.startsWith("_")) {
								flightMeal.setLogicalCCCode(ccString.substring(1));
							} else {
								flightMeal.setCabinClassCode(ccString);
							}

							if (mealCharge.getStatus().equals(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE)) {
								if (flightMeal.getStatus() != null) {
									flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE);
									if (flightMeal.getSoldMeals() > 0 && cntAllocatedMeals > 0) {
										cntSoldMeals = flightMeal.getSoldMeals();
										cntAvailableMeals = cntAllocatedMeals - cntSoldMeals;
										flightMeal.setAvailableMeals(cntAvailableMeals);
										flightMeal.setAllocatedMeals(cntAllocatedMeals);
									} else {
										flightMeal.setAllocatedMeals(cntAllocatedMeals);
										flightMeal.setAvailableMeals(cntAllocatedMeals);
										flightMeal.setSoldMeals(0);
									}
								} else {
									flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE);
									flightMeal.setAllocatedMeals(cntAllocatedMeals);
									flightMeal.setAvailableMeals(cntAllocatedMeals);
									flightMeal.setSoldMeals(0);
								}
							} else {
								flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
							}

							flightMeals.add(flightMeal);
						}
						if (!mapExFlightMeals.isEmpty()) {
							Collection<FlightMeal> colFlightMeals = mapExFlightMeals.values();
							for (FlightMeal staleFlightMeal : colFlightMeals) {
								staleFlightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
								staleFlightMeals.add(staleFlightMeal);
							}
						}
						// inactivate stale flight meals
						getMealDAO().saveFlightMeal(staleFlightMeals);
						// save new flight meals
						getMealDAO().saveFlightMeal(flightMeals);
						arrAssigned.add(flightSegID);
					}

				} else if (!isFromEdit) {
					// create charges for flights
					createChargesForNewFlights(colCOSMealCharges, flightSegID, userPrincipal);
					arrAssigned.add(flightSegID);
					arrNewlyAssigned.add(flightSegID);
				}
			}

		}
		updateFlightAnciAssignStatus(arrAssigned, arrNewlyAssigned, arrNotAssigned);
		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	private Collection<MealCharge> getCOSmealCharges(Collection<MealCharge> mealCharges, String classOfServise) {
		Collection<MealCharge> cosMealCharges = new ArrayList<MealCharge>();
		for (MealCharge mealCharge : mealCharges) {
			if (classOfServise.startsWith("_")) {
				if (classOfServise.substring(1).equals(mealCharge.getLogicalCCCode())) {
					cosMealCharges.add(mealCharge);
				}
			} else {
				if (classOfServise.equals(mealCharge.getCabinClass())) {
					cosMealCharges.add(mealCharge);
				}
			}
		}
		return cosMealCharges;
	}

	private Map<Integer, Integer> getSoldMealCountMap(int flightSegID) throws ModuleException {
		Collection<FlightMealDTO> flightMeals = getMealJDBCDAO().getSoldMeals(flightSegID);
		Map<Integer, Integer> mapSoldMeals = new HashMap<Integer, Integer>();
		for (FlightMealDTO flightMeal : flightMeals) {
			mapSoldMeals.put(flightMeal.getMealId(), flightMeal.getSoldMeals());
		}
		return mapSoldMeals;
	}

	private Map<Integer, FlightMeal> getExFlightMeals(Collection<FlightMeal> exFlightMeals, Map<Integer, FlightMeal> soldMealMap) {
		Map<Integer, FlightMeal> map = new HashMap<Integer, FlightMeal>();
		for (FlightMeal flightMeal : exFlightMeals) {
			flightMeal = soldMealMap.get(flightMeal.getMealChargeId()) == null ? flightMeal : soldMealMap.get(flightMeal
					.getMealChargeId());
			map.put(flightMeal.getMealChargeId(), flightMeal);
		}
		return map;
	}

	private Collection<MealCharge> getMealCharges(Integer templateID) throws ModuleException {
		MealTemplate mealTemplate = getChargeBD().getMealTemplate(templateID);
		return mealTemplate.getMealCharges();
	}

	private Collection<MealCharge> getMealChargeForTemplate(Integer templateID, Integer chargeID) throws ModuleException {
		MealTemplate mealTemplate = getChargeBD().getMealTemplate(templateID);
		Collection<MealCharge> mealCharges = new ArrayList<MealCharge>();

		for (MealCharge mc : mealTemplate.getMealCharges()) {
			if (mc.getChargeId() == chargeID.intValue()) {
				mealCharges.add(mc);
			}
		}

		if (mealCharges.size() > 0) {
			return mealCharges;
		} else {
			return mealTemplate.getMealCharges();
		}
	}

	private void createChargesForNewFlights(Collection<MealCharge> mealCharges, int flightSegID, UserPrincipal userPrincipal)
			throws ModuleException {
		Collection<FlightMeal> flightMeals = new ArrayList<FlightMeal>();
		for (MealCharge mealCharge : mealCharges) {
			FlightMeal flightMeal = new FlightMeal();
			flightMeal.setFlightSegId(flightSegID);
			flightMeal.setMealChargeId(mealCharge.getChargeId());
			flightMeal.setCabinClassCode(mealCharge.getCabinClass());
			flightMeal.setLogicalCCCode(mealCharge.getLogicalCCCode());
			if (mealCharge.getStatus().equals("ACT")) {
				flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.ACTIVE);
				flightMeal.setAllocatedMeals(mealCharge.getAllocatedMeal());
				flightMeal.setSoldMeals(0);
				flightMeal.setAvailableMeals(mealCharge.getAllocatedMeal());
			} else if (mealCharge.getStatus().equals("INA")) {
				flightMeal.setStatus(AirinventoryCustomConstants.FlightMealStatuses.INACTIVE);
				flightMeal.setAllocatedMeals(0);
				flightMeal.setSoldMeals(0);
				flightMeal.setAvailableMeals(0);
			}
			flightMeal.setUserDetails(userPrincipal);
			flightMeals.add(flightMeal);
		}
		if (!flightMeals.isEmpty()) {
			getMealDAO().saveFlightMeal(flightMeals);
		}

	}

	/**
	 * @author Harsha Udayapriya
	 * @param sourceFlightId
	 * @param destFlightIds
	 * @param userPrincipal
	 * @param classOfService
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce assignFlightMealChargesRollFoward(int sourceFlightId, Collection<Integer> destFlightIds,
			UserPrincipal userPrincipal, ArrayList<String> lstSelectedSeg) throws ModuleException {
		// 1.Get srctemplateID map <segId ,FlightMealDTO>
		// 2.Get desSegId col <Collection <flightSegIds> >
		// 3.Assign templates

		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();
		DefaultServiceResponse res = new DefaultServiceResponse(false);
		Map<Integer, Set<String>> mapTempalteCcCode = new HashMap<Integer, Set<String>>();
		Set<String> ccSet = new HashSet<String>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIds = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsFailed = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsNewlyAssigned = new HashMap<String, Collection<Integer>>();

		// 1. getting templateIDs for src flight
		Collection<FlightMealDTO> colGetTemplateIds = getMealJDBCDAO().getTempleateIds(sourceFlightId, null);
		for (FlightMealDTO flightMealDTO : colGetTemplateIds) {
			if (flightMealDTO.getCabinClassCode() != null) {
				ccSet.add(flightMealDTO.getCabinClassCode());
			} else {
				ccSet.add("_" + flightMealDTO.getLogicalCCCode());
			}
		}
		Map<String, FlightMealDTO> mapSegIdFlightMealDTO = buildMapWithSegCode(colGetTemplateIds);

		// 2. getting all flightSegIds between roll period
		Collection<FlightMealDTO> colFlightSegIds = getFlightBD().getFlightSegIdsforFlightforMeal(destFlightIds);
		Map mapSegCodeColSegIds = buildMapWithSegCodeColSegIds(colFlightSegIds);
		// 3.
		for (String segCode : mapSegIdFlightMealDTO.keySet()) {
			if (lstSelectedSeg.contains(segCode)) {
				lstSelectedSeg.remove(segCode);
			}
			FlightMealDTO flightMealDTO = mapSegIdFlightMealDTO.get(segCode);
			mapTempalteCcCode.put(flightMealDTO.getTemplateId(), ccSet);
			ServiceResponce sr = assignFlightMealCharges((Collection) mapSegCodeColSegIds.get(segCode),
					flightMealDTO.getTemplateId(), userPrincipal, mapTempalteCcCode, null, false);
			arrAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED));
			arrNotAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED));
			arrNewlyAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED));
			if (arrAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIds.put(Integer.toString(flightMealDTO.getTemplateId()),
						(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED));
			}
			if (arrNotAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(flightMealDTO.getTemplateId()),
						(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED));
			}
			if (arrNewlyAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(flightMealDTO.getTemplateId()),
						(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED));
			}
		}

		if (lstSelectedSeg != null && !lstSelectedSeg.isEmpty()) {
			for (String segCode : lstSelectedSeg) {
				if (mapSegCodeColSegIds.containsKey(segCode)) {
					Integer templateId = new Integer(0);
					ServiceResponce sr = assignFlightMealCharges((Collection) mapSegCodeColSegIds.get(segCode), templateId,
							userPrincipal, mapTempalteCcCode, null, false);
					arrAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED));
					arrNotAssigned.addAll((Collection) sr
							.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED));
					arrNewlyAssigned.addAll((Collection) sr
							.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED));
					if (arrAssigned.size() > 0) {
						mapAuditTemplateIDcolSegIds.put(Integer.toString(templateId),
								(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED));
					}
					if (arrNotAssigned.size() > 0) {
						mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(templateId),
								(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED));
					}
					if (arrNewlyAssigned.size() > 0) {
						mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(templateId),
								(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED));
					}

				}

			}
		}

		// Audit record
		Map<String, Map> map = new HashMap();
		map.put(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED, mapAuditTemplateIDcolSegIds);
		map.put(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED, mapAuditTemplateIDcolSegIdsFailed);
		map.put(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED, mapAuditTemplateIDcolSegIdsNewlyAssigned);
		AuditAirinventory.doAuditMealRollFowardMealCharges(map, userPrincipal);

		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	/**
	 * Deletes the meal templates
	 * 
	 * @author Harshana
	 * @param flightSegIDs
	 * @throws ModuleException
	 */
	public void deleteFlightMeals(Collection<Integer> flightSegIDs) throws ModuleException {
		getMealDAO().deleteFlightMeals(flightSegIDs);
	}

	private Map<String, FlightMealDTO> buildMapWithSegCode(Collection<FlightMealDTO> colTemplateIds) {
		Map<String, FlightMealDTO> mapSegCodeTempIds = new HashMap<String, FlightMealDTO>();
		for (FlightMealDTO flightMealDTO : colTemplateIds) {
			mapSegCodeTempIds.put(flightMealDTO.getFlightSegmentCode(), flightMealDTO);
		}
		return mapSegCodeTempIds;
	}

	@SuppressWarnings("rawtypes")
	private Map buildMapWithSegCodeColSegIds(Collection<FlightMealDTO> flightSegIds) {
		Map mapSegCodeColSegIds = new HashMap();
		for (FlightMealDTO flightmealDTO : flightSegIds) {
			addToCollectionOfMap(mapSegCodeColSegIds, flightmealDTO);
		}
		return mapSegCodeColSegIds;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void addToCollectionOfMap(Map map, FlightMealDTO flightmealDTO) {
		if (map.containsKey(flightmealDTO.getFlightSegmentCode())) {
			Collection colExisting = (Collection) map.get(flightmealDTO.getFlightSegmentCode());
			colExisting.add(flightmealDTO.getFlightSegmentID());
			map.put(flightmealDTO.getFlightSegmentCode(), colExisting);
		} else {
			Collection col = new HashSet();
			col.add(flightmealDTO.getFlightSegmentID());
			map.put(flightmealDTO.getFlightSegmentCode(), col);
		}
	}

	private Map<Integer, String> prepareMealChargeCabinClassMap(Collection<MealCharge> mealCharges, Set<String> ccSet) {
		Map<Integer, String> mealChargeCCMap = new HashMap<Integer, String>();
		for (String classOfService : ccSet) {
			for (MealCharge mealCharge : mealCharges) {
				if (classOfService.startsWith("_")) {
					if (mealCharge.getLogicalCCCode() != null
							&& mealCharge.getLogicalCCCode().equals(classOfService.substring(1))) {
						mealChargeCCMap.put(mealCharge.getChargeId(), classOfService);
					}
				} else {
					if (mealCharge.getCabinClass() != null && mealCharge.getCabinClass().equals(classOfService)) {
						mealChargeCCMap.put(mealCharge.getChargeId(), classOfService);
					}
				}
			}
		}
		return mealChargeCCMap;
	}

	private ChargeBD getChargeBD() {
		return AirInventoryModuleUtils.getChargeBD();
	}

	private MealDAO getMealDAO() {
		return (MealDAO) AirInventoryUtil.getInstance().getLocalBean("MealDAOImplProxy");
	}

	private MealJDBCDAO getMealJDBCDAO() {
		return (MealJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("mealJDBCDAO");
	}

	private FlightBD getFlightBD() {
		return AirInventoryModuleUtils.getFlightBD();
	}

	private void updateFlightAnciAssignStatus(ArrayList<Integer> arrAssigned, ArrayList<Integer> arrNewlyAssigned,
			ArrayList<Integer> arrNotAssigned) throws ModuleException {

		FlightBD flightBd = AirInventoryModuleUtils.getFlightBD();

		if (arrNotAssigned != null && arrNotAssigned.size() > 0) {
			flightBd.updateFltAnciAssignStatusByFltSegId(arrNotAssigned, ReservationInternalConstants.SERVICE_CALLER.MEAL,
					AnciDefaultTemplStatusEnum.CREATED_W_ERRORS);
		}

		if (arrAssigned != null && arrAssigned.size() > 0) {
			flightBd.updateFltAnciAssignStatusByFltSegId(arrAssigned, ReservationInternalConstants.SERVICE_CALLER.MEAL,
					AnciDefaultTemplStatusEnum.CREATED);
		}

		if (arrNewlyAssigned != null && arrNewlyAssigned.size() > 0) {
			flightBd.updateFltAnciAssignStatusByFltSegId(arrNewlyAssigned, ReservationInternalConstants.SERVICE_CALLER.MEAL,
					AnciDefaultTemplStatusEnum.CREATED);
		}

	}
	
	public List<FlightMealDTO> getMealsForPreference() throws ModuleException {
		
		List<FlightMealDTO> meals = getMealJDBCDAO().getMealDTOs();

		return meals;
	}
}
