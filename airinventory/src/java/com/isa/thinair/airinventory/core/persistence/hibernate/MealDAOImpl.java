/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.Collection;
import java.util.HashSet;

import com.isa.thinair.airinventory.api.model.FlightMeal;
import com.isa.thinair.airinventory.core.persistence.dao.MealDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @isa.module.dao-impl dao-name="MealDAOImpl"
 */
public class MealDAOImpl extends PlatformHibernateDaoSupport implements MealDAO {

	@Override
	public Collection<FlightMeal> getFlightMeals(Collection<Integer> flightMealIds) throws ModuleException {
		flightMealIds = new HashSet<Integer>(flightMealIds);
		return find(
				"from FlightMeal fm where fm.flightMealChargeId in (" + Util.buildIntegerInClauseContent(flightMealIds) + ")", FlightMeal.class);

	}

	@Override
	public Collection<FlightMeal> getFlightMeals(int flightSegId, String classOfService) throws ModuleException {
		String cabinClass = "";
		String logicalCCCode = "";
		if (classOfService != null) {
			if (classOfService.startsWith("_")) {
				logicalCCCode = classOfService.substring(1);
			} else {
				cabinClass = classOfService;
			}
		}
		return find(
				"from FlightMeal fm where fm.flightSegId in ("
						+ flightSegId
						+ ")"
						+ ((cabinClass != null && !"".equals(cabinClass)) ? getCabinClassCondition(cabinClass) : "")
						+ ((logicalCCCode != null && !"".equals(logicalCCCode))
								? getLogicalCabinClassCondition(logicalCCCode)
								: ""), FlightMeal.class);
	}

	@Override
	public Collection<FlightMeal> getFlightMeals(int flightSegId, int chargeId, String classOfService) throws ModuleException {
		String cabinClass = "";
		String logicalCCCode = "";
		if (classOfService.startsWith("_")) {
			logicalCCCode = classOfService.substring(1);
		} else {
			cabinClass = classOfService;
		}
		return find(
				"from FlightMeal fm where fm.flightSegId in ("
						+ flightSegId
						+ ") and fm.mealChargeId in ("
						+ chargeId
						+ ")"
						+ ((cabinClass != null && !"".equals(cabinClass)) ? getCabinClassCondition(cabinClass) : "")
						+ ((logicalCCCode != null && !"".equals(logicalCCCode))
								? getLogicalCabinClassCondition(logicalCCCode)
								: ""), FlightMeal.class);
	}

	@Override
	public void saveFlightMeal(FlightMeal flightMeal) throws ModuleException {
		hibernateSaveOrUpdate(flightMeal);
	}

	@Override
	public Integer createFlightMeal(FlightMeal flightMeal) throws ModuleException {
		return (Integer)hibernateSave(flightMeal);
	}

	@Override
	public void saveFlightMeal(Collection<FlightMeal> flightMeals) throws ModuleException {
		hibernateSaveOrUpdateAll(flightMeals);
	}

	private String getCabinClassCondition(String cabinClass) {
		return (cabinClass != null && !"".equals(cabinClass)) ? " and fm.cabinClassCode ='" + cabinClass + "' " : "";
	}

	private String getLogicalCabinClassCondition(String logicalCCCode) {
		return (logicalCCCode != null && !"".equals(logicalCCCode)) ? " and fm.logicalCCCode ='" + logicalCCCode + "' " : "";
	}

	@Override
	public void deleteFlightMeals(Collection<Integer> flightIDs) {
		if (flightIDs != null && !flightIDs.isEmpty()) {
			String flightMealDeleteQuery = "DELETE FlightMeal WHERE " + Util.getReplaceStringForIN("flightSegId", flightIDs);
			getSession().createQuery(flightMealDeleteQuery).executeUpdate();

		}
	}

}
