package com.isa.thinair.airinventory.core.persistence.jdbc.ps;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.PreparedStatementCreator;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * @author Nasly
 * 
 */
public class RetriveBucketsPrepStmtCreator implements PreparedStatementCreator {

	private String query;
	private Object[] criteria;

	public RetriveBucketsPrepStmtCreator(String query, Object[] criteria) {
		this.query = query;
		this.criteria = criteria;
	}

	public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(this.query);
		int index = 0;
		ps.setObject(++index, criteria[0]);
		ps.setString(++index, getReturnSegmentCode((String) criteria[1]));
		ps.setDate(++index, new java.sql.Date(CalendarUtil.getCurrentSystemTimeInZulu().getTime()));
		ps.setString(++index, Fare.STATUS_ACTIVE);
		ps.setObject(++index, criteria[0]);
		ps.setString(++index, BookingClass.STANDARD_CODE_Y);
		ps.setString(++index, BookingClass.FIXED_FLAG_N);

		return ps;
	}

	private String getReturnSegmentCode(String segmentCode) {
		String retSeg = "";
		String[] stations = StringUtils.split(segmentCode, "/");
		for (int i = stations.length; i > 0; --i) {
			if (retSeg.equals("")) {
				retSeg = stations[i - 1];
			} else {
				retSeg += "/" + stations[i - 1];
			}
		}
		return retSeg;
	}
}
