package com.isa.thinair.airinventory.core.bl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.bundledfare.ApplicableBundledFareSelectionCriteria;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OnDFareSegChargesRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentFareDTO;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * Methods related to bundled fare in reservation flow
 * 
 * @author rumesh
 * 
 */
public class BundledFareBL {

	public void getApplicableBundledFares(ChangeFaresDTO changeFaresDTO, AvailableFlightSearchDTO availableFlightSearchCriteria)
			throws ModuleException {
		if (AppSysParamsUtil.isBundledFaresEnabled(availableFlightSearchCriteria.getAppIndicator())
				&& availableFlightSearchCriteria.isBundledFareApplicable()) {
			Set<Integer> allOndSequences = new HashSet<Integer>();

			Collection<OndFareDTO> newOndFareDTOs = changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs();
			for (OndFareDTO newOndFareDTO : newOndFareDTOs) {
				allOndSequences.add(newOndFareDTO.getOndSequence());
			}

			String agentCode = availableFlightSearchCriteria.getAgentCode();

			for (Integer ondSequence : allOndSequences) {

				Set<String> flightNumbers = new HashSet<String>();
				String bookingClass = null;
				Date departureDateZulu = null;
				String selectedLogicalCC = null;

				LinkedHashMap<Integer, SegmentFareDTO> ondSegmentsMap = changeFaresDTO.getNewSegmentFaresMap(ondSequence);
//				if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
//					ondSegmentsMap = changeFaresDTO.getRequestedOndWiseOndNewSegmentFaresMap(ondSequence);
//				}
				Collection<SegmentFareDTO> ondSegmentFareDTOs = ondSegmentsMap.values();

				if (ondSegmentFareDTOs != null && !ondSegmentFareDTOs.isEmpty()) {
					List<SegmentFareDTO> ondSegmentFareDTOList = new ArrayList<SegmentFareDTO>(ondSegmentFareDTOs);
					Collections.sort(ondSegmentFareDTOList, new Comparator<SegmentFareDTO>() {

						@Override
						public int compare(SegmentFareDTO obj1, SegmentFareDTO obj2) {
							if (obj1 != null && obj2 != null && obj1.getFlightSegmentDTO() != null
									&& obj2.getFlightSegmentDTO() != null) {
								return obj1.getFlightSegmentDTO().getDepartureDateTimeZulu()
										.compareTo(obj2.getFlightSegmentDTO().getDepartureDateTimeZulu());
							}
							return 0;
						}
					});

					Set<String> bookingClasses = new HashSet<String>();
					List<String> ondCodes = new ArrayList<String>();
					for (SegmentFareDTO segmentFareDTO : ondSegmentFareDTOList) {
						FlightSegmentDTO flightSegmentDTO = segmentFareDTO.getFlightSegmentDTO();
						String bcChargeFlags = AirInventoryModuleUtils.getBookingClassDAO().getBookingClassChargeFlags(
								segmentFareDTO.getFareSummaryDTO().getBookingClassCode());
						if (!InventoryAPIUtil.isBusSegment(flightSegmentDTO) && !isSurchargeExcluded(bcChargeFlags)) {
							if (departureDateZulu == null) {
								departureDateZulu = flightSegmentDTO.getDepartureDateTimeZulu();
							}

							if (selectedLogicalCC == null) {
								selectedLogicalCC = segmentFareDTO.getSegmentSeatDistsDTO().getLogicalCabinClass();
							}

							flightNumbers.add(flightSegmentDTO.getFlightNumber());
							ondCodes.add(segmentFareDTO.getSegmentCode());
							bookingClasses.add(segmentFareDTO.getSegmentSeatDistsDTO().getBookingCode());
						}
					}

					if (bookingClasses.isEmpty()) {
						continue;
					}

					String ondCode = "";
					for (int i = 0; i < ondCodes.size(); i++) {
						String segCode = ondCodes.get(i);
						if (i == 0) {
							ondCode = segCode;
						} else {
							ondCode = ondCode.concat(segCode.substring(segCode.indexOf("/")));
						}
					}

					ApplicableBundledFareSelectionCriteria bundledFareCriteria = new ApplicableBundledFareSelectionCriteria(
							bookingClass, bookingClasses, ondCode, departureDateZulu, flightNumbers, agentCode,
							availableFlightSearchCriteria.getPreferredLanguage(), availableFlightSearchCriteria.getChannelCode(),
							availableFlightSearchCriteria.getPointOfSale());

					List<BundledFareLiteDTO> bundledFareDetails = AirInventoryModuleUtils.getBundledFareBD()
							.getApplicableBundledFares(bundledFareCriteria);

					if (bundledFareDetails != null && !bundledFareDetails.isEmpty()) {

						if (bookingClasses.size() > 1) {
							// Update Segment Booking Classes
							Map<String, String> segmentBookingClasses = new HashMap<String, String>();
							for (SegmentFareDTO segmentFareDTO : ondSegmentFareDTOList) {
								FlightSegmentDTO flightSegmentDTO = segmentFareDTO.getFlightSegmentDTO();
								if (!InventoryAPIUtil.isBusSegment(flightSegmentDTO)) {
									segmentBookingClasses.put(flightSegmentDTO.getSegmentCode(), segmentFareDTO
											.getFareSummaryDTO().getBookingClassCode());
								}
							}

							for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareDetails) {
								bundledFareLiteDTO.setSegmentBookingClass(segmentBookingClasses);
							}
						} else {
							for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareDetails) {
								if (bundledFareLiteDTO.getBookingClasses() == null
										|| bundledFareLiteDTO.getBookingClasses().isEmpty()) {
									bundledFareLiteDTO.setBookingClasses(bookingClasses);
								}
							}
						}

						if (!changeFaresDTO.getAvailableBundledFares().containsKey(ondSequence)) {
							changeFaresDTO.getAvailableBundledFares().put(ondSequence,
									new HashMap<String, List<BundledFareLiteDTO>>());
						}

						Map<String, List<BundledFareLiteDTO>> logicalCabinBundledFares = changeFaresDTO
								.getAvailableBundledFares().get(ondSequence);

						if (!logicalCabinBundledFares.containsKey(selectedLogicalCC)) {
							logicalCabinBundledFares.put(selectedLogicalCC, new ArrayList<BundledFareLiteDTO>());
						}

						logicalCabinBundledFares.get(selectedLogicalCC).addAll(bundledFareDetails);
					}

				}

			}
		}
	}

	public void getApplicableBundledFares(Collection<AvailableIBOBFlightSegment> availableIBOBFlightSegments,
			AvailableFlightSearchDTO availableFlightSearchDTO) {
		if (AppSysParamsUtil.isBundledFaresEnabled(availableFlightSearchDTO.getAppIndicator())
				&& availableFlightSearchDTO.isBundledFareApplicable()) {
			boolean isIBEorMobileBooking = false;
			/*
			 * Android & IOS apps are using service-app ibe api
			 */
			if (SalesChannelsUtil.isAppEngineWebOrMobile(availableFlightSearchDTO.getAppIndicator())) {
				isIBEorMobileBooking = true;
			}
			for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableIBOBFlightSegments) {
				if (isIBEorMobileBooking) {
					Date prefferedDate = availableFlightSearchDTO.getOndInfo(availableIBOBFlightSegment.getOndSequence())
							.getPreferredDateTimeStart();
					Date ondFlightDate = availableIBOBFlightSegment.getDepartureDate();
					boolean searchAvailableBundles = BundledFareBL.recalculateAvailableBundledFare(prefferedDate, ondFlightDate);

					if (!searchAvailableBundles) {
						continue;
					}
				}

				String ondCode = availableIBOBFlightSegment.getOndCode();
				String agentCode = availableFlightSearchDTO.getAgentCode();
				Date departureDateZulu = availableIBOBFlightSegment.getDepartureDateZulu();
				Set<String> flightNumbers = new HashSet<String>();
				String bookingClass = null;

				for (FlightSegmentDTO flightSegmentDTO : availableIBOBFlightSegment.getFlightSegmentDTOs()) {
					if (flightSegmentDTO.getOperationTypeID() == null
							|| (flightSegmentDTO.getOperationTypeID() != null && AirScheduleCustomConstants.OperationTypes.BUS_SERVICE != flightSegmentDTO
									.getOperationTypeID())) {
						flightNumbers.add(flightSegmentDTO.getFlightNumber());
					}
				}

				Set<String> bookingClasses = new HashSet<String>();
				if (availableIBOBFlightSegment.isDirectFlight()) {
					AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableIBOBFlightSegment;
					if (availableFlightSegment.getSelectedLogicalCabinClass() != null) {
						if (!isSurchargeExcluded(availableIBOBFlightSegment.getSelectedFareChargeRatio(availableIBOBFlightSegment
								.getSelectedLogicalCabinClass()))) {
							Collection<SeatDistribution> seatDistributions = availableFlightSegment
									.getSeatsAvailability(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
							// get the highest nest ranked booking code, in case of nested seats
							SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils
									.getLastElement(seatDistributions);
							bookingClasses.add(highestRankedSeatDistribution.getBookingClassCode());
						}

					}
				} else {
					AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;
					List<AvailableFlightSegment> availableFlightSegments = availableConnectedFlight.getAvailableFlightSegments();
					ondCode = null;

					if (availableConnectedFlight.getSelectedLogicalCabinClass() != null) {
						for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
							String selectedFareChgRatio = availableConnectedFlight
									.getSelectedFareChargeRatio(availableConnectedFlight.getSelectedLogicalCabinClass());
							FlightSegmentDTO flightSegmentDTO = availableFlightSegment.getFlightSegmentDTO();

							if (selectedFareChgRatio == null) {
								selectedFareChgRatio = availableFlightSegment.getSelectedFareChargeRatio(availableConnectedFlight
										.getSelectedLogicalCabinClass());
							}

							if (!availableFlightSegment.isBusSegment() && !isSurchargeExcluded(selectedFareChgRatio)) {
								Collection<SeatDistribution> seatDistributions = availableFlightSegment
										.getSeatsAvailability(availableConnectedFlight.getSelectedLogicalCabinClass());
								// get the highest nest ranked booking code, in case of nested seats
								SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils
										.getLastElement(seatDistributions);
								bookingClasses.add(highestRankedSeatDistribution.getBookingClassCode());

								ondCode = AirProxyReservationUtil.buildOndCode(ondCode, flightSegmentDTO.getSegmentCode());
							}
						}
					}

				}

				if (bookingClasses.isEmpty()) {
					continue;
				}

				ApplicableBundledFareSelectionCriteria bundledFareCriteria = new ApplicableBundledFareSelectionCriteria(
						bookingClass, bookingClasses, ondCode, departureDateZulu, flightNumbers, agentCode,
						availableFlightSearchDTO.getPreferredLanguage(), availableFlightSearchDTO.getChannelCode(),
						availableFlightSearchDTO.getPointOfSale());

				List<BundledFareLiteDTO> bundledFareDetails = AirInventoryModuleUtils.getBundledFareBD()
						.getApplicableBundledFares(bundledFareCriteria);

				if (bundledFareDetails != null && !bundledFareDetails.isEmpty()) {
					availableIBOBFlightSegment.getAvailableBundledFares().clear();
					if (bookingClasses.size() > 1) {
						populateSegmentBookingClassAllocation(availableIBOBFlightSegment, bundledFareDetails);
					} else if (!isIBEorMobileBooking) {
						for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareDetails) {
							if (bundledFareLiteDTO.getBookingClasses() == null
									|| bundledFareLiteDTO.getBookingClasses().isEmpty()) {
								bundledFareLiteDTO.setBookingClasses(bookingClasses);
							}
						}
					}
					availableIBOBFlightSegment.addAvailableBundledFares(
							availableIBOBFlightSegment.getSelectedLogicalCabinClass(), bundledFareDetails);
				}

			}
		}
	}

	public void populateSegmentBookingClassAllocation(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			List<BundledFareLiteDTO> bundledFareDetails) {
		Map<String, String> segmentBookingClasses = new HashMap<String, String>();

		if (availableIBOBFlightSegment.isDirectFlight()) {
			AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableIBOBFlightSegment;
			setSegmentWiseBookingClasses(availableFlightSegment, segmentBookingClasses);
		} else {
			AvailableConnectedFlight connectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;
			List<AvailableFlightSegment> availableFlightSegments = connectedFlight.getAvailableFlightSegments();
			for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
				setSegmentWiseBookingClasses(availableFlightSegment, segmentBookingClasses);
			}
		}

		for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareDetails) {
			bundledFareLiteDTO.setSegmentBookingClass(segmentBookingClasses);
		}

	}

	private void setSegmentWiseBookingClasses(AvailableFlightSegment availableFlightSegment,
			Map<String, String> segmentBookingClasses) {
		Collection<SeatDistribution> seatDistributions = availableFlightSegment.getSeatsAvailability(availableFlightSegment
				.getSelectedLogicalCabinClass());
		// get the highest nest ranked booking code, in case of nested seats
		SeatDistribution highestRankedSeatDistribution = (SeatDistribution) BeanUtils.getLastElement(seatDistributions);
		List<FlightSegmentDTO> flightSegmentDTOs = availableFlightSegment.getFlightSegmentDTOs();

		for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
			if (!InventoryAPIUtil.isBusSegment(flightSegmentDTO)) {
				segmentBookingClasses.put(flightSegmentDTO.getSegmentCode(), highestRankedSeatDistribution.getBookingClassCode());
			}
		}
	}

	/**
	 * This calculate tax portion for bundled fare fee when bundled fare is not selected
	 * 
	 * @param availableIBOBFlightSegment
	 */
	public void calculateTaxPortionForBundledFee(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		Map<String, List<BundledFareLiteDTO>> availableBundledFares = availableIBOBFlightSegment.getAvailableBundledFares();

		if (availableBundledFares != null && !availableBundledFares.isEmpty()) {
			for (Entry<String, List<BundledFareLiteDTO>> bundledFareEntry : availableBundledFares.entrySet()) {
				String logicalCabinClass = bundledFareEntry.getKey();
				List<BundledFareLiteDTO> bundledFares = bundledFareEntry.getValue();

				if (bundledFares != null && !bundledFares.isEmpty()) {
					Collection<QuotedChargeDTO> applicableCharges = new ArrayList<QuotedChargeDTO>();
					if (availableIBOBFlightSegment.isDirectFlight()) {
						applicableCharges = getApplicableTaxCharges(availableIBOBFlightSegment.getCharges(logicalCabinClass));
					} else {
						AvailableConnectedFlight connectedFlights = (AvailableConnectedFlight) availableIBOBFlightSegment;
						for (AvailableFlightSegment availFlightSegment : connectedFlights.getAvailableFlightSegments()) {
							if (availFlightSegment.getCharges(logicalCabinClass) != null) {
								applicableCharges.addAll(availFlightSegment.getCharges(logicalCabinClass));
							}
						}
					}

					BundledFareDTO selectedBundledFare = availableIBOBFlightSegment.getSelectedBundledFare();

					for (BundledFareLiteDTO bundledFareLiteDTO : bundledFares) {
						BigDecimal bundledFee = bundledFareLiteDTO.getPerPaxBundledFee();
						BigDecimal bundledFeeTaxPortion = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (QuotedChargeDTO quotedChargeDTO : applicableCharges) {

							String chargeBasis = quotedChargeDTO.getChargeBasis();

							if (ChargeRate.CHARGE_BASIS_PFS.equals(chargeBasis)
									|| ChargeRate.CHARGE_BASIS_PTFS.equals(chargeBasis)
									|| ChargeRate.CHARGE_BASIS_PS.equals(chargeBasis)) {

								BigDecimal defineChargeValueMin = quotedChargeDTO.getChargeValueMin();
								BigDecimal defineChargeValueMax = quotedChargeDTO.getChargeValueMax();

								BigDecimal chargePercentage = new BigDecimal(quotedChargeDTO.getChargeValuePercentage());
								BigDecimal taxValue = AccelAeroCalculator.multiply(bundledFee, chargePercentage);
								taxValue = AccelAeroCalculator.divide(taxValue, 100);

								Double calcEffectiveTax = quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT);

								Double totTaxAfterBundledFeeTax = calcEffectiveTax + taxValue.doubleValue();

								// if min and max value not defined it gets the calculated value
								if ((defineChargeValueMin != null)
										&& (totTaxAfterBundledFeeTax < defineChargeValueMin.doubleValue())) {
									totTaxAfterBundledFeeTax = defineChargeValueMin.doubleValue();
								} else if ((defineChargeValueMax != null)
										&& (totTaxAfterBundledFeeTax > defineChargeValueMax.doubleValue())) {
									totTaxAfterBundledFeeTax = defineChargeValueMax.doubleValue();
								}

								Double effectiveBundledFeeTax = totTaxAfterBundledFeeTax - calcEffectiveTax;

								bundledFeeTaxPortion = AccelAeroCalculator.add(bundledFeeTaxPortion, new BigDecimal(
										effectiveBundledFeeTax));
							}

							bundledFareLiteDTO.setApplicableTaxPortion(bundledFeeTaxPortion);

						}
						// }
					}
				}
			}
		}
	}

	private Collection<QuotedChargeDTO> getApplicableTaxCharges(Collection<QuotedChargeDTO> allCharges) {
		Collection<QuotedChargeDTO> applicableCharges = new ArrayList<QuotedChargeDTO>();
		if (allCharges != null) {
			for (QuotedChargeDTO quotedChargeDTO : allCharges) {
				if (quotedChargeDTO.getChargeGroupCode().equals(ChargeGroup.TAX) && quotedChargeDTO.isChargeValueInPercentage()) {
					applicableCharges.add(quotedChargeDTO);
				}
			}
		}
		return applicableCharges;
	}

	public void updateBundledFareFee(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		BundledFareDTO bundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();
		if (bundledFareDTO != null) {
			BigDecimal bundledFareFee = bundledFareDTO.getPerPaxBundledFee();
			String chargeCode = bundledFareDTO.getChargeCode();

			if (availableIBOBFlightSegment.isDirectFlight()) {
				Collection<QuotedChargeDTO> quotedChargeDTOs = availableIBOBFlightSegment.getCharges(availableIBOBFlightSegment
						.getSelectedLogicalCabinClass());
				double paxBundledFee = bundledFareFee.doubleValue();
				for (QuotedChargeDTO quotedChargeDTO : quotedChargeDTOs) {
					if (quotedChargeDTO.getChargeCode().equals(chargeCode)) {
						assignEffectiveBundledFareChargeToApplicablePaxTypes(quotedChargeDTO, paxBundledFee);
						quotedChargeDTO.setChargeValuePercentage(paxBundledFee);
					}
				}
			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;

				for (String availableLogicalCabinClass : availableConnectedFlight.getAvailableLogicalCabinClasses()) {

					Integer fareType = availableConnectedFlight.getFareType(availableLogicalCabinClass);

					if (fareType == FareTypes.HALF_RETURN_FARE || fareType == FareTypes.OND_FARE) {

						if (availableConnectedFlight.isSeatsAvailable(availableLogicalCabinClass)
								&& availableConnectedFlight.getFare(availableLogicalCabinClass) != null) {

							double effectiveBundledFee = bundledFareDTO.getPerPaxBundledFee().doubleValue();

							Collection<QuotedChargeDTO> quotedChargeDTOs = availableConnectedFlight
									.getCharges(availableLogicalCabinClass);
							for (QuotedChargeDTO quotedChargeDTO : quotedChargeDTOs) {
								if (quotedChargeDTO.getChargeCode().equals(chargeCode)) {
									assignEffectiveBundledFareChargeToApplicablePaxTypes(quotedChargeDTO, effectiveBundledFee);
									quotedChargeDTO.setChargeValuePercentage(effectiveBundledFee);
								}
							}
						}

					} else if (fareType == FareTypes.PER_FLIGHT_FARE || fareType == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE
							|| fareType == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE) {

						List<AvailableFlightSegment> availableFlightSegments = availableConnectedFlight
								.getAvailableFlightSegments();

						int airSegmentCount = getAirSegmentCount(availableFlightSegments);

						BigDecimal[] amountBreakDown = AccelAeroCalculator.roundAndSplit(bundledFareDTO.getPerPaxBundledFee(),
								airSegmentCount);
						int ondFlightCounter = 0;
						for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
							if (!availableFlightSegment.isBusSegment()) {
								Collection<QuotedChargeDTO> quotedChargeDTOs = availableFlightSegment
										.getCharges(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
								availableConnectedFlight.getCharges(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
								double effectiveBundledFee = amountBreakDown[ondFlightCounter].doubleValue();
								for (QuotedChargeDTO quotedChargeDTO : quotedChargeDTOs) {
									if (quotedChargeDTO.getChargeCode().equals(chargeCode)) {
										assignEffectiveBundledFareChargeToApplicablePaxTypes(quotedChargeDTO, effectiveBundledFee);
										quotedChargeDTO.setChargeValuePercentage(effectiveBundledFee);
									}
								}
								ondFlightCounter++;
							}
						}
					}
				}

			}
		}
	}

	public void updateBundledFareFee(ChangeFaresDTO changeFaresDTO) {
		Map<Integer, BundledFareDTO> ondSelectedBundledFares = changeFaresDTO.getOndSelectedBundledFares();
		if (ondSelectedBundledFares != null && !ondSelectedBundledFares.isEmpty()) {
			Collection<OndFareDTO> ondFareDTOs = changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs();

			Map<Integer, Collection<OndFareDTO>> ondWiseFareDTOMap = new HashMap<Integer, Collection<OndFareDTO>>();
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				if (!ondWiseFareDTOMap.containsKey(ondFareDTO.getOndSequence())) {
					ondWiseFareDTOMap.put(ondFareDTO.getOndSequence(), new ArrayList<OndFareDTO>());
				}

				ondWiseFareDTOMap.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
			}

			for (Entry<Integer, BundledFareDTO> bundleEntry : ondSelectedBundledFares.entrySet()) {
				Integer ondSeq = bundleEntry.getKey();
				BundledFareDTO bundledFareDTO = bundleEntry.getValue();

				Collection<OndFareDTO> colOndFares = ondWiseFareDTOMap.get(ondSeq);
				if (bundledFareDTO != null && colOndFares != null && !colOndFares.isEmpty()) {
					BigDecimal[] amountBreakDown = AccelAeroCalculator.roundAndSplit(bundledFareDTO.getPerPaxBundledFee(),
							colOndFares.size());

					int counter = 0;
					for (OndFareDTO ondFareDTO : colOndFares) {
						ondFareDTO.setSelectedBundledFarePeriodId(bundledFareDTO.getBundledFarePeriodId());
						Collection<QuotedChargeDTO> quotedChargeDTOs = ondFareDTO.getAllCharges();
						double effectiveBundledFee = amountBreakDown[counter].doubleValue();

						for (QuotedChargeDTO quotedChargeDTO : quotedChargeDTOs) {
							if (quotedChargeDTO.getChargeCode().equals(bundledFareDTO.getChargeCode())) {
								assignEffectiveBundledFareChargeToApplicablePaxTypes(quotedChargeDTO, effectiveBundledFee);
								quotedChargeDTO.setChargeValuePercentage(effectiveBundledFee);
								break;
							}
						}
					}
				}
			}
		}
	}

	public void updateOndEffectiveBundledFee(OndRebuildCriteria criteria, OnDFareSegChargesRebuildCriteria ondCriteria,
			Collection<QuotedChargeDTO> charges) {
		// Update bundled fare fee from charge defined in BundledFareDTO
		BundledFareDTO selectedOndBundledFare = ondCriteria.getBundledFareDTO();
		if (selectedOndBundledFare != null) {
			int flightSegmentCount = criteria.getOndFareSegCount(ondCriteria.getOndSequence());
			BigDecimal effectiveBundledFee = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (calculateFlightWiseEffectiveBundledFareCharge(flightSegmentCount, ondCriteria.getFareType(),
					ondCriteria.getOndSequence())) {
				BigDecimal[] amountBreakDown = AccelAeroCalculator.roundAndSplit(selectedOndBundledFare.getPerPaxBundledFee(),
						flightSegmentCount);
				int element = getOndIteration(criteria, ondCriteria);
				effectiveBundledFee = amountBreakDown[element];
			} else {
				effectiveBundledFee = selectedOndBundledFare.getPerPaxBundledFee();
			}

			updateSelectedBundledFareFee(charges, selectedOndBundledFare, effectiveBundledFee.doubleValue());

		}
	}

	public int getOndIteration(OndRebuildCriteria criteria, OnDFareSegChargesRebuildCriteria ondCriteria) {
		int ondIteration = 0;

		for (OnDFareSegChargesRebuildCriteria ondIteratorObj : criteria.getOndCriteria()) {
			if (ondIteratorObj.getOndSequence() == ondCriteria.getOndSequence()
					&& ondIteratorObj.getFareID().equals(ondCriteria.getFareID())) {
				return ondIteration;
			} else if (ondIteratorObj.getOndSequence() == ondCriteria.getOndSequence()) {
				ondIteration++;
			}
		}

		return ondIteration;
	}

	public boolean calculateFlightWiseEffectiveBundledFareCharge(int flightSegmentCount, Integer fareType, int ondSequence) {
		if (flightSegmentCount > 1 && fareType == FareTypes.PER_FLIGHT_FARE) {
			return true;
		} else if (flightSegmentCount > 1 && fareType == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE
				&& ondSequence == OndSequence.OUT_BOUND) {
			return true;
		} else if (flightSegmentCount > 1 && fareType == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE
				&& ondSequence == OndSequence.IN_BOUND) {
			return true;
		}

		return false;
	}

	public void updateSelectedBundledFareFee(Collection<QuotedChargeDTO> charges, BundledFareDTO bundledFareDTO,
			double effectiveBundledFee) {
		String budnledFareChargeCode = bundledFareDTO.getChargeCode();

		for (QuotedChargeDTO quotedChargeDTO : charges) {
			if (quotedChargeDTO.getChargeCode().equals(budnledFareChargeCode)) {
				assignEffectiveBundledFareChargeToApplicablePaxTypes(quotedChargeDTO, effectiveBundledFee);
				quotedChargeDTO.setChargeValuePercentage(effectiveBundledFee);
			}
		}
	}

	public void assignEffectiveBundledFareChargeToApplicablePaxTypes(QuotedChargeDTO quotedChargeDTO, double effectiveBundledFee) {
		Set<String> paxTypeSet = FareQuoteUtil.populatePaxTypes(quotedChargeDTO.getApplicableTo());

		if (paxTypeSet.contains(PaxTypeTO.ADULT)) {
			quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, effectiveBundledFee);
		}

		if (paxTypeSet.contains(PaxTypeTO.CHILD)) {
			quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, effectiveBundledFee);
		}

		if (paxTypeSet.contains(PaxTypeTO.INFANT)) {
			quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.INFANT, effectiveBundledFee);
		}
	}

	private void addSelectedBundle(AvailableIBOBFlightSegment availableIBOBFlightSegment, OriginDestinationInfoDTO ondInfoDTO)
			throws ModuleException {

		BundledFareDTO bundledFareDTO = AirInventoryModuleUtils.getBundledFareBD().getBundledFareDtoByBundlePeriodId(
				ondInfoDTO.getPreferredBundledFarePeriodId());
		availableIBOBFlightSegment.setSelectedBundledFare(bundledFareDTO);
		availableIBOBFlightSegment.removeUnmatchedBundledFare();
	}

	public void setPreferredBundledFareDetails(AvailableIBOBFlightSegment availableIBOBFlightSegment,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean defaultBundledSelectedFlight) throws ModuleException {

		int ondSequence = availableIBOBFlightSegment.getOndSequence();
		OriginDestinationInfoDTO ondInfoDTO = availableFlightSearchDTO.getOndInfo(ondSequence);

		if (ondInfoDTO != null) {

			if (ondInfoDTO.getPreferredBundledFarePeriodId() != null
					&& availableIBOBFlightSegment.getAvailableBundledFares().size() > 0
					&& availableIBOBFlightSegment.isAvailableBundledFare(ondInfoDTO.getPreferredBundledFarePeriodId())) {

				addSelectedBundle(availableIBOBFlightSegment, ondInfoDTO);

			} else if (AppSysParamsUtil.isDefaultBundledSelectionEnabled(availableFlightSearchDTO.getAppIndicator())
					&& defaultBundledSelectedFlight
					&& !availableFlightSearchDTO.isMultiCitySearch()) {

				if (ondInfoDTO.getPreferredBundledFarePeriodId() == null
						&& !availableIBOBFlightSegment.getAvailableBundledFares().isEmpty()
						&& availableFlightSearchDTO.isQuoteFares()) {

					BundledFareLiteDTO bundledFareLiteDTO = availableIBOBFlightSegment.getDefaultBundledFare();

					if (bundledFareLiteDTO != null) {
						if (ondInfoDTO.getPreferredLogicalCabin() == null || availableFlightSearchDTO.isFareCalendarSearch()) {
							ondInfoDTO.setPreferredBundledFarePeriodId(bundledFareLiteDTO.getBundleFarePeriodId());
							addSelectedBundle(availableIBOBFlightSegment, ondInfoDTO);
						}
					}

				}
			}
		}
	}

	public Set<String> getApplicableBundledFareChargeCode(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		BundledFareDTO bundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();
		Set<String> selectedBundledFareCodes = null;

		if (bundledFareDTO != null) {
			selectedBundledFareCodes = new HashSet<String>();
			selectedBundledFareCodes.add(bundledFareDTO.getChargeCode());
		}

		return selectedBundledFareCodes;
	}

	public void removeAvailableBundledFaresForFlown(AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		if (availableIBOBFlightSegment.getAvailableBundledFares() != null
				&& !availableIBOBFlightSegment.getAvailableBundledFares().isEmpty()
				&& availableIBOBFlightSegment.getSelectedBundledFare() == null && availableIBOBFlightSegment.isFlown()) {

			availableIBOBFlightSegment.getAvailableBundledFares().put(availableIBOBFlightSegment.getSelectedLogicalCabinClass(),
					new ArrayList<BundledFareLiteDTO>());
		}
	}

	public Map<Integer, BundledFareDTO> getOndWiseSelectedBundledFareDTOs(AvailableFlightSearchDTO availableFlightSearchDTO)
			throws ModuleException {
		Map<Integer, BundledFareDTO> ondSelectedBundles = new HashMap<Integer, BundledFareDTO>();
		Set<Integer> selectedBundledServicePeriodIds = new HashSet<Integer>();

		for (OriginDestinationInfoDTO ondInfoTo : availableFlightSearchDTO.getOrderedOriginDestinations()) {
			if (ondInfoTo.getPreferredBundledFarePeriodId() != null) {
				selectedBundledServicePeriodIds.add(ondInfoTo.getPreferredBundledFarePeriodId());
			}
		}

		if (!selectedBundledServicePeriodIds.isEmpty()) {
			List<BundledFareDTO> bundledFareDTOs = AirInventoryModuleUtils.getBundledFareBD().getBundledFareDTOsByBundlePeriodIds(
					selectedBundledServicePeriodIds);

			if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
				Map<Integer, BundledFareDTO> bundledMap = new HashMap<Integer, BundledFareDTO>();
				for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
					bundledMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
				}

				int ondSequence = 0;
				for (OriginDestinationInfoDTO ondInfoTo : availableFlightSearchDTO.getOrderedOriginDestinations()) {
					if (ondInfoTo.getPreferredBundledFarePeriodId() != null) {
						ondSelectedBundles.put(ondSequence, bundledMap.get(ondInfoTo.getPreferredBundledFarePeriodId()));
					}
					ondSequence++;
				}
			}
		}

		return ondSelectedBundles;
	}

	public static boolean recalculateAvailableBundledFare(Date prefferedDate, Date ondFlightDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(CalendarUtil.PATTERN1);
		String prefDateStr = formatter.format(prefferedDate);
		String ondFlightDateStr = formatter.format(ondFlightDate);
		// TODO commented for retrieving bundle fares in non - selected dates in new ibe service-app
		// if (prefDateStr.equals(ondFlightDateStr)) {
		// return true;
		// }
		//
		// return false;

		return true;
	}

	private int getAirSegmentCount(List<AvailableFlightSegment> availableFlightSegments) {
		int airSegCount = 0;

		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
			if (!availableFlightSegment.isBusSegment()) {
				airSegCount++;
			}
		}

		return (airSegCount == 0) ? 1 : airSegCount;
	}

	private boolean isSurchargeExcluded(String bcChargeRatio) {
		if (bcChargeRatio != null
				&& (bcChargeRatio.equals(ReservationInternalConstants.ChargeGroupRatios.NO_RATIO) || bcChargeRatio
						.equals(ReservationInternalConstants.ChargeGroupRatios.TAX_RATIO))) {
			if (AppSysParamsUtil.isBundleFareToQuoteIndependentOfBCSurChargeExcluition()) {
				return false;
			} else {
				return true;
			}
		}

		return false;
	}

}