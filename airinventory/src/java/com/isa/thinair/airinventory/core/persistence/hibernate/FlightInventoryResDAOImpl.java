package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author Lasantha
 * @author Nasly
 * 
 * @isa.module.dao-impl dao-name="FlightInventoryResDAOImpl"
 */
public class FlightInventoryResDAOImpl extends PlatformHibernateDaoSupport implements FlightInventoryResDAO {

	@SuppressWarnings("unchecked")
	public List<Integer> filterFlightsHavingReservations(Collection<Integer> flightIds, String cabinClassCode) {
		int flightIdCount = flightIds.size();
		if (flightIdCount == 0)
			return new ArrayList<Integer>();

		String hql = " Select FCCBC.flightId from FCCSegBCInventory FCCBC , LogicalCabinClass LCC where FCCBC.logicalCCCode=LCC.logicalCCCode AND "
				+ " (FCCBC.seatsSold > 0 OR FCCBC.onHoldSeats > 0 OR FCCBC.seatsCancelled > 0) "
				+ " AND FCCBC.flightId IN (:flightIds ) ";
		if (cabinClassCode != null && !cabinClassCode.equals("")) {
			hql += "AND LCC.cabinClassCode = '" + cabinClassCode + "' ";
		}
		hql += " group by FCCBC.flightId ";
		return getSession().createQuery(hql).setParameterList("flightIds", flightIds).list();
	}

	@SuppressWarnings("unchecked")
	public List<Integer> filterFlightsHavingWaitlistedReservations(Collection<Integer> flightIds, String cabinClassCode) {
		int flightIdCount = flightIds.size();
		if (flightIdCount == 0)
			return new ArrayList<Integer>();

		String hql = " Select FCCBC.flightId from FCCSegBCInventory FCCBC , LogicalCabinClass LCC where FCCBC.logicalCCCode=LCC.logicalCCCode AND "
				+ " (FCCBC.waitListedSeats > 0) " + " AND FCCBC.flightId IN (:flightIds ) ";
		if (cabinClassCode != null && !cabinClassCode.equals("")) {
			hql += "AND LCC.cabinClassCode = '" + cabinClassCode + "' ";
		}
		hql += " group by FCCBC.flightId ";
		return getSession().createQuery(hql).setParameterList("flightIds", flightIds).list();
	}

	@SuppressWarnings("unchecked")
	public List<Integer> filterFlightSegsHavingReservations(Collection<Integer> flightSegIds, String cabinClassCode) {
		int flightIdCount = flightSegIds.size();
		if (flightIdCount == 0)
			return new ArrayList<Integer>();

		String hql = "Select FSI.flightSegId from FCCSegBCInventory FSBI, FCCSegInventory FSI where "
				+ "FSBI.fccsInvId = FSI.fccsInvId "
				+ "AND (FSBI.seatsSold > 0 OR FSBI.onHoldSeats > 0 OR FSBI.seatsCancelled > 0) "
				+ "AND FSI.flightSegId IN ( :flightSegIds ) ";
		if (cabinClassCode != null && !cabinClassCode.equals("")) {
			hql += "AND FSBI.CCCode = '" + cabinClassCode + "' ";
		}
		hql += "group by FSI.flightSegId";
		return getSession().createQuery(hql).setParameterList("flightSegIds", flightSegIds).list();
	}

	public boolean isFlightHasReservations(Integer flightId) {
		String hql = "Select count(*) from FCCSegBCInventory where " + "(seatsSold > 0 OR onHoldSeats > 0) " + "AND flightId ="
				+ flightId;
		Long count = (Long) getSession().createQuery(hql).uniqueResult();
		if (count == null || count.intValue() == 0) {
			return false;
		}
		return true;
	}
}
