package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.BCONDFaresAgentsSummaryDTO;
import com.isa.thinair.airpricing.api.dto.FareSummaryLightDTO;

/**
 * Extracts Fares and Agents linked to BookingClass.
 * 
 * @author Nasly
 */
public class BCONDFaresAgentsSummaryRsExtractor implements ResultSetExtractor {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		LinkedHashMap<String, BCONDFaresAgentsSummaryDTO> bookingCodesFaresMap = null;
		while (rs.next()) {
			if (bookingCodesFaresMap == null) {
				bookingCodesFaresMap = new LinkedHashMap<String, BCONDFaresAgentsSummaryDTO>();
			}
			String bookingCode = rs.getString("booking_code");
			BCONDFaresAgentsSummaryDTO bcFaresAgentsDTO = (BCONDFaresAgentsSummaryDTO) bookingCodesFaresMap.get(bookingCode);
			if (bcFaresAgentsDTO == null) {
				bcFaresAgentsDTO = new BCONDFaresAgentsSummaryDTO();
				bcFaresAgentsDTO.setBookingCode(bookingCode);
				bookingCodesFaresMap.put(bookingCode, bcFaresAgentsDTO);

				bcFaresAgentsDTO.setStandardCode(rs.getString("standard_code"));
				bcFaresAgentsDTO.setFixedCode(rs.getString("fixed_flag"));

				bcFaresAgentsDTO.setOndEffectiveAgentsMap(new LinkedHashMap());

				bcFaresAgentsDTO.setOndEffectiveFaresMap(new LinkedHashMap());
			}

			Object objONDCode = rs.getObject("ond_code");
			if (objONDCode != null) {
				Object objAgentCode = rs.getObject("agent_code");
				if (objAgentCode != null) {

					if (!bcFaresAgentsDTO.getOndEffectiveAgentsMap().containsKey(objONDCode.toString())) {
						bcFaresAgentsDTO.getOndEffectiveAgentsMap().put(objONDCode.toString(), new ArrayList());
					}

					if (!((Collection) bcFaresAgentsDTO.getOndEffectiveAgentsMap().get(objONDCode.toString()))
							.contains(objAgentCode.toString())) {
						((Collection) bcFaresAgentsDTO.getOndEffectiveAgentsMap().get(objONDCode.toString())).add(objAgentCode
								.toString());
					}
				}

				Object objFareId = rs.getObject("fare_id");
				if (objFareId != null) {
					Integer fareId = new Integer(objFareId.toString());
					if (!bcFaresAgentsDTO.getOndEffectiveFaresMap().containsKey(objONDCode.toString())) {
						bcFaresAgentsDTO.getOndEffectiveFaresMap().put(objONDCode.toString(), new LinkedHashMap());
					}

					if (!((LinkedHashMap) bcFaresAgentsDTO.getOndEffectiveFaresMap().get(objONDCode.toString()))
							.containsKey(fareId)) {
						FareSummaryLightDTO fare = new FareSummaryLightDTO();
						fare.setFareId(fareId.intValue());
						fare.setFareAmount(rs.getDouble("fare_amount"));
						fare.setFareRuleId(rs.getInt("fare_rule_id"));
						fare.setChildFareAmount(rs.getDouble("child_fare"));
						fare.setInfantFareAmount(rs.getDouble("infant_fare"));
						fare.setChildFareType(rs.getString("child_fare_type"));
						fare.setInfantFareType(rs.getString("infant_fare_type"));
						fare.setReturnFlag(rs.getString("return_flag"));
						((LinkedHashMap) bcFaresAgentsDTO.getOndEffectiveFaresMap().get(objONDCode.toString())).put(fareId, fare);
					}
				}
			}
		}
		return bookingCodesFaresMap;
	}

}
