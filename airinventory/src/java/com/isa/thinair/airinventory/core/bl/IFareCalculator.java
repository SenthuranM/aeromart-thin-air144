package com.isa.thinair.airinventory.core.bl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
interface IFareCalculator {
	public void addOndFlightSegment(Map<IFareCalculator.FareCalType, List<FilteredBucketsDTO>> mapCalTypeBuckets)
			throws ModuleException;

	enum FareCalType {
		SEG, CON, RT, HRT;
	}

	/**
	 * Determine whether fare calculator is valid after segment buckets are checked. If no segments buckets (or no
	 * segment it self) is available, This will render false
	 * 
	 * @return
	 */
	public boolean isValid();

	/**
	 * Determine whether it is a valid fare calculator for the journey before any of the segments are processed This can
	 * be used to check whether fare calculator is invalid based on the ond journey info without checking actual flight
	 * segment buckets
	 * 
	 * @return
	 */
	public boolean isValidForJourney();

	/**
	 * Fare types this fare calculator requires to carryout it's fare calculations
	 * 
	 * @return
	 */
	public Set<IFareCalculator.FareCalType> dependantFareTypes();

	public double getTotalMinimumFare();

	public void setFareAndLogiCC(List<AvailableIBOBFlightSegment> ondFlights, boolean isMinimumFare);

	/**
	 * 1 - Highest priority greater than 1 - Low priority than 1 or anything that precedes
	 * 
	 * @return
	 */
	public int getPriority();

}