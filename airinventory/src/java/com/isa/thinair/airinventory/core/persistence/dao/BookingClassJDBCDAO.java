package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.BCONDFaresAgentsSummaryDTO;
import com.isa.thinair.airinventory.api.dto.inventory.BCDetailDTO;

/**
 * Interface for JDBC calls for manipulating Booking Class.
 * 
 * @author Thejaka
 * @author Nasly
 */
public interface BookingClassJDBCDAO {

	/**
	 * Returns booking class summary, attached fare summary and attached agents info keyed by booking code.
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @param logicalCabinClassCode
	 * @return LinkedHashMap<String, BCONDFaresAgentsSummaryDTO>
	 */
	public LinkedHashMap<String, BCONDFaresAgentsSummaryDTO> getBCONDFaresAgentsSummaryMap(int flightId, String segmentCode,
			String logicalCabinClassCode);

	public Object[] getMinimumPublicFare(int flightId, String segmentCode, String logicalCabinClassCode);

	/**
	 * Returns Map<BookingCode, boolean [] {hasFareRules, hasAgentsLinked}>.
	 * 
	 * @param bookingCodes
	 * @return Map<String, boolean[]>
	 */
	public HashMap<String, boolean[]> getFareRuleAgentCountsLinkedToBCs(Collection<String> bookingCodes);

	/**
	 * Filters out only those booking codes that has one or more allocations.
	 * 
	 * @param bookingCodes
	 * @return List<String>
	 */
	public List<String> getBookingClassesLinkedToInv(Collection<String> bookingCodes);

	/**
	 * Returns booking class type.
	 * 
	 * @param bookingCode
	 * @return String
	 */
	public String getBookingClassType(String bookingCode);

	/**
	 * Returns the booking class codes for given gds ids
	 * 
	 * @param gdsIds
	 * @return
	 */
	public Map<String, Collection<String>> getBookingClassCodes(Collection<Integer> gdsIds);

	public List<BCDetailDTO> getBCsForGrouping();
}
