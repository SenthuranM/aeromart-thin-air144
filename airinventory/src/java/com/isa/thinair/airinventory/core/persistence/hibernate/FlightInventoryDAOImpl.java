/*
 * 
==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
===============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.IntegerType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoriesExistsCriteria;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryCollection;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.GdsPublishFccSegBcInventoryTo;
import com.isa.thinair.airinventory.api.dto.RetrieveFCCSegBCInvCriteria;
import com.isa.thinair.airinventory.api.dto.RetrievePubAvailCriteria;
import com.isa.thinair.airinventory.api.dto.TempSegBcAllocDto;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.UpdateFccSegBCInvResponse;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.BookingClass.AllocationType;
import com.isa.thinair.airinventory.api.model.FCCInventory;
import com.isa.thinair.airinventory.api.model.FCCInventoryMovemant;
import com.isa.thinair.airinventory.api.model.FCCSegBCInvNesting;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.InterceptingFCCSegAlloc;
import com.isa.thinair.airinventory.api.model.PublishAvailability;
import com.isa.thinair.airinventory.api.model.TempSegBCInvClosure;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.model.TempSegBcAllocBKP;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventCode;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventStatus;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.api.util.InventoryDiffForAVS;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author Thejaka Usgoda Arachchi
 * @author Nasly
 * 
 *         TODO: clean up unused methods
 * 
 *         Hibernate implementation of FlightInventoryDAO
 * @isa.module.dao-impl dao-name="FlightInventoryDAOImpl"
 */

public class FlightInventoryDAOImpl extends PlatformHibernateDaoSupport implements FlightInventoryDAO {

	private final Log log = LogFactory.getLog(getClass());

	private final Log invLog = LogFactory.getLog("com.isa.inventory_log");

	@Override
	public FCCSegInventory getFCCSegInventory(int flightId, String logicalCabinClassCode, String segmentCode) {

		String hql = "Select FSI from FCCSegInventory as FSI where FSI.flightId = :flightId and FSI.segmentCode = :segmentCode "
				+ " and FSI.logicalCCCode = :logicalCCCode and FSI.validFlagChar = 'Y' ";

		FCCSegInventory fccSegInventory = (FCCSegInventory) getSession().createQuery(hql).setInteger("flightId", flightId)
				.setString("segmentCode", segmentCode).setString("logicalCCCode", logicalCabinClassCode).uniqueResult();

		return fccSegInventory;

	}

	@Override
	public FCCSegBCInventory getFCCSegBCInventory(int flightId, String logicalCabinClassCode, String segmentCode,
			String bookingCode) {
		String hql = "Select FSBI from FCCSegBCInventory as FSBI where FSBI.flightId = :flightId and FSBI.segmentCode = :segmentCode and "
				+ "FSBI.logicalCCCode = :logicalCCCode and FSBI.bookingCode = :bookingCode";

		FCCSegBCInventory bcInventory = (FCCSegBCInventory) getSession().createQuery(hql).setInteger("flightId", flightId)
				.setString("segmentCode", segmentCode).setString("logicalCCCode", logicalCabinClassCode)
				.setString("bookingCode", bookingCode).uniqueResult();

		return bcInventory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegInventory> getFCCSegInventories(int flightId, String cabinClassCode) {
		String hql = "Select FSI from FCCSegInventory as FSI , LogicalCabinClass as LCC  where FSI.logicalCCCode =LCC.logicalCCCode and FSI.flightId = :flightId "
				+ "and LCC.cabinClassCode = :cabinClassCode and FSI.validFlagChar = 'Y' ";

		Collection<FCCSegInventory> fccSegInventories = (Collection<FCCSegInventory>) getSession().createQuery(hql)
				.setInteger("flightId", flightId).setString("cabinClassCode", cabinClassCode).list();

		return fccSegInventories;
	}

	/**
	 * @param flightIds
	 * @param cabinClassCode
	 * @return
	 */
	private Map<Integer, Map<Integer, List<FCCSegInventory>>> getFCCSegInventories(Collection<Integer> flightIds,
			String cabinClassCode) {

		Map<Integer, Map<Integer, List<FCCSegInventory>>> fccSegmentInvMap = new HashMap<Integer, Map<Integer, List<FCCSegInventory>>>();
		StringBuilder query = new StringBuilder(
				"Select FSI from FCCSegInventory as FSI , LogicalCabinClass as LCC  where FSI.logicalCCCode =LCC.logicalCCCode and FSI.flightId IN (:flightIds)  and LCC.cabinClassCode =:CCCode and FSI.validFlagChar = 'Y' order by LCC.nestRank DESC ");
		@SuppressWarnings("unchecked")
		Collection<FCCSegInventory> fccSegInventories = getSession().createQuery(query.toString())
				.setParameterList("flightIds", flightIds).setString("CCCode", cabinClassCode).list();
		for (FCCSegInventory fccSegInventory : fccSegInventories) {
			if (fccSegmentInvMap.get(fccSegInventory.getFlightId()) == null) {
				fccSegmentInvMap.put(fccSegInventory.getFlightId(), new HashMap<Integer, List<FCCSegInventory>>());
			}
			if (fccSegmentInvMap.get(fccSegInventory.getFlightId()).get(fccSegInventory.getFlightSegId()) == null) {
				fccSegmentInvMap.get(fccSegInventory.getFlightId()).put(fccSegInventory.getFlightSegId(),
						new ArrayList<FCCSegInventory>());
			}
			fccSegmentInvMap.get(fccSegInventory.getFlightId()).get(fccSegInventory.getFlightSegId()).add(fccSegInventory);
		}

		return fccSegmentInvMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegInventory> getFCCSegInventories(int flightId, int flightSegmentId, String cabinClassCode) {
		String hql = "Select FSI from FCCSegInventory as FSI , LogicalCabinClass as LCC where  FSI.logicalCCCode = LCC.logicalCCCode  and FSI.flightId = :flightId "
				+ "and FSI.flightSegId = :flightSegId and LCC.cabinClassCode = :cabinClassCode and FSI.validFlagChar = 'Y' ";

		Collection<FCCSegInventory> fccSegInventories = (Collection<FCCSegInventory>) getSession().createQuery(hql)
				.setInteger("flightId", flightId).setInteger("flightSegId", flightSegmentId)
				.setString("cabinClassCode", cabinClassCode).list();

		return fccSegInventories;

	}

	/**
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param segmentCode
	 * @param bookingCode
	 *            Will never receive a collection of 1000 booking codes to filter.
	 * @return
	 */

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Integer, ExtendedFCCSegBCInventory> getExtendedFCCSegBCInventories(int fccsInvId) {
		HashMap<Integer, ExtendedFCCSegBCInventory> bcInventoriesMap = new HashMap<Integer, ExtendedFCCSegBCInventory>();
		String hql = "SELECT new com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory(FSBI,BC.fixedFlagChar,BC.standardCodeChar) "
				+ "FROM FCCSegBCInventory as FSBI, BookingClass as BC " + "WHERE FSBI.bookingCode = BC.bookingCode "
				+ "AND FSBI.fccsInvId=?";

		List<ExtendedFCCSegBCInventory> bcInventories = getSession().createQuery(hql).setInteger(0, fccsInvId).list();

		if (bcInventories != null) {
			Iterator<ExtendedFCCSegBCInventory> bcInventoriesIt = bcInventories.iterator();
			while (bcInventoriesIt.hasNext()) {
				ExtendedFCCSegBCInventory exBCInv = bcInventoriesIt.next();
				bcInventoriesMap.put(exBCInv.getFccSegBCInventory().getFccsbInvId(), exBCInv);
			}
		}

		return bcInventoriesMap;
	}

	@Override
	public FCCSegBCInventory getFCCSegBCInventory(Integer fccsbInvId) {
		return (FCCSegBCInventory) get(FCCSegBCInventory.class, fccsbInvId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegInventory> getFCCSegInventories(Collection<Integer> fccsaIds) {
		return getSession().createQuery("Select FSI from FCCSegInventory as FSI where FSI.fccsInvId in (:fccsaIds) ")
				.setParameterList("fccsaIds", fccsaIds).list();
	}

	@Override
	public ExtendedFCCSegBCInventory getExtendedBCInventory(int flightSegId, String bookingCode) {
		String hql = "SELECT new com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory(FSBI,"
				+ " BC.fixedFlagChar,BC.standardCodeChar,BC.allocationType,BC.paxType,BC.nestRank,BC.bcType,"
				+ " BC.paxCategoryCode,BC.fareCategoryCode) "
				+ " FROM FCCSegBCInventory as FSBI, BookingClass BC, FCCSegInventory FSI"
				+ " WHERE FSBI.fccsInvId = FSI.fccsInvId AND FSBI.bookingCode = BC.bookingCode"
				+ " AND FSI.flightSegId = ? AND FSBI.bookingCode = ?";
		return (ExtendedFCCSegBCInventory) getSession().createQuery(hql).setInteger(0, flightSegId).setString(1, bookingCode)
				.uniqueResult();
	}

	@Override
	public ExtendedFCCSegBCInventory getExtendedBCInventory(int fccsbInvId) {
		String hql = "SELECT new com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory(FSBI,BC.fixedFlagChar,"
				+ " BC.standardCodeChar,BC.allocationType,BC.paxType,BC.nestRank,BC.bcType,BC.paxCategoryCode,BC.fareCategoryCode) "
				+ " FROM FCCSegBCInventory as FSBI, BookingClass BC WHERE FSBI.bookingCode = BC.bookingCode"
				+ " AND FSBI.fccsbInvId = ? ";
		return (ExtendedFCCSegBCInventory) getSession().createQuery(hql).setInteger(0, fccsbInvId).uniqueResult();
	}

	@Override
	public void saveFCCInventory(FCCInventory fCCInventory) {
		hibernateSaveOrUpdate(fCCInventory);
	}

	@Override
	public void saveOrUpdateFCCSegInventory(Collection<FCCSegInventory> fccSegInventories) {
		hibernateSaveOrUpdateAll(fccSegInventories);
	}

	@Override
	public void saveFCCSegmentBCInventory(FCCSegBCInventory fccSegBCInventory) {
		hibernateSaveOrUpdate(fccSegBCInventory);
	}

	@Override
	public void saveFCCSegmentInventory(FCCSegInventory fccSegInventory) {
		hibernateSaveOrUpdate(fccSegInventory);
	}

	@Override
	public void deleteFCCSegmentBCInventory(FCCSegBCInventory fccSegBCInventory) {
		log.warn("Going remove BC inventory [bcInvId=" + fccSegBCInventory.getFccsbInvId() + "]");
		delete(fccSegBCInventory);
	}

	@Override
	public int updateFCCSegmentInventory(int fccSegInvId, int oversell, int curtailed, int infantAllocation,
			int waitListAllocation, long version, boolean includeVersionCheck) {
		String hql = "update FCCSegInventory set oversellSeats=:oversell, curtailedSeats=:curtailed, "
				+ "infantAllocation=:infantAllocation,allocatedWaitListSeats=:allocatedWaitListSeats, version = version + 1 where fccsInvId=:fccsInvId ";
		if (includeVersionCheck) {
			hql += " and version=:version";
		}

		Query query = getSession().createQuery(hql).setInteger("oversell", oversell).setInteger("curtailed", curtailed)
				.setInteger("infantAllocation", infantAllocation).setInteger("fccsInvId", fccSegInvId)
				.setInteger("allocatedWaitListSeats", waitListAllocation);

		if (includeVersionCheck) {
			query.setLong("version", version);
		}
		return query.executeUpdate();
	}

	@Override
	public void updateFCCSegmentInventory(int flightId, String logicalCabinClassCode, String segmentCode, int soldInfantCount,
			int onholdInfantCount, int interceptingBookingCount) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "UPDATE t_fcc_seg_alloc fccsa SET (sold_seats,waitlisted_seats,on_hold_seats,available_seats,sold_infant_seats,"
				+ "onhold_infant_seats,available_infant_seats)=(SELECT SUM(fccsba.sold_seats),SUM(fccsba.waitlisted_seats), SUM(fccsba.onhold_seats- "
				+ "(SELECT NVL(SUM(tsbca.seats_blocked), 0) exg_seats FROM t_temp_seg_bc_alloc tsbca WHERE "
				+ "tsbca.skip_seg_inv_update = 'Y' AND tsbca.fccsba_id = fccsba.fccsba_id    )), "
				+ "greatest(fccsa.allocated_seats + fccsa.oversell_seats - fccsa.curtailed_seats"
				+ "-SUM(fccsba.onhold_seats) - SUM(fccsba.sold_seats - "
				+ "(SELECT NVL(SUM(tsbca.seats_blocked), 0) exg_seats FROM t_temp_seg_bc_alloc tsbca WHERE "
				+ "tsbca.skip_seg_inv_update = 'Y' AND tsbca.fccsba_id = fccsba.fccsba_id) ) - " + interceptingBookingCount
				+ ",0), fccsa.sold_infant_seats -" + soldInfantCount + ",fccsa.onhold_infant_seats -" + onholdInfantCount
				+ ",fccsa.available_infant_seats +" + (onholdInfantCount + soldInfantCount)
				+ " FROM t_fcc_seg_bc_alloc fccsba, t_booking_class bc WHERE fccsba.booking_code = bc.booking_code "
				+ "AND fccsba.flight_id=" + flightId + " AND fccsba.segment_code='" + segmentCode
				+ "' AND fccsba.logical_cabin_class_code='" + logicalCabinClassCode + "' AND bc.bc_type NOT IN ("
				+ Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) + ")) WHERE fccsa.flight_id=" + flightId
				+ " and fccsa.segment_code='" + segmentCode + "' and fccsa.logical_cabin_class_code='" + logicalCabinClassCode
				+ "'";
		jdbcTemplate.update(sql);
		// TODO : update inventory audit tables
	}

	/**
	 * Update cummulative sold, onhold and fixed seats count on segment Standby seats are excluded from updating the
	 * segment sold/onhold/fixed seats counts [TODO: optimize] Optimize the implementation
	 */
	@Override
	public int updateFCCSegmentInventory(int flightId, String logicalCabinClassCode, String segmentCode, String userId) {

		Object[] initialSegLevelSeats = (Object[]) getSession()
				.createQuery(" SELECT FSI.allocatedSeats, FSI.curtailedSeats, FSI.oversellSeats, FSI.seatsSold, FSI.onHoldSeats, "
						+ " FSI.flightSegId  FROM FCCSegInventory FSI WHERE FSI.flightId=:flightId "
						+ " AND FSI.logicalCCCode=:logicalCCCode AND FSI.segmentCode=:segmentCode ")
				.setInteger("flightId", flightId).setString("logicalCCCode", logicalCabinClassCode)
				.setString("segmentCode", segmentCode).uniqueResult();

		int initialCount = (Integer) initialSegLevelSeats[0] + (Integer) initialSegLevelSeats[2]
				- (Integer) initialSegLevelSeats[1] - (Integer) initialSegLevelSeats[3] - (Integer) initialSegLevelSeats[4];

		int initialOverbookCount = 0;

		if (initialCount < 0) {
			initialOverbookCount = initialCount * -1;
		}

		// update sold, onhold and fixed
		String hql = "UPDATE FCCSegInventory SET  seatsSold=:soldSeats, onHoldSeats=:onholdSeats, "
				+ "fixedSeats=:fixedSeats, version = version + 1 WHERE flightId=:flightId AND logicalCCCode=:logicalCCCode AND segmentCode=:segmentCode";

		Object[] soldOnholdCounts = (Object[]) getSession()
				.createQuery("SELECT coalesce(sum(BCI.seatsSold),0), coalesce(sum(BCI.onHoldSeats),0) "
						+ " FROM FCCSegBCInventory BCI, BookingClass BC WHERE "
						+ "BCI.bookingCode = BC.bookingCode AND BCI.flightId=:flightId AND "
						+ "BCI.logicalCCCode=:logicalCCCode AND BCI.segmentCode=:segmentCode AND "
						+ "BC.bcType NOT IN (:standby) ")
				.setInteger("flightId", flightId).setString("logicalCCCode", logicalCabinClassCode)
				.setString("segmentCode", segmentCode).setParameterList("standby", BookingClassUtil.getBcTypes(true))
				.uniqueResult();

		Integer soldSeats = soldOnholdCounts[0] == null ? 0 : ((Long) soldOnholdCounts[0]).intValue();
		Integer onholdSeats = soldOnholdCounts[1] == null ? 0 : ((Long) soldOnholdCounts[1]).intValue();

		Long exgCount = (Long) getSession()
				.createQuery("SELECT sum(TSBA.seatsBlocked) FROM TempSegBcAlloc TSBA, FCCSegInventory FSI WHERE "
						+ "TSBA.fccaId = FSI.fccsInvId AND FSI.flightId=:flightId AND "
						+ "FSI.logicalCCCode=:logicalCCCode AND FSI.segmentCode=:segmentCode AND TSBA.skipSegInvUpdate='Y' ")
				.setInteger("flightId", flightId).setString("logicalCCCode", logicalCabinClassCode)
				.setString("segmentCode", segmentCode).uniqueResult();
		if (onholdSeats != null && exgCount != null) {
			onholdSeats = (onholdSeats - exgCount.intValue());
			if (onholdSeats < 0) {
				onholdSeats = 0;
			}
		}

		Long fixedSeats = (Long) getSession()
				.createQuery("SELECT sum(BCI.seatsAllocated) FROM FCCSegBCInventory BCI, BookingClass BC WHERE "
						+ "BCI.bookingCode = BC.bookingCode AND BC.fixedFlagChar=:fixedFlagChar AND "
						+ "BC.bcType NOT IN (:standby) AND BCI.flightId=:flightId AND "
						+ "BCI.logicalCCCode=:logicalCCCode AND BCI.segmentCode=:segmentCode")
				.setInteger("flightId", flightId).setString("logicalCCCode", logicalCabinClassCode)
				.setString("segmentCode", segmentCode).setString("fixedFlagChar", BookingClass.FIXED_FLAG_Y)
				.setParameterList("standby", BookingClassUtil.getBcTypes(true)).uniqueResult();

		int updateCount = getSession().createQuery(hql).setInteger("soldSeats", soldSeats != null ? soldSeats.intValue() : 0)
				.setInteger("onholdSeats", onholdSeats != null ? onholdSeats.intValue() : 0)
				.setInteger("fixedSeats", fixedSeats != null ? fixedSeats.intValue() : 0).setInteger("flightId", flightId)
				.setString("logicalCCCode", logicalCabinClassCode).setString("segmentCode", segmentCode).executeUpdate();

		Object[] segLevelSeats = (Object[]) getSession()
				.createQuery(" SELECT FSI.allocatedSeats, FSI.curtailedSeats, FSI.oversellSeats, FSI.seatsSold, FSI.onHoldSeats, "
						+ " FSI.flightSegId  FROM FCCSegInventory FSI WHERE FSI.flightId=:flightId "
						+ " AND FSI.logicalCCCode=:logicalCCCode AND FSI.segmentCode=:segmentCode ")
				.setInteger("flightId", flightId).setString("logicalCCCode", logicalCabinClassCode)
				.setString("segmentCode", segmentCode).uniqueResult();
		Integer segAlloc = (Integer) segLevelSeats[0];
		Integer segCurtail = (Integer) segLevelSeats[1];
		Integer segOversell = (Integer) segLevelSeats[2];
		Integer segSold = (Integer) segLevelSeats[3];
		Integer segOhd = (Integer) segLevelSeats[4];
		Integer flightSegId = (Integer) segLevelSeats[5];

		int finalCount = (Integer) initialSegLevelSeats[0] + (Integer) initialSegLevelSeats[2] - (Integer) initialSegLevelSeats[1]
				- (Integer) initialSegLevelSeats[3] - (Integer) initialSegLevelSeats[4];

		int finalOverbookCount = 0;
		if (finalCount < 0) {
			finalOverbookCount = finalCount * -1;
		}

		if ((initialOverbookCount != finalOverbookCount) && finalOverbookCount != 0) {
			// do OVERBOOK audit
			try {
				Collection<String> strFltSegIds = new ArrayList<String>();
				strFltSegIds.add(String.valueOf(flightSegId));
				AuditAirinventory.doAuditOverBookings(strFltSegIds, null, null, null, String.valueOf(flightId), userId,
						finalOverbookCount);
				log.debug("------Overbook audit SUCCESS for flightId : " + flightId + ", Segment code : " + segmentCode);
			} catch (Exception ex) {
				// do nothing since only audit fail
				log.debug("------Overbook audit FAILED for flightId : " + flightId + ", Segment code : " + segmentCode);
			}
		}
		// [TODO:verify]is flush() really required?
		getSession().flush();
		log.debug("------Session flush() invoked------");

		return updateCount;
	}

	@Override
	public void updateFCCSegmentInventory(int fccInvId, int numberOfOnholdInfantSeats, int interceptingBookingCount) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "UPDATE t_fcc_seg_alloc fccsa SET (sold_seats,on_hold_seats,available_seats,available_infant_seats,"
				+ "onhold_infant_seats) = (SELECT NVL(SUM(fccsba.sold_seats),0), NVL(SUM(fccsba.onhold_seats - "
				+ "(SELECT NVL(SUM(tsbca.seats_blocked), 0) exg_seats FROM t_temp_seg_bc_alloc tsbca WHERE "
				+ "tsbca.skip_seg_inv_update = 'Y' AND tsbca.fccsba_id = fccsba.fccsba_id   )),0), "
				+ "greatest(fccsa.allocated_seats + fccsa.oversell_seats - fccsa.curtailed_seats"
				+ "-NVL(SUM(fccsba.onhold_seats),0) - NVL(SUM(fccsba.sold_seats - "
				+ " (SELECT NVL(SUM(tsbca.seats_blocked), 0) exg_seats FROM t_temp_seg_bc_alloc tsbca WHERE "
				+ "tsbca.skip_seg_inv_update = 'Y' AND tsbca.fccsba_id = fccsba.fccsba_id    )),0) - " + interceptingBookingCount
				+ ",0), fccsa.available_infant_seats -" + numberOfOnholdInfantSeats + ",fccsa.onhold_infant_seats +"
				+ numberOfOnholdInfantSeats + " FROM t_fcc_seg_bc_alloc fccsba, t_booking_class bc "
				+ "WHERE fccsba.booking_code = bc.booking_code AND fccsba.fccsa_id=" + fccInvId + " AND bc.bc_type NOT IN ("
				+ Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) + ")) WHERE fccsa.fccsa_id=" + fccInvId;
		jdbcTemplate.update(sql);
		// TODO : update inventory audit tables
	}

	@Override
	public int[] getMaximumEffectiveReservedSetsCountInInterceptingSegments(Collection<Integer> interceptingFccsaIds) {
		// FIXME add this flight id wise for overlapping flight segments
		int maxSoldOhdAdultSeatCount = 0;
		int maxSoldOhdInfantSeatCount = 0;
		Collection<FCCSegInventory> interceptingFccSegInventories = getFCCSegInventories(interceptingFccsaIds);
		Set<String> pointToPointSegments = getPointToPointSegments(interceptingFccSegInventories);
		Set<Integer> allFlightIds = getAllFlightIds(interceptingFccSegInventories);

		for (String pointToPointSegment : pointToPointSegments) {
			Set<Integer> includedFlightSegments = new HashSet<Integer>();
			for (FCCSegInventory fccSegInventory : interceptingFccSegInventories) {
				if (fccSegInventory.getSegmentCode().contains(pointToPointSegment)) {
					includedFlightSegments.add(fccSegInventory.getFccsInvId());
				}
			}
			if (!includedFlightSegments.isEmpty()) {
				String logicalCabinClass = interceptingFccSegInventories.iterator().next().getLogicalCCCode();
				int[] effectiveSoldOnholdSeats = getFlightInventoryJDBCDAO()
						.getEffectiveSoldOnholdSeatsInInterceptingSegments(allFlightIds, Arrays.asList(pointToPointSegment),
								logicalCabinClass, includedFlightSegments, false)
						.get(pointToPointSegment);
				maxSoldOhdAdultSeatCount = Math.max(maxSoldOhdAdultSeatCount, effectiveSoldOnholdSeats[0]);
				maxSoldOhdInfantSeatCount = Math.max(maxSoldOhdInfantSeatCount, effectiveSoldOnholdSeats[1]);
			}
		}
		return new int[] { maxSoldOhdAdultSeatCount, maxSoldOhdInfantSeatCount };
	}

	private Set<String> getPointToPointSegments(Collection<FCCSegInventory> interceptingFccSegInventories) {
		Set<String> pointToPointSegments = new HashSet<String>();
		for (FCCSegInventory fccSegInventory : interceptingFccSegInventories) {
			pointToPointSegments
					.addAll(AirinventoryUtils.buildSingleLegSegmentCodes(Arrays.asList(fccSegInventory.getSegmentCode())));
		}
		return pointToPointSegments;
	}

	private Set<Integer> getAllFlightIds(Collection<FCCSegInventory> interceptingFccSegInventories) {
		Set<Integer> allFlightIds = new HashSet<Integer>();
		for (FCCSegInventory fccSegInventory : interceptingFccSegInventories) {
			allFlightIds.add(fccSegInventory.getFlightId());
		}
		return allFlightIds;
	}

	@Override
	public int updateFCCInterceptingSegmentInvAndAVS(List<Integer> interceptingFccsaIds) {

		if (interceptingFccsaIds == null || interceptingFccsaIds.isEmpty()) {
			return 0;
		}

		int fccsInvId = 0;
		int updatedSegInvCount = 0;

		// intercepting is a symmetric property
		fccsInvId = interceptingFccsaIds.get(0);

		InventoryDiffForAVS invDiffForAVS = new InventoryDiffForAVS(fccsInvId);

		try {

			log.debug("intial call to record Inv snapshot, status " + invDiffForAVS.takeIntialInvSnapshot());

			updatedSegInvCount = updateFCCInterceptingSegmentInventories(interceptingFccsaIds);

			log.debug("final call to record Inv snapshot and update diff, status: "
					+ invDiffForAVS.takeFinalInvSnapshotAndPublishDiff());
		} catch (ModuleException e) {
			// do nothing
		}
		return updatedSegInvCount;

	}

	@Override
	public int updateFCCInterceptingSegmentInventories(List<Integer> interceptingFccsaIds) {
		log.debug("Begin updateFCCInterceptingSegmentInventoriesV2 :" + System.currentTimeMillis());

		int updatedSegInvCount = 0;

		if (interceptingFccsaIds != null && interceptingFccsaIds.size() > 0) {
			for (Integer fccSegInvId : interceptingFccsaIds) {

				int[] soldAdultAndInfantSeatCount = getFlightInventoryJDBCDAO().getSoldOnholdFixedAndInfSeatsInSegs(fccSegInvId);

				int maxSoldAdultSeatCount = soldAdultAndInfantSeatCount[0];// sold + onhold
				int maxSoldInfantSeatCount = soldAdultAndInfantSeatCount[1];// sold + onhold
				// int maxWaitListedSeats = soldAdultAndInfantSeatCount[2]; // wait listed seats

				FCCSegInventory existingSegAlloc = (FCCSegInventory) getSession()
						.createQuery("SELECT FSI FROM FCCSegInventory FSI WHERE FSI.fccsInvId=?").setInteger(0, fccSegInvId)
						.uniqueResult();

				FCCInventory fccInventory = (FCCInventory) getSession()
						.createQuery("SELECT FI FROM FCCInventory FI WHERE FI.fccInvId=?")
						.setInteger(0, existingSegAlloc.getFccInvId().intValue()).uniqueResult();

				// get available fixed seat count
				Integer availableFixedSeatCount = getFlightInventoryJDBCDAO().getAvailableFixedSeatCount(fccSegInvId);

				int effectiveCapacity = existingSegAlloc.getAllocatedSeats() + existingSegAlloc.getOversellSeats()
						- existingSegAlloc.getCurtailedSeats() + availableFixedSeatCount;

				int curtailedSeats = existingSegAlloc.getCurtailedSeats();
				// Update new curtailed amount in case we can not accommodate fixed seats count int target flight
				// segment.
				// if ((effectiveCapacity - maxSoldAdultSeatCount) < availableFixedSeatCount) {
				// curtailedSeats = Math.max(
				// (curtailedSeats - (availableFixedSeatCount - (effectiveCapacity - maxSoldAdultSeatCount))), 0);
				// effectiveCapacity = existingSegAlloc.getAllocatedSeats() + existingSegAlloc.getOversellSeats()
				// - curtailedSeats + availableFixedSeatCount;
				// }
				int availableAdultSeats = Math.max((effectiveCapacity - maxSoldAdultSeatCount), 0);

				int effectiveInfCapacity = Math.min(fccInventory.getInfantCapacity(), existingSegAlloc.getInfantAllocation());
				int availableInfantSeats = Math.max((effectiveInfCapacity - maxSoldInfantSeatCount), 0);

				FCCSegInventory fccSegInventory = (FCCSegInventory) getSession().load(FCCSegInventory.class, fccSegInvId);
				getSession().refresh(fccSegInventory);

				// update availableSeats on segment
				StringBuffer hql = new StringBuffer("UPDATE FCCSegInventory SET availableSeats=:availableSeats, "
						+ "availableInfantSeats=:availableInfantSeats, curtailedSeats=:curtailedSeats, version = version + 1 WHERE fccsInvId="
						+ fccSegInvId);

				int tmpUpdatedSegInvCount = getSession().createQuery(hql.toString())
						.setInteger("availableSeats", availableAdultSeats)
						.setInteger("availableInfantSeats", availableInfantSeats).setInteger("curtailedSeats", curtailedSeats)
						.executeUpdate();
				updatedSegInvCount += tmpUpdatedSegInvCount;

				// explicit flushing of session to reflect changes in the database
				getSession().flush();
				log.debug("------Session flush() invoked------");

			}
		}

		log.debug("End updateFCCInterceptingSegmentInventoriesV2 :" + System.currentTimeMillis());
		return updatedSegInvCount;
	}

	private FCCSegInventory getFCCSegInventory(FCCSegInventory fCCSegInventory, int availableAdultSeats) {
		FCCSegInventory updatedFCCSegInventory = new FCCSegInventory();
		updatedFCCSegInventory.setAvailableSeats(availableAdultSeats);
		updatedFCCSegInventory.setAllocatedSeats(fCCSegInventory.getAllocatedSeats());
		updatedFCCSegInventory.setAvailableInfantSeats(fCCSegInventory.getAvailableInfantSeats());
		updatedFCCSegInventory.setCurtailedSeats(fCCSegInventory.getCurtailedSeats());
		updatedFCCSegInventory.setFccInvId(fCCSegInventory.getFccInvId());
		updatedFCCSegInventory.setFccsInvId(fCCSegInventory.getFccsInvId());
		updatedFCCSegInventory.setFlightId(fCCSegInventory.getFlightId());
		updatedFCCSegInventory.setFlightSegId(fCCSegInventory.getFlightSegId());
		updatedFCCSegInventory.setFixedSeats(fCCSegInventory.getFixedSeats());
		updatedFCCSegInventory.setInfantAllocation(fCCSegInventory.getInfantAllocation());
		updatedFCCSegInventory.setLogicalCCCode(fCCSegInventory.getLogicalCCCode());
		updatedFCCSegInventory.setOnholdInfantSeats(fCCSegInventory.getOnholdInfantSeats());
		updatedFCCSegInventory.setOnHoldSeats(fCCSegInventory.getOnHoldSeats());
		updatedFCCSegInventory.setOverlappingFlightId(fCCSegInventory.getOverlappingFlightId());
		updatedFCCSegInventory.setOversellSeats(fCCSegInventory.getOversellSeats());
		updatedFCCSegInventory.setSeatsSold(fCCSegInventory.getSoldAndOnholdAdultSeats());
		updatedFCCSegInventory.setSegmentCode(fCCSegInventory.getSegmentCode());
		updatedFCCSegInventory.setSoldInfantSeats(fCCSegInventory.getSoldAndOnholdInfantSeats());
		updatedFCCSegInventory.setValidFlag(fCCSegInventory.getValidFlag());
		return updatedFCCSegInventory;
	}

	@Override
	public int updateFCCSegmentBCInventory(int flightId, String logicalCabinClassCode, String segmentCode, String bookingCode,
			int allocated, int allocatedWaitList, int acquiredSeats, String status, String statusChangeAction, boolean isPriority,
			long version, boolean includeVersionCheck) {

		String hql = "update FCCSegBCInventory set seatsAllocated=:seatsAllocated, allocatedWaitListSeats =:allocatedWaitListSeats, seatsAcquired=:seatsAcquired, "
				+ "priorityFlagChar=:priorityFlagChar, status=:status, statusChangeAction=:statusChangeAction,"
				+ "seatsAvailable=(:seatsAllocated + :seatsAcquired - seatsSold - onHoldSeats - seatsCancelled), "
				+ "version = version + 1 where flightId=:flightId AND logicalCCCode=:logicalCCCode "
				+ "AND segmentCode=:segmentCode AND bookingCode=:bookingCode "
				+ "AND (:seatsAllocated + :seatsAcquired - seatsSold - onHoldSeats - seatsCancelled) >= 0 ";

		if (includeVersionCheck) {
			hql += " AND version=:version";
		}

		Query query = getSession().createQuery(hql).setInteger("seatsAllocated", allocated)
				.setInteger("allocatedWaitListSeats", allocatedWaitList).setInteger("seatsAcquired", acquiredSeats)
				.setString("priorityFlagChar", isPriority ? "Y" : "N").setString("status", status)
				.setString("statusChangeAction", statusChangeAction).setInteger("flightId", flightId)
				.setString("logicalCCCode", logicalCabinClassCode).setString("segmentCode", segmentCode)
				.setString("bookingCode", bookingCode);

		if (includeVersionCheck) {
			query.setLong("version", version);
		}
		int result = query.executeUpdate();

		if (invLog.isDebugEnabled()) {
			invLog.debug("updateFCCSegmentBCInventory::fltid:" + flightId + ",lcc:" + logicalCabinClassCode + ",seg:"
					+ segmentCode + ",bc:" + bookingCode + ",alloc:" + allocated + ",acquired:" + acquiredSeats + ",status:"
					+ status + ",priority:" + isPriority + ",version:" + version + ",vcheck:" + includeVersionCheck + ",result:"
					+ result);

		}
		return result;
	}

	@Override
	public int updateDedicatedFCCSegmentBCInventory(int flightId, String logicalCabinClassCode, String segmentCode,
			String bookingCode, int allocated, int acquiredSeats, String status, String statusChangeAction, boolean isPriority,
			long version, boolean includeVersionCheck) {

		String sql = "update FCCSegBCInventory set priorityFlagChar=:priorityFlagChar, status=:status, "
				+ "statusChangeAction=:statusChangeAction, version = version + 1 "
				+ "where flightId=:flightId AND logicalCCCode=:logicalCCCode AND segmentCode=:segmentCode AND bookingCode=:bookingCode "
				+ "AND (:seatsAllocated + :seatsAcquired - seatsSold - onHoldSeats - seatsCancelled) >= 0 ";

		if (includeVersionCheck) {
			sql += " AND version=:version";
		}

		Query query = getSession().createQuery(sql).setInteger("seatsAllocated", allocated)
				.setInteger("seatsAcquired", acquiredSeats).setString("priorityFlagChar", isPriority ? "Y" : "N")
				.setString("status", status).setString("statusChangeAction", statusChangeAction).setInteger("flightId", flightId)
				.setString("logicalCCCode", logicalCabinClassCode).setString("segmentCode", segmentCode)
				.setString("bookingCode", bookingCode);

		if (includeVersionCheck) {
			query.setLong("version", version);
		}
		int result = query.executeUpdate();
		if (invLog.isDebugEnabled()) {
			invLog.debug("updateDedicatedFCCSegmentBCInventory::fltid:" + flightId + ",lcc:" + logicalCabinClassCode + ",seg:"
					+ segmentCode + ",bc:" + bookingCode + ",alloc:" + allocated + ",acquired:" + acquiredSeats + ",status:"
					+ status + ",priority:" + isPriority + ",version:" + version + ",vcheck:" + includeVersionCheck + ",result:"
					+ result);
		}

		return result;
	}

	/**
	 * Update adultCapacity and infantCapacity in FCCInventory and FCCSegInventory
	 * 
	 * @throws ModuleException
	 */
	@Override
	public void updateAllocatedCapacities(Collection<Integer> flightIds, String cabinClassCode, int newAdultCapacity,
			int newInfantCapacity, String scheduleSegmentCode, boolean downgradeToAnyModel) throws ModuleException {
		Object[] flightIdsArr = flightIds.toArray();
		int chunkSize = AirinventoryCustomConstants.DB_OPERATION_CHUNK_SIZE;

		Collection<String> singleLegSegmentCodes = null;

		// update FCCIventory
		for (int page = 0; page < flightIds.size(); page += chunkSize) {

			ArrayList<Integer> pagedFlightIds = new ArrayList<Integer>();
			for (int i = page; i < page + chunkSize; i++) {
				if (i < flightIdsArr.length) {
					pagedFlightIds.add((Integer) flightIdsArr[i]);
				} else {
					break;
				}
			}
			// TODO overbook situation can not be addressed in this context since after making a over book , its not
			// possible to detect that there is a over book situation. so solution is to keep separate table column to
			// record over booking count[T_FCC_SEG_ALLOC].

			if (pagedFlightIds.size() != 0) {
				String updateFCCInventoryHql = "Update FCCInventory set actualCapacity=:actualCapacity, "
						+ "infantCapacity=:infantCapacity, version = version + 1 where flightId IN (:flightIds) AND CCCode=:CCCode";
				int updatedFCCInventoryCount = getSession().createQuery(updateFCCInventoryHql)
						.setInteger("actualCapacity", newAdultCapacity).setInteger("infantCapacity", newInfantCapacity)
						.setParameterList("flightIds", pagedFlightIds).setString("CCCode", cabinClassCode).executeUpdate();
				if (updatedFCCInventoryCount != pagedFlightIds.size()) {
					throw new CommonsDataAccessException("airinventory.dao.fccinv.update.failed",
							AirinventoryConstants.MODULE_NAME);
				}

				Map<Integer, Map<Integer, List<FCCSegInventory>>> fccSegmentInvMap = getFCCSegInventories(pagedFlightIds,
						cabinClassCode);

				List<FCCSegInventory> updatedfccSegInvs = new ArrayList<FCCSegInventory>();

				for (Integer flightId : fccSegmentInvMap.keySet()) {
					Map<Integer, List<FCCSegInventory>> segmentWiseInventoryMap = fccSegmentInvMap.get(flightId);
					// load logical cabin class wise sold seat count only if intercepting segments exist
					Collection<FCCSegInventory> fccSegmentInvetories = new ArrayList<FCCSegInventory>();
					for (Collection<FCCSegInventory> fccSegInvs : segmentWiseInventoryMap.values()) {
						fccSegmentInvetories.addAll(fccSegInvs);
					}
					boolean hasInterceptingSegments = AirinventoryUtils.hasInterceptingSegments(fccSegmentInvetories);
					Map<String, Map<String, int[]>> segmentAndLogicalCabinClassWiseSoldSeatCount = null;
					if (hasInterceptingSegments) {
						if (singleLegSegmentCodes == null) {
							singleLegSegmentCodes = AirinventoryUtils.buildSingleLegSegmentCodes(
									getFlightInventoryJDBCDAO().getFlightSegmentCodes(flightIds.iterator().next()));
						}
						segmentAndLogicalCabinClassWiseSoldSeatCount = getFlightInventoryJDBCDAO()
								.getTotalSoldSeatCountSegmentAndLogicalCabinClassWise(flightId, singleLegSegmentCodes,
										cabinClassCode);
					}
					for (Integer segmentId : segmentWiseInventoryMap.keySet()) {
						List<FCCSegInventory> fccSegInventories = segmentWiseInventoryMap.get(segmentId);

						if (fccSegInventories.size() > 1) {
							Collections.sort(fccSegInventories, Collections.reverseOrder(new Comparator<FCCSegInventory>() {
								@Override
								public int compare(FCCSegInventory first, FCCSegInventory second) {
									return Integer.valueOf(first.getSoldAndOnholdAdultSeats())
											.compareTo(Integer.valueOf(second.getSoldAndOnholdAdultSeats()));
								}
							}));

							for (int i = 0; i < fccSegInventories.size(); i++) {
								FCCSegInventory fccSegInventory = fccSegInventories.get(i);
								int segmentAdultSeatsSold = 0;
								if (hasInterceptingSegments) {
									segmentAdultSeatsSold = AirinventoryUtils.getMaxSoldSeatcCountOnFlightSegment(
											segmentAndLogicalCabinClassWiseSoldSeatCount, fccSegInventory.getSegmentCode(),
											fccSegInventory.getLogicalCCCode())[0];
								} else {
									segmentAdultSeatsSold = fccSegInventory.getSoldAndOnholdAdultSeats();
								}
								updatefccSegmentInvetory(fccSegInventory, segmentAdultSeatsSold,
										fccSegInventory.getSoldAndOnholdInfantSeats(), newAdultCapacity,
										fccSegInventory.getInfantAllocation(), hasInterceptingSegments);
							}

							Collections.sort(fccSegInventories, new Comparator<FCCSegInventory>() {

								private Map<String, LogicalCabinClassDTO> lccMap = CommonsServices.getGlobalConfig()
										.getAvailableLogicalCCMap();

								@Override
								public int compare(FCCSegInventory first, FCCSegInventory second) {
									LogicalCabinClassDTO fLcc = lccMap.get(first.getLogicalCCCode());
									LogicalCabinClassDTO sLcc = lccMap.get(second.getLogicalCCCode());
									if (fLcc == null && sLcc == null) {
										return 0;
									}
									return fLcc.getNestRank().compareTo(sLcc.getNestRank());
								}
							});

							Collections.sort(fccSegInventories, Collections.reverseOrder(new Comparator<FCCSegInventory>() {
								@Override
								public int compare(FCCSegInventory first, FCCSegInventory second) {
									return Integer.valueOf(first.getSoldAndOnholdInfantSeats())
											.compareTo(Integer.valueOf(second.getSoldAndOnholdInfantSeats()));
								}
							}));

							for (int i = 0; i < fccSegInventories.size(); i++) {
								FCCSegInventory fccSegInventory = fccSegInventories.get(i);
								int segmentInfantSeatsSold = 0;
								if (hasInterceptingSegments) {
									segmentInfantSeatsSold = AirinventoryUtils.getMaxSoldSeatcCountOnFlightSegment(
											segmentAndLogicalCabinClassWiseSoldSeatCount, fccSegInventory.getSegmentCode(),
											fccSegInventory.getLogicalCCCode())[1];
								} else {
									segmentInfantSeatsSold = fccSegInventory.getSoldAndOnholdInfantSeats();
								}

								updatefccSegmentInvetory(fccSegInventory, fccSegInventory.getSoldAndOnholdAdultSeats(),
										segmentInfantSeatsSold, fccSegInventory.getAllocatedSeats(), newInfantCapacity,
										hasInterceptingSegments);
							}

						} else {
							FCCSegInventory fccSegInventory = fccSegInventories.get(0);
							int[] maxSoldSeatCount = new int[] { fccSegInventory.getSoldAndOnholdAdultSeats(),
									fccSegInventory.getSoldAndOnholdInfantSeats() };
							updatefccSegmentInvetory(fccSegInventory, maxSoldSeatCount[0], maxSoldSeatCount[1], newAdultCapacity,
									newInfantCapacity, hasInterceptingSegments);
						}
						updatedfccSegInvs.addAll(fccSegInventories);
					}
				}

				saveOrUpdateFCCSegInventory(updatedfccSegInvs);

				// update inventory availability
				if (!updatedfccSegInvs.isEmpty()) {
					getSession().flush();
					List<Integer> fccSegInvIds = getFlightInventoryJDBCDAO().getFCCSegInventoryIds(pagedFlightIds,
							cabinClassCode);
					List<Integer> interceptingFccSegInvIds = getFlightInventoryDAO()
							.getInterceptingFCCSegmentInventoryIds(fccSegInvIds, true);
					if (interceptingFccSegInvIds != null && !interceptingFccSegInvIds.isEmpty()) {
						this.updateFCCInterceptingSegmentInvAndAVS(interceptingFccSegInvIds);
					}
				}
			}
		}
		// synchronizing the session with the database
		getSession().flush();
		log.debug("------Session flush() invoked------");
	}

	/**
	 * @param fccSegInventory
	 * @param maxSoldSeatCount
	 * @param newAdultCapacity
	 * @param newInfantCapacity
	 * @param hasInterceptingSegments
	 * @throws ModuleException
	 */
	private void updatefccSegmentInvetory(FCCSegInventory fccSegInventory, int soldAdultSeats, int soldInfantSeats,
			int newAdultCapacity, int newInfantCapacity, boolean hasInterceptingSegments) throws ModuleException {
		// update adult seat availability
		int availableFixedSeatCount = 0;
		if (hasInterceptingSegments) {
			availableFixedSeatCount = getFlightInventoryJDBCDAO().getAvailableFixedSeatCount(fccSegInventory.getFccsInvId());
		}
		int[] availableFixedSeatCountAll = getFlightInventoryJDBCDAO()
				.getEffectiveAvailableFixedSeatCount(fccSegInventory.getFccsInvId());
		int effectiveCapacity = newAdultCapacity + fccSegInventory.getOversellSeats() + availableFixedSeatCount
				- fccSegInventory.getCurtailedSeats();
		fccSegInventory.setAllocatedSeats(newAdultCapacity);
		int noOfSeatsToReduce = availableFixedSeatCountAll[0] - effectiveCapacity;

		if (AppSysParamsUtil.enableDowngradeAircraftModel() && noOfSeatsToReduce > 0) {

			if (availableFixedSeatCountAll[0] > effectiveCapacity) {
				// reduce the bc allocation of fixed seat

				Collection<FCCSegBCInventory> fccBCInventory = getFixedBCInventories(fccSegInventory.getFccsInvId());
				int reducedSeats = noOfSeatsToReduce;
				Collection<FCCSegBCInventory> updatedFccBCInventory = new ArrayList<FCCSegBCInventory>();

				int fixedBCCount = fccBCInventory.size();
				if (fixedBCCount > 0) {
					if (fixedBCCount == 1) {
						FCCSegBCInventory fccBCInv = (FCCSegBCInventory) fccBCInventory.toArray()[0];
						int actualCapacity = newAdultCapacity + fccSegInventory.getOversellSeats()
								- fccSegInventory.getCurtailedSeats();
						fccBCInv.setSeatsAllocated(actualCapacity);
						fccBCInv.setSeatsAvailable(
								Math.max(effectiveCapacity - fccBCInv.getSeatsSold() - fccBCInv.getOnHoldSeats(), 0));
						updatedFccBCInventory.add(fccBCInv);
					} else {
						throw new ModuleException("airschedules.logic.bl.cc.morethanonefixedbookingclasses",
								AirinventoryConstants.MODULE_NAME);
					}
				}

				updateBCInvetory(updatedFccBCInventory);
			}
		}

		// /*****************************************************************

		fccSegInventory.setAvailableSeats(Math.max((effectiveCapacity - soldAdultSeats), 0));
		fccSegInventory.setCurtailedSeats(Math.min(fccSegInventory.getCurtailedSeats(), newAdultCapacity));
		// update infant seats
		fccSegInventory.setInfantAllocation(newInfantCapacity);
		fccSegInventory.setAvailableInfantSeats(Math.max(newInfantCapacity - soldInfantSeats, 0));
	}

	@Override
	public void deleteFlightInventories(Collection<Integer> flightIds, String cabinClassCode) {
		if (flightIds == null || flightIds.size() == 0) {
			return;
		}

		log.warn("Deleting inventory [flightIds=" + flightIds.toString()
				+ (cabinClassCode == null ? "" : (",cabinClassCode=" + cabinClassCode)) + "]");
		Object[] flightIdsArr = flightIds.toArray();
		int chunkSize = AirinventoryCustomConstants.DB_OPERATION_CHUNK_SIZE;

		for (int page = 0; page < flightIds.size(); page += chunkSize) {

			ArrayList<Integer> pagedFlightIds = new ArrayList<Integer>();
			for (int i = page; i < page + chunkSize; i++) {
				if (i < flightIdsArr.length) {
					pagedFlightIds.add((Integer) flightIdsArr[i]);
				} else {
					break;
				}
			}

			if (pagedFlightIds.size() != 0) {

				// delete flightseats if exists --. success if flight get seatmaptemplate.
				/*
				 * String deletePassengerSeatHql =
				 * "DELETE FROM com.isa.thinair.airreservation.api.model.PassengerSeating WHERE paxSeatingid in " +
				 * "( SELECT FS.flightAmSeatId FROM FlightSeat FS,FCCSegInventory FSI WHERE FS.flightSegInventoryId=FSI.fccsInvId and FS.status='RES' and FSI.flightId IN (:flightIds) ) "
				 * ; getSession().createQuery(deletePassengerSeatHql).setParameterList("flightIds",
				 * pagedFlightIds).executeUpdate();
				 */
				String cabinClassHql = " AND logicalCCCode IN (select logicalCCCode from LogicalCabinClass where cabinClassCode='"
						+ cabinClassCode + "' ) ";
				// defect fix AARESAA-1340
				String deleteFlightSeatHql = "DELETE FROM FlightSeat WHERE flightSegInventoryId in "
						+ "( SELECT fccsInvId FROM FCCSegInventory WHERE flightId IN (:flightIds)  ";
				if (cabinClassCode != null) {
					deleteFlightSeatHql += cabinClassHql;
				}
				deleteFlightSeatHql += " )";

				getSession().createQuery(deleteFlightSeatHql).setParameterList("flightIds", pagedFlightIds).executeUpdate();

				String deleteFccBcInvHql = "DELETE FROM FCCSegBCInvNesting WHERE FROM_FCCSBA_ID in "
						+ "( SELECT fccsbInvId FROM FCCSegBCInventory WHERE flightId IN (:flightIds)" + cabinClassHql
						+ " ) or  TO_FCCSBA_ID in " + "( SELECT fccsbInvId FROM FCCSegBCInventory WHERE flightId IN (:flightIds)"
						+ cabinClassHql + " )";
				getSession().createQuery(deleteFccBcInvHql).setParameterList("flightIds", pagedFlightIds).executeUpdate();
				// delete bc inventories
				String deleteFCCSegBCInventoriesHql = "DELETE FROM FCCSegBCInventory WHERE flightId IN (:flightIds) ";
				if (cabinClassCode != null) {
					deleteFCCSegBCInventoriesHql += " AND logicalCCCode IN (select logicalCCCode from LogicalCabinClass where cabinClassCode='"
							+ cabinClassCode + "' ) ";
				}
				getSession().createQuery(deleteFCCSegBCInventoriesHql).setParameterList("flightIds", pagedFlightIds)
						.executeUpdate();

				// delete intercepting segement info records
				String deleteInterceptingSegHql = "DELETE FROM InterceptingFCCSegAlloc WHERE "
						+ "sourceFCCSegInvId IN (SELECT fccsInvId FROM FCCSegInventory WHERE flightId IN (:flightId) ";
				if (cabinClassCode != null) {
					deleteInterceptingSegHql += " AND logicalCCCode IN (select logicalCCCode from LogicalCabinClass where cabinClassCode='"
							+ cabinClassCode + "' ) ";
				}
				deleteInterceptingSegHql += ") OR interceptingFCCSegInvId IN "
						+ "(SELECT fccsInvId FROM FCCSegInventory WHERE flightId IN (:flightId) ";
				if (cabinClassCode != null) {
					deleteInterceptingSegHql += " AND logicalCCCode IN (select logicalCCCode from LogicalCabinClass where cabinClassCode='"
							+ cabinClassCode + "' ) ";
				}
				deleteInterceptingSegHql += ")";
				getSession().createQuery(deleteInterceptingSegHql).setParameterList("flightId", pagedFlightIds).executeUpdate();

				// delete segment inventories
				String deleteFCCSegInventoriesHql = "DELETE FCCSegInventory WHERE flightId IN (:flightIds) ";
				if (cabinClassCode != null) {
					deleteFCCSegInventoriesHql += " AND logicalCCCode IN (select logicalCCCode from LogicalCabinClass where cabinClassCode='"
							+ cabinClassCode + "' )";
				}
				getSession().createQuery(deleteFCCSegInventoriesHql).setParameterList("flightIds", pagedFlightIds)
						.executeUpdate();

				// delete cabinclass inventory
				String deleteFCCInventoryHql = "DELETE FCCInventory WHERE flightId IN (:flightIds)";
				if (cabinClassCode != null) {
					deleteFCCInventoryHql += " AND CCCode ='" + cabinClassCode + "'";
				}
				getSession().createQuery(deleteFCCInventoryHql).setParameterList("flightIds", pagedFlightIds).executeUpdate();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Integer> getFccsbInvIdForFlightIDs(Collection<Integer> flightIds, String logicalCabinClassCode) {
		String hql = "select bcInv.fccsbInvId FROM  FCCSegBCInventory AS bcInv WHERE bcInv.flightId IN (:flightIds) ";

		if (logicalCabinClassCode != null && !"".equals(logicalCabinClassCode)) {
			hql += "AND bcInv.logicalCCCode='" + logicalCabinClassCode + "' ";
		}
		return getSession().createQuery(hql).setParameterList("flightIds", flightIds).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO#deleteFlightSegmentInventories(java.lang
	 * .Integer)
	 */
	@Override
	public void deleteFlightSegmentInventories(Collection<Integer> fccSegInvIds) {

		// delete booking class inventories
		String deleteFCCSegBCInventoriesHql = "DELETE FROM FCCSegBCInventory WHERE fccsInvId IN (:fccSegInvIds) ";
		getSession().createQuery(deleteFCCSegBCInventoriesHql).setParameterList("fccSegInvIds", fccSegInvIds).executeUpdate();

		// delete intercepting segment info records
		String deleteInterceptingSegHql = "DELETE FROM InterceptingFCCSegAlloc WHERE sourceFCCSegInvId in (:fccSegInvIds) "
				+ "OR interceptingFCCSegInvId in (:fccSegInvIds)";
		getSession().createQuery(deleteInterceptingSegHql).setParameterList("fccSegInvIds", fccSegInvIds).executeUpdate();

		// delete segment inventories
		String deleteFCCSegInventoriesHql = "DELETE FCCSegInventory WHERE fccsInvId in (:fccSegInvIds) ";
		getSession().createQuery(deleteFCCSegInventoriesHql).setParameterList("fccSegInvIds", fccSegInvIds).executeUpdate();
	}

	@Override
	public boolean isSegmentEligibleForMakingValid(int flightSegId, List<Integer> interceptingSegInvIds) {
		// get sold, onhold, fixed on segment
		StringBuffer soldOnHoldSeatsCountHql = new StringBuffer("select sum(FSBI.seatsSold+FSBI.onHoldSeats) ");
		soldOnHoldSeatsCountHql.append("from FCCSegInventory FSI, FCCSegBCInventory FSBI, BookingClass BC where ")
				.append("FSI.fccsInvId = FSBI.fccsInvId AND ").append("FSBI.bookingCode = BC.bookingCode AND ")
				.append("FSI.flightSegId = " + flightSegId + " AND ").append("BC.fixedFlagChar='N' AND ")
				.append("BC.bcType NOT IN (" + Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) + " )");
		Long soldOnholdSeats = (Long) getSession().createQuery(soldOnHoldSeatsCountHql.toString()).uniqueResult();
		int requiredAvailableSeats = (soldOnholdSeats != null) ? soldOnholdSeats.intValue() : 0;

		StringBuffer fixedAndInfantSeatCountsHql = new StringBuffer("select sum(FSI.fixedSeats), ");
		fixedAndInfantSeatCountsHql.append("sum(FSI.soldInfantSeats), ").append("sum(FSI.onholdInfantSeats) ")
				.append("from FCCSegInventory FSI where ").append("FSI.flightSegId = " + flightSegId);
		Object[] fixedSeats = (Object[]) getSession().createQuery(fixedAndInfantSeatCountsHql.toString()).uniqueResult();
		requiredAvailableSeats += (fixedSeats[0] != null) ? ((Long) fixedSeats[0]).intValue() : 0;

		// get min(available) on intercepting segment inventories
		int allowableAvailableSeats = getMinimumAvailability(interceptingSegInvIds);

		// Skip auditing as this is not really want to audit
		if (allowableAvailableSeats >= requiredAvailableSeats) {
			return true;
		}

		return false;
	}

	@Override
	public boolean checkSegSoldOnholdCountValidity(int fccsInvId, int soldSeats, int onholdSeats, int soldInfantSeats,
			int onholdInfantSeats) {
		String hql = "SELECT count(*) FROM FCCSegInventory WHERE fccsInvId = ? AND (seatsSold < ?"
				+ " OR onHoldSeats < ? OR soldInfantSeats < ? OR onholdInfantSeats < ?)";
		Long count = (Long) getSession().createQuery(hql).setInteger(0, fccsInvId).setInteger(1, soldSeats)
				.setInteger(2, onholdSeats).setInteger(3, soldInfantSeats).setInteger(4, onholdInfantSeats).uniqueResult();
		if (count != null && count.intValue() > 0) {
			return false;
		}
		return true;
	}

	@Override
	public void updateFltSegInventoriesValidFlag(Collection<Integer> flightSegIds, String cabinClassCode, String validFlag) {
		String hql = "UPDATE FCCSegInventory SET validFlagChar='" + validFlag + "' WHERE flightSegId IN (:flightSegIds) ";
		if (cabinClassCode != null) {
			hql += "AND ccCode ='" + cabinClassCode + "'";
		}

		getSession().createQuery(hql).setParameterList("flightSegIds", flightSegIds).executeUpdate();

		// Skip auditing as this is not really want to audit
		// [TODO:verify] is flush() really required?
		getSession().flush();
		log.debug("------Session flush() invoked------");
	}

	@Override
	public void setOverlappingFlight(int flightId, int overlappingFligtId) {
		getSession()
				.createQuery(
						"UPDATE FCCSegInventory SET overlappingFlightId=" + overlappingFligtId + " WHERE flightId =" + flightId)
				.executeUpdate();
	}

	@Override
	public void unsetOverlappingFlight(Collection<Integer> flightIds) {
		getSession().createQuery("UPDATE FCCSegInventory SET overlappingFlightId=null WHERE flightId IN (:flightIds)")
				.setParameterList("flightIds", flightIds).executeUpdate();
	}

	@Override
	public void removeInventoryOverlap(Collection<Integer> flightIds) throws ModuleException {
		log.warn("Going to remove overlap [flightIds" + flightIds + "]");
		if (flightIds.size() != 2) {
			throw new ModuleException("airinventory.arg.dao.remove.overlap.invalid", AirinventoryCustomConstants.INV_MODULE_CODE);
		}

		// unset overlapping flightIds
		unsetOverlappingFlight(flightIds);

		// unset intercepting segments from the overlapping flights
		Object[] flightIdsArr = flightIds.toArray();
		String deleteInterceptingSegHql = "DELETE FROM InterceptingFCCSegAlloc WHERE "
				+ "sourceFCCSegInvId IN (SELECT fccsInvId FROM FCCSegInventory WHERE flightId = ? ) ";

		deleteInterceptingSegHql += " AND interceptingFCCSegInvId IN "
				+ "(SELECT fccsInvId FROM FCCSegInventory WHERE flightId = ? ) ";

		getSession().createQuery(deleteInterceptingSegHql).setInteger(0, ((Integer) flightIdsArr[0]).intValue())
				.setInteger(1, ((Integer) flightIdsArr[1]).intValue()).executeUpdate();

		getSession().createQuery(deleteInterceptingSegHql).setInteger(0, ((Integer) flightIdsArr[1]).intValue())
				.setInteger(1, ((Integer) flightIdsArr[0]).intValue()).executeUpdate();

		// [TODO:verify] Is session flush really required?
		getSession().flush();
		log.debug("------Session flush() invoked------");
	}

	@Override
	public Integer getBCInvNestingInfo(Integer fccsbInvNestingId) {
		String hql = "SELECT FSBIN.toFCCSegBCInvId FROM FCCSegBCInvNesting FSBIN WHERE FSBIN.fccsbInvNestingId = ?";
		return (Integer) getSession().createQuery(hql).setInteger(0, fccsbInvNestingId.intValue()).uniqueResult();
	}

	@Override
	public int getMinimumAvailability(List<Integer> fccSegInventoryIds) {
		String hql = "select min(availableSeats) from FCCSegInventory where fccsInvId IN (:fccsInvIds)";
		Object count = getSession().createQuery(hql).setParameterList("fccsInvIds", fccSegInventoryIds).uniqueResult();
		// count becomes null, if no matching records to take the minimum count
		if (count != null) {
			return ((Integer) count).intValue();
		}

		return 0;
	}

	@Override
	public void saveInterceptingFCCSegAlloc(Collection<InterceptingFCCSegAlloc> interceptingFCCSegAllocs) {
		hibernateSaveOrUpdateAll(interceptingFCCSegAllocs);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getFCCSegmentInventoriesSummary(Collection<Integer> flightIds, String logicalCabinClassCode) {
		String hql = "SELECT flightId, segmentCode, fccsInvId, oversellSeats, curtailedSeats FROM FCCSegInventory WHERE "
				+ "flightId IN (:flightIds) ";
		if (logicalCabinClassCode != null) {
			hql += "AND logicalCCCode='" + logicalCabinClassCode + "' ";
		}
		hql += "order by flightId asc, segmentCode asc";
		return getSession().createQuery(hql).setParameterList("flightIds", flightIds).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getFCCSegmentInventoriesSummary(Collection<Integer> flightIds, String cabinClassCode,
			Map<String, List<String>> segmentsAndLogicalCabinClasses) {
		StringBuffer hqlBuffer = new StringBuffer(
				"SELECT FFCSA.flightId, FFCSA.segmentCode, FFCSA.fccsInvId, FFCSA.oversellSeats, FFCSA.curtailedSeats , FFCSA.logicalCCCode, FFCSA.allocatedWaitListSeats FROM FCCSegInventory AS FFCSA,LogicalCabinClass AS LCC WHERE "
						+ " FFCSA.flightId IN (:flightIds) AND FFCSA.logicalCCCode=LCC.logicalCCCode AND ");
		int i = 0;

		if (!segmentsAndLogicalCabinClasses.keySet().isEmpty()) {
			hqlBuffer.append(" ( ");
		}

		for (String segmentCode : segmentsAndLogicalCabinClasses.keySet()) {
			if (i != 0) {
				hqlBuffer.append(" OR ");
			}
			List<String> logicalCabinClasses = segmentsAndLogicalCabinClasses.get(segmentCode);
			hqlBuffer.append(" ( FFCSA.segmentCode = '").append(segmentCode).append("' AND ")
					.append(prepareSqlINString("FFCSA.logicalCCCode", logicalCabinClasses, " ) "));
			i++;
		}

		if (!segmentsAndLogicalCabinClasses.keySet().isEmpty()) {
			hqlBuffer.append(" ) ").append(" AND ");
		}

		hqlBuffer.append(" LCC.cabinClassCode = '").append(cabinClassCode).append("'");
		hqlBuffer.append(" order by FFCSA.flightId asc, FFCSA.segmentCode asc");
		return getSession().createQuery(hqlBuffer.toString()).setParameterList("flightIds", flightIds).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getInterceptingFCCSegmentInventoryIds(int flightId, String logicalCabinClassCode, String segmentCode,
			boolean excludeInvalidSegments) {
		List<Object> paramsList = new ArrayList<>();
		String hql = "SELECT IFSI.interceptingFCCSegInvId FROM InterceptingFCCSegAlloc IFSI, FCCSegInventory FSI WHERE "
				+ "FSI.fccsInvId=IFSI.sourceFCCSegInvId AND FSI.flightId=? AND FSI.logicalCCCode like ? AND FSI.segmentCode=? ";
		paramsList.add(flightId);
		paramsList.add(logicalCabinClassCode);
		paramsList.add(segmentCode);
		if (excludeInvalidSegments) {
			hql += " AND FSI.validFlagChar = ?";
			paramsList.add(FCCSegInventory.VALID_FLAG_Y);
		}
		return find(hql, paramsList.toArray(new Object[0]), Integer.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getInterceptingFCCSegmentInventoryIds(int fccSegInventoryId, boolean excludeInvalidSegments) {
		String hql = "SELECT IFSI.interceptingFCCSegInvId FROM InterceptingFCCSegAlloc IFSI WHERE IFSI.sourceFCCSegInvId=?";
		if (excludeInvalidSegments) {
			hql = "SELECT IFSI.interceptingFCCSegInvId FROM InterceptingFCCSegAlloc IFSI, FCCSegInventory FSI WHERE "
					+ "FSI.fccsInvId=IFSI.sourceFCCSegInvId AND IFSI.sourceFCCSegInvId=? AND FSI.validFlagChar ='"
					+ FCCSegInventory.VALID_FLAG_Y + "'";
		}
		return getSession().createQuery(hql).setInteger(0, fccSegInventoryId).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getInterceptingFCCSegmentInventoryIds(Collection<Integer> fccSegInventoryIds,
			boolean excludeInvalidSegments) {
		String hql = "SELECT DISTINCT IFSI.interceptingFCCSegInvId FROM InterceptingFCCSegAlloc IFSI WHERE "
				+ Util.getReplaceStringForIN("IFSI.sourceFCCSegInvId", fccSegInventoryIds);
		if (excludeInvalidSegments) {
			hql = "SELECT DISTINCT IFSI.interceptingFCCSegInvId FROM InterceptingFCCSegAlloc IFSI, FCCSegInventory FSI WHERE "
					+ "FSI.fccsInvId=IFSI.sourceFCCSegInvId AND "
					+ Util.getReplaceStringForIN("IFSI.sourceFCCSegInvId", fccSegInventoryIds) + " AND " + "FSI.validFlagChar ='"
					+ FCCSegInventory.VALID_FLAG_Y + "'";
		}
		return getSession().createQuery(hql).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getInterceptingFCCSegInventoryIds(int flightSegId, String logicalCabinClassCode,
			boolean excludeInvalidSegments) {
		String hql = "SELECT IFSI.interceptingFCCSegInvId FROM InterceptingFCCSegAlloc IFSI, FCCSegInventory FSI where "
				+ "FSI.fccsInvId=IFSI.sourceFCCSegInvId AND FSI.flightSegId= " + flightSegId;
		if (logicalCabinClassCode != null) {
			hql += " AND FSI.logicalCCCode like '" + logicalCabinClassCode + "'";
		}
		if (excludeInvalidSegments) {
			hql += " AND FSI.validFlagChar ='" + FCCSegInventory.VALID_FLAG_Y + "'";
		}
		return getSession().createQuery(hql).list();
	}

	/**
	 * @return List of FCCSegInventoryDTOs - getFCCSegmentInventoriesSummary, fccSegInventories should be ordered by the
	 *         flight id to count the seg allocs for each flight
	 */
	@Override
	public List<FCCSegInventoryDTO> getFCCSegmentInventoryAndBCSummary(Collection<Integer> flightIds, String cabinClassCode,
			Collection<String> segmentCodes) {
		String hqlSegInventories = "SELECT new com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO(FSI.fccsInvId, "
				+ "FSI.flightId , LCC.cabinClassCode , FSI.logicalCCCode, FSI.segmentCode, FSI.seatsSold, FSI.onHoldSeats,"
				+ " FSI.availableSeats,FSI.oversellSeats,FSI.curtailedSeats, FSI.allocatedWaitListSeats) FROM FCCSegInventory as FSI ,LogicalCabinClass as LCC  "
				+ "WHERE  FSI.logicalCCCode = LCC.logicalCCCode AND " + prepareSqlINString("FSI.flightId", flightIds, " AND ")
				+ prepareSqlINString("FSI.segmentCode", segmentCodes, " AND ") + "LCC.cabinClassCode ='" + cabinClassCode + "' "
				+ "ORDER BY FSI.flightId asc, FSI.segmentCode asc";

		String hqlBCInventories = "SELECT FSBI FROM FCCSegBCInventory as FSBI, BookingClass as BC "
				+ "WHERE FSBI.bookingCode = BC.bookingCode AND " + prepareSqlINString("FSBI.flightId", flightIds, " AND ")
				+ prepareSqlINString("FSBI.segmentCode", segmentCodes, " AND ") + "BC.cabinClassCode ='" + cabinClassCode
				+ "' AND " + "BC.paxType <> '" + BookingClass.PassengerType.GOSHOW + "' "
				+ "ORDER BY FSBI.flightId asc, FSBI.segmentCode asc";
		@SuppressWarnings("unchecked")
		List<FCCSegInventoryDTO> fccSegmentInventories = getSession().createQuery(hqlSegInventories).list();
		@SuppressWarnings("unchecked")
		List<FCCSegBCInventory> fccSegBCInventories = getSession().createQuery(hqlBCInventories).list();

		// merge bc inventories with segment inventories
		if (fccSegmentInventories != null && fccSegBCInventories != null) {
			Iterator<FCCSegInventoryDTO> fccSegmentInventoriesIt = fccSegmentInventories.iterator();
			while (fccSegmentInventoriesIt.hasNext()) {
				FCCSegInventoryDTO fccSegInventoryDTO = fccSegmentInventoriesIt.next();
				Iterator<FCCSegBCInventory> fccSegBCInventoriesIt = fccSegBCInventories.iterator();
				while (fccSegBCInventoriesIt.hasNext()) {
					FCCSegBCInventory fccSegBCInventory = fccSegBCInventoriesIt.next();
					if (fccSegInventoryDTO.getFccsInvId() == fccSegBCInventory.getfccsInvId()) {
						fccSegInventoryDTO.addFCCSegBCInventory(fccSegBCInventory.getBookingCode(), fccSegBCInventory);
					}
				}
			}
		}

		return fccSegmentInventories;
	}

	@Override
	public HashMap<Integer, FlightInventorySummaryDTO> getFlightInventoriesSummary(Collection<Integer> flightIds,
			boolean isReporting) {
		String hql = "select new com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO(FSI.flightId, sum(FSI.allocatedSeats), sum(FSI.infantAllocation),"
				+ "sum(FSI.seatsSold), sum(FSI.soldInfantSeats), sum(FSI.onHoldSeats),"
				+ "sum(FSI.onholdInfantSeats), sum(FSI.availableSeats), sum(FSI.availableInfantSeats),sum(FSI.fixedSeats)) "
				+ "from FCCSegInventory as FSI where FSI.flightId IN (:flightIds) group by FSI.flightId";
		Session session = null;
		Connection connection = null;

		HashMap<Integer, FlightInventorySummaryDTO> flightInventoriesSummaryMap = new HashMap<Integer, FlightInventorySummaryDTO>();

		try {
			if (AppSysParamsUtil.isReportingDbAvailable() && isReporting) {
				DataSource ds = AirinventoryUtils.getReportingDatasource();
				connection = ds.getConnection();
				session = (Session) getSessionFactory().openStatelessSession(connection);
			} else {
				session = getSession();
			}
			@SuppressWarnings("unchecked")
			List<FlightInventorySummaryDTO> flightInventoriesSummaryList = session.createQuery(hql)
					.setParameterList("flightIds", flightIds).list();

			if (flightInventoriesSummaryList != null) {
				Iterator<FlightInventorySummaryDTO> flightInventoriesSummaryListIt = flightInventoriesSummaryList.iterator();
				while (flightInventoriesSummaryListIt.hasNext()) {
					FlightInventorySummaryDTO fltInvSummaryDTO = flightInventoriesSummaryListIt.next();
					flightInventoriesSummaryMap.put(new Integer(fltInvSummaryDTO.getFlightId()), fltInvSummaryDTO);
				}
			}

			if (isReporting && connection != null && session != null) {
				session.close();
				connection.close();
			}
		} catch (Exception ex) {
			log.error(ex.getStackTrace());
			throw new CommonsDataAccessException(ex, null);
		}
		return flightInventoriesSummaryMap;
	}

	@Override
	public void deleteFCCSegmentBCInventories(int flightId, String sourceLogicalCabinClassCode, String segmentCode,
			boolean excludeGoshow) {
		log.warn("Going to delete BC inventories [flightId=" + flightId + ", logicalCabinClassCode=" + sourceLogicalCabinClassCode
				+ ", segmentCode=" + segmentCode + "]");
		String hql = "delete FCCSegBCInventory where flightId=" + flightId + " AND logicalCCCode ='" + sourceLogicalCabinClassCode
				+ "' AND segmentCode='" + segmentCode + "' ";
		if (excludeGoshow) {
			hql.concat(" AND bookingCode <> (select bookingCode from BookingClass where logicalCCCode='"
					+ sourceLogicalCabinClassCode + "' AND paxType='" + BookingClass.PassengerType.GOSHOW + "')");
		}
		getSession().createQuery(hql).executeUpdate();

	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Map<String, Map<String, List<Object[]>>> getFCCSegmentBCInventoriesSummary(int flightId, String cabinClassCode,
			HashMap<String, HashMap<String, List<String>>> segmentVsSelectedBookingClassesMap) {
		StringBuffer hqlBuffer = new StringBuffer(
				"SELECT FSBI.segmentCode, FSBI.bookingCode, FSBI.seatsAllocated, FSBI.seatsAvailable, FSBI.priorityFlagChar, FSBI.status, ")
						.append(" BC.standardCodeChar, BC.fixedFlagChar, FSBI.statusChangeAction  ,BC.cabinClassCode , FSBI.logicalCCCode, FSBI.allocatedWaitListSeats")
						.append(" FROM FCCSegBCInventory FSBI, BookingClass BC ")
						.append(" WHERE FSBI.bookingCode = BC.bookingCode AND FSBI.logicalCCCode=BC.logicalCCCode AND FSBI.flightId="
								+ flightId + " AND ( ");

		Object[] segmentCodesArr = segmentVsSelectedBookingClassesMap.keySet().toArray();
		for (int i = 0; i < segmentCodesArr.length; i++) {
			String segmentCode = (String) segmentCodesArr[i];
			if (i != 0) {
				hqlBuffer.append(" OR ");
			}
			hqlBuffer.append(" ( FSBI.segmentCode = '").append(segmentCode).append("' AND ( ");
			HashMap<String, List<String>> lccVsBookingClassesMap = segmentVsSelectedBookingClassesMap.get(segmentCode);
			int j = 0;
			for (String logicalCabinClass : lccVsBookingClassesMap.keySet()) {
				List<String> bookingCodes = lccVsBookingClassesMap.get(logicalCabinClass);
				if (j != 0) {
					hqlBuffer.append(" OR ");
				}
				hqlBuffer.append(" ( FSBI.logicalCCCode = '").append(logicalCabinClass).append("' AND ")
						.append(prepareSqlINString("FSBI.bookingCode", bookingCodes, " ) "));
				j++;
			}
			hqlBuffer.append(" ) ) ");
		}

		hqlBuffer.append(" ) AND BC.cabinClassCode ='" + cabinClassCode + "' AND BC.paxType <> '"
				+ BookingClass.PassengerType.GOSHOW + "' ");

		hqlBuffer.append(" order by FSBI.segmentCode");

		Collection<Object[]> sourceSegBCInventories = getSession().createQuery(hqlBuffer.toString()).list();

		// index the result by the segment code
		Map<String, Map<String, List<Object[]>>> sourceSegBCInventoriesMap = new HashMap<String, Map<String, List<Object[]>>>();

		if (sourceSegBCInventories != null) {
			Iterator<Object[]> sourceSegBCInventoriesIt = sourceSegBCInventories.iterator();
			while (sourceSegBCInventoriesIt.hasNext()) {
				Object[] sourceSegBCInventory = sourceSegBCInventoriesIt.next();
				String segmentCode = sourceSegBCInventory[0].toString();
				String logicalCabinClassCode = sourceSegBCInventory[10].toString();
				if (sourceSegBCInventoriesMap.get(segmentCode) == null) {
					sourceSegBCInventoriesMap.put(segmentCode, new HashMap<String, List<Object[]>>());
				}
				if (sourceSegBCInventoriesMap.get(segmentCode).get(logicalCabinClassCode) == null) {
					sourceSegBCInventoriesMap.get(segmentCode).put(logicalCabinClassCode, new ArrayList<Object[]>());
				}
				sourceSegBCInventoriesMap.get(segmentCode).get(logicalCabinClassCode).add(sourceSegBCInventory);
			}
		}
		return sourceSegBCInventoriesMap;
	}

	@Override
	public int getFCCSegBCInvetoriesCount(int flightId) {
		String hql = "SELECT count(distinct FSBI.fccsbInvId) FROM FCCInventory FI, FCCSegBCInventory FSBI"
				+ " WHERE FI.flightId=FSBI.flightId AND FI.flightId=?";
		return ((Long) getSession().createQuery(hql).setInteger(0, flightId).uniqueResult()).intValue();
	}

	/**
	 * Returns the inventory details of given flights segments
	 * 
	 * @param flightIds
	 * @return
	 */
	@Override
	public Collection<FlightInventorySummaryDTO> getFlightSegmentInventoriesSummary(Collection<Integer> flightIds) {
		String hql = " SELECT FSI.flightId, FSI.segmentCode, SUM(FSI.seatsSold), SUM(FSI.soldInfantSeats), "
				+ " SUM(FSI.onHoldSeats), SUM(FSI.onholdInfantSeats), FSI.flightSegId FROM FCCSegInventory as FSI "
				+ " WHERE FSI.flightId IN (:flightIds) GROUP BY FSI.flightId, FSI.segmentCode, FSI.flightSegId ";
		@SuppressWarnings("unchecked")
		List<Object[]> flightSegmentList = getSession().createQuery(hql).setParameterList("flightIds", flightIds).list();
		HashMap<Integer, List<FCCSegInvSummaryDTO>> flightSegmentInventoriesSummaryMap = new HashMap<Integer, List<FCCSegInvSummaryDTO>>();
		List<FlightInventorySummaryDTO> flightSegmentInventoriesSummary = new ArrayList<FlightInventorySummaryDTO>();
		List<FCCSegInvSummaryDTO> segments = new ArrayList<FCCSegInvSummaryDTO>();
		Integer flightId;
		if (flightSegmentList != null) {
			Iterator<Object[]> flightSegmentListIt = flightSegmentList.iterator();
			while (flightSegmentListIt.hasNext()) {
				Object[] result = flightSegmentListIt.next();
				if (result != null) {
					flightId = (Integer) result[0];
					FCCSegInvSummaryDTO fltSegInvSummaryDTO = new FCCSegInvSummaryDTO((String) result[1],
							((Long) result[2]).intValue(), ((Long) result[3]).intValue(), ((Long) result[4]).intValue(),
							((Long) result[5]).intValue(), ((Integer) result[6]).intValue());
					if ((flightId != null) && (fltSegInvSummaryDTO != null)) {
						segments = flightSegmentInventoriesSummaryMap.get(flightId);
						if (segments == null) {
							segments = new ArrayList<FCCSegInvSummaryDTO>();
						}
						segments.add(fltSegInvSummaryDTO);
						flightSegmentInventoriesSummaryMap.put(flightId, segments);
					}
				}
			}
			Iterator<Integer> resultIt = flightSegmentInventoriesSummaryMap.keySet().iterator();
			while (resultIt.hasNext()) {
				flightId = resultIt.next();
				if (flightId != null) {
					FlightInventorySummaryDTO flgInvSummDTO = new FlightInventorySummaryDTO();
					flgInvSummDTO.setFlightId(flightId.intValue());
					flgInvSummDTO.setSegmentInventories(flightSegmentInventoriesSummaryMap.get(flightId));
					flightSegmentInventoriesSummary.add(flgInvSummDTO);
				}
			}
		}
		return flightSegmentInventoriesSummary;
	}

	// ------------------------------------------------------------------
	// Methods for manipulating bc inventory nesting info table
	// ------------------------------------------------------------------
	@Override
	public void saveBCInvNestingInfo(Collection<FCCSegBCInvNesting> fccSegBCInvNestings) {
		if (log.isDebugEnabled()) {
			for (FCCSegBCInvNesting fccSegBCInvNesting : fccSegBCInvNestings) {
				log.debug("Saving nesting information [from fccsbaId=" + fccSegBCInvNesting.getFromFCCSegBCInvId()
						+ ", to fccsbaId=" + fccSegBCInvNesting.getToFCCSegBCInvId() + ", onhold="
						+ fccSegBCInvNesting.getOnholdSeats() + ", sold=" + fccSegBCInvNesting.getSoldSeats() + "]");
			}
		}
		hibernateSaveOrUpdateAll(fccSegBCInvNestings);

		if (invLog.isDebugEnabled()) {
			for (FCCSegBCInvNesting fccSegBCInvNesting : fccSegBCInvNestings) {
				invLog.debug("updateBCInvNestingInfo::fccsbn:" + fccSegBCInvNesting.getFccsbInvNestingId() + ",from_fccsba:"
						+ fccSegBCInvNesting.getFromFCCSegBCInvId() + ",to_fccsba:" + fccSegBCInvNesting.getToFCCSegBCInvId()
						+ ",sold:" + fccSegBCInvNesting.getSoldSeats() + ",onhold:" + fccSegBCInvNesting.getOnholdSeats());
			}
		}
	}

	@Override
	public int updateBCInvNestingInfo(Integer fccsbInvNestingId, int onholdSeats, int soldSeats) {
		if (log.isDebugEnabled()) {
			log.debug("Updating(+/-) nesting information [Nest Rec Id=" + fccsbInvNestingId + ", onhold=" + onholdSeats
					+ ", sold=" + soldSeats + "]");
		}

		int result = 0;
		if (onholdSeats != 0 && soldSeats != 0) {
			String hqlUpdate = "UPDATE FCCSegBCInvNesting SET soldSeats = (soldSeats + ? ), onholdSeats= (onholdSeats + ?) "
					+ "WHERE fccsbInvNestingId = ? AND (soldSeats + ? ) >= 0 AND (onholdSeats + ? ) >= 0 ";
			result = getSession().createQuery(hqlUpdate).setInteger(0, soldSeats).setInteger(1, onholdSeats)
					.setInteger(2, fccsbInvNestingId.intValue()).setInteger(3, soldSeats).setInteger(4, onholdSeats)
					.executeUpdate();
		} else if (onholdSeats != 0) {
			String hqlUpdate = "UPDATE FCCSegBCInvNesting SET  onholdSeats= (onholdSeats + ?) "
					+ "WHERE fccsbInvNestingId = ? AND  (onholdSeats + ? ) >= 0 ";
			result = getSession().createQuery(hqlUpdate).setInteger(0, onholdSeats).setInteger(1, fccsbInvNestingId.intValue())
					.setInteger(2, onholdSeats).executeUpdate();
		} else if (soldSeats != 0) {
			String hqlUpdate = "UPDATE FCCSegBCInvNesting SET soldSeats = (soldSeats + ? ) "
					+ "WHERE fccsbInvNestingId = ? AND (soldSeats + ? ) >= 0  ";
			result = getSession().createQuery(hqlUpdate).setInteger(0, soldSeats).setInteger(1, fccsbInvNestingId.intValue())
					.setInteger(2, soldSeats).executeUpdate();
		}
		if (invLog.isDebugEnabled()) {
			invLog.debug("updateBCInvNestingInfo::fccsbn:" + fccsbInvNestingId + ",sold:" + soldSeats + ",onhold:" + onholdSeats
					+ ",result:" + result);
		}
		return result;
	}

	public int updateBCInvNestingToNonTemporary(Integer fccsbInvNestingId) {

		String hqlUpdate = "UPDATE FCCSegBCInvNesting SET temporary =  ?  " + " WHERE fccsbInvNestingId = ? ";
		int result = getSession().createQuery(hqlUpdate).setString(0, "N").setInteger(1, fccsbInvNestingId.intValue())
				.executeUpdate();
		if (invLog.isDebugEnabled()) {
			invLog.debug("updateBCInvNestingToNonTemporary::fccsbn:" + fccsbInvNestingId + ",result:" + result);
		}
		return result;
	}

	/**
	 * Updating sold/onhold on nested seats
	 * 
	 * @param fromFccsbInvId
	 * @param toFccsbInvId
	 * @param onholdSeats
	 * @param soldSeats
	 */
	public void saveBCInvNestingInfo(Integer fromFccsbInvId, Integer toFccsbInvId, int onholdSeats, int soldSeats) {
		if (log.isDebugEnabled()) {
			log.debug("Saving nesting information [from fccsbaId=" + fromFccsbInvId + ", to fccsbaId=" + toFccsbInvId
					+ ", onhold=" + onholdSeats + ", sold=" + soldSeats + "]");
		}
		FCCSegBCInvNesting fccSegBCInvNesting = new FCCSegBCInvNesting();
		fccSegBCInvNesting.setFromFCCSegBCInvId(fromFccsbInvId);
		fccSegBCInvNesting.setToFCCSegBCInvId(toFccsbInvId);
		fccSegBCInvNesting.setOnholdSeats(onholdSeats);
		fccSegBCInvNesting.setSoldSeats(soldSeats);

		hibernateSave(fccSegBCInvNesting);
		if (invLog.isDebugEnabled()) {
			invLog.debug("saveBCInvNestingInfo::fccsbn:" + fccSegBCInvNesting.getFccsbInvNestingId() + ",from_fccsba:"
					+ fccSegBCInvNesting.getFromFCCSegBCInvId() + ",to_fccsba:" + fccSegBCInvNesting.getToFCCSegBCInvId()
					+ ",sold:" + fccSegBCInvNesting.getSoldSeats() + ",onhold:" + fccSegBCInvNesting.getOnholdSeats());
		}
	}

	@Override
	public int deleteBCInvNestingInfo(Integer fccsbInvNestingId) {
		String hql = "DELETE FCCSegBCInvNesting WHERE fccsbInvNestingId = ?";
		int result = getSession().createQuery(hql).setInteger(0, fccsbInvNestingId.intValue()).executeUpdate();
		if (invLog.isDebugEnabled()) {
			invLog.debug("deleteBCInvNestingInfo::fccsbn:" + fccsbInvNestingId + ",result:" + result);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegBCInvNesting> getNestedSeats(int tofccsbInvId, boolean includeSoldNestedSeats,
			boolean orderNestRankDecending) {
		String hql = "SELECT FSBIN FROM FCCSegBCInvNesting FSBIN, FCCSegBCInventory FSBI, BookingClass BC"
				+ " WHERE FSBIN.fromFCCSegBCInvId = FSBI.fccsbInvId AND FSBI.bookingCode = BC.bookingCode"
				+ " AND FSBIN.temporary = 'N' AND FSBIN.toFCCSegBCInvId = ?";
		if (includeSoldNestedSeats) {
			hql += " AND (FSBIN.onholdSeats > 0 OR FSBIN.soldSeats > 0)";
		} else {
			hql += " AND FSBIN.onholdSeats > 0";
		}
		if (orderNestRankDecending) {
			hql += " ORDER BY BC.nestRank DESC";
		} else {
			hql += " ORDER BY BC.nestRank ASC";
		}

		return getSession().createQuery(hql).setInteger(0, tofccsbInvId).list();
	}

	/**
	 * 0 - nested out sold seats count 1 - nested out onhold seats + BLOCKED SEATS(NOT NESTED OUT) count
	 * 
	 * @param fromfccsbInvId
	 * @return
	 */
	@Override
	public int[] getNestedOutOrBlockedSeatCounts(int fromfccsbInvId) {
		int[] nestedOutOrBlockedSeats = { 0, 0 };
		String hql = "SELECT sum(FSBIN.soldSeats), sum(FSBIN.onholdSeats) FROM FCCSegBCInvNesting FSBIN "
				+ " WHERE FSBIN.fromFCCSegBCInvId=?";
		@SuppressWarnings("unchecked")
		List<Object[]> data = getSession().createQuery(hql).setInteger(0, fromfccsbInvId).list();
		if (data != null && data.size() == 1) {
			Object[] dataArr = data.get(0);
			if (dataArr[0] != null) {
				nestedOutOrBlockedSeats[0] = ((Long) dataArr[0]).intValue();
			}
			if (dataArr[1] != null) {
				nestedOutOrBlockedSeats[1] = ((Long) dataArr[1]).intValue();
			}
		}

		// Get Not Nested Out Blocked Seats
		String hqlBlockedSeats = "SELECT sum(T.seatsBlocked) FROM TempSegBcAlloc T WHERE T.fccsbaId=? AND "
				+ "T.nestRecordId is null";
		@SuppressWarnings("unchecked")
		List<Object[]> blockedData = getSession().createQuery(hqlBlockedSeats).setInteger(0, fromfccsbInvId).list();
		if (blockedData != null && blockedData.size() == 1) {
			Object objOnholdBlockedSeats = blockedData.get(0);
			if (objOnholdBlockedSeats != null) {
				nestedOutOrBlockedSeats[1] = nestedOutOrBlockedSeats[1] + ((Long) objOnholdBlockedSeats).intValue();
			}
		}

		return nestedOutOrBlockedSeats;
	}

	// ---------------------------------------------------------------------------------------------
	// ------------------------ Reservations related functionalities--------------------------------
	// ---------------------------------------------------------------------------------------------
	/**
	 * method to update fcc seg inventorys
	 */
	@Override
	public int updateFccSegInventorySoldOnholdSeats(int flightId, String logicalCabinClassCode, String segmentCode, int soldSeats,
			int onholdSeats, int soldInfantSeats, int onholdInfantSeats, boolean enforceFixedSeatsCheck,
			boolean enforceAdultSeatsCheck, boolean enforceInfantSeatCheck, int releasedWLSeats) {
		// TODO: need to make following two queries atomic

		int fixedSeats = 0;
		if (enforceFixedSeatsCheck && enforceAdultSeatsCheck) {
			String hqlFixedAvailable = "SELECT SUM(FSBI.seatsAvailable) FROM FCCSegBCInventory FSBI, BookingClass BC WHERE "
					+ "FSBI.bookingCode = BC.bookingCode AND BC.fixedFlagChar='" + BookingClass.FIXED_FLAG_Y + "' AND "
					+ "BC.bcType NOT IN (" + Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) + ") AND "
					+ "FSBI.flightId = :flightId AND FSBI.segmentCode = :segmentCode AND FSBI.logicalCCCode = :logicalCCCode "
					+ " AND FSBI.status = 'OPN'";
			Long fixedAvailable = (Long) getSession().createQuery(hqlFixedAvailable).setInteger("flightId", flightId)
					.setString("segmentCode", segmentCode).setString("logicalCCCode", logicalCabinClassCode).uniqueResult();

			if (fixedAvailable != null) {
				fixedSeats = fixedAvailable.intValue();
			}
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "UPDATE t_fcc_seg_alloc fccsa SET sold_seats = sold_seats + " + soldSeats + ",on_hold_seats=on_hold_seats+"
				+ onholdSeats + ",available_seats=greatest(available_seats - " + (soldSeats + onholdSeats)
				+ ",0),available_infant_seats=available_infant_seats - " + (soldInfantSeats + onholdInfantSeats) + ","
				+ "onhold_infant_seats=onhold_infant_seats+" + onholdInfantSeats + ", sold_infant_seats=sold_infant_seats+"
				+ soldInfantSeats + ",waitlisted_seats=waitlisted_seats-" + releasedWLSeats
				+ ", version=version+1 WHERE fccsa.flight_id=" + flightId + " AND fccsa.segment_code='" + segmentCode
				+ "' AND logical_cabin_class_code = '" + logicalCabinClassCode + "'";

		if (enforceFixedSeatsCheck && enforceAdultSeatsCheck) {
			sql += " AND (available_seats - " + (soldSeats + onholdSeats) + ") >= " + fixedSeats;
		}
		if (enforceInfantSeatCheck) {
			sql += " AND (available_infant_seats - " + (soldInfantSeats + onholdInfantSeats) + ") >= 0";
		}

		int updateCount = jdbcTemplate.update(sql);
		// [TODO:verify] Is flush() really required?
		getSession().flush();
		return updateCount;
	}

	@Override
	public int updateFccSegInventoryWaitListSeats(int flightId, String logicalCabinClassCode, String segmentCode,
			int waitListedSeats) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "UPDATE t_fcc_seg_alloc fccsa SET waitlisted_seats = waitlisted_seats + " + waitListedSeats
				+ ", version=version+1 WHERE fccsa.flight_id=" + flightId + " AND fccsa.segment_code='" + segmentCode
				+ "' AND logical_cabin_class_code = '" + logicalCabinClassCode + "'";

		int updateCount = jdbcTemplate.update(sql);
		// [TODO:verify] Is flush() really required?
		getSession().flush();
		return updateCount;
	}

	/**
	 * Update sold/onhold, status in FCC Segment BookingClass inventory
	 * 
	 * @return number of recordes updated 0 - update failed 1 - updated sold/onhold 2 - updated sold/onhold and updated
	 *         status to closed
	 */
	@Override
	public UpdateFccSegBCInvResponse updateFccSegBCInventory(Integer fccsbInvId, int onholdSeats, int soldSeats,
			boolean checkStatus, Collection<TempSegBCInvClosure> bucketsToReopen, boolean ignoreAvailability, boolean isOverbook,
			int releasedWLSeats, String userId) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET sold_seats = sold_seats + " + soldSeats
				+ ",waitlisted_seats = greatest(waitlisted_seats-" + releasedWLSeats + ", 0) ,onhold_seats = onhold_seats + "
				+ onholdSeats + ",version = version + 1";

		if (ignoreAvailability) {
			// sqlUpdate += ",allocated_seats = allocated_seats + " + (soldSeats + onholdSeats) +
			// ", available_seats = 0";
			// from above statement available seat is always updated to 0 (block_seat -> CNF) this leads to an inventory
			// (OPENRT BC)
			// miss-match when a block seat operation follows a release block seat operation
			sqlUpdate += ",allocated_seats = allocated_seats + " + (soldSeats + onholdSeats) + " ";
		} else if (soldSeats + onholdSeats != 0) {
			// Only if total affected seat count != 0
			sqlUpdate += ",available_seats = greatest(allocated_seats + (acquired_seats - cancelled_seats) - (sold_seats + onhold_seats + "
					+ (soldSeats + onholdSeats) + "),0)";
		}
		sqlUpdate += " WHERE fccsba_id=" + fccsbInvId;

		if (!isOverbook && !ignoreAvailability) {
			sqlUpdate += " AND available_seats - " + (soldSeats + onholdSeats) + " >= 0 ";
		}
		if (!isOverbook && checkStatus && releasedWLSeats == 0) {
			// to avoid selling seats from a bucket which is closed after fare quote
			sqlUpdate += " AND status ='" + FCCSegBCInventory.Status.OPEN + "' ";
		}

		sqlUpdate += " AND sold_seats + " + soldSeats + " >= 0 AND onhold_seats + " + onholdSeats + ">=0 ";

		int updateCount = jdbcTemplate.update(sqlUpdate);

		// when canceling blocked seats, re-open buckets closed due to seat blocking if required
		if (bucketsToReopen != null && bucketsToReopen.size() > 0) {
			StringBuffer hqlReopen = new StringBuffer("UPDATE FCCSegBCInventory SET status = :status,")
					.append(" statusChangeAction = :statusChangeAction, version = version + 1")
					.append(" WHERE fccsbInvId = :fccsbInvId ");
			Iterator<TempSegBCInvClosure> bucketsToReopenIt = bucketsToReopen.iterator();
			while (bucketsToReopenIt.hasNext()) {
				TempSegBCInvClosure bucket = bucketsToReopenIt.next();
				getSession().createQuery(hqlReopen.toString()).setString("status", FCCSegBCInventory.Status.OPEN)
						.setString("statusChangeAction", bucket.getPreviousStatusAction())
						.setInteger("fccsbInvId", bucket.getFccsbaId()).executeUpdate();
				log.warn("Reopening auto closed bucket due to block seat cnx [source fccsbInvId=" + fccsbInvId
						+ ",target fccsbInvId=" + bucket.getFccsbaId() + "]");
			}
		}
		UpdateFccSegBCInvResponse response = new UpdateFccSegBCInvResponse();
		FCCSegBCInventory fccSegBCInventory = getFCCSegBCInventory(fccsbInvId);
		if (updateCount != 0 && (onholdSeats + soldSeats) != 0) {
			response = checkAndExecuteBCClosure(fccSegBCInventory);
		}
		if (fccSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED)) {
			Set<FCCSegBCInventory> updatedFccBCInventory = new HashSet<FCCSegBCInventory>();
			updatedFccBCInventory.add(fccSegBCInventory);
			FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO = new FCCSegInventoryUpdateDTO();
			fccSegInventoryUpdateDTO.setFlightId(fccSegBCInventory.getFlightId());
			fccSegInventoryUpdateDTO.setSegmentCode(fccSegBCInventory.getSegmentCode());
			fccSegInventoryUpdateDTO.setUpdatedFCCSegBCInventories(updatedFccBCInventory);
			try {
				AuditAirinventory.addAuditForBCClosure(fccSegInventoryUpdateDTO, userId != null ? userId : "");
			} catch (ModuleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// [TODO:verify] Is session flush() really needed?
		getSession().flush();

		response.setUpdateCount(updateCount);
		if (invLog.isDebugEnabled()) {
			invLog.debug("updateFccSegBCInventory::fccsba:" + fccsbInvId + ",sold:" + soldSeats + ",onhold:" + onholdSeats
					+ ",result:" + updateCount);
		}
		return response;
	}

	private UpdateFccSegBCInvResponse checkAndExecuteBCClosure(Integer fccsbInvId) {
		FCCSegBCInventory fccSegBCInventory = getFCCSegBCInventory(fccsbInvId);
		return checkAndExecuteBCClosure(fccSegBCInventory);
	}

	private UpdateFccSegBCInvResponse checkAndExecuteBCClosure(FCCSegBCInventory fccSegBCInventory) {

		Integer fccsbInvId = fccSegBCInventory.getFccsbInvId();
		UpdateFccSegBCInvResponse response = new UpdateFccSegBCInvResponse();
		// as this method is also used for transferring on-hold to sold
		// update bc inventory status, if availability is zero
		int noRecsUpdated = 0;
		boolean isCloseBC = isAutoCloseValidForBC(fccsbInvId);

		if (isCloseBC) {
			StringBuffer hqlUpdateStatus = new StringBuffer(" UPDATE FCCSegBCInventory SET status = '")
					.append(FCCSegBCInventory.Status.CLOSED).append("', statusChangeAction = '")
					.append(FCCSegBCInventory.StatusAction.AVAILABILITY_ZERO).append("', version = version + 1")
					.append(" WHERE seatsAvailable = 0 AND fccsbInvId=").append(fccsbInvId);
			noRecsUpdated = getSession().createQuery(hqlUpdateStatus.toString()).executeUpdate();
		}

		if (noRecsUpdated > 0 && getAirInventoryConfig().isCloseAllLowerRankedStandardBCsWhenClosingHigherBC()) {
			closeAllLowerRankStandardAllocations(fccSegBCInventory, response);
		}

		// Close all higher group allocatoins
		if (noRecsUpdated > 0 && AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			updateAllHigherGroupAllocations(fccSegBCInventory, response, true, null);
		}

		if (noRecsUpdated != 0 && (getAirInventoryConfig().isApplyConnectionBucketsClosure()
				|| getAirInventoryConfig().isApplyReturnBucketsClosure())) {
			// close any connection/return standard bucket eligible

			/**
			 * For each open connection bucket (not manually open) If all the standard buckets of lower nest rank are
			 * closed and there is atleast higher nest ranked open standard bucket then close the connection bucket
			 */
			FCCSegBCInventoryCollection bucketsCollection = getFlightInventoryJDBCDAO().getBucketsCollection(fccsbInvId);

			String sourceBookingCode = fccSegBCInventory.getBookingCode();
			String bookingClassType = getBookingClassDAO().getBookingClassType(sourceBookingCode);
			boolean isOpenReturnBC = false;

			// This is to omit return bucket closure logic while closing an open return booking class AARESAA-12884
			if (sourceBookingCode != null && sourceBookingCode != "" && bookingClassType != null && bookingClassType != "") {
				isOpenReturnBC = bookingClassType.equals(BookingClass.BookingClassType.OPEN_RETURN);
			} else {
				log.warn("No booking bc_type or booking_code has been defined for fccsbInvId : " + fccsbInvId);
			}
			if (bucketsCollection != null && !isOpenReturnBC) {
				Collection<FCCSegBCInventoryTO> bucketsEligbleForClose = bucketsCollection.getBucketsEligbleForClosing();
				if (bucketsEligbleForClose != null && bucketsEligbleForClose.size() > 0) {
					// close eligible con/ret buckets
					StringBuffer hql = new StringBuffer(" UPDATE FCCSegBCInventory SET status = '")
							.append(FCCSegBCInventory.Status.CLOSED).append("', statusChangeAction = '")
							.append(FCCSegBCInventory.StatusAction.SEG_BUCKETS_CLOSURE).append("',")
							.append(" version = version + 1 WHERE fccsbInvId IN ( :fccsbInvIdsList ) ");

					ArrayList<Integer> fccbInvIdsList = new ArrayList<Integer>();
					StringBuilder bcsToSkip = null;
					int count = 0;
					Iterator<FCCSegBCInventoryTO> bucketsEligbleForCloseIt = bucketsEligbleForClose.iterator();
					while (bucketsEligbleForCloseIt.hasNext()) {
						Integer bcInvId = new Integer((bucketsEligbleForCloseIt.next()).getFccsbInvId());
						fccbInvIdsList.add(bcInvId);
						FCCSegBCInventory fccSBCInv = getFCCSegBCInventory(bcInvId);

						// Close all higher group allocatoins
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							updateAllHigherGroupAllocations(fccSBCInv, response, true, bcsToSkip);
							if (count == 0) {
								bcsToSkip = new StringBuilder();
								bcsToSkip.append("'").append(fccSBCInv.getBookingCode()).append("'");
							} else {
								bcsToSkip.append(",").append("'").append(fccSBCInv.getBookingCode()).append("'");
							}
							count++;
						}

						log.warn("Applying auto bucket closure [source fccsbInvId=" + fccsbInvId + ",target fccsbInvId=" + bcInvId
								+ "]");
					}
					getSession().createQuery(hql.toString()).setParameterList("fccsbInvIdsList", fccbInvIdsList).executeUpdate();
					// setting the response
					response.setFccsbInvId(fccsbInvId);
					if (response.getClosedFccsbInvs() == null) {
						response.setClosedFccsbInvs(bucketsEligbleForClose);
					} else {
						bucketsEligbleForClose.addAll(response.getClosedFccsbInvs());
						response.setClosedFccsbInvs(bucketsEligbleForClose);
					}

				}
			}
		}
		return response;
	}

	@Override
	public UpdateFccSegBCInvResponse updateFccSegBCInventoryForWaitListing(Integer fccsbInvId, int waitListedSeats,
			boolean ignoreAvailability) {

		UpdateFccSegBCInvResponse response = new UpdateFccSegBCInvResponse();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET waitlisted_seats = waitlisted_seats + " + waitListedSeats
				+ ",version = version + 1";

		sqlUpdate += " WHERE fccsba_id=" + fccsbInvId;

		if (!ignoreAvailability) {
			sqlUpdate += " AND allocated_waitlist_seats - " + (waitListedSeats) + " >= 0 ";
		}

		int updateCount = jdbcTemplate.update(sqlUpdate);

		// [TODO:verify] Is session flush() really needed?
		getSession().flush();

		response.setUpdateCount(updateCount);
		return response;
	}

	@Override
	public void reprotectSegInventoryInfantSeats(int fccSegInvId, int soldSeats, int onHoldSeats, int allocationChange) {

		String hqlUpdate = "update FCCSegInventory set soldInfantSeats = soldInfantSeats + :soldSeats, "
				+ "onholdInfantSeats = onholdInfantSeats + :onHoldSeats, "
				+ "infantAllocation = infantAllocation + :allocationChange where fccsInvId = " + fccSegInvId;
		getSession().createQuery(hqlUpdate).setInteger("soldSeats", soldSeats).setInteger("onHoldSeats", onHoldSeats)
				.setInteger("allocationChange", allocationChange).executeUpdate();
	}

	@Override
	public void reprotectBCInventorySeats(int soldSeats, int onHoldSeats, int segBCInvId, int allocationChange,
			int availableSeats) {

		String hqlUpdate = "update FCCSegBCInventory set seatsSold = seatsSold + :soldSeats, "
				+ "onHoldSeats = onHoldSeats + :onHoldSeats, seatsAllocated = seatsAllocated + :allocationChange,  "
				+ "seatsAvailable = seatsAvailable + :allocationChange - :soldSeats - :onHoldSeats, version = version + 1";

		hqlUpdate += " where fccsbInvId = " + segBCInvId;
		int updateCount = getSession().createQuery(hqlUpdate).setInteger("soldSeats", soldSeats)
				.setInteger("onHoldSeats", onHoldSeats).setInteger("allocationChange", allocationChange).executeUpdate();

		if (updateCount > 0 && (soldSeats + onHoldSeats) > 0) {
			checkAndExecuteBCClosure(segBCInvId);
		}
		if (invLog.isDebugEnabled()) {
			invLog.debug("reprotectBCInventorySeats::fccsba:" + segBCInvId + ",sold:" + soldSeats + ",onhold:" + onHoldSeats);
		}
	}

	// TODO improve the implementation reducing database queries
	@Override
	public void releaseBCInventorySeats(List<String> releaseCases, int soldSeats, int onHoldSeats, int releasedWLSeats,
			ExtendedFCCSegBCInventory extendedFCCSegBCInventory) {
		FCCSegBCInventory segBCInv = extendedFCCSegBCInventory.getFccSegBCInventory();
		int segBCInvId = segBCInv.getFccsbInvId().intValue();
		int segInvId = segBCInv.getfccsInvId();
		String logicalCCCode = segBCInv.getLogicalCCCode();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		int iterator = 0;

		for (String releaseCase : releaseCases) {
			boolean needUpdateStatus = true;
			if (releaseCase.equals("Case1")) {
				// Case 1 is selected
				// On the same bucket (Case1)
				// --sold, ++acquired, ++cancelled, ++available onHoldSeats
				// if wait listed reservation are there, they will be confirmed as on-hold
				String sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET sold_seats = sold_seats - " + soldSeats
						+ ",onhold_seats = onhold_seats - " + onHoldSeats + ", acquired_seats = acquired_seats+ "
						+ (soldSeats + onHoldSeats) + ", cancelled_seats = cancelled_seats+" + (soldSeats + onHoldSeats)
						+ ", available_seats = greatest(allocated_seats + (acquired_seats - cancelled_seats) - "
						+ "(sold_seats + onhold_seats - " + (soldSeats + onHoldSeats)
						+ "),0), waitlisted_seats = waitlisted_seats - " + releasedWLSeats
						+ " ,version = version + 1 WHERE fccsba_id=" + segBCInvId;

				jdbcTemplate.update(sqlUpdate);
				// if bucket is closed and availability > 0 and not manually closed, then re-open the bucket
				reOpenFCCSegBCInventory(segBCInvId, null);

			} else if (releaseCase.equals("Case2A")) {
				// Case 2A is selected
				// On the same bucket (Case2A)
				// --sold, ++cancelled
				String sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET sold_seats = sold_seats - " + soldSeats
						+ ", onhold_seats = onhold_seats - " + onHoldSeats + ", cancelled_seats = cancelled_seats+"
						+ (soldSeats + onHoldSeats) + ", waitlisted_seats = waitlisted_seats - " + releasedWLSeats
						+ " ,version = version + 1 WHERE fccsba_id=" + segBCInvId;

				jdbcTemplate.update(sqlUpdate);
				// If reservation canceled by an over-booking reservation, we should not release additional seats to
				// next booking class
				String overbookCountSql = "select (allocated_seats + acquired_seats - cancelled_seats)"
						+ "- (sold_seats + onhold_seats) as OVERBOOK from T_FCC_SEG_BC_ALLOC where fccsba_id=" + segBCInvId;
				Integer overbookCount = (Integer) jdbcTemplate.query(overbookCountSql, new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						if (resultSet != null) {
							while (resultSet.next()) {
								return resultSet.getInt("OVERBOOK") * -1;
							}
						}
						return null;
					}
				});

				if (overbookCount != null && overbookCount.intValue() > 0) {
					if (invLog.isDebugEnabled()) {
						invLog.debug("releaseBCInventorySeats/overbookadjust::fccsba:" + segBCInvId + ",obadj:-" + overbookCount);
					}
					sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET acquired_seats = acquired_seats + " + overbookCount
							+ ", version = version + 1 WHERE fccsba_id=" + segBCInvId;
					jdbcTemplate.update(sqlUpdate);
					// we need to propagate correct acquire amount (=cnx-acq-alloc) to the next booking class
					if (soldSeats > 0) {
						soldSeats -= overbookCount;
					} else if (onHoldSeats > 0) {
						onHoldSeats -= overbookCount;
					}
				}

				if (onHoldSeats < 0 || soldSeats < 0) {
					break;
				}
			} else if (releaseCase.equals("Case2B")) {// this case is not being used now !
				needUpdateStatus = true;
				// Case 2B is selected
				// On the open (lowest nest ranked?) standard bucket (Case2B)
				// ++acquired, ++available

				String hqlSelect = "select i.fccsbInvId from FCCSegBCInventory i, BookingClass b "
						+ "where i.bookingCode = b.bookingCode and i.fccsInvId = " + segInvId + " and b.nestRank = "
						+ "		( select min(BC.nestRank) from FCCSegBCInventory IB, BookingClass BC "
						+ "where IB.bookingCode = BC.bookingCode and IB.status = '" + FCCSegBCInventory.Status.OPEN
						+ "' and IB.fccsInvId = " + segInvId + " AND BC.bcType = '" + extendedFCCSegBCInventory.getBcType()
						+ "' AND BC.allocationType='" + extendedFCCSegBCInventory.getAllocationType()
						+ "' AND BC.paxCategoryCode='" + extendedFCCSegBCInventory.getPaxCategoryType()
						+ "' AND BC.fareCategoryCode='" + extendedFCCSegBCInventory.getFareCategoryTypeCode()
						+ "' AND BC.standardCodeChar = '" + BookingClass.STANDARD_CODE_Y + "' AND BC.logicalCCCode = '"
						+ logicalCCCode + "') ";

				Integer fCCSegBCInvId = (Integer) getSession().createQuery(hqlSelect).uniqueResult();

				if (fCCSegBCInvId == null) {
					// no open standard buckets; pick standard highest nest ranked PRIORITY bucket
					hqlSelect = "select i.fccsbInvId from FCCSegBCInventory i, BookingClass b "
							+ "where i.bookingCode = b.bookingCode and i.fccsInvId = " + segInvId
							+ "	and b.nestRank = ( select max(BC.nestRank) from FCCSegBCInventory IB, BookingClass BC "
							+ "where IB.bookingCode = BC.bookingCode and IB.fccsInvId = " + segInvId
							+ " AND IB.priorityFlagChar = '" + FCCSegBCInventory.PRIORITY_FLAG_Y + "' AND BC.bcType = '"
							+ extendedFCCSegBCInventory.getBcType() + "' AND BC.allocationType='"
							+ extendedFCCSegBCInventory.getAllocationType() + "' AND BC.paxCategoryCode='"
							+ extendedFCCSegBCInventory.getPaxCategoryType() + "' AND BC.fareCategoryCode='"
							+ extendedFCCSegBCInventory.getFareCategoryTypeCode() + "' AND BC.standardCodeChar = '"
							+ BookingClass.STANDARD_CODE_Y + "' AND BC.logicalCCCode = '" + logicalCCCode + "') ";

					fCCSegBCInvId = (Integer) getSession().createQuery(hqlSelect).uniqueResult();
				}

				if (fCCSegBCInvId == null) {
					// no standard bucket found; pick open, non-standard, priority, highest allocated bucket
					hqlSelect = "SELECT i.fccsbInvId FROM FCCSegBCInventory i, BookingClass b"
							+ " WHERE i.bookingCode = b.bookingCode AND b.standardCodeChar = '" + BookingClass.STANDARD_CODE_N
							+ "' AND b.fixedFlagChar = '" + BookingClass.FIXED_FLAG_N + "' AND b.bcType = '"
							+ extendedFCCSegBCInventory.getBcType() + "' AND b.allocationType='"
							+ extendedFCCSegBCInventory.getAllocationType() + "' AND b.paxCategoryCode='"
							+ extendedFCCSegBCInventory.getPaxCategoryType() + "' AND b.fareCategoryCode='"
							+ extendedFCCSegBCInventory.getFareCategoryTypeCode() + "' AND i.fccsInvId = " + segInvId
							+ " AND i.priorityFlagChar = '" + FCCSegBCInventory.PRIORITY_FLAG_Y + "' AND i.status = '"
							+ FCCSegBCInventory.Status.OPEN + "' AND b.logicalCCCode = '" + logicalCCCode
							+ "' ORDER BY i.seatsAllocated DESC";

					@SuppressWarnings("unchecked")
					List<Object> fccSegBCInvIds = getSession().createQuery(hqlSelect).list();

					if (fccSegBCInvIds != null && fccSegBCInvIds.size() > 0) {
						fCCSegBCInvId = (Integer) fccSegBCInvIds.get(0);
					}
				}

				if (fCCSegBCInvId == null) {
					// pick non-standard open bucket
					hqlSelect = "SELECT i.fccsbInvId FROM FCCSegBCInventory i, BookingClass b"
							+ " WHERE i.bookingCode = b.bookingCode AND b.standardCodeChar = '" + BookingClass.STANDARD_CODE_N
							+ "' AND b.fixedFlagChar = '" + BookingClass.FIXED_FLAG_N + "' AND b.bcType = '"
							+ extendedFCCSegBCInventory.getBcType() + "' AND b.allocationType='"
							+ extendedFCCSegBCInventory.getAllocationType() + "' AND b.paxCategoryCode='"
							+ extendedFCCSegBCInventory.getPaxCategoryType() + "' AND b.fareCategoryCode='"
							+ extendedFCCSegBCInventory.getFareCategoryTypeCode() + "' AND i.fccsInvId = " + segInvId
							+ " AND i.status = '" + FCCSegBCInventory.Status.OPEN + "' AND b.logicalCCCode = '" + logicalCCCode
							+ "' ORDER BY i.seatsAllocated DESC";

					@SuppressWarnings("unchecked")
					List<Object> fccSegBCInvIds = getSession().createQuery(hqlSelect).list();

					if (fccSegBCInvIds != null && fccSegBCInvIds.size() > 0) {
						fCCSegBCInvId = (Integer) fccSegBCInvIds.get(0);
					}
				}

				if (fCCSegBCInvId == null) {
					// no open non-std bucket found; pick non-standard, priority, highest allocated bucket
					hqlSelect = "SELECT i.fccsbInvId FROM FCCSegBCInventory i, BookingClass b"
							+ " WHERE i.bookingCode = b.bookingCode AND b.standardCodeChar = '" + BookingClass.STANDARD_CODE_N
							+ "' AND b.fixedFlagChar = '" + BookingClass.FIXED_FLAG_N + "' AND b.bcType = '"
							+ extendedFCCSegBCInventory.getBcType() + "' AND b.allocationType='"
							+ extendedFCCSegBCInventory.getAllocationType() + "' AND b.paxCategoryCode='"
							+ extendedFCCSegBCInventory.getPaxCategoryType() + "' AND b.fareCategoryCode='"
							+ extendedFCCSegBCInventory.getFareCategoryTypeCode() + "' AND i.fccsInvId = " + segInvId
							+ " AND i.priorityFlagChar = '" + FCCSegBCInventory.PRIORITY_FLAG_Y + "' AND b.logicalCCCode = '"
							+ logicalCCCode + "' ORDER BY i.seatsAllocated DESC";

					@SuppressWarnings("unchecked")
					List<Object> fccSegBCInvIds = getSession().createQuery(hqlSelect).list();

					if (fccSegBCInvIds != null && fccSegBCInvIds.size() > 0) {
						fCCSegBCInvId = (Integer) fccSegBCInvIds.get(0);
					}
				}

				if (fCCSegBCInvId == null) {
					// no option; pick the same bucket itself even if it violates rules
					fCCSegBCInvId = new Integer(segBCInvId);
					needUpdateStatus = false;
				}

				String hqlUpdate = "UPDATE FCCSegBCInventory SET seatsAcquired = seatsAcquired + :totNumberOfSeats  , "
						+ "seatsAvailable = seatsAvailable + :totNumberOfSeats, " + "version = version + 1 "
						+ "WHERE fccsbInvId = " + fCCSegBCInvId;
				getSession().createQuery(hqlUpdate).setInteger("totNumberOfSeats", soldSeats + onHoldSeats).executeUpdate();
				if (needUpdateStatus) {
					// if bucket is closed and availability > 0 and not manually closed, then re-open the bucket
					reOpenFCCSegBCInventory(fCCSegBCInvId.intValue(), null);
				}

			} else if (releaseCase.equals("Case2C")) {
				// Case 2C is selected
				// Already marked canceled, sold and on-hold seats. Updating acquired and available seats as seat is not
				// acquired to another bc
				String sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET acquired_seats = acquired_seats+ " + (soldSeats + onHoldSeats)
						+ ", available_seats = greatest(allocated_seats + (acquired_seats - cancelled_seats) - "
						+ "(sold_seats + onhold_seats - " + (soldSeats + onHoldSeats)
						+ "),0), version = version + 1 WHERE fccsba_id=" + segBCInvId;

				jdbcTemplate.update(sqlUpdate);
				// if bucket is closed and availability > 0 and not manually closed, then re-open the bucket
				reOpenFCCSegBCInventory(segBCInvId, null);

			} else if (releaseCase.equals("Case3A")) {
				// next higher ranked standard open bucket
				String hqlSelect = "SELECT i.fccsbInvId FROM FCCSegBCInventory i, BookingClass b "
						+ "	WHERE i.bookingCode = b.bookingCode AND i.fccsInvId = " + segInvId + " AND b.nestRank = "
						+ " ( SELECT MIN(BC.nestRank) FROM FCCSegBCInventory IB, BookingClass BC "
						+ "	WHERE IB.bookingCode = BC.bookingCode AND IB.fccsInvId = " + segInvId + " AND IB.status = '"
						+ FCCSegBCInventory.Status.OPEN + "' AND BC.bcType = '" + extendedFCCSegBCInventory.getBcType()
						+ "' AND BC.standardCodeChar = '" + BookingClass.STANDARD_CODE_Y + "' AND BC.nestRank > "
						+ extendedFCCSegBCInventory.getNestRank() + " AND BC.allocationType='"
						+ extendedFCCSegBCInventory.getAllocationType() + "' AND BC.paxCategoryCode='"
						+ extendedFCCSegBCInventory.getPaxCategoryType() + "' AND BC.fareCategoryCode='"
						+ extendedFCCSegBCInventory.getFareCategoryTypeCode() + "' AND BC.logicalCCCode='" + logicalCCCode
						+ "') ";

				Integer fCCSegBCInvId = (Integer) getSession().createQuery(hqlSelect).uniqueResult();

				if (fCCSegBCInvId != null) {
					String sqlUpdate = "UPDATE T_FCC_SEG_BC_ALLOC SET acquired_seats = acquired_seats+ "
							+ (soldSeats + onHoldSeats) + ", available_seats = greatest(allocated_seats + (acquired_seats - "
							+ "cancelled_seats) - (sold_seats + onhold_seats - " + (soldSeats + onHoldSeats)
							+ "),0), version = version + 1 WHERE fccsba_id=" + fCCSegBCInvId;

					jdbcTemplate.update(sqlUpdate);
					// if bucket is closed and availability > 0 and not manually closed, then re-open the bucket
					reOpenFCCSegBCInventory(fCCSegBCInvId.intValue(), null);
					// This means seat is acquired. We can skip next steps
					break;
				}
			}

			iterator++;
			if (releaseCases.size() == iterator) {
				// last update then close
				getFlightInventoryDAO().updateBCStatusForNestAvailability(segInvId);
			}

		}

		if (invLog.isDebugEnabled()) {
			invLog.debug("releaseBCInventorySeats::fccsba:" + segBCInvId + ",sold:" + -soldSeats + ",onhold:" + -onHoldSeats
					+ ",wl:" + -releasedWLSeats + ",cases:" + releaseCases);
		}
	}

	/**
	 * @param fccsbInvId
	 * @param blockedSeatRecord
	 *            This parameter is set, when called from releasing blocked seats method
	 */
	@Override
	public int reOpenFCCSegBCInventory(int fccsbInvId, TempSegBcAlloc blockedSeatRecord) {
		boolean isEligible = true;

		ExtendedFCCSegBCInventory extendedFCCSegBCInventory = getExtendedBCInventory(fccsbInvId);
		FCCSegBCInventory fccSegBCInventory = extendedFCCSegBCInventory.getFccSegBCInventory();
		if (extendedFCCSegBCInventory.isStandardCode()
				&& (extendedFCCSegBCInventory.getAllocationType().equals(BookingClass.AllocationType.CONNECTION)
						|| extendedFCCSegBCInventory.getAllocationType().equals(BookingClass.AllocationType.RETURN))) {

			if (fccSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED)
					&& !fccSegBCInventory.getStatusChangeAction().equals(FCCSegBCInventory.StatusAction.MANUAL)
					&& !fccSegBCInventory.getStatusChangeAction().equals(FCCSegBCInventory.StatusAction.LOWERSTANDARD_CLOSURE)
					&& !fccSegBCInventory.getStatusChangeAction().equals(FCCSegBCInventory.StatusAction.GROUP_CLOSURE)) {
				FCCSegBCInventoryCollection bucketsCollection = getFlightInventoryJDBCDAO()
						.getBucketsCollection(new Integer(fccsbInvId));
				if (bucketsCollection != null && blockedSeatRecord == null) {
					isEligible = bucketsCollection.isBucketEligibleForReopen(fccsbInvId);
				}
			}
		} else if (fccSegBCInventory != null && blockedSeatRecord == null
				&& fccSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED)
				&& !fccSegBCInventory.getPriorityFlag()) {
			// Should not reopne both STD/Non STD BCs if those are non priority BCs
			isEligible = false;
		}

		int updateCount = 0;
		if (isEligible) {

			// check nest available is their
			int nestedAvailableSeat = getNestedAvailableCount(fccsbInvId);

			StringBuffer hqlUpdateStatus = new StringBuffer(" UPDATE FCCSegBCInventory ").append(" SET status = ?,")
					.append(" statusChangeAction = ?,").append(" version = version + 1").append(" WHERE ");

			if (AppSysParamsUtil.showNestedAvailableSeats() && nestedAvailableSeat > 0) {
				hqlUpdateStatus.append(" seatsAvailable >= 0 ");
			} else {
				hqlUpdateStatus.append(" seatsAvailable > 0 ");
			}

			hqlUpdateStatus.append(" AND  status = ?").append(" AND statusChangeAction <> ?")
					.append(" AND statusChangeAction <> ?").append(" AND statusChangeAction <> ?").append(" AND fccsbInvId = ? ");

			updateCount = getSession().createQuery(hqlUpdateStatus.toString()).setString(0, FCCSegBCInventory.Status.OPEN)
					.setString(1,
							blockedSeatRecord != null
									? blockedSeatRecord.getPrevStatusChgAction()
									: FCCSegBCInventory.StatusAction.SEAT_AQUISITION)
					.setString(2, FCCSegBCInventory.Status.CLOSED).setString(3, FCCSegBCInventory.StatusAction.MANUAL)
					.setString(4, FCCSegBCInventory.StatusAction.GROUP_CLOSURE)
					.setString(5, FCCSegBCInventory.StatusAction.LOWERSTANDARD_CLOSURE).setInteger(6, fccsbInvId).executeUpdate();
			if (updateCount > 0) {
				updateAllHigherGroupAllocations(fccSegBCInventory, null, false, null);
			}
		}
		return updateCount;
	}

	/**
	 * mehtod to save temp seg bc alloc
	 */
	@Override
	public void saveTempSegBcAlloc(TempSegBcAlloc alloc) {
		hibernateSaveOrUpdate(alloc);

		if (invLog.isDebugEnabled()) {
			invLog.debug("saveTempSegBcAlloc::id=" + alloc.getId() + ",fccsa=" + alloc.getFccaId() + ",fccsba="
					+ alloc.getFccsbaId() + ",blked=" + alloc.getSeatsBlocked() + ",timestp:" + alloc.getTimestamp() + ",bcType:"
					+ alloc.getBookingClassType());
		}
		if (log.isDebugEnabled()) {
			log.debug("Saving temporary seat block record [id=" + alloc.getId() + ",fccsInvId=" + alloc.getFccaId()
					+ ",fccsbaInvId=" + alloc.getFccsbaId() + ",seatsCount=" + alloc.getSeatsBlocked() + "]");
		}
	}

	/**
	 * mehtod to save temp seg bc alloc
	 */
	@Override
	public void saveAllTempSegBcAlloc(Collection<TempSegBcAllocBKP> allocs) {
		hibernateSaveOrUpdateAll(allocs);

		if (log.isDebugEnabled()) {
			log.debug("Saving temporary seat block records [seze=" + allocs.size());
		}
	}

	/**
	 * method to get TempBcAlloc
	 */
	@Override
	public TempSegBcAlloc getTempSegBcAlloc(Integer id) {

		return (TempSegBcAlloc) get(TempSegBcAlloc.class, id);
	}

	/**
	 * method to get multiple TempBcAlloc
	 */
	public List<TempSegBcAlloc> getTempSegBcAllocs(List<Integer> ids) {

		Query query = getSession().createQuery(" select a from TempSegBcAlloc a where a.id in ( :ids ) ");
		query.setParameterList("ids", ids);
		return query.list();

	}

	/**
	 * method to remove the TempSegBcAlloc
	 * 
	 * @param alloc
	 */
	@Override
	public void removeTempSegBcAlloc(TempSegBcAlloc tempSegBcAlloc) {
		if (log.isDebugEnabled()) {
			log.debug("Going to remove temporary seat block record [id=" + tempSegBcAlloc.getId() + "]");
		}
		Object alloc = load(TempSegBcAlloc.class, tempSegBcAlloc.getId());
		delete(alloc);
		if (invLog.isDebugEnabled()) {
			invLog.debug("removeTempSegBcAlloc::id=" + tempSegBcAlloc.getId() + ",fccsa=" + tempSegBcAlloc.getFccaId()
					+ ",fccsba=" + tempSegBcAlloc.getFccsbaId() + ",blked(rel)=" + -tempSegBcAlloc.getSeatsBlocked() + ",timestp:"
					+ tempSegBcAlloc.getTimestamp());
		}
	}

	/**
	 * mehtod to get getFccaId
	 */
	@Override
	public Integer getFccaId(int flightId, String logicalCabinClassCode, String segmentCode) {

		String hql = "select fccseg.fccsInvId from FCCSegInventory as fccseg where fccseg.flightId = :flightId and "
				+ "fccseg.segmentCode = :segmentCode and fccseg.logicalCCCode = :logicalCCCode";

		Integer id = (Integer) getSession().createQuery(hql).setParameter("flightId", new Integer(flightId))
				.setParameter("segmentCode", segmentCode).setParameter("logicalCCCode", logicalCabinClassCode).uniqueResult();

		return id;
	}

	@Override
	public Object[] getFCCSegInventoryFlightInfo(Integer flightSegId, String logicalCCCode) {
		String hql = "SELECT FS.flightId, FS.segmentCode, FS.fccsInvId FROM FCCSegInventory AS FS WHERE "
				+ "FS.flightSegId = :flightSegId AND FS.logicalCCCode = :logicalCCCode";

		return (Object[]) getSession().createQuery(hql).setParameter("flightSegId", flightSegId)
				.setParameter("logicalCCCode", logicalCCCode).uniqueResult();
	}

	/**
	 * method to update fcc seg inventry for infant seats
	 */
	@Override
	public void updateFccSegInvInfants(Integer id, Integer infants, boolean isOverBooking) {

		String hql = "update FCCSegInventory set soldInfantSeats = soldInfantSeats +  :infants,";
		if (!isOverBooking) {
			hql += "onholdInfantSeats = onholdInfantSeats -  :infants,";
		} else {
			hql += "availableInfantSeats = availableInfantSeats -  :infants,";
		}
		hql += "version = version + 1 where fccsInvId = :fccsInvId";

		getSession().createQuery(hql).setParameter("fccsInvId", id).setParameter("infants", infants).executeUpdate();
	}

	/**
	 * method to release fcc seg inventry for adult seats
	 */
	@Override
	public void releaseFccSegInvAdults(Integer id, Integer seats) {

		String hql = "update FCCSegInventory set onHoldSeats = onHoldSeats - :seats, "
				+ "availableSeats = availableSeats + :seats, version = version + 1 where fccsInvId = :fccsInvId";

		getSession().createQuery(hql).setParameter("fccsInvId", id).setParameter("seats", seats).executeUpdate();
	}

	/**
	 * method to release fcc seg inventry for infant seats
	 */
	@Override
	public void releaseFccSegInvInfants(Integer id, Integer infants) {

		String hql = "update FCCSegInventory set onholdInfantSeats = onholdInfantSeats -  :infants, "
				+ "availableInfantSeats = availableInfantSeats +  :infants,version = version + 1 where "
				+ "fccsInvId = :fccsInvId";

		getSession().createQuery(hql).setParameter("fccsInvId", id).setParameter("infants", infants).executeUpdate();
	}

	/**
	 * method to find the blocked records required for clean up
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<TempSegBcAlloc> findBlockedSeatsRecsForCleanUp(Date date) {

		String hql = "SELECT blockedRec FROM TempSegBcAlloc as blockedRec WHERE blockedRec.timestamp <= :timestamp";

		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return getSession().createQuery(hql).setTimestamp("timestamp", cal.getTime()).list();
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------

	@Override
	public int updateGoshowFCCSegBCInventory(int fccsbaId, int additionalAllocation, int soldSeats) {

		String hqlUpdate = "UPDATE FCCSegBCInventory SET seatsAllocated = seatsAllocated + :seatsAllocated, "
				+ "seatsSold = seatsSold + :seatsSold, version = version + 1 WHERE fccsbInvId=:fccsbInvId ";

		int result = getSession().createQuery(hqlUpdate).setInteger("seatsAllocated", additionalAllocation)
				.setInteger("seatsSold", soldSeats).setInteger("fccsbInvId", fccsbaId).executeUpdate();

		if (invLog.isDebugEnabled()) {
			invLog.debug("updateGoshowFCCSegBCInventory::fccsba:" + fccsbaId + ",additiona:" + additionalAllocation + ",sold"
					+ soldSeats + ",result:" + result);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, Object[]> getFCCSegBCInventorys(Integer fccsID) {

		String sql = " SELECT BC.STANDARD_CODE,BC.ALLOCATION_TYPE,SBCA.FCCSBA_ID,SBCA.FCCSA_ID,SBCA.FLIGHT_ID,"
				+ "SBCA.SEGMENT_CODE,SBCA.LOGICAL_CABIN_CLASS_CODE,SBCA.BOOKING_CODE, "
				+ " SBCA.ALLOCATED_SEATS,SBCA.SOLD_SEATS,SBCA.AVAILABLE_SEATS,SBCA.STATUS,SBCA.PRIORITY_FLAG,SBCA.CANCELLED_SEATS, "
				+ " SBCA.ACQUIRED_SEATS,SBCA.ONHOLD_SEATS,SBCA.STATUS_CHG_ACTION,SBCA.VERSION "
				+ " FROM T_FCC_SEG_BC_ALLOC SBCA,T_BOOKING_CLASS BC WHERE BC.BOOKING_CODE = SBCA.BOOKING_CODE  "
				+ " AND SBCA.FCCSA_ID = " + fccsID;

		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		HashMap<String, Object[]> fccSegInventoryMap = (HashMap<String, Object[]>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				HashMap<String, Object[]> fccSegInventoryMap = new HashMap<String, Object[]>();

				if (rs != null) {
					while (rs.next()) {
						FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
						String standardCode = rs.getString("standard_code");
						String allocationType = rs.getString("allocation_type");

						Object objFCCsbaId = rs.getObject("fccsba_id");
						if (objFCCsbaId != null) {
							// set FCCSegBCInventory
							fccSegBCInventory.setFccsbInvId(new Integer(objFCCsbaId.toString()));

							fccSegBCInventory.setBookingCode(rs.getString("booking_code"));
							fccSegBCInventory.setfccsInvId(rs.getInt("fccsa_id"));
							fccSegBCInventory.setFlightId(rs.getInt("flight_id"));

							fccSegBCInventory.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
							fccSegBCInventory.setSegmentCode(rs.getString("segment_code"));

							fccSegBCInventory.setSeatsAllocated(rs.getInt("allocated_seats"));
							fccSegBCInventory.setSeatsSold(rs.getInt("sold_seats"));
							fccSegBCInventory.setOnHoldSeats(rs.getInt("onhold_seats"));

							fccSegBCInventory.setSeatsCancelled(rs.getInt("cancelled_seats"));
							fccSegBCInventory.setSeatsAcquired(rs.getInt("acquired_seats"));
							fccSegBCInventory.setSeatsAvailable(rs.getInt("available_seats"));

							fccSegBCInventory.setPriorityFlag(
									rs.getString("priority_flag").equals(FCCSegBCInventory.PRIORITY_FLAG_Y) ? true : false);
							fccSegBCInventory.setStatus(rs.getString("status"));
							fccSegBCInventory.setStatusChangeAction(rs.getString("status_chg_action"));

							fccSegBCInventory.setVersion(rs.getLong("version"));
						}

						fccSegInventoryMap.put(fccSegBCInventory.getBookingCode(),
								new Object[] { standardCode, fccSegBCInventory, allocationType });

					}
				}
				return fccSegInventoryMap;
			}
		});

		return fccSegInventoryMap;

	}

	@Override
	public FCCSegBCInventory getFCCSegBCInventory(int flightSegId, String bookingCode) {
		String hql = "SELECT FSBI FROM FCCSegInventory FS, FCCSegBCInventory FSBI "
				+ "WHERE FS.fccsInvId = FSBI.fccsInvId AND FS.flightSegId = :flightSegId AND "
				+ "FSBI.bookingCode = :bookingCode";
		return (FCCSegBCInventory) getSession().createQuery(hql).setInteger("flightSegId", flightSegId)
				.setString("bookingCode", bookingCode).uniqueResult();
	}

	@Override
	public FCCSegInventory getFCCSegInventory(int flightSegId, String logicalCabinClassCode) {
		String hql = " SELECT FS FROM FCCSegInventory FS WHERE FS.logicalCCCode=:logicalCCCode AND FS.flightSegId=:flightSegId ";

		return (FCCSegInventory) getSession().createQuery(hql).setString("logicalCCCode", logicalCabinClassCode)
				.setInteger("flightSegId", flightSegId).uniqueResult();
	}

	public List<SegmentSeatDistsDTO> getSegmentSeatDistsDTOs(List<Integer> tempSegBcAllocIds) {

		List<SegmentSeatDistsDTO> segmentSeatDistsDTOs;
		StringBuilder query = new StringBuilder();
		query.append("SELECT tsa.seats_blocked, bca.booking_code, tsa.bc_type, sa.flt_seg_id,    ")
				.append("  sa.flight_id, sa.segment_code, sa.logical_cabin_class_code, a.cabin_class_code   ")
				.append("FROM t_temp_seg_bc_alloc tsa, t_fcc_seg_alloc sa,  ")
				.append("  t_fcc_seg_bc_alloc bca, t_fcc_alloc a    ")
				.append("WHERE tsa.fccsa_id = sa.fccsa_id and tsa.fccsba_id = bca.fccsba_id ")
				.append("  AND sa.fcca_id = a.fcca_id AND tsa.id in ( " + Util.buildIntegerInClauseContent(tempSegBcAllocIds)
						+ " )  ");

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		segmentSeatDistsDTOs = (List<SegmentSeatDistsDTO>) jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				List<SegmentSeatDistsDTO> list = new ArrayList<SegmentSeatDistsDTO>();
				SegmentSeatDistsDTO dto;
				SeatDistribution seatDist;

				while (resultSet.next()) {
					dto = new SegmentSeatDistsDTO();
					dto.setFlightSegId(resultSet.getInt("flt_seg_id"));
					dto.setFlightId(resultSet.getInt("flight_id"));
					dto.setSegmentCode(resultSet.getString("segment_code"));
					dto.setCabinClassCode(resultSet.getString("cabin_class_code"));
					dto.setLogicalCabinClass(resultSet.getString("logical_cabin_class_code"));

					seatDist = new SeatDistribution();
					seatDist.setBookingClassCode(resultSet.getString("booking_code"));
					seatDist.setBookingClassType(resultSet.getString("bc_type"));
					seatDist.setNoOfSeats(resultSet.getInt("seats_blocked"));
					dto.addSeatDistribution(seatDist);

					list.add(dto);
				}

				return list;
			}
		});

		return segmentSeatDistsDTOs;
	}

	@Override
	public Map<String, Integer> getFCCSegInventoryMap(int flightSegId) {
		String sql = "SELECT fccsa.logical_cabin_class_code,  fccsa.allocated_seats , fccsa.fccsa_id, fcca.cabin_class_code  "
				+ "FROM t_fcc_seg_alloc fccsa, t_fcc_alloc fcca, t_logical_cabin_class lcc, t_cabin_class cc "
				+ "WHERE lcc.logical_cabin_class_code = fccsa.logical_cabin_class_code AND lcc.cabin_class_code = cc.cabin_class_code "
				+ "AND fccsa.fcca_id = fcca.fcca_id " + "AND fccsa.flt_seg_id = " + flightSegId
				+ " ORDER BY cc.ranking ASC, lcc.logical_cc_rank ASC";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		@SuppressWarnings("unchecked")
		Map<String, Integer> fccSegInventoryMap = (Map<String, Integer>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				Map<String, Integer> fccSegInventoryMap = new LinkedHashMap<String, Integer>();
				while (resultSet.next()) {
					fccSegInventoryMap.put(resultSet.getString("CABIN_CLASS_CODE") + "_"
							+ resultSet.getString("LOGICAL_CABIN_CLASS_CODE") + "_" + resultSet.getInt("ALLOCATED_SEATS"),
							resultSet.getInt("FCCSA_ID"));
				}
				return fccSegInventoryMap;
			}
		});
		return fccSegInventoryMap;
	}

	@Override
	public FCCSegInventory getFCCSegInventory(Integer fccSegAllocId) {
		String hql = " SELECT FS FROM FCCSegInventory FS WHERE FS.fccsInvId=:fccsInvId ";

		return (FCCSegInventory) getSession().createQuery(hql).setInteger("fccsInvId", fccSegAllocId).uniqueResult();
	}

	@Override
	public TransferSeatDTO getTransferSeatInfo(int flightSegId, String bookingCode) {

		String sql = "select fsegalloc.flt_seg_id, fsegalloc.flight_id, fsegalloc.segment_code, fsegalloc.logical_cabin_class_code "
				+ " from t_fcc_seg_alloc fsegalloc, t_fcc_seg_bc_alloc fsegbcalloc where fsegalloc.flt_seg_id = " + flightSegId
				+ " and fsegbcalloc.booking_code = '" + bookingCode + "' and fsegalloc.fccsa_id =  fsegbcalloc.fccsa_id ";

		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		TransferSeatDTO transferSeatDTO = (TransferSeatDTO) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				TransferSeatDTO transferSeatDTO = null;

				if (rs != null) {
					while (rs.next()) {
						transferSeatDTO = new TransferSeatDTO();
						int flightSegId = rs.getInt("FLT_SEG_ID");
						int flightId = rs.getInt("FLIGHT_ID");
						String segmentCode = rs.getString("SEGMENT_CODE");
						String logicalCCCode = rs.getString("LOGICAL_CABIN_CLASS_CODE");

						transferSeatDTO.setTargetFlightSegId(flightSegId);
						transferSeatDTO.setTargetFlightId(flightId);
						transferSeatDTO.setTargetSegCode(segmentCode);
						transferSeatDTO.setLogicalCCCode(logicalCCCode);

					}
				}
				return transferSeatDTO;
			}
		});
		return transferSeatDTO;
	}

	@Override
	public int deleteTempSegBCInvClosure(Integer tempSegBCAllocId) {

		Query q = getSession().createQuery("DELETE FROM TempSegBCInvClosure WHERE tempAllocId=:tempAllocId")
				.setInteger("tempAllocId", tempSegBCAllocId.intValue());
		int deletedCount = q.executeUpdate();

		if (log.isDebugEnabled()) {
			log.debug("Deleting Temporary Bucket Closure Records [tempAllocId=" + tempSegBCAllocId + ", count=" + deletedCount
					+ "]");
		}

		return deletedCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<TempSegBCInvClosure> getTempSegBCInvClosure(Integer tempSegBCAllocId) {
		Query q = getSession().createQuery("SELECT TSBC FROM TempSegBCInvClosure TSBC WHERE TSBC.tempAllocId=:tempAllocId")
				.setInteger("tempAllocId", tempSegBCAllocId.intValue());
		return q.list();
	}

	@Override
	public void saveTempSegBCInvClosure(TempSegBCInvClosure tempSegBCInvClosure) {
		hibernateSave(tempSegBCInvClosure);

		if (log.isDebugEnabled()) {
			log.debug("Saving Temporary Bucket Closure Record [tempAllocId=" + tempSegBCInvClosure.getId() + ",fccsbInvId="
					+ tempSegBCInvClosure.getFccsbaId() + "]");
		}
	}

	@Override
	public int updateOptimizationStatus(Collection<Integer> bcInvIds, String newOptimizationStatus) {
		StringBuffer hqlUpdateStatus = new StringBuffer(" UPDATE FCCSegBCInventory ").append(" SET optimizationStatus = '")
				.append(newOptimizationStatus).append("', ").append(" version = version + 1 ")
				.append(" WHERE fccsbInvId IN (:bcInvIds) ");

		return getSession().createQuery(hqlUpdateStatus.toString()).setParameterList("bcInvIds", bcInvIds).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegBCInventory> getFCCSegBCInventorys(int flightId, String cabinClassCode, String segmentCode,
			Collection<String> bookingCodes) {

		return getSession().createQuery("Select FSBI from FCCSegBCInventory as FSBI where FSBI.flightId = "
				+ String.valueOf(flightId) + "  and FSBI.segmentCode = '" + segmentCode + "' and FSBI.CCCode = '" + cabinClassCode
				+ "'" + "  and FSBI.statusChangeAction <> '" + FCCSegBCInventory.StatusAction.MANUAL + "'"
				+ "  and FSBI.bookingCode in  (" + Util.buildStringInClauseContent(bookingCodes) + ")").list();

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegInventory> getFCCSegInventories(Integer flightId) {
		return getSession().createQuery("SELECT FSI FROM FCCSegInventory AS FSI WHERE FSI.flightId = " + flightId).list();
	}

	@Override
	public void saveOrUpdatePublishAvailability(PublishAvailability publishAvailability) {
		getSession().saveOrUpdate(publishAvailability);
	}

	/*
	 * enum InventoryNumericAvailStatus { ABOVE_UPPER_THRESHOLD, INBETWEEN_THRESHOLD, BELOW_LOWER_THRESHOLD }
	 */

	// For segment inventory as well as BC inventory
	private enum NumericAvailStatus {
		REMAINS_ABOVE_UPPER_THRESHOLD,
		DECREASES_BELOW_UPPER_THRESHOLD,
		INCREASES_ABOVE_UPPER_THRESHOLD,
		REMAINS_INBETWEEN_THRESHOLDS,
		DECREASES_BELOW_LOWER_THRESHOLD,
		INCREASES_ABOVE_LOWER_THRESHOLD,
		REMAINS_BELOW_LOWER_THRESHOLD
	}

	private enum BCAvailStatus {
		REMAINS_OPEN,
		CHANGES_TO_CLOSED,
		CHANGES_TO_OPEN,
		REMAINS_CLOSED
	}

	public void saveOrUpdatePubAvailForUpdateCreateAndCancel(FCCSegInventory existingFccSegInventory,
			FCCSegInventory updatedFccSegInventory, FCCSegBCInventory existingFCCSegBCInventory,
			FCCSegBCInventory updatedFCCSegBCInventory, int gdsID, boolean isFreshBCInventory, boolean isDeleted) {

		// the availability values should be reduced with effective Fixed Seat capacity (its own + intercepting)
		int updatedConstraint;
		int existingConstraint;
		BCAvailStatus bcAvailStatus = null;
		Gds gds = null;
		PublishInventoryEventCode publishEventCode = null;

		Integer AVS_CLOSURE_THRESHOLD = null;
		Integer AVS_AVAIL_THRESHOLD = null;

		try {
			gds = AirInventoryModuleUtils.getGdsServiceBD().getGds(gdsID);
			if (gds != null) {
				// Get GDS specific value
				AVS_CLOSURE_THRESHOLD = gds.getAvsClosureThreshold();
				AVS_AVAIL_THRESHOLD = gds.getAvsAvailThreshold();

			} else {
				setDefaultAvsThresholds(AVS_CLOSURE_THRESHOLD, AVS_AVAIL_THRESHOLD);
			}
		} catch (Exception ex) {
			log.error("Gds loading failed for gds-id:" + gdsID, ex);
			setDefaultAvsThresholds(AVS_CLOSURE_THRESHOLD, AVS_AVAIL_THRESHOLD);

		}

		// cancel/closure/un-publish case
		if (updatedFccSegInventory == null && updatedFCCSegBCInventory == null && existingFCCSegBCInventory == null) {

			publishEventCode = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_FLIGHT_CLOSED;
			updatedConstraint = existingFccSegInventory.getAvailableSeats();
		}

		// create/publish case
		else if (updatedFccSegInventory == null && updatedFCCSegBCInventory == null) {

			publishEventCode = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_CREATE_FLIGHT;

			updatedConstraint = 0;

			if (existingFCCSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.OPEN)) {
				updatedConstraint = (existingFCCSegBCInventory.getSeatsAvailable() <= existingFccSegInventory.getAvailableSeats())
						? existingFCCSegBCInventory.getSeatsAvailable()
						: existingFccSegInventory.getAvailableSeats();
			}

		}

		// update case
		else {

			if (isDeleted) {
				updatedFCCSegBCInventory = existingFCCSegBCInventory;
			}

			if (isFreshBCInventory) {
				existingFCCSegBCInventory = updatedFCCSegBCInventory;
			}

			// get existing constraint (the availability to be published)
			if (existingFCCSegBCInventory.getSeatsAvailable() >= existingFccSegInventory.getAvailableSeats()) {
				existingConstraint = existingFccSegInventory.getAvailableSeats();
			} else {
				existingConstraint = existingFCCSegBCInventory.getSeatsAvailable();
			}
			// get updated constraint (the availability to be published)
			if (updatedFCCSegBCInventory.getSeatsAvailable() >= updatedFccSegInventory.getAvailableSeats()) {
				updatedConstraint = updatedFccSegInventory.getAvailableSeats();
			} else {
				updatedConstraint = updatedFCCSegBCInventory.getSeatsAvailable();
			}

			// BC status
			boolean isOpen = updatedFCCSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.OPEN);
			boolean wasOpen = existingFCCSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.OPEN);

			// newly added BC Scenario, 'LC' case not concerned
			if (isFreshBCInventory) {

				// freshBc is only concerned when it's above the closure threshold and it's open
				if (updatedConstraint >= AVS_CLOSURE_THRESHOLD && isOpen) {
					publishEventCode = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_REOPEN;
				}
			}

			// deleted BC Scenario, no updates sent for already LC state BCs
			else if (isDeleted) {
				// deletedBC is updated only if it was open and >=closure threshold at the deletion

				if (existingConstraint >= AVS_CLOSURE_THRESHOLD && wasOpen) {
					publishEventCode = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_CLOSED;
				}
			}

			// LC scenarios
			// 1.existing NOT closed AND
			// 2. (updated constraint below closure OR manually closed) AND
			// 3. existing constraint NOT below threshold
			else if (wasOpen && (updatedConstraint < AVS_CLOSURE_THRESHOLD || !isOpen)
					&& (existingConstraint >= AVS_CLOSURE_THRESHOLD)) {

				publishEventCode = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_CLOSED;

			}

			// L<digit> Scenarios
			// 1. updated BC is open AND
			// 2. updated constraint is NOT below closure threshold
			// 3. the constraint is updated either closed BC is opened
			else if (isOpen && updatedConstraint >= AVS_CLOSURE_THRESHOLD
					&& (existingConstraint != updatedConstraint || (!wasOpen && isOpen))) {

				publishEventCode = AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_REOPEN;

			}

		}

		if (publishEventCode != null) {
			PublishAvailability publishAvailability = new PublishAvailability();
			publishAvailability.setFccsaId(existingFccSegInventory.getFccsInvId());
			publishAvailability.setFlightSegId(existingFccSegInventory.getFlightSegId());

			// possible at cancel/closure/un-publish cases
			if (existingFCCSegBCInventory != null) {
				publishAvailability.setBookingCode(existingFCCSegBCInventory.getBookingCode());
				publishAvailability.setFccsbaId(existingFCCSegBCInventory.getFccsbInvId());
				// need to remove
				publishAvailability.setBcStatus(existingFCCSegBCInventory.getStatus());
			}

			publishAvailability.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT.getCode());
			publishAvailability.setEventTimestamp(new Date());
			publishAvailability.setGdsID(gdsID);
			publishAvailability.setEventCode(publishEventCode.getCode());

			// TODO need to remove both Seg and BC and add the field: constraint, for now using Seg & BC the same
			publishAvailability.setSegAvailableSeats(updatedConstraint);
			publishAvailability.setBcAvailableSeats(updatedConstraint);

			saveOrUpdatePublishAvailability(publishAvailability);
		}

	}

	@Override
	public PublishAvailability getPublishAvailability(Integer publishAvailabilityId) {
		return (PublishAvailability) getSession().get(PublishAvailability.class, publishAvailabilityId);
	}

	/**
	 * Retrieves PublishAvailability for specified criteria
	 * 
	 * @param retrievePubAvailCriteria
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<PublishAvailability> getPublishAvailability(RetrievePubAvailCriteria retrievePubAvailCriteria) {
		String hql = "SELECT PA FROM PublishAvailability AS PA WHERE ";

		boolean isFirstCondition = true;
		if (retrievePubAvailCriteria.getSegInvId() != null) {
			hql += (isFirstCondition ? "" : " AND ") + " PA.fccsaId = :fccsaId ";
			isFirstCondition = false;
		}

		if (retrievePubAvailCriteria.getStatusCollection() != null) {
			hql += (isFirstCondition ? "" : " AND ") + " PA.eventStatus IN ( :eventStatuses )";
		}

		Query query = getSession().createQuery(hql);

		if (retrievePubAvailCriteria.getSegInvId() != null) {
			query.setInteger("fccsaId", retrievePubAvailCriteria.getSegInvId());
		}

		if (retrievePubAvailCriteria.getStatusCollection() != null) {
			Collection<String> eventStatuses = new ArrayList<String>();
			for (PublishInventoryEventStatus eventStatus : retrievePubAvailCriteria.getStatusCollection()) {
				eventStatuses.add(eventStatus.getCode());
			}
			query.setParameterList("eventStatuses", eventStatuses);
		}

		return query.list();
	}

	@Override
	public boolean isFCCSegBCInventoriesExists(FCCSegBCInventoriesExistsCriteria fccSegBCAllocExistsCriteria) {
		boolean hasBCAlloc = false;

		String hql = "SELECT count(*) FROM FCCSegBCInventory BA, GDSBookingClass GBC WHERE BA.bookingCode = GBC.bookingCode ";

		if (fccSegBCAllocExistsCriteria.getFccSegInvId() != null) {
			hql += " AND BA.fccsInvId = :fccSegInvId ";
		}

		if (fccSegBCAllocExistsCriteria.getBookingCode() != null) {
			hql += " AND BA.bookingCode = :bookingCode ";
		}

		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))
				&& fccSegBCAllocExistsCriteria.getIsGDSTypeOnly() != null) {
			if (fccSegBCAllocExistsCriteria.getIsGDSTypeOnly()) {
				hql += " AND GBC.gdsId IS NOT NULL ";
			} else {
				hql += " AND GBC.gdsId IS NULL ";
			}
		}

		Query query = getSession().createQuery(hql);

		if (fccSegBCAllocExistsCriteria.getFccSegInvId() != null) {
			query.setInteger("fccSegInvId", fccSegBCAllocExistsCriteria.getFccSegInvId());
		}

		if (fccSegBCAllocExistsCriteria.getBookingCode() != null) {
			query.setString("bookingCode", fccSegBCAllocExistsCriteria.getBookingCode());
		}

		Long count = (Long) query.uniqueResult();

		if (count > 0) {
			hasBCAlloc = true;
		}

		return hasBCAlloc;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Collection<FCCSegBCInventory> getFCCSegBCInventories(RetrieveFCCSegBCInvCriteria retrieveFCCSegBCInvCriteria) {
		Boolean gdsFlightId = true;
		if (retrieveFCCSegBCInvCriteria.getIsGDSAllocationsOnly() && retrieveFCCSegBCInvCriteria.getFccsInvId() != null) {

			String sql = "select count(BA.flight_id)  from t_fcc_seg_bc_alloc BA , t_flight_gds FID"
					+ " where FID.flight_id=BA.flight_id and BA.fccsa_id IN(" + retrieveFCCSegBCInvCriteria.getFccsInvId() + ")";

			JdbcTemplate jt = new JdbcTemplate(getDatasource());
			gdsFlightId = (Boolean) jt.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					if (rs != null) {
						int result = 0;
						while (rs.next()) {
							result = rs.getInt("count(BA.flight_id)");
						}
						if (result > 0) {
							return true;
						}
					}
					return false;
				}
			});
		}
		if (gdsFlightId) {
			String hql = " SELECT BA FROM FCCSegBCInventory BA, BookingClass BC WHERE BA.bookingCode = BC.bookingCode ";
			if (retrieveFCCSegBCInvCriteria.getFccsInvId() != null) {
				hql += " AND BA.fccsInvId = :fccsInvId ";
			}

			Query query = getSession().createQuery(hql);

			if (retrieveFCCSegBCInvCriteria.getFccsInvId() != null) {
				query.setInteger("fccsInvId", retrieveFCCSegBCInvCriteria.getFccsInvId());
			}

			return query.list();
		}
		return null;
	}

	@Override
	public int updateOversellCurtailFCCSegmentInventory(int fccSegInvId, int oversell, int curtailed, int waitlisting) {
		String hql = "update FCCSegInventory set oversellSeats=:oversell, curtailedSeats=:curtailed, allocatedWaitListSeats=:waitlisting"
				+ ", version = version + 1 " + "where fccsInvId=:fccsInvId ";

		Query query = getSession().createQuery(hql).setInteger("oversell", oversell).setInteger("curtailed", curtailed)
				.setInteger("waitlisting", waitlisting).setInteger("fccsInvId", fccSegInvId);
		return query.executeUpdate();
	}

	@Override
	public void deleteFCCSegBCInventory(Collection<Integer> colfCCSegBCInventory) {
		String deleteFCCSegBCInventoriesHql = "DELETE FROM FCCSegBCInventory WHERE fccsbInvId IN (:fccBCInvIds) ";
		getSession().createQuery(deleteFCCSegBCInventoriesHql).setParameterList("fccBCInvIds", colfCCSegBCInventory)
				.executeUpdate();
	}

	private AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}

	private String prepareSqlINString(String columnName, Collection<? extends Object> values, String suffix) {
		if (values.size() == 0) {
			return "";
		}
		StringBuffer inStrBuffer = new StringBuffer(columnName + " IN ( ");
		Object[] valuesArr = values.toArray();
		if (valuesArr[0] instanceof Integer) {
			for (Object element : valuesArr) {
				inStrBuffer.append(((Integer) element).toString() + ",");
			}
		} else if (valuesArr[0] instanceof String) {
			for (Object element : valuesArr) {
				inStrBuffer.append("'" + (String) element + "',");
			}
		}
		String inStr = inStrBuffer.toString();
		return inStr.substring(0, inStr.lastIndexOf(',')).concat(")").concat(suffix);
	}

	private FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance()
				.getLocalBean("flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance()
				.getLocalBean("FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	@Override
	public void saveInventoryMovemant(FCCInventoryMovemant inventoryMovemant) {
		hibernateSave(inventoryMovemant);
	}

	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	private void closeAllLowerRankStandardAllocations(FCCSegBCInventory fccSegBCInventory, UpdateFccSegBCInvResponse response) {

		String lowerStandardBCsSQL = getLowerStandardBCsSQL(fccSegBCInventory.getBookingCode(),
				fccSegBCInventory.getLogicalCCCode());

		List<String[]> data = getAllocations(fccSegBCInventory.getFlightId(), lowerStandardBCsSQL, FCCSegBCInventory.Status.OPEN,
				fccSegBCInventory.getSegmentCode());

		Collection<FCCSegBCInventoryTO> closedFccsbInvs = new ArrayList<FCCSegBCInventoryTO>();
		for (String[] bcInvDetails : data) {
			updateFCCSegBC(Integer.valueOf(bcInvDetails[0]), FCCSegBCInventory.Status.CLOSED,
					FCCSegBCInventory.StatusAction.LOWERSTANDARD_CLOSURE);

			FCCSegBCInventoryTO fccSegBCInventoryTO = new FCCSegBCInventoryTO();
			fccSegBCInventoryTO.setFccsbInvId(Integer.valueOf(bcInvDetails[0]));
			fccSegBCInventoryTO.setStatusChangeAction(bcInvDetails[1]);

			closedFccsbInvs.add(fccSegBCInventoryTO);
		}
		if (response.getClosedFccsbInvs() != null && response.getClosedFccsbInvs().size() > 0) {
			response.getClosedFccsbInvs().addAll(closedFccsbInvs);
		} else {
			response.setClosedFccsbInvs(closedFccsbInvs);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCSegBCInventory> getFCCSegBCInventories(int fccsaId) {
		return getSession().createQuery("Select FSBI from FCCSegBCInventory as FSBI where FSBI.fccsInvId = " + fccsaId).list();
	}

	private void updateAllHigherGroupAllocations(FCCSegBCInventory fccSegBCInventory, UpdateFccSegBCInvResponse response,
			boolean isClose, StringBuilder bcsToSkip) {

		String higherGroupBCsSQL = getHigherGroupBCsSQL(fccSegBCInventory.getBookingCode(), fccSegBCInventory.getLogicalCCCode());

		if (isClose) {

			if (!SameGroupAllocationExists(fccSegBCInventory, bcsToSkip)) {
				List<String[]> data = getAllocations(fccSegBCInventory.getFlightId(), higherGroupBCsSQL,
						FCCSegBCInventory.Status.OPEN, fccSegBCInventory.getSegmentCode());

				Collection<FCCSegBCInventoryTO> closedFccsbInvs = new ArrayList<FCCSegBCInventoryTO>();
				for (String[] bcInvDetails : data) {
					updateFCCSegBC(Integer.valueOf(bcInvDetails[0]), FCCSegBCInventory.Status.CLOSED,
							FCCSegBCInventory.StatusAction.GROUP_CLOSURE);

					FCCSegBCInventoryTO fccSegBCInventoryTO = new FCCSegBCInventoryTO();
					fccSegBCInventoryTO.setFccsbInvId(Integer.valueOf(bcInvDetails[0]));
					fccSegBCInventoryTO.setStatusChangeAction(bcInvDetails[1]);

					closedFccsbInvs.add(fccSegBCInventoryTO);

					// Close all lower ranked standard bc allocations of closing group bc allocations
					if (getAirInventoryConfig().isCloseAllLowerRankedStandardBCsWhenClosingHigherBC()) {
						String lowerStandardBCsSQL = getLowerStandardBCsSQL(bcInvDetails[2], bcInvDetails[3]);
						List<String[]> lowerStandardAllocations = getAllocations(fccSegBCInventory.getFlightId(),
								lowerStandardBCsSQL, FCCSegBCInventory.Status.OPEN, fccSegBCInventory.getSegmentCode());
						for (String[] lowerStdDetails : lowerStandardAllocations) {
							updateFCCSegBC(Integer.valueOf(lowerStdDetails[0]), FCCSegBCInventory.Status.CLOSED,
									FCCSegBCInventory.StatusAction.LOWERSTANDARD_CLOSURE);

							FCCSegBCInventoryTO fccSegBCInvTO = new FCCSegBCInventoryTO();
							fccSegBCInvTO.setFccsbInvId(Integer.valueOf(lowerStdDetails[0]));
							fccSegBCInvTO.setStatusChangeAction(lowerStdDetails[1]);

							closedFccsbInvs.add(fccSegBCInvTO);
						}
					}
				}
				if (response.getClosedFccsbInvs() != null && response.getClosedFccsbInvs().size() > 0) {
					response.getClosedFccsbInvs().addAll(closedFccsbInvs);
				} else {
					response.setClosedFccsbInvs(closedFccsbInvs);
				}
			}
		} else {

			List<String[]> data = getAllocations(fccSegBCInventory.getFlightId(), higherGroupBCsSQL,
					FCCSegBCInventory.Status.CLOSED, fccSegBCInventory.getSegmentCode());
			// Get the higher logical cabin classe bc allocations and reopen only if they are closed due to group
			// closure. If one
			// is closed due to availability zero, then do not open all other higher ranked bc inventories.
			for (String[] dataArr : data) {
				if (dataArr[1] != null && dataArr[1].equals(FCCSegBCInventory.StatusAction.MANUAL)) {
					continue;
				} else if (dataArr[1] != null && dataArr[1].equals(FCCSegBCInventory.StatusAction.GROUP_CLOSURE)) {
					updateFCCSegBC(Integer.valueOf(dataArr[0]), FCCSegBCInventory.Status.OPEN,
							FCCSegBCInventory.StatusAction.SEAT_AQUISITION);
				} else if (dataArr[1] != null && dataArr[1].equals(FCCSegBCInventory.StatusAction.AVAILABILITY_ZERO)) {
					break;
				}
			}
		}
	}

	private List<String[]> getAllocations(int flightId, String bcSelectionSQL, String status, String segmentCode) {

		String sql = "select fsbci.fccsba_id, fsbci.status_chg_action, fsbci.booking_code, fsbci.logical_cabin_class_code"
				+ " from t_fcc_seg_bc_alloc fsbci where fsbci.status='" + status + "' and fsbci.flight_id=" + flightId
				+ " and fsbci.segment_code = '" + segmentCode + "' and fsbci.booking_code in (" + bcSelectionSQL + ")";

		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		@SuppressWarnings("unchecked")
		List<String[]> bcAllocations = (List<String[]>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<String[]> bcAllocations = new ArrayList<String[]>();

				if (rs != null) {
					while (rs.next()) {
						String[] bcAndStatusChgAction = { rs.getString("fccsba_id"), rs.getString("status_chg_action"),
								rs.getString("booking_code"), rs.getString("logical_cabin_class_code") };
						bcAllocations.add(bcAndStatusChgAction);
					}
				}
				return bcAllocations;
			}
		});

		return bcAllocations;
	}

	private boolean SameGroupAllocationExists(FCCSegBCInventory fccSegBCInventory, StringBuilder bcsToSkip) {

		String sql = "SELECT count(bc.booking_code) FROM t_fcc_seg_bc_alloc fccbca, t_booking_class bc"
				+ " WHERE fccbca.fccsa_id              =" + fccSegBCInventory.getfccsInvId()
				+ " AND fccbca.logical_cabin_class_code=bc.logical_cabin_class_code"
				+ " AND fccbca.booking_code            =bc.booking_code" + " AND fccbca.status                  ='OPN'"
				+ " AND bc.booking_code                <> '" + fccSegBCInventory.getBookingCode()
				+ "' AND bc.group_id                    <> 0"
				+ " AND bc.group_id                  in (select group_id from t_booking_class where booking_code='"
				+ fccSegBCInventory.getBookingCode() + "')";

		// Skip checked allocations for CON/RET bucket closure
		if (bcsToSkip != null) {
			sql += " AND bc.booking_code not in (" + bcsToSkip.toString() + ")";
		}

		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		Boolean groupAllocExist = (Boolean) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {
					int result = 0;
					while (rs.next()) {
						result = rs.getInt("COUNT(BC.BOOKING_CODE)");
					}
					if (result > 0) {
						return true;
					}
				}
				return false;
			}
		});

		return groupAllocExist;
	}

	private String getLowerStandardBCsSQL(String bcCode, String logicalCCCode) {

		String sql = "select booking_code from t_booking_class where logical_cabin_class_code = '" + logicalCCCode
				+ "' and standard_code = 'Y' and nest_rank < " + "(select nest_rank from t_booking_class where booking_code = '"
				+ bcCode + "' and allocation_type='SEG') and allocation_type='SEG'";

		return sql;
	}

	private String getHigherGroupBCsSQL(String bcCode, String logicalCCCode) {

		String sql = "select booking_code from t_booking_class where standard_code = 'Y' and group_id <> 0 and group_id in "
				+ "(select group_id from t_booking_class where booking_code='" + bcCode
				+ "') and logical_cabin_class_code in (select logical_cabin_class_code from t_logical_cabin_class where logical_cc_rank > "
				+ "(select logical_cc_rank from t_logical_cabin_class where status='ACT' and logical_cabin_class_code='"
				+ logicalCCCode + "'))";

		return sql;
	}

	private void updateFCCSegBC(int fccsbInvId, String statues, String statusAction) {

		StringBuffer hqlUpdateStatus = new StringBuffer(" UPDATE FCCSegBCInventory SET status = '").append(statues)
				.append("', statusChangeAction = '").append(statusAction).append("', version = version + 1")
				.append(" WHERE fccsbInvId=").append(fccsbInvId);
		getSession().createQuery(hqlUpdateStatus.toString()).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public LinkedHashMap<String, List<String>> getAllocatedBookingClassesForCabinClasses(Integer flightId) {
		String sql = "";
		
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				sql = "select distinct fcca.cabin_class_code, fccsbc.logical_cabin_class_code as fare_class from t_fcc_alloc fcca, t_fcc_seg_alloc fccsa, t_fcc_seg_bc_alloc fccsbc where fccsbc.fccsa_id = fccsa.fccsa_id and fccsa.fcca_id = fcca.fcca_id and fcca.flight_id=' "
						+ flightId + "'";
			} else {
				sql = "select distinct fcca.cabin_class_code, fccsbc.booking_code as fare_class, cc.ranking from t_fcc_alloc fcca, t_fcc_seg_alloc fccsa, t_fcc_seg_bc_alloc fccsbc, t_booking_class bc, t_cabin_class cc where fccsbc.booking_code = bc.booking_code and fccsbc.fccsa_id = fccsa.fccsa_id and fccsa.fcca_id = fcca.fcca_id and fcca.flight_id=' "
						+ flightId + "' and bc.bc_type <> '" + BookingClass.BookingClassType.OPEN_RETURN
						+ "' AND cc.cabin_class_code = bc.cabin_class_code order by cc.ranking";
			}
		} else {
			sql = "select distinct fcca.cabin_class_code, fcca.cabin_class_code as fare_class from t_fcc_alloc fcca, t_fcc_seg_alloc fccsa, t_fcc_seg_bc_alloc fccsbc where fccsbc.fccsa_id = fccsa.fccsa_id and fccsa.fcca_id = fcca.fcca_id and fcca.flight_id=' "
					+ flightId + "'";
		}
		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		LinkedHashMap<String, List<String>> bcsForCcs = (LinkedHashMap<String, List<String>>) jt.query(sql,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						LinkedHashMap<String, List<String>> bcsForCcs = new LinkedHashMap<String, List<String>>();

						if (rs != null) {
							while (rs.next()) {
								String cc = rs.getString("cabin_class_code");
								if (bcsForCcs.get(cc) == null) {
									bcsForCcs.put(cc, new ArrayList<String>());
									bcsForCcs.get(cc).add(rs.getString("fare_class"));

								} else {
									bcsForCcs.get(cc).add(rs.getString("fare_class"));
								}
							}
						}
						return bcsForCcs;
					}
				});
		return bcsForCcs;

	}

	private BookingClassDAO getBookingClassDAO() {
		BookingClassDAO bookingClassDAO = (BookingClassDAO) AirInventoryUtil.getInstance()
				.getLocalBean("BookingClassDAOImplProxy");
		return bookingClassDAO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Integer> getAvailableCabinClassesInfo() {

		String sql = "SELECT CABIN_CLASS_CODE,RANKING FROM T_CABIN_CLASS WHERE STATUS='ACT'";

		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		Map<String, Integer> cosInfoMap = (Map<String, Integer>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Map<String, Integer> cosInfoMap = new HashMap<String, Integer>();

				if (rs != null) {
					while (rs.next()) {

						cosInfoMap.put(rs.getString("CABIN_CLASS_CODE"), rs.getInt("RANKING"));

					}
				}
				return cosInfoMap;
			}
		});
		return cosInfoMap;
	}

	@Override
	public List<Integer> getBlockedSeatsIds(Collection<Integer> allBlockSeatIds, List<FlightSegmentDTO> flightSegsToRelease)
			throws ModuleException {

		List<Integer> results = null;

		if (!flightSegsToRelease.isEmpty() && !allBlockSeatIds.isEmpty()) {
			StringBuilder sql = new StringBuilder();
			sql.append(
					" SELECT TSA.ID temp_seg_alloc FROM T_TEMP_SEG_BC_ALLOC TSA, T_FCC_SEG_ALLOC FSA, T_FLIGHT_SEGMENT FS, T_FLIGHT F ")
					.append(" WHERE TSA.FCCSA_ID = FSA.FCCSA_ID AND FS.FLT_SEG_ID  = FSA.FLT_SEG_ID ")
					.append("AND FS.FLIGHT_ID   = F.FLIGHT_ID AND TSA.ID IN ( :block_seat_ids ) AND ( ");

			Iterator<FlightSegmentDTO> itrFltSegs = flightSegsToRelease.iterator();
			FlightSegmentDTO flightSegmentDTO;
			while (itrFltSegs.hasNext()) {
				flightSegmentDTO = itrFltSegs.next();
				sql.append(" (  F.FLIGHT_NUMBER = '").append(flightSegmentDTO.getFlightNumber()).append("'")
						.append(" AND TO_CHAR ( FS.EST_TIME_DEPARTURE_LOCAL , 'DD-MM-YYYY HH24:MI:SS' ) = '")
						.append(CalendarUtil.formatDate(flightSegmentDTO.getDepartureDateTime(), "dd-MM-yyyy HH:mm:ss"))
						.append("' AND TO_CHAR ( FS.EST_TIME_ARRIVAL_LOCAL , 'DD-MM-YYYY HH24:MI:SS' ) = '")
						.append(CalendarUtil.formatDate(flightSegmentDTO.getArrivalDateTime(), "dd-MM-yyyy HH:mm:ss"))
						.append("' AND FS.SEGMENT_CODE LIKE '").append(flightSegmentDTO.getFromAirport() + "/%'")
						.append(" AND FS.SEGMENT_CODE LIKE ").append("'%/" + flightSegmentDTO.getToAirport() + "' ) ");

				if (itrFltSegs.hasNext()) {
					sql.append(" OR ");
				}
			}

			sql.append(" ) ");

			SQLQuery query = getSession().createSQLQuery(sql.toString());
			query.setParameterList("block_seat_ids", allBlockSeatIds);
			query.addScalar("temp_seg_alloc", IntegerType.INSTANCE);

			results = query.list();

		}
		return results;
	}

	public List<TempSegBcAllocDto> getBlockedSeatsInfo(List<Integer> blockSeatIds) {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ta.id, fs.flt_seg_id, fs.segment_code, f.flight_number, fs.est_time_departure_local,    ")
				.append("   fs.est_time_arrival_local, bca.booking_code ")
				.append(" FROM t_temp_seg_bc_alloc ta, t_fcc_seg_bc_alloc bca, t_fcc_seg_alloc sa,   ")
				.append("       t_flight_segment fs, t_flight f ")
				.append(" WHERE ta.fccsba_id = bca.fccsba_id AND bca.fccsa_id = sa.fccsa_id ")
				.append("       AND sa.flt_seg_id = fs.flt_seg_id AND fs.flight_id   = f.flight_id ")
				.append("       AND ta.id in ( " + Util.buildIntegerInClauseContent(blockSeatIds) + " ) ");

		JdbcTemplate template = new JdbcTemplate(getDatasource());
		List<TempSegBcAllocDto> dtos = (List<TempSegBcAllocDto>) template.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				List<TempSegBcAllocDto> list = new ArrayList<TempSegBcAllocDto>();
				TempSegBcAllocDto dto;

				while (resultSet.next()) {
					dto = new TempSegBcAllocDto();
					dto.setId(resultSet.getInt("id"));
					dto.setFltSegId(resultSet.getInt("flt_seg_id"));
					dto.setSegmentCode(resultSet.getString("segment_code"));
					dto.setFlightNumber(resultSet.getString("flight_number"));
					dto.setDepartureTime(resultSet.getTimestamp("est_time_departure_local"));
					dto.setArrivalTime(resultSet.getTimestamp("est_time_arrival_local"));
					dto.setBookingClass(resultSet.getString("booking_code"));

					list.add(dto);
				}
				return list;
			}
		});

		return dtos;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<PublishAvailability> getPublishAvailabiity(Integer fccSegBCinventoryID) {

		String hql = "select PA from PublishAvailability as PA where fccsba_id = :fccSegBCinventoryID order by PA.eventTimestamp desc";

		Collection<PublishAvailability> publishAvailabilities = (getSession().createQuery(hql)
				.setInteger("fccSegBCinventoryID", fccSegBCinventoryID).list());

		return publishAvailabilities;
	}

	public Integer getTotalVacantSeats(Integer flightSegId) {
		StringBuilder queryContent = new StringBuilder();
		queryContent.append(" select sum(i.availableSeats) from FCCSegInventory i where i.flightSegId = :flightSegId ");

		Query query = getSession().createQuery(queryContent.toString()).setParameter("flightSegId", flightSegId);
		return ((Long) query.uniqueResult()).intValue();
	}

	private Collection<String> getAllocationTypes(String allocationType) {
		Collection<String> allocationTypes = new ArrayList<String>();
		if (AllocationType.SEGMENT.equals(allocationType)) {
			allocationTypes.add(AllocationType.COMBINED);
			allocationTypes.add(AllocationType.SEGMENT);
		} else if (AllocationType.RETURN.equals(allocationType)) {
			allocationTypes.add(AllocationType.COMBINED);
			allocationTypes.add(AllocationType.RETURN);
		} else if (AllocationType.CONNECTION.equals(allocationType)) {
			allocationTypes.add(AllocationType.COMBINED);
			allocationTypes.add(AllocationType.CONNECTION);
		} else if (AllocationType.COMBINED.equals(allocationType)) {
			allocationTypes.add(AllocationType.SEGMENT);
			allocationTypes.add(AllocationType.RETURN);
			allocationTypes.add(AllocationType.CONNECTION);
			allocationTypes.add(AllocationType.COMBINED);
		}
		return allocationTypes;
	}

	public int getNestedAvailableCount(Integer fccsbInvId) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT BA.fccsInvId,BA.bookingCode,BC.nestRank, BC.allocationType ");
		hql.append("FROM FCCSegBCInventory BA, BookingClass BC WHERE BA.bookingCode = BC.bookingCode ");
		hql.append("AND BA.fccsbInvId = :fccSegInvId AND BC.standardCodeChar = '" + BookingClass.STANDARD_CODE_Y + "'");

		Query query = getSession().createQuery(hql.toString());
		query.setInteger("fccSegInvId", fccsbInvId);

		Object[] fccsbInfo = (Object[]) query.uniqueResult();

		int nestedAvailableSeat = 0;

		if (fccsbInfo != null && fccsbInfo.length > 3 && fccsbInfo[2] != null) {
			Integer fccsInvId = (Integer) fccsbInfo[0];
			Integer nestRank = (Integer) fccsbInfo[2];
			String allocationType = (String) fccsbInfo[3];

			StringBuilder hql2 = new StringBuilder();
			hql2.append(
					"SELECT SUM(BA.seatsAvailable) FROM FCCSegBCInventory BA, BookingClass BC WHERE BA.bookingCode = BC.bookingCode ");
			hql2.append("AND BA.fccsInvId = :fccsInvId AND  BA.status='" + FCCSegBCInventory.Status.OPEN
					+ "' AND BC.nestRank <= :nestRank AND BC.allocationType in ("
					+ Util.buildStringInClauseContent(getAllocationTypes(allocationType)) + ")");

			Query query2 = getSession().createQuery(hql2.toString());
			query2.setInteger("fccsInvId", fccsInvId);
			query2.setInteger("nestRank", nestRank);

			Object results = query2.uniqueResult();

			if (results != null) {
				nestedAvailableSeat = ((Long) results).intValue();
			}
		}

		return nestedAvailableSeat;
	}

	private boolean isAutoCloseValidForBC(Integer fccsbInvId) {
		boolean isCloseBC = true;
		int noRecsUpdated = 0;
		if (fccsbInvId != null && AppSysParamsUtil.showNestedAvailableSeats()) {
			int nestedAvailableSeat = getNestedAvailableCount(fccsbInvId);

			if (nestedAvailableSeat > 0)
				isCloseBC = false;

		}
		return isCloseBC;
	}

	@Override
	public void updateBCStatusForNestAvailability(Integer fccsInvId) {

		List<String[]> data = getFccsbInvIdList(fccsInvId);

		if (AppSysParamsUtil.showNestedAvailableSeats() && data != null) {
			Collection<FCCSegBCInventoryTO> closedFccsbInvs = new ArrayList<FCCSegBCInventoryTO>();
			for (String[] fccsbInvInfo : data) {
				int nestedAvailableSeat = getNestedAvailableCount(Integer.valueOf(fccsbInvInfo[0]));

				if (nestedAvailableSeat > 0 && FCCSegBCInventory.Status.CLOSED.equals(fccsbInvInfo[2])) {
					updateFCCSegBC(Integer.valueOf(fccsbInvInfo[0]), FCCSegBCInventory.Status.OPEN,
							FCCSegBCInventory.StatusAction.NESTED_SEAT_OPERATION);
				} else if (nestedAvailableSeat == 0 && Integer.valueOf(fccsbInvInfo[1]) == 0
						&& FCCSegBCInventory.Status.OPEN.equals(fccsbInvInfo[2])) {
					updateFCCSegBC(Integer.valueOf(fccsbInvInfo[0]), FCCSegBCInventory.Status.CLOSED,
							FCCSegBCInventory.StatusAction.NESTED_SEAT_OPERATION);
				}
			}
		}
	}

	private List<String[]> getFccsbInvIdList(Integer fccsInvId) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT FCSEG.FCCSBA_ID,FCSEG.AVAILABLE_SEATS,FCSEG.STATUS,FCSEG.BOOKING_CODE ");
		sql.append("FROM T_FCC_SEG_BC_ALLOC FCSEG,T_BOOKING_CLASS BC ");
		sql.append("WHERE BC.BOOKING_CODE = FCSEG.BOOKING_CODE AND BC.STANDARD_CODE='Y' AND FCSEG.FCCSA_ID=" + fccsInvId + " ");
		sql.append("AND (FCSEG.STATUS='" + FCCSegBCInventory.Status.OPEN + "' OR (FCSEG.STATUS='"
				+ FCCSegBCInventory.Status.CLOSED + "' ");
		sql.append("AND FCSEG.STATUS_CHG_ACTION!='" + FCCSegBCInventory.StatusAction.MANUAL + "')) ORDER BY BC.NEST_RANK ASC");

		JdbcTemplate jt = new JdbcTemplate(getDatasource());

		@SuppressWarnings("unchecked")
		List<String[]> fccsbInvIds = (List<String[]>) jt.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<String[]> fccsbInvIds = new ArrayList<String[]>();

				if (rs != null) {
					while (rs.next()) {
						String[] fccsbInvInfo = { rs.getString("FCCSBA_ID"), rs.getString("AVAILABLE_SEATS"),
								rs.getString("STATUS"), rs.getString("BOOKING_CODE") };
						fccsbInvIds.add(fccsbInvInfo);
					}
				}
				return fccsbInvIds;
			}
		});

		return fccsbInvIds;
	}

	@Override
	public Collection<FCCSegBCInventory> getFixedBCInventories(Integer fccsaIds) {

		String sql = " SELECT BC.STANDARD_CODE,BC.ALLOCATION_TYPE,SBCA.FCCSBA_ID,SBCA.FCCSA_ID,SBCA.FLIGHT_ID,"
				+ "SBCA.SEGMENT_CODE,SBCA.LOGICAL_CABIN_CLASS_CODE,SBCA.BOOKING_CODE, "
				+ " SBCA.ALLOCATED_SEATS,SBCA.SOLD_SEATS,SBCA.AVAILABLE_SEATS,SBCA.STATUS,SBCA.PRIORITY_FLAG,SBCA.CANCELLED_SEATS, "
				+ " SBCA.ACQUIRED_SEATS,SBCA.ONHOLD_SEATS,SBCA.STATUS_CHG_ACTION,SBCA.VERSION "
				+ " FROM T_FCC_SEG_BC_ALLOC SBCA,T_BOOKING_CLASS BC WHERE BC.BOOKING_CODE = SBCA.BOOKING_CODE AND FIXED_FLAG='Y' "
				+ " AND SBCA.FCCSA_ID = " + fccsaIds;

		JdbcTemplate jt = new JdbcTemplate(getDatasource());
		Collection<FCCSegBCInventory> fccSegInventory = (Collection) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Collection<FCCSegBCInventory> fccSegInventory = new ArrayList<FCCSegBCInventory>();

				if (rs != null) {
					while (rs.next()) {
						FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
						String standardCode = rs.getString("standard_code");
						String allocationType = rs.getString("allocation_type");

						Object objFCCsbaId = rs.getObject("fccsba_id");
						if (objFCCsbaId != null) {
							// set FCCSegBCInventory
							fccSegBCInventory.setFccsbInvId(new Integer(objFCCsbaId.toString()));

							fccSegBCInventory.setBookingCode(rs.getString("booking_code"));
							fccSegBCInventory.setfccsInvId(rs.getInt("fccsa_id"));
							fccSegBCInventory.setFlightId(rs.getInt("flight_id"));

							fccSegBCInventory.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
							fccSegBCInventory.setSegmentCode(rs.getString("segment_code"));

							fccSegBCInventory.setSeatsAllocated(rs.getInt("allocated_seats"));
							fccSegBCInventory.setSeatsSold(rs.getInt("sold_seats"));
							fccSegBCInventory.setOnHoldSeats(rs.getInt("onhold_seats"));

							fccSegBCInventory.setSeatsCancelled(rs.getInt("cancelled_seats"));
							fccSegBCInventory.setSeatsAcquired(rs.getInt("acquired_seats"));
							fccSegBCInventory.setSeatsAvailable(rs.getInt("available_seats"));

							fccSegBCInventory.setPriorityFlag(
									rs.getString("priority_flag").equals(FCCSegBCInventory.PRIORITY_FLAG_Y) ? true : false);
							fccSegBCInventory.setStatus(rs.getString("status"));
							fccSegBCInventory.setStatusChangeAction(rs.getString("status_chg_action"));

							fccSegBCInventory.setVersion(rs.getLong("version"));
						}

						fccSegInventory.add(fccSegBCInventory);

					}
				}
				return fccSegInventory;
			}
		});

		return fccSegInventory;

	}

	@Override
	public void updateBCInvetory(Collection<FCCSegBCInventory> fccSegBCInv) {
		hibernateSaveOrUpdateAll(fccSegBCInv);

	}

	@Override
	public FCCSegBCInventory getFCCSegBCInventoryForBC(Integer fccsbInvId, String bookingCode) {
		// String hqlBCInventories = "SELECT FSBI FROM FCCSegBCInventory
		return (FCCSegBCInventory) getSession().createQuery(
				"select FSBI from  FCCSegInventory FSI, FCCSegBCInventory FSBI where FSI.fccsInvId = FSBI.fccsInvId and FSI.flightSegId = "
						+ fccsbInvId + "  and FSBI.bookingCode = '" + bookingCode + "'")
				.uniqueResult();
	}

	private void setDefaultAvsThresholds(Integer AVS_CLOSURE_THRESHOLD, Integer AVS_AVAIL_THRESHOLD) {
		log.error("Gds loading failed, setting default AVS thresholds\nLower Threshold: "
				+ AirinventoryCustomConstants.AVS_CLOSURE_THRESHOLD + "\nUpper Threshold: "
				+ AirinventoryCustomConstants.AVS_AVAIL_THRESHOLD);

		AVS_CLOSURE_THRESHOLD = AirinventoryCustomConstants.AVS_CLOSURE_THRESHOLD;
		AVS_AVAIL_THRESHOLD = AirinventoryCustomConstants.AVS_AVAIL_THRESHOLD;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setupBCStatusUpdate() throws ModuleException {

		String hql = "select PA from PublishAvailability as PA where PA.eventCode in (:activ, :inac) and PA.eventStatus = :new order by PA.bookingCode, PA.eventTimestamp asc";

		Query query = getSession().createQuery(hql.toString());

		query.setString("activ", AirinventoryCustomConstants.PublishInventoryEventCode.AVS_ACTIVATE_BC.getCode());
		query.setString("inac", AirinventoryCustomConstants.PublishInventoryEventCode.AVS_INACTIVATE_BC.getCode());
		query.setString("new", AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT.getCode());

		try {
			List<PublishAvailability> publishAvailabilities = (List<PublishAvailability>) query.list();

			if (publishAvailabilities == null || publishAvailabilities.size() == 0) {
				return;
			}

			PublishAvailability previousPublishAvailability = null;
			String previousBookingCode = null;
			int bcCount = 0;

			for (PublishAvailability currentPublishAvailability : publishAvailabilities) {

				String tempBookingCode = currentPublishAvailability.getBookingCode();

				if (previousBookingCode == null) {
					bcCount++;
				}

				else if (tempBookingCode.contentEquals(previousBookingCode)) {
					setEventStatusToPIGAndUpdate(previousPublishAvailability);
					bcCount++;
				} else {

					if (bcCount % 2 == 0) { // if even number of BC entries then all cancels out each other
						setEventStatusToPIGAndUpdate(previousPublishAvailability);
					}

					bcCount = 1; // for next Booking Code
				}

				previousPublishAvailability = currentPublishAvailability;
				previousBookingCode = tempBookingCode;
			}

			// for last Booking Code which comes out of loop
			if (bcCount % 2 == 0) {
				setEventStatusToPIGAndUpdate(previousPublishAvailability);
			}

		} catch (Exception e) {
			throw new ModuleException("gdsservices.booking.class.status.publish.setup.error", e);
		}

	}

	private void setEventStatusToPIGAndUpdate(PublishAvailability publishAvailability) {
		publishAvailability
				.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.EVENT_PUBLISH_IGNORED.getCode());
		saveOrUpdatePublishAvailability(publishAvailability);
	}

	public void publishGdsFccSegBcInventory(List<GdsPublishFccSegBcInventoryTo> gdsPublishFccSegBcInventoryTos) {

		for (GdsPublishFccSegBcInventoryTo gdsPublishFccSegBcInventoryTo : gdsPublishFccSegBcInventoryTos) {

			PublishAvailability publishAvailability = new PublishAvailability();
			publishAvailability.setFccsaId(gdsPublishFccSegBcInventoryTo.getFccsInvId());
			// publishAvailability.setFlightSegId(getFlightSegId());

			publishAvailability.setBookingCode(gdsPublishFccSegBcInventoryTo.getBookingClass());
			if (gdsPublishFccSegBcInventoryTo.getFccsbInvId() != 0) {
				publishAvailability.setFccsbaId(gdsPublishFccSegBcInventoryTo.getFccsbInvId());
			}
			// publishAvailability.setBcStatus(getStatus());

			publishAvailability.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT.getCode());
			publishAvailability.setEventTimestamp(new Date());
			publishAvailability.setGdsID(gdsPublishFccSegBcInventoryTo.getGdsId());
			publishAvailability.setEventCode(gdsPublishFccSegBcInventoryTo.getEventCode());

			publishAvailability.setSegAvailableSeats(0);
			publishAvailability.setBcAvailableSeats(0);

			saveOrUpdatePublishAvailability(publishAvailability);
		}
	}
}