package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.HashMap;

public interface FlightInventoryResJDBCDAO {

	public HashMap<String,Integer> getFCCSegInventoriesForUpdate(Collection<Integer> fccsInvIds, boolean includeInterceptingSegs, int waitSeconds);

	public HashMap<String,Integer> getFCCSegInventoryForUpdate(Integer fccsInvId, boolean includeInterceptingSegs, int waitSeconds);
}
