package com.isa.thinair.airinventory.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.inventory.LogicalCabinClassDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.LogicalCabinClassDAO;
import com.isa.thinair.airinventory.core.service.bd.LogicalCabinClassBDImpl;
import com.isa.thinair.airinventory.core.service.bd.LogicalCabinClassBDLocalImpl;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;

/*
 * If you are adding new methods that will return LogicalCabinClass or any object containing LogicalCabinClass please 
 * make sure that you call translateLogicalCabinClass(Collection<LogicalCabinClass> lccCol) or 
 * translateLogicalCabinClass(LogicalCabinClass lcc) before returning
 */
@Stateless
@RemoteBinding(jndiBinding = "LogicalCabinClassService.remote")
@LocalBinding(jndiBinding = "LogicalCabinClassService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Clustered
public class LogicalCabinClassServiceBean extends PlatformBaseSessionBean implements LogicalCabinClassBDLocalImpl,
		LogicalCabinClassBDImpl {

	private final Log log = LogFactory.getLog(getClass());

	private LogicalCabinClassDAO getLogicalCabinClassDAO() {
		return (LogicalCabinClassDAO) AirInventoryUtil.getInstance().getLocalBean("LogicalCabinClassDAOImplProxy");
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page<LogicalCabinClass> getLogicalCabinClass(LogicalCabinClassDTO searchParams, int startRec, int noRecs)
			throws ModuleException {
		Page<LogicalCabinClass> page = getLogicalCabinClassDAO().getLogicalCabinClass(searchParams, startRec, noRecs);
		translateLogicalCabinClass(page.getPageData());
		return page;
	}

	@Override
	public void saveTemplate(LogicalCabinClass logicalCabinClass, String chkNestShift) throws ModuleException {
		try {
			/*
			 * Checks if the internationalized fields are empty or not and send them to be created if empty. Otherwise
			 * they are sent to be updated. These will all be in English for now unless changed from the DB level
			 */
			if (logicalCabinClass.getDescriptionI18n().isEmpty() || logicalCabinClass.getFlexiDescriptionI18n().isEmpty()
					|| logicalCabinClass.getCommentsI18n().isEmpty() || logicalCabinClass.getFlexiCommentI18n().isEmpty()) {

				logicalCabinClass.setDescriptionI18n(I18nTranslationUtil.getI18nLogicalCabinClassKey(logicalCabinClass
						.getLogicalCCCode() + 1));
				logicalCabinClass.setFlexiDescriptionI18n(I18nTranslationUtil.getI18nLogicalCabinClassKey(logicalCabinClass
						.getLogicalCCCode() + 2));
				logicalCabinClass.setCommentsI18n(I18nTranslationUtil.getI18nLogicalCabinClassKey(logicalCabinClass
						.getLogicalCCCode() + 3));
				logicalCabinClass.setFlexiCommentI18n(I18nTranslationUtil.getI18nLogicalCabinClassKey(logicalCabinClass
						.getLogicalCCCode() + 4));

				AirInventoryModuleUtils.getCommonMasterBD().saveI18nMessageKeys(logicalCabinClass);
			} else {
				AirInventoryModuleUtils.getCommonMasterBD().updateI18nMessageKeys(logicalCabinClass);
			}

			getLogicalCabinClassDAO().saveTemplate(logicalCabinClass, chkNestShift);
			setOpenReturnBookingClassForLogicalCabinClass(logicalCabinClass);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Saving/Updating logical cabin class failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception e) {
			log.error("Saving/Updating logical cabin class failed.", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "Unable to save LogicalCabinClass");
		}
	}

	@Override
	public int deleteLogicalCabinClass(LogicalCabinClass logicalCabinClass) throws ModuleException {
		int result = getLogicalCabinClassDAO().deleteLogicalCabinClass(logicalCabinClass);

		/*
		 * Deletes the I18nMessageKey classes associated with this LogicalCabinCLass if the LogicalCabinClass delete is
		 * successful
		 */
		if (result == 0) {
			AirInventoryModuleUtils.getCommonMasterBD().deleteI18nMessages(logicalCabinClass);
		}
		return result;
	}

	@Override
	public String getDefaultLogicalCabinClass(String cabinClass) throws ModuleException {
		return getLogicalCabinClassDAO().getDefaultLogicalCabinClass(cabinClass);
	}

	@Override
	public LogicalCabinClass getLogicalCabinClass(String logicalCCCode) throws ModuleException {
		LogicalCabinClass lcc = getLogicalCabinClassDAO().getLogicalCabinClass(logicalCCCode);
		translateLogicalCabinClass(lcc);
		return lcc;
	}

	@Override
	public String getCabinClass(String logicalCCCode) throws ModuleException {
		return getLogicalCabinClassDAO().getCabinClass(logicalCCCode);
	}

	/** get all the logical cabin classes **/
	@Override
	public Map<String, LogicalCabinClass> getAllLogicalCabinClasses() throws ModuleException {
		Map<String, LogicalCabinClass> logicalCCs = new HashMap<String, LogicalCabinClass>();
		List<LogicalCabinClass> logicalCCList = getLogicalCabinClassDAO().getAllLogicalCabinClasses();
		translateLogicalCabinClass(logicalCCList);
		for (LogicalCabinClass logicalCC : logicalCCList) {
			logicalCCs.put(logicalCC.getLogicalCCCode(), logicalCC);
		}

		return logicalCCs;
	}

	@Override
	public Map<String, LogicalCabinClass> getLogicalCabinClasses(String cabinClass) throws ModuleException {
		Map<String, LogicalCabinClass> logicalCCs = new HashMap<String, LogicalCabinClass>();
		List<LogicalCabinClass> logicalCCList = getLogicalCabinClassDAO().getLogicalCabinClasses(cabinClass);
		translateLogicalCabinClass(logicalCCList);
		for (LogicalCabinClass logicalCC : logicalCCList) {
			logicalCCs.put(logicalCC.getLogicalCCCode(), logicalCC);
		}
		return logicalCCs;
	}

	/**
	 * {@inheritDoc}
	 */
	public Map<String, Map<String, String>> getTranslatedMessages() throws ModuleException {
		return AirInventoryModuleUtils.getCommonMasterBD().getTranslatedMessagesForLCC();
	}

	/**
	 * {@inheritDoc}
	 */
	public void saveTranslations(Map<String, Map<String, String>> translations) throws ModuleException {
		AirInventoryModuleUtils.getCommonMasterBD().saveTranslations(translations);
	}

	/**
	 * Convenient method to translate a single LogicalCabinClass. Delegates work to
	 * translateLogicalCabinClass(Collection<LogicalCabinClass> lccCol)
	 */
	private void translateLogicalCabinClass(LogicalCabinClass lcc) throws ModuleException {
		Collection<LogicalCabinClass> temp = new ArrayList<LogicalCabinClass>();
		temp.add(lcc);
		translateLogicalCabinClass(temp);
	}

	/**
	 * Accepts a Collection of LogicalCabinClass and sets the Description,FlexiDescription,Comment and FlexiComment from
	 * the 18n message keys. Currently setting only for English language since AirAdmin is not internationalized.
	 * 
	 * This is necessary since I18nMessageKey or I18nMessage entities cannot me used in the current module.
	 * 
	 * @param lccCol
	 *            Collection of LogicalCabinClass to be translated.
	 * @throws ModuleException
	 *             If any of the underlying operations throws an error.
	 */
	private void translateLogicalCabinClass(Collection<LogicalCabinClass> lccCol) throws ModuleException {
		Map<String, String> translatedMessages = AirInventoryModuleUtils.getCommonMasterBD().getTranslatedMessages(Locale.ENGLISH.toString(),
				getKeySet(lccCol));
		for (LogicalCabinClass lcc : lccCol) {
			lcc.setDescription(translatedMessages.get(lcc.getDescriptionI18n()));
			lcc.setFlexiDescription(translatedMessages.get(lcc.getFlexiDescriptionI18n()));
			lcc.setComments(translatedMessages.get(lcc.getCommentsI18n()));
			lcc.setFlexiComment(translatedMessages.get(lcc.getFlexiCommentI18n()));
		}
	}

	/**
	 * Extract the i18n message keys to a set of Strings from a Collection of LogicalCabinClass
	 * 
	 * @param lccCollection
	 *            Collection of LogicalCabinClass that the message keys will be extracted from
	 * @return The set of Strings containing the i18n message keys
	 */
	private Set<String> getKeySet(Collection<LogicalCabinClass> lccCollection) {
		Set<String> keySet = new HashSet<String>();
		for (LogicalCabinClass lcc : lccCollection) {
			keySet.add(lcc.getCommentsI18n());
			keySet.add(lcc.getDescriptionI18n());
			keySet.add(lcc.getFlexiCommentI18n());
			keySet.add(lcc.getFlexiDescriptionI18n());
		}
		return keySet;
	}

	// This method will set open return booking class for given logical cabin class
	private void setOpenReturnBookingClassForLogicalCabinClass(LogicalCabinClass logicalCabinClass) {
		BookingClassDAO bookingClassDAO = AirInventoryModuleUtils.getBookingClassDAO();

		try {
			BookingClass openRTBookingClass = bookingClassDAO.getOpenReturnBookingClass(logicalCabinClass.getLogicalCCCode());

			// If open return booking class not exists, add a new booking class
			if (openRTBookingClass == null) {
				String bookingClassCode = BookingClass.BookingClassType.OPEN_RETURN + logicalCabinClass.getLogicalCCCode();
				String description = "Open Return " + logicalCabinClass.getLogicalCCCode();
				BookingClass bookingClass = new BookingClass(bookingClassCode, description,
						logicalCabinClass.getCabinClassCode(), BookingClass.FIXED_FLAG_N,
						BookingClass.BookingClassType.OPEN_RETURN, BookingClass.BC_TYPE_ID_OPENRT, BookingClass.ON_HOLD_N,
						BookingClass.Status.ACTIVE, logicalCabinClass.getLogicalCCCode(), BookingClass.PassengerType.ADULT,
						BookingClass.AllocationType.COMBINED, AirPricingCustomConstants.FareCategoryType.ANY,
						BookingClass.RELEASE_FLAG_Y, 0);
				bookingClassDAO.saveBookingClass(bookingClass);
			}
		} catch (CommonsDataAccessException e) {
			log.error("Invalid open return booking classes exists. Please check, ", e);
		}
	}
}