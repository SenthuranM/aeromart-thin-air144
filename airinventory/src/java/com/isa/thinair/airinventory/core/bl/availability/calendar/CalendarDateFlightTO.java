package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.platform.core.commons.exception.PlatformRuntimeException;

class CalendarDateFlightTO {

	private ArrayList<AvailableONDFlight> flights = null;
	private Set<String> distinctFltOnds = null;
	private Map<String, List<String>> subOndCodesMap = null;
	private Map<String, List<AvailableONDFlight>> ondwiseONDFlights = null;

	public void addFlights(AvailableONDFlight flight) {
		getFlights().add(flight);
	}

	public List<AvailableONDFlight> getFlightFor(String ond) {
		return ondwiseONDFlights.get(ond);
	}

	public List<AvailableONDFlight> getFlightFor(String ond, Collection<String> subOndCodes) {
		List<AvailableONDFlight> flights = ondwiseONDFlights.get(ond);
		List<AvailableONDFlight> matchingFlights = new ArrayList<AvailableONDFlight>();
		for (AvailableONDFlight ondFlight : flights) {
			if (ondFlight.getSubONDCodes().containsAll(subOndCodes) && subOndCodes.containsAll(ondFlight.getSubONDCodes())) {
				matchingFlights.add(ondFlight);
			}
		}
		return matchingFlights;
	}

	public List<AvailableONDFlight> getFlights() {
		if (this.flights == null) {
			this.flights = new ArrayList<AvailableONDFlight>();
		}
		return this.flights;
	}

	public Set<String> getDistinctFlightOnds() {
		processFlights();
		return this.distinctFltOnds;
	}

	public boolean hasSubONDsFor(String ondCode) {
		return subOndCodesMap.containsKey(ondCode);
	}

	public List<String> getSubONDCodes(String ondCode) {
		return subOndCodesMap.get(ondCode);
	}

	private void processFlights() {
		if (this.distinctFltOnds == null) {
			this.distinctFltOnds = new HashSet<String>();
			this.subOndCodesMap = new HashMap<String, List<String>>();
			this.ondwiseONDFlights = new HashMap<String, List<AvailableONDFlight>>();
			for (AvailableONDFlight flt : getFlights()) {
				distinctFltOnds.add(flt.getOndCode());

				if (!ondwiseONDFlights.containsKey(flt.getOndCode())) {
					ondwiseONDFlights.put(flt.getOndCode(), new ArrayList<AvailableONDFlight>());
				}
				ondwiseONDFlights.get(flt.getOndCode()).add(flt);

				if (!flt.isDirectOnd()) {
					if (subOndCodesMap.containsKey(flt.getOndCode())
							&& (!flt.getSubONDCodes().containsAll(subOndCodesMap.get(flt.getOndCode())) || !subOndCodesMap.get(
									flt.getOndCode()).containsAll(flt.getSubONDCodes()))) {
						throw new PlatformRuntimeException("Unhandled condition of collusion ond for sub onds ");
					} else {
						subOndCodesMap.put(flt.getOndCode(), flt.getSubONDCodes());
					}
				}
			}
		}
	}
}