package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author MN
 * 
 */
public class FilterBucketsResultSetExtractor implements ResultSetExtractor {

	private final BucketFilteringCriteria bucketFilteringCriteria;

	public FilterBucketsResultSetExtractor(BucketFilteringCriteria filteringCriteria) {
		this.bucketFilteringCriteria = filteringCriteria;
	}

	/**
	 * Iteratre through a set of bc inventory summaries result set and filters out non-fixed bucket(s) and fixed bucket
	 */
	@Override
	public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
		FilteredBucketsDTO filteredBucketsDTO = AirInventoryDataExtractUtil.extractFilteredBucketsDTO(resultSet,
				bucketFilteringCriteria, AppSysParamsUtil.showNestedAvailableSeats());
		return filteredBucketsDTO;
	}

}
