package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.List;

/**
 * @author ISuru
 */
public interface FlightInventoryResDAO {

	/**
	 * Filter out those flights whose soldSeats > 0 or onholdSeats > 0 or cancelledSeats > 0
	 * 
	 * @param flightIds
	 *            List of flight Ids
	 * @return List flightIds having one or more reservations, onhold reservations or cancelled reservations
	 */
	public List<Integer> filterFlightsHavingReservations(Collection<Integer> flightIds, String cabinClassCode);

	/**
	 * Filter out those flight segments whose soldSeats > 0 or onholdSeats > 0 or cancelledSeats > 0
	 * 
	 * @param flightSegIds
	 * @param cabinClassCode
	 * @return
	 */
	public List<Integer> filterFlightSegsHavingReservations(Collection<Integer> flightSegIds, String cabinClassCode);

	public boolean isFlightHasReservations(Integer flightId);

	public List<Integer> filterFlightsHavingWaitlistedReservations(Collection<Integer> flightIds, String cabinClassCode);
}
