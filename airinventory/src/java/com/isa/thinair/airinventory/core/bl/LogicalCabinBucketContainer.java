package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.LogicalCabinFilteredBucketDTO;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class LogicalCabinBucketContainer {
	private Map<String, List<LogicalCabinFilteredBucketDTO>> logicalCabinWiseFilteredBuckets = new HashMap<String, List<LogicalCabinFilteredBucketDTO>>();

	private String lowestLogicalCabin = null;

	private double lowestFare = -1;

	public void addLogicalCabinBucket(double logicalCabinClassTotalFare,
			LogicalCabinFilteredBucketDTO logicalCabinFilteredBucketDTO) {
		if (logicalCabinClassTotalFare > -1) {
			String logicalCc = logicalCabinFilteredBucketDTO.getLogicalCabinClass();

			if (lowestFare == -1 || lowestFare > logicalCabinClassTotalFare) {
				lowestFare = logicalCabinClassTotalFare;
				lowestLogicalCabin = logicalCc;
			}

			if (!logicalCabinWiseFilteredBuckets.containsKey(logicalCc)) {
				logicalCabinWiseFilteredBuckets.put(logicalCc, new ArrayList<LogicalCabinFilteredBucketDTO>());
			}

			logicalCabinWiseFilteredBuckets.get(logicalCc).add(logicalCabinFilteredBucketDTO);
		}
	}

	public void addLogicalCabinBuckets(double logicalCabinClassTotalFare, String logicalCc,
			List<LogicalCabinFilteredBucketDTO> logicalCabinFilteredBucketDTOs) {
		if (logicalCabinClassTotalFare > -1) {

			if (lowestFare == -1 || lowestFare > logicalCabinClassTotalFare) {
				lowestFare = logicalCabinClassTotalFare;
				lowestLogicalCabin = logicalCc;
			}

			logicalCabinWiseFilteredBuckets.put(logicalCc, logicalCabinFilteredBucketDTOs);
		}
	}

	@Deprecated
	public List<LogicalCabinFilteredBucketDTO> getCheapestLogicalCabinBuckets() {
		if (lowestLogicalCabin == null) {
			return null;
		}
		return logicalCabinWiseFilteredBuckets.get(lowestLogicalCabin);
	}

	public List<LogicalCabinFilteredBucketDTO> getLogicalCabinBuckets(String logicalCC) {
		if (logicalCC != null && logicalCabinWiseFilteredBuckets.containsKey(logicalCC)) {
			return logicalCabinWiseFilteredBuckets.get(logicalCC);
		}
		return null;
	}

	public double getCheapestFare() {
		return lowestFare;
	}

	public Set<String> getAvailableCommonLogicalCabinClasses() {
		return logicalCabinWiseFilteredBuckets.keySet();
	}

	@Deprecated
	public LogicalCabinFilteredBucketDTO getCheapestBucketForReturn() {
		return getCheapestLogicalCabinBuckets().get(0);
	}

	public boolean isCheapestLogicalCabin(String logicalCabin) {
		return (lowestLogicalCabin != null && lowestLogicalCabin.equalsIgnoreCase(logicalCabin));
	}

	@Deprecated
	public LogicalCabinFilteredBucketDTO getCheapestBucketForConnection() {
		return getCheapestLogicalCabinBuckets().get(0);
	}

	public List<LogicalCabinFilteredBucketDTO> getMinimumForAllLogicalCabinBucketsForConnection() {
		return getMinimumForAllLogicalCabinBucketsForSegment(0);
	}

	public List<LogicalCabinFilteredBucketDTO> getMinimumForAllLogicalCabinBucketsForReturn() {
		return getMinimumForAllLogicalCabinBucketsForSegment(0);
	}

	public List<LogicalCabinFilteredBucketDTO> getMinimumForAllLogicalCabinBucketsForSegment(int ondFlight) {
		List<LogicalCabinFilteredBucketDTO> logicalCabinBuckets = new ArrayList<LogicalCabinFilteredBucketDTO>();
		for (String key : logicalCabinWiseFilteredBuckets.keySet()) {
			logicalCabinBuckets.add(logicalCabinWiseFilteredBuckets.get(key).get(ondFlight));
		}
		return logicalCabinBuckets;
	}

	public boolean isEmpty() {
		return logicalCabinWiseFilteredBuckets.size() == 0;
	}
}
