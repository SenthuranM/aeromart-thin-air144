package com.isa.thinair.airinventory.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.bl.FlightSeatBL;
import com.isa.thinair.airinventory.core.bl.SeatMapBL;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightSeatDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapJDBCDAO;
import com.isa.thinair.airinventory.core.service.bd.SeatMapBDImpl;
import com.isa.thinair.airinventory.core.service.bd.SeatMapBDLocalImpl;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author indika
 */
@Stateless
@RemoteBinding(jndiBinding = "SeatMapService.remote")
@LocalBinding(jndiBinding = "SeatMapService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SeatMapServiceBean extends PlatformBaseSessionBean implements SeatMapBDImpl, SeatMapBDLocalImpl {

	private final Log log = LogFactory.getLog(getClass());

	private SeatMapBL seatMapBL;

	private FlightSeatBL flightSeatBL;

	/**
	 * @param flightSegID
	 * @return FlightSeatsDTO
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightSeatsDTO getFlightSeats(int flightSegID) throws ModuleException {
		return getSeatMapJDBCDAO().getFlightSeats(flightSegID, "en", null, null, false);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, FlightSeatsDTO> getFlightSeats(List<Integer> flightSegIds, String locale, boolean loadSoicalIdentity) throws ModuleException {

		Map<Integer, FlightSeatsDTO> flightSegFlightSeatMap = new HashMap<Integer, FlightSeatsDTO>();

		for (Integer flightSegId : flightSegIds) {
			FlightSeatsDTO flightSeatsDto = getFlightSeatsDTO(flightSegId, locale, null, null, loadSoicalIdentity);
			flightSegFlightSeatMap.put(flightSegId, flightSeatsDto);
		}

		return flightSegFlightSeatMap;
	}

	private FlightSeatsDTO getFlightSeatsDTO(Integer flightSegId, String locale, String cabinClass, String logicalCC, boolean loadSoicalIdentity)
			throws ModuleException {
		FlightSeatsDTO flightSeatsDto = getSeatMapJDBCDAO().getFlightSeats(flightSegId, locale, cabinClass, logicalCC, loadSoicalIdentity);
		// Checking with the intercepting flight segs seats for multileg flights
		if (!flightSeatsDto.isFlightFull()) {
			List<Integer> sourceFltSegIds = new ArrayList<Integer>();
			sourceFltSegIds.add(flightSegId);
			List<Integer> interceptingFlightSegIDs = getFlightInventoryJDBCDAO().getInterceptingSegmentIDs(sourceFltSegIds);
			if (interceptingFlightSegIDs != null && interceptingFlightSegIDs.size() > 0) {
				for (Integer interceptingFlightSegId : interceptingFlightSegIDs) {
					Map<String, String> interceptingSeatStatusMap = getSeatMapJDBCDAO().getSeatStatus(interceptingFlightSegId,
							locale);
					for (SeatDTO seat : flightSeatsDto.getSeats()) {
						if (seat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {
							String status = interceptingSeatStatusMap.get(seat.getSeatCode());
							if (status != null && status.equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)) {
								seat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
							}
						}
					}
				}
			}
		}
		return flightSeatsDto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<SeatDTO> getFlightSeatsByChargeTemplate(Integer chargeTemplateID) throws ModuleException {
		return chargeTemplateID == null ? new ArrayList<SeatDTO>() : getSeatMapJDBCDAO().getFlightSeatsByChargeTemplate(
				chargeTemplateID);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, FlightSeatsDTO> getFlightSeats(Map<Integer, ClassOfServiceDTO> flightSegIds, String localeCode)
			throws ModuleException {
		Map<Integer, FlightSeatsDTO> flightSegFlightSeatMap = new HashMap<Integer, FlightSeatsDTO>();

		for (Entry<Integer, ClassOfServiceDTO> flightEntry : flightSegIds.entrySet()) {
			Integer flightSegId = flightEntry.getKey();
			String cabinClass = null;
			String logicalCC = null;

			ClassOfServiceDTO cosDTO = flightEntry.getValue();
			if (cosDTO != null) {
				cabinClass = cosDTO.getCabinClassCode();
				logicalCC = cosDTO.getLogicalCCCode();
			}

			FlightSeatsDTO flightSeatsDto = getFlightSeatsDTO(flightSegId, localeCode, cabinClass, logicalCC, false);
			flightSegFlightSeatMap.put(flightSegId, flightSeatsDto);
		}

		return flightSegFlightSeatMap;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, FlightSeatsDTO> getFlightSeatsRow(Collection<Integer> flightSegIds, String localeCode, int rowNo)
			throws ModuleException {

		Map<Integer, FlightSeatsDTO> flightSegFlightSeatMap = new HashMap<Integer, FlightSeatsDTO>();
		Iterator<Integer> flightSegIdIterater = flightSegIds.iterator();
		while (flightSegIdIterater.hasNext()) {
			Integer flightSegId = flightSegIdIterater.next();
			FlightSeatsDTO flightSeatsDto = getSeatMapJDBCDAO().getFlightSeatsRow(flightSegId, localeCode, rowNo);
			flightSegFlightSeatMap.put(flightSegId, flightSeatsDto);
		}
		return flightSegFlightSeatMap;
	}

	/**
	 * @param flightSegId
	 * @param seatCodes
	 * @return Collection<Integer>
	 */
	@Override
	public Map<String, Integer> getFlightSeatIdMap(int flightSegId, Collection<String> seatCodes) throws ModuleException {
		Collection<SeatDTO> seats = getSeatMapJDBCDAO().getFlightSeats(flightSegId, seatCodes);
		Map<String, Integer> seatIdMap = new HashMap<String, Integer>();
		for (SeatDTO seat : seats) {
			seatIdMap.put(seat.getSeatCode(), seat.getFlightAmSeatID());
		}
		return seatIdMap;
	}

	/**
	 * @param flightSegID
	 * @return FlightSeatsDTO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce assignFlightSeatCharges(Collection<Integer> flightSegIds, int templateId, String modelNo,
			ChargeTemplate chargeTemplate) throws ModuleException {
		return getSeatMapBL().assignFlightSeatCharges(flightSegIds, templateId, getUserPrincipal(), modelNo,
				chargeTemplate.getSeatCharges());
	}

	@Override
	public ServiceResponce assignFlightSeatCharge(Collection<Integer> flightSegIds, Map<Integer, String> newSeatCharges)
			throws ModuleException {
		return getSeatMapBL().assignFlightSeatCharge(flightSegIds, newSeatCharges);
	}

	/**
	 * 
	 * 
	 * @param flightSegID
	 * @return collection of templateIDs
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FlightSeatsDTO> getTempleateIds(int flightId) throws ModuleException {
		return getSeatMapJDBCDAO().getTempleateIds(flightId);
	}

	/**
	 * 
	 * @param sourceFlightId
	 * @param destinationFlightIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce assignFlightSeatChargesRollFoward(int sourceFlightId, Collection<Integer> destinationFlightIds,
			String modelNo) throws ModuleException {
		return getSeatMapBL()
				.assignFlightSeatChargesRollFoward(sourceFlightId, destinationFlightIds, getUserPrincipal(), modelNo);
	}

	/**
	 * 
	 * @param flightSeatDTOs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce assignFlightSeatCharges(Collection<FlightSeatsDTO> flightSeatDTOs, String modelNo)
			throws ModuleException {
		return getSeatMapBL().assignFlightSeatCharges(flightSeatDTOs, getUserPrincipal(), modelNo);
	}

	/**
	 * 
	 * @param flightSeatDTOs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce copyTemplateAsNew(int oldTemplateId, Collection<SeatCharge> seatCharges, String newTemplateCode,
			int flightSegId) throws ModuleException {
		return getSeatMapBL().copyTemplateAsNew(oldTemplateId, seatCharges, newTemplateCode, flightSegId, getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce blockSeats(Map<Integer, String> flightAMSeatIdPaxType) throws ModuleException {
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(true);
		try {
			getFlightSeatBL().updateFlightSeats(flightAMSeatIdPaxType, AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED,
					getUserPrincipal(), null);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			log.error(" block flight am Seats failed", e);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(ResponseCodes.SEAT_BLOCK_ERROR, e.getMessageString());
			return sr;
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error(" block flight am Seats failed", e);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(ResponseCodes.SEAT_BLOCK_ERROR, "block seat run time error");
			return sr;
		}
		return defaultServiceResponse;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void vacateSeats(Collection<Integer> flightAMSeatIds) throws ModuleException {
		getFlightSeatBL().updateFlightSeats(SMUtil.adaptToAHashMap(flightAMSeatIds),
				AirinventoryCustomConstants.FlightSeatStatuses.VACANT, getUserPrincipal(), null);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifySeat(Map<Integer, String> flightAmSeatIdsPaxTypeToCNF, Collection<Integer> flightAMSeatIdsToCancell)
			throws ModuleException {
		if (flightAMSeatIdsToCancell != null && flightAMSeatIdsToCancell.size() > 0) {
			vacateSeats(flightAMSeatIdsToCancell);
		}

		if (flightAmSeatIdsPaxTypeToCNF != null && flightAmSeatIdsPaxTypeToCNF.size() > 0) {
			getFlightSeatBL().updateFlightSeats(flightAmSeatIdsPaxTypeToCNF,
					AirinventoryCustomConstants.FlightSeatStatuses.RESERVED, getUserPrincipal(), null);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updatePaxType(Map<Integer, String> flightSeatIdPaxType, Map<Integer, Integer> paxFlightSeatSwapMap)
			throws ModuleException {
		getFlightSeatBL().updatePaxTypes(flightSeatIdPaxType, paxFlightSeatSwapMap);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void checkSeatingRules(Map<Integer, String> flightSeatIdPaxType, Collection<Integer> excludeCheckSeatIDs)
			throws ModuleException {
		getFlightSeatBL().checkSeatingRules(flightSeatIdPaxType, excludeCheckSeatIDs);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce releaseBlockedSeats(Collection<Integer> flightAMSeatIds) throws ModuleException {
		FlightSeatDAO seatDAO = AirInventoryModuleUtils.getFlightSeatDAO();
		Collection<FlightSeat> dbflightAmSeats = seatDAO.getFlightSeats(flightAMSeatIds);
		Iterator<FlightSeat> iterdbFlightASMseats = dbflightAmSeats.iterator();
		while (iterdbFlightASMseats.hasNext()) {
			FlightSeat flightSeat = iterdbFlightASMseats.next();
			if (flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)) {
				log.error("releaseBlockedSeats faiiled airinventory.seat.resrerved");
				throw new ModuleException("airinventory.seat.resrerved");
			}
		}

		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(true);

		try {
			getFlightSeatBL().updateFlightSeats(SMUtil.adaptToAHashMap(flightAMSeatIds),
					AirinventoryCustomConstants.FlightSeatStatuses.VACANT, getUserPrincipal(), null);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			log.error("releaseBlockedSeats faiiled" + e);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(ResponseCodes.SEAT_BLOCK_ERROR, e.getMessageString());
			return sr;
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("releaseBlockedSeats faiiled" + e);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(ResponseCodes.SEAT_BLOCK_ERROR, "release seat run time error-consult vendor");
			return sr;
		}
		return defaultServiceResponse;

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce releaseBlockedSeats() throws ModuleException {
		return getSeatMapBL().releaseBlockedSeats(getUserPrincipal());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(600)
	public ServiceResponce updateTemplateFromFlight(int flitghtSegID, Collection<SeatCharge> colSeatCharges, String modelNo)
			throws ModuleException {
		return getSeatMapBL().updateTemplateFromFlight(flitghtSegID, colSeatCharges, modelNo);
	}

	@SuppressWarnings("unused")
	private SeatMapDAO getSeatMapDAO() {
		return (SeatMapDAO) AirInventoryUtil.getInstance().getLocalBean("SeatMapDAO");
	}

	private SeatMapJDBCDAO getSeatMapJDBCDAO() {
		return (SeatMapJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("seatMapJDBCDAO");
	}

	private static FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance().getLocalBean(
				"flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	private SeatMapBL getSeatMapBL() {
		if (seatMapBL == null) {
			seatMapBL = new SeatMapBL();
		}
		return seatMapBL;
	}

	private FlightSeatBL getFlightSeatBL() {
		if (flightSeatBL == null) {
			flightSeatBL = new FlightSeatBL();
		}
		return flightSeatBL;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer getFlightSeatID(Integer flightSegmentId, String seatCode) {
		return getFlightSeatBL().getFlightSeatID(flightSegmentId, seatCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Integer getFlightAmSeatID(Integer flightSegmentId, String seatCode) {
		FlightSeatDAO seatDAO = AirInventoryModuleUtils.getFlightSeatDAO();
		return seatDAO.getFlightAmSeatID(flightSegmentId, seatCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFlightSeats(Collection<Integer> flightSegIds) throws ModuleException {
		getFlightSeatBL().deleteFlightSeats(flightSegIds);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<SeatDTO>> getSeatInfoForSeatChargeTemplates(
			Map<Integer, Integer> flightSegIDWiseOfferTemplateIDs) throws ModuleException {

		Map<Integer, Collection<SeatDTO>> seatCharges = new HashMap<Integer, Collection<SeatDTO>>();

		for (Entry<Integer, Integer> templateIDEntry : flightSegIDWiseOfferTemplateIDs.entrySet()) {
			seatCharges.put(templateIDEntry.getKey(), templateIDEntry.getValue() == null
					? new ArrayList<SeatDTO>()
					: getSeatMapJDBCDAO().getFlightSeatsByChargeTemplate(templateIDEntry.getValue()));
		}
		return seatCharges;
	}
}
