/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.FlightTo;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface MealJDBCDAO {

	public List<FlightMealDTO> getMealDTOs(int flightSegID, String cabinClass, String logicalCabinClass,
			Set<String> selectedMeals, PnrFarePassenger pnrFarePax, int salesChannel) throws ModuleException;

	public Collection<FlightMealDTO> getTempleateIds(int flightId, String cos) throws ModuleException;

	public Collection<FlightMealDTO> getSoldMeals(int flightSegID) throws ModuleException;

	public Collection<FlightMealDTO> getAvailbleMealDTOs(int flightSegID, Collection<Integer> mealIds) throws ModuleException;

	public List<FlightMealDTO> getMealDTOsWithTranslations(int flightSegID, String cabinClass, String logicalCabinClass,
			String selectedLanguage, Set<String> selectedMeals) throws ModuleException;

	/**
	 * Retrieves Meal information from the Meal charge template directly. No associated flights are necessary.
	 * 
	 * @param templateID
	 *            Meal charge template ID of the meals to be retrieved.
	 * @param cabinClass
	 *            TODO
	 * @param logicalCabinClass
	 *            TODO
	 * @param segmentID
	 *            TODO
	 * @return The Meals attached to the meal charge template. Or an empty collection if the templateID is invalid.
	 * @throws ModuleException
	 *             If any of the underlying data access operations fail.
	 */
	public List<FlightMealDTO> getMealsByTemplate(Integer templateID, String cabinClass, String logicalCabinClass,
			Integer segmentID, Set<String> existingMeals, int salesChannel) throws ModuleException;

	/**
	 * Retrieves Meal information from the Meal charge template directly along with their translations. No associated
	 * flights are necessary.
	 * 
	 * @param templateID
	 *            Meal charge template ID of the meals to be retrieved.
	 * @param cabinClass
	 *            TODO
	 * @param logicalCabinClass
	 *            TODO
	 * @param selectedLanguage
	 *            TODO
	 * @param segmentID
	 *            TODO
	 * @return The Meals attached to the meal charge template. Or an empty collection if the templateID is invalid.
	 * @throws ModuleException
	 *             If any of the underlying data access operations fail.
	 */
	public List<FlightMealDTO> getMealsByTemplateWithTranslations(Integer templateID, String cabinClass, String logicalCabinClass,
			String selectedLanguage, Integer segmentID, Set<String> existingMeals, int salesChannel);

	public FlightMealDTO checkIfMealExistsForFlight(Integer flightSegID, Integer mealChargeID);

	public FlightMealDTO getMealChargeDetails(Integer templateID, String mealCode);

	public List<FlightTo> getMealRecords(List<FlightTo> flights);

	public Map<Integer, Set<String>> getExistingMeals(List<Integer> pnrSegIds) throws ModuleException;

	public List<FlightMealDTO> getMealDTOs() throws ModuleException;
}
