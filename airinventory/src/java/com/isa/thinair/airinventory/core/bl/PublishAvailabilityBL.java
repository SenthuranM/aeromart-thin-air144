package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoriesExistsCriteria;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.dto.PubAvailStatusUpdateRQ;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.RetrievePubAvailCriteria;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.PublishAvailability;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventCode;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventStatus;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Mohamed Nasly
 * 
 *         BL class for handling AVS messages generation for flight events.
 */
public class PublishAvailabilityBL {

	private FlightInventoryDAO flightInventoryDAO;
	private BookingClassDAO bookingClassDAO;
	private FlightBD flightBD;
	private final Log log = LogFactory.getLog(getClass());
	
	public PublishAvailabilityBL() {
		flightInventoryDAO = AirInventoryModuleUtils.getFlightInventoryDAO();
		bookingClassDAO = AirInventoryModuleUtils.getBookingClassDAO();
		flightBD = AirInventoryModuleUtils.getFlightBD();
	}

	/**
	 * Handles flight(s) events
	 * 
	 * @param notifyFlightEventsRQ
	 * @throws ModuleException
	 */
	public void notifyFlightEvent(NotifyFlightEventsRQ notifyFlightEventsRQ) throws ModuleException {
		if (AirinventoryUtils.getAirInventoryConfig().isPublishAvailability()) {
			if (notifyFlightEventsRQ != null && notifyFlightEventsRQ.getNotifyFlightEventRQCol() != null) {
				for (NotifyFlightEventRQ notifyFlightEventRQ : notifyFlightEventsRQ.getNotifyFlightEventRQCol()) {
					FlightEventCode flightEventCode = notifyFlightEventRQ.getFlightEventCode();
					if (flightEventCode != null) {
						switch (flightEventCode) {
						case FLIGHT_CREATED:
							actionCreateFlightOrPublishToGDSEvent(notifyFlightEventRQ,
									PublishInventoryEventCode.AVS_CREATE_FLIGHT);
							break;
						case FLIGHT_GDS_PUBLISHED_CHANGED:
							actionFlightGDSPublishedChangedEvent(notifyFlightEventRQ);
							break;
						case FLIGHT_CANCELLED:
							actionFlightClosedOrCancelledOrUnpublishedEvent(notifyFlightEventRQ,
									PublishInventoryEventCode.AVS_CANCEL_FLIGHT);
							break;
						case FLIGHT_CLOSED:
							actionFlightClosedOrCancelledOrUnpublishedEvent(notifyFlightEventRQ,
									PublishInventoryEventCode.AVS_FLIGHT_CLOSED);
							break;
						case FLIGHT_REOPENED:
							actionCreateFlightOrPublishToGDSEvent(notifyFlightEventRQ,
									PublishInventoryEventCode.AVS_FLIGHT_REOPEN);
							break;
						default:
							throw new ModuleException("", "");
						}
					} else {
						throw new ModuleException("", "");
					}
				}
			}
		}
	}

	/**
	 * Updates status of PublishAvailabilities
	 * 
	 * @param pubAvailStatusUpdateRQs
	 */
	public void publishAvailabiltyUpdateStatus(Collection<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQs) {
		if (pubAvailStatusUpdateRQs != null && pubAvailStatusUpdateRQs.size() > 0) {
			for (PubAvailStatusUpdateRQ pubAvailStatusUpdateRQ : pubAvailStatusUpdateRQs) {
				PublishAvailability publishAvailability = AirInventoryModuleUtils.getFlightInventoryDAO().getPublishAvailability(
						pubAvailStatusUpdateRQ.getPublishAvailabilityId());
				publishAvailability.setEventStatus(pubAvailStatusUpdateRQ.getPublishInventoryEventStatus().getCode());

				AirInventoryModuleUtils.getFlightInventoryDAO().saveOrUpdatePublishAvailability(publishAvailability);
			}
		}
	}

	/**
	 * Retrieves PublishAvailabilities for publishing.
	 * 
	 * @return
	 */
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getAvailabilityInfoToPublish() {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getAvailabilityForPublishing();
	}

	/**
	 * Retrieves Publish Availabilities for publishing.
	 * 
	 * @return
	 */
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getBCUpdateAVSInfo() {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getBCStatusAVS();
	}

	/**
	 * setup Publish Availabilities for publishing. Eliminate out-dated entries
	 * 
	 * @return
	 */
	public void setupBCUpdateAVSInfo() throws ModuleException {
		AirInventoryModuleUtils.getFlightInventoryDAO().setupBCStatusUpdate();
	}

	/**
	 * Handles Add/Remove GDS(s) from flight(s) events
	 * 
	 * @param pubAvailStatusUpdateRQs
	 * @throws ModuleException
	 */
	private void actionFlightGDSPublishedChangedEvent(NotifyFlightEventRQ notifyFlightEventRQ) throws ModuleException {
		if (notifyFlightEventRQ.getGdsIdsAdded() != null) {
			actionCreateFlightOrPublishToGDSEvent(notifyFlightEventRQ, PublishInventoryEventCode.AVS_CREATE_FLIGHT);
		}

		if (notifyFlightEventRQ.getGdsIdsRemoved() != null) {
			actionFlightClosedOrCancelledOrUnpublishedEvent(notifyFlightEventRQ, PublishInventoryEventCode.AVS_UNPUBLISED_FLIGHT);
		}
	}

	/**
	 * Handles Flight closed/cencelled events
	 * 
	 * @param notifyFlightEventRQ
	 * @param publishInventoryEventCode
	 */
	private void actionFlightClosedOrCancelledOrUnpublishedEvent(NotifyFlightEventRQ notifyFlightEventRQ,
			PublishInventoryEventCode publishInventoryEventCode) {

		// Criteria for checking segment has GDS allocations
		FCCSegBCInventoriesExistsCriteria fccSegBCAllocExistsCriteria = new FCCSegBCInventoriesExistsCriteria();
		fccSegBCAllocExistsCriteria.setIsGDSTypeOnly(true);

		// Criteria for filtering already not published messages
		RetrievePubAvailCriteria retrievePubAvailCriteria = new RetrievePubAvailCriteria();
		Collection<PublishInventoryEventStatus> eventStatuses = new ArrayList<PublishInventoryEventStatus>();
		eventStatuses.add(PublishInventoryEventStatus.NEW_EVENT);
		eventStatuses.add(PublishInventoryEventStatus.EVENT_PUBLISH_FAILED);
		retrievePubAvailCriteria.setStatusCollection(eventStatuses);
		Map<Integer, Collection<BookingClass>> gdsBookingClasses = bookingClassDAO.getGDSBookingClasses(notifyFlightEventRQ
				.getGdsIdsRemoved());

		for (Integer flightId : notifyFlightEventRQ.getFlightIds()) {
			
			boolean publishAVS = true;

			if (PublishInventoryEventCode.AVS_UNPUBLISED_FLIGHT.equals(publishInventoryEventCode)) {
				publishAVS = isPublishAVS(flightId);
			}
			
			Collection<FCCSegInventory> segInventories = flightInventoryDAO.getFCCSegInventories(flightId);
			
			Set<String>publishedSegmentCodes= new HashSet<String>();
					
			for (FCCSegInventory segInventory : segInventories) {
				
				List<Integer> gdsListToPublish=new ArrayList<Integer>();
				
				// check if segment has atleast one GDS allocation
				fccSegBCAllocExistsCriteria.setFccSegInvId(segInventory.getFccsInvId());
				if (flightInventoryDAO.isFCCSegBCInventoriesExists(fccSegBCAllocExistsCriteria)) {

					// Invalidate the messages queued for publishing
					retrievePubAvailCriteria.setSegInvId(segInventory.getFccsInvId());
					Collection<PublishAvailability> pubAvailCol = flightInventoryDAO
							.getPublishAvailability(retrievePubAvailCriteria);
					if (pubAvailCol != null && pubAvailCol.size() > 0) {
						for (PublishAvailability publishAvailability : pubAvailCol) {
							publishAvailability.setEventStatus(PublishInventoryEventStatus.EVENT_PUBLISH_IGNORED.getCode());
							flightInventoryDAO.saveOrUpdatePublishAvailability(publishAvailability);
						}
					}
				}

				if (gdsBookingClasses != null) {
					
					for (Integer gdsId : gdsBookingClasses.keySet()) {
						
						
						for (BookingClass bookingClass : gdsBookingClasses.get(gdsId)) {							
							fccSegBCAllocExistsCriteria.setBookingCode(bookingClass.getBookingCode());
							// per GDS, if at least a single BC is published then add AVS entry and break
							if (flightInventoryDAO.isFCCSegBCInventoriesExists(fccSegBCAllocExistsCriteria)) {
								gdsListToPublish.add(gdsId);
								
								break;
							}
						
						}
						
						
					}
				}
				
				
				if (publishAVS && !publishedSegmentCodes.contains(segInventory.getSegmentCode())) { 
					// publish to relevant GDS: a single entry per segment
					// for closure/cancel/un_publish cases '/' is used to indicate all BC of the Segment
					for (Integer gdsIdToBePublished : gdsListToPublish) {
						AirInventoryModuleUtils.getFlightInventoryDAO().saveOrUpdatePubAvailForUpdateCreateAndCancel(
								segInventory, null, null, null, gdsIdToBePublished, false, false);
						
					}
					
					// logical SEGS also included in the segInventries above
					// so keep track of the published ones and ignore them (at multiple logical segment case)
					publishedSegmentCodes.add(segInventory.getSegmentCode());
				}
				
			
				
			}

			
		}
	}

	/**
	 * Handles Create flight(s) Or Add GDS(s) to flight(s) events
	 * 
	 * @param notifyFlightEventRQ
	 * @param publishInventoryEventCode
	 * @throws ModuleException
	 */
	private void actionCreateFlightOrPublishToGDSEvent(NotifyFlightEventRQ notifyFlightEventRQ,
			PublishInventoryEventCode publishInventoryEventCode) throws ModuleException {
		if (notifyFlightEventRQ.getFlightIds() != null && notifyFlightEventRQ.getGdsIdsAdded() != null) {
			boolean publishAVS = false;
			// Criteria for checking if segment already has GDS allocation
			FCCSegBCInventoriesExistsCriteria fccSegBCAllocExistsCriteria = new FCCSegBCInventoriesExistsCriteria();

			Map<Integer, Collection<BookingClass>> gdsBookingClasses = bookingClassDAO.getGDSBookingClasses(notifyFlightEventRQ
					.getGdsIdsAdded());

			if (gdsBookingClasses != null) {
				Collection<Integer> fltListToChgStatus = null; // The code does not support flight object
				for (Integer flightId : notifyFlightEventRQ.getFlightIds()) {
					Collection<FCCSegInventory> segInventories = flightInventoryDAO.getFCCSegInventories(flightId);
					for (FCCSegInventory segInventory : segInventories) {
						for (Integer gdsId : gdsBookingClasses.keySet()) {
							for (BookingClass bookingClass : gdsBookingClasses.get(gdsId)) {
								fccSegBCAllocExistsCriteria.setFccSegInvId(segInventory.getFccsInvId());
								fccSegBCAllocExistsCriteria.setBookingCode(bookingClass.getBookingCode());
								
								FCCSegBCInventory fccSegBCInventory = null;

								if (!flightInventoryDAO.isFCCSegBCInventoriesExists(fccSegBCAllocExistsCriteria)
										&& bookingClass.getLogicalCCCode().equals(segInventory.getLogicalCCCode())) {
									
									publishAVS=true; //create case, always publish AVS
									
									// Create zero allocation bc inventory
									fccSegBCInventory = AirinventoryUtils.createZeroAllocBCInventory(segInventory,
											bookingClass.getBookingCode());

									// For activating the flight (if the fligth is Created (CRE) status)
									Flight flight = flightBD.getFlight(segInventory.getFlightId());
									if (flight.getStatus().equals(FlightStatusEnum.CREATED.getCode())) {
										if (fltListToChgStatus == null) {
											fltListToChgStatus = new HashSet<Integer>();
										}
										fltListToChgStatus.add(flight.getFlightId());
									}
										
								} else if (bookingClass.getLogicalCCCode().equals(segInventory.getLogicalCCCode())){
									
									publishAVS = isPublishAVS(flightId);
									
									fccSegBCInventory = flightInventoryDAO.getFCCSegBCInventory(segInventory.getFlightSegId(), bookingClass.getBookingCode());
								}
								// Decided to not to send CC message when flight creation
								// Again decided uncomment below code block. CC message will be generated if a zero allocated BC is created. If a BC is already added 
								// to the inventory, relevant message should be generated based on the number available seats
								 if (fccSegBCInventory != null){
									if(publishAVS) {
										flightInventoryDAO.saveOrUpdatePubAvailForUpdateCreateAndCancel(segInventory, null,
												fccSegBCInventory, null, gdsId, false, false);
									}
								 }
															
							}
							
							
						}				
					}
				}
				if (fltListToChgStatus != null && fltListToChgStatus.size() > 0) {
					flightBD.changeFlightStatus(fltListToChgStatus, FlightStatusEnum.ACTIVE);
				}
			}
		}
	}

	private boolean isPublishAVS(Integer flightId) {
		boolean publishAVS = false;
		
		
		try {
			String flightStatus = AirInventoryModuleUtils.getFlightBD().getFlight(flightId).getStatus();
			if (flightStatus.contentEquals(FlightStatusEnum.ACTIVE.getCode())
					|| flightStatus.contentEquals(FlightStatusEnum.CREATED.getCode())) {
				publishAVS = true;// publish case (check whether flight is active or created)
			}
		} catch (Exception e) {
			// do nothing
		}
		return publishAVS;
	}
}
