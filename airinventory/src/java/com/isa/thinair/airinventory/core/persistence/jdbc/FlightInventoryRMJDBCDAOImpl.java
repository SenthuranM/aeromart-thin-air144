package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryRMTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryRMJDBCDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * RM related data retrievals.
 * 
 * @author Nasly
 * 
 */
public class FlightInventoryRMJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements FlightInventoryRMJDBCDAO {

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Retrieve BC inventory details for RM optimization alerting.
	 * 
	 * @param segInvIds
	 *            Collection<Integer>
	 * @return Map<Integer, Collection<FCCSegBCInventoryTO>>
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<FCCSegBCInventoryTO>> getInvenotriesForRMAlert(Collection<Integer> segInvIds) {

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		String query = getQuery(AirinventoryCustomConstants.RETRIEVE_INVENTORIES_FOR_RM_OPTIMIZATION,
				new String[] { getSqlInValuesStr(segInvIds) });

		log.debug("Retrieving Inventories for RM Alert [query=" + query + "]");

		Object data = template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Collection<FCCSegBCInventoryTO>> segBCInvMap = new LinkedHashMap<Integer, Collection<FCCSegBCInventoryTO>>();
				int lastSegInvId = -1;
				int currentSegInvId;
				Collection<FCCSegBCInventoryTO> currentBCInvCol = null;
				while (rs.next()) {
					currentSegInvId = rs.getInt("FCCSA_ID");
					if (lastSegInvId != currentSegInvId || lastSegInvId == -1) {
						currentBCInvCol = new LinkedList<FCCSegBCInventoryTO>();
						segBCInvMap.put(currentSegInvId, currentBCInvCol);
					}
					FCCSegBCInventoryTO bcInv = new FCCSegBCInventoryTO();
					bcInv.setFccsInvId(currentSegInvId);
					bcInv.setFccsbInvId(rs.getInt("FCCSBA_ID"));
					bcInv.setBookingClass(rs.getString("BOOKING_CODE"));
					bcInv.setAllocation(rs.getInt("ORIGINAL_ALLOC"));
					bcInv.setSoldSeats(rs.getInt("BC_SOLD"));
					bcInv.setOnholdSeats(rs.getInt("BC_OHD"));
					bcInv.setCancelledSeats(rs.getInt("CANCELLED_SEATS"));
					bcInv.setAvailable(rs.getInt("AVAILABLE_SEATS"));
					bcInv.setOptimizationStatus(rs.getString("OPTIMIZATION_STATUS"));
					bcInv.setThresholdStatus(rs.getString("THRESHOLD_STATUS"));
					bcInv.setVersion(rs.getLong("VERSION"));
					bcInv.setMinFare(AccelAeroCalculator.parseBigDecimal(rs.getDouble("MIN_FARE")));

					currentBCInvCol.add(bcInv);

					lastSegInvId = currentSegInvId;
				}
				return segBCInvMap;
			}

		});

		return (LinkedHashMap<Integer, Collection<FCCSegBCInventoryTO>>) data;
	}

	/**
	 * Retrieve flight details for optimization alerting.
	 * 
	 * @return Map<Integer, FCCSegBCInventoryRMTO> - Map<FCCSA_ID, FCCSegBCInventoryRMTO>
	 */
	public Map<Integer, FCCSegBCInventoryRMTO> getFlightsForRMAlert() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Object data = template.query(getQuery(AirinventoryCustomConstants.RETRIEVE_FIGHTS_FOR_RM_OPTIMIZATION),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						LinkedHashMap<Integer, FCCSegBCInventoryRMTO> results = new LinkedHashMap<Integer, FCCSegBCInventoryRMTO>();
						while (rs.next()) {
							FCCSegBCInventoryRMTO flightInventoryInfo = new FCCSegBCInventoryRMTO();

							flightInventoryInfo.setFccsaId(rs.getInt("FCCSA_ID"));
							flightInventoryInfo.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
							flightInventoryInfo.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
							flightInventoryInfo.setSegmentCode(rs.getString("SEGMENT_CODE"));
							flightInventoryInfo.setDepartureDateZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));

							results.put(flightInventoryInfo.getFccsaId(), flightInventoryInfo);
						}
						return results;
					}
				});

		return (LinkedHashMap<Integer, FCCSegBCInventoryRMTO>) data;
	}

}
