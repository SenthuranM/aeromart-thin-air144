package com.isa.thinair.airinventory.core.audit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;

public class AuditExternalDataProvider {

	private static final int MAX_CAPACITY_OF_CACHE = 5000;

	private static AuditExternalDataProvider auditExternalDataProvider;

	// <Integer, String> ==> <flightid, flightnumber>
	private final CacheForFlightNumber cachedFlightDetails = new CacheForFlightNumber(MAX_CAPACITY_OF_CACHE);

	// <Integer, FlightSegmentDTO> ==> <FlightSegmentID, FlightSegmentDTO>
	private final CacheForFlightSegment cachedFlightSegDetailsAgainstFltSegID = new CacheForFlightSegment(MAX_CAPACITY_OF_CACHE);

	// <Pair<Integer, String>, FlightSegmentDTO> ==> <Pair<FlightID, SegmentCode>, FlightSegmentDTO>
	private final CacheForFlightNumberPair cachedFlightSegDetailsAgainstFltID = new CacheForFlightNumberPair(
			MAX_CAPACITY_OF_CACHE);

	private final Object guardOne = new Object();

	private final Object guardTwo = new Object();

	private final Object guardThree = new Object();

	private AuditExternalDataProvider() {
		super();
	}

	public static AuditExternalDataProvider getInstance() {
		if (auditExternalDataProvider == null) {
			auditExternalDataProvider = new AuditExternalDataProvider();
		}
		return auditExternalDataProvider;
	}

	public String getCachedFlightNo(Integer flightID) throws ModuleException {

		if (cachedFlightDetails.get(flightID) == null) {
			synchronized (guardOne) {
				// double checking if previous thread already get this flight ID
				if (cachedFlightDetails.get(flightID) == null) {
					String flightNumber = AirInventoryModuleUtils.getFlightBD().getFlight(flightID).getFlightNumber();
					if (flightNumber != null) {
						cachedFlightDetails.put(flightID, flightNumber);
					}
				}
			}
		}

		return cachedFlightDetails.get(flightID);

	}

	public Map<Integer, String> getCachedFlightNoCollection(Collection<Integer> flightIDs) throws ModuleException {
		Collection<Integer> notCachedIDs = new HashSet<Integer>();
		Map<Integer, String> ret = new HashMap<Integer, String>();

		// filtering against cache
		for (Integer id : flightIDs) {
			if (cachedFlightDetails.get(id) == null) {
				notCachedIDs.add(id);
			} else {
				ret.put(id, cachedFlightDetails.get(id));
			}
		}

		if (notCachedIDs.size() > 0) {
			synchronized (guardOne) {
				// removing again if previous thread already get this flight IDs
				notCachedIDs.removeAll(cachedFlightDetails.keySet());
				List<Flight> flights = AirInventoryModuleUtils.getFlightBD().getLightWeightFlightDetails(notCachedIDs);
				for (Flight flight : flights) {
					cachedFlightDetails.put(flight.getFlightId(), flight.getFlightNumber());
					ret.put(flight.getFlightId(), flight.getFlightNumber());
				}
			}
		}
		return ret;
	}

	public FlightSegmentDTO getCachedFlightSegmentInfo(Integer flightSegmentID) throws ModuleException {

		if (cachedFlightSegDetailsAgainstFltSegID.get(flightSegmentID) == null) {
			synchronized (guardTwo) {
				// double checking if previous thread already get this flight ID
				if (cachedFlightSegDetailsAgainstFltSegID.get(flightSegmentID) == null) {
					Collection<Integer> ids = new ArrayList<Integer>();
					ids.add(flightSegmentID);
					Collection<FlightSegmentDTO> segment = AirInventoryModuleUtils.getFlightBD().getFlightSegments(ids);
					if (segment != null && segment.size() > 0) {
						cachedFlightSegDetailsAgainstFltSegID.put(flightSegmentID, segment.iterator().next());
					}
				}
			}
		}

		return cachedFlightSegDetailsAgainstFltSegID.get(flightSegmentID);
	}

	public Map<Integer, FlightSegmentDTO> getCachedFlightSegmentInfoCollection(Collection<Integer> flightSegmentIDs)
			throws ModuleException {

		Collection<Integer> notCachedIDs = new HashSet<Integer>();
		Map<Integer, FlightSegmentDTO> ret = new HashMap<Integer, FlightSegmentDTO>();

		for (Integer id : flightSegmentIDs) {
			if (cachedFlightSegDetailsAgainstFltSegID.get(id) == null) {
				notCachedIDs.add(id);
			} else {
				ret.put(id, cachedFlightSegDetailsAgainstFltSegID.get(id));
			}
		}

		if (notCachedIDs.size() > 0) {
			synchronized (guardTwo) {
				// removing again if previous thread already get this flight IDs
				notCachedIDs.removeAll(cachedFlightSegDetailsAgainstFltSegID.keySet());
				Collection<FlightSegmentDTO> segments = AirInventoryModuleUtils.getFlightBD().getFlightSegments(notCachedIDs);
				for (FlightSegmentDTO segment : segments) {
					cachedFlightSegDetailsAgainstFltSegID.put(segment.getSegmentId(), segment);
					ret.put(segment.getSegmentId(), segment);
				}
			}
		}

		return ret;
	}

	public FlightSegmentDTO getCachedFlightSegmentInfo(Integer flightID, String segmentCode) throws ModuleException {

		Pair<Integer, String> pair = Pair.of(flightID, segmentCode);
		if (cachedFlightSegDetailsAgainstFltID.get(pair) == null) {
			synchronized (guardThree) {
				// double checking if previous thread already get this flight ID
				if (cachedFlightSegDetailsAgainstFltID.get(pair) == null) {
					Collection<Integer> ids = new ArrayList<Integer>();
					ids.add(flightID);
					Collection<FlightSegmentDTO> segment = AirInventoryModuleUtils.getFlightBD().getFlightSegments(ids,
							segmentCode);
					if (segment != null && segment.size() == 1) {
						cachedFlightSegDetailsAgainstFltID.put(pair, segment.iterator().next());
					}
				}
			}
		}

		return cachedFlightSegDetailsAgainstFltID.get(pair);
	}

	public Map<Pair<Integer, String>, FlightSegmentDTO> getCachedFlightSegmentInfoCollection(Collection<Integer> flightIDs,
			String segmentCode) throws ModuleException {

		Collection<Integer> notCachedIDs = new HashSet<Integer>();
		Map<Pair<Integer, String>, FlightSegmentDTO> ret = new HashMap<Pair<Integer, String>, FlightSegmentDTO>();

		for (Integer id : flightIDs) {
			Pair<Integer, String> pair = Pair.of(id, segmentCode);
			if (cachedFlightSegDetailsAgainstFltID.get(pair) == null) {
				notCachedIDs.add(id);
			} else {
				ret.put(pair, cachedFlightSegDetailsAgainstFltID.get(pair));

			}
		}

		if (notCachedIDs.size() > 0) {
			synchronized (guardThree) {
				// it is difficult to implement removing logic when if previous threads added the same since the
				// uniqueness of the key depends on two values
				Collection<FlightSegmentDTO> segments = AirInventoryModuleUtils.getFlightBD().getFlightSegments(notCachedIDs,
						segmentCode);
				for (FlightSegmentDTO segment : segments) {
					Pair<Integer, String> pair = Pair.of(segment.getFlightId(), segmentCode);
					ret.put(pair, segment);
					cachedFlightSegDetailsAgainstFltID.put(pair, segment);
				}
			}
		}

		return ret;
	}

	public void clearCachedFltSegIdPair(Collection<Pair<Integer, String>> cachedFltIDs) {
		for (Iterator<Pair<Integer, String>> fltIte = cachedFltIDs.iterator(); fltIte.hasNext();) {
			Pair<Integer, String> fltPair = fltIte.next();
			this.cachedFlightSegDetailsAgainstFltID.remove(fltPair);
		}
	}

	private class CacheForFlightNumber extends LinkedHashMap<Integer, String> {

		private static final long serialVersionUID = -3419932144046375406L;

		private final int capacity;

		public CacheForFlightNumber(int capacity) {
			super(capacity + 1, 0.75F, false);
			this.capacity = capacity;
		}

		protected boolean removeEldestEntry() {
			return size() > capacity;
		}
	}

	private class CacheForFlightSegment extends LinkedHashMap<Integer, FlightSegmentDTO> {

		private static final long serialVersionUID = -3419932144046375407L;

		private final int capacity;

		public CacheForFlightSegment(int capacity) {
			super(capacity + 1, 0.75F, false);
			this.capacity = capacity;
		}

		protected boolean removeEldestEntry() {
			return size() > capacity;
		}
	}

	private class CacheForFlightNumberPair extends LinkedHashMap<Pair<Integer, String>, FlightSegmentDTO> {

		private static final long serialVersionUID = -3419932144046375406L;

		private final int capacity;

		public CacheForFlightNumberPair(int capacity) {
			super(capacity + 1, 0.75F, false);
			this.capacity = capacity;
		}

		protected boolean removeEldestEntry() {
			return size() > capacity;
		}
	}

}
