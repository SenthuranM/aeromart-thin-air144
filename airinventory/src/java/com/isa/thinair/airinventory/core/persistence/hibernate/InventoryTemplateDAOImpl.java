package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.model.InvTempCabinAlloc;
import com.isa.thinair.airinventory.api.model.InvTempCabinBCAlloc;
import com.isa.thinair.airinventory.api.model.InventoryTemplate;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airinventory.core.persistence.dao.InventoryTemplateDAO;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author Priyantha
 * 
 * @isa.module.dao-impl dao-name="InventoryTemplateDAO"
 */

public class InventoryTemplateDAOImpl extends PlatformHibernateDaoSupport implements InventoryTemplateDAO {

	private final Log log = LogFactory.getLog(getClass());

	public void updateExistingInvTemplateCabinAlloc(InvTempCabinAlloc invTempCabinAlloc) {
		hibernateSaveOrUpdate(invTempCabinAlloc);
	}

	public InventoryTemplate saveInventoryTemplate(InvTempDTO invTempDTO) {

		InventoryTemplate invTemp = new InventoryTemplate();

		invTemp.setAirCraftModel(invTempDTO.getAirCraftModel());
		if (invTempDTO.getSegment() != null) {
			invTemp.setSegment(invTempDTO.getSegment());
		}
		invTemp.setStatus(invTempDTO.getStatus());

		hibernateSaveOrUpdate(invTemp);

		ArrayList<InvTempCCAllocDTO> invTempCCAllocDTOList = invTempDTO.getInvTempCCAllocDTOList();
		invTempCCAllocDTOList.removeAll(Collections.singleton(null));
		Iterator<InvTempCCAllocDTO> iterator = invTempCCAllocDTOList.iterator();
		while (iterator.hasNext()) {
			InvTempCCAllocDTO invTempCCAllocDTO = iterator.next();
			InvTempCabinAlloc invTempCabinAlloc = new InvTempCabinAlloc();

			invTempCabinAlloc.setAdultAllocation(invTempCCAllocDTO.getAdultAllocation());
			invTempCabinAlloc.setCabinClassCode(invTempCCAllocDTO.getCabinClassCode());
			invTempCabinAlloc.setInfantAllocation(invTempCCAllocDTO.getInfantAllocation());
			invTempCabinAlloc.setLogicalCabinClassCode(invTempCCAllocDTO.getLogicalCabinClassCode());
			invTempCabinAlloc.setInventoryTemplate(invTemp);

			hibernateSaveOrUpdate(invTempCabinAlloc);

			if (invTempCCAllocDTO.getInvTempCCBCAllocDTOList() != null
					&& !invTempCCAllocDTO.getInvTempCCBCAllocDTOList().isEmpty()) {
				ArrayList<InvTempCCBCAllocDTO> invTempCCBCAllocDTOs = invTempCCAllocDTO.getInvTempCCBCAllocDTOList();
				Iterator<InvTempCCBCAllocDTO> itaratorSec = invTempCCBCAllocDTOs.iterator();
				while (itaratorSec.hasNext()) {
					InvTempCCBCAllocDTO invTempCCBCAllocDTO = itaratorSec.next();
					InvTempCabinBCAlloc invTempCabinBCAlloc = new InvTempCabinBCAlloc();

					invTempCabinBCAlloc.setAllocatedSeats(invTempCCBCAllocDTO.getAllocatedSeats());
					invTempCabinBCAlloc.setAllocWaitListedSeats(0);
					invTempCabinBCAlloc.setBookingCode(invTempCCBCAllocDTO.getBookingCode());
					invTempCabinBCAlloc.setPriorityFlag(invTempCCBCAllocDTO.getPriorityFlag());
					invTempCabinBCAlloc.setStatusBcAlloc(invTempCCBCAllocDTO.getStatusBcAlloc());
					invTempCabinBCAlloc.setInvTempCabinAlloc(invTempCabinAlloc);

					hibernateSaveOrUpdate(invTempCabinBCAlloc);

				}
			}
		}

		return invTemp;
	}
	
	@Override
	public void updateInvTempStatus(InvTempDTO invTempDTO) {
		// update status of inventory template
		String hql = "UPDATE InventoryTemplate SET status=:newStatus WHERE invTempID=:invTempID";
		Query query = getSession().createQuery(hql).setString("newStatus", invTempDTO.getStatus())
				.setInteger("invTempID", invTempDTO.getInvTempID());
		query.executeUpdate();
	}

	@Override
	public Page<InvTempDTO> getInvTempBasicDataPage(int start, int pageSize, String airCraftModel) {

		String countHql = "select count(*)";
		String hql = " from InventoryTemplate ";
		if (airCraftModel != null && !airCraftModel.isEmpty()) {
			hql += " where airCraftModel like :airCraftModel ";
		}
		hql += " ORDER BY invTempID";
		countHql = countHql + hql;
		Query countQuery;
		if (airCraftModel != null && !airCraftModel.isEmpty()) {
			countQuery = getSession().createQuery(countHql).setString("airCraftModel", airCraftModel);
		} else {
			countQuery = getSession().createQuery(countHql);
		}
		Long count = (Long) countQuery.uniqueResult();
		Page<InvTempDTO> invTempDTOPage = null;
		try {
			Query query;
			if (airCraftModel != null && !airCraftModel.isEmpty()) {
				query = getSession().createQuery(hql).setString("airCraftModel", airCraftModel);
			} else {
				query = getSession().createQuery(hql);
			}

			query.setFirstResult(start);
			query.setMaxResults(pageSize);
			List<InventoryTemplate> records = query.list();
		
			Page<InventoryTemplate> inventoryTemplatePage = new Page<InventoryTemplate>(pageSize, start, start + pageSize,
					count.intValue(), records);

			List<InventoryTemplate> inventoryTemplateList = (List<InventoryTemplate>) inventoryTemplatePage.getPageData();
			List<InvTempDTO> invTempDTOList = new ArrayList<InvTempDTO>();
			Iterator<InventoryTemplate> iterator = inventoryTemplateList.iterator();
			while (iterator.hasNext()) {
				InventoryTemplate inventoryTemplate = iterator.next();
				InvTempDTO invTempDTO = new InvTempDTO();

				invTempDTO.setAirCraftModel(inventoryTemplate.getAirCraftModel());
				invTempDTO.setInvTempID(inventoryTemplate.getInvTempID());
				invTempDTO.setSegment(inventoryTemplate.getSegment());
				invTempDTO.setStatus(inventoryTemplate.getStatus());

				invTempDTOList.add(invTempDTO);
			}
			invTempDTOPage = new Page<InvTempDTO>(inventoryTemplatePage.getTotalNoOfRecords(),
					inventoryTemplatePage.getStartPosition(), inventoryTemplatePage.getEndPosition(),
					inventoryTemplatePage.getTotalNoOfRecordsInSystem(), invTempDTOList);
		} catch (Exception ex) {
			log.error("FlightInventoryDAOImpl==> getInvTempBasicDataPage", ex);
		}
		return invTempDTOPage;
	}

	public InventoryTemplate getExistingInventoryTemplate(int invTempID) {
		return get(InventoryTemplate.class, invTempID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<LogicalCabinClass> getLogicalCabinClasses(String cabinClass) {
		String hql = " SELECT LogCC FROM LogicalCabinClass LogCC WHERE LogCC.cabinClassCode=:cabinClass";
		return (ArrayList<LogicalCabinClass>) getSession().createQuery(hql).setString("cabinClass", cabinClass).list();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<InventoryTemplate> getExistingTemplateList(String airCraftModel, String segment) {
		String hql = "from InventoryTemplate where airCraftModel like :airCraftModel";
		if (segment != null) {
			hql += " and segment like :segment";
		}
		Query query;
		query = getSession().createQuery(hql).setString("airCraftModel", airCraftModel);
		if (segment != null && !segment.isEmpty()) {
			query = query.setString("segment", segment);
		}
		List<InventoryTemplate> existingTemplates = query.list();
		return (ArrayList<InventoryTemplate>) existingTemplates;
	}

	@Override
	public ArrayList<InvTempDTO> getInventoryTemplateForAirCraftModel(String airCraftModelName) {

		ArrayList<InvTempDTO> invTempDTOList = new ArrayList<InvTempDTO>();

		StringBuilder query = new StringBuilder("from InventoryTemplate where airCraftModel = :airCraftModel ");
		@SuppressWarnings("unchecked")
		Collection<InventoryTemplate> inventoryTemplates = getSession().createQuery(query.toString())
				.setString("airCraftModel", airCraftModelName).list();
		if (inventoryTemplates != null && inventoryTemplates.size() > 0) {
			invTempDTOList = AirInventoryDataExtractUtil.extractInventoryTemplateData(inventoryTemplates);
		}

		return invTempDTOList;
	}

}