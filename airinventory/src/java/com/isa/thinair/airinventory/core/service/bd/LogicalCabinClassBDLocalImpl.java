package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;

@Local
public interface LogicalCabinClassBDLocalImpl extends LogicalCabinClassBD {

}
