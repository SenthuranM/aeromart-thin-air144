/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;

/**
 * @author mano
 * 
 */

@Local
public interface BaggageBusinessDelegateLocalImpl extends BaggageBusinessDelegate {

}
