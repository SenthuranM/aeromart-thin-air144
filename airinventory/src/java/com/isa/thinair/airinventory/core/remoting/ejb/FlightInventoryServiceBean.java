/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.remoting.ejb;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.isa.thinair.airinventory.api.dto.GdsPublishFccSegBcInventoryTo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.FCCBCOnewayReturnPaxCountDTO;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryRMTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.FLCCAllocationDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.InvDowngradeDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollforwardStatusDTO;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.dto.PubAvailStatusUpdateRQ;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateStatusRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.UpdateOptimizedInventoryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airinventory.api.model.PublishAvailability;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.audit.AuditExternalDataProvider;
import com.isa.thinair.airinventory.core.bl.FlightInventoryBL;
import com.isa.thinair.airinventory.core.bl.PublishAvailabilityBL;
import com.isa.thinair.airinventory.core.bl.RMPublishInventoryBL;
import com.isa.thinair.airinventory.core.bl.RMUpdateOptimizeSeatsBL;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryRMJDBCDAO;
import com.isa.thinair.airinventory.core.service.bd.FlightInventoryBDImpl;
import com.isa.thinair.airinventory.core.service.bd.FlightInventoryBDLocalImpl;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airinventory.core.util.InventoryAuditor;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.dto.FlightChangeInfo;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.auditor.api.util.AuditConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;

/**
 * @author MN
 */
@Stateless
@RemoteBinding(jndiBinding = "FlightInventoryService.remote")
@LocalBinding(jndiBinding = "FlightInventoryService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class FlightInventoryServiceBean extends PlatformBaseSessionBean implements FlightInventoryBDImpl,
		FlightInventoryBDLocalImpl {

	private static final long serialVersionUID = 1370896008670912034L;

	FlightInventoryBL flightInventoryBL = null;

	PublishAvailabilityBL publishAvailabilityBL = null;

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * 
	 * @param createFlightInventoryRQ
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createFlightInventory(CreateFlightInventoryRQ createFlightInventoryRQ) throws ModuleException {
		try {
			AuditAirinventory.createFlightInventory(getUserPrincipal().getUserId(), createFlightInventoryRQ);
			getFlightInventoryBL().createFlightInventory(createFlightInventoryRQ, getUserPrincipal().getUserId());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Creating flight inventory failed.", cdaex);
			sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (ModuleException mex) {
			log.error("Creating flight inventory failed.", mex);
			sessionContext.setRollbackOnly();
			throw mex;
		}
	}

	/**
	 * 
	 * @param flightId
	 * @param cos
	 * @param overlappingFlightId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FCCInventoryDTO> getFCCInventory(int flightId, String cos, Integer overlappingFlightId, boolean includeCnxFlts)
			throws ModuleException {
		try {
			return getFlightInventoryBL().getFCCInventoryDTOsNew(flightId, cos, overlappingFlightId, includeCnxFlts);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading FCC inventory failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}
	
	/**
	 * @param flightSegmentId
	 * @return Map of <code>FCCBCOnewayReturnPaxCountDTO</code>
	 * throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, FCCBCOnewayReturnPaxCountDTO> getFCCBCOnewayReturnPaxCountDTOMap(final Integer flightSegmentId)
			throws ModuleException {
		try {
			return getFlightInventoryBL().getFCCBCOnewayReturnPaxCountDTOMap(flightSegmentId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading BookingClass Specific Farebased oneway/return pax count failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}
	

	/**
	 * Update goshow passengers
	 * 
	 * @param segmentSeatDistsDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateInvForGoshowPassengers(SegmentSeatDistsDTO segmentSeatDistsDTO) throws ModuleException {
		try {
			getFlightInventoryBL().updateInvForGoshowPassengers(segmentSeatDistsDTO, getUserPrincipal().getUserId());
		} catch (CommonsDataAccessException cdaex) {
			log.error("Updating goshow passengers failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param flightIds
	 * @param aircraftModel
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DefaultServiceResponse checkFCCInventoryModelUpdatability(Collection<Integer> flightIds,
			AircraftModel updatedAircraftModel, AircraftModel existingAircraftModel, boolean downGradeToAnyModel,
			boolean isScheduleChange) throws ModuleException {
		try {
			return getFlightInventoryBL().checkFCCInventoryModelUpdatability(flightIds, updatedAircraftModel,
					existingAircraftModel, downGradeToAnyModel, isScheduleChange);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Checking inventory updatability for model change failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param flightIds
	 * @param updatedAircraftModel
	 * @param existingAircraftModel
	 * @param flightSegmentCode
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFCCInventoriesForModelUpdate(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, String flightSegmentCode, boolean downgradeToAnyModel,
			Collection<SegmentDTO> flightSegmentDTOs) throws ModuleException {
		try {
			AuditAirinventory.updateFCCInventoriesForModelUpdate(flightIds, updatedAircraftModel, existingAircraftModel,
					getUserPrincipal().getUserId(), flightSegmentCode);

			getFlightInventoryBL().updateFCCInventoriesForModelUpdate(flightIds, updatedAircraftModel, existingAircraftModel,
					flightSegmentCode, downgradeToAnyModel, flightSegmentDTOs);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Updating inventories for aircraft model update failed", cdaex);
			sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (ModuleException mex) {
			log.error("Updating inventories for aircraft model update failed", mex);
			sessionContext.setRollbackOnly();
			throw mex;
		}
	}

	/**
	 * 
	 * @param flightIds
	 * @param updatedAircraftModel
	 * @param existingAircraftModel
	 * @param olFlightIds
	 * @param segmentDTOs
	 * @param scheduleSegmentCode
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFCCInventoriesForModelChange(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, Collection<Integer> olFlightIds, Collection<SegmentDTO> segmentDTOs,
			String scheduleSegmentCode, boolean downgradeToAnyModel) throws ModuleException {
		try {
			AuditAirinventory.updateFCCInventoriesForModelChange(flightIds, updatedAircraftModel, existingAircraftModel,
					olFlightIds, getUserPrincipal().getUserId(), scheduleSegmentCode);
			getFlightInventoryBL().updateFCCInventoriesForModelChange(flightIds, updatedAircraftModel, existingAircraftModel,
					olFlightIds, segmentDTOs, scheduleSegmentCode, downgradeToAnyModel);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Updating inventories for aircraft model change failed", cdaex);
			sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (ModuleException mex) {
			log.error("Updating inventories for aircraft model change failed", mex);
			sessionContext.setRollbackOnly();
			throw mex;
		}
	}

	/**
	 * 
	 * @param fccSegInventoryUpdateDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public FCCSegInventoryUpdateStatusDTO updateFCCSegInventory(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO) {
		FCCSegInventoryUpdateStatusDTO updateStatus = null;
		try {
			updateStatus = getFlightInventoryBL().updateFCCSegInventoryNew(fccSegInventoryUpdateDTO,
					getUserPrincipal().getUserId());

			if (log.isDebugEnabled()) {
				if (updateStatus != null) {
					log.debug(updateStatus.getSummary());
				}
			}

			if (updateStatus != null && updateStatus.getStatus() == OperationStatusDTO.OPERATION_FAILURE) {
				log.error("Updating flight segment inventories failed.", updateStatus.getException());
				sessionContext.setRollbackOnly();
			} else {
				AuditAirinventory.updateFCCSegInventory(fccSegInventoryUpdateDTO, getUserPrincipal().getUserId(), false);
			}

		} catch (Exception ex) {
			log.error("Updating flight segment inventories failed", ex);
			sessionContext.setRollbackOnly();
			updateStatus = new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "Update failed. ",
					new ModuleException(ex, "module.ejb.exception"), FCCSegInventoryUpdateStatusDTO.UNIDENTIFIED_FAILURE,
					new Integer(fccSegInventoryUpdateDTO.getFccsInventoryId()), null);
		}
		return updateStatus;
	}

	/**
	 * 
	 * @param rollForwardCriteriaDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public InvRollforwardStatusDTO rollFlightInventory(Collection<Integer> flightIds,
			InvRollForwardCriteriaDTO rollForwardCriteriaDTO) throws ModuleException {
		try {
			InvRollforwardStatusDTO status = getFlightInventoryBL().rollForwardFCCSegBCInventoriesNew(flightIds,
					rollForwardCriteriaDTO, getUserPrincipal().getUserId());
			if (status.getStatus() == InvRollforwardStatusDTO.OPERATION_FAILURE) {
				this.sessionContext.setRollbackOnly();
			} else {
				AuditAirinventory.rollFlightInventory(flightIds, getUserPrincipal().getUserId(),
						rollForwardCriteriaDTO.getInvRollForwardFlightsSearchCriteria(),
						rollForwardCriteriaDTO.getSourceFlightId());
				AuditAirinventory.rollFwdFltInvDetailAudit(status.getRollforwardStatus(), getUserPrincipal().getUserId());
			}
			return status;
		} catch (CommonsDataAccessException cdaex) {
			log.error("Inventory rollforward failed", cdaex);
			throw new ModuleException(cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Inventory rollforward failed", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * 
	 * @param flightIds
	 * @param segmentCodes
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void invalidateFlightSegmentInventories(Collection<Integer> flightSegIds) throws ModuleException {
		try {
			AuditAirinventory.invalidateFlightSegmentInventories(flightSegIds, getUserPrincipal().getUserId());
			getFlightInventoryBL().invalidateFlightSegmentInventories(flightSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Invalidating flight segment inventores failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Invalidating flight segment inventores failed.", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * 
	 * @param flightIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeInventoryOverlap(Collection<Integer> flightIds) throws ModuleException {
		try {
			AuditAirinventory.removeInventoryOverlap(flightIds, getUserPrincipal().getUserId());
			getFlightInventoryBL().removeInventoryOverlap(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Removing inventory overlap failed", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Removing inventory overlap failed", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * 
	 * @param flightsIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFlightInventories(Collection<Integer> flightIds) throws ModuleException {
		try {
			AuditAirinventory.deleteFlightInventories(flightIds, getUserPrincipal().getUserId());
			getFlightInventoryBL().deleteFlightInventories(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Deleting flight inventories failed", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Deleting flight inventories failed", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * 
	 * @param flightsId
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getFCCSegBCInventoriesCount(int flightId) throws ModuleException {
		try {
			return getFlightInventoryBL().getFCCSegBCInventoriesCount(flightId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Getting BC inventories count failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param flightsId
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FCCInventoryDTO> getFlightInventoriesForOptimization(Collection<Integer> flightIds,
			OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO) throws ModuleException {
		try {
			// return getFlightInventoryBL().getSeatInventory(flightIds,optimizeSeatInvCriteriaDTO);

			optimizeSeatInvCriteriaDTO.setApplicapableCarrierCodes(getUserPrincipal().getCarrierCodes());
			return getFlightInventoryJDBCDAO().getFlightInventoriesForOptimization(flightIds, optimizeSeatInvCriteriaDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading flight inventories for optimization failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * @param flightIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public HashMap<Integer, FlightInventorySummaryDTO> getFlightInventoriesSummary(Collection<Integer> flightIds,
			boolean isReporting) throws ModuleException {
		try {
			return getFlightInventoryBL().getFlightInventoriesSummary(flightIds, isReporting);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading flight inventories' summary failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void makeInvalidFltSegInvsValid(Collection<Integer> flightSegIds) throws ModuleException {
		try {
			AuditAirinventory.makeInvalidFltSegInvsValid(flightSegIds, getUserPrincipal().getUserId());
			getFlightInventoryBL().makeInvalidFltSegInvsValid(flightSegIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Making flight segments valid failed", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Making flight segments valid failed", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * @param flightIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FlightInventorySummaryDTO> getFlightSegmentInventoriesSummary(Collection<Integer> flightIds)
			throws ModuleException {
		try {
			return getFlightInventoryBL().getFlightSegmentInventoriesSummary(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading segment ineventories summary failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param excludeInvalidSegments
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LinkedHashMap<String, List<Object[]>> getFCCSegBCInventories(int flightId, String cabinClassCode,
			boolean excludeInvalidSegments) throws ModuleException {
		try {
			return getFlightInventoryJDBCDAO().getFCCSegBCInventories(flightId, cabinClassCode, excludeInvalidSegments);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading bc inventories failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<FCCSegBCInventoryTO>> getInvenotriesForRMAlert(Collection<Integer> segInvIds)
			throws ModuleException {
		try {
			return getFlightInventoryRMJDBCDAO().getInvenotriesForRMAlert(segInvIds);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading inventories for RM alerting is failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, FCCSegBCInventoryRMTO> getFlightsForRMAlert() throws ModuleException {
		try {
			return getFlightInventoryRMJDBCDAO().getFlightsForRMAlert();
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading inventories for RM alerting is failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param bcInvIds
	 * @param newOptimizationStatus
	 * @return
	 * @throws ModuleException
	 * 
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int updateOptimizationStatus(Collection<Integer> bcInvIds, String newOptimizationStatus) throws ModuleException {
		try {
			return getFlightInventoryDAO().updateOptimizationStatus(bcInvIds, newOptimizationStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Updating optimization status", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	/**
	 * 
	 * @param bcInvIds
	 * @param newOptimizationStatus
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public UpdateOptimizedInventoryDTO updateOptimizedInventory(UpdateOptimizedInventoryDTO updateOptimizedInventoryDTO)
			throws ModuleException {
		try {
			return new RMUpdateOptimizeSeatsBL(getUserPrincipal().getUserId()).updateOptimizedSeats(updateOptimizedInventoryDTO);
		} catch (CommonsDataAccessException cdaex) {
			log.error("UpdateOptimizedInventory", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (ModuleException m) {

			throw m;// ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}

		catch (Exception e) {
			log.error("UpdateOptimizedInventory", e);
			throw new ModuleException(RMUpdateOptimizeSeatsBL.UNEXPECTED_ERROR, AirinventoryCustomConstants.INV_MODULE_CODE);

		}
	}

	/**
	 * Flight Batch Update Method called by RM
	 * 
	 * @param fccSegInventoryUpdateDTOs
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FCCSegInventoryUpdateStatusRMDTO> batchUpdateFCCSegInventory(
			Collection<FCCSegInventoryUpdateRMDTO> fccSegInventoryUpdateDTOs) throws ModuleException {
		try {

			// will call updateFCCSegInventory

			return new RMPublishInventoryBL(getUserPrincipal().getUserId()).batchUpdateFCCSegInventory(fccSegInventoryUpdateDTOs);

		} catch (ModuleException mex) {
			log.error("Updating inventories for aircraft model change failed", mex);

			throw mex;
		}
	}

	/**
	 * Release Fixed Allocations
	 * 
	 * Called By Scheduler Job, to release fixed allocations for BC's having cut-over time.
	 * 
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void releaseFixedAllocations() throws ModuleException {
		try {

			log.info("||||||||||||||||| Release Fixed Allocations Called");

			Date currentDateTime = new Date();
			// retrive the fixed bc's who has cutover time - i.e. cutover time is set.
			Collection<BookingClass> fxdBCs = AirInventoryModuleUtils.getBookingClassDAO().getFixedBookingClassesToRelease();

			if (fxdBCs == null || fxdBCs.size() < 1) {
				log.info("||||||||||||||||| Fixed Booking Classes with Cut over time Not Found - will Terminate");
				return;
			} else {
				log.info("|||||||||||||||||Found " + fxdBCs.size() + " Fixed Booking Classes with Cutover Time ");

			}

			Iterator<BookingClass> bookingClassesIter = fxdBCs.iterator();
			// debug log

			// iterate each bc
			while (bookingClassesIter.hasNext()) {

				BookingClass bookingClass = bookingClassesIter.next();
				// get the bc's cutover time
				Integer cutoverTiminInMins = bookingClass.getReleaseCutover();

				// the cut over time should never be null, as per retrieval query
				if (cutoverTiminInMins == null) {
					log.warn("[ATTENTION!!!!!!!!!!!]" + bookingClass.getBookingCode()
							+ " should not have been retrieved, this will be ignored as Cut over time is Null");
					continue;
				}

				// the flights dep time should be between thisTime and upperLimit time
				Timestamp thisTime = new Timestamp(currentDateTime.getTime());
				Timestamp upperLimit = new Timestamp(currentDateTime.getTime()
						+ (((cutoverTiminInMins.intValue() / 60)) * 3600 * 1000));

				// debug log
				if (log.isDebugEnabled()) {
					log.debug("################ Flights with Fxd Booking Class " + bookingClass.getBookingCode()
							+ "\n ################ Cut Over Time In Mins : " + cutoverTiminInMins
							+ "\n ################ this time : " + thisTime + "\n ################ upper limit : " + upperLimit);

				}

				// only one fcc-seg-id , and one fcc-seg-bc-id will be retrieved

				Collection<Object[]> idsCollection = AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getFCCSegIdAndSegBCIds(
						bookingClass.getBookingCode(), thisTime, upperLimit);

				if (idsCollection == null || idsCollection.size() < 0) {
					log.info(bookingClass.getBookingCode()
							+ "||||||||||||||||| not found in any flight bc inventory for this moment");
					continue;
				}

				Iterator<Object[]> iterCol = idsCollection.iterator();
				while (iterCol.hasNext()) {
					Object[] o = iterCol.next();

					// update the bc aloocation in a new transaction
					AirInventoryModuleUtils.getLocalFlightInventoryBD().updateFixedSegmentBCAllocation((Integer) o[0],
							(Integer) o[1]);

				}

			}
			log.info(" ||||||||||||||||| Ending releaseFixedAllocations Operation|||||||||||||||||");

		} catch (CommonsDataAccessException cdaex) {
			log.error("release Fixed Allocations", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("release FixedAllocations", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addInventoryNotes(Integer flightSegmentId, String userNote) throws ModuleException {
		try {

			InventoryAuditor a = new InventoryAuditor(getUserPrincipal());
			a.saveAudit(AuditConstants.InventoryAuditOperations.NOTES_SAVE, userNote, null, flightSegmentId);

		} catch (CommonsDataAccessException cdaex) {
			log.error("addInventoryNotes", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("addInventoryNotes", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<InventoryAudit> getInventoryNotes(Integer flightId) throws ModuleException {
		try {
			return AirinventoryUtils.getAuditorBD().getInventoryAudit(flightId);

		} catch (CommonsDataAccessException cdaex) {
			log.error("getInventoryNotes", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("getInventoryNotes", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * @param fccSegInvID
	 *            - The FLIGHTS SEGMENT INVENTORY
	 * @param fccsegBCInvID
	 *            - THE AFFECTING FIXED BC - AVAILABILITY greater to 0
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateFixedSegmentBCAllocation(Integer fccSegInvID, Integer fccsegBCInvID) {

		// prepare this collecion to block the fcc seg table, record locking scenario
		Collection<Integer> fccSegInvIds = new ArrayList<Integer>();
		fccSegInvIds.add(fccSegInvID);
		// block the flight
		try {

			// debug log
			if (log.isDebugEnabled()) {
				log.debug("############  Going to block Segment Allocation : " + fccSegInvID);
			}

			// retrieval from fcc seg alloc table to merely lock the record or the eapplication seg inv id
			AirInventoryModuleUtils.getFlightInventoryResJDBCDAO().getFCCSegInventoriesForUpdate(fccSegInvIds, false,
					AirInventoryModuleUtils.getAirInventoryConfig().getMaxWaitForLockDuration());

			// retrieve the affecting bc.
			FCCSegBCInventory segBCInventory = AirInventoryModuleUtils.getFlightInventoryDAO()
					.getFCCSegBCInventory(fccsegBCInvID);

			// this check should never be null - should not occur
			if (segBCInventory == null) {
				log.warn(" [ATTENTION!!!!!!!!!!!] " + fccsegBCInvID + " fccsegBCInvID not found !!!");
				return;
			}

			// debug log
			if (log.isDebugEnabled()) {
				log.debug(" ############ Befor Updating Fixed BC Allocations" + "  || Seg BC Inv ID : {"
						+ segBCInventory.getFccsbInvId() + "}" + " || Seats Allocated : {" + segBCInventory.getSeatsAllocated()
						+ "}" + " || Seats Available : {" + segBCInventory.getSeatsAvailable() + "}");

			}

			// set the seats allocated and make availabilty zero
			segBCInventory.setSeatsAllocated(segBCInventory.getSeatsAllocated() - segBCInventory.getSeatsAvailable());
			segBCInventory.setSeatsAvailable(0);

			// save the seg bc inventory
			AirInventoryModuleUtils.getFlightInventoryDAO().saveFCCSegmentBCInventory(segBCInventory);

			// debug log
			if (log.isDebugEnabled()) {
				log.debug(" ############ After Updating Fixed BC Allocations " + " || Seg BC Inv ID : { "
						+ segBCInventory.getFccsbInvId() + "}" + " || Seats Allocated : {" + segBCInventory.getSeatsAllocated()
						+ "}" + " || Seats Available : { " + segBCInventory.getSeatsAvailable() + "}");

			}

			log.info(" ||||||||||||||||||||Updated Fixed BC Allocation: fccsegBCInvID " + fccsegBCInvID);

			// if error do not throw exception merely roll back.
		} catch (Exception ex) {
			log.error("release FixedAllocations", ex);
			this.sessionContext.setRollbackOnly();

		}

	}

	/**
	 * Retrieves availability information for publishing.
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getAvailabilityInfoToPublish() throws ModuleException {
		try {
			return getPublishAvailabilityBL().getAvailabilityInfoToPublish();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getAvailabilityInfoToPublish", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("getAvailabilityInfoToPublish", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * Retrieves availability information for BC status update.
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getBCStatusUpdateToPublish() throws ModuleException {
		try {
			return getPublishAvailabilityBL().getBCUpdateAVSInfo();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getBCStatusUpdateToPublish", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("getBCStatusUpdateToPublish", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * Setup availability generation for BC status update. Eliminate out-dated entries
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void setupBCStatusUpdateToPublish() throws ModuleException {
		try {
			getPublishAvailabilityBL().setupBCUpdateAVSInfo();
		} catch (CommonsDataAccessException cdaex) {
			log.error("setupBCStatusUpdateToPublish", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("setupBCStatusUpdateToPublish", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * Notify flight events suchs creation, closure, cancellation, etc.
	 * 
	 */

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void notifyFlightEvent(NotifyFlightEventsRQ notifyFlightEventRQ) throws ModuleException {
		try {
			getPublishAvailabilityBL().notifyFlightEvent(notifyFlightEventRQ);
		} catch (CommonsDataAccessException cdaex) {
			log.error("notifyFlightEvent", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("notifyFlightEvent", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	/**
	 * Updates publish availability status
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void publishAvailabiltyUpdateStatus(Collection<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQs) throws ModuleException {
		try {
			getPublishAvailabilityBL().publishAvailabiltyUpdateStatus(pubAvailStatusUpdateRQs);
		} catch (CommonsDataAccessException cdaex) {
			log.error("publishAvailabiltyUpdateStatus", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("publishAvailabiltyUpdateStatus", ex);
			this.sessionContext.setRollbackOnly();
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	private PublishAvailabilityBL getPublishAvailabilityBL() {
		if (publishAvailabilityBL != null) {
			return publishAvailabilityBL;
		}

		publishAvailabilityBL = new PublishAvailabilityBL();
		return publishAvailabilityBL;
	}

	private FlightInventoryBL getFlightInventoryBL() {
		// [TODO: check] need synchronization in initailizing BL?
		if (flightInventoryBL != null) {
			return flightInventoryBL;
		}

		flightInventoryBL = new FlightInventoryBL();
		return flightInventoryBL;
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean(
				"FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	/**
	 * Data retrieval calls for RM.
	 * 
	 * @return FlightInventoryRMJDBCDAO (singleton)
	 */
	public FlightInventoryRMJDBCDAO getFlightInventoryRMJDBCDAO() {
		return (FlightInventoryRMJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("flightInventoryRMJdbcDAO");
	}

	private FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance().getLocalBean(
				"flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	/**
     * 
     * 
     */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public LinkedHashMap<Integer, FCCSegInventoryUpdateStatusDTO> updateFCCSegInventories(
			Collection<FCCSegInventoryUpdateDTO> fccSegInventoryUpdateDTOs) throws ModuleException {
		LinkedHashMap<Integer, FCCSegInventoryUpdateStatusDTO> fCCSegInventoryUpdateStatusMap = new LinkedHashMap<Integer, FCCSegInventoryUpdateStatusDTO>();
		FCCSegInventoryUpdateStatusDTO updateStatus = null;
		FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO = null;
		try {
			Iterator<FCCSegInventoryUpdateDTO> fccSegInventoryUpdateDTOsIt = fccSegInventoryUpdateDTOs.iterator();

			while (fccSegInventoryUpdateDTOsIt.hasNext()) {
				fccSegInventoryUpdateDTO = fccSegInventoryUpdateDTOsIt.next();
				updateStatus = this.updateFCCSegInventoryNewTx(fccSegInventoryUpdateDTO);
				fCCSegInventoryUpdateStatusMap.put(new Integer(fccSegInventoryUpdateDTO.getFccsInventoryId()), updateStatus);
			}
		} catch (Exception ex) {
			log.error("Optimized inventory update failed", ex);
			this.sessionContext.setRollbackOnly();
			fCCSegInventoryUpdateStatusMap.put(new Integer(fccSegInventoryUpdateDTO.getFccsInventoryId()),
					new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "Update failed. ",
							new ModuleException(ex, "module.ejb.exception"), FCCSegInventoryUpdateStatusDTO.UNIDENTIFIED_FAILURE,
							new Integer(fccSegInventoryUpdateDTO.getFccsInventoryId()), null));

		}

		return fCCSegInventoryUpdateStatusMap;
	}

	/**
	 * @param fccSegInventoryUpdateDTOs
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public FCCSegInventoryUpdateStatusDTO updateFCCSegInventoryNewTx(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO)
			throws ModuleException {
		FCCSegInventoryUpdateStatusDTO updateStatus = null;
		try {
			updateStatus = getFlightInventoryBL().updateFCCSegInventoryNew(fccSegInventoryUpdateDTO,
					getUserPrincipal().getUserId());
			if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_SUCCESS) {
				AuditAirinventory.updateFCCSegInventory(fccSegInventoryUpdateDTO, getUserPrincipal().getUserId(), true);
			} else {
				log.error("Optimized inventory update failed");
				this.sessionContext.setRollbackOnly();
			}

			if (log.isDebugEnabled()) {
				if (updateStatus != null) {
					log.debug(updateStatus.getSummary());
				}
			}
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			log.error("Optimized inventory update failed", ex);
		}

		return updateStatus;
	}

	@Override
	/**
	 * This method will prepare a list of meals required by each departing flight for a given station 
	 * and sends it to the catering department via an email.
	 * The list is prepared certain hours before the departure time of a flight (this threshold is defined in app. parameter)
	 * and sends an email to the catering department (email address is defined in a parameter)
	 * @author lalanthi
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendMealNotification() throws ModuleException {

		try {
			getFlightInventoryBL().sendMealNotification();

		} catch (CommonsDataAccessException cdaex) {
			log.error("getMealsListForStation", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Error in getMealsListForStation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}

	}

	@Override
	/**
	 * 
	 * @param mealsCollection
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendMealsListPerRoute(Object[] mealsCollection) throws ModuleException {
		try {
			getFlightInventoryBL().sendMealsListPerRoute(mealsCollection);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getMealsListForStation", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Error in getMealsListForStation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	@Override
	/**
	 * Returns available seats by cabin class based on search criteria
	 * 
	 * @param flightNumber
	 * @param origin
	 * @param destination
	 * @param departureDateStart
	 * @param departureDateEnd
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> getCabinClassAvailability(CabinClassAvailabilityDTO cabinClassAvailabilityDTO) {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getCabinClassAvailability(cabinClassAvailabilityDTO);
	}

	/**
	 * Returns the segment detail with the available cabin class for a given sengemtId
	 * 
	 * @param fltSegId
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FlightSegWithCCDTO getFlightSegmentsWithCabinClass(Integer fltSegId) {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getFlightSegmentDetailWithCabinClass(fltSegId);
	}

	@Override
	public Collection<InsurancePublisherDetailDTO> getInsuredFlightData() {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getInsuredFlightData();
	}

	@Override
	public Collection<InsurancePublisherDetailDTO> getFailedInsuredFlightData() {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getFailedInsuredFlightData();
	}

	@Override
	/**
	 * 
	 * @param hub
	 * @param mealNotificationBeginTime
	 * @param mealNotificationEndTime
	 * @param noofAttempts
	 * @param flightIds
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Object[] getMealsListForStation(List<String> hub, String mealNotificationBeginTime, String mealNotificationEndTime,
			int noofAttempts, List<Integer> flightIds) throws ModuleException {
		try {
			return getFlightInventoryJDBCDAO().getMealsListForStation(hub, mealNotificationBeginTime, mealNotificationEndTime,
					noofAttempts, flightIds);

		} catch (CommonsDataAccessException cdaex) {
			log.error("getMealsListForStation", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (Exception ex) {
			log.error("Error in getMealsListForStation", ex);
			if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getMaximumEffectiveReservedSetsCountInInterceptingSegments(Collection<Integer> fccSegInvIds)
			throws ModuleException {
		try {
			List<Integer> interceptingIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(fccSegInvIds, true);
			if (interceptingIds != null && !interceptingIds.isEmpty()) {
				return getFlightInventoryDAO().getMaximumEffectiveReservedSetsCountInInterceptingSegments(interceptingIds)[0];
			}
			return 0;
		} catch (CommonsDataAccessException cdaex) {
			log.error("Loading intercepting inventories failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createFCCSegInventory(Collection<FLCCAllocationDTO> flccAllocationDTOs, Integer flightId, String cabinClass,
			int splitOption) throws ModuleException {
		try {
			getFlightInventoryBL().createFCCSegInventory(flccAllocationDTOs, flightId, cabinClass, splitOption, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Creating flight Segment inventory failed.", cdaex);
			sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		} catch (ModuleException mex) {
			log.error("Creating flight Segment inventory failed.", mex);
			sessionContext.setRollbackOnly();
			throw mex;
		}

	}

	@Override
	public Collection<FLCCAllocationDTO> getLogicalCabinClassWiseAllocations(int flightId, String cabinClassCode)
			throws ModuleException {
		return getFlightInventoryJDBCDAO().getLogicalCabinClassWiseAllocations(flightId, cabinClassCode);
	}

	@Override
	public LinkedHashMap<String, List<String>> getAllocatedBookingClassesForCabinClasses(Integer flightId) {
		return getFlightInventoryDAO().getAllocatedBookingClassesForCabinClasses(flightId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFCCSegBCInventories(InvDowngradeDTO inventoryTO, int infantCount, int onHoldInfantCount)
			throws ModuleException {
		try {
			getFlightInventoryBL().updateFCCSegBCInventories(inventoryTO, infantCount, onHoldInfantCount);

		} catch (CommonsDataAccessException cdaex) {
			log.error("updateFCCSegBCInventory failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getCabinClassesToBeDeleted(AircraftModel updatedAircraftModel, AircraftModel existingAircraftModel)
			throws ModuleException {
		try {
			return getFlightInventoryBL().getCabinClassesToBeDeleted(updatedAircraftModel, existingAircraftModel);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCabinClassesToBeDeleted failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	public Map<String, Integer> getAvailableCabinClassesInfo() {
		return getFlightInventoryDAO().getAvailableCabinClassesInfo();
	}

	@Override
	public List<TempSegBcAlloc> getTempSegBcAllocs(List<Integer> ids) {
		return getFlightInventoryDAO().getTempSegBcAllocs(ids);
	}

	@Override
	// TODO -- remove the method
			public
			List<Integer> getBlockedSeatsIds(Collection<Integer> allBlockSeatIds, List<FlightSegmentDTO> flightSegsToRelease)
					throws ModuleException {
		return getFlightInventoryDAO().getBlockedSeatsIds(allBlockSeatIds, flightSegsToRelease);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FCCSegInventory getFlightSegmentsInventory(Integer fccsegInvId) {
		return AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegInventory(fccsegInvId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FCCSegBCInventory> getFCCSegBCInventories(int fccsaId) {
		return AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegBCInventories(fccsaId);
	}

	public boolean isExposedToGDS(Integer fccSegBCinventoryID) {

		if (AppSysParamsUtil.isGDSIntergrationEnabled()) {
			Collection<PublishAvailability> publishAvailabilities = getFlightInventoryDAO().getPublishAvailabiity(
					fccSegBCinventoryID);
			if (publishAvailabilities != null && !publishAvailabilities.isEmpty()) {
				return true;
			}
		}

		return false;
	}

	public void openGoshowBcInventory(FlightSegmentDTO fltSegment) {
		try {
			getFlightInventoryBL().openGoshowBcInventory(fltSegment);
		} catch (ModuleException e) {
			log.error("openGoshowBcInventory failed", e);
		}
	}

	/**
	 * 
	 * @param fltSegIds
	 * @param bookingClasses
	 * @param principal
	 * @param pnr
	 * @param flightId
	 * @param userId
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void doAuditOverBookings(Collection<String> fltSegIds, String bookingClass, UserPrincipal principal, String pnr,
			String flightId, String userId, int seatCount) {
		try {
			AuditAirinventory.doAuditOverBookings(fltSegIds, bookingClass, principal, pnr, flightId, userId, seatCount);
			log.debug(" ############ Doing overbook audit for PNR :" + pnr + " flightId:" + flightId);
		} catch (ModuleException cdaex) {
			log.error("Failed to do OverBook audit", cdaex);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Integer> getInterceptingSegmentIDs(List<Integer> sourceFltSegIds) throws ModuleException {
		return getFlightInventoryJDBCDAO().getInterceptingSegmentIDs(sourceFltSegIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, Set<Integer>> getInterceptingFlightSegmentIds(List<Integer> sourceFltSegIds) {
		return getFlightInventoryJDBCDAO().getInterceptingFlightSegmentIds(sourceFltSegIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void clearCachedFlightIds(Collection<Pair<Integer, String>> alertFlightIds) throws ModuleException {
		AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
		dataProvider.clearCachedFltSegIdPair(alertFlightIds);
	}

	@Override
	public DefaultServiceResponse getAffectedFlights(Collection<Integer> flightIds, AircraftModel updatedAircraftModel)
			throws ModuleException {
		try {
			return getFlightInventoryBL().getAffectedFlights(flightIds, updatedAircraftModel);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Checking inventory updatability for model change failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	@Override
	public void
			doAuditSeatSold(SegmentSeatDistsDTO segmentSeatDistsDTOs, String flightId, String userId, SeatDistribution seatDis) {
		try {
			AuditAirinventory.doSeatSold(segmentSeatDistsDTOs, flightId, userId, seatDis);
			log.debug(" ############ Doing seat sold(ASR) audit for  flightId:" + flightId);
		} catch (ModuleException cdaex) {
			log.error("Failed to do OverBook audit", cdaex);
		}

	}

	@Override
	public void updateOpenReturnFCCSegBCInventories(Integer fccsbInvId, String bookingCode, int adultCount, int onHoldadultCount,
			int infantCount, int onHoldInfantCount) throws ModuleException {
		try {
			getFlightInventoryBL().updateOPRTFCCSegBCInventories(fccsbInvId, bookingCode, adultCount, onHoldadultCount,
					infantCount, onHoldInfantCount);

		} catch (CommonsDataAccessException cdaex) {
			log.error("updateFCCSegBCInventory failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), AirinventoryCustomConstants.INV_MODULE_CODE);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Map<Integer, BigDecimal> getFlightLoadFactor(List<Integer> flightSegmentIds) {
		return getFlightInventoryJDBCDAO().getFlightLoadFactor(flightSegmentIds);
	}

	@Override
	public Collection<FlightChangeInfo> getFlightSeatChangeData(FlightSegement seg, String modelNumber) {
		return AirInventoryModuleUtils.getFlightInventoryJDBCDAO().getFlightSeatChangeData(seg, modelNumber);
	}

	public FCCSegBCInventory getFCCSegBCInventory(int flightSegId, String bookingCode) {
		return AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegBCInventory(flightSegId, bookingCode);
	}

	public FCCSegInventory getFCCSegInventory(int flightSegId, String logicalCabinClassCode) {
		return AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegInventory(flightSegId, logicalCabinClassCode);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void publishGdsFccSegBcInventory(List<GdsPublishFccSegBcInventoryTo> gdsPublishFccSegBcInventoryTos) {
		AirInventoryModuleUtils.getFlightInventoryDAO().publishGdsFccSegBcInventory(gdsPublishFccSegBcInventoryTos);
	}
}