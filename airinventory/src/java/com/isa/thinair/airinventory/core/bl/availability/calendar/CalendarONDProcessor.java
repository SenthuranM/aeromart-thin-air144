package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRS;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airschedules.api.dto.CalendarFlightRS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

class CalendarONDProcessor {
	private CalendarSearchRS calSearchRS = null;

	public CalendarONDProcessor(CalendarSearchRQ searchRQ, CalendarFaresRS calFares, CalendarFlightRS availFlightRS)
			throws ModuleException {
		process(searchRQ, calFares, availFlightRS);
	}

	private void process(CalendarSearchRQ searchRQ, CalendarFaresRS calFares, CalendarFlightRS availFlightRS)
			throws ModuleException {
		int ondSequence = 0;
		PaxCountAssembler paxAssmblr = new PaxCountAssembler(searchRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		String prefCurrency = searchRQ.getAvailPreferences().getPreferredCurrency();
		CurrencyExchangeRate currExchgRate = null;
		calSearchRS = new CalendarSearchRS();
		if (prefCurrency != null && !AppSysParamsUtil.getBaseCurrency().equals(prefCurrency)) {
			currExchgRate = exchangeRateProxy.getCurrencyExchangeRate(searchRQ.getAvailPreferences().getPreferredCurrency());
		}
		if (currExchgRate == null) {
			calSearchRS.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		} else {
			calSearchRS.setCurrencyCode(prefCurrency);
		}

		Map<Integer, Map<String, List<LightFareDTO>>> ondSequenceWiseFareOndWiseFare = groupFaresByONDCode(calFares);

		for (OriginDestinationInformationTO ond : searchRQ.getOrderedOriginDestinationInformationList()) {
			CalendarONDSummary ondSummary = new CalendarONDSummary(ondSequence, ond,
					ondSequenceWiseFareOndWiseFare.get(ondSequence));
			ondSummary.reduceToDateWiseMinimumFareOnd(paxAssmblr, calFares.getFaresForOnd(ondSequence),
					availFlightRS.getAvailableONDFlights(ondSequence));
			ondSummary.quoteCharges(paxAssmblr, searchRQ);

			calSearchRS.addOndCalendar(ondSummary.getFareCalendarOND(ond, currExchgRate));
			ondSequence++;
		}
	}

	private Map<Integer, Map<String, List<LightFareDTO>>> groupFaresByONDCode(CalendarFaresRS calFares) {
		Map<Integer, Map<String, List<LightFareDTO>>> ondSequenceWiseFareOndWiseFare = new HashMap<>();

		for (Map.Entry<Integer, List<LightFareDTO>> fareSegEntry : calFares.getOndFareMap().entrySet()) {
			for (LightFareDTO fareDTO : fareSegEntry.getValue()) {
				Map<String, List<LightFareDTO>> ondWiseFare = ondSequenceWiseFareOndWiseFare.get(fareSegEntry.getKey());
				if (ondWiseFare == null) {
					ondWiseFare = new HashMap<>();
					ondSequenceWiseFareOndWiseFare.put(fareSegEntry.getKey(), ondWiseFare);
				}
				List<LightFareDTO> fareList = ondWiseFare.get(fareDTO.getOndCode());
				if (fareList == null) {
					fareList = new ArrayList<>();
					ondWiseFare.put(fareDTO.getOndCode(), fareList);
				}
				fareList.add(fareDTO);

			}
		}
		return ondSequenceWiseFareOndWiseFare;
	}

	public CalendarSearchRS getCalendarResults() {
		return calSearchRS;
	}
}