package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
class MixFareCal extends BaseFareCal {

	/**
	 * 
	 */
	private final GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator;
	private Map<Integer, List<FilteredBucketsDTO>> ondCheapestBuckets = new HashMap<Integer, List<FilteredBucketsDTO>>();
	private Map<Integer, IFareCalculator.FareCalType> ondFareTypes = new HashMap<Integer, IFareCalculator.FareCalType>();
	private int ondSequence = 0;

	MixFareCal(GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator, AvailableFlightSearchDTO avilFltDTO,
			IFareCalculator.FareCalType... fareTypes) {
		this.groupedOndFlightsFareCalculator = groupedOndFlightsFareCalculator;
		this.avilFltDTO = avilFltDTO;
		fareCalTypes.addAll(Arrays.asList(fareTypes));
		if (avilFltDTO.isOpenReturnSearch()) {
			this.isValid = false;
		}
	}

	private void clear() {
		ondCheapestBuckets = null;
		ondFareTypes = null;
		fareCalTypes = null;
		ondSequence = -1;
	}

	@Override
	public void addOndFlightSegment(Map<IFareCalculator.FareCalType, List<FilteredBucketsDTO>> mapCalTypeBuckets)
			throws ModuleException {
		isOndFlightsAdded = true;
		if (isValid()) {
			double ondMinFare = -1;
			IFareCalculator.FareCalType minFareType = null;
			for (IFareCalculator.FareCalType fareType : mapCalTypeBuckets.keySet()) {
				if (dependantFareTypes().contains(fareType)) {
					List<FilteredBucketsDTO> buckets = mapCalTypeBuckets.get(fareType);
					if (buckets.size() > 0) {
						double ondFare = getMininumFare(fareType, buckets, avilFltDTO);
						if (ondFare != -1) {
							if (AppSysParamsUtil.isFareBiasingEnabled()) {
								if ((minFareType == null)
										|| ((fareType.equals(FareCalType.CON)
												&& FareCalType.CON.equals(minFareType) || fareType
												.equals(FareCalType.SEG)
												&& FareCalType.SEG.equals(minFareType)) && ondMinFare > ondFare)
										|| (fareType.equals(FareCalType.CON) && FareCalType.SEG
												.equals(minFareType))) {
									ondMinFare = ondFare;
									minFareType = fareType;
								}

							} else {
								if (ondMinFare == -1 || ondMinFare > ondFare) {
									ondMinFare = ondFare;
									minFareType = fareType;
								}
							}

						}

					}
				}
			}
			if (minFareType != null) {
				if (totalMinimumFare == -1) {
					totalMinimumFare = ondMinFare;
				} else {
					totalMinimumFare += ondMinFare;
				}
				ondCheapestBuckets.put(ondSequence, mapCalTypeBuckets.get(minFareType));
				ondFareTypes.put(ondSequence, minFareType);
			} else {
				isValid = false;
				clear();
			}
			ondSequence++;
		}
	}

	@Override
	public void setFareAndLogiCC(List<AvailableIBOBFlightSegment> ondFlights, boolean isMinimumFare) {
		int sequence = 0;
		for (AvailableIBOBFlightSegment ondFlight : ondFlights) {
			IFareCalculator.FareCalType fareCalType = ondFareTypes.get(sequence);
			this.groupedOndFlightsFareCalculator.setMinimumFareSet(this.groupedOndFlightsFareCalculator.isMinimumFareSet()
					|| isMinimumFare);
			if (fareCalType == FareCalType.HRT) {
				this.groupedOndFlightsFareCalculator.setFareAndLogicalCabinClass(ondFlight,
						ondCheapestBuckets.get(sequence), FareTypes.HALF_RETURN_FARE, isMinimumFare, null);
			} else if (fareCalType == FareCalType.CON) {
				this.groupedOndFlightsFareCalculator.setFareAndLogicalCabinClass(ondFlight,
						ondCheapestBuckets.get(sequence), FareTypes.OND_FARE, isMinimumFare, null);
			} else if (fareCalType == FareCalType.SEG) {
				List<AvailableFlightSegment> availableFlightSegments = new ArrayList<AvailableFlightSegment>();
				if (ondFlight.isDirectFlight()) {
					availableFlightSegments.add((AvailableFlightSegment) ondFlight);
				} else {
					for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) ondFlight)
							.getAvailableFlightSegments()) {
						availableFlightSegments.add(availableFlightSegment);
					}
				}
				int fltSeq = 0;
				for (AvailableFlightSegment avilFltSeg : availableFlightSegments) {
					this.groupedOndFlightsFareCalculator.setFareAndLogicalCabinClass(avilFltSeg, new ArrayList<FilteredBucketsDTO>(Arrays.asList(ondCheapestBuckets.get(sequence)
							.get(fltSeq++))), FareTypes.PER_FLIGHT_FARE, isMinimumFare, ondFlight);
				}
			}
			sequence++;
		}
	}

	@Override
	protected double getMininumFare(FareCalType calType, List<FilteredBucketsDTO> buckets, AvailableFlightSearchDTO avilFltDTO)
			throws ModuleException {
		if (calType == FareCalType.SEG) {
			Iterator<FilteredBucketsDTO> bucketIterator = buckets.iterator();
			double fareAmount = -1d;
			String cheapestLogicalCabinClass = null;
			while (bucketIterator.hasNext()) {
				FilteredBucketsDTO bucket = bucketIterator.next();
				if (cheapestLogicalCabinClass == null) {
					cheapestLogicalCabinClass = bucket.getChecpestFareLogicalCabinClass();
				} else if (!cheapestLogicalCabinClass.equals(bucket.getChecpestFareLogicalCabinClass())) {
					throw new ModuleException("airinventory.segment.inventory.invalidbookingclassCombination",
							AirinventoryCustomConstants.INV_MODULE_CODE);
				}
				AvailableBCAllocationSummaryDTO minFareSummaryDTO = AirInventoryDataExtractUtil.getLastElement(bucket
						.getCheapestBCAllocationSummaryDTOs());
				if (minFareSummaryDTO != null) {
					// to support the 0 fares
					if (fareAmount == -1d) {
						fareAmount = 0;
					}
					fareAmount += AirInventoryDataExtractUtil.getFareAmount(minFareSummaryDTO, avilFltDTO);
				} else {
					return -1d;
				}
			}
			return fareAmount;

		} else {
			return super.getMininumFare(calType, buckets, avilFltDTO);
		}
	}
	
	public boolean isAllOndsServedByInputFareType(FareCalType inputCalType) {

		boolean allOndsServedByInputFareType = true;
		if (!this.ondFareTypes.isEmpty()) {
			for (FareCalType fareCalType : this.ondFareTypes.values()) {
				if (!inputCalType.equals(fareCalType)) {
					allOndsServedByInputFareType = false;
					break;
				}
			}
		} else {
			allOndsServedByInputFareType = false;
		}

		return allOndsServedByInputFareType;
	}
}