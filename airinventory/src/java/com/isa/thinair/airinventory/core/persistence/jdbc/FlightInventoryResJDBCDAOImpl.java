package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResJDBCDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;

public class FlightInventoryResJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements FlightInventoryResJDBCDAO {

	private final Log log = LogFactory.getLog(getClass());

	@Override
	@SuppressWarnings("unchecked")
	public HashMap<String, Integer> getFCCSegInventoriesForUpdate(Collection<Integer> fccsInvIds,
			boolean includeInterceptingSegs, int waitSeconds) {
		String query = "SELECT SA.FCCSA_ID, SA.SEGMENT_CODE FROM T_FCC_SEG_ALLOC SA " + "WHERE SA.FCCSA_ID IN ("
				+ getSqlInValuesStr(fccsInvIds) + ") ";

		if (includeInterceptingSegs) {
			query += " OR SA.FCCSA_ID IN (SELECT INTETERCEPTING_FCCSA_ID FROM " + " T_INTERCEPTING_FCC_SEG_ALLOC WHERE "
					+ " SOURCE_FCCSA_ID IN (" + getSqlInValuesStr(fccsInvIds) + ")) ";
		}
		query += " FOR UPDATE ";

		if (waitSeconds == 0) {
			query += " NOWAIT ";
		} else {
			query += " WAIT " + waitSeconds;
		}

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (HashMap<String, Integer>) template.query(query, new ResultSetExtractor() {
			HashMap<String, Integer> segmentsMap = new HashMap<String, Integer>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					segmentsMap.put(rs.getString("SEGMENT_CODE"), new Integer(rs.getInt("FCCSA_ID")));
					if (log.isDebugEnabled()) {
						log.debug("Retrieved record for update [" + "fccsa_id=" + rs.getInt("FCCSA_ID") + ",segCode="
								+ rs.getString("SEGMENT_CODE") + "]");
					}
				}
				return segmentsMap;
			}

		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public HashMap<String, Integer> getFCCSegInventoryForUpdate(Integer fccsInvId, boolean includeInterceptingSegs,
			int waitSeconds) {
		String query = "SELECT SA.FCCSA_ID, SA.SEGMENT_CODE FROM T_FCC_SEG_ALLOC SA " + "WHERE SA.FCCSA_ID =" + fccsInvId;

		if (includeInterceptingSegs) {
			query += " OR SA.FCCSA_ID IN (SELECT INTETERCEPTING_FCCSA_ID FROM " + " T_INTERCEPTING_FCC_SEG_ALLOC WHERE "
					+ " SOURCE_FCCSA_ID = " + fccsInvId + ") ";
		}

		query += " FOR UPDATE ";

		if (waitSeconds == 0) {
			query += " NOWAIT ";
		} else {
			query += " WAIT " + waitSeconds;
		}

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (HashMap<String, Integer>) template.query(query, new ResultSetExtractor() {
			HashMap<String, Integer> segmentsMap = new HashMap<String, Integer>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					segmentsMap.put(rs.getString("SEGMENT_CODE"), new Integer(rs.getInt("FCCSA_ID")));
					if (log.isDebugEnabled()) {
						log.debug("Retrieved record for update [" + "fccsa_id=" + rs.getInt("FCCSA_ID") + ",segCode="
								+ rs.getString("SEGMENT_CODE") + "]");
					}
				}
				return segmentsMap;
			}

		});
	}
}
