package com.isa.thinair.airinventory.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AutomaticCheckin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Baggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAutomaticCheckins;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Meal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Seat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedAutoCheckin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedBaggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedFlexi;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedMeal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class AncillaryBL {

	private static final Log log = LogFactory.getLog(AncillaryBL.class);

	private String pnr;

	private String salesChannel;

	private String carrierCode;

	private boolean modifyAnci;
	
	private Map<String, BundledFareDTO> segmentBundledFareMap;

	// Meal
    public List<FlightSegmentMeals> getSelectedMealDetails(List<SelectedMeal> selectedMeals,
                                                    Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, boolean skipCutoverValidation, int salesChannelCode) throws ModuleException {

        List<FlightSegmentMeals> flightSegmentMealsList = new ArrayList<FlightSegmentMeals>();

        Map<Integer, ClassOfServiceDTO> flightSegmentIdWiseCos = getFlightSegmentIdWiseCosMeals(selectedMeals);
        Map<Integer, Integer> flightSegWiseOfferTemplateIDs = getflightSegWiseOfferTemplateIDs(selectedMeals);

        Map<Integer, List<FlightMealDTO>> mealMap = AirInventoryModuleUtils.getMealBD().getMeals(flightSegmentIdWiseCos,
                flightSegIdWiseSelectedMeals, skipCutoverValidation, null, salesChannelCode);

        Map<Integer, List<FlightMealDTO>> segWiseOfferMeals = AirInventoryModuleUtils.getMealBD().getSegWiseMealsByTemplates(
                flightSegWiseOfferTemplateIDs, salesChannelCode);

        Map<String, FlightSegmentMeals> flightSegmentMealsMap;
        FlightSegmentMeals flightSegmentMeals;

        for (Map.Entry<Integer, List<FlightMealDTO>> segmentEntry : mealMap.entrySet()) {

            Integer segmentId = segmentEntry.getKey();
            List<FlightMealDTO> flightMealDTOs = segmentEntry.getValue();
            if (!segWiseOfferMeals.isEmpty() && segWiseOfferMeals.get(segmentId) != null
                    && !segWiseOfferMeals.get(segmentId).isEmpty()) {
                flightMealDTOs = segWiseOfferMeals.get(segmentId);
            }

            flightSegmentMealsMap = new HashMap<>();

            for (FlightMealDTO flightMealDTO : flightMealDTOs) {

                for (SelectedMeal selectedMeal : selectedMeals) {
                    if (selectedMeal.getMealCode().contains(flightMealDTO.getMealCode())
                            && selectedMeal.getFlightSegment().getFlightRefNumber().equals(segmentId.toString())) {

                        if (!flightSegmentMealsMap.containsKey(String.valueOf(segmentId))) {
                            flightSegmentMeals = new FlightSegmentMeals();
                            flightSegmentMeals.setFlightSegment(selectedMeal.getFlightSegment());

                            if (flightSegWiseOfferTemplateIDs.get(segmentId) != null) {
                                flightSegmentMeals.setIsAnciOffer(true);
                                flightSegmentMeals.setOfferedTemplateID(flightSegWiseOfferTemplateIDs.get(segmentId));
                            }

                            flightSegmentMealsList.add(flightSegmentMeals);
                            flightSegmentMealsMap.put(String.valueOf(segmentId), flightSegmentMeals);
                        }

                        flightSegmentMeals = flightSegmentMealsMap.get(String.valueOf(segmentId));

                        Meal meal = new Meal();
                        meal.setMealCharge(flightMealDTO.getAmount());
                        meal.setMealCode(flightMealDTO.getMealCode());
                        meal.setMealCategoryCode(flightMealDTO.getMealCategoryCode());
                        flightSegmentMeals.getMeal().add(meal);

                    }
                }
            }
        }

        return flightSegmentMealsList;

    }

	private Map<Integer, Integer> getflightSegWiseOfferTemplateIDs(List<SelectedMeal> selectedMeals) throws ModuleException {
		Map<Integer, Integer> segWiseTemplateIDs = new HashMap<Integer, Integer>();
		boolean isBundledFareSelected = false;

		// TODO
		Map<String, BundledFareDTO> segmentBundledFareMap = getSegmentCodeWiseBundledFares(pnr);
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (SelectedMeal selectedMeal : selectedMeals) {
				BookingFlightSegment bkgFlightSeg = selectedMeal.getFlightSegment();
				if (segmentBundledFareMap.containsKey(bkgFlightSeg.getSegmentCode())) {
					isBundledFareSelected = true;
					BundledFareDTO bundledFareDTO = segmentBundledFareMap.get(bkgFlightSeg.getSegmentCode());
					if (bundledFareDTO != null) {
						segWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()), bundledFareDTO
								.getApplicableServiceTemplateId(ReservationInternalConstants.EXTERNAL_CHARGES.MEAL.toString()));
					}
				}
			}
		}

		// If bundled fare is not selected, then check for any applied anci offers
		if (!isBundledFareSelected) {
			for (SelectedMeal selectedMeal : selectedMeals) {
				BookingFlightSegment bkgFlightSeg = selectedMeal.getFlightSegment();
				segWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()), selectedMeal.getOfferedAnciTemplateID());
			}
		}

		return segWiseTemplateIDs;
	}

	// Baggage
	List<FlightSegmentBaggages> getSelectedBaggageDetails(List<SelectedBaggage> selectedBaggages, boolean skipCutoverValidation)
			throws ModuleException {

		List<FlightSegmentTO> applicableSegs;
		List<FlightSegmentBaggages> flightSegmentBaggagesList = new ArrayList<FlightSegmentBaggages>();
		Set<Integer> templateIds = new HashSet<Integer>();

		Map<Integer, List<FlightBaggageDTO>> baggageMap = new HashMap<Integer, List<FlightBaggageDTO>>();
		Map<Integer, List<FlightBaggageDTO>> tempBaggageMap = null;

		Map<Integer, Integer> segmentWiseCountAdd = new HashMap<Integer, Integer>();
		Map<Integer, Integer> segmentWiseCountRem = new HashMap<Integer, Integer>();

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {

			List<FlightSegmentTO> flightSegmentTOs = getFlightSegmentTOs(getFlightSegmentTOWithExistingOndGroupPreserved(selectedBaggages));

			for (SelectedBaggage baggage : selectedBaggages) {
				applicableSegs = new ArrayList<FlightSegmentTO>();
				templateIds = new HashSet<Integer>();
				templateIds.addAll(baggage.getBaggageTemplateId());

				lvl_flight_seg: for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					if (flightSegmentTO.getFlightSegId().toString().equals(baggage.getFlightSegment().getFlightRefNumber())) {
						applicableSegs.add(flightSegmentTO);
						break;
					}

					// for (BookingFlightSegment bookingFlightSegment : baggage.getExternalFlightSegments()) {
					// if
					// (flightSegmentTO.getFlightSegId().toString().equals(bookingFlightSegment.getFlightRefNumber())) {
					// applicableSegs.add(flightSegmentTO);
					// break;
					// }
					// }
				}

				for (int templateId : templateIds) {
					tempBaggageMap = AirInventoryModuleUtils.getBaggageBD().getBaggage(applicableSegs, templateId);
					for (int segId : tempBaggageMap.keySet()) {
						if (!baggageMap.containsKey(segId)) {
							baggageMap.put(segId, new ArrayList<FlightBaggageDTO>());
						}

						baggageMap.get(segId).addAll(tempBaggageMap.get(segId));
					}
				}
			}

			Object[] arr = getSegmentWiseCount(baggageMap,
					getFlightSegmentTOs(getFlightSegmentTOWithRemoveBaggages(selectedBaggages)));
			segmentWiseCountAdd = (Map<Integer, Integer>) arr[0];
			segmentWiseCountRem = (Map<Integer, Integer>) arr[1];
		} else {
			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = getFlightSegmentIdWiseCosBaggage(selectedBaggages);
			baggageMap = AirInventoryModuleUtils.getBaggageBD().getBaggages(flightSegIdWiseCos, false, skipCutoverValidation);
		}

		Map<Integer, Integer> segWiseOfferTemplateIDs = getFlightSegmentWiseTemplateIDs(selectedBaggages);

		for (Entry<Integer, List<FlightBaggageDTO>> segmentEntry : baggageMap.entrySet()) {

			Integer segmentId = segmentEntry.getKey();
			List<FlightBaggageDTO> flightBaggageDTOs = segmentEntry.getValue();

			List<FlightBaggageDTO> offerBaggages = AirInventoryModuleUtils.getBaggageBD().getBaggageFromTemplate(
					segWiseOfferTemplateIDs.get(segmentId), Locale.ENGLISH.getLanguage(), null);

			injectBaggageONDGroupID(offerBaggages, getBaggageONDGroupIDFromDTO(flightBaggageDTOs));

			if (!offerBaggages.isEmpty()) {
				flightBaggageDTOs = offerBaggages;
			}

			for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
				for (SelectedBaggage selectedBaggage : selectedBaggages) {
					if (selectedBaggage.getBaggageName().contains(flightBaggageDTO.getBaggageName())
							&& selectedBaggage.getFlightSegment().getFlightRefNumber().equals(segmentId.toString())) {

						Baggage baggage = new Baggage();

						if (AppSysParamsUtil.isONDBaggaeEnabled()) {

							if (selectedBaggage.getFlightSegment().getBaggageONDGroupId() != null) {
								baggage.setBaggageCharge(AccelAeroCalculator.divide(flightBaggageDTO.getAmount(),
										segmentWiseCountAdd.get(segmentId)));
							} else {
								baggage.setBaggageCharge(AccelAeroCalculator.divide(flightBaggageDTO.getAmount(),
										segmentWiseCountRem.get(segmentId)));
							}
						} else {
							baggage.setBaggageCharge(flightBaggageDTO.getAmount());
						}

						baggage.setBaggageName(flightBaggageDTO.getBaggageName());
						baggage.setOndGroupId(flightBaggageDTO.getOndGroupId());
						baggage.setOndChargeId(BeanUtils.nullHandler(flightBaggageDTO.getChargeId()));

						FlightSegmentBaggages flightSegmentBaggages = new FlightSegmentBaggages();
						flightSegmentBaggages.setFlightSegment(selectedBaggage.getFlightSegment());
						flightSegmentBaggages.getBaggage().add(baggage);

						if (segWiseOfferTemplateIDs.get(segmentId) != null) {
							flightSegmentBaggages.setIsAnciOffer(true);
							flightSegmentBaggages.setOfferedTemplateID(segWiseOfferTemplateIDs.get(segmentId));
						}

						flightSegmentBaggagesList.add(flightSegmentBaggages);
					}
				}
			}
		}

		return flightSegmentBaggagesList;

	}

	private List<BookingFlightSegment> getFlightSegmentTOWithRemoveBaggages(List<SelectedBaggage> selectedBaggages) {
		return getFlightSegmentTOs(selectedBaggages, false);
	}

	private List<BookingFlightSegment> getFlightSegmentTOs(List<SelectedBaggage> selectedBaggages,
			boolean mergeAndPreserveOndBaggageId) {
		Map<String, BookingFlightSegment> sameBookingFlightSegment = new HashMap<String, BookingFlightSegment>();

		for (SelectedBaggage selectedBaggage : selectedBaggages) {
			BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();

			String key = composeUniqueKey(bkgFlightSeg, mergeAndPreserveOndBaggageId);
			if (!sameBookingFlightSegment.containsKey(key)) {
				sameBookingFlightSegment.put(key, bkgFlightSeg);
			} else {
				if (!hasBaggageOndGroupId(sameBookingFlightSegment.get(key)) && hasBaggageOndGroupId(bkgFlightSeg)) {
					sameBookingFlightSegment.put(key, bkgFlightSeg);
				}
			}

			if (selectedBaggage.getExternalFlightSegments().size() > 0) {
				for (BookingFlightSegment externalBookingFlightSegment : selectedBaggage.getExternalFlightSegments()) {
					key = composeUniqueKey(externalBookingFlightSegment, mergeAndPreserveOndBaggageId);
					if (!sameBookingFlightSegment.containsKey(key)) {
						sameBookingFlightSegment.put(key, externalBookingFlightSegment);
					} else {
						if (!hasBaggageOndGroupId(sameBookingFlightSegment.get(key))
								&& hasBaggageOndGroupId(externalBookingFlightSegment)) {
							sameBookingFlightSegment.put(key, externalBookingFlightSegment);
						}
					}
				}
			}
		}

		return new ArrayList<BookingFlightSegment>(sameBookingFlightSegment.values());
	}

	private String composeUniqueKey(BookingFlightSegment bookingFlightSegment, boolean mergeAndPreserveOndBaggageId) {
		if (mergeAndPreserveOndBaggageId) {
			return BeanUtils.nullHandler(bookingFlightSegment.getOperatingAirline())
					+ BeanUtils.nullHandler(bookingFlightSegment.getFlightRefNumber());
		} else {
			return BeanUtils.nullHandler(bookingFlightSegment.getOperatingAirline())
					+ BeanUtils.nullHandler(bookingFlightSegment.getFlightRefNumber())
					+ BeanUtils.nullHandler(bookingFlightSegment.getBaggageONDGroupId());
		}
	}

	private boolean hasBaggageOndGroupId(BookingFlightSegment bookingFlightSegment) {
		return (bookingFlightSegment != null && bookingFlightSegment.getBaggageONDGroupId() != null && !""
				.equals(bookingFlightSegment.getBaggageONDGroupId()));
	}

	private String getBaggageONDGroupIDFromDTO(Collection<FlightBaggageDTO> flightBaggageDTOs) {
		String groupID = null;

		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			groupID = flightBaggageDTO.getOndGroupId();
			break;
		}

		return groupID;
	}

	private Map<Integer, Integer> getFlightSegmentWiseTemplateIDs(List<SelectedBaggage> selectedBaggages) throws ModuleException {
		Map<Integer, Integer> flightSegWiseTemplateIDs = new HashMap<Integer, Integer>();
		boolean isBundledFareSelected = false;

		Map<String, BundledFareDTO> segmentBundledFareMap = getSegmentCodeWiseBundledFares(pnr);
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (SelectedBaggage selectedBaggage : selectedBaggages) {
				BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();
				if (segmentBundledFareMap.containsKey(bkgFlightSeg.getSegmentCode())) {
					isBundledFareSelected = true;
					BundledFareDTO bundledFareDTO = segmentBundledFareMap.get(bkgFlightSeg.getSegmentCode());
					if (bundledFareDTO != null) {
						flightSegWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
								bundledFareDTO.getApplicableServiceTemplateId(EXTERNAL_CHARGES.BAGGAGE.toString()));
					}
				}
			}
		}

		// If bundled fare is not selected, then check for any applied anci offers
		if (!isBundledFareSelected) {
			for (SelectedBaggage selectedBaggage : selectedBaggages) {
				BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();

				flightSegWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
						selectedBaggage.getOfferedAnciTemplateID());
			}
		}

		return flightSegWiseTemplateIDs;
	}

	private List<BookingFlightSegment> getFlightSegmentTOWithExistingOndGroupPreserved(List<SelectedBaggage> selectedBaggages) {
		return getFlightSegmentTOs(selectedBaggages, true);
	}

	private Object[] getSegmentWiseCount(Map<Integer, List<FlightBaggageDTO>> baggageMap, List<FlightSegmentTO> flightSegmentTOs) {

		String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		List<FlightSegmentTO> flightSegmentTOsTemp = new ArrayList<FlightSegmentTO>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (defaultCarrierCode.equals(flightSegmentTO.getOperatingAirline())) {
				flightSegmentTOsTemp.add(flightSegmentTO);
			}
		}
		flightSegmentTOs = flightSegmentTOsTemp;
		Collections.sort(flightSegmentTOs);

		Map<String, Map<String, FlightSegmentTO>> flightSegsBaggageAdd = new HashMap<String, Map<String, FlightSegmentTO>>();
		Map<String, Integer> segCodeWiseCount = new HashMap<String, Integer>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getBaggageONDGroupId() != null) {
				if (!flightSegsBaggageAdd.containsKey(flightSegmentTO.getBaggageONDGroupId())) {
					flightSegsBaggageAdd.put(flightSegmentTO.getBaggageONDGroupId(), new HashMap<String, FlightSegmentTO>());
				}
				flightSegsBaggageAdd.get(flightSegmentTO.getBaggageONDGroupId()).put(flightSegmentTO.getSegmentCode(),
						flightSegmentTO);
			}
		}

		Map<Integer, Integer> fltSegIdWiseCount1 = new HashMap<Integer, Integer>();
		Map<Integer, Integer> fltSegIdWiseCount2 = new HashMap<Integer, Integer>();
		for (String baggageOndGrpId : flightSegsBaggageAdd.keySet()) {
			for (String segCode : flightSegsBaggageAdd.get(baggageOndGrpId).keySet()) {
				fltSegIdWiseCount1.put(flightSegsBaggageAdd.get(baggageOndGrpId).get(segCode).getFlightSegId(),
						flightSegsBaggageAdd.get(baggageOndGrpId).size());
				segCodeWiseCount.put(flightSegsBaggageAdd.get(baggageOndGrpId).get(segCode).getSegmentCode(),
						flightSegsBaggageAdd.get(baggageOndGrpId).size());
			}
		}

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getBaggageONDGroupId() == null) {
				if (segCodeWiseCount.containsKey(flightSegmentTO.getSegmentCode())) {
					fltSegIdWiseCount2.put(flightSegmentTO.getFlightSegId(),
							segCodeWiseCount.get(flightSegmentTO.getSegmentCode()));
				} else {
					fltSegIdWiseCount2.put(flightSegmentTO.getFlightSegId(), 1);
				}
			}
		}

		return new Object[] { fltSegIdWiseCount1, fltSegIdWiseCount2 };
	}

	// Airport Services
	public List<FlightSegmentAirportServiceRequests> getSelectedAirportServices(List<SelectedSSR> selectedAPS,
			boolean skipCutoverValidation) throws ModuleException {
		List<FlightSegmentAirportServiceRequests> fltSegAPSList = new ArrayList<FlightSegmentAirportServiceRequests>();

		Map<Integer, List<String>> airportFltSegIdMap = getAirportFltSegIdMap(selectedAPS);
		Map<String, List<AirportServiceDTO>> selectedAPSMap = AirInventoryModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(airportFltSegIdMap, skipCutoverValidation);
		Map<String, Integer> segmentBundledFareServiceInclusion = getSegmentBundledFareServiceAvailability(pnr,
				EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());

		List<BundledFareDTO> bundledFareDTOs = new ArrayList<BundledFareDTO>();
		if (segmentBundledFareServiceInclusion != null && segmentBundledFareServiceInclusion.values() != null
				&& !segmentBundledFareServiceInclusion.values().isEmpty()) {
			bundledFareDTOs = AirmasterUtils.getBundledFareBD().getBundledFareDTOsByBundlePeriodIds(
					new HashSet<Integer>(segmentBundledFareServiceInclusion.values()));
		}

		for (Entry<String, List<AirportServiceDTO>> entry : selectedAPSMap.entrySet()) {
			String[] keyArr = entry.getKey().split("\\" + AirportServiceDTO.SEPERATOR);
			List<AirportServiceDTO> apsDTO = entry.getValue();
			for (AirportServiceDTO airportServiceDTO : apsDTO) {
				for (SelectedSSR selectedAirportService : selectedAPS) {
					if (selectedAirportService.getSsrCode().contains(airportServiceDTO.getSsrCode())
							&& selectedAirportService.getFlightSegment().getFlightRefNumber().equals(keyArr[1])
							&& selectedAirportService.getFlightSegment().getAirportCode().equals(keyArr[0])) {

						BookingFlightSegment flightSegment = selectedAirportService.getFlightSegment();

						if (isOverrideServiceCharge(segmentBundledFareServiceInclusion, flightSegment.getSegmentCode(),
								bundledFareDTOs, airportServiceDTO.getSsrID())) {
							airportServiceDTO.setAdultAmount("0");
							airportServiceDTO.setChildAmount("0");
							airportServiceDTO.setInfantAmount("0");
							airportServiceDTO.setReservationAmount("0");
						}

						FlightSegmentAirportServiceRequests fltSegAPS = new FlightSegmentAirportServiceRequests();
						fltSegAPS.setFlightSegment(selectedAirportService.getFlightSegment());
						fltSegAPS.getAirportServices().add(populateAirportServiceRequest(airportServiceDTO));

						fltSegAPSList.add(fltSegAPS);
					}

				}

			}
		}

		return fltSegAPSList;
	}
	
	// Airport Transfer
	public List<FlightSegmentAirportServiceRequests> getSelectedAirportTransfer(List<SelectedSSR> selectedAPTs,
			boolean skipCutoverValidation) throws ModuleException {
		List<FlightSegmentAirportServiceRequests> fltSegAPTList = new ArrayList<FlightSegmentAirportServiceRequests>();
		FlightSegmentAirportServiceRequests fltSegAirportServiceRequests;
		AirportServiceRequest airportServiceRequest;

		// ASSUMPTIONS : no bundle fares for airport transfer
		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap =  getAirportWiseSegMap(selectedAPTs);
		Map<Integer, Boolean> segmetAvailability = getSegmetAvailabilityMapping(selectedAPTs);
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> availableAPTs = AirInventoryModuleUtils.getAirportTransferBD().getAvailableAirportTransfers(airportWiseSegMap, segmetAvailability);
		for(Entry<AirportServiceKeyTO, List<AirportServiceDTO>> entry : availableAPTs.entrySet()){
			AirportServiceKeyTO airportServiceKeyTO = entry.getKey();
			List<AirportServiceDTO> aptDTO = entry.getValue();
			for (SelectedSSR selectedAirportTransfer : selectedAPTs) {
				if(airportServiceKeyTO.equals(new AirportServiceKeyTO(selectedAirportTransfer.getFlightSegment().getAirportCode(), selectedAirportTransfer.getFlightSegment().getAirportType(), selectedAirportTransfer.getFlightSegment().getDepatureDateTimeZulu().getTime(), selectedAirportTransfer.getFlightSegment().getSegmentCode()))){
					fltSegAirportServiceRequests = new FlightSegmentAirportServiceRequests();
					fltSegAirportServiceRequests.setFlightSegment(selectedAirportTransfer.getFlightSegment());
					fltSegAirportServiceRequests.getAirportServices();
					for(String airportTransferId : selectedAirportTransfer.getSsrCode()){
						for(AirportServiceDTO airportServiceDTO : aptDTO){
							if(airportTransferId.equals(airportServiceDTO.getProvider().getAirportTransferId().toString())){
								airportServiceRequest = new AirportServiceRequest();
								airportServiceRequest.setAdultAmount(new BigDecimal(airportServiceDTO.getAdultAmount()));
								airportServiceRequest.setChildAmount(new BigDecimal(airportServiceDTO.getChildAmount()));
								airportServiceRequest.setInfantAmount(new BigDecimal(airportServiceDTO.getInfantAmount()));
								airportServiceRequest.setAirportCode(airportServiceDTO.getAirport());
								airportServiceRequest.setApplyOn(selectedAirportTransfer.getFlightSegment().getAirportType());
								airportServiceRequest.setFlightRefNumber(selectedAirportTransfer.getFlightSegment().getFlightRefNumber());
								airportServiceRequest.setDescription(airportServiceDTO.getDescription());
								airportServiceRequest.setSsrCode(airportTransferId);
								fltSegAirportServiceRequests.getAirportServices().add(airportServiceRequest);
							}
						}
					}
					if(!fltSegAirportServiceRequests.getAirportServices().isEmpty()){
						fltSegAPTList.add(fltSegAirportServiceRequests);
					}
				}
			}
		}

		return fltSegAPTList;
	}
	
	//TODO remove when proper logic implemented for APT
	private Map<AirportServiceKeyTO, FlightSegmentTO> getAirportWiseSegMap(List<SelectedSSR> selectedAPTs) {
		AirportServiceKeyTO airportServiceKeyTO;

		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap = new HashMap<AirportServiceKeyTO, FlightSegmentTO>();
		for (SelectedSSR selectedAPT : selectedAPTs) {
			airportServiceKeyTO = new AirportServiceKeyTO(selectedAPT.getFlightSegment().getAirportCode(), selectedAPT
					.getFlightSegment().getAirportType(), selectedAPT.getFlightSegment().getDepatureDateTimeZulu().getTime(),
					selectedAPT.getFlightSegment().getSegmentCode());
			if (!airportWiseSegMap.containsKey(airportServiceKeyTO)) {
				airportWiseSegMap.put(airportServiceKeyTO, getFlightSegmentTO(selectedAPT.getFlightSegment()));
			}
		}

		return airportWiseSegMap;
	}
	
	//TODO remove when proper logic implemented for APT
	private Map<Integer, Boolean> getSegmetAvailabilityMapping(List<SelectedSSR> selectedAPTs) {

		Integer fltSegId;
		Map<Integer, Boolean> segAvailabilityMap = new HashMap<Integer, Boolean>();
		for (SelectedSSR selectedAPT : selectedAPTs) {
			if (!StringUtil.isNullOrEmpty(selectedAPT.getFlightSegment().getFlightRefNumber())) {
				fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(selectedAPT.getFlightSegment().getFlightRefNumber());
				if (fltSegId != null && !segAvailabilityMap.containsKey(fltSegId)) {
					segAvailabilityMap.put(fltSegId, false);
				}
			}
		}

		return segAvailabilityMap;

	}
	
	//TODO remove when proper logic implemented for APT
	private static FlightSegmentTO getFlightSegmentTO(BookingFlightSegment bookingFlightSegment) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(bookingFlightSegment.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu());
		flightSegmentTO.setDepartureDateTime(bookingFlightSegment.getDepatureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu());
		flightSegmentTO.setFlightNumber(bookingFlightSegment.getFlightNumber());
		flightSegmentTO.setFlightRefNumber(bookingFlightSegment.getFlightRefNumber());
		flightSegmentTO.setOperatingAirline(bookingFlightSegment.getOperatingAirline());
		if (!StringUtil.isNullOrEmpty(bookingFlightSegment.getFlightRefNumber())) {
			flightSegmentTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(bookingFlightSegment
					.getFlightRefNumber()));
		}
		if (bookingFlightSegment.getReturnFlag().equals("Y")) {
			flightSegmentTO.setReturnFlag(true);
		} else {
			flightSegmentTO.setReturnFlag(false);
		}
		flightSegmentTO.setSegmentCode(bookingFlightSegment.getSegmentCode());
		flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
		flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
		flightSegmentTO.setBaggageONDGroupId(bookingFlightSegment.getBaggageONDGroupId());
		return flightSegmentTO;
	}

	private Map<Integer, List<String>> getAirportFltSegIdMap(List<SelectedSSR> selectedAPS) {
		Map<Integer, List<String>> airportFltSegIdMap = new HashMap<Integer, List<String>>();
		for (SelectedSSR selectedAirportService : selectedAPS) {
			BookingFlightSegment fltSeg = selectedAirportService.getFlightSegment();
			Integer fltRef = new Integer(fltSeg.getFlightRefNumber());
			List<String> airportList = null;
			if (airportFltSegIdMap.get(fltRef) != null) {
				airportList = airportFltSegIdMap.get(fltRef);
			} else {
				airportList = new ArrayList<String>();
				airportFltSegIdMap.put(fltRef, airportList);
			}
			airportList.add(fltSeg.getAirportCode());
		}

		return airportFltSegIdMap;
	}

	private AirportServiceRequest populateAirportServiceRequest(AirportServiceDTO airportServiceDTO) {
		AirportServiceRequest airportServiceRequest = new AirportServiceRequest();
		airportServiceRequest.setSsrCode(airportServiceDTO.getSsrCode());
		airportServiceRequest.setSsrName(airportServiceDTO.getSsrName());
		airportServiceRequest.setApplicabilityType(airportServiceDTO.getApplicabilityType());
		airportServiceRequest.setAdultAmount(new BigDecimal(airportServiceDTO.getAdultAmount()));
		airportServiceRequest.setChildAmount(new BigDecimal(airportServiceDTO.getChildAmount()));
		airportServiceRequest.setInfantAmount(new BigDecimal(airportServiceDTO.getInfantAmount()));
		airportServiceRequest.setReservationAmount(new BigDecimal(airportServiceDTO.getReservationAmount()));
		airportServiceRequest.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		airportServiceRequest.setAirportCode(airportServiceDTO.getAirport());
		airportServiceRequest.setStatus(airportServiceDTO.getStatus());
		airportServiceRequest.setSsrImagePath(airportServiceDTO.getSsrImagePath());
		airportServiceRequest.setSsrThumbnailImagePath(airportServiceDTO.getSsrThumbnailImagePath());

		if (airportServiceDTO.getDecriptionSelectedLanguage() == null
				|| StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage()).equalsIgnoreCase("null"))
			airportServiceRequest.setDescription(airportServiceDTO.getDescription());
		else {
			// TODO check whether this can be eliminated
			String ucs = StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage());
			ucs = ucs.replace("^", ",");
			airportServiceRequest.setDescription(ucs);
		}

		return airportServiceRequest;
	}

	// Seat
	List<FlightSegmentSeats> getSelectedSeatDetails(List<SelectedSeat> selectedSeats) throws ModuleException {

		List<FlightSegmentSeats> flightSegmentSeatsList = new ArrayList<FlightSegmentSeats>();
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = getFlightSegmentIdsMap(selectedSeats);
		Map<Integer, Integer> flightSegIDWiseOfferTemplateIDs = getFlightSegWiseOfferTemplateIDs(selectedSeats);

		Map<Integer, FlightSeatsDTO> seatMap = AirInventoryModuleUtils.getSeatMapBD().getFlightSeats(flightSegIdWiseCos, null);

		Map<Integer, Collection<SeatDTO>> segWiseOfferSeats = AirInventoryModuleUtils.getSeatMapBD()
				.getSeatInfoForSeatChargeTemplates(flightSegIDWiseOfferTemplateIDs);

		for (Entry<Integer, FlightSeatsDTO> segmentEntry : seatMap.entrySet()) {

			Integer segmentId = segmentEntry.getKey();
			FlightSeatsDTO segmentSeats = segmentEntry.getValue();

			Collection<SeatDTO> offerSeats = segWiseOfferSeats.get(segmentId);

			for (SeatDTO seatDTO : (Collection<SeatDTO>) segmentSeats.getSeats()) {
				for (SelectedSeat selectedSeat : selectedSeats) {
					if (selectedSeat.getSeatNumber().contains(seatDTO.getSeatCode())
							&& selectedSeat.getFlightSegment().getFlightRefNumber().equals(segmentId.toString())) {
						Seat seat = new Seat();
						seat.setSeatCharge(seatDTO.getChargeAmount());
						seat.setSeatNumber(seatDTO.getSeatCode());

						if (offerSeats != null) {
							for (SeatDTO offerSeat : offerSeats) {
								if (seat.getSeatNumber().equalsIgnoreCase(offerSeat.getSeatCode())) {
									seat.setSeatCharge(offerSeat.getChargeAmount());
								}
							}
						}

						FlightSegmentSeats flightSegmentSeats = new FlightSegmentSeats();
						flightSegmentSeats.setFlightSegment(selectedSeat.getFlightSegment());
						flightSegmentSeats.getSeat().add(seat);

						flightSegmentSeatsList.add(flightSegmentSeats);
					}
				}
			}
		}
		return flightSegmentSeatsList;
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdsMap(List<SelectedSeat> selectedSeats) {
		Map<Integer, ClassOfServiceDTO> flightSegmentIds = new HashMap<Integer, ClassOfServiceDTO>();

		for (SelectedSeat selectedSeat : selectedSeats) {
			BookingFlightSegment bkgFlightSeg = selectedSeat.getFlightSegment();
			flightSegmentIds.put(
					new Integer(bkgFlightSeg.getFlightRefNumber()),
					new ClassOfServiceDTO(bkgFlightSeg.getCabinClassCode(), bkgFlightSeg.getLogicalCabinClassCode(), bkgFlightSeg
							.getSegmentCode()));
		}

		return flightSegmentIds;
	}

	private Map<Integer, Integer> getFlightSegWiseOfferTemplateIDs(List<SelectedSeat> selectedSeats) throws ModuleException {
		Map<Integer, Integer> segOfferTemplateIDs = new HashMap<Integer, Integer>();
		boolean isBundledFareSelected = false;
		Map<String, BundledFareDTO> segmentBundledFareMap = getSegmentCodeWiseBundledFares(pnr);
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (SelectedSeat selectedSeat : selectedSeats) {
				BookingFlightSegment bkgFlightSeg = selectedSeat.getFlightSegment();
				if (segmentBundledFareMap.containsKey(bkgFlightSeg.getSegmentCode())) {
					isBundledFareSelected = true;
					BundledFareDTO bundledFareDTO = segmentBundledFareMap.get(bkgFlightSeg.getSegmentCode());
					if (bundledFareDTO != null) {
						segOfferTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
								bundledFareDTO.getApplicableServiceTemplateId(EXTERNAL_CHARGES.SEAT_MAP.toString()));
					}
				}
			}
		}

		// If bundled fare is not selected, then check for any applied anci offers
		if (!isBundledFareSelected) {
			for (SelectedSeat selectedSeat : selectedSeats) {
				BookingFlightSegment bkgFlightSeg = selectedSeat.getFlightSegment();
				segOfferTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()), selectedSeat.getOfferedAnciTemplateID());
			}
		}

		return segOfferTemplateIDs;
	}

	// Insurance
	InsuranceQuotation getSavedInsuranceQuote(String insuranceID) {
		InsuranceQuotation insuranceQuotation = new InsuranceQuotation();
		try {

			ReservationInsurance insuranceResponse = AirInventoryModuleUtils.getReservationBD().getReservationInsurance(
					Integer.parseInt(insuranceID));
			insuranceQuotation.setPolicyCode(insuranceResponse.getPolicyCode());
			insuranceQuotation.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());
			insuranceQuotation.setQuotedTotalPremiumAmount(insuranceResponse.getQuotedAmount());
			insuranceQuotation.setTotalPremiumAmount(insuranceResponse.getAmount());
			insuranceQuotation.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
			insuranceQuotation.setSuccess(true);
			insuranceQuotation.setInsuranceRefNumber(insuranceResponse.getInsuranceId());

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
		}
		return insuranceQuotation;
	}

	// Inflight Services
	public List<FlightSegmentSpecialServiceRequests> getSelectedInflightServices(List<SelectedSSR> selectedInflightServices,
			boolean skipeCutoverValidation) throws ModuleException {
		List<FlightSegmentSpecialServiceRequests> fltSegInflightList = new ArrayList<FlightSegmentSpecialServiceRequests>();

		if (selectedInflightServices != null) {

			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
			for (SelectedSSR selectedFlight : selectedInflightServices) {
				ClassOfServiceDTO cosDto = new ClassOfServiceDTO(selectedFlight.getFlightSegment().getCabinClassCode(),
						selectedFlight.getFlightSegment().getLogicalCabinClassCode(), selectedFlight.getFlightSegment()
								.getSegmentCode());
				flightSegIdWiseCos.put(
						FlightRefNumberUtil.getSegmentIdFromFlightRPH(selectedFlight.getFlightSegment().getFlightRefNumber()),
						cosDto);
			}

			Map<Integer, List<SpecialServiceRequestDTO>> availableServiceMap = AirInventoryModuleUtils.getSsrServiceBD()
					.getAvailableSSRs(flightSegIdWiseCos, salesChannel, false, modifyAnci, skipeCutoverValidation);

			for (Entry<Integer, List<SpecialServiceRequestDTO>> entry : availableServiceMap.entrySet()) {
				for (SelectedSSR selectedSSR : selectedInflightServices) {
					Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(selectedSSR.getFlightSegment()
							.getFlightRefNumber());
					if (flightSegId.intValue() == entry.getKey().intValue()) {
						FlightSegmentSpecialServiceRequests ssr = new FlightSegmentSpecialServiceRequests();
						ssr.setFlightSegment(selectedSSR.getFlightSegment());

						List<SpecialServiceRequestDTO> ssrList = entry.getValue();
						if (ssrList != null) {
							for (SpecialServiceRequestDTO specialServiceRequestDTO : ssrList) {
								ssr.getSpecialServiceRequest().add(populateSpecialServiceRequest(specialServiceRequestDTO));
							}

						}

						fltSegInflightList.add(ssr);
					}
				}
			}
		}

		return fltSegInflightList;
	}

	private SpecialServiceRequest populateSpecialServiceRequest(SpecialServiceRequestDTO specialServiceRequestDTO) {
		SpecialServiceRequest specialServiceRequest = new SpecialServiceRequest();
		specialServiceRequest.setStatus("ACT");
		specialServiceRequest.setSsrCode(specialServiceRequestDTO.getSsrCode());
		specialServiceRequest.setSsrName(specialServiceRequestDTO.getSsrName());

		BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (specialServiceRequestDTO.getChargeAmount() != null && !specialServiceRequestDTO.getChargeAmount().equals("")) {
			serviceCharge = new BigDecimal(specialServiceRequestDTO.getChargeAmount());
		}

		specialServiceRequest.setCharge(serviceCharge);
		specialServiceRequest.setCarrierCode(carrierCode);

		if (specialServiceRequestDTO.getDecriptionSelectedLanguage() == null
				|| StringUtil.getUnicode(specialServiceRequestDTO.getDecriptionSelectedLanguage()).equalsIgnoreCase("null")) {
			specialServiceRequest.setDescription(specialServiceRequestDTO.getDescription());
		} else {
			specialServiceRequest.setDescription(StringUtil.getUnicode(specialServiceRequestDTO.getDecriptionSelectedLanguage())
					.replace('^', ','));
		}
		specialServiceRequest.setServiceQuantity("");
		specialServiceRequest.setAvailableQty(specialServiceRequestDTO.getAvailableQty());

		return specialServiceRequest;
	}

	// Flexi
	List<FlexiQuotation> getSelectedFlexiDetails(List<SelectedFlexi> selectedFlexis) throws ModuleException {

		List<FlexiQuotation> flexiQuotation = new ArrayList<FlexiQuotation>();

		Collection<Integer> flexiRuleRateIds = getFlexiRuleRateIds(selectedFlexis);

		Map<String, List<FlexiRuleDTO>> ondFlexiCharges = getOndFlexiRulesForReservation(flexiRuleRateIds);
		for (Entry<String, List<FlexiRuleDTO>> entry : ondFlexiCharges.entrySet()) {
			String ondCode = entry.getKey();

			for (FlexiRuleDTO flexiRuleDTO : entry.getValue()) {
				FlexiQuotation flexiCharge = new FlexiQuotation();
				flexiCharge.setFlexiRuleRateId(flexiRuleDTO.getFlexiRuleRateId());
				flexiCharge.setAdultCharge((flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT)));
				flexiCharge.setChildCharge((flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD)));
				flexiCharge.setInfantCharge((flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)));
				flexiCharge.setOndCode(ondCode);
				flexiQuotation.add(flexiCharge);
			}

		}

		return flexiQuotation;
	}

	private Collection<Integer> getFlexiRuleRateIds(List<SelectedFlexi> selectedFlexis) {
		Collection<Integer> flexiRuleRateIds = new HashSet<Integer>();

		for (SelectedFlexi selectedFlexi : selectedFlexis) {
			flexiRuleRateIds.addAll(selectedFlexi.getFlexiRateIds());
		}
		return flexiRuleRateIds;
	}

	private Map<String, List<FlexiRuleDTO>> getOndFlexiRulesForReservation(Collection<Integer> flexiRuleRateIds)
			throws ModuleException {
		//
		// if (tnxResProcess == null) {
		// throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Transaction not found");
		// }

		Map<String, List<FlexiRuleDTO>> allFlexiCharges = new HashMap<String, List<FlexiRuleDTO>>();
		// if (tnxResProcess != null) {
		// Collection<OndFareDTO> ondFareDTOListForReservation = tnxResProcess
		// .getOndFareDTOListForReservation(aaSelectedAncillaryRQ.getSelectedFareTypes());
		// for (OndFareDTO ondFareDTO : ondFareDTOListForReservation) {
		//
		// String ondCode = ondFareDTO.getOndCode();
		// if (!allFlexiCharges.containsKey(ondCode)) {
		// allFlexiCharges.put(ondCode, new ArrayList<FlexiRuleDTO>());
		// }
		//
		// Collection<FlexiRuleDTO> flexiCharges = ondFareDTO.getAllFlexiCharges();
		// if (flexiCharges != null) {
		// for (FlexiRuleDTO flexiRuleDTO : flexiCharges) {
		// for (Integer flexiRuleRateId : flexiRuleRateIds) {
		// if (flexiRuleRateId.equals(flexiRuleDTO.getFlexiRuleRateId())) {
		// allFlexiCharges.get(ondCode).add(flexiRuleDTO);
		// }
		// }
		// }
		// }
		// }
		// }
		return allFlexiCharges;
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseCosMeals(List<SelectedMeal> selectedMeals) {
		Map<Integer, ClassOfServiceDTO> flightSegmentIds = new HashMap<Integer, ClassOfServiceDTO>();

		for (SelectedMeal selectedMeal : selectedMeals) {
			BookingFlightSegment bkgFlightSeg = selectedMeal.getFlightSegment();
			ClassOfServiceDTO cosDto = new ClassOfServiceDTO(bkgFlightSeg.getCabinClassCode(),
					bkgFlightSeg.getLogicalCabinClassCode(), bkgFlightSeg.getSegmentCode());
			flightSegmentIds.put(new Integer(bkgFlightSeg.getFlightRefNumber()), cosDto);
		}

		return flightSegmentIds;
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseCosBaggage(List<SelectedBaggage> selectedBaggages) {
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();

		for (SelectedBaggage selectedBaggage : selectedBaggages) {
			BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();
			ClassOfServiceDTO cosDto = new ClassOfServiceDTO(bkgFlightSeg.getCabinClassCode(),
					bkgFlightSeg.getLogicalCabinClassCode(), bkgFlightSeg.getSegmentCode());
			flightSegIdWiseCos.put(new Integer(bkgFlightSeg.getFlightRefNumber()), cosDto);
		}

		return flightSegIdWiseCos;
	}

	public static Map<Integer, Set<String>> getFlightSegIdWiseSelectedAnci(List<StringStringListMap> selectedAnciList) {
		Map<Integer, Set<String>> flightSegIdWiseMap = null;

		if (selectedAnciList != null) {
			flightSegIdWiseMap = new HashMap<Integer, Set<String>>();
			for (StringStringListMap mapObj : selectedAnciList) {
				Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(mapObj.getKey());
				if (flightSegIdWiseMap.get(flightSegId) == null) {
					flightSegIdWiseMap.put(flightSegId, new HashSet<String>());
				}

				if (mapObj.getValue() != null) {
					flightSegIdWiseMap.get(flightSegId).addAll(mapObj.getValue());
				}

			}
		}

		return flightSegIdWiseMap;
	}

	public static List<FlightSegmentTO> getFlightSegmentTOs(List<BookingFlightSegment> bookingFlightSegments)
			throws ModuleException {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (BookingFlightSegment bookingFlightSegment : bookingFlightSegments) {
			FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

			FlightSegement flightSegment = null;
			Integer flightSegId = null;

			if (!StringUtil.isNullOrEmpty(bookingFlightSegment.getFlightRefNumber())) {
				flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(bookingFlightSegment.getFlightRefNumber());
				if (flightSegId != null) {
					flightSegment = AirInventoryModuleUtils.getFlightBD().getFlightSegment(flightSegId);
				}
			}

			flightSegmentTO.setFlightNumber(bookingFlightSegment.getFlightNumber());
			flightSegmentTO.setAirportCode(bookingFlightSegment.getAirportCode());
			flightSegmentTO.setAirportType(bookingFlightSegment.getAirportType());
			flightSegmentTO.setArrivalDateTime(bookingFlightSegment.getArrivalDateTime());
			flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu() == null ? bookingFlightSegment
					.getArrivalDateTime() : bookingFlightSegment.getArrivalDateTimeZulu());
			flightSegmentTO.setDepartureDateTime(bookingFlightSegment.getDepatureDateTime());
			flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu());

			if (flightSegment != null && flightSegment.getEstTimeArrivalZulu() != null) {
				flightSegmentTO.setArrivalDateTimeZulu(new Date(flightSegment.getEstTimeArrivalZulu().getTime()));
			} else {
				flightSegmentTO
						.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu() == null ? bookingFlightSegment
								.getArrivalDateTime() : bookingFlightSegment.getArrivalDateTimeZulu());
			}

			if (flightSegment != null && flightSegment.getEstTimeDepatureZulu() != null) {
				flightSegmentTO.setDepartureDateTimeZulu(new Date(flightSegment.getEstTimeDepatureZulu().getTime()));

			} else {
				flightSegmentTO
						.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu() == null ? bookingFlightSegment
								.getDepatureDateTime() : bookingFlightSegment.getDepatureDateTimeZulu());
			}

			flightSegmentTO.setFlightSegId(Integer.parseInt(bookingFlightSegment.getFlightRefNumber()));
			flightSegmentTO.setOperatingAirline(bookingFlightSegment.getOperatingAirline());
			flightSegmentTO.setOperationType(bookingFlightSegment.getOperationType());
			flightSegmentTO.setReturnFlag("Y".equals(bookingFlightSegment.getReturnFlag()) ? true : false);
			flightSegmentTO.setSegmentCode(bookingFlightSegment.getSegmentCode());
			flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
			flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
			flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
			flightSegmentTO.setBaggageONDGroupId(bookingFlightSegment.getBaggageONDGroupId());
			flightSegmentTO.setPnrSegId(bookingFlightSegment.getBookingFlightRefNumber());
			if (bookingFlightSegment.getOndSequence() != null) {
				flightSegmentTO.setOndSequence(bookingFlightSegment.getOndSequence());
			} else {
				flightSegmentTO.setOndSequence(bookingFlightSegment.getJourneySeq());
			}

			flightSegmentTO.setAdultCount(bookingFlightSegment.getAdultCount());
			flightSegmentTO.setChildCount(bookingFlightSegment.getChildCount());
			flightSegmentTO.setInfantCount(bookingFlightSegment.getInfantCount());
			flightSegmentTO.setFareBasisCode(bookingFlightSegment.getFareBasisCode());
			flightSegmentTO.setFlexiID(bookingFlightSegment.getFlexiID() == null ? null : bookingFlightSegment.getFlexiID()
					.intValue());
			flightSegmentTO.setParticipatingJourneyReturn(bookingFlightSegment.isParticipatingJourneyReturn());
			flightSegmentTO.setSectorONDCode(bookingFlightSegment.getSectorONDCode());
			flightSegmentTO.setInverseSegmentDeparture(bookingFlightSegment.getInverseSegmentDeparture());
			flightSegmentTO.setBookingClass(bookingFlightSegment.getBookingClass());

			flightSegmentTOs.add(flightSegmentTO);
		}

		return flightSegmentTOs;
	}

	public Map<String, Integer> getSegmentBundledFareServiceAvailability(String pnr, String serviceName) throws ModuleException {

		Map<String, BundledFareDTO> segmentBundledFares = getSegmentCodeWiseBundledFares(pnr);
		Map<String, Integer> segmentBundledFareServiceAvailability = null;

		if (segmentBundledFares != null) {
			segmentBundledFareServiceAvailability = new HashMap<String, Integer>();
			for (Entry<String, BundledFareDTO> bundledFareEntry : segmentBundledFares.entrySet()) {
				String segmentCode = bundledFareEntry.getKey();
				BundledFareDTO bundledFareDTO = bundledFareEntry.getValue();

				if (bundledFareDTO != null
						&& (!segmentBundledFareServiceAvailability.containsKey(segmentCode) || segmentBundledFareServiceAvailability
								.get(segmentCode) == null) && bundledFareDTO.isServiceIncluded(serviceName)) {
					segmentBundledFareServiceAvailability.put(segmentCode, bundledFareDTO.getBundledFarePeriodId());
				}
			}
		}

		return segmentBundledFareServiceAvailability;
	}

	public static Collection<FlightBaggageDTO> injectBaggageONDGroupID(Collection<FlightBaggageDTO> flightBaggageDTOs,
			String ondGroupID) {
		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			flightBaggageDTO.setOndGroupId(ondGroupID);
		}
		return flightBaggageDTOs;
	}

	public static boolean isOverrideServiceCharge(Map<String, Integer> segmentBundledFareServiceInclusion, String segmentCode,
			List<BundledFareDTO> bundledFareDTOs, Long ssrId) {
		if (segmentBundledFareServiceInclusion != null && segmentBundledFareServiceInclusion.containsKey(segmentCode)
				&& segmentBundledFareServiceInclusion.get(segmentCode) != null) {
			Integer selectedBundledServicePeriodId = segmentBundledFareServiceInclusion.get(segmentCode);
			for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
				if (bundledFareDTO.getBundledFarePeriodId().equals(selectedBundledServicePeriodId)) {
					Set<Integer> freeApsIds = bundledFareDTO.getApplicableApsList();
					if (freeApsIds != null && freeApsIds.contains(ssrId.intValue())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public Map<String, BundledFareDTO> getSegmentCodeWiseBundledFares(String pnr) throws ModuleException {
		Map<String, BundledFareDTO> segmentCodeWiseBundledFares = new HashMap<String, BundledFareDTO>();
		if (segmentBundledFareMap == null && pnr != null && !"".equals(pnr)) {
			Map<String, Integer> segmentBundledFarePeriodIds = AirInventoryModuleUtils.getReservationQueryBD()
					.getSegmentBundledFarePeriodIds(pnr);
			Collection<Integer> colBundledFarePeriodIds = segmentBundledFarePeriodIds.values();
			if (colBundledFarePeriodIds != null && !colBundledFarePeriodIds.isEmpty()) {
				Set<Integer> selectedBundledFarePeriodIds = new HashSet<Integer>();
				for (Integer bundledFarePeriodId : colBundledFarePeriodIds) {
					if (bundledFarePeriodId != null) {
						selectedBundledFarePeriodIds.add(bundledFarePeriodId);
					}
				}

				if (!selectedBundledFarePeriodIds.isEmpty()) {
					List<BundledFareDTO> bundledFareDTOs = AirInventoryModuleUtils.getBundledFareBD()
							.getBundledFareDTOsByBundlePeriodIds(selectedBundledFarePeriodIds);

					Map<Integer, BundledFareDTO> bundledFareMap = new HashMap<Integer, BundledFareDTO>();
					for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
						bundledFareMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
					}

					segmentCodeWiseBundledFares = new HashMap<String, BundledFareDTO>();

					for (Entry<String, Integer> segmentEntry : segmentBundledFarePeriodIds.entrySet()) {
						String segmentCode = segmentEntry.getKey();
						Integer bundledFarePeriodId = segmentEntry.getValue();

						segmentCodeWiseBundledFares.put(segmentCode, bundledFareMap.get(bundledFarePeriodId));
					}
				}

			}

		} else {
			segmentCodeWiseBundledFares = segmentBundledFareMap;
		}

		return segmentCodeWiseBundledFares;
	}

	// This is to get Automatic Checkin details
	public List<FlightSegmentAutomaticCheckins>
			getSelectedAutomaticCheckinDetails(List<SelectedAutoCheckin> selectedAutoCheckins) throws ModuleException {

		List<FlightSegmentAutomaticCheckins> fltSegAutomaticCheckinList = new ArrayList<FlightSegmentAutomaticCheckins>();
		FlightSegmentAutomaticCheckins fltSegAutomaticCheckins;
		AutomaticCheckin automaticCheckin;
		AutomaticCheckinTemplate template;
		for (SelectedAutoCheckin selectedAutoCheckin : selectedAutoCheckins) {
			fltSegAutomaticCheckins = new FlightSegmentAutomaticCheckins();
			fltSegAutomaticCheckins.setFlightSegment(selectedAutoCheckin.getFlightSegment());
			for (Integer autoCheckinId : selectedAutoCheckin.getAutoCheckinId()) {
				template = AirInventoryModuleUtils.getAutomaticCheckinBD().getAutomaticCheckinTemplate(autoCheckinId);
				automaticCheckin = new AutomaticCheckin();
				automaticCheckin.setAirportCode(selectedAutoCheckin.getFlightSegment().getSegmentCode().split("/")[0]);
				automaticCheckin.setAutomaticCheckinCharge(template.getAmount());
				automaticCheckin.setStatus(template.getStatus());
				automaticCheckin.setAutoCheckinTempId(autoCheckinId);
				fltSegAutomaticCheckins.getAutomaticCheckin().add(automaticCheckin);
			}
			if (!fltSegAutomaticCheckins.getAutomaticCheckin().isEmpty())
				fltSegAutomaticCheckinList.add(fltSegAutomaticCheckins);
		}

		return fltSegAutomaticCheckinList;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public boolean isModifyAnci() {
		return modifyAnci;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}

	public Map<String, BundledFareDTO> getSegmentBundledFareMap() {
		return segmentBundledFareMap;
	}

	public void setSegmentBundledFareMap(Map<String, BundledFareDTO> segmentBundledFareMap) {
		this.segmentBundledFareMap = segmentBundledFareMap;
	}

}
