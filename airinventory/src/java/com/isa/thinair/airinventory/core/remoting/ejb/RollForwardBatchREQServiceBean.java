/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.remoting.ejb;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQ;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.bl.RollForwardBatchBL;
import com.isa.thinair.airinventory.core.persistence.dao.RollForwardBatchREQDAO;
import com.isa.thinair.airinventory.core.service.bd.RollForwardBatchREQServiceDelegateImpl;
import com.isa.thinair.airinventory.core.service.bd.RollForwardBatchREQServiceLocalDelegateImpl;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Raghu
 */
@Stateless
@RemoteBinding(jndiBinding = "RollForwardBatchREQService.remote")
@LocalBinding(jndiBinding = "RollForwardBatchREQService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class RollForwardBatchREQServiceBean extends PlatformBaseSessionBean implements RollForwardBatchREQServiceDelegateImpl,
		RollForwardBatchREQServiceLocalDelegateImpl {

	private final static Log log = LogFactory.getLog(RollForwardBatchREQServiceBean.class);

	/**
	 * Field sessionContext
	 */
	@Resource
	protected SessionContext sessionContext;

	/**
	 * Return the user principal
	 *
	 * @return
	 * @throws ModuleException
	 */
	public UserPrincipal getUserPrincipal() throws ModuleException {
		UserPrincipal principal = null;
		try {
			principal = (UserPrincipal) this.sessionContext.getCallerPrincipal();
		} catch (Exception ex) {
			log.error("Retrieving Caller Principal failed", ex);
		}
		return principal;
	}

	/**
	 * Sets tracking details.
	 *
	 * @param tracking
	 * @return
	 * @throws ModuleException
	 */
	public Tracking setUserDetails(Tracking tracking) throws ModuleException {
		UserPrincipal principal = getUserPrincipal();

		if (principal != null) {
			if (tracking.getVersion() < 0) { // save operation
				tracking.setCreatedBy(principal.getUserId() == null ? "" : principal.getUserId());
				tracking.setCreatedDate(new Date());

				if (tracking.getModifiedBy() == null) {
					tracking.setModifiedBy(principal.getUserId() == null ? "" : principal.getUserId());
					tracking.setModifiedDate(new Date());
				}
			} else { // update operation
				tracking.setModifiedBy(principal.getUserId() == null ? "" : principal.getUserId());
				tracking.setModifiedDate(new Date());
			}
		}

		return tracking;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection<RollForwardBatchREQSEG> saveRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ)
			throws ModuleException {

		try {
			UserPrincipal principal = getUserPrincipal();

			setUserDetails(rollForwardBatchREQ);

			for (RollForwardBatchREQSEG rollForwardBatchREQSEG : rollForwardBatchREQ.getRollForwardBatchREQSEGSet()) {

				setUserDetails(rollForwardBatchREQSEG);
			}

			AuditAirinventory.saveRollForwardBatchREQ(principal.getUserId(), rollForwardBatchREQ);

			Integer batchId = lookupRollForwardBatchREQDAO().saveRollForwardBatchREQ(rollForwardBatchREQ);

			Collection<RollForwardBatchREQSEG> rollForwardBatchREQSEGList = AirinventoryUtils.getRollForwardBatchREQSEGBD()
					.getRollForwardBatchREQSEGByBatchId(batchId);

			return rollForwardBatchREQSEGList;

		} catch (ModuleException moduleException) {
			this.sessionContext.setRollbackOnly();
			throw moduleException;
		} catch (CommonsDataAccessException cde) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cde.getExceptionCode(), "rollforward.desc");
		} catch (Exception exception) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("rollforward.technical.error", "rollforward.desc");
		}

	}

	@Override
	public void removeRollForwardBatchREQ(int key) throws ModuleException {
		try {
			lookupRollForwardBatchREQDAO().removeRollForwardBatchREQ(key);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "rollforward.desc");
		}

	}

	@Override
	public List<RollForwardBatchREQ> getRollForwardBatchREQs() throws ModuleException {
		try {
			return lookupRollForwardBatchREQDAO().getRollForwardBatchREQs();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		}

	}

	@Override
	public RollForwardBatchREQ getRollForwardBatchREQ(int RollForwardBatchREQId) throws ModuleException {
		try {
			return lookupRollForwardBatchREQDAO().getRollForwardBatchREQ(RollForwardBatchREQId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		}

	}

	private RollForwardBatchREQDAO lookupRollForwardBatchREQDAO() {

		LookupService lookupService = LookupServiceFactory.getInstance();
		return (RollForwardBatchREQDAO) lookupService.getBean("isa:base://modules/airinventory?id=RollForwardBatchREQDAOProxy");

	}

	@Override
	public void removeRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException {
		lookupRollForwardBatchREQDAO().removeRollForwardBatchREQ(rollForwardBatchREQ);

	}

	@Override
	public void updateRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException {
		lookupRollForwardBatchREQDAO().updateRollForwardBatchREQ(rollForwardBatchREQ);

	}

	@Override
	public void updateRollForwardBatchREQStatus(Integer batchId, String status) throws ModuleException {
		UserPrincipal principal = getUserPrincipal();
		AuditAirinventory.updateRollForwardBatchREQStatus(principal.getUserId(), batchId, status);
		lookupRollForwardBatchREQDAO().updateRollForwardBatchREQStatus(batchId, status);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Integer doAdvanceRollForward(List<InvRollForwardCriteriaDTO> criteriaDTOList) throws ModuleException {

		return new RollForwardBatchBL().doAdvanceRollForward(criteriaDTOList);
	}

	@Override
	public void doRollForwardLogic(InvRollForwardCriteriaDTO invRollForwardCriteriaDTO) throws ModuleException, ParseException {
		RollForwardBatchBL.doRollForwardLogic(invRollForwardCriteriaDTO);

	}

}
