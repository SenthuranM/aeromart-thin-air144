package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;

/**
 * 
 * @author MNs
 */
@Remote
public interface FlightInventoryResBDImpl extends FlightInventoryResBD {

}
