package com.isa.thinair.airinventory.core.persistence.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryCollection;
import com.isa.thinair.airinventory.api.dto.FCCSegONDCodesAndBookingClassesDTO;
import com.isa.thinair.airinventory.api.dto.FLCCAllocationDTO;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airreservation.api.dto.FlightChangeInfo;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;

public interface FlightInventoryJDBCDAO {

	public DefaultServiceResponse isFlightInventoryModelUpdatable(Collection<Integer> flightIds,
			Collection<AircraftCabinCapacity> cabinClassesUpdated, Collection<AircraftCabinCapacity> cabinClassesDeleted,
			boolean downGradeToAnyModel, boolean isScheduleChange) throws ModuleException;

	public Collection<FCCInventoryDTO> getFCCInventoryDTOs(int flightId, String cabinClassCode, Integer overlappingFlightId, boolean includeCnxFlts);

	public LinkedHashMap<String, List<Object[]>> getFCCSegBCInventories(int flightId, String cabinClassCode,
			boolean excludeInvalidSegments);

	public Collection<FCCInventoryDTO> getFlightInventoriesForOptimization(Collection<Integer> flightIds,
			OptimizeSeatInvCriteriaDTO criteria) throws ModuleException;

	public int[] getSoldOnholdFixedAndInfSeatsInSegs(Integer fccSegInvId);

	public Integer getAvailableFixedSeatCount(Integer fccSegInvId);

	public int[] getEffectiveAvailableFixedSeatCount(Integer fccSegInvId);

	public Map<String, int[]> getEffectiveSoldOnholdSeatsInInterceptingSegments(Collection<Integer> flightIds,
			Collection<String> segmentCodes, String logicalCabinClass,
			Collection<Integer> includingOrExcludingInventorySegmentIds, boolean isExcludeSegment);

	public Map<String, Map<String, int[]>> getTotalSoldSeatCountSegmentAndLogicalCabinClassWise(Integer flightId,
			Collection<String> singleLegSegmentCodes, String cabinClass);

	public List<String> getFlightSegmentCodes(Integer flightId);
	
	public List<FCCSegONDCodesAndBookingClassesDTO> getFCCSegONDCodesAndBookingClassesDTOs(Integer flightSegmentId);

	public FCCSegBCInventoryCollection getBucketsCollection(Integer fccsbInvId);

	public Collection<Object[]> getFCCSegIdAndSegBCIds(String bookingCode, Timestamp fromDate, Timestamp toDate);

	public Map<String, String> getCabinClassAvailability(CabinClassAvailabilityDTO cabinClassAvailabilityDTO);

	public FlightSegWithCCDTO getFlightSegmentDetailWithCabinClass(Integer fltSegId);

	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getAvailabilityForPublishing();
	
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getBCStatusAVS();


	public Object[] getMealsListForStation(List<String> hub, String mealNotificationBeginTime, String mealNotificationEndTime,
			int maxNoOfAttempts, List<Integer> flightIds);

	public Collection<InsurancePublisherDetailDTO> getInsuredFlightData();

	public Collection<InsurancePublisherDetailDTO> getFailedInsuredFlightData();

	public List<Integer> getOverBookedFlightSegmentIds(Collection<Integer> flightIds, String cabinClass, int seatCapacity);

	public List<Integer> getFCCSegInventoryIds(Collection<Integer> flightIds, String cabinClass);

	public Collection<FLCCAllocationDTO> getLogicalCabinClassWiseAllocations(int flightId, String cabinClassCode)
			throws ModuleException;

	public int getFlightLoadFactor(Integer flightSegmentId);

	public Map<Integer, BigDecimal> getFlightLoadFactor(List<Integer> flightSegmentIds);

	public List<Integer> getInterceptingSegmentIDs(List<Integer> sourceFltSegIds);

	public DefaultServiceResponse getAffectedFlights(Collection<Integer> flightIds,
			Collection<AircraftCabinCapacity> cabinClassesUpdated);

	public Map<Integer, Set<Integer>> getInterceptingFlightSegmentIds(List<Integer> sourceFltSegIds);

	public Collection<FlightChangeInfo> getFlightSeatChangeData(FlightSegement seg, String modelNumber);
}
