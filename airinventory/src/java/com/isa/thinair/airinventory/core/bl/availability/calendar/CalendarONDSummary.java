package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailDetails;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareCalendarDate;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareCalendarDate.FareCalendarDateStatus;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareCalendarOND;
import com.isa.thinair.airproxy.api.model.reservation.availability.SegInvSelection;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;

class CalendarONDSummary {

	private Map<String, CalendarOndFlight> dateWiseFlights = null;
	private Map<String, CalendarOndFlight> fareOndWiseSelectedFlight = null;
	private Set<String> bookingCodes = null;
	private OriginDestinationInformationTO ond;
	private int ondSequence;
	private Map<String, List<LightFareDTO>> fareONDWiseFareMap;

	public CalendarONDSummary(int ondSequence, OriginDestinationInformationTO ond,
			Map<String, List<LightFareDTO>> ondSequenceWiseFareOndWiseFare) {
		this.ond = ond;
		this.ondSequence = ondSequence;
		this.fareONDWiseFareMap = ondSequenceWiseFareOndWiseFare;
	}

	public void reduceToDateWiseMinimumFareOnd(IPaxCountAssembler paxAssmblr, List<LightFareDTO> fares,
			List<AvailableONDFlight> flights) throws ModuleException {
		Date fromDate = ond.getDepartureDateTimeStart();
		Date toDate = ond.getDepartureDateTimeEnd();
		FareProcessor fareProc = new FareProcessor(fares, paxAssmblr);
		DateProcessor dateProc = new DateProcessor(flights, fromDate, toDate);
		InventoryProcessor invProc = new InventoryProcessor(paxAssmblr);
		dateWiseFlights = new HashMap<String, CalendarOndFlight>();
		for (String date : dateProc.getDates()) {
			CalendarDateFlightTO calDTO = dateProc.getCalendarFlightTO(date);
			CalendarOndFlight selectedOndFlight = null;
			if (calDTO != null) {
				Set<String> distinctFltOnds = new HashSet<String>(calDTO.getDistinctFlightOnds());

				distinctFltOnds.retainAll(fareProc.getDistinctFareOnds());

				List<LightFareDTO> orderedFares = fareProc.getOrderedFaresFor(distinctFltOnds);
				for (LightFareDTO fare : orderedFares) {
					for (AvailableONDFlight flight : calDTO.getFlightFor(fare.getOndCode())) {
						if (invProc.hasValidInventory(flight, fare)) {
							if (selectedOndFlight == null) {
								selectedOndFlight = new CalendarOndFlight(fare, flight, paxAssmblr);
							}
							break;
						}
					}
				}

				for (String ondCode : distinctFltOnds) {
					if (selectedOndFlight == null) {
						for (AvailableONDFlight flight : calDTO.getFlightFor(ondCode)) {
							Map<FlightSegmentTO, LightFareDTO> segmentWiseFare = new HashMap<>();
							boolean allFlightFaresAdded = false;
							for (FlightSegmentTO flightSegment : flight.getFlightSegments()) {
								List<LightFareDTO> segmentFares = fareONDWiseFareMap.get(flightSegment.getSegmentCode());
								FARES: for (LightFareDTO fare : segmentFares) {
									if (invProc.hasValidInventory(flightSegment,
											flight.getSegmentInventoryAvailability(flightSegment), fare)) {
										segmentWiseFare.put(flightSegment, fare);
										allFlightFaresAdded = true;
										break FARES;
									} else {
										allFlightFaresAdded = false;
									}
								}
							}
							if (allFlightFaresAdded) {
								selectedOndFlight = new CalendarOndFlight(segmentWiseFare, flight, paxAssmblr);
								break;
							}
						}
					}
				}

				for (String ondCode : distinctFltOnds) {
					if (selectedOndFlight != null && calDTO.hasSubONDsFor(ondCode)) {
						List<String> subONDs = calDTO.getSubONDCodes(ondCode);
						SubONDProcessor subONDProc = new SubONDProcessor(ondCode, subONDs, selectedOndFlight.getTotalFareAmount(),
								fareProc, calDTO, invProc);
						CalendarOndFlight selectedSubOndFlight = subONDProc.processSubONDs(paxAssmblr);
						if (selectedSubOndFlight != null) {
							selectedOndFlight = selectedSubOndFlight;
						}
					}
				}

			}
			addFareOndWiseSelectedFlight(selectedOndFlight);
			dateWiseFlights.put(date, selectedOndFlight);
		}
	}

	private void addFareOndWiseSelectedFlight(CalendarOndFlight selectedOndFlight) throws ModuleException {
		if (selectedOndFlight != null) {
			String fareQuotedOnd = selectedOndFlight.getFareQuotedOnd();
			if (!getFareOndWiseSelectedFlights().containsKey(fareQuotedOnd)) {
				getFareOndWiseSelectedFlights().put(fareQuotedOnd, selectedOndFlight);
			}
			if (bookingCodes == null) {
				this.bookingCodes = new HashSet<String>();
			}
			for (LightFareDTO fare : selectedOndFlight.getFareMap().values()) {
				bookingCodes.add(fare.getBookingCode());
			}
		}

	}

	private Map<String, CalendarOndFlight> getFareOndWiseSelectedFlights() {
		if (this.fareOndWiseSelectedFlight == null) {
			this.fareOndWiseSelectedFlight = new HashMap<String, CalendarOndFlight>();
		}
		return fareOndWiseSelectedFlight;
	}

	public FareCalendarOND getFareCalendarOND(OriginDestinationInformationTO ond, CurrencyExchangeRate currencyExchangeRate) {
		FareCalendarOND fareOND = new FareCalendarOND(ond.getOrigin(), ond.getDestination(), ond.getPreferredClassOfService());

		SimpleDateFormat formatter = new SimpleDateFormat(DateProcessor.DATE_FORMAT_STRING);
		for (String dateKey : dateWiseFlights.keySet()) {
			Date date = null;
			try {
				date = formatter.parse(dateKey);
			} catch (ParseException e) {
				throw new ModuleRuntimeException("Invalid date format");
			}
			CalendarOndFlight ondFlight = dateWiseFlights.get(dateKey);
			FareCalendarDate fareCalDate = new FareCalendarDate(date, ondFlight == null
					? FareCalendarDateStatus.NOTAVL
					: FareCalendarDateStatus.AVL);
			if (ondFlight != null) {
				FareAvailDetails fareDetails = new FareAvailDetails();

				fareDetails.setFareAmount(getConvertedAmount(ondFlight.getTotalFareAmount(), currencyExchangeRate));
				fareDetails.setTotalAmount(getConvertedAmount(ondFlight.getTotalAmount(), currencyExchangeRate));
				for (FlightSegmentTO fltSegment : ondFlight.getFareMap().keySet()) {
					LightFareDTO fare = ondFlight.getFareMap().get(fltSegment);
					SegInvSelection selectedInvSegment = new SegInvSelection(fltSegment, fare.getBookingCode());
					fareDetails.addSelectedInvSegment(selectedInvSegment);
				}
				fareCalDate.setFareDetails(fareDetails);
			}
			fareOND.addCalendarDate(fareCalDate);
		}
		return fareOND;

	}

	private BigDecimal getConvertedAmount(BigDecimal amount, CurrencyExchangeRate currencyExchangeRate) {
		if (currencyExchangeRate == null) {
			return amount;
		}
		Currency currency = currencyExchangeRate.getCurrency();

		amount = AccelAeroRounderPolicy.convertAndRound(amount, currencyExchangeRate.getMultiplyingExchangeRate(),
				currency.getBoundryValue(), currency.getBreakPoint());
		return amount;
	}

	private Map<String, Collection<String>> getApplicableChargeGroups(Collection<String> bookingClasses) {
		if (bookingClasses != null && !bookingClasses.isEmpty()) {
			return getBookingClassDAO().getBookingClassChargeGroups(bookingClasses, null);
		}
		return null;
	}

	public void quoteCharges(PaxCountAssembler paxAssmblr, CalendarSearchRQ searchRQ) throws ModuleException {
		QuoteChargesCriteria baseCriteria = ChargeQuoteHelper.getBaseCriteria(new Date(), ond, searchRQ);
		boolean isFirstOnd = (ondSequence == 0);
		boolean isLastOnd = (ondSequence == (searchRQ.getOrderedOriginDestinationInformationList().size() - 1));
		Map<String, ChargeQuoteHelper> fareQuoteOndWiseQutoedCharges = new HashMap<String, ChargeQuoteHelper>();

		Map<String, Collection<String>> bcChargeGroups = getApplicableChargeGroups(this.bookingCodes);

		for (String fareQuotedOnd : getFareOndWiseSelectedFlights().keySet()) {
			CalendarOndFlight selectedOndFlight = getFareOndWiseSelectedFlights().get(fareQuotedOnd);
			ChargeQuoteHelper cqHelper = new ChargeQuoteHelper(selectedOndFlight, baseCriteria, isFirstOnd, isLastOnd);
			fareQuoteOndWiseQutoedCharges.put(fareQuotedOnd, cqHelper);
		}

		for (String date : dateWiseFlights.keySet()) {
			CalendarOndFlight ondFlt = dateWiseFlights.get(date);
			if (ondFlt != null) {
				ChargeQuoteHelper quoteHelper = fareQuoteOndWiseQutoedCharges.get(ondFlt.getFareQuotedOnd());
				quoteHelper.applyMatchingCharges(ondFlt, paxAssmblr, bcChargeGroups);
			}
		}
	}

	private BookingClassDAO getBookingClassDAO() {
		return (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
	}
}