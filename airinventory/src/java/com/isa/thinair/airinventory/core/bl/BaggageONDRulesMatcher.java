package com.isa.thinair.airinventory.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.baggage.ONDBaggageDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.ONDBaggageUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 */
public class BaggageONDRulesMatcher {

	private static BaggageJDBCDAO getBaggageJDBCDAO() {
		return (BaggageJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("baggageJDBCDAO");
	}

	private static FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance().getLocalBean(
				"flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	private static int getFlightLoadFactor(int flightSegmentId) {
		int load = getFlightInventoryJDBCDAO().getFlightLoadFactor(flightSegmentId);
		if (load > 100) {
			load = 100;
		}
		return load;
	}

	private static BigDecimal getTotalWeight(int flightSegmentId) {
		List<Integer> flightSegs = new ArrayList<Integer>();
		flightSegs.add(flightSegmentId);
		Map<Integer, Set<Integer>> interceptingSegs = getFlightInventoryJDBCDAO().getInterceptingFlightSegmentIds(flightSegs);
		return getBaggageJDBCDAO().getTotalBaggageWeight(interceptingSegs).values().iterator().next();
	}

	public static Map<Integer, List<FlightBaggageDTO>> getONDBaggages(List<FlightSegmentTO> flightSegmentTOs,
			boolean checkCutOver, boolean isRequote) throws ModuleException {
		flightSegmentTOs = removeBusSegments(flightSegmentTOs);
		Map<Integer, List<FlightBaggageDTO>> flightSegBaggageMap = new LinkedHashMap<Integer, List<FlightBaggageDTO>>();

		if (flightSegmentTOs.size() > 0) {
			Map<Integer, FlightBaggageDTO> pnrBaggagesMap = getPnrBaggagesMap(flightSegmentTOs);
			Collections.sort(flightSegmentTOs, new OldestFirstSortComparator());
			Map<String, List<FlightSegmentTO>> baggageOndWiseFlightSegments = getBaggageOndGroupIdWiseFlightSegments(flightSegmentTOs);

			Map<Integer, List<FlightBaggageDTO>> selectedBaggages = null;
			// TODO add the proper implementation to determine the create reservation flow or modify reservation flow
			if (flightSegmentTOs.get(0).getPnrSegId() != null) {
				selectedBaggages = getSelectedONDBaggages(flightSegmentTOs);
			}
			if (baggageOndWiseFlightSegments.size() > 0) {
				for (Entry<String, List<FlightSegmentTO>> entry : baggageOndWiseFlightSegments.entrySet()) {
					Map<Integer, List<FlightBaggageDTO>> tmpMap = getBaggagesONDWise(entry.getValue(), entry.getKey(),
							checkCutOver);
					if (selectedBaggages != null && selectedBaggages.size() > 0) {
						// flightSegBaggageMap.putAll(caterAdditionalParams(tmpMap, selectedBaggages));
						flightSegBaggageMap.putAll(tmpMap);
					} else {
						// flightSegBaggageMap.putAll(caterAdditionalParams(tmpMap, null));
						flightSegBaggageMap.putAll(tmpMap);
					}
				}
			}

			List<FlightSegmentTO> nonBaggageGrpWiseFlightSegmentTOs = getNonBaggageOndGroupWiseFlightSegment(flightSegmentTOs);
			flightSegBaggageMap
					.putAll(getOutboundInboundWiseBaggages(nonBaggageGrpWiseFlightSegmentTOs, checkCutOver, isRequote));

			appendPnrBaggages(flightSegBaggageMap, pnrBaggagesMap);
		}

		return flightSegBaggageMap;
	}

	private static void appendPnrBaggages(Map<Integer, List<FlightBaggageDTO>> flightSegBaggageMap,
			Map<Integer, FlightBaggageDTO> pnrBaggagesMap) {

		if (pnrBaggagesMap.size() > 0 && flightSegBaggageMap.size() > 0) {
			for (Entry<Integer, FlightBaggageDTO> entry : pnrBaggagesMap.entrySet()) {
				List<FlightBaggageDTO> lstFlightBaggageDTO = flightSegBaggageMap.get(entry.getKey());
				if (lstFlightBaggageDTO.size() > 0) {
					FlightBaggageDTO customFlightBaggageDTO = composeFlightBaggageDTO(entry.getValue(),
							BeanUtils.getFirstElement(lstFlightBaggageDTO));

					Map<Integer, FlightBaggageDTO> filteredBaggages = new HashMap<Integer, FlightBaggageDTO>();
					for (FlightBaggageDTO flightBaggageDTO : lstFlightBaggageDTO) {
						filteredBaggages.put(flightBaggageDTO.getBaggageId(), flightBaggageDTO);
					}

					filteredBaggages.put(customFlightBaggageDTO.getBaggageId(), customFlightBaggageDTO);
					flightSegBaggageMap.put(entry.getKey(), new ArrayList<FlightBaggageDTO>(filteredBaggages.values()));
				}
			}
		}
	}

	private static FlightBaggageDTO composeFlightBaggageDTO(FlightBaggageDTO value, FlightBaggageDTO firstElement) {
		value.setChargeId(firstElement.getChargeId());
		value.setTemplateId(firstElement.getTemplateId());
		value.setSeatFactor(firstElement.getSeatFactor());
		value.setTotalWeight(firstElement.getTotalWeight());
		value.setDefaultBaggage(firstElement.getDefaultBaggage());
		value.setOndGroupId(firstElement.getOndGroupId());
		value.setInactiveSelectedBaggage(firstElement.isInactiveSelectedBaggage());
		return value;
	}

	private static Map<Integer, FlightBaggageDTO> getPnrBaggagesMap(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		List<String> lstPnrSegIds = new ArrayList<String>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			String pnrSegId = BeanUtils.nullHandler(flightSegmentTO.getPnrSegId());
			if (pnrSegId.length() > 0) {
				lstPnrSegIds.add(pnrSegId);
			}
		}

		Map<Integer, FlightBaggageDTO> fltBaggageMap = new HashMap<Integer, FlightBaggageDTO>();

		if (lstPnrSegIds.size() > 0) {
			List<FlightBaggageDTO> lstFlightBaggageDTO = getBaggageJDBCDAO().getPnrBaggages(lstPnrSegIds);

			for (FlightBaggageDTO flightBaggageDTO : lstFlightBaggageDTO) {
				fltBaggageMap.put(flightBaggageDTO.getFlightSegmentID(), flightBaggageDTO);
			}
		}

		return fltBaggageMap;
	}

	private static List<FlightSegmentTO> removeBusSegments(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		List<FlightSegmentTO> removeLstFlightSegmentTO = new ArrayList<FlightSegmentTO>();
		for (FlightSegmentTO flightSeg : flightSegmentTOs) {
			if (AirInventoryModuleUtils.getAirportBD().isBusSegment(flightSeg.getSegmentCode())) {
				removeLstFlightSegmentTO.add(flightSeg);
			}
		}
		if (removeLstFlightSegmentTO.size() > 0) {
			flightSegmentTOs.removeAll(removeLstFlightSegmentTO);
		}

		return flightSegmentTOs;
	}

	private static Map<Integer, List<FlightBaggageDTO>> getOutboundInboundWiseBaggages(List<FlightSegmentTO> flightSegmentTOs,
			boolean checkCutOver, boolean isRequote) throws ModuleException {

		Map<Integer, List<FlightBaggageDTO>> flightSegBaggageMap = new LinkedHashMap<Integer, List<FlightBaggageDTO>>();

		if (isRequote) {
			for (int ondSeq = 0; ondSeq < flightSegmentTOs.size(); ondSeq++) {
				List<FlightSegmentTO> flightListByOndSeq = new ArrayList<FlightSegmentTO>();
				Iterator<FlightSegmentTO> flightSegmentTOItr = flightSegmentTOs.iterator();
				while (flightSegmentTOItr.hasNext()) {
					FlightSegmentTO fltSegmentTO = flightSegmentTOItr.next();
					if (fltSegmentTO != null && ondSeq == fltSegmentTO.getOndSequence()) {
						flightListByOndSeq.add(fltSegmentTO);
					}

				}
				Map<Integer, List<FlightBaggageDTO>> tmpMap = getBaggagesONDWise(flightListByOndSeq, null, checkCutOver);
				// flightSegBaggageMap.putAll(caterAdditionalParams(tmpMap, null));
				flightSegBaggageMap.putAll(tmpMap);

			}

		} else {
			List<FlightSegmentTO> depLstFlightSegmentTO = getFlightSegmentTOs(flightSegmentTOs, false);
			List<FlightSegmentTO> retLstFlightSegmentTO = getFlightSegmentTOs(flightSegmentTOs, true);

			if (depLstFlightSegmentTO.size() > 0) {
				Map<Integer, List<FlightBaggageDTO>> tmpMap = getBaggagesONDWise(depLstFlightSegmentTO, null, checkCutOver);

				flightSegBaggageMap.putAll(tmpMap);
			}

			if (retLstFlightSegmentTO.size() > 0) {
				Map<Integer, List<FlightBaggageDTO>> tmpMap = getBaggagesONDWise(retLstFlightSegmentTO, null, checkCutOver);
				// flightSegBaggageMap.putAll(caterAdditionalParams(tmpMap, null));
				flightSegBaggageMap.putAll(tmpMap);
			}

		}

		List<List<FlightSegmentTO>> splitFlightSegs = AirinventoryUtils.splitFlightSegmentTOs(flightSegmentTOs, isRequote);
		for (List<FlightSegmentTO> splitFlightSeg : splitFlightSegs) {
			Map<Integer, List<FlightBaggageDTO>> tmpMap = getBaggagesONDWise(splitFlightSeg, null, checkCutOver);
			// flightSegBaggageMap.putAll(caterAdditionalParams(tmpMap, null));
			flightSegBaggageMap.putAll(tmpMap);
		}

		return flightSegBaggageMap;
	}

	private static Map<Integer, List<FlightBaggageDTO>> getBaggagesONDWise(List<FlightSegmentTO> flightSegmentTOs,
			String baggageONDGroupId, boolean checkCutoverTime) throws ModuleException {
		Map<Integer, List<FlightBaggageDTO>> flightSegBaggageMap = new LinkedHashMap<Integer, List<FlightBaggageDTO>>();

		for (List<FlightSegmentTO> fltSegs : splitOndForSegs(flightSegmentTOs)) {
			ONDBaggageDTO ondBaggageDTO = ONDBaggageUtil.getONDBaggageDTO(fltSegs);

			if (ondBaggageDTO.isSegmentSearch()) {
				if (isBaggageCutOverTimeReached(fltSegs) && checkCutoverTime) {
					for (FlightSegmentTO flightSegmentTO : fltSegs) {
						flightSegBaggageMap.put(flightSegmentTO.getFlightSegId(), new ArrayList<FlightBaggageDTO>());
					}
				} else {
					int newOndGrpId = getNewBaggageONDGroupId(baggageONDGroupId);
					List<FlightBaggageDTO> segBaggages = getOneSegAllONDBaggages(ondBaggageDTO, newOndGrpId);
					for (FlightSegmentTO flightSegmentTO : fltSegs) {
						flightSegBaggageMap.put(flightSegmentTO.getFlightSegId(), segBaggages);
					}
				}
			} else {
				if (isBaggageCutOverTimeReached(fltSegs) && checkCutoverTime) {
					for (FlightSegmentTO flightSegmentTO : fltSegs) {
						flightSegBaggageMap.put(flightSegmentTO.getFlightSegId(), new ArrayList<FlightBaggageDTO>());
					}
				} else {
					int newOndGrpId = getNewBaggageONDGroupId(baggageONDGroupId);
					List<FlightBaggageDTO> flightBaggageDTOs = getAllONDBaggages(ondBaggageDTO, newOndGrpId);
					if (flightBaggageDTOs.size() > 0) {
						for (FlightSegmentTO flightSegmentTO : fltSegs) {
							flightSegBaggageMap.put(flightSegmentTO.getFlightSegId(), flightBaggageDTOs);
						}
					}
				}
			}
		}

		return flightSegBaggageMap;
	}

	private static int getNewBaggageONDGroupId(String baggageONDGroupId) {
		baggageONDGroupId = BeanUtils.nullHandler(baggageONDGroupId);
		if (baggageONDGroupId.length() > 0) {
			return new Integer(baggageONDGroupId);
		} else {
			return getBaggageJDBCDAO().getNextBaggageOndGroupId();
		}
	}

	private static boolean isBaggageCutOverTimeReached(List<FlightSegmentTO> lstFltSegs) {
		for (IFlightSegment fltSeg : lstFltSegs) {
			boolean isWithInFinalCutOver = BaggageTimeWatcher.isWithInFinalCutOver(fltSeg.getDepartureDateTimeZulu());

			if (!isWithInFinalCutOver) {
			} else {
				return true;
			}
		}

		return false;
	}

	private static List<FlightSegmentTO> getNonBaggageOndGroupWiseFlightSegment(List<FlightSegmentTO> flightSegmentTOs) {

		List<FlightSegmentTO> nonBaggageWiseFlightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			String baggeONDGroupId = BeanUtils.nullHandler(flightSegmentTO.getBaggageONDGroupId());
			if (baggeONDGroupId.length() == 0) {
				nonBaggageWiseFlightSegmentTOs.add(flightSegmentTO);
			}
		}

		return nonBaggageWiseFlightSegmentTOs;
	}

	private static Map<String, List<FlightSegmentTO>> getBaggageOndGroupIdWiseFlightSegments(
			List<FlightSegmentTO> flightSegmentTOs) {

		Map<String, List<FlightSegmentTO>> baggageOndGrpIdWiseFlightSegments = new LinkedHashMap<String, List<FlightSegmentTO>>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			String baggeONDGroupId = BeanUtils.nullHandler(flightSegmentTO.getBaggageONDGroupId());
			if (baggeONDGroupId.length() > 0) {
				List<FlightSegmentTO> lstFlightSegmentTO = baggageOndGrpIdWiseFlightSegments.get(baggeONDGroupId);

				if (lstFlightSegmentTO == null) {
					lstFlightSegmentTO = new ArrayList<FlightSegmentTO>();
					lstFlightSegmentTO.add(flightSegmentTO);

					baggageOndGrpIdWiseFlightSegments.put(baggeONDGroupId, lstFlightSegmentTO);
				} else {
					lstFlightSegmentTO.add(flightSegmentTO);
				}
			}
		}

		return baggageOndGrpIdWiseFlightSegments;
	}

	/* --------------------------- Utilities --------------------------- */

	private static Map<Integer, List<FlightBaggageDTO>> caterAdditionalParams(
			Map<Integer, List<FlightBaggageDTO>> flightSegBaggageMap, Map<Integer, List<FlightBaggageDTO>> selectedBaggageMap) {

		if (flightSegBaggageMap.size() > 0) {
			Integer flightSegmentId = BeanUtils.getFirstElement(flightSegBaggageMap.keySet());
			int flightLoadFactor = getFlightLoadFactor(flightSegmentId);
			BigDecimal totalWeight = getTotalWeight(flightSegmentId);

			if (flightSegmentId != null) {
				Set<FlightBaggageDTO> alreadyAvailableSelectedBaggages = new HashSet<FlightBaggageDTO>();
				for (Entry<Integer, List<FlightBaggageDTO>> entry : flightSegBaggageMap.entrySet()) {
					List<FlightBaggageDTO> lstFlightBaggageDTO = entry.getValue();
					if (lstFlightBaggageDTO != null && lstFlightBaggageDTO.size() > 0) {

						List<FlightBaggageDTO> removingFlightBaggeDTO = new ArrayList<FlightBaggageDTO>();
						loop: for (FlightBaggageDTO flightBaggageDTO : lstFlightBaggageDTO) {
							if (selectedBaggageMap != null && selectedBaggageMap.size() > 0) {
								List<FlightBaggageDTO> lstSelectedFlightBaggageDTO = selectedBaggageMap.get(flightSegmentId);
								for (FlightBaggageDTO selectedBaggage : lstSelectedFlightBaggageDTO) {
									if (selectedBaggage.getBaggageId().intValue() == flightBaggageDTO.getBaggageId()) {
										if (flightLoadFactor > flightBaggageDTO.getSeatFactor()) {
											flightBaggageDTO.setInactiveSelectedBaggage(true);
										}
										alreadyAvailableSelectedBaggages.add(selectedBaggage);
										continue loop;
									}
								}
							}
							if (flightLoadFactor > flightBaggageDTO.getSeatFactor()) {
								removingFlightBaggeDTO.add(flightBaggageDTO);
							} else if ((totalWeight.add(flightBaggageDTO.getBaggageWeight())).compareTo(flightBaggageDTO
									.getTotalWeight()) > 0) {
								// current total weight + baggage weight > allowed total weight
								removingFlightBaggeDTO.add(flightBaggageDTO);
							}
						}

						if (removingFlightBaggeDTO.size() > 0) {
							lstFlightBaggageDTO.removeAll(removingFlightBaggeDTO);
						}
					}
				}
				if (selectedBaggageMap != null && selectedBaggageMap.containsKey(flightSegmentId)
						&& selectedBaggageMap.get(flightSegmentId).size() > alreadyAvailableSelectedBaggages.size()) {
					for (FlightBaggageDTO selectedBaggage : selectedBaggageMap.get(flightSegmentId)) {
						if (alreadyAvailableSelectedBaggages.contains(selectedBaggage)) {
							continue;
						}
						selectedBaggage.setInactiveSelectedBaggage(true);
						if (!flightSegBaggageMap.containsKey(flightSegmentId)) {
							flightSegBaggageMap.put(flightSegmentId, new ArrayList<FlightBaggageDTO>());
						}
						flightSegBaggageMap.get(flightSegmentId).add(selectedBaggage);
					}
				}
			}
		}
		return flightSegBaggageMap;
	}

	private static List<FlightSegmentTO> getFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs, boolean isReturnSectors) {
		List<FlightSegmentTO> depLstFlightSegmentTO = new ArrayList<FlightSegmentTO>();
		List<FlightSegmentTO> retLstFlightSegmentTO = new ArrayList<FlightSegmentTO>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.isReturnFlag()) {
				retLstFlightSegmentTO.add(flightSegmentTO);
			} else {
				depLstFlightSegmentTO.add(flightSegmentTO);
			}
		}

		if (isReturnSectors) {
			return retLstFlightSegmentTO;
		} else {
			return depLstFlightSegmentTO;
		}
	}

	private static List<List<FlightSegmentTO>> splitOndForSegs(List<FlightSegmentTO> flightSegmentTOs) {
		List<List<FlightSegmentTO>> list = new ArrayList<List<FlightSegmentTO>>();
		String lastAirport = null;
		int ondSeq = -1;
		for (FlightSegmentTO seg : flightSegmentTOs) {
			String arr[] = seg.getSegmentCode().split("/");
			if (lastAirport == null || !lastAirport.equals(arr[0])) {
				list.add(new ArrayList<FlightSegmentTO>());
				ondSeq++;
			}
			lastAirport = arr[arr.length - 1];
			list.get(ondSeq).add(seg);
		}
		return list;
	}

	private static List<FlightBaggageDTO> getAllONDBaggages(ONDBaggageDTO ondBaggageDTO, int newOndGrpId) throws ModuleException {
		// get baggages for OND
		ondBaggageDTO.setOndGroupId(BeanUtils.nullHandler(newOndGrpId));
		List<FlightBaggageDTO> flightBaggageDTOs = getBaggageJDBCDAO().getONDBaggageDTOs(ondBaggageDTO);
		if (flightBaggageDTOs.size() == 0) {
			ondBaggageDTO.setOndCode("All/All");
			flightBaggageDTOs = getBaggageJDBCDAO().getONDBaggageDTOs(ondBaggageDTO);
		}

		return flightBaggageDTOs;
	}

	private static List<FlightBaggageDTO> getOneSegAllONDBaggages(ONDBaggageDTO ondBaggageDTO, int newOndGrpId)
			throws ModuleException {
		// get baggages for OND
		ondBaggageDTO.setOndGroupId(BeanUtils.nullHandler(newOndGrpId));
		List<FlightBaggageDTO> flightBaggageDTOs = getBaggageJDBCDAO().getONDBaggageDTOs(ondBaggageDTO);

		if (flightBaggageDTOs.size() == 0) {
			String lastSegCode = ondBaggageDTO.getSegCodes().get(ondBaggageDTO.getSegCodes().size() - 1);
			String[] contents = lastSegCode.split("/");
			ondBaggageDTO.setOndCode("All/" + contents[contents.length - 1]);
			flightBaggageDTOs = getBaggageJDBCDAO().getONDBaggageDTOs(ondBaggageDTO);
		}

		if (flightBaggageDTOs.size() == 0) {
			ondBaggageDTO.setOndCode(ondBaggageDTO.getSegCodes().get(0).split("/")[0] + "/All");
			flightBaggageDTOs = getBaggageJDBCDAO().getONDBaggageDTOs(ondBaggageDTO);
		}

		if (flightBaggageDTOs.size() == 0) {
			// search All/All
			ondBaggageDTO.setOndCode("All/All");
			flightBaggageDTOs = getBaggageJDBCDAO().getONDBaggageDTOs(ondBaggageDTO);
		}

		return flightBaggageDTOs;
	}

	private static class OldestFirstSortComparator implements Comparator<FlightSegmentTO> {

		@Override
		public int compare(FlightSegmentTO flightSegmentTO, FlightSegmentTO compareTo) {
			return (flightSegmentTO.getDepartureDateTimeZulu() == null || compareTo.getDepartureDateTimeZulu() == null) ? 0
					: flightSegmentTO.getDepartureDateTimeZulu().compareTo(compareTo.getDepartureDateTimeZulu());
		}
	}

	private static Map<Integer, List<FlightBaggageDTO>> getSelectedONDBaggages(List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {

		Map<Integer, List<FlightBaggageDTO>> selectedBaggageFlightSegWise = new LinkedHashMap<Integer, List<FlightBaggageDTO>>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			List<FlightBaggageDTO> lstFlightBaggageDTO = getBaggageJDBCDAO().getSelectedBaggages(flightSegmentTO.getPnrSegId());
			selectedBaggageFlightSegWise.put(flightSegmentTO.getFlightSegId(), lstFlightBaggageDTO);
		}
		return selectedBaggageFlightSegWise;
	}

}
