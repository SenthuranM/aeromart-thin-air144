package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.baggage.ONDBaggageDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.FlightBaggageStatuses;
import com.isa.thinair.airinventory.core.bl.BaggageSelector;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageJDBCDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author mano
 */
public class BaggageJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements BaggageJDBCDAO {

	@SuppressWarnings("unchecked")
	public List<FlightBaggageDTO> getBaggageDTOs(int flightSegId, String cabinClass, String logicalCabinClass)
			throws ModuleException {

		String query = "select fbc.flt_seg_id, fbc.flt_seg_baggage_charge_id, bc.baggage_template_id, bc.baggage_charge_id, bc.is_default, bg.baggage_id, fbc.amount, "
				+ " bg.baggage_weight, bg.baggage_name,fbc.sold_pieces,bg.baggage_desc, bc.cabin_class_code "
				+ " from  bg_t_flt_seg_baggage_charge fbc, bg_t_baggage bg, bg_t_baggage_charge bc"
				+ " where fbc.baggage_charge_id = bc.baggage_charge_id " + " and bc.baggage_id = bg.baggage_id "
				+ " and fbc.flt_seg_id = " + flightSegId + " and fbc.status = 'ACT' "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "bc")
				+ " order by bg.baggage_name";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

				while (rs.next()) {

					FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
					baggageDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					baggageDTO.setAmount(rs.getBigDecimal("amount"));
					baggageDTO.setBaggageWeight(rs.getBigDecimal("baggage_weight"));
					baggageDTO.setBaggageName(rs.getString("baggage_name"));
					baggageDTO.setBaggageId(rs.getInt("baggage_id"));
					baggageDTO.setSoldPieces(rs.getInt("sold_pieces"));
					baggageDTO.setChargeId(rs.getInt("baggage_charge_id"));
					baggageDTO.setBaggageDescription(rs.getString("baggage_desc"));
					baggageDTO.setTemplateId(rs.getInt("baggage_template_id"));
					baggageDTO.setFlightBaggageId(rs.getInt("flt_seg_baggage_charge_id"));
					baggageDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					baggageDTO.setDefaultBaggage(rs.getString("is_default"));
					colBaggages.add(baggageDTO);
				}
				return colBaggages;
			}
		});

		return colBaggageDtos;

	}

	@SuppressWarnings("unchecked")
	public List<FlightBaggageDTO> getBaggageDTOsWithTranslations(int flightSegId, String cabinClass, String logicalCabinClass,
			String selectedLanguage) throws ModuleException {

		String query = "select fbc.flt_seg_id, fbc.flt_seg_baggage_charge_id, bc.baggage_template_id, bc.baggage_charge_id, bc.is_default, bg.baggage_id, fbc.amount, "
				+ " bg.baggage_name,fbc.sold_pieces,bg.baggage_desc, bc.cabin_class_code, msg.message_content "
				+ " from  bg_t_flt_seg_baggage_charge   fbc, bg_t_baggage bg, bg_t_baggage_charge bc, t_i18n_message msg "
				+ " where fbc.baggage_charge_id = bc.baggage_charge_id " + " and bc.baggage_id = bg.baggage_id "
				+ " and bg.i18n_message_key = msg.i18n_message_key " + " and msg.message_locale = '" + selectedLanguage + "' "
				+ " and fbc.flt_seg_id = " + flightSegId + " and fbc.status = 'ACT' "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "bc")
				+ " order by bg.baggage_name";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

				while (rs.next()) {

					FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
					baggageDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					baggageDTO.setAmount(rs.getBigDecimal("amount"));
					baggageDTO.setBaggageName(rs.getString("baggage_name"));
					baggageDTO.setBaggageId(rs.getInt("baggage_id"));
					baggageDTO.setSoldPieces(rs.getInt("sold_pieces"));
					baggageDTO.setChargeId(rs.getInt("baggage_charge_id"));
					baggageDTO.setBaggageDescription(rs.getString("baggage_desc"));
					baggageDTO.setTemplateId(rs.getInt("baggage_template_id"));
					baggageDTO.setFlightBaggageId(rs.getInt("flt_seg_baggage_charge_id"));
					baggageDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					baggageDTO.setDefaultBaggage(rs.getString("is_default"));
					if (null != rs.getClob("message_content")) {
						baggageDTO.setTranslatedBaggageDescription(StringUtil.clobToString(rs.getClob("message_content")));
					}
					colBaggages.add(baggageDTO);
				}
				return colBaggages;
			}
		});

		return colBaggageDtos;

	}

	@SuppressWarnings("unchecked")
	public Collection<FlightBaggageDTO> getSoldBaggages(int flightSegID) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		String sql;
		sb.append(" select b.baggage_id, bag.baggage_name, count(b.baggage_id) as sold_baggage ");
		sb.append(" from bg_t_pnr_pax_seg_baggage b, bg_t_baggage bag ");
		sb.append(
				" where b.status != 'CNX' and b.baggage_id is not null and b.baggage_id = bag.baggage_id and b.pnr_seg_id in ( ");
		sb.append(" 	select a.pnr_seg_id ");
		sb.append(" 	from t_pnr_segment a ");
		sb.append("  	where a.flt_seg_id = '" + flightSegID + "')");
		sb.append(" group by (b.baggage_id, bag.baggage_name)");

		sql = sb.toString();

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Collection<FlightBaggageDTO>) template.query(sql, new ResultSetExtractor() {
			Collection<FlightBaggageDTO> colTemplateIds = new ArrayList<FlightBaggageDTO>();

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					FlightBaggageDTO flightBaggageDTO = new FlightBaggageDTO();
					flightBaggageDTO.setBaggageId(rs.getInt("baggage_id"));
					flightBaggageDTO.setBaggageName(rs.getString("baggage_name"));
					flightBaggageDTO.setSoldPieces(rs.getInt("sold_baggage"));
					colTemplateIds.add(flightBaggageDTO);
				}
				return colTemplateIds;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightBaggageDTO> getTempleateIds(int flightId) throws ModuleException {

		String sql = "SELECT distinct(d.baggage_template_id), a.flt_seg_id, a.segment_code, b.cabin_class_code,b.logical_cabin_class_code "
				+ " FROM t_flight_segment a, bg_t_flt_seg_baggage_charge b, bg_t_baggage_charge c, bg_t_baggage_template d"
				+ " WHERE a.flight_id=" + flightId
				+ "  AND a.flt_seg_id = b.flt_seg_id AND b.baggage_charge_id = c.baggage_charge_id"
				+ " AND c.baggage_template_id = d.baggage_template_id and b.status = 'ACT' ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Collection<FlightBaggageDTO>) template.query(sql, new ResultSetExtractor() {
			Collection<FlightBaggageDTO> colTemplateIds = new ArrayList<FlightBaggageDTO>();

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					FlightBaggageDTO flightBaggageDTO = new FlightBaggageDTO();
					flightBaggageDTO.setTemplateId(rs.getInt("baggage_template_id"));
					flightBaggageDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					flightBaggageDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					flightBaggageDTO.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
					flightBaggageDTO.setFlightSegmentCode(rs.getString("segment_code"));
					colTemplateIds.add(flightBaggageDTO);
				}
				return colTemplateIds;
			}
		});
	}

	private static String getCabinClassCondition(String classOfService) {
		return (classOfService != null && !"".equals(classOfService)) ? (" AND b.cabin_class_code='" + classOfService + "'") : "";
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightBaggageDTO> getAvailbleBaggageDTOs(int flightSegID, Collection<Integer> baggageIds)
			throws ModuleException {

		// TODO if cabin class needed

		String query = "select fbc.flt_seg_id, fbc.flt_seg_baggage_charge_id, bc.baggage_template_id, bc.baggage_charge_id, bg.baggage_id, bc.amount, "
				+ " bg.baggage_name, fbc.sold_pieces, bg.baggage_desc, fbc.cabin_class_code "
				+ " from bg_t_flt_seg_baggage_charge fbc, bg_t_baggage bg, bg_t_baggage_charge bc "
				+ " where fbc.baggage_charge_id = bc.baggage_charge_id " + " and bc.baggage_id = bg.baggage_id "
				+ " and fbc.flt_seg_id = " + flightSegID + " and bc.baggage_id in ( "
				+ Util.buildIntegerInClauseContent(baggageIds) + ") " + " and fbc.status = 'ACT' " + " order by bg.baggage_name";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Collection<FlightBaggageDTO> colBaggageDtos = (Collection<FlightBaggageDTO>) template.query(query,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

						while (rs.next()) {
							FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
							baggageDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
							baggageDTO.setAmount(rs.getBigDecimal("amount"));
							baggageDTO.setBaggageName(rs.getString("baggage_name"));
							baggageDTO.setBaggageId(rs.getInt("baggage_id"));
							baggageDTO.setSoldPieces(rs.getInt("sold_pieces"));
							baggageDTO.setChargeId(rs.getInt("baggage_charge_id"));
							baggageDTO.setBaggageDescription(rs.getString("baggage_desc"));
							baggageDTO.setTemplateId(rs.getInt("baggage_template_id"));
							baggageDTO.setFlightBaggageId(rs.getInt("flt_seg_baggage_charge_id"));
							baggageDTO.setCabinClassCode(BeanUtils.nullHandler(rs.getString("cabin_class_code")));
							colBaggages.add(baggageDTO);
						}
						return colBaggages;
					}
				});

		return colBaggageDtos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightBaggageDTO> getONDBaggageDTOs(final ONDBaggageDTO ondBaggageDTO) throws ModuleException {

		String sql = "select distinct b.baggage_name, b.baggage_desc, b.baggage_weight, bc.seat_factor, ct.charge_template_code, bc.cabin_class_code, bc.amount, "
				+ " b.baggage_id, b.status, bc.ond_baggage_charge_id, ct.ond_charge_template_id, bc.is_default, bc.total_weight_limit "
				+ " from bg_t_ond_bagg_charge_template bct,  bg_t_ond_charge_template ct,  bg_t_ond_baggage_charge bc, bg_t_baggage b "
				+ " where bct.ond_charge_template_id = ct.ond_charge_template_id "
				+ " and bc.ond_charge_template_id    = ct.ond_charge_template_id "
				+ " and bc.baggage_id                = b.baggage_id " + " and ct.status = '" + Baggage.STATUS_ACTIVE
				+ "' and bct.ond_baggage_ond_code like ? "
				+ " and ((ct.from_date is null and ct.end_date is null) OR (? between ct.from_date and ct.end_date)) ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(sql,
				new Object[] { ondBaggageDTO.getOndCode(), ondBaggageDTO.getFromDate() }, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

						while (rs.next()) {
							FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
							baggageDTO.setAmount(rs.getBigDecimal("amount"));
							baggageDTO.setBaggageName(rs.getString("baggage_name"));
							baggageDTO.setBaggageWeight(rs.getBigDecimal("baggage_weight"));
							baggageDTO.setBaggageId(rs.getInt("baggage_id"));
							baggageDTO.setChargeId(rs.getInt("ond_baggage_charge_id"));
							baggageDTO.setBaggageDescription(rs.getString("baggage_desc"));
							baggageDTO.setTemplateId(rs.getInt("ond_charge_template_id"));
							baggageDTO.setSeatFactor(rs.getInt("seat_factor"));
							baggageDTO.setDefaultBaggage(rs.getString("is_default"));
							baggageDTO.setTotalWeight(rs.getBigDecimal("total_weight_limit"));
							baggageDTO.setBaggageStatus(rs.getString("status"));
							baggageDTO.setOndGroupId(ondBaggageDTO.getOndGroupId());
							colBaggages.add(baggageDTO);
						}
						return colBaggages;
					}
				});

		return colBaggageDtos;
	}

	public Map<Integer, List<FlightBaggageDTO>> getONDBaggageDTOs(List<Integer> templateIds, List<Integer> templateChargeIds,
			String cabinClassCode, String logicalCC) throws ModuleException {
		Map<Integer, List<FlightBaggageDTO>> baggageDtos;
		StringBuilder queryContent = new StringBuilder();

		queryContent.append("   SELECT ct.ond_charge_template_id, b.baggage_name, b.baggage_desc,b.status, ")
				.append("       b.baggage_weight, bc.seat_factor, bc.seat_factor_cond_apply_for , ct.charge_template_code,  ")
				.append("       bc.cabin_class_code, bc.amount, b.baggage_id,   ")
				.append("       bc.ond_baggage_charge_id, bc.is_default, bc.total_weight_limit,  ")
				.append("       bc.cabin_class_code, bc.logical_cabin_class_code  ")
				.append("   FROM bg_t_ond_charge_template ct, bg_t_ond_baggage_charge bc, bg_t_baggage b    ")
				.append("   WHERE  bc.ond_charge_template_id = ct.ond_charge_template_id    ")
				.append("       AND bc.baggage_id = b.baggage_id ");

		Object args[] = null;
		if (cabinClassCode != null && !"".equals(cabinClassCode)) {
			queryContent.append(" AND ( bc.cabin_class_code = ? ");

			if (logicalCC != null && !"".equals(logicalCC)) {
				queryContent.append(" OR bc.logical_cabin_class_code = ? ");
				args = new Object[] { cabinClassCode, logicalCC };
			} else {
				args = new Object[] { cabinClassCode };
			}
			queryContent.append(" ) ");
		}
	
		if (templateIds != null && !templateIds.isEmpty()) {
			queryContent.append("   AND ct.ond_charge_template_id IN ( ")
					.append(BeanUtils.constructINString(templateIds) + " ) ");
		}

		if (templateChargeIds != null && !templateChargeIds.isEmpty()) {
			queryContent.append("   AND bc.ond_baggage_charge_id IN ( ")
					.append(BeanUtils.constructINString(templateChargeIds) + " ) ");
		}

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		baggageDtos = (Map<Integer, List<FlightBaggageDTO>>) template.query(queryContent.toString(), args,
				new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						Map<Integer, List<FlightBaggageDTO>> ret = new HashMap<Integer, List<FlightBaggageDTO>>();
						int templateId;
						FlightBaggageDTO baggageDTO;

						while (resultSet.next()) {
							templateId = resultSet.getInt("ond_charge_template_id");
							baggageDTO = new FlightBaggageDTO();

							if (!ret.containsKey(templateId)) {
								ret.put(templateId, new ArrayList<FlightBaggageDTO>());
							}

							baggageDTO.setAmount(resultSet.getBigDecimal("amount"));
							baggageDTO.setBaggageName(resultSet.getString("baggage_name"));
							baggageDTO.setBaggageWeight(resultSet.getBigDecimal("baggage_weight"));
							baggageDTO.setBaggageId(resultSet.getInt("baggage_id"));
							baggageDTO.setChargeId(resultSet.getInt("ond_baggage_charge_id"));
							baggageDTO.setBaggageDescription(resultSet.getString("baggage_desc"));
							baggageDTO.setTemplateId(templateId);
							baggageDTO.setSeatFactor(resultSet.getInt("seat_factor"));
							baggageDTO.setSeatFactorConditionApplyFor(resultSet.getInt("seat_factor_cond_apply_for"));
							baggageDTO.setDefaultBaggage(resultSet.getString("is_default"));
							baggageDTO.setTotalWeight(resultSet.getBigDecimal("total_weight_limit"));
							baggageDTO.setBaggageStatus(resultSet.getString("status"));
							baggageDTO.setCabinClassCode(resultSet.getString("cabin_class_code"));
							baggageDTO.setLogicalCCCode(resultSet.getString("logical_cabin_class_code"));

							ret.get(templateId).add(baggageDTO);
						}

						return ret;
					}
				});

		return baggageDtos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getNextBaggageOndGroupId() {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT s_baggage_ond_group_id.NEXTVAL OND_GROUP_ID FROM DUAL ";

		Integer nextBaggageOndGroupId = (Integer) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer nextBaggageOndGroupId = null;

				if (rs != null) {
					if (rs.next()) {
						nextBaggageOndGroupId = rs.getInt("OND_GROUP_ID");
					}
				}

				return nextBaggageOndGroupId;
			}
		});

		return nextBaggageOndGroupId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightBaggageDTO> getPnrBaggages(List<String> colPnrSegIds) throws ModuleException {
		String sql = "SELECT DISTINCT C.BAGGAGE_ID, C.BAGGAGE_DESC, C.BAGGAGE_NAME, B.AMOUNT, D.FLT_SEG_ID, D.PNR_SEG_ID "
				+ " FROM BG_T_PNR_PAX_SEG_BAGGAGE A, BG_T_FLT_SEG_BAGGAGE_CHARGE B, BG_T_BAGGAGE C, T_PNR_SEGMENT D "
				+ " WHERE A.PNR_SEG_ID IN (" + BeanUtils.constructINString(colPnrSegIds) + ") AND "
				+ " (A.IS_OND = 'N' OR A.OND_BAGGAGE_CHARGE_ID IS NULL) AND B.FLT_SEG_BAGGAGE_CHARGE_ID = A.FLT_SEG_BAGGAGE_CHARGE_ID "
				+ " AND A.BAGGAGE_ID = C.BAGGAGE_ID AND A.PNR_SEG_ID = D.PNR_SEG_ID " + " AND A.STATUS = '"
				+ FlightBaggageStatuses.RESERVED + "' ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

				while (rs.next()) {
					FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
					baggageDTO.setAmount(rs.getBigDecimal("AMOUNT"));
					baggageDTO.setBaggageName(rs.getString("BAGGAGE_NAME"));
					baggageDTO.setBaggageId(rs.getInt("BAGGAGE_ID"));
					baggageDTO.setFlightSegmentID(rs.getInt("FLT_SEG_ID"));
					baggageDTO.setBaggageDescription(rs.getString("BAGGAGE_DESC"));

					colBaggages.add(baggageDTO);
				}
				return colBaggages;
			}
		});

		return colBaggageDtos;
	}

	public List<FlightBaggageDTO> getSelectedBaggages(String pnrSegId) throws ModuleException {
		String sql = "SELECT DISTINCT b.baggage_id,b.baggage_name,b.baggage_desc,b.status "
				+ " FROM bg_t_baggage b,bg_t_pnr_pax_seg_baggage ppsb,t_pnr_segment ps "
				+ " WHERE b.baggage_id  = ppsb.baggage_id AND ps.pnr_seg_id   = ppsb.pnr_seg_id AND ppsb.pnr_seg_id =" + pnrSegId;
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

				while (rs.next()) {
					FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
					baggageDTO.setBaggageName(rs.getString("BAGGAGE_NAME"));
					baggageDTO.setBaggageId(rs.getInt("BAGGAGE_ID"));
					baggageDTO.setBaggageDescription(rs.getString("BAGGAGE_DESC"));
					baggageDTO.setBaggageStatus(rs.getString("status"));
					colBaggages.add(baggageDTO);
				}
				return colBaggages;
			}
		});

		return colBaggageDtos;

	}

	public Map<Integer, Map<Integer, FlightBaggageDTO>> getSelectedBaggages(List<Integer> pnrSegIds) throws ModuleException {

		StringBuilder queryContent = new StringBuilder();
		Map<Integer, Map<Integer, FlightBaggageDTO>> colBaggageDtos;
		if (!pnrSegIds.isEmpty()) {
			queryContent
					.append("  SELECT PS.FLT_SEG_ID FLT_SEG_ID, CT.OND_CHARGE_TEMPLATE_ID, B.BAGGAGE_NAME, B.BAGGAGE_DESC,B.STATUS, ")
					.append("				      B.BAGGAGE_WEIGHT, BC.SEAT_FACTOR, CT.CHARGE_TEMPLATE_CODE, ")
					.append("				      BC.CABIN_CLASS_CODE, BC.AMOUNT, B.BAGGAGE_ID, PS.BAGGAGE_OND_GROUP_ID, ")
					.append("				      BC.OND_BAGGAGE_CHARGE_ID, BC.IS_DEFAULT, BC.TOTAL_WEIGHT_LIMIT ")
					.append("	FROM BG_T_OND_CHARGE_TEMPLATE CT, BG_T_OND_BAGGAGE_CHARGE BC, BG_T_BAGGAGE B, ")
					.append("                BG_T_PNR_PAX_SEG_BAGGAGE PPSB, T_PNR_SEGMENT PS ")
					.append("	WHERE  BC.OND_CHARGE_TEMPLATE_ID = CT.OND_CHARGE_TEMPLATE_ID ")
					.append("				      AND BC.BAGGAGE_ID = B.BAGGAGE_ID AND PS.PNR_SEG_ID = PPSB.PNR_SEG_ID ")
					.append("              AND PPSB.OND_BAGGAGE_CHARGE_ID = BC.OND_BAGGAGE_CHARGE_ID ")
					.append("              AND ppsb.pnr_seg_id IN ( " + BeanUtils.constructINStringForInts(pnrSegIds) + " ) ")
					.append("              AND ppsb.status = '" + FlightBaggageStatuses.RESERVED + "' ");

			JdbcTemplate template = new JdbcTemplate(getDataSource());

			colBaggageDtos = (Map<Integer, Map<Integer, FlightBaggageDTO>>) template.query(queryContent.toString(),
					new ResultSetExtractor() {
						public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
							Map<Integer, Map<Integer, FlightBaggageDTO>> colBaggages = new HashMap<Integer, Map<Integer, FlightBaggageDTO>>();
							FlightBaggageDTO baggageDTO;
							int fltSegId;

							while (resultSet.next()) {

								fltSegId = resultSet.getInt("flt_seg_id");
								if (!colBaggages.containsKey(fltSegId)) {
									colBaggages.put(fltSegId, new HashMap<Integer, FlightBaggageDTO>());
								}

								baggageDTO = new FlightBaggageDTO();

								baggageDTO.setAmount(resultSet.getBigDecimal("amount"));
								baggageDTO.setBaggageName(resultSet.getString("baggage_name"));
								baggageDTO.setBaggageWeight(resultSet.getBigDecimal("baggage_weight"));
								baggageDTO.setBaggageId(resultSet.getInt("baggage_id"));
								baggageDTO.setChargeId(resultSet.getInt("ond_baggage_charge_id"));
								baggageDTO.setOndGroupId(resultSet.getString("baggage_ond_group_id"));
								baggageDTO.setBaggageDescription(resultSet.getString("baggage_desc"));
								baggageDTO.setTemplateId(resultSet.getInt("ond_charge_template_id"));
								baggageDTO.setSeatFactor(resultSet.getInt("seat_factor"));
								baggageDTO.setDefaultBaggage(resultSet.getString("is_default"));
								baggageDTO.setTotalWeight(resultSet.getBigDecimal("total_weight_limit"));
								baggageDTO.setBaggageStatus(resultSet.getString("status"));

								colBaggages.get(fltSegId).put(baggageDTO.getBaggageId(), baggageDTO);
							}
							return colBaggages;
						}
					});
		} else {
			colBaggageDtos = new HashMap<Integer, Map<Integer, FlightBaggageDTO>>();
		}

		return colBaggageDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FlightBaggageDTO> getBaggageByTemplateID(final Integer templateID, String cabinClassCode) throws ModuleException {
		String sql = "SELECT CHG.AMOUNT,CHG.BAGGAGE_ID,BG.BAGGAGE_NAME, BG.BAGGAGE_DESC, BG.STATUS, BG.BAGGAGE_WEIGHT, "
				+ "CHG.IS_DEFAULT, CHG.OND_BAGGAGE_CHARGE_ID, CHG.SEAT_FACTOR, CHG.TOTAL_WEIGHT_LIMIT, CHG.SEAT_FACTOR_COND_APPLY_FOR "
				+ "FROM BG_T_OND_BAGGAGE_CHARGE CHG INNER JOIN "
				+ "BG_T_BAGGAGE BG ON CHG.BAGGAGE_ID = BG.BAGGAGE_ID WHERE CHG.OND_CHARGE_TEMPLATE_ID = ?";

		Object[] args = null;
		if (cabinClassCode != null && !"".equals(cabinClassCode)) {
			sql = sql + " AND (CHG.CABIN_CLASS_CODE = ? OR CHG.LOGICAL_CABIN_CLASS_CODE IN "
					+ " (SELECT LCC.LOGICAL_CABIN_CLASS_CODE FROM T_LOGICAL_CABIN_CLASS LCC WHERE LCC.CABIN_CLASS_CODE = ?)) ";
			args = new Object[] { templateID, cabinClassCode, cabinClassCode };
		} else {
			args = new Object[] { templateID };
		}

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(sql, args, new ResultSetExtractor() {
			public Object extractData(final ResultSet rs) throws SQLException, DataAccessException {
				final List<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

				while (rs.next()) {
					final FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
					baggageDTO.setBaggageId(rs.getInt("BAGGAGE_ID"));
					baggageDTO.setAmount(rs.getBigDecimal("AMOUNT"));
					baggageDTO.setBaggageName(rs.getString("BAGGAGE_NAME"));
					baggageDTO.setBaggageDescription(rs.getString("BAGGAGE_DESC"));
					baggageDTO.setBaggageWeight(rs.getBigDecimal("BAGGAGE_WEIGHT"));
					baggageDTO.setTemplateId(templateID);
					baggageDTO.setDefaultBaggage(rs.getString("IS_DEFAULT"));
					baggageDTO.setChargeId(rs.getInt("OND_BAGGAGE_CHARGE_ID"));
					baggageDTO.setSeatFactor(rs.getInt("SEAT_FACTOR"));
					baggageDTO.setBaggageStatus(rs.getString("STATUS"));
					baggageDTO.setTotalWeight(rs.getBigDecimal("TOTAL_WEIGHT_LIMIT"));
					baggageDTO.setSeatFactorConditionApplyFor(rs.getInt("SEAT_FACTOR_COND_APPLY_FOR"));

					colBaggages.add(baggageDTO);
				}
				return colBaggages;
			}
		});

		return colBaggageDtos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FlightBaggageDTO> getBaggageByTemplateIDWithTranslations(final Integer templateID, String languageCode,
			String cabinClassCode) throws ModuleException {
		String sql = "SELECT CHG.AMOUNT,CHG.BAGGAGE_ID,BG.BAGGAGE_NAME, BG.BAGGAGE_DESC, BG.STATUS, BG.BAGGAGE_WEIGHT, "
				+ "CHG.IS_DEFAULT, CHG.OND_BAGGAGE_CHARGE_ID, CHG.SEAT_FACTOR, CHG.TOTAL_WEIGHT_LIMIT, CHG.SEAT_FACTOR_COND_APPLY_FOR, "
				+ "MSG.MESSAGE_CONTENT  FROM BG_T_OND_BAGGAGE_CHARGE CHG INNER JOIN "
				+ "BG_T_BAGGAGE BG ON CHG.BAGGAGE_ID = BG.BAGGAGE_ID LEFT JOIN T_I18N_MESSAGE MSG ON "
				+ "BG.I18N_MESSAGE_KEY = MSG.I18N_MESSAGE_KEY AND MSG.MESSAGE_LOCALE= ? WHERE CHG.OND_CHARGE_TEMPLATE_ID = ? ";
		Object[] args = null;
		if (cabinClassCode != null && !"".equals(cabinClassCode)) {
			sql = sql + " AND CHG.CABIN_CLASS_CODE = ?  OR CHG.LOGICAL_CABIN_CLASS_CODE IN "
					+ " (SELECT LCC.LOGICAL_CABIN_CLASS_CODE FROM T_LOGICAL_CABIN_CLASS LCC WHERE LCC.CABIN_CLASS_CODE = ?) ";
			args = new Object[] { languageCode, templateID, cabinClassCode, cabinClassCode };
		} else {
			args = new Object[] { languageCode, templateID };
		}
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<FlightBaggageDTO> colBaggageDtos = (List<FlightBaggageDTO>) template.query(sql, args, new ResultSetExtractor() {
			public Object extractData(final ResultSet rs) throws SQLException, DataAccessException {
				final List<FlightBaggageDTO> colBaggages = new ArrayList<FlightBaggageDTO>();

				while (rs.next()) {
					final FlightBaggageDTO baggageDTO = new FlightBaggageDTO();
					baggageDTO.setBaggageId(rs.getInt("BAGGAGE_ID"));
					baggageDTO.setAmount(rs.getBigDecimal("AMOUNT"));
					baggageDTO.setBaggageName(rs.getString("BAGGAGE_NAME"));
					baggageDTO.setBaggageDescription(rs.getString("BAGGAGE_DESC"));
					baggageDTO.setBaggageWeight(rs.getBigDecimal("BAGGAGE_WEIGHT"));
					baggageDTO.setTemplateId(templateID);
					baggageDTO.setDefaultBaggage(rs.getString("IS_DEFAULT"));
					baggageDTO.setChargeId(rs.getInt("OND_BAGGAGE_CHARGE_ID"));
					baggageDTO.setSeatFactor(rs.getInt("SEAT_FACTOR"));
					baggageDTO.setBaggageStatus(rs.getString("STATUS"));
					baggageDTO.setTotalWeight(rs.getBigDecimal("TOTAL_WEIGHT_LIMIT"));
					baggageDTO.setSeatFactorConditionApplyFor(rs.getInt("SEAT_FACTOR_COND_APPLY_FOR"));
					if (null != rs.getClob("MESSAGE_CONTENT")) {
						baggageDTO.setTranslatedBaggageDescription(StringUtil.clobToString(rs.getClob("MESSAGE_CONTENT")));
					}
					colBaggages.add(baggageDTO);
				}
				return colBaggages;
			}
		});

		return colBaggageDtos;
	}

	public Map<Integer, BigDecimal> getTotalBaggageWeight(final Map<Integer, Set<Integer>> interceptingFlightSegIds) {
		Map<Integer, BigDecimal> baggageWeight;
		StringBuilder queryContent = new StringBuilder();
		List<Integer> allFlightSegIds = new ArrayList<Integer>();

		for (Integer flightSegId : interceptingFlightSegIds.keySet()) {
			allFlightSegIds.add(flightSegId);
			allFlightSegIds.addAll(interceptingFlightSegIds.get(flightSegId));
		}

		queryContent
				.append("   SELECT fs.flight_id flight_id, fs.flt_seg_id flt_seg_id, NVL ( SUM(b.baggage_weight) , 0 ) baggage_weight    ")
				.append("   FROM bg_t_pnr_pax_seg_baggage rpsb, t_pnr_segment rs,   ")
				.append("       t_flight_segment fs, bg_t_baggage b     ")
				.append("   WHERE rs.pnr_seg_id = rpsb.pnr_seg_id (+) AND rpsb.baggage_id = b.baggage_id (+)    ")
				.append("       AND rs.flt_seg_id   = fs.flt_seg_id (+) AND rpsb.status = '" + FlightBaggageStatuses.RESERVED
						+ "' ")
				.append("       AND rs.flt_seg_id   IN  ( ").append(BeanUtils.constructINString(allFlightSegIds)).append(" ) ")
				.append(" AND rs.sub_status is null AND rs.status<>'CNX'")// Drop exchange and CNX
																			// segments
				.append("   GROUP BY fs.flight_id, fs.flt_seg_id    ");

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		baggageWeight = (Map<Integer, BigDecimal>) template.query(queryContent.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				Map<Integer, BigDecimal> temp = new HashMap<Integer, BigDecimal>();
				int fltSeg;
				while (resultSet.next()) {
					// temp.put(resultSet.getInt("flight_id"), resultSet.getBigDecimal("baggage_weight"));
					fltSeg = resultSet.getInt("flt_seg_id");
					if (!interceptingFlightSegIds.containsKey(fltSeg)) {
						for (int flightSegId : interceptingFlightSegIds.keySet()) {
							if (interceptingFlightSegIds.get(flightSegId).contains(fltSeg)) {
								fltSeg = flightSegId;
							}
						}
					}

					if (!temp.containsKey(fltSeg)) {
						temp.put(fltSeg, BigDecimal.ZERO);
					}
					temp.put(fltSeg, temp.get(fltSeg).add(resultSet.getBigDecimal("baggage_weight")));

				}

				return temp;
			}
		});

		for (int flightSegId : interceptingFlightSegIds.keySet()) {
			if (!baggageWeight.containsKey(flightSegId)) {
				baggageWeight.put(flightSegId, BigDecimal.ZERO);
			}
		}

		return baggageWeight;
	}

	public Integer getBaggageTemplateId(CommonsConstants.BaggageCriterion criterion, ONDBaggageDTO baggageDTO)
			throws ModuleException {

		Integer templateId = null;
		StringBuilder criteriaSubQuery = new StringBuilder();
		StringBuilder mainQueryContent = new StringBuilder();
		Iterator<String> iterator;

		// exact match gets 1st priority
		switch (criterion) {
		case FLIGHT_NUMBER:
			if (baggageDTO.getFlightNumbers() != null && !baggageDTO.getFlightNumbers().isEmpty()) {
				iterator = baggageDTO.getFlightNumbers().iterator();

				criteriaSubQuery.append(" SELECT ond_charge_template_id , 1 priority FROM ( ");
				while (iterator.hasNext()) {
					criteriaSubQuery.append(" SELECT ond_charge_template_id ")
							.append("   FROM bg_t_ond_template_flight WHERE flight_number = '" + iterator.next() + "' ");
					if (iterator.hasNext()) {
						criteriaSubQuery.append(" INTERSECT ");
					}
				}
				criteriaSubQuery.append(" ) UNION ");
			}
			criteriaSubQuery.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, 2 priority ")
					.append("         FROM bg_t_ond_template_flight WHERE flight_number = '"
							+ CommonsConstants.ALL_DESIGNATOR_STRING + "' )  ");

			break;
		case AGENT:
			if (baggageDTO.getAgent() != null) {
				criteriaSubQuery.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, 1 priority  ")
						.append("         FROM bg_t_ond_template_agent WHERE agent_code = '" + baggageDTO.getAgent() + "' ) ")
						.append("   UNION   ");
			}
			criteriaSubQuery.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, 2 priority  ")
					.append("         FROM bg_t_ond_template_agent WHERE agent_code = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' )  ");
			break;
		case BOOKING_CLASS:
			if (baggageDTO.getBookingClasses() != null && !baggageDTO.getBookingClasses().isEmpty()) {
				iterator = baggageDTO.getBookingClasses().iterator();
				criteriaSubQuery.append(" SELECT ond_charge_template_id , 1 priority FROM ( ");
				while (iterator.hasNext()) {
					criteriaSubQuery.append(" SELECT ond_charge_template_id ")
							.append("   FROM bg_t_ond_template_bc WHERE booking_code = '" + iterator.next() + "' ");
					if (iterator.hasNext()) {
						criteriaSubQuery.append(" INTERSECT ");
					}
				}
				criteriaSubQuery.append(" ) UNION ");
			}
			criteriaSubQuery.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, 2 priority  ")
					.append("         FROM bg_t_ond_template_bc WHERE booking_code = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' )  ");
			break;
		case OND:
			// TODO - throw exception : should not call for OND
			break;
		}

		mainQueryContent.append("   SELECT * FROM ( ").append("       SELECT ct.ond_charge_template_id ond_charge_template_id ")
				.append("       FROM bg_t_ond_charge_template ct,   ").append("           ( ").append(criteriaSubQuery.toString())
				.append(" ) bc ").append("       WHERE ct.ond_charge_template_id = bc.ond_charge_template_id     ")
				.append("           AND ct.status = 'ACT' AND TRUNC( ? ) BETWEEN ct.from_date AND ct.end_date ")
				.append("       ORDER BY bc.priority ) ").append("   WHERE ROWNUM < 2 ");

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		templateId = (Integer) template.query(mainQueryContent.toString(), new Object[] { baggageDTO.getFromDate() },
				new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						Integer templateId = null;
						if (resultSet.next()) {
							templateId = resultSet.getInt("ond_charge_template_id");
						}
						return templateId;
					}
				});

		return templateId;
	}

	@Override
	public SortedMap<Integer, Set<Integer>> getBaggageTemplateIds(CommonsConstants.BaggageCriterion criterion,
			ONDBaggageDTO baggageDTO) throws ModuleException {

		SortedMap<Integer, Set<Integer>> baggageTemplateIds;
		StringBuilder criteriaSubQuery = new StringBuilder();
		StringBuilder mainQueryContent = new StringBuilder();
		Iterator<String> iterator;
		List<Object> args = new ArrayList<Object>();

		// exact match gets 1st priority
		switch (criterion) {
		case FLIGHT_NUMBER:
			if (baggageDTO.getFlightNumbers() != null && !baggageDTO.getFlightNumbers().isEmpty()) {
				iterator = baggageDTO.getFlightNumbers().iterator();

				criteriaSubQuery.append(" SELECT ond_charge_template_id , 1 priority FROM ( ");
				while (iterator.hasNext()) {
					criteriaSubQuery.append(" SELECT ond_charge_template_id ")
							.append("   FROM bg_t_ond_template_flight WHERE flight_number = '" + iterator.next() + "' ");
					if (iterator.hasNext()) {
						criteriaSubQuery.append(" INTERSECT ");
					}
				}
				criteriaSubQuery.append(" ) UNION ");
			}
			criteriaSubQuery
					.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.ALL_CRITERION_PRIORITY + " priority ")
					.append("         FROM bg_t_ond_template_flight WHERE flight_number = '"
							+ CommonsConstants.ALL_DESIGNATOR_STRING + "' ) UNION ");

			criteriaSubQuery
					.append(" ( SELECT bt.ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.EMPTY_CRITERION_PRIORITY + " priority ")
					.append("   FROM bg_t_ond_charge_template bt, bg_t_ond_template_flight bf ")
					.append("   WHERE bt.ond_charge_template_id = bf.ond_charge_template_id (+) AND bt.status = 'ACT' ")
					.append("       AND TRUNC( ? ) BETWEEN bt.from_date AND bt.end_date ")
					.append("   GROUP BY bt.ond_charge_template_id ")
					.append("   HAVING COUNT( bf.ond_template_flight_id ) = 0 ) ");

			args.add(baggageDTO.getFromDate());

			break;
		case AGENT:
			if (baggageDTO.getAgent() != null) {
				criteriaSubQuery.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, 1 priority  ")
						.append("         FROM bg_t_ond_template_agent WHERE agent_code = '" + baggageDTO.getAgent() + "' ) ")
						.append("   UNION   ");
			}
			criteriaSubQuery
					.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.ALL_CRITERION_PRIORITY + " priority  ")
					.append("         FROM bg_t_ond_template_agent WHERE agent_code = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' ) UNION ");

			criteriaSubQuery
					.append(" ( SELECT bt.ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.EMPTY_CRITERION_PRIORITY + " priority ")
					.append("   FROM bg_t_ond_charge_template bt, bg_t_ond_template_agent ba ")
					.append("   WHERE bt.ond_charge_template_id = ba.ond_charge_template_id (+) AND bt.status = 'ACT' ")
					.append("       AND TRUNC( ? ) BETWEEN bt.from_date AND bt.end_date ")
					.append("   GROUP BY bt.ond_charge_template_id ")
					.append("   HAVING COUNT( ba.ond_template_agent_id ) = 0 ) ");

			args.add(baggageDTO.getFromDate());

			break;
		case BOOKING_CLASS:
			if (baggageDTO.getBookingClasses() != null && !baggageDTO.getBookingClasses().isEmpty()) {
				iterator = baggageDTO.getBookingClasses().iterator();
				criteriaSubQuery.append(" SELECT ond_charge_template_id , 1 priority FROM ( ");
				while (iterator.hasNext()) {
					criteriaSubQuery.append(" SELECT ond_charge_template_id ")
							.append("   FROM bg_t_ond_template_bc WHERE booking_code = '" + iterator.next() + "' ");
					if (iterator.hasNext()) {
						criteriaSubQuery.append(" INTERSECT ");
					}
				}
				criteriaSubQuery.append(" ) UNION ");
			}
			criteriaSubQuery
					.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.ALL_CRITERION_PRIORITY + " priority  ")
					.append("         FROM bg_t_ond_template_bc WHERE booking_code = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' ) UNION ");

			criteriaSubQuery
					.append(" ( SELECT bt.ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.EMPTY_CRITERION_PRIORITY + " priority ")
					.append("   FROM bg_t_ond_charge_template bt, bg_t_ond_template_bc bc ")
					.append("   WHERE bt.ond_charge_template_id = bc.ond_charge_template_id (+) AND bt.status = 'ACT' ")
					.append("       AND TRUNC( ? ) BETWEEN bt.from_date AND bt.end_date ")
					.append("   GROUP BY bt.ond_charge_template_id ").append("   HAVING COUNT( bc.ond_template_bc_id ) = 0 ) ");

			args.add(baggageDTO.getFromDate());

			break;
		case OND:

			List<String> matchingOptions = AirinventoryUtils.getOndCodeOptions(baggageDTO.getOndCode());

			for (int a = 0; a < matchingOptions.size(); a++) {
				criteriaSubQuery.append(" ( SELECT bo.ond_charge_template_id ond_charge_template_id, " + (a + 1) + " priority ")
						.append("   FROM bg_t_ond_bagg_charge_template bo ")
						.append("   WHERE bo.ond_baggage_ond_code = '" + matchingOptions.get(a) + "' ) UNION ");

			}
			criteriaSubQuery
					.append(" ( SELECT bo.ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.ALL_CRITERION_PRIORITY + " priority ")
					.append("   FROM bg_t_ond_bagg_charge_template bo ")
					.append("   WHERE bo.ond_baggage_ond_code = '" + AirinventoryUtils.getOndAllOption() + "' ) UNION ");

			criteriaSubQuery
					.append(" ( SELECT bt.ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.EMPTY_CRITERION_PRIORITY + " priority ")
					.append("   FROM bg_t_ond_charge_template bt, bg_t_ond_bagg_charge_template bo ")
					.append("   WHERE bt.ond_charge_template_id = bo.ond_charge_template_id (+) AND bt.status = 'ACT' ")
					.append("       AND TRUNC( ? ) BETWEEN bt.from_date AND bt.end_date ")
					.append("   GROUP BY bt.ond_charge_template_id ")
					.append("   HAVING COUNT( bo.ond_bagg_charge_template_id ) = 0 ) ");

			args.add(baggageDTO.getFromDate());

			break;
		case SALES_CHANNEL:
			if (!(baggageDTO.getSalesChannel().equals("0"))) {
				criteriaSubQuery.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, 1 priority  ")
						.append("         FROM bg_t_ond_template_channel WHERE sales_channel = '" + baggageDTO.getSalesChannel()
								+ "' ) ")
						.append("   UNION   ");
			}
			criteriaSubQuery
					.append("   ( SELECT DISTINCT ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.ALL_CRITERION_PRIORITY + " priority  ")
					.append("         FROM bg_t_ond_template_channel WHERE sales_channel = '"
							+ CommonsConstants.ALL_DESIGNATOR_STRING + "' ) UNION ");

			criteriaSubQuery
					.append(" ( SELECT bt.ond_charge_template_id ond_charge_template_id, "
							+ BaggageSelector.EMPTY_CRITERION_PRIORITY + " priority ")
					.append("   FROM bg_t_ond_charge_template bt, bg_t_ond_template_channel ba ")
					.append("   WHERE bt.ond_charge_template_id = ba.ond_charge_template_id (+) AND bt.status = 'ACT' ")
					.append("       AND TRUNC( ? ) BETWEEN bt.from_date AND bt.end_date ")
					.append("   GROUP BY bt.ond_charge_template_id ")
					.append("   HAVING COUNT( ba.ond_charge_template_id ) = 0 ) ");

			args.add(baggageDTO.getFromDate());

			break;
		}

		mainQueryContent.append("   SELECT ct.ond_charge_template_id ond_charge_template_id , bc.priority priority ")
				.append("   FROM bg_t_ond_charge_template ct, bg_t_ond_baggage_charge b,    ").append("       ( ")
				.append(criteriaSubQuery.toString()).append(" ) bc ")
				.append("   WHERE ct.ond_charge_template_id = bc.ond_charge_template_id     ")
				.append("       AND ct.ond_charge_template_id = b.ond_charge_template_id    ")
				.append("       AND ( b.cabin_class_code = ? OR b.logical_cabin_class_code = ? )    ")
				.append("       AND ct.status = 'ACT' AND TRUNC( ? ) BETWEEN ct.from_date AND ct.end_date   ")
				.append("   ORDER BY bc.priority ");

		args.add(baggageDTO.getClassOfService());
		args.add(baggageDTO.getClassOfService());
		args.add(baggageDTO.getFromDate());

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		baggageTemplateIds = (SortedMap<Integer, Set<Integer>>) template.query(mainQueryContent.toString(), args.toArray(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						SortedMap<Integer, Set<Integer>> templates = new TreeMap<Integer, java.util.Set<Integer>>();
						Integer priority;

						while (resultSet.next()) {
							priority = resultSet.getInt("priority");
							if (!templates.containsKey(priority)) {
								templates.put(priority, new HashSet<Integer>());
							}
							templates.get(priority).add(resultSet.getInt("ond_charge_template_id"));
						}

						return templates;
					}
				});

		return baggageTemplateIds;

	}
}