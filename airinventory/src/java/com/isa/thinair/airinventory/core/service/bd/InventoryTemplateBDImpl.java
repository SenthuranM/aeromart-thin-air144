package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.InventoryTemplateBD;

/**
 * 
 * @author priyantha
 */
@Remote
public interface InventoryTemplateBDImpl extends InventoryTemplateBD {

}
