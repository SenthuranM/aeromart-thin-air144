package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;

/**
 * @author MN
 */
@Local
public interface FlightInventoryResBDLocalImpl extends FlightInventoryResBD {

}
