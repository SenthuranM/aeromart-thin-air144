/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airinventory.api.model.RollForwardBatchREQ;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * RollForwardBatchREQDAO is business delegate interface for the Rollforward service apis
 * 
 * @author Raghuraman Kumar
 * 
 */

public interface RollForwardBatchREQDAO {

	public List<RollForwardBatchREQ> getRollForwardBatchREQs() throws ModuleException;

	public RollForwardBatchREQ getRollForwardBatchREQ(int id) throws ModuleException;

	public Integer saveRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException;

	public void updateRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException;

	public void removeRollForwardBatchREQ(int batchId) throws ModuleException;

	public void removeRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException;

	public void updateRollForwardBatchREQStatus(Integer batchId, String status);

}
