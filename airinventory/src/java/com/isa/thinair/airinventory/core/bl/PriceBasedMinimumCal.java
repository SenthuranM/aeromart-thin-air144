package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.core.bl.IFareCalculator.FareCalType;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class PriceBasedMinimumCal {

	private Map<FARE_ORDER, GroupedOndFlightsFareCalculator> minGoffCalMap = new HashMap<>();
	private Map<FARE_ORDER, IFareCalculator> minFareCalMap = new HashMap<>();
	private Map<FARE_ORDER, Double> minFareMap = new HashMap<>();
	private GroupedOndFlightsFareCalculator minGoffCal = null;

	enum FARE_ORDER {
		HRT_FARE(1), RETURN_FARE(2), CON_FARE(3), CON_SEG_FARE(4), SEG_FARE(5), NONE(6);

		int priority;

		FARE_ORDER(int priority) {
			this.priority = priority;
		}

		public int getPriority() {
			return this.priority;
		}

	};

	public PriceBasedMinimumCal() {
	}

	public void considerCal(IFareCalculator fareCal, GroupedOndFlightsFareCalculator goffCal) {
		double calMinFare = fareCal.getTotalMinimumFare();
		double minimumFare = getMinimumFare(fareCal);
		if (AirInventoryDataExtractUtil.isFirstFareLess(calMinFare, minimumFare)) {
			recordMinimum(goffCal, fareCal);
		}
	}

	private double getMinimumFare(IFareCalculator fareCal) {
		FARE_ORDER fareOrder = getFareOrder(fareCal);
		if (minFareMap.containsKey(fareOrder)) {
			return minFareMap.get(fareOrder);
		}
		return -1;
	}

	private void recordMinimum(GroupedOndFlightsFareCalculator goffCal, IFareCalculator fareCal) {
		FARE_ORDER fareOrder = getFareOrder(fareCal);
		minGoffCalMap.put(fareOrder, goffCal);
		minFareCalMap.put(fareOrder, fareCal);
		minFareMap.put(fareOrder, fareCal.getTotalMinimumFare());
	}

	private FARE_ORDER getFareOrder(IFareCalculator fareCal) {
		if (AppSysParamsUtil.isFareBiasingEnabled()) {
			if (fareCal.dependantFareTypes().contains(FareCalType.RT)) {
				return FARE_ORDER.RETURN_FARE;
			} else if (fareCal.dependantFareTypes().contains(FareCalType.HRT)) {
				return FARE_ORDER.HRT_FARE;
			} else if (fareCal.dependantFareTypes().contains(FareCalType.CON) && fareCal.dependantFareTypes().size() == 1) {
				return FARE_ORDER.CON_FARE;
			} else if (fareCal.dependantFareTypes().contains(FareCalType.CON)
					&& fareCal.dependantFareTypes().contains(FareCalType.SEG)) {
				return FARE_ORDER.CON_SEG_FARE;
			} else if (fareCal.dependantFareTypes().contains(FareCalType.SEG)) {
				return FARE_ORDER.SEG_FARE;
			}
		}
		return FARE_ORDER.NONE;
	}

	public boolean hasMinimimFare() {
		return (minFareMap.size() > 0);
	}

	public GroupedOndFlightsFareCalculator getMinimumGoffCal() {
		calculateMinimum();
		return minGoffCal;
	}

	public void calculateMinimum() {
		if (hasMinimimFare() && minGoffCal == null) {
			List<FARE_ORDER> list = new ArrayList<>(minFareCalMap.keySet());
			Collections.sort(list, new Comparator<FARE_ORDER>() {
				@Override
				public int compare(FARE_ORDER o1, FARE_ORDER o2) {
					return o1.getPriority() - o2.getPriority();
				}
			});
			FARE_ORDER selectedFare = list.get(0);
			minGoffCal = minGoffCalMap.get(selectedFare);
			minFareCalMap.get(selectedFare).setFareAndLogiCC(minGoffCal.getAvailableIBOBFlightSegments(), true);
		}

	}
}
