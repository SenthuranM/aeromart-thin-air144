/*
 * Created on 18-Aug-2005
 *
 */
package com.isa.thinair.airinventory.core.config;

import java.util.Collection;
import java.util.Properties;

import javax.sql.DataSource;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Thejaka
 * @isa.module.config-bean
 */
public class AirInventoryConfig extends DefaultModuleConfig {

	/** Reference to the application's datasource */
	private DataSource dataSource;

	/** SQL Queries */
	private Properties queries;

	/** Configurable application contants */
	private Properties constants;

	/** Attributes related to automatic buckets closure based on segment buckets' closure */
	/** Whether or not to apply closure logic for connection buckets */
	private boolean applyConnectionBucketsClosure;
	/** Whether or not to apply closure logic for return buckets */
	private boolean applyReturnBucketsClosure;

	/** Whether or not to acquire table locks before updating the inventory */
	private boolean applyLockingForSeatBlock;
	private boolean applyLockingForBlockSeatRel;
	private boolean applyLockingForSeatRelease;
	private boolean applyLockingForOhdConfirm;

	/** max wait duration (in seconds) before giving up lock aquisition */
	private int maxWaitForLockDuration;

	/** Keep track of Block Seats in a separate temp table | true or false **/
	private String trackBlockSeats;

	/** Controls whether or not to apply segment fares for interlined bookings */
	private boolean disableSegFaresForInterlining;

	/** Whether or not to perform availability publishing related functionalities */
	private boolean publishAvailability;

	/** Defines the fare count to be given out for Dynamic Best offers detailed search */
	private int bestOffersDisplayFareCount;

	/** Defines the search duration in months for which Best offers are to be given out */
	private int bestOffersSearchDurationInMonths;

	/** Defines the duration in mins for which Best offers are to be cached */
	private int bestOffersCachingTimeInMins;

	/** Defines the list of fare types handle in RM */
	private Collection<String> rmHandleFareTypes;

	private boolean closeAllLowerRankedStandardBCsWhenClosingHigherBC;

	private Collection<String> goshowBCListForAgentBookingsWithinCutoffTime;

	private Integer goshowBCAllocation;
	
	private boolean useNearestSearchDateForApplyCookieCharges;

	public DataSource getDataSource() {
		return dataSource;
	}

	public Properties getQueries() {
		return queries;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setQueries(Properties queries) {
		this.queries = queries;
	}

	public String getQuery(String queryId, String[] args) {
		StringBuffer queryBuff = new StringBuffer(queries.getProperty(queryId));
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				String placeHolder = "{" + i + "}";
				int startIndex = queryBuff.toString().indexOf(placeHolder);
				if (startIndex != -1) {
					queryBuff.delete(startIndex, startIndex + placeHolder.length());
					queryBuff.insert(startIndex, args[i]);
				}
			}
		}
		return queryBuff.toString();
	}

	public String getConstant(String key) {
		return constants.getProperty(key);
	}

	public void setConstants(Properties constants) {
		this.constants = constants;
	}

	/**
	 * @return Returns the applyConnectionBucketsClosure.
	 */
	public boolean isApplyConnectionBucketsClosure() {
		return applyConnectionBucketsClosure;
	}

	/**
	 * @param applyConnectionBucketsClosure
	 *            The applyConnectionBucketsClosure to set.
	 */
	public void setApplyConnectionBucketsClosure(boolean applyConnectionBucketsClosure) {
		this.applyConnectionBucketsClosure = applyConnectionBucketsClosure;
	}

	/**
	 * @return Returns the applyReturnBucketsClosure.
	 */
	public boolean isApplyReturnBucketsClosure() {
		return applyReturnBucketsClosure;
	}

	/**
	 * @param applyReturnBucketsClosure
	 *            The applyReturnBucketsClosure to set.
	 */
	public void setApplyReturnBucketsClosure(boolean applyReturnBucketsClosure) {
		this.applyReturnBucketsClosure = applyReturnBucketsClosure;
	}

	/**
	 * @return Returns the applyLockingForBlockSeatRel.
	 */
	public boolean isApplyLockingForBlockSeatRel() {
		return applyLockingForBlockSeatRel;
	}

	/**
	 * @param applyLockingForBlockSeatRel
	 *            The applyLockingForBlockSeatRel to set.
	 */
	public void setApplyLockingForBlockSeatRel(boolean applyLockingForBlockSeatRel) {
		this.applyLockingForBlockSeatRel = applyLockingForBlockSeatRel;
	}

	/**
	 * @return Returns the applyLockingForOhdConfirm.
	 */
	public boolean isApplyLockingForOhdConfirm() {
		return applyLockingForOhdConfirm;
	}

	/**
	 * @param applyLockingForOhdConfirm
	 *            The applyLockingForOhdConfirm to set.
	 */
	public void setApplyLockingForOhdConfirm(boolean applyLockingForOhdConfirm) {
		this.applyLockingForOhdConfirm = applyLockingForOhdConfirm;
	}

	/**
	 * @return Returns the applyLockingForSeatBlock.
	 */
	public boolean isApplyLockingForSeatBlock() {
		return applyLockingForSeatBlock;
	}

	/**
	 * @param applyLockingForSeatBlock
	 *            The applyLockingForSeatBlock to set.
	 */
	public void setApplyLockingForSeatBlock(boolean applyLockingForSeatBlock) {
		this.applyLockingForSeatBlock = applyLockingForSeatBlock;
	}

	/**
	 * @return Returns the applyLockingForSeatRelease.
	 */
	public boolean isApplyLockingForSeatRelease() {
		return applyLockingForSeatRelease;
	}

	/**
	 * @param applyLockingForSeatRelease
	 *            The applyLockingForSeatRelease to set.
	 */
	public void setApplyLockingForSeatRelease(boolean applyLockingForSeatRelease) {
		this.applyLockingForSeatRelease = applyLockingForSeatRelease;
	}

	/**
	 * @return Returns the maxWaitForLockDuration.
	 */
	public int getMaxWaitForLockDuration() {
		return maxWaitForLockDuration;
	}

	/**
	 * @param maxWaitForLockDuration
	 *            The maxWaitForLockDuration to set.
	 */
	public void setMaxWaitForLockDuration(int maxWaitForLockDuration) {
		this.maxWaitForLockDuration = maxWaitForLockDuration;
	}

	/**
	 * @return the trackBlockSeats
	 */
	public String getTrackBlockSeats() {
		return trackBlockSeats;
	}

	/**
	 * @param trackBlockSeats
	 *            the trackBlockSeats to set
	 */
	public void setTrackBlockSeats(String trackBlockSeats) {
		this.trackBlockSeats = trackBlockSeats;
	}

	public boolean isDisableSegFaresForInterlining() {
		return disableSegFaresForInterlining;
	}

	public void setDisableSegFaresForInterlining(boolean disableSegFaresForInterlining) {
		this.disableSegFaresForInterlining = disableSegFaresForInterlining;
	}

	/**
	 * @return Returns the publishAvailability.
	 */
	public boolean isPublishAvailability() {
		return publishAvailability;
	}

	/**
	 * @param publishAvailability
	 *            The publishAvailability to set.
	 */
	public void setPublishAvailability(boolean publishAvailability) {
		this.publishAvailability = publishAvailability;
	}

	/**
	 * @return the bestOffersDisplayFareCount
	 */
	public int getBestOffersDisplayFareCount() {
		return this.bestOffersDisplayFareCount;
	}

	/**
	 * @param bestOffersDisplayFareCount
	 *            The bestOffersDisplayFareCount to set.
	 */
	public void setBestOffersDisplayFareCount(int bestOffersDisplayFareCount) {
		this.bestOffersDisplayFareCount = bestOffersDisplayFareCount;
	}

	/**
	 * @return the bestOffersSearchDurationInMonths
	 */
	public int getBestOffersSearchDurationInMonths() {
		return this.bestOffersSearchDurationInMonths;
	}

	/**
	 * @param bestOffersSearchDurationInMonths
	 *            The bestOffersSearchDurationInMonths to set.
	 */
	public void setBestOffersSearchDurationInMonths(int bestOffersSearchDurationInMonths) {
		this.bestOffersSearchDurationInMonths = bestOffersSearchDurationInMonths;
	}

	/**
	 * @return the bestOffersCachingTimeInMins
	 */
	public int getBestOffersCachingTimeInMins() {
		return this.bestOffersCachingTimeInMins;
	}

	/**
	 * @param bestOffersCachingTimeInMins
	 *            The bestOffersCachingTimeInMins to set.
	 */
	public void setBestOffersCachingTimeInMins(int bestOffersCachingTimeInMins) {
		this.bestOffersCachingTimeInMins = bestOffersCachingTimeInMins;
	}

	/** Search fare based on the minimum base fare */
	public static final int ABSOLUTE_MIN_BASE_FARE = 1;

	/** Search fares ordering in base fare, giving precedence to through fares when searching for connection */
	public static final int ABSOLUTE_MIN_BASE_FARE_WITH_CNX_PRECEDENCE = 2;

	/**
	 * @return rmHandleFareTypes
	 */
	public Collection<String> getRmHandleFareTypes() {
		return rmHandleFareTypes;
	}

	/**
	 * @param rmHandleFareTypes
	 *            the list of fare types to set
	 */
	public void setRmHandleFareTypes(Collection<String> rmHandleFareTypes) {
		this.rmHandleFareTypes = rmHandleFareTypes;
	}

	public boolean isCloseAllLowerRankedStandardBCsWhenClosingHigherBC() {
		return closeAllLowerRankedStandardBCsWhenClosingHigherBC;
	}

	public void setCloseAllLowerRankedStandardBCsWhenClosingHigherBC(boolean closeAllLowerRankedStandardBCsWhenClosingHigherBC) {
		this.closeAllLowerRankedStandardBCsWhenClosingHigherBC = closeAllLowerRankedStandardBCsWhenClosingHigherBC;
	}

	/**
	 * @return the goshowBCAllocation
	 */
	public Integer getGoshowBCAllocation() {
		return goshowBCAllocation;
	}

	/**
	 * @param goshowBCAllocation
	 *            the goshowBCAllocation to set
	 */
	public void setGoshowBCAllocation(Integer goshowBCAllocation) {
		this.goshowBCAllocation = goshowBCAllocation;
	}

	/**
	 * @return the goshowBCListForAgentBookingsWithinCutoffTime
	 */
	public Collection<String> getGoshowBCListForAgentBookingsWithinCutoffTime() {
		return goshowBCListForAgentBookingsWithinCutoffTime;
	}

	/**
	 * @param goshowBCListForAgentBookingsWithinCutoffTime
	 *            the goshowBCListForAgentBookingsWithinCutoffTime to set
	 */
	public void setGoshowBCListForAgentBookingsWithinCutoffTime(Collection<String> goshowBCListForAgentBookingsWithinCutoffTime) {
		this.goshowBCListForAgentBookingsWithinCutoffTime = goshowBCListForAgentBookingsWithinCutoffTime;
	}

	public boolean isUseNearestSearchDateForApplyCookieCharges() {
		return useNearestSearchDateForApplyCookieCharges;
	}

	public void setUseNearestSearchDateForApplyCookieCharges(boolean useNearestSearchDateForApplyCookieCharges) {
		this.useNearestSearchDateForApplyCookieCharges = useNearestSearchDateForApplyCookieCharges;
	}

}
