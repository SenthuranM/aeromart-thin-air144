/**
 * 
 */
package com.isa.thinair.airinventory.core.bl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightSeatJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapJDBCDAO;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airpricing.api.model.SeatCharge.SeatChargeStatus;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author indika
 * 
 */
public class SeatMapBL implements Serializable {

	private static final long serialVersionUID = -2819397581365245200L;

	private static FlightSeatBL _flightSeatBL = null;

	private final Log log = LogFactory.getLog(getClass());

	public ServiceResponce updateTemplateFromFlight(int flitghtSegID, Collection<SeatCharge> colSeatCharges, String modelNo)
			throws ModuleException {
		DefaultServiceResponse res = new DefaultServiceResponse(false);

		Integer templateId = getSeatMapJDBCDAO().getTempleateId(flitghtSegID);
		ChargeTemplate chargeTemplate = getChargeBD().getChargeTemplate(templateId.intValue());

		// Assumption AMS_CHARGE_ID would be same for the both source and dest collections

		Map<Integer, SeatCharge> mapNewSeatCharge = buildMapChargeIDSeatCharge(colSeatCharges);

		Set<SeatCharge> colOldSeatCharges = chargeTemplate.getSeatCharges();
		for (SeatCharge seatCharge : colOldSeatCharges) {
			SeatCharge newSeatCharge = null;
			newSeatCharge = mapNewSeatCharge.get(seatCharge.getChargeId());

			if (newSeatCharge != null) {
				// Assigne new values
				seatCharge.setChargeAmount(newSeatCharge.getChargeAmount());
				seatCharge.setStatus(newSeatCharge.getStatus());
			}
		}

		getChargeBD().saveTemplate(chargeTemplate, flitghtSegID, modelNo);

		res.setSuccess(true);
		return res;
	}

	private Map<Integer, SeatCharge> buildMapChargeIDSeatCharge(Collection<SeatCharge> seatCharges) {
		Map<Integer, SeatCharge> map = new HashMap<Integer, SeatCharge>();
		for (SeatCharge seatCharge : seatCharges) {
			map.put(seatCharge.getChargeId(), seatCharge);
		}
		return map;
	}

	public ServiceResponce copyTemplateAsNew(int oldTemplateId, Collection<SeatCharge> seatCharges, String newTemplateCode,
			int flightSegId, UserPrincipal userPrincipal) throws ModuleException {
		// Copy and Save new template
		ChargeTemplate chargeTemplate = getChargeBD().copyChargeTemplate(oldTemplateId, newTemplateCode, seatCharges);

		// Assign new charges to flight segment
		Collection<Integer> colSegIds = new ArrayList<Integer>();
		colSegIds.add(flightSegId);
		return assignFlightSeatCharges(colSegIds, chargeTemplate.getTemplateId(), userPrincipal, null, null);
	}

	/**
	 * 
	 * @param sourceFlightId
	 * @param userPrincipal
	 *            TODO
	 * @param modelNo
	 * 
	 * @param templateCode
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce assignFlightSeatChargesRollFoward(int sourceFlightId, Collection<Integer> destFlightIds,
			UserPrincipal userPrincipal, String modelNo) throws ModuleException {
		// 1.Get srctemplateID <segcode,FlightSeatDTO>
		// 2.Get srcMap <segcode,tempIds >
		// 3.Get destMap <segCode, Collection <flightSegIds> >
		// 4.Assign templates

		ArrayList arrNotAssigned = new ArrayList();
		ArrayList arrAssigned = new ArrayList();
		ArrayList arrNewlyAssigned = new ArrayList();
		DefaultServiceResponse res = new DefaultServiceResponse(false);
		Map<String, Collection> mapAuditTemplateIDcolSegIds = new HashMap();
		Map<String, Collection> mapAuditTemplateIDcolSegIdsFailed = new HashMap();
		Map<String, Collection> mapAuditTemplateIDcolSegIdsNewlyAssigned = new HashMap();

		Collection<FlightSeatsDTO> colGetTemplateIds = getSeatMapJDBCDAO().getTempleateIds(sourceFlightId);
		Map<String, FlightSeatsDTO> mapSegCodeFlightSeatDTO = buildMapWithSegCode(colGetTemplateIds);

		// 2.
		// FIXED Can avoid this step as above is enough

		// 3.
		Collection<FlightSeatsDTO> colFlightSegIds = getFlightBD().getFlightSegIdsforFlight(destFlightIds);
		Map mapSegCodeColSegIds = buildMapWithSegCodeColSegIds(colFlightSegIds);

		Set<SeatCharge> seatCharges = getSourceFlightSeatCharges(colGetTemplateIds.iterator().next().getFlightSegmentID());

		// 4.
		for (String segCode : mapSegCodeFlightSeatDTO.keySet()) {
			FlightSeatsDTO flightSeatsDTO = mapSegCodeFlightSeatDTO.get(segCode);
			ServiceResponce sr = assignFlightSeatCharges((Collection) mapSegCodeColSegIds.get(segCode),
					flightSeatsDTO.getTemplateId(), userPrincipal, modelNo, seatCharges);
			arrAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED));
			arrNotAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED));
			arrNewlyAssigned.addAll((Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED));
			if (arrAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIds.put(Integer.toString(flightSeatsDTO.getTemplateId()),
						(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED));
			}
			if (arrNotAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(flightSeatsDTO.getTemplateId()),
						(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED));
			}
			if (arrNewlyAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(flightSeatsDTO.getTemplateId()),
						(Collection) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED));
			}
		}
		// Audit record
		Map<String, Map> map = new HashMap();
		map.put(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED, mapAuditTemplateIDcolSegIds);
		map.put(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED, mapAuditTemplateIDcolSegIdsFailed);
		map.put(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED, mapAuditTemplateIDcolSegIdsNewlyAssigned);
		AuditAirinventory.doAuditSeatMapRollFowardSeatCharges(map, userPrincipal);

		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	public ServiceResponce assignFlightSeatCharge(Collection<Integer> flightSegIds, Map<Integer, String> newSeatCharges)
			throws ModuleException {

		DefaultServiceResponse res = new DefaultServiceResponse(false);
		Collection<Integer> seatIds = new ArrayList<Integer>();

		for (Entry<Integer, String> entry : newSeatCharges.entrySet()) {
			seatIds.add(entry.getKey());
		}
		Collection<FlightSeat> filghtSeatList = new ArrayList<FlightSeat>();
		Collection<FlightSeat> exFlightSeats = getSeatMapDAO().getFilghtSeatData(flightSegIds, seatIds);
		for (FlightSeat exFlightSeat : exFlightSeats) {
			String[] newSeatData = newSeatCharges.get(exFlightSeat.getSeatId()).split("_");

			exFlightSeat.setAmount(new BigDecimal(newSeatData[0].trim()));
			
			if (exFlightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)
					|| exFlightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED)
					|| exFlightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.OHD)) {
				// Skip reserved, blocked or onhold
			} else {
				if (("ACT").equals(newSeatData[1].trim())) {
					exFlightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
				} else if (("INA").equals(newSeatData[1].trim())) {
					exFlightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
				}
			}
			filghtSeatList.add(exFlightSeat);
		}
		getSeatMapDAO().saveSeatFares(filghtSeatList);
		res.setSuccess(true);
		return res;
	}

	/**
	 * 
	 * 
	 * @param flightSegmentIds
	 * @param userPrincipal
	 * @param userPrincipal
	 * @param modelNo
	 *            TODO
	 * @param seatCharges
	 *            TODO
	 * @param templateCode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce assignFlightSeatCharges(Collection<Integer> flightSegmentIds, int templateId,
			UserPrincipal userPrincipal, String modelNo, Set<SeatCharge> seatChargesList) throws ModuleException {
		/*
		 * Consist main logic of template assigning
		 */

		DefaultServiceResponse res = new DefaultServiceResponse(false);
		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();

		// If blank template
		if (templateId == 0) {

			// TODO charith we may able to extract saveSeatFares method out of the for loop
			for (Integer iflightSegId : flightSegmentIds) {

				Collection<FlightSeat> reservedSeats = getSeatMapDAO().getSelectedFlightSeats(iflightSegId);
				if (reservedSeats != null && !reservedSeats.isEmpty()) {
					throw new ModuleException("airinventory.logic.seat.allocation.already.reserved");
				}

				// check if flight seg already has charge assigned
				Collection<FlightSeat> exFlightSeats = getSeatMapDAO().getFilghtSeats(iflightSegId);
				// If there's existing flights
				if (exFlightSeats != null && !exFlightSeats.isEmpty()) {
					// make charge reference null
					Collection<FlightSeat> flightSeats = new ArrayList<FlightSeat>();
					for (FlightSeat flightSeat : exFlightSeats) {
						flightSeat.setChargeId(null);
						// defect fix TAIR-00007
						flightSeat.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						// flightSeat.setSeatId(null);
						flightSeat.setUserDetails(userPrincipal);
						flightSeats.add(flightSeat);
					}
					getSeatMapDAO().saveSeatFares(flightSeats);
					arrAssigned.add(iflightSegId);
				}
			}

			res.setSuccess(true);
			res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED, arrAssigned);
			res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED, arrNotAssigned);
			res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED, arrNewlyAssigned);
			return res;
		}

		for (Integer iflightSegId : flightSegmentIds) {
			// Get seatcharges from templatecode
			Collection<SeatCharge> seatCharges = (seatChargesList == null) ? getSeatCharges(templateId) : seatChargesList;
			boolean isSeatSold = false;
			boolean isEmptyChargeTemplate = false;
			Map<Integer, String> seatWiseLogicalCabinClasses = getSeatWiseLogicalCabinClasses(iflightSegId, modelNo);
			// check if flight seg already has charge assigned
			Collection<FlightSeat> exSeatsCharges = getSeatMapDAO().getFilghtSeats(iflightSegId);
			if (exSeatsCharges != null && !exSeatsCharges.isEmpty()) {
				// check whether seats sold
				Collection<Integer> soldSeats = new ArrayList<Integer>();
				for (FlightSeat flightSeat : exSeatsCharges) {
					if (flightSeat.getChargeId() == null) {
						isEmptyChargeTemplate = true;
					} else {
						if (flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)
								|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED)
								|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.OHD)) {
							soldSeats.add(flightSeat.getSeatId());
						}
					}
				}

				if (isEmptyChargeTemplate) {
					if (seatCharges.size() >= exSeatsCharges.size()) {
						Collection<FlightSeat> flightSeats = new ArrayList<FlightSeat>();
						Map<Integer, SeatCharge> seatChargesMap = getSeatChargesMap(seatCharges);
						for (FlightSeat flightSeat : exSeatsCharges) {

							// Should retrieve the data for the same seat ID from the charges
							SeatCharge seatCharge = seatChargesMap.get(flightSeat.getSeatId());
							if (seatCharge != null) {
								flightSeat.setAmount(seatCharge.getChargeAmount());
								flightSeat.setChargeId(seatCharge.getChargeId());
								flightSeat.setSeatId(seatCharge.getSeatId());

								if (flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)
										|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED)
										|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.OHD)) {
									// Skip reserved, blocked or onhold
								} else {
									if (seatCharge.getStatus().equals("ACT")) {
										flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
									} else if (seatCharge.getStatus().equals("INA")) {
										flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
									}
								}
								flightSeat.setUserDetails(userPrincipal);
								flightSeats.add(flightSeat);
								if (seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()) != null) {
									flightSeat.setLogicalCCCode(seatWiseLogicalCabinClasses.get(seatCharge.getSeatId())
											.split("_")[1].trim());
								}
							}
							getSeatMapDAO().saveSeatFares(flightSeats);
							arrAssigned.add(iflightSegId);
							continue;
						}
						getSeatMapDAO().saveSeatFares(flightSeats);
						arrAssigned.add(iflightSegId);
						continue;
					}

				}

				if (soldSeats.size() > 0) {
					isSeatSold = true;
				}
				Map<Integer, FlightSeat> exSeatFlights = getExFlightSeatMap(exSeatsCharges);

				if (templateId == 0) { // Null or iniit to 0
					// If sold
					if (isSeatSold) {
						res.setSuccess(false);
						throw new ModuleException("airinventory.logic.bl.flightseatcharge.failed.soldseats",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
					// Remove template
					getSeatMapDAO().deleteSeatFares(exSeatFlights.values());
					res.setSuccess(true);
				} else {

					// If seats were not sold, assign the new charges
					if (!isSeatSold) {
						// Load
						Collection<FlightSeat> flightSeats = new ArrayList<FlightSeat>();
						for (SeatCharge seatCharge : seatCharges) {

							FlightSeat flightSeat = null;
							if (exSeatFlights.containsKey(seatCharge.getChargeId())) {
								flightSeat = exSeatFlights.get(seatCharge.getChargeId());
								flightSeat.setUserDetails(userPrincipal);
								exSeatFlights.remove(seatCharge.getChargeId());
							} else {
								flightSeat = new FlightSeat();
								flightSeat.setUserDetails(userPrincipal);
							}

							flightSeat.setFlightSegId(iflightSegId);
							flightSeat.setChargeId(seatCharge.getChargeId());
							flightSeat.setSeatId(seatCharge.getSeatId());
							flightSeat.setAmount(seatCharge.getChargeAmount());
							if (flightSeat.getStatus() != null) {
								if (flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)
										|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED)
										|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.OHD)) {
									arrNotAssigned.add(iflightSegId);
								} else {
									if (seatCharge.getStatus().equals("ACT") || seatCharge.getStatus().equals("VAC")) {
										flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
									} else if (seatCharge.getStatus().equals("INA")) {
										flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
									}
								}
							} else {
								if (seatCharge.getStatus().equals("ACT") || seatCharge.getStatus().equals("VAC")) {
									flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
								} else if (seatCharge.getStatus().equals("INA")) {
									flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
								}
							}

							// check the inventory defined for logical cabin class code.If not not update the charges.
							if (seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()) != null) {
								String[] seatInvData = seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()).split("_");
								flightSeat.setFlightSegInventoryId(new Integer(seatInvData[0].trim()));
								flightSeat.setLogicalCCCode(seatInvData[1].trim());
								flightSeat.setUserDetails(userPrincipal);
								flightSeats.add(flightSeat);
							}
						}
						if (log.isDebugEnabled()) {
							List<Integer> flithSeatIds = new ArrayList<Integer>();
							for (FlightSeat flightSeat : exSeatFlights.values()) {
								flithSeatIds.add(flightSeat.getFlightAmSeatId());
							}
							log.debug("deleting flight seats " + flithSeatIds);
						}
						getSeatMapDAO().deleteSeatFares(exSeatFlights.values()); // Deleting to accomodate variation of
																					// AirCraft (No of seat change)
						getSeatMapDAO().saveSeatFares(flightSeats);
						arrAssigned.add(iflightSegId);
					} else {
						// Seats Sold, but still needs to change the template.
						// 1. Verify all Sold seats available (matching seat code)
						Map<Integer, FlightSeat> mapSeatIDFlightSeat = buildMapSeatIdFlightSeat(exSeatFlights);						
						updateFlightWithNewTemplate(mapSeatIDFlightSeat, seatCharges, iflightSegId, modelNo, soldSeats);
						
					}
				}
			} else {
				// else
				// create chargesfor flights
				createChargesForNewFlights(seatCharges, iflightSegId, userPrincipal, modelNo);
				arrAssigned.add(iflightSegId);
				arrNewlyAssigned.add(iflightSegId);
			}
		}
		updateFlightAnciAssignStatus(arrAssigned, arrNewlyAssigned, arrNotAssigned);
		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	/**
	 * 
	 * @param flightSeatDTOs
	 * @param userPrincipal
	 * @param modelNo
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked" })
	public ServiceResponce assignFlightSeatCharges(Collection<FlightSeatsDTO> flightSeatDTOs, UserPrincipal userPrincipal,
			String modelNo) throws ModuleException {
		DefaultServiceResponse res = new DefaultServiceResponse(false);
		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();
		Map<Integer, Collection<Integer>> mapFlightSeats = new HashMap<Integer, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIds = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsFailed = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsNewlyAssigned = new HashMap<String, Collection<Integer>>();

		for (FlightSeatsDTO flightSeatDTO : flightSeatDTOs) {
			if (mapFlightSeats.containsKey(flightSeatDTO.getTemplateId())) {
				Collection<Integer> flightSegIds = mapFlightSeats.get(flightSeatDTO.getTemplateId());
				flightSegIds.add(flightSeatDTO.getFlightSegmentID());
				mapFlightSeats.put(flightSeatDTO.getTemplateId(), flightSegIds);
			} else {
				Collection<Integer> flightSegIds = new ArrayList<Integer>();
				flightSegIds.add(flightSeatDTO.getFlightSegmentID());
				mapFlightSeats.put(flightSeatDTO.getTemplateId(), flightSegIds);
			}
		}

		for (Integer templateId : mapFlightSeats.keySet()) {
			Collection<Integer> colSegIds = mapFlightSeats.get(templateId);

			ServiceResponce sr = assignFlightSeatCharges(colSegIds, templateId, userPrincipal, modelNo, null);
			arrAssigned.addAll((Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED));
			arrNotAssigned.addAll((Collection<Integer>) sr
					.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED));
			arrNewlyAssigned.addAll((Collection<Integer>) sr
					.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED));
			if (arrAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIds.put(Integer.toString(templateId),
						(Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED));
			}
			if (arrNotAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(templateId),
						(Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED));
			}
			if (arrNewlyAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(templateId),
						(Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED));
			}

		}

		// Audit record
		Map<String, Map<String, Collection<Integer>>> map = new HashMap<String, Map<String, Collection<Integer>>>();
		map.put(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED, mapAuditTemplateIDcolSegIds);
		map.put(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED, mapAuditTemplateIDcolSegIdsFailed);
		map.put(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED, mapAuditTemplateIDcolSegIdsNewlyAssigned);
		AuditAirinventory.doAuditSeatMapAssignSeatCharges(map, userPrincipal);

		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	public ServiceResponce releaseBlockedSeats(UserPrincipal up) throws ModuleException {

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		// Load FlightSeats
		Calendar oCal = Calendar.getInstance();
		oCal.add(Calendar.MINUTE, -1 * AppSysParamsUtil.getTimeForSeatMapSeatRelease());
		Date seatReleaseEndDate = oCal.getTime();

		int noOfBackDays = AppSysParamsUtil.getNoOfBackDaysToReconcileCardPayments();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -1 * noOfBackDays);
		Date seatReleaseStartDate = cal.getTime();

		Collection<Integer> colFlightSeatIds = getFlightSeatJDBCDAO().getFlightSeatsToRelease(seatReleaseStartDate,
				seatReleaseEndDate);

		if (!colFlightSeatIds.isEmpty()) {
			// Update with VACANT state
			getFlightSeatBL().updateFlightSeats(SMUtil.adaptToAHashMap(colFlightSeatIds),
					AirinventoryCustomConstants.FlightSeatStatuses.VACANT, up, null);
		}
		sr.setSuccess(true);

		return sr;
	}

	private Map<Integer, FlightSeat> getExFlightSeatMap(Collection<FlightSeat> exFlightSeat) {
		Map<Integer, FlightSeat> map = new HashMap<Integer, FlightSeat>();
		for (FlightSeat flightSeat : exFlightSeat) {
			map.put(flightSeat.getChargeId(), flightSeat);
		}
		return map;

	}

	private void createChargesForNewFlights(Collection<SeatCharge> seatCharges, int iflightSegId, UserPrincipal userPrincipal,
			String modelNo) throws ModuleException {
		Collection<FlightSeat> flightSeats = new ArrayList<FlightSeat>();
		Map<Integer, String> seatWiseLogicalCabinClasses = getSeatWiseLogicalCabinClasses(iflightSegId, modelNo);
		for (SeatCharge seatCharge : seatCharges) {
			FlightSeat flightSeat = new FlightSeat();
			flightSeat.setFlightSegId(iflightSegId);
			flightSeat.setChargeId(seatCharge.getChargeId());
			flightSeat.setAmount(seatCharge.getChargeAmount());
			flightSeat.setSeatId(seatCharge.getSeatId());
			if (seatCharge.getStatus().equals("ACT")) {
				flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
			} else if (seatCharge.getStatus().equals("INA")) {
				flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
			}

			// check the inventory defined for cabin class code.If not not update the charges.
			if (seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()) != null) {
				String[] seatInvData = seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()).split("_");
				flightSeat.setFlightSegInventoryId(new Integer(seatInvData[0]));
				flightSeat.setLogicalCCCode(seatInvData[1]);
				flightSeat.setUserDetails(userPrincipal);
				flightSeats.add(flightSeat);
			}

		}
		getSeatMapDAO().saveSeatFares(flightSeats);

	}

	/**
	 * Build map with <seatID,FlightSeat>
	 * 
	 * @param chargeIdFlightSeat
	 *            <chargeId,FlightSeat>
	 * @return
	 * @throws ModuleException
	 */
	private Map<Integer, FlightSeat> buildMapSeatIdFlightSeat(Map<Integer, FlightSeat> chargeIdFlightSeat) throws ModuleException {
		Map<Integer, FlightSeat> mapSeatIDFlightSeat = new HashMap<Integer, FlightSeat>();
		Collection<SeatCharge> existingSeatCharges = getChargeBD().getSeatCharges(
				new ArrayList<Integer>(chargeIdFlightSeat.keySet()));
		for (SeatCharge seatCharge : existingSeatCharges) {
			mapSeatIDFlightSeat.put(seatCharge.getSeatId(), chargeIdFlightSeat.get(seatCharge.getChargeId()));
		}
		return mapSeatIDFlightSeat;
	}

	private void updateFlightWithNewTemplate(Map<Integer, FlightSeat> mapSeatIDFlightSeat, Collection<SeatCharge> seatCharges,
			Integer iflightSegId, String modelNo, Collection<Integer> soldSeats) throws ModuleException {
		Collection<FlightSeat> flightSeats = new ArrayList<FlightSeat>();
		Map<Integer, String> seatWiseLogicalCabinClasses = getSeatWiseLogicalCabinClasses(iflightSegId, modelNo);
		for (SeatCharge seatCharge : seatCharges) {
			// Get new SeatCharge
			// Get Matching old FlightSeat by SeatID (Assume same aircraft)
			FlightSeat flightSeat = mapSeatIDFlightSeat.get(seatCharge.getSeatId());
			if (flightSeat == null){
				flightSeat = new FlightSeat();
			} else {
				mapSeatIDFlightSeat.remove(seatCharge.getSeatId());
			}
			// Assigne new amount, charge id,

			flightSeat.setAmount(seatCharge.getChargeAmount());
			flightSeat.setChargeId(seatCharge.getChargeId());
			flightSeat.setSeatId(seatCharge.getSeatId());
			flightSeat.setFlightSegId(iflightSegId); // Only if flight is also changing

			if (flightSeat.getStatus() != null) {
				if (flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)
						|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED)
						|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.OHD)) {
					// Leave if already SOLD
				} else {
					if (seatCharge.getStatus().equals("ACT")) {
						flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
					} else if (seatCharge.getStatus().equals("INA")) {
						flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
					}
				}
			} else {
				if (seatCharge.getStatus().equals("ACT")) {
					flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.VACANT);
				} else if (seatCharge.getStatus().equals("INA")) {
					flightSeat.setStatus(AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE);
				}
			}

			// check the inventory defined for cabin class code.If not not update the charges.
			if (seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()) != null) {
				String[] seatInvData = seatWiseLogicalCabinClasses.get(seatCharge.getSeatId()).split("_");
				flightSeat.setFlightSegInventoryId(new Integer(seatInvData[0]));
				flightSeat.setLogicalCCCode(seatInvData[1]);
				flightSeats.add(flightSeat);
			} else {

			}
		}

		Map<Integer, AirCraftModelSeats> seatIDAircraftSeat = getAircraftBD().getNewReservedAircraftSeats(iflightSegId,
				modelNo);

		for (Integer seatId : soldSeats) {
			FlightSeat flightSeat = mapSeatIDFlightSeat.get(seatId);
			AirCraftModelSeats newReservedSeat = seatIDAircraftSeat.get(seatId);
			if (newReservedSeat != null && flightSeat != null) {
				if (newReservedSeat.getCabinClassCode().equals(flightSeat.getLogicalCCCode())
						&& !newReservedSeat.getStatus().equals("INA")) {
					mapSeatIDFlightSeat.remove(seatId);
				} else if (newReservedSeat.getStatus().equals("INA")) {
					throw new ModuleException("airinventory.logic.inactive.destination.for.sold.seat");
				} else {
					throw new ModuleException("airinventory.logic.invalid.destination.class.of.service.for.sold.seat");
				}
			} else {
				mapSeatIDFlightSeat.remove(seatId);
			}
		}

		List<Integer> seatFarestoDelete = new ArrayList<Integer>(mapSeatIDFlightSeat.keySet());
		getSeatMapDAO().deleteExistingSeatFares((ArrayList<Integer>) seatFarestoDelete , iflightSegId);
		getSeatMapDAO().saveSeatFares(flightSeats);
	}

	private Map<String, Collection<Integer>> buildMapWithSegCodeColSegIds(Collection<FlightSeatsDTO> flightSegIds) {
		Map<String, Collection<Integer>> mapSegCodeColSegIds = new HashMap<String, Collection<Integer>>();
		for (FlightSeatsDTO flightSeatsDTO : flightSegIds) {
			addToCollectionOfMap(mapSegCodeColSegIds, flightSeatsDTO);
		}
		return mapSegCodeColSegIds;
	}

	private void addToCollectionOfMap(Map<String, Collection<Integer>> map, FlightSeatsDTO flightSeatsDTO) {
		if (map.containsKey(flightSeatsDTO.getSegmentCode())) {
			Collection<Integer> colExisting = map.get(flightSeatsDTO.getSegmentCode());
			colExisting.add(flightSeatsDTO.getFlightSegmentID());
			map.put(flightSeatsDTO.getSegmentCode(), colExisting);
		} else {
			Collection<Integer> col = new HashSet<Integer>();
			col.add(flightSeatsDTO.getFlightSegmentID());
			map.put(flightSeatsDTO.getSegmentCode(), col);
		}
	}

	private Map<String, FlightSeatsDTO> buildMapWithSegCode(Collection<FlightSeatsDTO> colTemplateIds) {
		Map<String, FlightSeatsDTO> mapSegCodeTempIds = new HashMap<String, FlightSeatsDTO>();
		for (FlightSeatsDTO flightSeatDTO : colTemplateIds) {
			mapSegCodeTempIds.put(flightSeatDTO.getSegmentCode(), flightSeatDTO);
		}
		return mapSegCodeTempIds;
	}

	private Collection<SeatCharge> getSeatCharges(int templateId) throws ModuleException {
		ChargeTemplate chargeTemplate = getChargeBD().getChargeTemplate(templateId);
		return chargeTemplate.getSeatCharges();
	}

	private Map<Integer, String> getSeatWiseLogicalCabinClasses(int iflightSegId, String modelNo) throws ModuleException {
		Map<String, List<Integer>> seatIds = getAircraftBD().getAirCraftModelSeatsSeatIDModelSeats(modelNo);
		Map<String, Integer> mapBccCodeFCCInvId = getFlightInventoryDAO().getFCCSegInventoryMap(iflightSegId);
		Map<String, Integer> hiddenSeatCount = getAircraftBD().getHiddenSeatsMap(modelNo);
		Map<Integer, String> seatWiseLogicalCabinClasses = createSeatWiseLogicalCC(seatIds, mapBccCodeFCCInvId, hiddenSeatCount);
		return seatWiseLogicalCabinClasses;
	}

	private Map<Integer, String> createSeatWiseLogicalCC(Map<String, List<Integer>> seatIds, Map<String, Integer> mapBccCodeFCCInvId,
			Map<String, Integer> hiddenSeatsMap) {
		int seatCount = 0;
		boolean hiddenSeatsAvailabble = (hiddenSeatsMap != null && !hiddenSeatsMap.isEmpty());
		Collection<Integer> checkedSeatIds = new ArrayList<Integer>();
		Map<Integer, String> seatWiseSegmentIds = new HashMap<Integer, String>();
		for (Entry<String, Integer> entry : mapBccCodeFCCInvId.entrySet()) {
			String logicalCC = entry.getKey().split("_")[1];
			int logicalCCCOunt = new Integer(entry.getKey().split("_")[2]);
			String ccCode = entry.getKey().split("_")[0];

			if (hiddenSeatsAvailabble && hiddenSeatsMap.containsKey(logicalCC)) {
				logicalCCCOunt += hiddenSeatsMap.get(logicalCC);
			}

			for (Integer seatId : seatIds.get(ccCode)) {
				if (!checkedSeatIds.contains(seatId) && seatCount < logicalCCCOunt) {
					seatWiseSegmentIds.put(seatId, entry.getValue() + "_" + logicalCC);
					checkedSeatIds.add(seatId);
					seatCount++;
				}
			}
			seatCount = 0;
		}
		return seatWiseSegmentIds;
	}

	private ChargeBD getChargeBD() {
		return AirInventoryModuleUtils.getChargeBD();
	}

	private SeatMapDAO getSeatMapDAO() {
		return (SeatMapDAO) AirInventoryUtil.getInstance().getLocalBean("SeatMapDAOImplProxy");
	}

	private SeatMapJDBCDAO getSeatMapJDBCDAO() {
		return (SeatMapJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("seatMapJDBCDAO");
	}

	private FlightSeatJDBCDAO getFlightSeatJDBCDAO() {
		return (FlightSeatJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("flightSeatJDBCDAO");
	}

	private FlightBD getFlightBD() {
		return AirInventoryModuleUtils.getFlightBD();
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean(
				"FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	private AircraftBD getAircraftBD() {
		return AirInventoryModuleUtils.getAircraftBD();
	}

	private FlightSeatBL getFlightSeatBL() {
		if (_flightSeatBL == null) {
			_flightSeatBL = new FlightSeatBL();
		}
		return _flightSeatBL;
	}

	private Map<Integer, SeatCharge> getSeatChargesMap(Collection<SeatCharge> seatCharges) {

		Map<Integer, SeatCharge> seatChargesTemplateMap = new HashMap<Integer, SeatCharge>();
		for (SeatCharge charge : seatCharges) {
			seatChargesTemplateMap.put(charge.getSeatId(), charge);
		}

		return seatChargesTemplateMap;

	}

	private void updateFlightAnciAssignStatus(ArrayList<Integer> arrAssigned, ArrayList<Integer> arrNewlyAssigned,
			ArrayList<Integer> arrNotAssigned) throws ModuleException {

		FlightBD flightBd = AirInventoryModuleUtils.getFlightBD();

		if (arrNotAssigned != null && arrNotAssigned.size() > 0) {
			flightBd.updateFltAnciAssignStatusByFltSegId(arrNotAssigned, ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP,
					AnciDefaultTemplStatusEnum.CREATED_W_ERRORS);
		}

		if (arrAssigned != null && arrAssigned.size() > 0) {
			flightBd.updateFltAnciAssignStatusByFltSegId(arrAssigned, ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP,
					AnciDefaultTemplStatusEnum.CREATED);
		}

		if (arrNewlyAssigned != null && arrNewlyAssigned.size() > 0) {
			flightBd.updateFltAnciAssignStatusByFltSegId(arrNewlyAssigned, ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP,
					AnciDefaultTemplStatusEnum.CREATED);
		}

	}
	
	private Set<SeatCharge> getSourceFlightSeatCharges(int flightSegId) throws ModuleException {
		Collection<SeatDTO> seatDtos = getSeatMapDAO().getFlightSeatasSeatDTOs(flightSegId);  
		Set<SeatCharge> seatCharges = new HashSet<SeatCharge>();
		for (SeatDTO seatDto : seatDtos) {
			SeatCharge seatCharge = new SeatCharge();
			seatCharge.setSeatId(seatDto.getSeatID());
			seatCharge.setChargeId(seatDto.getChargeID());
			if (AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE.equals(seatDto.getStatus())) {
				seatCharge.setStatus(SeatChargeStatus.INACTIVE.getStatusCode());
			} else {
				seatCharge.setStatus(SeatChargeStatus.ACTIVE.getStatusCode());
			}
			seatCharge.setChargeAmount(seatDto.getChargeAmount());
			seatCharges.add(seatCharge);
		}
		return seatCharges;
	}
}
