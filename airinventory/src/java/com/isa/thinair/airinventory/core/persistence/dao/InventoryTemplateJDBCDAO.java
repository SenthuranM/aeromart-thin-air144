package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.ArrayList;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.commons.api.dto.Page;

public interface InventoryTemplateJDBCDAO {

	public Page<InvTempCCAllocDTO> getInvTemplCCAllocDataPage(int start, int pageSize, String aircraftModel);

	public ArrayList<InvTempCCBCAllocDTO> getBCDetailsForInvTemp(String ccCode);

	public ArrayList<InvTempCCBCAllocDTO> loadGridBCallocDetails(InvTempDTO invTempDTO, String cabinClassCode, String logicalCabinClassCode);

	public Page<InvTempCCAllocDTO> getExistInvTempCCAlloPage(int start, int pageSize, String airCraftModel, int invTempID);

}
