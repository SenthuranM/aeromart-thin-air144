package com.isa.thinair.airinventory.core.audit;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FlightInvRollResultsSummaryDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.dto.inventory.UpdatedInvTempDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.InvTempCabinAlloc;
import com.isa.thinair.airinventory.api.model.InvTempCabinBCAlloc;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQ;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;

/**
 * @author Byorn
 */
public abstract class AuditAirinventory {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditSeatMapAssignSeatCharges(String templateCode, Collection<String> segmentIds, boolean isRollFwd,
			boolean isApplied, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template id", templateCode);
		contents.put("segment_ids", getContent(segmentIds));
		contents.put("isRollFwd", isRollFwd ? "Y" : "N");
		contents.put("isApplied", isApplied ? "Y" : "N");

		audit.setTaskCode(String.valueOf(TasksUtil.SEATMAP_ASSIGN_SEATCHARGE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditSeatMapChangeSeatCharges(String templateCode, String newTemplateCode,
			Collection<String> segmentIds, boolean isRollFwded, boolean isApplied, UserPrincipal principal)
			throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template id", templateCode);
		contents.put("new_template_code", newTemplateCode);
		contents.put("segment_ids", getContent(segmentIds));
		contents.put("isRollFwd", isRollFwded ? "Y" : "N");
		contents.put("isApplied", isApplied ? "Y" : "N");

		audit.setTaskCode(String.valueOf(TasksUtil.SEATMAP_CHANGE_SEATCHARGE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditSeatMapRollFowardSeatChargesWrapper(Map mapTemplateIDSegIds, boolean isApplied,
			UserPrincipal principal, boolean isRollFoward) throws ModuleException {
		Iterator<String> itrKeys = mapTemplateIDSegIds.keySet().iterator();
		while (itrKeys.hasNext()) {
			String templateId = itrKeys.next();
			Collection colSegIds = (Collection) mapTemplateIDSegIds.get(templateId);
			doAuditSeatMapChangeSeatCharges(templateId, null, colSegIds, isRollFoward, isApplied, principal);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditSeatMapRollFowardNewSeatChargesWrapper(Map mapTemplateIDSegIds, boolean isApplied,
			UserPrincipal principal, boolean isRollFoward) throws ModuleException {
		Iterator itrKeys = mapTemplateIDSegIds.keySet().iterator();
		while (itrKeys.hasNext()) {
			String templateId = (String) itrKeys.next();
			Collection colSegIds = (Collection) mapTemplateIDSegIds.get(templateId);
			doAuditSeatMapAssignSeatCharges(templateId, colSegIds, isRollFoward, isApplied, principal);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditSeatMapRollFowardSeatCharges(Map<String, Map> mapFailSuccess, UserPrincipal principal)
			throws ModuleException {
		Iterator itrKeys = mapFailSuccess.keySet().iterator();
		while (itrKeys.hasNext()) {
			String isSuccessList = (String) itrKeys.next();
			Map<String, Collection> mapTemplateIDSegIds = mapFailSuccess.get(isSuccessList);
			if (isSuccessList.equals(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED)) {
				doAuditSeatMapRollFowardSeatChargesWrapper(mapTemplateIDSegIds, true, principal, true);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED)) {
				doAuditSeatMapRollFowardSeatChargesWrapper(mapTemplateIDSegIds, false, principal, true);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED)) {
				doAuditSeatMapRollFowardNewSeatChargesWrapper(mapTemplateIDSegIds, true, principal, true);
			}

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditSeatMapAssignSeatCharges(Map<String, Map<String, Collection<Integer>>> mapFailSuccess,
			UserPrincipal principal) throws ModuleException {
		for (String isSuccessList : mapFailSuccess.keySet()) {
			Map<String, Collection<Integer>> mapTemplateIDSegIds = mapFailSuccess.get(isSuccessList);
			if (isSuccessList.equals(AirinventoryCustomConstants.FlightSeatCharge.ASSIGNED)) {
				doAuditSeatMapRollFowardSeatChargesWrapper(mapTemplateIDSegIds, true, principal, false);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightSeatCharge.NOTASSIGNED)) {
				doAuditSeatMapRollFowardSeatChargesWrapper(mapTemplateIDSegIds, false, principal, false);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightSeatCharge.NEWLYASSIGNED)) {
				doAuditSeatMapRollFowardNewSeatChargesWrapper(mapTemplateIDSegIds, true, principal, false);
			}

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditDeleteBookingClass(String bookingCode, UserPrincipal principal) throws ModuleException {

		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("booking code", bookingCode);
		audit.setTaskCode(String.valueOf(TasksUtil.INVENTORY_DELETE_BOOKING_CODES));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditSaveUpdateBookingClass(Persistent persistant, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof BookingClass) {
			BookingClass obj = (BookingClass) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.INVENTORY_ADD_BOOKING_CODES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.INVENTORY_EDIT_BOOKNG_CODES));
			}

			contents.put("booking code", nullHandler(obj.getBookingCode()));
			contents.put("des", nullHandler(obj.getBookingCodeDescription()));
			contents.put("cc code", nullHandler(obj.getCabinClassCode()));
			contents.put("nest rank", nullHandler(obj.getNestRank()));
			contents.put("pax type", nullHandler(obj.getPaxType()));
			contents.put("status", nullHandler(obj.getStatus()));
			contents.put("remarks", nullHandler(obj.getRemarks()));
			contents.put("fxd flag", obj.getFixedFlag() ? "true" : "false");
			contents.put("standard code", obj.getStandardCode() ? "yes" : "no");

		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void createFlightInventory(String userName, CreateFlightInventoryRQ createFlightInventoryRQ)
			throws ModuleException {

		if (createFlightInventoryRQ.getAircraftModel() == null || createFlightInventoryRQ.getSegmentDTOs() == null
				|| userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		AuditExternalDataProvider auditExternalDataProvider = AuditExternalDataProvider.getInstance();
		Map<Integer, FlightSegmentDTO> segmentInfo = new HashMap<Integer, FlightSegmentDTO>();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && createFlightInventoryRQ.getSegmentDTOs() != null
				&& createFlightInventoryRQ.getSegmentDTOs().size() > 0) {
			Collection<Integer> fltSegIDs = new HashSet<Integer>();
			for (SegmentDTO segmentDTO : createFlightInventoryRQ.getSegmentDTOs()) {
				fltSegIDs.add(segmentDTO.getSegmentId());
			}
			if (fltSegIDs.size() > 0) {
				segmentInfo.putAll(auditExternalDataProvider.getCachedFlightSegmentInfoCollection(fltSegIDs));
			}
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		// create flight inventory
		audit.setTaskCode(TasksUtil.INVENTORY_CREATE_FLIGHT_INVENTORY);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_FLIGHT_ID,
				nullHandler(createFlightInventoryRQ.getFlightId()));
		if (isDescriptiveAuditEnabled && nullHandler(createFlightInventoryRQ.getFlightId()).length() > 0) {
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_FLIGHT_NUMBER,
					nullHandler(auditExternalDataProvider.getCachedFlightNo(createFlightInventoryRQ.getFlightId())));
		}
		contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_MODEL_NUMBER, nullHandler(createFlightInventoryRQ
				.getAircraftModel().getModelNumber()));

		Iterator cabinClassCapacitiesIt = createFlightInventoryRQ.getAircraftModel().getAircraftCabinCapacitys().iterator();
		int i = 0;
		while (cabinClassCapacitiesIt.hasNext()) {
			AircraftCabinCapacity aircraftCabinCapacity = (AircraftCabinCapacity) cabinClassCapacitiesIt.next();
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_MODEL_CABINCLASS + i,
					nullHandler(aircraftCabinCapacity.getCabinClassCode()));
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_MODEL_CC_ADULTS + i,
					nullHandler(aircraftCabinCapacity.getSeatCapacity()));
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_MODEL_CC_INFANTS + i,
					nullHandler(aircraftCabinCapacity.getInfantCapacity()));
		}

		contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_OL_FLIGHT_ID,
				nullHandler(createFlightInventoryRQ.getOverlappingFlightId()));
		if (isDescriptiveAuditEnabled && nullHandler(createFlightInventoryRQ.getOverlappingFlightId()).length() > 0) {
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_OL_FLIGHT_NUMBER,
					nullHandler(auditExternalDataProvider.getCachedFlightNo(createFlightInventoryRQ.getOverlappingFlightId())));
		}
		Iterator segments = createFlightInventoryRQ.getSegmentDTOs().iterator();
		i = 0;
		while (segments.hasNext()) {
			SegmentDTO segmentDTO = (SegmentDTO) segments.next();
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_SEG_CODE + i, nullHandler(segmentDTO.getSegmentCode()));
			contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_FLT_SEG_ID + i, nullHandler(segmentDTO.getSegmentId()));

			if (isDescriptiveAuditEnabled && nullHandler(segmentDTO.getSegmentId()).length() > 0) {
				FlightSegmentDTO segment = segmentInfo.get(segmentDTO.getSegmentId());
				if (segment != null) {
					contents.put(AuditInventoryConstants.CREATE_FLIGHT_INVENTORY_FLT_SEG_DEP_DATE + i,
							nullHandler(segment.getDepartureDateTime()));
				}
			}
			i = i + 1;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void updateFCCInventoriesForModelUpdate(Collection flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, String userName, String segmentCode) throws ModuleException {
		if (flightIds == null || updatedAircraftModel == null || existingAircraftModel == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Map<Pair<Integer, String>, FlightSegmentDTO> segmentInfo = new HashMap<Pair<Integer, String>, FlightSegmentDTO>();
		AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && flightIds.size() > 0) {
			segmentInfo.putAll(dataProvider.getCachedFlightSegmentInfoCollection(flightIds, segmentCode));
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_FCCINVENTORIES_FOR_MODEL_UPDATE);
		Iterator iterator = flightIds.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			Integer flightId = (Integer) iterator.next();
			LinkedHashMap contents = new LinkedHashMap();
			contents.put("flightid" + i, nullHandler(flightId));
			if (isDescriptiveAuditEnabled && nullHandler(flightId).length() > 0) {
				Pair<Integer, String> pair = Pair.of(flightId, segmentCode);
				FlightSegmentDTO segment = segmentInfo.get(pair);
				if (segment != null) {
					contents.put("flightno" + i, nullHandler(segment.getFlightNumber()));
					contents.put("dep date" + i, nullHandler(segment.getDepartureDateTime()));
					contents.put("seg code" + i, nullHandler(segment.getSegmentCode()));
				}
			}

			contents.put("updated aircraft model" + i, nullHandler(updatedAircraftModel.getModelNumber()));
			contents.put("existing aircraft model" + i, nullHandler(existingAircraftModel.getModelNumber()));
			AirinventoryUtils.getAuditorBD().audit(audit, contents);
			i = i + 1;
		}

	}

	public static void updateFCCInventoriesForModelChange(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, Collection<Integer> olFlightIds, String userName, String segmentCode)
			throws ModuleException {

		if (flightIds == null || updatedAircraftModel == null || existingAircraftModel == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Map<Pair<Integer, String>, FlightSegmentDTO> segmentInfo = new HashMap<Pair<Integer, String>, FlightSegmentDTO>();
		AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && flightIds.size() > 0) {
			Collection<Integer> allIDs = new HashSet<Integer>();
			allIDs.addAll(flightIds);
			if (olFlightIds != null && olFlightIds.size() > 0) {
				allIDs.addAll(olFlightIds);
			}
			segmentInfo.putAll(dataProvider.getCachedFlightSegmentInfoCollection(allIDs, segmentCode));
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		// create flight inventory
		audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_FCCINVENTORIES_FOR_MODEL_CHANGE);

		Iterator<Integer> iterator = flightIds.iterator();
		int i = 0;
		while (iterator.hasNext()) {

			String flightId = iterator.next().toString();
			Integer olFlightId = null;
			if (olFlightIds != null) {
				if (olFlightIds.contains(flightId)) {
					Iterator<Integer> iterOlflights = olFlightIds.iterator();
					while (iterOlflights.hasNext()) {
						Integer olflightid = iterOlflights.next();
						if (olflightid.equals(flightId)) {
							olFlightId = olflightid;
						}
					}
				}
			}
			LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
			contents.put("flightid" + i, flightId);

			if (isDescriptiveAuditEnabled && nullHandler(flightId).length() > 0) {
				Pair<Integer, String> pair = Pair.of(Integer.parseInt(flightId), segmentCode);
				FlightSegmentDTO segment = segmentInfo.get(pair);
				if (segment != null) {
					contents.put("flightno" + i, nullHandler(segment.getFlightNumber()));
					contents.put("dep date" + i, nullHandler(segment.getDepartureDateTime()));
					contents.put("seg code" + i, nullHandler(segment.getSegmentCode()));
				}
			}

			contents.put("Overlapping flightid" + i, nullHandler(olFlightId));

			if (isDescriptiveAuditEnabled && nullHandler(olFlightId).length() > 0) {
				Pair<Integer, String> pair = Pair.of(olFlightId.intValue(), segmentCode);
				FlightSegmentDTO segment = segmentInfo.get(pair);
				if (segment != null) {
					contents.put("Overlapping flightno" + i, nullHandler(segment.getFlightNumber()));
					contents.put("Overlapping dep date" + i, nullHandler(segment.getDepartureDateTime()));
					contents.put("Overlapping seg code" + i, nullHandler(segment.getSegmentCode()));
				}

			}
			contents.put("updated aircraft model" + i, nullHandler(updatedAircraftModel.getModelNumber()));
			contents.put("existing aircraft model" + i, nullHandler(existingAircraftModel.getModelNumber()));
			i = i + 1;
			AirinventoryUtils.getAuditorBD().audit(audit, contents);
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void updateFCCSegInventory(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO, String userName,
			boolean isOptimizeFunc) throws ModuleException {

		if (fccSegInventoryUpdateDTO == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		FlightSegmentDTO segment = null;
		if (isDescriptiveAuditEnabled && nullHandler(fccSegInventoryUpdateDTO.getFlightId()).length() > 0
				&& nullHandler(fccSegInventoryUpdateDTO.getSegmentCode()).length() > 0) {
			AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
			segment = dataProvider.getCachedFlightSegmentInfo(fccSegInventoryUpdateDTO.getFlightId(),
					fccSegInventoryUpdateDTO.getSegmentCode());
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);

		if (isOptimizeFunc) {
			audit.setTaskCode(TasksUtil.INVENTORY_OPTIMISE_BOOKING_CODE_INVENTORY);
		} else {
			audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_SEGMENT_ALLOCATIONS);
		}

		LinkedHashMap contents = new LinkedHashMap();
		contents.put("flightid", String.valueOf(fccSegInventoryUpdateDTO.getFlightId()));
		if (isDescriptiveAuditEnabled && segment != null) {
			contents.put("flightno", nullHandler(segment.getFlightNumber()));
			contents.put("dep date", nullHandler(segment.getDepartureDateTimeZulu()));
		}
		contents.put("fcc inv id", String.valueOf(fccSegInventoryUpdateDTO.getFccsInventoryId()));
		contents.put("logical cc code", fccSegInventoryUpdateDTO.getLogicalCCCode());
		contents.put("seg code", fccSegInventoryUpdateDTO.getSegmentCode());
		contents.put("curtaild seats", String.valueOf(fccSegInventoryUpdateDTO.getCurtailedSeats()));
		contents.put("oversell seats", String.valueOf(fccSegInventoryUpdateDTO.getOversellSeats()));
		contents.put("infant alloc", String.valueOf(fccSegInventoryUpdateDTO.getInfantAllocation()));
		contents.put("WL alloc", String.valueOf(fccSegInventoryUpdateDTO.getWaitListSeats()));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

		Set addedSet = fccSegInventoryUpdateDTO.getAddedFCCSegBCInventories();

		if (addedSet != null && addedSet.size() > 0) {

			contents.clear();
			contents.put("flightid", String.valueOf(fccSegInventoryUpdateDTO.getFlightId()));
			if (isDescriptiveAuditEnabled && segment != null) {
				contents.put("flightno", nullHandler(segment.getFlightNumber()));
				contents.put("dep date", nullHandler(segment.getDepartureDateTimeZulu()));
			}
			contents.put("seg code", fccSegInventoryUpdateDTO.getSegmentCode());

			Iterator iterAddedSet = addedSet.iterator();
			int i = 0;
			while (iterAddedSet.hasNext()) {
				FCCSegBCInventory segBCInventory = (FCCSegBCInventory) iterAddedSet.next();
				contents = copyFCCSegBCInventory(contents, segBCInventory, i);
				i = i + 1;
			}

			audit.setTaskCode(TasksUtil.INVENTORY_ADD_BOOKING_CODE_ALLLOCATIONS);
			AirinventoryUtils.getAuditorBD().audit(audit, contents);

		}

		Set deletedSet = fccSegInventoryUpdateDTO.getDeletedFCCSegBCInventories();
		Set deletedBookingCodes = fccSegInventoryUpdateDTO.getDeletedFCCSegBookingCodes();
		if (deletedSet != null && deletedSet.size() > 0 && deletedBookingCodes != null && deletedBookingCodes.size() > 0) {
			contents.clear();
			// contents.put("DELETED FCCBCInve","000000");
			contents.put("flightid", String.valueOf(fccSegInventoryUpdateDTO.getFlightId()));
			if (isDescriptiveAuditEnabled && segment != null) {
				contents.put("flightno", nullHandler(segment.getFlightNumber()));
				contents.put("dep date", nullHandler(segment.getDepartureDateTimeZulu()));
			}
			contents.put("seg code", fccSegInventoryUpdateDTO.getSegmentCode());

			Iterator iterdeletedSet = deletedSet.iterator();
			Iterator iterDeletedBC = deletedBookingCodes.iterator();
			String deletedInvId = "";
			String deletedBC = "";
			while (iterdeletedSet.hasNext()) {
				deletedInvId += iterdeletedSet.next() + " ";
				deletedBC += iterDeletedBC.next() + " ";
			}
			contents.put("deleted fccsbinvId", deletedInvId);
			contents.put("deleted fccsbc", deletedBC);

			audit.setTaskCode(TasksUtil.INVENTORY_DELETE_BOOKING_CODE_ALLLOCATIONS);
			AirinventoryUtils.getAuditorBD().audit(audit, contents);

		}

		Set updatedSet = fccSegInventoryUpdateDTO.getUpdatedFCCSegBCInventories();
		if (updatedSet != null && updatedSet.size() > 0) {
			contents.clear();
			Iterator iterupdatedSet = updatedSet.iterator();
			contents.put("flightid", String.valueOf(fccSegInventoryUpdateDTO.getFlightId()));
			if (isDescriptiveAuditEnabled && segment != null) {
				contents.put("flightno", nullHandler(segment.getFlightNumber()));
				contents.put("dep date", nullHandler(segment.getDepartureDateTimeZulu()));
			}
			contents.put("seg code", fccSegInventoryUpdateDTO.getSegmentCode());

			int i = 0;
			while (iterupdatedSet.hasNext()) {
				FCCSegBCInventory segBCInventory = (FCCSegBCInventory) iterupdatedSet.next();
				contents = copyFCCSegBCInventory(contents, segBCInventory, i);
				i = i + 1;
			}
			contents.put("", "");
			audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_BOOKING_CODE_ALLLOCATIONS);
			AirinventoryUtils.getAuditorBD().audit(audit, contents);
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addAuditForBCClosure(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO, String userId)
			throws ModuleException {

		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		FlightSegmentDTO segment = null;
		if (isDescriptiveAuditEnabled && nullHandler(fccSegInventoryUpdateDTO.getFlightId()).length() > 0
				&& nullHandler(fccSegInventoryUpdateDTO.getSegmentCode()).length() > 0) {
			AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
			segment = dataProvider.getCachedFlightSegmentInfo(fccSegInventoryUpdateDTO.getFlightId(),
					fccSegInventoryUpdateDTO.getSegmentCode());
		}
		Set updatedSet = fccSegInventoryUpdateDTO.getUpdatedFCCSegBCInventories();
		LinkedHashMap contents = new LinkedHashMap();
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		if (updatedSet != null && updatedSet.size() > 0) {

			Iterator iterupdatedSet = updatedSet.iterator();
			contents.put("flightid", String.valueOf(fccSegInventoryUpdateDTO.getFlightId()));
			if (isDescriptiveAuditEnabled && segment != null) {
				contents.put("flightno", nullHandler(segment.getFlightNumber()));
				contents.put("dep date", nullHandler(segment.getDepartureDateTimeZulu()));
			}
			contents.put("seg code", fccSegInventoryUpdateDTO.getSegmentCode());

			int i = 0;
			while (iterupdatedSet.hasNext()) {
				FCCSegBCInventory segBCInventory = (FCCSegBCInventory) iterupdatedSet.next();
				contents = copyFCCSegBCInventory(contents, segBCInventory, i);
				i = i + 1;
			}
			contents.put("", "");
			audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_BOOKING_CODE_ALLLOCATIONS);
			AirinventoryUtils.getAuditorBD().audit(audit, contents);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static LinkedHashMap copyFCCSegBCInventory(LinkedHashMap contents, FCCSegBCInventory segBCInventory, int i) {

		if (segBCInventory.getOnHoldSeats() == null) {
			segBCInventory.setOnHoldSeats(0);
		}
		if (segBCInventory.getSeatsSold() == null) {
			segBCInventory.setSeatsSold(0);
		}
		if (segBCInventory.getSeatsCancelled() == null) {
			segBCInventory.setSeatsCancelled(0);
		}
		if (segBCInventory.getSeatsAvailable() == null) {
			segBCInventory.setSeatsAvailable(0);
		}
		if (segBCInventory.getPreviousSeatsAllocated() == null) {
			segBCInventory.setPreviousSeatsAllocated(segBCInventory.getSeatsAllocated());
		}

		contents.put("fccsbinvId" + i, String.valueOf(segBCInventory.getFccsbInvId()));
		contents.put("previos seats allocated" + i, String.valueOf(segBCInventory.getPreviousSeatsAllocated()));
		contents.put("new seats allocated" + i, String.valueOf(segBCInventory.getSeatsAllocated()));
		contents.put("actual seats onhold" + i, String.valueOf(segBCInventory.getOnHoldSeats()));
		contents.put("seats sold" + i, String.valueOf(segBCInventory.getSeatsSold()));
		contents.put("booking code" + i, nullHandler(segBCInventory.getBookingCode()));
		contents.put("status chg action" + i, segBCInventory.getStatusChangeAction());
		contents.put("onhold seats" + i, String.valueOf(segBCInventory.getOnHoldSeats()));
		contents.put("Previous WL allcation" + i, String.valueOf(segBCInventory.getPreviousWLAllocated()));
		contents.put("WL allcation" + i, String.valueOf(segBCInventory.getAllocatedWaitListSeats()));
		contents.put("WL sold seats" + i, String.valueOf(segBCInventory.getWaitListedSeats()));
		contents.put("is priotrity" + i, segBCInventory.getPriorityFlag() ? "yes" : "no");
		contents.put("onhold acquired by nesting" + i, String.valueOf(segBCInventory.getSeatsOnHoldAquiredByNesting()));
		contents.put("seats onhold nestd" + i, String.valueOf(segBCInventory.getSeatsOnHoldNested()));
		contents.put("seats sold" + i, String.valueOf(segBCInventory.getSeatsSold()));
		contents.put("seats sold nested" + i, String.valueOf(segBCInventory.getSeatsSoldNested()));
		contents.put("status" + i, String.valueOf(segBCInventory.getStatus()));
		return contents;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void rollFlightInventory(Collection flightIds, String userName, InvRollForwardFlightsSearchCriteria criteria,
			int sourceFlightId) throws ModuleException {

		if (flightIds == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		audit.setTaskCode(TasksUtil.INVENTORY_ROLL_BOOKING_CODE_ALLLOCATIONS);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put("num of flights ", String.valueOf(flightIds.size()));
		contents.put(" inventories updated for flights -->", "");
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && criteria != null) {
			contents.put("Source Flight Id", nullHandler(sourceFlightId));
			contents.put("flightno", nullHandler(criteria.getFlightNumber()));
			contents.put("from date", nullHandler(criteria.getFromDate()));
			contents.put("to date", nullHandler(criteria.getToDate()));
		}
		Iterator iterator = flightIds.iterator();
		int i = 1;
		while (iterator.hasNext()) {
			contents.put("flight id" + i, iterator.next());
			i = i + 1;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void rollFwdFltInvDetailAudit(LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollforwardStatus,
			String userName) throws ModuleException {

		Iterator<FlightInvRollResultsSummaryDTO> rollStatusCollIt = rollforwardStatus.values().iterator();
		while (rollStatusCollIt.hasNext()) {
			FlightInvRollResultsSummaryDTO status = rollStatusCollIt.next();
			Audit audit = new Audit();
			audit.setTimestamp(new Date());
			audit.setUserId(userName);
			audit.setTaskCode(TasksUtil.INVENTORY_ROLL_FORWARD_FLIGHT_INVENTORY);
			LinkedHashMap contents = new LinkedHashMap();
			contents.put("DETAIL_ROLLFWD_AUDIT", status.getSummary());
			AirinventoryUtils.getAuditorBD().audit(audit, contents);
		}
	}

	public static void invalidateFlightSegmentInventories(Collection<Integer> flightSegIds, String userName)
			throws ModuleException {

		if (flightSegIds == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Map<Integer, FlightSegmentDTO> segmentInfo = new HashMap<Integer, FlightSegmentDTO>();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && flightSegIds.size() > 0) {
			AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
			segmentInfo.putAll(dataProvider.getCachedFlightSegmentInfoCollection(flightSegIds));
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		audit.setTaskCode(TasksUtil.INVENTORY_INVALIDATE_FLIGHT_SEGMENT_INVENTORIES);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put("num of flight segs ", String.valueOf(flightSegIds.size()));
		contents.put(" invalidating  inventories for flight segments -->", "");
		Iterator iterator = flightSegIds.iterator();
		int i = 1;
		while (iterator.hasNext()) {
			Integer flightSegmentID = (Integer) iterator.next();
			FlightSegmentDTO segment = segmentInfo.get(flightSegmentID);
			if (isDescriptiveAuditEnabled && segment != null) {
				contents.put("flight no" + i, nullHandler(segment.getFlightNumber()));
				contents.put("dep date" + i, nullHandler(segment.getDepartureDateTime()));
				contents.put("seg code" + i, nullHandler(segment.getSegmentCode()));
			}
			contents.put("flight seg id" + i, flightSegmentID);
			i = i + 1;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void removeInventoryOverlap(Collection flightIds, String userName) throws ModuleException {

		if (flightIds == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Map<Integer, String> flightNoInfo = new HashMap<Integer, String>();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && flightIds.size() > 0) {
			AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
			flightNoInfo.putAll(dataProvider.getCachedFlightNoCollection(flightIds));
		}
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		audit.setTaskCode(TasksUtil.INVENTORY_REMOVE_INVENTORY_OVERLAP);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put("num of flightIds ", String.valueOf(flightIds.size()));
		contents.put(" remove inventories for overlapping flights -->", "");
		Iterator iterator = flightIds.iterator();
		int i = 1;
		while (iterator.hasNext()) {
			Integer id = (Integer) iterator.next();
			contents.put("flight ids" + i, id.toString());
			if (isDescriptiveAuditEnabled && flightNoInfo.get(id) != null) {
				contents.put("flightno" + i, nullHandler(flightNoInfo.get(id)));
			}
			i = i + 1;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void deleteFlightInventories(Collection flightIds, String userName) throws ModuleException {

		if (flightIds == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Map<Integer, String> flightNoInfo = new HashMap<Integer, String>();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && flightIds.size() > 0) {
			AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
			flightNoInfo.putAll(dataProvider.getCachedFlightNoCollection(flightIds));
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		audit.setTaskCode(TasksUtil.INVENTORY_DELETE_FLIGHTINVENTORIES);
		LinkedHashMap contents = new LinkedHashMap();

		contents.put(" deleting flight inventories  -->", "");
		Iterator iterator = flightIds.iterator();
		int i = 1;
		while (iterator.hasNext()) {
			Integer id = (Integer) iterator.next();
			contents.put("flight ids" + i, id.toString());
			if (isDescriptiveAuditEnabled && flightNoInfo.get(id) != null) {
				contents.put("flightno" + i, nullHandler(flightNoInfo.get(id)));
			}
			i = i + 1;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void makeInvalidFltSegInvsValid(Collection flightSegIds, String userName) throws ModuleException {

		if (flightSegIds == null || userName == null) {
			throw new ModuleException("airinventory.auditor.invalidparams", "airinventory.desc");
		}

		Map<Integer, FlightSegmentDTO> segmentInfo = new HashMap<Integer, FlightSegmentDTO>();
		boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
		if (isDescriptiveAuditEnabled && flightSegIds.size() > 0) {
			AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
			segmentInfo.putAll(dataProvider.getCachedFlightSegmentInfoCollection(flightSegIds));
		}

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userName);
		audit.setTaskCode(TasksUtil.INVENTORY_VALIDATE_FIGHT_SEGMENT_INVENTORIES);
		LinkedHashMap contents = new LinkedHashMap();

		contents.put(" make flt segs valid  -->", "");
		Iterator iterator = flightSegIds.iterator();
		int i = 1;
		while (iterator.hasNext()) {
			Integer id = (Integer) iterator.next();
			contents.put("flight seg id" + i, id);
			if (isDescriptiveAuditEnabled && segmentInfo.get(id) != null) {
				FlightSegmentDTO segment = segmentInfo.get(id);
				contents.put("flight no" + i, nullHandler(segment.getFlightNumber()));
				contents.put("dep date" + i, nullHandler(segment.getDepartureDateTime()));
				contents.put("seg code" + i, nullHandler(segment.getSegmentCode()));
			}
			i = i + 1;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	private static void moveBlockedSeatsToSold(Collection blockedRecords, String userId) throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		audit.setTaskCode(TasksUtil.INVENTORY_MOVE_BLOCK_SEATS_TO_SOLD);

		LinkedHashMap contents = new LinkedHashMap();
		;
		Iterator blockedRecordsIt = blockedRecords.iterator();
		for (int i = 0; blockedRecordsIt.hasNext(); ++i) {

			TempSegBcAlloc tempSegBcAlloc = (TempSegBcAlloc) blockedRecordsIt.next();

			contents.put("rec no", String.valueOf(i));
			contents.put("seg inv id", tempSegBcAlloc.getFccaId());

			if (tempSegBcAlloc.getNestRecordId() != null) {
				contents.put("nested bc inv id", tempSegBcAlloc.getNestRecordId());
			}

			if (tempSegBcAlloc.getFccsbaId() != null) {
				contents.put("bc inv id", tempSegBcAlloc.getFccsbaId());
				contents.put("no of adult seats", tempSegBcAlloc.getSeatsBlocked());
			} else {
				contents.put("no of infant seats", tempSegBcAlloc.getSeatsBlocked());
			}
		}
		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public static void doSeatSold(SegmentSeatDistsDTO segmentSeatDTO, String flighId, String userId, SeatDistribution seatDis)
			throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_MAKE_CNX_RESEVATION);

		LinkedHashMap contents = new LinkedHashMap();

		if (BeanUtils.nullHandler(flighId).length() > 0) {
			contents.put("flight id", flighId);
		}

		contents.put("cabin class", segmentSeatDTO.getCabinClassCode());
		// contents.put("booking code", segmentSeatDTO.getBookingCode());
		contents.put("booking class code", seatDis.getBookingClassCode());
		// contents.put("isNested", seatDis.isNestedSeats());
		if (seatDis.getNoOfSeats() > 0) {
			contents.put("no of adult/child seats", seatDis.getNoOfSeats());
			// contents.put("no of all adult seats", segmentSeatDTO.getTotalAdultSeats());

			contents.put("no of infant seats", segmentSeatDTO.getNoOfInfantSeats());

			AirinventoryUtils.getAuditorBD().audit(audit, contents);
		}

	}


	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public static void doSeatRelease(Collection<ReservationSegmentDTO> colReservationSegmentDTO, String userId,
			Reservation reservation) throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		audit.setTaskCode(TasksUtil.INVENTORY_UPDATE_MAKE_CNX_RESEVATION);

		Iterator<ReservationSegmentDTO> itColReservationSegmentDTO = colReservationSegmentDTO.iterator();
		ReservationSegmentDTO reservationSegmentDTO;

		StringBuilder changes = new StringBuilder();
		LinkedHashMap contents = new LinkedHashMap();
		while (itColReservationSegmentDTO.hasNext()) {
			reservationSegmentDTO = itColReservationSegmentDTO.next();
			contents.put("Seat released Flight No", reservationSegmentDTO.getFlightNo());
			contents.put("Dep date", reservationSegmentDTO.getDepartureStringDate("dd-MM-yyyy"));
			contents.put("Segment Code", reservationSegmentDTO.getSegmentCode());
			contents.put("flight id", reservationSegmentDTO.getFlightId());

		}
		contents.put("no of adults", reservation.getTotalPaxAdultCount());
		contents.put("no of childs", reservation.getTotalPaxChildCount());
		contents.put("no of infant", reservation.getTotalPaxInfantCount());
		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditMealRollFowardMealCharges(Map<String, Map> mapFailSuccess, UserPrincipal principal)
			throws ModuleException {
		Iterator itrKeys = mapFailSuccess.keySet().iterator();
		while (itrKeys.hasNext()) {
			String isSuccessList = (String) itrKeys.next();
			Map<String, Collection> mapTemplateIDSegIds = mapFailSuccess.get(isSuccessList);
			if (isSuccessList.equals(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED)) {
				doAuditMealRollFowardMealChargesWrapper(mapTemplateIDSegIds, true, principal, true);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED)) {
				doAuditMealRollFowardMealChargesWrapper(mapTemplateIDSegIds, false, principal, true);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED)) {
				doAuditMealRollFowardMealChargesWrapper(mapTemplateIDSegIds, true, principal, true);
			}

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditBaggageRollFowardBaggageCharges(Map<String, Map<String, Collection<Integer>>> mapFailSuccess,
			UserPrincipal principal) throws ModuleException {
		Iterator itrKeys = mapFailSuccess.keySet().iterator();
		while (itrKeys.hasNext()) {
			String isSuccessList = (String) itrKeys.next();
			Map<String, Collection> mapTemplateIDSegIds = (Map) mapFailSuccess.get(isSuccessList);
			if (isSuccessList.equals(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED)) {
				doAuditBaggageRollFowardBaggageChargesWrapper(mapTemplateIDSegIds, true, principal, true);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED)) {
				doAuditBaggageRollFowardBaggageChargesWrapper(mapTemplateIDSegIds, false, principal, true);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED)) {
				doAuditBaggageRollFowardBaggageChargesWrapper(mapTemplateIDSegIds, true, principal, true);
			}

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditMealRollFowardMealChargesWrapper(Map mapTemplateIDSegIds, boolean isApplied,
			UserPrincipal principal, boolean isRollFoward) throws ModuleException {
		Iterator itrKeys = mapTemplateIDSegIds.keySet().iterator();
		while (itrKeys.hasNext()) {
			String templateId = (String) itrKeys.next();
			Collection colSegIds = (Collection) mapTemplateIDSegIds.get(templateId);
			doAuditMealChangeMealCharges(templateId, null, colSegIds, isRollFoward, isApplied, principal);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditBaggageRollFowardBaggageChargesWrapper(Map mapTemplateIDSegIds, boolean isApplied,
			UserPrincipal principal, boolean isRollFoward) throws ModuleException {
		Iterator itrKeys = mapTemplateIDSegIds.keySet().iterator();
		while (itrKeys.hasNext()) {
			String templateId = (String) itrKeys.next();
			Collection colSegIds = (Collection) mapTemplateIDSegIds.get(templateId);
			doAuditBaggageChangeBaggageCharges(templateId, null, colSegIds, isRollFoward, isApplied, principal);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditMealChangeMealCharges(String templateCode, String newTemplateCode, Collection<String> segmentIds,
			boolean isRollFwded, boolean isApplied, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template id", templateCode);
		contents.put("new_template_code", newTemplateCode);
		contents.put("segment_ids", getContent(segmentIds));
		contents.put("isRollFwd", isRollFwded ? "Y" : "N");
		contents.put("isApplied", isApplied ? "Y" : "N");

		audit.setTaskCode(String.valueOf(TasksUtil.MEAL_ASSIGN_MEALCHARGE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void doAuditBaggageChangeBaggageCharges(String templateCode, String newTemplateCode,
			Collection<String> segmentIds, boolean isRollFwded, boolean isApplied, UserPrincipal principal)
			throws ModuleException {
		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		contents.put("template id", templateCode);
		contents.put("new_template_code", newTemplateCode);
		contents.put("segment_ids", getContent(segmentIds));
		contents.put("isRollFwd", isRollFwded ? "Y" : "N");
		contents.put("isApplied", isApplied ? "Y" : "N");

		audit.setTaskCode(String.valueOf(TasksUtil.BAGGAGE_ASSIGN_BAGGAGECHARGE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditOverBookings(Collection<String> segmentIds, String bookingClass, UserPrincipal principal,
			String pnr, String flightId, String userId, int seatCount) throws ModuleException {
		Audit audit = new Audit();
		if (principal != null) {
			audit.setUserId(principal.getUserId());
		} else {
			if (userId != "" && userId != null) {
				audit.setUserId(userId);
			} else {
				audit.setUserId("");
			}
		}

		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();
		if (BeanUtils.nullHandler(pnr).length() > 0) {
			contents.put("pnr", pnr);
		}
		contents.put("flight_details", getContent(segmentIds));
		if (BeanUtils.nullHandler(bookingClass).length() > 0) {
			contents.put("booking_class", bookingClass);
		}
		if (BeanUtils.nullHandler(flightId).length() > 0) {
			contents.put("flight id", flightId);
		}
		if (seatCount > 0) {
			contents.put("seat count", seatCount);
		}
		audit.setTaskCode(String.valueOf(TasksUtil.OVERBOOK_SEAT_TEMPLATE));

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditMealAssignMealCharges(Map<String, Map> mapFailSuccess, UserPrincipal principal)
			throws ModuleException {
		Iterator itrKeys = mapFailSuccess.keySet().iterator();
		while (itrKeys.hasNext()) {
			String isSuccessList = (String) itrKeys.next();
			Map<String, Collection> mapTemplateIDSegIds = mapFailSuccess.get(isSuccessList);
			if (isSuccessList.equals(AirinventoryCustomConstants.FlightMealCharge.ASSIGNED)) {
				doAuditMealRollFowardMealChargesWrapper(mapTemplateIDSegIds, true, principal, false);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightMealCharge.NOTASSIGNED)) {
				doAuditMealRollFowardMealChargesWrapper(mapTemplateIDSegIds, false, principal, false);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightMealCharge.NEWLYASSIGNED)) {
				doAuditMealRollFowardMealChargesWrapper(mapTemplateIDSegIds, true, principal, false);
			}

		}
	}

	@SuppressWarnings({ "rawtypes" })
	public static void doAuditBaggageAssignBaggageCharges(Map<String, Map<String, Collection<Integer>>> mapFailSuccess,
			UserPrincipal principal) throws ModuleException {
		Iterator itrKeys = mapFailSuccess.keySet().iterator();
		while (itrKeys.hasNext()) {
			String isSuccessList = (String) itrKeys.next();
			Map<String, Collection<Integer>> mapTemplateIDSegIds = mapFailSuccess.get(isSuccessList);
			if (isSuccessList.equals(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED)) {
				doAuditBaggageRollFowardBaggageChargesWrapper(mapTemplateIDSegIds, true, principal, false);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED)) {
				doAuditBaggageRollFowardBaggageChargesWrapper(mapTemplateIDSegIds, false, principal, false);
			} else if (isSuccessList.equals(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED)) {
				doAuditBaggageRollFowardBaggageChargesWrapper(mapTemplateIDSegIds, true, principal, false);
			}

		}
	}

	private static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	private static String getContent(Collection<String> collection) throws ModuleException {
		String retrn = "";
		if (collection != null && collection.size() > 0) {
			Iterator<String> itr = collection.iterator();
			boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
			if (isDescriptiveAuditEnabled) {
				Collection<Integer> ids = getIntIDs(collection);
				Map<Integer, FlightSegmentDTO> segmentInfo = AuditExternalDataProvider.getInstance()
						.getCachedFlightSegmentInfoCollection(ids);
				for (Integer id : ids) {
					retrn += "seg id:" + id;
					if (segmentInfo.get(id) != null) {
						FlightSegmentDTO segment = segmentInfo.get(id);
						retrn += ("-flightNo:" + segment.getFlightNumber() + "-depdate:" + segment.getDepartureDateTime()
								+ "-segcode:" + segment.getSegmentCode());
						retrn += ",";
					}
				}

			} else {
				while (itr.hasNext()) {
					Object element = itr.next();
					// 12DEC2011 CJ : Dont understand why instanceof checking if it is a string collection :(
					if (element instanceof Integer) {
						Integer integer = (Integer) element;
						element = integer.toString();
					}
					if (element instanceof String) {
						retrn += element + ",";
					}
				}
			}
		}

		return retrn;
	}

	@SuppressWarnings("rawtypes")
	private static Collection<Integer> getIntIDs(Collection ids) {
		Collection<Integer> retIDs = new HashSet<Integer>();
		for (Object id : ids) {
			if (nullHandler(id).length() > 0) {
				Integer retID = Integer.valueOf(id.toString().trim());
				retIDs.add(retID);
			}
		}
		return retIDs;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditCreateInventoryTemplate(InvTempDTO invTempDTO, UserPrincipal userPrincipal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (userPrincipal != null) {
			audit.setUserId(userPrincipal.getUserId());
		} else {
			audit.setUserId("");
		}
		
		if(invTempDTO.getInvTempID() != null){
			contents.put("AirCraft Model ID", nullHandler(invTempDTO.getAirCraftModel()));
			contents.put("Segment", nullHandler(invTempDTO.getSegment()));
			contents.put("status", nullHandler(invTempDTO.getStatus()));
			Iterator<InvTempCCAllocDTO> iterator = invTempDTO.getInvTempCCAllocDTOList().iterator();
			while (iterator.hasNext()) {
				InvTempCCAllocDTO invTempCCAllocDTO = iterator.next();
				contents.put("Cabin class code", nullHandler(invTempCCAllocDTO.getCabinClassCode()));
				contents.put("Logical cabin Class code", nullHandler(invTempCCAllocDTO.getLogicalCabinClassCode()));

				if (invTempCCAllocDTO.getInvTempCCBCAllocDTOList() != null
						&& !invTempCCAllocDTO.getInvTempCCBCAllocDTOList().isEmpty()) {
					Iterator<InvTempCCBCAllocDTO> bcIterator = invTempCCAllocDTO.getInvTempCCBCAllocDTOList().iterator();
					while (bcIterator.hasNext()) {
						InvTempCCBCAllocDTO invTempCCBCAllocDTO = bcIterator.next();
						contents.put("bc code- " + invTempCCBCAllocDTO.getBookingCode(), "priority -"
								+ nullHandler(invTempCCBCAllocDTO.getPriorityFlag()) + ",allocations -"
								+ nullHandler(invTempCCBCAllocDTO.getAllocatedSeats()) + ",status -"
								+ nullHandler(invTempCCBCAllocDTO.getStatusBcAlloc()));
					}
				}
			}
		}
		audit.setTaskCode(String.valueOf(TasksUtil.ADD_INVENTORY_TEMPLATE));
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditUpdateInventoryTemplate(InvTempDTO invTempDTO, ArrayList<InvTempCabinAlloc> auditList,
			UpdatedInvTempDTO updatedList, UserPrincipal userPrincipal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (userPrincipal != null) {
			audit.setUserId(userPrincipal.getUserId());
		} else {
			audit.setUserId("");
		}
		contents.put("AirCraft Model ID", nullHandler(invTempDTO.getAirCraftModel()));
		contents.put("Segment", nullHandler(invTempDTO.getSegment()));
		contents.put("status", nullHandler(invTempDTO.getStatus()));
		
		if (auditList != null) {
			Iterator<InvTempCabinAlloc> ccAllocIt = auditList.iterator();
			while (ccAllocIt.hasNext()) {
				InvTempCabinAlloc ccAllocToAudit = ccAllocIt.next();
				contents.put("Cabin Class Code", nullHandler(ccAllocToAudit.getCabinClassCode()));
				contents.put("Logical Cabin Class Code", nullHandler(ccAllocToAudit.getLogicalCabinClassCode()));
				if (updatedList != null) {
					if (updatedList.getAddedInvTempBCAllocs() != null && !updatedList.getAddedInvTempBCAllocs().isEmpty()) {
						Iterator<InvTempCCBCAllocDTO> addedIt = updatedList.getAddedInvTempBCAllocs().iterator();
						while (addedIt.hasNext()) {
							InvTempCCBCAllocDTO invTempCCBCAllocDTO = addedIt.next();
							contents.put("Added bc code- " + invTempCCBCAllocDTO.getBookingCode(), "priority -"
									+ nullHandler(invTempCCBCAllocDTO.getPriorityFlag()) + ",allocations -"
									+ nullHandler(invTempCCBCAllocDTO.getAllocatedSeats()) + ",status -"
									+ nullHandler(invTempCCBCAllocDTO.getStatusBcAlloc()));
						}
					}
					if (updatedList.getDeletedInvTempBCAllocs() != null && !updatedList.getDeletedInvTempBCAllocs().isEmpty()) {
						Iterator<InvTempCCBCAllocDTO> deletedIt = updatedList.getDeletedInvTempBCAllocs().iterator();
						while (deletedIt.hasNext()) {
							InvTempCCBCAllocDTO invTempCCBCAllocDTO = deletedIt.next();
							Iterator<InvTempCabinBCAlloc> olderIt = ccAllocToAudit.getInvTempCabinBCAllocs().iterator();
							while (olderIt.hasNext()) {
								InvTempCabinBCAlloc deletedBcAlloc = olderIt.next();
								if (invTempCCBCAllocDTO.getInvTmpCabinBCAllocID()
										.equals(deletedBcAlloc.getInvTmpCabinBCAllocID())) {
									contents.put("Deleted bc code- " + deletedBcAlloc.getBookingCode(), "priority -"
											+ nullHandler(deletedBcAlloc.getPriorityFlag()) + ",allocations -"
											+ nullHandler(deletedBcAlloc.getAllocatedSeats()) + ",status -"
											+ nullHandler(deletedBcAlloc.getStatusBcAlloc()));
								}
							}
						}
					}
					if (updatedList.getEditedInvTempBCAllocs() != null && !updatedList.getEditedInvTempBCAllocs().isEmpty()) {
						Iterator<InvTempCCBCAllocDTO> editedIt = updatedList.getEditedInvTempBCAllocs().iterator();
						while (editedIt.hasNext()) {
							InvTempCCBCAllocDTO newInvTempBcAlloc = editedIt.next();
							Iterator<InvTempCabinBCAlloc> oldIt = ccAllocToAudit.getInvTempCabinBCAllocs().iterator();
							while (oldIt.hasNext()) {
								InvTempCabinBCAlloc oldInvTempBcAlloc = oldIt.next();
								if (newInvTempBcAlloc.getInvTmpCabinBCAllocID().equals(
										oldInvTempBcAlloc.getInvTmpCabinBCAllocID())) {
									contents.put(
											"Edited bc code- " + oldInvTempBcAlloc.getBookingCode(),
											"old values - priority_" + oldInvTempBcAlloc.getPriorityFlag() + ",allocations_"
													+ oldInvTempBcAlloc.getAllocatedSeats() + ",status_"
													+ oldInvTempBcAlloc.getStatusBcAlloc() + " new Values -priority_"
													+ newInvTempBcAlloc.getPriorityFlag() + ",allocations_"
													+ newInvTempBcAlloc.getAllocatedSeats() + ",status_"
													+ newInvTempBcAlloc.getStatusBcAlloc());
									break;
								}

							}
						}
					}
				}
			}
		}
		audit.setTaskCode(String.valueOf(TasksUtil.MODIFY_INVENTORY_TEMPLATE));
		AirinventoryUtils.getAuditorBD().audit(audit, contents);

	}
	public static void saveRollForwardBatchREQ(String userId, RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException {
		
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		// create Roll forward Adavance
		audit.setTaskCode(TasksUtil.INVENTORY_ADVANCE_ROLL_FORWARD_FLIGHT_INVENTORY);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_REQUESTED_DATA,
				nullHandler(rollForwardBatchREQ.getRequestedData()));
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_STATUS,
				nullHandler(rollForwardBatchREQ.getStatus()));
	
		Iterator rollForwardBatchREQSEGIteraror = rollForwardBatchREQ.getRollForwardBatchREQSEGSet().iterator();
		int i = 0;
		while (rollForwardBatchREQSEGIteraror.hasNext()) {
			RollForwardBatchREQSEG rollForwardBatchREQSEG = (RollForwardBatchREQSEG) rollForwardBatchREQSEGIteraror.next();
			contents.put(AuditInventoryConstants.CREATE_ROLLFORWARDSEG_FLIGHT_NUMBER + i,
					nullHandler(rollForwardBatchREQSEG.getFlightNumber()));
			contents.put(AuditInventoryConstants.CREATE_ROLLFORWARDSEG_FROM_DATE + i,
					nullHandler(rollForwardBatchREQSEG.getFromDate()));
			contents.put(AuditInventoryConstants.CREATE_ROLLFORWARDSEG_TO_DATE + i,
					nullHandler(rollForwardBatchREQSEG.getToDate()));
			i++;
		}

		AirinventoryUtils.getAuditorBD().audit(audit, contents);
		
	}

	public static void updateRollForwardBatchREQStatus(String userId, Integer batchId, String status) throws ModuleException {
		
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		audit.setTaskCode(TasksUtil.INVENTORY_ADVANCE_ROLL_FORWARD_FLIGHT_INVENTORY);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_BATCH_ID,
				nullHandler(batchId));
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_STATUS,
				nullHandler(status));
		
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}
	public static void updateRollForwardBatchREQSEQStatus(String userId, Integer batchSeqId, String flightNumber, Date fromDate, Date toDate, String status) throws ModuleException {
		
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		audit.setTaskCode(TasksUtil.INVENTORY_ADVANCE_ROLL_FORWARD_FLIGHT_INVENTORY);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_BATCH_SEQ_ID,
				nullHandler(batchSeqId));
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_FLIGHT_NUMBER,
				nullHandler(flightNumber));
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_FROM_DATE,
				nullHandler(fromDate));
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_TO_DATE,
				nullHandler(toDate));
		contents.put(AuditInventoryConstants.CREATE_ROLLFORWARD_STATUS,
				nullHandler(status));
		
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
	}

	public static void startRollForwardBatch(String userId, String strSelectedSegBCs, String strFlights, String strFromDate_1,
			String strToDate_1, String strFromDate_2, String strToDate_2, String strFromDate_3, String strToDate_3,
			String strFromDate_4, String strToDate_4, String strFromDate_5, String strToDate_5, String strDeleteTarget,
			String strOverrideLevel, String strCreateSegAllocs, String strRadioMode, String strFCCInventoryInfo,
			String strSeatFactorMin, String strSeatFactorMax, String priority, String bcStatus, String deleteODDBC,
			String createSegAllocs, int curTail, int overSell, int waitListing, StringBuffer daysColSB) throws ModuleException {
		
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userId);
		audit.setTaskCode(TasksUtil.INVENTORY_ADVANCE_ROLL_FORWARD_FLIGHT_INVENTORY);
		LinkedHashMap contents = new LinkedHashMap();
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_BATCH_SELECTED_SEGS,
				nullHandler(strSelectedSegBCs));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_BATCH_FLIGHTS,
				nullHandler(strFlights));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_FROM_DATE_1,
				nullHandler(strFromDate_1));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_TO_DATE_1,
				nullHandler(strToDate_1));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_FROM_DATE_2,
				nullHandler(strFromDate_2));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_TO_DATE_2,
				nullHandler(strToDate_2));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_FROM_DATE_3,
				nullHandler(strFromDate_3));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_TO_DATE_3,
				nullHandler(strToDate_3));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_FROM_DATE_4,
				nullHandler(strFromDate_4));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_TO_DATE_4,
				nullHandler(strToDate_4));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_FROM_DATE_5,
				nullHandler(strFromDate_5));
		contents.put(AuditInventoryConstants.START_ROLLFORWARDSEG_TO_DATE_5,
				nullHandler(strToDate_5));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_DELETE_TARGET,
				nullHandler(strDeleteTarget));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_OVERRIDE_LEVEL,
				nullHandler(strOverrideLevel));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_CREATE_SEG_ALLOCS,
				nullHandler(strCreateSegAllocs));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_FCC_INVENTORY_INFO,
				nullHandler(strFCCInventoryInfo));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_SEAT_FACTOR_MIN,
				nullHandler(strSeatFactorMin));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_SEAT_FACTOR_MAX,
				nullHandler(strSeatFactorMax));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_PRIORITY,
				nullHandler(priority));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_BCSTATUS,
				nullHandler(bcStatus));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_DELETEODDBC,
				nullHandler(deleteODDBC));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_CUR_TAIL,
				nullHandler(curTail));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_OVER_SELL,
				nullHandler(overSell));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_WAIT_LISTING,
				nullHandler(waitListing));
		contents.put(AuditInventoryConstants.START_ROLLFORWARD_DAYS_COLS_DB,
				nullHandler(daysColSB));
		AirinventoryUtils.getAuditorBD().audit(audit, contents);
		
	}
}
