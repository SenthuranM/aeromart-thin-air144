/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoriesExistsCriteria;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.GdsPublishFccSegBcInventoryTo;
import com.isa.thinair.airinventory.api.dto.RetrieveFCCSegBCInvCriteria;
import com.isa.thinair.airinventory.api.dto.RetrievePubAvailCriteria;
import com.isa.thinair.airinventory.api.dto.TempSegBcAllocDto;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.UpdateFccSegBCInvResponse;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.FCCInventory;
import com.isa.thinair.airinventory.api.model.FCCInventoryMovemant;
import com.isa.thinair.airinventory.api.model.FCCSegBCInvNesting;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.InterceptingFCCSegAlloc;
import com.isa.thinair.airinventory.api.model.PublishAvailability;
import com.isa.thinair.airinventory.api.model.TempSegBCInvClosure;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.model.TempSegBcAllocBKP;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Thejaka Inventory Master Data maintenance related Data Access APIs
 */
public interface FlightInventoryDAO {

	/**
	 * update oversell, curtailed, and infantAllocation on segment
	 * 
	 * @param fccSegInventoryId
	 * @param oversell
	 * @param curtailed
	 * @param infantAllocation
	 * @return int updated record count
	 */
	public int updateFCCSegmentInventory(int fccSegInventoryId, int oversell, int curtailed, int infantAllocation,
			int waitListAllocation, long version, boolean includeVersionCheck);

	public Collection<FCCSegInventory> getFCCSegInventories(Collection<Integer> fccsaIds);

	/**
	 * Update sold, onhold and fixed on segment from cummulative values of sold, onhold and fixed from the bc
	 * inventories
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param segmentCode
	 * @param userId
	 * @return int updated record count
	 */
	public int updateFCCSegmentInventory(int flightId, String logicalCabinClassCode, String segmentCode, String userId);

	public void updateFCCSegmentInventory(int flightId, String logicalCabinClassCode, String segmentCode, int soldInfantCount,
			int onholdInfantCount, int interceptingBookingCount);

	public void updateFCCSegmentInventory(int fccInvId, int numberOfOnholdInfantSeats, int interceptingBookingCount);

	public int[] getMaximumEffectiveReservedSetsCountInInterceptingSegments(Collection<Integer> interceptingFccsaIds);

	/**
	 * @param fccSegInvIds
	 * @param fccSegInventoryAuditDTO
	 * @return
	 */
	public int updateFCCInterceptingSegmentInvAndAVS(List<Integer> interceptingFccsaIds);

	/**
	 * @param fccSegInvIds
	 * @param fccSegInventoryAuditDTO
	 * @return
	 */
	public int updateFCCInterceptingSegmentInventories(List<Integer> interceptingFccsaIds);

	/**
	 * Update allocated, priority, status,manually closed and availability on segment booking class inventory
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param segmentCode
	 * @param bookingCode
	 * @param allocated
	 * @param status
	 * @param isPriority
	 * @return int updated record count
	 */
	public int updateFCCSegmentBCInventory(int flightId, String logicalCabinClassCode, String segmentCode, String bookingCode,
			int allocated, int allocatedWaitList, int acquiredSeats, String status, String statusChangeAction, boolean isPriority,
			long version, boolean includeVersionCheck);

	/**
	 * Update adultcapacity, infantcapacity on fccinventory and fccseginventory and available on segment
	 * 
	 * @param flightIds
	 * @param cabinClassCode
	 * @param seatCapacity
	 * @param scheduleSegmentCode
	 * @param infantCapacity
	 * @throws ModuleException
	 */
	public void updateAllocatedCapacities(Collection<Integer> flightIds, String cabinClassCode, int seatCapacity,
			int infantCapacity, String scheduleSegmentCode, boolean downgradeToAnyModel) throws ModuleException;

	/**
	 * Delete FCCInventory, FCCSegInventories and FCCSegBCInventories for the given flightIds and cabinclass If
	 * cabinclass is null, all the cabin class inventories are deleted
	 * 
	 * @param cabinClassCode
	 * @param flightIds
	 *            Collection of flightIds
	 */
	public void deleteFlightInventories(Collection<Integer> flightIds, String cabinClassCode);

	/**
	 * @param fccSegInvId
	 */
	public void deleteFlightSegmentInventories(Collection<Integer> fccSegInvIds);

	/**
	 * Check if semgent is eligible for making valid
	 * 
	 * @param flightSegId
	 * @return
	 */
	public boolean isSegmentEligibleForMakingValid(int flightSegId, List<Integer> interceptingSegInvIds);

	/**
	 * Set all the specified flight cabin class segment inventories' validFlag
	 * 
	 * @param flightSegIds
	 * @param cabinClassCode
	 * @param validFlag
	 */
	public void updateFltSegInventoriesValidFlag(Collection<Integer> flightSegIds, String cabinClassCode, String validFlag);

	/**
	 * Updates the overlapping flight id of already created flight inventory
	 * 
	 * @param flightId
	 * @param overlappingFligtId
	 */
	public void setOverlappingFlight(int flightId, int overlappingFligtId);

	/**
	 * Set overlapping flightId to NULL in all the FCCSegInventories to null
	 * 
	 * @param overlappingFlightId
	 */
	public void unsetOverlappingFlight(Collection<Integer> flightIds);

	/**
	 * Removes intercepting segments from overlapping flight, set overlapping flight id to null
	 * 
	 * @param flightIds
	 * @throws ModuleException
	 */
	public void removeInventoryOverlap(Collection<Integer> flightIds) throws ModuleException;

	public int getFCCSegBCInvetoriesCount(int flightId);

	public HashMap<Integer, ExtendedFCCSegBCInventory> getExtendedFCCSegBCInventories(int fccsInvId);

	/**
	 * Retreive summary of a collection of flights
	 * 
	 * @param flightIds
	 * @param isReporting
	 *            TODO
	 * @return
	 */
	public HashMap<Integer, FlightInventorySummaryDTO> getFlightInventoriesSummary(Collection<Integer> flightIds,
			boolean isReporting);

	public void saveFCCInventory(FCCInventory fCCInventory);

	public void saveOrUpdateFCCSegInventory(Collection<FCCSegInventory> fccSegInventories) throws ModuleException;

	public void saveFCCSegmentBCInventory(FCCSegBCInventory fccSegBCInventory);

	public void saveFCCSegmentInventory(FCCSegInventory fccSegInventory);

	public FCCSegInventory getFCCSegInventory(int flightId, String logicalCabinClassCode, String segmentCode);

	public Collection<FCCSegInventory> getFCCSegInventories(int flightId, String cabinClassCode);

	public Collection<FCCSegInventory> getFCCSegInventories(int flightId, int flightSegmentId, String cabinClassCode);

	public FCCSegBCInventory getFCCSegBCInventory(Integer fccsbInvId);

	public FCCSegBCInventory getFCCSegBCInventoryForBC(Integer fccsbInvId, String bookingCode);

	public Collection<FCCSegBCInventory> getFixedBCInventories(Integer fccsaIds);

	public HashMap<String, Object[]> getFCCSegBCInventorys(Integer fccsaInvId);

	public FCCSegBCInventory getFCCSegBCInventory(int flightId, String logicalCabinClassCode, String segmentCode,
			String bookingCode);

	public void deleteFCCSegmentBCInventory(FCCSegBCInventory fccSegBCInventory);

	public int getMinimumAvailability(List<Integer> fccSegInventoryIds);

	public void saveInterceptingFCCSegAlloc(Collection<InterceptingFCCSegAlloc> interceptingFCCSegAllocs);

	/**
	 * Retreives a list of arrays, array elements are flightId, segmentCode, segment inventory id
	 * 
	 * @param flightIds
	 * @param logicalCabinClassCode
	 * @return
	 */
	public List<Object[]> getFCCSegmentInventoriesSummary(Collection<Integer> flightIds, String logicalCabinClassCode);

	public List<Object[]> getFCCSegmentInventoriesSummary(Collection<Integer> flightIds, String cabinClassCode,
			Map<String, List<String>> segmentsAndLogicalCabinClasses);

	public List<Integer> getInterceptingFCCSegmentInventoryIds(int flightId, String logicalabinClassCode, String segmentCode,
			boolean excludeInvalidSegments);

	public List<Integer> getInterceptingFCCSegmentInventoryIds(int fccSegInventoryId, boolean excludeInvalidSegments);

	public List<Integer> getInterceptingFCCSegmentInventoryIds(Collection<Integer> fccSegInventoryIds,
			boolean excludeInvalidSegments);

	public List<Integer> getInterceptingFCCSegInventoryIds(int flightSegId, String logicalCabinClassCode,
			boolean excludeInvalidSegments);

	public List<FCCSegInventoryDTO> getFCCSegmentInventoryAndBCSummary(Collection<Integer> flightIds, String cabinClassCode,
			Collection<String> segmentCodes);

	public void deleteFCCSegmentBCInventories(int flightId, String sourceLogicalCabinClassCode, String segmentCode,
			boolean excludeGoshow);

	/**
	 * Retreives a list of arrays, array elements are segmentCode, bookingCode, allocated, priorityFlagChar, status
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param segmentCodes
	 * @param includeStandardBCs
	 * @param includeFixedBCs
	 * @param includeNonStandardBCs
	 * @return HashMap keyed by segmentCode, value = List containing Object []
	 */
	public Map<String, Map<String, List<Object[]>>> getFCCSegmentBCInventoriesSummary(int flightId, String cabinClassCode,
			HashMap<String, HashMap<String, List<String>>> segmentVsSelectedBookingClassesMap);

	// public Collection getFCCSegInventoryDTOsNew(int flightId, String cabinClassCode, Integer overlappingFlightId);

	public ExtendedFCCSegBCInventory getExtendedBCInventory(int flightSegId, String bookingCode);

	public ExtendedFCCSegBCInventory getExtendedBCInventory(int fccsbInvId);

	/**
	 * Returns the inventory details of given flights segments
	 * 
	 * @param flightIds
	 * @return
	 */
	public Collection<FlightInventorySummaryDTO> getFlightSegmentInventoriesSummary(Collection<Integer> flightIds);

	// -------------------------------------------------------------------------
	// Nesting related functionalities
	// -------------------------------------------------------------------------
	public void saveBCInvNestingInfo(Collection<FCCSegBCInvNesting> fccSegBCInvNestings);

	/**
	 * Update the last saved nesting record for the source bc inventory
	 * 
	 * @param fromFccsbInvId
	 * @param onholdSeats
	 * @param soldSeats
	 * @return
	 */
	public int updateBCInvNestingInfo(Integer fccsbInvNestingId, int onholdSeats, int soldSeats);

	public int updateBCInvNestingToNonTemporary(Integer fccsbInvNestingId);

	public int deleteBCInvNestingInfo(Integer fccsbInvNestingId);

	public Integer getBCInvNestingInfo(Integer fccsbInvNestingId);

	public Collection<FCCSegBCInvNesting> getNestedSeats(int tofccsbInvId, boolean includeSoldNestedSeats,
			boolean orderNestRankDecending);

	public int[] getNestedOutOrBlockedSeatCounts(int fromfccsbInvId);

	public boolean checkSegSoldOnholdCountValidity(int fccsInvId, int soldSeat, int onholdSeat, int soldInfantSeats,
			int onholdInfantSeats);

	// ---------------------------------------------------------------------------------------------
	// Reservations related functionalities
	// ---------------------------------------------------------------------------------------------

	public Object[] getFCCSegInventoryFlightInfo(Integer flightSegId, String logicalCCCode);

	public int reOpenFCCSegBCInventory(int fccsbInvId, TempSegBcAlloc blockedSeatsRecord);

	/**
	 * method to update the Fcc Seg Inventroy
	 */
	public int updateFccSegInventorySoldOnholdSeats(int flightId, String logicalCabinClassCode, String segmentCode, int soldSeats,
			int onholdSeats, int soldInfantSeats, int onholdInfantSeats, boolean enforceFixedSeatsCheck,
			boolean enforceAdultSeatsCheck, boolean enforceInfantSeatCheck, int releasedWLSeats);

	public int updateFccSegInventoryWaitListSeats(int flightId, String logicalCabinClassCode, String segmentCode,
			int waitListedSeats);

	// public UpdateFccSegBCInvResponse updateFccSegBCInventory(Integer fccsbInvId, int onholdSeats, int soldSeats);

	public UpdateFccSegBCInvResponse updateFccSegBCInventory(Integer fccsbInvId, int onholdSeats, int soldSeats,
			boolean checkStatus, Collection<TempSegBCInvClosure> bucketsToReopen, boolean ignoreAvailability, boolean isOverbook,
			int releasedWLSeats, String userId);

	public UpdateFccSegBCInvResponse updateFccSegBCInventoryForWaitListing(Integer fccsbInvId, int waitListedSeats,
			boolean ignoreAvailability);

	/**
	 * method to save temp seg bc alloc
	 */
	public void saveTempSegBcAlloc(TempSegBcAlloc tempSegBcAlloc);

	public void saveAllTempSegBcAlloc(Collection<TempSegBcAllocBKP> allocs);

	/**
	 * method to get TempSegBcAlloc
	 * 
	 * @param id
	 */
	public TempSegBcAlloc getTempSegBcAlloc(Integer id);

	public List<TempSegBcAlloc> getTempSegBcAllocs(List<Integer> ids);

	/**
	 * method to get fccaId
	 */
	public Integer getFccaId(int flightId, String logicalCabinClassCode, String segmentCode);

	/**
	 * Releases onhold and sold seats of a BC inventory for given cases
	 * 
	 * @param releasing
	 *            cases
	 * @param number
	 *            of released sold seats
	 * @param number
	 *            of released on-hold seats
	 * @param Segment
	 *            BC inventory that released the seats
	 * @param Segment
	 *            inventory that released the seats
	 */
	public void releaseBCInventorySeats(List<String> releaseCases, int soldSeats, int onHoldSeats, int releasedWaitListedSeats,
			ExtendedFCCSegBCInventory extendedFCCSegBCInventory);

	/**
	 * method to update fcc seg inventry for incant seats
	 */
	public void updateFccSegInvInfants(Integer id, Integer infants, boolean isOverBooking);

	/**
	 * method to remove the TempSegBcAlloc
	 * 
	 * @param alloc
	 */
	public void removeTempSegBcAlloc(TempSegBcAlloc tmpSegBcAlloc);

	/**
	 * method to release fcc seg inventry for adult seats
	 */
	public void releaseFccSegInvAdults(Integer id, Integer seats);

	/**
	 * method to release fcc seg inventry for incant seats
	 */
	public void releaseFccSegInvInfants(Integer id, Integer infants);

	/**
	 * method to find the blocked records required for clean up
	 */
	public Collection<TempSegBcAlloc> findBlockedSeatsRecsForCleanUp(Date date);

	/**
	 * Reprotect onhold and sold seats of a segment inventory for given cases
	 * 
	 * @param effected
	 *            segment inventory id
	 * @param number
	 *            of released sold seats
	 * @param number
	 *            of released on-hold seats
	 * @param effect
	 *            on the allocated seats
	 */
	public void reprotectSegInventoryInfantSeats(int fccSegInvId, int soldSeats, int onHoldSeats, int allocationChange);

	/**
	 * Reprotect onhold and sold seats of a BC inventory for given cases
	 * 
	 * @param availableSeats
	 *            TODO
	 * @param number
	 *            of released sold seats
	 * @param number
	 *            of released on-hold seats
	 * @param Segment
	 *            BC inventory that released the seats
	 * @param effect
	 *            on the allocated seats
	 */
	public void reprotectBCInventorySeats(int soldSeats, int onHoldSeats, int segBCInvId, int allocationChange,
			int availableSeats);

	public FCCSegInventory getFCCSegInventory(int flightSegId, String logicalCabinClassCode);

	public List<SegmentSeatDistsDTO> getSegmentSeatDistsDTOs(List<Integer> tempSegBcAllocIds);

	public Map<String, Integer> getFCCSegInventoryMap(int flightSegId);

	public FCCSegInventory getFCCSegInventory(Integer fccSegAllocId);

	public TransferSeatDTO getTransferSeatInfo(int flightSegId, String bookingCode);

	public FCCSegBCInventory getFCCSegBCInventory(int flightSegId, String bookingCode);

	public int updateGoshowFCCSegBCInventory(int fccsbaId, int additionalAllocation, int soldSeats);

	/** Method for manipuldating TempSegBCInvClosure */

	/**
	 * save bucket closure info
	 * 
	 * @param tempSegBCInvClosure
	 */
	public void saveTempSegBCInvClosure(TempSegBCInvClosure tempSegBCInvClosure);

	/**
	 * Retrieves bucket closure info
	 * 
	 * @param tempSegBCAllocId
	 * @return
	 */
	public Collection<TempSegBCInvClosure> getTempSegBCInvClosure(Integer tempSegBCAllocId);

	/**
	 * Deletes bucket clousre info
	 * 
	 * @param tempSegBCAllocId
	 * @return
	 */
	public int deleteTempSegBCInvClosure(Integer tempSegBCAllocId);

	// Revenue Management Module Integration Related APIs

	/**
	 * Updates Optimization Status.
	 * 
	 * @param bcInvIds
	 *            Collection<Integer>
	 * @param newOptimizationStatus
	 *            The optimization status to set
	 * 
	 * @return No of bc inventories updated
	 */
	public int updateOptimizationStatus(Collection<Integer> bcInvIds, String newOptimizationStatus);

	/**
	 * @author byorn RM getFCCSegBCInventorys
	 * @param flightId
	 * @param cabinClassCode
	 * @param segmentCode
	 * @param bookingCodes
	 * @return
	 */
	public Collection<FCCSegBCInventory> getFCCSegBCInventorys(int flightId, String cabinClassCode, String segmentCode,
			Collection<String> bookingCodes);

	/**
	 * @author byorn RM
	 * @param inventoryMovemant
	 */
	public void saveInventoryMovemant(FCCInventoryMovemant inventoryMovemant);

	/**
	 * Return all the flight segment inventories of a flight
	 * 
	 * @param flightId
	 * @return
	 */
	public Collection<FCCSegInventory> getFCCSegInventories(Integer flightId);

	/**
	 * Saves or Updates an object of PublishAvailability
	 * 
	 * @param publishAvailability
	 */
	public void saveOrUpdatePublishAvailability(PublishAvailability publishAvailability);

	public PublishAvailability getPublishAvailability(Integer publishAvailabilityId);

	public void saveOrUpdatePubAvailForUpdateCreateAndCancel(FCCSegInventory existingFccSegInventory,
			FCCSegInventory updatedFccSegInventory, FCCSegBCInventory existingFCCSegBCInventory,
			FCCSegBCInventory updatedFCCSegBCInventory, int gdsID, boolean isFreshBCInventory, boolean isDeleted);

	/**
	 * Retrieves PublishAvailability for specified criteria
	 * 
	 * @param retrievePubAvailCriteria
	 *            Criteria
	 * @return Collection of PublishAvailability
	 */
	public Collection<PublishAvailability> getPublishAvailability(RetrievePubAvailCriteria retrievePubAvailCriteria);

	/**
	 * Check if BCAllocation(s) matching specified criteria exists
	 * 
	 * @param fccSegBCAllocExistsCriteria
	 * @return
	 */
	public boolean isFCCSegBCInventoriesExists(FCCSegBCInventoriesExistsCriteria fccSegBCAllocExistsCriteria);

	/**
	 * Retrieves FCCSegBCInventories for specified criteria
	 * 
	 * @param retrieveFCCSegBCInvCriteria
	 * @return
	 */
	public Collection<FCCSegBCInventory> getFCCSegBCInventories(RetrieveFCCSegBCInvCriteria retrieveFCCSegBCInvCriteria);

	public int updateOversellCurtailFCCSegmentInventory(int fccSegInvId, int oversell, int curtailed, int waitlisting);

	public int updateDedicatedFCCSegmentBCInventory(int flightId, String logicalCabinClassCode, String segmentCode,
			String bookingCode, int allocated, int acquiredSeats, String status, String statusChangeAction, boolean isPriority,
			long version, boolean includeVersionCheck);

	/**
	 * Delete FCCSegBCInventory
	 * 
	 * @param colFCCSegBCInventory
	 */
	public void deleteFCCSegBCInventory(Collection<Integer> colFCCSegBCInventory);

	/**
	 * @param fccsaId
	 * @return
	 */
	public Collection<FCCSegBCInventory> getFCCSegBCInventories(int fccsaId);

	/**
	 * Return all the flight segment bc inventories of a flight
	 * 
	 * @param flightId
	 * @return
	 */
	public LinkedHashMap<String, List<String>> getAllocatedBookingClassesForCabinClasses(Integer flightId);

	public Integer getTotalVacantSeats(Integer flightSegId);

	public Map<String, Integer> getAvailableCabinClassesInfo();

	public List<Integer> getBlockedSeatsIds(Collection<Integer> allBlockSeatIds, List<FlightSegmentDTO> flightSegs)
			throws ModuleException;

	public List<TempSegBcAllocDto> getBlockedSeatsInfo(List<Integer> blockSeatIds);

	public Collection<PublishAvailability> getPublishAvailabiity(Integer fccSegBCinventoryID);

	public Collection<Integer> getFccsbInvIdForFlightIDs(Collection<Integer> flightIds, String logicalCabinClassCode);

	public int getNestedAvailableCount(Integer fccsbInvId);

	public void updateBCStatusForNestAvailability(Integer fccsInvId);

	public void updateBCInvetory(Collection<FCCSegBCInventory> fccSegBCInv);

	public void setupBCStatusUpdate() throws ModuleException;

	public void publishGdsFccSegBcInventory(List<GdsPublishFccSegBcInventoryTo> gdsPublishFccSegBcInventoryTos);
}