package com.isa.thinair.airinventory.core.bl;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author Dilan Anuruddha
 */
class HalfReturnFareCal extends MixFareCal {

	public HalfReturnFareCal(GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator, AvailableFlightSearchDTO avilFltDTO) {
		super(groupedOndFlightsFareCalculator, avilFltDTO, FareCalType.HRT);
		this.priority = 2;
		if ((!isSingleSectorHalfReturnAllowed(avilFltDTO) && avilFltDTO.getJourneyType() == JourneyType.SINGLE_SECTOR)
				|| avilFltDTO.getJourneyType() == JourneyType.MULTI_SECTOR) {
			this.isValid = false;
		}
		if (AppSysParamsUtil.isAllowHalfReturnFaresForOpenReturnBookings()) {
			if (avilFltDTO.isHalfReturnFareQuote() && avilFltDTO.isOpenReturnSearch()) {
				this.isValid = true;
			}
		} else {
			if (avilFltDTO.isOpenReturnSearch()) {
				this.isValid = false;
			}
		}
	}

	private boolean isSingleSectorHalfReturnAllowed(AvailableFlightSearchDTO avilFltDTO) {
		return avilFltDTO.isModifyBooking() && !AppSysParamsUtil.isRequoteEnabled();
	}

}