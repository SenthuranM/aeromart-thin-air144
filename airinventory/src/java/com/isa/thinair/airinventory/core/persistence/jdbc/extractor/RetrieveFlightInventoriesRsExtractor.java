package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;

public class RetrieveFlightInventoriesRsExtractor implements ResultSetExtractor {

	public RetrieveFlightInventoriesRsExtractor() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		LinkedHashMap<Integer, FCCInventoryDTO> fccInventoryDTOsMap = null;
		while (rs.next()) {
			if (fccInventoryDTOsMap == null) {
				fccInventoryDTOsMap = new LinkedHashMap<Integer, FCCInventoryDTO>();
			}

			Integer fccaId = new Integer(rs.getInt("fcca_id"));
			FCCInventoryDTO fccInventoryDTO = fccInventoryDTOsMap.get(fccaId);
			if (fccInventoryDTO == null || fccInventoryDTO.getFccaInvId() != fccaId.intValue()) {
				fccInventoryDTO = new FCCInventoryDTO();
				fccInventoryDTOsMap.put(fccaId, fccInventoryDTO);

				fccInventoryDTO.setFccaInvId(fccaId.intValue());
				fccInventoryDTO.setFlightId(rs.getInt("flight_id"));
				fccInventoryDTO.setOrigin(rs.getString("origin"));
				fccInventoryDTO.setDestination(rs.getString("destination"));
				fccInventoryDTO.setCabinClassCode(rs.getString("cabin_class_code"));
				fccInventoryDTO.setCapacity(rs.getInt("actual_capacity"));
				fccInventoryDTO.setInfantCapacity(rs.getInt("infant_capacity"));

				fccInventoryDTO.setFccSegInventoryDTOsMap(new LinkedHashMap<Integer, FCCSegInventoryDTO>());
			}

			Integer fccsaId = new Integer(rs.getInt("fccsa_id"));
			FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) fccInventoryDTO.getFccSegInventoryDTOsMap().get(fccsaId);

			if (fccSegInventoryDTO == null || fccSegInventoryDTO.getFccsInvId() != fccsaId.intValue()) {
				fccSegInventoryDTO = new FCCSegInventoryDTO();
				fccInventoryDTO.getFccSegInventoryDTOsMap().put(fccsaId, fccSegInventoryDTO);

				fccSegInventoryDTO.setFccsInvId(fccsaId.intValue());
				fccSegInventoryDTO.setFlightId(rs.getInt("fccsa_flight_id"));

				fccSegInventoryDTO.setLogicalCCCode(rs.getString("fccsa_logical_cc_code"));
				fccSegInventoryDTO.setLogicalCCRank(rs.getInt("logical_cc_rank"));
				fccSegInventoryDTO.setSegmentCode(rs.getString("segment_code"));
				fccSegInventoryDTO.setFlightNumber(rs.getString("flight_number"));
				fccSegInventoryDTO.setFlightSegmentStatus(rs.getString("fs_status"));

				// setting FCCSegInventory
				FCCSegInventory fccSegInventory = new FCCSegInventory();
				fccSegInventoryDTO.setFccSegInventory(fccSegInventory);

				fccSegInventory.setFccInvId(fccaId);
				fccSegInventory.setFccsInvId(fccsaId.intValue());
				fccSegInventory.setFlightId(fccSegInventoryDTO.getFlightId());
				fccSegInventory.setLogicalCCCode(fccSegInventoryDTO.getLogicalCCCode());
				fccSegInventory.setSegmentCode(fccSegInventoryDTO.getSegmentCode());
				fccSegInventory.setFlightSegId(rs.getInt("flt_seg_id"));

				fccSegInventory.setAllocatedSeats(rs.getInt("allocated_seats"));
				fccSegInventory.setOversellSeats(rs.getInt("oversell_seats"));
				fccSegInventory.setCurtailedSeats(rs.getInt("curtailed_seats"));

				fccSegInventory.setAllocatedWaitListSeats(rs.getInt("allocated_waitlist_seats"));
				fccSegInventory.setWaitListedSeats(rs.getInt("waitlisted_seats"));

				fccSegInventory.setSeatsSold(rs.getInt("sold_seats"));
				fccSegInventory.setOnHoldSeats(rs.getInt("on_hold_seats"));
				fccSegInventory.setFixedSeats(rs.getInt("fixed_seats"));
				fccSegInventory.setAvailableSeats(rs.getInt("available_seats"));

				fccSegInventory.setInfantAllocation(rs.getInt("infant_allocation"));
				fccSegInventory.setSoldInfantSeats(rs.getInt("sold_infant_seats"));
				fccSegInventory.setOnholdInfantSeats(rs.getInt("onhold_infant_seats"));
				fccSegInventory.setAvailableInfantSeats(rs.getInt("available_infant_seats"));

				Object overlappingFlightId = rs.getObject("overlapping_flight_id");
				fccSegInventory.setOverlappingFlightId(overlappingFlightId != null ? new Integer(overlappingFlightId.toString())
						: null);

				fccSegInventory.setValidFlag(rs.getString("valid_flag").equals(FCCSegInventory.VALID_FLAG_Y) ? true : false);

				fccSegInventory.setVersion(rs.getLong("version"));

				fccSegInventoryDTO.setFccSegBCInventoriesMap(new LinkedHashMap());
			}

			Object objFCCsbaId = rs.getObject("fccsba_id");
			Integer fccsbaId = null;

			ExtendedFCCSegBCInventory exFCCSegBCInventory = null;
			if (objFCCsbaId != null) {
				fccsbaId = new Integer(objFCCsbaId.toString());
				exFCCSegBCInventory = (ExtendedFCCSegBCInventory) fccSegInventoryDTO.getFccSegBCInventoriesMap().get(fccsbaId);
			}

			if (objFCCsbaId != null && exFCCSegBCInventory == null) {
				FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
				fccSegBCInventory.setFccsbInvId(fccsbaId);
				exFCCSegBCInventory = new ExtendedFCCSegBCInventory(fccSegBCInventory);
				fccSegInventoryDTO.getFccSegBCInventoriesMap().put(fccsbaId, exFCCSegBCInventory);

				// set FCCSegBCInventory
				fccSegBCInventory.setBookingCode(rs.getString("booking_code"));
				fccSegBCInventory.setfccsInvId(rs.getInt("fccsba_fccsa_id"));
				fccSegBCInventory.setFlightId(rs.getInt("fccsba_flight_id"));
				fccSegBCInventory.setLogicalCCCode(rs.getString("fccsba_logical_cc_code"));
				fccSegBCInventory.setSegmentCode(rs.getString("fccsba_segment_code"));

				fccSegBCInventory.setSeatsAllocated(rs.getInt("fccsba_allocated_seats"));
				fccSegBCInventory.setSeatsSold(rs.getInt("fccsba_sold_seats"));
				fccSegBCInventory.setOnHoldSeats(rs.getInt("fccsba_onhold_seats"));

				fccSegBCInventory.setAllocatedWaitListSeats(rs.getInt("fccsba_allocated_wl_seats"));
				fccSegBCInventory.setWaitListedSeats(rs.getInt("fccsba_wl_seats"));

				fccSegBCInventory.setSurchargeApplicability(new Boolean(rs.getString("surchargeApplicability")));

				// ------nesting info
				fccSegBCInventory.setSeatsSoldNested(rs.getInt("nested_sold_seats"));
				fccSegBCInventory.setSeatsSoldAquiredByNesting(rs.getInt("nested_aquired_sold_seats"));

				fccSegBCInventory.setSeatsOnHoldNested(rs.getInt("nested_onhold_seats"));
				fccSegBCInventory.setSeatsOnHoldAquiredByNesting(rs.getInt("nested_acquired_onhold_seats"));
				fccSegBCInventory.setSeatsAvailableNested(rs.getInt("nested_available_seats"));

				// ---------------------

				fccSegBCInventory.setSeatsCancelled(rs.getInt("cancelled_seats"));
				fccSegBCInventory.setSeatsAcquired(rs.getInt("acquired_seats"));
				fccSegBCInventory.setSeatsAvailable(rs.getInt("fccsba_available_seats"));

				fccSegBCInventory.setPriorityFlag(rs.getString("priority_flag").equals(FCCSegBCInventory.PRIORITY_FLAG_Y) ? true
						: false);
				fccSegBCInventory.setStatus(rs.getString("status"));
				fccSegBCInventory.setStatusChangeAction(rs.getString("status_chg_action"));

				fccSegBCInventory.setVersion(rs.getLong("fccsba_version"));

				// extended BC
				exFCCSegBCInventory.setFixedFlag(rs.getString("fixed_flag").equals(BookingClass.FIXED_FLAG_Y) ? true : false);
				exFCCSegBCInventory.setStandardCode(rs.getString("standard_code").equals(BookingClass.STANDARD_CODE_Y) ? true
						: false);
				exFCCSegBCInventory.setPaxType(rs.getString("pax_type"));
				Object nestRank = rs.getObject("nest_rank");
				exFCCSegBCInventory.setNestRank(nestRank != null ? new Integer(nestRank.toString()) : null);

				exFCCSegBCInventory.setLinkedEffectiveFaresMap(new LinkedHashMap());

				exFCCSegBCInventory.setLinkedEffectiveAgents(new ArrayList());
				exFCCSegBCInventory.setBcType(rs.getString("bc_type"));
				// exFCCSegBCInventory.setGdsId(rs.getInt("gds_id")); // haider 14Sep08
				exFCCSegBCInventory.setGroupId(rs.getInt("group_id"));

				exFCCSegBCInventory.setAllocationType(rs.getString("allocation_type"));
			}
		}
		if (fccInventoryDTOsMap != null) {// returning fccInventoryDTOsMap.values() causes serialization problem?
			return AirinventoryUtils.getSearializableValues(fccInventoryDTOsMap);
		}
		return null;
	}

}
