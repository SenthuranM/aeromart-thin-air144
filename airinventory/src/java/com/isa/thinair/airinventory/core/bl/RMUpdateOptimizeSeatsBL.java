package com.isa.thinair.airinventory.core.bl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.rm.InventorySeatMovementDTO;
import com.isa.thinair.airinventory.api.dto.rm.UpdateOptimizedInventoryDTO;
import com.isa.thinair.airinventory.api.model.FCCInventoryMovemant;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.InventoryRMOptimizationStatus;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BL Class Used When Updating Optimized Seat Info Sent From RM Service
 * 
 * @author byorn.
 * 
 */
public class RMUpdateOptimizeSeatsBL {

	private Log log = LogFactory.getLog(getClass());
	/** Holds the system-messages in property file **/
	public static String UNEXPECTED_ERROR = "webservices.airinventory.unexpected";

	private static interface ErrorCodes {
		public String FLIGHT_NOT_FOUND = "webservices.airinventory.flight.notfound";
		public String BC_NOT_FOUND = "webservices.airinventory.bc.notfound";
		public String SEAT_AVAIL_FAIL = "webservices.airinventory.optimize.seats.invalid";
		public String PARAM_INVALID = "webservices.airinventory.optimize.param.invalid";
		public String BC_INVENTORY_NOT_FOUND = "webservices.airinventory.bcinventory.null";
		public String BC_INVENTORY_OPT_STAT = "webservices.airinventory.bcinventory.threshstat";
		public String BC_ALLOCATION_NEG = "webservices.airinventory.bcinventory.allocation.error";
	}

	/** Set the user ID in constructor **/
	private String userID;

	/**
	 * This method is called after WS Client sends details (Above and Below Thresh data) to RM App. Until RM confirms -
	 * the status's will be updated to PEnding - this method does that! updateRMOptimizationStatus
	 * 
	 * @param fccSegBcInvId
	 * @param optimizationStatus
	 * @return
	 * @throws ModuleException
	 */

	public RMUpdateOptimizeSeatsBL(String userID) {
		if (this.userID == null) {
			this.userID = userID;
		}
	}

	/**
	 * updateOptimizedSeats
	 * 
	 * @param updateOptimizedInventoryDTO
	 * @return
	 * @throws ModuleException
	 */
	public UpdateOptimizedInventoryDTO updateOptimizedSeats(UpdateOptimizedInventoryDTO updateOptimizedInventoryDTO)
			throws ModuleException {

		log.debug("Going to Update Optizied Seats for RM");

		// request data variables
		Collection<InventorySeatMovementDTO> inventorySeatMovements = updateOptimizedInventoryDTO.getInventorySeatMovements();
		String flightNumber = updateOptimizedInventoryDTO.getFlightNumber();
		Date departureDate = updateOptimizedInventoryDTO.getDepatureDate();
		String segmentCode = updateOptimizedInventoryDTO.getSegmentCode();
		String cabinClassCode = updateOptimizedInventoryDTO.getCabinClass();
		Collection<String> bcs = updateOptimizedInventoryDTO.getBcsInSeatMovemants();

		if (inventorySeatMovements == null || inventorySeatMovements.size() == 0 || flightNumber == null || departureDate == null
				|| segmentCode == null || cabinClassCode == null) {
			throw new ModuleException(ErrorCodes.PARAM_INVALID);
		}

		// get the flight id
		Integer flightID = AirInventoryModuleUtils.getFlightBD().getFlightID(segmentCode, flightNumber, departureDate);

		if (flightID == null) {
			throw new ModuleException(ErrorCodes.FLIGHT_NOT_FOUND);
		}

		if (log.isDebugEnabled()) {
			log.debug("##########update optimized seats##################" + "\n Flt Num : " + flightNumber + "\n Dep Date :"
					+ departureDate + "\n Segment Code : " + segmentCode + "\n cabin class code :" + cabinClassCode
					+ "\n flight Id : " + flightID + "\n inventory seat movemants size : " + inventorySeatMovements.size()
					+ "\n fitered bcs size :" + bcs.size() + "\n user id :" + this.userID
					+ "\n ############################################");
		}

		// get the bc inventories for the opartion
		Collection<FCCSegBCInventory> fCCSegBCInventorys = AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegBCInventorys(flightID,
				cabinClassCode, segmentCode, bcs);

		if (fCCSegBCInventorys == null || fCCSegBCInventorys.size() == 0) {
			throw new ModuleException(ErrorCodes.BC_INVENTORY_NOT_FOUND);
		}
		// prepare a map ..key is the booking code
		HashMap<String, FCCSegBCInventory> map = prepareMap(fCCSegBCInventorys);

		// for all the seat movements for optimiztion perform the operation
		for (InventorySeatMovementDTO inventorySeatMovementDTO : inventorySeatMovements) {
			try {
				checkAndUpdate(inventorySeatMovementDTO, map);
			} catch (ModuleException e) {
				updateSourceStatus(inventorySeatMovementDTO, map);
			}
		}

		log.debug("Update Optimized Seats for RM Completed");

		return updateOptimizedInventoryDTO;

	}

	/**
	 * Prepare a Map Key: BCCode Value: FCCSegBCInventory
	 */
	private HashMap<String, FCCSegBCInventory> prepareMap(Collection<FCCSegBCInventory> fccSegBcs) {
		HashMap<String, FCCSegBCInventory> map = new HashMap<String, FCCSegBCInventory>();
		for (FCCSegBCInventory fccSegBCInventory : fccSegBcs) {
			map.put(fccSegBCInventory.getBookingCode(), fccSegBCInventory);
		}
		return map;
	}

	private void updateSourceStatus(InventorySeatMovementDTO optimizationMovemant, HashMap<String, FCCSegBCInventory> map)
			throws ModuleException {
		FCCSegBCInventory source = map.get(optimizationMovemant.getFromBookingClass());
		if (source != null) {
			FlightInventoryDAO dao = AirInventoryModuleUtils.getFlightInventoryDAO();
			source.setOptimizationStatus(InventoryRMOptimizationStatus.OPTIMISATION_FAILED);
			dao.saveFCCSegmentBCInventory(source);

		}
	}

	/**
	 * 
	 * @param inventorySeatMovementDTO
	 * @param map
	 * @return
	 * @throws ModuleException
	 */
	private void checkAndUpdate(InventorySeatMovementDTO optimizationMovemant, HashMap<String, FCCSegBCInventory> map)
			throws ModuleException {

		FCCSegBCInventory source = map.get(optimizationMovemant.getFromBookingClass());
		FCCSegBCInventory target = map.get(optimizationMovemant.getToBookingClass());

		// if either booking code was not found
		if (source == null || target == null) {
			throw new ModuleException(ErrorCodes.BC_NOT_FOUND);
		}

		// if threshold has changed
		if (!source.getThresholdStatus().equals(optimizationMovemant.getMessageType())) {

			throw new ModuleException(ErrorCodes.BC_INVENTORY_OPT_STAT);
		}

		// if allocation exceeds zero
		if (source.getSeatsAllocated() - optimizationMovemant.getNoOfSeats() < 0
				|| source.getSeatsAvailable() - optimizationMovemant.getNoOfSeats() < 0) {
			throw new ModuleException(ErrorCodes.BC_ALLOCATION_NEG);
		}

		// if source bc has exactly the mentioned amount of seats to be moved - RM Adviced.
		if (source.getSeatsAvailable() != optimizationMovemant.getNoOfSeats()) {

			throw new ModuleException(ErrorCodes.SEAT_AVAIL_FAIL);
		}

		// update the seats if all checks are a success
		optimizeSeats(source, target, optimizationMovemant);

	}

	/**
	 * Update the Source Allocation and Target Allocation
	 * 
	 * @param source
	 * @param target
	 * @param movemant
	 */
	private void optimizeSeats(FCCSegBCInventory source, FCCSegBCInventory target, InventorySeatMovementDTO movemant) {
		log.info("############ Start Seat-Optimization for RM ###########");
		FlightInventoryDAO dao = AirInventoryModuleUtils.getFlightInventoryDAO();

		if (log.isDebugEnabled()) {
			log.debug("Num of Seats: " + movemant.getNoOfSeats() + " will be moved [FROM :" + movemant.getFromBookingClass()
					+ "] -->" + "[TO: " + movemant.getToBookingClass() + "]");
			log.debug("|-- Before Updating Source [Seats Allocated  :" + source.getSeatsAllocated() + "] [Source Available :"
					+ source.getSeatsAvailable() + "]");
			log.debug("|-- Before Updating Target [Seats Allocated  :" + target.getSeatsAllocated() + "] [Target Available :"
					+ target.getSeatsAvailable() + "]");
		}

		target.setSeatsAllocated(movemant.getNoOfSeats() + target.getSeatsAllocated());
		target.setSeatsAvailable(movemant.getNoOfSeats() + target.getSeatsAvailable());

		source.setSeatsAllocated(source.getSeatsAllocated() - movemant.getNoOfSeats());
		source.setSeatsAvailable(source.getSeatsAvailable() - movemant.getNoOfSeats());
		source.setOptimizationStatus(InventoryRMOptimizationStatus.OPTIMIZED);
		if (source.getSeatsAvailable() == 0) {
			source.setStatus(FCCSegBCInventory.Status.CLOSED);
		}
		if (target.getSeatsAvailable() == 0) {
			target.setStatus(FCCSegBCInventory.Status.CLOSED);
		}

		// source.setStatus(status)

		dao.saveFCCSegmentBCInventory(source);
		dao.saveFCCSegmentBCInventory(target);
		dao.saveInventoryMovemant(getInventoryMovemant(source.getFccsbInvId(), target.getFccsbInvId(), movemant.getNoOfSeats()));

		if (log.isDebugEnabled()) {

			log.debug("|-- After Updating Source [Seats Allocated  :" + source.getSeatsAllocated() + "] Source Available :["
					+ source.getSeatsAvailable() + "]");
			log.debug("|-- After Updating Target [Seats Allocated  :" + target.getSeatsAllocated() + "] Target Available :["
					+ target.getSeatsAvailable() + "]");
		}

		log.info("############ Finished Updating Seats ############");
	}

	/**
	 * Make the Model Object to save
	 * 
	 * @param srcfccsbInvId
	 * @param trgtfccsbInvId
	 * @param noOfSeats
	 * @return FCCInventoryMovemant
	 */
	private FCCInventoryMovemant getInventoryMovemant(Integer sourceFccsbInvId, Integer targetFccsbInvId, int numSeats) {
		FCCInventoryMovemant inventoryMovemant = new FCCInventoryMovemant();
		inventoryMovemant.setFromFCCSegBCInvId(sourceFccsbInvId);
		inventoryMovemant.setToFCCSegBCInvId(targetFccsbInvId);
		inventoryMovemant.setNumSeats(numSeats);
		inventoryMovemant.setTimeStamp(new Date());
		inventoryMovemant.setUserId(this.userID);
		return inventoryMovemant;
	}

}
