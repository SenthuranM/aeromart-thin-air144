package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.FlightSegAvailableBCWithFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersSearchDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.persistence.dao.BestOffersDAOJDBC;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class BestOffersDAOJDBCImpl implements BestOffersDAOJDBC {

	public Object[] getDynamicBestOffersForSingleFlights(BestOffersSearchDTO bestOfferSearchCriteria, String ondCode,
			int recordCount) throws ModuleException {
		String ondCodes = "'" + ondCode + "'";
		String displayCntCondition = "";
		String likeOrInCondition = "IN";

		if (recordCount != -1) {
			displayCntCondition = "where rank between 1 and " + recordCount;
		}

		if (recordCount == -1) {
			likeOrInCondition = "LIKE"; // For connection onds, to compare segment codes with %
		}

		String[] args = { ondCodes, ondCodes, ondCodes, displayCntCondition, likeOrInCondition, likeOrInCondition,
				likeOrInCondition, likeOrInCondition, ondCodes, likeOrInCondition, ondCodes, likeOrInCondition, ondCodes };
		Object[] params = { new java.sql.Timestamp(bestOfferSearchCriteria.getDateFrom().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateTo().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateFrom().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateTo().getTime()) };

		String query = getQuery(args, false);
		return getDynamiCBestOffersForSingleFlightsResults(query, params);
	}

	@Override
	public Object[] getOverallDynamiCBestOffersForSingleFlights(BestOffersSearchDTO bestOfferSearchCriteria,
			Collection<String> colOndCodes, int recordCount) throws ModuleException {
		String ondCodes = Util.buildStringInClauseContent(colOndCodes);
		String likeOrInCondition = "IN";
		String displayCntCondition = "1 and ";
		String rankTop = "";
		String rankBottom = "";

		if (recordCount == 1 && colOndCodes.size() == 1) {
			displayCntCondition = displayCntCondition + "1";
		} else {
			displayCntCondition = displayCntCondition + AirinventoryUtils.getAirInventoryConfig().getBestOffersDisplayFareCount();
		}

		if (colOndCodes.size() > 1 && recordCount == 1) {
			rankTop = rankTop + "Select * from (";
			rankBottom = rankBottom + ") where rank = 1";
		}

		String[] args = { ondCodes, ondCodes, ondCodes, displayCntCondition, likeOrInCondition, likeOrInCondition,
				likeOrInCondition, likeOrInCondition, ondCodes, likeOrInCondition, ondCodes, likeOrInCondition, ondCodes,
				rankTop, rankBottom };
		Object[] params = { new java.sql.Timestamp(bestOfferSearchCriteria.getDateFrom().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateTo().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateFrom().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateTo().getTime()) };

		String query = AirinventoryUtils
				.getAirInventoryConfig()
				.getQuery(
						AppSysParamsUtil.enableModifiedQueryForBestOffers() ? AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_ENHANCED_MODIFIED
								: AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_ENHANCED, args);
		return getDynamiCBestOffersForSingleFlightsResults(query, params);
	}

	private Object[] getDynamiCBestOffersForSingleFlightsResults(String query, Object[] params) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getReportingDatasource());

		return (Object[]) jdbcTemplate.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<BestOffersDTO> colBestOffers = new ArrayList<BestOffersDTO>();

				while (rs.next()) {
					BestOffersDTO bestOffers = new BestOffersDTO();
					bestOffers.setOriginAirport(rs.getString("ORIGIN"));
					bestOffers.setDestinationAirport(rs.getString("DESTINATION"));
					bestOffers.setOriginAirportName(rs.getString("ORIGINAIRPORTNAME"));
					bestOffers.setDestinationAirportName(rs.getString("DESTINATIONAIRPORTNAME"));
					bestOffers.setSegmentCode(rs.getString("SEGMENT_CODE"));
					bestOffers.getFlightNos().add(rs.getString("FLIGHT_NUMBER"));
					bestOffers.setUniqueKey(rs.getString("FLIGHT_ID"));
					bestOffers.setDepartureDateTimeLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
					bestOffers.setArrivalDateTimeLocal(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
					bestOffers.setFareAmount(rs.getBigDecimal("FARE_AMOUNT"));
					bestOffers.setRank(rs.getInt("RANK"));
					bestOffers.getBookingCodes().add(rs.getString("BOOKING_CODE"));
					colBestOffers.add(bestOffers);
				}
				return colBestOffers.toArray();
			}

		});
	}

	public Object[] getDynamicBestOffersForConnectedFlights(BestOffersSearchDTO bestOfferSearchCriteria, String segmentCode,
			String ondCode) throws ModuleException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getReportingDatasource());

		// String[] args = {segmentCode, segmentCode, ondCode};
		Object[] params = { segmentCode, new java.sql.Timestamp(bestOfferSearchCriteria.getDateFrom().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateTo().getTime()), segmentCode, ondCode };

		String query = getQuery(null, true);

		return (Object[]) jdbcTemplate.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<FlightSegAvailableBCWithFareDTO> colFlights = new ArrayList<FlightSegAvailableBCWithFareDTO>();
				HashMap<Integer, FlightSegAvailableBCWithFareDTO> flightBCMap = new HashMap<Integer, FlightSegAvailableBCWithFareDTO>();
				FlightSegAvailableBCWithFareDTO flightWiseBC;

				while (rs.next()) {
					if (!flightBCMap.containsKey(rs.getInt("FLIGHT_ID"))) {
						flightWiseBC = new FlightSegAvailableBCWithFareDTO();
						flightWiseBC.setFlightId(rs.getInt("FLIGHT_ID"));
						flightWiseBC.setFlightNo(rs.getString("FLIGHT_NUMBER"));
						flightWiseBC.setSegmentCode(rs.getString("SEGMENT_CODE"));
						flightWiseBC.setDepartureDateTimeLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
						flightWiseBC.setArrivalDateTimeLocal(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
						flightWiseBC.addToAvailableBCsInOrder(rs.getString("BOOKING_CODE"));
						flightWiseBC.addToAvailableBCsWithFares(rs.getString("BOOKING_CODE"), rs.getBigDecimal("FARE_AMOUNT"));
						flightBCMap.put(rs.getInt("FLIGHT_ID"), flightWiseBC);
						colFlights.add(flightWiseBC);
					} else {
						flightWiseBC = flightBCMap.get(rs.getInt("FLIGHT_ID"));
						flightWiseBC.addToAvailableBCsInOrder(rs.getString("BOOKING_CODE"));
						flightWiseBC.addToAvailableBCsWithFares(rs.getString("BOOKING_CODE"), rs.getBigDecimal("FARE_AMOUNT"));
					}

				}
				return colFlights.toArray();
			}

		});
	}

	@Override
	public Object[] getLimitedDynamicBestOffersForConnectedFlights(BestOffersSearchDTO bestOfferSearchCriteria,
			String segmentCode, String ondCode, int fromRecordCount, int toRecordCount) throws ModuleException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getReportingDatasource());

		String displayCntCondition = " where rank between " + fromRecordCount + " and " + toRecordCount;

		String standard = "nest_rank,fare_amount";
		String nonStandard = "fare_amount";

		// Query for standard booking classes
		String[] args = { displayCntCondition, standard, standard };
		Object[] params = { segmentCode, new java.sql.Timestamp(bestOfferSearchCriteria.getDateFrom().getTime()),
				new java.sql.Timestamp(bestOfferSearchCriteria.getDateTo().getTime()), CommonsConstants.YES, segmentCode, ondCode };

		String queryStandard = AirinventoryUtils.getAirInventoryConfig().getQuery(
				AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS_ENHANCED, args);

		@SuppressWarnings("unchecked")
		HashMap<Integer, FlightSegAvailableBCWithFareDTO> colFlightsStandard = (HashMap<Integer, FlightSegAvailableBCWithFareDTO>) jdbcTemplate
				.query(queryStandard, params, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						HashMap<Integer, FlightSegAvailableBCWithFareDTO> flightBCMap = new HashMap<Integer, FlightSegAvailableBCWithFareDTO>();
						FlightSegAvailableBCWithFareDTO flightWiseBC;

						while (rs.next()) {
							if (!flightBCMap.containsKey(rs.getInt("FLIGHT_ID"))) {
								flightWiseBC = new FlightSegAvailableBCWithFareDTO();
								flightWiseBC.setFlightId(rs.getInt("FLIGHT_ID"));
								flightWiseBC.setFlightNo(rs.getString("FLIGHT_NUMBER"));
								flightWiseBC.setSegmentCode(rs.getString("SEGMENT_CODE"));
								flightWiseBC.setDepartureDateTimeLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
								flightWiseBC.setArrivalDateTimeLocal(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
								flightWiseBC.addToAvailableBCsInOrder(rs.getString("BOOKING_CODE"));
								flightWiseBC.addToAvailableBCsWithFares(rs.getString("BOOKING_CODE"),
										rs.getBigDecimal("FARE_AMOUNT"));
								flightBCMap.put(rs.getInt("FLIGHT_ID"), flightWiseBC);
							} else {
								flightWiseBC = flightBCMap.get(rs.getInt("FLIGHT_ID"));
								flightWiseBC.addToAvailableBCsInOrder(rs.getString("BOOKING_CODE"));
								flightWiseBC.addToAvailableBCsWithFares(rs.getString("BOOKING_CODE"),
										rs.getBigDecimal("FARE_AMOUNT"));
							}

						}
						return flightBCMap;
					}

				});

		// Non-standard
		args[1] = nonStandard;
		args[2] = nonStandard;
		params[3] = CommonsConstants.NO;

		String queryNonStandard = AirinventoryUtils.getAirInventoryConfig().getQuery(
				AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS_ENHANCED, args);

		@SuppressWarnings("unchecked")
		HashMap<Integer, FlightSegAvailableBCWithFareDTO> colFlightsNonStandard = (HashMap<Integer, FlightSegAvailableBCWithFareDTO>) jdbcTemplate
				.query(queryNonStandard, params, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						HashMap<Integer, FlightSegAvailableBCWithFareDTO> flightBCMap = new HashMap<Integer, FlightSegAvailableBCWithFareDTO>();
						FlightSegAvailableBCWithFareDTO flightWiseBC;

						while (rs.next()) {
							if (!flightBCMap.containsKey(rs.getInt("FLIGHT_ID"))) {
								flightWiseBC = new FlightSegAvailableBCWithFareDTO();
								flightWiseBC.setFlightId(rs.getInt("FLIGHT_ID"));
								flightWiseBC.setFlightNo(rs.getString("FLIGHT_NUMBER"));
								flightWiseBC.setSegmentCode(rs.getString("SEGMENT_CODE"));
								flightWiseBC.setDepartureDateTimeLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
								flightWiseBC.setArrivalDateTimeLocal(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
								flightWiseBC.addToAvailableBCsInOrder(rs.getString("BOOKING_CODE"));
								flightWiseBC.addToAvailableBCsWithFares(rs.getString("BOOKING_CODE"),
										rs.getBigDecimal("FARE_AMOUNT"));
								flightBCMap.put(rs.getInt("FLIGHT_ID"), flightWiseBC);
							} else {
								flightWiseBC = flightBCMap.get(rs.getInt("FLIGHT_ID"));
								flightWiseBC.addToAvailableBCsInOrder(rs.getString("BOOKING_CODE"));
								flightWiseBC.addToAvailableBCsWithFares(rs.getString("BOOKING_CODE"),
										rs.getBigDecimal("FARE_AMOUNT"));
							}

						}
						return flightBCMap;
					}

				});

		HashMap<Integer, FlightSegAvailableBCWithFareDTO> combinedflightBCMap = new HashMap<Integer, FlightSegAvailableBCWithFareDTO>();

		if (colFlightsStandard.size() > 0) {
			for (FlightSegAvailableBCWithFareDTO flightBC : colFlightsStandard.values()) {
				combinedflightBCMap.put(flightBC.getFlightId(), flightBC);
			}
		}

		if (colFlightsNonStandard.size() > 0) {
			for (FlightSegAvailableBCWithFareDTO flightNonBC : colFlightsNonStandard.values()) {
				if (!combinedflightBCMap.containsKey(flightNonBC.getFlightId())) {
					combinedflightBCMap.put(flightNonBC.getFlightId(), flightNonBC);
				} else {
					FlightSegAvailableBCWithFareDTO tempBC = combinedflightBCMap.get(flightNonBC.getFlightId());
					// We need to sort available BCs in order with regard to
					// available non standard fare
					List<String> standardOrderedBCList = tempBC.getAvailableBCsInOrder();
					List<String> finalBCOrder = new ArrayList<String>();

					for (String cloneBC : standardOrderedBCList) {
						finalBCOrder.add(cloneBC);
					}

					for (String bc : flightNonBC.getAvailableBCsInOrder()) {
						BigDecimal fareAmount = flightNonBC.getAvailableBCsWithFares().get(bc);
						if (fareAmount != null) {
							for (int i = 0; i < standardOrderedBCList.size(); i++) {
								String standardBc = standardOrderedBCList.get(i);
								BigDecimal fareAmountStandard = tempBC.getAvailableBCsWithFares().get(standardBc);
								if (fareAmountStandard != null) {
									if (fareAmount.compareTo(fareAmountStandard) == -1) {
										finalBCOrder.add(finalBCOrder.indexOf(standardBc), bc);
										break;
									} else if (i == (standardOrderedBCList.size() - 1)) {
										finalBCOrder.add(bc);
									}
								}

							}
						}
					}
					tempBC.setAvailableBCsInOrder(finalBCOrder);
					tempBC.getAvailableBCsWithFares().putAll(flightNonBC.getAvailableBCsWithFares());
				}
			}
		}

		return combinedflightBCMap.values().toArray();

	}

	@Override
	public List<RouteInfoTO> getAvailableRoutesForCountryOfOrigin(String countryCode, int availabilityRestrictionLevel) {
		String sql = "SELECT distinct a.route_id, a.from_airport, a.to_airport, a.ond_code, a.direct_flag, "
				+ " a.no_of_transits, b.transit_airport_code, a.search_order "
				+ " FROM t_route_info a, t_route_transits b, T_Best_Offers_Routes bor " + " WHERE bor.country_code = ? "
				+ " AND a.route_id = bor.route_id " + " AND bor.status = ? " + " AND a.route_id = b.route_id (+) "
				+ " AND a.status = ? " + " ORDER BY a.no_of_transits, a.search_order";
		DataSource ds = LookUpUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { countryCode, "ACT", "ACT" };

		final Map<Long, RouteInfoTO> routesMap = new HashMap<Long, RouteInfoTO>();
		final int fAvailabilityRestrictionLevel = availabilityRestrictionLevel;
		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					boolean isDirect = "Y".equals(rs.getString("direct_flag")) ? true : false;

					if (!((isDirect && fAvailabilityRestrictionLevel == AvailableFlightSearchDTO.CONNECTED_FLIGHTS_ONLY) || (!isDirect && fAvailabilityRestrictionLevel == AvailableFlightSearchDTO.SINGLE_FLIGHTS_ONLY))) {
						Long routeId = rs.getLong("route_id");

						RouteInfoTO routeInfoTO = null;
						if (routesMap.containsKey(routeId)) {
							routeInfoTO = routesMap.get(routeId);
						} else {
							routeInfoTO = new RouteInfoTO();
						}
						routeInfoTO.setId(routeId);
						routeInfoTO.setOndCode(rs.getString("ond_code"));
						routeInfoTO.setFromAirportCode(rs.getString("from_airport"));
						routeInfoTO.setToAirportCode(rs.getString("to_airport"));
						routeInfoTO.addTransitAirportCode(rs.getString("transit_airport_code"), true, null);
						routeInfoTO.setDirectRoute(isDirect);

						routesMap.put(routeId, routeInfoTO);
					}
				}
				return routesMap;
			}
		});

		List<RouteInfoTO> routesList = new ArrayList<RouteInfoTO>();
		if (routesMap.size() > 0) {
			for (RouteInfoTO routeInfoTO : routesMap.values()) {
				routesList.add(routeInfoTO);
			}
		}
		return routesList;
	}

	@Override
	public int checkForCachedBestOffersRequest(BestOffersSearchDTO bestOfferSearchCriteria, Integer cachingTimeInMinutes)
			throws ModuleException {

		String sql = "select best_offers_request_id from T_best_offers_request where country_code "
				+ (bestOfferSearchCriteria.getCountryCode() == null ? " is null" : "= '"
						+ bestOfferSearchCriteria.getCountryCode() + "'") + " and summary_Flag = '"
				+ (bestOfferSearchCriteria.isSummaryFlag() ? "Y" : "N") + "'" + " and total_quote_flag = '"
				+ (bestOfferSearchCriteria.isTotalQuoteFlag() ? "Y" : "N") + "'";

		if (bestOfferSearchCriteria.getOriginAirport() != null) {
			sql = sql + "	and origin = '" + bestOfferSearchCriteria.getOriginAirport() + "'";
		} else {
			sql = sql + "	and origin is null";
		}

		if (bestOfferSearchCriteria.getDestinationAirport() != null) {
			sql = sql + " and destination = '" + bestOfferSearchCriteria.getDestinationAirport() + "'";
		} else {
			sql = sql + " and destination is null";
		}

		if (bestOfferSearchCriteria.getMonthOfTravel() != null) {
			sql = sql + " and month_of_travel = " + bestOfferSearchCriteria.getMonthOfTravel();
		} else {
			sql = sql + " and month_of_travel is null";
		}
		sql = sql
				+ " and to_timestamp(to_char(?,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') between last_request_time And (last_request_time + ? / 1440)";

		DataSource ds = LookUpUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { new java.sql.Timestamp(System.currentTimeMillis()), cachingTimeInMinutes };

		Integer recordId = (Integer) templete.query(sql, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int recordId = -1;
				if (rs.next()) {
					recordId = rs.getInt("BEST_OFFERS_REQUEST_ID");
				}
				return recordId;
			}
		});

		return recordId;
	}

	@Override
	public int getNextBestOffersRequestId() throws ModuleException {

		String sql = "select S_BEST_OFFERS_REQUEST.nextval from dual";
		DataSource ds = LookUpUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Integer nextVal = templete.queryForObject(sql, Integer.class);

		return nextVal;
	}

	private String getQuery(String[] args, boolean blnConnection) {
		if (blnConnection) {
			return AirinventoryUtils.getAirInventoryConfig().getQuery(
					AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_CONNECTION_FLIGHTS, args);
		} else {
			if (AppSysParamsUtil.enableModifiedQueryForBestOffers()) {
				return AirinventoryUtils.getAirInventoryConfig().getQuery(
						AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS_MODIFIED, args);
			} else {
				return AirinventoryUtils.getAirInventoryConfig().getQuery(
						AirinventoryCustomConstants.DYNAMIC_BEST_OFFERS_SINGLE_FLIGHTS, args);
			}
		}

	}

	private DataSource getDataSource() {
		return ((AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig()).getDataSource();
	}
	
	private DataSource getActiveActiveDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.CLUSTER_DATA_SOURCE_BEAN);
	}
	
	private DataSource getReportingDatasource() {
		if(AppSysParamsUtil.isActiveActiveEnabled()) {
			LookupService lookup = LookupServiceFactory.getInstance();
			return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.REPORT_DATA_SOURCE_BEAN);
		}else {
			return getDataSource();
		}
		
	}

}
