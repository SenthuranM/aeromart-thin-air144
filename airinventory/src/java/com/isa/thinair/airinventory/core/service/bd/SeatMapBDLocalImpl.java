/**
 * 
 */
package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.SeatMapBD;

/**
 * @author indika
 */
@Local
public interface SeatMapBDLocalImpl extends SeatMapBD {

}
