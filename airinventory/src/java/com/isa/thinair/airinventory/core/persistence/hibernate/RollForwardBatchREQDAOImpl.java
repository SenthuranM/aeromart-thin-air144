/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.List;

import org.hibernate.Query;

import com.isa.thinair.airinventory.api.model.RollForwardBatchREQ;
import com.isa.thinair.airinventory.core.persistence.dao.RollForwardBatchREQDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * BHive: RollForward module
 * 
 * @author Raghuraman Kumar
 * 
 * 
 *         RollForwardBatchREQDAOImpl is the RollForwardBatchREQDAO implementation for hibernate
 * 
 * @isa.module.dao-impl dao-name="RollForwardBatchREQDAO"
 */
public class RollForwardBatchREQDAOImpl extends PlatformBaseHibernateDaoSupport implements RollForwardBatchREQDAO {

	/** get list of RollForwardBatchREQ **/
	@Override
	public List<RollForwardBatchREQ> getRollForwardBatchREQs() {

		return find("from RollForwardBatchREQ", RollForwardBatchREQ.class);

	}

	/** get RollForwardBatchREQ for batchId **/
	@Override
	public RollForwardBatchREQ getRollForwardBatchREQ(int batchId) {
		return (RollForwardBatchREQ) get(RollForwardBatchREQ.class, new Integer(batchId));

	}

	/**
	 * save RollForwardBatchREQ
	 * 
	 * @return
	 * @return
	 **/
	@Override
	public Integer saveRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) {
		hibernateSaveOrUpdate(rollForwardBatchREQ);
		return rollForwardBatchREQ.getBatchId();
	}

	/** remove RollForwardBatchREQ **/
	@Override
	public void removeRollForwardBatchREQ(int batchId) {
		Object rollForwardBatchREQ = load(RollForwardBatchREQ.class, new Integer(batchId));

		delete(rollForwardBatchREQ);
	}

	/** remove RollForwardBatchREQ **/
	@Override
	public void removeRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) {

		delete(rollForwardBatchREQ);
	}

	@Override
	public void updateRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException {
		hibernateSaveOrUpdate(rollForwardBatchREQ);
	}

	@Override
	public void updateRollForwardBatchREQStatus(Integer batchId, String status) {
		String sql = "UPDATE RollForwardBatchREQ SET status='" + status + "' where batchId=" + batchId;
		Query query = getSession().createQuery(sql);

		int results = query.executeUpdate();

	}

}
