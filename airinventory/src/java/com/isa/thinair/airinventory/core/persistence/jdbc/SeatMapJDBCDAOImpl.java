/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapJDBCDAO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author indika
 * 
 */
public class SeatMapJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements SeatMapJDBCDAO {

	@Override
	@SuppressWarnings("unchecked")
	public FlightSeatsDTO getFlightSeats(int flightSegID, String localeCode, String cabinClass, String logicalCabinClass, boolean loadSoicalData)
			throws ModuleException {

		final boolean loadSoicalIdentity = loadSoicalData;
		FlightSeatsDTO flightSeatsDTO = new FlightSeatsDTO();
		flightSeatsDTO.setFlightSegmentID(flightSegID);
		// flightSeatsDTO.setFlightId(0);
		if (StringUtils.isBlank(localeCode)) {
			localeCode = "en";
		} else {
			localeCode = localeCode.trim();
		}

		StringBuilder sb = new StringBuilder();
		sb.append("   SELECT a.flight_am_seat_id,   ");
		sb.append("    a.flt_seg_id             ,   ");
		sb.append("    d.segment_code           ,   ");
		sb.append("    a.ams_charge_id          ,   ");
		sb.append("    (   ");
		sb.append("    CASE   ");
		sb.append("      WHEN lcc.free_seat_enabled='Y'   ");
		sb.append("      THEN 0   ");
		sb.append("      ELSE b.charge_amount   ");
		sb.append("    END) charge_amount        ,   ");
		sb.append("    a.status                  ,   ");
		sb.append("    b.am_seat_id              ,   ");
		sb.append("    b.amc_template_id         ,   ");
		sb.append("    c.seat_code               ,   ");
		sb.append("    c.seat_type               ,   ");
		sb.append("    c.seat_location_type      ,   ");
		sb.append("    c.row_id                  ,   ");
		sb.append("    c.col_id                  ,   ");
		sb.append("    c.row_grp_id              ,   ");
		sb.append("    c.col_grp_id              ,   ");
		sb.append("    c.cabin_class_code        ,   ");
		sb.append("    a.logical_cabin_class_code,   ");
		sb.append("    c.seat_desc               ,   ");
		sb.append("    a.fccsa_id                ,   ");
		sb.append("    a.pax_type                ,   ");
		sb.append("    msg.message_content       ,   ");

		if (loadSoicalIdentity) {
			sb.append("    e.customer_id             ,     ");
			sb.append("    g.social_site_cust_id     ,   ");
			sb.append("    g.social_customer_type_id ,   ");
			sb.append("    initcap(h.title)||' '||initcap(h.first_name) ||' '||initcap(h.last_name) as pax_name ,   ");
		}

		sb.append("    c.seat_visibility   ");
		sb.append("     FROM sm_t_flight_am_seat a   ");

		if (loadSoicalIdentity) {
			sb.append("       LEFT OUTER JOIN sm_t_pnr_pax_seg_am_seat e    ");
			sb.append("          ON (a.flight_am_seat_id        = e.flight_am_seat_id)   ");
			sb.append("       LEFT OUTER JOIN t_customer f              ");
			sb.append("         ON  (e.customer_id              = f.customer_id)   ");
			sb.append("       LEFT OUTER JOIN  t_social_customer g            ");
			sb.append("         ON (f.customer_id  = g.customer_id )   ");
			sb.append("       LEFT OUTER JOIN t_pnr_passenger h   ");
			sb.append("         ON (e.pnr_pax_id    = h.pnr_pax_id),   ");
		} else {
			sb.append(" , ");
		}

		sb.append("     sm_t_am_seat_charge b      ,   ");
		sb.append("    t_flight_segment d         ,   ");
		sb.append("    t_logical_cabin_class lcc  ,   ");
		sb.append("    sm_t_aircraft_model_seats c   ");
		sb.append("  LEFT OUTER JOIN t_i18n_message msg   ");
		sb.append("       ON (c.i18n_message_key    = msg.i18n_message_key   ");
		sb.append("  AND msg.message_locale         ='");
		sb.append(localeCode);
		sb.append("')   ");
		sb.append("    WHERE a.ams_charge_id        = b.ams_charge_id   ");
		sb.append("  AND b.am_seat_id               = c.am_seat_id   ");
		sb.append("  AND a.flt_seg_id               = d.flt_seg_id   ");
		sb.append("  AND a.flt_seg_id               = ").append(flightSegID);

		if (logicalCabinClass != null && !logicalCabinClass.isEmpty()) {
			sb.append(" AND a.logical_cabin_class_code ='" + logicalCabinClass + "' ");
		} else if (cabinClass != null && !cabinClass.isEmpty()) {
			sb.append(" AND c.cabin_class_code ='" + cabinClass + "' ");
		}

		sb.append("  AND a.logical_cabin_class_code = lcc.logical_cabin_class_code   ");
		sb.append("  ORDER BY c.cabin_class_code,   ");
		sb.append("    c.col_grp_id             ,   ");
		sb.append("    c.row_grp_id             ,   ");
		sb.append("    c.row_id                 ,   ");
		sb.append("    c.col_id ");

		String query = sb.toString();
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Object o[] = (Object[]) template.query(query, new ResultSetExtractor() {
			boolean hasVacantSeat = false;

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				LinkedHashMap<Integer, SeatDTO> seatHolder = new LinkedHashMap<Integer, SeatDTO>();

				while (rs.next()) {

					Integer flightAmSeatId = rs.getInt("flight_am_seat_id");
					if (loadSoicalIdentity) {
						String socialCustomerType = rs.getString("social_customer_type_id");
						String socialSiteCustId = rs.getString("social_site_cust_id");
						if (seatHolder.containsKey(flightAmSeatId)) {
							SeatDTO countedSeat = seatHolder.get(flightAmSeatId);
							if (socialCustomerType != null && socialSiteCustId != null) {
								countedSeat.addToSocialIdentity(socialCustomerType, socialSiteCustId);
							}
							continue;
						}
					}

					SeatDTO seatDTO = new SeatDTO();
					seatDTO.setFlightAmSeatID(flightAmSeatId);
					seatDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					seatDTO.setChargeID(rs.getInt("ams_charge_id"));
					seatDTO.setChargeAmount(rs.getBigDecimal("charge_amount"));
					seatDTO.setStatus(rs.getString("status"));
					seatDTO.setSeatType(rs.getString("seat_type"));
					seatDTO.setSeatLocationType(rs.getString("seat_location_type"));
					if (!hasVacantSeat) {
						if (seatDTO.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {
							hasVacantSeat = true;
						}
					}
					seatDTO.setSeatID(rs.getInt("am_seat_id"));
					seatDTO.setTemplateId(rs.getInt("amc_template_id"));
					seatDTO.setSeatCode(rs.getString("seat_code"));
					seatDTO.setRowId(rs.getInt("row_id"));
					seatDTO.setColId(rs.getInt("col_id"));
					seatDTO.setRowGroupId(rs.getInt("row_grp_id"));
					seatDTO.setColGroupId(rs.getInt("col_grp_id"));
					seatDTO.setCabinClassCode(rs.getString("cabin_class_code"));
					seatDTO.setLogicalCabinClassCode(rs.getString("logical_cabin_class_code"));
					seatDTO.setPaxType(rs.getString("pax_type"));
					seatDTO.setSeatMessage(getSeatDescription(rs.getString("seat_desc"), rs.getString("message_content")));
					seatDTO.setSeatVisibility(rs.getString("seat_visibility"));

					if (loadSoicalIdentity) {
						String socialCustomerType = rs.getString("social_customer_type_id");
						String socialSiteCustId = rs.getString("social_site_cust_id");
						if (socialCustomerType != null && socialSiteCustId != null) {
							seatDTO.addToSocialIdentity(socialCustomerType, socialSiteCustId);
						}
						seatDTO.setName(rs.getString("pax_name"));
					}
					
					seatHolder.put(flightAmSeatId, seatDTO);
				}
				return new Object[] { new ArrayList<SeatDTO>(seatHolder.values()), new Boolean(hasVacantSeat) };
			}
		});

		flightSeatsDTO.setSeats((Collection<SeatDTO>) o[0]);
		boolean isFlightFull = ((Boolean) o[1]).booleanValue() == true ? false : true;
		flightSeatsDTO.setFlightFull(isFlightFull);
		return flightSeatsDTO;
	}
	

	@Override
	@SuppressWarnings("unchecked")
	public FlightSeatsDTO getFlightSeatsRow(int flightSegID, String localeCode, int rowid) throws ModuleException {

		FlightSeatsDTO flightSeatsDTO = new FlightSeatsDTO();
		flightSeatsDTO.setFlightSegmentID(flightSegID);
		// flightSeatsDTO.setFlightId(0);
		if (StringUtils.isBlank(localeCode)) {
			localeCode = "en";
		} else {
			localeCode = localeCode.trim();
		}

		String query = "SELECT a.status,c.row_grp_id,a.pax_type "
				+ " FROM sm_t_flight_am_seat a, sm_t_am_seat_charge b, sm_t_aircraft_model_seats c "
				+ " WHERE a.ams_charge_id = b.ams_charge_id " + " AND b.am_seat_id = c.am_seat_id AND a.flt_seg_id = "
				+ flightSegID + " AND c.col_id = " + rowid
				+ " order by c.cabin_class_code,c.col_grp_id,c.row_grp_id,c.row_id,c.col_id";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Object o[] = (Object[]) template.query(query, new ResultSetExtractor() {
			boolean hasVacantSeat = false;
			Collection<SeatDTO> colSeats = new ArrayList<SeatDTO>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					SeatDTO seatDTO = new SeatDTO();
					seatDTO.setStatus(rs.getString("status"));
					if (!hasVacantSeat) {
						if (seatDTO.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.VACANT)) {
							hasVacantSeat = true;
						}
					}
					seatDTO.setRowGroupId(rs.getInt("row_grp_id"));
					seatDTO.setPaxType(rs.getString("pax_type"));
					colSeats.add(seatDTO);
				}
				return new Object[] { colSeats, new Boolean(hasVacantSeat) };

			}
		});

		flightSeatsDTO.setSeats((Collection<SeatDTO>) o[0]);
		boolean isFlightFull = ((Boolean) o[1]).booleanValue() == true ? false : true;
		flightSeatsDTO.setFlightFull(isFlightFull);
		return flightSeatsDTO;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<FlightSeatsDTO> getTempleateIds(int flightId) throws ModuleException {

		String sql = "SELECT distinct(c.amc_template_id), a.flt_seg_id, a.segment_code"
				+ " FROM t_flight_segment a, sm_t_flight_am_seat b, sm_t_am_seat_charge c WHERE a.flight_id=" + flightId
				+ "  AND a.flt_seg_id = b.flt_seg_id (+) AND b.ams_charge_id = c.ams_charge_id (+) AND b.status (+) not in ('RES') order by flt_seg_id";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Collection<FlightSeatsDTO>) template.query(sql, new ResultSetExtractor() {
			Collection<FlightSeatsDTO> colTemplateIds = new ArrayList<FlightSeatsDTO>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					FlightSeatsDTO flightSeatDTO = new FlightSeatsDTO();
					flightSeatDTO.setTemplateId(rs.getInt("AMC_TEMPLATE_ID"));
					flightSeatDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					flightSeatDTO.setSegmentCode(rs.getString("segment_code"));
					colTemplateIds.add(flightSeatDTO);
				}
				return colTemplateIds;
			}
		});
	}

	@Override
	public Integer getTempleateId(int flightSegId) throws ModuleException {

		String sql = "SELECT distinct(c.amc_template_id)" + " FROM sm_t_flight_am_seat b, sm_t_am_seat_charge c"
				+ " WHERE b.flt_seg_id =" + flightSegId + " AND b.ams_charge_id = c.ams_charge_id (+) ";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Integer) template.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer templateId = null;
				while (rs.next()) {
					templateId = rs.getInt("AMC_TEMPLATE_ID");
				}
				return templateId;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<SeatDTO> getFlightSeats(int flightSegId, Collection<String> seatCodes) throws ModuleException {
		String sql = "SELECT flight_seat.flight_am_seat_id, aircraft_seat.seat_code FROM sm_t_flight_am_seat flight_seat, sm_t_aircraft_model_seats aircraft_seat "
				+ "WHERE flight_seat.flt_seg_id="
				+ flightSegId
				+ " AND aircraft_seat.seat_code in ("
				+ Util.buildStringInClauseContent(seatCodes) + ") " + " AND aircraft_seat.am_seat_id = flight_seat.am_seat_id";

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection<SeatDTO> flightSeats = (Collection<SeatDTO>) template.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<SeatDTO> flightSeats = new ArrayList<SeatDTO>();
				while (rs.next()) {
					SeatDTO seat = new SeatDTO();
					seat.setFlightAmSeatID(new Integer(rs.getInt("flight_am_seat_id")));
					seat.setSeatCode(rs.getString("seat_code"));
					flightSeats.add(seat);
				}
				return flightSeats;
			}
		});
		return flightSeats;
	}

	@Override
	@SuppressWarnings("unchecked")
	public FlightSeatsDTO getFlightSeatsForCheck(Collection<Integer> flightAmSeatIds) throws ModuleException {

		FlightSeatsDTO flightSeatsDTO = new FlightSeatsDTO();

		String query = "SELECT a.flight_am_seat_id, a.flt_seg_id,c.seat_type, c.seat_location_type, c.col_id,c.row_grp_id,a.pax_type"
				+ " FROM sm_t_flight_am_seat a, sm_t_am_seat_charge b, sm_t_aircraft_model_seats c, t_flight_segment d "
				+ " WHERE a.ams_charge_id = b.ams_charge_id "
				+ " AND b.am_seat_id = c.am_seat_id AND a.flt_seg_id = d.flt_seg_id"
				+ " AND d.est_time_departure_zulu >= sysdate " + " AND a.flight_am_seat_id in ("
				+ Util.constructINStringForInts(flightAmSeatIds) + ")"
				+ " order by c.cabin_class_code,c.col_grp_id,c.row_grp_id,c.row_id,c.col_id";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Collection<SeatDTO> colSeats = (Collection<SeatDTO>) template.query(query, new ResultSetExtractor() {

			Collection<SeatDTO> colSeats = new ArrayList<SeatDTO>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					SeatDTO seatDTO = new SeatDTO();
					seatDTO.setFlightAmSeatID(rs.getInt("flight_am_seat_id"));
					seatDTO.setFlightSegmentID(rs.getInt("flt_seg_id"));
					seatDTO.setSeatType(rs.getString("seat_type"));
					seatDTO.setSeatLocationType(rs.getString("seat_location_type"));
					seatDTO.setColId(rs.getInt("col_id"));
					seatDTO.setRowGroupId(rs.getInt("row_grp_id"));
					seatDTO.setPaxType(rs.getString("pax_type"));
					colSeats.add(seatDTO);
				}
				return colSeats;

			}
		});

		flightSeatsDTO.setSeats(colSeats);
		return flightSeatsDTO;
	}

	@Override
	public boolean hasParentInRowGroupAndColumn(int rowGroup, int colGroup, Integer flightSegId, int flightAmSeatID,
			Collection<Integer> excludeCheckSeatIDs)
			throws ModuleException {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT count(c.am_seat_id) as cnt");
		sql.append(" FROM sm_t_flight_am_seat a, sm_t_am_seat_charge b, sm_t_aircraft_model_seats c, t_flight_segment d");
		sql.append(" WHERE a.ams_charge_id = b.ams_charge_id");
		sql.append(" AND b.am_seat_id = c.am_seat_id AND a.flt_seg_id = d.flt_seg_id ");
		sql.append(" AND a.flt_seg_id = " + flightSegId);
		sql.append(" and c.row_grp_id=" + rowGroup + " and c.col_id=" + colGroup);
		sql.append(" and a.pax_type='" + ReservationInternalConstants.PassengerType.PARENT + "'");
		sql.append(" and a.flight_am_seat_id not in ( " + flightAmSeatID);

		if (excludeCheckSeatIDs != null && !excludeCheckSeatIDs.isEmpty()) {
			sql.append(", ");
			Iterator<Integer> iterator = excludeCheckSeatIDs.iterator();
			while (iterator.hasNext()) {
				Integer seatID = iterator.next();
				if (seatID != null) {
					sql.append(seatID);
				}
				if (iterator.hasNext()) {
					sql.append(", ");
				}
			}
		}

		sql.append(" ) ");

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Integer recs = (Integer) template.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});

		if (recs == null || recs.intValue() == 0) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * Method Will Return collection of FlightSeatsDTO which matched with given flight segment id and seatcodes
	 */
	@Override
	@SuppressWarnings("unchecked")
	public FlightSeatsDTO getFlightSeatsWithCodes(int flightSegID, Collection<String> SeatCodeCollection) throws ModuleException {

		FlightSeatsDTO flightSeatsDTO = new FlightSeatsDTO();
		flightSeatsDTO.setFlightSegmentID(flightSegID);
		// flightSeatsDTO.setFlightId(0);

		String query = "SELECT a.flight_am_seat_id,a.status,c.seat_code "
				+ " FROM sm_t_flight_am_seat a, sm_t_am_seat_charge b, sm_t_aircraft_model_seats c "
				+ " WHERE a.ams_charge_id = b.ams_charge_id " + " AND b.am_seat_id = c.am_seat_id " + " AND c.seat_code IN ("
				+ Util.buildStringInClauseContent(SeatCodeCollection) + ")" + " AND a.flt_seg_id = " + flightSegID
				+ " order by c.cabin_class_code,c.col_grp_id,c.row_grp_id,c.row_id,c.col_id";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Object o[] = (Object[]) template.query(query, new ResultSetExtractor() {

			boolean hasVacantSeat = false;

			Collection<SeatDTO> colSeats = new ArrayList<SeatDTO>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					SeatDTO seatDTO = new SeatDTO();
					seatDTO.setFlightAmSeatID(rs.getInt("flight_am_seat_id"));
					seatDTO.setStatus(rs.getString("status"));
					seatDTO.setSeatCode(rs.getString("seat_code"));
					colSeats.add(seatDTO);
				}
				return new Object[] { colSeats, new Boolean(hasVacantSeat) };
			}
		});

		flightSeatsDTO.setSeats((Collection<SeatDTO>) o[0]);
		boolean isFlightFull = ((Boolean) o[1]).booleanValue() == true ? false : true;
		flightSeatsDTO.setFlightFull(isFlightFull);

		return flightSeatsDTO;
	}

	// returns the seat description for a particular seat.
	private String getSeatDescription(String defaultDescriptionContent, String translatedContent) {
		String seatDescription = null;
		// if there's translated content we must use it for seat description.
		if (!StringUtils.isBlank(translatedContent)) {
			seatDescription = StringUtil.getUnicode(translatedContent.trim());
		} else if (!StringUtils.isBlank(defaultDescriptionContent)) {
			seatDescription = defaultDescriptionContent;
		}
		return seatDescription;
	}

	private String composeName(String paxTitle, String paxFirstName, String paxLastName) {
		String title = toInitCap(paxTitle);
		String firstName = toInitCap(paxFirstName);
		String lastName = toInitCap(paxLastName);

		StringBuilder name = new StringBuilder();

		if (title.length() > 0) {
			name.append(title).append(" ");
		}

		if (firstName.length() > 0) {
			name.append(firstName).append(" ");
		}

		if (lastName.length() > 0) {
			name.append(lastName).append(" ");
		}

		return name.toString().trim();
	}

	private String toInitCap(String str) {
		if (str == null) {
			return "";
		} else {
			str = str.trim();
		}
		str = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
		return str;
	}

	public Map<String, String> getSeatStatus(Integer flightSegID, String localeCode) throws ModuleException {

		if (StringUtils.isBlank(localeCode)) {
			localeCode = "en";
		} else {
			localeCode = localeCode.trim();
		}

		String query = "SELECT a.status,c.seat_code"
				+ " FROM sm_t_flight_am_seat a, sm_t_am_seat_charge b, t_flight_segment d, t_logical_cabin_class lcc, "
				+ " sm_t_aircraft_model_seats c LEFT OUTER JOIN t_i18n_message msg "
				+ " ON (c.i18n_message_key = msg.i18n_message_key AND msg.message_locale='" + localeCode + "') "
				+ " WHERE a.ams_charge_id = b.ams_charge_id AND b.am_seat_id = c.am_seat_id AND a.flt_seg_id = d.flt_seg_id"
				+ " AND a.flt_seg_id = " + flightSegID;

		query += " AND a.logical_cabin_class_code = lcc.logical_cabin_class_code "
				+ " ORDER BY c.cabin_class_code,c.col_grp_id,c.row_grp_id,c.row_id,c.col_id";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Map<String, String> seatStatusMap = (Map<String, String>) template.query(query, new ResultSetExtractor() {
			boolean hasVacantSeat = false;
			Map<String, String> seatStatusMap = new HashMap<String, String>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					seatStatusMap.put(rs.getString("seat_code"), rs.getString("status"));

				}
				return seatStatusMap;

			}
		});

		return seatStatusMap;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<SeatDTO> getFlightSeatsByChargeTemplate(int chargeTemplateID) throws ModuleException {

		String query = "SELECT MS.AM_SEAT_ID, MS.SEAT_CODE, SC.CHARGE_AMOUNT,CT.DEFAULT_CHARGE_AMOUNT, SC.STATUS "
				+ "FROM SM_T_AIRCRAFT_MODEL_SEATS MS INNER JOIN SM_T_AM_SEAT_CHARGE SC ON MS.AM_SEAT_ID=SC.AM_SEAT_ID "
				+ "INNER JOIN SM_T_AM_CHARGE_TEMPLATE CT ON SC.AMC_TEMPLATE_ID=CT.AMC_TEMPLATE_ID WHERE CT.AMC_TEMPLATE_ID=?";

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		Collection<SeatDTO> seats = (Collection<SeatDTO>) template.query(query, new Object[] { chargeTemplateID },
				new ResultSetExtractor() {
					Collection<SeatDTO> colSeats = new ArrayList<SeatDTO>();

					@Override
					public Object extractData(final ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							SeatDTO seatDTO = new SeatDTO();

							seatDTO.setChargeAmount(rs.getBigDecimal("CHARGE_AMOUNT"));

							seatDTO.setSeatID(rs.getInt("AM_SEAT_ID"));

							seatDTO.setSeatCode(rs.getString("SEAT_CODE"));

							seatDTO.setStatus(rs.getString("STATUS"));
							colSeats.add(seatDTO);
						}
						return colSeats;

					}
				});

		return seats;
	}
}
