package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryRMTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;

/**
 * RM related data retrievals.
 * 
 * @author Nasly
 */
public interface FlightInventoryRMJDBCDAO {

	/**
	 * Returns BC inventory details for RM optimization.
	 * 
	 * @param segInvIds
	 *            Collection<Integer>
	 * @return Map<Integer, Collection<FCCSegBCInventoryTO>>
	 */
	public Map<Integer, Collection<FCCSegBCInventoryTO>> getInvenotriesForRMAlert(Collection<Integer> segInvIds);

	/**
	 * Returns flight details for RM optimization.
	 * 
	 * @return Map<Integer,FCCSegBCInventoryRMTO>
	 */
	public Map<Integer, FCCSegBCInventoryRMTO> getFlightsForRMAlert();

}
