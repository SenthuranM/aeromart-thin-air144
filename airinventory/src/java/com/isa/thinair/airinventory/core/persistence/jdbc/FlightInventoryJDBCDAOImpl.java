package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryCollection;
import com.isa.thinair.airinventory.api.dto.FCCSegONDCodesAndBookingClassesDTO;
import com.isa.thinair.airinventory.api.dto.FLCCAllocationDTO;
import com.isa.thinair.airinventory.api.dto.FlightSummaryDTO;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.RetriveFlightInventoryCritera;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealClassDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.AvsGenerationFlow;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.RetrieveAvailabilityForPublishRsExtractor;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.RetrieveBucketsRsExtractor;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.RetrieveFlightInventoriesRsExtractor;
import com.isa.thinair.airinventory.core.persistence.jdbc.ps.RetriveBucketsPrepStmtCreator;
import com.isa.thinair.airinventory.core.persistence.jdbc.ps.RetriveFlightInventoriesPrepStmtCreator;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airreservation.api.dto.FlightChangeInfo;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;
import com.isa.thinair.scheduler.core.config.SchedulerConfig;

/**
 * @author Thejaka
 */
public class FlightInventoryJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements FlightInventoryJDBCDAO {
	// Query Keys

	private static final org.apache.commons.logging.Log log = LogFactory.getLog(FlightInventoryJDBCDAOImpl.class);

	private static final String FIXED_BC_ALLOCATIONS_TO_RELEASE = "FIXED_BC_ALLOCATIONS_TO_RELEASE";
	private static final String RETRIEVE_FCC_SEG_INVENTORIES = "RETRIEVE_FCC_SEG_INVENTORIES";
	private static final String RETRIEVE_FCC_SEG_BC_INVENTORIES = "RETRIEVE_FCC_SEG_BC_INVENTORIES";
	private static final String RETRIEVE_BUCKETS = "RETRIEVE_BUCKETS";
	private static final String RETRIEVE_AVAILABILITY_FOR_PUBLISHING = "RETRIEVE_AVAILABILITY_FOR_PUBLISHING";
	private static final String RETRIEVE_AVAILABILITY_FOR_BC_STATUS_UPDATE = "RETRIEVE_AVAILABILITY_FOR_BC_STATUS_UPDATE";
	private static final String CABIN_CLASS_AVAILABILITY = "CABIN_CLASS_AVAILABILITY";
	private static final String FLIGHT_SEGMENT_DETAIL_WITH_CABIN_CLASS = "FLIGHT_SEGMENT_DETAIL_WITH_CABIN_CLASS";
	private static final String RETRIEVE_FCC_SEG_LCC_INVENTORY_SUMMARY = "RETRIEVE_FCC_SEG_LCC_INVENTORY_SUMMARY";
	private static final String EFFECTIVE_SOLD_ONHOLD_SEATS_IN_INTCEPTING_SEGMENTS = "EFFECTIVE_SOLD_ONHOLD_SEATS_IN_INTCEPTING_SEGMENTS";
	private static final String TOTAL_SOLD_SEAT_COUNT_FOR_CABIN_CLASS = "TOTAL_SOLD_SEAT_COUNT_FOR_CABIN_CLASS";
	private static final String FLIGHT_DETAILS_FOR_FLIGHT_ID = "FLIGHT_DETAILS_FOR_FLIGHT_ID";
	private static final String AVAILABLE_FIXED_SEAT_COUNT = "AVAILABLE_FIXED_SEAT_COUNT";
	private static final String TOATL_SOLD_SEAT_COUNT_LOGICAL_CABIN_CLASS_WISE = "TOATL_SOLD_SEAT_COUNT_LOGICAL_CABIN_CLASS_WISE";
	private static final String SOLD_SEAT_COUNT_FOR_SINGLE_LEG = "SOLD_SEAT_COUNT_FOR_SINGLE_LEG";
	private static final String AVAILABLE_EFFECTIVE_FIXED_SEAT_COUNT = "AVAILABLE_EFFECTIVE_FIXED_SEAT_COUNT";
	private static final String AVAILABLE_EFFECTIVE_FIXED_SEAT_COUNT_FOR_FLIGHT = "AVAILABLE_EFFECTIVE_FIXED_SEAT_COUNT_FOR_FLIGHT";
	private static final String SOLD_WL_SEATS_PER_LEG = "SOLD_WL_SEATS_PER_LEG";

	@Override
	public DefaultServiceResponse isFlightInventoryModelUpdatable(Collection<Integer> flightIds,
			Collection<AircraftCabinCapacity> cabinClassesUpdated, Collection<AircraftCabinCapacity> cabinClassesDeleted,
			boolean downGradeToAnyModel, boolean isScheduleChange) throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		Object[] flightIdsArr = flightIds.toArray();
		int chunkSize = AirinventoryCustomConstants.DB_OPERATION_CHUNK_SIZE;
		// TODO check if cabinclasses deletable
		if (cabinClassesDeleted != null) {
			for (AircraftCabinCapacity deletedCC : cabinClassesDeleted) {
				for (int page = 0; page < flightIds.size(); page += chunkSize) {
					ArrayList<Integer> pagedFlightIds = new ArrayList<Integer>();
					for (int i = page; i < page + chunkSize; i++) {
						if (i < flightIdsArr.length) {
							pagedFlightIds.add((Integer) flightIdsArr[i]);
						} else {
							break;
						}
					}

					if (pagedFlightIds.size() != 0
							&& (!AppSysParamsUtil.isEnableAircraftCapacityDowngradeToDifferentCC() || isScheduleChange)) {
						List<Integer> flightsHavingRes = getFlightInventoryResDAO().filterFlightsHavingReservations(
								pagedFlightIds, deletedCC.getCabinClassCode());
						if (flightsHavingRes != null && flightsHavingRes.size() > 0) {
							response.setSuccess(false);
							response.setResponseCode(ResponceCodes.CANNOT_DELETE_BC_INVENTORY);
							response.addResponceParam(ResponceCodes.CANNOT_DELETE_BC_INVENTORY, deletedCC.getCabinClassCode());
							response.addResponceParam(
									ResponceCodes.DATE,
									AirInventoryModuleUtils.getFlightBD().getFlightDates(flightIds, 10,
											new SimpleDateFormat("dd-MM")));
							return response;
						}
					}
					if (!downGradeToAnyModel) { // todo - get into the same if condition
						List<Integer> flightsHavingRes = getFlightInventoryResDAO().filterFlightsHavingReservations(
								pagedFlightIds, deletedCC.getCabinClassCode());
						if (flightsHavingRes != null && flightsHavingRes.size() > 0) {
							response.setSuccess(false);
							response.setResponseCode(ResponceCodes.CANNOT_DELETE_BC_INVENTORY);
							response.addResponceParam(ResponceCodes.CANNOT_DELETE_BC_INVENTORY, deletedCC.getCabinClassCode());
							response.addResponceParam(
									ResponceCodes.DATE,
									AirInventoryModuleUtils.getFlightBD().getFlightDates(flightIds, 10,
											new SimpleDateFormat("dd-MM")));
							return response;
						}
					}
				}
			}
		}

		// check seat constrains , fixed , infant and sold seat constrain
		/*
		 * Collection<String> singleLegSegmentCodes = null; if (!cabinClassesUpdated.isEmpty()) { List<String>
		 * flightSegmentCodes = this.getFlightSegmentCodes(flightIds.iterator().next()); singleLegSegmentCodes =
		 * AirinventoryUtils.buildSingleLegSegmentCodes(flightSegmentCodes); }
		 */

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		final boolean downGradable = downGradeToAnyModel;
		String custumMessage = "";
		boolean updatable = true;
		final Collection<Integer> flightsToHandleManually = new ArrayList<Integer>();
		final Collection<Pair<Integer, String>> pairFlight = new ArrayList<Pair<Integer, String>>();
		for (final AircraftCabinCapacity aircraftCabinCapacity : cabinClassesUpdated) {
			for (int page = 0; page < flightIds.size(); page += chunkSize) {

				ArrayList<Integer> pagedFlightIds = new ArrayList<Integer>();
				for (int i = page; i < page + chunkSize; i++) {
					if (i < flightIdsArr.length) {
						pagedFlightIds.add((Integer) flightIdsArr[i]);
					} else {
						break;
					}
				}

				if (pagedFlightIds.size() != 0) {
					final List<Integer> fpagedFlightIds = pagedFlightIds;

					// Check fixed seat constrain
					boolean fixedUpdatable = (Boolean) template.query(new PreparedStatementCreator() {
						@Override
						public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
							String strInClause = Util.buildInClauseContent("?", fpagedFlightIds.size(),
									Util.CODE_TYPE_STRING_WITHOUT_QUOTE, " ", ",");
							PreparedStatement ps = conn.prepareStatement(getQuery(
									AirinventoryCustomConstants.CHECK_FCC_INVENTORY_FIXED_SEATS_CONSTRAIN,
									new String[] { strInClause }));
							int index = 0;
							ps.setString(++index, aircraftCabinCapacity.getCabinClassCode());
							for (Integer element : fpagedFlightIds) {
								ps.setInt(++index, element.intValue());
							}
							ps.setString(++index, aircraftCabinCapacity.getCabinClassCode());
							return ps;
						}
					}, new ResultSetExtractor() {
						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							boolean proceed = true;
							while (rs.next()) {
								int fixedSeatCount = rs.getInt("fixedSeats");
								if (fixedSeatCount > aircraftCabinCapacity.getSeatCapacity()) {
									if (!downGradable) {
										proceed = false;
									} else {

										if (!AppSysParamsUtil.enableDowngradeAircraftModel()) {
											proceed = false;
										}
									}
									Pair<Integer, String> pairAdd = Pair.of(rs.getInt("flight_id"),
											aircraftCabinCapacity.getCabinClassCode());
									pairFlight.add(pairAdd);
									flightsToHandleManually.add(rs.getInt("flight_id"));
								}
							}
							return new Boolean(proceed);
						}
					});

					if (updatable) {
						updatable = fixedUpdatable;
						if (!fixedUpdatable) {
							response.setResponseCode(ResponceCodes.CC_FIXED_SEATS_INSUFFICIENT);
						}
					}
					if (fixedUpdatable) {
						// Check adult and infant seat constrain
						for (Integer flightId : pagedFlightIds) {
							Collection<String> singleLegSegmentCodes = null;
							if (!cabinClassesUpdated.isEmpty()) {
								List<String> flightSegmentCodes = this.getFlightSegmentCodes(flightId);
								singleLegSegmentCodes = AirinventoryUtils.buildSingleLegSegmentCodes(flightSegmentCodes);
							}
							int[] soldAdultInfSeatCount = getMaxSoldSeatCountForFlight(flightId, singleLegSegmentCodes,
									aircraftCabinCapacity.getCabinClassCode());
							if (soldAdultInfSeatCount[2] > aircraftCabinCapacity.getSeatCapacity()) {
								response.setResponseCode(ResponceCodes.CC_CURTAILED_CAPACITY_INSUFFICIENT);
								custumMessage = "Following flights have more curtail seats than allocated seats - ";

								updatable = false;
								Pair<Integer, String> pairAdd = Pair.of(flightId, aircraftCabinCapacity.getCabinClassCode());
								pairFlight.add(pairAdd);
								flightsToHandleManually.add(flightId);

							}
							if (soldAdultInfSeatCount[3] >= aircraftCabinCapacity.getSeatCapacity()) {
								response.setResponseCode(ResponceCodes.CC_WAITLISTED_CAPACITY_INSUFFICIENT);
								custumMessage = "Following flights have more waitlisted seats than allocated seats - ";
								updatable = false;
								Pair<Integer, String> pairAdd = Pair.of(flightId, aircraftCabinCapacity.getCabinClassCode());
								pairFlight.add(pairAdd);
								flightsToHandleManually.add(flightId);

							}
							if (soldAdultInfSeatCount[1] > aircraftCabinCapacity.getInfantCapacity()) {
								response.setResponseCode(ResponceCodes.CC_INFANT_CAPACITY_INSUFFICIENT);
								custumMessage = "Following flights have more infant sold count than allocated seats - ";

								updatable = false;
								Pair<Integer, String> pairAdd = Pair.of(flightId, aircraftCabinCapacity.getCabinClassCode());
								pairFlight.add(pairAdd);
								flightsToHandleManually.add(flightId);
							} else if (soldAdultInfSeatCount[0] > aircraftCabinCapacity.getSeatCapacity() && !downGradeToAnyModel) {
								if (!AppSysParamsUtil.enableDowngradeAircraftModel()) {
									response.setResponseCode(ResponceCodes.CC_CAPACITY_INSUFFICIENT);
									response.addResponceParam(ResponceCodes.CC_CAPACITY_INSUFFICIENT,
											aircraftCabinCapacity.getCabinClassCode());
									updatable = false;
									Pair<Integer, String> pairAdd = Pair.of(flightId, aircraftCabinCapacity.getCabinClassCode());
									pairFlight.add(pairAdd);
									flightsToHandleManually.add(flightId);
								}

							}
						}
					} else {
						response.setResponseCode(ResponceCodes.CC_FIXED_SEATS_INSUFFICIENT);
						custumMessage = "Following flights has fixed seats than the allocated - ";
					}
				}
			}
		}

		if (!updatable) {
			response.setSuccess(false);
			response.addResponceParam(
					ResponceCodes.DATE,
					AirInventoryModuleUtils.getFlightBD().getFlightDates(flightsToHandleManually, 10,
							new SimpleDateFormat("dd-MM")));
			StringBuilder strFlightBuffer = new StringBuilder();
			int size = pairFlight.size();
			if (size > 0) {
				strFlightBuffer.append(custumMessage);
			}
			int i = 0;
			for (Pair<Integer, String> pairGet : pairFlight) {
				if (i == size - 1) {
					String[] flightDetails = getFlightDetails(pairGet.getLeft());
					strFlightBuffer.append(flightDetails[0] + "|" + flightDetails[1] + "|" + pairGet.getRight());
				} else {
					String[] flightDetails = getFlightDetails(pairGet.getLeft());
					strFlightBuffer.append(flightDetails[0] + "|" + flightDetails[1] + "|" + pairGet.getRight() + ",");
				}
			}
			response.addResponceParam(ResponceCodes.FLIGHT, strFlightBuffer.toString());
			return response;
		}
		response.setSuccess(true);
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getFlightSegmentCodes(final Integer flightId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (List<String>) template.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				PreparedStatement ps = conn.prepareStatement("select segment_code from t_flight_segment where flight_id = ?");
				int index = 0;
				ps.setInt(++index, flightId.intValue());
				return ps;
			}
		}, new ResultSetExtractor() {
			@Override
			public List<String> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> data = new ArrayList<String>();
				if (rs.next()) {
					data.add(rs.getString("SEGMENT_CODE"));
				}
				return data;
			}
		});
	}

	/**
	 * @param flightSegmentId
	 * @return list of <code>FCCSegONDCodesAndBookingClassesDTO</code>
	 */
	public List<FCCSegONDCodesAndBookingClassesDTO> getFCCSegONDCodesAndBookingClassesDTOs(
			final Integer flightSegmentId) {
		
		Objects.requireNonNull(flightSegmentId, "FlightSegmentId cannot be null");

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = "SELECT ppfs.booking_code, tfr.return_flag, fare.ond_code "
				+ "FROM t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, t_pnr_pax_fare ppf, t_ond_fare fare, t_pnr_passenger pp, t_fare_rule tfr "
				+ "WHERE ppfs.pnr_seg_id = ps.pnr_seg_id AND ppfs.ppf_id = ppf.ppf_id  AND  ppf.fare_id = fare.fare_id "
				+ "AND ps.status = 'CNF' AND tfr.fare_rule_id = fare.fare_rule_id AND ppf.pnr_pax_id = pp.pnr_pax_id "
				+ "AND pp.status = 'CNF' AND ppfs.booking_code IS NOT NULL AND ps.flt_seg_id = " + flightSegmentId;

		return template.query(query.toString(), new ResultSetExtractor<List<FCCSegONDCodesAndBookingClassesDTO>>() {
			@Override
			public List<FCCSegONDCodesAndBookingClassesDTO> extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				List<FCCSegONDCodesAndBookingClassesDTO> bookingClassDataList = new ArrayList<>();
				while (rs.next()) {
					FCCSegONDCodesAndBookingClassesDTO bookingClassData = new FCCSegONDCodesAndBookingClassesDTO();
					bookingClassData.setBookingCode(rs.getString("booking_code"));
					bookingClassData.setReturnFlag(StringUtils.equalsIgnoreCase(rs.getString("return_flag"), "Y"));
					bookingClassData.setOndCode(rs.getString("ond_code"));
					bookingClassDataList.add(bookingClassData);
				}
				return Collections.unmodifiableList(bookingClassDataList);
			}
		});
	}
	
	
	/**
	 * @param flightId
	 * @param singleLegSegmentCodes
	 * @param cabinClass
	 * @return
	 */
	private int[] getMaxSoldSeatCountForFlight(Integer flightId, Collection<String> singleLegSegmentCodes, String cabinClass) {

		int maxAdultSoldSeatCount = 0;
		int maxInfantSoldSeatCount = 0;
		int maxCurtailCount = 0;
		int waitListCount = 0;
		int fixedSeatsCount = 0;
		int fixedSeatsSold = 0;

		for (String segmentCode : singleLegSegmentCodes) {
			int[] soldSeatCount = getTotalSoldAndOnholdFixedAndInfSeatsInSegs(flightId, segmentCode, cabinClass);
			maxAdultSoldSeatCount = Math.max(soldSeatCount[0], maxAdultSoldSeatCount);
			maxInfantSoldSeatCount = Math.max(soldSeatCount[1], maxInfantSoldSeatCount);

			maxCurtailCount = Math.max(soldSeatCount[2], maxCurtailCount);
			waitListCount = Math.max(soldSeatCount[3], waitListCount);
			int[] fixedSeatCount = getEffectiveAvailableFixedSeatCountForFLight(flightId, segmentCode, cabinClass);
			fixedSeatsCount = Math.max(fixedSeatCount[0], fixedSeatsCount);
			fixedSeatsSold = Math.max(fixedSeatCount[1], fixedSeatsSold);
		}
		return new int[] { maxAdultSoldSeatCount, maxInfantSoldSeatCount, maxCurtailCount, waitListCount,
				fixedSeatsCount - fixedSeatsSold, fixedSeatsSold };

	}

	/**
	 * @param flightId
	 * @param singleLegSegmentCodes
	 * @param cabinClass
	 * @return
	 */
	@Override
	public Map<String, Map<String, int[]>> getTotalSoldSeatCountSegmentAndLogicalCabinClassWise(Integer flightId,
			Collection<String> singleLegSegmentCodes, String cabinClass) {
		Map<String, Map<String, int[]>> segmentAndLogicalCabinClassWiseSoldSeatCount = new HashMap<String, Map<String, int[]>>();
		for (String segmentCode : singleLegSegmentCodes) {
			segmentAndLogicalCabinClassWiseSoldSeatCount.put(segmentCode,
					getTotalSoldAndOnholdFixedAndInfSeatsLogicalCabinClass(flightId, segmentCode, cabinClass));
		}
		return segmentAndLogicalCabinClassWiseSoldSeatCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FCCInventoryDTO> getFCCInventoryDTOs(int flightId, String cabinClassCode, Integer overlappingFlightId, boolean isIncludeCnxFlts) {
		RetriveFlightInventoryCritera criteria = new RetriveFlightInventoryCritera();
		criteria.setFlightId(flightId);
		criteria.setCabinClassCode(cabinClassCode);
		criteria.setOverlappingFlightId(overlappingFlightId);
		criteria.setExcludeInvalidSegmentInventories(true);

		String includeCnxFlts = "' '";
		if (!isIncludeCnxFlts)
			includeCnxFlts = "'CNX'";

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(RETRIEVE_FCC_SEG_INVENTORIES, new String[] {
				getSqlInPlaceholders(criteria.getFlightIds().toArray()), "" , includeCnxFlts });
		return (Collection<FCCInventoryDTO>) template.query(new RetriveFlightInventoriesPrepStmtCreator(query, criteria),
				new RetrieveFlightInventoriesRsExtractor());
	}

	/**
	 * The method can be extended to have additional bc inventory columns as required
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param excludeInvalidSegments
	 * @return Map key by segmentCode, having Lists of Object []
	 */
	@SuppressWarnings("unchecked")
	@Override
	public LinkedHashMap<String, List<Object[]>> getFCCSegBCInventories(int flightId, String cabinClassCode,
			boolean excludeInvalidSegments) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(RETRIEVE_FCC_SEG_BC_INVENTORIES);

		Object[] params = { new Integer(flightId), cabinClassCode, excludeInvalidSegments ? FCCSegInventory.VALID_FLAG_Y : "%",
				BookingClass.PassengerType.GOSHOW };

		return (LinkedHashMap<String, List<Object[]>>) template.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				LinkedHashMap<String, List<Object[]>> segmentBCsMap = new LinkedHashMap<String, List<Object[]>>();
				while (rs.next()) {
					String segmentCode = rs.getString("segment_code");
					List<Object[]> bcInvs = segmentBCsMap.get(segmentCode);
					if (bcInvs == null) {
						bcInvs = new ArrayList<Object[]>();
						segmentBCsMap.put(segmentCode, bcInvs);
					}

					Object[] bcInv = { rs.getString("booking_code"), rs.getString("standard_code"), rs.getString("fixed_flag"),
							rs.getString("logical_cabin_class_code") };
					bcInvs.add(bcInv);
				}
				return segmentBCsMap;
			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Collection<FCCInventoryDTO> getFlightInventoriesForOptimization(Collection<Integer> flightIds,
			OptimizeSeatInvCriteriaDTO criteria) throws ModuleException {
		if (flightIds == null || flightIds.size() == 0) {
			return null;
		}
		String[] params = new String[3];
		params[0] = getSqlInPlaceholders(flightIds.toArray());

		StringBuffer sql = new StringBuffer();
		if (criteria.isIncludeStandardBCsOnly()) {
			sql.append(" AND bc.standard_code = '").append(BookingClass.STANDARD_CODE_Y).append("' ");
		}

		Collection bookingCodesToInclude = null;
		if (criteria.getSelectedBCs() != null && criteria.getSelectedBCs().size() > 0) {
			bookingCodesToInclude = criteria.getSelectedBCs();
		}

		if (criteria.getAgentCode() != null && !criteria.getAgentCode().equals("")) {
			Collection agentBCs = AirInventoryModuleUtils.getFareBD().getBookingCodesLinkedToAgent(criteria.getAgentCode());
			bookingCodesToInclude = agentBCs;

			if (agentBCs == null || agentBCs.size() == 0) {
				// do not include the segment in this scenario
				sql.append(" AND fccsa.fccsa_id IS NULL ");
			}
		}

		if (bookingCodesToInclude != null && bookingCodesToInclude.size() > 0) {
			sql.append(" AND bc.booking_code IN (").append(getSqlInValuesStr(bookingCodesToInclude)).append(") ");
		}

		if (criteria.isPerformAvailableOnSegment()) {
			if (criteria.isCheckNegativeAvailability()) {
				sql.append(" AND fccsa.available_seats < 0 ");
			} else {
				sql.append(" AND fccsa.available_seats ").append(criteria.getAvailabilityOperator()).append(" ")
						.append(criteria.getNoOfSeats()).append(" ");
			}
		}

		if (criteria.isPerformAvailableOnBC()) {
			// if none of the bc has the requested availability, the segment is excluded
			sql.append(" AND fccsba.available_seats ").append(criteria.getAvailabilityOperator()).append(" ")
					.append(criteria.getNoOfSeats()).append(" ");
		}
		sql.append(" AND substr(f.flight_number,0,2) IN ("
				+ Util.constructINStringForCharactors(criteria.getApplicapableCarrierCodes()) + ")");
		params[1] = sql.toString();
		params[2] = "'CNX'";

		RetriveFlightInventoryCritera inventoryRetrivalCriteria = new RetriveFlightInventoryCritera();
		inventoryRetrivalCriteria.setFlightIds(flightIds);
		inventoryRetrivalCriteria.setCabinClassCode("%");
		inventoryRetrivalCriteria.setExcludeInvalidSegmentInventories(true);

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(RETRIEVE_FCC_SEG_INVENTORIES, params);
		return (Collection) template.query(new RetriveFlightInventoriesPrepStmtCreator(query, inventoryRetrivalCriteria),
				new RetrieveFlightInventoriesRsExtractor());
	}

	public int[] getSoldOnholdFixedAndInfSeatsInSegs(final Integer fccSegInvId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (int[]) template.query(
				getQuery(SOLD_WL_SEATS_PER_LEG,
						new String[] { Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)),
								fccSegInvId.toString(), fccSegInvId.toString() }), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						SegAllocObj currentOne = null;
						Map<String, SegAllocObj> legwiseSegAllocs = new HashMap<String, FlightInventoryJDBCDAOImpl.SegAllocObj>();
						while (rs.next()) {
							SegAllocObj segAlloc = new SegAllocObj();
							segAlloc.segAllocId = rs.getInt("fccsa_id");
							segAlloc.segmentCode = rs.getString("segment_code");
							segAlloc.soldAdults = rs.getInt("sof");
							segAlloc.soldInfants = rs.getInt("soi");
							segAlloc.soldWaitlist = rs.getInt("wls");
							if (fccSegInvId == segAlloc.segAllocId) {
								currentOne = segAlloc;
							} else {
								String airports[] = segAlloc.segmentCode.split("/");
								for (int i = 0; i < airports.length - 1; i++) {
									String legCode = airports[i] + "/" + airports[i + 1];

									if (!legwiseSegAllocs.containsKey(legCode)) {
										legwiseSegAllocs.put(legCode, new SegAllocObj());
									}
									SegAllocObj legAlloc = legwiseSegAllocs.get(legCode);
									legAlloc.segmentCode = segAlloc.segmentCode;
									legAlloc.soldAdults += segAlloc.soldAdults;
									legAlloc.soldInfants += segAlloc.soldInfants;
									legAlloc.soldWaitlist += segAlloc.soldWaitlist;
								}

							}
						}
						int adultSubMax = 0;
						int infantSubMax = 0;
						int waitlistSubMax = 0;

						int adultCountExcludingSubs = 0;
						int infantCountExcludingSubs = 0;
						int waitlistCountExcludingSubs = 0;
						if (currentOne != null) {
							adultCountExcludingSubs = currentOne.soldAdults;
							infantCountExcludingSubs = currentOne.soldInfants;
							waitlistCountExcludingSubs = currentOne.soldWaitlist;
						}
						for (SegAllocObj segAllocSold : legwiseSegAllocs.values()) {
							adultSubMax = Math.max(adultSubMax, segAllocSold.soldAdults);
							infantSubMax = Math.max(infantSubMax, segAllocSold.soldInfants);
							waitlistSubMax = Math.max(waitlistSubMax, segAllocSold.soldWaitlist);
						}
						return new int[] { adultSubMax + adultCountExcludingSubs, infantSubMax + infantCountExcludingSubs,
								waitlistSubMax + waitlistCountExcludingSubs };
					}

				});
	}

	class SegAllocObj {
		protected int segAllocId;
		protected String segmentCode;
		protected int soldAdults;
		protected int soldInfants;
		protected int soldWaitlist;
	}

	/**
	 * Retrieves bc inventory + booking class information
	 * 
	 * @param fccsbInvId
	 * @return
	 */
	@Override
	public FCCSegBCInventoryCollection getBucketsCollection(Integer fccsbInvId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		String query = getQuery(RETRIEVE_BUCKETS);
		return (FCCSegBCInventoryCollection) template.query(new RetriveBucketsPrepStmtCreator(query,
				getSegmentInventoryInfo(fccsbInvId)), new RetrieveBucketsRsExtractor());
	}

	private Object[] getSegmentInventoryInfo(Integer fccsbInvId) {
		final Integer fccsbInvIdLocal = fccsbInvId;
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (Object[]) template.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				PreparedStatement ps = conn
						.prepareStatement("SELECT FCCSA_ID, SEGMENT_CODE FROM T_FCC_SEG_BC_ALLOC WHERE FCCSBA_ID=?");
				int index = 0;
				ps.setInt(++index, fccsbInvIdLocal.intValue());
				return ps;
			}
		}, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Object[] data = new Object[2];
				if (rs.next()) {
					data[0] = new Integer(rs.getInt("FCCSA_ID"));
					data[1] = rs.getString("SEGMENT_CODE");
				}
				return data;
			}
		});
	}

	private FlightInventoryResDAO getFlightInventoryResDAO() {
		return (FlightInventoryResDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryResDAOImplProxy");
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Object[]> getFCCSegIdAndSegBCIds(String bookingCode, Timestamp fromDate, Timestamp toDate) {

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Object params[] = { fromDate, toDate, bookingCode };
		final String query = getQuery(FIXED_BC_ALLOCATIONS_TO_RELEASE);

		return (Collection<Object[]>) template.query(query, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Object[]> c = new ArrayList<Object[]>();
				while (rs.next()) {
					Object[] o = new Object[2];
					o[0] = new Integer(rs.getInt("fccsa_id"));
					o[1] = new Integer(rs.getInt("fccsba_id"));
					c.add(o);
				}
				return c;
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getCabinClassAvailability(CabinClassAvailabilityDTO cabinClassAvailabilityDTO) {

		String query = getQuery(CABIN_CLASS_AVAILABILITY);
		Object params[] = { cabinClassAvailabilityDTO.getFlightNumber(), cabinClassAvailabilityDTO.getOrigin(),
				cabinClassAvailabilityDTO.getDestination(), cabinClassAvailabilityDTO.getDepartureDateTimeStart(),
				cabinClassAvailabilityDTO.getDepartureDateTimeEnd() };

		return (Map<String, String>) new JdbcTemplate(getDataSource()).query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, String> map = new HashMap<String, String>();
				while (rs.next()) {

					String cabinClassCode = rs.getString(1);
					String availableSeats = rs.getString(2);

					map.put(cabinClassCode, availableSeats);
				}

				return map;
			}
		});
	}

	@Override
	public FlightSegWithCCDTO getFlightSegmentDetailWithCabinClass(Integer fltSegId) {
		String sql = getQuery(FLIGHT_SEGMENT_DETAIL_WITH_CABIN_CLASS);
		Object[] params = { fltSegId };

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		FlightSegWithCCDTO flightSegmentWithCabinClassDTO = (FlightSegWithCCDTO) jdbcTemplate.query(sql, params,
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						FlightSegWithCCDTO dto = new FlightSegWithCCDTO();
						while (rs.next()) {
							dto.setOrigin(rs.getString("ORIGIN"));
							dto.setDestination(rs.getString("DESTINATION"));
							dto.setDepartureDate(rs.getTimestamp("DEPARTURE_LOCAL"));
							dto.setArrivalDate(rs.getTimestamp("ARRIVAL_LOCAL"));
							dto.setSegDuration(new Integer(rs.getInt("SEG_DURATION")));
							dto.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
							dto.setModelNumber(rs.getString("MODEL_NUMBER"));
							dto.setFrequency(rs.getString("FREQUENCY"));
							dto.addCabinClassAvailability(rs.getString("C_AVAILABILITY"));
						}
						return dto;
					}
				});
		return flightSegmentWithCabinClassDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getAvailabilityForPublishing() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (Map<Integer, Collection<PublishSegAvailMsgDTO>>) template.query(getQuery(RETRIEVE_AVAILABILITY_FOR_PUBLISHING),
				new RetrieveAvailabilityForPublishRsExtractor(AvsGenerationFlow.NORMAL));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Collection<PublishSegAvailMsgDTO>> getBCStatusAVS() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (Map<Integer, Collection<PublishSegAvailMsgDTO>>) template.query(
				getQuery(RETRIEVE_AVAILABILITY_FOR_BC_STATUS_UPDATE), new RetrieveAvailabilityForPublishRsExtractor(
						AvsGenerationFlow.BC_STATUS_UPDATE));
	}

	/**
	 * This method will return meals requirement for all out bound flights for a given period of time and the meals
	 * requirement for corresponding return flight for each out bound flight.
	 * 
	 * @author lalanthi
	 */
	@Override
	public Object[] getMealsListForStation(List<String> hub, String mealNotificationBeginTime, String mealNotificationEndTime,
			int noofAttempts, List<Integer> flightIds) {
		Object[] allMealsList = new Object[4];
		// 1. Get all the out bound flights that for given time range
		Map<Integer, FlightSummaryDTO> obFlightSummaryDTOMap = getFlightsForMeals(hub, mealNotificationBeginTime,
				mealNotificationEndTime, noofAttempts, flightIds);

		allMealsList[0] = obFlightSummaryDTOMap;// Out bound flights
		if (obFlightSummaryDTOMap != null && obFlightSummaryDTOMap.size() > 0) {
			// 2. Gets the meal requirement for all out bound flights for given time range.
			Map<Integer, FlightMealsDTO> obFlightMealsDTOMap = getFlightWiseMealsBooked(obFlightSummaryDTOMap.keySet(), null);

			if (obFlightMealsDTOMap == null || obFlightMealsDTOMap.size() == 0) {
				log.info("No outbound has meal selected for meal notification [" + "hub=" + hub + ",mealNotificationBeginTime="
						+ mealNotificationBeginTime + ",mealNotificationEndTime=" + mealNotificationEndTime + ",noofAttempts="
						+ noofAttempts + "]");
			}

			allMealsList[1] = obFlightMealsDTOMap;// Out bound meals

			// 3. For each out bound flight , get the corresponding return flight's meal requirement
			Object[] ibFlightsAndMeals = getInBoundMealsListForStation(obFlightSummaryDTOMap);

			// 3. Add the return flight meal requirements to the collection.
			if (ibFlightsAndMeals != null) {
				allMealsList[2] = ibFlightsAndMeals[0];// In bound flights
				allMealsList[3] = ibFlightsAndMeals[1];// In bound meals
			}
			return allMealsList;
		} else {
			log.info("No outbound flight found for meal notification [" + "hub=" + hub + ",mealNotificationBeginTime="
					+ mealNotificationBeginTime + ",mealNotificationEndTime=" + mealNotificationEndTime + ",noofAttempts="
					+ noofAttempts + "]");
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, FlightSummaryDTO> getFlightsForMeals(List<String> listHubs, String mealNotificationBeginTime,
			String mealNotificationEndTime, Integer noofAttempts, List<Integer> flightIds) {

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String queryForStation = "SELECT DISTINCT F.flight_id, F.flight_number, "
				+ "fa.cabin_class_code, sa.sold_seats, sa.sold_infant_seats, FS.est_time_arrival_local, FS.est_time_departure_local, "
				+ "FS.est_time_arrival_zulu, FS.est_time_departure_zulu, " + "F.ORIGIN, F.DESTINATION   "
				+ "FROM T_FLIGHT_SEGMENT FS,T_FLIGHT F, T_FCC_SEG_ALLOC SA, T_FCC_ALLOC FA WHERE "
				+ "FS.FLIGHT_ID = F.FLIGHT_ID AND FS.flt_seg_id = sa.flt_seg_id AND SA.FCCA_ID = FA.FCCA_ID "
				+ "AND f.status <> 'CNX' ";
		if (flightIds == null || flightIds.size() == 0) {
			if (mealNotificationEndTime != null) {
				queryForStation += "AND FS.EST_TIME_DEPARTURE_ZULU < to_timestamp(" + mealNotificationEndTime + ") ";
			}
			if (mealNotificationBeginTime != null) {
				queryForStation += "AND FS.EST_TIME_DEPARTURE_ZULU  > to_timestamp(" + mealNotificationBeginTime + ")  ";
			}

			queryForStation += "AND (F.meal_notification_status is null OR (F.meal_notification_status <> 'S' AND F.meal_notification_status <> 'E')) "
					+ "AND (F.MEAL_NOTIFICATION_ATTEMPTS is null OR F.MEAL_NOTIFICATION_ATTEMPTS < " + noofAttempts + ")";

			if (listHubs != null && listHubs.size() > 0) {
				queryForStation += "AND (";

				for (Iterator<String> iterator = listHubs.iterator(); iterator.hasNext();) {
					String hub = iterator.next();
					queryForStation += " FS.SEGMENT_CODE LIKE '" + hub + "%' ";
					if (iterator.hasNext()) {
						queryForStation += " OR ";
					} else {
						queryForStation += " )";
					}
				}

			}

		} else {
			queryForStation += "AND " + Util.getReplaceStringForIN("F.FLIGHT_ID", flightIds);
		}

		return (Map<Integer, FlightSummaryDTO>) template.query(queryForStation, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, FlightSummaryDTO> obFlightsMap = new HashMap<Integer, FlightSummaryDTO>();
				Integer flightId = null;
				FlightSummaryDTO flightSummaryDTO = null;
				while (rs.next()) {
					flightId = new Integer(rs.getInt("flight_id"));
					if (!obFlightsMap.containsKey(flightId)) {
						flightSummaryDTO = new FlightSummaryDTO();
						obFlightsMap.put(flightId, flightSummaryDTO);

						flightSummaryDTO.setFlightId(flightId);
						flightSummaryDTO.setFlightNumber(rs.getString("flight_number"));

						Timestamp tsDep = rs.getTimestamp("est_time_departure_local");
						flightSummaryDTO.setDepartureDateTimeLocal(new java.util.Date(tsDep.getTime()));

						Timestamp tsArr = rs.getTimestamp("est_time_arrival_local");
						flightSummaryDTO.setArrivalDateTimeLocal(new java.util.Date(tsArr.getTime()));

						Timestamp tsDepZulu = rs.getTimestamp("est_time_departure_zulu");
						flightSummaryDTO.setDepartureDateTimeZulu(new java.util.Date(tsDepZulu.getTime()));

						Timestamp tsArrZulu = rs.getTimestamp("est_time_arrival_zulu");
						flightSummaryDTO.setArrivalDateTimeZulu(new java.util.Date(tsArrZulu.getTime()));

						flightSummaryDTO.setOrigin(rs.getString("ORIGIN"));
						flightSummaryDTO.setDestination(rs.getString("DESTINATION"));

					} else {
						// Multi-leg flight
						flightSummaryDTO = obFlightsMap.get(flightId);

						Timestamp tsDepZulu = rs.getTimestamp("est_time_departure_zulu");
						java.util.Date depDateZulu = new java.util.Date(tsDepZulu.getTime());
						if (flightSummaryDTO.getDepartureDateTimeZulu().after(depDateZulu)) {
							flightSummaryDTO.setDepartureDateTimeLocal(depDateZulu);

							Timestamp tsDepLocal = rs.getTimestamp("est_time_departure_local");
							flightSummaryDTO.setDepartureDateTimeLocal(new java.util.Date(tsDepLocal.getTime()));
						}

						Timestamp tsArrZulu = rs.getTimestamp("est_time_arrival_zulu");
						java.util.Date arrDateZulu = new java.util.Date(tsArrZulu.getTime());
						if (flightSummaryDTO.getArrivalDateTimeZulu().before(arrDateZulu)) {
							flightSummaryDTO.setArrivalDateTimeZulu(arrDateZulu);

							Timestamp tsArr = rs.getTimestamp("est_time_arrival_local");
							flightSummaryDTO.setArrivalDateTimeLocal(new java.util.Date(tsArr.getTime()));
						}

						// -----------------------------------

						// -----------------------------------
					}

					Map<String, FlightMealClassDTO> mapCabinClassFlightMealClassDTO = flightSummaryDTO
							.getMapCabinClassFlightMealClassDTO();
					String cabinClass = rs.getString("cabin_class_code");
					if (mapCabinClassFlightMealClassDTO != null && mapCabinClassFlightMealClassDTO.size() > 0) {
						if (!mapCabinClassFlightMealClassDTO.containsKey(cabinClass)) {
							FlightMealClassDTO flightMealClassDTO = new FlightMealClassDTO();
							flightMealClassDTO.setCabinClass(cabinClass);
							flightMealClassDTO.setTotalAdultCount(rs.getInt("sold_seats"));
							flightMealClassDTO.setTotalInfantCount(rs.getInt("sold_infant_seats"));
							mapCabinClassFlightMealClassDTO.put(cabinClass, flightMealClassDTO);
						}
					} else {
						mapCabinClassFlightMealClassDTO = new HashMap<String, FlightMealClassDTO>();
						FlightMealClassDTO flightMealClassDTO = new FlightMealClassDTO();
						flightMealClassDTO.setCabinClass(cabinClass);
						flightMealClassDTO.setTotalAdultCount(rs.getInt("sold_seats"));
						flightMealClassDTO.setTotalInfantCount(rs.getInt("sold_infant_seats"));
						mapCabinClassFlightMealClassDTO.put(cabinClass, flightMealClassDTO);
						flightSummaryDTO.setMapCabinClassFlightMealClassDTO(mapCabinClassFlightMealClassDTO);
					}
				}
				return obFlightsMap;
			}
		});
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, FlightMealsDTO> getFlightWiseMealsBooked(Collection<Integer> flightIds,
			Map<Integer, Integer> flightIdPairs) {
		final Map<Integer, Integer> fflightIdPairs = flightIdPairs;
		if (flightIds != null && flightIds.size() > 0) {
			JdbcTemplate template = new JdbcTemplate(getDataSource());
			// TODO : Logical CC Change : remove cabin_class_code
			String queryForStation = "SELECT flight_id, flight_number, segment_code, notification_email,  departure_date, cabin_class_code, sold_seats,on_hold_seats, sold_infant_seats,  meal_id, meal_code,iata_code, meal_name, COUNT(*) MEALS_COUNT  "
					+ "FROM ( "
					+ "SELECT DISTINCT PPSM.pnr_pax_seg_meal_id, F.flight_id, F.flight_number, FS.segment_code, A.notification_email, F.departure_date, "
					+ "mc.cabin_class_code, SA.sold_seats , SA.on_hold_seats, SA.sold_infant_seats, M.meal_id, M.meal_code, M.iata_code, M.meal_name "
					+ "FROM ML_T_PNR_PAX_SEG_MEAL PPSM, "
					+ "    ML_T_MEAL M, "
					+ "    ML_T_MEAL_COS MC, "
					+ "    T_PNR_SEGMENT PS, "
					+ "    T_FLIGHT_SEGMENT FS, "
					+ "    T_FLIGHT F, T_FCC_ALLOC FA, T_FCC_SEG_ALLOC SA , T_AIRPORT A "
					+ "WHERE "
					+ "    PPSM.meal_id = M.meal_id "
					+ "    AND PPSM.pnr_seg_id = PS.pnr_seg_id "
					+ "    AND PS.flt_seg_id = FS.flt_seg_id "
					+ "    AND FS.flight_id = F.flight_id "
					+ "    AND FS.flt_seg_id = SA.flt_seg_id "
					+ "    AND FA.fcca_id = SA.fcca_id "
					+ "    AND M.meal_id = MC.meal_id "
					+ "    AND f.origin = a.airport_code "
					+ "	   AND (mc.logical_cabin_class_code = sa.logical_cabin_class_code OR "
					+ "    mc.cabin_class_code = FA.cabin_class_code) "
					+ "    AND PPSM.status <> 'CNX' "
					+ "    AND PS.status <> 'CNX' "
					+ "    AND "
					+ Util.getReplaceStringForIN("FS.flight_id", flightIds)
					+ ") GROUP BY flight_id, flight_number, segment_code, notification_email,  departure_date,  cabin_class_code, sold_seats,on_hold_seats, sold_infant_seats, meal_id, meal_code, iata_code, meal_name"
					+ " ORDER BY flight_id "; // To ensure first same flight ID's will be returned

			return (Map<Integer, FlightMealsDTO>) template.query(queryForStation, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Map<Integer, FlightMealsDTO> flightMealsDTOMap = new HashMap<Integer, FlightMealsDTO>();

					FlightMealsDTO flightMealsDTO = null;
					Integer flightId = null;
					Integer key = null;
					while (rs.next()) {
						flightId = rs.getInt("flight_id");
						if (fflightIdPairs != null && fflightIdPairs.get(flightId) != null) {
							key = fflightIdPairs.get(flightId);// use corresponding out bound flightId to key the Map
						} else {
							key = flightId;
						}
						if (flightMealsDTOMap.containsKey(key)) {
							flightMealsDTO = flightMealsDTOMap.get(key);
						} else {
							flightMealsDTO = new FlightMealsDTO();
							flightMealsDTOMap.put(key, flightMealsDTO);
							flightMealsDTO.setFlightId(flightId);
							flightMealsDTO.setFlightNumber(rs.getString("flight_number"));
							flightMealsDTO.setSegmentCode(rs.getString("segment_code"));
							flightMealsDTO.setDepartureTime(rs.getDate("departure_date"));
							flightMealsDTO.setNotifyEmail(rs.getString("notification_email"));
						}

						FlightMealDTO flightMealDTO = new FlightMealDTO();
						String cabinClass = rs.getString("cabin_class_code");
						flightMealDTO.setMealCode(rs.getString("meal_code"));
						flightMealDTO.setMealName(rs.getString("meal_name"));
						flightMealDTO.setSoldMeals(rs.getInt("MEALS_COUNT"));
						if (cabinClass != null) {
						flightMealDTO.setCabinClassCode(cabinClass);
						} else {
							flightMealDTO.setCabinClassCode("");
						}
						flightMealDTO.setMealIATACode(rs.getString("iata_code"));

						Map<String, FlightMealClassDTO> mapCabinClassFlightMealClassDTO = flightMealsDTO
								.getMapCabinClassFlightMealClassDTO();

						if (mapCabinClassFlightMealClassDTO != null && mapCabinClassFlightMealClassDTO.size() > 0) {
							if (!mapCabinClassFlightMealClassDTO.containsKey(cabinClass)) {
								FlightMealClassDTO flightMealClassDTO = new FlightMealClassDTO();
								flightMealClassDTO.setCabinClass(cabinClass);
								flightMealClassDTO.setTotalAdultCount(rs.getInt("sold_seats"));
								flightMealClassDTO.setTotalInfantCount(rs.getInt("sold_infant_seats"));
								mapCabinClassFlightMealClassDTO.put(cabinClass, flightMealClassDTO);
							}
						} else {
							mapCabinClassFlightMealClassDTO = new HashMap<String, FlightMealClassDTO>();
							FlightMealClassDTO flightMealClassDTO = new FlightMealClassDTO();
							flightMealClassDTO.setCabinClass(cabinClass);
							flightMealClassDTO.setTotalAdultCount(rs.getInt("sold_seats"));
							flightMealClassDTO.setTotalInfantCount(rs.getInt("sold_infant_seats"));
							mapCabinClassFlightMealClassDTO.put(cabinClass, flightMealClassDTO);
							flightMealsDTO.setMapCabinClassFlightMealClassDTO(mapCabinClassFlightMealClassDTO);
						}

						flightMealsDTO.addFlightMeal(flightMealDTO);
					}
					return flightMealsDTOMap;
				}
			});
		}
		return null;
	}

	/**
	 * This method first iterate through the outboundMeals list and gets the unique flight segment id for each flight
	 * and the corresponding arrival time. Then it checks whether this is a triangular flight or not. If yes, gets the
	 * corresponding return segment's meal requirement. If not the it finds the corresponding return flight code and the
	 * return flight segment id and then retrieves meals requirement for each return segment.
	 * 
	 * @param flightMealsList
	 *            - out bound Meals Requirement
	 * @return InBoundMealsList for each out bound flight
	 * @author lalanthi
	 */
	private Object[] getInBoundMealsListForStation(Map<Integer, FlightSummaryDTO> obFlights) {
		if (obFlights != null && obFlights.size() > 0) {
			Map<Integer, FlightSummaryDTO> ibFlightSummaryDTOMap = new HashMap<Integer, FlightSummaryDTO>();
			Map<Integer, FlightMealsDTO> ibFlightMealsDTOMap = new HashMap<Integer, FlightMealsDTO>();

			Integer olFlightId = null;
			List<Integer> olFlightIdList = null;
			for (Integer fltId : obFlights.keySet()) {
				Map<Integer, FlightMealsDTO> innerIbFlightMealsDTOMap = null;
				FlightSummaryDTO obFlightSummaryDTO = obFlights.get(fltId);

				// Check for the triangular flights
				olFlightId = isTriangularFlight(obFlightSummaryDTO.getFlightId());
				if (olFlightId != null && olFlightId.intValue() > 0) { // Overlapping flight
					olFlightIdList = new ArrayList<Integer>();
					olFlightIdList.add(olFlightId);
					Map<Integer, FlightSummaryDTO> olFlightSummaryDTOMap = getFlightsForMeals(null, null, null, null,
							olFlightIdList);

					if (olFlightSummaryDTOMap != null && olFlightSummaryDTOMap.containsKey(olFlightId)) {
						ibFlightSummaryDTOMap.put(fltId, olFlightSummaryDTOMap.get(olFlightId));
					}

					innerIbFlightMealsDTOMap = getTriangularFlightMealDetailsForSegment(obFlightSummaryDTO, olFlightId);
				} else { // Return flights
					innerIbFlightMealsDTOMap = getReturnFlightMealDetailsForSegment(obFlightSummaryDTO, ibFlightSummaryDTOMap);
				}
				if (innerIbFlightMealsDTOMap != null && innerIbFlightMealsDTOMap.size() > 0) {
					for (Integer obFlightId : innerIbFlightMealsDTOMap.keySet()) {
						ibFlightMealsDTOMap.put(obFlightId, innerIbFlightMealsDTOMap.get(obFlightId));
					}
				}
			}
			return new Object[] { ibFlightSummaryDTOMap, ibFlightMealsDTOMap };
		} else {
			return new Object[] { null, null };
		}
	}

	/**
	 * Gets the meal requirement for given flight segment.
	 * 
	 * @param obFlightSummaryDTO
	 *            - the outbound flight segment
	 * @return ArrayList of FlightMealDTO objects.
	 * @author lalanthi
	 */
	private Map<Integer, FlightMealsDTO> getReturnFlightMealDetailsForSegment(FlightSummaryDTO obFlightSummaryDTO,
			Map<Integer, FlightSummaryDTO> ibFlightsMap) {
		GregorianCalendar calMinRetDepTime = new GregorianCalendar();
		calMinRetDepTime.setTime(obFlightSummaryDTO.getArrivalDateTimeZulu());
		String minReturnDepTimeZulu = CalendarUtil.formatForSQL_toDate(new Timestamp(calMinRetDepTime.getTime().getTime()));

		GregorianCalendar calMapRetDepTime = new GregorianCalendar();
		calMapRetDepTime.setTime(obFlightSummaryDTO.getArrivalDateTimeZulu());
		calMapRetDepTime.add(Calendar.HOUR, 6); // FIXME: MAX GAP BETWEEN OUTBOUND AND RETURN FLIGHT TO BE TAKEN FROM
												// APP PARAM
		String maxReturnDepTimeZulu = CalendarUtil.formatForSQL_toDate(new Timestamp(calMapRetDepTime.getTime().getTime()));

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String queryForReturnFlightId = "SELECT DISTINCT F.FLIGHT_ID, FS.EST_TIME_DEPARTURE_ZULU "
				+ "FROM T_FLIGHT_SEGMENT FS, T_FLIGHT F WHERE " + "FS.FLIGHT_ID = F.FLIGHT_ID " + "AND F.STATUS ='ACT' "
				+ "AND F.ORIGIN='" + obFlightSummaryDTO.getDestination() + "' " + "AND F.DESTINATION='"
				+ obFlightSummaryDTO.getOrigin() + "' " + "AND FS.STATUS='OPN' " + "AND FS.EST_TIME_DEPARTURE_ZULU>TO_TIMESTAMP("
				+ minReturnDepTimeZulu + ") " + "AND FS.EST_TIME_DEPARTURE_ZULU<TO_TIMESTAMP(" + maxReturnDepTimeZulu + ") "
				+ "ORDER BY FS.EST_TIME_DEPARTURE_ZULU ASC";

		Integer returnFightId = (Integer) template.query(queryForReturnFlightId, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer fltId = null;
				while (rs.next()) {
					fltId = rs.getInt("FLIGHT_ID");// Gets the first departing flight segment id.
					break;
				}
				return fltId;
			}
		});
		if (returnFightId != null && returnFightId.intValue() > 0) {
			List<Integer> returnFlightIdList = new ArrayList<Integer>();
			returnFlightIdList.add(returnFightId);
			Map<Integer, FlightSummaryDTO> returnFlightSummary = getFlightsForMeals(null, null, null, null, returnFlightIdList);

			if (returnFlightSummary != null && returnFlightSummary.containsKey(returnFightId)) {
				ibFlightsMap.put(obFlightSummaryDTO.getFlightId(), returnFlightSummary.get(returnFightId));
			}

			List<Integer> ibFlightIdList = new ArrayList<Integer>();
			ibFlightIdList.add(returnFightId);

			Map<Integer, Integer> flightIdPairsMap = new HashMap<Integer, Integer>();
			flightIdPairsMap.put(returnFightId, obFlightSummaryDTO.getFlightId());

			return getFlightWiseMealsBooked(ibFlightIdList, flightIdPairsMap);
		} else {
			return null;
		}
	}

	/**
	 * Checks whether there is an overlapping flight for this flight.
	 * 
	 * @param flightId
	 * @return Overlapping flight id if any, else null.
	 * @author lalanthi
	 */
	private Integer isTriangularFlight(Integer flightId) {
		Integer olFlightId = null;
		if (flightId != null) {
			JdbcTemplate template = new JdbcTemplate(getDataSource());
			String query = "SELECT OVERLAP_FLIGHT_INSTANCE_ID OVERLAP_ID FROM T_FLIGHT WHERE FLIGHT_ID=" + flightId.intValue();
			olFlightId = (Integer) template.query(query, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Integer olFlightId = null;
					while (rs.next()) {
						olFlightId = new Integer(rs.getInt("OVERLAP_ID"));// Gets the first departing flight id.
						break;
					}

					return olFlightId;
				}
			});
		}
		return olFlightId;
	}

	/**
	 * Gets the overlapping flight's meal requirements.
	 * 
	 * @param obFlightSummaryDTO
	 * @param olFlightId
	 * @return Collection of mealDTO objects for this segment.
	 * @author lalanthi
	 */
	private Map<Integer, FlightMealsDTO> getTriangularFlightMealDetailsForSegment(FlightSummaryDTO obFlightSummaryDTO,
			Integer olFlightId) {
		if (olFlightId != null && olFlightId.intValue() > 0) {
			List<Integer> olFlightIds = new ArrayList<Integer>();
			olFlightIds.add(olFlightId);

			Map<Integer, Integer> flightIdPairsMap = new HashMap<Integer, Integer>();
			flightIdPairsMap.put(olFlightId, obFlightSummaryDTO.getFlightId()); // For mapping corresponding out bound
																				// flightId

			return getFlightWiseMealsBooked(olFlightIds, flightIdPairsMap);
		} else {
			return null;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<InsurancePublisherDetailDTO> getInsuredFlightData() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		StringBuilder query = new StringBuilder();
		Collection<InsurancePublisherDetailDTO> flightInsData = null;

		SchedulerConfig sc = (SchedulerConfig) SchedulerUtils.getInstance().getModuleConfig();
		Integer flightSearchDuration = sc.getJobSearchDurationInHrs();

		// Due to bus service , operation type of the t_flight need to be considered.
		query.append("SELECT DISTINCT fs.flt_seg_id,f.flight_id, fs.est_time_departure_zulu as depTimeZulu, ");
		query.append("f.flight_number, f.origin,fs.est_time_departure_local as depTimeLocal ");
		query.append("FROM t_flight f, t_flight_segment fs ");
		query.append("WHERE fs.status <> 'CNX' AND fs.flight_id = f.flight_id ");
		query.append(" AND fs.flt_seg_id NOT IN (select flt_seg_id from t_flight_seg_notification_evnt fsne where fsne.NOTIFICATION_TYPE = 'INS' ");
		query.append(" AND fsne.flt_seg_id = fs.flt_seg_id ) ");
		query.append(" AND f.operation_type_id <> (SELECT ot.operation_type_id FROM t_operation_type ot WHERE ot.operation_type_description = '"
				+ InsurancePublisherDetailDTO.IGNORE_OPERATION_TYPE_BUS_SERVICE + "') "); // Operating type for Bus
																							// service is 5
		query.append(" AND f.flight_id IN ( SELECT flight_id FROM t_flight fl ");
		query.append(" WHERE  fl.status<>'CNX' AND fl.departure_date BETWEEN SYSDATE AND SYSDATE + NUMTODSINTERVAL("
				+ flightSearchDuration + ", 'HOUR' )) ");

		flightInsData = (Collection<InsurancePublisherDetailDTO>) template.query(query.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<InsurancePublisherDetailDTO> insData = new ArrayList<InsurancePublisherDetailDTO>();
				InsurancePublisherDetailDTO insDataObj = null;
				while (rs.next()) {
					insDataObj = new InsurancePublisherDetailDTO();
					insDataObj.setAncillaryType("INS");
					insDataObj.setDeparturetimeLocal(rs.getTimestamp("depTimeLocal"));
					insDataObj.setFlightId(rs.getInt("flight_id"));
					insDataObj.setFlightSegId(rs.getInt("flt_seg_id"));
					insDataObj.setFlightNumber(rs.getString("flight_number"));
					insDataObj.setDepartureTimeZulu(rs.getTimestamp("depTimeZulu"));
					insDataObj.setFlightOrigin(rs.getString("origin"));
					insDataObj.setRecovery(false);
					insData.add(insDataObj);
				}
				return insData;
			}
		});
		return flightInsData;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<InsurancePublisherDetailDTO> getFailedInsuredFlightData() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Collection<InsurancePublisherDetailDTO> flightInsData = null;
		Integer maxAttempts = Integer.parseInt(AppSysParamsUtil.getRakMaxPublishRetries());
		SchedulerConfig sc = (SchedulerConfig) SchedulerUtils.getInstance().getModuleConfig();
		Integer searchBackDuration = sc.getFailedJobSearchBackDurationInHrs();

		String query = "SELECT  fs.flt_seg_id, f.flight_id,fs.est_time_departure_zulu AS depTimeZulu , f.flight_number, "
				+ "f.origin, fs.est_time_departure_local AS depTimeLocal, evnt.flt_seg_notification_evnt_id AS event_id, "
				+ "evnt.anci_notify_status AS status, evnt.notification_type, evnt.ins_first_response "
				+ "FROM  t_flight f, t_flight_segment fs, t_flight_seg_notification_evnt evnt "
				+ "WHERE f.flight_id = fs.flight_id " + "AND evnt.flt_seg_id = fs.flt_seg_id " + "AND fs.flt_seg_id IN ("
				+ "SELECT noti_evnt.flt_seg_id " + "FROM t_flight_seg_notification_evnt noti_evnt "
				+ "WHERE noti_evnt.notification_type = 'INS' "
				+ "AND noti_evnt.anci_notify_status IN ('RAK_INS_FIRST_UPLOAD_FAILED'," + "'RAK_INS_FIRST_UPLOAD_SENT',"
				+ "'RAK_INS_LAST_UPLOAD_FAILED'," + "'RAK_INS_LAST_UPLOAD_SENT'," + "'NOT_SENT') "
				+ "AND noti_evnt.timestamp   < (sysdate - NUMTODSINTERVAL(" + searchBackDuration + ", 'HOUR' )) "
				+ "AND noti_evnt.no_of_attempts <" + maxAttempts + ")";

		flightInsData = (Collection<InsurancePublisherDetailDTO>) template.query(query.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<InsurancePublisherDetailDTO> insData = new ArrayList<InsurancePublisherDetailDTO>();
				InsurancePublisherDetailDTO insDataObj = null;
				while (rs.next()) {
					insDataObj = new InsurancePublisherDetailDTO();
					insDataObj.setAncillaryType("INS");
					insDataObj.setDeparturetimeLocal(rs.getTimestamp("depTimeLocal"));
					insDataObj.setFlightId(rs.getInt("flight_id"));
					insDataObj.setFlightSegId(rs.getInt("flt_seg_id"));
					insDataObj.setFlightNumber(rs.getString("flight_number"));
					insDataObj.setDepartureTimeZulu(rs.getTimestamp("depTimeZulu"));
					insDataObj.setFlightOrigin(rs.getString("origin"));
					insDataObj.setFlightSegmentNotificationEventId(rs.getInt("event_id"));
					insDataObj.setStatus(rs.getString("status"));
					insDataObj.setInsResponse(rs.getString("ins_first_response"));
					insData.add(insDataObj);
				}
				return insData;
			}
		});
		return flightInsData;
	}

	@Override
	public List<Integer> getOverBookedFlightSegmentIds(final Collection<Integer> flightIds, final String cabinClass,
			final int seatCapacity) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<Integer> overBookedFlightSegmentIds = (List<Integer>) template.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				String strInClause = Util.buildInClauseContent("?", flightIds.size(), Util.CODE_TYPE_STRING_WITHOUT_QUOTE, " ",
						",");
				PreparedStatement ps = conn.prepareStatement(getQuery(
						AirinventoryCustomConstants.CHECK_FCC_INVENTORY_ADULT_SEATS_CONSTRAIN, new String[] { strInClause }));
				int index = 0;
				ps.setString(++index, cabinClass);

				for (Integer element : flightIds) {
					ps.setInt(++index, element.intValue());
				}
				ps.setInt(++index, seatCapacity);
				return ps;
			}
		}, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> overBookedFlightSegmentIds = new ArrayList<Integer>();
				while (rs.next()) {
					overBookedFlightSegmentIds.add(rs.getInt("flt_seg_id"));
				}
				return overBookedFlightSegmentIds;
			}
		});
		return overBookedFlightSegmentIds;

	}

	@Override
	public List<Integer> getFCCSegInventoryIds(final Collection<Integer> flightIds, final String cabinClass) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		List<Integer> fccSegmentIds = (List<Integer>) template.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				String strInClause = Util.buildInClauseContent("?", flightIds.size(), Util.CODE_TYPE_STRING_WITHOUT_QUOTE, " ",
						",");
				PreparedStatement ps = conn.prepareStatement(getQuery(
						AirinventoryCustomConstants.RETRIEVE_FCC_SEG_INVENTORY_SUMMARY, new String[] { strInClause }));
				int index = 0;
				ps.setString(++index, cabinClass);
				for (Integer element : flightIds) {
					ps.setInt(++index, element.intValue());
				}
				return ps;
			}
		}, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> fccSegmentIds = new ArrayList<Integer>();
				while (rs.next()) {
					fccSegmentIds.add(rs.getInt("fccsa_id"));
				}
				return fccSegmentIds;
			}
		});
		return fccSegmentIds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FLCCAllocationDTO> getLogicalCabinClassWiseAllocations(int flightId, String cabinClassCode)
			throws ModuleException {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Object params[] = { cabinClassCode, flightId };
		final String query = getQuery(RETRIEVE_FCC_SEG_LCC_INVENTORY_SUMMARY);

		return (Collection<FLCCAllocationDTO>) template.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FLCCAllocationDTO> flccAllocationDTOs = new ArrayList<FLCCAllocationDTO>();
				while (rs.next()) {
					FLCCAllocationDTO fccSegInventory = new FLCCAllocationDTO();

					fccSegInventory.setCabinClassCode(rs.getString("cabin_class_code"));
					fccSegInventory.setLogicalCabinClassCode(rs.getString("logical_cabin_class_code"));
					fccSegInventory.setAdultAllocation(rs.getInt("allocated_seats"));
					fccSegInventory.setInfantAllocation(rs.getInt("infant_allocation"));
					fccSegInventory.setActualAdultCapacity(rs.getInt("ACTUAL_CAPACITY"));
					fccSegInventory.setActualInfantCapacity(rs.getInt("INFANT_CAPACITY"));
					fccSegInventory.setLogicalCCRank(rs.getInt("logical_cc_rank"));
					flccAllocationDTOs.add(fccSegInventory);
				}
				return flccAllocationDTOs;
			}
		});
	}

	@Override
	public Map<String, int[]> getEffectiveSoldOnholdSeatsInInterceptingSegments(Collection<Integer> flightIds,
			Collection<String> segmentCodes, String logicalCabinClass,
			Collection<Integer> includingOrExcludingInventorySegmentIds, boolean isExcludeSegment) {
		Map<String, int[]> segmentWiseEffectiveSoldSeats = new HashMap<String, int[]>();
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		for (String segmentCode : segmentCodes) {
			segmentWiseEffectiveSoldSeats.put(segmentCode, (int[]) template.query(
					getQuery(
							EFFECTIVE_SOLD_ONHOLD_SEATS_IN_INTCEPTING_SEGMENTS,
							new String[] { BeanUtils.constructINStringForInts(flightIds),
									getLogicalCabinClassClauseConsideringInventoryRestriction(logicalCabinClass), segmentCode,
									Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)),
									isExcludeSegment ? "NOT" : "",
									BeanUtils.constructINStringForInts(includingOrExcludingInventorySegmentIds) }),
					new ResultSetExtractor() {
						@Override
						public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
							resultSet.next();
							return new int[] { resultSet.getInt("sold_seats"), resultSet.getInt("sold_infant_seats") };
						}
					}));
		}
		return segmentWiseEffectiveSoldSeats;
	}

	private int[] getTotalSoldAndOnholdFixedAndInfSeatsInSegs(Integer flightId, String segmentCode, String cabinClass) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (int[]) template.query(
				getQuery(
						TOTAL_SOLD_SEAT_COUNT_FOR_CABIN_CLASS,
						new String[] { flightId.toString(), segmentCode, cabinClass,
								Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) }), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						rs.next();

						return new int[] { rs.getInt("sof"), rs.getInt("soi"), rs.getInt("cus"), rs.getInt("wl") };
					}

				});
	}

	private int[] getEffectiveAvailableFixedSeatCountForFLight(Integer FlightId, String segmentCode, String cabinClass) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (int[]) template.query(
				getQuery(AVAILABLE_EFFECTIVE_FIXED_SEAT_COUNT_FOR_FLIGHT, new String[] { FlightId.toString(), segmentCode,
						cabinClass }), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						rs.next();

						return new int[] { new Integer(rs.getInt("allocatedFixedSeats")),
								new Integer(rs.getInt("soldFixedSeats")) };
					}

				});
	}

	private String[] getFlightDetails(Integer flightId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (String[]) template.query(getQuery(FLIGHT_DETAILS_FOR_FLIGHT_ID, new String[] { flightId.toString() }),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						rs.next();

						return new String[] { rs.getString("flight_number"), rs.getString("departure_date") };
					}

				});
	}

	@SuppressWarnings("unchecked")
	private Map<String, int[]> getTotalSoldAndOnholdFixedAndInfSeatsLogicalCabinClass(Integer flightId, String segmentCode,
			String cabinClass) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Map<String, int[]>) template.query(
				getQuery(TOATL_SOLD_SEAT_COUNT_LOGICAL_CABIN_CLASS_WISE, new String[] { flightId.toString(), segmentCode,
						cabinClass, Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) }),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, int[]> logicalCabinClassWiseSoldSeatCount = new HashMap<String, int[]>();
						while (rs.next()) {
							logicalCabinClassWiseSoldSeatCount.put(rs.getString("logicalCabinClass"),
									new int[] { rs.getInt("sof"), rs.getInt("soi") });

						}
						return logicalCabinClassWiseSoldSeatCount;
					}
				});
	}

	@Override
	public Integer getAvailableFixedSeatCount(Integer fccSegInvId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (Integer) template
				.query(getQuery(
						AVAILABLE_FIXED_SEAT_COUNT,
						new String[] { fccSegInvId.toString(), Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) }),
						new ResultSetExtractor() {

							@Override
							public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
								rs.next();

								return new Integer(rs.getInt("availableFixedSeat"));
							}

						});
	}

	@Override
	public int getFlightLoadFactor(Integer flightSegmentId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String query = " SELECT ROUND(sum((fccsa.sold_seats + fccsa.on_hold_seats) * 100)/ "
				+ "sum(fccsa.allocated_seats+ fccsa.oversell_seats- fccsa.curtailed_seats)) AS load_factor "
				+ " FROM t_fcc_seg_alloc fccsa  WHERE fccsa.flt_seg_id = ?";

		return jdbcTemplate.queryForObject(query, new Object[] { flightSegmentId }, Integer.class);
	}

	public Map<Integer, BigDecimal> getFlightLoadFactor(List<Integer> flightSegmentIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String query = " SELECT fccsa.flt_seg_id flt_seg_id, ROUND(sum((fccsa.sold_seats + fccsa.on_hold_seats) * 100)/ "
				+ "     sum(fccsa.allocated_seats+ fccsa.oversell_seats- fccsa.curtailed_seats)) AS load_factor "
				+ " FROM t_fcc_seg_alloc fccsa  WHERE fccsa.flt_seg_id IN ( "
				+ Util.buildIntegerInClauseContent(flightSegmentIds) + " ) " + " group by fccsa.flt_seg_id ";

		return (Map<Integer, BigDecimal>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				Map<Integer, BigDecimal> temp = new HashMap<Integer, BigDecimal>();

				while (resultSet.next()) {
					temp.put(resultSet.getInt("flt_seg_id"), resultSet.getBigDecimal("load_factor"));
				}

				return temp;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Integer> getInterceptingSegmentIDs(List<Integer> sourceFltSegIds) {
		StringBuffer query = new StringBuffer();
		query.append("SELECT fs.flt_seg_id"
				+ " FROM t_flight_segment fs,t_fcc_seg_alloc fsa "
				+ "WHERE fs.flt_seg_id = fsa.flt_seg_id AND fsa.fccsa_id   IN"
				+ " (SELECT ifsa.intetercepting_fccsa_id FROM t_flight_segment fs,t_fcc_seg_alloc fsa,t_intercepting_fcc_seg_alloc ifsa"
				+ "  WHERE fs.flt_seg_id = fsa.flt_seg_id AND fsa.fccsa_id = ifsa.source_fccsa_id AND fs.flt_seg_id IN(");
		query.append(Util.buildIntegerInClauseContent(sourceFltSegIds));
		query.append("))");

		JdbcTemplate jt = new JdbcTemplate(getDataSource());

		List<Integer> interceptingFlightSegIds = (List<Integer>) jt.query(query.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> interceptingFlightSegIds = new ArrayList<Integer>();
				while (rs.next()) {
					interceptingFlightSegIds.add(rs.getInt("flt_seg_id"));

				}
				return interceptingFlightSegIds;
			}
		});
		return interceptingFlightSegIds;
	}

	public Map<Integer, Set<Integer>> getInterceptingFlightSegmentIds(final List<Integer> sourceFltSegIds) {

		Map<Integer, Set<Integer>> interceptingSegs;
		StringBuffer query = new StringBuffer();
		query.append("   SELECT FSA_1.FLT_SEG_ID FLIGHT_SEG, FSA_2.FLT_SEG_ID INTERCEPT_FLIGHT_SEG  ")
				.append("   FROM T_INTERCEPTING_FCC_SEG_ALLOC ISA,  ")
				.append("       T_FCC_SEG_ALLOC FSA_1, T_FCC_SEG_ALLOC FSA_2    ")
				.append("   WHERE ISA.SOURCE_FCCSA_ID = FSA_1.FCCSA_ID  ")
				.append("       AND ISA.INTETERCEPTING_FCCSA_ID = FSA_2.FCCSA_ID    ")
				.append("       AND FSA_1.FLT_SEG_ID IN ( ").append(Util.buildIntegerInClauseContent(sourceFltSegIds))
				.append(" )");

		JdbcTemplate jt = new JdbcTemplate(getDataSource());

		interceptingSegs = (Map<Integer, Set<Integer>>) jt.query(query.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Set<Integer>> tempVal = new HashMap<Integer, Set<Integer>>();
				int flightSeg;

				for (Integer flightSegId : sourceFltSegIds) {
					tempVal.put(flightSegId, new HashSet<Integer>());
				}

				while (rs.next()) {
					flightSeg = rs.getInt("FLIGHT_SEG");
					tempVal.get(flightSeg).add(rs.getInt("INTERCEPT_FLIGHT_SEG"));
				}
				return tempVal;
			}
		});
		return interceptingSegs;
	}

	private String getLogicalCabinClassClauseConsideringInventoryRestriction(String logicalCabinClass) {
		if (AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
			return " AND logcc.logical_cabin_class_code = '" + logicalCabinClass + "' ";
		} else {
			return " AND logcc.cabin_class_code in (select cabin_class_code from t_logical_cabin_class where logical_cabin_class_code = '"
					+ logicalCabinClass + "') ";
		}
	}

	@Override
	public DefaultServiceResponse getAffectedFlights(Collection<Integer> flightIds,
			Collection<AircraftCabinCapacity> cabinClassesUpdated) {
		int chunkSize = AirinventoryCustomConstants.DB_OPERATION_CHUNK_SIZE;
		Object[] flightIdsArr = flightIds.toArray();
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Collection<String> singleLegSegmentCodes = null;

		// final Collection<Integer> oversellFlights = new ArrayList<Integer>();
		Collection<String> overbookFlights = new ArrayList<String>();
		for (final AircraftCabinCapacity aircraftCabinCapacity : cabinClassesUpdated) {
			for (int page = 0; page < flightIds.size(); page += chunkSize) {

				ArrayList<Integer> pagedFlightIds = new ArrayList<Integer>();
				for (int i = page; i < page + chunkSize; i++) {
					if (i < flightIdsArr.length) {
						pagedFlightIds.add((Integer) flightIdsArr[i]);
					} else {
						break;
					}
				}

				if (pagedFlightIds.size() != 0) {
					final List<Integer> fpagedFlightIds = pagedFlightIds;

					// Check adult and infant seat constrain
					for (Integer flightId : pagedFlightIds) {
						if (!cabinClassesUpdated.isEmpty()) {
							List<String> flightSegmentCodes = this.getFlightSegmentCodes(flightId);
							singleLegSegmentCodes = AirinventoryUtils.buildSingleLegSegmentCodes(flightSegmentCodes);
						}
						int[] soldAdultInfSeatCount = getMaxSoldSeatCountForFlight(flightId, singleLegSegmentCodes,
								aircraftCabinCapacity.getCabinClassCode());

						/*
						 * if (soldAdultInfSeatCount[1] > aircraftCabinCapacity.getInfantCapacity()) { String[]
						 * getFlightDetails = getFlightDetails(flightId); overbookFlights.add(getFlightDetails[0] + "|"
						 * + getFlightDetails[1].substring(0, 10) + "|" + aircraftCabinCapacity.getCabinClassCode());
						 * 
						 * }
						 */
						if (soldAdultInfSeatCount[0] - soldAdultInfSeatCount[5] > 0) {
							if (soldAdultInfSeatCount[4] + soldAdultInfSeatCount[0] + soldAdultInfSeatCount[2] > aircraftCabinCapacity
									.getSeatCapacity()) {

								String[] getFlightDetails = getFlightDetails(flightId);
								overbookFlights.add(getFlightDetails[0] + "|" + getFlightDetails[1].substring(0, 10) + "|"
										+ aircraftCabinCapacity.getCabinClassCode());

							} else {
								if (soldAdultInfSeatCount[0] + soldAdultInfSeatCount[2] > aircraftCabinCapacity.getSeatCapacity()) {
									String[] getFlightDetails = getFlightDetails(flightId);
									overbookFlights.add(getFlightDetails[0] + "|" + getFlightDetails[1].substring(0, 10) + "|"
											+ aircraftCabinCapacity.getCabinClassCode());

								}
							}
						}
					}

				}
			}
		}
		DefaultServiceResponse returnResponce = new DefaultServiceResponse(true);
		int a = 0;

		if (overbookFlights.size() > 0) {
			StringBuilder strOverbookFlights = new StringBuilder();
			strOverbookFlights.append("<b>Flights(FlightsOverbook):</b> ");
			for (String flightId : overbookFlights) {
				a++;
				if (a % 8 == 0) {
					strOverbookFlights.append(flightId + ",   ");
				} else if (a == overbookFlights.size()) {
					strOverbookFlights.append(flightId);
				} else {
					strOverbookFlights.append(flightId + ",");
				}
			}
			returnResponce.addResponceParam("overbookFlights", strOverbookFlights.toString());
		}

		return returnResponce;
	}

	@Override
	public int[] getEffectiveAvailableFixedSeatCount(Integer fccSegInvId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		return (int[]) template
				.query(getQuery(
						AVAILABLE_EFFECTIVE_FIXED_SEAT_COUNT,
						new String[] { fccSegInvId.toString(), Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)) }),
						new ResultSetExtractor() {

							@Override
							public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
								rs.next();

								return new int[] { new Integer(rs.getInt("allocatedFixedSeats")),
										new Integer(rs.getInt("soldFixedSeats")) };
							}

						});
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FlightChangeInfo> getFlightSeatChangeData(FlightSegement seg, String modelNumber) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		StringBuilder tempQuery = new StringBuilder();
		Collection<FlightChangeInfo> flightInsData = null;

		tempQuery.append(" SELECT DISTINCT tpp.pnr as pnr,  tpp.c_first_name  ||' '  || tpp.c_last_name AS name, ");
		tempQuery
				.append("  c_email as email,  f.model_number, (SELECT seatmodel.seat_code  FROM sm_t_aircraft_model_seats seatmodel  WHERE  ");
		tempQuery.append(" seatmodel.model_number='" + modelNumber
				+ "' AND seatmodel.seat_code     =modseat.seat_code) as seat_code ,  modseat.seat_code    AS ");
		tempQuery
				.append(" old_seat_code,  modseat.model_number AS old_model FROM sm_t_pnr_pax_seg_am_seat pss INNER JOIN sm_t_flight_am_seat fas ON ");
		tempQuery
				.append(" fas.flight_am_seat_id=pss.flight_am_seat_id INNER JOIN sm_t_am_seat_charge sc ON sc.am_seat_id=fas.am_seat_id INNER JOIN ");
		tempQuery
				.append(" t_flight_segment tfs ON tfs.flt_seg_id=fas.flt_seg_id INNER JOIN sm_t_aircraft_model_seats modseat ON ");
		tempQuery
				.append(" modseat.am_seat_id=fas.am_seat_id INNER JOIN t_flight f ON f.flight_id=tfs.flight_id INNER JOIN t_pnr_segment tpseg ");
		tempQuery
				.append(" ON tpseg.pnr_seg_id =pss.pnr_seg_id INNER JOIN t_reservation_contact tpp ON tpp.pnr=tpseg.pnr WHERE modseat.model_number!=f.model_number and ");

		tempQuery.append("  tfs.flt_seg_id=" + seg.getFltSegId());

		flightInsData = (Collection<FlightChangeInfo>) template.query(tempQuery.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightChangeInfo> insData = new ArrayList<FlightChangeInfo>();
				while (rs.next()) {
					FlightChangeInfo insDataObj = new FlightChangeInfo();
					insDataObj.setPnr(rs.getString("PNR"));
					insDataObj.setEmail(rs.getString("EMAIL"));
					insDataObj.setReservationName(rs.getString("NAME"));
					insDataObj.setSeatNumber(rs.getString("OLD_SEAT_CODE"));

					insData.add(insDataObj);
				}
				return insData;
			}
		});
		return flightInsData;
	}
}
