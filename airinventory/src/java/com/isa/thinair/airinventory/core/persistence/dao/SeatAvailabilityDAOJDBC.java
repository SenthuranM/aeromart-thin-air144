package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.LightBCAvailability;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;

/**
 * @author Nilindra Fernando
 */
public interface SeatAvailabilityDAOJDBC {

	public AvailableFlightSegment flightSegmentAvailabilitySearch(AvailableFlightSegment flightSegment,
			BucketFilteringCriteria bucketFilteringCriteria) throws ModuleException;

	public FilteredBucketsDTO fileterBucketsForFlightSegment(BucketFilteringCriteria bucketFilteringCriteria,
			AvailableFlightSegment availableFlightSegment) throws ModuleException;

	// AllFaresBCInventoriesSummaryDTOs
	public LinkedHashMap<String, AllFaresBCInventorySummaryDTO> getAllBucketFareCombinationsForSegment(
			BucketFilteringCriteria criteria) throws ModuleException;

	public Map<String, List<Integer>> getAvailableNonFixedAndFixedSeatsCounts(int flightId, String segmentCode,
			String cabinClassCode, String logicalCabinClassCode, String bookingClassCode) throws ModuleException;

	public Map<String, List<Integer>> getAvailableNonFixedAndFixedSeatsCounts(int fccsaId, String bookingClassCode)
			throws ModuleException;

	public Object[] quoteGoshowFare(ReconcilePassengersTO goshowFQCriteria);

	public FareSummaryDTO quoteFareForExternalBookingChannel(ReconcilePassengersTO passengersTO, BookingClass gdsBookingClass);

	public Map<Integer, List<Pair<String, Integer>>> getBcAvailabilityForMyIDTravel(List<Integer> flightSegIds, String agentCode);

	public boolean isValidConnectionRoute(String segmentCode);

	public LightBCAvailability getSegmentBCAvailability(String bookingCode, boolean standardBC, Integer flightSegId);

}
