package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Interface for hibernate calls for manipulating Booking Class.
 * 
 * @author Thejaka
 * @author Nasly
 */
public interface BookingClassDAO {

	public BookingClass getBookingClass(String code);

	public BookingClass getLightWeightBookingClass(String code);

	public Map<String, Collection<String>> getBookingClassChargeGroups(Collection<String> bookingClasses,
			Collection<String> busBookingClasses);

	public BookingClass getGoShowBookingClass(String cabinClassCode, String paxCategoryCode);

	public Collection<BookingClass> getBookingClasses(Collection<ModuleCriterion> criteria);

	public HashMap<String, BookingClass> getBookingClassesMap(Collection<String> bookingCodes);

	public Collection<String> getStandbyBookingClasses();

	public Collection<Object[]> getBookingCodesWithFlags(String cabinClassCode);

	public Collection<Object[]> getBookingCodesWithFlagsForLogicalCC(Collection<String> logicalCCCodes,
			Collection<String> bcListWithinFlightCutoffTime);

	public BookingClassDTO getBookingClassDTO(String bookingCode) throws ModuleException;

	public Page getBookingClassDTOs(SearchBCsCriteria criteria) throws ModuleException;

	public void saveBookingClass(BookingClass bookingClass);

	public void createBookingClass(BookingClassDTO bookingClassDTO);

	public void updateBookingClass(BookingClassDTO bookingClassDTO) throws ModuleException;

	public void updateBCGroupIds(List<String> bcCodes, int groupId);

	public void deleteBookingClass(String bookingClassCode);

	public Collection<BookingClass> getFixedBookingClassesToRelease();

	/**
	 * Returns all the booking classes linked to all gdsIds
	 * 
	 * @param gdsIds
	 * @return Map<Integer, Collection<BookingClass>> => <gdsId, bookingClassesCollection>
	 */
	public Map<Integer, Collection<BookingClass>> getGDSBookingClasses(Collection<Integer> gdsIds);

	public BookingClass getOpenReturnBookingClass(String logicalCabinClassCode);

	public Collection<String> getSingleCharBookingClassesForGDS(int gdsId);

	public String getCabinClassForBookingClass(String bookingClass);

	public List<BookingClass> getAllGoshowBookingClasses();

	public Collection<BookingClass> getGoshowBCListForAgentBookingsWithinCutoffTime(Collection<String> bookingCodes);

	public String getBookingClassType(String bookingCode);

	public String getBookingClassChargeFlags(String bookingCode);

	public List<String> getInactiveGdsBookingClasse();
	
	public Collection<BookingClass> getBookingClasses();
}
