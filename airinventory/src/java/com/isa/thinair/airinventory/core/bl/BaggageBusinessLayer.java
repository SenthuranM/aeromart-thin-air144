package com.isa.thinair.airinventory.core.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.model.FlightBaggage;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BaggageJDBCDAO;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author mano
 */
public class BaggageBusinessLayer implements Serializable {

	private static final long serialVersionUID = 7500043051722330473L;

	private static final Log log = LogFactory.getLog(BaggageBusinessLayer.class);

	private BaggageDAO baggageDAO = null;

	public BaggageBusinessLayer() {
		if (baggageDAO == null) {
			this.baggageDAO = AirInventoryModuleUtils.getBaggageDAO();
		}
	}

	private BaggageJDBCDAO getBaggageJDBCDAO() {
		return (BaggageJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("baggageJDBCDAO");
	}

	public Map<Integer, List<FlightBaggageDTO>> getBaggagesWithTranslations(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String selectedLanguage, boolean skipCutoverValidation) throws ModuleException {

		selectedLanguage = BeanUtils.nullHandler(selectedLanguage);
		Map<Integer, List<FlightBaggageDTO>> flightSegBaggageMap = new HashMap<Integer, List<FlightBaggageDTO>>();
		Collection<FlightSegmentDTO> colSegs = AirInventoryModuleUtils.getFlightBD().getFlightSegments(
				flightSegIdWiseCos.keySet());

		for (FlightSegmentDTO fltSeg : colSegs) {
			Integer flightSegId = fltSeg.getSegmentId();
			log.debug("flightSegID for Baggage selection " + flightSegId);

			boolean isWithInFinalCutOver = fltSeg.getDepartureDateTimeZulu().after(CalendarUtil.getCurrentZuluDateTime())
					&& BaggageTimeWatcher.isWithInFinalCutOver(fltSeg.getDepartureDateTimeZulu());

			if (!isWithInFinalCutOver || skipCutoverValidation) {

				String cabinClass = null;
				String logicalCabinClass = null;
				ClassOfServiceDTO logicalCCDto = flightSegIdWiseCos.get(flightSegId);

				if (logicalCCDto != null) {
					cabinClass = logicalCCDto.getCabinClassCode();
					logicalCabinClass = logicalCCDto.getLogicalCCCode();
				}

				if (selectedLanguage.length() == 0) {
					List<FlightBaggageDTO> baggages = (List<FlightBaggageDTO>) getBaggageJDBCDAO().getBaggageDTOs(flightSegId,
							cabinClass, logicalCabinClass);
					flightSegBaggageMap.put(flightSegId, baggages);
				} else {
					List<FlightBaggageDTO> baggages = (List<FlightBaggageDTO>) getBaggageJDBCDAO()
							.getBaggageDTOsWithTranslations(flightSegId, cabinClass, logicalCabinClass, selectedLanguage);
					flightSegBaggageMap.put(flightSegId, baggages);
				}

			} else {
				List<FlightBaggageDTO> baggages = new ArrayList<FlightBaggageDTO>();
				flightSegBaggageMap.put(flightSegId, baggages);
			}
		}

		return flightSegBaggageMap;

	}

	public void updateFlightBaggages(Collection<Integer> flightBaggageIds, String status) throws ModuleException {

		Collection<FlightBaggage> dbflightbaggages = baggageDAO.getFlightBaggages(flightBaggageIds);
		if (dbflightbaggages == null || dbflightbaggages.size() < 1) {
			throw new ModuleException("airinventory.baggages.notfound");
		}
		Integer selFltBgId = null;
		for (Integer integer : flightBaggageIds) {
			selFltBgId = integer;
			for (FlightBaggage fltBaggage : dbflightbaggages) {
				if (selFltBgId.intValue() == fltBaggage.getFlightBaggageChargeId()) {
					if (status.equals(AirinventoryCustomConstants.FlightBaggageStatuses.RESERVED)) {
						fltBaggage.setSoldPieces(fltBaggage.getSoldPieces() + 1);
						// fltBaggage.setUserDetails(userPrincipal);
					} else if (status.equals(AirinventoryCustomConstants.FlightBaggageStatuses.VACANT)) {
						fltBaggage.setSoldPieces(fltBaggage.getSoldPieces() - 1);
						// fltBaggage.setUserDetails(userPrincipal);
					}
					baggageDAO.saveFlightBaggage(fltBaggage);
				}
			}
		}

	}

	/**
	 * 
	 * @param sourceFlightId
	 * @param userPrincipal
	 * 
	 * @param destFlightIds
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce assignFlightBaggageChargesRollFoward(int sourceFlightId, Collection<Integer> destFlightIds,
			UserPrincipal userPrincipal, ArrayList<String> lstSelectedSeg, String classOfService) throws ModuleException {
		// 1.Get srctemplateID map <segId ,FlightBaggageDTO>
		// 2.Get desSegId col <Collection <flightSegIds> >
		// 3.Assign templates

		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();
		DefaultServiceResponse res = new DefaultServiceResponse(false);
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIds = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsFailed = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsNewlyAssigned = new HashMap<String, Collection<Integer>>();

		// 1. getting templateIDs for src flight
		Collection<FlightBaggageDTO> colGetTemplateIds = getBaggageJDBCDAO().getTempleateIds(sourceFlightId);
		Map<String, FlightBaggageDTO> mapSegIdFlightBaggageDTO = buildMapWithSegCode(colGetTemplateIds);

		// 2. getting all flightSegIds between roll period
		Collection<FlightBaggageDTO> colFlightSegIds = getFlightBD().getFlightSegIdsforFlightforBaggage(destFlightIds);
		Map<String, Collection<Integer>> mapSegCodeColSegIds = buildMapWithSegCodeColSegIds(colFlightSegIds);
		// 3.
		Iterator<String> itrSegmentCode = mapSegIdFlightBaggageDTO.keySet().iterator();
		while (itrSegmentCode.hasNext()) {
			String segCode = itrSegmentCode.next().toString();
			if (lstSelectedSeg.contains(segCode)) {
				lstSelectedSeg.remove(segCode);
			}
			FlightBaggageDTO flightBaggageDTO = mapSegIdFlightBaggageDTO.get(segCode);
			ServiceResponce sr = assignFlightBaggageCharges(mapSegCodeColSegIds.get(segCode), flightBaggageDTO.getTemplateId(),
					userPrincipal, classOfService, null, false);
			arrAssigned.addAll((Collection<Integer>) sr
					.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED));
			arrNotAssigned.addAll((Collection<Integer>) sr
					.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED));
			arrNewlyAssigned.addAll((Collection<Integer>) sr
					.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED));
			if (arrAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIds.put(Integer.toString(flightBaggageDTO.getTemplateId()),
						(Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED));
			}
			if (arrNotAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(flightBaggageDTO.getTemplateId()),
						(Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED));
			}
			if (arrNewlyAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(flightBaggageDTO.getTemplateId()),
						(Collection<Integer>) sr.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED));
			}
		}

		if (lstSelectedSeg != null && !lstSelectedSeg.isEmpty()) {
			Iterator<String> iter = lstSelectedSeg.iterator();
			while (iter.hasNext()) {
				String segCode = iter.next().toString();
				if (mapSegCodeColSegIds.containsKey(segCode)) {
					Integer templateId = new Integer(0);
					ServiceResponce sr = assignFlightBaggageCharges(mapSegCodeColSegIds.get(segCode), templateId, userPrincipal,
							classOfService, null, false);
					arrAssigned.addAll((Collection<Integer>) sr
							.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED));
					arrNotAssigned.addAll((Collection<Integer>) sr
							.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED));
					arrNewlyAssigned.addAll((Collection<Integer>) sr
							.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED));
					if (arrAssigned.size() > 0) {
						mapAuditTemplateIDcolSegIds.put(Integer.toString(templateId), (Collection<Integer>) sr
								.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED));
					}
					if (arrNotAssigned.size() > 0) {
						mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(templateId), (Collection<Integer>) sr
								.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED));
					}
					if (arrNewlyAssigned.size() > 0) {
						mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(templateId), (Collection<Integer>) sr
								.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED));
					}

				}

			}
		}

		// Audit record
		Map<String, Map<String, Collection<Integer>>> map = new HashMap<String, Map<String, Collection<Integer>>>();
		map.put(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED, mapAuditTemplateIDcolSegIds);
		map.put(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED, mapAuditTemplateIDcolSegIdsFailed);
		map.put(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED, mapAuditTemplateIDcolSegIdsNewlyAssigned);
		AuditAirinventory.doAuditBaggageRollFowardBaggageCharges(map, userPrincipal);

		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	private Map<String, FlightBaggageDTO> buildMapWithSegCode(Collection<FlightBaggageDTO> colTemplateIds) {
		Map<String, FlightBaggageDTO> mapSegCodeTempIds = new HashMap<String, FlightBaggageDTO>();
		Iterator<FlightBaggageDTO> itrFlightBaggage = colTemplateIds.iterator();
		while (itrFlightBaggage.hasNext()) {
			FlightBaggageDTO flightBaggageDTO = itrFlightBaggage.next();
			mapSegCodeTempIds.put(flightBaggageDTO.getFlightSegmentCode(), flightBaggageDTO);
		}
		return mapSegCodeTempIds;
	}

	private Map<String, Collection<Integer>> buildMapWithSegCodeColSegIds(Collection<FlightBaggageDTO> flightSegIds) {
		Map<String, Collection<Integer>> mapSegCodeColSegIds = new HashMap<String, Collection<Integer>>();
		Iterator<FlightBaggageDTO> itrFlightSegs = flightSegIds.iterator();
		while (itrFlightSegs.hasNext()) {
			FlightBaggageDTO flightbaggageDTO = itrFlightSegs.next();
			addToCollectionOfMap(mapSegCodeColSegIds, flightbaggageDTO);
		}
		return mapSegCodeColSegIds;
	}

	private void addToCollectionOfMap(Map<String, Collection<Integer>> map, FlightBaggageDTO flightbaggageDTO) {
		if (map.containsKey(flightbaggageDTO.getFlightSegmentCode())) {
			Collection<Integer> colExisting = map.get(flightbaggageDTO.getFlightSegmentCode());
			colExisting.add(flightbaggageDTO.getFlightSegmentID());
			map.put(flightbaggageDTO.getFlightSegmentCode(), colExisting);
		} else {
			Collection<Integer> col = new HashSet<Integer>();
			col.add(flightbaggageDTO.getFlightSegmentID());
			map.put(flightbaggageDTO.getFlightSegmentCode(), col);
		}
	}

	private FlightBD getFlightBD() {
		return AirInventoryModuleUtils.getFlightBD();
	}

	/**
	 * @param flightBaggageDTOs
	 * @param userPrincipal
	 * @param classOfService
	 *            TODO
	 * @return ServiceResponce
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce assignFlightBaggageCharges(Collection<FlightBaggageDTO> flightBaggageDTOs,
			UserPrincipal userPrincipal, Integer chargeId, boolean isFromEdit, String classOfService) throws ModuleException {

		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		Iterator<FlightBaggageDTO> Itr = flightBaggageDTOs.iterator();
		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();
		Map<Integer, Collection<Integer>> mapFlightBaggages = new HashMap<Integer, Collection<Integer>>();
		Map<Integer, Set<String>> mapTemplateCcCode = new HashMap<Integer, Set<String>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIds = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsFailed = new HashMap<String, Collection<Integer>>();
		Map<String, Collection<Integer>> mapAuditTemplateIDcolSegIdsNewlyAssigned = new HashMap<String, Collection<Integer>>();

		while (Itr.hasNext()) {
			FlightBaggageDTO flightBaggageDTO = Itr.next();

			if (mapFlightBaggages.containsKey(flightBaggageDTO.getTemplateId())) {
				Collection<Integer> flightSegIDs = mapFlightBaggages.get(flightBaggageDTO.getTemplateId());
				String cabinClsCode = flightBaggageDTO.getCabinClassCode();
				flightSegIDs.add(flightBaggageDTO.getFlightSegmentID());
				mapFlightBaggages.put(flightBaggageDTO.getTemplateId(), flightSegIDs);
				mapTemplateCcCode.get(flightBaggageDTO.getTemplateId()).add(cabinClsCode);
			} else {
				Collection<Integer> flightSegIDs = new ArrayList<Integer>();
				String cabinClsCode = flightBaggageDTO.getCabinClassCode();
				flightSegIDs.add(flightBaggageDTO.getFlightSegmentID());
				mapFlightBaggages.put(flightBaggageDTO.getTemplateId(), flightSegIDs);
				Set<String> ccSet = new HashSet<String>();
				ccSet.add(cabinClsCode);
				mapTemplateCcCode.put(flightBaggageDTO.getTemplateId(), ccSet);
			}
		}

		for (Integer integer : mapFlightBaggages.keySet()) {
			Integer templateID = integer;
			Collection<Integer> colFlightSegIDs = mapFlightBaggages.get(templateID);
			ServiceResponce serRes = assignFlightBaggageCharges(colFlightSegIDs, templateID, userPrincipal, classOfService,
					chargeId, isFromEdit);
			arrAssigned.addAll((Collection<Integer>) serRes
					.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED));
			arrNotAssigned.addAll((Collection<Integer>) serRes
					.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED));
			arrNewlyAssigned.addAll((Collection<Integer>) serRes
					.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED));
			if (arrAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIds.put(Integer.toString(templateID),
						(Collection<Integer>) serRes.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED));
			}
			if (arrNotAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsFailed.put(Integer.toString(templateID), (Collection<Integer>) serRes
						.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED));
			}
			if (arrNewlyAssigned.size() > 0) {
				mapAuditTemplateIDcolSegIdsNewlyAssigned.put(Integer.toString(templateID), (Collection<Integer>) serRes
						.getResponseParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED));
			}
		}
		Map<String, Map<String, Collection<Integer>>> map = new HashMap<String, Map<String, Collection<Integer>>>();
		map.put(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED, mapAuditTemplateIDcolSegIds);
		map.put(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED, mapAuditTemplateIDcolSegIdsFailed);
		map.put(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED, mapAuditTemplateIDcolSegIdsNewlyAssigned);
		AuditAirinventory.doAuditBaggageAssignBaggageCharges(map, userPrincipal);
		serviceResponse.setSuccess(true);
		serviceResponse.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED, arrAssigned);
		serviceResponse.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED, arrNotAssigned);
		serviceResponse.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return serviceResponse;
	}

	/**
	 * 
	 * @param colFlightSegmentIDs
	 * @param templateID
	 * @param userPrincipal
	 * @param classOfService
	 * @param chargeId
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unused")
	private ServiceResponce assignFlightBaggageCharges(Collection<Integer> colFlightSegmentIDs, Integer templateID,
			UserPrincipal userPrincipal, String classOfService, Integer chargeId, boolean isFromEdit) throws ModuleException {
		DefaultServiceResponse res = new DefaultServiceResponse(false);
		ArrayList<Integer> arrNotAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrAssigned = new ArrayList<Integer>();
		ArrayList<Integer> arrNewlyAssigned = new ArrayList<Integer>();

		// blank template
		if (templateID == null || templateID == 0) {
			Iterator<Integer> itrFlightSegIDs = colFlightSegmentIDs.iterator();
			while (itrFlightSegIDs.hasNext()) {
				int flightSegID = (itrFlightSegIDs.next()).intValue();

				Collection<FlightBaggage> exstFlightBaggage = getBaggageDAO().getFlightBaggages(flightSegID, classOfService);

				if (exstFlightBaggage != null && !exstFlightBaggage.isEmpty()) {
					Collection<FlightBaggage> flightBaggages = new ArrayList<FlightBaggage>();
					Iterator<FlightBaggage> itr = exstFlightBaggage.iterator();
					while (itr.hasNext()) {
						FlightBaggage flightBaggage = itr.next();
						flightBaggage.setBaggageChargeId(null);
						flightBaggage.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						flightBaggage.setUserDetails(userPrincipal);
						flightBaggage.setCabinClassCode(classOfService);
						flightBaggages.add(flightBaggage);
						flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
					}
					getBaggageDAO().saveFlightBaggage(flightBaggages);
					arrAssigned.add(flightSegID);
				}
			}
			res.setSuccess(true);
			res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED, arrAssigned);
			res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED, arrNotAssigned);
			res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED, arrNewlyAssigned);
			return res;
		}

		BaggageTemplate baggageTemplate = getBaggageTemplate(templateID);
		Collection<BaggageCharge> allBaggageCharges = null;
		if (chargeId == null) {
			allBaggageCharges = getBaggageCharges(templateID);
		} else {
			allBaggageCharges = getBaggageChargeForTemplate(templateID, chargeId);
		}

		Collection<Integer> colBaggageIds = new ArrayList<Integer>();

		for (BaggageCharge baggageCharge : allBaggageCharges) {
			colBaggageIds.add(baggageCharge.getBaggageId());
		}
		Collection<Baggage> baggages = AirInventoryModuleUtils.getCommonMasterBD().getBaggages(colBaggageIds);
		Collection<BaggageCharge> baggageCharges = null;
		if (classOfService != null) {
			baggageCharges = getBaggageChargesForCabinClass(allBaggageCharges, classOfService);
		} else {
			baggageCharges = allBaggageCharges;
		}

		if (allBaggageCharges.isEmpty()) {
			throw new ModuleException("airinventory.logic.bl.flightmealcharge.failed.countmismatch",
					AirinventoryCustomConstants.INV_MODULE_CODE);
		}

		Iterator<Integer> flightSegmentIDs = colFlightSegmentIDs.iterator();
		while (flightSegmentIDs.hasNext()) {
			int flightSegID = (flightSegmentIDs.next()).intValue();
			boolean isBaggageSold = false;
			boolean isEmptyBaggageChargeTemplate = false;
			boolean hasSoldBaggage = false;
			boolean hasSoldBGEmptyTemp = false;
			Map<Integer, Integer> mapBaggageIdSoldCnt = null;

			Collection<FlightBaggage> exFltBaggagesCharges = null;
			if (chargeId == null && classOfService == null) {
				exFltBaggagesCharges = getBaggageDAO().getFlightBaggages(flightSegID);
			} else if (chargeId == null) {
				exFltBaggagesCharges = getBaggageDAO().getFlightBaggages(flightSegID, classOfService);
			} else {
				exFltBaggagesCharges = getBaggageDAO().getFlightBaggages(flightSegID, chargeId.intValue(), classOfService);
			}

			if (exFltBaggagesCharges != null && !exFltBaggagesCharges.isEmpty()) {
				List<Integer> soldBaggageChargeList = new ArrayList<Integer>();
				Collection<BaggageCharge> soldBaggageCharges = null;
				Map<Integer, BaggageCharge> soldBaggageMapByBaggage = new HashMap<Integer, BaggageCharge>();

				for (FlightBaggage flightBaggage : exFltBaggagesCharges) {
					if (flightBaggage.getBaggageChargeId() == null) {
						isEmptyBaggageChargeTemplate = true;
						if (flightBaggage.getSoldPieces() > 0) {
							hasSoldBGEmptyTemp = true;// has sold baggages, but
							// existing seg has no
							// template
						}
					} else {
						if (flightBaggage.getSoldPieces() > 0) {
							soldBaggageChargeList.add(flightBaggage.getBaggageChargeId());
							hasSoldBaggage = true; // assign true, when having at
							// least one sold baggages in
							// existing set
						}
					}

				}

				if (soldBaggageChargeList.size() > 0) {
					soldBaggageCharges = getChargeBD().getBaggageCharges(soldBaggageChargeList);
					if (soldBaggageCharges != null) {
						for (BaggageCharge soldBaggageCharge : soldBaggageCharges) {
							soldBaggageMapByBaggage.put(soldBaggageCharge.getBaggageId(), soldBaggageCharge);
						}
					}
				}

				if (hasSoldBGEmptyTemp) {
					mapBaggageIdSoldCnt = getSoldBaggageCountMap(flightSegID);
				}

				if (isEmptyBaggageChargeTemplate) {
					for (FlightBaggage exFlightBaggage : exFltBaggagesCharges) {
						exFlightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
					}

					Collection<FlightBaggage> flightBaggages = new ArrayList<FlightBaggage>();
					int cntSoldBaggages = 0;
					int cntAllocatedBaggages = 0;
					for (BaggageCharge baggageCharge : baggageCharges) {
						// Skip adding baggages in existing template
						if (soldBaggageMapByBaggage.containsKey(baggageCharge.getBaggageId())) {
							continue;
						}

						FlightBaggage flightBaggage = new FlightBaggage();
						flightBaggage.setFlightSegmentId(flightSegID);
						flightBaggage.setBaggageChargeId(baggageCharge.getChargeId());
						flightBaggage.setAmount(baggageCharge.getAmount());
						if (baggageCharge.getCabinClass() != null) {
							flightBaggage.setCabinClassCode(baggageCharge.getCabinClass());
						} else {
							flightBaggage.setLogicalCCCode(baggageCharge.getLogicalCCCode());
						}

						flightBaggage.setUserDetails(userPrincipal);
						cntAllocatedBaggages = baggageCharge.getAllocatedPieces();

						if (mapBaggageIdSoldCnt != null && mapBaggageIdSoldCnt.containsKey(baggageCharge.getBaggageId())) {
							cntSoldBaggages = mapBaggageIdSoldCnt.get(baggageCharge.getBaggageId());
							flightBaggage.setSoldPieces(cntSoldBaggages);

						} else {
							flightBaggage.setSoldPieces(0);
						}
						if (baggageTemplate != null && baggageTemplate.getStatus() != null
								&& baggageTemplate.getStatus().equals(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE)) {
							flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
						} else if (baggageCharge.getStatus().equals(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE)) {
							flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE);
						} else {
							flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
						}
						flightBaggages.add(flightBaggage);

					}
					getBaggageDAO().saveFlightBaggage(flightBaggages);
					arrAssigned.add(flightSegID);
					// continue;
				}

				Map<Integer, List<FlightBaggage>> mapExFlightBaggages = getExFlightBaggages(exFltBaggagesCharges);

				// If baggages were not sold, assign the new charges
				if (!isEmptyBaggageChargeTemplate && !hasSoldBaggage) {
					// Load
					Iterator<BaggageCharge> itr = baggageCharges.iterator();

					Collection<List<FlightBaggage>> fightBaggages = new ArrayList<List<FlightBaggage>>();
					Collection<List<FlightBaggage>> staleFlightBaggages = new ArrayList<List<FlightBaggage>>();
					
					while (itr.hasNext()) {

						BaggageCharge baggageCharge = itr.next();

						List<FlightBaggage> flightBaggageList = null;
						if (mapExFlightBaggages.containsKey(baggageCharge.getChargeId())) {
							flightBaggageList = mapExFlightBaggages.get(baggageCharge.getChargeId());
							
							for (FlightBaggage flightBaggage : flightBaggageList) {
								flightBaggage.setUserDetails(userPrincipal);
								// remove existing flight baggage from the map
							}

							mapExFlightBaggages.remove(baggageCharge.getChargeId());
						} else {
							
							flightBaggageList = new ArrayList<FlightBaggage>();
							for (FlightBaggage flightBaggage : flightBaggageList) {
								flightBaggage.setUserDetails(userPrincipal);
							}
						}

						
						for (FlightBaggage flightBaggage : flightBaggageList) {
							
							flightBaggage.setFlightSegmentId(flightSegID);
							flightBaggage.setBaggageChargeId(baggageCharge.getChargeId());
							flightBaggage.setAmount(baggageCharge.getAmount());
							if (baggageCharge.getCabinClass() != null) {
								flightBaggage.setCabinClassCode(baggageCharge.getCabinClass());
							} else {
								flightBaggage.setLogicalCCCode(baggageCharge.getLogicalCCCode());
							}

							if (baggageTemplate != null && baggageTemplate.getStatus() != null
									&& baggageTemplate.getStatus().equals(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE)) {
								flightBaggage.setSoldPieces(0);
								flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
							} else if (baggageCharge.getStatus().equals(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE)) {
								flightBaggage.setSoldPieces(0);

								flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE);
							} else if (baggageCharge.getStatus().equals("INA")) {
								flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
								flightBaggage.setSoldPieces(0);
								flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
							}
							
						}
						
						fightBaggages.add(flightBaggageList);
						
					}
					if (!mapExFlightBaggages.isEmpty()) {
						Collection<List<FlightBaggage>> colFlightBaggages = mapExFlightBaggages.values();
						Iterator<List<FlightBaggage>> iter = colFlightBaggages.iterator();
						while (iter.hasNext()) {
							List<FlightBaggage> staleFlightBaggage = iter.next();
							
							for (FlightBaggage aStaleFlightBaggage : staleFlightBaggage) {
								aStaleFlightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
							}
							staleFlightBaggages.add(staleFlightBaggage);
						}
					}
					// inactivate stale flight baggages
					getBaggageDAO().saveFlightBaggageMultiList(staleFlightBaggages);
					// save new flight baggages
					getBaggageDAO().saveFlightBaggageMultiList(fightBaggages);
					arrAssigned.add(flightSegID);
				} else if (hasSoldBaggage) {
					// Baggages Sold, override the template.

					Iterator<BaggageCharge> itr = baggageCharges.iterator();
					Collection<List<FlightBaggage>> flightBaggages = new ArrayList<List<FlightBaggage>>();
					Collection<List<FlightBaggage>> staleFlightBaggages = new ArrayList<List<FlightBaggage>>();

					while (itr.hasNext()) {
						BaggageCharge baggageCharge = itr.next();
						List<FlightBaggage> flightBaggageList = null;
						int cntSoldBaggages = 0;
						int cntAvailableBaggages = 0;
						int cntAllocatedBaggages = baggageCharge.getAllocatedPieces();

						if (mapExFlightBaggages.containsKey(baggageCharge.getChargeId())) {
							flightBaggageList = mapExFlightBaggages.get(baggageCharge.getChargeId());
							for (FlightBaggage flightBaggage : flightBaggageList) {
								flightBaggage.setUserDetails(userPrincipal);
							}
							
							// remove existing flight baggage from the map
							mapExFlightBaggages.remove(baggageCharge.getChargeId());
						} else {
							flightBaggageList = new ArrayList<FlightBaggage>();
							for (FlightBaggage flightBaggage : flightBaggageList) {
								flightBaggage.setUserDetails(userPrincipal);
								flightBaggage.setFlightSegmentId(flightSegID);
							}
							
						}

						for (FlightBaggage flightBaggage : flightBaggageList) {
							flightBaggage.setBaggageChargeId(baggageCharge.getChargeId());
							flightBaggage.setAmount(baggageCharge.getAmount());
							if (baggageCharge.getCabinClass() != null) {
								flightBaggage.setCabinClassCode(baggageCharge.getCabinClass());
							} else {
								flightBaggage.setLogicalCCCode(baggageCharge.getLogicalCCCode());
							}
							if (baggageTemplate != null && baggageTemplate.getStatus() != null
									&& baggageTemplate.getStatus().equals(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE)) {
								flightBaggage.setSoldPieces(0);
								flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
							} else if (baggageCharge.getStatus().equals(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE)) {
								if (flightBaggage.getStatus() != null) {
									flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE);
									if (flightBaggage.getSoldPieces() > 0 && cntAllocatedBaggages > 0) {
										cntSoldBaggages = flightBaggage.getSoldPieces();
										cntAvailableBaggages = cntAllocatedBaggages - cntSoldBaggages;
	
									} else {
	
										flightBaggage.setSoldPieces(0);
									}
								} else {
									flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE);
									flightBaggage.setSoldPieces(0);
								}
							} else {
								flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
								flightBaggage.setSoldPieces(0);
							}
						}
						

						flightBaggages.add(flightBaggageList);
					}
					if (!mapExFlightBaggages.isEmpty()) {
						Collection<List<FlightBaggage>> colFlightBaggages = mapExFlightBaggages.values();
						Iterator<List<FlightBaggage>> iter = colFlightBaggages.iterator();
						while (iter.hasNext()) {
							List<FlightBaggage> staleFlightBaggageList = iter.next();
							for (FlightBaggage aStaleFlightBaggage : staleFlightBaggageList) {
								aStaleFlightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
							}
							
							staleFlightBaggages.add(staleFlightBaggageList);
						}
					}
					// inactivate stale flight baggages
					getBaggageDAO().saveFlightBaggageMultiList(staleFlightBaggages);
					// save new flight baggages
					getBaggageDAO().saveFlightBaggageMultiList(flightBaggages);
					arrAssigned.add(flightSegID);
				}

			} else if (!isFromEdit) {
				// create charges for flights
				createChargesForNewFlights(baggageCharges, flightSegID, userPrincipal);
				arrAssigned.add(flightSegID);
				arrNewlyAssigned.add(flightSegID);
			}
		}
		res.setSuccess(true);
		res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.ASSIGNED, arrAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NOTASSIGNED, arrNotAssigned);
		res.addResponceParam(AirinventoryCustomConstants.FlightBaggageCharge.NEWLYASSIGNED, arrNewlyAssigned);
		return res;
	}

	private BaggageDAO getBaggageDAO() {
		return (BaggageDAO) AirInventoryUtil.getInstance().getLocalBean("BaggageDAOImplProxy");
	}

	private BaggageTemplate getBaggageTemplate(Integer templateID) throws ModuleException {
		BaggageTemplate baggageTemplate = getChargeBD().getBaggageTemplate(templateID);
		return baggageTemplate;
	}

	private Collection<BaggageCharge> getBaggageCharges(Integer templateID) throws ModuleException {
		BaggageTemplate baggageTemplate = getChargeBD().getBaggageTemplate(templateID);
		return baggageTemplate.getBaggageCharges();
	}

	private Collection<BaggageCharge> getBaggageChargeForTemplate(Integer templateID, Integer chargeID) throws ModuleException {
		BaggageTemplate baggageTemplate = getChargeBD().getBaggageTemplate(templateID);
		Collection<BaggageCharge> baggageCharges = new ArrayList<BaggageCharge>();

		Iterator<BaggageCharge> itrBaggageCharge = baggageTemplate.getBaggageCharges().iterator();

		while (itrBaggageCharge.hasNext()) {
			BaggageCharge mc = itrBaggageCharge.next();
			if (mc.getChargeId() == chargeID.intValue()) {
				baggageCharges.add(mc);
			}
		}

		if (baggageCharges.size() > 0) {
			return baggageCharges;
		} else {
			return baggageTemplate.getBaggageCharges();
		}
	}

	private ChargeBD getChargeBD() {
		return AirInventoryModuleUtils.getChargeBD();
	}

	private Map<Integer, Integer> getSoldBaggageCountMap(int flightSegID) throws ModuleException {
		Collection<FlightBaggageDTO> flightBaggages = getBaggageJDBCDAO().getSoldBaggages(flightSegID);
		Iterator<FlightBaggageDTO> iter = flightBaggages.iterator();
		Map<Integer, Integer> mapSoldBaggages = new HashMap<Integer, Integer>();

		while (iter.hasNext()) {
			FlightBaggageDTO flightBaggage = iter.next();
			mapSoldBaggages.put(flightBaggage.getBaggageId(), flightBaggage.getSoldPieces());
		}
		return mapSoldBaggages;
	}

	private Map<Integer, List<FlightBaggage>> getExFlightBaggages(Collection<FlightBaggage> exFlightBaggages) {
		Map<Integer, List<FlightBaggage>> map = new HashMap<Integer, List<FlightBaggage>>();
		Iterator<FlightBaggage> itr = exFlightBaggages.iterator();
		while (itr.hasNext()) {
			FlightBaggage flightBaggage = itr.next();
			
			if (!map.containsKey(flightBaggage.getBaggageChargeId())) {
				map.put(flightBaggage.getBaggageChargeId(), new ArrayList<FlightBaggage>());
			}
			
			map.get(flightBaggage.getBaggageChargeId()).add(flightBaggage);
		}
		return map;
	}

	private Collection<BaggageCharge> getBaggageChargesForCabinClass(Collection<BaggageCharge> baggageCharges, String cabinClass)
			throws ModuleException {
		Collection<BaggageCharge> baggageChargesForCC = new ArrayList<BaggageCharge>();
		Map<String, LogicalCabinClassDTO> availableLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		Iterator<BaggageCharge> baggageChargeItr = baggageCharges.iterator();
		while (baggageChargeItr.hasNext()) {
			BaggageCharge baggageCharge = baggageChargeItr.next();
			if (baggageCharge.getCabinClass() != null && cabinClass.equals(baggageCharge.getCabinClass())) {
				baggageChargesForCC.add(baggageCharge);
			} else {
				LogicalCabinClassDTO logicalCCObj = availableLogicalCCMap.get(baggageCharge.getLogicalCCCode());
				if (logicalCCObj != null && logicalCCObj.getCabinClassCode().equals(cabinClass)) {
					baggageChargesForCC.add(baggageCharge);
				}
			}
		}
		return baggageChargesForCC;
	}

	private void
			createChargesForNewFlights(Collection<BaggageCharge> baggageCharges, int flightSegID, UserPrincipal userPrincipal)
					throws ModuleException {
		Iterator<BaggageCharge> itr = baggageCharges.iterator();
		Collection<FlightBaggage> flightBaggages = new ArrayList<FlightBaggage>();
		while (itr.hasNext()) {
			FlightBaggage flightBaggage = new FlightBaggage();
			BaggageCharge baggageCharge = itr.next();

			flightBaggage.setFlightSegmentId(flightSegID);
			flightBaggage.setBaggageChargeId(baggageCharge.getChargeId());
			flightBaggage.setAmount(baggageCharge.getAmount());
			if (baggageCharge.getCabinClass() != null) {
				flightBaggage.setCabinClassCode(baggageCharge.getCabinClass());
			} else {
				flightBaggage.setLogicalCCCode(baggageCharge.getLogicalCCCode());
			}
			if (baggageCharge.getStatus().equals("ACT")) {
				flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.ACTIVE);
				flightBaggage.setSoldPieces(0);

			} else if (baggageCharge.getStatus().equals("INA")) {
				flightBaggage.setStatus(AirinventoryCustomConstants.FlightBaggageStatuses.INACTIVE);
				flightBaggage.setSoldPieces(0);

			}
			flightBaggage.setUserDetails(userPrincipal);
			flightBaggages.add(flightBaggage);
		}
		getBaggageDAO().saveFlightBaggage(flightBaggages);

	}

	public void deleteFlightBaggages(Collection<Integer> flightIds) throws ModuleException {
		baggageDAO.deleteFlightBaggages(flightIds);
	}

	public Map<Integer, List<FlightBaggageDTO>> getBaggage(List<FlightSegmentTO> flightSegmentTOs, Integer baggageChargeId)
			throws ModuleException {
		Map<Integer, List<FlightBaggageDTO>> baggageMap = new HashMap<Integer, List<FlightBaggageDTO>>();
		List<Integer> baggageChargeIds = new ArrayList<Integer>();
		baggageChargeIds.add(baggageChargeId);
		Map<Integer, List<FlightBaggageDTO>> baggage = AirInventoryModuleUtils.getBaggageJDBCDAO().getONDBaggageDTOs(null,
				baggageChargeIds, null, null);
		List<FlightBaggageDTO> baggageDTOs = new ArrayList<FlightBaggageDTO>();
		for (List<FlightBaggageDTO> dtos : baggage.values()) {
			baggageDTOs.addAll(dtos);
		}

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (AppSysParamsUtil.getCarrierCode().equals(flightSegmentTO.getOperatingAirline())) {
				baggageMap.put(flightSegmentTO.getFlightSegId(), baggageDTOs);
			}
		}

		return baggageMap;
	}
}
