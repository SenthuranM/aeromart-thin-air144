package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersSearchDTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 */
public interface BestOffersDAOJDBC {

	public Object[] getDynamicBestOffersForSingleFlights(BestOffersSearchDTO bestOfferSearchCriteria, String ondCode,
			int recordCount) throws ModuleException;

	public Object[] getOverallDynamiCBestOffersForSingleFlights(BestOffersSearchDTO bestOfferSearchCriteria,
			Collection<String> colOndCodes, int recordCount) throws ModuleException;

	public Object[] getDynamicBestOffersForConnectedFlights(BestOffersSearchDTO bestOfferSearchCriteria, String segmentCode,
			String ondCode) throws ModuleException;

	public Object[] getLimitedDynamicBestOffersForConnectedFlights(BestOffersSearchDTO bestOfferSearchCriteria,
			String segmentCode, String ondCode, int fromRecordCount, int toRecordCount) throws ModuleException;

	public List<RouteInfoTO> getAvailableRoutesForCountryOfOrigin(String countryCode, int availabilityRestrictionLevel);

	public int checkForCachedBestOffersRequest(BestOffersSearchDTO bestOfferSearchCriteria, Integer cachingTimeInMinutes)
			throws ModuleException;

	public int getNextBestOffersRequestId() throws ModuleException;

}
