package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;

/**
 * 
 * @author MN
 */
@Remote
public interface FlightInventoryBDImpl extends FlightInventoryBD {

}
