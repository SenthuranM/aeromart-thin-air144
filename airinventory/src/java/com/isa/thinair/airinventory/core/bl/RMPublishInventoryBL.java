package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateRMDTO;
import com.isa.thinair.airinventory.api.dto.rm.FCCSegInventoryUpdateStatusRMDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.core.persistence.dao.LogicalCabinClassDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightSegmentStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BL Class Used When Updating Optimized Seat Info Sent From RM Service
 * 
 * @author byorn.
 * 
 */
public class RMPublishInventoryBL {

	private final Log log = LogFactory.getLog(getClass());
	/** Holds the system-messages in property file **/
	private static String UNEXPECTED_ERROR = "webservices.airinventory.unexpected";

	private static interface ErrorCodes {
		public String FLIGHT_NOT_FOUND = "webservices.airinventory.flight.notfound";
		public String FCC_SEG_INVENTORY_NOT_FOUND = "webservices.airinventory.fccseginv.notfound";
		public String FCC_SEG_CLOSED = "webservices.airinventory.fccseginv.closed";

	}

	/** Set the user ID in constructor **/
	private String userID;

	public RMPublishInventoryBL(String userID) {
		if (this.userID == null) {
			this.userID = userID;
		}
	}

	/**
	 * UpdateFCCSegInventory
	 * 
	 * @param fccSegInventoryUpdateDTOs
	 * @return
	 * @throws ModuleException
	 */
	public Collection<FCCSegInventoryUpdateStatusRMDTO> batchUpdateFCCSegInventory(
			Collection<FCCSegInventoryUpdateRMDTO> fccSegInventoryUpdateExtDTOs) throws ModuleException {

		// final results to be returned
		Collection<FCCSegInventoryUpdateStatusRMDTO> results = new ArrayList<FCCSegInventoryUpdateStatusRMDTO>();

		// iterate each flightseg level info
		for (FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext : fccSegInventoryUpdateExtDTOs) {
			FCCSegInventoryUpdateStatusRMDTO result = null;
			ModuleException exception = null;

			try {
				// construct dto and get the result
				result = constructDTOandPerformOperation(fccSegInventoryUpdateDTOext);

			} catch (ModuleException e) {
				exception = e;

			} catch (Exception e) {
				log.error("Error :", e);
				exception = new ModuleException(e, UNEXPECTED_ERROR);

			} finally {

				if (exception != null) {
					result = new FCCSegInventoryUpdateStatusRMDTO(FCCSegInventoryUpdateStatusRMDTO.OPERATION_FAILURE,
							exception.getMessageString(), exception, null, null, null);
					result.setSegmentCode(fccSegInventoryUpdateDTOext.getSegmentCode());
					// TODO : Logical CC Change - Got the Cabin class of Logical CC
					result.setCabinClass(getLogicalCabinClassDAO().getCabinClass(fccSegInventoryUpdateDTOext.getLogicalCCCode()));
					result.setDepZuluDate(fccSegInventoryUpdateDTOext.getDepDateZulu());
					result.setFlightNumber(fccSegInventoryUpdateDTOext.getFlightNumber());
				}

			}

			results.add(result);
		}

		return results;
	}

	/**
	 * Construct the DTO and pass it to method FlightInventoryBL.updateFCCSegInventoryNew() which executies in new txn.
	 * 
	 * @param fccSegInventoryUpdateDTOext
	 *            - DTO Sent from WS
	 * @return FCCSegInventoryUpdateStatusRMDTO
	 * @throws ModuleException
	 */
	private FCCSegInventoryUpdateStatusRMDTO constructDTOandPerformOperation(
			FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext) throws ModuleException {

		String warningMSG = "";

		FlightBD flightBD = AirInventoryModuleUtils.getFlightBD();
		// get the flightID
		Integer flightID = flightBD.getFlightID(fccSegInventoryUpdateDTOext.getSegmentCode(),
				fccSegInventoryUpdateDTOext.getFlightNumber(), fccSegInventoryUpdateDTOext.getDepDateZulu());

		// if not found throw error
		if (flightID == null) {
			throw new ModuleException(ErrorCodes.FLIGHT_NOT_FOUND);
		}

		// get the fccSegInventory
		FCCSegInventory fccSeginventory = AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegInventory(flightID,
				fccSegInventoryUpdateDTOext.getLogicalCCCode(), fccSegInventoryUpdateDTOext.getSegmentCode());

		// if seg inventory not found throw error
		if (fccSeginventory == null) {
			throw new ModuleException(ErrorCodes.FCC_SEG_INVENTORY_NOT_FOUND);
		}
		
		FlightSegement flightSegment = flightBD.getFlightSegment(fccSeginventory.getFlightSegId());
		if (flightSegment.getStatus().equals(FlightSegmentStatusEnum.CLOSED.getCode())) {
			throw new ModuleException(ErrorCodes.FCC_SEG_CLOSED);
		}

		fccSegInventoryUpdateDTOext.setFlightId(fccSeginventory.getFlightId());
		fccSegInventoryUpdateDTOext.setFccsInventoryId(fccSeginventory.getFccsInvId());

		// load all the fccsegbcinventories with booking class standard type for the fccseginventory
		HashMap<String, Object[]> bcStdMap = AirInventoryModuleUtils.getFlightInventoryDAO().getFCCSegBCInventorys(
				fccSeginventory.getFccsInvId());

		HashMap<String, FCCSegBCInventory> map = new HashMap<String, FCCSegBCInventory>();

		for (Entry<String, Object[]> entry : bcStdMap.entrySet()) {
			Object[] tmpFCCSegInventoryList = entry.getValue();
			FCCSegBCInventory tmpFCCSegBCInventory = (FCCSegBCInventory) tmpFCCSegInventoryList[1];
			map.put(tmpFCCSegBCInventory.getBookingCode(), tmpFCCSegBCInventory);
		}

		// the deletions list will have the fccsbcid
		warningMSG = contructDeletions(fccSegInventoryUpdateDTOext, map);

		// the updateing inventories should be the loaded model obj from db
		warningMSG = contructUpdations(fccSegInventoryUpdateDTOext, bcStdMap, warningMSG);

		// add additional info to the Model objec to be saved
		warningMSG = contructAdditions(fccSegInventoryUpdateDTOext, map, warningMSG);

		// Construct the final list for RM. Delete all existing standard buckets having no sold, on-hold or canceled.
		// Close other existing standard buckets
		constructChangesForExistingInv(fccSegInventoryUpdateDTOext, bcStdMap);
		
		updateUntouchedFields(fccSegInventoryUpdateDTOext, fccSeginventory, bcStdMap);

		// perform operation
		FCCSegInventoryUpdateStatusDTO inventoryUpdateStatusDTO = AirInventoryModuleUtils.getLocalFlightInventoryBD().updateFCCSegInventory(
				fccSegInventoryUpdateDTOext);

		Flight flight = flightBD.getFlight(flightID);
		if (flight != null && flight.getStatus() != null && flight.getStatus().equals(FlightStatusEnum.CREATED)) {
			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flightID);
			flightBD.changeFlightStatus(flightIds, FlightStatusEnum.ACTIVE);
		}

		// return response
		FCCSegInventoryUpdateStatusRMDTO extension = new FCCSegInventoryUpdateStatusRMDTO(inventoryUpdateStatusDTO.getStatus(),
				inventoryUpdateStatusDTO.getMsg(), inventoryUpdateStatusDTO.getException(),
				inventoryUpdateStatusDTO.getFailureLevel(), inventoryUpdateStatusDTO.getFccsInvId(),
				inventoryUpdateStatusDTO.getFccsbInvId());

		if (!warningMSG.equals("") && inventoryUpdateStatusDTO.getStatus() != OperationStatusDTO.OPERATION_FAILURE) {
			extension.setStatus(OperationStatusDTO.OPERATION_COMPLETED);
			extension.setMsg(warningMSG);
		}

		extension.setSegmentCode(fccSegInventoryUpdateDTOext.getSegmentCode());
		// TODO : Logical CC Change - Got the Cabin class of Logical CC
		extension.setCabinClass(getLogicalCabinClassDAO().getCabinClass(fccSegInventoryUpdateDTOext.getLogicalCCCode()));
		extension.setDepZuluDate(fccSegInventoryUpdateDTOext.getDepDateZulu());
		extension.setFlightNumber(fccSegInventoryUpdateDTOext.getFlightNumber());

		return extension;
	}

	/**
	 * Have to send cached stuff. with updated vals.
	 * 
	 * @param fccSegInventoryUpdateDTOext
	 * @param map
	 */
	private String contructUpdations(FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext, HashMap<String, Object[]> map,
			String warningMSG) {

		Set<FCCSegBCInventory> finalUpdatedFCCSegBCInvSetForDTO = new HashSet<FCCSegBCInventory>();
		Set<FCCSegBCInventory> updatedFCCSegBCs = fccSegInventoryUpdateDTOext.getUpdatedFCCSegBCInventories();
		Set<FCCSegBCInventory> finalAddedFCCSegBCInvSetForDTO = fccSegInventoryUpdateDTOext.getAddedFCCSegBCInventories();

		Collection<String> colValidFareTypes = AirinventoryUtils.getAirInventoryConfig().getRmHandleFareTypes();
		List<String> validFareList = new ArrayList<String>(colValidFareTypes);

		for (FCCSegBCInventory valReturnedsegBCInventory : updatedFCCSegBCs) {
			Object[] tmpFCCSegInventoryList = map.get(valReturnedsegBCInventory.getBookingCode());
			if (tmpFCCSegInventoryList != null) {
				String bcAllocationType = (String) tmpFCCSegInventoryList[2];
				FCCSegBCInventory segBCInventory = (FCCSegBCInventory) tmpFCCSegInventoryList[1];

				if (segBCInventory != null && valReturnedsegBCInventory.getPriorityFlagWrapper() != null) {
					segBCInventory.setPriorityFlag(valReturnedsegBCInventory.getPriorityFlag());
				} else if (segBCInventory != null) {
					segBCInventory.setPriorityFlag(segBCInventory.getPriorityFlag());
				}

				if (segBCInventory != null && fccSegInventoryUpdateDTOext.isPublish() && validFareList.contains(bcAllocationType)) {
					segBCInventory.setStatus(valReturnedsegBCInventory.getStatus());
					segBCInventory.setStatusChangeAction(valReturnedsegBCInventory.getStatusChangeAction());
					if (!valReturnedsegBCInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED)) {

						int newAllocation = valReturnedsegBCInventory.getSeatsAvailable() + segBCInventory.getSeatsCancelled()
								+ segBCInventory.getSeatsSold() + segBCInventory.getOnHoldSeats()
								- segBCInventory.getSeatsAcquired();
						if (newAllocation < 0) {
							warningMSG += "\n ";
							warningMSG += "BC inventory seat allocation can not be less than zero. " + "[bookingcode="
									+ segBCInventory.getBookingCode() + "]";										
						} else {
							segBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
							segBCInventory.setSeatsAllocated(newAllocation);
							segBCInventory.setSeatsAcquired(valReturnedsegBCInventory.getSeatsAcquired());
							finalUpdatedFCCSegBCInvSetForDTO.add(segBCInventory);
						}

					} else { // If trying to CLS the BC then seat numbers will not update
						finalUpdatedFCCSegBCInvSetForDTO.add(segBCInventory);
					}
				} else if (segBCInventory != null && !fccSegInventoryUpdateDTOext.isPublish()) {
					segBCInventory.setStatusChangeAction(valReturnedsegBCInventory.getStatusChangeAction());
					if (valReturnedsegBCInventory.getStatus() == null
							|| (valReturnedsegBCInventory.getStatus() != null && (valReturnedsegBCInventory.getSeatsAllocated() > 0 || valReturnedsegBCInventory
									.getSeatsAvailable() > 0))) {
						if (valReturnedsegBCInventory.getSeatsAllocated() == 0
								&& valReturnedsegBCInventory.getSeatsAvailable() == 0) {
							warningMSG += "\n ";
							warningMSG += "BC inventory seat allocation can not be zero or null. " + "[bookingcode="
									+ segBCInventory.getBookingCode() + "]";
						} else if (valReturnedsegBCInventory.getSeatsAllocated() < 0
								|| valReturnedsegBCInventory.getSeatsAvailable() < 0) {
							warningMSG += "\n ";
							warningMSG += "BC inventory seat allocation/availability can not be less than zero. " + "[bookingcode="
									+ segBCInventory.getBookingCode() + "]";
						} else if (valReturnedsegBCInventory.getSeatsAllocated() == 0
								&& valReturnedsegBCInventory.getSeatsAvailable() > 0) {
							if (segBCInventory.getSeatsAcquired() - segBCInventory.getSeatsCancelled() > valReturnedsegBCInventory.getSeatsAvailable()) {
								warningMSG += "\n ";
								warningMSG += "BC inventory seat availability can not be less than effective acquired seats. " + "[bookingcode="
										+ segBCInventory.getBookingCode() + "]";								
							} else {
								int newAllocation = valReturnedsegBCInventory.getSeatsAvailable()
										+ segBCInventory.getSeatsCancelled() + segBCInventory.getSeatsSold()
										+ segBCInventory.getOnHoldSeats() - segBCInventory.getSeatsAcquired();
								
								if (newAllocation <= 0) {
									warningMSG += "\n ";
									warningMSG += "BC inventory seat allocation can not be less than or equal to zero. " + "[bookingcode="
											+ segBCInventory.getBookingCode() + "]";										
								} else {	
									segBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
									segBCInventory.setSeatsAllocated(newAllocation);
									if (valReturnedsegBCInventory.getStatus() != null) {
										segBCInventory.setStatus(valReturnedsegBCInventory.getStatus());
									}
									finalUpdatedFCCSegBCInvSetForDTO.add(segBCInventory);
								}
							}

						} else {
							int allocatedSeatsReduction = segBCInventory.getSeatsAllocated()
									- valReturnedsegBCInventory.getSeatsAllocated();
							if (segBCInventory.getSeatsAvailable() < allocatedSeatsReduction) {
								warningMSG += "\n ";
								warningMSG += "BC inventory seat allocation is no longer valid. " + "[bookingcode="
										+ segBCInventory.getBookingCode() + "]" + "[Available seats="
										+ segBCInventory.getSeatsAvailable() + "] " + "[Requested allocation reduction="
										+ allocatedSeatsReduction + "]";

							} else {
								segBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
								segBCInventory.setSeatsAllocated(valReturnedsegBCInventory.getSeatsAllocated());
								if (valReturnedsegBCInventory.getStatus() != null) {
									segBCInventory.setStatus(valReturnedsegBCInventory.getStatus());
								}
								finalUpdatedFCCSegBCInvSetForDTO.add(segBCInventory);
							}
						}
					} else { // If trying to CLS the BC then seat numbers will not update
						segBCInventory.setStatus(valReturnedsegBCInventory.getStatus());
						finalUpdatedFCCSegBCInvSetForDTO.add(segBCInventory);
					}
				} else if (fccSegInventoryUpdateDTOext.isPublish() && validFareList.contains(bcAllocationType)) {
					valReturnedsegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);
					valReturnedsegBCInventory.setSeatsAcquired(0);
					// If BC added newly to the inventory allocation count = available count
					valReturnedsegBCInventory.setSeatsAllocated(valReturnedsegBCInventory.getSeatsAvailable());
					finalAddedFCCSegBCInvSetForDTO.add(valReturnedsegBCInventory);
					log.debug("Inventory seat allocation not existed for the [BC :" + valReturnedsegBCInventory.getBookingCode()
							+ "]. " + "New allocation added as [Allocated seats=" + valReturnedsegBCInventory.getSeatsAllocated()
							+ "]");
					// if(!warningMSG.equals("")) warningMSG += "\n ";
					// warningMSG += "Inventory seat allocation not existed for the [BC :" +
					// valReturnedsegBCInventory.getBookingCode()+ "]. "
					// + "New allocation added as [Allocated seats="+valReturnedsegBCInventory.getSeatsAllocated()+"] ";
				} else {
					if (!warningMSG.equals("")) {
						warningMSG += "\n ";
					}
					warningMSG += "Inventory seat allocation not existed for the [BC :"
							+ valReturnedsegBCInventory.getBookingCode() + "]. ";
				}
			} else if (fccSegInventoryUpdateDTOext.isPublish()) {
				valReturnedsegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);
				valReturnedsegBCInventory.setSeatsAcquired(0);
				// If BC added newly to the inventory allocation count = available count
				valReturnedsegBCInventory.setSeatsAllocated(valReturnedsegBCInventory.getSeatsAvailable());
				finalAddedFCCSegBCInvSetForDTO.add(valReturnedsegBCInventory);
				log.debug("Inventory seat allocation not existed for the [BC :" + valReturnedsegBCInventory.getBookingCode()
						+ "]. " + "New allocation added as [Allocated seats=" + valReturnedsegBCInventory.getSeatsAllocated()
						+ "]");
				// if(!warningMSG.equals("")) warningMSG += "\n ";
				// warningMSG += "Inventory seat allocation not existed for the [BC :" +
				// valReturnedsegBCInventory.getBookingCode()+ "]. "
				// + "New allocation added as [Allocated seats="+valReturnedsegBCInventory.getSeatsAllocated()+"] ";
			} else {
				if (!warningMSG.equals("")) {
					warningMSG += "\n ";
				}
				warningMSG += "Inventory seat allocation not existed for the [BC :" + valReturnedsegBCInventory.getBookingCode()
						+ "]. ";
			}
		}
		fccSegInventoryUpdateDTOext.setUpdatedFCCSegBCInventories(null);
		fccSegInventoryUpdateDTOext.setAddedFCCSegBCInventories(null);
		fccSegInventoryUpdateDTOext.setUpdatedFCCSegBCInventories(finalUpdatedFCCSegBCInvSetForDTO);
		fccSegInventoryUpdateDTOext.setAddedFCCSegBCInventories(finalAddedFCCSegBCInvSetForDTO);

		return warningMSG;

	}
	
	private void updateUntouchedFields(FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext, FCCSegInventory fccSeginventory,
			HashMap<String, Object[]> map) {
		fccSegInventoryUpdateDTOext.setInfantAllocation(fccSeginventory.getInfantAllocation());
		if (!fccSegInventoryUpdateDTOext.isCurtailUpdate()) {
			fccSegInventoryUpdateDTOext.setCurtailedSeats(fccSeginventory.getCurtailedSeats());
		}
		if (!fccSegInventoryUpdateDTOext.isOversellUpdate()) {
			fccSegInventoryUpdateDTOext.setOversellSeats(fccSeginventory.getOversellSeats());
		}
		fccSegInventoryUpdateDTOext.setWaitListSeats(fccSeginventory.getAllocatedWaitListSeats());
		
		Set<FCCSegBCInventory> updatedFCCSegBCs = fccSegInventoryUpdateDTOext.getUpdatedFCCSegBCInventories();
		for (Entry<String, Object[]> entry : map.entrySet()) {
			Object[] tmpFCCSegInventoryList = entry.getValue();
			FCCSegBCInventory currentSegBCInv = (FCCSegBCInventory) tmpFCCSegInventoryList[1];
			String bookingCode = entry.getKey();

			for (FCCSegBCInventory fccSegBCInv : updatedFCCSegBCs) { 
				if (bookingCode.equals(fccSegBCInv.getBookingCode())) {
					fccSegBCInv.setPriorityFlag(currentSegBCInv.getPriorityFlag());
				}
			}
		}
	}

	/**
	 * Have to put ids and sent accoring to the method
	 * 
	 * @param fccSegInventoryUpdateDTOext
	 * @param map
	 */
	@SuppressWarnings("unchecked")
	private String
			contructDeletions(FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext, HashMap<String, FCCSegBCInventory> map) {
		String warningMSG = "";
		
		Set<Integer> fccsbcids = new HashSet<Integer>();
		Set<FCCSegBCInventory> delFCCSegBCs = fccSegInventoryUpdateDTOext.getDeletedFCCSegBCInventories();
		for (FCCSegBCInventory valReturnedsegBCInventory : delFCCSegBCs) {
			FCCSegBCInventory segBCInventory = map.get(valReturnedsegBCInventory.getBookingCode());
			if (segBCInventory != null) {
				fccsbcids.add(segBCInventory.getFccsbInvId());
			} else {
				warningMSG += "\n ";
				warningMSG += "BC inventory does not exists to delete. " + "[bookingcode="
						+ valReturnedsegBCInventory.getBookingCode() + "]";
			}
		}
		fccSegInventoryUpdateDTOext.setDeletedFCCSegBCInventories(null);
		fccSegInventoryUpdateDTOext.setDeletedFCCSegBCInventories(fccsbcids);
		return warningMSG;
	}

	private String
			contructAdditions(FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext, HashMap<String, FCCSegBCInventory> map, String warningMSG) {
		Set<FCCSegBCInventory> addFCCSegBCs = fccSegInventoryUpdateDTOext.getAddedFCCSegBCInventories();

		Collection<FCCSegBCInventory> bookingClassesToRemove = new ArrayList<FCCSegBCInventory>();

		for (FCCSegBCInventory fccSegBCInv : addFCCSegBCs) {

			if (!map.containsKey(fccSegBCInv.getBookingCode())) {
				if (fccSegBCInv.getSeatsAllocated() > 0) {
					fccSegBCInv.setFlightId(fccSegInventoryUpdateDTOext.getFlightId());
					fccSegBCInv.setfccsInvId(fccSegInventoryUpdateDTOext.getFccsInventoryId());
				} else {
					bookingClassesToRemove.add(fccSegBCInv);
					warningMSG += "\n ";
					warningMSG += "BC allocation should be greater than zero. " + "[bookingcode="
							+ fccSegBCInv.getBookingCode() + "]";					
				}
			} else {
				// if bc already exists..remove it from the collection as you dont need to add it again.
				// addFCCSegBCs.remove(fccSegBCInv);
				bookingClassesToRemove.add(fccSegBCInv);
				warningMSG += "\n ";
				warningMSG += "BC inventory already exists. " + "[bookingcode="
						+ fccSegBCInv.getBookingCode() + "]";
			}

		}

		if (bookingClassesToRemove.size() > 0) {
			for (FCCSegBCInventory fccSegBCInv : bookingClassesToRemove) {
				addFCCSegBCs.remove(fccSegBCInv);
			}
		}
		return warningMSG;
	}

	/**
	 * Have to send cached stuff. with updated vals.
	 * 
	 * @param fccSegInventoryUpdateDTOext
	 * @param map
	 */
	@SuppressWarnings("unchecked")
	private void constructChangesForExistingInv(FCCSegInventoryUpdateRMDTO fccSegInventoryUpdateDTOext,
			HashMap<String, Object[]> map) {

		Set<FCCSegBCInventory> finalUpdatedFCCSegBCInvSetForDTO = fccSegInventoryUpdateDTOext.getUpdatedFCCSegBCInventories();
		Set<Integer> finalDeletedFCCSegBCIds = fccSegInventoryUpdateDTOext.getDeletedFCCSegBCInventories();
		Set<FCCSegBCInventory> updatedFCCSegBCs = fccSegInventoryUpdateDTOext.getUpdatedFCCSegBCInventories();
		Set<FCCSegBCInventory> addedFCCSegBCs = fccSegInventoryUpdateDTOext.getAddedFCCSegBCInventories();
		Set<Integer> fccsbcids = fccSegInventoryUpdateDTOext.getDeletedFCCSegBCInventories();

		Collection<String> colValidFareTypes = AirinventoryUtils.getAirInventoryConfig().getRmHandleFareTypes();
		List<String> validFareList = new ArrayList<String>(colValidFareTypes);

		boolean isExist = false;

		for (Entry<String, Object[]> entry : map.entrySet()) {
			isExist = false;
			Object[] tmpFCCSegInventoryList = entry.getValue();
			FCCSegBCInventory currentSegBCInv = (FCCSegBCInventory) tmpFCCSegInventoryList[1];
			String bcStandardCode = (String) tmpFCCSegInventoryList[0];
			String bcAllocationType = (String) tmpFCCSegInventoryList[2];
			String bookingCode = entry.getKey();

			for (FCCSegBCInventory fccSegBCInv : updatedFCCSegBCs) { // check the existing booking class already in the
																		// update list
				if (bookingCode.equals(fccSegBCInv.getBookingCode())) {
					isExist = true;
					break;
				}
			}

			if (!isExist) {
				for (FCCSegBCInventory fccSegBCInv : addedFCCSegBCs) { // check the existing booking class already in
																		// the addition list
					if (bookingCode.equals(fccSegBCInv.getBookingCode())) {
						isExist = true;
						break;
					}
				}
			}

			if (!isExist) {
				for (Integer fccSegBCId : fccsbcids) { // check the existing booking class already in the delete list
					if (fccSegBCId == currentSegBCInv.getFccsbInvId()) {
						isExist = true;
						break;
					}
				}
			}

			if (!isExist && fccSegInventoryUpdateDTOext.isPublish()) {
				if (bcStandardCode.equals(BookingClass.STANDARD_CODE_Y) && // check the existing booking class is
																			// standard.CLS the BC
						(currentSegBCInv.getSeatsSold() > 0 || currentSegBCInv.getOnHoldSeats() > 0 || currentSegBCInv
								.getSeatsCancelled() > 0) && validFareList.contains(bcAllocationType)) {
					currentSegBCInv.setPriorityFlag(false);
					currentSegBCInv.setStatus(FCCSegBCInventory.Status.CLOSED);
					currentSegBCInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
					finalUpdatedFCCSegBCInvSetForDTO.add(currentSegBCInv);
				} else if (bcStandardCode.equals(BookingClass.STANDARD_CODE_Y)
						&& // check the existing booking class is standard.DEL the BC
						currentSegBCInv.getActualSeatsSold() == 0 && currentSegBCInv.getActualSeatsOnHold() == 0
						&& currentSegBCInv.getSeatsCancelled() == 0 && validFareList.contains(bcAllocationType)) {
					finalDeletedFCCSegBCIds.add(currentSegBCInv.getFccsbInvId());
				}
			}

		}

		fccSegInventoryUpdateDTOext.setUpdatedFCCSegBCInventories(null);
		fccSegInventoryUpdateDTOext.setDeletedFCCSegBCInventories(null);
		fccSegInventoryUpdateDTOext.setUpdatedFCCSegBCInventories(finalUpdatedFCCSegBCInvSetForDTO);
		fccSegInventoryUpdateDTOext.setDeletedFCCSegBCInventories(finalDeletedFCCSegBCIds);

	}

	private LogicalCabinClassDAO getLogicalCabinClassDAO() {
		LogicalCabinClassDAO logicalCabinClassDAO = (LogicalCabinClassDAO) AirInventoryUtil.getInstance().getLocalBean(
				"LogicalCabinClassDAOImplProxy");
		return logicalCabinClassDAO;
	}

}
