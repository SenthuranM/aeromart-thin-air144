package com.isa.thinair.airinventory.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * Helper class for auditing inventory functionalities Uses Director-Builder Pattern .
 * 
 * @author Nasly
 * 
 */
public class InventoryAuditor {

	private UserPrincipal userPrincipal;

	public InventoryAuditor(UserPrincipal userPrincipal) {
		this.userPrincipal = userPrincipal;
	}

	public void saveAudit(int operationId, String content, Integer fccsbInvId, Integer fltSegId) throws ModuleException {
		InventoryAudit audit = new InventoryAudit();
		audit.setUserId(userPrincipal.getUserId());
		audit.setSalesChannelCode(userPrincipal.getSalesChannel());
		audit.setOperationId(operationId);
		audit.setAuditTime(new Date());
		audit.setContent(content);
		audit.setFccsbInvId(fccsbInvId);
		audit.setFccSegInvId(fltSegId);
		Collection<InventoryAudit> invAudit = new ArrayList<InventoryAudit>();
		invAudit.add(audit);
		AirinventoryUtils.getAuditorBD().auditInventory(invAudit);
	}

}
