/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author indika
 * 
 */
public interface FlightSeatJDBCDAO {

	public Collection<Integer> getFlightSeatsToRelease(Date blockTime, Date seatReleaseEndDate) throws ModuleException;

}
