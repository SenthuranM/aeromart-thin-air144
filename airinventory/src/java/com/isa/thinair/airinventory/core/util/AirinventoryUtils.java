package com.isa.thinair.airinventory.core.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.api.service.RollForwardBatchREQBD;
import com.isa.thinair.airinventory.api.service.RollForwardBatchREQSEGBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class AirinventoryUtils {

	/**
	 * Returns airinventory module config
	 */
	public static AirInventoryConfig getAirInventoryConfig() {
		return AirInventoryModuleUtils.getAirInventoryConfig();
	}

	/**
	 * Returns AuditorBD
	 */
	public static AuditorBD getAuditorBD() {
		return AirInventoryModuleUtils.getAuditorBD();
	}

	/**
	 * Returns air reservation module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public static DataSource getReportingDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.REPORT_DATA_SOURCE_BEAN);
	}

	/**
	 * Returns serializable list of values from a map.
	 */
	public static Collection getSearializableValues(Map<?, ?> map) {
		ArrayList values = null;
		if (map != null) {
			values = new ArrayList();
			Iterator mapIt = map.values().iterator();
			while (mapIt.hasNext()) {
				values.add(mapIt.next());
			}
		}
		return values;
	}

	public static Collection<QuotedChargeDTO> getQuotedChargeDTOList(Map<String, QuotedChargeDTO> quotedChargeDTOs) {
		Collection<QuotedChargeDTO> chargeDTOs = new ArrayList<QuotedChargeDTO>();
		if (quotedChargeDTOs != null && !quotedChargeDTOs.isEmpty()) {
			chargeDTOs.addAll(quotedChargeDTOs.values());
		}
		return chargeDTOs;
	}

	public static Collection<FlexiRuleDTO> getFlexiRuleDTOList(Map<String, FlexiRuleDTO> flexiRuleDTOs) {
		Collection<FlexiRuleDTO> chargeDTOs = new ArrayList<FlexiRuleDTO>();
		if (flexiRuleDTOs != null && !flexiRuleDTOs.isEmpty()) {
			chargeDTOs.addAll(flexiRuleDTOs.values());
		}
		return chargeDTOs;
	}

	/**
	 * Return the global confirguration
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	public static FCCSegInventory createZeroFccSegAllocInventory() {

		FCCSegInventory fccSegInventory = new FCCSegInventory();
		fccSegInventory.setOversellSeats(0);
		fccSegInventory.setCurtailedSeats(0);
		fccSegInventory.setSeatsSold(0);
		fccSegInventory.setOnHoldSeats(0);
		fccSegInventory.setFixedSeats(0);
		fccSegInventory.setSoldInfantSeats(0);
		fccSegInventory.setOnholdInfantSeats(0);
		return fccSegInventory;
	}

	public static FCCSegBCInventory createZeroAllocBCInventory(FCCSegInventory fccSegInventory, String bookingClassCode) {
		FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
		fccSegBCInventory.setBookingCode(bookingClassCode);
		fccSegBCInventory.setLogicalCCCode(fccSegInventory.getLogicalCCCode());
		fccSegBCInventory.setfccsInvId(fccSegInventory.getFccsInvId());
		fccSegBCInventory.setFlightId(fccSegInventory.getFlightId());
		fccSegBCInventory.setOnHoldSeats(0);
		fccSegBCInventory.setPriorityFlag(false);
		fccSegBCInventory.setSeatsAcquired(0);
		fccSegBCInventory.setSeatsAllocated(0);
		fccSegBCInventory.setSeatsAvailable(0);
		fccSegBCInventory.setSeatsCancelled(0);
		fccSegBCInventory.setSeatsSold(0);
		fccSegBCInventory.setWaitListedSeats(0);
		fccSegBCInventory.setAllocatedWaitListSeats(0);
		fccSegBCInventory.setSegmentCode(fccSegInventory.getSegmentCode());
		fccSegBCInventory.setStatus(FCCSegBCInventory.Status.OPEN);
		fccSegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);

		AirInventoryModuleUtils.getFlightInventoryDAO().saveFCCSegmentBCInventory(fccSegBCInventory);

		return fccSegBCInventory;
	}

	/**
	 * XBE we can select over book BC type with out selecting the booking class Or we can select over book BC type with
	 * selected booking class. This method will return true for second scenario. We need this check only for standard
	 * booking classes as they participate in nesting. But used in non standard bc check just to reuse code
	 * 
	 * @return
	 */
	public static boolean isOverbookingWithBookingClass(BucketFilteringCriteria bucketFilteringCriteria) {
		if (bucketFilteringCriteria.getBookingClassCode() != null && !bucketFilteringCriteria.getBookingClassCode().equals("")
				&& BookingClassUtil.byPassSegInvUpdateForOverbook(bucketFilteringCriteria.getBookingClassTypes())) {
			return true;
		}
		return false;
	}

	public static String prepareClassOfServiceWhereClause(String cabinClass, String logicalCC, String aliasCosTable) {
		String clause = "";

		if (logicalCC != null && !"".equals(logicalCC)) {
			clause += " AND (" + aliasCosTable + ".logical_cabin_class_code = '" + logicalCC + "' ";

			if (cabinClass != null && !"".equals(cabinClass)) {
				clause += " OR " + aliasCosTable + ".cabin_class_code = '" + cabinClass + "' ";
			}

			clause += ") ";
		} else if (cabinClass != null && !"".equals(cabinClass)) {
			clause += " AND " + aliasCosTable + ".cabin_class_code = '" + cabinClass + "' ";
		}

		return clause;
	}

	/**
	 * @param segmentCode
	 * @return
	 */
	private static List<String> buildSingleLegSegmentCodes(String segmentCode) {
		List<String> segmentList = new ArrayList<String>();
		if (segmentCode.length() > 7) {
			String[] airportCodes = segmentCode.split("\\/");
			for (int i = 0; i < airportCodes.length - 1; i++) {
				segmentList.add(airportCodes[i] + "/" + airportCodes[i + 1]);
			}
		} else {
			segmentList.add(segmentCode);
		}
		return segmentList;
	}

	/**
	 * @param flightSegmentCodes
	 * @return
	 */
	public static Collection<String> buildSingleLegSegmentCodes(List<String> flightSegmentCodes) {
		Set<String> singleLegSegments = new HashSet<String>();
		for (String segmentCode : flightSegmentCodes) {
			List<String> singleLegCodes = buildSingleLegSegmentCodes(segmentCode);
			singleLegSegments.addAll(singleLegCodes);
		}
		return singleLegSegments;
	}

	/**
	 * @param ffcFccSegInventories
	 * @return
	 */
	public static boolean hasInterceptingSegments(Collection<FCCSegInventory> ffcFccSegInventories) {

		for (FCCSegInventory ffcSegInventoryOut : ffcFccSegInventories) {
			for (FCCSegInventory ffcSegInventoryIn : ffcFccSegInventories) {
				if (!ffcSegInventoryIn.getSegmentCode().equals(ffcSegInventoryOut.getSegmentCode())
						&& (ffcSegInventoryOut.getSegmentCode().contains(ffcSegInventoryIn.getSegmentCode()) || ffcSegInventoryIn
								.getSegmentCode().contains(ffcSegInventoryOut.getSegmentCode()))
						&& ffcSegInventoryOut.getLogicalCCCode().equals(ffcSegInventoryIn.getLogicalCCCode())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param ffcSegInventories
	 * @param fccSegemntInvntory
	 * @return
	 */
	public static boolean
			isInterceptingSegment(Collection<FCCSegInventory> ffcSegInventories, FCCSegInventory fccSegemntInvntory) {

		for (FCCSegInventory ffcSegInventoryOut : ffcSegInventories) {
			if (!fccSegemntInvntory.getSegmentCode().equals(ffcSegInventoryOut.getSegmentCode())
					&& (ffcSegInventoryOut.getSegmentCode().contains(fccSegemntInvntory.getSegmentCode()) || fccSegemntInvntory
							.getSegmentCode().contains(ffcSegInventoryOut.getSegmentCode()))
					&& ffcSegInventoryOut.getLogicalCCCode().equals(fccSegemntInvntory.getLogicalCCCode())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param ffcSegInventories
	 * @param fccSegemntInvntory
	 * @return
	 */
	public static List<Integer> getInterceptingSegments(Collection<FCCSegInventory> ffcSegInventories,
			FCCSegInventory fccSegemntInvntory) {
		List<Integer> interceptingSegmentIds = new ArrayList<Integer>();
		for (FCCSegInventory ffcSegInventoryOut : ffcSegInventories) {
			if (fccSegemntInvntory.getFccsInvId() != ffcSegInventoryOut.getFccsInvId()
					&& (ffcSegInventoryOut.getSegmentCode().contains(fccSegemntInvntory.getSegmentCode()) || fccSegemntInvntory
							.getSegmentCode().contains(ffcSegInventoryOut.getSegmentCode()))
					&& (ffcSegInventoryOut.getLogicalCCCode().equals(fccSegemntInvntory.getLogicalCCCode()) || !AppSysParamsUtil
							.isLogicalCabinClassInventoryRestricted())) {
				interceptingSegmentIds.add(ffcSegInventoryOut.getFccsInvId());
			}
		}
		return interceptingSegmentIds;
	}

	/**
	 * @param segmentAndLogicalCabinClassWiseSoldSeatCount
	 * @param fccSegAllocSegmentCode
	 * @param logicalCabinClassCode
	 * @return
	 */
	public static int[] getMaxSoldSeatcCountOnFlightSegment(
			Map<String, Map<String, int[]>> segmentAndLogicalCabinClassWiseSoldSeatCount, String fccSegAllocSegmentCode,
			String logicalCabinClassCode) {
		int maxAdultSoldSeatCount = 0;
		int maxinfantSoldSeatCount = 0;
		List<String> singleLegSegmentCodes = buildSingleLegSegmentCodes(fccSegAllocSegmentCode);
		for (String sigleLegCode : singleLegSegmentCodes) {
			int totalAdultSoldSeatCount = 0;
			int totalinfantSoldSeatCount = 0;

			for (String segmentCode : segmentAndLogicalCabinClassWiseSoldSeatCount.keySet()) {

				if (sigleLegCode.toUpperCase().contains(segmentCode.toUpperCase())) {
					for (String logicalCabinClass : segmentAndLogicalCabinClassWiseSoldSeatCount.get(segmentCode).keySet()) {
						if (logicalCabinClass.equals(logicalCabinClassCode)
								|| !AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
							int[] soldSeatCount = segmentAndLogicalCabinClassWiseSoldSeatCount.get(segmentCode).get(
									logicalCabinClass);
							if (soldSeatCount != null) {
								totalAdultSoldSeatCount += soldSeatCount[0];
								totalinfantSoldSeatCount += soldSeatCount[1];
							}
						}
					}
				}
			}
			maxAdultSoldSeatCount = Math.max(maxAdultSoldSeatCount, totalAdultSoldSeatCount);
			maxinfantSoldSeatCount = Math.max(maxinfantSoldSeatCount, totalinfantSoldSeatCount);
		}
		return new int[] { maxAdultSoldSeatCount, maxinfantSoldSeatCount };
	}

	public static boolean isIbeBooking(String appIndicator) {
		if (appIndicator != null
				&& (SalesChannelsUtil.isAppEngineWebOrMobile(appIndicator))) {
			return true;
		}
		return false;
	}

	public static Map<String, List<AvailableIBOBFlightSegment>> getDateWiseAvailableFlightSegments(
			AvailableIBOBFlightSegment selectedOndFlight, Collection<AvailableIBOBFlightSegment> availableAllFlightSegments,
			boolean includeSelectedDate) {
		Map<String, List<AvailableIBOBFlightSegment>> dateWiseAvailableFlightSegments = new HashMap<String, List<AvailableIBOBFlightSegment>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableAllFlightSegments) {
			if (includeSelectedDate || !isSelectedFlight(selectedOndFlight, availableIBOBFlightSegment)) {
				String formattedDate = dateFormat.format(availableIBOBFlightSegment.getDepartureDate());
				if (dateWiseAvailableFlightSegments.get(formattedDate) == null) {
					dateWiseAvailableFlightSegments.put(formattedDate, new ArrayList<AvailableIBOBFlightSegment>());
				}
				dateWiseAvailableFlightSegments.get(formattedDate).add(availableIBOBFlightSegment);
				availableIBOBFlightSegment.cleanFares();
			}
		}
		return dateWiseAvailableFlightSegments;
	}

	private static boolean isSelectedFlight(AvailableIBOBFlightSegment selectedOndFlight, AvailableIBOBFlightSegment ondFlight) {
		if (isOutSideDateRange(selectedOndFlight.getDepartureDate(), selectedOndFlight.getDepartureDate(),
				ondFlight.getDepartureDate())) {
			return false;
		} else {
			if (selectedOndFlight.getFlightSegmentDTOs().size() != ondFlight.getFlightSegmentDTOs().size()) {
				return false;
			}
			if (!selectedOndFlight.getOndCode().equals(ondFlight.getOndCode())) {
				return false;
			}
			List<FlightSegmentDTO> selectedFlights = new ArrayList<FlightSegmentDTO>(selectedOndFlight.getFlightSegmentDTOs());
			List<FlightSegmentDTO> otherFlights = new ArrayList<FlightSegmentDTO>(ondFlight.getFlightSegmentDTOs());
			for (int i = 0; i < selectedFlights.size(); i++) {
				FlightSegmentDTO selectedFltSeg = selectedFlights.get(i);
				FlightSegmentDTO otherFlightSeg = otherFlights.get(i);
				if (selectedFltSeg.compareTo(otherFlightSeg) == 0
						&& selectedFltSeg.getFlightNumber().equals(otherFlightSeg.getFlightNumber())) {
					;
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public static Map<String, List<AvailableIBOBFlightSegment>> getDateWiseAvailableFlightSegments(
			Collection<AvailableIBOBFlightSegment> availableAllFlightSegments) {
		Map<String, List<AvailableIBOBFlightSegment>> dateWiseAvailableFlightSegments = new HashMap<String, List<AvailableIBOBFlightSegment>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		for (AvailableIBOBFlightSegment availableIBOBFlightSegment : availableAllFlightSegments) {
			String formattedDate = dateFormat.format(availableIBOBFlightSegment.getDepartureDate());
			if (dateWiseAvailableFlightSegments.get(formattedDate) == null) {
				dateWiseAvailableFlightSegments.put(formattedDate, new ArrayList<AvailableIBOBFlightSegment>());
			}
			dateWiseAvailableFlightSegments.get(formattedDate).add(availableIBOBFlightSegment);
		}
		return dateWiseAvailableFlightSegments;
	}

	public static void addDebugLog(Log log, String logMessage) {
		if (log.isDebugEnabled()) {
			log.debug(logMessage);
		}
	}

	public static void addErrorLog(Log log, String logMessage) {
		log.error(logMessage);
	}

	public static void addErrorLog(Log log, String logMessage, Exception exception) {
		log.error(logMessage, exception);
	}

	private static boolean isOutSideDateRange(Date startTime, Date endTime, Date flightDepartureTime) {
		return flightDepartureTime.before(startTime) || flightDepartureTime.after(endTime);
	}

	/**
	 * Checks if Return fare is selected.
	 * 
	 * @param selectedFlightSegments
	 *            List<AvailableIBOBFlightSegment> of selected flight segment which will be checked to see if selected
	 *            fares are of FareTypes.HALF_RETURN_FARE.
	 * @return true if returns fares were selected, false otherwise.
	 */
	public static boolean isReturnFareSelected(List<AvailableIBOBFlightSegment> selectedFlightSegments) {
		if (selectedFlightSegments.size() == 2) {
			for (AvailableIBOBFlightSegment availableSeg : selectedFlightSegments) {
				for (int fareType : availableSeg.getSelectedeFareTypeMap().values()) {
					if (FareTypes.HALF_RETURN_FARE == fareType) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Copies List<AvailableIBOBFlightSegment> shallowly. (Some selected properties only) via the
	 * AvailableIBOBFlightSegment's getShallowCopy() implementations. Current implementation copies
	 * 
	 * @param originalList
	 *            List<AvailableIBOBFlightSegment> to be copied.
	 * @return A Shallow copy of the passed List<AvailableIBOBFlightSegment>.
	 */
	public static List<AvailableIBOBFlightSegment>
			getShallowCopyOfFlightSegmentList(List<AvailableIBOBFlightSegment> originalList) {
		List<AvailableIBOBFlightSegment> copiedList = new ArrayList<AvailableIBOBFlightSegment>();
		for (AvailableIBOBFlightSegment originalSeg : originalList) {
			copiedList.add(originalSeg.getShallowCopy());
		}
		return copiedList;
	}

	/**
	 * Gets each element of a passed list and puts them in separate lists and returns them wrapped up in another list.
	 * If the passed List<T> contain 5 elements. Resulting List<List<T>> will contain 5 List<T> elements.
	 * 
	 * @param elements
	 *            List of elements to be put into separate lists.
	 * @return List<List<T>> of elements that was put into lists.
	 */
	public static <T> List<List<T>> getElementsAsList(List<T> elements) {
		List<List<T>> listOfLists = new ArrayList<List<T>>();
		for (T element : elements) {
			listOfLists.add(Arrays.asList(element));
		}
		return listOfLists;
	}

	public static List<List<FlightSegmentTO>> splitFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs, boolean isReQuote) {
		List<List<FlightSegmentTO>> splitTos = new ArrayList<List<FlightSegmentTO>>();
		Map<Object, List<FlightSegmentTO>> tempDtos;
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		long maxConnectionTime = AppSysParamsUtil.getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.MAX_TRANSIT_TIME));
		int ondSequence;
		long maxConnectionLimit;
		long timeDifference;
		Date arrivalTime;
		Date departureTime;
		String[] airports;
		int ondSequenceDefference;
		int lastPutOndSequence = 0;
		int lastPutOriginalOndSequence = 0;
		FlightSegmentTO lastflightSegmentTOOfCurrentOndSequence, firstflightSegmentTOOfNextOndSequence;
		String startAirPortOfCurrentOndSequence, endAirPortOfCurrentOndSequence, startAirPortOfNextOndSequence, endAirPortOfNextOndSequence;

		List<FlightSegmentTO> tempFlightSegmentTOList = new ArrayList<FlightSegmentTO>();

		tempDtos = new LinkedHashMap<Object, List<FlightSegmentTO>>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {

			ondSequence = flightSegmentTO.getOndSequence();

			if (!tempDtos.containsKey(ondSequence)) {

				tempDtos.put(ondSequence, new ArrayList<FlightSegmentTO>());
				lastPutOndSequence = ondSequence;
				lastPutOriginalOndSequence = ondSequence;
				tempDtos.get(ondSequence).add(flightSegmentTO);

			} else {
				if (lastPutOriginalOndSequence == ondSequence) {
					tempDtos.get(ondSequence).add(flightSegmentTO);
				} else {
					lastPutOriginalOndSequence = ondSequence;
					lastPutOndSequence++;
					tempDtos.put(lastPutOndSequence, new ArrayList<FlightSegmentTO>());
					tempDtos.get(lastPutOndSequence).add(flightSegmentTO);
				}
			}
		}

		Iterator<Map.Entry<Object, List<FlightSegmentTO>>> iter = tempDtos.entrySet().iterator();

		while (iter.hasNext()) {

			Map.Entry<Object, List<FlightSegmentTO>> flightSegmentTOEntry = iter.next();

			ondSequence = (Integer) flightSegmentTOEntry.getKey();
			tempFlightSegmentTOList = flightSegmentTOEntry.getValue();
			lastflightSegmentTOOfCurrentOndSequence = tempFlightSegmentTOList.get(tempFlightSegmentTOList.size() - 1);

			airports = lastflightSegmentTOOfCurrentOndSequence.getSegmentCode().split("/");

			if (tempDtos.containsKey(ondSequence + 1)) {

				firstflightSegmentTOOfNextOndSequence = tempDtos.get(ondSequence + 1).get(0);
				startAirPortOfNextOndSequence = firstflightSegmentTOOfNextOndSequence.getSegmentCode().split("/")[0];

				if (airports[airports.length - 1].equals(startAirPortOfNextOndSequence)) {
					if (globalConfig.isHubAirport(startAirPortOfNextOndSequence)) {
						maxConnectionLimit = AppSysParamsUtil.getTimeInMillis(globalConfig.getMixMaxTransitDurations(
								startAirPortOfNextOndSequence, null, null)[1]);
					} else {
						maxConnectionLimit = maxConnectionTime;
					}

					departureTime = firstflightSegmentTOOfNextOndSequence.getDepartureDateTime();
					arrivalTime = lastflightSegmentTOOfCurrentOndSequence.getArrivalDateTime();

					timeDifference = departureTime.getTime() - arrivalTime.getTime();

					ondSequenceDefference = firstflightSegmentTOOfNextOndSequence.getOndSequence()
							- lastflightSegmentTOOfCurrentOndSequence.getOndSequence();

					if ((timeDifference < maxConnectionLimit) && ondSequenceDefference != 1) {

						startAirPortOfCurrentOndSequence = tempFlightSegmentTOList.get(0).getAirportCode().split("/")[0];

						airports = lastflightSegmentTOOfCurrentOndSequence.getAirportCode().split("/");
						endAirPortOfCurrentOndSequence = airports[airports.length - 1];

						airports = tempDtos.get(ondSequence + 1).get(tempDtos.get(ondSequence + 1).size() - 1).getSegmentCode()
								.split("/");
						endAirPortOfNextOndSequence = airports[airports.length - 1];

						if (!(startAirPortOfCurrentOndSequence.equals(endAirPortOfNextOndSequence) && startAirPortOfNextOndSequence
								.equals(endAirPortOfCurrentOndSequence))) {

							tempDtos.get(ondSequence).addAll(tempDtos.get(ondSequence + 1));
							iter.next();
							iter.remove();

						}

					}

				}

			}

		}

		splitTos.addAll(tempDtos.values());
		return splitTos;
	}

	public static List<List<FlightSegmentTO>> splitFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		List<List<FlightSegmentTO>> splitTos = new ArrayList<List<FlightSegmentTO>>();
		NavigableMap<Integer, List<FlightSegmentTO>> tempDtos;
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		long maxConnectionTime = AppSysParamsUtil.getTimeInMillis(globalConfig.getBizParam(SystemParamKeys.MAX_TRANSIT_TIME));

		tempDtos = new TreeMap<Integer, List<FlightSegmentTO>>();

		// split based on ond-sequence
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (!tempDtos.containsKey(flightSegmentTO.getOndSequence())) {
				tempDtos.put(flightSegmentTO.getOndSequence(), new ArrayList<FlightSegmentTO>());
			}

			tempDtos.get(flightSegmentTO.getOndSequence()).add(flightSegmentTO);
		}

		/*
		 * 
		 * // merge ONDs further int k; boolean merge; for (Integer loop1Key : tempDtos.keySet()) { k = loop1Key; // go
		 * up-wards for (Integer a = k; a != null; a = tempDtos.higherKey(a)) { merge = false;
		 * 
		 * // if can be merged merge
		 * 
		 * 
		 * 
		 * }
		 * 
		 * 
		 * 
		 * }
		 */

		splitTos.addAll(tempDtos.values());

		/*
		 * if (isReQuote) {
		 * 
		 * } else { Set<String> coveredAirports = new HashSet<String>(); int ondSequence = 0; String lastDepartedAirport
		 * = null; Date lastArrivalTime = null; Date departureTime; String[] airports; boolean returnStarted = false;
		 * long maxConnectionLimit;
		 * 
		 * tempDtos = new LinkedHashMap<Object, List<FlightSegmentTO>>(); for (FlightSegmentTO flightSegmentTO :
		 * flightSegmentTOs) { airports = flightSegmentTO.getSegmentCode().split("/"); if (lastDepartedAirport == null)
		 * { // first segment ondSequence++; } else { if (!lastDepartedAirport.equals(airports[0])) { // journey is not
		 * continuous ondSequence++;
		 * 
		 * } else { // journey is continuous if (globalConfig.isHubAirport(airports[0])) { maxConnectionLimit =
		 * AppSysParamsUtil.getTimeInMillis(globalConfig.getMixMaxTransitDurations( airports[0], null, null)[1]); } else
		 * { maxConnectionLimit = maxConnectionTime; }
		 * 
		 * departureTime = flightSegmentTO.getDepartureDateTime();
		 * 
		 * if (departureTime.getTime() - lastArrivalTime.getTime() > maxConnectionLimit) { // exceed connection limit
		 * ondSequence++; returnStarted = coveredAirports.contains(airports[airports.length - 1]); } else if
		 * (coveredAirports.contains(airports[airports.length - 1])) { // inbound if (returnStarted) { if
		 * (!lastDepartedAirport.equals(airports[0])) { ondSequence++; } } else { ondSequence++; returnStarted = true; }
		 * } } }
		 * 
		 * coveredAirports.add(airports[0]); coveredAirports.add(airports[airports.length - 1]); lastDepartedAirport =
		 * airports[airports.length - 1]; lastArrivalTime = flightSegmentTO.getArrivalDateTime();
		 * 
		 * if (!tempDtos.containsKey(ondSequence)) { tempDtos.put(ondSequence, new ArrayList<FlightSegmentTO>()); }
		 * 
		 * tempDtos.get(ondSequence).add(flightSegmentTO); } splitTos.addAll(tempDtos.values()); }
		 */

		return splitTos;
	}

	public static int getNewBaggageONDGroupId(String baggageONDGroupId) {
		baggageONDGroupId = BeanUtils.nullHandler(baggageONDGroupId);
		if (baggageONDGroupId.length() > 0) {
			return new Integer(baggageONDGroupId);
		} else {
			return AirInventoryModuleUtils.getBaggageJDBCDAO().getNextBaggageOndGroupId();
		}
	}

	public static List<String> getOndCodeOptions(String ondCode) {
		String airportsDelimiter = "/";
		String all = "All";

		List<String> possibleOnds = new ArrayList<String>();
		String[] onds = ondCode.split(airportsDelimiter);
		int ondLength = onds.length;
		String possibleOnd;

		possibleOnds.add(ondCode);
		for (int ondCount = ondLength - 1; ondCount > 0; ondCount--) {
			possibleOnd = "";
			for (int a = 0; a < ondCount; a++) {
				possibleOnd += onds[a] + airportsDelimiter;
			}
			possibleOnd += all;
			possibleOnds.add(possibleOnd);

			possibleOnd = all;
			for (int a = ondLength - ondCount; a < ondLength; a++) {
				possibleOnd += airportsDelimiter + onds[a];
			}
			possibleOnds.add(possibleOnd);
		}

		return possibleOnds;
	}

	public static String getOndAllOption() {
		String airportsDelimiter = "/";
		String all = "All";
		String allOption = all + airportsDelimiter + all;
		return allOption;
	}

	public static boolean isFareQuoateWithLogicalCabinClassSelected(List<OriginDestinationInfoDTO> originDestinationInfoDTOs) {
		for (OriginDestinationInfoDTO originDestinationInfoDTO : originDestinationInfoDTOs) {
			if (originDestinationInfoDTO.getPreferredLogicalCabin() != null
					&& !originDestinationInfoDTO.getPreferredLogicalCabin().equals("")) {
				return true;
			}
		}
		return false;
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirInventoryConfig(), "airinventory",
				"airinventory.config.dependencymap.invalid");
	}

	public static RollForwardBatchREQBD getRollForwardBatchREQBD() {
		return (RollForwardBatchREQBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, RollForwardBatchREQBD.SERVICE_NAME);
	}

	public static RollForwardBatchREQSEGBD getRollForwardBatchREQSEGBD() {
		return (RollForwardBatchREQSEGBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, RollForwardBatchREQSEGBD.SERVICE_NAME);
	}

}
