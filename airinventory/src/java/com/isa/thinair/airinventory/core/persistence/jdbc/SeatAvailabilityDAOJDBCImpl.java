package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.LightBCAvailability;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.persistence.dao.SeatAvailabilityDAOJDBC;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.FilterAllBucketsResultSetExtractor;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.FilterBucketsResultSetExtractor;
import com.isa.thinair.airinventory.core.persistence.jdbc.extractor.SeatAvailabilityResultSetExtractor;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class SeatAvailabilityDAOJDBCImpl implements SeatAvailabilityDAOJDBC {

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Retrieve minimum standard and fixed fare from a segment inventory.
	 * 
	 * @param outboundSingleFlights
	 *            Collection of AvailableFlightSegment
	 * @return AvailableFlightSegment
	 */
	@Override
	public AvailableFlightSegment flightSegmentAvailabilitySearch(AvailableFlightSegment flightSegment,
			BucketFilteringCriteria bucketFilteringCriteria) throws ModuleException {
		// filter booking codes wise seats based on availability and fare rules
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		AvailableFlightSegment availableFlightSegment = null;
		if (bucketFilteringCriteria.isQuoteSameFareBucket()) {
			availableFlightSegment = (AvailableFlightSegment) jdbcTemplate.query(
					getQuerySameFareBucket(bucketFilteringCriteria, false),
					new SeatAvailabilityResultSetExtractor(flightSegment, bucketFilteringCriteria));
		} else {
			availableFlightSegment = (AvailableFlightSegment) jdbcTemplate.query(getQuery(bucketFilteringCriteria, false),
					new SeatAvailabilityResultSetExtractor(flightSegment, bucketFilteringCriteria));
		}

		if (bucketFilteringCriteria.isWaitListingSearch() && !isBucketsAvailable(availableFlightSegment)) {
			bucketFilteringCriteria.setSearchForWaitListingSeatAvailability(true);
			availableFlightSegment = (AvailableFlightSegment) jdbcTemplate.query(
					getQueryForWaitListingSearch(bucketFilteringCriteria, false),
					new SeatAvailabilityResultSetExtractor(flightSegment, bucketFilteringCriteria));
			if (isBucketsAvailable(availableFlightSegment)) {
				flightSegment.setWaitListed(true);
			}
		}
		return availableFlightSegment;
	}

	/**
	 * Filters buckets available in a flight segment (buckets are filter based on effective fare and fare rules) Only
	 * non-standard buckets are filtered based on available seats on the bucket
	 * 
	 * @param flightSegment
	 * @param bucketFilteringCriteria
	 * @return
	 * @throws ModuleException
	 */

	@Override
	public FilteredBucketsDTO fileterBucketsForFlightSegment(BucketFilteringCriteria bucketFilteringCriteria,
			AvailableFlightSegment availableFlightSegment) throws ModuleException {
		// check segment seat availability
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		FilteredBucketsDTO filteredBucketsDTO = null;
		if (bucketFilteringCriteria.isQuoteSameFareBucket()) {
			filteredBucketsDTO = (FilteredBucketsDTO) jdbcTemplate.query(getQuerySameFareBucket(bucketFilteringCriteria, false),
					new FilterBucketsResultSetExtractor(bucketFilteringCriteria));
		} else {
			filteredBucketsDTO = (FilteredBucketsDTO) jdbcTemplate.query(getQuery(bucketFilteringCriteria, false),
					new FilterBucketsResultSetExtractor(bucketFilteringCriteria));
		}

		if (bucketFilteringCriteria.isWaitListingSearch() && isBucketsEmpty(filteredBucketsDTO)) {
			bucketFilteringCriteria.setSearchForWaitListingSeatAvailability(true);
			filteredBucketsDTO = (FilteredBucketsDTO) jdbcTemplate.query(
					getQueryForWaitListingSearch(bucketFilteringCriteria, true),
					new FilterBucketsResultSetExtractor(bucketFilteringCriteria));
			if (!isBucketsEmpty(filteredBucketsDTO)) {
				availableFlightSegment.setWaitListed(true);
			}
		}

		return filteredBucketsDTO;

	}

	// AllFaresBCInventoriesSummaryDTOs
	@Override
	public LinkedHashMap<String, AllFaresBCInventorySummaryDTO>
			getAllBucketFareCombinationsForSegment(BucketFilteringCriteria criteria) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		@SuppressWarnings("unchecked")
		LinkedHashMap<String, AllFaresBCInventorySummaryDTO> allFaresBCInventorySummaryDTOs = (LinkedHashMap<String, AllFaresBCInventorySummaryDTO>) jdbcTemplate
				.query(getQuery(criteria, false), new FilterAllBucketsResultSetExtractor(criteria));
		return allFaresBCInventorySummaryDTOs;
	}

	/**
	 * Retrieve overall seat availability, unsold fixed quota for agent, unsold fixed for other agents/non-agents
	 * 
	 * @param flightId
	 * @param segmentCode
	 * @param logicalCabinClassCode
	 * @param agentCode
	 * @return int [] element 0 - overall availability element 1 - available fixed quota for any agent element 2 -
	 *         available infant seats - element 3 - available child seats element 4 - Allocated seats for the flight
	 *         segment. element 5 - Oversell Seats element 6 - Curtailed Seats element 7 - Sold Seats element 8 - On
	 *         hold Seats
	 * 
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, List<Integer>> getAvailableNonFixedAndFixedSeatsCounts(int flightId, String segmentCode,
			String cabinClassCode, String logicalCabinClassCode, String bookingClassCode) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String[] args = new String[3];
		Object[] params = { BookingClass.FIXED_FLAG_Y, Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)),
				new Integer(flightId), segmentCode, FCCSegBCInventory.Status.OPEN, new Integer(flightId), segmentCode };
		if (bookingClassCode == null) {
			args[0] = "";
		} else {
			args[0] = "AND fccsba.booking_code like (SELECT DECODE(bc1.fixed_flag, 'Y', bc1.booking_code, '%')"
					+ "FROM t_booking_class bc1 WHERE bc1.booking_code = '" + bookingClassCode + "')";
		}
		if (cabinClassCode != null) {
			args[1] = "AND ilogcc.cabin_class_code = '" + cabinClassCode + "'";
			args[2] = "AND logcc.cabin_class_code = '" + cabinClassCode + "'";
		} else if (logicalCabinClassCode != null) {
			args[1] = "AND ilogcc.logical_cabin_class_code = '" + logicalCabinClassCode + "'";
			args[2] = "AND logcc.logical_cabin_class_code = '" + logicalCabinClassCode + "'";
		} else {
			args[1] = "";
			args[2] = "";
		}

		String query = AirinventoryUtils.getAirInventoryConfig()
				.getQuery(AirinventoryCustomConstants.SINGLE_FLT_SEGMENT_SEAT_AVAIL, args);
		return (Map<String, List<Integer>>) jdbcTemplate.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, List<Integer>> logicalCabinClassWiseSeatInfo = new HashMap<String, List<Integer>>();
				while (rs.next()) {
					List<Integer> logicalCabinClassSeatInfo = new ArrayList<Integer>();
					logicalCabinClassSeatInfo.add(rs.getInt("available_seats"));
					logicalCabinClassSeatInfo.add(rs.getInt("available_fixed_seats"));
					logicalCabinClassSeatInfo.add(rs.getInt("available_infant_seats"));
					logicalCabinClassWiseSeatInfo.put(rs.getString("logical_cabin_class_code"), logicalCabinClassSeatInfo);
				}
				return logicalCabinClassWiseSeatInfo;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, List<Integer>> getAvailableNonFixedAndFixedSeatsCounts(int fccsaId, String bookingClassCode)
			throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Object[] params = { BookingClass.FIXED_FLAG_Y, Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)),
				fccsaId, FCCSegBCInventory.Status.OPEN, bookingClassCode, fccsaId };

		String query = AirinventoryUtils.getAirInventoryConfig()
				.getQuery(AirinventoryCustomConstants.SINGLE_FLT_SEGMENT_SEAT_AVAILABILITY_FOR_GIVEN_FCCSA_ID, null);
		return (Map<String, List<Integer>>) jdbcTemplate.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, List<Integer>> logicalCabinClassWiseSeatInfo = new HashMap<String, List<Integer>>();
				while (rs.next()) {
					List<Integer> logicalCabinClassSeatInfo = new ArrayList<Integer>();
					logicalCabinClassSeatInfo.add(rs.getInt("available_seats"));
					logicalCabinClassSeatInfo.add(rs.getInt("available_fixed_seats"));
					logicalCabinClassSeatInfo.add(rs.getInt("available_infant_seats"));
					logicalCabinClassWiseSeatInfo.put(rs.getString("logical_cabin_class_code"), logicalCabinClassSeatInfo);
				}
				return logicalCabinClassWiseSeatInfo;
			}
		});
	}

	/**
	 * Quotes goshow fare. TODO replace with quoteFareForExternalBookingChannel
	 * 
	 * @param goshowFQCriteria
	 * @return
	 */
	@Override
	public Object[] quoteGoshowFare(ReconcilePassengersTO goshowFQCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Collection<String> withInCutOffTimeBCList = AirinventoryUtils.getAirInventoryConfig()
				.getGoshowBCListForAgentBookingsWithinCutoffTime();

		String withInCutOffTimeBCStr = "'-'";
		String[] args = new String[1];
		if (withInCutOffTimeBCList != null && withInCutOffTimeBCList.size() > 0) {
			withInCutOffTimeBCStr = Util.buildStringInClauseContent(withInCutOffTimeBCList);
		}
		args[0] = withInCutOffTimeBCStr;

		Object[] params = { new Integer(goshowFQCriteria.getFlightSegId()), new Integer(goshowFQCriteria.getFlightSegId()),
				new java.sql.Date(CalendarUtil.getCurrentSystemTimeInZulu().getTime()), BookingClass.PassengerType.GOSHOW,
				goshowFQCriteria.getCabinClassCode(), goshowFQCriteria.getPaxCategoryCode() };

		String query = AirinventoryUtils.getAirInventoryConfig().getQuery(AirinventoryCustomConstants.GOSHOW_FARE_QUOTE, args);

		return (Object[]) jdbcTemplate.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					Object[] fareInfo = new Object[7];
					fareInfo[0] = new Integer(rs.getInt("fare_id"));
					fareInfo[1] = new Double(rs.getDouble("fare_amount"));
					fareInfo[2] = rs.getString("booking_code");
					fareInfo[3] = new Double(rs.getDouble("child_fare"));
					fareInfo[4] = rs.getString("child_fare_type");
					fareInfo[5] = new Double(rs.getDouble("infant_fare"));
					fareInfo[6] = rs.getString("infant_fare_type");

					return fareInfo;
				} else {
					// no matching goshow fare found
					return null;
				}
			}

		});
	}

	@Override
	public FareSummaryDTO quoteFareForExternalBookingChannel(ReconcilePassengersTO passengersTO, BookingClass gdsBookingClass) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Object[] params = { new Integer(passengersTO.getFlightSegId()), new Integer(passengersTO.getFlightSegId()),
				new java.sql.Date(CalendarUtil.getCurrentSystemTimeInZulu().getTime()), gdsBookingClass.getPaxType(),
				passengersTO.getCabinClassCode(), passengersTO.getPaxCategoryCode() };

		String query = AirinventoryUtils.getAirInventoryConfig().getQuery(AirinventoryCustomConstants.GOSHOW_FARE_QUOTE, null);

		return (FareSummaryDTO) jdbcTemplate.query(query, params, new ResultSetExtractor() {

			@Override
			public FareSummaryDTO extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
					fareSummaryDTO.setFareId(new Integer(rs.getInt("fare_id")));
					fareSummaryDTO.setAdultFare(new Double(rs.getDouble("fare_amount")));
					fareSummaryDTO.setBookingClassCode(rs.getString("booking_code"));
					fareSummaryDTO.setChildFare(new Double(rs.getDouble("child_fare")));
					fareSummaryDTO.setChildFareType(rs.getString("child_fare_type"));
					fareSummaryDTO.setInfantFare(new Double(rs.getDouble("infant_fare")));
					fareSummaryDTO.setInfantFareType(rs.getString("infant_fare_type"));

					return fareSummaryDTO;
				} else {
					// no matching goshow fare found
					return null;
				}
			}

		});
	}

	private DataSource getDataSource() {
		return ((AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig()).getDataSource();
	}

	private String getQuery(BucketFilteringCriteria bucketFilteringCriteria, boolean restictBcAvailability) {
		StringBuffer stringBuffer = new StringBuffer();
		java.util.Date currentZuluTume = CalendarUtil.getCurrentSystemTimeInZulu();

		if (bucketFilteringCriteria.isFlightWithinCutoff() && bucketFilteringCriteria.isOverbookEnabled()
				&& !bucketFilteringCriteria.getBookingClassTypes().contains(BookingClass.BookingClassType.OVERBOOK)) {
			Collection<String> bookingClassTypes = new ArrayList<String>();
			bookingClassTypes.add(BookingClass.BookingClassType.OVERBOOK);
			bucketFilteringCriteria.setBookingClassTypes(bookingClassTypes);
		}
		boolean isNestedAvailability = false;
		if (!StringUtil.isNullOrEmpty(bucketFilteringCriteria.getBookingClassCode())
				&& AppSysParamsUtil.showNestedAvailableSeats()
				&& BookingClassUtil.isOnlyNormalOrOverbookBcType(bucketFilteringCriteria.getBookingClassTypes())) {
			isNestedAvailability = true;
		}

		String preferredLanguage = bucketFilteringCriteria.getPreferredLanguage() != null
				&& !bucketFilteringCriteria.getPreferredLanguage().isEmpty()
						? bucketFilteringCriteria.getPreferredLanguage()
						: Locale.ENGLISH.getLanguage();

		boolean isChannelWebAndrioidOrIOS = SalesChannelsUtil.isSalesChannelWebOrMobile(bucketFilteringCriteria.getChannelCode());

		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			stringBuffer.append(" SELECT frc.RULES_COMMENTS as comment_in_selected, avail_result.* ");
			stringBuffer.append(" FROM t_fare_rule_comment frc, ( ");
		}

		boolean isTotalPriceBasedOnMinFareQuoteEnabled = AppSysParamsUtil.isEnableTotalPriceBasedMinimumFareQuote();
		stringBuffer.append("SELECT distinct ");

		if (isNestedAvailability) {
			stringBuffer.append("	decode(bc.booking_code,'" + bucketFilteringCriteria.getBookingClassCode() + "',0,99), ");
		}

		stringBuffer.append("fccsba.fccsba_id as fccsba_id, fccsba.flight_id as flight_id,");
		stringBuffer.append(" fccsba.segment_code as segment_code, fccsba.allocated_seats as allocated_seats, ");
		stringBuffer.append(" fccsba.available_seats as available_seats, fccsba.sold_seats as sold_seats, ");
		stringBuffer.append(" fccsba.onhold_seats as onhold_seats, fccsba.status as status, lgcc.cabin_class_code ");
		stringBuffer.append(" as cabin_class_code, f.fare_id as fare_id, f.fare_amount as fare_amount, ");

		if (isTotalPriceBasedOnMinFareQuoteEnabled) {
			stringBuffer.append(" (CASE WHEN (bc.charge_flags = 'SYTY' AND fr.return_flag = 'N')   THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SYTY") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactor("SYTY") + ") "
					+ "WHEN (bc.charge_flags = 'SYTY' AND fr.return_flag = 'Y')   THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SYTY") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactorForReturn("SYTY") + ") "
					+ "    WHEN (bc.charge_flags = 'SYTN' AND fr.return_flag = 'N')   THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SYTN") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactor("SYTN") + ") "
					+ "    WHEN (bc.charge_flags = 'SYTN' AND fr.return_flag = 'Y')   THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SYTN") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactorForReturn("SYTN") + ") "
					+ "    WHEN (bc.charge_flags = 'SNTY' AND fr.return_flag = 'N')    THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SNTY") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactor("SNTY") + ") "
					+ "    WHEN (bc.charge_flags = 'SNTY' AND fr.return_flag = 'Y')    THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SNTY") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactorForReturn("SNTY") + ") "
					+ "    WHEN (bc.charge_flags = 'SNTN' AND fr.return_flag = 'N')    THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SNTN") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactor("SNTN") + ") "
					+ "    WHEN (bc.charge_flags = 'SNTN' AND fr.return_flag = 'Y')    THEN (f.fare_amount * ("
					+ bucketFilteringCriteria.getTotalPercentageFactor("SNTN") + ") + "
					+ bucketFilteringCriteria.getTotalValueFactorForReturn("SNTN") + ") END ) as TOTAL_PRORATED_FARE , ");
		} else {
			stringBuffer.append(" f.fare_amount as TOTAL_PRORATED_FARE , ");
		}

		stringBuffer.append(" f.currency_code as currency_code, f.ond_code,");
		stringBuffer.append(" fr.fare_rule_code,fr.fare_basis_code, fr.rules_comments, fr.agent_instructions, ");
		stringBuffer.append(" fr.fare_cat_id, fr.fare_rule_id, fr.minimum_stay_over_mins, ");
		stringBuffer.append(" fr.maximum_stay_over_mins,fr.openrt_conf_period_mins, fr.minimum_stay_over_months, ");
		stringBuffer.append(" fr.maximum_stay_over_months,fr.openrt_conf_stay_over_months, fr.halfrt_flag, ");
		stringBuffer.append(" fr.fare_discount_max, fr.agent_commission_type, fr.agent_commission_amount, fr.return_flag, ");
		stringBuffer.append(" f.booking_code as booking_code, f.child_fare_type, f.child_fare,");
		stringBuffer.append(" f.infant_fare_type, f.infant_fare,bc.standard_code as standard_flag, bc.nest_rank as nest_rank, ");
		stringBuffer.append(" bc.fixed_flag as fixed_flag, bc.bc_type as bc_type, bc.onhold_flag,fr.fare_discount_min,");
		stringBuffer.append(" lgcc.logical_cabin_class_code as logical_cabin_class, lgcc.LOGICAL_CC_RANK");

		if (isTotalPriceBasedOnMinFareQuoteEnabled) {
			stringBuffer.append(" , bc.charge_flags as RATIO_CAT ");

		} else {
			stringBuffer.append(" , 'NN' as RATIO_CAT ");
		}

		if (bucketFilteringCriteria.isFlexiQuote()) {
			Date departureDate = null;
			long timeGapToFlightDeparture = 0;
			if (bucketFilteringCriteria.isReturnOnd() && !bucketFilteringCriteria.isOpenReturn()
					&& bucketFilteringCriteria.isReturnFareSearch()) {
				timeGapToFlightDeparture = bucketFilteringCriteria.getInboundDateZulu().getTime() - currentZuluTume.getTime();
				departureDate = bucketFilteringCriteria.getInboundDate();
			} else {
				departureDate = bucketFilteringCriteria.getDepatureDate();
				timeGapToFlightDeparture = bucketFilteringCriteria.getDepartureDateTimeZulu().getTime()
						- currentZuluTume.getTime();
			}

			stringBuffer.append(",(select unique frfr.flexi_rule_id from t_flexi_rule_rate flxrate, t_fare_rule_flexi_rule");
			stringBuffer.append(" frfr , t_flexi_rule_flexibility frf WHERE fr.fare_rule_id = frfr.fare_rule_id ");
			stringBuffer.append("AND flxrate.flexi_rule_id = frfr.flexi_rule_id ");
			stringBuffer.append("AND to_date(" + CalendarUtil.formatForSQL_toDate(currentZuluTume));
			stringBuffer.append(") BETWEEN flxrate.sales_date_from AND flxrate.sales_date_to AND ");
			stringBuffer.append("to_date(" + CalendarUtil.formatForSQL_toDate(departureDate));
			stringBuffer.append(") BETWEEN flxrate.dep_date_from AND flxrate.dep_date_to AND frf.flexi_rule_id ");
			stringBuffer.append("= frfr.flexi_rule_id AND frf.cut_over_mins * 60000 < ");
			stringBuffer.append(timeGapToFlightDeparture + ") as flexi_rule_id ");
		} else {
			stringBuffer.append(", 0 as flexi_rule_id ");
		}
		if (bucketFilteringCriteria.getNoOfAdults() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='AD' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as AD_APPLICABILITY");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='AD' AND FRP.fare_rule_id(+) = fr.fare_rule_id) AD_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.getNoOfChilds() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='CH' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as CH_APPLICABILITY");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='CH' AND FRP.fare_rule_id(+) = fr.fare_rule_id) as CH_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.getNoOfInfants() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='IN' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as IN_APPLICABILITY ");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='IN' AND FRP.fare_rule_id(+) = fr.fare_rule_id) IN_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.isRetrieveChannelAndAgentInfo()) {
			stringBuffer.append(" , fv.sales_channel_code as sales_channel_code");

			if (!isChannelWebAndrioidOrIOS) {
				stringBuffer.append(" , fa.agent_code as agent_code ");
			}
		}
		stringBuffer.append(",(SELECT fccsa.available_seats - NVL(SUM(fix_fsba.allocated_seats - FIX_FSBA.SOLD_SEATS -");
		stringBuffer.append(" FIX_FSBA.ONHOLD_SEATS),0) FROM t_fcc_seg_bc_alloc");
		stringBuffer.append(" fix_fsba, t_booking_class fix_bc WHERE fix_fsba.booking_code = fix_bc.booking_code AND ");
		stringBuffer.append("fix_fsba.fccsa_id = fccsa.fccsa_id AND fix_bc.fixed_flag = 'Y' AND fix_bc.bc_type NOT IN ");
		stringBuffer.append(" ('STANDBY','OPENRT') AND fix_fsba.status like 'OPN') AS available_non_fixed_seats, ");

		// nested available seat count
		stringBuffer.append("(CASE WHEN bc.standard_code='Y' AND fccsba.status = 'OPN' AND ");
		stringBuffer.append("bc.nest_rank > 0  THEN (SELECT SUM(available_seats) FROM t_fcc_seg_bc_alloc a,t_booking_class b ");
		stringBuffer.append("where fccsa_id = fccsa.fccsa_id and b.booking_code=a.booking_code ");
		stringBuffer.append("AND a.status = 'OPN' AND b.nest_rank  <= bc.nest_rank) ");
		stringBuffer.append("ELSE fccsba.available_seats  END) AS nested_available_seats,");

		stringBuffer.append("(SELECT nvl(sum(sold_seats),0) FROM t_fcc_seg_bc_nesting WHERE from_fccsba_id=fccsba.fccsba_id) ");
		stringBuffer.append(" AS nested_sold_seats, (SELECT nvl(sum(onhold_seats),0) FROM t_fcc_seg_bc_nesting WHERE ");
		stringBuffer.append("from_fccsba_id=fccsba.fccsba_id) AS nested_onhold_seats, (SELECT nvl(sum(sold_seats),0) FROM ");
		stringBuffer.append(" t_fcc_seg_bc_nesting WHERE to_fccsba_id=fccsba.fccsba_id) AS nested_aquired_sold_seats,");
		stringBuffer.append("(SELECT nvl(sum(onhold_seats),0) FROM t_fcc_seg_bc_nesting WHERE to_fccsba_id=fccsba.fccsba_id) ");
		stringBuffer.append(" AS nested_acquired_onhold_seats,flt.operation_type_id AS operation_type_id ");

		stringBuffer.append(" FROM t_booking_class bc,t_ond_fare f,t_fare_rule fr,t_fare_visibility fv,t_fcc_seg_alloc fccsa,");
		stringBuffer
				.append(" t_fare_rule_pos frpos, t_fare_rule_pax frp, t_fcc_seg_bc_alloc fccsba, t_logical_cabin_class lgcc, ");

		if (!isChannelWebAndrioidOrIOS) {
			stringBuffer.append(" t_fare_agent fa, ");
		}

		stringBuffer.append(
				" t_flight flt WHERE bc.booking_code = f.booking_code AND bc.status = 'ACT' AND fr.fare_rule_id = f.fare_rule_id ");
		stringBuffer.append(" AND fr.fare_rule_id = fv.fare_rule_id AND fr.fare_rule_id = frp.fare_rule_id ");

		if (!isChannelWebAndrioidOrIOS) {
			stringBuffer.append("  AND fr.fare_rule_id = fa.fare_rule_id (+) ");
		}

		stringBuffer.append(" AND fr.fare_rule_id = frpos.fare_rule_id(+) AND lgcc.logical_cabin_class_code = ");
		stringBuffer.append(" fccsba.logical_cabin_class_code AND bc.booking_code = fccsba.booking_code AND ");
		stringBuffer.append(" fccsa.fccsa_id  = fccsba.fccsa_id AND fccsba.flight_id= " + bucketFilteringCriteria.getFlightId());
		stringBuffer.append(" AND fccsba.segment_code = '" + bucketFilteringCriteria.getSegmentCode() + "' ");
		stringBuffer.append(" AND fccsba.flight_id = flt.flight_id ");
		if (bucketFilteringCriteria.getRequestedLogicalCabinClassCode() != null
				&& !bucketFilteringCriteria.getRequestedLogicalCabinClassCode().equals("")) {
			stringBuffer.append(
					"AND lgcc.logical_cabin_class_code = '" + bucketFilteringCriteria.getRequestedLogicalCabinClassCode() + "' ");
		} else if (bucketFilteringCriteria.getCabinClassCode() != null) {
			stringBuffer.append(" AND lgcc.cabin_class_code = '" + bucketFilteringCriteria.getCabinClassCode() + "' ");
		}
		if (!bucketFilteringCriteria.skipInvCheckForFlown()) {
			if (bucketFilteringCriteria.hasBookedFlightBookingClass()) {
				if (BookingClassUtil.byPassSegInvUpdateForOverbook(bucketFilteringCriteria.getBookingClassTypes())
						&& bucketFilteringCriteria.getBookingClassCode() != null) {
					stringBuffer
							.append(" AND (fccsba.status = 'OPN' or (fccsba.status = 'CLS' AND fccsba.status_chg_action <> '");
					stringBuffer.append(FCCSegBCInventory.StatusAction.MANUAL + "') ");
				} else {
					stringBuffer.append(" AND (fccsba.status = 'OPN' ");
				}
				stringBuffer.append(
						" OR ( fccsba.booking_code = '" + bucketFilteringCriteria.getBookedFlightBookingClass() + "' )) ");
			} else {
				if (BookingClassUtil.byPassSegInvUpdateForOverbook(bucketFilteringCriteria.getBookingClassTypes())
						&& bucketFilteringCriteria.getBookingClassCode() != null) {
					stringBuffer
							.append(" AND (fccsba.status = 'OPN' or (fccsba.status = 'CLS' AND fccsba.status_chg_action <> '");
					stringBuffer.append(FCCSegBCInventory.StatusAction.MANUAL + "')) ");
				} else {
					stringBuffer.append(" AND fccsba.status = 'OPN' ");
				}
			}
		}
		stringBuffer.append(" AND f.ond_code = '" + (bucketFilteringCriteria.isConfirmOpenReturn()
				? ReservationApiUtils.getInverseOndCode(bucketFilteringCriteria.getOndCode())
				: bucketFilteringCriteria.getOndCode()) + "' AND lgcc.STATUS = 'ACT' ");
		// if (!bucketFilteringCriteria.isReturnFlag()
		// || (bucketFilteringCriteria.isReturnFlag() && bucketFilteringCriteria.getInboundDate() != null)) {
		// stringBuffer.append(" AND to_date(" +
		// CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
		// stringBuffer.append(" ) BETWEEN f.effective_from_date AND f.effective_to_date ");
		// } else if (bucketFilteringCriteria.isReturnFlag() && bucketFilteringCriteria.getInboundDate() == null) {
		// stringBuffer.append(" AND to_date(" +
		// CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
		// stringBuffer.append(" ) BETWEEN f.rt_effective_from_date AND f.rt_effective_to_date ");
		// }
		// if (bucketFilteringCriteria.isReturnFlag() && bucketFilteringCriteria.getInboundDate() != null) {
		// stringBuffer.append(" AND to_date(" +
		// CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getInboundDate()));
		// stringBuffer.append(" ) BETWEEN f.rt_effective_from_date AND f.rt_effective_to_date ");
		// }

		if (bucketFilteringCriteria.isConfirmOpenReturn()) {
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
			stringBuffer.append(" ) BETWEEN f.rt_effective_from_date AND f.rt_effective_to_date ");
		} else if (!bucketFilteringCriteria.isOpenReturn() && bucketFilteringCriteria.isReturnFareSearch()) {
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
			stringBuffer.append(" ) BETWEEN f.effective_from_date AND f.effective_to_date ");
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getInboundDate()));
			stringBuffer.append(" ) BETWEEN f.rt_effective_from_date AND f.rt_effective_to_date ");
		} else {
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
			stringBuffer.append(" ) BETWEEN f.effective_from_date AND f.effective_to_date ");
		}

		stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getFareQuoteDate()));
		stringBuffer.append(" ) BETWEEN f.sales_valid_from AND f.sales_valid_to AND f.status = 'ACT'");
		// if (bucketFilteringCriteria.getEnforceFareCheckForModSeg() && bucketFilteringCriteria.isReturnFlag()) {
		// // half return fare % for MOD segment
		// if (bucketFilteringCriteria.isInboundFlight()) {
		// stringBuffer
		// .append(" And ((select (100-param_value) from t_app_parameter where param_key = 'RES_5') * f.fare_amount/100)
		// >= "
		// + Math.floor(bucketFilteringCriteria.getMinPaxFareAmount().doubleValue()));
		// } else {
		// stringBuffer
		// .append(" And ((select param_value from t_app_parameter where param_key = 'RES_5') * f.fare_amount/100) >= "
		// + Math.floor(bucketFilteringCriteria.getMinPaxFareAmount().doubleValue()));
		// }
		if (bucketFilteringCriteria.getEnforceFareCheckForModSeg() && bucketFilteringCriteria.getMinPaxFareAmount() != null) {
			stringBuffer
					.append(" AND f.fare_amount >= " + Math.floor(bucketFilteringCriteria.getMinPaxFareAmount().doubleValue()));
		}
		if (((!AppSysParamsUtil.isSkipSalesChannelCheckForFlown()
				|| AppSysParamsUtil.isSkipSalesChannelCheckForFlown() && !bucketFilteringCriteria.isFlownOnd())
				&& bucketFilteringCriteria.getChannelCode() != -1)
				|| isChannelWebAndrioidOrIOS) {
			int salesChannel = (bucketFilteringCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES
					? SalesChannelsUtil.SALES_CHANNEL_AGENT
					: bucketFilteringCriteria.getChannelCode());
			stringBuffer.append(" AND (fv.sales_channel_code = "
					+ SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY)
					+ " OR fv.sales_channel_code = " + salesChannel + ") ");

			if (AppSysParamsUtil.isPosWebFaresEnabled()
					&& isChannelWebAndrioidOrIOS) {
				stringBuffer.append(" AND ((fv.sales_channel_code <> ").append(SalesChannelsUtil.SALES_CHANNEL_WEB)
						.append(" AND fv.sales_channel_code <> ").append(SalesChannelsUtil.SALES_CHANNEL_IOS)
						.append(" AND fv.sales_channel_code <> ").append(SalesChannelsUtil.SALES_CHANNEL_ANDROID).append(" ) ");
				stringBuffer.append(" OR ((fv.sales_channel_code = ").append(SalesChannelsUtil.SALES_CHANNEL_WEB)
						.append(" OR fv.sales_channel_code = ").append(SalesChannelsUtil.SALES_CHANNEL_IOS)
						.append(" OR fv.sales_channel_code = ").append(SalesChannelsUtil.SALES_CHANNEL_ANDROID).append(" ) ");
				stringBuffer.append(" AND (fr.applicable_to_all_countries = 'Y' OR (fr.applicable_to_all_countries = 'N' ");
				stringBuffer.append(" AND frpos.country_code = '")
						.append(PlatformUtiltiies.nullHandler(bucketFilteringCriteria.getPointOfSale())).append("')))) ");
			}
		}

		if (bucketFilteringCriteria.isGoshoFareOnly()) {
			stringBuffer.append(" AND fa.agent_code like " + "'" + bucketFilteringCriteria.getAgentCode() + "' ");
		} else if (!isChannelWebAndrioidOrIOS && bucketFilteringCriteria.getAgentCode() != null) {
			stringBuffer.append(
					" AND (fa.agent_code like '" + bucketFilteringCriteria.getAgentCode() + "'  OR fa.agent_code is null) ");
		}
		stringBuffer.append(" AND (fr.advance_booking_days*60*1000<=" + bucketFilteringCriteria.getAdvancedBookingDays()
				+ " OR fr.advance_booking_days is null)");
		if (!AirPricingCustomConstants.BookingPaxType.ANY.equals(bucketFilteringCriteria.getBookingPaxType())
				&& !StringUtil.isNullOrEmpty(bucketFilteringCriteria.getBookingPaxType())) {
			stringBuffer.append(" AND fr.pax_category_code = '" + bucketFilteringCriteria.getBookingPaxType() + "' ");
		}
		if (!AirPricingCustomConstants.FareCategoryType.ANY.equals(bucketFilteringCriteria.getFareCategoryType())
				&& bucketFilteringCriteria.getFareCategoryType() != null) {
			stringBuffer.append(" AND fr.fare_cat_id = " + bucketFilteringCriteria.getFareCategoryType());
		}
		String dayNumberStr = Integer.toString(CalendarUtil.getDayNumber(bucketFilteringCriteria.getDepatureDate()));
		String depArrFlag = "d";
		if (!bucketFilteringCriteria.isConfirmOpenReturn()) {
			if (!bucketFilteringCriteria.isOpenReturn() && bucketFilteringCriteria.isReturnFareSearch()) {
				if (bucketFilteringCriteria.getInboundDate() != null && bucketFilteringCriteria.isReturnOnd()) {
					stringBuffer.append(" AND to_date("
							+ CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getSqlInboundTime()) + ")");
					stringBuffer.append(" BETWEEN fr.arrival_time_from AND fr.arrival_time_to ");
					depArrFlag = "a";
					String inboundDayNumberStr = Integer
							.toString(CalendarUtil.getDayNumber(bucketFilteringCriteria.getInboundDate()));
					stringBuffer.append(" AND fr.valid_days_of_week_" + depArrFlag + inboundDayNumberStr + "=1 ");
				} else if (bucketFilteringCriteria.getOutboundDate() != null) {
					stringBuffer.append(" AND to_date("
							+ CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getSqlOutboundTime()) + ")");
					stringBuffer.append(" BETWEEN fr.departure_time_from AND fr.departure_time_to ");
					String outboundDayNumberStr = Integer
							.toString(CalendarUtil.getDayNumber(bucketFilteringCriteria.getOutboundDate()));
					stringBuffer.append(" AND fr.valid_days_of_week_" + depArrFlag + outboundDayNumberStr + "=1 ");
				}
			} else {
				stringBuffer.append(
						" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getDepatureTime()) + ")");
				stringBuffer.append(" BETWEEN fr.departure_time_from AND fr.departure_time_to ");
				stringBuffer.append(" AND fr.valid_days_of_week_" + depArrFlag + dayNumberStr + "=1 ");
			}
		}
		stringBuffer.append(" AND fr.return_flag = ");
		stringBuffer.append((bucketFilteringCriteria.isReturnFareSearch() || bucketFilteringCriteria.isOpenReturn()
				|| bucketFilteringCriteria.isReturnFareRule() || bucketFilteringCriteria.isConfirmOpenReturn()) ? "'Y' " : "'N'");
		stringBuffer.append(" AND fr.status = 'ACT' ");
		stringBuffer
				.append(" AND (fr.bulk_ticket_flag = 'N' OR (fr.bulk_ticket_flag = 'Y' AND fr.min_bulk_ticket_seat_count <= ");
		stringBuffer.append(bucketFilteringCriteria.getNoOfActualRequiredSeats() + ")) ");

		if (bucketFilteringCriteria.isReturnFareJourney()
				&& bucketFilteringCriteria.isExcludeOnewayPublicFaresFromReturnJourneys()) {
			stringBuffer.append(" AND ((  fr.return_flag <> 'N' AND fv.sales_channel_code = "
					+ SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY)
					+ " )OR (fv.sales_channel_code <> "
					+ +SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY) + "))");
		}

		if (bucketFilteringCriteria.isReturnFareJourney() && bucketFilteringCriteria.hasStayOverTime()
				&& !bucketFilteringCriteria.isIgnoreStayOverTime()) {
			long stayOverMilliSeconds = bucketFilteringCriteria.getStayOverTime();
			String formattedArriavalDate = CalendarUtil
					.formatForSQL_toDate(new Timestamp(bucketFilteringCriteria.getArrivalDateTime().getTime()));
			stringBuffer.append(" AND ((" + (stayOverMilliSeconds / (1000 * 60)) + " BETWEEN "
					+ getOracleDateDifferance("fr.minimum_stay_over_mins", "fr.minimum_stay_over_months", formattedArriavalDate));
			stringBuffer.append(" and "
					+ getOracleDateDifferance("fr.maximum_stay_over_mins", "fr.maximum_stay_over_months", formattedArriavalDate)
					+ ") OR ");

			stringBuffer.append(" (fr.minimum_stay_over_mins is null AND fr.minimum_stay_over_months is null AND ");
			stringBuffer.append(
					getOracleDateDifferance("fr.maximum_stay_over_mins", "fr.maximum_stay_over_months", formattedArriavalDate)
							+ " >= " + (stayOverMilliSeconds / (1000 * 60)) + ") OR ");

			stringBuffer.append(" ("
					+ getOracleDateDifferance("fr.minimum_stay_over_mins", "fr.minimum_stay_over_months", formattedArriavalDate));
			stringBuffer.append(" <= " + (stayOverMilliSeconds / (1000 * 60))
					+ " AND fr.maximum_stay_over_mins is null AND fr.maximum_stay_over_months is null) OR ");

			stringBuffer.append(" (fr.minimum_stay_over_mins is null AND fr.minimum_stay_over_months is null AND ");
			stringBuffer.append(" fr.maximum_stay_over_mins is null AND fr.maximum_stay_over_months is null)) ");
		}
		if (bucketFilteringCriteria.getNoOfAdults() > 0) {
			stringBuffer.append(" AND fr.FARE_RULE_ID IN (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE ");
			stringBuffer.append(" (TF.PAX_TYPE_CODE='" + PaxTypeTO.ADULT + "') AND TF.APPLY_FARE = 'Y') ");
		}
		if (bucketFilteringCriteria.getNoOfChilds() > 0) {
			stringBuffer.append(" AND fr.FARE_RULE_ID IN (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE ");
			stringBuffer.append(" (TF.PAX_TYPE_CODE='" + PaxTypeTO.CHILD + "') AND TF.APPLY_FARE = 'Y') ");
		}
		if (bucketFilteringCriteria.getNoOfInfants() > 0) {
			stringBuffer.append(" AND fr.FARE_RULE_ID IN (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE ");
			stringBuffer.append(" (TF.PAX_TYPE_CODE='" + PaxTypeTO.INFANT + "') AND TF.APPLY_FARE = 'Y') ");
		}
		if (bucketFilteringCriteria.getBookingClassCode() != null) {

			if (isNestedAvailability) {
				stringBuffer.append(" AND (bc.booking_code LIKE '" + bucketFilteringCriteria.getBookingClassCode() + "' OR  ");
				stringBuffer.append(
						" (bc.nest_rank > 0 AND bc.nest_rank <= (select ba.nest_rank from t_booking_class ba,t_fcc_seg_bc_alloc ac ");
				stringBuffer.append("where ba.booking_code='" + bucketFilteringCriteria.getBookingClassCode()
						+ "' and ba.standard_code='Y' and ba.booking_code=ac.booking_code and ac.flight_id = "
						+ bucketFilteringCriteria.getFlightId() + " and ac.available_seats < "
						+ bucketFilteringCriteria.getNoOfActualRequiredSeats() + ")))");

			} else {
				stringBuffer.append(" AND (bc.booking_code like '" + bucketFilteringCriteria.getBookingClassCode() + "' ) ");
			}

		}

		if (bucketFilteringCriteria.isGoshoFareOnly() && bucketFilteringCriteria.isFlightWithinCutoff()) {
			stringBuffer.append("AND (bc.pax_type = '" + BookingClass.PassengerType.GOSHOW + "' )");
			stringBuffer.append(" AND (bc.fixed_flag = 'N' ) ");
		} else if (bucketFilteringCriteria.isFixedFareOnly()) {
			stringBuffer.append(" AND (bc.fixed_flag = 'Y' ) ");
		}

		if (bucketFilteringCriteria.isOpenReturn() || bucketFilteringCriteria.isConfirmOpenReturn()) {
			stringBuffer.append(" AND fr.is_open_return = 'Y' ");
		}

		if (bucketFilteringCriteria.getBookingClassTypes() != null && bucketFilteringCriteria.getBookingClassTypes().size() > 0) {
			stringBuffer.append(" AND ( ");
			int index = 0;
			for (String bookingClass : bucketFilteringCriteria.getBookingClassTypes()) {
				if (BookingClass.BookingClassType.OVERBOOK.equals(bookingClass)
						|| ReservationInternalConstants.ReservationType.WL.getDescription().equals(bookingClass)) {
					bookingClass = BookingClass.BookingClassType.NORMAL;
				}
				if (index == 0) {
					stringBuffer.append(" bc.bc_type like '" + bookingClass + "' ");
				} else {
					stringBuffer.append(" OR bc.bc_type like '" + bookingClass + "' ");
				}
				index++;
			}
			stringBuffer.append(" ) ");
		}

		stringBuffer.append(" AND (fr.load_factor_min is null OR (fr.load_factor_min <= ");
		stringBuffer.append(" ROUND(((fccsa.sold_seats + fccsa.on_hold_seats) * 100)/ ");
		stringBuffer.append(" (fccsa.allocated_seats+ fccsa.oversell_seats- fccsa.curtailed_seats)))) ");
		stringBuffer.append(" AND (fr.load_factor_max is null OR (fr.load_factor_max >= ");
		stringBuffer.append(" ROUND(((fccsa.sold_seats + fccsa.on_hold_seats) * 100)/ ");
		stringBuffer.append(" (fccsa.allocated_seats+ fccsa.oversell_seats- fccsa.curtailed_seats)))) ");

		if (bucketFilteringCriteria.getFareRuleId() != null && bucketFilteringCriteria.getFareId() != null) {
			stringBuffer.append(" AND fr.fare_rule_id=" + bucketFilteringCriteria.getFareRuleId() + " AND f.fare_id= "
					+ bucketFilteringCriteria.getFareId());
		} else if (bucketFilteringCriteria.getFareId() != null) {
			stringBuffer.append(" AND f.fare_id= " + bucketFilteringCriteria.getFareId());
		}
		if (!BookingClassUtil.byPassSegInvUpdate(bucketFilteringCriteria.getBookingClassTypes())) {
			if (bucketFilteringCriteria.hasBookedFlightCabinClass()) {
				stringBuffer.append(" AND ((fccsa.available_seats >= " + bucketFilteringCriteria.getNoOfActualRequiredSeats());
				stringBuffer.append(" OR (bc.fixed_flag = 'Y' AND fccsba.available_seats >= "
						+ bucketFilteringCriteria.getNoOfActualRequiredSeats() + ")) OR lgcc.cabin_class_code = '"
						+ bucketFilteringCriteria.getBookedFlightCabinClass() + "') ");
			} else {
				stringBuffer.append(" AND (fccsa.available_seats >= " + bucketFilteringCriteria.getNoOfActualRequiredSeats());
				stringBuffer.append(" OR (bc.fixed_flag = 'Y' AND fccsba.available_seats >= "
						+ +bucketFilteringCriteria.getNoOfActualRequiredSeats() + ")) ");
			}
		}
		if (!bucketFilteringCriteria.hasBookedFlightBookingClass()) {
			if (!BookingClassUtil.byPassSegInvUpdate(bucketFilteringCriteria.getBookingClassTypes()) && restictBcAvailability) {
				stringBuffer.append(" AND fccsba.available_seats >= " + bucketFilteringCriteria.getNoOfActualRequiredSeats());
			}
		} else {
			if (!BookingClassUtil.byPassSegInvUpdate(bucketFilteringCriteria.getBookingClassTypes()) && restictBcAvailability) {
				stringBuffer.append(" AND (fccsba.available_seats >= " + bucketFilteringCriteria.getNoOfActualRequiredSeats());
				stringBuffer
						.append(" OR fccsba.booking_code = '" + bucketFilteringCriteria.getBookedFlightBookingClass() + "' ) ");
			}
		}
		if (bucketFilteringCriteria.hasBookedFlightCabinClass()) {
			stringBuffer.append(" AND (fccsa.available_infant_seats >= " + bucketFilteringCriteria.getNoOfInfants()
					+ " OR lgcc.cabin_class_code = '" + bucketFilteringCriteria.getBookedFlightCabinClass() + "' )");
		} else {
			stringBuffer.append(" AND fccsa.available_infant_seats >= " + bucketFilteringCriteria.getNoOfInfants());
		}
		stringBuffer.append(" ORDER BY ");

		if (isNestedAvailability) {
			stringBuffer.append("	decode(bc.booking_code,'" + bucketFilteringCriteria.getBookingClassCode() + "',0,99), ");
		}

		stringBuffer.append("lgcc.LOGICAL_CC_RANK, bc.standard_code desc, bc.nest_rank asc,");
		stringBuffer.append(" bc.fixed_flag asc, TOTAL_PRORATED_FARE asc");

		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			stringBuffer.append(" ) avail_result ");
			stringBuffer.append(" WHERE frc.fare_rule_id(+) = avail_result.fare_rule_id ");
			stringBuffer.append(" AND frc.language_code(+)  = '" + preferredLanguage + "' ");
		}

		if (log.isDebugEnabled()) {
			log.debug("availability search query for :" + bucketFilteringCriteria.getOndCode() + " and agent "
					+ bucketFilteringCriteria.getAgentCode() + "is " + stringBuffer.toString());
		}
		return stringBuffer.toString();
	}

	private String getQueryForWaitListingSearch(BucketFilteringCriteria bucketFilteringCriteria, boolean restictBcAvailability) {
		StringBuffer stringBuffer = new StringBuffer();
		java.util.Date currentZuluTume = CalendarUtil.getCurrentSystemTimeInZulu();

		String preferredLanguage = bucketFilteringCriteria.getPreferredLanguage() != null
				&& !bucketFilteringCriteria.getPreferredLanguage().isEmpty()
						? bucketFilteringCriteria.getPreferredLanguage()
						: Locale.ENGLISH.getLanguage();
						
		boolean isChannelWebAndrioidOrIOS = false;

		if (bucketFilteringCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_WEB
				|| bucketFilteringCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_IOS
				|| bucketFilteringCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_ANDROID) {
			isChannelWebAndrioidOrIOS = true;
		}

		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			stringBuffer.append(" SELECT frc.RULES_COMMENTS as comment_in_selected, avail_result.* ");
			stringBuffer.append(" FROM t_fare_rule_comment frc, ( ");
		}

		stringBuffer.append("SELECT distinct fccsba.fccsba_id as fccsba_id, fccsba.flight_id as flight_id,");
		stringBuffer.append(" fccsba.segment_code as segment_code, fccsba.allocated_seats as allocated_seats, ");
		stringBuffer.append(" fccsba.available_seats as available_seats, fccsba.sold_seats as sold_seats, ");
		stringBuffer.append(" fccsba.onhold_seats as onhold_seats, fccsba.status as status, lgcc.cabin_class_code ");
		stringBuffer.append(" as cabin_class_code, f.fare_id as fare_id, f.fare_amount as fare_amount,");

		stringBuffer.append(" f.currency_code as currency_code,f.ond_code,");
		stringBuffer.append(" fr.fare_rule_code,fr.fare_basis_code, fr.rules_comments, fr.agent_instructions, ");
		stringBuffer.append(" fr.fare_cat_id, fr.fare_rule_id, fr.minimum_stay_over_mins, ");
		stringBuffer.append(" fr.maximum_stay_over_mins,fr.openrt_conf_period_mins, fr.minimum_stay_over_months, ");
		stringBuffer.append(" fr.maximum_stay_over_months,fr.openrt_conf_stay_over_months, fr.halfrt_flag, ");
		stringBuffer.append(" fr.fare_discount_max, fr.agent_commission_type, fr.agent_commission_amount, ");
		stringBuffer.append(" f.booking_code as booking_code, f.child_fare_type, f.child_fare,");
		stringBuffer.append(" f.infant_fare_type, f.infant_fare,bc.standard_code as standard_flag, bc.nest_rank as nest_rank, ");
		stringBuffer.append(" bc.fixed_flag as fixed_flag, bc.bc_type as bc_type, bc.onhold_flag,fr.fare_discount_min,");
		stringBuffer.append(" lgcc.logical_cabin_class_code as logical_cabin_class, lgcc.LOGICAL_CC_RANK");
		stringBuffer.append(" , (SELECT CASE WHEN (SUR = 'true' AND TAX = 'true') THEN 'SYTY' "
				+ " WHEN (SUR = 'true' AND TAX = 'false') THEN 'SYTN' WHEN (SUR = 'false' AND TAX = 'true') THEN 'SNTY' "
				+ " WHEN (SUR = 'false' AND TAX = 'false') THEN 'SNTN' END AS RATIO_CAT "
				+ " FROM (SELECT bc_tmp.booking_code, (SELECT CASE WHEN (COUNT(booking_code) > 0 ) "
				+ " THEN 'true' ELSE 'false' END FROM t_bc_charge_group WHERE booking_code= bc_tmp.booking_code "
				+ " AND charge_group_code = 'SUR' ) AS SUR, (SELECT CASE WHEN (COUNT(booking_code) > 0 ) "
				+ " THEN 'true' ELSE 'false' END FROM t_bc_charge_group WHERE booking_code= bc_tmp.booking_code "
				+ " AND charge_group_code = 'TAX' ) AS TAX FROM t_booking_class bc_tmp where bc_tmp.booking_code = booking_code "
				+ " AND rownum  = 1)) as RATIO_CAT ");
		if (bucketFilteringCriteria.isFlexiQuote()) {
			long timeGapToFlightDeparture = bucketFilteringCriteria.getDepatureDate().getTime() - currentZuluTume.getTime();
			stringBuffer.append(",(select unique frfr.flexi_rule_id from t_flexi_rule_rate flxrate, t_fare_rule_flexi_rule");
			stringBuffer.append(" frfr , t_flexi_rule_flexibility frf WHERE fr.fare_rule_id = frfr.fare_rule_id ");
			stringBuffer.append("AND flxrate.flexi_rule_id = frfr.flexi_rule_id ");
			stringBuffer.append("AND to_date(" + CalendarUtil.formatForSQL_toDate(currentZuluTume));
			stringBuffer.append(") BETWEEN flxrate.sales_date_from AND flxrate.sales_date_to AND ");
			stringBuffer.append("to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getDepatureDate()));
			stringBuffer.append(") BETWEEN flxrate.dep_date_from AND flxrate.dep_date_to AND frf.flexi_rule_id ");
			stringBuffer.append("= frfr.flexi_rule_id AND frf.cut_over_mins * 60000 < ");
			stringBuffer.append(timeGapToFlightDeparture + ") as flexi_rule_id ");
		} else {
			stringBuffer.append(", 0 as flexi_rule_id ");
		}
		if (bucketFilteringCriteria.getNoOfAdults() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='AD' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as AD_APPLICABILITY");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='AD' AND FRP.fare_rule_id(+) = fr.fare_rule_id) AD_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.getNoOfChilds() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='CH' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as CH_APPLICABILITY");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='CH' AND FRP.fare_rule_id(+) = fr.fare_rule_id) as CH_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.getNoOfInfants() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='IN' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as IN_APPLICABILITY ");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='IN' AND FRP.fare_rule_id(+) = fr.fare_rule_id) IN_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.isRetrieveChannelAndAgentInfo()) {
			stringBuffer.append(" , fv.sales_channel_code as sales_channel_code, fa.agent_code as agent_code ");
		}
		stringBuffer.append(",(SELECT fccsa.available_seats - NVL(SUM(fix_fsba.allocated_seats - FIX_FSBA.SOLD_SEATS -");
		stringBuffer.append(" FIX_FSBA.ONHOLD_SEATS),0) FROM t_fcc_seg_bc_alloc");
		stringBuffer.append(" fix_fsba, t_booking_class fix_bc WHERE fix_fsba.booking_code = fix_bc.booking_code AND ");
		stringBuffer.append("fix_fsba.fccsa_id = fccsa.fccsa_id AND fix_bc.fixed_flag = 'Y' AND fix_bc.bc_type NOT IN ");
		stringBuffer.append(" ('STANDBY','OPENRT') AND fix_fsba.status like 'OPN') AS available_non_fixed_seats, ");

		// nested available seat count
		stringBuffer.append("(CASE WHEN fr.return_flag = 'N' AND bc.standard_code='Y' AND fccsba.status = 'OPN' AND ");
		stringBuffer.append("bc.nest_rank > 0  THEN (SELECT SUM(available_seats) FROM t_fcc_seg_bc_alloc a,t_booking_class b ");
		stringBuffer.append("where fccsa_id = fccsa.fccsa_id and b.booking_code=a.booking_code ");
		stringBuffer.append("AND a.status = 'OPN' AND b.nest_rank  <= bc.nest_rank) ");
		stringBuffer.append("ELSE fccsba.available_seats  END) AS nested_available_seats,");

		stringBuffer.append("(SELECT nvl(sum(sold_seats),0) FROM t_fcc_seg_bc_nesting WHERE from_fccsba_id=fccsba.fccsba_id) ");
		stringBuffer.append(" AS nested_sold_seats, (SELECT nvl(sum(onhold_seats),0) FROM t_fcc_seg_bc_nesting WHERE ");
		stringBuffer.append("from_fccsba_id=fccsba.fccsba_id) AS nested_onhold_seats, (SELECT nvl(sum(sold_seats),0) FROM ");
		stringBuffer.append(" t_fcc_seg_bc_nesting WHERE to_fccsba_id=fccsba.fccsba_id) AS nested_aquired_sold_seats,");
		stringBuffer.append("(SELECT nvl(sum(onhold_seats),0) FROM t_fcc_seg_bc_nesting WHERE to_fccsba_id=fccsba.fccsba_id) ");
		stringBuffer.append(" AS nested_acquired_onhold_seats,flt.operation_type_id AS operation_type_id ");

		stringBuffer.append(" FROM t_booking_class bc,t_ond_fare f,t_fare_rule fr,t_fare_visibility fv,t_fcc_seg_alloc fccsa,");
		stringBuffer.append(
				" t_fare_agent fa, t_fare_rule_pos frpos, t_fare_rule_pax frp, t_fcc_seg_bc_alloc fccsba, t_logical_cabin_class lgcc, ");
		stringBuffer.append(" t_flight flt WHERE bc.booking_code = f.booking_code AND fr.fare_rule_id = f.fare_rule_id ");
		stringBuffer.append(" AND fr.fare_rule_id = fv.fare_rule_id AND fr.fare_rule_id = frp.fare_rule_id ");
		stringBuffer.append(" AND fr.fare_rule_id = frpos.fare_rule_id(+) ");
		stringBuffer.append(" AND fr.fare_rule_id = fa.fare_rule_id (+) AND lgcc.logical_cabin_class_code = ");
		stringBuffer.append(" fccsba.logical_cabin_class_code AND bc.booking_code = fccsba.booking_code AND ");
		stringBuffer.append(" fccsa.fccsa_id  = fccsba.fccsa_id AND fccsba.flight_id= " + bucketFilteringCriteria.getFlightId());
		stringBuffer.append(" AND fccsba.segment_code = '" + bucketFilteringCriteria.getSegmentCode() + "' ");
		stringBuffer.append(" AND fccsba.flight_id = flt.flight_id ");
		if (bucketFilteringCriteria.getRequestedLogicalCabinClassCode() != null
				&& !bucketFilteringCriteria.getRequestedLogicalCabinClassCode().equals("")) {
			stringBuffer.append(
					"AND lgcc.logical_cabin_class_code = '" + bucketFilteringCriteria.getRequestedLogicalCabinClassCode() + "' ");
		} else if (bucketFilteringCriteria.getCabinClassCode() != null) {
			stringBuffer.append(" AND lgcc.cabin_class_code = '" + bucketFilteringCriteria.getCabinClassCode() + "' ");
		}

		if (!bucketFilteringCriteria.skipInvCheckForFlown()) {
			if (bucketFilteringCriteria.hasBookedFlightBookingClass()) {
				stringBuffer.append(" AND (fccsba.status = 'OPN' or (fccsba.status = 'CLS' AND fccsba.status_chg_action <> '");
				stringBuffer.append(FCCSegBCInventory.StatusAction.MANUAL + "') ");

				stringBuffer.append(
						" OR ( fccsba.booking_code = '" + bucketFilteringCriteria.getBookedFlightBookingClass() + "' )) ");
			} else {
				stringBuffer.append(" AND (fccsba.status = 'OPN' or (fccsba.status = 'CLS' AND fccsba.status_chg_action <> '");
				stringBuffer.append(FCCSegBCInventory.StatusAction.MANUAL + "')) ");

			}
		}

		stringBuffer.append(" AND f.ond_code = '" + (bucketFilteringCriteria.isConfirmOpenReturn()
				? ReservationApiUtils.getInverseOndCode(bucketFilteringCriteria.getOndCode())
				: bucketFilteringCriteria.getOndCode()) + "' AND lgcc.STATUS = 'ACT' ");

		if (bucketFilteringCriteria.isConfirmOpenReturn()) {
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
			stringBuffer.append(" ) BETWEEN f.rt_effective_from_date AND f.rt_effective_to_date ");
		} else if (!bucketFilteringCriteria.isOpenReturn() && bucketFilteringCriteria.isReturnFareSearch()) {
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
			stringBuffer.append(" ) BETWEEN f.effective_from_date AND f.effective_to_date ");
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getInboundDate()));
			stringBuffer.append(" ) BETWEEN f.rt_effective_from_date AND f.rt_effective_to_date ");
		} else {
			stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getOutboundDate()));
			stringBuffer.append(" ) BETWEEN f.effective_from_date AND f.effective_to_date ");
		}

		stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getFareQuoteDate()));
		stringBuffer.append(" ) BETWEEN f.sales_valid_from AND f.sales_valid_to AND f.status = 'ACT'");

		if (bucketFilteringCriteria.getEnforceFareCheckForModSeg() && bucketFilteringCriteria.getMinPaxFareAmount() != null) {
			stringBuffer
					.append(" AND f.fare_amount >= " + Math.floor(bucketFilteringCriteria.getMinPaxFareAmount().doubleValue()));
		}
		if (bucketFilteringCriteria.getChannelCode() != -1) {
			int salesChannel = (bucketFilteringCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES
					? SalesChannelsUtil.SALES_CHANNEL_AGENT
					: bucketFilteringCriteria.getChannelCode());
			stringBuffer.append(" AND (fv.sales_channel_code = "
					+ SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY)
					+ " OR fv.sales_channel_code = " + salesChannel + ") ");

			if (AppSysParamsUtil.isPosWebFaresEnabled() && isChannelWebAndrioidOrIOS) {
				stringBuffer.append(" AND ((fv.sales_channel_code <> ").append(SalesChannelsUtil.SALES_CHANNEL_WEB)
						.append(" OR fv.sales_channel_code <> ").append(SalesChannelsUtil.SALES_CHANNEL_IOS)
						.append(" OR fv.sales_channel_code <> ").append(SalesChannelsUtil.SALES_CHANNEL_ANDROID).append(" ) ");
				stringBuffer.append(" OR ((fv.sales_channel_code = ").append(SalesChannelsUtil.SALES_CHANNEL_WEB)
						.append(" OR fv.sales_channel_code = ").append(SalesChannelsUtil.SALES_CHANNEL_IOS)
						.append(" OR fv.sales_channel_code = ").append(SalesChannelsUtil.SALES_CHANNEL_ANDROID).append(" ) ");
				stringBuffer.append(" AND (fr.applicable_to_all_countries = 'Y' OR (fr.applicable_to_all_countries = 'N' ");
				stringBuffer.append(" AND frpos.country_code = '")
						.append(PlatformUtiltiies.nullHandler(bucketFilteringCriteria.getPointOfSale())).append("')))) ");
			}
		}
		stringBuffer.append(" AND (fa.agent_code like "
				+ ((bucketFilteringCriteria.getAgentCode() == null || bucketFilteringCriteria.getAgentCode().equals(""))
						? "'%'"
						: "'" + bucketFilteringCriteria.getAgentCode() + "' ")
				+ " OR fa.agent_code is null) ");
		stringBuffer.append(" AND (fr.advance_booking_days*60*1000<=" + bucketFilteringCriteria.getAdvancedBookingDays()
				+ " OR fr.advance_booking_days is null)");
		if (!AirPricingCustomConstants.BookingPaxType.ANY.equals(bucketFilteringCriteria.getBookingPaxType())
				&& !StringUtil.isNullOrEmpty(bucketFilteringCriteria.getBookingPaxType())) {
			stringBuffer.append(" AND fr.pax_category_code = '" + bucketFilteringCriteria.getBookingPaxType() + "' ");
		}
		if (!AirPricingCustomConstants.FareCategoryType.ANY.equals(bucketFilteringCriteria.getFareCategoryType())
				&& bucketFilteringCriteria.getFareCategoryType() != null) {
			stringBuffer.append(" AND fr.fare_cat_id = " + bucketFilteringCriteria.getFareCategoryType());
		}
		String dayNumberStr = Integer.toString(CalendarUtil.getDayNumber(bucketFilteringCriteria.getDepatureDate()));
		String depArrFlag = "d";
		if ((bucketFilteringCriteria.isReturnOnd() && bucketFilteringCriteria.isReturnFareSearch())
				|| bucketFilteringCriteria.isConfirmOpenReturn()) {
			depArrFlag = "a";
		}
		stringBuffer.append(" AND fr.valid_days_of_week_" + depArrFlag + dayNumberStr + "=1 ");
		stringBuffer.append(" AND to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getDepatureTime()) + ")");
		stringBuffer.append(" BETWEEN fr.departure_time_from AND fr.departure_time_to AND fr.return_flag = ");
		stringBuffer.append((bucketFilteringCriteria.isReturnFareSearch() || bucketFilteringCriteria.isOpenReturn()
				|| bucketFilteringCriteria.isConfirmOpenReturn()) ? "'Y' " : "'N'");
		if (bucketFilteringCriteria.isReturnFareJourney() && bucketFilteringCriteria.hasStayOverTime()
				&& !bucketFilteringCriteria.isIgnoreStayOverTime()) {
			long stayOverMilliSeconds = bucketFilteringCriteria.getStayOverTime();
			String formattedArriavalDate = CalendarUtil
					.formatForSQL_toDate(new Timestamp(bucketFilteringCriteria.getArrivalDateTime().getTime()));
			stringBuffer.append(" AND ((" + (stayOverMilliSeconds / (1000 * 60)) + " BETWEEN "
					+ getOracleDateDifferance("fr.minimum_stay_over_mins", "fr.minimum_stay_over_months", formattedArriavalDate));
			stringBuffer.append(" and "
					+ getOracleDateDifferance("fr.maximum_stay_over_mins", "fr.maximum_stay_over_months", formattedArriavalDate)
					+ ") OR ");

			stringBuffer.append(" (fr.minimum_stay_over_mins is null AND fr.minimum_stay_over_months is null AND ");
			stringBuffer.append(
					getOracleDateDifferance("fr.maximum_stay_over_mins", "fr.maximum_stay_over_months", formattedArriavalDate)
							+ " >= " + (stayOverMilliSeconds / (1000 * 60)) + ") OR ");

			stringBuffer.append(" ("
					+ getOracleDateDifferance("fr.minimum_stay_over_mins", "fr.minimum_stay_over_months", formattedArriavalDate));
			stringBuffer.append(" <= " + (stayOverMilliSeconds / (1000 * 60))
					+ " AND fr.maximum_stay_over_mins is null AND fr.maximum_stay_over_months is null) OR ");

			stringBuffer.append(" (fr.minimum_stay_over_mins is null AND fr.minimum_stay_over_months is null AND ");
			stringBuffer.append(" fr.maximum_stay_over_mins is null AND fr.maximum_stay_over_months is null)) ");
		}
		if (bucketFilteringCriteria.getNoOfAdults() > 0) {
			stringBuffer.append(" AND fr.FARE_RULE_ID IN (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE ");
			stringBuffer.append(" (TF.PAX_TYPE_CODE='" + PaxTypeTO.ADULT + "') AND TF.APPLY_FARE = 'Y') ");
		}
		if (bucketFilteringCriteria.getNoOfChilds() > 0) {
			stringBuffer.append(" AND fr.FARE_RULE_ID IN (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE ");
			stringBuffer.append(" (TF.PAX_TYPE_CODE='" + PaxTypeTO.CHILD + "') AND TF.APPLY_FARE = 'Y') ");
		}
		if (bucketFilteringCriteria.getNoOfInfants() > 0) {
			stringBuffer.append(" AND fr.FARE_RULE_ID IN (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE ");
			stringBuffer.append(" (TF.PAX_TYPE_CODE='" + PaxTypeTO.INFANT + "') AND TF.APPLY_FARE = 'Y') ");
		}
		if (bucketFilteringCriteria.getBookingClassCode() != null) {
			stringBuffer.append(" AND (bc.booking_code like '" + bucketFilteringCriteria.getBookingClassCode() + "' ) ");
		}

		if (bucketFilteringCriteria.isGoshoFareOnly()) {// TODo
			stringBuffer.append("AND (bc.pax_type = '" + BookingClass.PassengerType.GOSHOW + "' )");
			stringBuffer.append(" AND (bc.fixed_flag = 'N' ) ");
		} else if (bucketFilteringCriteria.isFixedFareOnly()) {
			stringBuffer.append(" AND (bc.fixed_flag = 'Y' ) ");
		}

		if (bucketFilteringCriteria.isOpenReturn() || bucketFilteringCriteria.isConfirmOpenReturn()) {
			stringBuffer.append(" AND fr.is_open_return = 'Y' ");
		}

		if (bucketFilteringCriteria.getBookingClassTypes() != null && bucketFilteringCriteria.getBookingClassTypes().size() > 0) {
			stringBuffer.append(" AND ( ");
			int index = 0;
			for (String bookingClassType : bucketFilteringCriteria.getBookingClassTypes()) {
				if (ReservationInternalConstants.ReservationType.WL.getDescription().equals(bookingClassType)) {
					bookingClassType = BookingClass.BookingClassType.NORMAL;
				}
				if (index == 0) {
					stringBuffer.append(" bc.bc_type like '" + bookingClassType + "' ");
				} else {
					stringBuffer.append(" OR bc.bc_type like '" + bookingClassType + "' ");
				}
				index++;
			}
			stringBuffer.append(" ) ");
		}

		if (bucketFilteringCriteria.getFareRuleId() != null && bucketFilteringCriteria.getFareId() != null) {
			stringBuffer.append(" AND fr.fare_rule_id=" + bucketFilteringCriteria.getFareRuleId() + " AND f.fare_id= "
					+ bucketFilteringCriteria.getFareId());
		}
		// Do not need to check availability as this is any how wait listed reservation
		// if (!bucketFilteringCriteria.hasBookedFlightBookingClass()) {
		// if (!BookingClassUtil.byPassSegInvUpdate(bucketFilteringCriteria.getBookingClassTypes()) &&
		// restictBcAvailability) {
		// stringBuffer.append(" AND (fccsba.available_seats < " +
		// bucketFilteringCriteria.getNoOfActualRequiredSeats());
		// stringBuffer.append(" OR fccsa.available_seats < " + bucketFilteringCriteria.getNoOfActualRequiredSeats() +
		// ")");
		// }
		// } else {
		// if (!BookingClassUtil.byPassSegInvUpdate(bucketFilteringCriteria.getBookingClassTypes()) &&
		// restictBcAvailability) {
		// stringBuffer.append(" AND (fccsba.available_seats < " +
		// bucketFilteringCriteria.getNoOfActualRequiredSeats());
		// stringBuffer.append(" OR fccsba.booking_code = '" + bucketFilteringCriteria.getBookedFlightBookingClass()
		// + "' ) ");
		// }
		// }
		// checking waitlisting inventory
		stringBuffer.append(" AND (fccsba.allocated_waitlist_seats - fccsba.waitlisted_seats) >= "
				+ bucketFilteringCriteria.getNoOfActualRequiredSeats());
		stringBuffer.append(" AND (fccsa.allocated_waitlist_seats - fccsa.waitlisted_seats) >= "
				+ bucketFilteringCriteria.getNoOfActualRequiredSeats());

		// Only checking infant allocation is enough to confirm wait listing reservation
		if (bucketFilteringCriteria.hasBookedFlightCabinClass()) {
			stringBuffer.append(" AND (fccsa.available_infant_seats >= " + bucketFilteringCriteria.getNoOfInfants()
					+ " OR lgcc.cabin_class_code = '" + bucketFilteringCriteria.getBookedFlightCabinClass() + "' )");
		} else {
			stringBuffer.append(" AND fccsa.available_infant_seats >= " + bucketFilteringCriteria.getNoOfInfants());
		}

		stringBuffer.append(" ORDER BY lgcc.LOGICAL_CC_RANK, bc.standard_code desc, bc.nest_rank asc,");
		stringBuffer.append(" bc.fixed_flag asc, f.fare_amount asc");
		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			stringBuffer.append(" ) avail_result ");
			stringBuffer.append(" WHERE frc.fare_rule_id(+) = avail_result.fare_rule_id ");
			stringBuffer.append(" AND frc.language_code(+)  = '" + preferredLanguage + "' ");
		}
		if (log.isDebugEnabled()) {
			log.debug("availability search query for :" + bucketFilteringCriteria.getOndCode() + " and agent "
					+ bucketFilteringCriteria.getAgentCode() + "is " + stringBuffer.toString());
		}
		return stringBuffer.toString();
	}

	private String getOracleDateDifferance(String minutesColumnName, String monthsColumnName, String formattedDate) {
		return " floor(((add_months((trunc(to_date(" + formattedDate + "), 'MI') + nvl(" + minutesColumnName + ", 0)/1440), nvl("
				+ monthsColumnName + ", 0))-trunc(to_date(" + formattedDate + "),'MI')))*1440) ";
	}

	// TODO add other buckets
	private boolean isBucketsAvailable(AvailableFlightSegment availableFlightSegment) {
		if (availableFlightSegment != null && availableFlightSegment.getFilteredBucketsDTOForSegmentFares() != null
				&& availableFlightSegment.getFilteredBucketsDTOForSegmentFares().getLogicalCCWiseCheapestStandardBuckets()
						.isEmpty()
				&& availableFlightSegment.getFilteredBucketsDTOForSegmentFares().getLogicalCCWiseCheapestNonStandardBuckets()
						.isEmpty()
				&& availableFlightSegment.getFilteredBucketsDTOForSegmentFares().getLogicalCCWiseCheapestFixedBuckets()
						.isEmpty()) {
			return false;
		}
		return true;
	}

	private boolean isBucketsEmpty(FilteredBucketsDTO filteredBucketsDTO) {
		if (filteredBucketsDTO.getLogicalCCWiseCheapestStandardBuckets().isEmpty()
				&& filteredBucketsDTO.getLogicalCCWiseCheapestNonStandardBuckets().isEmpty()
				&& filteredBucketsDTO.getLogicalCCWiseCheapestFixedBuckets().isEmpty()) {
			return true;
		}
		return false;
	}

	/*
	 * BC level availability for myidtravel
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, List<Pair<String, Integer>>> getBcAvailabilityForMyIDTravel(List<Integer> flightSegIds,
			String agentCode) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String query = AirinventoryUtils.getAirInventoryConfig().getQuery(AirinventoryCustomConstants.MYID_FLIGHT_BC_AVAILABILITY,
				null);

		Map<Integer, List<Pair<String, Integer>>> bcAvailabilityMap = new HashMap<Integer, List<Pair<String, Integer>>>();

		for (Integer flightSegId : flightSegIds) {

			Object[] params = { flightSegId, agentCode, flightSegId };
			List<Pair<String, Integer>> bcList = (List<Pair<String, Integer>>) jdbcTemplate.query(query, params,
					new ResultSetExtractor() {
						@Override
						public Object extractData(final ResultSet rs) throws SQLException, DataAccessException {

							List<Pair<String, Integer>> availableBcList = null;
							while (rs.next()) {
								Pair<String, Integer> bcAvailability = Pair.of(rs.getString("booking_code"),
										rs.getInt("available_seats"));
								if (availableBcList == null) {
									availableBcList = new ArrayList<Pair<String, Integer>>();
								}
								availableBcList.add(bcAvailability);
							}
							return availableBcList;
						}
					});
			bcAvailabilityMap.put(flightSegId, bcList);
		}
		return bcAvailabilityMap;
	}

	private String getQuerySameFareBucket(BucketFilteringCriteria bucketFilteringCriteria, boolean restictBcAvailability)
			throws ModuleException {
		if (bucketFilteringCriteria.getFareId() == null) {
			throw new ModuleException("airinventory.arg.invalid.null");
		}
		StringBuffer stringBuffer = new StringBuffer();
		java.util.Date currentZuluTime = CalendarUtil.getCurrentSystemTimeInZulu();

		boolean isNestedAvailability = false;
		if (!StringUtil.isNullOrEmpty(bucketFilteringCriteria.getBookingClassCode())
				&& !bucketFilteringCriteria.isReturnFareSearch() && AppSysParamsUtil.showNestedAvailableSeats()
				&& BookingClassUtil.isOnlyNormalOrOverbookBcType(bucketFilteringCriteria.getBookingClassTypes())) {
			isNestedAvailability = true;
		}

		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			stringBuffer.append(" SELECT frc.RULES_COMMENTS as comment_in_selected, avail_result.* ");
			stringBuffer.append(" FROM t_fare_rule_comment frc, ( ");
		}

		stringBuffer.append("SELECT distinct ");

		String preferredLanguage = bucketFilteringCriteria.getPreferredLanguage() != null
				&& !bucketFilteringCriteria.getPreferredLanguage().isEmpty()
						? bucketFilteringCriteria.getPreferredLanguage()
						: Locale.ENGLISH.getLanguage();

		if (isNestedAvailability) {
			stringBuffer.append("	decode(bc.booking_code,'" + bucketFilteringCriteria.getBookingClassCode() + "',0,99), ");
		}

		stringBuffer.append("fccsba.fccsba_id as fccsba_id, fccsba.flight_id as flight_id,");
		stringBuffer.append(" fccsba.segment_code as segment_code, fccsba.allocated_seats as allocated_seats, ");
		stringBuffer.append(" fccsba.available_seats as available_seats, fccsba.sold_seats as sold_seats, ");
		stringBuffer.append(" fccsba.onhold_seats as onhold_seats, fccsba.status as status, lgcc.cabin_class_code ");
		stringBuffer.append(
				" as cabin_class_code, f.fare_id as fare_id, f.fare_amount as fare_amount, f.currency_code as currency_code, f.ond_code,");
		stringBuffer.append(" fr.fare_rule_code,fr.fare_basis_code, fr.rules_comments, fr.agent_instructions, ");
		stringBuffer.append(" fr.fare_cat_id, fr.fare_rule_id, fr.minimum_stay_over_mins, ");
		stringBuffer.append(" fr.maximum_stay_over_mins,fr.openrt_conf_period_mins, fr.minimum_stay_over_months, ");
		stringBuffer.append(" fr.maximum_stay_over_months,fr.openrt_conf_stay_over_months, fr.halfrt_flag, ");
		stringBuffer.append(" fr.fare_discount_max, fr.agent_commission_type, fr.agent_commission_amount, ");
		stringBuffer.append(" f.booking_code as booking_code, f.child_fare_type, f.child_fare,");
		stringBuffer.append(" f.infant_fare_type, f.infant_fare,bc.standard_code as standard_flag, bc.nest_rank as nest_rank, ");
		stringBuffer.append(" bc.fixed_flag as fixed_flag, bc.bc_type as bc_type, bc.onhold_flag,fr.fare_discount_min,");
		stringBuffer.append(" lgcc.logical_cabin_class_code as logical_cabin_class, lgcc.LOGICAL_CC_RANK,");
		stringBuffer.append(" F_GET_BC_CHARGE_GROUP(f.booking_code) AS RATIO_CAT ");

		if (bucketFilteringCriteria.isFlexiQuote()) {
			long timeGapToFlightDeparture = bucketFilteringCriteria.getDepartureDateTimeZulu().getTime()
					- currentZuluTime.getTime();
			stringBuffer.append(",(select unique frfr.flexi_rule_id from t_flexi_rule_rate flxrate, t_fare_rule_flexi_rule");
			stringBuffer.append(" frfr , t_flexi_rule_flexibility frf WHERE fr.fare_rule_id = frfr.fare_rule_id ");
			stringBuffer.append("AND flxrate.flexi_rule_id = frfr.flexi_rule_id ");
			stringBuffer.append("AND to_date(" + CalendarUtil.formatForSQL_toDate(currentZuluTime));
			stringBuffer.append(") BETWEEN flxrate.sales_date_from AND flxrate.sales_date_to AND ");
			stringBuffer.append("to_date(" + CalendarUtil.formatForSQL_toDate(bucketFilteringCriteria.getDepatureDate()));
			stringBuffer.append(") BETWEEN flxrate.dep_date_from AND flxrate.dep_date_to AND frf.flexi_rule_id ");
			stringBuffer.append("= frfr.flexi_rule_id AND frf.cut_over_mins * 60000 < ");
			stringBuffer.append(timeGapToFlightDeparture + ") as flexi_rule_id ");
		} else {
			stringBuffer.append(", 0 as flexi_rule_id ");
		}
		if (bucketFilteringCriteria.getNoOfAdults() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='AD' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as AD_APPLICABILITY");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='AD' AND FRP.fare_rule_id(+) = fr.fare_rule_id) AD_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.getNoOfChilds() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='CH' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as CH_APPLICABILITY");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='CH' AND FRP.fare_rule_id(+) = fr.fare_rule_id) as CH_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.getNoOfInfants() > 0) {
			stringBuffer.append(", (SELECT FRP.APPLY_FARE FROM T_FARE_RULE_PAX FRP WHERE FRP.PAX_TYPE_CODE='IN' ");
			stringBuffer.append(" AND FRP.fare_rule_id = fr.fare_rule_id) as IN_APPLICABILITY ");
			stringBuffer.append(", (SELECT FRP.REFUNDABLE_FLAG FROM T_FARE_RULE_PAX FRP WHERE ");
			stringBuffer.append(" FRP.PAX_TYPE_CODE='IN' AND FRP.fare_rule_id(+) = fr.fare_rule_id) IN_REFUND_APPLICABILITY ");
		}
		if (bucketFilteringCriteria.isRetrieveChannelAndAgentInfo()) {
			stringBuffer.append(" , fv.sales_channel_code as sales_channel_code ");
		}
		stringBuffer.append(",(SELECT fccsa.available_seats - NVL(SUM(fix_fsba.allocated_seats - FIX_FSBA.SOLD_SEATS -");
		stringBuffer.append(" FIX_FSBA.ONHOLD_SEATS),0) FROM t_fcc_seg_bc_alloc");
		stringBuffer.append(" fix_fsba, t_booking_class fix_bc WHERE fix_fsba.booking_code = fix_bc.booking_code AND ");
		stringBuffer.append("fix_fsba.fccsa_id = fccsa.fccsa_id AND fix_bc.fixed_flag = 'Y' AND fix_bc.bc_type NOT IN ");
		stringBuffer.append(" ('STANDBY','OPENRT') AND fix_fsba.status like 'OPN') AS available_non_fixed_seats, ");

		// nested available seat count
		stringBuffer.append("(CASE WHEN bc.standard_code='Y' AND fccsba.status = 'OPN' AND ");
		stringBuffer.append("bc.nest_rank > 0  THEN (SELECT SUM(available_seats) FROM t_fcc_seg_bc_alloc a,t_booking_class b ");
		stringBuffer.append("where fccsa_id = fccsa.fccsa_id and b.booking_code=a.booking_code ");
		stringBuffer.append("AND a.status = 'OPN' AND b.nest_rank  <= bc.nest_rank) ");
		stringBuffer.append("ELSE fccsba.available_seats  END) AS nested_available_seats,");

		stringBuffer.append("(SELECT nvl(sum(sold_seats),0) FROM t_fcc_seg_bc_nesting WHERE from_fccsba_id=fccsba.fccsba_id) ");
		stringBuffer.append(" AS nested_sold_seats, (SELECT nvl(sum(onhold_seats),0) FROM t_fcc_seg_bc_nesting WHERE ");
		stringBuffer.append("from_fccsba_id=fccsba.fccsba_id) AS nested_onhold_seats, (SELECT nvl(sum(sold_seats),0) FROM ");
		stringBuffer.append(" t_fcc_seg_bc_nesting WHERE to_fccsba_id=fccsba.fccsba_id) AS nested_aquired_sold_seats,");
		stringBuffer.append("(SELECT nvl(sum(onhold_seats),0) FROM t_fcc_seg_bc_nesting WHERE to_fccsba_id=fccsba.fccsba_id) ");
		stringBuffer.append(" AS nested_acquired_onhold_seats,flt.operation_type_id AS operation_type_id ");

		stringBuffer.append(" FROM t_booking_class bc,t_ond_fare f,t_fare_rule fr,t_fare_visibility fv,t_fcc_seg_alloc fccsa,");
		stringBuffer.append(" t_fare_agent fa, t_fare_rule_pax frp, t_fcc_seg_bc_alloc fccsba, t_logical_cabin_class lgcc, ");
		stringBuffer.append(" t_flight flt WHERE bc.booking_code = f.booking_code AND fr.fare_rule_id = f.fare_rule_id ");
		stringBuffer.append(" AND fr.fare_rule_id = fv.fare_rule_id AND fr.fare_rule_id = frp.fare_rule_id ");
		stringBuffer.append(" AND fr.fare_rule_id = fa.fare_rule_id (+) AND lgcc.logical_cabin_class_code = ");
		stringBuffer.append(" fccsba.logical_cabin_class_code AND bc.booking_code = fccsba.booking_code AND ");
		stringBuffer.append(" fccsa.fccsa_id  = fccsba.fccsa_id AND fccsba.flight_id= " + bucketFilteringCriteria.getFlightId());
		stringBuffer.append(" AND fccsba.segment_code = '" + bucketFilteringCriteria.getSegmentCode() + "' ");
		stringBuffer.append(" AND fccsba.flight_id = flt.flight_id ");
		stringBuffer
				.append(" AND (fr.bulk_ticket_flag = 'N' OR (fr.bulk_ticket_flag = 'Y' AND fr.min_bulk_ticket_seat_count <= ");
		stringBuffer.append(bucketFilteringCriteria.getNoOfActualRequiredSeats() + ")) ");
		if (bucketFilteringCriteria.getRequestedLogicalCabinClassCode() != null
				&& !bucketFilteringCriteria.getRequestedLogicalCabinClassCode().equals("")) {
			stringBuffer.append(
					"AND lgcc.logical_cabin_class_code = '" + bucketFilteringCriteria.getRequestedLogicalCabinClassCode() + "' ");
		} else if (bucketFilteringCriteria.getCabinClassCode() != null) {
			stringBuffer.append(" AND lgcc.cabin_class_code = '" + bucketFilteringCriteria.getCabinClassCode() + "' ");
		}

		// when same fare is promised to serve no need to check the sales channel,make no sense
		// if ((!bucketFilteringCriteria.isFlownOnd() && bucketFilteringCriteria.getChannelCode() != -1)
		// || bucketFilteringCriteria.getChannelCode() == SalesChannelsUtil.SALES_CHANNEL_WEB) {
		// stringBuffer.append(" AND (fv.sales_channel_code = "
		// + SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY)
		// + " OR fv.sales_channel_code = " + bucketFilteringCriteria.getChannelCode() + ") ");
		// }

		// if (bucketFilteringCriteria.getBookingClassCode() != null) {
		//
		// if (isNestedAvailability) {
		// stringBuffer.append(" AND (bc.booking_code LIKE '" + bucketFilteringCriteria.getBookingClassCode() +
		// "' OR ");
		// stringBuffer
		// .append(" (bc.nest_rank > 0 AND bc.nest_rank <= (select ba.nest_rank from t_booking_class
		// ba,t_fcc_seg_bc_alloc ac ");
		// stringBuffer.append("where ba.booking_code='" + bucketFilteringCriteria.getBookingClassCode()
		// + "' and ba.standard_code='Y' and ba.booking_code=ac.booking_code and ac.flight_id = "
		// + bucketFilteringCriteria.getFlightId() + " and ac.available_seats < "
		// + bucketFilteringCriteria.getNoOfActualRequiredSeats() + ")))");
		//
		// } else {
		// stringBuffer.append(" AND (bc.booking_code like '" + bucketFilteringCriteria.getBookingClassCode() + "' ) ");
		// }
		//
		// }

		if (bucketFilteringCriteria.isOpenReturn() || bucketFilteringCriteria.isConfirmOpenReturn()) {
			stringBuffer.append(" AND fr.is_open_return = 'Y' ");
		}

		if (bucketFilteringCriteria.getBookingClassTypes() != null && bucketFilteringCriteria.getBookingClassTypes().size() > 0) {
			stringBuffer.append(" AND ( ");
			int index = 0;
			for (String bookingClass : bucketFilteringCriteria.getBookingClassTypes()) {
				if (BookingClass.BookingClassType.OVERBOOK.equals(bookingClass)
						|| ReservationInternalConstants.ReservationType.WL.getDescription().equals(bookingClass)) {
					bookingClass = BookingClass.BookingClassType.NORMAL;
				}
				if (index == 0) {
					stringBuffer.append(" bc.bc_type like '" + bookingClass + "' ");
				} else {
					stringBuffer.append(" OR bc.bc_type like '" + bookingClass + "' ");
				}
				index++;
			}
			stringBuffer.append(" ) ");
		}

		if (bucketFilteringCriteria.getFareId() != null) {
			stringBuffer.append(" AND f.fare_id= " + bucketFilteringCriteria.getFareId());
		}

		stringBuffer.append(" ORDER BY ");

		if (isNestedAvailability) {
			stringBuffer.append("	decode(bc.booking_code,'" + bucketFilteringCriteria.getBookingClassCode() + "',0,99), ");
		}

		stringBuffer.append("lgcc.LOGICAL_CC_RANK, bc.standard_code desc, bc.nest_rank asc,");
		stringBuffer.append(" bc.fixed_flag asc, f.fare_amount asc");
		if (AppSysParamsUtil.isEnableFareRuleComments()) {
			stringBuffer.append(" ) avail_result ");
			stringBuffer.append(" WHERE frc.fare_rule_id(+) = avail_result.fare_rule_id ");
			stringBuffer.append(" AND frc.language_code(+)  = '" + preferredLanguage + "' ");
		}
		if (log.isDebugEnabled()) {
			log.debug("availability search query for :" + bucketFilteringCriteria.getOndCode() + " and agent "
					+ bucketFilteringCriteria.getAgentCode() + "is " + stringBuffer.toString());
		}
		return stringBuffer.toString();
	}

	@Override
	public boolean isValidConnectionRoute(String segmentCode) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		StringBuffer stringBuffer = new StringBuffer();

		stringBuffer.append("SELECT COUNT(*) AS FARE_COUNT FROM T_OND_FARE ");
		stringBuffer.append("WHERE OND_CODE='" + segmentCode + "' AND STATUS ='ACT' ");
		stringBuffer.append("AND TO_DATE(" + CalendarUtil.formatForSQL_toDate(CalendarUtil.getCurrentSystemTimeInZulu())
				+ ") BETWEEN SALES_VALID_FROM AND SALES_VALID_TO ");

		Integer fareCount = (Integer) jdbcTemplate.query(stringBuffer.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer count = 0;
				if (rs != null) {
					while (rs.next()) {
						count = rs.getInt("FARE_COUNT");
					}
				}
				return count;
			}
		});

		if (fareCount != null && fareCount > 0) {
			return true;
		}

		return false;
	}

	@Override
	public LightBCAvailability getSegmentBCAvailability(String bookingCode, boolean standardBC, Integer flightSegId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String query = "SELECT fccsba.BOOKING_CODE, fccsba.ALLOCATED_SEATS, fccsba.AVAILABLE_SEATS, "
				+ "fccsba.SOLD_SEATS,  fccsba.STATUS ";
		if (standardBC) {
			query += " ,   (SELECT SUM(NVL(b.available_seats,0))"
					+ "   FROM    T_FCC_SEG_BC_ALLOC b,     T_BOOKING_CLASS c   WHERE  b.FCCSA_ID = fccsba.FCCSA_ID   AND  "
					+ " b.BOOKING_CODE = c.BOOKING_CODE    AND c.NEST_RANK    <= bc.NEST_RANK  "
					+ " and (bc.ALLOCATION_TYPE = c.ALLOCATION_TYPE)   ) AS nest_availability ";
		} else {
			query += ", fccsba.AVAILABLE_SEATS as nest_availability ";
		}
		query += "  FROM T_FCC_SEG_ALLOC fccsa,   T_FCC_SEG_BC_ALLOC fccsba ,   "
				+ " T_BOOKING_CLASS bc WHERE fccsa.FCCSA_ID    = fccsba.FCCSA_ID AND fccsba.BOOKING_CODE = bc.BOOKING_CODE"
				+ "  AND fccsa.FLT_SEG_ID    = ? AND fccsba.STATUS       = 'OPN' and fccsba.BOOKING_CODE = ?  ";
		LightBCAvailability lightBcAvail = (LightBCAvailability) jdbcTemplate.query(query, new Object[] { flightSegId,
				bookingCode }, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				LightBCAvailability lightBcAvail = null;
				if (rs.next()) {
					lightBcAvail = new LightBCAvailability(rs.getString("booking_code"), rs.getInt("available_seats"), rs
							.getInt("nest_availability"));
				}
				return lightBcAvail;
			}

		});
		return lightBcAvail;
	}
}
