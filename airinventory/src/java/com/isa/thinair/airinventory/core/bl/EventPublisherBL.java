package com.isa.thinair.airinventory.core.bl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airmaster.api.model.Event;
import com.isa.thinair.airmaster.api.model.EventProfile;
import com.isa.thinair.airmaster.api.util.EventConstants;
import com.isa.thinair.airschedules.api.dto.BasicFlightDTO;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * 
 * 1. Convert Business objects to Event Friendly objects
 * 
 * 2 . Publish events
 *
 */
public class EventPublisherBL {

	public static void publishFlights(Collection<BasicFlightDTO> publishableFlights) {
		for (BasicFlightDTO publishableFlight : publishableFlights) {
			EventProfile eventProfile = new EventProfile();
			eventProfile.setTopicName(EventConstants.TOPIC_NAME);

			Event event = new Event();
			event.setEventCategory(EventConstants.EventCategory.INVENTORY);
			event.setEventName(EventConstants.EventName.INVENTORY_MOVEMENT);

			Map<String, String> eventData = composeEventData(publishableFlight);
			event.setEventData(eventData);
			eventProfile.setEvent(event);

			AirInventoryModuleUtils.getEventServiceBD().publishEvent(eventProfile);
		}

	}

	private static Map<String, String> composeEventData(BasicFlightDTO basicFlightDTO) {
		Map<String, String> eventData = new HashMap<String, String>();

		eventData.put(EventConstants.EventData.FLIGHT_NUMBER, basicFlightDTO.getFlightNumber());
		eventData.put(EventConstants.EventData.ORIGIN, basicFlightDTO.getOrigin());
		eventData.put(EventConstants.EventData.DESTINATION, basicFlightDTO.getDestination());

		String departureDate = CalendarUtil.formatDate(basicFlightDTO.getLocalDepartureDate(), CalendarUtil.PATTERN14);
		eventData.put(EventConstants.EventData.LOCAL_FLIGHT_DATE, departureDate);

		return eventData;
	}

}
