package com.isa.thinair.airinventory.core.bl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollforwardStatusDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaSearchTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQ;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.api.service.RollForwardBatchREQBD;
import com.isa.thinair.airinventory.api.service.RollForwardBatchREQSEGBD;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.RollForwardThread;
import com.isa.thinair.airinventory.api.util.RollForwardThreadPoolExecutor;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.BatchStatus;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Class to implement the business logic related to the RollForwardBatch
 * 
 * @author Raghu
 */

public class RollForwardBatchBL {

	private final static Log log = LogFactory.getLog(RollForwardBatchBL.class);

	private final RollForwardBatchREQBD rollForwardBatchREQBD = AirinventoryUtils.getRollForwardBatchREQBD();

	private final static RollForwardBatchREQSEGBD rollForwardBatchREQSEGBD = AirinventoryUtils.getRollForwardBatchREQSEGBD();

	private static ExecutorService taskExecutor = new RollForwardThreadPoolExecutor();

	public RollForwardBatchREQ getRollForwardBatchREQ(Integer batchId) throws ModuleException {
		return rollForwardBatchREQBD.getRollForwardBatchREQ(batchId);
	}

	public Collection<RollForwardBatchREQSEG> save(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException {
		return rollForwardBatchREQBD.saveRollForwardBatchREQ(rollForwardBatchREQ);
	}

	public void updateRollForwardBatchREQ(RollForwardBatchREQ rollForwardBatchREQ) throws ModuleException {
		rollForwardBatchREQBD.updateRollForwardBatchREQ(rollForwardBatchREQ);
	}

	public void saveAllRollForwardBatchREQSEG(Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet) throws ModuleException {
		rollForwardBatchREQSEGBD.saveAllRollForwardBatchREQSEG(rollForwardBatchREQSEGSet);

	}

	public void deleteRollForwardBatchREQSEG(Integer id) throws ModuleException {
		rollForwardBatchREQSEGBD.removeRollForwardBatchREQSEG(id);

	}

	public RollForwardBatchREQSEG getRollForwardBatchREQSEG(Integer id) throws ModuleException {
		return rollForwardBatchREQSEGBD.getRollForwardBatchREQSEG(id);

	}

	public Page<SearchRollForwardCriteriaTO> searchRollForwardBatchREQSEQ(
			SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO, int start, int size) {

		return rollForwardBatchREQSEGBD.search(searchRollForwardCriteriaSearchTO, start, size);
	}

	public void updateRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException {
		rollForwardBatchREQSEGBD.updateRollForwardBatchREQSEG(rollForwardBatchREQSEG);

	}

	public void updateRollForwardBatchREQStatus(Integer batchId, String status) throws ModuleException {
		rollForwardBatchREQBD.updateRollForwardBatchREQStatus(batchId, status);

	}

	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByBatchId(Integer batchId) throws ModuleException {
		return rollForwardBatchREQSEGBD.getRollForwardBatchREQSEGByBatchId(batchId);
	}

	/**
	 * This Method will save the Batches
	 * 
	 * @param searchCriteriaDTOList
	 * @return
	 * @throws ModuleException
	 */
	private Collection<RollForwardBatchREQSEG> saveBatchData(List<InvRollForwardCriteriaDTO> searchCriteriaDTOList) throws ModuleException {

		XStream xstream = new XStream(new DomDriver());


		RollForwardBatchREQ rollForwardBatchREQ = new RollForwardBatchREQ();

		rollForwardBatchREQ.setStatus(BatchStatus.WAITING);

		Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet = new HashSet<RollForwardBatchREQSEG>();

		for (InvRollForwardCriteriaDTO criteriaDTO : searchCriteriaDTOList) {

			RollForwardBatchREQSEG rollForwardBatchREQSEG = new RollForwardBatchREQSEG();
			rollForwardBatchREQSEG.setFlightNumber(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getFlightNumber());
			rollForwardBatchREQSEG.setFromDate(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getFromDate());
			rollForwardBatchREQSEG.setToDate(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getToDate());
			rollForwardBatchREQSEG.setSegmentBatchData(xstream.toXML(criteriaDTO));
			rollForwardBatchREQSEG.setRollForwardBatchREQ(rollForwardBatchREQ);
			rollForwardBatchREQSEG.setStatus(BatchStatus.WAITING);
			rollForwardBatchREQSEGSet.add(rollForwardBatchREQSEG);

		}

		rollForwardBatchREQ.setRollForwardBatchREQSEGSet(rollForwardBatchREQSEGSet);

		Collection<RollForwardBatchREQSEG> rollForwardBatchREQSEGList = save(rollForwardBatchREQ);

		return rollForwardBatchREQSEGList;

	}

	/**
	 * This Method has the business logic for Basic Roll Forward
	 * 
	 * @param criteriaDTO
	 * @return
	 * @throws ParseException
	 * @throws ModuleException
	 */
	public static InvRollforwardStatusDTO doRollForwardLogic(InvRollForwardCriteriaDTO criteriaDTO) throws
			ModuleException {
		
		// preparing the flight IDs
		Set<Integer> flightIds = new HashSet<Integer>();
		Collection<FlightSummaryDTO> collectionFligthSummary = AirInventoryModuleUtils.getFlightBD().getLikeFlightSummary(
				criteriaDTO.getInvRollForwardFlightsSearchCriteria());
		if (collectionFligthSummary != null && collectionFligthSummary.size() > 0) {
			FlightSummaryDTO flightSummaryDTO = null;
			Iterator<FlightSummaryDTO> iter = collectionFligthSummary.iterator();

			while (iter.hasNext()) {
				flightSummaryDTO = iter.next();
				int flightID = flightSummaryDTO.getFlightId();
				flightIds.add(flightID);
			}
		}

		InvRollforwardStatusDTO rollForwardStatus = new InvRollforwardStatusDTO(-1, "", null);

		// If no matching flights found, throw the exception. No need to call the BD method if no
		// matching flights found
		if (flightIds.size() > 0) {

			log.info("saveData() About to Rollforward inv, Source Flight Id is : " + criteriaDTO.getSourceFlightId()
					+ " And Source Cabin class is : " + criteriaDTO.getSourceCabinClassCode() + ". Target Flight Ids are : "
					+ flightIds.toString());

			if (log.isDebugEnabled()) {
				log.debug("Inventory Roll forward Criteria is  : - " + criteriaDTO.toString());
			}

			rollForwardStatus = AirInventoryModuleUtils.getFlightInventoryBD().rollFlightInventory(flightIds, criteriaDTO);

			// updating flight status
			if (rollForwardStatus.getFlightIdsForStatusUpdating().size() > 0) {
				Collection<Integer> chunkedLists = new ArrayList<Integer>();
				Iterator<Integer> flightIdsIt = rollForwardStatus.getFlightIdsForStatusUpdating().iterator();
				for (int i = 0; flightIdsIt.hasNext(); ++i) {
					chunkedLists.add(flightIdsIt.next());
					if (((i != 0) && (i % 30 == 0)) || (i == (rollForwardStatus.getFlightIdsForStatusUpdating().size() - 1))) {
						AirInventoryModuleUtils.getFlightBD().changeFlightStatus(chunkedLists, FlightStatusEnum.ACTIVE);
						chunkedLists.clear();
					}
				}
			}

			if (rollForwardStatus.getFltInventoryErrorMap().size() > 0) {
				Set<String> flightIdsWithCC = rollForwardStatus.getFltInventoryErrorMap().keySet();
				// This list is used to query the db. We can't use keyset here.
				List<Integer> tmpAuditFlightIds = new ArrayList<Integer>();
				Map<Integer, String> flightIdsAndCC = new HashMap<Integer, String>();
				for (String flightIdAndCC : flightIdsWithCC) {
					String[] idAndCC = flightIdAndCC.split("\\|");
					int auditFlightId = Integer.valueOf(idAndCC[0]);
					flightIdsAndCC.put(auditFlightId, idAndCC[1]);
					tmpAuditFlightIds.add(auditFlightId);
				}
				String rollForwardAudit = "";
				List<Flight> flights = AirInventoryModuleUtils.getFlightBD().getLightWeightFlightDetails(tmpAuditFlightIds);
				int i = 0;
				for (Flight flight : flights) {
					if (i != 0) {
						rollForwardAudit += "@";
					}
					SimpleDateFormat sdt = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					Date arrivalDateZulu = flight.getDepartureDate();
					Iterator<FlightSegement> segmentIterator = flight.getFlightSegements().iterator();
					if (segmentIterator.hasNext()) {
						arrivalDateZulu = segmentIterator.next().getEstTimeArrivalZulu();
					}
					String flightIdAndCC = flight.getFlightId() + "|" + flightIdsAndCC.get(flight.getFlightId());
					rollForwardAudit += flight.getFlightId() + "|" + flight.getFlightNumber() + "|"
							+ sdt.format(flight.getDepartureDate()) + "|" + flight.getOriginAptCode() + "|"
							+ rollForwardStatus.getFltInventoryErrorMap().get(flightIdAndCC) + "|" + flight.getModelNumber()
							+ "|" + flightIdsAndCC.get(flight.getFlightId()) + "|" + sdt.format(arrivalDateZulu);
					i++;
				}
				rollForwardStatus.setRollForwardAudit(rollForwardAudit);
				log.info("saveData() Finish Rollforwarding inv, Source Flight Id is : " + criteriaDTO.getSourceFlightId()
						+ " And Source Cabin class is : " + criteriaDTO.getSourceCabinClassCode());
			}else{
				if(rollForwardStatus.getStrLog() == null){
					rollForwardStatus = new InvRollforwardStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "Inventory rollforwarding failed", null);
					StringBuilder strLog = new StringBuilder();
					strLog.append("No Flights scheduled for the below");
					strLog.append("<br> From Date : "+CalendarUtil.formatDate(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getFromDate(), CalendarUtil.PATTERN1));
					strLog.append("<br> To Date : "+CalendarUtil.formatDate(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getToDate(), CalendarUtil.PATTERN1));
					strLog.append("<br> Flight code : "+criteriaDTO.getInvRollForwardFlightsSearchCriteria().getFlightNumber());
					strLog.append("<br> Source Flight Id : " + criteriaDTO.getSourceFlightId());
					strLog.append("<br> Source Cabin class : " + criteriaDTO.getSourceCabinClassCode());
					strLog.append("<br> Segments : " +criteriaDTO.getCreateSegAllocs());
					rollForwardStatus.setStrLog(strLog);
				}
			}

		} else {
			log.info("saveData() Finish Rollforwarding inv, Source Flight Id is : " + criteriaDTO.getSourceFlightId()
					+ " And Source Cabin class is : " + criteriaDTO.getSourceCabinClassCode());
			rollForwardStatus = new InvRollforwardStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "Inventory rollforwarding failed", null);
			StringBuilder strLog = new StringBuilder();
			strLog.append("No Flights scheduled for the below");
			strLog.append("<br> From Date : "+CalendarUtil.formatDate(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getFromDate(), CalendarUtil.PATTERN1));
			strLog.append("<br> To Date : "+CalendarUtil.formatDate(criteriaDTO.getInvRollForwardFlightsSearchCriteria().getToDate(), CalendarUtil.PATTERN1));
			strLog.append("<br> Flight code : "+criteriaDTO.getInvRollForwardFlightsSearchCriteria().getFlightNumber());
			strLog.append("<br> Source Flight Id : " + criteriaDTO.getSourceFlightId());
			strLog.append("<br> Source Cabin class : " + criteriaDTO.getSourceCabinClassCode());
			strLog.append("<br> Segments : " +criteriaDTO.getCreateSegAllocs());
			rollForwardStatus.setStrLog(strLog);
		}
		return rollForwardStatus;
	}

	/**
	 * 
	 * This Method has the business logic for Advance Roll Forward
	 * 
	 * @param criteriaDTOList
	 * @return
	 * @throws ModuleException
	 */
	public Integer doAdvanceRollForward(List<InvRollForwardCriteriaDTO> criteriaDTOList) throws ModuleException {

		List<RollForwardBatchREQSEG> rollForwardBatchREQSEGList = (ArrayList)saveBatchData(criteriaDTOList);
		
		rollForwardBatchREQSEGList.forEach(rollForwardBatchREQSEG -> {
			InvRollForwardCriteriaDTO invRollForwardCriteriaDTO = (InvRollForwardCriteriaDTO) XMLStreamer
					.decompose(rollForwardBatchREQSEG.getSegmentBatchData());
			
			Runnable worker = new RollForwardThread(rollForwardBatchREQSEG, invRollForwardCriteriaDTO);
			taskExecutor.execute(worker);
		});
		
		Integer batchId = rollForwardBatchREQSEGList.get(0).getRollForwardBatchREQ().getBatchId();

		return batchId;
	}

	/**
	 * 
	 * This Method will have single transaction logic for roll forwarding a batch sequence
	 * 
	 * @param invRollForwardCriteriaDTO
	 * @param rollForwardBatchREQSEG
	 * @throws ModuleException
	 * @throws ParseException
	 */
	public static void rollForwardBatchSegment(InvRollForwardCriteriaDTO invRollForwardCriteriaDTO,
			RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException {

		// Performing Roll Forward operation
		InvRollforwardStatusDTO rollforwardStatus = doRollForwardLogic(invRollForwardCriteriaDTO);

		// Updating Segment status to DONE
		rollForwardBatchREQSEGBD.updateRollForwardBatchREQSEGStatus(rollForwardBatchREQSEG.getBatchSegId(), 
				rollForwardBatchREQSEG.getFlightNumber(), rollForwardBatchREQSEG.getFromDate(), 
				rollForwardBatchREQSEG.getToDate(),	BatchStatus.DONE, rollforwardStatus.getStrLog().toString());
		

		
	}
}
