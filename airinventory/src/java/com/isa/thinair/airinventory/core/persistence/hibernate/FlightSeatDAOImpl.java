package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightSeatDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @isa.module.dao-impl dao-name="FlightSeatDAOImpl"
 */
public class FlightSeatDAOImpl extends PlatformHibernateDaoSupport implements FlightSeatDAO {

	public Collection<FlightSeat> getFlightSeats(Collection<Integer> flightAmseatIds) {
		return find(
				"from FlightSeat fv where " + Util.getReplaceStringForIN("fv.flightAmSeatId", flightAmseatIds), FlightSeat.class);
	}

	@Override
	public Collection<FlightSeat> getFlightSeatsWithFccInv(Collection<Integer> fccInv, Collection<Integer> amSeats) {
		return find(
				"from FlightSeat fv where " + Util.getReplaceStringForIN("fv.flightSegInventoryId", fccInv) + " and  "
						+ Util.getReplaceStringForIN("fv.seatId", amSeats), FlightSeat.class);
	}

	public boolean isStatusEqualInInterceptingFccSegIvs(Collection<Integer> interCeptingfccSegIvs, String status) {
		String hql = "Select count(*) from FlightSeat fv where"
				+ Util.getReplaceStringForIN("fv.flightSegInventoryId", interCeptingfccSegIvs) + " and (fv.status=:status "
				+ " or fv.status='" + AirinventoryCustomConstants.FlightSeatStatuses.INACTIVE + "'" + " or fv.status='"
				+ AirinventoryCustomConstants.FlightSeatStatuses.ACQUIRED + "')";
		Query countQuery = getSession().createQuery(hql);
		countQuery.setParameter("status", status);
		int count = ((Long) countQuery.uniqueResult()).intValue();
		if (count >= 1) {
			return true;
		}
		return false;

	}

	public void saveFlightSeats(Collection<FlightSeat> fs) {
		hibernateSaveOrUpdateAll(fs);
	}

	public Integer getFlightSeatID(Integer flightSegmentId, String seatCode) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

		String sql = "SELECT AM_SEAT_ID FROM SM_T_FLIGHT_AM_SEAT " + "WHERE FLT_SEG_ID = " + flightSegmentId
				+ " AND AM_SEAT_ID IN (" + "SELECT AM_SEAT_ID FROM SM_T_AIRCRAFT_MODEL_SEATS " + "WHERE SEAT_CODE = '" + seatCode
				+ "')";

		Integer amSeatId = jdbcTemplate.query(sql, new ResultSetExtractor<Integer>() {

			public Integer extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				while (resultSet.next()) {
					return new Integer(resultSet.getInt("AM_SEAT_ID"));
				}
				return null;
			}
		});

		return amSeatId;
	}

	public Integer getFlightAmSeatID(Integer flightSegmentId, String seatCode) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

		String sql = "SELECT FLIGHT_AM_SEAT_ID FROM SM_T_FLIGHT_AM_SEAT " + "WHERE FLT_SEG_ID = " + flightSegmentId
				+ " AND AM_SEAT_ID IN (" + "SELECT AM_SEAT_ID FROM SM_T_AIRCRAFT_MODEL_SEATS " + "WHERE SEAT_CODE = '" + seatCode
				+ "')";

		Integer flightAmSeatId = jdbcTemplate.query(sql, new ResultSetExtractor<Integer>() {

			public Integer extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				while (resultSet.next()) {
					return new Integer(resultSet.getInt("FLIGHT_AM_SEAT_ID"));
				}
				return null;
			}
		});

		return flightAmSeatId;
	}

	@Override
	public void deleteFlightSeats(Collection<Integer> flightSegIds) {
		String query = "delete FlightSeat where " + Util.getReplaceStringForIN("flightSegId", flightSegIds);
		getSession().createQuery(query).executeUpdate();
	}

	public void deleteReservationSeats(Collection<Integer> flightAMSeatIds) {
		try {

			String sql = "delete sm_t_pnr_pax_seg_am_seat where status='RES' and flight_am_seat_id in ("
					+ Util.buildIntegerSQLInClauseContent(flightAMSeatIds) + ")";

			JdbcTemplate jdbcTemplate = new JdbcTemplate();
			jdbcTemplate.setDataSource(AirinventoryUtils.getDatasource());

			jdbcTemplate.update(sql);
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

}
