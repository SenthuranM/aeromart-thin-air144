package com.isa.thinair.airinventory.core.bl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareONDChargeInfo;
import com.isa.thinair.airinventory.api.dto.seatavailability.PaxTypewiseFareONDChargeInfo;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import org.springframework.util.CollectionUtils;

class CategoryChargeQuoteDTO {

	private Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> ondChargeInfo = new HashMap<Integer, Map<String, PaxTypewiseFareONDChargeInfo>>();

	boolean calculated = false;

	public void calculateTotals() {
		if (!calculated) {
			synchronized (this) {
				if (!calculated) {
					double totalSurchargeValueAdult = 0;
					double totalSurchargeValueChild = 0;
					double totalSurchargeValueInfant = 0;

					for (Map<String, PaxTypewiseFareONDChargeInfo> ondFareInfo : ondChargeInfo.values()) {
						for (PaxTypewiseFareONDChargeInfo fareInfo : ondFareInfo.values()) {
							totalSurchargeValueAdult += fareInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT)
									.getSurchargeValue();
							totalSurchargeValueChild += fareInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD)
									.getSurchargeValue();
							totalSurchargeValueInfant += fareInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT)
									.getSurchargeValue();
						}
					}

					for (Map<String, PaxTypewiseFareONDChargeInfo> ondFareInfo : ondChargeInfo.values()) {
						for (PaxTypewiseFareONDChargeInfo fareInfo : ondFareInfo.values()) {
							fareInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT).setTotalJourneyValueSurcharge(
									totalSurchargeValueAdult);
							fareInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD).setTotalJourneyValueSurcharge(
									totalSurchargeValueChild);
							fareInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT).setTotalJourneyValueSurcharge(
									totalSurchargeValueInfant);
						}
					}

					calculated = true;
				}
			}
		}
	}

	public Map<Integer, Map<String, PaxTypewiseFareONDChargeInfo>> getChargeInfo() {
		return ondChargeInfo;
	}

	public Map<String, Double> getRatios() {
		return null;
	}

	public void addOndCharges(AvailableIBOBFlightSegment ondFlight,
							  HashMap<String, HashMap<String, QuotedChargeDTO>> ondChargeQuote) {
		if (ondChargeQuote != null) {
			PaxTypewiseFareONDChargeInfo chargeInfo = null;

			for (String ondCode : ondChargeQuote.keySet()) {
				HashMap<String, QuotedChargeDTO> ondCharges = ondChargeQuote.get(ondCode);
				if (!ondChargeInfo.containsKey(ondFlight.getOndSequence())) {
					chargeInfo = new PaxTypewiseFareONDChargeInfo();
					ondChargeInfo.put(ondFlight.getOndSequence(), new HashMap<String, PaxTypewiseFareONDChargeInfo>());
					ondChargeInfo.get(ondFlight.getOndSequence()).put(ondCode, chargeInfo);
				} else {
					if (!ondChargeInfo.get(ondFlight.getOndSequence()).containsKey(ondCode)) {
						chargeInfo = new PaxTypewiseFareONDChargeInfo();
						ondChargeInfo.get(ondFlight.getOndSequence()).put(ondCode, chargeInfo);
					} else {
						chargeInfo = ondChargeInfo.get(ondFlight.getOndSequence()).get(ondCode);
					}
				}

				setServiceTaxInfoToChargeONDInfo(chargeInfo, ondFlight);

				for (QuotedChargeDTO charge : ondCharges.values()) {
					boolean applied = false;
					if (ChargeQuoteUtils.isChgApplicableForPaxType(charge, PaxTypeTO.ADULT)) {
						addCharge(charge, chargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT));
						applyServiceTaxes(ondFlight, charge, chargeInfo , PaxTypeTO.ADULT);
						applied = true;
					}

					if (ChargeQuoteUtils.isChgApplicableForPaxType(charge, PaxTypeTO.CHILD)) {
						addCharge(charge, chargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD));
						applyServiceTaxes(ondFlight, charge, chargeInfo , PaxTypeTO.CHILD);
						applied = true;
					}

					if (ChargeQuoteUtils.isChgApplicableForPaxType(charge, PaxTypeTO.INFANT)) {
						addCharge(charge, chargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT));
						applyServiceTaxes(ondFlight, charge, chargeInfo , PaxTypeTO.INFANT);
						applied = true;
					}

					if (!applied) {
						addCharge(charge, chargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT));
						addCharge(charge, chargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD));
						addCharge(charge, chargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT));

						applyServiceTaxes(ondFlight, charge, chargeInfo , PaxTypeTO.ADULT);
						applyServiceTaxes(ondFlight, charge, chargeInfo , PaxTypeTO.CHILD);
						applyServiceTaxes(ondFlight, charge, chargeInfo , PaxTypeTO.INFANT);
					}
				}
			}
		}
	}

	private void setServiceTaxInfoToChargeONDInfo(PaxTypewiseFareONDChargeInfo paxTypewiseFareONDChargeInfo, AvailableIBOBFlightSegment ondFlight) {
		Map<String, QuotedChargeDTO> serviceTaxes = ondFlight.getServiceTaxes();
		if (!CollectionUtils.isEmpty(serviceTaxes)) {
			for (Map.Entry<String, QuotedChargeDTO> serviceTaxEntry : serviceTaxes.entrySet()) {
				paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT).getServiceTaxInfoMap().
						put(serviceTaxEntry.getKey(), new FareONDChargeInfo());
				paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD).getServiceTaxInfoMap().
						put(serviceTaxEntry.getKey(), new FareONDChargeInfo());
				paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT).getServiceTaxInfoMap().
						put(serviceTaxEntry.getKey(), new FareONDChargeInfo());

				if (serviceTaxEntry.getValue().isChargeValueInPercentage()) {
					paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT).getServiceTaxInfoMap().
							get(serviceTaxEntry.getKey()).setPercentServiceTax(serviceTaxEntry.getValue().getChargeValuePercentage());
					paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD).getServiceTaxInfoMap().
							get(serviceTaxEntry.getKey()).setPercentServiceTax(serviceTaxEntry.getValue().getChargeValuePercentage());
					paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT).getServiceTaxInfoMap().
							get(serviceTaxEntry.getKey()).setPercentServiceTax(serviceTaxEntry.getValue().getChargeValuePercentage());

				} else {
					paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.ADULT).getServiceTaxInfoMap().
							get(serviceTaxEntry.getKey()).addTaxValue(serviceTaxEntry.getValue().getChargeValuePercentage());
					paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.CHILD).getServiceTaxInfoMap().
							get(serviceTaxEntry.getKey()).addTaxValue(serviceTaxEntry.getValue().getChargeValuePercentage());
					paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(PaxTypeTO.INFANT).getServiceTaxInfoMap().
							get(serviceTaxEntry.getKey()).addTaxValue(serviceTaxEntry.getValue().getChargeValuePercentage());
				}
			}
		}
	}


	private void applyServiceTaxes(AvailableIBOBFlightSegment ondFlight, QuotedChargeDTO charge, PaxTypewiseFareONDChargeInfo paxTypewiseFareONDChargeInfo,
								   String paxtype) {
		Map<String, QuotedChargeDTO> serviceTaxes = ondFlight.getServiceTaxes();
		FareONDChargeInfo fareONDChargeInfo = paxTypewiseFareONDChargeInfo.getFareONDChargeInfoForPaxType(paxtype);
		if (!CollectionUtils.isEmpty(serviceTaxes)) {
			Map<String, List<String>> serviceTaxExcludedCharges = ondFlight.getServiceTaxExcludedCharges();
			for (Map.Entry<String, QuotedChargeDTO> serviceTaxEntry : serviceTaxes.entrySet()) {
				if (CollectionUtils.isEmpty(serviceTaxExcludedCharges) ||
						CollectionUtils.isEmpty(serviceTaxExcludedCharges.get(serviceTaxEntry.getKey())) ||
						!serviceTaxExcludedCharges.get(serviceTaxEntry.getKey()).contains(charge.getChargeCode())) {
					addServiceTaxCharge(charge, serviceTaxEntry.getValue(), fareONDChargeInfo.getServiceTaxInfoMap().get(serviceTaxEntry.getKey()));
				}
			}

		}
	}

	private void addServiceTaxCharge(QuotedChargeDTO charge, QuotedChargeDTO serviceTaxQuoteDTO, FareONDChargeInfo serviceTaxONDChargeInfo) {
		if (serviceTaxQuoteDTO.isChargeValueInPercentage()) {
			boolean tax = ChargeGroup.TAX.equals(charge.getChargeGroupCode());
			boolean surcharge = ChargeGroup.SUR.equals(charge.getChargeGroupCode());
			double value = serviceTaxQuoteDTO.getChargeValuePercentage();
			if (charge.isChargeValueInPercentage()) {
				if (ChargeRate.CHARGE_BASIS_PF.equals(charge.getChargeBasis())) {
					if (tax) {
						serviceTaxONDChargeInfo.addTaxPF(value);
					} else if (surcharge) {
						serviceTaxONDChargeInfo.addSurchargePF(value);
					}
				} else if (ChargeRate.CHARGE_BASIS_PTF.equals(charge.getChargeBasis())) {
					if (tax) {
						serviceTaxONDChargeInfo.addTaxPTF(value);
					} else if (surcharge) {
						serviceTaxONDChargeInfo.addSurchargePTF(value);
					}
				} else if (ChargeGroup.TAX.equals(charge.getChargeGroupCode())) {
					if (ChargeRate.CHARGE_BASIS_PS.equals(charge.getChargeBasis())) {
						serviceTaxONDChargeInfo.addPS(value);
					} else if (ChargeRate.CHARGE_BASIS_PFS.equals(charge.getChargeBasis())) {
						serviceTaxONDChargeInfo.addPFS(value);
					} else if (ChargeRate.CHARGE_BASIS_PTFS.equals(charge.getChargeBasis())) {
						serviceTaxONDChargeInfo.addPTFS(value);
					}
				}
			} else {
				if (tax) {
					serviceTaxONDChargeInfo.addTaxValue(charge.getChargeValuePercentage() * value / 100.0);
				} else if (surcharge) {
					serviceTaxONDChargeInfo.addSurchargeValue(charge.getChargeValuePercentage() * value / 100.0);
				}
			}
		}
	}

	private void addCharge(QuotedChargeDTO charge, FareONDChargeInfo chargeInfo) {
		boolean tax = ChargeGroup.TAX.equals(charge.getChargeGroupCode());
		boolean surcharge = ChargeGroup.SUR.equals(charge.getChargeGroupCode());
		double value = charge.getChargeValuePercentage();
		if (charge.isChargeValueInPercentage()) {
			if (ChargeRate.CHARGE_BASIS_PF.equals(charge.getChargeBasis())) {
				if (tax) {
					chargeInfo.addTaxPF(value);
				} else if (surcharge) {
					chargeInfo.addSurchargePF(value);
				}
			} else if (ChargeRate.CHARGE_BASIS_PTF.equals(charge.getChargeBasis())) {
				if (tax) {
					chargeInfo.addTaxPTF(value);
				} else if (surcharge) {
					chargeInfo.addSurchargePTF(value);
				}
			} else if (ChargeGroup.TAX.equals(charge.getChargeGroupCode())) {
				if (ChargeRate.CHARGE_BASIS_PS.equals(charge.getChargeBasis())) {
					chargeInfo.addPS(value);
				} else if (ChargeRate.CHARGE_BASIS_PFS.equals(charge.getChargeBasis())) {
					chargeInfo.addPFS(value);
				} else if (ChargeRate.CHARGE_BASIS_PTFS.equals(charge.getChargeBasis())) {
					chargeInfo.addPTFS(value);
				}
			}
		} else {
			if (tax) {
				chargeInfo.addTaxValue(value);
			} else if (surcharge) {
				chargeInfo.addSurchargeValue(value);
			}
		}
	}

}