package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresBCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FCCSegBCInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.SeatAvailabilityDAOJDBC;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * Extracts all buckets with fares in a flight segment
 * 
 * @author Nasly
 * 
 */
public class FilterAllBucketsResultSetExtractor implements ResultSetExtractor {

	private final BucketFilteringCriteria bucketFilteringCriteria;
	
	private static final int AGENTS_THRESHOLD = 900;
	
	public FilterAllBucketsResultSetExtractor(BucketFilteringCriteria bucketFilteringCriteria) {
		this.bucketFilteringCriteria = bucketFilteringCriteria;
	}

	/**
	 * Returns an ordered Map of AllFaresBCInventorySummaryDTO collection keyed by booking code
	 */
	@Override
	public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {

		boolean skipFlexiAvailabilityValidation = false;
		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

		LinkedHashMap<String, AllFaresBCInventorySummaryDTO> bcInvsMap = new LinkedHashMap<String, AllFaresBCInventorySummaryDTO>();
		boolean isExcludePublicFare = false;
		Integer salesChannelCode = null;
		int bcInvAvailableSeats = 0;

		HashMap<Integer, Integer> nestedAvailableSeatCountMap = null;
		HashMap<String, HashMap<Integer, Integer>> nestedAvailableSeatCountByLogCCMap = new HashMap<String, HashMap<Integer, Integer>>();
		HashMap<Integer, Integer> nestedHRTAvailableSeatCountMap = null;
		HashMap<String, HashMap<Integer, Integer>> nestedHRTAvailableSeatCountByLogCCMap = new HashMap<String, HashMap<Integer, Integer>>();
		boolean showNestedAvailability = AppSysParamsUtil.showNestedAvailableSeats();
		Map<String, List<Integer>> availableSeatsMap = getAvailableNonFixedAndFixedSeatsCounts();

		while (resultSet.next()) {
			String bookingCode = resultSet.getString("booking_code");
			String logicalCabinClassCode = BeanUtils.nullHandler(resultSet.getString("logical_cabin_class"));
			int operationType = resultSet.getInt("operation_type_id");
			
			
			if(!bucketFilteringCriteria.getBookingClassTypes().contains(BookingClass.BookingClassType.STANDBY)) {
				List<Integer> availableSeats = availableSeatsMap.get(logicalCabinClassCode);
				boolean fixedFlag = resultSet.getString("fixed_flag").equals(BookingClass.FIXED_FLAG_Y) ? true : false;
				if (availableSeats != null) {
					int availableTotalNonFixedSeats = availableSeats.get(0) - availableSeats.get(1);
					if (!bucketFilteringCriteria.isOverbookEnabled()
							&& !bucketFilteringCriteria.getBookingClassTypes().contains(BookingClass.BookingClassType.OVERBOOK)
							&& !fixedFlag && availableTotalNonFixedSeats < bucketFilteringCriteria.getNoOfActualRequiredSeats()
							&& !bucketFilteringCriteria.isSameBookedFltCabinBeingSearched()) {
						continue;
					}
				}
			}


			if (AppSysParamsUtil.isFlexiFareEnabled() && AirScheduleCustomConstants.OperationTypes.BUS_SERVICE != operationType
					&& !bucketFilteringCriteria.isUnTouchedOnd()) {
				if (logicalCabinClassCode != null && !"".equals(logicalCabinClassCode)) {
					LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(logicalCabinClassCode);
					if (logicalCabinClassDTO.isFreeFlexiEnabled()) {
						skipFlexiAvailabilityValidation = true;
					}
				}

				if (!skipFlexiAvailabilityValidation) {
					boolean hasFlexi = resultSet.getInt("flexi_rule_id") != 0;
					// If fare don't have flexi & show only flexi fare flag enabled, skip fare. This uses in change fare
					if (!hasFlexi && bucketFilteringCriteria.isShowFlexiFareOnly()) {
						continue;
					}
				}
			}

			// For filtering min public fare only, if requested
			Object objSalesChannelCode = resultSet.getObject("sales_channel_code");
			if (objSalesChannelCode != null) {
				salesChannelCode = new Integer(objSalesChannelCode.toString());
				if (isExcludePublicFare
						&& SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY) == salesChannelCode) {
					continue;
				}
			}

			// Exclude buckets not having enough seats when fares are requested by restricted users
			bcInvAvailableSeats = resultSet.getInt("available_seats");
			if (!showNestedAvailability && bucketFilteringCriteria.isCheckAvailability()
					&& bcInvAvailableSeats < bucketFilteringCriteria.getNoOfActualRequiredSeats()) {
				continue;
			}

			AllFaresBCInventorySummaryDTO faresBCInv = bcInvsMap.get(bookingCode);
			if (faresBCInv == null) {
				faresBCInv = new AllFaresBCInventorySummaryDTO();
				faresBCInv.setCcCode(resultSet.getString("cabin_class_code"));
				faresBCInv.setLogicalCabinclass(logicalCabinClassCode);
				bcInvsMap.put(bookingCode, faresBCInv);
				FCCSegBCInvSummaryDTO bcInv = new FCCSegBCInvSummaryDTO();

				faresBCInv.setBcInventorySummaryDTO(bcInv);

				// extract bc inventory information
				bcInv.setBookingCode(resultSet.getString("booking_code"));
				bcInv.setStandardFlag(resultSet.getString("standard_flag").equals(BookingClass.STANDARD_CODE_Y) ? true : false);
				bcInv.setFixedFlag(resultSet.getString("fixed_flag").equals(BookingClass.FIXED_FLAG_Y) ? true : false);

				bcInv.setAllocatedSeats(resultSet.getInt("allocated_seats"));
				bcInv.setSoldSeats(resultSet.getInt("sold_seats"));
				bcInv.setOnholdSeats(resultSet.getInt("onhold_seats"));
				bcInv.setAvailableSeats(resultSet.getInt("available_seats"));
				bcInv.setBookedFlightCabinSearch(bucketFilteringCriteria.isSameBookedFltCabinBeingSearched(faresBCInv.getCcCode()));
				bcInv.setFlownOnd(bucketFilteringCriteria.isFlownOnd());
				bcInv.setSameBookingClassAsExisting(bcInv.getBookingCode().equals(
						bucketFilteringCriteria.getBookedFlightBookingClass()));

				bcInv.setBcInvStatus(resultSet.getString("status"));
				bcInv.setOnholdRestricted(resultSet.getString("onhold_flag"));
				bcInv.setNestedRank(resultSet.getInt("nest_rank"));

				bcInv.setSeatsSoldAquiredByNesting(resultSet.getInt("nested_aquired_sold_seats"));
				bcInv.setSeatsSoldNested(resultSet.getInt("nested_sold_seats"));
				bcInv.setSeatsOnHoldAquiredByNesting(resultSet.getInt("nested_acquired_onhold_seats"));
				bcInv.setSeatsOnHoldNested(resultSet.getInt("nested_onhold_seats"));

			}

			Integer fareId = new Integer(resultSet.getInt("fare_id"));
			FareSummaryDTO fare = faresBCInv.getFareSummaryDTO(fareId);
			if (fare == null) {
				// extract fares
				fare = new FareSummaryDTO();

				fare.setFareId(fareId.intValue());
				fare.setFareRuleID(resultSet.getInt("fare_rule_id"));
				fare.setBookingClassCode(bookingCode);
				fare.setAdultFare(resultSet.getDouble("fare_amount"));
				fare.setFareRuleCode(resultSet.getString("fare_rule_code"));
				fare.setFareBasisCode(resultSet.getString("fare_basis_code"));
				fare.setFareRuleComment(resultSet.getString("rules_comments"));
				fare.setAgentInstructions(resultSet.getString("agent_instructions"));
				fare.setMinimumStayOverInMins(resultSet.getLong("minimum_stay_over_mins"));
				fare.setMaximumStayOverInMins(resultSet.getLong("maximum_stay_over_mins"));
				fare.setOpenRTConfPeriodInMins(resultSet.getLong("openrt_conf_period_mins"));
				fare.setMinimumStayOverInMonths(resultSet.getLong("minimum_stay_over_months"));
				fare.setMaximumStayOverInMonths(resultSet.getLong("maximum_stay_over_months"));
				fare.setOpenRTConfPeriodInMonths(resultSet.getLong("openrt_conf_stay_over_months"));
				if (AppSysParamsUtil.isEnableFareRuleComments()) {
					String commentInSelected = StringUtil.clobToString(resultSet.getClob("comment_in_selected"));
					fare.setFareRuleCommentSelectedLanguage(StringUtil.getUnicode(commentInSelected));
				}
				fare.setFareCategoryCode(resultSet.getString("fare_cat_id"));
				fare.setChildFareType(resultSet.getString("child_fare_type"));
				fare.setChildFare(resultSet.getDouble("child_fare"));
				fare.setInfantFareType(resultSet.getString("infant_fare_type"));
				fare.setInfantFare(resultSet.getDouble("infant_fare"));
				fare.setAgentCommissionType(resultSet.getString("agent_commission_type"));
				fare.setAgentCommissionAmount(resultSet.getBigDecimal("agent_commission_amount"));
				if (resultSet.getString("fare_discount_min") != null && resultSet.getString("fare_discount_max") != null) {
					fare.setFareDiscountMin(resultSet.getInt("fare_discount_min"));
					fare.setFareDiscountMax(resultSet.getInt("fare_discount_max"));
				}
				fare.setReturnFare("Y".equals(resultSet.getString("return_flag")));
				fare.setHalfReturnFlag(resultSet.getString("halfrt_flag"));

				// TODO Set Applicabilities DONE

				faresBCInv.addFareSummaryDTO(fare);
			}

			FCCSegBCInvSummaryDTO bcInv = faresBCInv.getBcInventorySummaryDTO();
			int nestedRank = bcInv.getNestedRank();
			if (bcInv.isStandardFlag() && FCCSegBCInventory.Status.OPEN.equals(bcInv.getBcInvStatus()) && nestedRank > 0) {

				if (nestedAvailableSeatCountByLogCCMap.get(logicalCabinClassCode) == null) {
					nestedAvailableSeatCountMap = new HashMap<Integer, Integer>();
				} else {
					nestedAvailableSeatCountMap = nestedAvailableSeatCountByLogCCMap.get(logicalCabinClassCode);
				}

				if (nestedAvailableSeatCountMap.get(nestedRank) == null) {
					nestedAvailableSeatCountMap.put(nestedRank, 0);
					nestedAvailableSeatCountMap.put(nestedRank, resultSet.getInt("available_seats"));
					nestedAvailableSeatCountByLogCCMap.put(logicalCabinClassCode, nestedAvailableSeatCountMap);
				}

				if (faresBCInv.isHRTFareExists()) {
					if (nestedHRTAvailableSeatCountByLogCCMap.get(logicalCabinClassCode) == null) {
						nestedHRTAvailableSeatCountMap = new HashMap<Integer, Integer>();
					} else {
						nestedHRTAvailableSeatCountMap = nestedHRTAvailableSeatCountByLogCCMap.get(logicalCabinClassCode);
					}

					if (nestedHRTAvailableSeatCountMap.get(nestedRank) == null) {
						nestedHRTAvailableSeatCountMap.put(nestedRank, 0);
						nestedHRTAvailableSeatCountMap.put(nestedRank, resultSet.getInt("available_seats"));
						nestedHRTAvailableSeatCountByLogCCMap.put(logicalCabinClassCode, nestedHRTAvailableSeatCountMap);
					}
				}
			}

			if (salesChannelCode != null) {
				if (bucketFilteringCriteria.isExcludeNonLowestPublicFare()
						&& SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_PUBLIC_KEY) == salesChannelCode) {
					isExcludePublicFare = true;
				}
				if (fare.getVisibleChannels() == null || !fare.getVisibleChannels().contains(salesChannelCode)) {
					fare.addVisibleChannel(salesChannelCode);
				}
			}

			Object objAgentCode = resultSet.getObject("agent_code");
			if (objAgentCode != null) {
				if ((fare.getVisibleAgents() == null || !fare.getVisibleAgents().contains(objAgentCode.toString() ) && fare.getVisibleAgents().size() < AGENTS_THRESHOLD )) {
					fare.addVisibleAgent(objAgentCode.toString());
				}
			}
		}

		if (showNestedAvailability) {
			updateNestedAvailableSeatsCount(bcInvsMap, nestedAvailableSeatCountByLogCCMap, nestedHRTAvailableSeatCountByLogCCMap,
					bucketFilteringCriteria);
		}

		return bcInvsMap;
	}

	/**
	 * following function used to identify the nested available seat count, when request from change fare we need to get
	 * the nested available count according to the logical cabin class
	 * */
	private void updateNestedAvailableSeatsCount(LinkedHashMap<String, AllFaresBCInventorySummaryDTO> bcInvsMap,
			HashMap<String, HashMap<Integer, Integer>> nestedAvailableSeatCountByLogCCMap,
			HashMap<String, HashMap<Integer, Integer>> nestedHRTAvailableSeatCountByLogCCMap,
			BucketFilteringCriteria bucketFilteringCriteria) {

		if (bcInvsMap != null && bcInvsMap.size() > 0) {
			Collection<String> bookingCodeColl = bcInvsMap.keySet();
			Iterator<String> bookingCodeItr = bookingCodeColl.iterator();
			while (bookingCodeItr.hasNext()) {
				String bookingCode = bookingCodeItr.next();

				AllFaresBCInventorySummaryDTO faresBCInv = bcInvsMap.get(bookingCode);
				String logicalCC = faresBCInv.getLogicalCabinclass();
				FCCSegBCInvSummaryDTO bcInv = faresBCInv.getBcInventorySummaryDTO();
				if (bcInv.isStandardFlag() && FCCSegBCInventory.Status.OPEN.equals(bcInv.getBcInvStatus())
						&& bcInv.getNestedRank() > 0 && logicalCC != null) {
					int availableNestedSeats = getAvailableNestedSeatByRank(bcInv.getNestedRank(), logicalCC,
							nestedAvailableSeatCountByLogCCMap);
					bcInv.setAvailableNestedSeats(availableNestedSeats);

					int availableHRTNestedSeats = getAvailableNestedSeatByRank(bcInv.getNestedRank(), logicalCC,
							nestedHRTAvailableSeatCountByLogCCMap);
					bcInv.setAvailableHRTNestedSeats(availableHRTNestedSeats);

				} else {
					bcInv.setAvailableNestedSeats(bcInv.getAvailableSeats());
					bcInv.setAvailableHRTNestedSeats(bcInv.getAvailableSeats());
				}

				if (!(bucketFilteringCriteria.isSameBookedFltCabinBeingSearched()
						&& bucketFilteringCriteria.getBookedFlightBookingClass() != null && bucketFilteringCriteria
						.getBookedFlightBookingClass().equals(bookingCode)) && !bcInv.isStandardFlag()) {
					if (bucketFilteringCriteria.isCheckAvailability()
							&& bcInv.getAvailableSeats() < bucketFilteringCriteria.getNoOfActualRequiredSeats()
							&& bcInv.getAvailableNestedSeats() < bucketFilteringCriteria.getNoOfActualRequiredSeats()) {
						bookingCodeItr.remove();
					}
				}
			}
		}
	}

	private int getAvailableNestedSeatByRank(int nestedRank, String logicalCC,
			HashMap<String, HashMap<Integer, Integer>> nestedAvailableSeatCountByLogCCMap) {
		int total = 0;

		if (nestedAvailableSeatCountByLogCCMap != null && nestedAvailableSeatCountByLogCCMap.size() > 0 && logicalCC != null) {
			HashMap<Integer, Integer> nestedAvailableSeatCountMap = nestedAvailableSeatCountByLogCCMap.get(logicalCC);

			if (nestedAvailableSeatCountMap != null && nestedAvailableSeatCountMap.size() > 0) {

				for (Integer rank : nestedAvailableSeatCountMap.keySet()) {
					if (rank <= nestedRank) {
						total += nestedAvailableSeatCountMap.get(rank);
					}
				}

			}

		}

		return total;
	}
	
	private SeatAvailabilityDAOJDBC getSeatAvailabilityDAO() {
		return (SeatAvailabilityDAOJDBC) AirInventoryUtil.getInstance().getLocalBean("seatAvailabilityDAOJDBC");
	}
	
	private Map<String, List<Integer>> getAvailableNonFixedAndFixedSeatsCounts() {
		Map<String, List<Integer>> availableSeatsMap = new HashMap<String, List<Integer>>();
		try {
			availableSeatsMap =  getSeatAvailabilityDAO().getAvailableNonFixedAndFixedSeatsCounts(
					bucketFilteringCriteria.getFlightId(), bucketFilteringCriteria.getSegmentCode(), null, null, null);
		} catch (Exception ex) {
			//do nothing
		}
		return availableSeatsMap;
	}
}
