package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BucketFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;

/**
 * @author Nasly
 */
public class SeatAvailabilityResultSetExtractor implements ResultSetExtractor {
	private final AvailableFlightSegment _availableFlightSegment;
	private final BucketFilteringCriteria bucketFilteringCriteria;

	public SeatAvailabilityResultSetExtractor(AvailableFlightSegment availableFlightSegment,
			BucketFilteringCriteria filteringCriteria) {
		this._availableFlightSegment = availableFlightSegment;
		this.bucketFilteringCriteria = filteringCriteria;
	}

	/**
	 * Iterate through a set of BC inventory summaries result set and filters out cheapest non-fixed bucket(s) and fixed
	 * bucket
	 */
	@Override
	public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {

		FilteredBucketsDTO filteredBucketsDTO = AirInventoryDataExtractUtil.extractFilteredBucketsDTO(resultSet,
				bucketFilteringCriteria, true);
		_availableFlightSegment.setFilteredBucketsDTOForSegmentFares(filteredBucketsDTO);
		return _availableFlightSegment;
	}
}
