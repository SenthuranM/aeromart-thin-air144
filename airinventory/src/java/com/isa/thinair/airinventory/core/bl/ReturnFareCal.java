package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FarePriceOnd;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
class ReturnFareCal extends BaseFareCal {
	/**
	 * 
	 */
	private final GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator;
	private final List<List<FilteredBucketsDTO>> ondBuckets = new ArrayList<List<FilteredBucketsDTO>>();
	private List<List<FilteredBucketsDTO>> cheapestCommonBuckets = new ArrayList<List<FilteredBucketsDTO>>();
	private int ondSequence = 0;
	private List<Set<String>> ondWiseAvailableLogicalCabinClasses;
	private String requestedLogicalCabinClass;
	private int totalOnds;

	public ReturnFareCal(GroupedOndFlightsFareCalculator groupedOndFlightsFareCalculator, AvailableFlightSearchDTO avilFltDTO,
			List<Set<String>> ondWiseAvailableLogicalCabinClasses, int totalOnds) {
		this.groupedOndFlightsFareCalculator = groupedOndFlightsFareCalculator;
		this.priority = 1;
		if (avilFltDTO.getJourneyType() == JourneyType.ROUNDTRIP && totalOnds == 2) {
			if (validateSameLogiCCSelection(avilFltDTO) && validateSameCabinSelection(avilFltDTO)) {
				this.avilFltDTO = avilFltDTO;
				this.ondWiseAvailableLogicalCabinClasses = ondWiseAvailableLogicalCabinClasses;
				fareCalTypes.add(FareCalType.RT);
				this.totalOnds = totalOnds;
			} else {
				this.isValid = false;
			}
		} else {
			this.isValid = false;
		}
	}

	private boolean validateSameCabinSelection(AvailableFlightSearchDTO avilFltDTO) {
		boolean isFirst = true;
		String cabinClass = null;
		for (OriginDestinationInfoDTO ond : avilFltDTO.getOrderedOriginDestinations()) {
			if (isFirst) {
				isFirst = false;
				cabinClass = ond.getPreferredClassOfService();
			} else {
				// both logical cabins are null skip
				// logical cabins do not match return false
				if ((cabinClass == null && ond.getPreferredClassOfService() != null)
						&& (cabinClass != null && ond.getPreferredClassOfService() == null)
						|| (cabinClass != null && ond.getPreferredClassOfService() != null && !cabinClass.equals(ond
								.getPreferredClassOfService()))) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean validateSameLogiCCSelection(AvailableFlightSearchDTO avilFltDTO) {
		boolean isFirst = true;
		String logicalCC = null;
		for (OriginDestinationInfoDTO ond : avilFltDTO.getOrderedOriginDestinations()) {
			if (isFirst) {
				isFirst = false;
				logicalCC = ond.getPreferredLogicalCabin();
			} else {
				// both logical cabins are null skip
				// logical cabins do not match return false
				if ((logicalCC == null && ond.getPreferredLogicalCabin() != null)
						&& (logicalCC != null && ond.getPreferredLogicalCabin() == null)
						|| (logicalCC != null && ond.getPreferredLogicalCabin() != null && !logicalCC.equals(ond
								.getPreferredLogicalCabin()))) {
					return false;
				}
			}
		}
		this.requestedLogicalCabinClass = logicalCC;
		return true;
	}

	@Override
	public void addOndFlightSegment(Map<FareCalType, List<FilteredBucketsDTO>> mapCalTypeBuckets) throws ModuleException {
		isOndFlightsAdded = true;
		if (isValid()) {
			boolean bucketExits = false;
			for (FareCalType fareType : mapCalTypeBuckets.keySet()) {
				if (dependantFareTypes().contains(fareType)) {
					List<FilteredBucketsDTO> buckets = mapCalTypeBuckets.get(fareType);
					if (buckets.size() > 0) {
						bucketExits = true;
						ondBuckets.add(ondSequence, buckets);
					}
				}
			}
			if (!bucketExits) {
				isValid = false;
			}

			if ((ondSequence == totalOnds - 1) && isValid()) {
				triggerReturnFareCalculation();
			}
			ondSequence++;
		}
	}

	private String getBCKey(AvailableBCAllocationSummaryDTO bcAlloc) {
		return bcAlloc.getBookingCode() + bcAlloc.getFareSummaryDTO().getFareId();
	}

	private Set<String> getBCSet(Collection<AvailableBCAllocationSummaryDTO> allocs) {
		Set<String> bcSet = new HashSet<String>();
		for (AvailableBCAllocationSummaryDTO allocSummary : allocs) {
			bcSet.add(getBCKey(allocSummary));
		}
		return bcSet;
	}

	private Set<String> getCommonBkgCSet(Set<String> set1, Set<String> set2) {
		Set<String> retSet = new HashSet<String>(set1);
		if (set1 != null && set2 != null && !set1.isEmpty() && !set2.isEmpty()) {
			retSet.addAll(set1);
			retSet.retainAll(set2);
		}
		return retSet;
	}

	enum BCType {
		STD, NONSTD, FIXED, EXT
	}

	class BucketContainer {
		private List<ONDBucketContainer> ondContainers = new ArrayList<ReturnFareCal.ONDBucketContainer>();
		private boolean initialized = false;
		private boolean valid = false;
		private Map<String, Set<String>> lccWiseCommonBCSet = new HashMap<String, Set<String>>();
		private boolean adjustBC = true;

		BucketContainer(boolean adjustBC) {
			this.adjustBC = adjustBC;
		}

		public ONDBucketContainer addOndBuckContainer() {
			ONDBucketContainer container = new ONDBucketContainer();
			ondContainers.add(container);
			return container;
		}

		public boolean isValid() {
			if (ondContainers.isEmpty()) {
				return false;
			} else {
				for (ONDBucketContainer container : ondContainers) {
					if (!container.isValid()) {
						return false;
					}
				}
			}
			return valid;
		}

		private ONDBucketContainer getOndBucketContainer(int ondSequence) {
			if (ondContainers.size() == ondSequence) {
				return addOndBuckContainer();
			} else if (ondContainers.size() > ondSequence) {
				return ondContainers.get(ondSequence);
			}
			return null;
		}

		public List<ONDBucketContainer> getOndBuckContainers() {
			return ondContainers;
		}

		public void addBuckets(int ondSequence, int segmentSequence, String logicalCC,
				List<AvailableBCAllocationSummaryDTO> allocs) {
			if (!initialized) {
				valid = true;
			}
			if (valid) {
				ONDBucketContainer ondBuckContainer = getOndBucketContainer(ondSequence);
				if (ondBuckContainer != null) {
					Set<String> bcSet = getBCSet(allocs);
					if (!initialized) {
						lccWiseCommonBCSet.put(logicalCC, bcSet);
						ondBuckContainer.addBuckets(segmentSequence, logicalCC, allocs);
						if (!ondBuckContainer.isValid()) {
							invalidBuckContainer();
						}
					} else if (lccWiseCommonBCSet.containsKey(logicalCC)) {
						Set<String> commonBCKeySet = getCommonBkgCSet(lccWiseCommonBCSet.get(logicalCC), bcSet);
						if (commonBCKeySet.isEmpty()) {
							removeLogicalCabin(logicalCC);
						} else {
							ondBuckContainer.addBuckets(segmentSequence, logicalCC, allocs);
							if (adjustBC) {
								adjustBuckets(logicalCC, commonBCKeySet);
							}
							if (!ondBuckContainer.isValid()) {
								invalidBuckContainer();
							}
						}
					}
				} else {
					invalidBuckContainer();
				}
			}
			if (!initialized) {
				initialized = true;
			}
		}

		private void adjustBuckets(String logicalCC, Set<String> commonBCKeySet) {
			for (ONDBucketContainer ondBuckContainer : ondContainers) {
				ondBuckContainer.adjustBuckets(logicalCC, commonBCKeySet);
			}
		}

		private void removeLogicalCabin(String logicalCC) {
			lccWiseCommonBCSet.remove(logicalCC);
			if (lccWiseCommonBCSet.size() == 0) {
				invalidBuckContainer();
			} else {
				for (ONDBucketContainer container : ondContainers) {
					container.removeLogicalCabin(logicalCC);
				}
			}
		}

		private void invalidBuckContainer() {
			valid = false;
			ondContainers.clear();
		}
	}

	class ONDBucketContainer {
		private List<SegBucketContainer> segContainers = new ArrayList<ReturnFareCal.SegBucketContainer>();
		private boolean initialized = false;
		private boolean valid = false;

		public SegBucketContainer addSegBuckContainer() {
			SegBucketContainer container = new SegBucketContainer();
			segContainers.add(container);
			return container;
		}

		public void adjustBuckets(String logicalCC, Set<String> commonBCKeySet) {
			for (SegBucketContainer container : segContainers) {
				container.adjustBuckets(logicalCC, commonBCKeySet);
			}
		}

		public void removeLogicalCabin(String logicalCC) {
			for (SegBucketContainer container : segContainers) {
				container.removeLogicalCabin(logicalCC);
			}
		}

		private SegBucketContainer getSegBuckContainer(int segmentSequence) {
			if (segContainers.size() == segmentSequence) {
				return addSegBuckContainer();
			} else if (segContainers.size() > segmentSequence) {
				return segContainers.get(segmentSequence);
			}
			return null;
		}

		public void addBuckets(int segmentSequence, String logicalCC, List<AvailableBCAllocationSummaryDTO> allocs) {
			if (!initialized) {
				valid = true;
			}

			if (valid) {
				SegBucketContainer segBuckContainer = getSegBuckContainer(segmentSequence);
				if (segBuckContainer != null) {
					segBuckContainer.addBuckets(logicalCC, allocs);
					if (!segBuckContainer.isValid()) {
						invalidBuckContainer();
					}
				} else {
					invalidBuckContainer();
				}
			}
		}

		private void invalidBuckContainer() {
			valid = false;
			segContainers.clear();
		}

		public boolean isValid() {
			if (segContainers.isEmpty()) {
				return false;
			} else {
				for (SegBucketContainer container : segContainers) {
					if (!container.isValid()) {
						return false;
					}
				}
			}
			return true;
		}

		public SegBucketContainer getFirstSegBuckContainer() {
			return segContainers.get(0);
		}

		public List<SegBucketContainer> getSegBuckContainers() {
			return segContainers;
		}
	}

	class SegBucketContainer {
		Map<String, List<AvailableBCAllocationSummaryDTO>> lccContainers = new HashMap<String, List<AvailableBCAllocationSummaryDTO>>();

		public void addBuckets(String logicalCC, List<AvailableBCAllocationSummaryDTO> allocs) {
			if (logicalCC != null && !allocs.isEmpty()) {
				lccContainers.put(logicalCC, allocs);
			}
		}

		public void removeLogicalCabin(String logicalCC) {
			lccContainers.remove(logicalCC);
		}

		public void adjustBuckets(String logicalCC, Set<String> cmnBCKeySet) {
			Iterator<AvailableBCAllocationSummaryDTO> itr = lccContainers.get(logicalCC).iterator();
			while (itr.hasNext()) {
				if (!cmnBCKeySet.contains(getBCKey(itr.next()))) {
					itr.remove();
				}
			}
			if (lccContainers.get(logicalCC).size() == 0) {
				removeLccAlloc(logicalCC);
			}
		}

		public boolean isValid() {
			return !lccContainers.isEmpty();
		}

		public void removeLccAlloc(String logicalCC) {
			lccContainers.remove(logicalCC);
		}

		public Set<String> getLogicalCabins() {
			return lccContainers.keySet();
		}

		public List<AvailableBCAllocationSummaryDTO> getAllocation(String logicalCC) {
			return lccContainers.get(logicalCC);
		}
	}

	class BucketFilter {
		Map<BCType, BucketContainer> bcTypeBucketContainers = new HashMap<ReturnFareCal.BCType, ReturnFareCal.BucketContainer>();

		public void addBucketContainer(BCType bcType, boolean adjustBuckets) {
			bcTypeBucketContainers.put(bcType, new BucketContainer(adjustBuckets));
		}

		public void addBuckets(BCType bcType, int ondSequence, int segmentSequence, String logicalCC,
				List<AvailableBCAllocationSummaryDTO> allocs) {
			if (bcTypeBucketContainers.containsKey(bcType)) {
				bcTypeBucketContainers.get(bcType).addBuckets(ondSequence, segmentSequence, logicalCC, allocs);
				if (!bcTypeBucketContainers.get(bcType).isValid()) {
					bcTypeBucketContainers.remove(bcType);
				}
			}

		}

		public boolean isValid() {
			if (bcTypeBucketContainers.isEmpty()) {
				return false;
			}
			for (BucketContainer container : bcTypeBucketContainers.values()) {
				if (!container.isValid()) {
					return false;
				}
			}
			return true;
		}

		void invalidate() {
			bcTypeBucketContainers.clear();
		}

		public Set<BCType> getBCTypes() {
			return bcTypeBucketContainers.keySet();
		}

		public BucketContainer getBucketContainer(BCType bcType) {
			return bcTypeBucketContainers.get(bcType);
		}
	}

	private List<List<FilteredBucketsDTO>> ondBucketList() {
		List<List<FilteredBucketsDTO>> retList = new ArrayList<List<FilteredBucketsDTO>>();

		BucketFilter buckFilter = new BucketFilter();
		buckFilter.addBucketContainer(BCType.STD, !AppSysParamsUtil.isReturnFareNestingEnabled());
		buckFilter.addBucketContainer(BCType.NONSTD, true);
		buckFilter.addBucketContainer(BCType.FIXED, true);
		buckFilter.addBucketContainer(BCType.EXT, true);

		int ondSequence = 0;
		ond: for (List<FilteredBucketsDTO> ondWiseBuckets : ondBuckets) {
			int segmentSequence = 0;
			for (FilteredBucketsDTO segBucket : ondWiseBuckets) {
				if (segBucket.getAvailableLogicalCabinClasses().size() == 0) {
					buckFilter.invalidate();
					break ond;
				}
				for (String logicalCC : segBucket.getAvailableLogicalCabinClasses()) {
					List<AvailableBCAllocationSummaryDTO> stndAllocs = new ArrayList<AvailableBCAllocationSummaryDTO>(
							segBucket.getBucket(logicalCC, true, false, false));
					List<AvailableBCAllocationSummaryDTO> nonStdNonFixedAllocs = new ArrayList<AvailableBCAllocationSummaryDTO>(
							segBucket.getBucket(logicalCC, false, false, false));
					List<AvailableBCAllocationSummaryDTO> nonStdFixedAllocs = new ArrayList<AvailableBCAllocationSummaryDTO>(
							segBucket.getBucket(logicalCC, false, true, false));
					List<AvailableBCAllocationSummaryDTO> extAllocs;
					if (segBucket.getExternalBookingClassAvailability(logicalCC) != null) {
						extAllocs = new ArrayList<AvailableBCAllocationSummaryDTO>(
								segBucket.getExternalBookingClassAvailability(logicalCC));
					} else {
						extAllocs = new ArrayList<AvailableBCAllocationSummaryDTO>();
					}
					BCType bcType = BCType.STD;
					List<AvailableBCAllocationSummaryDTO> allocs = stndAllocs;
					buckFilter.addBuckets(bcType, ondSequence, segmentSequence, logicalCC, allocs);
					bcType = BCType.NONSTD;
					allocs = nonStdNonFixedAllocs;
					buckFilter.addBuckets(bcType, ondSequence, segmentSequence, logicalCC, allocs);
					bcType = BCType.FIXED;
					allocs = nonStdFixedAllocs;
					buckFilter.addBuckets(bcType, ondSequence, segmentSequence, logicalCC, allocs);
					bcType = BCType.EXT;
					allocs = extAllocs;
					buckFilter.addBuckets(bcType, ondSequence, segmentSequence, logicalCC, allocs);
					if (!buckFilter.isValid()) {
						break ond;
					}

				}
				segmentSequence++;
			}
			ondSequence++;
		}
		if (buckFilter.isValid()) {
			for (BCType bcType : buckFilter.getBCTypes()) {
				ondSequence = 0;
				for (ONDBucketContainer ondBuckContainer : buckFilter.getBucketContainer(bcType).getOndBuckContainers()) {
					List<FilteredBucketsDTO> ondRetList = null;
					if (retList.size() == ondSequence) {
						ondRetList = new ArrayList<FilteredBucketsDTO>();
						retList.add(ondRetList);
					} else {
						ondRetList = retList.get(ondSequence);
					}
					int segmentSequence = 0;
					for (SegBucketContainer segBuckContainer : ondBuckContainer.getSegBuckContainers()) {
						FilteredBucketsDTO filteredBucket = null;
						if (ondRetList.size() == segmentSequence) {
							filteredBucket = new FilteredBucketsDTO();
							ondRetList.add(filteredBucket);
						} else {
							filteredBucket = ondRetList.get(segmentSequence);
						}
						for (String logicalCC : segBuckContainer.getLogicalCabins()) {
							for (AvailableBCAllocationSummaryDTO alloc : segBuckContainer.getAllocation(logicalCC)) {
								if (bcType == BCType.STD) {
									filteredBucket.addBucket(logicalCC, alloc, true, false, false, false);
								} else if (bcType == BCType.NONSTD) {
									filteredBucket.addBucket(logicalCC, alloc, false, false, false, false);
								} else if (bcType == BCType.FIXED) {
									filteredBucket.addBucket(logicalCC, alloc, false, true, false, false);
								} else if (bcType == BCType.EXT) {
									filteredBucket.addExternalBookingClassAvailability(logicalCC, alloc);
								}
							}
						}
						segmentSequence++;
					}
					ondSequence++;
				}
			}
		}

		return retList;
	}

	private void triggerReturnFareCalculation() throws ModuleException {
		List<List<FilteredBucketsDTO>> commonBuckets = ondBucketList();
		if (!commonBuckets.isEmpty()) {
			this.cheapestCommonBuckets.add(this.groupedOndFlightsFareCalculator.getMinimumFareBCAllocationSummaryDTO(
					commonBuckets.get(0), requestedLogicalCabinClass, ondWiseAvailableLogicalCabinClasses));
		}
		boolean returnFareNestingEnabled = AppSysParamsUtil.showNestedAvailableSeats();
		if (cheapestCommonBuckets.isEmpty() || cheapestCommonBuckets.get(0).isEmpty()) {
			isValid = false;
		} else {
			double perOndFare = getMininumFare(null, cheapestCommonBuckets.get(0), avilFltDTO);
			if (perOndFare != -1) {
				FilteredBucketsDTO cheapestBucket = cheapestCommonBuckets.get(0).get(0);
				String selectedLogicCC = cheapestBucket.getChecpestFareLogicalCabinClass();
				AvailableBCAllocationSummaryDTO allocationSummary = AirInventoryDataExtractUtil.getLastElement(cheapestBucket
						.getCheapestBCAllocationSummaryDTOs());
				if (!AppSysParamsUtil.isEnableTotalPriceBasedMinimumFareQuote()) {
					this.totalMinimumFare = perOndFare * totalOnds;
				} else {
					allocationSummary.getBookingCode();
					AvailableBCAllocationSummaryDTO retAlloc = null;
					LoopBuckets: for (FilteredBucketsDTO retBucket : commonBuckets.get(1)) {
						List<AvailableBCAllocationSummaryDTO> retAllocs = retBucket.getBucket(selectedLogicCC,
								allocationSummary.isStandardBC(), allocationSummary.isFixedBC(), false);
						for (AvailableBCAllocationSummaryDTO retAllocSummary : retAllocs) {
							if (allocationSummary.getBookingCode().equals(retAllocSummary.getBookingCode())) {
								retAlloc = retAllocSummary;
								break LoopBuckets;
							}
						}
					}
					double retOndProrateFare = AirInventoryDataExtractUtil.getFareAmount(retAlloc, avilFltDTO);
					this.totalMinimumFare = perOndFare + retOndProrateFare;
				}

				int ondSequence = 0;
				for (List<FilteredBucketsDTO> ondBucket : ondBuckets) {
					if (ondSequence == 0) {
						ondSequence++;
						continue;
					}
					int segSequence = 0;
					for (FilteredBucketsDTO bucket : ondBucket) {
						List<AvailableBCAllocationSummaryDTO> invAlloc = null;
						if (allocationSummary.isStandardBC()) {
							invAlloc = new ArrayList<AvailableBCAllocationSummaryDTO>(bucket
									.getLogicalCCWiseCheapestStandardBuckets().get(selectedLogicCC));
						} else if (allocationSummary.isFixedBC()) {
							invAlloc = new ArrayList<AvailableBCAllocationSummaryDTO>(bucket
									.getLogicalCCWiseCheapestFixedBuckets().get(selectedLogicCC));
						} else {
							invAlloc = new ArrayList<AvailableBCAllocationSummaryDTO>(bucket
									.getLogicalCCWiseCheapestNonStandardBuckets().get(selectedLogicCC));
						}

						if (invAlloc != null && !invAlloc.isEmpty()) {
							if (allocationSummary.isStandardBC()) {
								Iterator<AvailableBCAllocationSummaryDTO> invItr = invAlloc.iterator();
								boolean matchFound = false;
								int seatCount = 0;
								int totalRequestedCount = avilFltDTO.getAdultCount() + avilFltDTO.getChildCount();
								while (invItr.hasNext()) {
									AvailableBCAllocationSummaryDTO inv = invItr.next();
									if (!returnFareNestingEnabled) {
										if (!(getBCKey(inv).equals(getBCKey(allocationSummary)))) {
											invItr.remove();
										}
									} else {
										seatCount += inv.getNoOfSeats();
										if (inv.getBookingCode().equals(allocationSummary.getBookingCode())) {
											if (!matchFound && seatCount >= totalRequestedCount) {
												matchFound = true;
											}

										} else if (matchFound) {
											invItr.remove();
										}
									}
								}

								if (cheapestCommonBuckets.size() > ondSequence) {
									if (cheapestCommonBuckets.get(ondSequence).size() > segSequence) {
										FilteredBucketsDTO currentCheapBucket = cheapestCommonBuckets.get(ondSequence).get(
												segSequence);
										List<AvailableBCAllocationSummaryDTO> currentInvAlloc = currentCheapBucket
												.getLogicalCCWiseCheapestBCAllocationSummaryDTOs().get(selectedLogicCC);
										if (currentInvAlloc != null && currentInvAlloc.size() > 0) {
											AvailableBCAllocationSummaryDTO currentLastBCAlloc = AirInventoryDataExtractUtil
													.getLastElement(currentInvAlloc);
											AvailableBCAllocationSummaryDTO lastBCAlloc = AirInventoryDataExtractUtil
													.getLastElement(invAlloc);
											if (!currentLastBCAlloc.getBookingCode().equals(lastBCAlloc.getBookingCode())) {
												isValid = false;
											}
										}
									} else {
										if (!AirInventoryDataExtractUtil.getLastElement(invAlloc).getBookingCode()
												.equals(allocationSummary.getBookingCode())) {
											isValid = false;
										} else {
											FilteredBucketsDTO cheapBucket = bucket.clone();
											cheapBucket.setChecpestFareLogicalCabinClass(selectedLogicCC);
											cheapBucket.getCheapestBCAllocationSummaryDTOs().clear();
											cheapBucket.getCheapestBCAllocationSummaryDTOs().addAll(invAlloc);
											cheapBucket.getLogicalCCWiseCheapestBCAllocationSummaryDTOs().put(selectedLogicCC,
													invAlloc);
											cheapestCommonBuckets.get(ondSequence).add(cheapBucket);
										}
									}
								} else {
									if (!AirInventoryDataExtractUtil.getLastElement(invAlloc).getBookingCode()
											.equals(allocationSummary.getBookingCode())) {
										isValid = false;
									} else {
										FilteredBucketsDTO cheapBucket = bucket.clone();
										cheapBucket.setChecpestFareLogicalCabinClass(selectedLogicCC);
										cheapBucket.getCheapestBCAllocationSummaryDTOs().clear();
										cheapBucket.getCheapestBCAllocationSummaryDTOs().addAll(invAlloc);
										cheapBucket.getLogicalCCWiseCheapestBCAllocationSummaryDTOs().put(selectedLogicCC,
												invAlloc);
										List<FilteredBucketsDTO> cheapBuckets = new ArrayList<FilteredBucketsDTO>();
										cheapBuckets.add(cheapBucket);
										cheapestCommonBuckets.add(ondSequence, cheapBuckets);
									}
								}
							} else {
								Iterator<AvailableBCAllocationSummaryDTO> invItr = invAlloc.iterator();
								while (invItr.hasNext()) {
									if (!(getBCKey(invItr.next()).equals(getBCKey(allocationSummary)))) {
										invItr.remove();
									}
								}
								AvailableBCAllocationSummaryDTO bucketAllocSum = AirInventoryDataExtractUtil
										.getChapestElement(invAlloc);
								if (bucketAllocSum.getBookingCode().equals(allocationSummary.getBookingCode())) {
									invAlloc.clear();
									invAlloc.add(bucketAllocSum);
									FilteredBucketsDTO cheapBucket = bucket.clone();
									cheapBucket.setChecpestFareLogicalCabinClass(selectedLogicCC);
									cheapBucket.getCheapestBCAllocationSummaryDTOs().clear();
									cheapBucket.getCheapestBCAllocationSummaryDTOs().addAll(invAlloc);
									cheapBucket.getLogicalCCWiseCheapestBCAllocationSummaryDTOs().put(selectedLogicCC, invAlloc);
									List<FilteredBucketsDTO> cheapBuckets = new ArrayList<FilteredBucketsDTO>();
									cheapBuckets.add(cheapBucket);
									cheapestCommonBuckets.add(ondSequence, cheapBuckets);
									break;
								}
							}
						}
						segSequence++;
					}

					if (cheapestCommonBuckets.size() <= ondSequence || cheapestCommonBuckets.get(ondSequence) == null) {
						isValid = false;
						break;
					}

					ondSequence++;
				}

			} else {
				isValid = false;
			}
		}
	}

	@Override
	public void setFareAndLogiCC(List<AvailableIBOBFlightSegment> ondFlights, boolean isMinimumFare) {
		int sequence = 0;
		this.groupedOndFlightsFareCalculator.setMinimumFareSet(this.groupedOndFlightsFareCalculator.isMinimumFareSet()
				|| isMinimumFare);
		for (AvailableIBOBFlightSegment ondFlight : ondFlights) {
			this.groupedOndFlightsFareCalculator.setMinimumFareSet(this.groupedOndFlightsFareCalculator.isMinimumFareSet()
					|| isMinimumFare);
			this.groupedOndFlightsFareCalculator.setFareAndLogicalCabinClass(ondFlight, new ArrayList<FilteredBucketsDTO>(
					cheapestCommonBuckets.get(sequence)), FareTypes.HALF_RETURN_FARE, isMinimumFare, null);
			sequence++;
		}
		this.groupedOndFlightsFareCalculator.updateAvailableLogcalCabinClasses(this.ondWiseAvailableLogicalCabinClasses,
				ondFlights);
	}

	public void setMinimumFares(List<AvailableIBOBFlightSegment> ondFlights) {
		int sequence = 0;
		for (AvailableIBOBFlightSegment ondFlight : ondFlights) {
			if (sequence == OndSequence.IN_BOUND) {
				ondFlight.setMinimumFareByPriceOnd(FarePriceOnd.RETURN, this.totalMinimumFare);
			}
			sequence++;
		}
		this.groupedOndFlightsFareCalculator.updateAvailableLogcalCabinClasses(this.ondWiseAvailableLogicalCabinClasses,
				ondFlights);
	}
}