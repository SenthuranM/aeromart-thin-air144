package com.isa.thinair.airinventory.core.bl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FilteredBucketsDTO;
import com.isa.thinair.airinventory.core.util.AirInventoryDataExtractUtil;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
abstract class BaseFareCal implements IFareCalculator {
	protected Set<FareCalType> fareCalTypes = new HashSet<FareCalType>();
	boolean isValid = true;
	boolean isOndFlightsAdded = false;
	protected double totalMinimumFare = -1d;
	protected AvailableFlightSearchDTO avilFltDTO = null;
	protected int priority = 100;

	@Override
	public boolean isValid() {
		return isOndFlightsAdded && isValid;
	}

	@Override
	public boolean isValidForJourney() {
		return isValid;
	}

	@Override
	public Set<FareCalType> dependantFareTypes() {
		return fareCalTypes;
	}

	protected double getMininumFare(FareCalType calType, List<FilteredBucketsDTO> buckets, AvailableFlightSearchDTO avilFltDTO)
			throws ModuleException {
		AvailableBCAllocationSummaryDTO minFareSummaryDTO = AirInventoryDataExtractUtil.getLastElement(buckets.get(0)
				.getCheapestBCAllocationSummaryDTOs());
		if (minFareSummaryDTO != null) {
			return AirInventoryDataExtractUtil.getFareAmount(minFareSummaryDTO, avilFltDTO);
		}
		return -1d;
	}

	@Override
	public double getTotalMinimumFare() {
		return totalMinimumFare;
	}

	@Override
	public int getPriority() {
		return priority;
	}
}