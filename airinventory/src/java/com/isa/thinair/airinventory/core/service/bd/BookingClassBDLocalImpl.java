package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airinventory.api.service.BookingClassBD;

@Local
public interface BookingClassBDLocalImpl extends BookingClassBD {

}
