package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.BookingClassBD;

@Remote
public interface BookingClassBDImpl extends BookingClassBD {

}
