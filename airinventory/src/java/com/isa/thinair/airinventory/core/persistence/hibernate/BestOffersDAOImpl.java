package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.model.BestOffersRequest;
import com.isa.thinair.airinventory.api.model.BestOffersResponse;
import com.isa.thinair.airinventory.core.persistence.dao.BestOffersDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @isa.module.dao-impl dao-name="BestOffersDAOImpl"
 */
public class BestOffersDAOImpl extends PlatformBaseHibernateDaoSupport implements BestOffersDAO {

	public Collection<BestOffersDTO> getCashedResultsForBestOffers(int recordId) throws ModuleException {
		Collection<BestOffersDTO> colBestOffers = null;
		List<BestOffersResponse> colResponse = find("from BestOffersResponse res where res.bestOffersRequestId = ?", recordId, BestOffersResponse.class);
		if (colResponse != null) {
			colBestOffers = new ArrayList<BestOffersDTO>();
			Iterator<BestOffersResponse> iterRes = colResponse.iterator();
			while (iterRes.hasNext()) {
				BestOffersResponse response = (BestOffersResponse) iterRes.next();
				if (response != null) {
					BestOffersDTO bestOffers = new BestOffersDTO();
					bestOffers.setOriginAirport(response.getOrigin());
					bestOffers.setDestinationAirport(response.getDestination());
					bestOffers.setOriginAirportName(response.getOriginAirportName());
					bestOffers.setDestinationAirportName(response.getDestinationAirportName());
					bestOffers.getFlightNos().add(response.getFlightNos());
					bestOffers.setDepartureDateTimeLocal(response.getDepartureDateTimeLocal());
					bestOffers.setFareAmount(response.getFareAmount());
					bestOffers.setTotalTaxAmount(response.getTotalTax());
					bestOffers.setTotalSurchargesAmount(response.getTotalSurcharges());
					bestOffers.setTotalAmount(response.getTotalAmount());
					bestOffers.setRank(response.getRank());
					colBestOffers.add(bestOffers);
				}
			}
		}
		return colBestOffers;
	}

	public void saveBestOffersDataForCaching(BestOffersRequest bestOffersRequest) throws ModuleException {
		hibernateSaveOrUpdate(bestOffersRequest);
	}

	@SuppressWarnings("rawtypes")
	public void removeExpiredBestOffersData(int expireTimeInMins) throws ModuleException {
		try {
			String hql = "Select req from BestOffersRequest req where "
					+ " to_timestamp(to_char(:currentTime,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') >  (req.lastRequestTime + :expireTimeInMins / 1440)";

			List lstBestOffersRequest = getSession().createQuery(hql)
					.setParameter("currentTime", new java.sql.Timestamp(System.currentTimeMillis()))
					.setParameter("expireTimeInMins", expireTimeInMins).list();
			if (lstBestOffersRequest != null) {
				deleteAll(lstBestOffersRequest);
			}
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}
}
