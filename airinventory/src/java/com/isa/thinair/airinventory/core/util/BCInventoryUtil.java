package com.isa.thinair.airinventory.core.util;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;

public class BCInventoryUtil {
	private static Map<String, Boolean> bookingClassMap = new HashMap<>();

	public static boolean isStandardBucket(String bookingCode) {
		if (!bookingClassMap.containsKey(bookingCode)) {
			synchronized (BCInventoryUtil.class) {
				if (!bookingClassMap.containsKey(bookingCode)) {
					loadBookingClasses();
				}
			}
		}
		if (bookingClassMap.containsKey(bookingCode) && bookingClassMap.get(bookingCode)) {
			return true;
		}
		return false;
	}

	private static void loadBookingClasses() {
		Map<String, Boolean> bcMap = new HashMap<>();
		for (BookingClass bc : getBookingClassDAO().getBookingClasses()) {
			bcMap.put(bc.getBookingCode(), bc.getStandardCode());
		}

		bookingClassMap = bcMap;
	}

	private static BookingClassDAO getBookingClassDAO() {
		return (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
	}

	private static BookingClassJDBCDAO getBookingClassJDBCDAO() {
		return (BookingClassJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("bookingClassJdbcDAO");
	}

}
