package com.isa.thinair.airinventory.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.FlightSegAvailableBCWithFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BestOffersSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.model.BestOffersRequest;
import com.isa.thinair.airinventory.api.model.BestOffersResponse;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BestOffersDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BestOffersDAOJDBC;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class BestOffersBL {

	private final Log log = LogFactory.getLog(getClass());
	private final FlightDAO flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(
			InternalConstants.DAOProxyNames.FLIGHT_DAO);

	private final AirportDAO airportDAO = lookupAirportDAO();

	private final BestOffersDAO bestOffersDAO = AirInventoryModuleUtils.getBestOffersDAO();

	private final BookingClassDAO bookingClassDAO = AirInventoryModuleUtils.getBookingClassDAO();

	public Collection<BestOffersDTO> getBestOffers(BestOffersSearchDTO bestOffersSearchDTO) throws ModuleException {
		Collection<BestOffersDTO> colBestOffers = null;
		if (AppSysParamsUtil.isEnableDynamicBestOffers()) {
			boolean cachedResultsEnabled = AppSysParamsUtil.isEnableDynamicBestOffersCaching();
						
			if (cachedResultsEnabled) {
				colBestOffers = getBestOffersFromCachedResults(bestOffersSearchDTO);
				if (colBestOffers != null) {
					Collections.sort((List<BestOffersDTO>) colBestOffers);
				}
			}

			if (colBestOffers == null) {
				colBestOffers = quoteBestOffers(bestOffersSearchDTO);
				if (cachedResultsEnabled) {
					cacheBestOffersResults(bestOffersSearchDTO, colBestOffers);
				}
			}
		}
		return colBestOffers;
	}

	// @SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection<BestOffersDTO> quoteBestOffers(BestOffersSearchDTO bestOffersSearchDTO) throws ModuleException {
		int displayFareCnt = 0;
		boolean enableTotalQuote = bestOffersSearchDTO.isTotalQuoteFlag();
		boolean ondSepecified = false;
		if (bestOffersSearchDTO.isSummaryFlag()) {
			displayFareCnt = 1;
		} else {
			displayFareCnt = AirinventoryUtils.getAirInventoryConfig().getBestOffersDisplayFareCount();
		}

		bestOffersSearchDTO.setNoOfMonths(AirinventoryUtils.getAirInventoryConfig().getBestOffersSearchDurationInMonths());
		Collection<BestOffersDTO> colBestOffers = new ArrayList<BestOffersDTO>();
		Collection colChargeQuoteCriteria = new ArrayList();

		boolean quotePerSegmentChargesForConnections = false;

		List<String> segmentCodesForConChargeQuote = new ArrayList<String>();
		List availableOutBoundRoutes;

		if (bestOffersSearchDTO.getOriginAirport() != null || bestOffersSearchDTO.getDestinationAirport() != null) {
			String originAirport = bestOffersSearchDTO.getOriginAirport() != null ? bestOffersSearchDTO.getOriginAirport() : "%";
			String destinationAirport = bestOffersSearchDTO.getDestinationAirport() != null ? bestOffersSearchDTO
					.getDestinationAirport() : "%";
			availableOutBoundRoutes = flightDAO.getAvailableRoutes(originAirport, destinationAirport,
					AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
			ondSepecified = true;
		} else {
			availableOutBoundRoutes = getBestOffersDAOJDBCImpl().getAvailableRoutesForCountryOfOrigin(
					bestOffersSearchDTO.getCountryCode(), AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
		}

		List<String> ondCodeList = new ArrayList<String>();
		Set<String> bookingCodeSet = new HashSet<String>();
		
		for (Iterator availableRoutesIt = availableOutBoundRoutes.iterator(); availableRoutesIt.hasNext();) {
			RouteInfoTO availableRoute = (RouteInfoTO) availableRoutesIt.next();
			if (availableRoute.isDirectRoute()) {
				ondCodeList.add(availableRoute.getOndCode());
			} else {
				// connection fares search - search for connected outbound
				// flights on one or more route(s)
				Collection<BestOffersDTO> bestOffersConnFares = getBestOffersForConnectionOnDs(
						availableRoute.getFromAirportCode(), availableRoute.getToAirportCode(), availableRoute.getOndCode(),
						availableRoute.getTransitAirportCodesList(), bestOffersSearchDTO, displayFareCnt);

				// Connection is not complete
				if (!doesAllRouteMatch(availableRoute.getFromAirportCode(), availableRoute.getToAirportCode(),
						bestOffersConnFares)) {
					bestOffersConnFares = Collections.emptyList();
				}

				if (bestOffersConnFares != null && bestOffersConnFares.size() > 0) {
					for (Object bestOffer : bestOffersConnFares) {
						BestOffersDTO conBestOffer = (BestOffersDTO) bestOffer;
						if (conBestOffer.isConnectionWithSegmentFare()) {
							if (!segmentCodesForConChargeQuote.contains(conBestOffer.getSegmentCode())) {
								segmentCodesForConChargeQuote.add(conBestOffer.getSegmentCode());
							}
						}
						bookingCodeSet.addAll(conBestOffer.getBookingCodes());
					}

					if (segmentCodesForConChargeQuote.size() > 0) {
						quotePerSegmentChargesForConnections = true;
					}
				}
				colBestOffers.addAll(bestOffersConnFares);
			}
		}
		if (ondCodeList.size() > 0) {
			if (bestOffersSearchDTO.isSummaryFlag() && !ondSepecified) {
				for (String ondCode : ondCodeList) {
					List<String> ondCodes = new ArrayList<String>();
					ondCodes.add(ondCode);
					Object[] bestOffersSingle = getBestOffersDAOJDBCImpl().getOverallDynamiCBestOffersForSingleFlights(
							bestOffersSearchDTO, ondCodes, displayFareCnt);
					if (bestOffersSingle != null && bestOffersSingle.length > 0) {
						BestOffersDTO singleBestOffer = (BestOffersDTO) bestOffersSingle[0];
						bookingCodeSet.addAll(singleBestOffer.getBookingCodes());
						colBestOffers.add(singleBestOffer);
					}
				}
			} else {
				Object[] bestOffers = getBestOffersDAOJDBCImpl().getOverallDynamiCBestOffersForSingleFlights(bestOffersSearchDTO,
						ondCodeList, displayFareCnt);
				for (Object bestOffer : bestOffers) {
					BestOffersDTO bestOfferOverall = (BestOffersDTO) bestOffer;
					bookingCodeSet.addAll(bestOfferOverall.getBookingCodes());
					colBestOffers.add(bestOfferOverall);
				}
			}
		}
		// Charge Quote for best offers
		if (enableTotalQuote && !bookingCodeSet.isEmpty()) {

			// Get BC wise applicable charge codes at once

			//Best offers are not quoted with buses hence we are setting bus booking codes empty
			
			Map<String, Collection<String>> bookingClassWiseApplicableChargeGroups = bookingClassDAO
					.getBookingClassChargeGroups(bookingCodeSet, new HashSet<String>());

			List<String> chargeQuotingSegments = new ArrayList<String>();
			for (Iterator availableRoutesIt = availableOutBoundRoutes.iterator(); availableRoutesIt.hasNext();) {
				RouteInfoTO availableRoute = (RouteInfoTO) availableRoutesIt.next();
				if (!chargeQuotingSegments.contains(availableRoute.getOndCode())) {
					addToChargeQuoteCriteria(colChargeQuoteCriteria, getRestrictionsDTO(bestOffersSearchDTO),
							availableRoute.getOndCode(), false);
					chargeQuotingSegments.add(availableRoute.getOndCode());
				}
			}

			if (quotePerSegmentChargesForConnections) {
				for (String segCode : segmentCodesForConChargeQuote) {
					String[] airports = segCode.split("/");
					for (int i = 0; i < airports.length - 1; i++) {

						String tempSegCode = airports[i] + "/" + airports[i + 1];

						if (!chargeQuotingSegments.contains(tempSegCode)) {
							addToChargeQuoteCriteria(colChargeQuoteCriteria, getRestrictionsDTO(bestOffersSearchDTO),
									tempSegCode, false);
						}

					}
				}
			}

			HashMap charges = getChargeBD().quoteCharges(colChargeQuoteCriteria);
			quoteChargesPerFlight(colBestOffers, charges, bookingClassWiseApplicableChargeGroups);
		}

		
		List<BestOffersDTO> colBestOffersList = new ArrayList<BestOffersDTO>();
		
		if (colBestOffers != null && !colBestOffers.isEmpty()) {
			colBestOffersList = removeDuplicateResults(colBestOffers);
			Collections.sort(colBestOffersList);
		}
		
		
		if (colBestOffersList != null && colBestOffersList.size() > displayFareCnt) {
			List tmp = new ArrayList();
			int i = 0;
			for (Object element : colBestOffers) {
				if (i < displayFareCnt) {
					tmp.add(element);
					++i;
				} else {
					break;
				}
			}
			colBestOffersList = tmp;
		}
		return colBestOffersList;
	}
	
	private List<BestOffersDTO> removeDuplicateResults(Collection<BestOffersDTO> bestOffers) {

		Map<String, BestOffersDTO> resultsMap = new HashMap<String, BestOffersDTO>();
		for (BestOffersDTO bestOffer : bestOffers) {
			if (!resultsMap.containsKey(bestOffer.getUniqueKey())) {
				resultsMap.put(bestOffer.getUniqueKey(), bestOffer);
			}
		}
		return new ArrayList<BestOffersDTO>(resultsMap.values());
	}

	private boolean doesAllRouteMatch(String origin, String destination, Collection<BestOffersDTO> routeOffers) {
		boolean hasOrigin = false;
		boolean hasDestination = false;

		for (BestOffersDTO offer : routeOffers) {
			hasOrigin = offer.getSegmentCode().contains(origin);
			hasDestination = offer.getSegmentCode().contains(destination);

			if (hasOrigin && hasDestination)
				break;
		}

		return hasOrigin && hasDestination;
	}

	private AvailableFlightSearchDTO getRestrictionsDTO(BestOffersSearchDTO bestOffersSearchDTO) {
		AvailableFlightSearchDTO restrictionsDTO = new AvailableFlightSearchDTO();
		restrictionsDTO.setAdultCount(1);
		restrictionsDTO.setDepatureDateTimeStart(bestOffersSearchDTO.getDateFrom());
		restrictionsDTO.setDepatureDateTimeEnd(bestOffersSearchDTO.getDateTo());
		restrictionsDTO.setFromAirport(bestOffersSearchDTO.getOriginAirport());
		restrictionsDTO.setToAirport(bestOffersSearchDTO.getDestinationAirport());
		Map<String, List<Integer>> cabinClassWiseFlightSegmentIds = new HashMap<String, List<Integer>>();
		cabinClassWiseFlightSegmentIds.put("Y", new ArrayList<Integer>());
		restrictionsDTO.setCabinClassSelection(cabinClassWiseFlightSegmentIds);
		restrictionsDTO.setChannelCode(Integer.valueOf(bestOffersSearchDTO.getChannelCode()));
		return restrictionsDTO;
	}

	private void quoteChargesPerFlight(Collection<BestOffersDTO> colBestOffers,
			HashMap<String, HashMap<String, QuotedChargeDTO>> charges,
			Map<String, Collection<String>> mapBookingClassCodeAndChargeGroupCodes) throws ModuleException {
		Iterator<BestOffersDTO> bestOffersIt = colBestOffers.iterator();
		while (bestOffersIt.hasNext()) {
			BestOffersDTO bestOffer = bestOffersIt.next();
			setApplicableChargesForFlight(bestOffer, charges, mapBookingClassCodeAndChargeGroupCodes);
			
			if (log.isDebugEnabled()) {
				log.debug(bestOffer.toString());
			}
			bestOffer.setTotalSurchargesAmount(populatePaxTaxSurcharge(ChargeGroups.SURCHARGE, PaxTypeTO.ADULT, bestOffer));
			bestOffer.setTotalTaxAmount(populatePaxTaxSurcharge(ChargeGroups.TAX, PaxTypeTO.ADULT, bestOffer));
			bestOffer.setTotalAmount(AccelAeroCalculator.add(bestOffer.getFareAmount(), bestOffer.getTotalTaxAmount(),
					bestOffer.getTotalSurchargesAmount()));
		}
	}

	private BigDecimal populatePaxTaxSurcharge(String ChargeGroup, String paxType, BestOffersDTO bestOffer)
			throws ModuleException {
		BigDecimal total = BigDecimal.ZERO;

		StringBuilder appliedChargesBuilder = new StringBuilder();
		if (bestOffer.getCharges() != null) {
			appliedChargesBuilder.append("[ ");
			for (QuotedChargeDTO quotedChargeDTO : bestOffer.getCharges()) {
				if (quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroup) == 0) {
					if (paxType.compareTo(PaxTypeTO.ADULT) == 0) {
						if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
								|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL) {
							/**
							 * Assume we have only total fare percentage
							 * Apply ChargeValueInPercentage
							 */
							if (quotedChargeDTO.isChargeValueInPercentage()) {
								setReCulatedPrecentageCharge(bestOffer, quotedChargeDTO, PaxTypeTO.ADULT);
							}
							BigDecimal amount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO
									.getEffectiveChargeAmount(paxType));
							total = AccelAeroCalculator.add(total, amount);
							appliedChargesBuilder.append(quotedChargeDTO.getChargeCode());
							appliedChargesBuilder.append(" = ");
							appliedChargesBuilder.append(amount);
							appliedChargesBuilder.append(" \n");
						}
					}
				}
			}
			appliedChargesBuilder.append("]");

		}

		if (log.isDebugEnabled()) {
			log.debug("======================Charge Group - " +ChargeGroup+" ==================");
			log.debug(appliedChargesBuilder.toString());
			log.debug("========================================================================");
		}
		return total;
	}
	
	/**
	 * As per old implementation,  best offers will not return correct best offer , if there are charges[TAX,SUR] in precentage
	 * This module should compatible with applicability of normal charge code[ChargeQuoteUtils],
	 * As per limitations and user requirements we will implement basic tax & surchagres bahaviour based on Adult fare. 
	 */
	
	/**
	 * Apply precentage charge for best offers
	 * @param bestOffer
	 * @param quotedChargeDTO
	 * @param paxType
	 */
	private static void setReCulatedPrecentageCharge(BestOffersDTO bestOffer, QuotedChargeDTO quotedChargeDTO,
			String paxType) {

		BigDecimal percentageApplicableAmount = bestOffer.getFareAmount();

		String chargeBasis = quotedChargeDTO.getChargeBasis();
		if (!quotedChargeDTO.getChargeGroupCode().equals(ChargeGroups.SURCHARGE)
				&& (chargeBasis.equals(ChargeRate.CHARGE_BASIS_PFS) || chargeBasis.equals(ChargeRate.CHARGE_BASIS_PTFS))) {
			percentageApplicableAmount = AccelAeroCalculator.add(percentageApplicableAmount,
					bestOffer.getTotalSurchargesAmount());
		}

		BigDecimal paxNewCharge = AccelAeroCalculator.multiply(percentageApplicableAmount, new BigDecimal(
				quotedChargeDTO.getChargeValuePercentage()));
		paxNewCharge = AccelAeroCalculator.divide(paxNewCharge, new BigDecimal(100));

		if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
			paxNewCharge = AccelAeroRounderPolicy.getRoundedValue(paxNewCharge, quotedChargeDTO.getBoundryValue(),
					quotedChargeDTO.getBreakPoint());
		}

		if (quotedChargeDTO.getChargeValueMin() != null
				&& quotedChargeDTO.getChargeValueMin().compareTo(paxNewCharge) > 0) {
			paxNewCharge = quotedChargeDTO.getChargeValueMin();
		} else if (quotedChargeDTO.getChargeValueMax() != null
				&& paxNewCharge.compareTo(quotedChargeDTO.getChargeValueMax()) > 0) {
			paxNewCharge = quotedChargeDTO.getChargeValueMax();
		}

		quotedChargeDTO.setEffectiveChargeAmount(paxType, paxNewCharge.doubleValue());
	}

	@SuppressWarnings("unchecked")
	private void setApplicableChargesForFlight(BestOffersDTO bestOffer,
			HashMap<String, HashMap<String, QuotedChargeDTO>> charges,
			Map<String, Collection<String>> mapBookingClassCodeAndChargeGroupCodes) throws ModuleException {

		HashMap ondCharges;

		Iterator<String> bookingCodeIterator = bestOffer.getBookingCodes().iterator();

		if (bestOffer.isConnectionWithSegmentFare()) {
			String[] airports = bestOffer.getSegmentCode().split("/");
			for (int i = 0; i < airports.length - 1; i++) {
				ondCharges = charges.get(airports[i] + "/" + airports[i + 1]);

				Collection chargeCollection = AirinventoryUtils.getSearializableValues(ondCharges);
				Date flightDepartureDate = bestOffer.getDepartureDateTimeLocal();
				String ondPosition = QuoteChargesCriteria.FIRST_AND_LAST_OND;
				boolean isQualifyForShortTransit = false;

				Collection applicableChargesForFlight = getApplicableChargesForFlight(chargeCollection, flightDepartureDate,
						ondPosition, isQualifyForShortTransit);

				String bookingCode = "";
				if (bookingCodeIterator.hasNext()) {
					bookingCode = bookingCodeIterator.next();
				}

				if (bestOffer.getCharges() == null) {
					bestOffer.setCharges(filterChargesForChargeGroups(
							getApplicableChargeGroups(bookingCode, mapBookingClassCodeAndChargeGroupCodes),
							applicableChargesForFlight));
				} else {
					bestOffer.getCharges().addAll(
							filterChargesForChargeGroups(
									getApplicableChargeGroups(bookingCode, mapBookingClassCodeAndChargeGroupCodes),
									applicableChargesForFlight));
				}
			}

		} else {

			ondCharges = charges.get(bestOffer.getSegmentCode());

			Collection chargeCollection = AirinventoryUtils.getSearializableValues(ondCharges);
			Date flightDepartureDate = bestOffer.getDepartureDateTimeLocal();
			String ondPosition = QuoteChargesCriteria.FIRST_AND_LAST_OND;
			boolean isQualifyForShortTransit = false;

			Collection applicableChargesForFlight = getApplicableChargesForFlight(chargeCollection, flightDepartureDate,
					ondPosition, isQualifyForShortTransit);

			String bookingCode = "";
			if (bookingCodeIterator.hasNext()) {
				bookingCode = bookingCodeIterator.next();
			}

			if (bestOffer.getCharges() == null) {
				bestOffer.setCharges(filterChargesForChargeGroups(
						getApplicableChargeGroups(bookingCode, mapBookingClassCodeAndChargeGroupCodes),
						applicableChargesForFlight));
			}
		}
	}

	/**
	 * Filter charges having specified charge groups. If no charge groups specified, no charges filtered and null is
	 * returned.
	 * 
	 * @param chargeGroups
	 * @param charges
	 * @return
	 */
	private Collection<QuotedChargeDTO> filterChargesForChargeGroups(Collection<String> chargeGroups,
			Collection<QuotedChargeDTO> charges) {
		List<QuotedChargeDTO> filteredCharges = null;
		if (charges != null && chargeGroups != null) {
			filteredCharges = new ArrayList<QuotedChargeDTO>();
			Iterator<QuotedChargeDTO> chargesIt = charges.iterator();
			while (chargesIt.hasNext()) {
				QuotedChargeDTO quotedCharge = chargesIt.next();
				if (chargeGroups.contains(quotedCharge.getChargeGroupCode())) {
					filteredCharges.add(quotedCharge.clone());
				}
			}
		}
		return filteredCharges;
	}

	private Collection<String> getApplicableChargeGroups(String bookingCode,
			Map<String, Collection<String>> mapBookingClassCodeAndChargeGroupCodes) {
		Collection<String> chargeGroups = null;

		if (bookingCode != null) {
			chargeGroups = mapBookingClassCodeAndChargeGroupCodes.get(bookingCode);

			if (chargeGroups == null) {
				chargeGroups = new ArrayList<String>();
				// set charge groups linked to booking class
				// chargeGroups.addAll(getBookingClassDAO().getBookingClassChargeGroups(bookingCode));
				// Get the bookingClass from best offers and assign chargeGroups according to that
				chargeGroups.add(ChargeGroups.SURCHARGE);
				chargeGroups.add(ChargeGroups.TAX);
				mapBookingClassCodeAndChargeGroupCodes.put(bookingCode, chargeGroups);
			}
		}

		return chargeGroups;
	}

	private Collection<QuotedChargeDTO> getApplicableChargesForFlight(Collection<QuotedChargeDTO> chargeCollection,
			Date flightDepartureDate, String ondPosition, boolean isQualifyForShortTransit) throws ModuleException {

		if (chargeCollection == null) {
			return chargeCollection;
		}

		Collection<QuotedChargeDTO> applicableChargesForFlight = new ArrayList<QuotedChargeDTO>();
		Iterator<QuotedChargeDTO> chargeIter = chargeCollection.iterator();
		while (chargeIter.hasNext()) {
			QuotedChargeDTO quotedChargeDTO = chargeIter.next();

			boolean isBetweenVarianceDates = (flightDepartureDate.compareTo(quotedChargeDTO.getEffectiveFromDate()) >= 0 && flightDepartureDate
					.compareTo(quotedChargeDTO.getEffectiveToDate()) <= 0) ? true : false;

			boolean isSegApplicable = quotedChargeDTO.getSegApplicable().equals(Charge.APPLY_TO_ALL_ONDs);
			if (ondPosition != null && !isSegApplicable) {
				if (ondPosition.equals(QuoteChargesCriteria.FIRST_AND_LAST_OND)) {
					isSegApplicable = (quotedChargeDTO.getSegApplicable().equals(Charge.APPLY_TO_FIRST_OND_ONLY) || quotedChargeDTO
							.getSegApplicable().equals(Charge.APPLY_TO_LAST_OND_ONLY)) ? true : false;
				} else if (ondPosition.equals(QuoteChargesCriteria.FIRST_OND)) {
					isSegApplicable = (quotedChargeDTO.getSegApplicable().equals(Charge.APPLY_TO_FIRST_OND_ONLY)) ? true : false;
				} else if (ondPosition.equals(QuoteChargesCriteria.LAST_OND)) {
					isSegApplicable = (quotedChargeDTO.getSegApplicable().equals(Charge.APPLY_TO_LAST_OND_ONLY)) ? true : false;
				} else {
					isSegApplicable = false; // inbetween ond cannot have last and first charges applied.
				}
			} else {
				isSegApplicable = true; // if charge is for all onds
			}

			boolean blnJourneyTypeMatch = false;
			if (isQualifyForShortTransit) {
				blnJourneyTypeMatch = (!quotedChargeDTO.getJourneyType().equals(Charge.JOURNEY_TYPE_EXCLUDE_SHORT_TRANSIT))
						? true
						: false;
			} else {
				blnJourneyTypeMatch = true;
			}

			if (isBetweenVarianceDates && isSegApplicable && blnJourneyTypeMatch) {
				applicableChargesForFlight.add(quotedChargeDTO);
			}
		}

		return applicableChargesForFlight;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void addToChargeQuoteCriteria(Collection colChargeQuoteCriteria, AvailableFlightSearchDTO restrictionsDTO,
			String ondCode, boolean isInboundJourney) {

		boolean hasInwardCxnFlight = false;
		boolean hasOutwardCxnFlight = false;
		Date fromDate = null;
		Date toDate = null;

		long inwardTransitDuration = 0;
		long outwardTransitDuration = 0;
		Collection ondTransitDurList = new ArrayList();

		if (!isInboundJourney) {
			fromDate = restrictionsDTO.getDepatureDateTimeStart();
			toDate = restrictionsDTO.getDepatureDateTimeEnd();
		} else {
			fromDate = restrictionsDTO.getReturnDateTimeStart();
			toDate = restrictionsDTO.getReturnDateTimeEnd();
		}

		String cabinClassCode = FareQuoteUtil.getClassOfserviceFromFlightSegId(restrictionsDTO.getCabinClassSelection(), null);

		colChargeQuoteCriteria.add(createChargeQuoteCriteriaForVariance(ondCode, restrictionsDTO.getPosAirport(), fromDate,
				toDate, AirinventoryCustomConstants.DEFAULT_CHARGE_GROUPS,
				AirinventoryCustomConstants.NO_CHARGE_CAT_CODE_RESTRICTION, null, null, false, restrictionsDTO.getAgentCode(),
				hasInwardCxnFlight, hasOutwardCxnFlight, null, inwardTransitDuration, outwardTransitDuration, ondTransitDurList,
				cabinClassCode, restrictionsDTO.getEffectiveLastFQDate(null), restrictionsDTO.getChannelCode()));
	}

	@SuppressWarnings("rawtypes")
	private QuoteChargesCriteria createChargeQuoteCriteriaForVariance(String ond, String posAirport, Date minDepartureDate,
			Date maxDepartureDate, String[] chargeGroupCodes, String[] chargeCatCodes, FareSummaryDTO fareSummaryDTO,
			String ondPosition, boolean isOndQualifyForShortTransit, String agentCode, boolean hasInwardCxnFlight,
			boolean hasOutwardCxnFlight, Map totalJourneyFare, long inwardTransitDuration, long outwardTransitDuration,
			Collection ondTransitDurList, String cabinClassCode, Date chargeQuoteDate, int salesChannelId) {

		QuoteChargesCriteria criterion = createChargeQuoteCriteria(ond, posAirport, null, chargeGroupCodes, chargeCatCodes,
				fareSummaryDTO, ondPosition, isOndQualifyForShortTransit, agentCode, hasInwardCxnFlight, hasOutwardCxnFlight,
				totalJourneyFare, inwardTransitDuration, outwardTransitDuration, ondTransitDurList, cabinClassCode,
				chargeQuoteDate);

		criterion.setMinDepartureDate(minDepartureDate);
		criterion.setMaxDepartureDate(maxDepartureDate);
		criterion.setVarianceQuote(true);
		criterion.setChannelId(salesChannelId);
		return criterion;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private QuoteChargesCriteria createChargeQuoteCriteria(String ond, String posAirport, Date depatureDate,
			String[] chargeGroupCodes, String[] chargeCatCodes, FareSummaryDTO fareSummaryDTO, String ondPosition,
			boolean isOndQualifyForShortTransit, String agentCode, boolean hasInwardCxnFlight, boolean hasOutwardCxnFlight,
			Map totalJourneyFare, long inwardTransitDuration, long outwardTransitDuration, Collection ondTransitDurList,
			String cabinClassCode, Date chargeQuoteDate) {
		QuoteChargesCriteria criterion = new QuoteChargesCriteria();
		criterion.setOnd(ond);
		criterion.setPosAirport(posAirport);
		criterion.setDepatureDate(depatureDate);
		criterion.setChargeGroups(chargeGroupCodes);
		criterion.setChargeCatCodes(chargeCatCodes);
		criterion.setFareSummaryDTO(fareSummaryDTO);
		criterion.setOndPosition(ondPosition);
		criterion.setQualifyForShortTransit(isOndQualifyForShortTransit);
		criterion.setAgentCode(agentCode);
		criterion.setHasInwardTransit(hasInwardCxnFlight);
		criterion.setHasOutwardTransit(hasOutwardCxnFlight);
		criterion.setTotalJourneyFare(totalJourneyFare);
		criterion.setInwardTransitGap(inwardTransitDuration);
		criterion.setOutwardTransitGap(outwardTransitDuration);
		criterion.setOndTransitDurList(ondTransitDurList);
		criterion.setCabinClassCode(cabinClassCode);
		criterion.setChargeQuoteDate(chargeQuoteDate);
		return criterion;
	}

	@SuppressWarnings({ "rawtypes" })
	private Collection<BestOffersDTO> getBestOffersForConnectionOnDs(String origin, String destination, String ondCode,
			List<TransitAirport> stopOverAirports, BestOffersSearchDTO bestOffersSearchDTO, int displayFareCnt)
			throws ModuleException {
		Collection<String[]> transitDurations = new ArrayList<String[]>();
		Collection<Object[]> connFlightsSegmentWise = new ArrayList<Object[]>();
		Collection<Object[]> singleFlightsSegmentWise = new ArrayList<Object[]>();
		boolean excludeSegFares = AppSysParamsUtil.isExcludeSegFareFromConAndRet();
		int displayFareCntConfig = AirinventoryUtils.getAirInventoryConfig().getBestOffersDisplayFareCount();

		if (stopOverAirports != null && stopOverAirports.size() > 0) {

			for (int segmentSequence = 0; segmentSequence <= stopOverAirports.size(); ++segmentSequence) {
				// minDate and maxDate
				String fromAirport = null;
				String toAirport = null;
				String segmentCode = null;

				if (segmentSequence == 0) {
					// First segment of the journey
					fromAirport = origin;
					toAirport = stopOverAirports.get(segmentSequence).getAirportCode();
					segmentCode = fromAirport + "/%" + toAirport;
					Object[] bestOffersConn = getBestOffersDAOJDBCImpl().getLimitedDynamicBestOffersForConnectedFlights(
							bestOffersSearchDTO, segmentCode, ondCode, 1, displayFareCntConfig);
					connFlightsSegmentWise.add(bestOffersConn);
				} else if (stopOverAirports.get(segmentSequence - 1).isOrigin()) {
					fromAirport = stopOverAirports.get(segmentSequence - 1).getAirportCode();
					if (segmentSequence == stopOverAirports.size()) {
						toAirport = destination;
					} else {
						toAirport = stopOverAirports.get(segmentSequence).getAirportCode();
					}
					try {
						String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
								fromAirport, null, null);
						transitDurations.add(minMaxTransitDurations);

						segmentCode = fromAirport + "/%" + toAirport;
						Object[] bestOffersConn = getConnectionFlightsSegmentWise(
								(BestOffersSearchDTO) bestOffersSearchDTO.clone(), segmentCode, ondCode, displayFareCntConfig,
								connFlightsSegmentWise, segmentSequence, minMaxTransitDurations);
						connFlightsSegmentWise.add(bestOffersConn);

					} catch (ModuleRuntimeException ex) {
						if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(ex.getExceptionCode())) {
							log.error("Transit duration not defined ");
						}
					}
				} else {
					continue;
				}

				if (!excludeSegFares) {
					Collection<String> ondCodeList = new ArrayList<String>();
					segmentCode = fromAirport + "/" + toAirport;
					ondCodeList.add(segmentCode);
					if (segmentSequence == 0) {
						Object[] bestOffersSegment = getBestOffersDAOJDBCImpl().getOverallDynamiCBestOffersForSingleFlights(
								bestOffersSearchDTO, ondCodeList, displayFareCntConfig);
						singleFlightsSegmentWise.add(bestOffersSegment);
					} else {
						if (transitDurations.size() > segmentSequence - 1) {
							Object[] bestOffersSegment = getSingleFlightsSegmentWise(
									(BestOffersSearchDTO) bestOffersSearchDTO.clone(), ondCodeList, displayFareCntConfig,
									singleFlightsSegmentWise, segmentSequence,
									(String[]) ((List) transitDurations).get(segmentSequence - 1));
							singleFlightsSegmentWise.add(bestOffersSegment);
						}
					}
				}
			}
		}

		Map<String, BestOffersDTO> bestOfferConnectionFares = getBestOffersConnectionFares(connFlightsSegmentWise,
				transitDurations, displayFareCntConfig, origin, destination);

		Map<String, BestOffersDTO> bestOfferSegmentFares = null;
		if (!excludeSegFares) {
			bestOfferSegmentFares = getBestOffersSegmentFares(singleFlightsSegmentWise, transitDurations, origin, destination,
					displayFareCntConfig);
		}

		Collection<BestOffersDTO> colRemovedFares = new ArrayList<BestOffersDTO>();

		boolean fareBiasingEnabled = AppSysParamsUtil.isFareBiasingEnabled();

		if (bestOfferConnectionFares != null && bestOfferSegmentFares != null) {
			Iterator<String> connFareIter = bestOfferConnectionFares.keySet().iterator();
			Set<String> segFareKeySet = bestOfferSegmentFares.keySet();
			while (connFareIter.hasNext()) {
				String uniqueKey = (String) connFareIter.next();
				if (segFareKeySet.contains(uniqueKey)) {
					BestOffersDTO connFare = (BestOffersDTO) bestOfferConnectionFares.get(uniqueKey);
					BestOffersDTO segFare = (BestOffersDTO) bestOfferSegmentFares.get(uniqueKey);
					if (fareBiasingEnabled || connFare.compareTo(segFare) < 0) {
						colRemovedFares.add(segFare);
					} else {
						colRemovedFares.add(connFare);
					}
				}
			}
		}

		List<BestOffersDTO> bestOffers = new ArrayList<BestOffersDTO>();
		bestOffers.addAll(bestOfferConnectionFares.values());
		if (bestOfferSegmentFares != null) {
			bestOffers.addAll(bestOfferSegmentFares.values());
		}
		bestOffers.removeAll(colRemovedFares);

		Collections.sort(bestOffers);

		if (bestOffers.size() >= displayFareCnt) {
			bestOffers = bestOffers.subList(0, displayFareCnt);
		}

		// Set rank
		int rank = 1;
		for (int i = 0; i < bestOffers.size(); i++) {
			((BestOffersDTO) bestOffers.get(i)).setRank(rank++);
		}

		return bestOffers;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map<String, BestOffersDTO> getBestOffersConnectionFares(Collection flightsSegmentWise, Collection transitDurations,
			int displayFareCnt, String origin, String destination) throws ModuleException {
		Collection colBestOffers = new ArrayList();
		HashMap<String, BestOffersDTO> bestOffersMap = new HashMap<String, BestOffersDTO>();

		if (flightsSegmentWise != null) {
			String[] airportNames = getAirportNames(origin, destination);
			for (int i = 0; i < flightsSegmentWise.size(); i++) {
				if (i + 1 >= flightsSegmentWise.size()) {
					break;
				}
				Object[] firstSegment = (Object[]) ((List) flightsSegmentWise).get(i);
				Object[] nextSegment = (Object[]) ((List) flightsSegmentWise).get(i + 1);
				String[] minMaxTransitDurations = (String[]) ((List) transitDurations).get(i);

				if (colBestOffers.size() == displayFareCnt) {
					break;
				}

				for (Object element : firstSegment) {
					FlightSegAvailableBCWithFareDTO flightSegDTOFirst = (FlightSegAvailableBCWithFareDTO) element;
					Date minDepartTime = this.getMinTransitTime(flightSegDTOFirst.getArrivalDateTimeLocal(),
							minMaxTransitDurations[0]);
					Date maxDepartTime = this.getMaxTransitTime(flightSegDTOFirst.getArrivalDateTimeLocal(),
							minMaxTransitDurations[1]);

					for (Object element2 : nextSegment) {
						FlightSegAvailableBCWithFareDTO flightSegDTONext = (FlightSegAvailableBCWithFareDTO) element2;
						Date actualDepartTime = flightSegDTONext.getDepartureDateTimeLocal();
						if ((minDepartTime.getTime() <= actualDepartTime.getTime())
								&& (actualDepartTime.getTime()) <= maxDepartTime.getTime()) {

							for (int j = 0; j < flightSegDTOFirst.getAvailableBCsInOrder().size(); j++) {
								String availableBCFirst = (String) ((List) flightSegDTOFirst.getAvailableBCsInOrder()).get(j);

								if (flightSegDTONext.getAvailableBCsInOrder().contains(availableBCFirst)) {
									BestOffersDTO bestOffersDTO = new BestOffersDTO();
									bestOffersDTO.setOriginAirport(origin);
									bestOffersDTO.setDestinationAirport(destination);
									bestOffersDTO.setOriginAirportName(airportNames[0]);
									bestOffersDTO.setDestinationAirportName(airportNames[1]);
									bestOffersDTO.setDepartureDateTimeLocal(flightSegDTOFirst.getDepartureDateTimeLocal());
									bestOffersDTO.getFlightNos().add(flightSegDTOFirst.getFlightNo());
									bestOffersDTO.getFlightNos().add(flightSegDTONext.getFlightNo());
									bestOffersDTO.setUniqueKey(flightSegDTOFirst.getFlightId() + ","
											+ flightSegDTONext.getFlightId());
									bestOffersDTO.setFareAmount(flightSegDTOFirst.getAvailableBCsWithFares()
											.get(availableBCFirst));
									bestOffersDTO.getBookingCodes().add(availableBCFirst);
									bestOffersDTO.setSegmentCode(createOndCode(flightSegDTOFirst.getSegmentCode(),
											flightSegDTONext.getSegmentCode()));
									bestOffersMap.put(bestOffersDTO.getUniqueKey(), bestOffersDTO);
									colBestOffers.add(bestOffersDTO);
									break;
								}
							}
						}

					}
				}

			}
		}
		return bestOffersMap;
	}

	private String createOndCode(String segmentCode, String segmentCode2) {
		String ondCode = null;
		int SINGLE_SEGMENT_CODE_LENGTH = 7;
		int AIRPORT_CODE_LENGTH = 3;
		if (segmentCode != null) {
			ondCode = segmentCode;
			if (segmentCode.length() == SINGLE_SEGMENT_CODE_LENGTH && segmentCode2 != null
					&& segmentCode2.length() == SINGLE_SEGMENT_CODE_LENGTH) {
				if (segmentCode.substring(segmentCode.length() - AIRPORT_CODE_LENGTH, segmentCode.length()).equals(
						segmentCode2.substring(0, AIRPORT_CODE_LENGTH))) {
					ondCode = segmentCode + "/"
							+ segmentCode2.substring(segmentCode.length() - AIRPORT_CODE_LENGTH, segmentCode.length());
				}
			}
		}
		return ondCode;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object[] getConnectionFlightsSegmentWise(BestOffersSearchDTO bestOffersSearchDTO, String segmentCode, String ondCode,
			int displayFareCnt, Collection flightsSegmentWise, int segmentSequence, String[] minMaxTransitDurations)
			throws ModuleException {
		Collection colBestOffers = new ArrayList();
		if (flightsSegmentWise != null) {
			Object[] firstSegment = (Object[]) ((List) flightsSegmentWise).get(segmentSequence - 1);
			for (Object element : firstSegment) {
				FlightSegAvailableBCWithFareDTO flightSegDTOFirst = (FlightSegAvailableBCWithFareDTO) element;
				Date minDepartTime = this.getMinTransitTime(flightSegDTOFirst.getArrivalDateTimeLocal(),
						minMaxTransitDurations[0]);
				Date maxDepartTime = this.getMaxTransitTime(flightSegDTOFirst.getArrivalDateTimeLocal(),
						minMaxTransitDurations[1]);
				bestOffersSearchDTO.setDateFrom(minDepartTime);
				bestOffersSearchDTO.setDateTo(maxDepartTime);

				Object[] bestOffersConn = getBestOffersDAOJDBCImpl().getDynamicBestOffersForConnectedFlights(bestOffersSearchDTO,
						segmentCode, ondCode);
				for (int i = 0; i < bestOffersConn.length; i++) {
					if (!colBestOffers.contains(bestOffersConn[i])) {
						colBestOffers.add(bestOffersConn[i]);
					}
				}
			}
		}
		return colBestOffers.toArray();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object[] getSingleFlightsSegmentWise(BestOffersSearchDTO bestOffersSearchDTO, Collection<String> colOndCodes,
			int displayFareCnt, Collection flightsSegmentWise, int segmentSequence, String[] minMaxTransitDurations)
			throws ModuleException {
		Collection colBestOffers = new ArrayList();
		if (flightsSegmentWise != null) {
			Object[] firstSegment = (Object[]) ((List) flightsSegmentWise).get(segmentSequence - 1);
			for (Object element : firstSegment) {
				BestOffersDTO bestOfferFirst = (BestOffersDTO) element;
				Date minDepartTime = this.getMinTransitTime(bestOfferFirst.getArrivalDateTimeLocal(), minMaxTransitDurations[0]);
				Date maxDepartTime = this.getMaxTransitTime(bestOfferFirst.getArrivalDateTimeLocal(), minMaxTransitDurations[1]);
				bestOffersSearchDTO.setDateFrom(minDepartTime);
				bestOffersSearchDTO.setDateTo(maxDepartTime);

				Object[] bestOffersSingle = getBestOffersDAOJDBCImpl().getOverallDynamiCBestOffersForSingleFlights(
						bestOffersSearchDTO, colOndCodes, displayFareCnt);
				for (int i = 0; i < bestOffersSingle.length; i++) {
					if (!colBestOffers.contains(bestOffersSingle[i])) {
						colBestOffers.add(bestOffersSingle[i]);
					}
				}
			}
		}
		return colBestOffers.toArray();
	}

	@SuppressWarnings({ "rawtypes" })
	private Map<String, BestOffersDTO> getBestOffersSegmentFares(Collection flightsSegmentWise, Collection transitDurations,
			String origin, String destination, int displayFareCnt) throws ModuleException {
		Collection<BestOffersDTO> colBestOffers = new ArrayList<BestOffersDTO>();
		HashMap<String, BestOffersDTO> bestOffersMap = new HashMap<String, BestOffersDTO>();

		if (flightsSegmentWise != null) {
			String[] airportNames = getAirportNames(origin, destination);
			for (int i = 0; i < flightsSegmentWise.size(); i++) {
				if (i + 1 >= flightsSegmentWise.size()) {
					break;
				}
				Object[] firstSegment = (Object[]) ((List) flightsSegmentWise).get(i);
				Object[] nextSegment = (Object[]) ((List) flightsSegmentWise).get(i + 1);
				String[] minMaxTransitDurations = (String[]) ((List) transitDurations).get(i);

				if (colBestOffers.size() == displayFareCnt) {
					break;
				}

				for (Object element : firstSegment) {
					BestOffersDTO bestOfferFirst = (BestOffersDTO) element;
					Date minDepartTime = this.getMinTransitTime(bestOfferFirst.getArrivalDateTimeLocal(),
							minMaxTransitDurations[0]);
					Date maxDepartTime = this.getMaxTransitTime(bestOfferFirst.getArrivalDateTimeLocal(),
							minMaxTransitDurations[1]);

					for (Object element2 : nextSegment) {
						BestOffersDTO bestOfferNext = (BestOffersDTO) element2;
						Date actualDepartTime = bestOfferNext.getDepartureDateTimeLocal();
						if ((minDepartTime.getTime() <= actualDepartTime.getTime())
								&& (actualDepartTime.getTime()) <= maxDepartTime.getTime()) {

							BestOffersDTO bestOffersDTO = new BestOffersDTO();
							bestOffersDTO.setOriginAirport(origin);
							bestOffersDTO.setDestinationAirport(destination);
							bestOffersDTO.setOriginAirportName(airportNames[0]);
							bestOffersDTO.setDestinationAirportName(airportNames[1]);
							bestOffersDTO.setDepartureDateTimeLocal(bestOfferFirst.getDepartureDateTimeLocal());
							bestOffersDTO.getFlightNos().addAll(bestOfferFirst.getFlightNos());
							bestOffersDTO.getFlightNos().addAll(bestOfferNext.getFlightNos());
							bestOffersDTO.setUniqueKey(bestOfferFirst.getUniqueKey() + "," + bestOfferNext.getUniqueKey());
							bestOffersDTO.setSegmentCode(createOndCode(bestOfferFirst.getSegmentCode(),
									bestOfferNext.getSegmentCode()));
							BigDecimal totalFare = AccelAeroCalculator.add(bestOfferFirst.getFareAmount(),
									bestOfferNext.getFareAmount());
							bestOffersDTO.setFareAmount(totalFare);
							bestOffersDTO.setConnectionWithSegmentFare(true);
							bestOffersDTO.getBookingCodes().addAll(bestOfferFirst.getBookingCodes());
							bestOffersDTO.getBookingCodes().addAll(bestOfferNext.getBookingCodes());
							bestOffersMap.put(bestOffersDTO.getUniqueKey(), bestOffersDTO);
							colBestOffers.add(bestOffersDTO);
						}
					}
				}

			}
		}
		return bestOffersMap;
	}

	private Collection<BestOffersDTO> getBestOffersFromCachedResults(BestOffersSearchDTO bestOffersSearchDTO)
			throws ModuleException {
		Collection<BestOffersDTO> colBestOffers = null;
		int cachingTimeInMinutes = AirinventoryUtils.getAirInventoryConfig().getBestOffersCachingTimeInMins();

		int cachedRequestId = getBestOffersDAOJDBCImpl().checkForCachedBestOffersRequest(bestOffersSearchDTO,
				cachingTimeInMinutes);
		if (cachedRequestId != -1) {
			colBestOffers = bestOffersDAO.getCashedResultsForBestOffers(cachedRequestId);
		}
		return colBestOffers;
	}

	private BestOffersDAOJDBC getBestOffersDAOJDBCImpl() {
		return (BestOffersDAOJDBC) AirInventoryUtil.getInstance().getLocalBean("bestOffersDAOJDBC");
	}

	@SuppressWarnings({ "rawtypes" })
	private void cacheBestOffersResults(BestOffersSearchDTO bestOffersSearchDTO, Collection colBestOffers) throws ModuleException {
		Integer nextVal = getBestOffersDAOJDBCImpl().getNextBestOffersRequestId();
		BestOffersRequest request = new BestOffersRequest();
		request.setBestOffersRequestId(nextVal);
		request.setCountryCode(bestOffersSearchDTO.getCountryCode());
		request.setSummaryFlag((bestOffersSearchDTO.isSummaryFlag() ? "Y" : "N"));
		request.setTotalQuoteFlag((bestOffersSearchDTO.isTotalQuoteFlag() ? "Y" : "N"));
		request.setOrigin(bestOffersSearchDTO.getOriginAirport());
		request.setDestination(bestOffersSearchDTO.getDestinationAirport());
		request.setMonthOfTravel(bestOffersSearchDTO.getMonthOfTravel());
		request.setLastRequestTime(new Date(System.currentTimeMillis()));
		if (colBestOffers != null && !colBestOffers.isEmpty()) {
			Iterator iterOffers = colBestOffers.iterator();
			while (iterOffers.hasNext()) {
				BestOffersDTO bestOffersDto = (BestOffersDTO) iterOffers.next();
				if (bestOffersDto != null) {
					BestOffersResponse response = new BestOffersResponse();
					response.setOrigin(bestOffersDto.getOriginAirport());
					response.setDestination(bestOffersDto.getDestinationAirport());
					response.setOriginAirportName(bestOffersDto.getOriginAirportName());
					response.setDestinationAirportName(bestOffersDto.getDestinationAirportName());
					response.setDepartureDateTimeLocal(bestOffersDto.getDepartureDateTimeLocal());
					response.setFareAmount(bestOffersDto.getFareAmount());
					response.setTotalTax(bestOffersDto.getTotalTaxAmount());
					response.setTotalSurcharges(bestOffersDto.getTotalSurchargesAmount());
					response.setTotalAmount(bestOffersDto.getTotalAmount());

					String flightNos = StringUtils.join(bestOffersDto.getFlightNos().iterator(), ",");

					response.setFlightNos(flightNos);
					response.setRank(bestOffersDto.getRank());
					request.addBestOffersResponse(response);
				}
			}
		}
		bestOffersDAO.saveBestOffersDataForCaching(request);
	}

	/**
	 * method to get minimum transit time
	 * 
	 * @param date
	 * @param minTransitTimeValue
	 * @return min transit date
	 */
	private Date getMinTransitTime(Date date, String minTrValueStr) {

		String hours = minTrValueStr.substring(0, minTrValueStr.indexOf(":"));
		String mins = minTrValueStr.substring(minTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	/**
	 * method to get maximum transit time
	 * 
	 * @param date
	 * @param maxTransitTimeValue
	 * @return max transit date
	 */
	private Date getMaxTransitTime(Date date, String maxTrValueStr) {

		String hours = maxTrValueStr.substring(0, maxTrValueStr.indexOf(":"));
		String mins = maxTrValueStr.substring(maxTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	public void removeExpiredBestOffersData() throws ModuleException {
		int expireTimeInMins = AirinventoryUtils.getAirInventoryConfig().getBestOffersCachingTimeInMins();
		bestOffersDAO.removeExpiredBestOffersData(expireTimeInMins);
	}

	private String[] getAirportNames(String origin, String destination) throws ModuleException {
		Collection<String> colAirportCodes = new ArrayList<String>();
		colAirportCodes.add(origin);
		colAirportCodes.add(destination);
		Map<String, Airport> airportMap = airportDAO.getAirports(colAirportCodes);

		String originAirportName = "";
		String destinationAirportName = "";
		if (airportMap != null) {
			if (airportMap.containsKey(origin)) {
				originAirportName = (airportMap.get(origin)).getAirportName();
			}
			if (airportMap.containsKey(destination)) {
				destinationAirportName = (airportMap.get(destination)).getAirportName();
			}
		}
		String[] airportNames = { originAirportName, destinationAirportName };
		return airportNames;

	}

	private AirportDAO lookupAirportDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AirportDAO) lookupService.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
	}

	@SuppressWarnings("unused")
	private class BestOffersDTOComparator implements Comparator<BestOffersDTO> {
		@Override
		public int compare(BestOffersDTO o1, BestOffersDTO o2) {
			return (new Integer(o1.getRank()).compareTo(new Integer(o2.getRank())));
		}
	}

	private ChargeBD getChargeBD() {
		return AirInventoryModuleUtils.getChargeBD();
	}
}
