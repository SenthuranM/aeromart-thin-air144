package com.isa.thinair.airinventory.core.persistence.jdbc.ps;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import org.springframework.jdbc.core.PreparedStatementCreator;

import com.isa.thinair.airinventory.api.dto.RetriveFlightInventoryCritera;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;

public class RetriveFlightInventoriesPrepStmtCreator implements PreparedStatementCreator {
	private RetriveFlightInventoryCritera _criteria;

	private String _sqlQuery;

	public RetriveFlightInventoriesPrepStmtCreator(String sqlQuery, RetriveFlightInventoryCritera criteria) {
		this._criteria = criteria;
		this._sqlQuery = sqlQuery;
	}

	public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(this._sqlQuery);
		int index = 0;
		Iterator<Integer> flightIdsIt = _criteria.getFlightIds().iterator();
		while (flightIdsIt.hasNext()) {
			ps.setInt(++index, ((Integer) flightIdsIt.next()).intValue());
		}
		ps.setString(++index, _criteria.getCabinClassCode());
		ps.setString(++index, _criteria.isExcludeInvalidSegmentInventories() ? FCCSegInventory.VALID_FLAG_Y : "%");
		// ps.setString(++index, BookingClass.PAXTYPE_GOSHOW);
		// ps.setString(++index, Fare.STATUS_ACTIVE);
		return ps;
	}

}
