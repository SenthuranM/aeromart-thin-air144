package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.ArrayList;

import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.model.InvTempCabinAlloc;
import com.isa.thinair.airinventory.api.model.InventoryTemplate;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.commons.api.dto.Page;

/**
 * @author priyantha
 */
public interface InventoryTemplateDAO {

	public InventoryTemplate saveInventoryTemplate(InvTempDTO invTempDTO);

	public Page<InvTempDTO> getInvTempBasicDataPage(int start, int pageSize, String aircraftModel);

	public InventoryTemplate getExistingInventoryTemplate(int invTempID);

	public void updateExistingInvTemplateCabinAlloc(InvTempCabinAlloc invTempCabinAlloc);

	public ArrayList<LogicalCabinClass> getLogicalCabinClasses(String cabinClass);

	public void updateInvTempStatus(InvTempDTO invTempDTO);

	public ArrayList<InventoryTemplate> getExistingTemplateList(String airCraftModel, String segment);

	public ArrayList<InvTempDTO> getInventoryTemplateForAirCraftModel(String airCraftModelName);

}