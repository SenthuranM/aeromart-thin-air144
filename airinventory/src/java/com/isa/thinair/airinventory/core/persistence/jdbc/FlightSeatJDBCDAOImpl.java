/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightSeatJDBCDAO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;

/**
 * @author indika
 * 
 */
public class FlightSeatJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements FlightSeatJDBCDAO {

	@SuppressWarnings("unchecked")
	public Collection<Integer> getFlightSeatsToRelease(Date seatReleaseStartDate, Date seatReleaseEndDate) throws ModuleException {

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Object params[] = { BeanUtils.getTimestamp(seatReleaseStartDate), BeanUtils.getTimestamp(seatReleaseEndDate),
				AirinventoryCustomConstants.FlightSeatStatuses.BLOCKED };
		final String query = "SELECT A.FLIGHT_AM_SEAT_ID  FROM SM_T_FLIGHT_AM_SEAT A, T_FLIGHT_SEGMENT B WHERE "
				+ " (A.BLOCKED_TIME BETWEEN ? AND ? ) AND A.STATUS = ?  AND A.FLT_SEG_ID = B.FLT_SEG_ID and B.EST_TIME_DEPARTURE_ZULU > SYSDATE";

		return (Collection<Integer>) template.query(query, params,

		new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colFlightSeatIDs = new ArrayList<Integer>();

				while (rs.next()) {

					Integer flightSeatID = new Integer(rs.getInt("FLIGHT_AM_SEAT_ID"));
					colFlightSeatIDs.add(flightSeatID);
				}
				return colFlightSeatIDs;
			}

		});
	}

}
