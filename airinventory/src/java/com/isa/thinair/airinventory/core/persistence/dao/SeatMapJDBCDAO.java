/**
 * 
 */
package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author indika byorn
 */
public interface SeatMapJDBCDAO {

	/**
	 * 
	 * @param flightSegID
	 *            : The flight segment Id.
	 * @param cabinClass
	 * @param logicalCabinClass
	 * @param loadSoicalIdentity
	 * @param localCode
	 *            : The locale code of the user. TODO - when there's a
	 * @return
	 * @throws ModuleException
	 */
	public FlightSeatsDTO getFlightSeats(int flightSegID, String localeCode, String cabinClass, String logicalCabinClass, boolean loadSoicalIdentity)
			throws ModuleException;

	public FlightSeatsDTO getFlightSeatsRow(int flightSegID, String localeCode, int rowid) throws ModuleException;

	/**
	 * Method Will Return collection of FlightSeatsDTO which matched with given flight segment id and seatcodes
	 */
	public FlightSeatsDTO getFlightSeatsWithCodes(int flightSegID, Collection<String> SeatCodeCollection) throws ModuleException;

	public Collection<FlightSeatsDTO> getTempleateIds(int flightId) throws ModuleException;

	public Collection<SeatDTO> getFlightSeats(int flightSegId, Collection<String> seatCodes) throws ModuleException;

	public boolean hasParentInRowGroupAndColumn(int rowGroup, int colGroup, Integer flightSegId, int flightAmSeatID,
			Collection<Integer> excludeCheckSeatIDs)
			throws ModuleException;

	public Integer getTempleateId(int flightSegId) throws ModuleException;

	public FlightSeatsDTO getFlightSeatsForCheck(Collection<Integer> flightAmSeatIds) throws ModuleException;

	public Map<String, String> getSeatStatus(Integer flightSegIds, String localeCode) throws ModuleException;

	/**
	 * Retrieves Seat information from the Seat charge template directly. No associated flights are necessary.
	 * 
	 * @param chargeTemplateID
	 *            Seat charge template ID of the seats to be retrieved.
	 * @return The seats attached to the seat charge template. Or an empty collection if the chargeTemplateID is
	 *         invalid.
	 * @throws ModuleException
	 *             If any of the underlying data access operations fail.
	 */
	public Collection<SeatDTO> getFlightSeatsByChargeTemplate(int chargeTemplateID) throws ModuleException;

}
