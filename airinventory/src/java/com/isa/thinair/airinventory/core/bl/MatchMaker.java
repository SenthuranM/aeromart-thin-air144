package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
class MatchMaker {
	/**
	 * 
	 */
	private SeatAvailabilityBL seatAvailBL;
	private JourneyType journeyType;
	private boolean valid = true;
	private int combinationSequence = 1;

	MatchMaker(SeatAvailabilityBL seatAvailabilityBL, JourneyType journeyType) {
		seatAvailBL = seatAvailabilityBL;
		this.journeyType = journeyType;
	}

	private List<MatchingCombination> mcList = new ArrayList<MatchingCombination>();
	private int i = 0;

	public boolean processOndFlights(List<AvailableIBOBFlightSegment> fltSegments) {
		if (valid) {
			if (i == 0) {
				for (AvailableIBOBFlightSegment fltSegment : fltSegments) {
					mcList.add(addMatchingCombination(fltSegment));
				}
			} else {
				Iterator<MatchingCombination> mcItr = mcList.iterator();
				List<MatchingCombination> tmpList = new ArrayList<MatchingCombination>();
				while (mcItr.hasNext()) {
					MatchingCombination mc = mcItr.next();
					for (AvailableIBOBFlightSegment fltSegment : fltSegments) {
						MatchingCombination newCmb = mc.clone(combinationSequence++);
						newCmb.addFlightSegment(fltSegment);
						if (newCmb.isValid()) {
							tmpList.add(newCmb);
						}
					}
				}
				mcList = tmpList;
			}

			i++;
			valid = mcList.size() > 0;
		}
		if (!valid) {
			mcList.clear();
		}
		return valid;
	}

	public boolean isValid() {
		return valid;
	}

	private MatchingCombination addMatchingCombination(AvailableIBOBFlightSegment fltSegment) {
		MatchingCombination mc = new MatchingCombination(seatAvailBL, journeyType, fltSegment, combinationSequence++);
		return mc;
	}

	public List<MatchingCombination> getMatchingCombinationList() {
		return mcList;
	}

	public MatchingCombination getMatchingCombinationFor(String combinationKey) {
		if (combinationKey != null) {
			for (MatchingCombination mc : getMatchingCombinationList()) {
				if (combinationKey.equals(mc.getCombinationKey())) {
					return mc;
				}
			}
		}
		return null;
	}
}