package com.isa.thinair.airinventory.core.util;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.TransitInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

public class ChargeQuoteCriteriaUtil {

	public static String getONDPosition(boolean isFirstOnd, boolean isLastOnd, String localOndPosition) {
		String ondPosition = null;
		ondPosition = localOndPosition;
		if (isFirstOnd && isLastOnd) {
			ondPosition = (localOndPosition == null) ? QuoteChargesCriteria.FIRST_AND_LAST_OND : localOndPosition;
		} else if (isFirstOnd) {
			if (localOndPosition == null) {
				ondPosition = QuoteChargesCriteria.FIRST_OND;
			} else if (!localOndPosition.equals(QuoteChargesCriteria.FIRST_OND)) {
				ondPosition = QuoteChargesCriteria.INBETWEEN_OND;
			}
		} else if (isLastOnd) {
			if (localOndPosition == null) {
				ondPosition = QuoteChargesCriteria.LAST_OND;
			} else if (!localOndPosition.equals(QuoteChargesCriteria.LAST_OND)) {
				ondPosition = QuoteChargesCriteria.INBETWEEN_OND;
			}
		} else {
			ondPosition = QuoteChargesCriteria.INBETWEEN_OND;
		}
		return ondPosition;
	}

	public static TransitInfoDTO getTransitInfoDTO(Collection<? extends IFlightSegment> quotedFltSegmentDTOs,
			IFlightSegment connectingFltSegment) {

		TransitInfoDTO transitInfoDTO = new TransitInfoDTO();

		if (quotedFltSegmentDTOs != null && quotedFltSegmentDTOs.size() > 0) { // Connection
			Date previousSegmentArrivalTimeZulu = null;
			for (IFlightSegment fltSegment : quotedFltSegmentDTOs) {
				long transitGap = -1;
				if (previousSegmentArrivalTimeZulu == null && connectingFltSegment != null) {
					Set<String> hubAirports = CommonsServices.getGlobalConfig().getHubAirports();
					String currentStartAirport = SegmentUtil.getFromAirport(fltSegment.getSegmentCode());
					String connectingEndAirport = SegmentUtil.getToAirport(connectingFltSegment.getSegmentCode());
					// if connecting segment make a connection with the current segment, and aipport is a hub
					if (currentStartAirport.equals(connectingEndAirport) && hubAirports.contains(currentStartAirport)) {
						transitGap = fltSegment.getDepartureDateTimeZulu().getTime()
								- connectingFltSegment.getArrivalDateTimeZulu().getTime();

					}

				} else if (previousSegmentArrivalTimeZulu != null) {
					transitGap = fltSegment.getDepartureDateTimeZulu().getTime() - previousSegmentArrivalTimeZulu.getTime();
				}
				if (transitGap > 0) {
					transitInfoDTO.addTransit(fltSegment.getDepartureDateTimeZulu(),
							SegmentUtil.getFromAirport(fltSegment.getSegmentCode()), transitGap);
					if (transitGap < AppSysParamsUtil.getTransitTimeThresholdInMillis()) {
						transitInfoDTO.setQualifyForShortTransit(fltSegment.getDepartureDateTimeZulu(), true);
					}
				}
				previousSegmentArrivalTimeZulu = fltSegment.getArrivalDateTimeZulu();
			}
			/*
			 * In own airline reservation modification , new adding segments should be check whether satisfy inward or
			 * outward.Here it is not checked.
			 */

			/*
			 * Reservation modification (Modify Segment or Add Segment)
			 */
		}

		return transitInfoDTO;
	}

}
