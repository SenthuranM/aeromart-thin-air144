package com.isa.thinair.airinventory.core.util;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.model.FlightSeat;

/**
 * @author Byorn.
 * 
 */
public class TODataExtractor {

	public static Collection<Integer> getFlightAmSeatIds(Collection<FlightSeat> flightSeats) {
		Collection<Integer> s = new ArrayList<Integer>();
		for (FlightSeat flightSeat : flightSeats) {
			s.add(flightSeat.getFlightAmSeatId());
		}
		return s;
	}

	public static SeatDTO find(int flightAmSeatId, Collection<SeatDTO> seats) {

		for (SeatDTO seatDTO : seats) {
			if (seatDTO.getFlightAmSeatID() == flightAmSeatId) {
				return seatDTO;
			}
		}
		return null;
	}

}
