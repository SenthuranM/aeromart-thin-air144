package com.isa.thinair.airinventory.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airinventory.api.dto.inventory.LogicalCabinClassDTO;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface LogicalCabinClassDAO {
	public Page<LogicalCabinClass> getLogicalCabinClass(LogicalCabinClassDTO searchParams, int startRec, int noRecs);

	public void saveTemplate(LogicalCabinClass logicalCabinClass, String chkNestShift) throws ModuleException;

	public int deleteLogicalCabinClass(LogicalCabinClass logicalCabinClass) throws ModuleException;

	public String getDefaultLogicalCabinClass(String cabinClass) throws ModuleException;

	public LogicalCabinClass getLogicalCabinClass(String logicalCCCode) throws ModuleException;

	public String getCabinClass(String logicalCCCode) throws ModuleException;

	public List<LogicalCabinClass> getAllLogicalCabinClasses() throws ModuleException;

	public List<LogicalCabinClass> getLogicalCabinClasses(String cabinClass) throws ModuleException;
}
