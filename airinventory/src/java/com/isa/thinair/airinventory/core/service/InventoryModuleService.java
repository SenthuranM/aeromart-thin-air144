/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airinventory.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * 
 *         Security Module's service interface
 * @isa.module.service-interface module-name="airinventory" description="inventory module"
 */
public class InventoryModuleService extends DefaultModule {
}
