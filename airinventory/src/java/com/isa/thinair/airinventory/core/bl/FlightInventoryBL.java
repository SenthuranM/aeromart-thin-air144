/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.bl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.summingInt;
import static java.util.stream.Collectors.groupingBy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAutomaticCheckins;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedAutoCheckin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedBaggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedFlexi;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedInsurance;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedMeal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.airinventory.api.dto.BCONDFaresAgentsSummaryDTO;
import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCBCOnewayReturnPaxCountDTO;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegONDCodesAndBookingClassesDTO;
import com.isa.thinair.airinventory.api.dto.FLCCAllocationDTO;
import com.isa.thinair.airinventory.api.dto.FlightInvRollResultsSummaryDTO;
import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FlightSummaryDTO;
import com.isa.thinair.airinventory.api.dto.InterceptingFlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.InvDowngradeDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollforwardStatusDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.dto.UpdateFccSegBCInvResponse;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.airinventory.api.model.InterceptingFCCSegAlloc;
import com.isa.thinair.airinventory.api.model.PublishAvailability;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.InventoryDiffForAVS;
import com.isa.thinair.airinventory.api.util.InventoryEventPublisher;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.audit.AuditExternalDataProvider;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.log.FlightInventoryBLLogger;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.LogicalCabinClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatMapDAO;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightMealNotifyStatusDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.constants.PlatformConstants;

/**
 * Bussiness logic related to the Inventory Master data maintenance
 * 
 * @author Thejaka
 * @author Nasly
 */
public class FlightInventoryBL {
	private final Log log = LogFactory.getLog(getClass());
	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	public FlightInventoryBL() {
		String maxAttempts = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_ATTEMPTS);
		if (maxAttempts != null && !maxAttempts.trim().equalsIgnoreCase("")) {
			Integer.parseInt(maxAttempts);
		}

	}

	/**
	 * Create FCCInventory and FCCSegInventory for a given flight Update intercepting inventory table
	 * 
	 * @param createFlightInventoryRQ
	 *            parameters for creating flight inventory
	 * @param userId
	 */
	public void createFlightInventory(CreateFlightInventoryRQ createFlightInventoryRQ, String userId) throws ModuleException {

		Set<AircraftCabinCapacity> aircraftCCs = createFlightInventoryRQ.getAircraftModel().getAircraftCabinCapacitys();

		if (createFlightInventoryRQ.getCcSegmentWiseInvTemplate() != null
				&& createFlightInventoryRQ.getCcSegmentWiseInvTemplate().size() > 0) {

			for (AircraftCabinCapacity aircraftCabinCapacity : aircraftCCs) {
				String cabinClassCode = aircraftCabinCapacity.getCabinClassCode();
				createFCCInventoryFromTemplate(cabinClassCode,
						createFlightInventoryRQ.getCcSegmentWiseInvTemplate().get(cabinClassCode), createFlightInventoryRQ,
						aircraftCabinCapacity.getSeatCapacity(), aircraftCabinCapacity.getInfantCapacity(), userId);
			}

		} else {
			for (AircraftCabinCapacity aircraftCabinCapacity : aircraftCCs) {
				String defaultLogicalCabinClass = createFlightInventoryRQ.getDefaultLogicalCCs().get(
						aircraftCabinCapacity.getCabinClassCode());
				if (defaultLogicalCabinClass != null) {
					createFCCInventory(aircraftCabinCapacity, createFlightInventoryRQ, defaultLogicalCabinClass);
				} else {
					throw new ModuleException("airinventory.arg.model.flightInv.failed", AirinventoryConstants.MODULE_NAME);
				}
			}
		}
	}

	/**
	 * @param tragetFlightIds
	 * @param cabinClass
	 * @param sourceFlightLccInventory
	 * @param fccSegInventoryAuditDTO
	 * @throws ModuleException
	 */
	private void rollForwardFCCSegInventory(Collection<Integer> tragetFlightIds, String cabinClass,
			Collection<FLCCAllocationDTO> sourceFlightLccInventory, int splitOption,
			Map<String, StringBuffer> fltInventoryErrorMap) throws ModuleException {

		for (Integer tragetFlightId : tragetFlightIds) {
			createFCCSegInventory(sourceFlightLccInventory, tragetFlightId, cabinClass, splitOption, fltInventoryErrorMap);
		}
	}

	/**
	 * @param flccAllocationDTOs
	 * @param flightId
	 * @param cabinClass
	 * @param fccSegInventoryAuditDTO
	 * @param flightNo
	 *            TODO
	 * @throws ModuleException
	 */
	public void createFCCSegInventory(Collection<FLCCAllocationDTO> flccAllocationDTOs, Integer flightId, String cabinClass,
			int splitOption, Map<String, StringBuffer> fltInventoryErrorMap) throws ModuleException {
		Collection<FCCSegInventory> fccSegInventories = getFlightInventoryDAO().getFCCSegInventories(flightId, cabinClass);
		Map<String, List<FCCSegInventory>> segmentWiseInventoryMap = getSegmentWiseSegmentInventory(fccSegInventories);
		boolean hasInterceptingSegments = AirinventoryUtils.hasInterceptingSegments(fccSegInventories);
		Map<String, Map<String, int[]>> segmentAndLogicalCabinClassWiseSoldSeatCount = null;

		Collection<String> newFccSegInveLCCs = new ArrayList<String>();

		if (hasInterceptingSegments || !AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
			Collection<String> singleLegSegmentCodes = AirinventoryUtils
					.buildSingleLegSegmentCodes(new ArrayList<String>(segmentWiseInventoryMap.keySet()));
			segmentAndLogicalCabinClassWiseSoldSeatCount = getFlightInventoryJDBCDAO()
					.getTotalSoldSeatCountSegmentAndLogicalCabinClassWise(flightId, singleLegSegmentCodes, cabinClass);
		}

		for (String flightSegmentCode : segmentWiseInventoryMap.keySet()) {
			Collection<FCCSegInventory> updatedFccSegInventories = new ArrayList<FCCSegInventory>();
			int flightSegmentId = -1;
			List<FCCSegInventory> segInventories = segmentWiseInventoryMap.get(flightSegmentCode);
			boolean changeFlightSeats = false;
			boolean updateSegAllocs = true;
			List<FlightSeat> flightSeatData = null;
			if (segInventories != null && !segInventories.isEmpty()) {
				flightSeatData = AirInventoryModuleUtils.getAircraftBD()
						.getFlightSeatData(segInventories.get(0).getFlightSegId());
				if (flightSeatData != null && flightSeatData.size() > 0) {
					changeFlightSeats = true;
				}
			}
			List<Integer> inventriesToDelete = new ArrayList<Integer>();
			for (FLCCAllocationDTO flccAllocationDTO : flccAllocationDTOs) {
				boolean found = false;
				for (FCCSegInventory fccSegInventory : segInventories) {
					if (fccSegInventory.getLogicalCCCode().equals(flccAllocationDTO.getLogicalCabinClassCode())) {
						found = true;
						if (flccAllocationDTO.getAdultAllocation() == 0 && fccSegInventory.getSoldAndOnholdAdultSeats() == 0) {
							// Keep details to remove the segment allocation inventory and its booking classes
							inventriesToDelete.add(fccSegInventory.getFccsInvId());
							break;
						}

						flightSegmentId = fccSegInventory.getFlightSegId();
						if (flccAllocationDTO.getAdultAllocation() != fccSegInventory.getAllocatedSeats()
								|| flccAllocationDTO.getInfantAllocation() != fccSegInventory.getInfantAllocation()) {

							int[] soldAdultInfantSeatCount = new int[] { fccSegInventory.getSoldAndOnholdAdultSeats(),
									fccSegInventory.getSoldAndOnholdInfantSeats() };
							if (hasInterceptingSegments) {
								soldAdultInfantSeatCount = AirinventoryUtils.getMaxSoldSeatcCountOnFlightSegment(
										segmentAndLogicalCabinClassWiseSoldSeatCount, fccSegInventory.getSegmentCode(),
										fccSegInventory.getLogicalCCCode());
							}
							updateFccSegmentInventoryAvailability(fccSegInventory, flccAllocationDTO, soldAdultInfantSeatCount,
									hasInterceptingSegments);
							updatedFccSegInventories.add(fccSegInventory);
						}
						break;
					}
				}
				if (!found && flccAllocationDTO.getAdultAllocation() > 0) {
					FCCSegInventory fccSegInventory = segInventories.iterator().next();
					FCCSegInventory newFccSegInventory = AirinventoryUtils.createZeroFccSegAllocInventory();
					newFccSegInventory.setFccInvId(fccSegInventory.getFccInvId());
					newFccSegInventory.setFlightId(fccSegInventory.getFlightId());
					newFccSegInventory.setLogicalCCCode(flccAllocationDTO.getLogicalCabinClassCode());
					newFccSegInventory.setSegmentCode(fccSegInventory.getSegmentCode());
					newFccSegInventory.setFlightSegId(fccSegInventory.getFlightSegId());
					newFccSegInventory.setValidFlag(fccSegInventory.getValidFlag());
					newFccSegInventory.setOverlappingFlightId(fccSegInventory.getOverlappingFlightId());

					boolean interceptingSegment = AirinventoryUtils.isInterceptingSegment(fccSegInventories, newFccSegInventory);
					int[] soldAdultInfantSeatCount = new int[] { 0, 0 };
					if (interceptingSegment || !AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
						soldAdultInfantSeatCount = AirinventoryUtils.getMaxSoldSeatcCountOnFlightSegment(
								segmentAndLogicalCabinClassWiseSoldSeatCount, newFccSegInventory.getSegmentCode(),
								newFccSegInventory.getLogicalCCCode());
					}
					updateFccSegmentInventoryAvailability(newFccSegInventory, flccAllocationDTO, soldAdultInfantSeatCount,
							hasInterceptingSegments);
					updatedFccSegInventories.add(newFccSegInventory);
					if (!newFccSegInveLCCs.contains(flccAllocationDTO.getLogicalCabinClassCode())) {
						newFccSegInveLCCs.add(flccAllocationDTO.getLogicalCabinClassCode());
					}
				}
			}

			// Remove logical cabin class allocation(s) from target flight of those not found in found in source flight
			// & without any sold or onhold bookings
			for (FCCSegInventory fccSegInventory : segInventories) {
				boolean found = false;
				for (FLCCAllocationDTO flccAllocationDTO : flccAllocationDTOs) {
					if (fccSegInventory.getLogicalCCCode().equals(flccAllocationDTO.getLogicalCabinClassCode())) {
						found = true;
					}
				}

				if (!found && (fccSegInventory.getSoldAndOnholdAdultSeats() == 0)
						&& !inventriesToDelete.contains(fccSegInventory.getFccsInvId())) {
					inventriesToDelete.add(fccSegInventory.getFccsInvId());
				} else if (!found && (fccSegInventory.getSoldAndOnholdAdultSeats() != 0)) {
					ModuleException moduleException = new ModuleException("airinventory.logic.bl.cannot.delete.lcc.inventory",
							AirinventoryConstants.MODULE_NAME);
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMMdd");
					List<Integer> flightIds = new ArrayList<Integer>();
					flightIds.add(flightId);
					String flightDptDate = AirInventoryModuleUtils.getFlightBD().getFlightDates(flightIds, 1, dateFormat);
					List<String> params = new ArrayList<String>();
					params.add(flightDptDate);
					params.add(fccSegInventory.getLogicalCCCode());
					params.add(Integer.toString(fccSegInventory.getSoldAndOnholdAdultSeats()));
					moduleException.setMessageParameters(params);
					throw moduleException;
				}
			}

			if (changeFlightSeats) {
				int startCount = 0;
				int endCount = 0;
				Collections.sort((List<FLCCAllocationDTO>) flccAllocationDTOs);
				Collections.reverse((List<FLCCAllocationDTO>) flccAllocationDTOs);
				FLCCLOOP: for (FLCCAllocationDTO flccAllocationDTO : flccAllocationDTOs) {
					if (AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
						endCount += flccAllocationDTO.getAdultAllocation();
					} else {
						// If logical cabin class inv restriction disabled, each logical cabin class
						// has aircraft capacity
						endCount = flccAllocationDTO.getAdultAllocation();
					}

					for (int i = startCount; i < endCount; i++) {
						if (flightSeatData.size() >= i) {
							FlightSeat fltSeat = flightSeatData.get(0);
							log.warn("FLIGHT SEAT MODEL having less seats than segment allocations SEG_ID: "
									+ fltSeat.getFlightSegId() + ", SEG_INV_ID:" + fltSeat.getFlightSegInventoryId());
							break FLCCLOOP;
						}
						if (!flightSeatData.get(i).getLogicalCCCode().equals(flccAllocationDTO.getLogicalCabinClassCode())) {
							flightSeatData.get(i).setUpdateToADifLogicalCC(true);
						}
						flightSeatData.get(i).setLogicalCCCode(flccAllocationDTO.getLogicalCabinClassCode());
					}

					if (AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
						startCount = endCount;
					} else {
						startCount = 0;
					}
				}

				boolean canOverrideSeatAllocs = true;
				int conflictingSeatCount = 0;
				StringBuilder conflictingSeats = new StringBuilder();
				for (FlightSeat flightSeat : flightSeatData) {
					if (flightSeat.getStatus() != null
							&& (flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.RESERVED)
									|| flightSeat.getStatus().equals(AirinventoryCustomConstants.FlightSeatStatuses.OHD))
							&& flightSeat.isUpdateToADifLogicalCC()) {
						canOverrideSeatAllocs = false;
						if (conflictingSeatCount == 0) {
							conflictingSeats.append(flightSeat.getSeatCode());
						} else {
							conflictingSeats.append(", ").append(flightSeat.getSeatCode());
						}
						conflictingSeatCount++;
					}
				}
				if (!canOverrideSeatAllocs) {
					if (splitOption == InvRollForwardCriteriaDTO.SKIP_SPLITING_ALLOCATIONS) {
						ModuleException moduleException = new ModuleException(
								"airinventory.arg.logicalCabinClass.Split.Update.invalid", AirinventoryConstants.MODULE_NAME);
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMMdd");
						List<Integer> flightIds = new ArrayList<Integer>();
						flightIds.add(flightId);
						String flightDptDate = AirInventoryModuleUtils.getFlightBD().getFlightDates(flightIds, 1, dateFormat);
						List<String> params = new ArrayList<String>();
						params.add(String.valueOf(conflictingSeatCount));
						params.add(conflictingSeats.toString());
						params.add(flightDptDate);
						moduleException.setMessageParameters(params);
						throw moduleException;
					} else {
						if (fltInventoryErrorMap != null) {
							String flightIdAndCC = flightId + "|" + cabinClass;
							if (splitOption == InvRollForwardCriteriaDTO.COPY_SEG_ALLOCS_AND_OVERRIDE_SEATS) {
								fltInventoryErrorMap.put(flightIdAndCC,
										new StringBuffer("Logical classes rollforwarded with overriding seats("
												+ conflictingSeats.toString() + ")."));
							} else {
								updateSegAllocs = false;
								fltInventoryErrorMap.put(flightIdAndCC, new StringBuffer(
										"Can not split due to reserved seats(" + conflictingSeats.toString() + ")."));
							}
						}
					}
				}
			}

			if (updateSegAllocs) {
				if (!updatedFccSegInventories.isEmpty()) {
					getFlightInventoryDAO().saveOrUpdateFCCSegInventory(updatedFccSegInventories);
				}
				// reload the fccSegInventories for selected segment if new inventories were created
				if (!newFccSegInveLCCs.isEmpty()) {
					fccSegInventories = getFlightInventoryDAO().getFCCSegInventories(flightId, flightSegmentId, cabinClass);
					List<FCCSegInventory> removeInvList = new ArrayList<FCCSegInventory>();
					for (FCCSegInventory fccSegInv : fccSegInventories) {
						if (!inventriesToDelete.isEmpty() && inventriesToDelete.contains(fccSegInv.getFccsInvId())) {
							removeInvList.add(fccSegInv);
						}
					}
					fccSegInventories.removeAll(removeInvList);
				}
				// update flight seat details
				if (changeFlightSeats) {
					Collection<FCCSegInventory> updatedSegInventories = getFlightInventoryDAO().getFCCSegInventories(flightId,
							flightSegmentId, cabinClass);
					updatedSegInventories.removeAll(inventriesToDelete);
					for (FlightSeat flightSeat : flightSeatData) {
						String updatedLogicalCC = flightSeat.getLogicalCCCode();
						FCCSegInventory fccSegInv = null;
						for (FCCSegInventory fccSInv : updatedSegInventories) {
							if (fccSInv.getLogicalCCCode().equals(updatedLogicalCC)) {
								fccSegInv = fccSInv;
								break;
							}
						}
						if (fccSegInv != null) {
							flightSeat.setFlightSegInventoryId(fccSegInv.getFccsInvId());
							flightSeat.setLogicalCCCode(updatedLogicalCC);
						}
					}
					getSeatMapDAO().saveSeatFares(flightSeatData);
				}
				if (!inventriesToDelete.isEmpty()) {
					getFlightInventoryDAO().deleteFlightSegmentInventories(inventriesToDelete);
				}
			}
		}

		// create intercepting segments
		createInterceptingSegments(newFccSegInveLCCs, flightId, cabinClass);
	}

	/**
	 * @param fccSegInventory
	 * @param flccAllocationDTO
	 * @param soldAdultInfantSeatCount
	 * @param hasInterceptingSegments
	 * @throws ModuleException
	 */
	private void updateFccSegmentInventoryAvailability(FCCSegInventory fccSegInventory, FLCCAllocationDTO flccAllocationDTO,
			int[] soldAdultInfantSeatCount, boolean hasInterceptingSegments) throws ModuleException {

		if (fccSegInventory.getSoldAndOnholdAdultSeats() + fccSegInventory.getCurtailedSeats() > flccAllocationDTO
				.getAdultAllocation()) {
			throw new ModuleException("airinventory.arg.splitAdultAllocation.invalid", AirinventoryConstants.MODULE_NAME);
		}
		if (fccSegInventory.getSoldAndOnholdInfantSeats() > flccAllocationDTO.getInfantAllocation()) {
			throw new ModuleException("airinventory.arg.splitInfantAllocation.invalid", AirinventoryConstants.MODULE_NAME);
		}

		fccSegInventory.setAllocatedSeats(flccAllocationDTO.getAdultAllocation());
		fccSegInventory.setInfantAllocation(flccAllocationDTO.getInfantAllocation());

		// get available fixed seat count
		Integer availableFixedSeatCount = 0;
		if (fccSegInventory.getFccsInvId() > 0 && hasInterceptingSegments) {
			availableFixedSeatCount = getFlightInventoryJDBCDAO().getAvailableFixedSeatCount(fccSegInventory.getFccsInvId());
		}
		int effectiveCapacity = flccAllocationDTO.getAdultAllocation() + fccSegInventory.getOversellSeats()
				- fccSegInventory.getCurtailedSeats() + availableFixedSeatCount;
		fccSegInventory.setAvailableSeats(Math.max((effectiveCapacity - soldAdultInfantSeatCount[0]), availableFixedSeatCount));

		fccSegInventory
				.setAvailableInfantSeats(Math.max(flccAllocationDTO.getInfantAllocation() - soldAdultInfantSeatCount[1], 0));
	}

	/**
	 * @param newFccSegInveLCCs
	 * @param flightId
	 * @param cabinClass
	 */
	private void createInterceptingSegments(Collection<String> newFccSegInveLCCs, Integer flightId, String cabinClass) {

		Collection<FCCSegInventory> fccSegInventories = getFlightInventoryDAO().getFCCSegInventories(flightId, cabinClass);
		Set<InterceptingFCCSegAlloc> interceptingFCCSegAllocs = new HashSet<InterceptingFCCSegAlloc>();
		for (FCCSegInventory fccSegInventory : fccSegInventories) {
			if (newFccSegInveLCCs.contains(fccSegInventory.getLogicalCCCode())) {
				List<Integer> interceptingFccSegInvIds = AirinventoryUtils.getInterceptingSegments(fccSegInventories,
						fccSegInventory);

				List<Integer> existingInterceptingSegments = getFlightInventoryDAO().getInterceptingFCCSegInventoryIds(
						fccSegInventory.getFlightSegId(), fccSegInventory.getLogicalCCCode(), false);

				for (Integer interceptingFccSeginvId : interceptingFccSegInvIds) {
					if (!existingInterceptingSegments.contains(interceptingFccSeginvId)) {
						interceptingFCCSegAllocs
								.add(createInterceptingFCCSegAlloc(fccSegInventory.getFccsInvId(), interceptingFccSeginvId));
						if (!AppSysParamsUtil.isLogicalCabinClassInventoryRestricted()) {
							interceptingFCCSegAllocs
									.add(createInterceptingFCCSegAlloc(interceptingFccSeginvId, fccSegInventory.getFccsInvId()));
						}
					}
				}
			}
		}
		getFlightInventoryDAO().saveInterceptingFCCSegAlloc(interceptingFCCSegAllocs);
	}

	private InterceptingFCCSegAlloc createInterceptingFCCSegAlloc(Integer sourceFccSegInvId, Integer interceptingFccSegId) {
		InterceptingFCCSegAlloc interceptingFCCSegAlloc = new InterceptingFCCSegAlloc();
		interceptingFCCSegAlloc.setSourceFCCSegInvId(sourceFccSegInvId);
		interceptingFCCSegAlloc.setInterceptingFCCSegInvId(interceptingFccSegId);
		return interceptingFCCSegAlloc;
	}

	/**
	 * @param fccSegInventories
	 */
	private Map<String, List<FCCSegInventory>> getSegmentWiseSegmentInventory(Collection<FCCSegInventory> fccSegInventories) {
		Map<String, List<FCCSegInventory>> segmentWiseInventoryMap = new HashMap<String, List<FCCSegInventory>>();

		for (FCCSegInventory fccSegInventory : fccSegInventories) {
			String segmentCode = fccSegInventory.getSegmentCode();
			List<FCCSegInventory> fccSeginvenCol = segmentWiseInventoryMap.get(segmentCode);
			if (fccSeginvenCol == null) {
				segmentWiseInventoryMap.put(segmentCode, new ArrayList<FCCSegInventory>());
			}
			segmentWiseInventoryMap.get(segmentCode).add(fccSegInventory);
		}
		return segmentWiseInventoryMap;
	}

	private void createFCCInventory(AircraftCabinCapacity aircraftCabinCapacity, CreateFlightInventoryRQ createFlightInventoryRQ,
			String defaultLogicalCabinClass) {

		// create flight cabin class inventory
		FCCInventory fccInventory = new FCCInventory();
		fccInventory.setFlightId(createFlightInventoryRQ.getFlightId());
		fccInventory.setCCCode(aircraftCabinCapacity.getCabinClassCode());
		fccInventory.setActualCapacity(aircraftCabinCapacity.getSeatCapacity());
		fccInventory.setInfantCapacity(aircraftCabinCapacity.getInfantCapacity());
		Set<FCCSegInventory> fccSegInvRecs = new HashSet<FCCSegInventory>();

		// create fcc segment inventories for each segment, including any segment flagged as invalid as well
		Iterator<SegmentDTO> segmentDTOsIterator = createFlightInventoryRQ.getSegmentDTOs().iterator();
		if (defaultLogicalCabinClass != null) {
			while (segmentDTOsIterator.hasNext()) {
				SegmentDTO segDTO = segmentDTOsIterator.next();
				if (createFlightInventoryRQ.getFlightId().intValue() == segDTO.getFlightId()) {
					String segmentCode = segDTO.getSegmentCode();

					/*
					 * set followings in FCCSegmentInventory (fccInventoryId, segmentCode, allocation, available,
					 * validFlag, overlappingFlightId) [set available=allocated, sold=0, onhold=0,
					 * curtailed=0,oversell=0]
					 */
					FCCSegInventory fccSegInventory = AirinventoryUtils.createZeroFccSegAllocInventory();
					fccSegInventory.setFlightId(createFlightInventoryRQ.getFlightId());
					fccSegInventory.setLogicalCCCode(defaultLogicalCabinClass);
					fccSegInventory.setSegmentCode(segmentCode);
					fccSegInventory.setFlightSegId(segDTO.getSegmentId());
					fccSegInventory.setAllocatedSeats(aircraftCabinCapacity.getSeatCapacity());
					fccSegInventory.setAvailableSeats(aircraftCabinCapacity.getSeatCapacity());
					fccSegInventory.setInfantAllocation(aircraftCabinCapacity.getInfantCapacity());
					fccSegInventory.setAvailableInfantSeats(aircraftCabinCapacity.getInfantCapacity());
					fccSegInventory.setValidFlag(segDTO.isInvalidSegment() ? false : true);
					fccSegInventory.setOverlappingFlightId(createFlightInventoryRQ.getOverlappingFlightId());
					fccSegInvRecs.add(fccSegInventory);

					FlightInventoryBLLogger.logCreateFlightSegInventory(log, createFlightInventoryRQ, segDTO,
							aircraftCabinCapacity.getCabinClassCode());
				}

			}
			fccInventory.setSegAlloc(fccSegInvRecs);
			getFlightInventoryDAO().saveFCCInventory(fccInventory);

			if (defaultLogicalCabinClass != null) {
				saveInterceptingFCCSegments(defaultLogicalCabinClass, createFlightInventoryRQ);
			}

			if (createFlightInventoryRQ.getOverlappingFlightId() != null) {
				// set the overlapping flight id in overlapping flight
				getFlightInventoryDAO().setOverlappingFlight(createFlightInventoryRQ.getOverlappingFlightId(),
						createFlightInventoryRQ.getFlightId());

				// [TODO:test] update availabile seats in flight cabin class segements of created inventory
				List<Integer> flightIds = new ArrayList<Integer>();
				flightIds.add(createFlightInventoryRQ.getOverlappingFlightId());

				if (getFlightInventoryResDAO().isFlightHasReservations(createFlightInventoryRQ.getOverlappingFlightId())) {
					Iterator<FCCSegInventory> fccSegInvRecsIt = fccSegInvRecs.iterator();
					List<Integer> fccSegInvIds = new ArrayList<Integer>();
					while (fccSegInvRecsIt.hasNext()) {
						fccSegInvIds.add(new Integer(fccSegInvRecsIt.next().getFccsInvId()));
					}
					getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(fccSegInvIds);
				}
			}
		}
	}

	private void createFCCInventoryFromTemplate(String cabinClassCode,
			Map<String, List<InvTempCCAllocDTO>> tmpSegmentWiseInvTemplate, CreateFlightInventoryRQ createFlightInventoryRQ,
			int actualCapacity, int infantCapacity, String userId) throws ModuleException {

		// create flight cabin class inventory
		FCCInventory fccInventory = new FCCInventory();
		fccInventory.setFlightId(createFlightInventoryRQ.getFlightId());
		fccInventory.setCCCode(cabinClassCode);
		fccInventory.setActualCapacity(actualCapacity);
		fccInventory.setInfantCapacity(infantCapacity);


		Set<FCCSegInventory> fccSegInvRecs = new HashSet<FCCSegInventory>();

		Iterator<SegmentDTO> segmentDTOsIterator = createFlightInventoryRQ.getSegmentDTOs().iterator();

		while (segmentDTOsIterator.hasNext()) {
			SegmentDTO segDTO = segmentDTOsIterator.next();
			String segmentCode = segDTO.getSegmentCode();

			for (InvTempCCAllocDTO invTempCCAllocDTO : tmpSegmentWiseInvTemplate.get(segmentCode)) {
				FCCSegInventory fccSegInventory = AirinventoryUtils.createZeroFccSegAllocInventory();
				fccSegInventory.setFlightId(createFlightInventoryRQ.getFlightId());
				fccSegInventory.setLogicalCCCode(invTempCCAllocDTO.getLogicalCabinClassCode());
				fccSegInventory.setSegmentCode(segmentCode);
				fccSegInventory.setFlightSegId(segDTO.getSegmentId());
				fccSegInventory.setAllocatedSeats(invTempCCAllocDTO.getAdultAllocation());
				fccSegInventory.setAvailableSeats(invTempCCAllocDTO.getAdultAllocation());
				fccSegInventory.setInfantAllocation(invTempCCAllocDTO.getInfantAllocation());
				fccSegInventory.setAvailableInfantSeats(invTempCCAllocDTO.getInfantAllocation());
				fccSegInventory.setValidFlag(segDTO.isInvalidSegment() ? false : true);
				fccSegInventory.setOverlappingFlightId(createFlightInventoryRQ.getOverlappingFlightId());
				fccSegInvRecs.add(fccSegInventory);
			}

			FlightInventoryBLLogger.logCreateFlightSegInventory(log, createFlightInventoryRQ, segDTO, cabinClassCode);
		}


		fccInventory.setSegAlloc(fccSegInvRecs);
		getFlightInventoryDAO().saveFCCInventory(fccInventory);

		createFCCSegBCInventoryFromTemplate(tmpSegmentWiseInvTemplate, fccInventory, userId);

		Iterator<SegmentDTO> segmentDTOsIteratorTmp = createFlightInventoryRQ.getSegmentDTOs().iterator();

		while (segmentDTOsIteratorTmp.hasNext()) {
			SegmentDTO segDTO = segmentDTOsIteratorTmp.next();
			String segmentCode = segDTO.getSegmentCode();

			for (InvTempCCAllocDTO invTempCCAllocDTO : tmpSegmentWiseInvTemplate.get(segmentCode)) {
				saveInterceptingFCCSegments(invTempCCAllocDTO.getLogicalCabinClassCode(), createFlightInventoryRQ);
			}
			break;
		}

		if (createFlightInventoryRQ.getOverlappingFlightId() != null) {
			// set the overlapping flight id in overlapping flight
			getFlightInventoryDAO().setOverlappingFlight(createFlightInventoryRQ.getOverlappingFlightId(),
					createFlightInventoryRQ.getFlightId());

			// [TODO:test] update availabile seats in flight cabin class segements of created inventory
			List<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(createFlightInventoryRQ.getOverlappingFlightId());

			if (getFlightInventoryResDAO().isFlightHasReservations(createFlightInventoryRQ.getOverlappingFlightId())) {
				Iterator<FCCSegInventory> fccSegInvRecsIt = fccSegInvRecs.iterator();
				List<Integer> fccSegInvIds = new ArrayList<Integer>();
				while (fccSegInvRecsIt.hasNext()) {
					fccSegInvIds.add(new Integer(fccSegInvRecsIt.next().getFccsInvId()));
				}
				getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(fccSegInvIds);
			}
		}
	}

	private void createFCCSegBCInventoryFromTemplate(Map<String, List<InvTempCCAllocDTO>> tmpSegmentWiseInvTemplate,
			FCCInventory fccInventory, String userId) throws ModuleException {
		Collection<FCCSegBCInventory> segBCInventories = new ArrayList<FCCSegBCInventory>();
		FCCSegInventoryUpdateDTO fCCSegInventoryUpdateDTO = new FCCSegInventoryUpdateDTO();
		for (FCCSegInventory fccSegInventory : fccInventory.getSegAlloc()) {

			fCCSegInventoryUpdateDTO.setFccsInventoryId(fccSegInventory.getFccsInvId());
			fCCSegInventoryUpdateDTO.setFlightId(fccSegInventory.getFlightId());
			fCCSegInventoryUpdateDTO.setSegmentCode(fccSegInventory.getSegmentCode());
			fCCSegInventoryUpdateDTO.setLogicalCCCode(fccSegInventory.getLogicalCCCode());
			fCCSegInventoryUpdateDTO.setOversellSeats(0);
			fCCSegInventoryUpdateDTO.setCurtailedSeats(0);
			fCCSegInventoryUpdateDTO.setInfantAllocation(fccSegInventory.getInfantAllocation());
			fCCSegInventoryUpdateDTO.setVersion(0);

			for (InvTempCCAllocDTO invTempCCAllocDTO : tmpSegmentWiseInvTemplate.get(fccSegInventory.getSegmentCode())) {
				if (invTempCCAllocDTO.getLogicalCabinClassCode().equals(fccSegInventory.getLogicalCCCode())) {
					for (InvTempCCBCAllocDTO invTempCCBCAllocDTO : invTempCCAllocDTO.getInvTempCCBCAllocDTOList()) {
						FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
						fccSegBCInventory.setBookingCode(invTempCCBCAllocDTO.getBookingCode());
						fccSegBCInventory.setfccsInvId(fccSegInventory.getFccsInvId());
						fccSegBCInventory.setFlightId(fccSegInventory.getFlightId());
						fccSegBCInventory.setLogicalCCCode(fccSegInventory.getLogicalCCCode());
						fccSegBCInventory.setSegmentCode(fccSegInventory.getSegmentCode());
						fccSegBCInventory.setSeatsAllocated(invTempCCBCAllocDTO.getAllocatedSeats());
						fccSegBCInventory.setSeatsAvailable(invTempCCBCAllocDTO.getAllocatedSeats());
						fccSegBCInventory.setSeatsSold(0);
						fccSegBCInventory.setOnHoldSeats(0);
						fccSegBCInventory.setSeatsAcquired(0);
						fccSegBCInventory.setSeatsCancelled(0);
						fccSegBCInventory.setPriorityFlag(invTempCCBCAllocDTO.getPriorityFlag().equals(
								FCCSegBCInventory.PRIORITY_FLAG_Y) ? true : false);
						fccSegBCInventory.setStatus(invTempCCBCAllocDTO.getStatusBcAlloc());
						fccSegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);

						segBCInventories.add(fccSegBCInventory);

						Set<FCCSegBCInventory> addedBCInventories = new HashSet<FCCSegBCInventory>(segBCInventories);
						fCCSegInventoryUpdateDTO.setAddedFCCSegBCInventories(addedBCInventories);
					}
				}
			}
		}

		if (segBCInventories.size() > 0) {
			getFlightInventoryDAO().updateBCInvetory(segBCInventories);
			AuditAirinventory.updateFCCSegInventory(fCCSegInventoryUpdateDTO, userId, false);
		}

	}

	/**
	 * Prepare and save intercepting segments map for flight cabinclass
	 * 
	 * @param flightId
	 * @param cabinClassCode
	 * @param segmentDTOs
	 * @param overlappingFlightId
	 */
	private void saveInterceptingFCCSegments(String logicalCabinClassCode, CreateFlightInventoryRQ createFlightInventoryRQ) {

		Collection<InterceptingFCCSegAlloc> interceptingSegInvenories = new ArrayList<InterceptingFCCSegAlloc>();
		List<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(new Integer(createFlightInventoryRQ.getFlightId()));
		if (createFlightInventoryRQ.getOverlappingFlightId() != null) {
			flightIds.add(createFlightInventoryRQ.getOverlappingFlightId());
		}
		List<Object[]> segmentsInventories = getFlightInventoryDAO().getFCCSegmentInventoriesSummary(flightIds,
				logicalCabinClassCode);

		Iterator<SegmentDTO> segmentDTOsIt = createFlightInventoryRQ.getSegmentDTOs().iterator();
		while (segmentDTOsIt.hasNext()) {
			SegmentDTO segDTO = segmentDTOsIt.next();
			if (segDTO.getInterceptingFlightSeg() != null) {
				Iterator<InterceptingFlightSegmentDTO> interceptiongSegsIt = segDTO.getInterceptingFlightSeg().iterator();
				while (interceptiongSegsIt.hasNext()) {
					// prepare intercepting segments for source flight's fcc segment
					// inventories
					InterceptingFlightSegmentDTO interceptingFlightSeg = interceptiongSegsIt.next();
					InterceptingFCCSegAlloc interceptingFCCSegInventoryModel = new InterceptingFCCSegAlloc();
					Integer sourceFCCSegInvId = getFCCSegmentInventoryId(createFlightInventoryRQ.getFlightId(),
							segDTO.getSegmentCode(), segmentsInventories.iterator());
					Integer interceptingFCCSegInvId = getFCCSegmentInventoryId(interceptingFlightSeg.getFlightId(),
							interceptingFlightSeg.getInterceptingSegCode(), segmentsInventories.iterator());
					if (sourceFCCSegInvId == null || interceptingFCCSegInvId == null) {
						break;
					}
					interceptingFCCSegInventoryModel.setSourceFCCSegInvId(sourceFCCSegInvId.intValue());
					interceptingFCCSegInventoryModel.setInterceptingFCCSegInvId(interceptingFCCSegInvId.intValue());
					interceptingSegInvenories.add(interceptingFCCSegInventoryModel);

					// prepare intercepting segments for overlapping flight's fcc
					// segment inventories
					if (createFlightInventoryRQ.getOverlappingFlightId() != null && (interceptingFlightSeg
							.getFlightId() == createFlightInventoryRQ.getOverlappingFlightId().intValue())) {
						InterceptingFCCSegAlloc olFlightInterceptingFCCSegInventoryModel = new InterceptingFCCSegAlloc();
						olFlightInterceptingFCCSegInventoryModel
								.setSourceFCCSegInvId(getFCCSegmentInventoryId(createFlightInventoryRQ.getOverlappingFlightId(),
										interceptingFlightSeg.getInterceptingSegCode(), segmentsInventories.iterator())
												.intValue());
						olFlightInterceptingFCCSegInventoryModel
								.setInterceptingFCCSegInvId(getFCCSegmentInventoryId(createFlightInventoryRQ.getFlightId(),
										segDTO.getSegmentCode(), segmentsInventories.iterator()).intValue());
						interceptingSegInvenories.add(olFlightInterceptingFCCSegInventoryModel);
					}
				}
			}
		}
		// save intercepting fcc segment inventories
		getFlightInventoryDAO().saveInterceptingFCCSegAlloc(interceptingSegInvenories);
	}

	// [TODO:enhance] Make this method efficient using a map keyed by flightSegId
	private Integer getFCCSegmentInventoryId(int flightId, String segmentCode, Iterator<Object[]> segmentsInventoriesIt) {
		while (segmentsInventoriesIt.hasNext()) {
			Object[] record = segmentsInventoriesIt.next();
			int fId = ((Integer) record[0]).intValue();
			String sCode = (String) record[1];
			if ((fId == flightId) && sCode.equals(segmentCode)) {
				return ((Integer) record[2]);
			}
		}
		return null;
	}

	public void openGoshowBcInventory(FlightSegmentDTO flightSegment) throws ModuleException {
		if (AppSysParamsUtil.isOnlyGoshoFaresVisibleInFlightCutoffTime()) {
			boolean isFlightWithinCutOffTime = FlightUtil.isFlightWithinCutOffTime(flightSegment.getSegmentId());
			int goshowBCAllocattion = AirinventoryUtils.getAirInventoryConfig().getGoshowBCAllocation();
			Collection<String> bookingCodes = AirinventoryUtils.getAirInventoryConfig()
					.getGoshowBCListForAgentBookingsWithinCutoffTime();

			if (isFlightWithinCutOffTime) {
				Collection<BookingClass> goshowBCList = getBookingClassDAO()
						.getGoshowBCListForAgentBookingsWithinCutoffTime(bookingCodes);

				for (BookingClass bc : goshowBCList) {
					FCCSegBCInventory goShowFCCSegBCInventory = getFlightInventoryDAO()
							.getFCCSegBCInventory(flightSegment.getSegmentId(), bc.getBookingCode());
					FCCSegInventory fccSegInventory = getFlightInventoryDAO().getFCCSegInventory(flightSegment.getSegmentId(),
							bc.getLogicalCCCode());

					if (goShowFCCSegBCInventory == null) {
						if (fccSegInventory != null) {
							goShowFCCSegBCInventory = new FCCSegBCInventory();
							goShowFCCSegBCInventory.setBookingCode(bc.getBookingCode());
							goShowFCCSegBCInventory.setLogicalCCCode(bc.getLogicalCCCode());
							goShowFCCSegBCInventory.setfccsInvId(fccSegInventory.getFccsInvId());
							goShowFCCSegBCInventory.setFlightId(fccSegInventory.getFlightId());
							goShowFCCSegBCInventory.setOnHoldSeats(0);
							goShowFCCSegBCInventory.setPriorityFlag(false);
							goShowFCCSegBCInventory.setSeatsAcquired(0);
							goShowFCCSegBCInventory.setSeatsAllocated(goshowBCAllocattion);
							goShowFCCSegBCInventory.setSeatsAvailable(goshowBCAllocattion);
							goShowFCCSegBCInventory.setSeatsCancelled(0);
							goShowFCCSegBCInventory.setSeatsSold(0);
							goShowFCCSegBCInventory.setStatus(FCCSegBCInventory.Status.OPEN);
							goShowFCCSegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.SEAT_AQUISITION);
							goShowFCCSegBCInventory.setSegmentCode(fccSegInventory.getSegmentCode());
							getFlightInventoryDAO().saveFCCSegmentBCInventory(goShowFCCSegBCInventory);
						}
					} else {
						getFlightInventoryDAO().updateGoshowFCCSegBCInventory(goShowFCCSegBCInventory.getFccsbInvId().intValue(),
								goShowFCCSegBCInventory.getSeatsSold() + goshowBCAllocattion,
								goShowFCCSegBCInventory.getSeatsSold());
					}
				}
			}
		}
	}

	public void updateInvForGoshowPassengers(SegmentSeatDistsDTO segmentSeatDistsDTO, String userId) {
		SeatDistribution goshowSeatDist = segmentSeatDistsDTO.getSeatDistribution().iterator().next();
		String goshowBookingCode = goshowSeatDist.getBookingClassCode();

		BookingClass goshowBC = getBookingClassDAO().getLightWeightBookingClass(goshowBookingCode);

		FCCSegBCInventory goShowFCCSegBCInventory = getFlightInventoryDAO()
				.getFCCSegBCInventory(segmentSeatDistsDTO.getFlightSegId(), goshowBC.getBookingCode());
		FCCSegInventory fccSegInventory = getFlightInventoryDAO().getFCCSegInventory(segmentSeatDistsDTO.getFlightSegId(),
				goshowBC.getLogicalCCCode());
		if (goShowFCCSegBCInventory == null) {
			// create goshow fccsegbc inventory
			goShowFCCSegBCInventory = new FCCSegBCInventory();
			goShowFCCSegBCInventory.setBookingCode(goshowBC.getBookingCode());
			goShowFCCSegBCInventory.setLogicalCCCode(goshowBC.getLogicalCCCode());
			goShowFCCSegBCInventory.setfccsInvId(fccSegInventory.getFccsInvId());
			goShowFCCSegBCInventory.setFlightId(fccSegInventory.getFlightId());
			goShowFCCSegBCInventory.setOnHoldSeats(0);
			goShowFCCSegBCInventory.setPriorityFlag(false);
			goShowFCCSegBCInventory.setSeatsAcquired(0);
			goShowFCCSegBCInventory.setSeatsAllocated(goshowSeatDist.getNoOfSeats());
			goShowFCCSegBCInventory.setSeatsAvailable(0);
			goShowFCCSegBCInventory.setSeatsCancelled(0);
			goShowFCCSegBCInventory.setSeatsSold(goshowSeatDist.getNoOfSeats());
			goShowFCCSegBCInventory.setStatus(FCCSegBCInventory.Status.CLOSED);
			goShowFCCSegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
			goShowFCCSegBCInventory.setSegmentCode(fccSegInventory.getSegmentCode());
			getFlightInventoryDAO().saveFCCSegmentBCInventory(goShowFCCSegBCInventory);

		} else {
			// update goshow bc inventory seats counts
			getFlightInventoryDAO().updateGoshowFCCSegBCInventory(goShowFCCSegBCInventory.getFccsbInvId().intValue(),
					goshowSeatDist.getNoOfSeats(), goshowSeatDist.getNoOfSeats());
		}

		// update segment availability, and intercepting segments' availability
		getFlightInventoryDAO().updateFCCSegmentInventory(fccSegInventory.getFlightId(), fccSegInventory.getLogicalCCCode(),
				fccSegInventory.getSegmentCode(), userId);

		// update available seats count
		List<Integer> currentAndInterceptingSegIds = new ArrayList<Integer>();
		currentAndInterceptingSegIds.add(new Integer(fccSegInventory.getFccsInvId()));

		List<Integer> interceptingSegs = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(
				fccSegInventory.getFlightId(), fccSegInventory.getLogicalCCCode(), fccSegInventory.getSegmentCode(), true);
		if (interceptingSegs != null && interceptingSegs.size() > 0) {
			currentAndInterceptingSegIds.addAll(interceptingSegs);
		}
		getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(currentAndInterceptingSegIds);
	}

	/**
	 * Updates the inventory when aircraft model is udpated
	 * 
	 * @param flightIds
	 * @param updatedAircraftModel
	 * @param existingAircraftModel
	 * @param scheduleSegmentCode
	 * @param flightSegmentDTOs
	 * @throws ModuleException
	 */
	public void updateFCCInventoriesForModelUpdate(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, String scheduleSegmentCode, boolean downgradeToAnyModel,
			Collection<SegmentDTO> flightSegmentDTOs) throws ModuleException {
		updateFCCInventoriesForModelChange(flightIds, updatedAircraftModel, existingAircraftModel, null, flightSegmentDTOs,
				scheduleSegmentCode, downgradeToAnyModel);
	}

	/**
	 * Updates the inventory when aircraft model is changed
	 * 
	 * @param flightIds
	 *            Flight Ids whose associated aircraft model is changed; only those flights which are yet to depature
	 * @param updatedAircraftModel
	 * @param existingAircraftModel
	 * @param olFlightIds
	 * @param segmentDTOs
	 * @param scheduleSegmentCode
	 * @throws ModuleException
	 */

	public void updateFCCInventoriesForModelChange(Collection<Integer> flightIds, AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel, Collection<Integer> olFlightIds, Collection<SegmentDTO> segmentDTOs,
			String scheduleSegmentCode, boolean downgradeToAnyModel) throws ModuleException {

		if (flightIds != null && flightIds.size() > 0) {
			if (log.isDebugEnabled()) {
				log.debug("Updating aircraft model for flights " + "[flightIds = " + flightIds + ", " + "existingAircraftModel = "
						+ existingAircraftModel.getModelNumber() + ", " + "updatedAircraftModel = "
						+ updatedAircraftModel.getModelNumber() + "]");
			}

			Collection<AircraftCabinCapacity> cabinClassesUpdated = getCabinClassesOnlyInOneModelOrBoth(updatedAircraftModel,
					existingAircraftModel, true);
			Collection<AircraftCabinCapacity> cabinClassesAdded = getCabinClassesOnlyInOneModelOrBoth(existingAircraftModel,
					updatedAircraftModel, false);
			Collection<AircraftCabinCapacity> cabinClassesDeleted = getCabinClassesOnlyInOneModelOrBoth(updatedAircraftModel,
					existingAircraftModel, false);

			for (AircraftCabinCapacity deletedCC : cabinClassesDeleted) {

				Collection<Integer> flightIdsHavingRes = getFlightInventoryResDAO()
						.filterFlightsHavingWaitlistedReservations(flightIds, deletedCC.getCabinClassCode());
				if (flightIdsHavingRes != null && flightIdsHavingRes.size() > 0) {
					throw new ModuleException("airinventory.arg.model.change.waitliestedpnr", AirinventoryConstants.MODULE_NAME);
				}

				getFlightInventoryDAO().deleteFlightInventories(flightIds, deletedCC.getCabinClassCode());
			}

			for (AircraftCabinCapacity updatedCC : cabinClassesUpdated) {
				// update adultcapacity, infantcapacity and available seats on FCCInventory and FCCSegInventory
				getFlightInventoryDAO().updateAllocatedCapacities(flightIds, updatedCC.getCabinClassCode(),
						updatedCC.getSeatCapacity(), updatedCC.getInfantCapacity(), scheduleSegmentCode, downgradeToAnyModel);
			}

			if (cabinClassesAdded != null && cabinClassesAdded.size() > 0 && segmentDTOs == null) {
				throw new ModuleException("airinventory.arg.model.change.failed", AirinventoryConstants.MODULE_NAME);
			}

			for (AircraftCabinCapacity addedCC : cabinClassesAdded) {
				Iterator<Integer> flightIdsIt = flightIds.iterator();
				Iterator<Integer> olflightIdsIt = null;
				if (olFlightIds != null) {
					olflightIdsIt = olFlightIds.iterator();
				}
				Integer olFlightId = null;
				while (flightIdsIt.hasNext()) {
					if (olflightIdsIt != null && olflightIdsIt.hasNext()) {
						olFlightId = olflightIdsIt.next();
					}
					// create fcc inventory
					CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
					createFlightInventoryRQ.setFlightId(flightIdsIt.next());
					createFlightInventoryRQ.setOverlappingFlightId(olFlightId);
					createFlightInventoryRQ.setSegmentDTOs(segmentDTOs);

					String defaultLogicalCC = getLogicalCabinClassDAO().getDefaultLogicalCabinClass(addedCC.getCabinClassCode());
					createFCCInventory(addedCC, createFlightInventoryRQ, defaultLogicalCC);
				}
			}
		}
	}

	/**
	 * Checks updatability of flight inventories when associated aircarft model capacities changes
	 * 
	 * @param flightIds
	 * @param aircraftModel
	 * @return true if all the flight inventory are updatable
	 */
	public DefaultServiceResponse checkFCCInventoryModelUpdatability(Collection<Integer> flightIds,
			AircraftModel updatedAircraftModel, AircraftModel existingAircraftModel, boolean downGradeToAnyModel,
			boolean isScheduleChange) throws ModuleException {
		return getFlightInventoryJDBCDAO().isFlightInventoryModelUpdatable(flightIds,
				getCabinClassesOnlyInOneModelOrBoth(updatedAircraftModel, existingAircraftModel, true),
				getCabinClassesOnlyInOneModelOrBoth(updatedAircraftModel, existingAircraftModel, false), downGradeToAnyModel,
				isScheduleChange);
	}

	public DefaultServiceResponse getAffectedFlights(Collection<Integer> flightIds, AircraftModel updatedAircraftMode)
			throws ModuleException {
		return getFlightInventoryJDBCDAO().getAffectedFlights(flightIds,
				getCabinClassesOnlyInOneModelOrBoth(new AircraftModel(), updatedAircraftMode, false));
	}

	private Collection<AircraftCabinCapacity> getCabinClassesOnlyInOneModelOrBoth(AircraftModel newAircraftModel,
			AircraftModel existingModel, boolean inbothModels) {
		Collection<AircraftCabinCapacity> cabinClassesOnlyInOneModel = new ArrayList<AircraftCabinCapacity>();
		Collection<AircraftCabinCapacity> cabinClassesInBothModels = new ArrayList<AircraftCabinCapacity>();
		if (existingModel != null) {
			for (AircraftCabinCapacity cc : existingModel.getAircraftCabinCapacitys()) {
				boolean inOnlyOneModel = true;
				if (newAircraftModel.getAircraftCabinCapacitys() != null) {
					for (AircraftCabinCapacity newCC : newAircraftModel.getAircraftCabinCapacitys()) {
						if (cc.getCabinClassCode().equals(newCC.getCabinClassCode())) {
							cabinClassesInBothModels.add(newCC);
							inOnlyOneModel = false;
							break;
						}
					}
				}
				if (inOnlyOneModel && !inbothModels) {
					cabinClassesOnlyInOneModel.add(cc);
				}
			}
		}
		return inbothModels ? cabinClassesInBothModels : cabinClassesOnlyInOneModel;
	}

	/**
	 * Updates FCCSegInventory along with add/update/delete FCCSegBCInventories and updates cumulative sums and
	 * availability in FCCSegInventory and Updates availability in intercepting segment inventories
	 * 
	 * [TODO: test] fixed quota validity check on flight with multiple segments
	 * 
	 * @param fccSegInventoryUpdateDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public FCCSegInventoryUpdateStatusDTO updateFCCSegInventoryNew(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO,
			String userId) throws ModuleException {
		// synchronized (FlightInventoryResBL.segInvUpdateLock) {
		// aquire the lock on segment and intercepting segment(s)
		List<Integer> interceptingSegInventoryIds = getFlightInventoryDAO()
				.getInterceptingFCCSegmentInventoryIds(fccSegInventoryUpdateDTO.getFccsInventoryId(), true);

		InventoryDiffForAVS invDiffForAVS = new InventoryDiffForAVS(fccSegInventoryUpdateDTO.getFccsInventoryId());
		log.debug("intial call to record Inv snapshot, status " + invDiffForAVS.takeIntialInvSnapshot());

		getFlightInventoryResJDBCDAO().getFCCSegInventoriesForUpdate(Arrays.asList(fccSegInventoryUpdateDTO.getFccsInventoryId()),
				false, getAirInventoryConfig().getMaxWaitForLockDuration());

		boolean isPublishAvailability = AirinventoryUtils.getAirInventoryConfig().isPublishAvailability();
		
		// check FCCSegInventory updatability
		FCCSegInventory currentFCCSegInventory = getFlightInventoryDAO().getFCCSegInventory(
				fccSegInventoryUpdateDTO.getFlightId(), fccSegInventoryUpdateDTO.getLogicalCCCode(),
				fccSegInventoryUpdateDTO.getSegmentCode());

		// [TODO:test] Validity constraints on fixed seats allocations
		HashMap<Integer, ExtendedFCCSegBCInventory> existingFCCSegBCInventories = getFlightInventoryDAO()
				.getExtendedFCCSegBCInventories(currentFCCSegInventory.getFccsInvId());
		int[] additionalFixedAndTotalUnsoldFixedCounts = calculateNewOutstandingFixedAllocations(fccSegInventoryUpdateDTO,
				existingFCCSegBCInventories);
		// check and save added FCCSegBCInventories
		List<Integer> addedFCCSegBCInventoryIds = new ArrayList<Integer>();

		int effectiveAllocReduction = 0;
		if (fccSegInventoryUpdateDTO.isFCCSegInvUpdated()) {
			int additionalCurtailedSeats = fccSegInventoryUpdateDTO.getCurtailedSeats() - currentFCCSegInventory.getCurtailedSeats();
			int oversellSeatsReduction = currentFCCSegInventory.getOversellSeats() - fccSegInventoryUpdateDTO.getOversellSeats();
			effectiveAllocReduction = additionalCurtailedSeats + oversellSeatsReduction;

		if (currentFCCSegInventory.getAvailableSeats() >= 0) {
			if ((currentFCCSegInventory.getAvailableSeats() - oversellSeatsReduction) < additionalCurtailedSeats) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Oversell, Curtailed seats combination Invalid. " + "[seg available="
								+ currentFCCSegInventory.getAvailableSeats() + "] " + "[additional curtailed ="
								+ additionalCurtailedSeats + "]",
						new ModuleException("airinventory.logic.bl.curtailed.invalid"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);

			}
			if ((currentFCCSegInventory.getAvailableSeats() - additionalCurtailedSeats) < oversellSeatsReduction) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Oversell, Curtailed seats combination Invalid. " + "[seg available="
								+ currentFCCSegInventory.getAvailableSeats() + "] " + "[oversell reduction ="
								+ oversellSeatsReduction + "]",
						new ModuleException("airinventory.logic.bl.oversell.invalid"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
			if (currentFCCSegInventory.getAllocatedSeats() < fccSegInventoryUpdateDTO.getWaitListSeats()) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Wait List seats combination Invalid. " + "[seg allocation=" + currentFCCSegInventory.getAllocatedSeats()
								+ "] " + "[Wait List Seats =" + fccSegInventoryUpdateDTO.getWaitListSeats() + "]",
						new ModuleException("airinventory.logic.bl.waitlist.invalid"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
		} else {
			// for overloaded flights, curtailed seats not allowed to increase
			// oversell not allowed to reduce
			if ((oversellSeatsReduction + additionalCurtailedSeats) > 0) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Cannot reduce effective allocation for overloaded flights. " + "[seg available="
								+ currentFCCSegInventory.getAvailableSeats() + "] " + "[oversell reduction ="
								+ oversellSeatsReduction + "] " + "[additional curtailed =" + additionalCurtailedSeats + "]",
						new ModuleException("airinventory.logic.bl.oversellcurtail.invalid"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
		}

		int infantAllocationReduction = currentFCCSegInventory.getInfantAllocation()
				- fccSegInventoryUpdateDTO.getInfantAllocation();
		if (currentFCCSegInventory.getAvailableInfantSeats() >= 0) {
			if (currentFCCSegInventory.getAvailableInfantSeats() < infantAllocationReduction) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Invalid infant allocation. " + "[infant available=" + currentFCCSegInventory.getAvailableInfantSeats()
								+ "] " + "[infant reduction=" + infantAllocationReduction + "] ",
						new ModuleException("airinventory.logic.bl.infantalloc.invalid"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
		} else {
			// for flights with infant seats overloaded, infant allocation
			// is not allowed to reduce
			if (infantAllocationReduction > 0) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Infant allocation is not allowed to reduce. " + "[infant available="
								+ currentFCCSegInventory.getAvailableInfantSeats() + "] " + "[infant reduction="
								+ infantAllocationReduction + "] ",
						new ModuleException("airinventory.logic.bl.infantalloc.invalid"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
		}
		}
		// int maximumAllowedAdditionalFixed =
		// getFlightInventoryDAO().getMinimumAvailability(currentAndInterceptingFCCSegInvIds);

		// check segment allocation is enough to accomodate outstanding fixed seats

		if (currentFCCSegInventory.getAvailableSeats() < 0) {
			// if any of the intercepting segmetns availability is negative
			// then overall fixed allocations on the segment is not allowed to increase
			if (additionalFixedAndTotalUnsoldFixedCounts[0] > 0) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Cannot increase fixed allocation for overloadded flights. " + "[Seg available seats="
								+ currentFCCSegInventory.getAvailableSeats() + "] "
								+ "[Additional unsold total fixed seats allocation=" + additionalFixedAndTotalUnsoldFixedCounts[0]
								+ "] " + "[Total unsold total fixed seats allocation="
								+ additionalFixedAndTotalUnsoldFixedCounts[1] + "] ",
						new ModuleException("airinventory.logic.bl.fixedalloc.invalid"),
						FCCSegInventoryUpdateStatusDTO.FIXED_BC_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
			// } else if ((currentFCCSegInventory.getAvailableSeats() - effectiveAllocReduction) <
			// additionalFixedAndTotalUnsoldFixedCounts[1]) {
			// return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
			// "Total available fixed seats is more than available seats. " +
			// "[Seg available seats=" + currentFCCSegInventory.getAvailableSeats() + "] " +
			// "[Additional sellable total fixed seats allocation=" + additionalFixedAndTotalUnsoldFixedCounts[0] + "] "
			// +
			// "[Total sellable total fixed seats allocation=" + additionalFixedAndTotalUnsoldFixedCounts[1] + "] ",
			// new ModuleException("airinventory.logic.bl.fixedalloc.invalid"),
			// FCCSegInventoryUpdateStatusDTO.FIXED_BC_INVENTORY_FAILURE,
			// new Integer(currentFCCSegInventory.getFccsInvId()),
			// null,null);
		} else if ((currentFCCSegInventory.getAvailableSeats()
				- effectiveAllocReduction) < additionalFixedAndTotalUnsoldFixedCounts[1]) {
			String msg = "";
			if (additionalFixedAndTotalUnsoldFixedCounts[0] > 0) {
				msg = "Fixed seats addition is more than available seats.";
			}
			if (effectiveAllocReduction > 0) {
				msg = "Seat allocation reduction is more than available seats.";
			}
			return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
					msg + "[Seg available seats=" + currentFCCSegInventory.getAvailableSeats() + "] " + "[Allocation reduction ="
							+ effectiveAllocReduction + "] " + "[Additional unsold total fixed seats allocation="
							+ additionalFixedAndTotalUnsoldFixedCounts[0] + "] " + "[Total unsold total fixed seats allocation="
							+ additionalFixedAndTotalUnsoldFixedCounts[1] + "] ",
					new ModuleException("airinventory.logic.bl.fixedalloc.invalid"),
					FCCSegInventoryUpdateStatusDTO.FIXED_BC_INVENTORY_FAILURE, new Integer(currentFCCSegInventory.getFccsInvId()),
					null);
		}

		if (fccSegInventoryUpdateDTO.isFCCSegInvUpdated()) {
			// oversell, curtailed, infantAllocation on Segment are ok to update
			if (getFlightInventoryDAO().updateFCCSegmentInventory(fccSegInventoryUpdateDTO.getFccsInventoryId(),
					fccSegInventoryUpdateDTO.getOversellSeats(), fccSegInventoryUpdateDTO.getCurtailedSeats(),
					fccSegInventoryUpdateDTO.getInfantAllocation(), fccSegInventoryUpdateDTO.getWaitListSeats(),
					fccSegInventoryUpdateDTO.getVersion(), false) != 1) {
				return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
						"Updating segment inventory failed. Some other concurrent current update on segment may have happend.",
						new ModuleException("airinventory.logic.bl.seginv.update.failed"),
						FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE,
						new Integer(currentFCCSegInventory.getFccsInvId()), null);
			}
		}

		// check and delete, deleted FCCSegBcInventories
		Iterator bcInventoryIt;
		if (fccSegInventoryUpdateDTO.getDeletedFCCSegBCInventories() != null) {
			bcInventoryIt = fccSegInventoryUpdateDTO.getDeletedFCCSegBCInventories().iterator();
			while (bcInventoryIt.hasNext()) {
				Integer deletedFCCSegBCInvId = (Integer) bcInventoryIt.next();
				// byorn: 12-17-2007 added check to avoid null pointer error - this is very unlikely to happen though
				if (existingFCCSegBCInventories.get(deletedFCCSegBCInvId) != null) {
					FCCSegBCInventory currentFCCSegBCInventory = existingFCCSegBCInventories.get(deletedFCCSegBCInvId)
							.getFccSegBCInventory();
					// getFlightInventoryDAO().getFCCSegBCInventory(deletedFCCSegBCInvId);
					if (currentFCCSegBCInventory != null) {
						if (currentFCCSegBCInventory.getSeatsSold() > 0 || currentFCCSegBCInventory.getOnHoldSeats() > 0
								|| currentFCCSegBCInventory.getSeatsCancelled() > 0) {
							return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
									"Cannot delete bc inv having sold, onhold or cancelled seats. " + "[bookingcode="
											+ currentFCCSegBCInventory.getBookingCode() + "]" + "[sold seats="
											+ currentFCCSegBCInventory.getSeatsSold() + "] " + "[onhold seats="
											+ currentFCCSegBCInventory.getOnHoldSeats() + "] " + "[cancelled seats="
											+ currentFCCSegBCInventory.getSeatsCancelled() + "] ",
									new ModuleException("airinventory.logic.bl.bcinventory.delete.failed"),
									FCCSegInventoryUpdateStatusDTO.BC_INVENTORY_FAILURE,
									new Integer(currentFCCSegInventory.getFccsInvId()), currentFCCSegBCInventory.getFccsbInvId());
						}
						getFlightInventoryDAO().deleteFCCSegmentBCInventory(currentFCCSegBCInventory);
						
						if (isPublishAvailability) {
							// if GDS published BC, publish latest BC details to respective GDS carrier
							fccSegInventoryUpdateDTO.getFlightId();
							Flight flight = AirInventoryModuleUtils.getFlightBD().getFlight(
									fccSegInventoryUpdateDTO.getFlightId());
							Set<Integer> gdsIds = flight.getGdsIds();
							if (gdsIds != null && !gdsIds.isEmpty()) {
								Set<GDSBookingClass> gdsBCs = getBookingClassDAO().getBookingClass(
										currentFCCSegBCInventory.getBookingCode()).getGdsBCs();
								for (GDSBookingClass gdsBookingClass : gdsBCs) {
									if (gdsIds.contains(gdsBookingClass.getGdsId())) {
										PublishAvailability publishAvailability = new PublishAvailability();
										publishAvailability.setFccsaId(fccSegInventoryUpdateDTO.getFccsInventoryId());
										publishAvailability.setFccsbaId(currentFCCSegBCInventory.getFccsbInvId());
										publishAvailability.setSegAvailableSeats(0);
										publishAvailability.setBcAvailableSeats(0);
										publishAvailability.setBcStatus(FCCSegBCInventory.Status.CLOSED);
										publishAvailability
												.setEventCode(AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_CLOSED
														.getCode());
										publishAvailability
												.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT
														.getCode());
										publishAvailability.setEventTimestamp(new Date());
										publishAvailability.setBookingCode(currentFCCSegBCInventory.getBookingCode());
										publishAvailability.setFlightSegId(currentFCCSegInventory.getFlightSegId());
									}
								}
							}
						}
					}
				}
			}
		}
						

		if (fccSegInventoryUpdateDTO.getAddedFCCSegBCInventories() != null) {
			Set<FCCSegBCInventory> freshFCCSegBCInventories = checkAlreadyExistingBC(existingFCCSegBCInventories,
					fccSegInventoryUpdateDTO.getAddedFCCSegBCInventories());
			bcInventoryIt = freshFCCSegBCInventories.iterator();
			while (bcInventoryIt.hasNext()) {
				FCCSegBCInventory addedFCCSegBCInventory = (FCCSegBCInventory) bcInventoryIt.next();
				// set the default values
				addedFCCSegBCInventory.setSeatsSold(0);
				addedFCCSegBCInventory.setOnHoldSeats(0);
				addedFCCSegBCInventory.setSeatsCancelled(0);
				addedFCCSegBCInventory.setSeatsAcquired(0);
				addedFCCSegBCInventory.setWaitListedSeats(0);
				addedFCCSegBCInventory.setSeatsAvailable(addedFCCSegBCInventory.getSeatsAllocated());
				try {
					getFlightInventoryDAO().saveFCCSegmentBCInventory(addedFCCSegBCInventory);
					addedFCCSegBCInventoryIds.add(addedFCCSegBCInventory.getFccsbInvId());
				} catch (CommonsDataAccessException cdaex) {
					log.error("Adding bc inventory failed " + "[fccsInvId = " + addedFCCSegBCInventory.getfccsInvId() + ", "
							+ "bookingcode = " + addedFCCSegBCInventory.getBookingCode() + "]");

					return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
							"Adding bc inventory failed." + "[bookingcode=" + addedFCCSegBCInventory.getBookingCode() + "]",
							new ModuleException("airinventory.logic.bl.seginv.save.failed"),
							FCCSegInventoryUpdateStatusDTO.BC_INVENTORY_FAILURE,
							new Integer(currentFCCSegInventory.getFccsInvId()), null);
				}
			}
		}

		// check and update updated FCCSegBCInventories
		if (fccSegInventoryUpdateDTO.getUpdatedFCCSegBCInventories() != null) {
			bcInventoryIt = fccSegInventoryUpdateDTO.getUpdatedFCCSegBCInventories().iterator();
			while (bcInventoryIt.hasNext()) {
				FCCSegBCInventory updatedFCCSegBCInventory = (FCCSegBCInventory) bcInventoryIt.next();
				FCCSegBCInventory currentFCCSegBCInventory = existingFCCSegBCInventories
						.get(updatedFCCSegBCInventory.getFccsbInvId()).getFccSegBCInventory();
				updatedFCCSegBCInventory.setPreviousSeatsAllocated(currentFCCSegBCInventory.getSeatsAllocated());
				updatedFCCSegBCInventory.setPreviousWLAllocated(currentFCCSegBCInventory.getAllocatedWaitListSeats());
				updatedFCCSegBCInventory.setWaitListedSeats(currentFCCSegBCInventory.getWaitListedSeats());
				updatedFCCSegBCInventory.setOnHoldSeats(currentFCCSegBCInventory.getOnHoldSeats());

				// getFlightInventoryDAO().getFCCSegBCInventory(updatedFCCSegBCInventory.getFccsbInvId());
				int allocatedSeatsReduction = currentFCCSegBCInventory.getSeatsAllocated()
						+ currentFCCSegBCInventory.getSeatsAcquired() - updatedFCCSegBCInventory.getSeatsAllocated()
						- updatedFCCSegBCInventory.getSeatsAcquired();

				if (currentFCCSegBCInventory.getSeatsAvailable() < allocatedSeatsReduction) {
					return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
							"BC inventory seat allocation or acquired seat count is no longer valid. " + "[bookingcode="
									+ currentFCCSegBCInventory.getBookingCode() + "]" + "[Available seats="
									+ currentFCCSegBCInventory.getSeatsAvailable() + "] " + "[Requested allocation reduction="
									+ allocatedSeatsReduction + "]",
							new ModuleException("airinventory.logic.bl.bcalloc.invalid"),
							FCCSegInventoryUpdateStatusDTO.BC_INVENTORY_FAILURE,
							new Integer(currentFCCSegInventory.getFccsInvId()), currentFCCSegBCInventory.getFccsbInvId());
				}
				// Only changed attributes are updated, version check is removed to allow concurrent update!!!
				if (getFlightInventoryDAO().updateFCCSegmentBCInventory(updatedFCCSegBCInventory.getFlightId(),
						updatedFCCSegBCInventory.getLogicalCCCode(), updatedFCCSegBCInventory.getSegmentCode(),
						updatedFCCSegBCInventory.getBookingCode(), updatedFCCSegBCInventory.getSeatsAllocated(),
						updatedFCCSegBCInventory.getAllocatedWaitListSeats(), updatedFCCSegBCInventory.getSeatsAcquired(),
						updatedFCCSegBCInventory.getStatus(), updatedFCCSegBCInventory.getStatusChangeAction(),
						updatedFCCSegBCInventory.getPriorityFlag(), updatedFCCSegBCInventory.getVersion(), false) != 1) {
					return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
							"Updating bc inventory failed. BC Availability check violated." + "[bookingcode="
									+ currentFCCSegBCInventory.getBookingCode() + "]",
							new ModuleException("airinventory.logic.bl.segbcinv.update.failed"),
							FCCSegInventoryUpdateStatusDTO.BC_INVENTORY_FAILURE,
							new Integer(currentFCCSegInventory.getFccsInvId()), currentFCCSegBCInventory.getFccsbInvId());
				}
			}
		}

		if (getFlightInventoryDAO().updateFCCSegmentInventory(fccSegInventoryUpdateDTO.getFlightId(),
				fccSegInventoryUpdateDTO.getLogicalCCCode(), fccSegInventoryUpdateDTO.getSegmentCode(), userId) != 1) {
			return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
					"Updating segment inventory cumulative sums failed.",
					new ModuleException("airinventory.logic.bl.seginv.update.failed"),
					FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE, new Integer(currentFCCSegInventory.getFccsInvId()),
					null);
		}

		interceptingSegInventoryIds.add(fccSegInventoryUpdateDTO.getFccsInventoryId());
		if (getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(interceptingSegInventoryIds) == 0) {
			return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE,
					"Updating segment inventory availabilities failed. " + "[seg inventories tried to update ="
							+ interceptingSegInventoryIds + "]",
					new ModuleException("airinventory.logic.bl.seginv.update.failed"),
					FCCSegInventoryUpdateStatusDTO.SEGMENT_INVENTORY_FAILURE, new Integer(currentFCCSegInventory.getFccsInvId()),
					null);
		}
		
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();
		inventoryEventPublisher.add(fccSegInventoryUpdateDTO.getFccsInventoryId());
		inventoryEventPublisher.publishEvents();

		log.debug("final call to record Inv snapshot and update diff, status: "
				+ invDiffForAVS.takeFinalInvSnapshotAndPublishDiff());

		return new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_SUCCESS, "Save/Update operation is successful. ",
				null, null, new Integer(currentFCCSegInventory.getFccsInvId()), null);
		

	}

	/**
	 * Calculates additionally fixed seats on segment inventory
	 * 
	 * @param fccSegInventoryUpdateDTO
	 * @return additional fixed seats
	 */
	@SuppressWarnings("rawtypes")
	private int[] calculateNewOutstandingFixedAllocations(FCCSegInventoryUpdateDTO fccSegInventoryUpdateDTO,
			HashMap<Integer, ExtendedFCCSegBCInventory> existingFCCSegBCInventories) {
		int additionalFixedSeats = 0;
		int totalUnsoldFixedSeats = 0;

		List<String> fixedBookingCodesInUpdateDTO = new ArrayList<String>();

		if (fccSegInventoryUpdateDTO.getAddedFCCSegBCInventories() != null) {
			Iterator<FCCSegBCInventory> addedBCInventoriesIt = fccSegInventoryUpdateDTO.getAddedFCCSegBCInventories().iterator();
			while (addedBCInventoriesIt.hasNext()) {
				FCCSegBCInventory bcInventory = addedBCInventoriesIt.next();
				if (getBookingClassDAO().getLightWeightBookingClass(bcInventory.getBookingCode()).getFixedFlag()
						&& bcInventory.getStatus() != null && bcInventory.getStatus().equals(FCCSegBCInventory.Status.OPEN)) {
					fixedBookingCodesInUpdateDTO.add(bcInventory.getBookingCode());

					additionalFixedSeats += bcInventory.getSeatsAllocated();
					totalUnsoldFixedSeats += bcInventory.getSeatsAllocated();
				}
			}
		}

		Collection<Integer> addedDeletedBCInvIds = new ArrayList<Integer>();

		if (fccSegInventoryUpdateDTO.getDeletedFCCSegBCInventories() != null) {
			Iterator deletedBCInventoriesIt = fccSegInventoryUpdateDTO.getDeletedFCCSegBCInventories().iterator();
			while (deletedBCInventoriesIt.hasNext()) {
				Integer bcInventoryId = (Integer) deletedBCInventoriesIt.next();
				addedDeletedBCInvIds.add(bcInventoryId);
				FCCSegBCInventory deletedBCInventory = getFlightInventoryDAO().getFCCSegBCInventory(bcInventoryId);
				if (getBookingClassDAO().getLightWeightBookingClass(deletedBCInventory.getBookingCode()).getFixedFlag()
						&& deletedBCInventory.getStatus() != null
						&& deletedBCInventory.getStatus().equals(FCCSegBCInventory.Status.OPEN)) {
					fixedBookingCodesInUpdateDTO.add(deletedBCInventory.getBookingCode());
					additionalFixedSeats -= deletedBCInventory.getSeatsAvailable();
				}
			}
		}

		if (fccSegInventoryUpdateDTO.getUpdatedFCCSegBCInventories() != null) {
			Iterator updatedBCInventoriesIt = fccSegInventoryUpdateDTO.getUpdatedFCCSegBCInventories().iterator();
			while (updatedBCInventoriesIt.hasNext()) {
				FCCSegBCInventory bcInventory = (FCCSegBCInventory) updatedBCInventoriesIt.next();
				addedDeletedBCInvIds.add(bcInventory.getFccsbInvId());
				if (getBookingClassDAO().getLightWeightBookingClass(bcInventory.getBookingCode()).getFixedFlag()
						&& bcInventory.getStatus() != null && bcInventory.getStatus().equals(FCCSegBCInventory.Status.OPEN)) {
					fixedBookingCodesInUpdateDTO.add(bcInventory.getBookingCode());
					FCCSegBCInventory existingBCInv = getFlightInventoryDAO().getFCCSegBCInventory(bcInventory.getFccsbInvId());

					additionalFixedSeats += bcInventory.getSeatsAllocated() - existingBCInv.getSeatsAllocated();
					totalUnsoldFixedSeats += bcInventory.getSeatsAllocated()
							- (existingBCInv.getSeatsAllocated() - existingBCInv.getSeatsAvailable());
				}
			}
		}

		// find not updated bc inventories
		if (existingFCCSegBCInventories != null && existingFCCSegBCInventories.size() > 0) {
			Iterator existingFCCSegBCInvIdsIt = existingFCCSegBCInventories.keySet().iterator();
			while (existingFCCSegBCInvIdsIt.hasNext()) {
				Integer fccInvId = (Integer) existingFCCSegBCInvIdsIt.next();
				ExtendedFCCSegBCInventory exBCInv = existingFCCSegBCInventories.get(fccInvId);
				if (exBCInv.isFixedFlag() && !addedDeletedBCInvIds.contains(fccInvId) && exBCInv.getFccSegBCInventory() != null
						&& exBCInv.getFccSegBCInventory().getStatus().equals(FCCSegBCInventory.Status.OPEN)) {
					totalUnsoldFixedSeats += exBCInv.getFccSegBCInventory().getSeatsAvailable();
				}
			}
		}

		return new int[] { additionalFixedSeats, totalUnsoldFixedSeats };
	}

	private static boolean isRollFwdFCCSegInventory(InvRollForwardCriteriaDTO criteriaDTO) {
		if (criteriaDTO.getRollForwardCurtail() != 0 || criteriaDTO.getRollForwardOversell() != 0
				|| criteriaDTO.getRollForwardWaitListing() != 0) {
			return true;
		}
		return false;
	}

	private boolean isFCCSegInvUpdatable(FCCSegInventoryDTO fCCSegInventoryDTO, Map<String, StringBuffer> fltInventoryErrorMap)
			throws ModuleException {

		FCCSegInventory currentFCCSegInventory = getFlightInventoryDAO().getFCCSegInventory(fCCSegInventoryDTO.getFlightId(),
				fCCSegInventoryDTO.getLogicalCCCode(), fCCSegInventoryDTO.getSegmentCode());
		if (currentFCCSegInventory != null) {
			int additionalCurtailedSeats = fCCSegInventoryDTO.getCurtailedSeats() - currentFCCSegInventory.getCurtailedSeats();
			int oversellSeatsReduction = currentFCCSegInventory.getOversellSeats() - fCCSegInventoryDTO.getOversellSeats();
			int effectiveAllocReduction = additionalCurtailedSeats + oversellSeatsReduction;
			if (currentFCCSegInventory.getAvailableSeats() < effectiveAllocReduction) {
				StringBuffer errorLog = new StringBuffer();
				// effectiveAllocReduction < 0 means we are going to increase the availability. Its checked indirectly
				// here as availability is always > 0
				// This condition will fire if user increases curtail seats or user decreases over sell seats or both
				if (fCCSegInventoryDTO.getCurtailedSeats() > 0 && fCCSegInventoryDTO.getOversellSeats() > 0) {
					int availDiffWithCurtailSeats = currentFCCSegInventory.getAvailableSeats()
							- fCCSegInventoryDTO.getCurtailedSeats();
					if (availDiffWithCurtailSeats >= 0) {
						errorLog.append("Oversell seats requested - " + fCCSegInventoryDTO.getOversellSeats() + " adjusted - "
								+ availDiffWithCurtailSeats);
						fCCSegInventoryDTO.setOversellSeats(fCCSegInventoryDTO.getOversellSeats() + availDiffWithCurtailSeats);
					} else {
						errorLog.append("Curtail seats requested - " + fCCSegInventoryDTO.getCurtailedSeats() + " adjusted - "
								+ currentFCCSegInventory.getAvailableSeats() + " , oversell seats requested - "
								+ fCCSegInventoryDTO.getOversellSeats() + " adjusted - " + 0);
						fCCSegInventoryDTO.setCurtailedSeats(
								fCCSegInventoryDTO.getCurtailedSeats() + currentFCCSegInventory.getAvailableSeats());
						fCCSegInventoryDTO.setOversellSeats(0);
					}
				} else if (fCCSegInventoryDTO.getCurtailedSeats() > 0) {
					errorLog.append("Curtail seats requested - " + fCCSegInventoryDTO.getCurtailedSeats() + " adjusted - "
							+ currentFCCSegInventory.getAvailableSeats());
					fCCSegInventoryDTO.setCurtailedSeats(
							currentFCCSegInventory.getAvailableSeats() + currentFCCSegInventory.getCurtailedSeats());
				} else if (fCCSegInventoryDTO.getOversellSeats() > 0) {
					if (oversellSeatsReduction >= 0) {
						fCCSegInventoryDTO.setOversellSeats(
								currentFCCSegInventory.getOversellSeats() - currentFCCSegInventory.getAvailableSeats());
					} else {
						errorLog.append("Oversell seats requested - " + fCCSegInventoryDTO.getOversellSeats() + " adjusted - "
								+ currentFCCSegInventory.getAvailableSeats());
						fCCSegInventoryDTO.setOversellSeats(
								fCCSegInventoryDTO.getOversellSeats() - currentFCCSegInventory.getAvailableSeats());
					}
				} else {
					throw new ModuleException("");
				}
				String flightIdAndLogicalCC = fCCSegInventoryDTO.getFlightId() + "|" + fCCSegInventoryDTO.getLogicalCCCode();
				if (fltInventoryErrorMap.get(fCCSegInventoryDTO.getFlightId()) == null) {
					fltInventoryErrorMap.put(flightIdAndLogicalCC, errorLog);
				} else {
					fltInventoryErrorMap.get(flightIdAndLogicalCC).append(errorLog);
				}
			} // else nothing to do. We can directly update the inventory
			return true;
		}
		return false;
	}

	/**
	 * [TODO : implement] correct fixed quota seat eligibility check FUTURE IMPROVEMENT. STOP UNNESSARY DB CALLS WHEN
	 * SOURCE AND TARGET ARE THE SAME.....
	 */
	public InvRollforwardStatusDTO rollForwardFCCSegBCInventoriesNew(Collection<Integer> flightIds,
			InvRollForwardCriteriaDTO criteriaDTO, String userId) throws ModuleException {
		LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollforwardStatus = new LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO>();
		boolean isRollFwdFCCSegInventory = isRollFwdFCCSegInventory(criteriaDTO);
		String strRollFwdStatusMsg = "";
		boolean doAddFCCInvSegFailMsg = false;
		boolean isSegmentInvUpdated = false;

		/** load source BCs inventories depends on selected segment , logical cabin class and booking classes ) **/
		// Map<SegmentCode, Map<LogicalCabinClass, List<BC details>>>
		Map<String, Map<String, List<Object[]>>> sourceSegBCInventoriesMap = new HashMap<String, Map<String, List<Object[]>>>();
		if (!criteriaDTO.getInvRollForwardFlightsSearchCriteria().getSegmentVsSelectedBookingClassesMap().isEmpty()) {
			sourceSegBCInventoriesMap = getFlightInventoryDAO().getFCCSegmentBCInventoriesSummary(criteriaDTO.getSourceFlightId(),
					criteriaDTO.getSourceCabinClassCode(),
					criteriaDTO.getInvRollForwardFlightsSearchCriteria().getSegmentVsSelectedBookingClassesMap());
		}

		/**
		 * load source segment inventories depends on selected segment , logical cabin class if curtail and over sell
		 * allocations are updatable
		 **/
		Map<String, Map<String, Object[]>> sourceFCCSegInvMap = new HashMap<String, Map<String, Object[]>>();
		if (isRollFwdFCCSegInventory) {
			Collection<Integer> colSourceFlightIds = new ArrayList<Integer>();
			colSourceFlightIds.add(criteriaDTO.getSourceFlightId());
			List<Object[]> sourceFCCSegInventories = getFlightInventoryDAO().getFCCSegmentInventoriesSummary(colSourceFlightIds,
					criteriaDTO.getSourceCabinClassCode(),
					criteriaDTO.getInvRollForwardFlightsSearchCriteria().getSelectedSegmentCodesAndLogicalCainClasses());

			for (Object[] objFCCSegInvInfo : sourceFCCSegInventories) {
				String intFccSegmentCode = objFCCSegInvInfo[1].toString();
				String intFccLogicalCabinClassCode = objFCCSegInvInfo[5].toString();

				Map<String, Object[]> intFccInvMap = sourceFCCSegInvMap.get(intFccSegmentCode);
				if (intFccInvMap == null) {
					intFccInvMap = new HashMap<String, Object[]>();
					sourceFCCSegInvMap.put(intFccSegmentCode, intFccInvMap);
				}
				intFccInvMap.put(intFccLogicalCabinClassCode, objFCCSegInvInfo);
			}
		}

		/** load source flight logical cabin class inventory summary **/
		Collection<FLCCAllocationDTO> sourceFlightLccInventory = getFlightInventoryJDBCDAO()
				.getLogicalCabinClassWiseAllocations(criteriaDTO.getSourceFlightId(), criteriaDTO.getSourceCabinClassCode());
		Collection<FLCCAllocationDTO> sourceFlightAllLccInventories = new ArrayList<FLCCAllocationDTO>();
		/** remove unselected logical cabin classes **/
		Collection<FLCCAllocationDTO> unselectedLccs = new ArrayList<FLCCAllocationDTO>();
		List<String> selectedLogicalCabinClasses = criteriaDTO.getInvRollForwardFlightsSearchCriteria()
				.getSelectedLogicalCabinClasses();
		for (FLCCAllocationDTO flccAllocationDTO : sourceFlightLccInventory) {
			sourceFlightAllLccInventories.add(flccAllocationDTO);
			if (!selectedLogicalCabinClasses.contains(flccAllocationDTO.getLogicalCabinClassCode())) {
				unselectedLccs.add(flccAllocationDTO);
			}
		}
		sourceFlightLccInventory.removeAll(unselectedLccs);

		/** roll forward logical cabin classes and booking classes **/
		Object[] flightIdsArr = flightIds.toArray();
		int flightsPerRollOperation = 30;
		Map<String, StringBuffer> fltInventoryErrorMap = new HashMap<String, StringBuffer>();
		for (int page = 0; page < flightIds.size(); page += flightsPerRollOperation) {

			/** build page **/
			ArrayList<Integer> pagedFlightIds = new ArrayList<Integer>();
			for (int i = page; i < page + flightsPerRollOperation; i++) {
				if (i < flightIdsArr.length) {
					pagedFlightIds.add(Integer.parseInt(flightIdsArr[i].toString()));
				} else {
					break;
				}
			}

			if (pagedFlightIds.size() != 0) {

				/**
				 * roll forward FCC segment inventories for flights which do not have inventory records for the selected
				 * logical cabin classes, if the option is enabled.
				 **/
				int createSegAllocOption = criteriaDTO.getCreateSegAllocs();
				if (createSegAllocOption != InvRollForwardCriteriaDTO.SKIP_SPLITING_ALLOCATIONS) {
					this.rollForwardFCCSegInventory(pagedFlightIds, criteriaDTO.getSourceCabinClassCode(),
							sourceFlightAllLccInventories, createSegAllocOption, fltInventoryErrorMap);
				}

				/**
				 * load all available FCC segment inventories for selected flight ids , segmentCodes and logical cabin
				 * class, Should be ordered by the flight id to count the seg allocs for each flight.
				 **/
				List<FCCSegInventoryDTO> targetFCCSegInventories = getFlightInventoryDAO().getFCCSegmentInventoryAndBCSummary(
						pagedFlightIds, criteriaDTO.getSourceCabinClassCode(),
						criteriaDTO.getInvRollForwardFlightsSearchCriteria().getSegmentVsSelectedBookingClassesMap().keySet());
				
				InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();

				Iterator<FCCSegInventoryDTO> targetFCCSegInventoriesIt = targetFCCSegInventories.iterator();
				while (targetFCCSegInventoriesIt.hasNext()) {

					FCCSegInventoryDTO fccSegInventoryDTO = targetFCCSegInventoriesIt.next();
					String segmentCode = fccSegInventoryDTO.getSegmentCode();
					boolean needsAvailabilityUpdate = false;
					String segInvAudit = FlightInvRollResultsSummaryDTO.SEG_INV_UPDATE_SUCESS + " Oversell||Curtail||WaitList ";

					InventoryDiffForAVS invDiffForAVS = new InventoryDiffForAVS(fccSegInventoryDTO.getFccsInvId());
					log.debug("intial call to record Inv snapshot, status " + invDiffForAVS.takeIntialInvSnapshot());

					@SuppressWarnings("unchecked")
					LinkedHashMap<String, FCCSegBCInventory> bcInventoriesMap = fccSegInventoryDTO.getFccSegBCInventoriesMap();

					boolean targetHasBCInventories = bcInventoriesMap != null && bcInventoriesMap.size() > 0;

					/** Delete the booking classes for the Fcc segments **/
					if (bcInventoriesMap != null && !bcInventoriesMap.isEmpty()
							&& criteriaDTO.isDeleteTargetBCInventoriesFirst()) {
						if (!fccSegInventoryDTO.hasReservation()) {
							// delete existing booking class inventories of the fcc segment
							getFlightInventoryDAO().deleteFCCSegmentBCInventories(fccSegInventoryDTO.getFlightId(),
									fccSegInventoryDTO.getLogicalCCCode(), segmentCode, true);
						} else {
							// target bc inventories are not deleted as having reservation
							appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
									fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(),
									FlightInvRollResultsSummaryDTO.SEG_BC_DELETION_FAILED, null, null,
									targetHasBCInventories ? false : true);
							continue;
						}
					}

					/** Update over sell ,curtail allocations - 1 if only target is 0 2 always **/
					if (isRollFwdFCCSegInventory && sourceFCCSegInvMap != null && !sourceFCCSegInvMap.isEmpty()) {

						if (sourceFCCSegInvMap.get(fccSegInventoryDTO.getSegmentCode()) != null && sourceFCCSegInvMap
								.get(fccSegInventoryDTO.getSegmentCode()).get(fccSegInventoryDTO.getLogicalCCCode()) != null) {
							Object[] arrSourceFccSegInventory = sourceFCCSegInvMap.get(fccSegInventoryDTO.getSegmentCode())
									.get(fccSegInventoryDTO.getLogicalCCCode());

							int overSellSeatCount = (Integer) arrSourceFccSegInventory[3];
							int curtailSeatCount = (Integer) arrSourceFccSegInventory[4];
							int waitListingSeatCount = (Integer) arrSourceFccSegInventory[6];

							segInvAudit += "Prev:" + fccSegInventoryDTO.getOversellSeats() + "||"
									+ fccSegInventoryDTO.getCurtailedSeats() + "||" + fccSegInventoryDTO.getWaitListingSeats();

							// update over sell seat count
							if (criteriaDTO.getRollForwardOversell() == 2) {
								needsAvailabilityUpdate = true;
								fccSegInventoryDTO.setOversellSeats(overSellSeatCount);
							} else if (criteriaDTO.getRollForwardOversell() == 1 && fccSegInventoryDTO.getOversellSeats() == 0) {
								needsAvailabilityUpdate = true;
								fccSegInventoryDTO.setOversellSeats(overSellSeatCount);
							}

							// update curtail seat count
							if (criteriaDTO.getRollForwardCurtail() == 2) {
								needsAvailabilityUpdate = true;
								fccSegInventoryDTO.setCurtailedSeats(curtailSeatCount);
							} else if (criteriaDTO.getRollForwardCurtail() == 1 && fccSegInventoryDTO.getCurtailedSeats() == 0) {
								needsAvailabilityUpdate = true;
								fccSegInventoryDTO.setCurtailedSeats(curtailSeatCount);
							}
							// update wait listed seats allocation
							if (criteriaDTO.getRollForwardWaitListing() == 2) {
								needsAvailabilityUpdate = true;
								fccSegInventoryDTO.setWaitListingSeats(waitListingSeatCount);
							} else if (criteriaDTO.getRollForwardWaitListing() == 1
									&& fccSegInventoryDTO.getWaitListingSeats() == 0) {
								needsAvailabilityUpdate = true;
								fccSegInventoryDTO.setWaitListingSeats(waitListingSeatCount);
							}
						}
					}

					/** process booking class inventories **/

					if (!sourceSegBCInventoriesMap.isEmpty()) {

						List<Object[]> sourceBCInventories = sourceSegBCInventoriesMap.get(segmentCode)
								.get(fccSegInventoryDTO.getLogicalCCCode());
						if (sourceBCInventories != null && !sourceBCInventories.isEmpty()) {

							Iterator<Object[]> sourceBCInventoriesItr = sourceBCInventories.iterator();
							Integer totalSourceFixedSeatCount = 0;
							boolean totalSourceFixedSeatCountHigherThanNew = false;
							while (sourceBCInventoriesItr.hasNext()) {
								Object[] sourceBCInv = sourceBCInventoriesItr.next();
								if ((sourceBCInv[7] != null
										&& ((String) sourceBCInv[7]).equals(BookingClass.FIXED_FLAG_Y)) == true) {
									totalSourceFixedSeatCount += ((Integer) sourceBCInv[2]).intValue();
								}
							}
							if (totalSourceFixedSeatCount > fccSegInventoryDTO.getAvailableSeats()) {
								totalSourceFixedSeatCountHigherThanNew = true;
							}

							if (!totalSourceFixedSeatCountHigherThanNew) {
								Collection<Integer> unUtilizedBCInveIds = new ArrayList<Integer>();
								HashMap<Integer, String> mapBCCode = new HashMap<Integer, String>();
								if (!criteriaDTO.isDeleteTargetBCInventoriesFirst()
										&& fccSegInventoryDTO.getFccSegBCInventoriesMap() != null) {
									@SuppressWarnings("unchecked")
									Collection<FCCSegBCInventory> colFccBCInv = fccSegInventoryDTO.getFccSegBCInventoriesMap()
											.values();
									for (FCCSegBCInventory fccSegBCInventory : colFccBCInv) {
										boolean isExposedToGDS = false;
										isExposedToGDS = AirInventoryModuleUtils.getLocalFlightInventoryBD()
												.isExposedToGDS(fccSegBCInventory.getFccsbInvId());

										if (!fccSegBCInventory.hasReservations() && !isExposedToGDS) {
											unUtilizedBCInveIds.add(fccSegBCInventory.getFccsbInvId());
										}
										mapBCCode.put(fccSegBCInventory.getFccsbInvId(), fccSegBCInventory.getBookingCode());
									}
								}
								Collection<Integer> updatedBCInvIds = new ArrayList<Integer>();
								Iterator<Object[]> sourceBCInventoriesIt = sourceBCInventories.iterator();
								while (sourceBCInventoriesIt.hasNext()) {
									Object[] sourceBCInventory = sourceBCInventoriesIt.next();
									String bookingCode = sourceBCInventory[1].toString();

									if (!criteriaDTO.isDeleteTargetBCInventoriesFirst()
											&& fccSegInventoryDTO.getFccSegBCInventoriesMap() != null
											&& fccSegInventoryDTO.getFccSegBCInventoriesMap().containsKey(bookingCode)) {
										// source booking class exist in the target
										FCCSegBCInventory fccSegBCInventory = (FCCSegBCInventory) fccSegInventoryDTO
												.getFccSegBCInventoriesMap().get(bookingCode);
										updatedBCInvIds.add(fccSegBCInventory.getFccsbInvId());
										if (criteriaDTO
												.getBcInventoryOverridingLevel() != InvRollForwardCriteriaDTO.OVERRIDE_NONE) {
											if (fccSegBCInventory.hasReservations()) {
												if (criteriaDTO
														.getBcInventoryOverridingLevel() == InvRollForwardCriteriaDTO.OVERRIDE_ALL) {
													fccSegBCInventory.setPriorityFlag(setBCPrority(
															criteriaDTO.isRollFwdBCPriority(), (String) sourceBCInventory[4],
															fccSegBCInventory.getPriorityFlag()));
													fccSegBCInventory.setStatus(setBCStatus(criteriaDTO.isRollFwdBCStatus(),
															(String) sourceBCInventory[5], fccSegBCInventory.getStatus()));
													if (criteriaDTO.isRollFwdBCStatus()) {
														fccSegBCInventory.setStatusChangeAction((String) sourceBCInventory[8]);
													}
													needsAvailabilityUpdate = updateFCCSegBCInventory(fccSegBCInventory,
															sourceBCInventory, fccSegInventoryDTO, rollforwardStatus,
															targetHasBCInventories);
												} else {
													// bc inventory has reservations and override set false
													appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(),
															fccSegBCInventory.getFlightId(), fccSegBCInventory.getSegmentCode(),
															null, fccSegBCInventory.getBookingCode(),
															FlightInvRollResultsSummaryDTO.BC_NOT_UPDATED,
															targetHasBCInventories ? false : true);
												}
											} else {
												if (criteriaDTO
														.getBcInventoryOverridingLevel() == InvRollForwardCriteriaDTO.OVERRIDE_ALL
														|| criteriaDTO
																.getBcInventoryOverridingLevel() == InvRollForwardCriteriaDTO.OVERRIDE_NOT_HAVING_RESERVATION_ONLY) {
													fccSegBCInventory.setPriorityFlag(setBCPrority(
															criteriaDTO.isRollFwdBCPriority(), (String) sourceBCInventory[4],
															fccSegBCInventory.getPriorityFlag()));
													fccSegBCInventory.setStatus(setBCStatus(criteriaDTO.isRollFwdBCStatus(),
															(String) sourceBCInventory[5], fccSegBCInventory.getStatus()));
													if (criteriaDTO.isRollFwdBCStatus()) {
														fccSegBCInventory.setStatusChangeAction((String) sourceBCInventory[8]);
													}
													needsAvailabilityUpdate = updateFCCSegBCInventory(fccSegBCInventory,
															sourceBCInventory, fccSegInventoryDTO, rollforwardStatus,
															targetHasBCInventories);
												}
											}
										} else {
											boolean isNeedStatusUpdate = (sourceBCInventory[5] != null
													&& !sourceBCInventory[5].toString().equals(fccSegBCInventory.getStatus()));

											String priorityFlag = "N";
											if (fccSegBCInventory.getPriorityFlag()) {
												priorityFlag = "Y";
											}
											boolean isNeedPriorityUpdate = (sourceBCInventory[4] != null
													&& !sourceBCInventory[4].toString().equals(priorityFlag));
											if ((criteriaDTO.isRollFwdBCStatus() || criteriaDTO.isRollFwdBCPriority())
													&& (isNeedStatusUpdate || isNeedPriorityUpdate)) {
												if (isNeedPriorityUpdate && criteriaDTO.isRollFwdBCPriority()) {
													fccSegBCInventory.setPriorityFlag(
															sourceBCInventory[4].toString().equalsIgnoreCase("Y"));
												}
												if (isNeedStatusUpdate && criteriaDTO.isRollFwdBCStatus()) {
													fccSegBCInventory.setStatus((String) sourceBCInventory[5]);
													fccSegBCInventory.setStatusChangeAction((String) sourceBCInventory[8]);
												}
												needsAvailabilityUpdate = updateFCCSegBCFlags(fccSegBCInventory,
														sourceBCInventory, rollforwardStatus);
											} else {
												// bc not overriden
												appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(),
														fccSegBCInventory.getFlightId(), fccSegBCInventory.getSegmentCode(), null,
														fccSegBCInventory.getBookingCode(),
														FlightInvRollResultsSummaryDTO.BC_NOT_UPDATED,
														targetHasBCInventories ? false : true);
											}
										}

									} else {
										// Doesn't exist a booking class since then add new booking class
										boolean fixedFlag = (sourceBCInventory[7] != null
												&& ((String) sourceBCInventory[7]).equals(BookingClass.FIXED_FLAG_Y))
														? true
														: false;
										if (fixedFlag) {
											if (((Integer) sourceBCInventory[2]).intValue() <= fccSegInventoryDTO
													.getAvailableSeats()) {
												FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
												fccSegBCInventory.setBookingCode(bookingCode);
												fccSegBCInventory.setfccsInvId(fccSegInventoryDTO.getFccsInvId());
												fccSegBCInventory.setFlightId(fccSegInventoryDTO.getFlightId());
												fccSegBCInventory.setLogicalCCCode(fccSegInventoryDTO.getLogicalCCCode());
												fccSegBCInventory.setSegmentCode(segmentCode);
												fccSegBCInventory.setSeatsAllocated(((Integer) sourceBCInventory[2]).intValue());
												fccSegBCInventory.setSeatsAvailable(((Integer) sourceBCInventory[2]).intValue());
												fccSegBCInventory.setSeatsSold(0);
												fccSegBCInventory.setOnHoldSeats(0);
												fccSegBCInventory.setSeatsAcquired(0);
												fccSegBCInventory.setSeatsCancelled(0);
												fccSegBCInventory.setPriorityFlag(
														(sourceBCInventory[4] != null && criteriaDTO.isRollFwdBCPriority()
																&& ((String) sourceBCInventory[4]).equalsIgnoreCase("Y"))
																		? true
																		: false);
												fccSegBCInventory.setStatus(
														(sourceBCInventory[5] != null && criteriaDTO.isRollFwdBCStatus()
																&& ((String) sourceBCInventory[5]).equals("CLS"))
																		? FCCSegBCInventory.Status.CLOSED
																		: FCCSegBCInventory.Status.OPEN);

												fccSegBCInventory.setStatusChangeAction((criteriaDTO.isRollFwdBCStatus()
														? (String) sourceBCInventory[8]
														: FCCSegBCInventory.StatusAction.CREATION));

												getFlightInventoryDAO().saveFCCSegmentBCInventory(fccSegBCInventory);

												appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
														fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(),
														null, bookingCode, FlightInvRollResultsSummaryDTO.BC_CREATED,
														targetHasBCInventories ? false : true);
												needsAvailabilityUpdate = true;
											} else {
												// fixed allocation is more than available seats on the segment
												appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
														fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(),
														null, bookingCode, FlightInvRollResultsSummaryDTO.BC_UPDATE_FAILED,
														targetHasBCInventories ? false : true);
											}
										} else {
											FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
											fccSegBCInventory.setBookingCode(bookingCode);
											fccSegBCInventory.setfccsInvId(fccSegInventoryDTO.getFccsInvId());
											fccSegBCInventory.setFlightId(fccSegInventoryDTO.getFlightId());
											fccSegBCInventory.setLogicalCCCode(fccSegInventoryDTO.getLogicalCCCode());
											fccSegBCInventory.setSegmentCode(segmentCode);
											fccSegBCInventory.setSeatsAllocated(((Integer) sourceBCInventory[2]).intValue());
											fccSegBCInventory.setSeatsAvailable(((Integer) sourceBCInventory[2]).intValue());
											fccSegBCInventory.setSeatsSold(0);
											fccSegBCInventory.setOnHoldSeats(0);
											fccSegBCInventory.setSeatsAcquired(0);
											fccSegBCInventory.setSeatsCancelled(0);
											fccSegBCInventory.setPriorityFlag(
													(sourceBCInventory[4] != null && criteriaDTO.isRollFwdBCPriority()
															&& ((String) sourceBCInventory[4]).equalsIgnoreCase("Y"))
																	? true
																	: false);
											fccSegBCInventory
													.setStatus((sourceBCInventory[5] != null && criteriaDTO.isRollFwdBCStatus()
															&& ((String) sourceBCInventory[5]).equals("CLS"))
																	? FCCSegBCInventory.Status.CLOSED
																	: FCCSegBCInventory.Status.OPEN);

											fccSegBCInventory.setStatusChangeAction((criteriaDTO.isRollFwdBCStatus()
													? (String) sourceBCInventory[8]
													: FCCSegBCInventory.StatusAction.CREATION)); // TODO TEST THIS

											getFlightInventoryDAO().saveFCCSegmentBCInventory(fccSegBCInventory);
											String createdStatus = FlightInvRollResultsSummaryDTO.BC_CREATED + " New:"
													+ sourceBCInventory[2];

											appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
													fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(), null,
													bookingCode, createdStatus, targetHasBCInventories ? false : true);
										}
									}
								}
								// end while

								if (updatedBCInvIds.size() > 0) {
									unUtilizedBCInveIds.removeAll(updatedBCInvIds);
								}
								// kill BCs not in source and sold seat seat count is zero
								if (criteriaDTO.isRemoveOddTargetBCs() && !criteriaDTO.isDeleteTargetBCInventoriesFirst()
										&& unUtilizedBCInveIds.size() > 0) {
									getFlightInventoryDAO().deleteFCCSegBCInventory(unUtilizedBCInveIds);
									// audit
									for (Integer intFccsbInvId : unUtilizedBCInveIds) {
										String strBookingCode = mapBCCode.get(intFccsbInvId);
										appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
												fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(), null,
												strBookingCode, FlightInvRollResultsSummaryDTO.BC_REMOVED,
												targetHasBCInventories ? false : true);
									}
								}
							}
						}
					}
					if (needsAvailabilityUpdate) {
						// update cumulative sums on segment
						getFlightInventoryDAO().updateFCCSegmentInventory(fccSegInventoryDTO.getFlightId(),
								fccSegInventoryDTO.getLogicalCCCode(), segmentCode, userId);

						// update availability in intercepting segments
						List<Integer> interceptiongFCCSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(
								fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getLogicalCCCode(), segmentCode, true);
						if (interceptiongFCCSegInvIds != null && interceptiongFCCSegInvIds.size() > 0) {
							getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(interceptiongFCCSegInvIds);
						}
					}

					// do this after updating availability
					if (isFCCSegInvUpdatable(fccSegInventoryDTO, fltInventoryErrorMap)) {
						int FCCSegInvUpdateStatus = getFlightInventoryDAO().updateOversellCurtailFCCSegmentInventory(
								fccSegInventoryDTO.getFccsInvId(), fccSegInventoryDTO.getOversellSeats(),
								fccSegInventoryDTO.getCurtailedSeats(), fccSegInventoryDTO.getWaitListingSeats());
						if (FCCSegInvUpdateStatus != 1) {
							doAddFCCInvSegFailMsg = true;
							appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
									fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(), null, null,
									FlightInvRollResultsSummaryDTO.SEG_INV_UPDATE_FAILED, targetHasBCInventories ? false : true,
									String.valueOf(fccSegInventoryDTO.getCurtailedSeats()),
									String.valueOf(fccSegInventoryDTO.getOversellSeats()));

						} else {
							/*
							 * call update availability again to support new oversell curtail values. not nice. Best
							 * solution is to keep a virtual FccSegInventoryDTO do the validations against it and update
							 * in the end
							 */
							List<Integer> interceptingfCCSegInvIds = getFlightInventoryDAO()
									.getInterceptingFCCSegmentInventoryIds(fccSegInventoryDTO.getFlightId(),
											fccSegInventoryDTO.getLogicalCCCode(), segmentCode, true);
							List<Integer> fCCSegInvIds = new ArrayList<Integer>(interceptingfCCSegInvIds);
							fCCSegInvIds.add(new Integer(fccSegInventoryDTO.getFccsInvId()));
							if (fCCSegInvIds != null && fCCSegInvIds.size() > 0) {
								getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(fCCSegInvIds);
							}
							isSegmentInvUpdated = (criteriaDTO.getRollForwardCurtail() != 0
									&& criteriaDTO.getRollForwardOversell() != 0);
							segInvAudit += " New:" + fccSegInventoryDTO.getOversellSeats() + "||"
									+ fccSegInventoryDTO.getCurtailedSeats() + "||" + fccSegInventoryDTO.getWaitListingSeats();
							appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
									fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(), segInvAudit, null,
									null, targetHasBCInventories ? false : true,
									String.valueOf(fccSegInventoryDTO.getCurtailedSeats()),
									String.valueOf(fccSegInventoryDTO.getOversellSeats()));
						}
					}

					appendRollForwardStatus(rollforwardStatus, fccSegInventoryDTO.getFccsInvId(),
							fccSegInventoryDTO.getFlightId(), fccSegInventoryDTO.getSegmentCode(),
							FlightInvRollResultsSummaryDTO.SEG_COMPLETED_WITHOUT_ERROR, null, null,
							targetHasBCInventories ? false : true);

					log.debug("final call to record Inv snapshot and update diff, status: "
							+ invDiffForAVS.takeFinalInvSnapshotAndPublishDiff());
					
					inventoryEventPublisher.add(fccSegInventoryDTO.getFccsInvId());

				} // end while

				inventoryEventPublisher.publishEvents();
			}		
		}
		
		InvRollforwardStatusDTO rollStatus = new InvRollforwardStatusDTO(-1, "", null);

		// write roll forward error messages
		rollStatus.setFltInventoryErrorMap(fltInventoryErrorMap);
		rollStatus.setRollforwardStatus(rollforwardStatus);

		// write roll forward status to file
		Iterator<FlightInvRollResultsSummaryDTO> rollStatusCollIt = rollforwardStatus.values().iterator();
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMMdd_HHmm");
		StringBuilder strLogOutput = new StringBuilder();
		BufferedWriter out = null;
		try {
			File auditsDir = new File(PlatformConstants.getAbsAuditLogsPath());
			if (!auditsDir.exists()) {
				auditsDir.mkdir();
			}
			out = new BufferedWriter(new FileWriter(PlatformConstants.getAbsAuditLogsPath() + "/InventoryRollForwardStatusReport_"
					+ dateFormat.format(date) + ".txt"));
			out.write("----------------Inventory Rollforwarding Status Report-----------------");
			out.newLine();
			out.write("----------------RollForward Criteria -----------------");
			final String BR = "\n";
			StringBuilder strRollFwdCriteria = new StringBuilder();
			strRollFwdCriteria.append(criteriaDTO.getInvRollForwardFlightsSearchCriteria().toString()).append(BR);
			strRollFwdCriteria.append(criteriaDTO.toString()).append(BR);
			out.write(strRollFwdCriteria.toString());
			out.newLine();
			out.write("---------------- Affected Flights -----------------");
			out.newLine();
			out.write("FltSegmentId:FlightId:SegmentCode:[BC Inventory Roll Status]");
			out.newLine();
			while (rollStatusCollIt.hasNext()) {
				FlightInvRollResultsSummaryDTO status = rollStatusCollIt.next();
				out.write(status.getSummary());
				out.newLine();
				// identify flights need status updating
				Integer flightId = new Integer(status.getFlightId());
				if (status.isNeedsFlightStatusUpdating()) {
					if (!rollStatus.getFlightIdsForStatusUpdating().contains(flightId)) {
						rollStatus.getFlightIdsForStatusUpdating().add(flightId);
					}
				} else {
					if (rollStatus.getFlightIdsForStatusUpdating().contains(flightId)) {
						rollStatus.getFlightIdsForStatusUpdating().remove(flightId);
					}
				}
				strLogOutput.append(status.getFormattedSummary());
			}
			out.write("----------------------------D---O---N---E------------------------------");
			out.newLine();
			out.flush();
			out.close();

			rollStatus.setStrLog(strLogOutput);
		} catch (IOException e) {
			return new InvRollforwardStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "Inventory rollforwarding failed", e);
		}

		rollStatus.setStatus(OperationStatusDTO.OPERATION_COMPLETED);
		if (doAddFCCInvSegFailMsg) {
			strRollFwdStatusMsg = ". But failed to update some curtailed oversell values.";
		}
		String strSegInv = "";
		if (isSegmentInvUpdated && !doAddFCCInvSegFailMsg) {
			strSegInv = "including curtail and over sell update is";
		}
		rollStatus.setMsg(
				"Inventory rollforwarding " + strSegInv + " completed, " + strRollFwdStatusMsg + " See audit log for details");

		return rollStatus;
	}

	private boolean updateFCCSegBCFlags(FCCSegBCInventory fccSegBCInventory, Object[] sourceBCInventory,
			LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollforwardStatus) {
		boolean needsAvailabilityUpdate = false;
		if (getFlightInventoryDAO().updateDedicatedFCCSegmentBCInventory(fccSegBCInventory.getFlightId(),
				fccSegBCInventory.getLogicalCCCode(), fccSegBCInventory.getSegmentCode(), fccSegBCInventory.getBookingCode(),
				((Integer) sourceBCInventory[2]).intValue(), fccSegBCInventory.getSeatsAcquired(), fccSegBCInventory.getStatus(),
				sourceBCInventory[8].toString(), fccSegBCInventory.getPriorityFlag(), fccSegBCInventory.getVersion(),
				false) == 1) {
			// success
			appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(), fccSegBCInventory.getFlightId(),
					fccSegBCInventory.getSegmentCode(), null, fccSegBCInventory.getBookingCode(),
					FlightInvRollResultsSummaryDTO.BC_UPDATED, false);
			needsAvailabilityUpdate = true;
		} else {
			// update failed
			appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(), fccSegBCInventory.getFlightId(),
					fccSegBCInventory.getSegmentCode(), null, fccSegBCInventory.getBookingCode(),
					FlightInvRollResultsSummaryDTO.BC_UPDATE_FAILED, false);
		}
		return needsAvailabilityUpdate;
	}

	private boolean updateFCCSegBCInventory(FCCSegBCInventory fccSegBCInventory, Object[] sourceBCInventory,
			FCCSegInventoryDTO fccSegInventoryDTO, LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollforwardStatus,
			boolean targetHasBCInventories) {
		boolean needsAvailabilityUpdate = false;
		boolean fixedFlag = ((String) sourceBCInventory[7]).equals(BookingClass.FIXED_FLAG_Y) ? true : false;
		int allocationReduction = fccSegBCInventory.getSeatsAllocated() - ((Integer) sourceBCInventory[2]).intValue();
		String updatedStatus = FlightInvRollResultsSummaryDTO.BC_UPDATED + " Prev:" + fccSegBCInventory.getSeatsAllocated() + "||"
				+ (fccSegBCInventory.getPriorityFlag() ? "Y" : "N") + "||" + fccSegBCInventory.getStatus() + " New:"
				+ sourceBCInventory[2] + "||" + sourceBCInventory[4] + "||" + sourceBCInventory[5];
		// if (allocationReduction <= ((Integer)sourceBCInventory[3]).intValue()){
		if (allocationReduction <= fccSegBCInventory.getSeatsAvailable()) {
			if (fixedFlag) {
				// TODO: calculate allowable fixed allocation
				if ((-allocationReduction) <= fccSegInventoryDTO.getAvailableSeats()) {// this check is not correct!!!
					if (getFlightInventoryDAO().updateFCCSegmentBCInventory(fccSegBCInventory.getFlightId(),
							fccSegBCInventory.getLogicalCCCode(), fccSegBCInventory.getSegmentCode(),
							fccSegBCInventory.getBookingCode(), ((Integer) sourceBCInventory[2]).intValue(),
							((Integer) sourceBCInventory[11]).intValue(), fccSegBCInventory.getSeatsAcquired(),
							fccSegBCInventory.getStatus(), fccSegBCInventory.getStatusChangeAction(),
							fccSegBCInventory.getPriorityFlag(), fccSegBCInventory.getVersion(), false) == 1) {
						// success, needs updating segment inventory and intercepting segment inventories
						appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(),
								fccSegBCInventory.getFlightId(), fccSegBCInventory.getSegmentCode(), null,
								fccSegBCInventory.getBookingCode(), updatedStatus, targetHasBCInventories ? false : true);
						needsAvailabilityUpdate = true;
					} else {
						// update failed
						appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(),
								fccSegBCInventory.getFlightId(), fccSegBCInventory.getSegmentCode(), null,
								fccSegBCInventory.getBookingCode(), FlightInvRollResultsSummaryDTO.BC_UPDATE_FAILED,
								targetHasBCInventories ? false : true);
					}
				} else {
					// additional fixed allocation is more than available seats on the segment
					appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(), fccSegBCInventory.getFlightId(),
							fccSegBCInventory.getSegmentCode(), null, fccSegBCInventory.getBookingCode(),
							FlightInvRollResultsSummaryDTO.BC_UPDATE_FAILED, targetHasBCInventories ? false : true);
				}
			} else {
				if (getFlightInventoryDAO().updateFCCSegmentBCInventory(fccSegBCInventory.getFlightId(),
						fccSegBCInventory.getLogicalCCCode(), fccSegBCInventory.getSegmentCode(),
						fccSegBCInventory.getBookingCode(), ((Integer) sourceBCInventory[2]).intValue(),
						((Integer) sourceBCInventory[11]).intValue(), fccSegBCInventory.getSeatsAcquired(),
						fccSegBCInventory.getStatus(), fccSegBCInventory.getStatusChangeAction(),
						fccSegBCInventory.getPriorityFlag(), fccSegBCInventory.getVersion(), false) == 1) {
					// success
					appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(), fccSegBCInventory.getFlightId(),
							fccSegBCInventory.getSegmentCode(), null, fccSegBCInventory.getBookingCode(), updatedStatus,
							targetHasBCInventories ? false : true);
				} else {
					// update failed
					appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(), fccSegBCInventory.getFlightId(),
							fccSegBCInventory.getSegmentCode(), null, fccSegBCInventory.getBookingCode(),
							FlightInvRollResultsSummaryDTO.BC_UPDATE_FAILED, targetHasBCInventories ? false : true);
				}
			}
		} else {
			// allocation reduction is more than available seats
			appendRollForwardStatus(rollforwardStatus, fccSegBCInventory.getfccsInvId(), fccSegBCInventory.getFlightId(),
					fccSegBCInventory.getSegmentCode(), null, fccSegBCInventory.getBookingCode(),
					FlightInvRollResultsSummaryDTO.BC_UPDATE_FAILED, targetHasBCInventories ? false : true);
		}
		return needsAvailabilityUpdate;
	}

	private void appendRollForwardStatus(LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollForwardStatus,
			int fccSegInvId, int flightId, String segmentCode, String segStatus, String bookingCode, String bcStatus,
			boolean needFlightStatusUpdating) {
		appendRollForwardStatus(rollForwardStatus, fccSegInvId, flightId, segmentCode, segStatus, bookingCode, bcStatus,
				needFlightStatusUpdating, null, null);
	}

	private void appendRollForwardStatus(LinkedHashMap<Integer, FlightInvRollResultsSummaryDTO> rollForwardStatus,
			int fccSegInvId, int flightId, String segmentCode, String segStatus, String bookingCode, String bcStatus,
			boolean needFlightStatusUpdating, String curtail, String overSell) {
		try {
			FlightInvRollResultsSummaryDTO status = rollForwardStatus.get(new Integer(fccSegInvId));
			if (status == null) {
				status = new FlightInvRollResultsSummaryDTO();
				rollForwardStatus.put(new Integer(fccSegInvId), status);
			}
			boolean isDescriptiveAuditEnabled = AppSysParamsUtil.isDescriptiveInvenoryAuditEnabled();
			FlightSegmentDTO segment = null;
			if (isDescriptiveAuditEnabled && segmentCode != null) {
				AuditExternalDataProvider dataProvider = AuditExternalDataProvider.getInstance();
				segment = dataProvider.getCachedFlightSegmentInfo(flightId, segmentCode);
				status.setFlightNo(segment.getFlightNumber());
				status.setDepartureDateTime(segment.getDepartureDateTime());
			}

			status.setFccSegInvId(fccSegInvId);
			status.setFlightId(flightId);
			status.setSegmentCode(segmentCode);
			if (!status.isNeedsFlightStatusUpdating() && needFlightStatusUpdating) {
				status.setNeedsFlightStatusUpdating(needFlightStatusUpdating);
			}
			if (segStatus != null && !segStatus.equals("")) {
				status.setOperationStatus(segStatus);
			}
			if (bookingCode != null) {
				status.addBCRollStatus(bookingCode, bcStatus);
			}
			if (curtail != null) {
				status.setCurtail(curtail);
			}
			if (overSell != null) {
				status.setOverSel(overSell);
			}
		} catch (ModuleException exp) {
			log.error("AUDIT ERROR AT appendRollForwardStatus " + "[flightId = " + flightId + ", " + "segmentCode = "
					+ segmentCode + "fccSegInvId = " + fccSegInvId + "]");
		}
	}

	public Collection<FCCInventoryDTO> getFCCInventoryDTOsNew(int flightId, String cabinClassCode, Integer overlappingFlightId,
			boolean includeCnxFlts) {
		Collection<FCCInventoryDTO> fccInvDTOs = getFlightInventoryJdbcDAO().getFCCInventoryDTOs(flightId, cabinClassCode,
				overlappingFlightId, includeCnxFlts);

		// set effective fares and agents count against each booking code
		if (fccInvDTOs != null) {
			for (FCCInventoryDTO fccInventoryDTO : fccInvDTOs) {
				for (FCCSegInventoryDTO fccSegInventoryDTO : fccInventoryDTO.getFccSegInventoryDTOs()) {
					String segmentCode = fccSegInventoryDTO.getSegmentCode();
					LinkedHashMap<String, BCONDFaresAgentsSummaryDTO> bcONDFaresAgentsSummaryMap = getBookingClassJdbcDAO()
							.getBCONDFaresAgentsSummaryMap(fccSegInventoryDTO.getFlightId(), segmentCode,
									fccSegInventoryDTO.getLogicalCCCode());
					if (bcONDFaresAgentsSummaryMap != null) {
						for (ExtendedFCCSegBCInventory fccSegBCInv : fccSegInventoryDTO.getFccSegBCInventories()) {
							String bookingCode = fccSegBCInv.getFccSegBCInventory().getBookingCode();
							BCONDFaresAgentsSummaryDTO bcOndFaresAgentsSummaryDTO = bcONDFaresAgentsSummaryMap.get(bookingCode);
							if (bcOndFaresAgentsSummaryDTO != null) {
								fccSegBCInv.setLinkedEffectiveAgents(bcOndFaresAgentsSummaryDTO.getLinkedAgentIds(segmentCode));
								fccSegBCInv.setLinkedEffectiveFares(bcOndFaresAgentsSummaryDTO.getLinkedFares(segmentCode));
							}
						}
					}
				}
			}
		}

		if (overlappingFlightId != null) {
			// do reshuffling of the record
			Iterator<FCCInventoryDTO> fccSegInvDTOsIt = fccInvDTOs.iterator();
			FCCInventoryDTO fccInventoryDTO1 = fccSegInvDTOsIt.next();
			FCCInventoryDTO fccInventoryDTO2 = fccSegInvDTOsIt.next();

			// setting overlapping fccInventoryDTO as second element of the collection
			if (fccInventoryDTO1.getFlightId() != flightId) {
				FCCInventoryDTO temp;
				temp = fccInventoryDTO1;
				fccInventoryDTO1 = fccInventoryDTO2;
				fccInventoryDTO2 = temp;
			}

			// rearranging the segment inventories
			Collection<FCCSegInventoryDTO> fccSegInventoryDTOs1 = fccInventoryDTO1.getFccSegInventoryDTOs();
			Collection<FCCSegInventoryDTO> fccSegInventoryDTOs2 = fccInventoryDTO2.getFccSegInventoryDTOs();
			List<FCCSegInventoryDTO> segInvCol1 = new ArrayList<FCCSegInventoryDTO>();
			List<FCCSegInventoryDTO> segInvOverlapCol1 = new ArrayList<FCCSegInventoryDTO>();
			List<FCCSegInventoryDTO> segInvCol2 = new ArrayList<FCCSegInventoryDTO>();
			List<FCCSegInventoryDTO> segInvOverlapCol2 = new ArrayList<FCCSegInventoryDTO>();
			// Get overlapping segments of the first flight
			for (FCCSegInventoryDTO fCCSegInventoryDTO1 : fccSegInventoryDTOs1) {
				boolean overlaped = false;
				for (FCCSegInventoryDTO fCCSegInventoryDTO2 : fccSegInventoryDTOs2) {
					if (fCCSegInventoryDTO1.getSegmentCode().equals(fCCSegInventoryDTO2.getSegmentCode())) {
						overlaped = true;
						fCCSegInventoryDTO1.setOverlap(true);
						segInvOverlapCol1.add(fCCSegInventoryDTO1);
						fCCSegInventoryDTO2.setOverlap(true);
						segInvOverlapCol2.add(fCCSegInventoryDTO2);
						break;
					}
				}
				// Retain non-overlapping segments of the first flight
				if (!overlaped) {
					segInvCol1.add(fCCSegInventoryDTO1);
				}
			}
			// Add the overlapping segements to the end of the list for the first flight
			segInvCol1.addAll(segInvOverlapCol1);

			// Make the second flight inventory list first with overlapping flights
			segInvCol2.addAll(segInvOverlapCol2);
			// Then add the non-overlapping flights to the second flight's inventory list
			for (FCCSegInventoryDTO fCCSegInventoryDTO1 : fccSegInventoryDTOs2) {
				boolean available = false;
				for (FCCSegInventoryDTO fCCSegInventoryDTO2 : segInvOverlapCol2) {
					if (fCCSegInventoryDTO1.getSegmentCode().equals(fCCSegInventoryDTO2.getSegmentCode())) {
						available = true;
						break;
					}
				}
				if (!available) {
					segInvCol2.add(fCCSegInventoryDTO1);
				}
			}
			// Update the DTOs
			List<FCCInventoryDTO> reorderedFCCInventoryDTOs = new ArrayList<FCCInventoryDTO>();
			fccInventoryDTO1.setFccSegInventoryDTOs(segInvCol1);
			fccInventoryDTO2.setFccSegInventoryDTOs(segInvCol2);
			reorderedFCCInventoryDTOs.add(fccInventoryDTO1);
			reorderedFCCInventoryDTOs.add(fccInventoryDTO2);
			return reorderedFCCInventoryDTOs;
		}
		return fccInvDTOs;
	}

	/**
	 * Retrieve booking-classwise pax count information 
	 * 
	 * @param flightSegmentId
	 * @return a map of <code>FCCBCOnewayReturnPaxCountDTO</code>
	 */
	public Map<String, FCCBCOnewayReturnPaxCountDTO> getFCCBCOnewayReturnPaxCountDTOMap(final Integer flightSegmentId) {
		
		List<FCCSegONDCodesAndBookingClassesDTO> fccSegOndCodesAndBookingClasses = getFlightInventoryJdbcDAO()
				.getFCCSegONDCodesAndBookingClassesDTOs(flightSegmentId);
		Map<String, FCCBCOnewayReturnPaxCountDTO> fccBCOnewayReturnPaxCountMap = new HashMap<>();

		Map<String, List<FCCSegONDCodesAndBookingClassesDTO>> fccSegOndCodesPerBookingClassMap = fccSegOndCodesAndBookingClasses
				.stream().collect(groupingBy(FCCSegONDCodesAndBookingClassesDTO::getBookingCode));
		for (Map.Entry<String, List<FCCSegONDCodesAndBookingClassesDTO>> fccSegOndCodesPerBookingClassEntry : fccSegOndCodesPerBookingClassMap
				.entrySet()) {
			String bookingClass = fccSegOndCodesPerBookingClassEntry.getKey();
			List<FCCSegONDCodesAndBookingClassesDTO> fccSegOndCodesPerBookingClassList = fccSegOndCodesPerBookingClassEntry
					.getValue();
			Map<Boolean, List<FCCSegONDCodesAndBookingClassesDTO>> partitionedList = fccSegOndCodesPerBookingClassList
					.stream().collect(partitioningBy(e -> e.isReturnFlag()));
			List<FCCSegONDCodesAndBookingClassesDTO> returnPaxInfo = partitionedList.get(Boolean.TRUE);
			List<FCCSegONDCodesAndBookingClassesDTO> oneWayPaxInfo = partitionedList.get(Boolean.FALSE);
			Map<String, Integer> oneWayConnectionSegments = oneWayPaxInfo.stream()
					.filter(bc -> bc.getOndCode().length() > 7)
					.collect(groupingBy(FCCSegONDCodesAndBookingClassesDTO::getOndCode, summingInt(n -> 1)));
			Integer onewayConnectionPax = oneWayConnectionSegments.values().stream().reduce(0, Integer::sum);
			Integer oneWayP2PPax = oneWayPaxInfo.size() - onewayConnectionPax;
			Map<String, Integer> returnConnectionSegments = returnPaxInfo.stream()
					.filter(bc -> bc.getOndCode().length() > 7)
					.collect(groupingBy(FCCSegONDCodesAndBookingClassesDTO::getOndCode, summingInt(n -> 1)));
			Integer returnConnectionPax = returnConnectionSegments.values().stream().reduce(0, Integer::sum);
			Integer returnP2PPax = returnPaxInfo.size() - returnConnectionPax;
			FCCBCOnewayReturnPaxCountDTO fCCBCOnewayReturnPaxCountDTO = new FCCBCOnewayReturnPaxCountDTO(oneWayP2PPax,
					onewayConnectionPax, returnP2PPax, returnConnectionPax, oneWayConnectionSegments,
					returnConnectionSegments);
			fccBCOnewayReturnPaxCountMap.put(bookingClass, fCCBCOnewayReturnPaxCountDTO);
		}
		return Collections.unmodifiableMap(fccBCOnewayReturnPaxCountMap);
	}
	
	

	/**
	 * Deletes flight inventories for the given flight ids Deletion happens only if specified flights does not have
	 * reservations
	 * 
	 * TODO Verify whether there can be a possibility that only one flight of overlapping flights is deleted!!! Updates
	 * intercepting segment inventories table
	 * 
	 * @param flightIds
	 * @throws ModuleException
	 */
	public void deleteFlightInventories(Collection<Integer> flightIds) throws ModuleException {
		Collection<Integer> flightIdsHavingRes = getFlightInventoryResDAO().filterFlightsHavingReservations(flightIds, null);
		if (flightIdsHavingRes != null && flightIdsHavingRes.size() > 0) {
			throw new ModuleException("airinventory.logic.bl.delinv.failed.has.res", AirinventoryCustomConstants.INV_MODULE_CODE);
		}
		getFlightInventoryDAO().deleteFlightInventories(flightIds, null);
	}

	/**
	 * Invalidates a set of segments in a flight Steps: Set invalid flag for flight segment inventories
	 * 
	 * @param flightSegIds
	 */
	public void invalidateFlightSegmentInventories(Collection<Integer> flightSegIds) {
		// set invalid flag in segment inventories
		getFlightInventoryDAO().updateFltSegInventoriesValidFlag(flightSegIds, null, FCCSegInventory.VALID_FLAG_N);

		// update availability in intercepting segment of each flight segment being made invalid
		Iterator<Integer> flightSegIdsIt = flightSegIds.iterator();
		while (flightSegIdsIt.hasNext()) {
			Integer flightSegId = flightSegIdsIt.next();
			List<Integer> interceptingSegInvIds = getFlightInventoryDAO()
					.getInterceptingFCCSegInventoryIds(flightSegId.intValue(), null, true);
			if (interceptingSegInvIds.size() > 0) {
				getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(interceptingSegInvIds);
			}
		}
	}

	/**
	 * Making invalid segments valid Steps: Check possibility of making the segment valid (sold+onhold+fixed on segment
	 * should available in all the intercepting segments) If eligibility check ok, then Set valid flag on the segment
	 * Update availabilities in the segment and the intercepting segments
	 * 
	 * @param flightSegIds
	 */
	public void makeInvalidFltSegInvsValid(Collection<Integer> flightSegIds) throws ModuleException {
		Collection<Integer> flightSeg = new ArrayList<Integer>();

		Iterator<Integer> flightSegIdsIt = flightSegIds.iterator();
		boolean isEligible = false;
		while (flightSegIdsIt.hasNext()) {
			Integer flightSegId = flightSegIdsIt.next();
			List<Integer> interceptingSegInvIds = getFlightInventoryDAO()
					.getInterceptingFCCSegInventoryIds(flightSegId.intValue(), null, false);

			if (interceptingSegInvIds == null || interceptingSegInvIds.size() == 0) {
				isEligible = true;
			} else {
				isEligible = getFlightInventoryDAO().isSegmentEligibleForMakingValid(flightSegId.intValue(),
						interceptingSegInvIds);
			}

			if (!isEligible) {
				throw new ModuleException("airinventory.logic.bl.validateinv.failed",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}

		// eligibility check ok, proceed with updating
		flightSegIdsIt = flightSegIds.iterator();
		while (flightSegIdsIt.hasNext()) {
			Integer flightSegId = flightSegIdsIt.next();
			flightSeg.add(flightSegId);
			getFlightInventoryDAO().updateFltSegInventoriesValidFlag(flightSeg, null, FCCSegInventory.VALID_FLAG_Y);
			List<Integer> interceptingSegInvIds = getFlightInventoryDAO()
					.getInterceptingFCCSegInventoryIds(flightSegId.intValue(), null, true);
			if (interceptingSegInvIds != null && interceptingSegInvIds.size() > 0) {
				getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(interceptingSegInvIds);
			}
			flightSeg.clear();
		}
	}

	public void removeInventoryOverlap(Collection<Integer> flightIds) throws ModuleException {
		// remove overlap
		getFlightInventoryDAO().removeInventoryOverlap(flightIds);

		// update avaialabilities
		Collection<Object[]> fltSegSummaryCol = getFlightInventoryDAO().getFCCSegmentInventoriesSummary(flightIds, null);
		List<Integer> segInvIdsToUpdate = new ArrayList<Integer>();
		if (fltSegSummaryCol != null) {
			Iterator<Object[]> fltSegSummaryColIt = fltSegSummaryCol.iterator();
			while (fltSegSummaryColIt.hasNext()) {
				Object[] fltSegSummary = fltSegSummaryColIt.next();
				segInvIdsToUpdate.add((Integer) fltSegSummary[2]);
			}
			getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(segInvIdsToUpdate);
		}
	}

	/**
	 * This method will send meal notification messages for all newly scheduled flights 4-16 hours before the departure
	 * time and also resends messages for the flights, which were failed previously due to some error. Once a meal
	 * notification message is sent, the status of the corresponding flight is updated.
	 * 
	 * @author lalanthi
	 * @throws ModuleException
	 */

	public void sendMealNotification() throws ModuleException {
		String hubAirPort = globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		int maxNoOfAttempts = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_ATTEMPTS));
		sendMealNotificationForNewAndFailedFlights(hubAirPort, maxNoOfAttempts);
	}

	/**
	 * This method will prepare a list of meals required by each departing flight for a given station and sends it to
	 * the catering department via an email. The list is prepared certain hours before the departure time of a flight
	 * (this threshold is defined in app. parameter) and sends an email to the catering department (email address is
	 * defined in a parameter)
	 * 
	 * @author lalanthi
	 * @param hubAirPort
	 * @param maxNoOfAttempts
	 * @throws ModuleException
	 */
	private void sendMealNotificationForNewAndFailedFlights(String hubAirPort, int maxNoOfAttempts) throws ModuleException {
		String mealRequestStartCutover = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_START_CUTOVER);// 16
		String mealRequestStopCutover = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFICATION_STOP_CUTOVER); // 4
		int mealRequestStartCutoverTime = Integer.parseInt(mealRequestStartCutover);
		int mealRequestStopCutoverTime = Integer.parseInt(mealRequestStopCutover);
		Object[] mealsCollection = null;

		// -------------- Sets the time stamp values ---------------------------------------------------//
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(CalendarUtil.getCurrentSystemTimeInZulu());
		calendar.add(GregorianCalendar.MINUTE, mealRequestStopCutoverTime);
		// Sets the lower boundary to filter flights to send meal notifications - Ex: 8.00 am
		String mealNotificationBeginTime = CalendarUtil.formatForSQL_toDate(new Timestamp(calendar.getTime().getTime()));
		// Sets the upper boundary to filter flights to send meal notifications - Ex: 8.00 + 16 hrs = 24.00
		GregorianCalendar calendar2 = new GregorianCalendar();
		calendar2.setTime(CalendarUtil.getCurrentSystemTimeInZulu());
		calendar2.add(GregorianCalendar.MINUTE, (mealRequestStartCutoverTime));
		String mealNotificationEndTime = CalendarUtil.formatForSQL_toDate(new Timestamp(calendar2.getTime().getTime()));

		List<String> lisHubs = new ArrayList<String>();
		lisHubs.add(hubAirPort);

		// ------------ Gets meal requirement for flights ---------------------------------------------//
		// Gets meal requirement for flights departing between 'mealNotificationBeginTime'(8.00 am) and
		// 'mealNotificationEndTime'(12 midnight)
		mealsCollection = getFlightInventoryJDBCDAO().getMealsListForStation(lisHubs, mealNotificationBeginTime,
				mealNotificationEndTime, maxNoOfAttempts, null);
		// -------------------- Format and send email to catering ----------------------------------//
		if (mealsCollection != null) {
			sendMealsListPerRoute(mealsCollection);
		}
	}

	/**
	 * This method will iterates the meals collection and groups each meal requirement flight no wise and sends the mail
	 * message.
	 * 
	 * @param mealsCollection
	 * @throws ModuleException
	 */
	public static void sendMealsListPerRoute(Object[] mealsCollection) throws ModuleException {
		ArrayList<Topic> topics = composeMealNotificationMessagesPerRoute(mealsCollection);
		if (topics != null && topics.size() > 0) {
			sendMealNotificationMessages(topics);
		}
	}

	/**
	 * Sends meal notification messages for all the meal requirements for the station.
	 * 
	 * @param topicList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMealNotificationMessages(ArrayList<Topic> topicList) {
		String emailAddress = globalConfig.getBizParam(SystemParamKeys.MEAL_NOTIFY_EMAIL);

		// Profile List
		List messageProfiles = new ArrayList();
		MessageProfile profile = null;
		for (Topic topic : topicList) {

			if (!AppSysParamsUtil.isHubBasedAlertsEnabled()) {
				String stationBaseEmail = (String) topic.getTopicParams().get("obNoifyEmail");
				if (stationBaseEmail != null && !stationBaseEmail.trim().equalsIgnoreCase("")) {
					emailAddress = stationBaseEmail;
				}
			}

			// Email id list
			List messageList = new ArrayList();
			UserMessaging user = new UserMessaging();
			user.setToAddres(emailAddress);
			messageList.add(user);

			profile = new MessageProfile();
			profile.setUserMessagings(messageList);
			profile.setTopic(topic);
			messageProfiles.add(profile);
		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = AirInventoryModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Update BC status Flag on rollforward
	 * 
	 * @param isStatusCopy
	 * @param sourceBCStatus
	 * @param targetBCStatus
	 * @return
	 */
	private String setBCStatus(boolean isStatusCopy, String sourceBCStatus, String targetBCStatus) {
		if (sourceBCStatus == null) {
			return targetBCStatus;
		}

		if (isStatusCopy) {
			return sourceBCStatus;
		} else {
			return targetBCStatus;
		}
	}

	private boolean setBCPrority(boolean isPriorityCopy, String sourceBCStatus, boolean targetBCStatus) {
		if (sourceBCStatus == null) {
			return targetBCStatus;
		}
		if (isPriorityCopy) {
			if (sourceBCStatus.equalsIgnoreCase("Y")) {
				return true;
			} else {
				return false;
			}
		} else {
			return targetBCStatus;
		}
	}

	/**
	 * Prepares meal notification messages for each out bound and in bound flights pair.
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static ArrayList<Topic> composeMealNotificationMessagesPerRoute(Object[] mealsCollection) throws ModuleException {
		ArrayList<Topic> topicList = new ArrayList<Topic>();
		Topic topic = null;
		HashMap map = null;
		List<Integer> flightPairsHavingNoMeals = new ArrayList<Integer>();
		boolean hubBasedNotification = false;

		hubBasedNotification = AppSysParamsUtil.isHubBasedAlertsEnabled();

		Map<Integer, FlightSummaryDTO> obFlights = (Map<Integer, FlightSummaryDTO>) mealsCollection[0];
		Map<Integer, FlightMealsDTO> obFlightsMeals = (Map<Integer, FlightMealsDTO>) mealsCollection[1];
		Map<Integer, FlightSummaryDTO> ibFlights = (Map<Integer, FlightSummaryDTO>) mealsCollection[2];
		Map<Integer, FlightMealsDTO> ibFlightsMeals = (Map<Integer, FlightMealsDTO>) mealsCollection[3];

		boolean obFlightHasMeal = false;
		boolean ibFlightHasMeal = false;
		FlightSummaryDTO obFlightSummaryDTO = null;
		FlightSummaryDTO ibFlightSummaryDTO = null;
		for (Integer obFlightId : obFlights.keySet()) {
			obFlightHasMeal = (obFlightsMeals != null && obFlightsMeals.containsKey(obFlightId));
			ibFlightHasMeal = (ibFlightsMeals != null && ibFlightsMeals.containsKey(obFlightId));
			if (obFlightHasMeal || ibFlightHasMeal || AppSysParamsUtil.isFOCMealSelectionEnabled()) {
				obFlightSummaryDTO = obFlights.get(obFlightId);
				String obDepDateTimeLocalStr = CalendarUtil.getDateInFormattedString("dd-MMM-yy HH:mm",
						obFlightSummaryDTO.getDepartureDateTimeLocal());
				String mailSubject = "MEALS NOTIFICATION - " + obFlightSummaryDTO.getOrigin() + "-"
						+ obFlightSummaryDTO.getDestination() + " " + obFlightSummaryDTO.getFlightNumber() + " "
						+ obDepDateTimeLocalStr + "(Local)";
				topic = new Topic();
				map = new HashMap();
				map.put("subject", mailSubject);
				map.put("outFlightNo", obFlightSummaryDTO.getOrigin() + "-" + obFlightSummaryDTO.getDestination() + " "
						+ obFlightSummaryDTO.getFlightNumber() + " " + obDepDateTimeLocalStr + "(Local)");
				map.put("hubBaseAlertEnabled", AppSysParamsUtil.isHubBasedAlertsEnabled());
				map.put("obFlightId", obFlightSummaryDTO.getFlightId());
				if (obFlightHasMeal) {
					map.put("obNoifyEmail", obFlightsMeals.get(obFlightId).getNotifyEmail());
				} else {
					map.put("obNoifyEmail", null);
				}

				if (obFlightHasMeal) {
					map.put("outFlightHasMeal", true);
					map.put("outboundMealsList", obFlightsMeals.get(obFlightId).getFlightMealsList());
					map.put("outboundFlightMeal", obFlightsMeals.get(obFlightId));

				}

				HashSet colFlightMealClass = new HashSet(obFlights.get(obFlightId).getMapCabinClassFlightMealClassDTO().values());
				map.put("outboundCabinTotals", colFlightMealClass);

				if (ibFlights != null && ibFlights.containsKey(obFlightId) && hubBasedNotification) {
					ibFlightSummaryDTO = ibFlights.get(obFlightId);
					map.put("hasInboundFlight", true);
					map.put("inFlightNo",
							ibFlightSummaryDTO.getOrigin() + "-" + ibFlightSummaryDTO.getDestination() + " "
									+ ibFlightSummaryDTO.getFlightNumber() + " " + CalendarUtil.getDateInFormattedString(
											"dd-MMM-yy HH:mm", ibFlightSummaryDTO.getDepartureDateTimeLocal())
									+ "(Local)");
					if (ibFlightHasMeal) {
						map.put("inFlightHasMeal", true);
						map.put("ibMealsList", ibFlightsMeals.get(obFlightId).getFlightMealsList());
						map.put("inboundFlightMeal", ibFlightsMeals.get(obFlightId));

					}
					HashSet colFlightMealClass2 = new HashSet(
							ibFlights.get(obFlightId).getMapCabinClassFlightMealClassDTO().values());
					map.put("inboundCabinTotals", colFlightMealClass2);
				} else {
					map.put("hasInboundFlight", false);
				}
				List<FlightMealNotifyStatusDTO> flightNotifyStatusDTOList = new ArrayList<FlightMealNotifyStatusDTO>();
				FlightMealNotifyStatusDTO flightMealNotifyStatusDTO = new FlightMealNotifyStatusDTO();
				flightNotifyStatusDTOList.add(flightMealNotifyStatusDTO);
				flightMealNotifyStatusDTO.addFlightId(obFlightSummaryDTO.getFlightId());

				topic.setObjectInfo(flightNotifyStatusDTOList);// for updating the notify status of out bound flight
				topic.setTopicParams(map);
				topic.setTopicName(AirinventoryCustomConstants.MEAL_NOTIFY_EMAIL_TEMPLATE);
				topicList.add(topic);
			} else { // Neither ob or ib flight has meal
				flightPairsHavingNoMeals.add(obFlightId);
			}
		}

		if (flightPairsHavingNoMeals.size() > 0) {
			FlightMealNotifyStatusDTO flightMealNotifyStatusDTO = new FlightMealNotifyStatusDTO();
			flightMealNotifyStatusDTO.setStatus(Flight.MEAL_NOTIFICATION_EMPTY);
			flightMealNotifyStatusDTO.addFlightIds(flightPairsHavingNoMeals);

			AirInventoryModuleUtils.getFlightBD().updateFlightMealNotifyStatus(flightMealNotifyStatusDTO);
		}

		return topicList;
	}

	public void updateFCCSegBCInventories(InvDowngradeDTO inventoryTO, int infantCount, int onHoldInfantCount)
			throws ModuleException {
		FCCSegBCInventory bcInventory = getFlightInventoryDAO().getFCCSegBCInventory(inventoryTO.getFccsbInvId());

		// Update segment Inventory
		FCCSegInventory segInventory = getFlightInventoryDAO().getFCCSegInventory(bcInventory.getfccsInvId());
		segInventory.setSeatsSold(segInventory.getSeatsSold() + inventoryTO.getSoldSeats());
		segInventory.setSoldInfantSeats(segInventory.getSoldInfantSeats() + infantCount);
		segInventory.setOnHoldSeats(segInventory.getOnHoldSeats() + inventoryTO.getOnholdSeats());
		segInventory.setOnholdInfantSeats(segInventory.getOnholdInfantSeats() + onHoldInfantCount);
		// Setting adult available seat counts
		// Setting adult available seat counts
		int availableSeatCount = ((segInventory.getAvailableSeats()
				- (inventoryTO.getSoldSeats() + inventoryTO.getOnholdSeats())) > 0)
						? (segInventory.getAvailableSeats() - (inventoryTO.getSoldSeats() + inventoryTO.getOnholdSeats()))
						: 0;
		segInventory.setAvailableSeats(availableSeatCount);
		int availableInfantSeatCount = ((segInventory.getAvailableInfantSeats() - (infantCount + onHoldInfantCount)) > 0)
				? (segInventory.getAvailableInfantSeats() - (infantCount + onHoldInfantCount))
				: 0;
		segInventory.setAvailableInfantSeats(availableInfantSeatCount);
		getFlightInventoryDAO().saveFCCSegmentInventory(segInventory);

		// Update booking class inventory
		UpdateFccSegBCInvResponse response = getFlightInventoryDAO().updateFccSegBCInventory(bcInventory.getFccsbInvId(),
				inventoryTO.getOnholdSeats(), inventoryTO.getSoldSeats(), false, null, false, true, 0, null);
		if (response.getUpdateCount() == 0) {
			throw new ModuleException("airinventory.logic.bl.segbcinv.update.failed",
					AirinventoryCustomConstants.INV_MODULE_CODE);
		}
	}

	public void updateOPRTFCCSegBCInventories(Integer flightSegId, String bookingCode, int adultCount, int onHoldadultCount,
			int infantCount, int onHoldInfantCount) throws ModuleException {
		FCCSegBCInventory bcInventory = getFlightInventoryDAO().getFCCSegBCInventoryForBC(flightSegId, bookingCode);
		if (bcInventory == null) {
			bcInventory = new FCCSegBCInventory();
			String cabinClass = bookingCode.substring(bookingCode.length() - 1);
			FCCSegInventory fccSegInventory = getFlightInventoryDAO().getFCCSegInventory(flightSegId, cabinClass);
			bcInventory.setBookingCode(bookingCode);
			bcInventory.setLogicalCCCode(fccSegInventory.getLogicalCCCode());

			bcInventory.setfccsInvId(fccSegInventory.getFccsInvId());
			bcInventory.setFlightId(fccSegInventory.getFlightId());
			bcInventory.setOnHoldSeats(0);
			bcInventory.setPriorityFlag(false);
			bcInventory.setSeatsAcquired(0);
			bcInventory.setSeatsAllocated(adultCount + onHoldadultCount);
			bcInventory.setSeatsAvailable(0);
			bcInventory.setSeatsCancelled(0);
			bcInventory.setSeatsSold(0);
			bcInventory.setStatus(FCCSegBCInventory.Status.CLOSED);
			bcInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
			bcInventory.setSegmentCode(fccSegInventory.getSegmentCode());
		}
		bcInventory.setSeatsSold(bcInventory.getSeatsSold() + adultCount);
		bcInventory.setOnHoldSeats(bcInventory.getOnHoldSeats() + onHoldadultCount);
		int availableSeatCount = ((bcInventory.getSeatsAvailable() - (adultCount + onHoldadultCount)) > 0)
				? (bcInventory.getSeatsAvailable() - (adultCount + onHoldadultCount))
				: 0;
		bcInventory.setSeatsAvailable(availableSeatCount);
		// Update booking class Inventory
		getFlightInventoryDAO().saveFCCSegmentBCInventory(bcInventory);
	}

	public int getFCCSegBCInventoriesCount(int flightId) {
		return getFlightInventoryDAO().getFCCSegBCInvetoriesCount(flightId);
	}

	public HashMap<Integer, FlightInventorySummaryDTO> getFlightInventoriesSummary(Collection<Integer> flightIds,
			boolean isReporting) {
		return getFlightInventoryDAO().getFlightInventoriesSummary(flightIds, isReporting);
	}

	public Collection<FlightInventorySummaryDTO> getFlightSegmentInventoriesSummary(Collection<Integer> flightIds) {
		return getFlightInventoryDAO().getFlightSegmentInventoriesSummary(flightIds);
	}

	public Collection<String> getCabinClassesToBeDeleted(AircraftModel updatedAircraftModel,
			AircraftModel existingAircraftModel) {
		Collection<AircraftCabinCapacity> accs = getCabinClassesOnlyInOneModelOrBoth(updatedAircraftModel, existingAircraftModel,
				false);
		Collection<String> cabinClassesToBeDeleted = new ArrayList<String>();
		for (AircraftCabinCapacity acc : accs) {
			cabinClassesToBeDeleted.add(acc.getCabinClassCode());
		}
		return cabinClassesToBeDeleted;
	}

	private FlightInventoryResDAO getFlightInventoryResDAO() {
		return (FlightInventoryResDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryResDAOImplProxy");
	}

	private BookingClassDAO getBookingClassDAO() {
		BookingClassDAO bookingClassDAO = (BookingClassDAO) AirInventoryUtil.getInstance()
				.getLocalBean("BookingClassDAOImplProxy");
		return bookingClassDAO;
	}

	private LogicalCabinClassDAO getLogicalCabinClassDAO() {
		LogicalCabinClassDAO logicalCabinClassDAO = (LogicalCabinClassDAO) AirInventoryUtil.getInstance()
				.getLocalBean("LogicalCabinClassDAOImplProxy");
		return logicalCabinClassDAO;
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		FlightInventoryDAO flightInventoryDAO = (FlightInventoryDAO) AirInventoryUtil.getInstance()
				.getLocalBean("FlightInventoryDAOImplProxy");
		return flightInventoryDAO;
	}

	private SeatMapDAO getSeatMapDAO() {
		SeatMapDAO seatMapDAO = (SeatMapDAO) AirInventoryUtil.getInstance().getLocalBean("SeatMapDAOImplProxy");
		return seatMapDAO;
	}

	private BookingClassJDBCDAO getBookingClassJdbcDAO() {
		BookingClassJDBCDAO bookingClassDAO = (BookingClassJDBCDAO) AirInventoryUtil.getInstance()
				.getLocalBean("bookingClassJdbcDAO");
		return bookingClassDAO;
	}

	private FlightInventoryJDBCDAO getFlightInventoryJdbcDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance()
				.getLocalBean("flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	private FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
		FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil.getInstance()
				.getLocalBean("flightInventoryJdbcDAO");
		return flightInventoryJDBCDAO;
	}

	private AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}

	private FlightInventoryResJDBCDAO getFlightInventoryResJDBCDAO() {
		return (FlightInventoryResJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("flightInventoryResJdbcDAO");
	}

	private Set<FCCSegBCInventory> checkAlreadyExistingBC(HashMap<Integer, ExtendedFCCSegBCInventory> existingFCCSegBCInventories,
			Set<FCCSegBCInventory> addedFCCSegBCInventories) {
		Iterator<FCCSegBCInventory> bcInventoryIt = addedFCCSegBCInventories.iterator();
		Set<FCCSegBCInventory> tmpRemovedSegBcInv = new HashSet<FCCSegBCInventory>();

		while (bcInventoryIt.hasNext()) {
			FCCSegBCInventory addedFCCSegBCInventory = bcInventoryIt.next();
			if (existingFCCSegBCInventories != null && existingFCCSegBCInventories.size() > 0) {
				Iterator<Integer> existingFCCSegBCInvIdsIt = existingFCCSegBCInventories.keySet().iterator();
				while (existingFCCSegBCInvIdsIt.hasNext()) {
					Integer fccInvId = (Integer) existingFCCSegBCInvIdsIt.next();
					ExtendedFCCSegBCInventory exBCInv = existingFCCSegBCInventories.get(fccInvId);
					FCCSegBCInventory fccsegBCInventory = exBCInv.getFccSegBCInventory();
					if (fccsegBCInventory != null
							&& fccsegBCInventory.getBookingCode().equals(addedFCCSegBCInventory.getBookingCode())
							&& fccsegBCInventory.getfccsInvId().equals(addedFCCSegBCInventory.getfccsInvId())) {
						tmpRemovedSegBcInv.add(addedFCCSegBCInventory);
					}
				}
			}
		}

		if (tmpRemovedSegBcInv.size() > 0) {
			addedFCCSegBCInventories.removeAll(tmpRemovedSegBcInv);
		}

		return addedFCCSegBCInventories;
	}

	public AASelectedAncillaryRS getSelectedAncillaryDetails(AASelectedAncillaryRQ aaSelectedAncillaryRQ,
			Map<String, BundledFareDTO> segmentBundledFareMap, TrackInfoDTO trackInfo, int salesChannelCode) throws ModuleException {
		AASelectedAncillaryRS selectedAncillaryRS = new AASelectedAncillaryRS();
		selectedAncillaryRS.setResponseAttributes(new AAResponseAttributes());
		selectedAncillaryRS.getResponseAttributes().setSuccess(new AASuccess());

		List<SelectedSeat> selectedSeats = aaSelectedAncillaryRQ.getSelectedSeats();
		List<SelectedMeal> selectedMeals = aaSelectedAncillaryRQ.getSelectedMeals();
		List<SelectedBaggage> selectedBaggages = aaSelectedAncillaryRQ.getSelectedBaggages();
		List<SelectedInsurance> selectedInsurances = aaSelectedAncillaryRQ.getSelectedInsurances();
		List<SelectedFlexi> selectedFlexis = aaSelectedAncillaryRQ.getSelectedFlexis();
		List<SelectedSSR> selectedAPS = aaSelectedAncillaryRQ.getSelectedAirportServices();
		List<SelectedSSR> selectedInflightServices = aaSelectedAncillaryRQ.getSelectedInflightServices();
		List<StringStringListMap> flightRefWiseSelectedMeals = aaSelectedAncillaryRQ.getFlightRefWiseSelectedMeals();
		List<SelectedSSR> selectedAirportTransfers = aaSelectedAncillaryRQ.getSelectedAirportTransfers();
		List<SelectedAutoCheckin> selectedAutoCheckins = aaSelectedAncillaryRQ.getSelectedAutoCheckins();
		boolean isModifyAnci = aaSelectedAncillaryRQ.isModifyAnci();
		boolean skipeCutoverValidation = aaSelectedAncillaryRQ.isSkipCutoverCheck();

		AncillaryBL ancillaryBL;
		ancillaryBL = new AncillaryBL();
		ancillaryBL.setPnr(aaSelectedAncillaryRQ.getPnr());
		ancillaryBL.setSegmentBundledFareMap(segmentBundledFareMap);

		if (selectedMeals != null && selectedMeals.size() > 0) {
			Map<Integer, Set<String>> flightSegIdWiseSelectedMeals = AncillaryBL
					.getFlightSegIdWiseSelectedAnci(flightRefWiseSelectedMeals);
			List<FlightSegmentMeals> meals = ancillaryBL.getSelectedMealDetails(selectedMeals, flightSegIdWiseSelectedMeals,
					skipeCutoverValidation, salesChannelCode);
			selectedAncillaryRS.getMeals().addAll(meals);
		}

		if (selectedBaggages != null && selectedBaggages.size() > 0) {
			List<FlightSegmentBaggages> baggages = ancillaryBL.getSelectedBaggageDetails(selectedBaggages,
					skipeCutoverValidation);
			selectedAncillaryRS.getBaggages().addAll(baggages);
		}

		if (selectedAPS != null && selectedAPS.size() > 0) {
			List<FlightSegmentAirportServiceRequests> airportServices = ancillaryBL.getSelectedAirportServices(selectedAPS,
					skipeCutoverValidation);
			selectedAncillaryRS.getAirportServices().addAll(airportServices);
		}

		if (selectedSeats != null && selectedSeats.size() > 0) {
			if (selectedSeats != null && selectedSeats.size() > 0) {
				ancillaryBL.setPnr(aaSelectedAncillaryRQ.getPnr());
				List<FlightSegmentSeats> seats = ancillaryBL.getSelectedSeatDetails(selectedSeats);
				selectedAncillaryRS.getSeats().addAll(seats);
			}
		}

		if (selectedInsurances != null && !selectedInsurances.isEmpty()) {
			for (SelectedInsurance selectedInsurance : selectedInsurances) {
				if (selectedInsurance != null && selectedInsurance.getInsuranceReferences() != null
						&& !selectedInsurance.getInsuranceReferences().isEmpty()) {

					List<String> insuranceReferences = selectedInsurance.getInsuranceReferences();

					for (String insuranceRef : insuranceReferences) {
						InsuranceQuotation insuranceQuotation = ancillaryBL.getSavedInsuranceQuote(insuranceRef);
						selectedAncillaryRS.getInsuranceQuotations().add(insuranceQuotation);
					}
				}
			}
		}

		if (selectedInflightServices != null && selectedInflightServices.size() > 0) {
			ancillaryBL.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			Integer salesChannel = trackInfo.getOriginChannelId();
			ancillaryBL.setSalesChannel(salesChannel.toString());
			ancillaryBL.setModifyAnci(isModifyAnci);
			List<FlightSegmentSpecialServiceRequests> ssrList = ancillaryBL.getSelectedInflightServices(selectedInflightServices,
					skipeCutoverValidation);
			selectedAncillaryRS.getInflightServices().addAll(ssrList);
		}

		if (selectedAirportTransfers != null && selectedAirportTransfers.size() > 0) {
			List<FlightSegmentAirportServiceRequests> airportTransfers = ancillaryBL
					.getSelectedAirportTransfer(selectedAirportTransfers, skipeCutoverValidation);
			selectedAncillaryRS.getAirportTransfers().addAll(airportTransfers);
		}

		if (selectedAutoCheckins != null && selectedAutoCheckins.size() > 0) {
			List<FlightSegmentAutomaticCheckins> autoCheckins = ancillaryBL
					.getSelectedAutomaticCheckinDetails(selectedAutoCheckins);
			selectedAncillaryRS.getAutomaticCheckins().addAll(autoCheckins);
		}

		if (selectedFlexis != null && selectedFlexis.size() > 0) {
			List<FlexiQuotation> flexis = ancillaryBL.getSelectedFlexiDetails(selectedFlexis);
			selectedAncillaryRS.getFlexiQuotation().addAll(flexis);
		}

		return selectedAncillaryRS;
	}

}
