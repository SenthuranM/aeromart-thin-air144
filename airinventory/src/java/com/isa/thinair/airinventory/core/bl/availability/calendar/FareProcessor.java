package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;

class FareProcessor {
	private Set<String> distinctFareOnds = new HashSet<String>();
	private Set<String> distinctFareBookingCodes = new HashSet<String>();
	private Map<String, Set<String>> ondWiseDistinctFareBookingCodes = new HashMap<String, Set<String>>();
	private Map<String, Integer> nestRank = new HashMap<String, Integer>();
	private List<LightFareDTO> finalFareOrderedList = new ArrayList<LightFareDTO>();

	public FareProcessor(List<LightFareDTO> fares, final IPaxCountAssembler paxAssmblr) {
		List<LightFareDTO> orderedStdFares = new ArrayList<LightFareDTO>();
		List<LightFareDTO> orderedNonStdFares = new ArrayList<LightFareDTO>();

		for (LightFareDTO fare : fares) {
			distinctFareBookingCodes.add(fare.getBookingCode());
			distinctFareOnds.add(fare.getOndCode());

			if (ondWiseDistinctFareBookingCodes.get(fare.getOndCode()) == null) {
				ondWiseDistinctFareBookingCodes.put(fare.getOndCode(), new HashSet<String>());
			}
			ondWiseDistinctFareBookingCodes.get(fare.getOndCode()).add(fare.getBookingCode());
			if (fare.isStandardBC()) {
				nestRank.put(fare.getBookingCode(), fare.getNestRank());
			}
			if (fare.isStandardBC()) {
				orderedStdFares.add(fare);
			} else {
				orderedNonStdFares.add(fare);
			}
		}

		Collections.sort(orderedStdFares, new Comparator<LightFareDTO>() {
			@Override
			public int compare(LightFareDTO fareFirst, LightFareDTO fareSecond) {
				return fareFirst.getNestRank().compareTo(fareSecond.getNestRank());
			}
		});

		Collections.sort(orderedNonStdFares, new Comparator<LightFareDTO>() {
			@Override
			public int compare(LightFareDTO fareFirst, LightFareDTO fareSecond) {
				return fareFirst.getTotalFare(paxAssmblr).compareTo(fareSecond.getTotalFare(paxAssmblr));
			}
		});

		Iterator<LightFareDTO> stdItr = orderedStdFares.iterator();
		Iterator<LightFareDTO> nonstdItr = orderedNonStdFares.iterator();

		LightFareDTO lastStdItem = null;
		LightFareDTO lastNonStdItem = null;
		while (true) {
			if (lastStdItem == null && stdItr.hasNext()) {
				lastStdItem = stdItr.next();
			}

			if (lastNonStdItem == null && nonstdItr.hasNext()) {
				lastNonStdItem = nonstdItr.next();
			}

			if (lastStdItem == null && lastNonStdItem == null) {
				break;
			} else if (lastStdItem == null && lastNonStdItem != null) {
				finalFareOrderedList.add(lastNonStdItem);
				lastNonStdItem = null;
			} else if (lastStdItem != null && lastNonStdItem == null) {
				finalFareOrderedList.add(lastStdItem);
				lastStdItem = null;
			} else {
				if (lastStdItem.getTotalFare(paxAssmblr).compareTo(lastNonStdItem.getTotalFare(paxAssmblr)) > 0) {
					finalFareOrderedList.add(lastNonStdItem);
					lastNonStdItem = null;
				} else {
					finalFareOrderedList.add(lastStdItem);
					lastStdItem = null;
				}
			}
		}
	}

	public List<LightFareDTO> getOrderedFaresFor(Set<String> ondCodes) {
		List<LightFareDTO> orderedFares = new ArrayList<LightFareDTO>();
		for (LightFareDTO fare : finalFareOrderedList) {
			if (ondCodes.contains(fare.getOndCode())) {
				orderedFares.add(fare);
			}
		}
		return orderedFares;
	}

	public List<LightFareDTO> getOrderedFaresFor(String subOND) {
		Set<String> ondCodes = new HashSet<String>();
		return getOrderedFaresFor(ondCodes);
	}

	public Set<String> getDistinctFareOnds() {
		return distinctFareOnds;
	}

}