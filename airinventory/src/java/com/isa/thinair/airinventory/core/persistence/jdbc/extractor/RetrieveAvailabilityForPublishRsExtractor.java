package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.PublishBCAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.AvsGenerationFlow;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventCode;

public class RetrieveAvailabilityForPublishRsExtractor implements ResultSetExtractor {

	private final AvsGenerationFlow avsGenerationFlow;

	public RetrieveAvailabilityForPublishRsExtractor(AvsGenerationFlow avsGenerationFlow) {
		this.avsGenerationFlow = avsGenerationFlow;
	}

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		Map<Integer, Collection<PublishSegAvailMsgDTO>> publishSegAvailMsgDTOMap = null;

		Collection<PublishSegAvailMsgDTO> publishSegAvailMsgDTOs;

		while (rs.next()) {
			Integer gdsId = rs.getInt("GDS_ID");
			Integer publishAvailabilityId = rs.getInt("PUBLISH_AVAILABILITY_ID");

			// TODO related messages to be grouped into PublishSegAvailMsgDTO

			if (publishSegAvailMsgDTOMap == null) {
				publishSegAvailMsgDTOMap = new HashMap<Integer, Collection<PublishSegAvailMsgDTO>>();
			}

			if (!publishSegAvailMsgDTOMap.containsKey(gdsId)) {
				publishSegAvailMsgDTOMap.put(gdsId, new ArrayList<PublishSegAvailMsgDTO>());
			}

			publishSegAvailMsgDTOs = publishSegAvailMsgDTOMap.get(gdsId);

			PublishSegAvailMsgDTO publishSegAvailMsgDTO = new PublishSegAvailMsgDTO();
			publishSegAvailMsgDTOs.add(publishSegAvailMsgDTO);

			publishSegAvailMsgDTO.setDepartureDateLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
			publishSegAvailMsgDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));

			String segmentCode = rs.getString("SEGMENT_CODE");
			publishSegAvailMsgDTO.setOriginAirport(segmentCode.substring(0, 3));
			publishSegAvailMsgDTO.setDestinationAirport(segmentCode.substring(segmentCode.length() - 3));

			// TODO clean-up: no effect setting seat availability
			// as segment level is only used for closure/cancel/unpublish
			publishSegAvailMsgDTO.setSeatsAvailable(rs.getInt("CUR_SEG_AVAILABLE_SEATS"));

			PublishInventoryEventCode eventCodeEnum = PublishInventoryEventCode.getPublishInventoryEventCode(rs
					.getString("EVENT_CODE"));

			if (eventCodeEnum != null) {
				if (eventCodeEnum.isSegmentEvent()) {
					publishSegAvailMsgDTO.setPublishAvailabilityId(publishAvailabilityId);
					publishSegAvailMsgDTO.setSegmentEventCode(eventCodeEnum);
				} else {
					Collection<PublishBCAvailMsgDTO> publishBCAvailMsgDTOs = new ArrayList<PublishBCAvailMsgDTO>();
					publishSegAvailMsgDTO.setBcAvailabilityDTOs(publishBCAvailMsgDTOs);

					PublishBCAvailMsgDTO publishBCAvailMsgDTO = new PublishBCAvailMsgDTO();
					publishBCAvailMsgDTO.setPublishAvailabilityId(publishAvailabilityId);
					// publishBCAvailMsgDTO.setBookingCode(rs.getString("BOOKING_CODE"));
					publishBCAvailMsgDTO.setBookingCode(rs.getString("MAPPED_BC"));
					publishBCAvailMsgDTO.setBcEventCode(eventCodeEnum);

					int seatsAvailable = 0;

					String bcStatus = rs.getString("CUR_BC_STATUS");
					if (FCCSegBCInventory.Status.OPEN.equals(bcStatus)) { // if close then leave to 0
						seatsAvailable = Math.min(rs.getInt("CUR_BC_AVAILABLE_SEATS"), rs.getInt("CUR_SEG_AVAILABLE_SEATS"));
						if(PublishInventoryEventCode.AVS_RBD_CLOSED.equals(publishBCAvailMsgDTO.getBcEventCode())){
							publishBCAvailMsgDTO.setBcEventCode(PublishInventoryEventCode.AVS_RBD_REOPEN);
						}
					}

					publishBCAvailMsgDTO.setSeatsAvailable(seatsAvailable);

					publishBCAvailMsgDTO.setBcSeatsAvailable(rs.getInt("CUR_BC_AVAILABLE_SEATS"));

					// NOTE: segment hierarchy is not imposed. BC only.
					// Thus publishBCAvailMsgDTOs max size is 1.
					// That leads to publishSegAvailMsgDTO.bcAvailabilityDTOs also be one always
					// This is why the limit of 10 entries per message is preserved
					publishBCAvailMsgDTOs.add(publishBCAvailMsgDTO);
				}
			}
		}

		return publishSegAvailMsgDTOMap;
	}
}
