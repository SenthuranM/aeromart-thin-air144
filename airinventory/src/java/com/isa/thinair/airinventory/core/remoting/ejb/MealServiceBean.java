package com.isa.thinair.airinventory.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.FlightTo;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.model.FlightMeal;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.bl.MealBL;
import com.isa.thinair.airinventory.core.persistence.dao.MealDAO;
import com.isa.thinair.airinventory.core.persistence.dao.MealJDBCDAO;
import com.isa.thinair.airinventory.core.service.bd.MealBDImpl;
import com.isa.thinair.airinventory.core.service.bd.MealBDLocalImpl;
import com.isa.thinair.airreservation.api.model.PnrFarePassenger;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.ServiceResponce;

@Stateless
@RemoteBinding(jndiBinding = "MealService.remote")
@LocalBinding(jndiBinding = "MealService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class MealServiceBean extends PlatformBaseSessionBean implements MealBDImpl, MealBDLocalImpl {

	MealBL flightMealBL;

	/**
	 * @param flightSegID
	 * @return FlightSeatsDTO
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightMealDTO>> getMeals(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS,
			Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, boolean skipeCutoverValidation, PnrFarePassenger pnrFarePax,
			int salesChannelCode) throws ModuleException {
		return getMealBL().getMeals(flightSegIdWiseCOS, flightSegIdWiseSelectedMeals, skipeCutoverValidation, pnrFarePax,
				salesChannelCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightMealDTO>> getMealsWithTranslations(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS,
			String selectedLanguage, Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, int salesChannelCode)
			throws ModuleException {
		return getMealBL().getMealsWithTranslations(flightSegIdWiseCOS, selectedLanguage, flightSegIdWiseSelectedMeals,
				salesChannelCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FlightMealDTO> getMealsByTemplate(Integer templateID, String cabinClassCode, String logicalCabinClass,
			String selectedLanguage, Integer flightSegID, Set<String> existingMeals, int salesChannelCode)
			throws ModuleException {
		return templateID == null
				? new ArrayList<FlightMealDTO>()
				: Locale.ENGLISH.getLanguage().equals(selectedLanguage)
						? getMealJDBCDAO().getMealsByTemplate(templateID, cabinClassCode, logicalCabinClass, flightSegID,
								existingMeals, salesChannelCode)
						: getMealJDBCDAO().getMealsByTemplateWithTranslations(templateID, cabinClassCode, logicalCabinClass,
								selectedLanguage, flightSegID, existingMeals, salesChannelCode);
	}

	/**
	 * 
	 * 
	 * @param flightSegID
	 * @return collection of templateIDs
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FlightMealDTO> getMealTempleateIds(int flightId, String cos) throws ModuleException {
		return getMealJDBCDAO().getTempleateIds(flightId, cos);
	}

	/**
	 * 
	 * @param sourceFlightId
	 * @param destinationFlightIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce assignFlightMealChargesRollFoward(int sourceFlightId, Collection<Integer> destinationFlightIds,
			ArrayList<String> lstSelectedSeg) throws ModuleException {
		return getMealBL().assignFlightMealChargesRollFoward(sourceFlightId, destinationFlightIds, getUserPrincipal(),
				lstSelectedSeg);
	}

	/**
	 * Function to cancell Meals
	 * 
	 * @param flightMealIds
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void vacateMeals(Collection<Integer> flightMealIds) throws ModuleException {
		getMealBL().updateFlightMeals(flightMealIds, AirinventoryCustomConstants.FlightSeatStatuses.VACANT, getUserPrincipal(),
				null);
	}

	/**
	 * Function to Modify the Meals
	 * 
	 * @param flightMealIdsToAdd
	 * @param flightMealIdsToCancell
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void modifyMeal(Collection<Integer> flightMealIdsToAdd, Collection<Integer> flightMealIdsToCancell)
			throws ModuleException {
		if (flightMealIdsToCancell != null && flightMealIdsToCancell.size() > 0) {
			vacateMeals(flightMealIdsToCancell);
		}

		if (flightMealIdsToAdd != null && flightMealIdsToAdd.size() > 0) {
			getMealBL().updateFlightMeals(flightMealIdsToAdd, AirinventoryCustomConstants.FlightSeatStatuses.RESERVED,
					getUserPrincipal(), null);
		}

	}

	/**
	 * Get a collection of FlightMealDtos for given seg id and meal ids
	 * 
	 * @param flightSegId
	 * @param mealIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Collection<FlightMealDTO> getAvailableMealDtos(int flightSegId, Collection<Integer> mealIds) throws ModuleException {
		return getMealJDBCDAO().getAvailbleMealDTOs(flightSegId, mealIds);
	}

	/**
	 * @author Harsha Udayapriya
	 * @param flightMealDTOs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce assignFlightMealCharges(Collection<FlightMealDTO> flightMealDTOs, Integer chargeId, boolean isFromEdit)
			throws ModuleException {
		return getMealBL().assignFlightMealCharges(flightMealDTOs, getUserPrincipal(), chargeId, isFromEdit);
	}

	/**
	 * Delete Flight Meals
	 * 
	 * @author Harshana
	 * @param flightSegIDs
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteFlightMeals(Collection<Integer> flightSegIDs) throws ModuleException {
		getMealBL().deleteFlightMeals(flightSegIDs);
	}

	private MealDAO getMealDAO() {
		return (MealDAO) AirInventoryUtil.getInstance().getLocalBean("MealDAOImplProxy");
	}

	private MealJDBCDAO getMealJDBCDAO() {
		return (MealJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("mealJDBCDAO");
	}

	private MealBL getMealBL() {
		if (flightMealBL == null) {
			flightMealBL = new MealBL();
		}
		return flightMealBL;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, List<FlightMealDTO>> getSegWiseMealsByTemplates(Map<Integer, Integer> segWiseTemplateIDs,
			int salesChannelCode) throws ModuleException {
		Map<Integer, List<FlightMealDTO>> segWiseMeals = new HashMap<Integer, List<FlightMealDTO>>();

		for (Entry<Integer, Integer> segWiseTemplateEntry : segWiseTemplateIDs.entrySet()) {

			segWiseMeals.put(segWiseTemplateEntry.getKey(),
					segWiseTemplateEntry.getValue() == null
							? new ArrayList<FlightMealDTO>()
							: getMealJDBCDAO().getMealsByTemplate(segWiseTemplateEntry.getValue(), null, null,
									segWiseTemplateEntry.getKey(), null, salesChannelCode));
		}

		return segWiseMeals;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public FlightMealDTO createFlightMealTemplateIfDoesntExist(Integer offeredTemplateID, Integer flightSegID, String mealCode)
			throws ModuleException {
		FlightMealDTO chargeDetails = getMealJDBCDAO().getMealChargeDetails(offeredTemplateID, mealCode);

		if (chargeDetails.getChargeId() == null) {
			return null;
		}

		FlightMealDTO flightMealDTO = getMealJDBCDAO().checkIfMealExistsForFlight(flightSegID, chargeDetails.getChargeId());

		if (flightMealDTO == null) {
			flightMealDTO = new FlightMealDTO();

			FlightMeal flightMeal = new FlightMeal();
			flightMeal.setAllocatedMeals(chargeDetails.getAllocatedMeal());
			flightMeal.setAmount(chargeDetails.getAmount());
			flightMeal.setAnciOfferCreated(true);
			flightMeal.setAvailableMeals(chargeDetails.getAllocatedMeal());
			flightMeal.setCabinClassCode(chargeDetails.getCabinClassCode());
			flightMeal.setLogicalCCCode(chargeDetails.getLogicalCCCode());
			flightMeal.setCreatedBy("SYSTEM");
			flightMeal.setCreatedDate(new Date());
			flightMeal.setFlightSegId(flightSegID);
			flightMeal.setMealChargeId(chargeDetails.getChargeId());
			flightMeal.setSoldMeals(0);
			flightMeal.setStatus("INA");

			Integer flightMealID = getMealDAO().createFlightMeal(flightMeal);
			flightMealDTO.setFlightMealId(flightMealID);
		}
		flightMealDTO.setAmount(chargeDetails.getAmount());
		flightMealDTO.setMealId(chargeDetails.getMealId());
		return flightMealDTO;
	}

	public List<FlightTo> getMealRecords(List<FlightTo> flights) {
		return getMealJDBCDAO().getMealRecords(flights);
	}

	@Override
	public Map<Integer, Set<String>> getExistingMeals(List<Integer> pnrSegIds) throws ModuleException {
		return getMealJDBCDAO().getExistingMeals(pnrSegIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FlightMealDTO> getMealsForPreference() throws ModuleException {
		return getMealBL().getMealsForPreference();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<FlightMealDTO> getMealsForPreferenceWithTranslations(String selectedLanguage) throws ModuleException {
		return getMealBL().getMealsForPreference();
	}

}
