package com.isa.thinair.airinventory.core.persistence.jdbc.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryCollection;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;

/**
 * 
 * @author Nasly
 * 
 */
public class RetrieveBucketsRsExtractor implements ResultSetExtractor {
	private boolean excludeStandardBuckets = false;
	private boolean excludeNonStandardBuckets = true;
	private boolean excludeFixedBuckets = true;

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		FCCSegBCInventoryCollection bucketsCollection = new FCCSegBCInventoryCollection();
		boolean standardBucket = false;
		boolean fixedBucket = false;
		int fccsbInvId;
		while (rs.next()) {
			standardBucket = ((String) rs.getString("standard_code")).equals(BookingClass.STANDARD_CODE_Y);
			fixedBucket = ((String) rs.getString("fixed_flag")).equals(BookingClass.FIXED_FLAG_Y);
			fccsbInvId = rs.getInt("fccsba_id");

			if (standardBucket && !excludeStandardBuckets) {
				FCCSegBCInventoryTO bucket = bucketsCollection.getBucket(new Integer(fccsbInvId), standardBucket, fixedBucket);
				if (bucket == null) {

					bucket = new FCCSegBCInventoryTO();

					bucket.setFccsbInvId(fccsbInvId);
					bucket.setBookingClass(rs.getString("booking_code"));
					bucket.setStandardBookingClass(standardBucket);
					bucket.setFixedBookingClass(fixedBucket);
					bucket.setNestRank(rs.getInt("nest_rank"));
					bucket.setPriorityBucket(((String) rs.getString("priority_flag")).equals(FCCSegBCInventory.PRIORITY_FLAG_Y));
					bucket.setAllocationType(rs.getString("allocation_type"));
					Object objFareType = rs.getObject("fare_type");
					if (objFareType != null) {
						bucket.addFareType(new Integer(objFareType.toString()));
					}
					bucket.setAllocation(rs.getInt("allocated_seats"));
					bucket.setStatus(rs.getString("status"));
					bucket.setStatusChangeAction(rs.getString("status_chg_action"));
					bucket.setAvailable(rs.getInt("available_seats"));
					bucket.setSoldSeats(rs.getInt("sold_seats"));
					bucket.setOnholdSeats(rs.getInt("onhold_seats"));

					bucketsCollection.addBucket(bucket);
				} else {
					bucket.addFareType(new Integer(rs.getInt("fare_type")));
				}

				if ((bucket.getAllocationType().equals(BookingClass.AllocationType.SEGMENT)
						&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN) && bucket.getFareTypes() != null)
						|| (bucket.getAllocationType().equals(BookingClass.AllocationType.COMBINED)
								&& bucket.getStatus().equals(FCCSegBCInventory.Status.OPEN) && bucket.getFareTypes() != null && bucket
								.getFareTypes().contains(new Integer(FareTypes.PER_FLIGHT_FARE)))) {
					break;
				}
			}

			if (!standardBucket && excludeNonStandardBuckets && excludeFixedBuckets) {
				break;
				// TODO - Handle non-standard buckets as necessary
			}

		}
		return bucketsCollection;
	}
}
