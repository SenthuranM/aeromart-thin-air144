/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airinventory.api.service.RollForwardBatchREQBD;

/**
 * @author Raghu
 */
@Remote
public interface RollForwardBatchREQServiceDelegateImpl extends RollForwardBatchREQBD {

}
