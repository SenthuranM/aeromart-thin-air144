package com.isa.thinair.airinventory.core.bl.availability.calendar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.commons.core.util.CalendarUtil;

class DateProcessor {
	final static String DATE_FORMAT_STRING = "yy-MM-dd";
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_STRING);
	private Map<String, CalendarDateFlightTO> dateWiseCalendarFlights = new HashMap<String, CalendarDateFlightTO>();

	public DateProcessor(List<AvailableONDFlight> flights, Date fromDate, Date toDate) {
		for (AvailableONDFlight flt : flights) {

			IFlightSegment fs = flt.getFirstFlightSegment();
			String formattedDate = dateFormat.format(fs.getDepartureDateTime());
			if (!dateWiseCalendarFlights.containsKey(formattedDate)) {
				dateWiseCalendarFlights.put(formattedDate, new CalendarDateFlightTO());
			}
			dateWiseCalendarFlights.get(formattedDate).addFlights(flt);
		}

		Date dateIndex = new Date(fromDate.getTime());
		while (dateIndex.compareTo(toDate) <= 0) {
			String dateStr = dateFormat.format(dateIndex);
			if (!dateWiseCalendarFlights.containsKey(dateStr)) {
				dateWiseCalendarFlights.put(dateStr, null);
			}
			dateIndex = CalendarUtil.addDateVarience(dateIndex, 1);
		}
	}

	public CalendarDateFlightTO getCalendarFlightTO(String date) {
		return dateWiseCalendarFlights.get(date);
	}

	public Set<String> getDates() {
		return dateWiseCalendarFlights.keySet();
	}
}