package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCSegBCInventoryTO;
import com.isa.thinair.airinventory.api.dto.SeatDistForRelease;
import com.isa.thinair.airinventory.api.dto.TempSegBcAllocDto;
import com.isa.thinair.airinventory.api.dto.TransferSeatBcCount;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.TransferSeatList;
import com.isa.thinair.airinventory.api.dto.UpdateFccSegBCInvResponse;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegBCAllocDTOBuilder;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInvNesting;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBCInvClosure;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.model.TempSegBcAllocBKP;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airinventory.api.util.InventoryEventPublisher;
import com.isa.thinair.airinventory.api.util.InventoryDiffForAVS;
import com.isa.thinair.airinventory.api.util.TempBlockSeatUtil;
import com.isa.thinair.airinventory.core.config.AirInventoryConfig;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airinventory.core.persistence.dao.SeatAvailabilityDAOJDBC;
import com.isa.thinair.airinventory.core.util.AirInventoryDAOUtils;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airinventory.core.util.BCInventoryUtil;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author Nasly
 * @author Lasantha P.
 * @author Thejaka
 * 
 *         Inventory seat movement related business logic. [TODO:ehance] implement operation synchronizer to sync
 *         segment/bc inventory updates
 * 
 */
public class FlightInventoryResBL {

	private final Log log = LogFactory.getLog(getClass());

	private final Log invLog = LogFactory.getLog("com.isa.inventory_log");

	public Collection<TempSegBcAlloc> blockSeatsForRes(Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs,
			UserPrincipal userPrincipal) throws ModuleException {

		ArrayList<TempSegBcAlloc> allBlockedRecsList = new ArrayList<TempSegBcAlloc>();
		String userID = null;
		if (userPrincipal != null) { // If IBE user may not have the userPrincipal init
			userID = userPrincipal.getUserId();
		}
				
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();
			
		for (SegmentSeatDistsDTO segmentSeatDistsDTO : segmentSeatDistsDTOs) {

			// TODO set fccsInvId in SegmentSeatDistsDTO during fare quote
			Integer fccsInvId = getFlightInventoryDAO().getFccaId(segmentSeatDistsDTO.getFlightId(),
					segmentSeatDistsDTO.getLogicalCabinClass(), segmentSeatDistsDTO.getSegmentCode());

			InventoryDiffForAVS inventoryDiffForAVS = new InventoryDiffForAVS(fccsInvId);
			inventoryDiffForAVS.takeIntialInvSnapshot();

			List<TempSegBcAlloc> blockedRecsList = new ArrayList<TempSegBcAlloc>();
			HashMap<Integer, Collection<FCCSegBCInventoryTO>> bucketClosureInfo = new HashMap<Integer, Collection<FCCSegBCInventoryTO>>();
			List<Integer> interceptingSegs = null;
			/**
			 * Segment inventory update is skipped for standby bookings. Only BC inventory is updated for standby
			 * bookings.
			 */
			String bookingClassType = segmentSeatDistsDTO.getBookingClassType();
			boolean isOverbook = segmentSeatDistsDTO.isOverbook();
			boolean isWaitListed = segmentSeatDistsDTO.isWaitListed();
			boolean isBookedFlightCabinBeingSearch = segmentSeatDistsDTO.isBookedFlightCabinBeingSearched();
			if (isWaitListed) {
				bookingClassType = ReservationInternalConstants.ReservationType.WL.getDescription();
			} else if (isOverbook) {
				bookingClassType = BookingClass.BookingClassType.OVERBOOK;
			}
			if (bookingClassType == null) {
				bookingClassType = getBookingClassJDBCDAO().getBookingClassType(segmentSeatDistsDTO.getBookingCode());
			}

			String InboundOutBound = segmentSeatDistsDTO.isBelongToInboundOnd() == true
					? SegBCAllocDTOBuilder.DIRECTION.IB.toString()
					: SegBCAllocDTOBuilder.DIRECTION.OB.toString();

			/**
			 * update segment inventory only for non-standby bookings FIXME provision for keeping track of standby
			 * infants count
			 */
			FCCSegInventory updatedFCCSegInventory = null;
			boolean skipSegInvUpdate = true;
			if (!isBookedFlightCabinBeingSearch && bookingClassType != null
					&& ((!isOverbook && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) || isWaitListed)) {

				// acquire lock on intercepting segment inventories
				// TODO - consider Redesign with the concept of leg level inventory, with each inventory mutually
				// exclusive!!!

				interceptingSegs = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(fccsInvId, true);
				if (interceptingSegs != null && interceptingSegs.size() > 0
						&& getAirInventoryConfig().isApplyLockingForSeatBlock()) {
					Collection<Integer> fccsInvIds = new ArrayList<Integer>();
					fccsInvIds.add(fccsInvId);
					fccsInvIds.addAll(interceptingSegs);

					if (log.isDebugEnabled()) {
						log.debug("Going to acquire lock on seg/intercepting segs for blocking seats[fccSegInvIds=" + fccsInvIds
								+ "]");
					}

					AirInventoryDAOUtils.getInventoryResDAO().getFCCSegInventoriesForUpdate(fccsInvIds, false,
							getAirInventoryConfig().getMaxWaitForLockDuration());

					if (log.isDebugEnabled()) {
						log.debug("Acquired lock on seg/intercepting segs for blocking seats[fccSegInvIds=" + fccsInvIds + "]");
					}
				}

				if (!isWaitListed) {
					String logicalCabinClass = segmentSeatDistsDTO.getLogicalCabinClass();
					// Segment seat availability check
					List<Integer> availableSeats = getSeatAvailabilityDAO()
							.getAvailableNonFixedAndFixedSeatsCounts(fccsInvId, segmentSeatDistsDTO.getBookingCode())
							.get(logicalCabinClass);

					if ((!segmentSeatDistsDTO.isFixedQuotaSeats() && segmentSeatDistsDTO.getTotalAdultSeats() > 0
							&& segmentSeatDistsDTO.getTotalAdultSeats() > (availableSeats.get(0) - availableSeats.get(1)))
							|| (segmentSeatDistsDTO.isFixedQuotaSeats() && segmentSeatDistsDTO.getTotalAdultSeats() > 0
									&& segmentSeatDistsDTO.getTotalAdultSeats() > availableSeats.get(1))
							|| (segmentSeatDistsDTO.getNoOfInfantSeats() > 0
									&& segmentSeatDistsDTO.getNoOfInfantSeats() > availableSeats.get(2))) {
						addErrorLog("Seat block inventory check failed due to insufficient seats in " + fccsInvId
								+ ". Requested adults - " + segmentSeatDistsDTO.getTotalAdultSeats() + ", infant seats - "
								+ segmentSeatDistsDTO.getNoOfInfantSeats() + ". Available non fixed seats "
								+ (availableSeats.get(0) - availableSeats.get(1)) + ", Available fixed seats "
								+ availableSeats.get(1) + ", Available infant seats " + availableSeats.get(2));
						throw new ModuleException("airinventory.logic.blockseats.segavailability.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}

					// update cumulative sums, infant sold/onhold seats in segment inventory
					int segUpdateCount = getFlightInventoryDAO().updateFccSegInventorySoldOnholdSeats(
							segmentSeatDistsDTO.getFlightId(), segmentSeatDistsDTO.getLogicalCabinClass(),
							segmentSeatDistsDTO.getSegmentCode(), 0, segmentSeatDistsDTO.getTotalAdultSeats(), 0,
							segmentSeatDistsDTO.getNoOfInfantSeats(), !segmentSeatDistsDTO.isFixedQuotaSeats(),
							segmentSeatDistsDTO.getTotalAdultSeats() > 0 ? true : false,
							segmentSeatDistsDTO.getNoOfInfantSeats() > 0 ? true : false, 0);

					if (segUpdateCount == 0) {
						addErrorLog("Seat block operation failed due to insufficient seats in " + fccsInvId
								+ ". Requested adults - " + segmentSeatDistsDTO.getTotalAdultSeats() + ", infant seats - "
								+ segmentSeatDistsDTO.getNoOfInfantSeats() + ". Available non fixed seats "
								+ (availableSeats.get(0) - availableSeats.get(1)) + ", Available fixed seats "
								+ availableSeats.get(1) + ", Available infant seats " + availableSeats.get(2));
						throw new ModuleException("airinventory.logic.blockseats.segavailability.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);

					} else if (segUpdateCount > 0 && ("Y"
							.equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED)))) {
						updatedFCCSegInventory = getFlightInventoryDAO().getFCCSegInventory(segmentSeatDistsDTO.getFlightId(),
								segmentSeatDistsDTO.getLogicalCabinClass(), segmentSeatDistsDTO.getSegmentCode());
					}
				} else {
					// update wait listed count in segment inventory
					int segUpdateCount = getFlightInventoryDAO().updateFccSegInventoryWaitListSeats(
							segmentSeatDistsDTO.getFlightId(), segmentSeatDistsDTO.getLogicalCabinClass(),
							segmentSeatDistsDTO.getSegmentCode(), segmentSeatDistsDTO.getTotalAdultSeats());

					if (segUpdateCount == 0) {
						addErrorLog("Seat block operation failed due to insufficient wait list seats in " + fccsInvId
								+ ". Requested adults - " + segmentSeatDistsDTO.getTotalAdultSeats() + ", infant seats - "
								+ segmentSeatDistsDTO.getNoOfInfantSeats() + ".");
						throw new ModuleException("airinventory.logic.blockseats.segavailability.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);

					}
				}

				skipSegInvUpdate = false;
			} else if (!isBookedFlightCabinBeingSearch && BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
				skipSegInvUpdate = false;
			}

			boolean seatsNested = false;
			List<FCCSegBCInvNesting> nestedBCInvInfo = null;
			Collection<SeatDistribution> seatDistributions = segmentSeatDistsDTO.getSeatDistribution();
			if (seatDistributions != null && !seatDistributions.isEmpty() && segmentSeatDistsDTO.getTotalAdultSeats() > 0) {
				// Seat blocking from multiple BookingCodes with in single
				// segment takes place only when seats are nested
				int seatDistsCount = seatDistributions.size();
				if (seatDistsCount > 1) {
					seatsNested = true;
					nestedBCInvInfo = new ArrayList<FCCSegBCInvNesting>();
				}
				int iterator = 0;
				for (SeatDistribution seatDist : seatDistributions) {
					String bookingCode = seatDist.getBookingClassCode();
					int noOfSeatsFromBC = seatDist.getNoOfSeats();
					boolean isBCOverbook = seatDist.isOverbook();
					boolean isBCWaitListed = seatDist.isWaitListed();
					boolean isSameBookingClassAsExisting = seatDist.isSameBookingClassAsExisting();
					boolean isFlownOnd = seatDist.isFlownOnd();

					if (seatsNested && !BCInventoryUtil.isStandardBucket(bookingCode)) {
						log.error("Nested from Non standard buckets. Requested adults - "
								+ segmentSeatDistsDTO.getTotalAdultSeats() + " bc: " + bookingCode);
						throw new ModuleException("airinventory.logic.blockseats.segavailability.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}

					// getting bc inventory id
					FCCSegBCInventory bcInv = getFlightInventoryDAO().getFCCSegBCInventory(segmentSeatDistsDTO.getFlightId(),
							segmentSeatDistsDTO.getLogicalCabinClass(), segmentSeatDistsDTO.getSegmentCode(), bookingCode);

					BookingClass openReturnBookingClass = getBookingClassDAO().getLightWeightBookingClass(bookingCode);
					boolean isOpenReturnBookingClass = BookingClass.BookingClassType.OPEN_RETURN
							.equalsIgnoreCase(openReturnBookingClass.getBcType());

					if (isOpenReturnBookingClass && bcInv == null) {
						bcInv = createFCCSegBCInventory(segmentSeatDistsDTO, openReturnBookingClass);
					} else if ((!isSameBookingClassAsExisting || (seatDistributions.size() > 1 && noOfSeatsFromBC > 0))
							&& !isFlownOnd) {
						// update bc inventory
						UpdateFccSegBCInvResponse bcUpdateResponse = null;
						if (!isBCWaitListed) {
							bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(bcInv.getFccsbInvId(),
									noOfSeatsFromBC, 0, isOpenReturnBookingClass ? false : true, null,
									isOpenReturnBookingClass ? true : false,
									BookingClassUtil.byPassSegInvUpdateForOverbook(bookingClassType) || isBCOverbook, 0,
									userPrincipal.getUserId());
						} else {
							bcUpdateResponse = getFlightInventoryDAO()
									.updateFccSegBCInventoryForWaitListing(bcInv.getFccsbInvId(), noOfSeatsFromBC, false);
						}

						if (bcUpdateResponse.getUpdateCount() == 0) {
							addErrorLog("Seat block failed due to insufficient seats in " + bcInv.getFccsbInvId()
									+ " bucket. Requested adults - " + segmentSeatDistsDTO.getTotalAdultSeats());
							throw new ModuleException("airinventory.logic.blockseats.bcavailability.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);

						}

						if (bcUpdateResponse.getClosedFccsbInvs() != null && bcUpdateResponse.getClosedFccsbInvs().size() > 0) {
							bucketClosureInfo.put(bcInv.getFccsbInvId(), bcUpdateResponse.getClosedFccsbInvs());
						}
					}

					Integer fccsbInvId = bcInv.getFccsbInvId();

					// keeping track of seat nested
					if (seatsNested) {
						if (iterator == (seatDistsCount - 1)) {
							for (FCCSegBCInvNesting bcInvNested : nestedBCInvInfo) {
								bcInvNested.setToFCCSegBCInvId(fccsbInvId);
							}
							getFlightInventoryDAO().saveBCInvNestingInfo(nestedBCInvInfo);
						} else {
							FCCSegBCInvNesting nestedBCInvRecord = new FCCSegBCInvNesting();
							nestedBCInvRecord.setFromFCCSegBCInvId(fccsbInvId);
							nestedBCInvRecord.setOnholdSeats(noOfSeatsFromBC);
							nestedBCInvRecord.setSoldSeats(0);
							nestedBCInvRecord.setTemporary("Y");
							nestedBCInvInfo.add(nestedBCInvRecord);
						}
					}

					// prepare seat block record
					int blockSeatDuration = AppSysParamsUtil.getInventorySeatBlockDuration(userPrincipal);
					Date timestamp = CalendarUtil.addMinutesToCurrentZuluTime(blockSeatDuration);
					/**
					 * Note : timestamp now represent the release timestamp. Previously it was the recorded timestamp
					 * The scheduler for removing block seat logic was also changed accordingly. Now the scheduler will
					 * pick all the TempSegBcAlloc with timestamp less than the scheduler running time to be removed
					 */
					if ((!isSameBookingClassAsExisting || (seatDistributions.size() > 1 && noOfSeatsFromBC > 0)) && !isFlownOnd) {
						TempSegBcAlloc blockedSeatsRecord = new TempSegBcAlloc(fccsInvId, fccsbInvId, noOfSeatsFromBC, timestamp,
								bookingClassType, bcInv.getStatusChangeAction(), userID,
								AirinventoryCustomConstants.Seats.BLOCKED, InboundOutBound, skipSegInvUpdate);
						blockedRecsList.add(blockedSeatsRecord);
					}

					if (iterator == (seatDistsCount - 1)) {
						for (int j = 0; j < blockedRecsList.size(); j++) {
							TempSegBcAlloc tempSegBcAlloc = blockedRecsList.get(j);
							if (j != (seatDistsCount - 1)) {
								if (seatsNested) {
									tempSegBcAlloc.setNestRecordId((nestedBCInvInfo.get(j)).getFccsbInvNestingId());
								}
							}
							getFlightInventoryDAO().saveTempSegBcAlloc(tempSegBcAlloc);

							if (bucketClosureInfo.get(tempSegBcAlloc.getFccsbaId()) != null) {
								// save any bucket connection/return clousure resulted
								for (FCCSegBCInventoryTO bucket : bucketClosureInfo.get(tempSegBcAlloc.getFccsbaId())) {
									TempSegBCInvClosure saveBucket = new TempSegBCInvClosure();
									saveBucket.setFccsbaId(bucket.getFccsbInvId());
									saveBucket.setTempAllocId(tempSegBcAlloc.getId().intValue());
									saveBucket.setPreviousStatusAction(bucket.getStatusChangeAction());

									getFlightInventoryDAO().saveTempSegBCInvClosure(saveBucket);
								}
							}
							addDebugLog(tempSegBcAlloc.getSeatsBlocked() + " seats from inventory bc "
									+ tempSegBcAlloc.getFccsbaId() + " are blocked");
						}
					}
					iterator++;
					if (seatDistributions.size() == iterator) {
						// last update then close
						getFlightInventoryDAO().updateBCStatusForNestAvailability(bcInv.getfccsInvId());
					}
				}
			}

			// no infant seat blocking for standby bookings
			if (segmentSeatDistsDTO.getNoOfInfantSeats() > 0 && !segmentSeatDistsDTO.isSameBookingClassAsExisting()) {

				int blockSeatDuration = AppSysParamsUtil.getInventorySeatBlockDuration(userPrincipal);
				Date timestamp = CalendarUtil.addMinutesToCurrentZuluTime(blockSeatDuration);
				TempSegBcAlloc blockedSeatsRecord = new TempSegBcAlloc(fccsInvId, null,
						new Integer(segmentSeatDistsDTO.getNoOfInfantSeats()), timestamp, bookingClassType, null, userID,
						AirinventoryCustomConstants.Seats.BLOCKED, InboundOutBound, skipSegInvUpdate);
				getFlightInventoryDAO().saveTempSegBcAlloc(blockedSeatsRecord);
				blockedRecsList.add(blockedSeatsRecord);
				addDebugLog(segmentSeatDistsDTO.getNoOfInfantSeats() + " seats for flight segment "
						+ segmentSeatDistsDTO.getFlightSegId() + " are blocked");
			}

			allBlockedRecsList.addAll(blockedRecsList);

			// update segment availability - done only for non-standby bookings
			if (bookingClassType != null && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
				if (interceptingSegs != null && interceptingSegs.size() > 0) {
					getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(interceptingSegs);
				}
			}

			inventoryDiffForAVS.takeFinalInvSnapshotAndPublishDiff();
			inventoryEventPublisher.filterAndAdd(bookingClassType, segmentSeatDistsDTO.isBelongToInboundOnd(), fccsInvId);
		}
		
		inventoryEventPublisher.publishEvents();
		
		return allBlockedRecsList;
	}

	public Collection<TempSegBcAlloc> adjustSeatsBlocked(List<Integer> blockedSeats, int adjustmentAdult, int adjustmentInfant,
			UserPrincipal userPrincipal) throws ModuleException {

		List<SegmentSeatDistsDTO> segmentSeatDistsDTOs = getFlightInventoryDAO().getSegmentSeatDistsDTOs(blockedSeats);

		List<TempSegBcAlloc> tempSegBcAllocs = getFlightInventoryDAO().getTempSegBcAllocs(blockedSeats);
		releaseBlockedInventory(tempSegBcAllocs, false, userPrincipal);

		for (SegmentSeatDistsDTO dto : segmentSeatDistsDTOs) {
			for (SeatDistribution seatDist : dto.getSeatDistribution()) {
				seatDist.setNoOfSeats(seatDist.getNoOfSeats() + adjustmentAdult);
			}
			dto.setNoOfInfantSeats(dto.getNoOfInfantSeats() + adjustmentInfant);
		}
		return blockSeatsForRes(segmentSeatDistsDTOs, userPrincipal);

	}

	public List<TempSegBcAllocDto> getBlockedSeatsInfo(List<Integer> blockSeatIds) {
		return getFlightInventoryDAO().getBlockedSeatsInfo(blockSeatIds);
	}

	/**
	 * Note : booking class obj doesnt contain all the fields set
	 * 
	 * @param segSeats
	 * @param openReturnBookingClass
	 * @return
	 */
	private FCCSegBCInventory createFCCSegBCInventory(SegmentSeatDistsDTO segSeats, BookingClass openReturnBookingClass) {

		FCCSegInventory fccSegInventory = getFlightInventoryDAO().getFCCSegInventory(segSeats.getFlightSegId(),
				segSeats.getLogicalCabinClass());

		// create goshow fccsegbc inventory
		FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
		fccSegBCInventory.setBookingCode(openReturnBookingClass.getBookingCode());
		fccSegBCInventory.setLogicalCCCode(openReturnBookingClass.getLogicalCCCode());
		fccSegBCInventory.setfccsInvId(fccSegInventory.getFccsInvId());
		fccSegBCInventory.setFlightId(fccSegInventory.getFlightId());
		fccSegBCInventory.setOnHoldSeats(segSeats.getTotalAdultSeats());
		fccSegBCInventory.setPriorityFlag(false);
		fccSegBCInventory.setSeatsAcquired(0);
		fccSegBCInventory.setSeatsAllocated(segSeats.getTotalAdultSeats());
		fccSegBCInventory.setSeatsAvailable(0);
		fccSegBCInventory.setSeatsCancelled(0);
		fccSegBCInventory.setSeatsSold(0);
		fccSegBCInventory.setStatus(FCCSegBCInventory.Status.CLOSED);
		fccSegBCInventory.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
		fccSegBCInventory.setSegmentCode(fccSegInventory.getSegmentCode());

		getFlightInventoryDAO().saveFCCSegmentBCInventory(fccSegBCInventory);

		return fccSegBCInventory;
	}

	public void partiallyMoveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, int adultSeatsToSell,
			int infantSeatsToSell, UserPrincipal userPrincipal) throws ModuleException {
		if (adultSeatsToSell == 0 && infantSeatsToSell == 0) {
			removeBlockedSeats(blockedRecords);
		} else {
			moveBlockedSeatsToSold(blockedRecords, new Integer(adultSeatsToSell), new Integer(infantSeatsToSell),
					userPrincipal.getUserId());
		}
	}

	public void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, UserPrincipal userPrincipal)
			throws ModuleException {
		moveBlockedSeatsToSold(blockedRecords, null, null, userPrincipal.getUserId());
	}

	private void moveBlockedSeatsToSold(Collection<TempSegBcAlloc> blockedRecords, Integer adultSeatsToSell,
			Integer infantSeatsToSell, String userId) throws ModuleException {

		// log.debug("Entered moveBlockedSeatsToSold [" + Thread.currentThread().getName() + "] time=" +
		// System.currentTimeMillis());
		StringBuffer soldSeatsSummary = null;
		if (log.isDebugEnabled()) {
			soldSeatsSummary = new StringBuffer();
			soldSeatsSummary.append("\n\r--------Summary of seats TO BE moved from blocked to sold--------\n\r");
		}

		// acquire the write lock on segment inventories get updated
		/*
		 * if (getAirInventoryConfig().isApplyExplicitLocking()){ if (blockedRecords != null){ Collection fccsInvIds =
		 * new ArrayList(); for (Iterator it=blockedRecords.iterator(); it.hasNext();){ TempSegBcAlloc tempSegBcAlloc =
		 * (TempSegBcAlloc)it.next(); if (!fccsInvIds.contains(tempSegBcAlloc.getFccaId())){
		 * fccsInvIds.add(tempSegBcAlloc.getFccaId()); } }
		 * getFlightInventoryResJDBCDAO().getFCCSegInventoriesForUpdate(fccsInvIds, false); } }
		 */
		// check configuration if want to backup blocked seats
		boolean trackBlockedSeats = checkConfigForTrackingBlockedSeats();
		Collection<TempSegBcAllocBKP> bkpBlockedSeats = new ArrayList<TempSegBcAllocBKP>();

		Iterator<TempSegBcAlloc> itBlockedList = blockedRecords.iterator();
		int seatsMovedToSold = 0;
		boolean nestedSeatsRemaingToSell = true;
		int iterator = 0;
		while (itBlockedList.hasNext()) {

			TempSegBcAlloc tempSegAlloc = itBlockedList.next();

			if (TempBlockSeatUtil.isExpired(tempSegAlloc)) {
				log.error("Moving blocked seats to sold failed. " + "[blockRecId=" + tempSegAlloc.getId() + ",totalBlockedSeats="
						+ tempSegAlloc.getSeatsBlocked() + ",expiry=" + tempSegAlloc.getTimestamp() + "]");
				throw new ModuleException("airinventory.booking.class.inventory.invalid",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			}

			log.debug(
					"================TempSegBcAlloc @ start moveBlockedSeatsToSold [hashCode =" + tempSegAlloc.hashCode() + "]");
			Integer seatsCountToSell = tempSegAlloc.getSeatsBlocked();
			if (tempSegAlloc.getFccsbaId() != null) {

				if (adultSeatsToSell != null) {
					if (tempSegAlloc.getNestRecordId() != null) {
						if (nestedSeatsRemaingToSell) {
							if ((tempSegAlloc.getSeatsBlocked().intValue() + seatsMovedToSold) < adultSeatsToSell.intValue()) {
								seatsCountToSell = tempSegAlloc.getSeatsBlocked();
								seatsMovedToSold += tempSegAlloc.getSeatsBlocked().intValue();
								nestedSeatsRemaingToSell = true;
							} else if ((tempSegAlloc.getSeatsBlocked().intValue() + seatsMovedToSold) == adultSeatsToSell
									.intValue()) {
								seatsCountToSell = tempSegAlloc.getSeatsBlocked();
								seatsMovedToSold += seatsCountToSell.intValue();
								nestedSeatsRemaingToSell = false;
							} else {
								seatsCountToSell = new Integer(adultSeatsToSell.intValue() - seatsMovedToSold);
								seatsMovedToSold += seatsCountToSell.intValue();
								nestedSeatsRemaingToSell = false;
							}
						} else {
							seatsCountToSell = new Integer(0);
						}
					} else {
						seatsCountToSell = new Integer(adultSeatsToSell.intValue() - seatsMovedToSold);
						seatsMovedToSold = 0;
						nestedSeatsRemaingToSell = true;
					}
					if (seatsCountToSell.intValue() < 0
							|| (!BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType())
									&& tempSegAlloc.getSeatsBlocked().intValue() < seatsCountToSell.intValue())) {
						log.error("Moving blocked seats to sold failed. " + "[blockRecId=" + tempSegAlloc.getId()
								+ ",totalBlockedSeats=" + tempSegAlloc.getSeatsBlocked() + ",seatsToSell="
								+ seatsCountToSell.intValue() + "]");
						throw new ModuleException("airinventory.logic.moving.blocked.seats.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
				}

				if (seatsCountToSell > 0) {
					UpdateFccSegBCInvResponse bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(tempSegAlloc.getFccsbaId(), -seatsCountToSell.intValue(),
							seatsCountToSell.intValue(), false, null, false,
							BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType()), 0, userId);
					

					if (bcUpdateResponse.getUpdateCount() == 0) {
						log.error("Seat move from Block OHD to CNF failed due to insufficient seats in "
								+ tempSegAlloc.getFccsbaId() + " bucket. Requested adults - " + seatsCountToSell.intValue());
						throw new ModuleException("airinventory.logic.moving.blocked.seats.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);

					}
					if (invLog.isDebugEnabled()) {
						invLog.debug("mbssXXX::id=" + tempSegAlloc.getId() + ",fccsa=" + tempSegAlloc.getFccaId() + ",fccsba="
								+ tempSegAlloc.getFccsbaId() + ",blked=" + tempSegAlloc.getSeatsBlocked() + ",bcType:"
								+ tempSegAlloc.getBookingClassType() + ",sc:" + seatsCountToSell + ",adt:" + adultSeatsToSell);

						if (tempSegAlloc.getSeatsBlocked().intValue() != seatsCountToSell.intValue()) {
							invLog.error("mbssXXX::id=" + tempSegAlloc.getId() + ",MISMATCH_DETECTED");
						}
					}
					if (log.isDebugEnabled()) {
						soldSeatsSummary.append("Adult seats : \n\r").append("Temp block Rec Id      = ")
								.append(tempSegAlloc.getId()).append("\n\r").append("FccsInvId              = ")
								.append(tempSegAlloc.getFccaId()).append("\n\r").append("FccsbInvId             = ")
								.append(tempSegAlloc.getFccsbaId()).append("\n\r").append("Total blocked Seats    = ")
								.append(tempSegAlloc.getSeatsBlocked()).append("\n\r").append("Moved to sold          = ")
								.append(seatsCountToSell.intValue()).append("\n\r").append("Blocked timestamp      = ")
								.append(tempSegAlloc.getTimestamp()).append("\n\r")
								.append("Prev StatusremoveTempQuotedFare(id) Chg Action= ")
								.append(tempSegAlloc.getPrevStatusChgAction()).append("\n\r")
								.append("BookingClass Type              = ").append(tempSegAlloc.getBookingClassType())
								.append("\n\r").append("Nested Rec Id          = ").append(tempSegAlloc.getNestRecordId())
								.append("\n\r");
					}

					if (!"Y".equals(tempSegAlloc.getSkipSegInvUpdate())) {
						// no segment inventory update for standby bookings. This is checked within the method
						List<Integer> interceptingIds = getFlightInventoryDAO()
								.getInterceptingFCCSegmentInventoryIds(tempSegAlloc.getFccaId(), true);
						if (interceptingIds == null || interceptingIds.isEmpty()) {
							getFlightInventoryDAO().updateFCCSegmentInventory(tempSegAlloc.getFccaId(), 0, 0);
						} else {
							int[] totalSoldOhdCountIncludingInterceptingSegments = getFlightInventoryDAO()
									.getMaximumEffectiveReservedSetsCountInInterceptingSegments(interceptingIds);
							getFlightInventoryDAO().updateFCCSegmentInventory(tempSegAlloc.getFccaId(), 0,
									totalSoldOhdCountIncludingInterceptingSegments[0]);
						}
						// update intercepting segments' seat availability
						if (interceptingIds != null && !interceptingIds.isEmpty()) {
							interceptingIds.add(tempSegAlloc.getFccaId());
							getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(interceptingIds);
						}
					}
				}
			} else if (!tempSegAlloc.getBookingClassType()
					.equals(ReservationInternalConstants.ReservationType.WL.getDescription())) {
				Integer infantSeatsCountToSell = tempSegAlloc.getSeatsBlocked();
				if (infantSeatsToSell != null) {
					if (tempSegAlloc.getSeatsBlocked().intValue() < infantSeatsToSell.intValue()) {
						log.error("Partiallay moving blocked seats to sold failed. " + "[blockRecId=" + tempSegAlloc.getId()
								+ ",totalBlockedSeats=" + tempSegAlloc.getSeatsBlocked() + ",infantSeatsToSell="
								+ infantSeatsToSell.intValue() + "]");
						throw new ModuleException("airinventory.logic.moving.blocked.seats.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
					infantSeatsCountToSell = infantSeatsToSell;
				}

				if (infantSeatsCountToSell > 0 && (!BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType())
						|| BookingClassUtil.byPassSegInvUpdateForOverbook(tempSegAlloc.getBookingClassType()))) {
					getFlightInventoryDAO().updateFccSegInvInfants(tempSegAlloc.getFccaId(), infantSeatsCountToSell,
							BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType())
									|| "Y".equals(tempSegAlloc.getSkipSegInvUpdate()));

					List<Integer> interceptingIds = getFlightInventoryDAO()
							.getInterceptingFCCSegmentInventoryIds(tempSegAlloc.getFccaId(), true);
					// update intercepting segments' seat availability
					if (interceptingIds != null && !interceptingIds.isEmpty()) {
						interceptingIds.add(tempSegAlloc.getFccaId());
						getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(interceptingIds);
					}
					if (log.isDebugEnabled()) {
						soldSeatsSummary.append("Infant seats : \n\r").append("Temp block Rec Id      = ")
								.append(tempSegAlloc.getId()).append("\n\r").append("FccsInvId              = ")
								.append(tempSegAlloc.getFccaId()).append("\n\r").append("FccsbInvId             = ")
								.append(tempSegAlloc.getFccsbaId()).append("\n\r").append("Total blocket inf seats= ")
								.append(tempSegAlloc.getSeatsBlocked()).append("\n\r").append("Moved to sold          = ")
								.append(infantSeatsCountToSell.intValue()).append("\n\r").append("Blocked timestamp      = ")
								.append(tempSegAlloc.getTimestamp()).append("\n\r").append("Prev Status Chg Action = ")
								.append(tempSegAlloc.getPrevStatusChgAction()).append("\n\r")
								.append("BookingClass Type              = ").append(tempSegAlloc.getBookingClassType())
								.append("\n\r");
					}
				}
			}
			// update nesting info table, if seats were nested
			Integer nestRecId = tempSegAlloc.getNestRecordId();
			if (nestRecId != null) {
				if (seatsCountToSell != null) {
					if (seatsCountToSell.intValue() > 0) {
						if (getFlightInventoryDAO().updateBCInvNestingInfo(nestRecId, -seatsCountToSell.intValue(),
								seatsCountToSell.intValue()) == 0) {
							throw new ModuleException("airinventory.logic.moving.blocked.seats.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}

						if (getFlightInventoryDAO().updateBCInvNestingToNonTemporary(nestRecId) == 0) {
							throw new ModuleException("airinventory.logic.moving.blocked.seats.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}
					}
				}
			}

			// for keeping a backup of the records
			if (trackBlockedSeats) {
				bkpBlockedSeats.add(tempSegAlloc.copyAndUpdateStatus(AirinventoryCustomConstants.Seats.CONSUMED));
			}

			log.debug("================TempSegBcAlloc @ before deleteTempSegBCInvClosure [hashCode =" + tempSegAlloc.hashCode()
					+ "]");

			// delete any bucket closure info saved
			getFlightInventoryDAO().deleteTempSegBCInvClosure(tempSegAlloc.getId());

			log.debug("================TempSegBcAlloc @ after deleteTempSegBCInvClosure [hashCode =" + tempSegAlloc.hashCode()
					+ "]");

			getFlightInventoryDAO().removeTempSegBcAlloc(tempSegAlloc);

			iterator++;
			if (blockedRecords.size() == iterator) {
				// last update then close
				getFlightInventoryDAO().updateBCStatusForNestAvailability(tempSegAlloc.getFccaId());
			}
		}

		if (trackBlockedSeats) {
			try {
				getFlightInventoryDAO().saveAllTempSegBcAlloc(bkpBlockedSeats);
			} catch (Exception e) {
				// Since blocked seats are saved for audit purpose only, exception is not propagated
				log.error("Error When moving TempSegBCAlloc to Backup Table", e);
			}
		}

		if (log.isDebugEnabled()) {
			soldSeatsSummary.append("\n\r-----------------------------------------------------------\n\r");
			log.debug(soldSeatsSummary.toString());
		}
		// log.debug("Exiting moveBlockedSeatsToSold [" + Thread.currentThread().getName() + "] time=" +
		// System.currentTimeMillis());
	}

	/**
	 * For cancelling temporarility blocked seats Steps: Removes onhold seats on segment and bc inventory Remove any
	 * nesting record Removes any temporary record on bucket closure Remove temporary seat blocking record
	 * 
	 * @param blockedRecords
	 * 
	 *            Scheduled Task will 'R - Release Seats' and will purge all the seats which are not blocked.
	 * @param userPrincipal
	 *            TODO
	 * 
	 */
	public void releaseBlockedInventory(Collection<TempSegBcAlloc> blockedRecords, boolean isFailSilently,
			UserPrincipal userPrincipal) throws ModuleException {
		// check configuration if want to keep track of blocked seats
		boolean trackBlockedSeats = checkConfigForTrackingBlockedSeats();
		Collection<TempSegBcAllocBKP> blockedSeatsForBKP = new ArrayList<TempSegBcAllocBKP>();
		Collection<Integer> fccsInvId = new ArrayList<Integer>();
		boolean needAcquireLock = false;
		int iterator = 0;
		Iterator<TempSegBcAlloc> itBlockedList = blockedRecords.iterator();
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();
		
		while (itBlockedList.hasNext()) {
			List<Integer> interceptingSegs = null;
			List<Integer> fccsInvIds = null;
			TempSegBcAlloc tempSegAlloc = null;
			InventoryDiffForAVS inventoryDiffForAVS = null;
			Collection<TempSegBCInvClosure> bucketsToReopen = null;
			try {
				tempSegAlloc = itBlockedList.next();
				inventoryDiffForAVS = new InventoryDiffForAVS(tempSegAlloc.getFccaId());

				inventoryDiffForAVS.takeIntialInvSnapshot();
				if (getFlightInventoryDAO().getTempSegBcAlloc(tempSegAlloc.getId()) != null) {// to prevent problem in
																								// incorrectly
																								// requesting releasing
																								// seats many times
					if (fccsInvId.contains(tempSegAlloc.getFccaId())) {
						needAcquireLock = false;
					} else {
						fccsInvId.add(tempSegAlloc.getFccaId());
						needAcquireLock = true;
					}
					if (!"Y".equals(tempSegAlloc.getSkipSegInvUpdate())
							&& !BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType())) {
						interceptingSegs = getFlightInventoryDAO()
								.getInterceptingFCCSegmentInventoryIds(tempSegAlloc.getFccaId().intValue(), true);
					}

					if (trackBlockedSeats) {
						try {
							if (isFailSilently) {
								blockedSeatsForBKP
										.add(tempSegAlloc.copyAndUpdateStatus(AirinventoryCustomConstants.Seats.RELEASED));
							} else {
								blockedSeatsForBKP
										.add(tempSegAlloc.copyAndUpdateStatus(AirinventoryCustomConstants.Seats.ABORTED));
							}
						} catch (Exception e) {
							log.error("Error releaseBlockedSeats when Copying the DTO to BKP Temp Seg Alloc", e);
						}
					}

					getFlightInventoryDAO().removeTempSegBcAlloc(tempSegAlloc);

					if (tempSegAlloc.getFccsbaId() != null) {
						// no segment inventory update for standby bookings
						if (!"Y".equals(tempSegAlloc.getSkipSegInvUpdate())
								&& !BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType())) {

							// acquiring lock on the intercepting segment
							// inventories
							if (needAcquireLock && interceptingSegs != null && interceptingSegs.size() > 0
									&& getAirInventoryConfig().isApplyLockingForBlockSeatRel()) {
								fccsInvIds = new ArrayList<Integer>();
								fccsInvIds.add(tempSegAlloc.getFccaId());
								fccsInvIds.addAll(interceptingSegs);

								String segIdsStr = "";
								if (log.isDebugEnabled()) {
									for (Iterator<Integer> it = fccsInvIds.iterator(); it.hasNext();) {
										if (segIdsStr.equals("")) {
											segIdsStr += it.next();
										} else {
											segIdsStr += "," + it.next();
										}
									}
									log.debug(
											"Going to acquire lock on seg/intercepting segs for blocking seats release [fccSegInvIds="
													+ segIdsStr + "]");
								}
								AirInventoryDAOUtils.getInventoryResDAO().getFCCSegInventoriesForUpdate(fccsInvIds, false,
										getAirInventoryConfig().getMaxWaitForLockDuration());
								if (log.isDebugEnabled()) {
									log.debug("Acquireed lock on seg/intercepting segs for blocking seats release [fccSegInvIds="
											+ segIdsStr + "]");
								}
							}

							getFlightInventoryDAO().releaseFccSegInvAdults(tempSegAlloc.getFccaId(),
									tempSegAlloc.getSeatsBlocked());

						}

						// retrieve any bucket closure resulted
						bucketsToReopen = getFlightInventoryDAO().getTempSegBCInvClosure(tempSegAlloc.getId());

						// remove any bucket closure info saved
						getFlightInventoryDAO().deleteTempSegBCInvClosure(tempSegAlloc.getId());

						// delete nesting info record, if seats were nested
						Integer nestRecId = tempSegAlloc.getNestRecordId();

						if (nestRecId != null) {
							int updateCount = getFlightInventoryDAO().deleteBCInvNestingInfo(nestRecId);
							if (updateCount == 0) {
								log.error("Nested inventory release failed for  " + tempSegAlloc.getFccsbaId()
										+ " bucket. Requested adults - " + tempSegAlloc.getSeatsBlocked().intValue()
										+ ". Nest block seats: " + nestRecId);
								throw new ModuleException("airinventory.logic.bl.blocked.release.failed",
										AirinventoryCustomConstants.INV_MODULE_CODE);
							}
						}

						// update bc inventory
						UpdateFccSegBCInvResponse bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(
								tempSegAlloc.getFccsbaId(), -tempSegAlloc.getSeatsBlocked().intValue(), 0, false, bucketsToReopen,
								false,

								BookingClassUtil.byPassSegInvUpdateForOverbook(tempSegAlloc.getBookingClassType()), 0,
								userPrincipal.getUserId());

						if (bcUpdateResponse.getUpdateCount() == 0) {
							log.error("Seat release failed from Block OHD due to insufficient seats in "
									+ tempSegAlloc.getFccsbaId() + " bucket. Requested adults - "
									+ tempSegAlloc.getSeatsBlocked().intValue());
							throw new ModuleException("airinventory.logic.bl.blocked.release.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);

						}

						// open the bucket if bucket is closed and not manually
						// closed
						getFlightInventoryDAO().reOpenFCCSegBCInventory(tempSegAlloc.getFccsbaId().intValue(), tempSegAlloc);

						/*
						 * if (!tempSegAlloc.getBookingClassType().equals(BookingClass.BookingClassType.STANDBY)){//no
						 * segment inventory update for standby List interceptingSegs = getFlightInventoryDAO()
						 * .getInterceptingFCCSegmentInventoryIds(tempSegAlloc.getFccaId().intValue());
						 * getFlightInventoryDAO().updateFCCSegmentInventoriesAvailability(interceptingSegs); }
						 */
						if (log.isDebugEnabled()) {
							log.debug(new StringBuffer().append("Blocked seats to be removed :: Adult seats : \n\r")
									.append("Temp block Rec Id      = ").append(tempSegAlloc.getId()).append("\n\r")
									.append("FccsInvId              = ").append(tempSegAlloc.getFccaId()).append("\n\r")
									.append("FccsbInvId             = ").append(tempSegAlloc.getFccsbaId()).append("\n\r")
									.append("No of Seats            = ").append(tempSegAlloc.getSeatsBlocked()).append("\n\r")
									.append("Blocked timestamp      = ").append(tempSegAlloc.getTimestamp()).append("\n\r")
									.append("Prev Status Chg Action = ").append(tempSegAlloc.getPrevStatusChgAction())
									.append("\n\r").append("Booking Type              = ")
									.append(tempSegAlloc.getBookingClassType()).append("\n\r").append("Nested Rec Id          = ")
									.append(tempSegAlloc.getNestRecordId()).append("\n\r").toString());
						}
					} else {
						if (!"Y".equals(tempSegAlloc.getSkipSegInvUpdate())
								&& !BookingClassUtil.byPassSegInvUpdate(tempSegAlloc.getBookingClassType())) {

							// acquiring lock on the intercepting segment inventories
							if (needAcquireLock && interceptingSegs != null && interceptingSegs.size() > 0
									&& getAirInventoryConfig().isApplyLockingForBlockSeatRel()) {
								fccsInvIds = new ArrayList<Integer>();
								fccsInvIds.add(tempSegAlloc.getFccaId());
								fccsInvIds.addAll(interceptingSegs);

								String segIdsStr = "";
								if (log.isDebugEnabled()) {
									for (Iterator<Integer> it = fccsInvIds.iterator(); it.hasNext();) {
										if (segIdsStr.equals("")) {
											segIdsStr += it.next();
										} else {
											segIdsStr += "," + it.next();
										}
									}
									log.debug(
											"Going to acquire lock on seg/intercepting segs for blocking seats release [fccSegInvIds="
													+ segIdsStr + "]");
								}
								AirInventoryDAOUtils.getInventoryResDAO().getFCCSegInventoriesForUpdate(fccsInvIds, false,
										getAirInventoryConfig().getMaxWaitForLockDuration());
								if (log.isDebugEnabled()) {
									log.debug("Acquireed lock on seg/intercepting segs for blocking seats release [fccSegInvIds="
											+ segIdsStr + "]");
								}
							}

							getFlightInventoryDAO().releaseFccSegInvInfants(tempSegAlloc.getFccaId(),
									tempSegAlloc.getSeatsBlocked());
						}

						/*
						 * List interceptingSegs = getFlightInventoryDAO()
						 * .getInterceptingFCCSegmentInventoryIds(tempSegAlloc.getFccaId().intValue());
						 * getFlightInventoryDAO().updateFCCSegmentInventoriesAvailability(interceptingSegs);
						 */

						if (log.isDebugEnabled()) {
							log.debug(new StringBuffer().append("Blocked seats to be removed :: Infant seats : \n\r")
									.append("Temp block Rec Id      = ").append(tempSegAlloc.getId()).append("\n\r")
									.append("FccsInvId              = ").append(tempSegAlloc.getFccaId()).append("\n\r")
									.append("FccsbInvId             = ").append(tempSegAlloc.getFccsbaId()).append("\n\r")
									.append("No of Seats            = ").append(tempSegAlloc.getSeatsBlocked()).append("\n\r")
									.append("Blocked timestamp      = ").append(tempSegAlloc.getTimestamp()).append("\n\r")
									.append("Prev Status Chg Action = ").append(tempSegAlloc.getPrevStatusChgAction())
									.append("\n\r").append("Booking Type           = ").append(tempSegAlloc.getBookingClassType())
									.append("\n\r").toString());
						}
					}

					Collection<Integer> fullSegs = new HashSet<Integer>();
					fullSegs.add(tempSegAlloc.getFccaId());

					if (interceptingSegs != null && interceptingSegs.size() > 0) {
						fullSegs.addAll(interceptingSegs);
					}

					// update intercepting segments' seat availability
					if (interceptingSegs != null && interceptingSegs.size() > 0) {
						getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(interceptingSegs);

					}

					inventoryEventPublisher.filterAndAdd(tempSegAlloc.getBookingClassType(), false, tempSegAlloc.getFccaId());

				} else {
					log.warn("Removing already removed temporarily blocked seats requested [tempSegAlloc.Id="
							+ tempSegAlloc.getId() + " tempSegAlloc.FccaId=" + tempSegAlloc.getFccaId()
							+ " tempSegAlloc.FccsbaId=" + tempSegAlloc.getFccsbaId() + " tempSegAlloc.SeatsBlocked="
							+ tempSegAlloc.getSeatsBlocked() + "]");
				}
			} catch (Exception ex) {
				log.error("Removing temporarily blocked seats failed [tempSegAlloc.Id=" + tempSegAlloc.getId()
						+ " tempSegAlloc.FccaId=" + tempSegAlloc.getFccaId() + " tempSegAlloc.FccsbaId="
						+ tempSegAlloc.getFccsbaId() + " tempSegAlloc.SeatsBlocked=" + tempSegAlloc.getSeatsBlocked() + "]", ex);
				if (!isFailSilently) {
					throw new ModuleException(ex, "airinventory.logic.bl.blocked.release.failed",
							AirinventoryCustomConstants.INV_MODULE_CODE);

				}
			}

			iterator++;
			if (blockedRecords.size() == iterator) {
				// last update then close
				getFlightInventoryDAO().updateBCStatusForNestAvailability(tempSegAlloc.getFccaId());
			}

			/** save to the backup table of TempSegBCAllocBKP **/
			if (trackBlockedSeats) {
				try {
					getFlightInventoryDAO().saveAllTempSegBcAlloc(blockedSeatsForBKP);
				} catch (CommonsDataAccessException e) {
					log.error("When in ReleaseBlockedSeats when Saving to Temp Seg BC Alloc BKP", e);
				}
			}

			if (inventoryDiffForAVS != null) {
				inventoryDiffForAVS.takeFinalInvSnapshotAndPublishDiff();
			} else {
				log.error("Removing temporarily blocked seats AVS update failed [tempSegAlloc.Id=" + tempSegAlloc.getId()
						+ " tempSegAlloc.FccaId=" + tempSegAlloc.getFccaId() + " tempSegAlloc.FccsbaId="
						+ tempSegAlloc.getFccsbaId() + " tempSegAlloc.SeatsBlocked=" + tempSegAlloc.getSeatsBlocked() + "]");
				if (!isFailSilently) {
					throw new ModuleException("airinventory.logic.bl.blocked.release.failed",
							AirinventoryCustomConstants.INV_MODULE_CODE);
				}
			}
		}
		
		inventoryEventPublisher.publishEvents();

	}

	/**
	 * For onholding the temporarily blocked seats Steps: Remove temporary seat blocking records from temporary
	 * allocation table
	 * 
	 * @param blockedRecords
	 * @throws ModuleException
	 */
	private void removeBlockedSeats(Collection<TempSegBcAlloc> blockedRecords) throws ModuleException {
		// log.debug("Entering removeBlockedSeats [" + Thread.currentThread().getName() + "] time=" +
		// System.currentTimeMillis());

		boolean trackBlockedSeats = checkConfigForTrackingBlockedSeats();
		Collection<TempSegBcAllocBKP> bkpBlockedSeats = new ArrayList<TempSegBcAllocBKP>();

		StringBuffer deleteBlockedRecSummary = null;
		if (log.isDebugEnabled()) {
			deleteBlockedRecSummary = new StringBuffer();
			deleteBlockedRecSummary.append("\n\r--------temporary alloc records TO BE deleted for ONHOLDING SEATS--------\n\r");
			deleteBlockedRecSummary.append("Deleted Rec Ids =[ ");
		}
		Iterator<TempSegBcAlloc> itBlockedList = blockedRecords.iterator();
		// to hold the tempQuotedFareIds to be deleted with block seats
		while (itBlockedList.hasNext()) {
			TempSegBcAlloc tempSegBcAlloc = itBlockedList.next();

			if (TempBlockSeatUtil.isExpired(tempSegBcAlloc)) {
				log.error("Moving blocked seats to ohd failed. " + "[blockRecId=" + tempSegBcAlloc.getId() + ",totalBlockedSeats="
						+ tempSegBcAlloc.getSeatsBlocked() + ",expiry=" + tempSegBcAlloc.getTimestamp() + "]");
				throw new ModuleException("airinventory.booking.class.inventory.invalid",
						AirinventoryCustomConstants.INV_MODULE_CODE);
			}

			Integer recId = tempSegBcAlloc.getId();

			log.debug("================@ removeBlockedSeats TempSegBcAlloc [hashCode =" + tempSegBcAlloc.hashCode()
					+ "trackBlockedSeats" + trackBlockedSeats + " ]");
			// for keeping a backup of the records
			if (trackBlockedSeats) {
				bkpBlockedSeats.add(tempSegBcAlloc.copyAndUpdateStatus(AirinventoryCustomConstants.Seats.CONSUMED));
			}

			log.debug("================@ removeBlockedSeats after tracked TempSegBcAlloc [hashCode =" + tempSegBcAlloc.hashCode()
					+ "trackBlockedSeats" + trackBlockedSeats + " ]");

			// delete any bucket closure info
			getFlightInventoryDAO().deleteTempSegBCInvClosure(tempSegBcAlloc.getId());

			getFlightInventoryDAO().removeTempSegBcAlloc(tempSegBcAlloc);

			if (tempSegBcAlloc.getNestRecordId() != null) {
				if (getFlightInventoryDAO().updateBCInvNestingToNonTemporary(tempSegBcAlloc.getNestRecordId()) == 0) {
					throw new ModuleException("airinventory.booking.class.inventory.invalid",
							AirinventoryCustomConstants.INV_MODULE_CODE);
				}
			}

			if (log.isDebugEnabled()) {
				deleteBlockedRecSummary.append(recId + " ");
			}
			if (!"Y".equals(tempSegBcAlloc.getSkipSegInvUpdate())
					&& BookingClassUtil.byPassSegInvUpdateForOverbook(tempSegBcAlloc.getBookingClassType())) {
				// no segment inventory update for standby bookings.
				int numberOfOnholdInfantSeats = 0;
				if (tempSegBcAlloc.getFccsbaId() == null) {
					// infant count
					numberOfOnholdInfantSeats = tempSegBcAlloc.getSeatsBlocked();
				}
				List<Integer> interceptingIds = getFlightInventoryDAO()
						.getInterceptingFCCSegmentInventoryIds(tempSegBcAlloc.getFccaId(), true);
				if (interceptingIds == null || interceptingIds.isEmpty()) {
					getFlightInventoryDAO().updateFCCSegmentInventory(tempSegBcAlloc.getFccaId(), numberOfOnholdInfantSeats, 0);
				} else {
					int[] totalSoldOhdCountIncludingInterceptingSegments = getFlightInventoryDAO()
							.getMaximumEffectiveReservedSetsCountInInterceptingSegments(interceptingIds);
					getFlightInventoryDAO().updateFCCSegmentInventory(tempSegBcAlloc.getFccaId(), numberOfOnholdInfantSeats,
							totalSoldOhdCountIncludingInterceptingSegments[0]);
				}

				// update intercepting segments' seat availability
				if (interceptingIds != null && !interceptingIds.isEmpty()) {
					getFlightInventoryDAO().updateFCCInterceptingSegmentInvAndAVS(interceptingIds);
				}
			}
		}

		if (trackBlockedSeats) {
			try {
				getFlightInventoryDAO().saveAllTempSegBcAlloc(bkpBlockedSeats);
			} catch (Exception e) {
				// Since blocked seats are saved for audit purpose only, exception is not propagated
				log.error("Error When moving TempSegBCAlloc to Backup Table", e);
			}
		}

		if (log.isDebugEnabled()) {
			deleteBlockedRecSummary.append("]\n\r");
			deleteBlockedRecSummary.append("-------------------------------------------------------------------\n\r");
			log.debug(deleteBlockedRecSummary.toString());
		}
		// log.debug("Exiting removeBlockedSeats [" + Thread.currentThread().getName() + "] time=" +
		// System.currentTimeMillis());
	}

	/**
	 * Removes temporarily blocked records along with onhold seats Steps: Filter blocked records to remove based on time
	 * elapsed from the seat being blocked Remove segment and bc inventory onhold seats, along with any nesting records
	 * Remove temporary block records
	 * 
	 * @param numberOfMins
	 * @throws ModuleException
	 */
	public void cleanupBlockedSeats(boolean isScheduledService) throws ModuleException {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(new Date(System.currentTimeMillis()));
		// calander.add(GregorianCalendar.MINUTE, -numberOfMins);

		Date cleanUpTime = calander.getTime();

		Collection<TempSegBcAlloc> blockedRecsForCleanup = getFlightInventoryDAO().findBlockedSeatsRecsForCleanUp(cleanUpTime);
		if (log.isDebugEnabled()) {
			log.debug("Going to clean up temporarily blocked seats [Clean up timestamp = " + cleanUpTime + "]");
		}

		if (blockedRecsForCleanup != null) {
			Iterator<TempSegBcAlloc> blockedRecsForCleanupIt = blockedRecsForCleanup.iterator();
			while (blockedRecsForCleanupIt.hasNext()) {
				try {
					TempSegBcAlloc tempSegAlloc = blockedRecsForCleanupIt.next();

					if (log.isInfoEnabled()) {
						StringBuffer msg = new StringBuffer(
								"\n\r-----------------TEMPORARILY  B L O C K E D  SEATS TO BE  R E M O V E D-----------------\n\r");
						msg.append("Blocked records TO BE removed>> \n\r");
						msg.append("Temp block Rec Id      = ").append(tempSegAlloc.getId()).append("\n\r");
						msg.append("FccsInvId              = ").append(tempSegAlloc.getFccaId()).append("\n\r");
						msg.append("FccsbInvId             = ").append(tempSegAlloc.getFccsbaId()).append("\n\r");
						msg.append("No of Seats            = ").append(tempSegAlloc.getSeatsBlocked()).append("\n\r");
						msg.append("Nest Rec Id            = ").append(tempSegAlloc.getNestRecordId()).append("\n\r");
						msg.append("Blocked timestamp      = ").append(tempSegAlloc.getTimestamp()).append("\n\r");
						msg.append("Prev Status Chg Action  = ").append(tempSegAlloc.getPrevStatusChgAction()).append("\n\r");
						msg.append("Booking Type           = ").append(tempSegAlloc.getBookingClassType()).append("\n\r");
						msg.append("-------------------------------------------------------------------------------------\n\r");

						if (log.isDebugEnabled()) {
							log.debug(msg.toString());
						}
					}

					Collection<TempSegBcAlloc> blockedSeatsCol = new ArrayList<TempSegBcAlloc>();
					blockedSeatsCol.add(tempSegAlloc);
					getFlightInventoryResBD().releaseBlockedSeatsInIndepedentTx(blockedSeatsCol, isScheduledService);
				} catch (Exception ex) {
					log.error("Blocked seats release failed", ex);
				}
			}
		}
		// this.releaseBlockedSeats(blockedRecsForCleanup, true);
	}

	/**
	 * Moves onhold seats to sold state.
	 * 
	 * @param segmentSeatDistDTOs
	 * @param exchangedSegInvIds
	 */
	public void confirmOnholdSeats(Collection<SegmentSeatDistsDTO> segmentSeatDistDTOs, Collection<Integer> exchangedSegInvIds)
			throws ModuleException {
		StringBuffer cnfOnholdSeatsSummary = null;
		if (log.isDebugEnabled()) {
			cnfOnholdSeatsSummary = new StringBuffer();
			cnfOnholdSeatsSummary.append("\n\r--------Confirming Onhold Seats--------\n\r");
		}
		
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();

		for (SegmentSeatDistsDTO segmentSeatDistsDTO : segmentSeatDistDTOs) {
			Map<String, Integer> logicalCabinClassWiseTotalSeats = new HashMap<String, Integer>();
			Integer fligthId = null;
			String segmentCode = null;
			int iterator = 0;

			Map<String, Boolean> segmentLogicalCabinClassWiseLockAquired = new HashMap<String, Boolean>();
			for (SeatDistribution seatDist : segmentSeatDistsDTO.getSeatDistribution()) {
				// get bc inventory
				ExtendedFCCSegBCInventory exBCInv = getFlightInventoryDAO()
						.getExtendedBCInventory(segmentSeatDistsDTO.getFlightSegId(), seatDist.getBookingClassCode());
				FCCSegBCInventory bcInv = exBCInv.getFccSegBCInventory();
				String logicalCabinClassCode = bcInv.getLogicalCCCode();
				fligthId = new Integer(bcInv.getFlightId());
				segmentCode = bcInv.getSegmentCode();
				String bookingClassType = seatDist.getBookingClassType();
				if (bookingClassType == null) {
					bookingClassType = getBookingClassJDBCDAO().getBookingClassType(seatDist.getBookingClassCode());
				}
				if (exchangedSegInvIds != null && exchangedSegInvIds.size() > 0) {
					if (exchangedSegInvIds.contains(bcInv.getfccsInvId())) {
						continue;
					}
				}

				// check segment on-hold seat eligibility; check is skipped for standby bookings
				if (bookingClassType != null && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
					boolean segOnholdCountOk = getFlightInventoryDAO().checkSegSoldOnholdCountValidity(bcInv.getfccsInvId(), 0,
							seatDist.getNoOfSeats(), 0, segmentSeatDistsDTO.getNoOfInfantSeats());
					if (!segOnholdCountOk) {
						log.error("Invalid onhold seat confirmation operation. "
								+ "Requested number of onhold adult or onhold infant seats not found on the segment.\n"
								+ "[segmentInvId=" + bcInv.getfccsInvId() + ", " + "adult onhold seats requested="
								+ seatDist.getNoOfSeats() + ", " + "infant onhold seats requested="
								+ segmentSeatDistsDTO.getNoOfInfantSeats() + "]");
						throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
					int logicalCabinClassWiseSeats = seatDist.getNoOfSeats();
					if (logicalCabinClassWiseTotalSeats.get(logicalCabinClassCode) != null) {
						logicalCabinClassWiseSeats += logicalCabinClassWiseTotalSeats.get(logicalCabinClassCode);
					}
					logicalCabinClassWiseTotalSeats.put(logicalCabinClassCode, logicalCabinClassWiseSeats);
				}

				// acquire the update lock on the segment inventories of the flight
				if ((segmentLogicalCabinClassWiseLockAquired.get(logicalCabinClassCode) == null
						|| !segmentLogicalCabinClassWiseLockAquired.get(logicalCabinClassCode).booleanValue())
						&& getAirInventoryConfig().isApplyLockingForOhdConfirm()) {
					if (log.isDebugEnabled()) {
						log.debug("Going to acquire lock on seg for ohd confirm [fccSegInvId=" + bcInv.getfccsInvId() + "]");
					}
					AirInventoryDAOUtils.getInventoryResDAO().getFCCSegInventoryForUpdate(new Integer(bcInv.getfccsInvId()),
							false, getAirInventoryConfig().getMaxWaitForLockDuration());
					if (log.isDebugEnabled()) {
						log.debug("Acquired lock on seg for ohd confirm [fccSegInvId=" + bcInv.getfccsInvId() + "]");
					}
					segmentLogicalCabinClassWiseLockAquired.put(logicalCabinClassCode, true);
				}

				if (exBCInv.isStandardCode()) {
					// get nested-in on-hold seat info, order by nest rank
					Collection<FCCSegBCInvNesting> nestedInOnholdSeatsCol = getFlightInventoryDAO()
							.getNestedSeats(bcInv.getFccsbInvId().intValue(), false, false);

					// get nested-out seats
					int[] nestedOutSoldOnholdSeats = getFlightInventoryDAO()
							.getNestedOutOrBlockedSeatCounts(bcInv.getFccsbInvId().intValue());
					int effectiveOnholdSeats = bcInv.getOnHoldSeats() - nestedOutSoldOnholdSeats[1];

					if (effectiveOnholdSeats < 0) {
						// inventory inconsistent data found
						log.error("Inconsistent onhold data found in inventory. [bcInvId=" + bcInv.getFccsbInvId()
								+ ", effectiveOnholdSeats=" + effectiveOnholdSeats + "]");
						throw new ModuleException("airinventory.logic.bl.onhold.confirmation.data.inconsistency",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}

					// check on-hold seats count eligibility
					if (effectiveOnholdSeats < seatDist.getNoOfSeats()) {
						int onholdSeatsFound = effectiveOnholdSeats;
						if (nestedInOnholdSeatsCol != null) {
							for (FCCSegBCInvNesting nestedRecord : nestedInOnholdSeatsCol) {
								onholdSeatsFound += nestedRecord.getOnholdSeats();
								if (onholdSeatsFound >= seatDist.getNoOfSeats()) {
									break;
								}
							}
						}
						if (onholdSeatsFound < seatDist.getNoOfSeats()) {
							log.error("Invalid onhold confirmation operation. "
									+ "Requested number of onhold adult seats not found on the bc inventory.\n" + "[bcInvId="
									+ bcInv.getFccsbInvId() + ", " + "adult onhold seats requested=" + seatDist.getNoOfSeats()
									+ "]");
							throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}
					}

					// ok to proceed with on-hold confirmation
					boolean onholdConfirmationCompleted = false;
					int nestedOnholdSeatsCount = 0;
					if (nestedInOnholdSeatsCol != null) {
						// first confirm seats from the nested IN bucket(s)
						for (FCCSegBCInvNesting nestedInRecord : nestedInOnholdSeatsCol) {
							if (nestedOnholdSeatsCount < seatDist.getNoOfSeats()) {
								int noOfSeatToConfirm = 0;
								if ((nestedOnholdSeatsCount + nestedInRecord.getOnholdSeats()) < seatDist.getNoOfSeats()) {
									noOfSeatToConfirm = nestedInRecord.getOnholdSeats();
								} else if ((nestedOnholdSeatsCount + nestedInRecord.getOnholdSeats()) == seatDist
										.getNoOfSeats()) {
									noOfSeatToConfirm = nestedInRecord.getOnholdSeats();
									onholdConfirmationCompleted = true;
								} else {
									noOfSeatToConfirm = seatDist.getNoOfSeats() - nestedOnholdSeatsCount;
									onholdConfirmationCompleted = true;
								}
								nestedOnholdSeatsCount += noOfSeatToConfirm;
								// update nest record, sold +=noOfSeatToConfirm, on-hold-=noOfSeatToConfirm
								int updatedCount = getFlightInventoryDAO().updateBCInvNestingInfo(
										nestedInRecord.getFccsbInvNestingId(), -noOfSeatToConfirm, noOfSeatToConfirm);

								if (updatedCount != 1) {
									log.error("Nested inventory update failed. "
											+ "Requested number of onhold adult seats not found on the bc inventory.\n"
											+ "[bcInvNestId=" + nestedInRecord.getFccsbInvNestingId() + ", "
											+ "adult onhold seats requested=" + noOfSeatToConfirm + "]");
									throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
											AirinventoryCustomConstants.INV_MODULE_CODE);
								}

								// update the nested bucket; sold +=noOfSeatToConfirm, on-hold-=noOfSeatToConfirm
								UpdateFccSegBCInvResponse bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(
										nestedInRecord.getFromFCCSegBCInvId(), -noOfSeatToConfirm, noOfSeatToConfirm, false, null,
										false, BookingClassUtil.byPassSegInvUpdateForOverbook(bookingClassType), 0, null);
								// UpdateFccSegBCInvResponse bcUpdateResponse =
								if (bcUpdateResponse.getUpdateCount() == 0) {
									log.error("Seat move from OHD to CNF failed due to insufficient seats in "
											+ nestedInRecord.getFromFCCSegBCInvId() + " bucket. Requested adults - "
											+ noOfSeatToConfirm);
									throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
											AirinventoryCustomConstants.INV_MODULE_CODE);

								}
								if (onholdConfirmationCompleted) {
									break;
								}
							}
						}
					}
					if (!onholdConfirmationCompleted) {
						int remainingOnholdSeatsToConfirm = seatDist.getNoOfSeats() - nestedOnholdSeatsCount;
						// update the bucket; sold+=remainingOnholdSeatsToConfirm,
						// on-hold-=remainingOnholdSeatsToConfirm
						UpdateFccSegBCInvResponse bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(bcInv.getFccsbInvId(), -remainingOnholdSeatsToConfirm,
								remainingOnholdSeatsToConfirm, false, null, false,
								BookingClassUtil.byPassSegInvUpdateForOverbook(bookingClassType), 0, null);
					
						if (bcUpdateResponse.getUpdateCount() == 0) {
							log.error("Seat move from OHD to CNF failed due to insufficient seats in " + bcInv.getFccsbInvId()
									+ " bucket. Requested adults - " + remainingOnholdSeatsToConfirm);
							throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);

						}
					}
				} else {
					// non-standard seat confirmation
					if (bcInv.getOnHoldSeats() < seatDist.getNoOfSeats()) {
						// invalid on-hold seats count
						log.error("Invalid onhold confirmation operation. "
								+ "Requested number of onhold adult seats not found on the bc inventory.\n" + "[bcInvId="
								+ bcInv.getFccsbInvId() + ", " + "adult onhold seats requested=" + seatDist.getNoOfSeats() + "]");
						throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}
					// update the non-standard bucket, then +x sold, -x on-hold
					UpdateFccSegBCInvResponse bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(
							bcInv.getFccsbInvId(), -seatDist.getNoOfSeats(), seatDist.getNoOfSeats(), false, null, false,
							BookingClassUtil.byPassSegInvUpdateForOverbook(bookingClassType), 0, null);
				
					if (bcUpdateResponse.getUpdateCount() == 0) {
						log.error("Seat move from OHD to CNF failed due to insufficient seats in " + bcInv.getFccsbInvId()
								+ " bucket. Requested adults - " + seatDist.getNoOfSeats());
						throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
								AirinventoryCustomConstants.INV_MODULE_CODE);

					}
				}
				
				inventoryEventPublisher.filterAndAdd(bookingClassType, segmentSeatDistsDTO.isBelongToInboundOnd(), bcInv.getfccsInvId());

				iterator++;
				if (segmentSeatDistsDTO.getSeatDistribution().size() == iterator) {
					// last update then close
					getFlightInventoryDAO().updateBCStatusForNestAvailability(bcInv.getfccsInvId());
				}

			}
			// update segment inventory; +x sold adult, -x onhold adult, +y sold infant, -y onhold infant
			// segment update is skipped for standby bookings
			for (String logicalCabinClass : logicalCabinClassWiseTotalSeats.keySet()) {
				int segUpdateCount = getFlightInventoryDAO().updateFccSegInventorySoldOnholdSeats(fligthId.intValue(),
						logicalCabinClass, segmentCode, logicalCabinClassWiseTotalSeats.get(logicalCabinClass),
						-logicalCabinClassWiseTotalSeats.get(logicalCabinClass), segmentSeatDistsDTO.getNoOfInfantSeats(),
						-segmentSeatDistsDTO.getNoOfInfantSeats(), false, true, true, 0);
				// TODO : we need to pass whether this is a fixed seat confirmation or not with out hard coding false;
				// Since this is onhold confirmation we can skip the segment inventory check.

				if (segUpdateCount == 0) {
					String nl = "\n\r";
					log.error("Aborting seats blocking. Segment inventory availability check failed. "
							+ "Not enough adult or infant seats." + nl + "FlightId           	= " + fligthId + nl
							+ "Segment Code       	= " + segmentCode + nl + "LogicalCabinClass Code    	= "
							+ logicalCabinClass + nl + "Requested Adult Seats  = "
							+ logicalCabinClassWiseTotalSeats.get(logicalCabinClass) + nl + "Requested Infant Seats = "
							+ segmentSeatDistsDTO.getNoOfInfantSeats() + nl);
					throw new ModuleException("airinventory.logic.bl.onhold.confirmation.failed",
							AirinventoryCustomConstants.INV_MODULE_CODE);
				}
			}

			if (log.isDebugEnabled()) {
				cnfOnholdSeatsSummary.append("\n\rSegment Seats Distribution").append("\n\r")
						.append("Flight Seg Id		= " + segmentSeatDistsDTO.getFlightSegId() + "\n\r");
				if (segmentSeatDistsDTO.getSeatDistribution() != null && segmentSeatDistsDTO.getSeatDistribution().size() > 0) {
					cnfOnholdSeatsSummary.append("\n\r").append("Adult Seat Distribution [bc,onhold]");
					Iterator<SeatDistribution> seatIt = segmentSeatDistsDTO.getSeatDistribution().iterator();
					while (seatIt.hasNext()) {
						SeatDistribution seatDist = seatIt.next();
						cnfOnholdSeatsSummary.append("[" + seatDist.getBookingClassCode())
								.append("," + seatDist.getNoOfSeats() + "]");
					}
				}
				cnfOnholdSeatsSummary.append("\n\r")
						.append("Infant Seats [onhold][" + segmentSeatDistsDTO.getNoOfInfantSeats() + "]");
			}
		}
		
		inventoryEventPublisher.publishEvents();
		
		if (log.isDebugEnabled()) {
			log.debug(cnfOnholdSeatsSummary);
		}
	}

	/**
	 * Onhold/Sold seats cancellation.
	 * 
	 * @param segmentSeatDistDTOsForRel
	 *            Collection of SegmentSeatDistsDTOForRelease
	 * @throws ModuleException
	 *             TODO : should be able to re-factor this method along with confirmOnholdSeats method
	 */
	public void releaseInventory(Collection<SegmentSeatDistsDTOForRelease> segmentSeatDistDTOsForRel) throws ModuleException {
		StringBuffer releasedSeatsSummary = null;
		if (log.isDebugEnabled()) {
			releasedSeatsSummary = new StringBuffer();
			releasedSeatsSummary.append("\n\r--------Releasing Seats--------\n\r");
		}
		
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();
		
		List<Integer> segInventoryIds = new ArrayList<Integer>();
		for (SegmentSeatDistsDTOForRelease segmentSeatDistsDTO : segmentSeatDistDTOsForRel) {
			String logicalCabinClassCode = null;
			Integer flightId = null;
			Integer fccsInvId =null;
			String segmentCode = null;
			Integer currentFccSegId = null;
			List<Integer> interceptingSegs = null;
			List<Integer> fccsInvIds = null;
			int[] totalSoldOnholdAdultSeats = segmentSeatDistsDTO.getTotalSoldOnholdAdultSeats();
			String bookingClassType = null;

			Map<Integer, FCCSegBCInventory> gdsAllocatedExistingBCInvs = new HashMap<Integer, FCCSegBCInventory>();
			Map<Integer, FCCSegBCInventory> gdsAllocatedUpdatedBCInvs = new HashMap<Integer, FCCSegBCInventory>();

			if (StringUtil.isNullOrEmpty(segmentSeatDistsDTO.getLogicalCCCode())) {
				logicalCabinClassCode = getFlightInventoryDAO()
						.getExtendedBCInventory(segmentSeatDistsDTO.getFlightSegId(),
								segmentSeatDistsDTO.getSeatDistributionsForRelease().iterator().next().getBookingCode())
						.getFccSegBCInventory().getLogicalCCCode();
			} else {
				logicalCabinClassCode = segmentSeatDistsDTO.getLogicalCCCode();
			}
			
			fccsInvId = (Integer) getFlightInventoryDAO().getFCCSegInventoryFlightInfo(
					new Integer(segmentSeatDistsDTO.getFlightSegId()), logicalCabinClassCode)[2];
			InventoryDiffForAVS inventoryDiffForAVS = new InventoryDiffForAVS(fccsInvId);
			inventoryDiffForAVS.takeIntialInvSnapshot();

			if (totalSoldOnholdAdultSeats[0] > 0 || totalSoldOnholdAdultSeats[1] > 0) {

				Map<String, Boolean> segmentLogicalCabinClassWiseLockAquired = new HashMap<String, Boolean>();

				for (SeatDistForRelease seatDist : segmentSeatDistsDTO.getSeatDistributionsForRelease()) {
					// get bc inventory

					ExtendedFCCSegBCInventory exBCInv = getFlightInventoryDAO()
							.getExtendedBCInventory(segmentSeatDistsDTO.getFlightSegId(), seatDist.getBookingCode());
					if (exBCInv == null) {
						throw new ModuleException("airinventory.improper.data.release.fccsegbc");
					}

					FCCSegBCInventory bcInv = exBCInv.getFccSegBCInventory();

					logicalCabinClassCode = bcInv.getLogicalCCCode();
					flightId = new Integer(bcInv.getFlightId());
					segmentCode = bcInv.getSegmentCode();
					bookingClassType = seatDist.getBookingClassType();
					Set<GDSBookingClass> gdsBCs = getBookingClassDAO().getBookingClass(seatDist.getBookingCode()).getGdsBCs();
					if (bookingClassType == null) {
						bookingClassType = getBookingClassJDBCDAO().getBookingClassType(seatDist.getBookingCode());
					}
					if (gdsBCs != null && gdsBCs.size() > 0) {
						gdsAllocatedExistingBCInvs.put(bcInv.getFccsbInvId(), bcInv);
					}

					if (bookingClassType != null && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)
							&& !seatDist.isWaitListed()) {
						boolean segSoldOnholdCountOk = getFlightInventoryDAO().checkSegSoldOnholdCountValidity(
								bcInv.getfccsInvId(), seatDist.getSoldSeats(), seatDist.getOnHoldSeats(),
								segmentSeatDistsDTO.getSoldInfantSeatsForRelease(),
								segmentSeatDistsDTO.getOnholdInfantSeatsForRelease());
						if (!segSoldOnholdCountOk) {
							log.error("Invalid seat release operation. "
									+ "Requested number of onhold/sold adult or infant seats not found on the segment.\n"
									+ "[segmentInvId=" + bcInv.getfccsInvId() + ", " + "adult sold seats requested="
									+ seatDist.getSoldSeats() + ", " + "adult onhold seats requested=" + seatDist.getOnHoldSeats()
									+ ", " + "infant sold seats requested=" + segmentSeatDistsDTO.getSoldInfantSeatsForRelease()
									+ ", " + "infant onhold seats requested="
									+ segmentSeatDistsDTO.getOnholdInfantSeatsForRelease() + "]");
							if (!AppSysParamsUtil.isByPassInvChecksForSeatRelease()) {
								throw new ModuleException("airinventory.logic.bl.onhold.release.failed",
										AirinventoryCustomConstants.INV_MODULE_CODE);
							} else {
								log.warn("BY_PASS_INV: SEG_INV_CHECK: segmentInvId=" + bcInv.getfccsInvId());
							}
						}
						// Retrieve the intercepting segment inventory Ids
						interceptingSegs = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(bcInv.getfccsInvId(),
								true);
					}

					/**
					 * acquire the update lock on the segment inventories of the flight. This done to maintain the
					 * segment inventory lock first then bc inventory lock order
					 */
					if ((segmentLogicalCabinClassWiseLockAquired.get(logicalCabinClassCode) == null
							|| !segmentLogicalCabinClassWiseLockAquired.get(logicalCabinClassCode).booleanValue())
							&& getAirInventoryConfig().isApplyLockingForSeatRelease()) {
						fccsInvIds = new ArrayList<Integer>();
						fccsInvIds.add(new Integer(bcInv.getfccsInvId()));
						if (interceptingSegs != null && interceptingSegs.size() > 0) {
							fccsInvIds.addAll(interceptingSegs);
						}
						segInventoryIds.addAll(fccsInvIds);
						if (log.isDebugEnabled()) {
							log.debug("Going to acquire lock on seg/intercepting segs for seats release [fccSegInvIds="
									+ fccsInvIds + "]");
						}
						// acquiring lock with no wait
						AirInventoryDAOUtils.getInventoryResDAO().getFCCSegInventoriesForUpdate(fccsInvIds, false,
								getAirInventoryConfig().getMaxWaitForLockDuration());

						if (log.isDebugEnabled()) {
							log.debug(
									"Acquired seg/lock on intercepting segs for seats release [fccSegInvIds=" + fccsInvIds + "]");
						}
						segmentLogicalCabinClassWiseLockAquired.put(logicalCabinClassCode, true);
					}

					if (exBCInv.isStandardCode()) {
						// standard bucket onhold release

						Collection<FCCSegBCInvNesting> nestedInSoldOnholdSeatsCol = null;

						// get nested out seats
						int[] nestedOutSoldOnholdSeats = getFlightInventoryDAO()
								.getNestedOutOrBlockedSeatCounts(bcInv.getFccsbInvId().intValue());
						int effectiveSoldSeats = bcInv.getSeatsSold() - nestedOutSoldOnholdSeats[0];// non nested out
																									// sold seats
						int effectiveOnholdSeats = bcInv.getOnHoldSeats() - nestedOutSoldOnholdSeats[1];// non nested
																										// out onhold
																										// seats
						if (effectiveSoldSeats < 0 || effectiveOnholdSeats < 0) {
							// inventory inconsistent data found
							log.error("Inconsistent data found in inventory. [bcInvId=" + bcInv.getFccsbInvId()
									+ ", effectiveOnholdSeats=" + effectiveOnholdSeats + ", effectiveSoldSeats="
									+ effectiveSoldSeats + "]");
							throw new ModuleException("airinventory.logic.bl.onhold.release.data.inconsistency",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}

						// check sold/onhold release eligibility
						if (effectiveSoldSeats < seatDist.getSoldSeats() || effectiveOnholdSeats < seatDist.getOnHoldSeats()) {

							nestedInSoldOnholdSeatsCol = getFlightInventoryDAO().getNestedSeats(bcInv.getFccsbInvId().intValue(),
									true, true);

							// check the requested number of sold/onhold seats available
							int soldSeatsFound = effectiveSoldSeats;
							int onholdSeatsFound = effectiveOnholdSeats;

							if (nestedInSoldOnholdSeatsCol != null) {
								for (FCCSegBCInvNesting nestedRecord : nestedInSoldOnholdSeatsCol) {
									soldSeatsFound += nestedRecord.getSoldSeats();
									onholdSeatsFound += nestedRecord.getOnholdSeats();
									if (soldSeatsFound >= seatDist.getSoldSeats()
											&& onholdSeatsFound >= seatDist.getOnHoldSeats()) {
										break;
									}
								}
							}
							if ((soldSeatsFound < seatDist.getSoldSeats() || onholdSeatsFound < seatDist.getOnHoldSeats())
									&& !seatDist.isWaitListed()) {
								// invalid sold/onhold release request
								log.error("Invalid seat release operation. "
										+ "Requested number of sold/onhold adult seats not found on the bc inventory.\n"
										+ "[bcInvId=" + bcInv.getFccsbInvId() + ", " + "adult sold seats requested="
										+ seatDist.getSoldSeats() + ", " + "adult onhold seats requested="
										+ seatDist.getOnHoldSeats() + "]");
								throw new ModuleException("airinventory.logic.bl.onhold.release.failed",
										AirinventoryCustomConstants.INV_MODULE_CODE);
							}
						}

						// ok to proceed with sold/onhold release
						if ((seatDist.getSoldSeats() > 0 && effectiveSoldSeats > 0)
								|| (seatDist.getOnHoldSeats() > 0 && (effectiveOnholdSeats > 0 || seatDist.isWaitListed()))) {
							// release sold/onhold seats from the own bucket first
							List<String> cases = findSeatReleaseCase(exBCInv.isStandardCode(), bcInv.getPriorityFlag(),
									exBCInv.isFixedFlag(), bcInv.getStatus().equals(FCCSegBCInventory.Status.CLOSED));

							int releasedWLSeats = 0;
							int soldSeatsToRelease = 0;
							if (seatDist.getSoldSeats() > 0 && effectiveSoldSeats > 0) {
								if (!seatDist.isWaitListed()) {
									if (effectiveSoldSeats < seatDist.getSoldSeats()) {
										soldSeatsToRelease = effectiveSoldSeats;
									} else {
										soldSeatsToRelease = seatDist.getSoldSeats();
									}
								} else {
									releasedWLSeats = seatDist.getSoldSeats();
								}
							}

							int onholdSeatsToRelease = 0;
							// wait listed seats are in onhold state so , we need to check whether the releasing segment
							// is wait listed
							if (seatDist.getOnHoldSeats() > 0 && effectiveOnholdSeats > 0) {
								if (!seatDist.isWaitListed()) {
									if (effectiveOnholdSeats < seatDist.getOnHoldSeats()) {
										onholdSeatsToRelease = effectiveOnholdSeats;
									} else {
										onholdSeatsToRelease = seatDist.getOnHoldSeats();
									}
								} else {
									releasedWLSeats = seatDist.getOnHoldSeats();
								}
							}

							getFlightInventoryDAO().releaseBCInventorySeats(cases, soldSeatsToRelease, onholdSeatsToRelease,
									releasedWLSeats, exBCInv);
						}

						if ((seatDist.getSoldSeats() > 0 && effectiveSoldSeats < seatDist.getSoldSeats())
								|| (seatDist.getOnHoldSeats() > 0 && effectiveOnholdSeats < seatDist.getOnHoldSeats())) {
							// proceed with releasing nested sold/onhold seats
							if (nestedInSoldOnholdSeatsCol != null) {
								int releasedOnholdSeatsCount = effectiveOnholdSeats;
								int releasedSoldSeatsCount = effectiveSoldSeats;
								boolean soldReleaseCompleted = (seatDist.getSoldSeats() == 0
										|| effectiveSoldSeats >= seatDist.getSoldSeats());
								boolean onholdReleaseCompleted = (seatDist.getOnHoldSeats() == 0
										|| effectiveOnholdSeats >= seatDist.getOnHoldSeats());
								for (FCCSegBCInvNesting nestedInRecord : nestedInSoldOnholdSeatsCol) {
									int noOfSoldSeatsToRelease = 0;
									if (!soldReleaseCompleted && nestedInRecord.getSoldSeats() > 0) {
										if ((releasedSoldSeatsCount + nestedInRecord.getSoldSeats()) < seatDist.getSoldSeats()) {
											noOfSoldSeatsToRelease = nestedInRecord.getSoldSeats();
										} else if ((releasedSoldSeatsCount + nestedInRecord.getSoldSeats()) == seatDist
												.getSoldSeats()) {
											noOfSoldSeatsToRelease = nestedInRecord.getSoldSeats();
											soldReleaseCompleted = true;
										} else {
											noOfSoldSeatsToRelease = seatDist.getSoldSeats() - releasedSoldSeatsCount;
											soldReleaseCompleted = true;
										}
									}

									int noOfOnholdSeatsToRelease = 0;
									if (!onholdReleaseCompleted && nestedInRecord.getOnholdSeats() > 0) {
										if ((releasedOnholdSeatsCount + nestedInRecord.getOnholdSeats()) < seatDist
												.getOnHoldSeats()) {
											noOfOnholdSeatsToRelease = nestedInRecord.getOnholdSeats();
										} else if ((releasedOnholdSeatsCount + nestedInRecord.getOnholdSeats()) == seatDist
												.getOnHoldSeats()) {
											noOfOnholdSeatsToRelease = nestedInRecord.getOnholdSeats();
											onholdReleaseCompleted = true;
										} else {
											noOfOnholdSeatsToRelease = seatDist.getOnHoldSeats() - releasedOnholdSeatsCount;
											onholdReleaseCompleted = true;
										}
									}

									if (noOfSoldSeatsToRelease > 0 || noOfOnholdSeatsToRelease > 0) {
										releasedOnholdSeatsCount += noOfOnholdSeatsToRelease;
										releasedSoldSeatsCount += noOfSoldSeatsToRelease;
										// update nest record, sold -=noOfSoldSeatsToRelease,
										// onhold-=noOfOnholdSeatsToRelease
										int nestingUpdate = getFlightInventoryDAO().updateBCInvNestingInfo(
												nestedInRecord.getFccsbInvNestingId(), -noOfOnholdSeatsToRelease,
												-noOfSoldSeatsToRelease);
										if (nestingUpdate != 1) {
											log.error("Nesting inventory update failed: " + "[bcInvId="
													+ nestedInRecord.getFccsbInvNestingId() + ", " + "adult ohd to release="
													+ noOfOnholdSeatsToRelease + ", " + "adult sold to release="
													+ noOfSoldSeatsToRelease + "]");
											throw new ModuleException("airinventory.logic.bl.onhold.release.failed",
													AirinventoryCustomConstants.INV_MODULE_CODE);
										}
										// release seats from the nested bucket
										ExtendedFCCSegBCInventory exNestedBCInv = getFlightInventoryDAO()
												.getExtendedBCInventory(nestedInRecord.getFromFCCSegBCInvId().intValue());
										FCCSegBCInventory nestedBCInv = exNestedBCInv.getFccSegBCInventory();
										List<String> cases = findSeatReleaseCase(exNestedBCInv.isStandardCode(),
												nestedBCInv.getPriorityFlag(), exNestedBCInv.isFixedFlag(),
												nestedBCInv.getStatus().equals(FCCSegBCInventory.Status.CLOSED));
										getFlightInventoryDAO().releaseBCInventorySeats(cases, noOfSoldSeatsToRelease,
												noOfOnholdSeatsToRelease, 0, exNestedBCInv);
									}
									if (soldReleaseCompleted && onholdReleaseCompleted) {
										break;
									}
								}

								if (!(soldReleaseCompleted && onholdReleaseCompleted)) {
									log.error("Nesting inventory not properly released for:" + "[bcInvId=" + bcInv.getFccsbInvId()
											+ ",released OHD:" + releasedOnholdSeatsCount + ",relased SOLD: "
											+ releasedSoldSeatsCount + ", adult sold seats requested=" + seatDist.getSoldSeats()
											+ ", adult onhold seats requested=" + seatDist.getOnHoldSeats() + "]");
								}
							}
						}
					} else {
						// non standard bucket sold/onhold release
						if (((seatDist.getSoldSeats() > 0 && bcInv.getSeatsSold() < seatDist.getSoldSeats())
								|| (seatDist.getOnHoldSeats() > 0 && bcInv.getOnHoldSeats() < seatDist.getOnHoldSeats()))
								&& !seatDist.isWaitListed()) {
							// invalid sold/onhold release request
							log.error("Invalid seat release operation. "
									+ "Requested number of sold/onhold adult seats not found on the bc inventory.\n" + "[bcInvId="
									+ bcInv.getFccsbInvId() + ", " + "adult sold seats requested=" + seatDist.getSoldSeats()
									+ ", " + "adult onhold seats requested=" + seatDist.getOnHoldSeats() + "]");
							throw new ModuleException("airinventory.logic.bl.onhold.release.failed",
									AirinventoryCustomConstants.INV_MODULE_CODE);
						}

						int releasedWLSeats = 0;
						int onholdSeatsToRelease = 0;
						int soldSeatsToRelease = 0;
						if (seatDist.isWaitListed()) {
							if (seatDist.getOnHoldSeats() > 0) {
								releasedWLSeats = seatDist.getOnHoldSeats();
							}
							if (seatDist.getSoldSeats() > 0) {
								releasedWLSeats = seatDist.getSoldSeats();
							}

						} else {
							onholdSeatsToRelease = seatDist.getOnHoldSeats();
							soldSeatsToRelease = seatDist.getSoldSeats();
						}

						List<String> cases = findSeatReleaseCase(exBCInv.isStandardCode(), bcInv.getPriorityFlag(),
								exBCInv.isFixedFlag(), bcInv.getStatus().equals(FCCSegBCInventory.Status.CLOSED));
						getFlightInventoryDAO().releaseBCInventorySeats(cases, soldSeatsToRelease, onholdSeatsToRelease,
								releasedWLSeats, exBCInv);
					}
					if (gdsBCs != null && gdsBCs.size() > 0) {
						FCCSegBCInventory updatedFCCSegBCInventory = getFlightInventoryDAO()
								.getFCCSegBCInventory(bcInv.getFccsbInvId());
						gdsAllocatedUpdatedBCInvs.put(bcInv.getFccsbInvId(), updatedFCCSegBCInventory);
					}

				}

			}

			if (totalSoldOnholdAdultSeats[0] > 0 || totalSoldOnholdAdultSeats[1] > 0
					|| segmentSeatDistsDTO.getSoldInfantSeatsForRelease() > 0
					|| segmentSeatDistsDTO.getOnholdInfantSeatsForRelease() > 0) {

				if (totalSoldOnholdAdultSeats[0] == 0 && totalSoldOnholdAdultSeats[1] == 0) {
					// for infant only seat release
					if (segmentSeatDistsDTO.getLogicalCCCode() == null || segmentSeatDistsDTO.getLogicalCCCode().equals("")) {
						log.error("Infant seat release failed. Logical cabin class not specified.");
						throw new ModuleException("airinventory.logic.bl.release.infant.invalid",
								AirinventoryCustomConstants.INV_MODULE_CODE);
					}

					Object[] flightInfo = getFlightInventoryDAO().getFCCSegInventoryFlightInfo(
							new Integer(segmentSeatDistsDTO.getFlightSegId()), segmentSeatDistsDTO.getLogicalCCCode());

					logicalCabinClassCode = segmentSeatDistsDTO.getLogicalCCCode();
					flightId = (Integer) flightInfo[0];
					segmentCode = (String) flightInfo[1];
					currentFccSegId = (Integer) flightInfo[2];
					// Retrieve the intercepting segment inventory Ids
					interceptingSegs = getFlightInventoryDAO()
							.getInterceptingFCCSegInventoryIds(segmentSeatDistsDTO.getFlightSegId(), logicalCabinClassCode, true);
					fccsInvIds = new ArrayList<Integer>();
					fccsInvIds.addAll(interceptingSegs);
					fccsInvIds.add(currentFccSegId);
				}

				// TODO : should be removed and come up with a consistent way to to check the BC type
				if (bookingClassType == null) {
					bookingClassType = segmentSeatDistsDTO.getBookingClassType();
				}

				// update segment inventory
				// no segment inventory update for standby bookings. This is checked within the method
				if (bookingClassType == null || !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
					// bookingClassType null means infant seat cancellation
					if (interceptingSegs == null || interceptingSegs.isEmpty()) {
						// This logic is valid only if no intercepting inventories found
						getFlightInventoryDAO().updateFCCSegmentInventory(flightId.intValue(), logicalCabinClassCode, segmentCode,
								segmentSeatDistsDTO.getSoldInfantSeatsForRelease(),
								segmentSeatDistsDTO.getOnholdInfantSeatsForRelease(), 0);
					} else {
						int[] totalSoldOhdCountIncludingInterceptingSegments = getFlightInventoryDAO()
								.getMaximumEffectiveReservedSetsCountInInterceptingSegments(interceptingSegs);
						getFlightInventoryDAO().updateFCCSegmentInventory(flightId.intValue(), logicalCabinClassCode, segmentCode,
								segmentSeatDistsDTO.getSoldInfantSeatsForRelease(),
								segmentSeatDistsDTO.getOnholdInfantSeatsForRelease(),
								totalSoldOhdCountIncludingInterceptingSegments[0]);
					}
				}

				// update intercepting segments' seat availability
				if (fccsInvIds != null && fccsInvIds.size() > 0) {
					getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(fccsInvIds);
				}

				inventoryEventPublisher.filterAndAdd(bookingClassType, false, fccsInvId);
			}

			if (log.isDebugEnabled()) {
				releasedSeatsSummary.append("\n\rSegment Seats Distribution").append("\n\r")
						.append("Flight Seg Id		= " + segmentSeatDistsDTO.getFlightSegId() + "\n\r");
				if (segmentSeatDistsDTO.getSeatDistributionsForRelease() != null
						&& segmentSeatDistsDTO.getSeatDistributionsForRelease().size() > 0) {
					releasedSeatsSummary.append("\n\r").append("Adult Seat Distribution [bc,sold,onhold]");
					Iterator<SeatDistForRelease> seatDistsIt = segmentSeatDistsDTO.getSeatDistributionsForRelease().iterator();
					while (seatDistsIt.hasNext()) {
						SeatDistForRelease seatDistForRelease = seatDistsIt.next();
						releasedSeatsSummary.append("[" + seatDistForRelease.getBookingCode())
								.append("," + seatDistForRelease.getSoldSeats())
								.append("," + seatDistForRelease.getOnHoldSeats() + "]");
					}
				}
				releasedSeatsSummary.append("\n\r")
						.append("Infant Seats [sold,onhold][" + segmentSeatDistsDTO.getSoldInfantSeatsForRelease() + ","
								+ segmentSeatDistsDTO.getOnholdInfantSeatsForRelease() + "]");
			}

			inventoryDiffForAVS.takeFinalInvSnapshotAndPublishDiff();
		}

		// Call to Confirm Wait listed reservation if any
		if (AppSysParamsUtil.isWaitListedReservationsEnabled()) {
			FCCSegInventory segInv = null;
			Collection<FCCSegBCInventory> bcInvs = null;
			WLConfirmationRequestDTO wlDTO = null;
			List<WLConfirmationRequestDTO> wlRequests = new ArrayList<WLConfirmationRequestDTO>();
			for (Integer segInvId : segInventoryIds) {
				segInv = getFlightInventoryDAO().getFCCSegInventory(segInvId);
				// If wait listed seats are there in the inventory, call WL confirm process
				if (segInv.getWaitListedSeats() > 0) {
					bcInvs = getFlightInventoryDAO().getFCCSegBCInventories(segInvId);
					wlDTO = new WLConfirmationRequestDTO();
					wlDTO.setLogicalCabinCalss(segInv.getLogicalCCCode());
					wlDTO.setSegmentAvailabileCount(segInv.getAvailableSeats() - segInv.getFixedSeats());
					wlDTO.setSegmentAvailableInfantCount(segInv.getAvailableInfantSeats());
					wlDTO.setFlightSegId(segInv.getFlightSegId());
					wlDTO.setFlightId(segInv.getFlightId());
					wlDTO.setSegmentCode(segInv.getSegmentCode());
					Map<String, Integer> bcAvailableCount = new HashMap<String, Integer>();
					for (FCCSegBCInventory bcInv : bcInvs) {
						bcAvailableCount.put(bcInv.getBookingCode(), bcInv.getSeatsAvailable());
					}
					wlDTO.setSegmentBcAvailableCount(bcAvailableCount);
					wlRequests.add(wlDTO);
				}
			}
			getReservationBD().confirmWaitListedReservations(wlRequests);
		}
		
		inventoryEventPublisher.publishEvents();
		
		if (log.isDebugEnabled()) {
			log.debug(releasedSeatsSummary);// log only if successful
		}
	}

	private List<String> findSeatReleaseCase(boolean standardFlag, boolean priorityFlag, boolean fixedFlag, boolean closedFlag) {

		List<String> cases = new ArrayList<String>();
		// Standard Flag Priority Flag Fixed Flag Closed Flag Behavior when a seat released

		if (standardFlag && priorityFlag && !fixedFlag && !closedFlag) {
			// Y Y N N On the same bucket (Case1)
			cases.add("Case1");
		} else if (standardFlag && priorityFlag && !fixedFlag && closedFlag) {
			// Y Y N Y Same as previous (Case1)
			cases.add("Case1");
		} else if (standardFlag && !priorityFlag && !fixedFlag && !closedFlag) {
			// Y N N N On the same bucket (Case1)
			cases.add("Case1");
		} else if (standardFlag && !priorityFlag && !fixedFlag && closedFlag) {
			// Y N N Y On the same bucket (Case2A)
			cases.add("Case2A");
			// cases.add("Case2B");
			cases.add("Case3A");
			cases.add("Case2C");
		} else if (!standardFlag && priorityFlag && !fixedFlag && !closedFlag) {
			// N Y N N On the same bucket (Case1)
			cases.add("Case1");
		} else if (!standardFlag && priorityFlag && !fixedFlag && closedFlag) {
			// N Y N Y Same as previous(Case2A)
			// cases.add("Case2A");
			// cases.add("Case2B");
			cases.add("Case1");
		} else if (!standardFlag && !priorityFlag && !fixedFlag && !closedFlag) {
			// N N N N On the same bucket (Case2A)
			cases.add("Case1");
			// cases.add("Case2B");
		} else if (!standardFlag && !priorityFlag && !fixedFlag && closedFlag) {
			// N N N Y Same as previous (Case2A), (Case2B)
			cases.add("Case1");
			// cases.add("Case2B");
		} else if (!standardFlag && priorityFlag && fixedFlag && !closedFlag) {
			// N Y Y N On the same bucket (Case1)
			cases.add("Case1");
		} else if (!standardFlag && priorityFlag && fixedFlag && closedFlag) {
			// N Y Y Y Same as previous (Case1)
			cases.add("Case1");
		} else if (!standardFlag && !priorityFlag && fixedFlag && !closedFlag) {
			// N N Y N On the same bucket (Case2A)
			cases.add("Case1");
			// cases.add("Case2B");
		} else if (!standardFlag && !priorityFlag && fixedFlag && closedFlag) {
			// N N Y Y Same as previous (Case2A), (Case2B)
			cases.add("Case1");
			// cases.add("Case2B");
		}
		return cases;
	}

	/**
	 * Removed the synchrozied block as it does not make any sense in clustered environment - 13 Dec 2006
	 */
	public void reprotectSeatsWithoutRelease(TransferSeatDTO transferSeatDTO, String userId) throws ModuleException {
		TransferSeatDTO newTransferSeatDTO = castingUtil(transferSeatDTO);
		reprotectSeats(newTransferSeatDTO, userId);
	}

	/**
	 * Removed the synchrozied block as it does not make any sense in clustered environment - 13 Dec 2006
	 * 
	 * @param transferSeatDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, String> transferSeats(TransferSeatDTO transferSeatDTO, String userId) throws ModuleException {
		TransferSeatDTO newTransferSeatDTO = castingUtil(transferSeatDTO);
		releaseInventory(newTransferSeatDTO.getSourceFltSegSeatDists());

		if (transferSeatDTO.getConnectingSeatDTOs() == null) {
			return reprotectSeats(newTransferSeatDTO, userId);
		} else {
			return reprotectSeatsForConnections(newTransferSeatDTO, userId);
		}
	}

	@SuppressWarnings("unchecked")
	private String utilGetBookingCode(TransferSeatDTO transferSeatDTO) throws ModuleException {
		Collection<SegmentSeatDistsDTOForRelease> segmentSeatDistsForRelease = transferSeatDTO.getSourceFltSegSeatDists();
		if (segmentSeatDistsForRelease.size() == 0) {
			throw new ModuleException("airinventory.unknown.sourcedist");
		}

		Iterator<SegmentSeatDistsDTOForRelease> segmentSeatDistsDTOForReleaseIt = segmentSeatDistsForRelease.iterator();

		String bookingCode = null;
		String bookingClassType = null;
		while (segmentSeatDistsDTOForReleaseIt.hasNext()) {
			SegmentSeatDistsDTOForRelease seatDistsDTOForRelease = segmentSeatDistsDTOForReleaseIt.next();
			if (seatDistsDTOForRelease.getSeatDistributionsForRelease().size() == 0) {
				continue;
				// throw new ModuleException("airinventory.flightinventoryresbl.utilgetbookingcode");
			}

			bookingClassType = seatDistsDTOForRelease.getBookingClassType();
			bookingCode = seatDistsDTOForRelease.getBookingCode();
			if (bookingClassType == null) {
				bookingClassType = getBookingClassJDBCDAO().getBookingClassType(bookingCode);
			}

			if (bookingClassType != null) {
				if (BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
					continue;
				}
			}

			if (bookingCode != null) {
				break;// identify the first normal booking class type and return
			}
		}
		return bookingCode;
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, String> reprotectSeats(TransferSeatDTO transferSeatDTO, String userId) throws ModuleException {
		FlightInventoryDAO flightInventoryDAO = getFlightInventoryDAO();
		TransferSeatDTO transferSeatDTO2 = null;
		String bookingCode = utilGetBookingCode(transferSeatDTO);
		Map<Integer, String> newBookingClassCodes = new HashMap<Integer, String>();
		if (bookingCode == null) {
			// only standby reprotect requested; skip updating inventory
			return null;
		} else {
			String bookingClassType = getBookingClassJDBCDAO().getBookingClassType(bookingCode);

			// only standby reprotect requested; skip updating inventory
			if (BookingClassUtil.isStandbyBookingClassType(bookingClassType) && transferSeatDTO.isSkipStandbyBookings()) {
				return null;
			}
		}

		if (transferSeatDTO.getTargetFlightId() == 0 /* || transferSeatDTO.getTargetCabinClassCode() == null */
				|| transferSeatDTO.getTargetSegCode() == null) {
			transferSeatDTO2 = flightInventoryDAO.getTransferSeatInfo(transferSeatDTO.getTargetFlightSegId(), bookingCode);
			// transferSeatDTO.setLogicalCCCode(transferSeatDTO2.getLogicalCCCode());
			transferSeatDTO.setTargetFlightId(transferSeatDTO2.getTargetFlightId());
			transferSeatDTO.setTargetSegCode(transferSeatDTO2.getTargetSegCode());
			transferSeatDTO.setTargetFlightSegId(transferSeatDTO2.getTargetFlightSegId());
		}
		if (log.isDebugEnabled()) {
			log.debug("============Going to reprotect seats======");
			log.debug("---------------TransferSeatDTO------------");
			log.debug("target logical cabin class:" + transferSeatDTO.getTargetCabinClassCode());
			log.debug("target flightid:" + transferSeatDTO.getTargetFlightId());
			log.debug("target segment code:" + transferSeatDTO.getTargetSegCode());
			log.debug("target flight segment id:" + transferSeatDTO.getTargetFlightSegId());
			log.debug("-------------------------------------------");
		}

		// FIXME Add target logical cabin class
		FCCSegInventory fccTargetSegInv = flightInventoryDAO.getFCCSegInventory(transferSeatDTO.getTargetFlightId(),
				transferSeatDTO.getLogicalCCCode(), transferSeatDTO.getTargetSegCode());

		if (fccTargetSegInv == null) {
			throw new ModuleException("airinventory.flightinventoryresbl.reprotectseats");
		}

		for (Iterator<SegmentSeatDistsDTOForRelease> iterFSSDD = transferSeatDTO.getSourceFltSegSeatDists().iterator(); iterFSSDD
				.hasNext();) {
			SegmentSeatDistsDTOForRelease segmentSeatDistsDTOForRelease = iterFSSDD.next();

			Collection<SeatDistForRelease> seatDists = segmentSeatDistsDTOForRelease.getSeatDistributionsForRelease();
			Iterator<SeatDistForRelease> iterSeatDists = seatDists.iterator();
						
			InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();

			while (iterSeatDists.hasNext()) {

				SeatDistForRelease seatDistribution = iterSeatDists.next();

				/*
				 * if (log.isInfoEnabled()) log.info("reprotecting seats  \n" + "\t[flightId="+
				 * String.valueOf(flightSegSeatDistDTO
				 * .getFlightId())+",cabinClassCode="+flightSegSeatDistDTO.getCabinClassCode()+
				 * ",segment="+flightSegSeatDistDTO.getSegmentCode()+",bookingCode="+bookingCode+"]");
				 */// Check whether Target BC inv is available
				FCCSegBCInventory fccSegBCInv = flightInventoryDAO.getFCCSegBCInventory(transferSeatDTO.getTargetFlightId(),
						transferSeatDTO.getLogicalCCCode(), transferSeatDTO.getTargetSegCode(),
						seatDistribution.getBookingCode());
				InventoryDiffForAVS inventoryDiffForAVS = null;
				if (fccSegBCInv != null) {
					inventoryDiffForAVS = new InventoryDiffForAVS(fccSegBCInv.getfccsInvId());
					inventoryDiffForAVS.takeIntialInvSnapshot();
				}

				String fccSegBC = seatDistribution.getBookingCode();
				if (fccSegBCInv == null && transferSeatDTO.isCabinClassChanged()) {
					log.debug("Getting the Target BC inventory");
					FCCSegInventory fccSegInventory = flightInventoryDAO.getFCCSegInventory(
							transferSeatDTO.getTargetFlightSegId(), transferSeatDTO.getTargetCabinClassCode());
					if (fccSegInventory != null) {
						Collection<FCCSegBCInventory> fccSegBCInventories = flightInventoryDAO
								.getFCCSegBCInventories(fccSegInventory.getFccsInvId());
						// TODO change this logic
						for (FCCSegBCInventory fccSegBCInventory : fccSegBCInventories) {
							BookingClass bc = getBookingClassDAO().getBookingClass(fccSegBCInventory.getBookingCode());
							if (!bc.getBcType().equals(BookingClass.BookingClassType.OPEN_RETURN)
									&& !bc.getBcType().equals(BookingClass.BookingClassType.STANDBY) && !bc.getFixedFlag()) {
								fccSegBCInv = fccSegBCInventory;
							}
						}
					}
					if (fccSegBCInv == null) {
						log.error("FCCSegBCInventory NOT FOUND IN DB for Flight Seg Id, Cabin Class Code:-"
								+ transferSeatDTO.getTargetFlightSegId() + ", " + transferSeatDTO.getTargetCabinClassCode());
						throw new ModuleException("module.hibernate.objectnotfound");
					}
					// Getting the new booking class
					fccSegBC = fccSegBCInv.getBookingCode();
					newBookingClassCodes.put(transferSeatDTO.getTargetFlightSegId(), fccSegBCInv.getBookingCode());

				}
				if (fccSegBCInv == null) {
					log.debug("Target BC inventory is not availableTargetSegInv, Createing a new BC inventory");
					// Target BC inventory is not available
					// Create new BC inventory
					fccSegBCInv = new FCCSegBCInventory();
					fccSegBCInv.setBookingCode(seatDistribution.getBookingCode());
					fccSegBCInv.setLogicalCCCode(transferSeatDTO.getLogicalCCCode());
					fccSegBCInv.setfccsInvId(fccTargetSegInv.getFccsInvId());
					fccSegBCInv.setFlightId(transferSeatDTO.getTargetFlightId());
					fccSegBCInv.setOnHoldSeats(seatDistribution.getOnHoldSeats());
					fccSegBCInv.setPriorityFlag(true);
					fccSegBCInv.setSeatsAllocated(seatDistribution.getSoldSeats() + seatDistribution.getOnHoldSeats());
					fccSegBCInv.setSeatsSold(seatDistribution.getSoldSeats());
					fccSegBCInv.setSegmentCode(transferSeatDTO.getTargetSegCode());
					fccSegBCInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
					fccSegBCInv.setStatus(FCCSegBCInventory.Status.CLOSED);
					fccSegBCInv.setSeatsAvailable(new Integer(0)); // bug fix
					fccSegBCInv.setSeatsCancelled(new Integer(0));
					fccSegBCInv.setSeatsAcquired(new Integer(0));
					flightInventoryDAO.saveFCCSegmentBCInventory(fccSegBCInv);
				} else {
					BookingClass bookingClass = getBookingClassDAO().getLightWeightBookingClass(fccSegBC);
					if (bookingClass == null) {
						log.error("BOOKING CLASS NOT FOUND IN DB :-" + fccSegBC);
						throw new ModuleException("module.hibernate.objectnotfound");
					}
					
					inventoryEventPublisher.filterAndAdd(bookingClass.getBcType(), false, fccSegBCInv.getfccsInvId());
					
					if (bookingClass.getFixedFlag()) {
						// Inventory is fixed. Increase both allocation and sold/onhold seats.
						flightInventoryDAO.reprotectBCInventorySeats(seatDistribution.getSoldSeats(),
								seatDistribution.getOnHoldSeats(), fccSegBCInv.getFccsbInvId().intValue(),
								seatDistribution.getSoldSeats() + seatDistribution.getOnHoldSeats(),
								fccSegBCInv.getSeatsAvailable());
					} else {
						// Transfer the seats
						flightInventoryDAO.reprotectBCInventorySeats(seatDistribution.getSoldSeats(),
								seatDistribution.getOnHoldSeats(), fccSegBCInv.getFccsbInvId().intValue(),
								(fccSegBCInv.getSeatsAvailable() < (seatDistribution.getSoldSeats()
										+ seatDistribution.getOnHoldSeats())
												? (seatDistribution.getSoldSeats() + seatDistribution.getOnHoldSeats()
														- fccSegBCInv.getSeatsAvailable())
												: 0),
								fccSegBCInv.getSeatsAvailable());
					}
				}
				if (inventoryDiffForAVS != null) {
					inventoryDiffForAVS.takeFinalInvSnapshotAndPublishDiff();
				}
			}
	
			inventoryEventPublisher.publishEvents();

			// skip updating seg inventory for standby bookings
			String bookingClassType = segmentSeatDistsDTOForRelease.getBookingClassType();
			if (bookingClassType == null) {
				bookingClassType = getBookingClassJDBCDAO().getBookingClassType(segmentSeatDistsDTOForRelease.getBookingCode());
			}

			if (bookingClassType != null) {
				if (BookingClassUtil.isStandbyBookingClassType(bookingClassType)) {
					continue;
				}
			}

			// Update segment inventory for infants
			flightInventoryDAO.reprotectSegInventoryInfantSeats(fccTargetSegInv.getFccsInvId(),
					segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease(),
					segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease(),
					(fccTargetSegInv.getAvailableInfantSeats() < (segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease()
							+ segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease())
									? (segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease()
											+ segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease()
											- fccTargetSegInv.getAvailableInfantSeats())
									: 0));

			log.debug("finished updating segment inventory for infants");

			// Update availability on the segment -- should be target flights.
			flightInventoryDAO.updateFCCSegmentInventory(transferSeatDTO.getTargetFlightId(), transferSeatDTO.getLogicalCCCode(),
					transferSeatDTO.getTargetSegCode(), userId);
			log.debug("finished updating availability on the segment");
			// Update availability on overlapping segments
			List<Integer> overlappingSegs = flightInventoryDAO.getInterceptingFCCSegmentInventoryIds(
					transferSeatDTO.getTargetFlightId(), transferSeatDTO.getTargetCabinClassCode(),
					transferSeatDTO.getTargetSegCode(), true);
			// FIXME add target logical cabin class
			overlappingSegs.add(new Integer(flightInventoryDAO.getFCCSegInventory(transferSeatDTO.getTargetFlightId(),
					transferSeatDTO.getLogicalCCCode(), transferSeatDTO.getTargetSegCode()).getFccsInvId()));
			flightInventoryDAO.updateFCCInterceptingSegmentInvAndAVS(overlappingSegs);
			log.debug("finished updating availability on overlapping segments");

		}
		return newBookingClassCodes;
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, String> reprotectSeatsForConnections(TransferSeatDTO transferSeatDTO, String userId)
			throws ModuleException {
		FlightInventoryDAO flightInventoryDAO = getFlightInventoryDAO();
		String bookingCode = utilGetBookingCode(transferSeatDTO);
		Map<Integer, String> newBookingClassCodes = new HashMap<Integer, String>();
		if (bookingCode == null) {
			// only standby reprotect requested; skip updating inventory
			return null;
		} else {
			String bookingClassType = getBookingClassJDBCDAO().getBookingClassType(bookingCode);

			// only standby reprotect requested; skip updating inventory
			if (BookingClassUtil.isStandbyBookingClassType(bookingClassType) && transferSeatDTO.isSkipStandbyBookings()) {
				return null;
			}
		}
		
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();

		for (TransferSeatDTO connectingTransferSeatDTO : transferSeatDTO.getConnectingSeatDTOs()) {

			if (log.isDebugEnabled()) {
				log.debug("============Going to reprotect seats======");
				log.debug("---------------TransferSeatDTO------------");
				log.debug("target cabin class:" + transferSeatDTO.getTargetCabinClassCode());
				log.debug("target flightid:" + connectingTransferSeatDTO.getTargetFlightId());
				log.debug("target segment code:" + connectingTransferSeatDTO.getTargetSegCode());
				log.debug("target flight segment id:" + connectingTransferSeatDTO.getTargetFlightSegId());
				log.debug("-------------------------------------------");
			}
			FCCSegInventory fccTargetSegInv = flightInventoryDAO.getFCCSegInventory(connectingTransferSeatDTO.getTargetFlightId(),
					transferSeatDTO.getTargetCabinClassCode(), connectingTransferSeatDTO.getTargetSegCode());

			if (fccTargetSegInv == null) {
				throw new ModuleException("airinventory.flightinventoryresbl.reprotectseats");
			}

			for (Iterator<SegmentSeatDistsDTOForRelease> iterFSSDD = transferSeatDTO.getSourceFltSegSeatDists()
					.iterator(); iterFSSDD.hasNext();) {
				SegmentSeatDistsDTOForRelease segmentSeatDistsDTOForRelease = iterFSSDD.next();

				Collection<SeatDistForRelease> seatDists = segmentSeatDistsDTOForRelease.getSeatDistributionsForRelease();
				Iterator<SeatDistForRelease> iterSeatDists = seatDists.iterator();

				while (iterSeatDists.hasNext()) {

					SeatDistForRelease seatDistribution = iterSeatDists.next();

					/*
					 * if (log.isInfoEnabled()) log.info("reprotecting seats  \n" + "\t[flightId="+
					 * String.valueOf(flightSegSeatDistDTO .getFlightId())+",cabinClassCode="
					 * +flightSegSeatDistDTO.getCabinClassCode()+ ",segment="+flightSegSeatDistDTO
					 * .getSegmentCode()+",bookingCode="+bookingCode+"]");
					 */// Check whether Target BC inv is available
					FCCSegBCInventory fccSegBCInv = flightInventoryDAO.getFCCSegBCInventory(
							connectingTransferSeatDTO.getTargetFlightId(), transferSeatDTO.getTargetCabinClassCode(),
							connectingTransferSeatDTO.getTargetSegCode(), seatDistribution.getBookingCode());
					InventoryDiffForAVS inventoryDiffForAVS = null;
					if (fccSegBCInv != null) {
						inventoryDiffForAVS = new InventoryDiffForAVS(fccSegBCInv.getfccsInvId());
						inventoryDiffForAVS.takeIntialInvSnapshot();
					}
					String fccSegBC = seatDistribution.getBookingCode();
					if (fccSegBCInv == null && transferSeatDTO.isCabinClassChanged()) {
						log.debug("Getting the Target BC inventory");
						FCCSegInventory fccSegInventory = flightInventoryDAO.getFCCSegInventory(
								connectingTransferSeatDTO.getTargetFlightId(), transferSeatDTO.getTargetCabinClassCode());
						if (fccSegInventory != null) {
							Collection<FCCSegBCInventory> fccSegBCInventories = flightInventoryDAO
									.getFCCSegBCInventories(fccSegInventory.getFccsInvId());
							// TODO change this logic
							for (FCCSegBCInventory fccSegBCInventory : fccSegBCInventories) {
								fccSegBCInv = fccSegBCInventory;
							}
						}
						if (fccSegBCInv == null) {
							log.error("FCCSegBCInventory NOT FOUND IN DB for Flight Seg Id, Cabin Class Code:-"
									+ connectingTransferSeatDTO.getTargetFlightSegId() + ", "
									+ transferSeatDTO.getCabinClassCode());
							throw new ModuleException("module.hibernate.objectnotfound");
						}
						// Getting the new booking class
						fccSegBC = fccSegBCInv.getBookingCode();
						newBookingClassCodes.put(transferSeatDTO.getTargetFlightSegId(), fccSegBCInv.getBookingCode());

					}
					if (fccSegBCInv == null) {
						log.debug("Target BC inventory is not availableTargetSegInv, Createing a new BC inventory");
						// Target BC inventory is not available
						// Create new BC inventory
						fccSegBCInv = new FCCSegBCInventory();
						fccSegBCInv.setBookingCode(seatDistribution.getBookingCode());
						fccSegBCInv.setLogicalCCCode(transferSeatDTO.getLogicalCCCode());
						fccSegBCInv.setfccsInvId(fccTargetSegInv.getFccsInvId());
						fccSegBCInv.setFlightId(connectingTransferSeatDTO.getTargetFlightId());
						fccSegBCInv.setOnHoldSeats(seatDistribution.getOnHoldSeats());
						fccSegBCInv.setPriorityFlag(true);
						fccSegBCInv.setSeatsAllocated(seatDistribution.getSoldSeats() + seatDistribution.getOnHoldSeats());
						fccSegBCInv.setSeatsSold(seatDistribution.getSoldSeats());
						fccSegBCInv.setSegmentCode(connectingTransferSeatDTO.getTargetSegCode());
						fccSegBCInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
						fccSegBCInv.setStatus(FCCSegBCInventory.Status.CLOSED);
						fccSegBCInv.setSeatsAvailable(new Integer(0)); // bug
																		// fix
						fccSegBCInv.setSeatsCancelled(new Integer(0));
						fccSegBCInv.setSeatsAcquired(new Integer(0));
						flightInventoryDAO.saveFCCSegmentBCInventory(fccSegBCInv);
					} else {
						BookingClass bookingClass = getBookingClassDAO().getLightWeightBookingClass(fccSegBC);
						if (bookingClass == null) {
							log.error("BOOKING CLASS NOT FOUND IN DB :-" + fccSegBC);
							throw new ModuleException("module.hibernate.objectnotfound");
						}
						if (bookingClass.getFixedFlag()) {
							// Inventory is fixed. Increase both allocation and
							// sold/onhold seats.
							flightInventoryDAO.reprotectBCInventorySeats(seatDistribution.getSoldSeats(),
									seatDistribution.getOnHoldSeats(), fccSegBCInv.getFccsbInvId().intValue(),
									seatDistribution.getSoldSeats() + seatDistribution.getOnHoldSeats(), 0);
						} else {
							// Transfer the seats
							flightInventoryDAO.reprotectBCInventorySeats(seatDistribution.getSoldSeats(),
									seatDistribution.getOnHoldSeats(), fccSegBCInv.getFccsbInvId().intValue(),
									(fccSegBCInv.getSeatsAvailable() < (seatDistribution.getSoldSeats()
											+ seatDistribution.getOnHoldSeats())
													? (seatDistribution.getSoldSeats() + seatDistribution.getOnHoldSeats()
															- fccSegBCInv.getSeatsAvailable())
													: 0),
									fccSegBCInv.getSeatsAvailable());
						}
						
						inventoryEventPublisher.filterAndAdd(bookingClass.getBcType(), false, fccSegBCInv.getfccsInvId());
						
					}
					if (inventoryDiffForAVS != null) {
						inventoryDiffForAVS.takeFinalInvSnapshotAndPublishDiff();
					}
				}

				// skip updating seg inventory for standby bookings
				String bookingClassType = segmentSeatDistsDTOForRelease.getBookingClassType();
				if (bookingClassType == null) {
					bookingClassType = getBookingClassJDBCDAO()
							.getBookingClassType(segmentSeatDistsDTOForRelease.getBookingCode());
				}

				if (bookingClassType != null) {
					if (BookingClassUtil.isStandbyBookingClassType(bookingClassType)) {
						continue;
					}
				}

				// Update segment inventory for infants

				flightInventoryDAO.reprotectSegInventoryInfantSeats(fccTargetSegInv.getFccsInvId(),
						segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease(),
						segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease(),
						(fccTargetSegInv.getAvailableInfantSeats() < (segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease()
								+ segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease())
										? (segmentSeatDistsDTOForRelease.getSoldInfantSeatsForRelease()
												+ segmentSeatDistsDTOForRelease.getOnholdInfantSeatsForRelease()
												- fccTargetSegInv.getAvailableInfantSeats())
										: 0));

				log.debug("finished updating segment inventory for infants");

				// Update availability on the segment -- should be target
				// flights.
				flightInventoryDAO.updateFCCSegmentInventory(connectingTransferSeatDTO.getTargetFlightId(),
						transferSeatDTO.getTargetCabinClassCode(), connectingTransferSeatDTO.getTargetSegCode(), userId);
				log.debug("finished updating availability on the segment");
				// Update availability on overlapping segments
				List<Integer> overlappingSegs = flightInventoryDAO.getInterceptingFCCSegmentInventoryIds(
						connectingTransferSeatDTO.getTargetFlightId(), transferSeatDTO.getTargetCabinClassCode(),
						connectingTransferSeatDTO.getTargetSegCode(), true);
				overlappingSegs.add(new Integer(flightInventoryDAO
						.getFCCSegInventory(connectingTransferSeatDTO.getTargetFlightId(),
								transferSeatDTO.getTargetCabinClassCode(), connectingTransferSeatDTO.getTargetSegCode())
						.getFccsInvId()));
				flightInventoryDAO.updateFCCInterceptingSegmentInvAndAVS(overlappingSegs);
				log.debug("finished updating availability on overlapping segments");
			}

		}
		return newBookingClassCodes;
	}

	public void confirmWaitListedSeats(WLConfirmationRequestDTO wlRequest, boolean isConfirmedReservation)
			throws ModuleException {

		HashMap<Integer, Collection<FCCSegBCInventoryTO>> bucketClosureInfo = new HashMap<Integer, Collection<FCCSegBCInventoryTO>>();
		List<Integer> interceptingSegs = null;

		String bookingClassType = wlRequest.getBookingClassType();

		if (bookingClassType == null) {
			bookingClassType = getBookingClassJDBCDAO().getBookingClassType(wlRequest.getBookingClass());
		}

		Integer fccsInvId = getFlightInventoryDAO().getFccaId(wlRequest.getFlightId(), wlRequest.getLogicalCabinCalss(),
				wlRequest.getSegmentCode());

		InventoryDiffForAVS inventoryDiffForAVS = new InventoryDiffForAVS(fccsInvId);
		inventoryDiffForAVS.takeIntialInvSnapshot();
		
		InventoryEventPublisher inventoryEventPublisher = new InventoryEventPublisher();
		
		if (bookingClassType != null && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {

			interceptingSegs = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(fccsInvId, true);

			String logicalCabinClass = wlRequest.getLogicalCabinCalss();
			// Segment seat availability check
			List<Integer> availableSeats = getSeatAvailabilityDAO()
					.getAvailableNonFixedAndFixedSeatsCounts(fccsInvId, wlRequest.getBookingClass()).get(logicalCabinClass);

			if ((wlRequest.getTotalAdultCount() > 0
					&& wlRequest.getTotalAdultCount() > (availableSeats.get(0) - availableSeats.get(1)))

					|| (wlRequest.getTotalInfantCount() > 0 && wlRequest.getTotalInfantCount() > availableSeats.get(2))) {
				addErrorLog("Wait listing confirmation failed due to insufficient seats in " + fccsInvId + ". Requested adults - "
						+ wlRequest.getTotalAdultCount() + ", infant seats - " + wlRequest.getTotalInfantCount()
						+ ". Available non fixed seats " + (availableSeats.get(0) - availableSeats.get(1))
						+ ", Available fixed seats " + availableSeats.get(1) + ", Available infant seats "
						+ availableSeats.get(2));
				return;
			}

			int totalOnHoldSeats = 0;
			int totalSoldSeats = 0;
			int totalInfantOnHoldSeats = 0;
			int totalInfantSoldSeats = 0;
			if (isConfirmedReservation) {
				totalSoldSeats = wlRequest.getTotalAdultCount();
				totalInfantSoldSeats = wlRequest.getTotalInfantCount();
			} else {
				totalOnHoldSeats = wlRequest.getTotalAdultCount();
				totalInfantOnHoldSeats = wlRequest.getTotalInfantCount();
			}
			// update cumulative sums, infant sold/onhold seats in segment inventory
			int segUpdateCount = getFlightInventoryDAO().updateFccSegInventorySoldOnholdSeats(wlRequest.getFlightId(),
					wlRequest.getLogicalCabinCalss(), wlRequest.getSegmentCode(), totalSoldSeats, totalOnHoldSeats,
					totalInfantSoldSeats, totalInfantOnHoldSeats, false, wlRequest.getTotalAdultCount() > 0 ? true : false,
					wlRequest.getTotalInfantCount() > 0 ? true : false, wlRequest.getTotalAdultCount());

			if (segUpdateCount == 0) {
				addErrorLog("Wait listing confirmation failed due to insufficient seats in " + fccsInvId + ". Requested adults - "
						+ wlRequest.getTotalAdultCount() + ", infant seats - " + wlRequest.getTotalInfantCount()
						+ ". Available non fixed seats " + (availableSeats.get(0) - availableSeats.get(1))
						+ ", Available fixed seats " + availableSeats.get(1) + ", Available infant seats "
						+ availableSeats.get(2));
				return;
			}

		}

		String bookingCode = wlRequest.getBookingClass();
		int noOfSoldSeatsFromBC = 0;
		int noOfOnHoldSeatsFromBC = 0;
		if (isConfirmedReservation) {
			noOfSoldSeatsFromBC = wlRequest.getTotalAdultCount();
		} else {
			noOfOnHoldSeatsFromBC = wlRequest.getTotalAdultCount();
		}
		// getting bc inventory id
		FCCSegBCInventory bcInv = getFlightInventoryDAO().getFCCSegBCInventory(wlRequest.getFlightId(),
				wlRequest.getLogicalCabinCalss(), wlRequest.getSegmentCode(), bookingCode);

		BookingClass openReturnBookingClass = getBookingClassDAO().getLightWeightBookingClass(bookingCode);
		boolean isOpenReturnBookingClass = BookingClass.BookingClassType.OPEN_RETURN
				.equalsIgnoreCase(openReturnBookingClass.getBcType());

		// update bc inventory
		UpdateFccSegBCInvResponse bcUpdateResponse = null;

		bcUpdateResponse = getFlightInventoryDAO().updateFccSegBCInventory(bcInv.getFccsbInvId(), noOfOnHoldSeatsFromBC,
				noOfSoldSeatsFromBC, isOpenReturnBookingClass ? false : true, null, isOpenReturnBookingClass ? true : false,
				BookingClassUtil.byPassSegInvUpdateForOverbook(bookingClassType), wlRequest.getTotalAdultCount(), null);

		if (bcUpdateResponse.getUpdateCount() == 0) {
			addErrorLog("Wait Listing confirmation failed due to insufficient seats in " + bcInv.getFccsbInvId()
					+ " bucket. Requested adults - " + wlRequest.getTotalAdultCount());
			return;

		}

		if (bcUpdateResponse.getClosedFccsbInvs() != null && bcUpdateResponse.getClosedFccsbInvs().size() > 0) {
			bucketClosureInfo.put(bcInv.getFccsbInvId(), bcUpdateResponse.getClosedFccsbInvs());
		}

		// update segment availability - done only for non-standby bookings
		if (bookingClassType != null && !BookingClassUtil.byPassSegInvUpdate(bookingClassType)) {
			if (interceptingSegs != null && interceptingSegs.size() > 0) {
				interceptingSegs.add(fccsInvId);
				getFlightInventoryDAO().updateFCCInterceptingSegmentInventories(interceptingSegs);
			}
		}
		
		inventoryEventPublisher.filterAndAdd(bookingClassType, false, fccsInvId);

		inventoryDiffForAVS.takeFinalInvSnapshotAndPublishDiff();

	}

	@SuppressWarnings("unchecked")
	private TransferSeatDTO castingUtil(TransferSeatDTO transferSeatDTO) {
		Collection<SegmentSeatDistsDTOForRelease> appropriateSourceSeatDists = new ArrayList<SegmentSeatDistsDTOForRelease>();
		Collection<TransferSeatList> sourceSeatDists = transferSeatDTO.getSourceFltSegSeatDists();
		Iterator<TransferSeatList> iterSourceSeatDists = sourceSeatDists.iterator();
		while (iterSourceSeatDists.hasNext()) {
			Collection<SeatDistForRelease> seatDistsForRelease = new ArrayList<SeatDistForRelease>();
			TransferSeatList transferSeatList = iterSourceSeatDists.next();
			HashMap<String, TransferSeatBcCount> hashMap = transferSeatList.getBcCount();
			Iterator<String> hs = hashMap.keySet().iterator();
			while (hs.hasNext()) {
				String bookingCode = hs.next();
				TransferSeatBcCount seatBcCount = hashMap.get(bookingCode);
				SeatDistForRelease distForRelease = new SeatDistForRelease();
				distForRelease.setBookingCode(bookingCode);
				distForRelease.setOnHoldSeats(seatBcCount.getOnHoldCount());
				distForRelease.setSoldSeats(seatBcCount.getSoldCount());
				seatDistsForRelease.add(distForRelease);
			}
			SegmentSeatDistsDTOForRelease distsDTOForRelease = new SegmentSeatDistsDTOForRelease();
			distsDTOForRelease.setFlightSegId(transferSeatList.getSourceSegmentId());
			distsDTOForRelease.setOnholdInfantSeatsForRelease(transferSeatList.getOnHoldInfantCount());
			distsDTOForRelease.setSoldInfantSeatsForRelease(transferSeatList.getConfirmedInfantCount());
			distsDTOForRelease.setSeatDistributionsForRelease(seatDistsForRelease);
			appropriateSourceSeatDists.add(distsDTOForRelease);
		}
		transferSeatDTO.getSourceFltSegSeatDists().clear();
		transferSeatDTO.setSourceFltSegSeatDists(appropriateSourceSeatDists);
		return transferSeatDTO;
	}

	private AirInventoryConfig getAirInventoryConfig() {
		return (AirInventoryConfig) AirInventoryUtil.getInstance().getModuleConfig();
	}

	private FlightInventoryDAO getFlightInventoryDAO() {
		return (FlightInventoryDAO) AirInventoryUtil.getInstance().getLocalBean("FlightInventoryDAOImplProxy");
	}

	private BookingClassDAO getBookingClassDAO() {
		return (BookingClassDAO) AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
	}

	public BookingClassJDBCDAO getBookingClassJDBCDAO() {
		return (BookingClassJDBCDAO) AirInventoryUtil.getInstance().getLocalBean("bookingClassJdbcDAO");
	}

	private SeatAvailabilityDAOJDBC getSeatAvailabilityDAO() {
		return (SeatAvailabilityDAOJDBC) AirInventoryUtil.getInstance().getLocalBean("seatAvailabilityDAOJDBC");
	}

	private FlightInventoryResBD getFlightInventoryResBD() {
		return AirInventoryModuleUtils.getFlightInventoryResBD();
	}

	private ReservationBD getReservationBD() {
		return AirInventoryModuleUtils.getReservationBD();
	}

	private boolean checkConfigForTrackingBlockedSeats() {
		return AirinventoryUtils.getAirInventoryConfig().getTrackBlockSeats().equalsIgnoreCase("true") ? true : false;
	}

	private void addDebugLog(String logMessage) {
		if (log.isDebugEnabled()) {
			log.debug(logMessage);
		}
	}

	private void addErrorLog(String logMessage) {
		log.error(logMessage);
	}
}
