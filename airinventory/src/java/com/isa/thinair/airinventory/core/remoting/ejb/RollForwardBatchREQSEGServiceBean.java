/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airinventory.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaSearchTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.core.audit.AuditAirinventory;
import com.isa.thinair.airinventory.core.bl.RollForwardBatchBL;
import com.isa.thinair.airinventory.core.persistence.dao.RollForwardBatchREQSEGDAO;
import com.isa.thinair.airinventory.core.service.bd.RollForwardBatchREQSEGServiceDelegateImpl;
import com.isa.thinair.airinventory.core.service.bd.RollForwardBatchREQSEGServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Raghu
 */
@Stateless
@RemoteBinding(jndiBinding = "RollForwardBatchREQSEGService.remote")
@LocalBinding(jndiBinding = "RollForwardBatchREQSEGService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class RollForwardBatchREQSEGServiceBean extends PlatformBaseSessionBean implements
		RollForwardBatchREQSEGServiceDelegateImpl, RollForwardBatchREQSEGServiceLocalDelegateImpl {
	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Field sessionContext
	 */
	@Resource
	protected SessionContext sessionContext;

	/**
	 * Return the user principal
	 *
	 * @return
	 * @throws ModuleException
	 */
	public UserPrincipal getUserPrincipal() throws ModuleException {
		UserPrincipal principal = null;
		try {
			principal = (UserPrincipal) this.sessionContext.getCallerPrincipal();
		} catch (Exception ex) {
			log.error("Retrieving Caller Principal failed", ex);
		}
		return principal;
	}

	/**
	 * Sets tracking details.
	 *
	 * @param tracking
	 * @return
	 * @throws ModuleException
	 */
	public Tracking setUserDetails(Tracking tracking) throws ModuleException {
		UserPrincipal principal = getUserPrincipal();

		if (principal != null) {
			if (tracking.getVersion() < 0) { // save operation
				tracking.setCreatedBy(principal.getUserId() == null ? "" : principal.getUserId());
				tracking.setCreatedDate(new Date());

				if (tracking.getModifiedBy() == null) {
					tracking.setModifiedBy(principal.getUserId() == null ? "" : principal.getUserId());
					tracking.setModifiedDate(new Date());
				}
			} else { // update operation
				tracking.setModifiedBy(principal.getUserId() == null ? "" : principal.getUserId());
				tracking.setModifiedDate(new Date());
			}
		}

		return tracking;
	}

	@Override
	public void saveRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException {
		try {
			setUserDetails(rollForwardBatchREQSEG);
			lookupRollForwardBatchREQSEGDAO().saveRollForwardBatchREQSEG(rollForwardBatchREQSEG);

		}

		catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw e;
		} catch (CommonsDataAccessException cde) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cde.getExceptionCode(), "rollforward.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("rollforward.technical.error", "rollforward.desc");
		}

	}

	@Override
	public void removeRollForwardBatchREQSEG(int key) throws ModuleException {
		try {
			lookupRollForwardBatchREQSEGDAO().removeRollForwardBatchREQSEG(key);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		}

	}

	@Override
	public List<RollForwardBatchREQSEG> getRollForwardBatchREQSEGs() throws ModuleException {
		try {
			return lookupRollForwardBatchREQSEGDAO().getRollForwardBatchREQSEGs();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		}

	}

	@Override
	public RollForwardBatchREQSEG getRollForwardBatchREQSEG(int rollForwardBatchREQSEGId) throws ModuleException {
		try {
			return lookupRollForwardBatchREQSEGDAO().getRollForwardBatchREQSEG(rollForwardBatchREQSEGId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		}

	}

	private RollForwardBatchREQSEGDAO lookupRollForwardBatchREQSEGDAO() {

		LookupService lookupService = LookupServiceFactory.getInstance();
		return (RollForwardBatchREQSEGDAO) lookupService
				.getBean("isa:base://modules/airinventory?id=RollForwardBatchREQSEGDAOProxy");

	}

	@Override
	public void removeRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException {
		lookupRollForwardBatchREQSEGDAO().removeRollForwardBatchREQSEG(rollForwardBatchREQSEG);

	}

	@Override
	public void saveAllRollForwardBatchREQSEG(Set<RollForwardBatchREQSEG> rollForwardBatchREQSEGSet) throws ModuleException {
		try {
			for (RollForwardBatchREQSEG rollForwardBatchREQSEG : rollForwardBatchREQSEGSet) {
				setUserDetails(rollForwardBatchREQSEG);
			}

			lookupRollForwardBatchREQSEGDAO().saveAllRollForwardBatchREQSEG(rollForwardBatchREQSEGSet);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airinventory.desc");
		} catch (Exception e) {
			throw new ModuleException(e, e.getMessage(), "airinventory.desc");
		}

	}

	@Override
	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByBatchId(Integer batchId) {

		return lookupRollForwardBatchREQSEGDAO().getRollForwardBatchREQSEGByBatchId(batchId);
	}

	@Override
	public Page<SearchRollForwardCriteriaTO> search(SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO,
			int start, int size) {

		Page<RollForwardBatchREQSEG> resultPage = lookupRollForwardBatchREQSEGDAO().search(searchRollForwardCriteriaSearchTO,
				start, size);

		Collection<SearchRollForwardCriteriaTO> transformedDTOs = new HashSet<SearchRollForwardCriteriaTO>();

		for (RollForwardBatchREQSEG criteria : resultPage.getPageData()) {
			transformedDTOs.add(rollForwardBatchTosearchRollForwardCriteriaTO(criteria));
		}

		Page<SearchRollForwardCriteriaTO> resultTOPage = new Page<SearchRollForwardCriteriaTO>(resultPage.getTotalNoOfRecords(),
				resultPage.getStartPosition(), resultPage.getEndPosition(), transformedDTOs);

		return resultTOPage;

	}

	private SearchRollForwardCriteriaTO rollForwardBatchTosearchRollForwardCriteriaTO(RollForwardBatchREQSEG criteria) {

		SearchRollForwardCriteriaTO searchRollForwardCriteriaTO = new SearchRollForwardCriteriaTO();

		if (null != criteria) {

			searchRollForwardCriteriaTO.setBatchSegId(criteria.getBatchSegId());
			searchRollForwardCriteriaTO.setBatchId(criteria.getRollForwardBatchREQ().getBatchId());
			searchRollForwardCriteriaTO.setFlightNo(criteria.getFlightNumber());
			searchRollForwardCriteriaTO.setFromDate(criteria.getFromDate());
			searchRollForwardCriteriaTO.setToDate(criteria.getToDate());
			searchRollForwardCriteriaTO.setAuditLog(criteria.getDetailAudit());
			searchRollForwardCriteriaTO.setCreatedBy(criteria.getCreatedBy());
			searchRollForwardCriteriaTO.setCreatedDate(criteria.getCreatedDate());
			searchRollForwardCriteriaTO.setStatus(criteria.getStatus());
		}

		return searchRollForwardCriteriaTO;
	}

	@Override
	public void updateRollForwardBatchREQSEG(RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException {

		lookupRollForwardBatchREQSEGDAO().updateRollForwardBatchREQSEG(rollForwardBatchREQSEG);

	}

	@Override
	public void updateRollForwardBatchREQSEGStatus(Integer batchSegId, String flightNumber, Date fromDate, Date toDate,
			String status, String detailAudit) throws ModuleException {

		try {

			lookupRollForwardBatchREQSEGDAO().updateRollForwardBatchREQSEGStatus(batchSegId, status, detailAudit);
		} catch (ModuleException moduleException) {
			this.sessionContext.setRollbackOnly();
			throw moduleException;
		}
		// Auditing the status update
		try {
			UserPrincipal principal = getUserPrincipal();
			AuditAirinventory.updateRollForwardBatchREQSEQStatus(principal.getUserId(), batchSegId, flightNumber, fromDate,
					toDate, status);
		} catch (Exception exception) {
			log.error(exception);
		}

	}

	@Override
	public Collection<RollForwardBatchREQSEG> getRollForwardBatchREQSEGByDateAndFlights(String strFromDate, String strToDate,
			String flightNumber) {

		return lookupRollForwardBatchREQSEGDAO().getRollForwardBatchREQSEGByDateAndFlights(strFromDate, strToDate, flightNumber);

	}

	/**
	 * 
	 * This method has the logic to invoke roll forward and change the status to done for the batches
	 * 
	 * @param invRollForwardCriteriaDTO
	 * @param rollForwardBatchREQSEG
	 * @throws ModuleException
	 */
	@Override
	public void rollForwardBatchSegment(InvRollForwardCriteriaDTO invRollForwardCriteriaDTO,
			RollForwardBatchREQSEG rollForwardBatchREQSEG) throws ModuleException {

		try {
			RollForwardBatchBL.rollForwardBatchSegment(invRollForwardCriteriaDTO, rollForwardBatchREQSEG);
		} catch (Exception exception) {
			this.sessionContext.setRollbackOnly();
			if (exception instanceof ModuleException) {
				throw (ModuleException) exception;
			} else {
				throw new ModuleException(exception, "module.ejb.exception", AirinventoryCustomConstants.INV_MODULE_CODE);
			}
		}
		try {
			UserPrincipal principal = getUserPrincipal();
			AuditAirinventory.updateRollForwardBatchREQSEQStatus(principal.getUserId(), rollForwardBatchREQSEG.getBatchSegId(),
					rollForwardBatchREQSEG.getFlightNumber(), rollForwardBatchREQSEG.getFromDate(),
					rollForwardBatchREQSEG.getToDate(), rollForwardBatchREQSEG.getStatus());
		} catch (Exception exception) {
			log.error("RollForwardBatchSegment Auditing status update failed :: "+exception);
		}

	}
}
