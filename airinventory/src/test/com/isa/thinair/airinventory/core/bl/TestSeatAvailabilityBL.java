package com.isa.thinair.airinventory.core.bl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airschedules.api.utils.AvailabilitySearchTypeEnum;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestSeatAvailabilityBL extends PlatformTestCase {
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void TestSingleFlightOnewayAvailabilitySearch() throws ModuleException{
		Collection outboundSingleFlights = new ArrayList();
		
		AvailableFlightSegment availableFlightSegment = new AvailableFlightSegment();
		FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
		flightSegmentDTO.setFlightId(new Integer(100));
		flightSegmentDTO.setFromAirport("CMB");
		flightSegmentDTO.setToAirport("SHJ");
		flightSegmentDTO.setSegmentCode("CMB/SHJ");
		GregorianCalendar cal = new GregorianCalendar(2006, Calendar.NOVEMBER, 11, 8, 0);
		flightSegmentDTO.setDepartureDateTime(cal.getTime());
		availableFlightSegment.setFlightSegmentDTO(flightSegmentDTO);
		
		outboundSingleFlights.add(availableFlightSegment);
		
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(5);
		availableFlightSearchDTO.setCabinClassCode("I");
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		
		AvailableFlightDTO availableFlightDTO = new AvailableFlightDTO();
		availableFlightDTO.setOutboundSingleFlights(outboundSingleFlights);
		getSeatAvailabilityBL().searchSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
				
		assertTrue(availableFlightSegment.isSeatsAvailable());
		assertEquals(500, availableFlightSegment.getFare().getFareAmount(PaxTypeTO.ADULT), 0.001);
		assertTrue(availableFlightSegment.isFixedSeatsAvailable());
		assertEquals(560, availableFlightSegment.getFixedFare().getFareAmount(PaxTypeTO.ADULT), 0.001);
	}
	
	public void TestConnectedFlightAvailabilitySearch() throws ModuleException {
		Collection outboundConnectedFlightSegments = new ArrayList();
		
		
		AvailableConnectedFlight connFlight1 = new AvailableConnectedFlight();
		
		Collection outboundConnectedFlightSegment = new ArrayList();
		connFlight1.setAvailableFlightSegments(outboundConnectedFlightSegment);
		
		AvailableFlightSegment flightSeg1 = new AvailableFlightSegment();
		FlightSegmentDTO flightSegmentDTO1 = new FlightSegmentDTO();
		flightSegmentDTO1.setFlightId(new Integer(101));
		flightSegmentDTO1.setFromAirport("CMB");
		flightSegmentDTO1.setToAirport("SHJ");
		flightSegmentDTO1.setSegmentCode("CMB/SHJ");
		GregorianCalendar cal = new GregorianCalendar(2006, Calendar.NOVEMBER, 11, 8, 0);
		flightSegmentDTO1.setDepartureDateTime(cal.getTime());
		flightSeg1.setFlightSegmentDTO(flightSegmentDTO1);
		outboundConnectedFlightSegment.add(flightSeg1);
		
		AvailableFlightSegment flightSeg2 = new AvailableFlightSegment();
		FlightSegmentDTO flightSegmentDTO2 = new FlightSegmentDTO();
		flightSegmentDTO2.setFlightId(new Integer(102));
		flightSegmentDTO2.setFromAirport("SHJ");
		flightSegmentDTO2.setToAirport("DOH");
		flightSegmentDTO2.setSegmentCode("SHJ/DOH");
		GregorianCalendar cal2 = new GregorianCalendar(2006, Calendar.NOVEMBER, 11, 10, 0);
		flightSegmentDTO2.setDepartureDateTime(cal2.getTime());
		flightSeg2.setFlightSegmentDTO(flightSegmentDTO2);
		outboundConnectedFlightSegment.add(flightSeg2);
		connFlight1.setOndCode("CMB/SHJ/DOH");
		outboundConnectedFlightSegments.add(connFlight1);
		
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(5);
		availableFlightSearchDTO.setCabinClassCode("I");
		availableFlightSearchDTO.setOndCode("CMB/SHJ/DOH");
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
		
		AvailableFlightDTO availableFlightDTO = new AvailableFlightDTO();
		availableFlightDTO.setOutboundConnectedFlights(outboundConnectedFlightSegments);
		getSeatAvailabilityBL().searchSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
		
		assertTrue(connFlight1.isSeatsAvailable());
//		assertNotNull(connFlight1.getSeatsAvailability());
		assertNotNull(connFlight1.getFare());
		assertTrue(connFlight1.isFixedSeatsAvailable());
//		assertNull(connFlight1.getFixedSeatsAvailability());
		assertNull(connFlight1.getFixedFare());
		assertNotNull(flightSeg1.getFixedSeatsAvailability());
		assertNotNull(flightSeg2.getFixedSeatsAvailability());
	}
	
	public void TestSigleReturnFlightAvailabilty() throws ModuleException{
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();
		selectedFlightDTO.setReturnFareQuote(true);
		
		AvailableFlightSegment outboundFlightSeg = new AvailableFlightSegment();
		FlightSegmentDTO outboundFlightSegDTO = new FlightSegmentDTO();
		outboundFlightSegDTO.setFlightId(new Integer(103));
		outboundFlightSegDTO.setFromAirport("SHJ");
		outboundFlightSegDTO.setToAirport("CMB");
		outboundFlightSegDTO.setSegmentCode("SHJ/CMB");
		GregorianCalendar cal = new GregorianCalendar(2006, Calendar.OCTOBER, 6, 8, 0);
		outboundFlightSegDTO.setDepartureDateTime(cal.getTime());
		GregorianCalendar cal1 = new GregorianCalendar(2006, Calendar.OCTOBER, 6, 10, 0);
		outboundFlightSegDTO.setArrivalDateTime(cal1.getTime());
		outboundFlightSeg.setFlightSegmentDTO(outboundFlightSegDTO);
		selectedFlightDTO.setOutboundFlight(outboundFlightSeg);
		
		AvailableFlightSegment inboundFlightSeg = new AvailableFlightSegment();
		inboundFlightSeg.setInboundFlightSegment(true);
		FlightSegmentDTO inboundFlightSegDTO = new FlightSegmentDTO();
		inboundFlightSegDTO.setFlightId(new Integer(104));
		inboundFlightSegDTO.setFromAirport("CMB");
		inboundFlightSegDTO.setToAirport("SHJ");
		inboundFlightSegDTO.setSegmentCode("CMB/SHJ");
		GregorianCalendar cal2 = new GregorianCalendar(2006, Calendar.OCTOBER, 8, 8, 0);
		inboundFlightSegDTO.setDepartureDateTime(cal2.getTime());
		inboundFlightSeg.setFlightSegmentDTO(inboundFlightSegDTO);
		selectedFlightDTO.setInboundFlight(inboundFlightSeg);
		
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(5);
		availableFlightSearchDTO.setCabinClassCode("I");
		availableFlightSearchDTO.setOndCode("SHJ/CMB");
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
		
		getSeatAvailabilityBL().searchSelectedFlightsSeatAvailability(selectedFlightDTO, availableFlightSearchDTO);
		
//		assertNotNull(selectedFlightDTO.getSeatsAvailability());
		assertNotNull(selectedFlightDTO.getFare());
		
		assertNotNull(selectedFlightDTO.getFixedFare());
//		assertNotNull(selectedFlightDTO.getFixedSeatsAvailability());
	}
	
	public void TestConnectedReturnFlightAvailabilty() throws ModuleException{
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();
		selectedFlightDTO.setReturnFareQuote(true);
		
		AvailableConnectedFlight outboundConnectedFlight = new AvailableConnectedFlight();
		
		Collection outboundConnectedFlightSegments = new ArrayList();
		outboundConnectedFlight.setAvailableFlightSegments(outboundConnectedFlightSegments);
		
		AvailableFlightSegment outboundConnectedFlightSegment1 = new AvailableFlightSegment();
		FlightSegmentDTO outboundConnectedFlightSegment1DTO = new FlightSegmentDTO();
		outboundConnectedFlightSegment1DTO.setFlightId(new Integer(105));
		outboundConnectedFlightSegment1DTO.setFromAirport("CMB");
		outboundConnectedFlightSegment1DTO.setToAirport("SHJ");
		outboundConnectedFlightSegment1DTO.setSegmentCode("CMB/SHJ");
		GregorianCalendar cal = new GregorianCalendar(2006, Calendar.NOVEMBER, 1, 8, 0);
		outboundConnectedFlightSegment1DTO.setDepartureDateTime(cal.getTime());
		outboundConnectedFlightSegment1.setFlightSegmentDTO(outboundConnectedFlightSegment1DTO);
		outboundConnectedFlightSegments.add(outboundConnectedFlightSegment1);
		
		AvailableFlightSegment outboundConnectedFlightSegment2 = new AvailableFlightSegment();
		FlightSegmentDTO outboundConnectedFlightSegment2DTO = new FlightSegmentDTO();
		outboundConnectedFlightSegment2DTO.setFlightId(new Integer(106));
		outboundConnectedFlightSegment2DTO.setFromAirport("SHJ");
		outboundConnectedFlightSegment2DTO.setToAirport("DOH");
		outboundConnectedFlightSegment2DTO.setSegmentCode("SHJ/DOH");
		GregorianCalendar cal2 = new GregorianCalendar(2006, Calendar.NOVEMBER, 1, 10, 0);
		outboundConnectedFlightSegment2DTO.setDepartureDateTime(cal2.getTime());
		GregorianCalendar cal2arr = new GregorianCalendar(2006, Calendar.NOVEMBER, 1, 10, 0);
		outboundConnectedFlightSegment2DTO.setArrivalDateTime(cal2arr.getTime());
		outboundConnectedFlightSegment2.setFlightSegmentDTO(outboundConnectedFlightSegment2DTO);
		outboundConnectedFlightSegments.add(outboundConnectedFlightSegment2);
		outboundConnectedFlight.setOndCode("CMB/SHJ/DOH");
		
		selectedFlightDTO.setOutboundFlight(outboundConnectedFlight);
		
		AvailableConnectedFlight inboundConnectedFlight = new AvailableConnectedFlight();
		
		Collection inboundConnectedFlightSegments = new ArrayList();
		inboundConnectedFlight.setAvailableFlightSegments(inboundConnectedFlightSegments);
		
		AvailableFlightSegment inboundConnectedFlightSegment1 = new AvailableFlightSegment();
		inboundConnectedFlightSegment1.setInboundFlightSegment(true);
		FlightSegmentDTO inboundConnectedFlightSegment1DTO = new FlightSegmentDTO();
		inboundConnectedFlightSegment1DTO.setFlightId(new Integer(107));
		inboundConnectedFlightSegment1DTO.setFromAirport("DOH");
		inboundConnectedFlightSegment1DTO.setToAirport("SHJ");
		inboundConnectedFlightSegment1DTO.setSegmentCode("DOH/SHJ");
		GregorianCalendar incal = new GregorianCalendar(2006, Calendar.NOVEMBER, 10, 8, 0);
		inboundConnectedFlightSegment1DTO.setDepartureDateTime(incal.getTime());
		inboundConnectedFlightSegment1.setFlightSegmentDTO(inboundConnectedFlightSegment1DTO);
		inboundConnectedFlightSegments.add(inboundConnectedFlightSegment1);
		
		AvailableFlightSegment inboundConnectedFlightSegment2 = new AvailableFlightSegment();
		inboundConnectedFlightSegment2.setInboundFlightSegment(true);
		FlightSegmentDTO inboundConnectedFlightSegment2DTO = new FlightSegmentDTO();
		inboundConnectedFlightSegment2DTO.setFlightId(new Integer(108));
		inboundConnectedFlightSegment2DTO.setFromAirport("SHJ");
		inboundConnectedFlightSegment2DTO.setToAirport("CMB");
		inboundConnectedFlightSegment2DTO.setSegmentCode("SHJ/CMB");
		GregorianCalendar incal2 = new GregorianCalendar(2006, Calendar.NOVEMBER, 10, 10, 0);
		inboundConnectedFlightSegment2DTO.setDepartureDateTime(incal2.getTime());
		inboundConnectedFlightSegment2.setFlightSegmentDTO(inboundConnectedFlightSegment2DTO);
		inboundConnectedFlightSegments.add(inboundConnectedFlightSegment2);
		inboundConnectedFlight.setOndCode("DOH/SHJ/CMB");
		
		selectedFlightDTO.setInboundFlight(inboundConnectedFlight);
		
		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
		availableFlightSearchDTO.setAdultCount(10);
		availableFlightSearchDTO.setCabinClassCode("I");
		availableFlightSearchDTO.setOndCode("CMB/SHJ/DOH");
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
		
		getSeatAvailabilityBL().searchSelectedFlightsSeatAvailability(selectedFlightDTO, availableFlightSearchDTO);
		
		assertTrue(selectedFlightDTO.isSeatsAvailable());
//		assertNull(selectedFlightDTO.getSeatsAvailability());
		assertNull(selectedFlightDTO.getFare());
		assertFalse(selectedFlightDTO.isFixedSeatsAvailable());
//		assertNull(selectedFlightDTO.getFixedSeatsAvailability());
		assertNull(selectedFlightDTO.getFixedFare());
		
		assertEquals(250.0,outboundConnectedFlightSegment1.getFare().getFareAmount(PaxTypeTO.ADULT),0.001);
		assertEquals(200.0,outboundConnectedFlightSegment2.getFare().getFareAmount(PaxTypeTO.ADULT),0.001);
		assertEquals(200.0,inboundConnectedFlightSegment1.getFare().getFareAmount(PaxTypeTO.ADULT),0.001);
		assertEquals(250.0,inboundConnectedFlightSegment2.getFare().getFareAmount(PaxTypeTO.ADULT),0.001);
	}
	
    public void TestSearchSingleFlights() throws ParseException, ModuleException{
        AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
        availableFlightSearchDTO.setAdultCount(1);
        availableFlightSearchDTO.setInfantCount(0);
        availableFlightSearchDTO.setAgentCode(null);
        availableFlightSearchDTO.setCabinClassCode("Y");
        availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
        availableFlightSearchDTO.setFromAirport("CMB");
        availableFlightSearchDTO.setToAirport("SHJ");
        availableFlightSearchDTO.setOndCode("CMB/SHJ");
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date depatureDate = simpleDateFormat.parse("04/10/2006");
        availableFlightSearchDTO.setDepatureDate(depatureDate);
        availableFlightSearchDTO.setDepartureVariance(1);
        
        availableFlightSearchDTO.setReturnFlag(false);
        
        ServiceResponce res = getFlightBD().searchAvailableFlights(availableFlightSearchDTO, AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_PERIOD);
        assertNotNull(res);
        AvailableFlightDTO availableFlightDTO =(AvailableFlightDTO)res.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
//        assertEquals(1, availableFlightDTO.getOutboundSingleFlights().size());
        
        AvailableFlightDTO results = getSeatAvailabilityBL().searchSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
        Iterator obSingleFlightsIt = results.getOutboundSingleFlights().iterator();
        while (obSingleFlightsIt.hasNext()){
            AvailableFlightSegment avlFltSeg = (AvailableFlightSegment)obSingleFlightsIt.next();
            System.out.println("flightId="+avlFltSeg.getFlightSegmentDTO().getFlightId() +
                    " segCode="+avlFltSeg.getFlightSegmentDTO().getSegmentCode() + 
                    " depatureDate="+avlFltSeg.getFlightSegmentDTO().getDepartureDateTime() +
                    " seatAvailability=" + avlFltSeg.isSeatsAvailable() + 
                    " fareAmount=" + avlFltSeg.getFare().getFareAmount(PaxTypeTO.ADULT) +
                    " adultChargeAmount=" + ((avlFltSeg.getTotalCharges()!=null)?Double.toString(avlFltSeg.getTotalCharges()[0]):"null"));
        }
    }
    
    /**
     * @throws ParseException
     * @throws ModuleException
     */
    public void testGetAllFares() throws ParseException, ModuleException{
        AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
        availableFlightSearchDTO.setAdultCount(1);
        availableFlightSearchDTO.setInfantCount(0);
        availableFlightSearchDTO.setAgentCode(null);
        availableFlightSearchDTO.setCabinClassCode("Y");
        availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
        availableFlightSearchDTO.setFromAirport("CMB");
        availableFlightSearchDTO.setToAirport("BEY");
        availableFlightSearchDTO.setOndCode("CMB/SHJ/BEY");
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date depatureDate = simpleDateFormat.parse("04/10/2006");
        availableFlightSearchDTO.setDepatureDate(depatureDate);
        availableFlightSearchDTO.setDepartureVariance(1);
        
        availableFlightSearchDTO.setReturnFlag(false);
        
        ServiceResponce res = getFlightBD().searchAvailableFlights(availableFlightSearchDTO, AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_PERIOD);
        assertNotNull(res);
        AvailableFlightDTO availableFlightDTO =(AvailableFlightDTO)res.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
//        assertEquals(1, availableFlightDTO.getOutboundSingleFlights().size());
        
        AvailableFlightDTO results = getSeatAvailabilityBL().searchSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
        Iterator obSingleFlightsIt = results.getOutboundSingleFlights().iterator();
        while (obSingleFlightsIt.hasNext()){
            AvailableFlightSegment avlFltSeg = (AvailableFlightSegment)obSingleFlightsIt.next();
            System.out.println("flightId="+avlFltSeg.getFlightSegmentDTO().getFlightId() +
                    " segCode="+avlFltSeg.getFlightSegmentDTO().getSegmentCode() + 
                    " depatureDate="+avlFltSeg.getFlightSegmentDTO().getDepartureDateTime() +
                    " seatAvailability=" + avlFltSeg.isSeatsAvailable() + 
                    " fareAmount=" + avlFltSeg.getFare().getFareAmount(PaxTypeTO.ADULT) +
                    " adultChargeAmount=" + ((avlFltSeg.getTotalCharges()!=null)?Double.toString(avlFltSeg.getTotalCharges()[0]):"null"));
            
            AllFaresDTO allFaresDTO = new AllFaresDTO();
            allFaresDTO.addOutboundFlightSegmentDTO(avlFltSeg.getFlightSegmentDTO());
            allFaresDTO.setSelectedSegmentId(avlFltSeg.getFlightSegmentDTO().getSegmentId());
            
            FareFilteringCriteria fareFilteringCriteria = new FareFilteringCriteria();
            fareFilteringCriteria.setCabinClassCode(availableFlightSearchDTO.getCabinClassCode());
            fareFilteringCriteria.setChannelCode(availableFlightSearchDTO.getChannelCode());
            fareFilteringCriteria.setNumberOfAdults(availableFlightSearchDTO.getAdultCount());
            fareFilteringCriteria.setReturnFlag(false);
            fareFilteringCriteria.setOutboundOndCode(availableFlightSearchDTO.getOndCode());
            fareFilteringCriteria.setInboundOndCode(null);
            
            getSeatAvailabilityBL().getAllBucketFareCombinationsForSegment(allFaresDTO, fareFilteringCriteria);
        }
        
        
    }
	
    public void TestSearchConnectedFlights() throws ParseException, ModuleException{
        AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();
        availableFlightSearchDTO.setAdultCount(1);
        availableFlightSearchDTO.setInfantCount(0);
        availableFlightSearchDTO.setAgentCode(null);
        availableFlightSearchDTO.setCabinClassCode("Y");
        availableFlightSearchDTO.setChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));
        availableFlightSearchDTO.setFromAirport("SHJ");
        availableFlightSearchDTO.setToAirport("ATZ");
        availableFlightSearchDTO.setOndCode("SHJ/BEY/ATZ");
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date depatureDate = simpleDateFormat.parse("02/10/2006");
        availableFlightSearchDTO.setDepatureDate(depatureDate);
        availableFlightSearchDTO.setDepartureVariance(10);
        
        availableFlightSearchDTO.setReturnFlag(false);
        
        ServiceResponce res = getFlightBD().searchAvailableFlights(availableFlightSearchDTO, AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_PERIOD);
        assertNotNull(res);
        AvailableFlightDTO availableFlightDTO =(AvailableFlightDTO)res.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
//        assertEquals(1, availableFlightDTO.getOutboundSingleFlights().size());
        
        AvailableFlightDTO results = getSeatAvailabilityBL().searchSeatAvailability(availableFlightDTO, availableFlightSearchDTO);
        Iterator obSingleFlightsIt = results.getOutboundConnectedFlights().iterator();
        
        while (obSingleFlightsIt.hasNext()){
            AvailableConnectedFlight avlConnFlt = (AvailableConnectedFlight)obSingleFlightsIt.next();
            Iterator singleFltIterator = avlConnFlt.getAvailableFlightSegments().iterator();
            while (singleFltIterator.hasNext()){
                AvailableFlightSegment avlFltSeg = (AvailableFlightSegment)obSingleFlightsIt.next();
                System.out.println("flightId="+avlFltSeg.getFlightSegmentDTO().getFlightId() +
                        " segCode="+avlFltSeg.getFlightSegmentDTO().getSegmentCode() + 
                        " depatureDate="+avlFltSeg.getFlightSegmentDTO().getDepartureDateTime() +
                        " seatAvailability=" + avlFltSeg.isSeatsAvailable() + 
                        " fareAmount=" + avlFltSeg.getFare().getFareAmount(PaxTypeTO.ADULT) +
                        " adultChargeAmount=" + ((avlFltSeg.getTotalCharges()!=null)?Double.toString(avlFltSeg.getTotalCharges()[0]):"null"));
            }
        }
    }
	protected void tearDown() throws Exception{
		super.tearDown();
	}
	
	private SeatAvailabilityBL getSeatAvailabilityBL(){
		return new SeatAvailabilityBL();
	}
    
    private FlightBD getFlightBD(){
        return (FlightBD)AirInventoryUtil.getInstance().lookupServiceBD(AirschedulesConstants.MODULE_NAME, AirschedulesConstants.BDKeys.FLIGHT_SERVICE);
    }
}
