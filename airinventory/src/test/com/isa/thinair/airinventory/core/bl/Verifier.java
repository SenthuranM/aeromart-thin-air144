package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.FlightInvRollResultsSummaryDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.dto.InvRollforwardStatusDTO;
import com.isa.thinair.airinventory.api.dto.SeatDistForRelease;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryJDBCDAO;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.core.bl.ChargeBL;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;


public class Verifier extends PlatformTestCase {
    public void TestGetFccInventory(){
        Collection fccInventoryDTOs = createFlightInventoryBL().getFCCInventoryDTOsNew(24754, "Y", null);
        assertNotNull(fccInventoryDTOs);
    }
    
    public void TestGetBCONDFaresAgentsSummary() throws ModuleException{
        LinkedHashMap bcFaresAgents = createFlightInventoryBL().getBCONDFaresAgentsSummary(20008, "CMB/SHJ", "Y");
        assertNotNull(bcFaresAgents);
    }
    
    public void TestCreateFlightInventory() throws ModuleException{
        Collection segmentDTOs = new ArrayList();
        SegmentDTO segment1 = new SegmentDTO();
        segment1.setSegmentCode("CMB/SHJ");
        segment1.setInvalidSegment(false);  
        segment1.setSegmentId(38275);
        segmentDTOs.add(segment1);
        
        CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
        createFlightInventoryRQ.setFlightId(24774);
        createFlightInventoryRQ.setAircraftModel(getAircraftBD().getAircraftModel("A320INVN02"));
        createFlightInventoryRQ.setSegmentDTOs(segmentDTOs);
        
        createFlightInventoryBL().createFlightInventory(createFlightInventoryRQ);
    }
    
    public void TestGetInventoryForOptimization() throws ModuleException{
        OptimizeSeatInvCriteriaDTO criteria = new OptimizeSeatInvCriteriaDTO();
//        criteria.setAgentCode("DOH010");
        criteria.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_GREATER_THAN);
        criteria.setCheckNegativeAvailability(false);
        criteria.setFlightNumber("G9 999");
        GregorianCalendar calFrom = new GregorianCalendar(2006, Calendar.AUGUST, 1, 0, 0);
        criteria.setFromDate(calFrom.getTime());
        GregorianCalendar calTo = new GregorianCalendar(2006, Calendar.AUGUST, 3, 0, 0);
        criteria.setToDate(calTo.getTime());
        
        criteria.setIncludeAllBCs(false);
        criteria.setIncludeStandardBCsOnly(true);
//        Collection bcCollection = new ArrayList();
//        bcCollection.add("MN");
//        criteria.setSelectedBCs(bcCollection);
        criteria.setSelectedBCs(null);
        
        criteria.setNoOfSeats(20);
        Collection onds = new ArrayList();
        onds.add("CMB/SHJ");
//        onds.add("SHJ/DOH");
//        onds.add("SHJ/SSH/LXR");
        criteria.setOndCodes(onds);
        criteria.setPerformAvailableOnBC(true);
        criteria.setPerformAvailableOnSegment(false);
        
        
        Collection flightIds = new ArrayList();
        flightIds.add(new Integer(24754));
//        flightIds.add(new Integer(27409));
//        flightIds.add(new Integer(27410));
//        
//        flightIds.add(new Integer(27431));
//        flightIds.add(new Integer(27432));
//        
//        flightIds.add(new Integer(27453));
//        flightIds.add(new Integer(27454));
//        
//        flightIds.add(new Integer(27475));
//        flightIds.add(new Integer(27476));
        
        Collection fccInventoryDTOs = getFlightInventoryJDBCDAO().
                                            getFlightInventoriesForOptimization(flightIds, criteria);
        assertNotNull(fccInventoryDTOs);
        
    }
    
    public void TestRollForwardInventory() throws ModuleException{
        InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria(); 
        
        searchCriteriaDTO.setFlightNumber("G99999");
        searchCriteriaDTO.setOriginAirportCode("CMB");
        searchCriteriaDTO.setDestinationAirportCode("SHJ");
        GregorianCalendar calFrom = new GregorianCalendar(2008, Calendar.JANUARY, 2, 0, 0);
        GregorianCalendar calTo = new GregorianCalendar(2008, Calendar.JANUARY, 31, 0, 0);
        
        searchCriteriaDTO.setFromDate(calFrom.getTime());
        searchCriteriaDTO.setToDate(calTo.getTime());
        
        Frequency f = new Frequency();
        f.setDay0(false);
        f.setDay1(false);
        f.setDay2(true);
        f.setDay3(true);
        f.setDay4(true);
        f.setDay5(true);
        f.setDay6(true);
        searchCriteriaDTO.setFrequency(f);
        
        List bookingCodes = new ArrayList();
        bookingCodes.add("YC");
        bookingCodes.add("YD");
        bookingCodes.add("YF");
        searchCriteriaDTO.addSegmentBookingCodes("CMB/SHJ", bookingCodes);
        
//        List bookingCodes1 = new ArrayList();
//        bookingCodes1.add("MN");
//        searchCriteriaDTO.addSegmentBookingCodes("SHJ/SSH/LXR", bookingCodes1);
//        
//        List bookingCodes2 = new ArrayList();
//        bookingCodes2.add("MN");
//        bookingCodes2.add("MNF");
//        searchCriteriaDTO.addSegmentBookingCodes("SSH/LXR", bookingCodes2);
        
        Collection collectionFligthSummary = getFlightBD().getLikeFlightSummary(searchCriteriaDTO);
        
        Set flightIds = new HashSet();
        if (collectionFligthSummary != null && collectionFligthSummary.size() > 0) {
            FlightSummaryDTO flightSummaryDTO = null;
            Iterator iter = collectionFligthSummary.iterator();

            while (iter.hasNext()) {
                flightSummaryDTO = (FlightSummaryDTO) iter.next();
                int flightID = flightSummaryDTO.getFlightId();
                flightIds.add(new Integer(flightID));
            }
        }
        
        InvRollForwardCriteriaDTO criteriaDTO = new InvRollForwardCriteriaDTO();
        criteriaDTO.setInvRollForwardFlightsSearchCriteria(searchCriteriaDTO);
        criteriaDTO.setBcInventoryOverridingLevel(InvRollForwardCriteriaDTO.OVERRIDE_ALL);
        
//        criteriaDTO.setRollFixedBCAllocs(true);
//        criteriaDTO.setRollNonStadardBCAllocs(true);
//        criteriaDTO.setRollStandardBCAllocs(true);
        
        criteriaDTO.setSourceCabinClassCode("Y");
        criteriaDTO.setSourceFlightId(22963);
        
//        Set flightIds = new HashSet();      
//        flightIds.add(new Integer(27476));
//        flightIds.add(new Integer(27477));
//        flightIds.add(new Integer(27478));
//        flightIds.add(new Integer(27479));

        InvRollforwardStatusDTO status = createFlightInventoryBL().rollForwardFCCSegBCInventoriesNew(flightIds,criteriaDTO);
        assertNotNull(status);
        
//        Iterator statusIt = status.iterator();
//        while (statusIt.hasNext()){
//            FlightInvRollResultsSummaryDTO s = (FlightInvRollResultsSummaryDTO)statusIt.next();
//            System.out.println("---------SegmentCode = " + s.getSegmentCode());
//            Iterator bcIt = s.getBcRollStatus().keySet().iterator();
//            while (bcIt.hasNext()){
//                String bc = (String) bcIt.next();
//                System.out.println("bc="+bc+",status="+s.getBcRollStatus().get(bc));
//            }
//        }
    }
    
    public void TestIsFCCInventoryUpdatable() throws ModuleException{
        Collection flightIds = new ArrayList();
        flightIds.add(new Integer(35004));
        boolean isUpdatable = createFlightInventoryBL().checkFCCInventoryModelUpdatability(flightIds, 
                getAircraftBD().getAircraftModel("A320MN1"), getAircraftBD().getAircraftModel("A320NAS "));
        assertTrue(isUpdatable);
    }
    
    public void TestUpdateInventoryCabinClasses() throws ModuleException{
        Collection flightIds = new ArrayList();
        flightIds.add(new Integer(22966));
        createFlightInventoryBL().updateFCCInventoriesForModelUpdate(flightIds, 
                getAircraftBD().getAircraftModel("A330"), getAircraftBD().getAircraftModel("A320MN1"));
    }
    
    public void TestDeleteFlightInventories() throws ModuleException{
        Collection flightIds = new ArrayList();
        flightIds.add(new Integer(35003));
        flightIds.add(new Integer(35004));
        createFlightInventoryBL().deleteFlightInventories(flightIds);
    }
    
    public void TestMakingSegmentInValid(){
        List segIds = new ArrayList();
        segIds.add(new Integer(58365));
        
        createFlightInventoryBL().invalidateFlightSegmentInventories(segIds);
    }
    
    public void TestMakeInvalidSegValid() throws ModuleException{
        List segIds = new ArrayList();
        segIds.add(new Integer(58365));
        createFlightInventoryBL().makeInvalidFltSegInvsValid(segIds);
    }
    
    //--------------- Reservations related funtionalities
    public void TestBlockSeats() throws ModuleException{
        Collection segSeatDistDTOs = new ArrayList();

        SegmentSeatDistsDTO segSeatDistDTO = new SegmentSeatDistsDTO();
        segSeatDistDTOs.add(segSeatDistDTO);
        
        segSeatDistDTO.setFlightId(21510);
        segSeatDistDTO.setCabinClassCode("Y");
        segSeatDistDTO.setSegmentCode("CMB/SHJ");
        segSeatDistDTO.setNoOfInfantSeats(5);
        Collection seatDists = new ArrayList();
        segSeatDistDTO.setSeatDistributions(seatDists);
        
        SeatDistribution seatDist1 = new SeatDistribution();
        seatDist1.setBookingClassCode("YA");
        seatDist1.setNoOfSeats(50);
        seatDists.add(seatDist1);
        
        SeatDistribution seatDist2 = new SeatDistribution();
        seatDist2.setBookingClassCode("YB");
        seatDist2.setNoOfSeats(30);
        seatDists.add(seatDist2);
        
        SeatDistribution seatDist3 = new SeatDistribution();
        seatDist3.setBookingClassCode("YC");
        seatDist3.setNoOfSeats(10);
        seatDists.add(seatDist3);
        
        //Collection blockedRecs = createFlightInventoryResBL().blockSeatsForRes(segSeatDistDTOs);
        Collection blockedRecs = null; //FIXME
        assertNotNull(blockedRecs);
        
        createFlightInventoryResBL().moveBlockedSeatsToSold(blockedRecs);
    }
    
    public void testReleaseSeats() throws ModuleException {
    	Collection segDTOsForRelease = new ArrayList();
    	
    	SegmentSeatDistsDTOForRelease segSeatDist = new SegmentSeatDistsDTOForRelease();
    	segSeatDist.setFlightSegId(59915);
    	
    	SeatDistForRelease seatDist = new SeatDistForRelease();
    	seatDist.setBookingCode("N1");
    	seatDist.setSoldSeats(5);
    	
    	segSeatDist.addSeatDistributionForRelease(seatDist);
    	
    	segSeatDist.setSoldInfantSeatsForRelease(1);
    	
    	segDTOsForRelease.add(segSeatDist);
    	
    	createFlightInventoryResBL().releaseSeats(segDTOsForRelease);
    }
    
    public void TestFilterFlightsHavingReservations(){
        Collection flightIds = new ArrayList();
        flightIds.add(new Integer(34913));
        Collection havingRes = getFlightInventoryResDAO().filterFlightsHavingReservations(flightIds, "Y");
        assertNotNull(havingRes);
    }
    
    
    
    private FlightInventoryResBL createFlightInventoryResBL(){
        return new FlightInventoryResBL();
    }  
    //---------------------------------------------------
    
    private FlightInventoryBL createFlightInventoryBL(){
        return new FlightInventoryBL();
    }  
    
    private FlightBD getFlightBD(){
        return (FlightBD) AirInventoryUtil.getInstance().lookupServiceBD(
                AirschedulesConstants.MODULE_NAME,
                AirschedulesConstants.BDKeys.FLIGHT_SERVICE);
    }
    
    private AircraftBD getAircraftBD() {
        return (AircraftBD) AirInventoryUtil.getInstance().lookupServiceBD(
                "airmaster", "aircraft.service");
    }
    
    private FlightInventoryJDBCDAO getFlightInventoryJDBCDAO() {
        FlightInventoryJDBCDAO flightInventoryJDBCDAO = (FlightInventoryJDBCDAO) AirInventoryUtil
                .getInstance().getLocalBean("flightInventoryJdbcDAO");
        return flightInventoryJDBCDAO;
    }
    
    private FlightInventoryResDAO getFlightInventoryResDAO(){
        return (FlightInventoryResDAO)AirInventoryUtil.getInstance().
                        getLocalBean("FlightInventoryResDAOImplProxy");
    }
}
