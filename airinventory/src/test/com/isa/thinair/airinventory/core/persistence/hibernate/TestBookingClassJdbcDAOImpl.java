package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.Iterator;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria.BookingClassTypes;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassJDBCDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

import com.isa.thinair.platform.api.util.PlatformTestCase;



public class TestBookingClassJdbcDAOImpl extends PlatformTestCase {
    protected void setUp() throws Exception{
        super.setUp();
    }
    
    public void testGetBookingClassDTOs() throws ModuleException{
        SearchBCsCriteria criteria = new SearchBCsCriteria();
//        criteria.setIncludeStandardBCsOnly(false);
        criteria.setClassOfService("Y");
        criteria.setBookingClassType(BookingClassTypes.STANDARD_ONLY);
        criteria.setStartIndex(1);
        criteria.setPageSize(5);
        //Page page = getBookingClassJdbcDAO().getBookingClassesLinkedToInvDTOs(criteria);
        Page page = null; //FIXME
        assertNotNull(page);
        assertEquals(5, page.getPageData().size());
        Iterator bcDTOIt = page.getPageData().iterator();
        while (bcDTOIt.hasNext()){
            BookingClassDTO bcDTO = (BookingClassDTO)bcDTOIt.next();
            System.out.println(bcDTO.getBookingClass().getBookingCode());
        }
    }
    
    public void testGetBookingClassDTO() throws ModuleException {
        //BookingClassDTO bcDTO = getBookingClassJdbcDAO().getBookingClassDTO("YA");
        BookingClassDTO bcDTO = null; //FIXME
        assertNotNull(bcDTO);
    }
    
    private BookingClassJDBCDAO getBookingClassJdbcDAO(){
        return (BookingClassJDBCDAO)AirInventoryUtil.getInstance().getLocalBean("bookingClassJdbcDAO");
    }
}
