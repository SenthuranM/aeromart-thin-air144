package com.isa.thinair.airinventory.core.bl;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.InterceptingFlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryDAO;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;


public class TestCreateFlightInventory extends PlatformTestCase {
    public TestCreateFlightInventory(){
        super("airinventoryTestModulesConfig");
    }
    
    protected void setUp() throws Exception{
        super.setUp();
    }
    
//    public void testCreateInvForSingleSegSingleCCFlight() throws ModuleException{
//        Collection segmentDTOs = new ArrayList();
//        SegmentDTO segment1 = new SegmentDTO();
//        segment1.setSegmentCode("CMB/SHJ");
//        segment1.setInvalidSegment(false);     
//        segmentDTOs.add(segment1);
//        
//        getFlightInventoryBL().createFlightInventory(19000, 
//                getAircraftBD().getAircraftModel("A320XINV1"), segmentDTOs, null);
//        
//        Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19000, "I", null).iterator();
//        while (fccInventoryDTOsIt.hasNext()){
//            FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//            Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//            assertNotNull(fccSegInventoryDTOs);
//            assertEquals(1, fccSegInventoryDTOs.size());
//        }
//    }
    
    public void testCreateInvForSingleSegMultipleCCFlight() throws ModuleException{
        Collection segmentDTOs = new ArrayList();
        SegmentDTO segment1 = new SegmentDTO();
        segment1.setSegmentCode("CMB/SHJ");
        segment1.setInvalidSegment(false);     
        segmentDTOs.add(segment1);
        
        CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
        createFlightInventoryRQ.setFlightId(19001);
        createFlightInventoryRQ.setAircraftModel(getAircraftBD().getAircraftModel("A320XINV2"));
        createFlightInventoryRQ.setSegmentDTOs(segmentDTOs);
        
        getFlightInventoryBL().createFlightInventory(createFlightInventoryRQ);
        String [] cabinClasses = {"I", "Y", "J"};
//        for (int i=0; i< cabinClasses.length; i++){
//        Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19001, cabinClasses[i], null).iterator();
//            while (fccInventoryDTOsIt.hasNext()){
//                FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//                Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//                assertNotNull(fccSegInventoryDTOs);
//                assertEquals(1, fccSegInventoryDTOs.size());
//            }
//        }
    }
    
    public void testCreateInvForMultipleSegSingleCCFlight() throws ModuleException{
        Collection segmentDTOs = new ArrayList();
        SegmentDTO segment1 = new SegmentDTO();
        segment1.setSegmentCode("SHJ/SSH");
        segment1.setInvalidSegment(false);
        Collection interceptingSegs1 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg1 = new InterceptingFlightSegmentDTO();
        interceptingSeg1.setFlightId(19002);
        interceptingSeg1.setInterceptingSegCode("SHJ/SSH/LXR");
        interceptingSegs1.add(interceptingSeg1);
        segment1.setInterceptingFlightSeg(interceptingSegs1);
        segmentDTOs.add(segment1);
        
        SegmentDTO segment2 = new SegmentDTO();
        segment2.setSegmentCode("SSH/LXR");
        segment2.setInvalidSegment(false);
        Collection interceptingSegs2 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg2 = new InterceptingFlightSegmentDTO();
        interceptingSeg2.setFlightId(19002);
        interceptingSeg2.setInterceptingSegCode("SHJ/SSH/LXR");
        interceptingSegs2.add(interceptingSeg2);
        segment2.setInterceptingFlightSeg(interceptingSegs2);
        segmentDTOs.add(segment2);
        
        SegmentDTO segment3 = new SegmentDTO();
        segment3.setSegmentCode("SHJ/SSH/LXR");
        segment3.setInvalidSegment(false);
        Collection interceptingSegs3 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg3 = new InterceptingFlightSegmentDTO();
        interceptingSeg3.setFlightId(19002);
        interceptingSeg3.setInterceptingSegCode("SHJ/SSH");
        interceptingSegs3.add(interceptingSeg3);
        segment3.setInterceptingFlightSeg(interceptingSegs3);
        InterceptingFlightSegmentDTO interceptingSeg4 = new InterceptingFlightSegmentDTO();
        interceptingSeg4.setFlightId(19002);
        interceptingSeg4.setInterceptingSegCode("SSH/LXR");
        interceptingSegs3.add(interceptingSeg4);
        segment3.setInterceptingFlightSeg(interceptingSegs3);
        segmentDTOs.add(segment3);
        
        CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
        createFlightInventoryRQ.setFlightId(19002);
        createFlightInventoryRQ.setAircraftModel(getAircraftBD().getAircraftModel("A320XINV1"));
        createFlightInventoryRQ.setSegmentDTOs(segmentDTOs);
        
        getFlightInventoryBL().createFlightInventory(createFlightInventoryRQ);    
        
//        Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19002, "I", null).iterator();
//        HashMap fccSegInvIds = new HashMap();
//        while (fccInventoryDTOsIt.hasNext()){
//            FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//            Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//            assertNotNull(fccSegInventoryDTOs);
//            assertEquals(3, fccSegInventoryDTOs.size());
//            Iterator fccSegInventoryDTOsIt = fccSegInventoryDTOs.iterator();
//            while (fccSegInventoryDTOsIt.hasNext()){
//                FCCSegInventoryDTO fccSegInvDTO = (FCCSegInventoryDTO)fccSegInventoryDTOsIt.next();
//                fccSegInvIds.put(fccSegInvDTO.getFccSegInventory().getSegmentCode(), new Integer(fccSegInvDTO.getFccSegInventory().getFccsInvId()));
//            }
//        }
//        List interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19002, "I", "SHJ/SSH/LXR");
//        assertEquals(2, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SHJ/SSH")));
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR")));
//        interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19002, "I", "SHJ/SSH");
//        assertEquals(1, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SHJ/SSH/LXR")));
//        interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19002, "I", "SSH/LXR");
//        assertEquals(1, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SHJ/SSH/LXR")));
    }
    
    public void testCreateInvForMultipleSegMultipleCCFlight() throws ModuleException{
        Collection segmentDTOs = new ArrayList();
        SegmentDTO segment1 = new SegmentDTO();
        segment1.setSegmentCode("SHJ/SSH");
        segment1.setInvalidSegment(false);
        Collection interceptingSegs1 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg1 = new InterceptingFlightSegmentDTO();
        interceptingSeg1.setFlightId(19003);
        interceptingSeg1.setInterceptingSegCode("SHJ/SSH/LXR");
        interceptingSegs1.add(interceptingSeg1);
        segment1.setInterceptingFlightSeg(interceptingSegs1);
        segmentDTOs.add(segment1);
        
        SegmentDTO segment2 = new SegmentDTO();
        segment2.setSegmentCode("SSH/LXR");
        segment2.setInvalidSegment(false);
        Collection interceptingSegs2 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg2 = new InterceptingFlightSegmentDTO();
        interceptingSeg2.setFlightId(19003);
        interceptingSeg2.setInterceptingSegCode("SHJ/SSH/LXR");
        interceptingSegs2.add(interceptingSeg2);
        segment2.setInterceptingFlightSeg(interceptingSegs2);
        segmentDTOs.add(segment2);
        
        SegmentDTO segment3 = new SegmentDTO();
        segment3.setSegmentCode("SHJ/SSH/LXR");
        segment3.setInvalidSegment(false);
        Collection interceptingSegs3 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg3 = new InterceptingFlightSegmentDTO();
        interceptingSeg3.setFlightId(19003);
        interceptingSeg3.setInterceptingSegCode("SHJ/SSH");
        interceptingSegs3.add(interceptingSeg3);
        segment3.setInterceptingFlightSeg(interceptingSegs3);
        InterceptingFlightSegmentDTO interceptingSeg4 = new InterceptingFlightSegmentDTO();
        interceptingSeg4.setFlightId(19003);
        interceptingSeg4.setInterceptingSegCode("SSH/LXR");
        interceptingSegs3.add(interceptingSeg4);
        segment3.setInterceptingFlightSeg(interceptingSegs3);
        segmentDTOs.add(segment3);
        
        CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
        createFlightInventoryRQ.setFlightId(19003);
        createFlightInventoryRQ.setAircraftModel(getAircraftBD().getAircraftModel("A320XINV2"));
        createFlightInventoryRQ.setSegmentDTOs(segmentDTOs);
        
        getFlightInventoryBL().createFlightInventory(createFlightInventoryRQ);    
        String [] cabinClasses = {"I", "Y", "J"};
//        for (int i=0; i< cabinClasses.length; i++){
//            Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19003, cabinClasses[i], null).iterator();
//            HashMap fccSegInvIds = new HashMap();
//            while (fccInventoryDTOsIt.hasNext()){
//                FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//                Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//                assertNotNull(fccSegInventoryDTOs);
//                assertEquals(3, fccSegInventoryDTOs.size());
//                Iterator fccSegInventoryDTOsIt = fccSegInventoryDTOs.iterator();
//                while (fccSegInventoryDTOsIt.hasNext()){
//                    FCCSegInventoryDTO fccSegInvDTO = (FCCSegInventoryDTO)fccSegInventoryDTOsIt.next();
//                    fccSegInvIds.put(fccSegInvDTO.getFccSegInventory().getSegmentCode(), new Integer(fccSegInvDTO.getFccSegInventory().getFccsInvId()));
//                }
//            }
//            List interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19003, cabinClasses[i], "SHJ/SSH/LXR");
//            assertEquals(2, interceptingSegInvIds.size());
//            assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SHJ/SSH")));
//            assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR")));
//            interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19003, cabinClasses[i], "SHJ/SSH");
//            assertEquals(1, interceptingSegInvIds.size());
//            assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SHJ/SSH/LXR")));
//            interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19003, cabinClasses[i], "SSH/LXR");
//            assertEquals(1, interceptingSegInvIds.size());
//            assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SHJ/SSH/LXR")));
//        }
    }
    
    public void testCreateInvForMultipleSegSingleCCFltWithOverlappingFlt() throws ModuleException{
        Collection segmentDTOs = new ArrayList();
        SegmentDTO segment1 = new SegmentDTO();
        segment1.setSegmentCode("SSH/LXR");
        segment1.setInvalidSegment(false);
        Collection interceptingSegs1 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg1 = new InterceptingFlightSegmentDTO();
        interceptingSeg1.setFlightId(19005);
        interceptingSeg1.setInterceptingSegCode("SSH/LXR/SHJ");
        interceptingSegs1.add(interceptingSeg1);
        InterceptingFlightSegmentDTO interceptingSeg11 = new InterceptingFlightSegmentDTO();
        interceptingSeg11.setFlightId(19004);
        interceptingSeg11.setInterceptingSegCode("SSH/LXR");
        interceptingSegs1.add(interceptingSeg11);
        InterceptingFlightSegmentDTO interceptingSeg12 = new InterceptingFlightSegmentDTO();
        interceptingSeg12.setFlightId(19004);
        interceptingSeg12.setInterceptingSegCode("SHJ/SSH/LXR");
        interceptingSegs1.add(interceptingSeg12);
        segment1.setInterceptingFlightSeg(interceptingSegs1);
        
        segmentDTOs.add(segment1);
        
        SegmentDTO segment2 = new SegmentDTO();
        segment2.setSegmentCode("LXR/SHJ");
        segment2.setInvalidSegment(false);
        Collection interceptingSegs2 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg2 = new InterceptingFlightSegmentDTO();
        interceptingSeg2.setFlightId(19005);
        interceptingSeg2.setInterceptingSegCode("SSH/LXR/SHJ");
        interceptingSegs2.add(interceptingSeg2);
        segment2.setInterceptingFlightSeg(interceptingSegs2);
        segmentDTOs.add(segment2);
        
        SegmentDTO segment3 = new SegmentDTO();
        segment3.setSegmentCode("SSH/LXR/SHJ");
        segment3.setInvalidSegment(false);
        Collection interceptingSegs3 = new ArrayList();
        InterceptingFlightSegmentDTO interceptingSeg3 = new InterceptingFlightSegmentDTO();
        interceptingSeg3.setFlightId(19005);
        interceptingSeg3.setInterceptingSegCode("SSH/LXR");
        interceptingSegs3.add(interceptingSeg3);
//        segment3.setInterceptingFlightSeg(interceptingSegs3);
        InterceptingFlightSegmentDTO interceptingSeg4 = new InterceptingFlightSegmentDTO();
        interceptingSeg4.setFlightId(19005);
        interceptingSeg4.setInterceptingSegCode("LXR/SHJ");
        interceptingSegs3.add(interceptingSeg4);
//        segment3.setInterceptingFlightSeg(interceptingSegs3);
        
        InterceptingFlightSegmentDTO interceptingSeg5 = new InterceptingFlightSegmentDTO();
        interceptingSeg5.setFlightId(19004);
        interceptingSeg5.setInterceptingSegCode("SHJ/SSH/LXR");
        interceptingSegs3.add(interceptingSeg5);
//        segment3.setInterceptingFlightSeg(interceptingSegs3);
        
        InterceptingFlightSegmentDTO interceptingSeg6 = new InterceptingFlightSegmentDTO();
        interceptingSeg6.setFlightId(19004);
        interceptingSeg6.setInterceptingSegCode("SSH/LXR");
        interceptingSegs3.add(interceptingSeg6);
        segment3.setInterceptingFlightSeg(interceptingSegs3);
        segmentDTOs.add(segment3);
        
        CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
        createFlightInventoryRQ.setFlightId(19005);
        createFlightInventoryRQ.setAircraftModel(getAircraftBD().getAircraftModel("A320XINV1"));
        createFlightInventoryRQ.setSegmentDTOs(segmentDTOs);
        createFlightInventoryRQ.setOverlappingFlightId(19004);
        
        getFlightInventoryBL().createFlightInventory(createFlightInventoryRQ);    
        
//        Iterator fccInventoryDTOsIt = getFlightInventoryBL().getFCCInventoryDTOs(19005, "I", null).iterator();
//        HashMap fccSegInvIds = new HashMap();
//        while (fccInventoryDTOsIt.hasNext()){
//            FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO)fccInventoryDTOsIt.next();
//            Collection fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
//            assertNotNull(fccSegInventoryDTOs);
//            assertEquals(3, fccSegInventoryDTOs.size());
//            Iterator fccSegInventoryDTOsIt = fccSegInventoryDTOs.iterator();
//            while (fccSegInventoryDTOsIt.hasNext()){
//                FCCSegInventoryDTO fccSegInvDTO = (FCCSegInventoryDTO)fccSegInventoryDTOsIt.next();
//                String segmentCode = fccSegInvDTO.getFccSegInventory().getSegmentCode();
//                fccSegInvIds.put((segmentCode+fccSegInvDTO.getFccSegInventory().getFlightId()), new Integer(fccSegInvDTO.getFccSegInventory().getFccsInvId()));
//                if (segmentCode.equals("SSH/LXR")){
//                    assertEquals(35, fccSegInvDTO.getFccSegInventory().getAvailableSeats());
//                    assertEquals(5, fccSegInvDTO.getFccSegInventory().getAvailableInfantSeats());
//                } else if (segmentCode.equals("SSH/LXR/SHJ")){
//                    assertEquals(35, fccSegInvDTO.getFccSegInventory().getAvailableSeats());
//                    assertEquals(5, fccSegInvDTO.getFccSegInventory().getAvailableInfantSeats());
//                }else if (segmentCode.equals("LXR/SHJ")){
//                    assertEquals(150, fccSegInvDTO.getFccSegInventory().getAvailableSeats());
//                    assertEquals(10, fccSegInvDTO.getFccSegInventory().getAvailableInfantSeats());
//                }
//            }
//        }
//        List interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19005, "I", "SSH/LXR/SHJ");
//        assertEquals(4, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR" + 19005)));
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("LXR/SHJ" + 19005)));
//        assertTrue(interceptingSegInvIds.contains(new Integer(19001)));
//        assertTrue(interceptingSegInvIds.contains(new Integer(19002)));
//        interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19005, "I", "SSH/LXR");
//        assertEquals(3, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR/SHJ"+19005)));
//        assertTrue(interceptingSegInvIds.contains(new Integer(19001)));
//        assertTrue(interceptingSegInvIds.contains(new Integer(19002)));
//        interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19005, "I", "LXR/SHJ");
//        assertEquals(1, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR/SHJ"+19005)));
//        
//        //overlapping flights' intercepting segments
//        interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19004, "I", "SHJ/SSH/LXR");
//        assertEquals(4, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR" + 19005)));
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR/SHJ" + 19005)));
//        interceptingSegInvIds = getFlightInventoryDAO().getInterceptingFCCSegmentInventoryIds(19004, "I", "SSH/LXR");
//        assertEquals(3, interceptingSegInvIds.size());
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR" + 19005)));
//        assertTrue(interceptingSegInvIds.contains(fccSegInvIds.get("SSH/LXR/SHJ" + 19005)));
    }
    
    protected void tearDown(){
        
    }
    
    private FlightInventoryDAO getFlightInventoryDAO() {
        return (FlightInventoryDAO) AirInventoryUtil.getInstance()
                .getLocalBean("FlightInventoryDAOImplProxy");
    }
    
    private FlightInventoryBL getFlightInventoryBL(){
        return new FlightInventoryBL();
    }
    
    private AircraftBD getAircraftBD() {
        return (AircraftBD) AirInventoryUtil.getInstance().lookupServiceBD(
                "airmaster", "aircraft.service");
    }
}
