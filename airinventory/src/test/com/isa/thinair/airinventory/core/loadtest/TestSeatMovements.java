package com.isa.thinair.airinventory.core.loadtest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.SeatDistForRelease;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.IFaresChargesAndSeats;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTOForRelease;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class TestSeatMovements implements Runnable {
	
	private final Log log = LogFactory.getLog(getClass());
	
	private int id;
	private static List list = Collections.synchronizedList(new ArrayList());
	
	private static final int ONEWAY_SINGLE_SEGMENT = 1;
	private static final int ONEWAY_CONNECTED_SEGMETS = 2;
	private static final int RETURN_SINGLE_SEGMENTS = 3;
	private static final int RETURN_CONNECTED_SEGMENTS = 4;
	private static final int ONEWAY_OVERLAPPING_FLIGHT_SEGMENT = 5;
	
	int [] searchTypes = {RETURN_SINGLE_SEGMENTS};
	
	private static final int BLOCK_AND_SELL_SINGLE_TRANSACTION = 1;
	private static final int BLOCK_AND_SELL_TWO_TRANSACTIONS = 2;
	private static final int RELEASE_SEATS = 3;
	private static final int BLOCK_ONHOLD_AND_SELL = 4;
	
	int [] operations = {BLOCK_ONHOLD_AND_SELL};
	
	public TestSeatMovements(int id){
		this.id = id;
	}

	/**
	 * @param args
	 * @throws ModuleException 
	 * @throws ParseException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws ModuleException, ParseException, InterruptedException {
		
		/** Need following configs : platform,airinventory,airreservation,airschedules,airpricing,airmaster,
		 * auditor,crypto,login at following location before starting running this program*/
		System.setProperty("repository.home", "C:/isaconfig_test");
		
		ModuleFramework.startup();
		
		for (int i=0; i<500; i++){
			new Thread(new TestSeatMovements(i)).start();
			if (i!=0 && i%2==0){
				Thread.sleep(500);
			}
		}
//		Thread.sleep(1000);
//		new Thread(new TestSeatMovements(123456)).start();
		
		
	}

	public void run() {
		
		if (hasElement(operations, RELEASE_SEATS) && id == 123456){
			while (true){
				try {
					if (list.size()>0){
						Collection segSeatDists = (Collection) list.remove(0);
						if (segSeatDists != null){
							try {
								System.out.println("["+id + "] RELEASE START");
								getFlightInventoryResBD().releaseSeats(prepareSeatDistForRelease(segSeatDists));
								System.out.println("["+id + "] RELEASE SUCCESS");
							} catch (Exception e){
								if (e instanceof ModuleException){
									System.out.println("  *********  ["+id + "]FAILED :: exCode=" + ((ModuleException)e).getExceptionCode());
									if (((ModuleException)e).getExceptionCode() != null && ((ModuleException)e).getExceptionCode().equals("airinventory.logic.bl.onhold.release.failed")){
										e.printStackTrace();
									}
								} else {
									System.out.println(" ********* ["+id + "]FAILED :: msg=" + ((ModuleException)e).getMessage());
								}
								System.out.println("["+id + "] ADDING RELEASE FAILED SEATS AGAIN");
								list.add(segSeatDists);
//								e.printStackTrace();
							}
						}
					} else {
						Thread.sleep(1000);
						if (list.size()>0){
							continue;
						} else {
							break;
						}
					}
				} catch (Exception e){
					System.out.println(" ********* ["+id + "]FAILED :: msg=" + ((ModuleException)e).getMessage());
				}
			}
			return;
		} else {
		
			try {
				AvailableFlightSearchDTO availableFlightSearchDTO = null;
				if (hasElement(searchTypes, ONEWAY_CONNECTED_SEGMETS)){
					availableFlightSearchDTO = new AvailableFlightSearchDTO();
					availableFlightSearchDTO.setFromAirport("CMB");
					availableFlightSearchDTO.setToAirport("BAH");
					
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					availableFlightSearchDTO.setDepatureDate(df.parse("11/01/2007"));
					availableFlightSearchDTO.setDepartureVariance(0);
					availableFlightSearchDTO.setAdultCount(3);
					availableFlightSearchDTO.setInfantCount(0);
					availableFlightSearchDTO.setAgentCode(null);
					availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
					availableFlightSearchDTO.setBookingType(AirinventoryCustomConstants.BookingType.NORMAL);
					availableFlightSearchDTO.setCabinClassCode("Y");
					availableFlightSearchDTO.setChannelCode(SalesChannel.WEB);
					availableFlightSearchDTO.setOndCode("CMB/SHJ/BAH");
					
					Collection outFlightSegIds = new ArrayList();
					outFlightSegIds.add(new Integer(109892));
					outFlightSegIds.add(new Integer(109923));
					availableFlightSearchDTO.setOutBoundFlights(outFlightSegIds);
				} 
				if (hasElement(searchTypes, ONEWAY_SINGLE_SEGMENT)){
					availableFlightSearchDTO = new AvailableFlightSearchDTO();
					availableFlightSearchDTO.setFromAirport("CMB");
					availableFlightSearchDTO.setToAirport("SHJ");
					
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					availableFlightSearchDTO.setDepatureDate(df.parse("11/01/2007"));
					availableFlightSearchDTO.setDepartureVariance(0);
					availableFlightSearchDTO.setAdultCount(4);
					availableFlightSearchDTO.setInfantCount(0);
					availableFlightSearchDTO.setAgentCode(null);
					availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
					availableFlightSearchDTO.setBookingType(AirinventoryCustomConstants.BookingType.NORMAL);
					availableFlightSearchDTO.setCabinClassCode("Y");
					availableFlightSearchDTO.setChannelCode(SalesChannel.WEB);
					availableFlightSearchDTO.setOndCode("CMB/SHJ");
					
					Collection outFlightSegIds = new ArrayList();
					outFlightSegIds.add(new Integer(109892));
					availableFlightSearchDTO.setOutBoundFlights(outFlightSegIds);
				}
				
				if (hasElement(searchTypes, RETURN_SINGLE_SEGMENTS)){
					availableFlightSearchDTO = new AvailableFlightSearchDTO();
					availableFlightSearchDTO.setFromAirport("CMB");
					availableFlightSearchDTO.setToAirport("SHJ");
					
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					availableFlightSearchDTO.setDepatureDate(df.parse("08/01/2007"));
					availableFlightSearchDTO.setReturnDate(df.parse("09/01/2007"));
					availableFlightSearchDTO.setReturnFlag(true);
					availableFlightSearchDTO.setDepartureVariance(0);
					availableFlightSearchDTO.setAdultCount(3);
					availableFlightSearchDTO.setInfantCount(0);
					availableFlightSearchDTO.setAgentCode(null);
					availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
					availableFlightSearchDTO.setBookingType(AirinventoryCustomConstants.BookingType.NORMAL);
					availableFlightSearchDTO.setCabinClassCode("Y");
					availableFlightSearchDTO.setChannelCode(SalesChannel.WEB);
					availableFlightSearchDTO.setOndCode("CMB/SHJ");
					
					Collection outFlightSegIds = new ArrayList();
					outFlightSegIds.add(new Integer(109889));
					availableFlightSearchDTO.setOutBoundFlights(outFlightSegIds);
					
					Collection ibFlightSegIds = new ArrayList();
					ibFlightSegIds.add(new Integer(110321));
					availableFlightSearchDTO.setInBoundFlights(ibFlightSegIds);
				}
				
				if (hasElement(searchTypes, ONEWAY_OVERLAPPING_FLIGHT_SEGMENT)){
					if (id != 0 && (id%3==0 ||id%4==0)){
						availableFlightSearchDTO = new AvailableFlightSearchDTO();
						availableFlightSearchDTO.setFromAirport("SHJ");
						availableFlightSearchDTO.setToAirport("LXR");
						
						SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						availableFlightSearchDTO.setDepatureDate(df.parse("11/01/2007"));
						availableFlightSearchDTO.setDepartureVariance(0);
						availableFlightSearchDTO.setAdultCount(3);
						availableFlightSearchDTO.setInfantCount(0);
						availableFlightSearchDTO.setAgentCode(null);
						availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
						availableFlightSearchDTO.setBookingType(AirinventoryCustomConstants.BookingType.NORMAL);
						availableFlightSearchDTO.setCabinClassCode("Y");
						availableFlightSearchDTO.setChannelCode(SalesChannel.WEB);
						availableFlightSearchDTO.setOndCode("SHJ/SSH/LXR");
						
						Collection outFlightSegIds = new ArrayList();
						outFlightSegIds.add(new Integer(109974));
						availableFlightSearchDTO.setOutBoundFlights(outFlightSegIds);
					} else {
						availableFlightSearchDTO = new AvailableFlightSearchDTO();
						availableFlightSearchDTO.setFromAirport("SSH");
						availableFlightSearchDTO.setToAirport("SHJ");
						
						SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						availableFlightSearchDTO.setDepatureDate(df.parse("11/01/2007"));
						availableFlightSearchDTO.setDepartureVariance(0);
						availableFlightSearchDTO.setAdultCount(4);
						availableFlightSearchDTO.setInfantCount(0);
						availableFlightSearchDTO.setAgentCode(null);
						availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
						availableFlightSearchDTO.setBookingType(AirinventoryCustomConstants.BookingType.NORMAL);
						availableFlightSearchDTO.setCabinClassCode("Y");
						availableFlightSearchDTO.setChannelCode(SalesChannel.WEB);
						availableFlightSearchDTO.setOndCode("SSH/LXR/SHJ");
						
						Collection outFlightSegIds = new ArrayList();
						outFlightSegIds.add(new Integer(110067));
						availableFlightSearchDTO.setOutBoundFlights(outFlightSegIds);
					}
				}
				
				System.out.println("["+id + "]CRITERIA:: ond="+availableFlightSearchDTO.getOndCode()+", seats="+availableFlightSearchDTO.getAdultCount());
				
				FlightBD flightBD  = getFlightBD();
		        ServiceResponce sr = flightBD.getFilledSelectedFlight(availableFlightSearchDTO, false);
		        SelectedFlightDTO selectedFlightDTO = (SelectedFlightDTO) 
		            sr.getResponseParam(ResponceCodes.FILL_SELECTED_FLIGHT_SUCCESSFULL);
		        
		        FlightInventoryResBD flightInventoryResBD = getFlightInventoryResBD();
		        selectedFlightDTO = flightInventoryResBD.searchSelectedFlightsSeatAvailability(
		                    selectedFlightDTO, availableFlightSearchDTO);
		        if (selectedFlightDTO != null & selectedFlightDTO.getSelectedOndFlight(SegmentSequence.OUT_BOUND).isSeatsAvailable()){
		        	Collection ondFareDTOs = ((IFaresChargesAndSeats)selectedFlightDTO).getFareChargesSeatsSegmentsDTOs();
		        	if (ondFareDTOs != null){
		        		Collection allSegmentSeatDistsDTO = new LinkedList();
		        		Iterator ondFareDTOsIt = ondFareDTOs.iterator();
		        		String infoStr = "";
		        		while (ondFareDTOsIt.hasNext()){
		        			OndFareDTO ondFareDTO = (OndFareDTO) ondFareDTOsIt.next();
		        			Collection seatDists = ondFareDTO.getSegmentSeatDistsDTO();
		        			allSegmentSeatDistsDTO.addAll(seatDists);
		        			
		        			
		        			for (Iterator it = seatDists.iterator(); it.hasNext(); ){
		        				SegmentSeatDistsDTO segSeatDist = (SegmentSeatDistsDTO) it.next();
		        				Collection seats = segSeatDist.getSeatDistribution();
		        				if (infoStr.equals("")){
		        					infoStr += segSeatDist.getSegmentCode() + "::";
		        				} else {
		        					infoStr += ", " + segSeatDist.getSegmentCode() + "::";
		        				}
		        				
		        				for (Iterator sIt = seats.iterator(); sIt.hasNext(); ){
		        					SeatDistribution seatDist = (SeatDistribution) sIt.next();
		        					if (infoStr.endsWith("::")){
		        						infoStr += seatDist.getBookingClassCode() + ":" + seatDist.getNoOfSeats();
		        					} else {
		        						infoStr += "|" + seatDist.getBookingClassCode() + ":" + seatDist.getNoOfSeats();
		        					}
		        				}
		        			}
		        			infoStr += "#fare=" + ondFareDTO.getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT);
		        		}
	        			System.out.println("["+id + "]" + infoStr);
	        			
//	        			int current = BLOCK_AND_SELL_SINGLE_TRANSACTION;
//	        			if (id%8==0) current = BLOCK_AND_SELL_TWO_TRANSACTIONS;
//	        			if (id%7==0 ||id%9==0) current = BLOCK_ONHOLD_AND_SELL;
	        			
	        			int current = BLOCK_ONHOLD_AND_SELL;
	        			
	        			if (hasElement(operations, BLOCK_AND_SELL_SINGLE_TRANSACTION) && current == BLOCK_AND_SELL_SINGLE_TRANSACTION){
	        				System.out.println("["+id + "] START BLOCK/SELL IN SINGLE TX");
	        				getFlightInventoryResBD().testBlockAndSeatSeats(allSegmentSeatDistsDTO, availableFlightSearchDTO.getAdultCount(), availableFlightSearchDTO.getInfantCount());
	        				list.add(allSegmentSeatDistsDTO);
	        				System.out.println("["+id + "] SUCCESS BLOCK/SELL IN SINGLE TX");
	        				log.info("["+id + "] SUCCESS [" + infoStr + "]");
	        			} 
	        			if (hasElement(operations, BLOCK_AND_SELL_TWO_TRANSACTIONS) && current == BLOCK_AND_SELL_TWO_TRANSACTIONS){
	        				System.out.println("["+id + "] START BLOCK/SELL IN TWO TX");
		        			Collection blockedSeats = getFlightInventoryResBD().blockSeatsForRes(allSegmentSeatDistsDTO);
		        			getFlightInventoryResBD().partiallyMoveBlockedSeatsToSold(blockedSeats, availableFlightSearchDTO.getAdultCount(), availableFlightSearchDTO.getInfantCount());
		        			list.add(allSegmentSeatDistsDTO);
		        			System.out.println("["+id + "] SUCCESS BLOCK/SELL IN TWO TX");
	        			} 
	        			if (hasElement(operations, BLOCK_ONHOLD_AND_SELL) && current == BLOCK_ONHOLD_AND_SELL) {
	        				System.out.println("["+id + "] START BLOCK/ONHOLD/SELL IN TWO TX");
		        			Collection blockedSeats = getFlightInventoryResBD().blockSeatsForRes(allSegmentSeatDistsDTO);
		        			getFlightInventoryResBD().onholdBlockedSeats(blockedSeats);
		        			getFlightInventoryResBD().confirmOnholdSeats(prepareSeatDistForConfirm(allSegmentSeatDistsDTO));
		        			list.add(allSegmentSeatDistsDTO);
		        			System.out.println("["+id + "] SUCCESS BLOCK/ONHOLD/SELL IN TWO TX");
	        			}
	        			
	        			if (hasElement(operations, RELEASE_SEATS) && (id%2==0 || id%3==0)){
	        				if (list.size()>0) {
		        				Collection segSeatDists = (Collection) list.remove(0);
		        				if (segSeatDists != null){
		        					try {
		        						System.out.println("["+id + "] RELEASE START");
		        						getFlightInventoryResBD().releaseSeats(prepareSeatDistForRelease(segSeatDists));
		        						System.out.println("["+id + "] RELEASE SUCCESS");
		        					} catch (Exception e){
		        						if (e instanceof ModuleException){
											System.out.println("  *********  ["+id + "]FAILED :: exCode=" + ((ModuleException)e).getExceptionCode());
											if (((ModuleException)e).getExceptionCode() != null && ((ModuleException)e).getExceptionCode().equals("airinventory.logic.bl.onhold.release.failed")){
												e.printStackTrace();
											}
										} else {
											System.out.println(" ********* ["+id + "]FAILED :: msg=" + ((ModuleException)e).getMessage());
										}
										System.out.println("["+id + "] ADDING RELEASE FAILED SEATS AGAIN");
										list.add(segSeatDists);
		        					}
		        				}
	        				}
		        		}
		        	}
		        } else {
		        	System.out.println("["+id + "] FAILED, FARE NOT FOUND");
		        }
			} catch (Exception e){
				if (e instanceof ModuleException){
					System.out.println("["+id + "] ##### FAILED :: exCode=" + ((ModuleException)e).getExceptionCode());
					if (((ModuleException)e).getExceptionCode() != null && ((ModuleException)e).getExceptionCode().equals("airinventory.logic.bl.onhold.release.failed")){
						e.printStackTrace();
					}
				} else {
					System.out.println("["+id + "] ##### FAILED :: msg=" + e.getMessage());
				}
//				e.printStackTrace();
			}
		}
		
	}
	
	private Collection prepareSeatDistForConfirm(Collection segmentSeatDistsDTO) {
		Collection seatDistForConfirm = new ArrayList();
		String seatsStr = "";
		for (Iterator it = segmentSeatDistsDTO.iterator(); it.hasNext(); ){
			SegmentSeatDistsDTO segSeatDist = (SegmentSeatDistsDTO) it.next();
			SegmentSeatDistsDTO segSeatDistForCnf =new SegmentSeatDistsDTO();
			seatDistForConfirm.add(segSeatDistForCnf);
			segSeatDistForCnf.setBookingClassType(segSeatDist.getBookingClassType());
			segSeatDistForCnf.setCabinClassCode(segSeatDist.getCabinClassCode());
			segSeatDistForCnf.setFlightSegId(segSeatDist.getFlightSegId());
			
			if (seatsStr.equals("")){
				seatsStr += segSeatDist.getSegmentCode() + "::";
			} else {
				seatsStr += ", " + segSeatDist.getSegmentCode() + "::";
			}
			
			Collection seats = segSeatDist.getEffectiveSeatDistribution();
			
			
			for (Iterator sIt = seats.iterator(); sIt.hasNext(); ){
				SeatDistribution seatDist = (SeatDistribution) sIt.next();
				SeatDistribution seatDistribution = new SeatDistribution();
				segSeatDistForCnf.addSeatDistribution(seatDistribution);
				
				seatDistribution.setBookingClassType(seatDist.getBookingClassType());
				seatDistribution.setBookingClassCode(seatDist.getBookingClassCode());
				seatDistribution.setNoOfSeats(seatDist.getNoOfSeats());
				
				if (seatsStr.endsWith("::")){
					seatsStr += seatDist.getBookingClassCode() + ":" + seatDist.getNoOfSeats();
				} else {
					seatsStr += "|" + seatDist.getBookingClassCode() + ":" + seatDist.getNoOfSeats();
				}
			}
		}
		System.out.println("["+id + "] CONFIRMING seats=" + seatsStr);
		return seatDistForConfirm;
	}

	private Collection prepareSeatDistForRelease(Collection segSeatDists) {
		Collection seatDistForRelease = new ArrayList();
		String seatsStr = "";
		for (Iterator it = segSeatDists.iterator(); it.hasNext(); ){
			SegmentSeatDistsDTO segSeatDist = (SegmentSeatDistsDTO) it.next();
			SegmentSeatDistsDTOForRelease segSeatDistForRel =new SegmentSeatDistsDTOForRelease();
			seatDistForRelease.add(segSeatDistForRel);
			segSeatDistForRel.setBookingClassType(segSeatDist.getBookingClassType());
			segSeatDistForRel.setCabinClassCode(segSeatDist.getCabinClassCode());
			segSeatDistForRel.setFlightSegId(segSeatDist.getFlightSegId());
			
			if (seatsStr.equals("")){
				seatsStr += segSeatDist.getSegmentCode() + "::";
			} else {
				seatsStr += ", " + segSeatDist.getSegmentCode() + "::";
			}
			
			Collection seats = segSeatDist.getEffectiveSeatDistribution();
			
			
			for (Iterator sIt = seats.iterator(); sIt.hasNext(); ){
				SeatDistribution seatDist = (SeatDistribution) sIt.next();
				SeatDistForRelease seatDistRel = new SeatDistForRelease();
				segSeatDistForRel.addSeatDistributionForRelease(seatDistRel);
				
				seatDistRel.setBookingClassType(seatDist.getBookingClassType());
				seatDistRel.setBookingCode(seatDist.getBookingClassCode());
				seatDistRel.setSoldSeats(seatDist.getNoOfSeats());
				
				if (seatsStr.endsWith("::")){
					seatsStr += seatDist.getBookingClassCode() + ":" + seatDist.getNoOfSeats();
				} else {
					seatsStr += "|" + seatDist.getBookingClassCode() + ":" + seatDist.getNoOfSeats();
				}
			}
		}
		System.out.println("["+id + "] RELEASING seats=" + seatsStr);
		return seatDistForRelease;
	}
	
	private boolean hasElement(int [] array, int element){
		if (array != null){
			for (int i=0; i < array.length; i++){
				if (array[i] == element){
					return true;
				}
			}
		}
		return false;
	}

	private FlightBD getFlightBD() {
		return (FlightBD) ReservationModuleUtils.lookupServiceBD(
							AirschedulesConstants.MODULE_NAME,
							AirschedulesConstants.BDKeys.FLIGHT_SERVICE);
	}
	
	private FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) ReservationModuleUtils.lookupServiceBD(
										AirinventoryConstants.MODULE_NAME,
										AirinventoryConstants.BDKeys.FLIGHTINVENTORYRES_SERVICE);
	}

}
