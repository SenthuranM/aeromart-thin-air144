/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on August 17, 2005
 * 
 * TestFlightInventoryMasterDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.FlightInventoryResDAO;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author MN
 *
 */
public class TestFlightInventoryResDAOImpl extends PlatformTestCase {
	
	protected void setUp(){
		
	}
	
	public void testFilterFlightsHavingReservations(){
		List flightIds = new ArrayList();
		flightIds.add(new Integer(9200));
		flightIds.add(new Integer(9201));
		flightIds.add(new Integer(9202));
//		List flightsWithRes = getFlightInventoryResDAO().filterFlightsHavingReservations(flightIds);
//		assertEquals(flightsWithRes.size(), 2);
	}
	
	
	private FlightInventoryResDAO getFlightInventoryResDAO(){
		return (FlightInventoryResDAO)AirInventoryUtil.getInstance().getLocalBean("FlightInventoryResDAOImplProxy");
	}
	
	protected void tearDown(){	
	}
}

