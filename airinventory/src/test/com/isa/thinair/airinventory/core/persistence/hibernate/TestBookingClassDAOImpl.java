package com.isa.thinair.airinventory.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.AirInventoryUtil;
import com.isa.thinair.airinventory.core.persistence.dao.BookingClassDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

import com.isa.thinair.platform.api.util.PlatformTestCase;


/**
 * 
 * @author MN
 *
 */
public class TestBookingClassDAOImpl extends PlatformTestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetBookingClass() {
		assertNotNull(getBookingClassDAO().getBookingClass("BS1"));
	}

	public void testGetBookingClassesIntIntList() {
		List list = new ArrayList();
		list.add("bookingCode");
		Page page = getBookingClassDAO().getBookingClasses(0, 3, list);
		assertEquals(3, page.getPageData().size());
	}

	public void testGetBookingClassesListIntIntList() {
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setFieldName("bookingCode");
		criterion.setCondition(ModuleCriterion.CONDITION_LIKE);
		List values = new ArrayList();
		values.add("BF%");
		criterion.setValue(values);
		List criteria = new ArrayList();
		criteria.add(criterion);
		List list = new ArrayList();
		list.add("bookingCode");
		Collection data = getBookingClassDAO().getBookingClasses(criteria, 0, 3, list).getPageData();
		assertNotNull(data);
		assertEquals(2,data.size());
	}
	
	public void testGetBookingClasses(){
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setFieldName("bookingCode");
		criterion.setCondition(ModuleCriterion.CONDITION_IN);
		List values = new ArrayList();
		values.add("BS1");
		values.add("BS2");
		criterion.setValue(values);
		List criteria = new ArrayList();
		criteria.add(criterion);
		Collection data = getBookingClassDAO().getBookingClasses(criteria);
		assertEquals(2, data.size());
	}
	
	public void testCreateBookingClass() throws ModuleException{
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		String bookingCode = "BSxx";
		bc.setBookingCode(bookingCode);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(250));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		assertNotNull(getBookingClassDAO().getBookingClass(bookingCode));
		getBookingClassDAO().deleteBookingClass(bookingCode);
	}
	
	public void testCreateBookingClass_GOSHOW() throws ModuleException{
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		String bookingCode = "BSxx";
		bc.setBookingCode(bookingCode);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.GOSHOW);
		bc.setNestRank(new Integer(250));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		
		try {		
			bc = new BookingClass();
			String bookingCode1 = "BSxy";
			bc.setBookingCode(bookingCode1);
			bc.setCabinClassCode("Y");
			bc.setStandardCode(true);
			bc.setPaxType(BookingClass.PassengerType.GOSHOW);
			bc.setNestRank(new Integer(251));
			bc.setFixedFlag(false);
			bc.setStatus(BookingClass.Status.ACTIVE);
			
			bcDTO.setShiftingNestRankAllowed(true);
			bcDTO.setBookingClass(bc);
			getBookingClassDAO().createBookingClass(bcDTO);
			fail("two goshow fares for the same cabinclass not allowed");
		} catch (Exception ex){
		}
		getBookingClassDAO().deleteBookingClass(bookingCode);
	}
	
	public void testNestRankShifting() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		String bookingCode1 = "BSxx";
		bc.setBookingCode(bookingCode1);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(250));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		
		bc = new BookingClass();
		String bookingCode2 = "BSxy";
		bc.setBookingCode(bookingCode2);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(251));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		
		bc = new BookingClass();
		String bookingCode3 = "BSxz";
		bc.setBookingCode(bookingCode3);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(250));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		
		assertEquals(new Integer(252), getBookingClassDAO().getBookingClass(bookingCode2).getNestRank());
		getBookingClassDAO().deleteBookingClass(bookingCode1);
		getBookingClassDAO().deleteBookingClass(bookingCode2);
		getBookingClassDAO().deleteBookingClass(bookingCode3);	
	}
	
	public void testUpdateBookingClass() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		String bookingCode = "BSxx";
		bc.setBookingCode(bookingCode);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(250));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		
		BookingClass bcLoaded = getBookingClassDAO().getBookingClass(bookingCode);
		bcLoaded.setCabinClassCode("I");
		bcLoaded.setNestRank(new Integer(251));
		bcLoaded.setStatus(BookingClass.Status.INACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bcLoaded);
		getBookingClassDAO().updateBookingClass(bcDTO);
		
		BookingClass bcUpdated = getBookingClassDAO().getBookingClass(bookingCode);
		assertEquals(new Integer(251), bcUpdated.getNestRank());
		
		getBookingClassDAO().deleteBookingClass(bookingCode);
	}
	
	public void testDeleteBookingClass() throws ModuleException {
		BookingClassDTO bcDTO = new BookingClassDTO();
		
		BookingClass bc = new BookingClass();
		String bookingCode = "BSxx";
		bc.setBookingCode(bookingCode);
		bc.setCabinClassCode("Y");
		bc.setStandardCode(true);
		bc.setPaxType(BookingClass.PassengerType.ADULT);
		bc.setNestRank(new Integer(250));
		bc.setFixedFlag(false);
		bc.setStatus(BookingClass.Status.ACTIVE);
		
		bcDTO.setShiftingNestRankAllowed(true);
		bcDTO.setBookingClass(bc);
		getBookingClassDAO().createBookingClass(bcDTO);
		
		getBookingClassDAO().deleteBookingClass(bookingCode);
		
		BookingClass bcDeleted = getBookingClassDAO().getBookingClass(bookingCode);
		assertNull(bcDeleted);
	}
	
	//temp
//	public void testTempGetCount(){
//		List criteria = new ArrayList();
//		ModuleCriterion criterion = new ModuleCriterion();
//		criterion.setFieldName("bookingCode");
//		criterion.setCondition(ModuleCriterion.CONDITION_IN);
//		List values = new ArrayList();
//		values.add("T1");
//		values.add("T2");
//		criterion.setValue(values);
//		
//		criteria.add(criterion);
		
//		ModuleCriterion criterion1 = new ModuleCriterion();
//		criterion1.setFieldName("nestRank");
//		criterion1.setCondition(ModuleCriterion.CONDITION_BETWEEN);
//		List values1 = new ArrayList();
//		values1.add(new Integer(1));
//		values1.add(new Integer(4));
//		criterion1.setValue(values1);
//		
//		criteria.add(criterion1);
		
//		ModuleCriterion criterion1 = new ModuleCriterion();
//		criterion1.setFieldName("createdDate");
//		criterion1.setCondition(ModuleCriterion.CONDITION_BETWEEN);
//		List values1 = new ArrayList();
//		
//		Calendar cal = Calendar.getInstance();
//		cal.set(2005, 9, 1);
//		
//		Calendar cal1 = Calendar.getInstance();
//		cal1.set(2005, 9, 3);
//		
//		values1.add(cal.getTime());
//		values1.add(cal1.getTime());
//		
//		criterion1.setValue(values1);
//		
//		criteria.add(criterion1);
//		
//		assertEquals(1,getBookingClassDAO().tempGetCount(criteria, BookingClass.class));
//	}
	
	private BookingClassDAO getBookingClassDAO(){
		return (BookingClassDAO)AirInventoryUtil.getInstance().getLocalBean("BookingClassDAOImplProxy");
	}
}
