/**
 * 
 */
package com.isa.thinair.airinventory.api.service;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.util.LookupUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.platform.api.util.BaseTestCase;

/**
 * @author rumesh
 * 
 */
public class FlightInventoryResBDTest extends BaseTestCase {

	private static FlightInventoryResBD flightInventoryResBD;

	@BeforeClass
	public static void initMethod() {
		System.out.println("init Started");
		BaseTestCase.initMethod();
		try {
			new ClientLoginModule().login(BaseTestCase.USER_NAME, BaseTestCase.PASSWORD, BaseTestCase.LOGIN_CONFIG_FILE, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		flightInventoryResBD = LookupUtil.getFlightInventoryResBD();
		System.out.println("init completed");
	}

	@Test
	public void getAvailableNonFixedAndFixedSeatsCountsTest() throws ModuleException {
		System.out.println("############Testing getAvailableNonFixedAndFixedSeatsCounts#############");
		int[] actual = flightInventoryResBD.getAvailableNonFixedAndFixedSeatsCounts(284609, "SHJ/CMB", "Y");
		int[] expected = { 150, 0, 10, 0, 0, 0, 0, 0, 0 };
		assertEquals("Seat count", expected.length, actual.length);
	}

	@Ignore
	@Test(expected = com.isa.thinair.commons.api.exception.ModuleException.class)
	public void searchSeatAvailabilityTest() throws ModuleException {
		System.out.println("#####################Testing searchSeatAvailability#####################");
		AvailableFlightDTO actual = flightInventoryResBD.searchSeatAvailability(new AvailableFlightDTO(),
				new AvailableFlightSearchDTO());
		AvailableFlightDTO expected = new AvailableFlightDTO();

		assertEquals("Available seat search", expected, actual);
	}
	
	

}
