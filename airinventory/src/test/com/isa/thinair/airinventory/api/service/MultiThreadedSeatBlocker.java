package com.isa.thinair.airinventory.api.service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.core.remoting.ejb.FlightInventoryResService;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.core.controller.ModuleFramework;


public class MultiThreadedSeatBlocker implements Runnable {
	
	FlightInventoryResService flightInventoryResService = null;
	Collection segmentSeatDistsDTOs = null;
	
	
	public MultiThreadedSeatBlocker(FlightInventoryResService flightInventoryResService, String bookingCode, int noOfSeats){
		this.flightInventoryResService = flightInventoryResService;
		this.segmentSeatDistsDTOs = getSegmentSeatDistsDTO(bookingCode, noOfSeats);
	}
	
    public static void main( String [] args ) throws RemoteException, CreateException, NamingException {
        ModuleFramework.startup();
        
        FlightInventoryResService flightInventoryResService1 = BeanLookupUtil.getFlightInventoryResBean("10.2.241.115");
        
        FlightInventoryResService flightInventoryResService2 = BeanLookupUtil.getFlightInventoryResBean("10.2.241.175");
        
        for (int i=0; i<200; ++i){
            new Thread(new MultiThreadedSeatBlocker(flightInventoryResService1, "S1", 1)).start();
            new Thread(new MultiThreadedSeatBlocker(flightInventoryResService1, "S2", 1)).start();
        }
    }
    
    public void run() {
        try { 
            flightInventoryResService.blockSeatsForRes(segmentSeatDistsDTOs);
        } catch (ModuleException e){
            e.printStackTrace();
        } catch (RemoteException e) {
			e.printStackTrace();
		}
    }
    
    private Collection getSegmentSeatDistsDTO(String bookingCode, int noOfSeats){
    	Collection segSeatsCol = new ArrayList();
        
        SegmentSeatDistsDTO segSeats = new SegmentSeatDistsDTO();
        segSeats.setCabinClassCode("Y");
        segSeats.setFlightId(27350);
        segSeats.setSegmentCode("JED/SHJ");
        segSeats.setFlightSegId(27774);

        SeatDistribution seatDist = new SeatDistribution();
        seatDist.setBookingClassCode(bookingCode);
        seatDist.setNoOfSeats(noOfSeats);
        
        segSeats.addSeatDistribution(seatDist);
        segSeats.setFixedQuotaSeats(false);
        
        segSeatsCol.add(segSeats);
        
        return segSeatsCol;
    }

}
