package com.isa.thinair.airinventory.api.service;

import java.rmi.RemoteException;
import java.util.Properties;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import com.isa.thinair.airinventory.core.remoting.ejb.FlightInventoryResService;
import com.isa.thinair.airinventory.core.remoting.ejb.FlightInventoryResServiceHome;
import com.isa.thinair.airinventory.core.remoting.ejb.FlightInventoryResServiceUtil;

public class BeanLookupUtil {
	
	public static FlightInventoryResService getFlightInventoryResBean(String ipAddress) 
						throws RemoteException, CreateException, NamingException {
		Properties props = new Properties();
		props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		props.put("java.naming.provider.url", "jnp://" + ipAddress + ":1099");
		return ((FlightInventoryResServiceHome)FlightInventoryResServiceUtil.getHome(props)).create();
	}
}
