--For an Agent that has
select C.fcca_id,s.fccsa_id, s.segment_code,  B.fccsba_id, bc.booking_code
from T_FCC_ALLOC C, T_FCC_SEG_ALLOC S, T_FCC_SEG_BC_ALLOC B,
    T_BOOKING_CLASS BC
where C.FLIGHT_ID in (9510,9501)
and C.fcca_id = S.fcca_id
and s.fccsa_id = b.fccsa_id
and b.booking_code = BC.booking_code
and BC.standard_code = 'Y'
and  BC.booking_code in ('XS1')
order by C.fcca_id,s.fccsa_id,s.segment_code, B.fccsba_id;


--All and BC availability > 110
select C.fcca_id,s.fccsa_id, s.segment_code, 
 B.fccsba_id, b.available_seats
from T_FCC_ALLOC C, T_FCC_SEG_ALLOC S, T_FCC_SEG_BC_ALLOC B
where C.FLIGHT_ID in (9510,9501)
and C.fcca_id = S.fcca_id
and s.fccsa_id = b.fccsa_id (+)
and b.available_seats > 110
order by C.fcca_id,s.fccsa_id,s.segment_code, B.fccsba_id;

--All and Segment availability > 110
select C.fcca_id,s.fccsa_id, s.segment_code, s.available_seats, B.fccsba_id
from T_FCC_ALLOC C, T_FCC_SEG_ALLOC S, T_FCC_SEG_BC_ALLOC B
where C.FLIGHT_ID in (9510,9501)
and C.fcca_id = S.fcca_id
and s.fccsa_id = b.fccsa_id (+)
and s.available_seats > 110
--and s.segment_code in ('SSH/LXR','SHJ/SSH')
order by C.fcca_id,s.fccsa_id,s.segment_code, B.fccsba_id;

--Selected Segment codes
select C.fcca_id,s.fccsa_id, s.segment_code,  B.fccsba_id
from T_FCC_ALLOC C, T_FCC_SEG_ALLOC S, T_FCC_SEG_BC_ALLOC B,
    T_BOOKING_CLASS BC
where C.FLIGHT_ID in (9510,9501)
and C.fcca_id = S.fcca_id
and s.fccsa_id = b.fccsa_id
and b.booking_code = BC.booking_code
--and BC.standard_code = 'Y'
and s.segment_code in ('SSH/LXR','SHJ/SSH')
order by C.fcca_id,s.fccsa_id,s.segment_code, B.fccsba_id;

--Standard BC
select C.fcca_id,s.fccsa_id, s.segment_code,  B.fccsba_id
from T_FCC_ALLOC C, T_FCC_SEG_ALLOC S, T_FCC_SEG_BC_ALLOC B,
    T_BOOKING_CLASS BC
where C.FLIGHT_ID in (9510,9501)
and C.fcca_id = S.fcca_id
and s.fccsa_id = b.fccsa_id
and b.booking_code = BC.booking_code
and BC.standard_code = 'Y'
--and s.segment_code in ('SSH/LXR','SHJ/SSH')
order by C.fcca_id,s.fccsa_id,s.segment_code, B.fccsba_id;


--All
select C.fcca_id,s.fccsa_id, s.segment_code,  B.fccsba_id
from T_FCC_ALLOC C, T_FCC_SEG_ALLOC S, T_FCC_SEG_BC_ALLOC B
where C.FLIGHT_ID in (9510,9501)
and C.fcca_id = S.fcca_id
and s.fccsa_id = b.fccsa_id (+)
--and s.segment_code in ('SSH/LXR','SHJ/SSH')
order by C.fcca_id,s.fccsa_id,s.segment_code, B.fccsba_id;

