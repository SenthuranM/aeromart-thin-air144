DECLARE
	booking_code		T_BOOKING_CLASS.BOOKING_CODE%TYPE;
	max_nest_rank		T_BOOKING_CLASS.NEST_RANK%TYPE;
	bc_allocation_type	T_BOOKING_CLASS.ALLOCATION_TYPE%TYPE;
	fare_amount		T_OND_FARE.FARE_AMOUNT%TYPE;
	ond_code		T_OND_FARE.OND_CODE%TYPE;
	fare_rule_id		T_OND_FARE.FARE_RULE_ID%TYPE;
	flight_id		T_FLIGHT.FLIGHT_ID%TYPE;
	segment_code		T_FLIGHT_SEGMENT.SEGMENT_CODE%TYPE;
	fccsa_id		T_FCC_SEG_ALLOC.FCCSA_ID%TYPE;
BEGIN
	SELECT MAX(NVL(NEST_RANK,0)) INTO max_nest_rank FROM T_BOOKING_CLASS;
	SELECT FARE_RULE_ID INTO fare_rule_id FROM T_FARE_RULE WHERE MASTER_FARE_RULE_CODE = 'YOW';
	DBMS_OUTPUT.ENABLE(20000);
	
	dbms_output.put_line('max nest rank='||max_nest_rank||' fare rule id=' || fare_rule_id);

	FOR i IN 1..100 LOOP
		-- define booking class
		booking_code := 'LTS' || i;
		bc_allocation_type := 'SEG';
		IF ((MOD(i,3) = 0 OR MOD(i,4) = 0) AND i <> 0) THEN
			booking_code := 'LTC' || i;
			bc_allocation_type := 'CON';
		END IF; 

		INSERT INTO T_BOOKING_CLASS
		(BOOKING_CODE,BOOKING_CODE_DESCRIPTION,CABIN_CLASS_CODE,STANDARD_CODE,
		NEST_RANK,REMARKS,STATUS,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,
		VERSION,FIXED_FLAG,PAX_TYPE,BC_TYPE,ALLOCATION_TYPE)
		VALUES
		(booking_code, 'LD TEST STD BC ' || i, 'Y', 'Y',
		max_nest_rank + i, 'BC GEN FOR LOAD TEST', 'ACT', 'SYSTEM', SYSDATE, NULL, NULL,
		0,'N', 'ADULT','NORMAL', bc_allocation_type);
		
		INSERT INTO T_BC_CHARGE_GROUP (BOOKING_CODE, CHARGE_GROUP_CODE) VALUES
		(booking_code, 'TAX');
		
		INSERT INTO T_BC_CHARGE_GROUP (BOOKING_CODE, CHARGE_GROUP_CODE) VALUES
		(booking_code, 'SUR');

		-- define fare
		fare_amount := 100 * i;
		ond_code := 'CMB/SHJ';
		segment_code := 'CMB/SHJ';
		IF ((MOD(i,3) = 0 OR MOD(i,4) = 0) AND i <> 0) THEN
			fare_amount := 125 * i;
			ond_code := 'CMB/SHJ/BAH';
			segment_code := 'SHJ/BAH';
		END IF;

		INSERT INTO T_OND_FARE 
		(FARE_ID, EFFECTIVE_FROM_DATE, EFFECTIVE_TO_DATE, FARE_AMOUNT,
		STATUS, OND_CODE, VERSION, FARE_RULE_ID, BOOKING_CODE)
		VALUES
		(S_OND_FARE.NEXTVAL, TO_DATE('01-JAN-2007 00:00', 'DD-MON-YYYY HH24:MI'), 
		TO_DATE('31-JAN-2007 23:59', 'DD-MON-YYYY HH24:MI'), fare_amount,
		'ACT', ond_code, 0, fare_rule_id, booking_code);

		dbms_output.put_line('FARE :: booking_code='||booking_code||',ond_code='||ond_code||',fare_amount='||fare_amount);
		-- allocate inventory

		FOR j IN 0..1 LOOP
			segment_code:='CMB/SHJ';
			flight_id :=109254;
			fccsa_id:=59475;

			IF j=1 THEN
				segment_code:='SHJ/BAH';
				flight_id :=109285;
				fccsa_id:=59506;
			END IF;

			INSERT INTO T_FCC_SEG_BC_ALLOC
			(FCCSBA_ID, FCCSA_ID, FLIGHT_ID,SEGMENT_CODE, CABIN_CLASS_CODE,
			BOOKING_CODE, ALLOCATED_SEATS, SOLD_SEATS, AVAILABLE_SEATS,
			STATUS, PRIORITY_FLAG, VERSION, CANCELLED_SEATS, ACQUIRED_SEATS,
			ONHOLD_SEATS, MANUALLY_CLOSED_FLAG, STATUS_CHG_ACTION)
			VALUES
			(S_FCC_SEG_BC_ALLOC_FCCSBA_ID.NEXTVAL, fccsa_id, flight_id, segment_code, 'Y',
			booking_code, 10, 0, 10, 'OPN', 'N', 0, 0, 0,
			0, 'N', 'C');

			dbms_output.put_line('INVENTORY :: booking_code='||booking_code||',segment_code='||segment_code);
		END LOOP;
	END LOOP;
END;
/

delete T_FCC_SEG_BC_NESTING where from_fccsba_id in (select fccsba_id from t_fcc_seg_bc_alloc where fccsa_id in (59475,59506));

delete t_fcc_seg_bc_alloc where fccsa_id in (59475,59506);

update t_fcc_seg_alloc set allocated_seats=900, sold_seats=0, on_hold_seats=0, available_seats=900 where
fccsa_id in (59475,59506);

delete from t_ond_fare where booking_code like 'LT%' 
and ond_code in ('CMB/SHJ', 'CMB/SHJ/BAH') 
and trunc(effective_from_date)='01-jan-2007' 
and trunc(effective_to_date)='31-jan-2007';

delete from t_bc_charge_group where booking_code in (
select booking_code from t_booking_class where booking_code like 'LT%' and remarks like 'BC GEN FOR LOAD TEST');

delete from t_booking_class where booking_code like 'LT%' and remarks like 'BC GEN FOR LOAD TEST';
