/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.research.core.persistence.dao;

import com.isa.thinair.research.api.model.ResearchOjbect;

/**
 * @author Nasly
 * 
 */
public interface IResearchDAO {
	public void saveOrUpdate(ResearchOjbect obj);

	public void delete(String primaryKey);
}
