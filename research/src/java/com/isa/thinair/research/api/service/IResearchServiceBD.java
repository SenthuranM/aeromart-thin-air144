/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.research.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.research.api.model.ResearchOjbect;

/**
 * @author Nasly
 * 
 */
public interface IResearchServiceBD extends IServiceDelegate {
	public void saveOrUpdate(ResearchOjbect obj) throws ModuleException;

	public void delete(String key) throws ModuleException;
}
