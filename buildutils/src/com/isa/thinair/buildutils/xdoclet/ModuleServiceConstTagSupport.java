/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 5, 2005
 * 
 * ===============================================================================
 */
package com.isa.thinair.buildutils.xdoclet;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import xdoclet.XDocletTagSupport;
import xjavadoc.XClass;
import xjavadoc.XDoc;
import xjavadoc.XTag;

/**
 * Class to define custom xdoclet tags to generate the module service interface specific constants
 * 
 * @author Lasantha Pambagoda
 */
public class ModuleServiceConstTagSupport extends XDocletTagSupport {

	/**
	 * returns the constant name for the module name
	 * 
	 * @param props
	 * @return constant name
	 */
	public String moduleNameConst(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_SERVICE_INTERFACE);

		return tag.getAttributeValue(MOSULE_NAME).toUpperCase();
	}

	/**
	 * returns the package name of the module
	 * 
	 * @param props
	 * @return module package name
	 */
	public String modulePackageName(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_SERVICE_INTERFACE);

		return tag.getAttributeValue(MOSULE_NAME).toLowerCase();
	}

	/**
	 * returns the module constant class name
	 * 
	 * @param props
	 * @return module constant class name
	 */
	public String moduleConstClassName(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_SERVICE_INTERFACE);

		String moduleName = upperCaseFirstLetter(tag.getAttributeValue(MOSULE_NAME)) + SUFFIX_MODULE_CONSTANT;

		return moduleName;
	}

	/**
	 * returns the command constant Name
	 * 
	 * @param props
	 * @return
	 */
	public String commandNameConst(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_COMMAND);

		String commandName = tag.getAttributeValue(NAME);

		commandName = getStaticConstantName(commandName).toUpperCase();

		return commandName;
	}

	/**
	 * returns the macro command constant Name
	 * 
	 * @param props
	 * @return
	 */
	public String macroCommandNameConst(Properties props) {

		XDoc doc = getCurrentField().getDoc();
		XTag tag = doc.getTag(MODULE_MACRO_COMMAND);

		String commandName = tag.getAttributeValue(NAME);

		commandName = getStaticConstantName(commandName).toUpperCase();

		return commandName;
	}

	/**
	 * returns the name of the module
	 * 
	 * @param props
	 * @return module name
	 */
	public String moduleName(Properties props) {
		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_SERVICE_INTERFACE);
		return tag.getAttributeValue(MOSULE_NAME);
	}

	/**
	 * returns the key name constant of the module BD
	 * 
	 * @param props
	 * @return bd key constant name
	 */
	public String moduleFullBDKeyNameConst(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_BD_IMPL);

		String rowModuleBDName = tag.getAttributeValue(MODULE_BD_ID);
		String moduleBDName = rowModuleBDName.toUpperCase().replace(FST, UNDCR);

		return moduleBDName;
	}

	/**
	 * returns the key name constant of the module BD
	 * 
	 * @param props
	 * @return bd key constant name
	 */
	public String moduleBDKeyNameConst(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_BD_INTF);

		String rowModuleBDName = tag.getAttributeValue(MODULE_BD_ID);
		String moduleBDName = rowModuleBDName.toUpperCase().replace(FST, UNDCR);

		return moduleBDName;
	}

	/**
	 * returns the keys of the module BDs
	 * 
	 * @param props
	 * @return moduel bd keys
	 */
	public String moduleFullBDKey(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_BD_IMPL);

		return tag.getAttributeValue(MODULE_BD_ID);
	}

	/**
	 * returns the keys of the module BDs
	 * 
	 * @param props
	 * @return moduel bd keys
	 */
	public String moduleBDKey(Properties props) {

		XDoc doc = getCurrentClass().getDoc();
		XTag tag = doc.getTag(MODULE_BD_INTF);

		return tag.getAttributeValue(MODULE_BD_ID);
	}

	/**
	 * list of interfaces implemented by this class
	 * 
	 * @param prop
	 * @return list of interfaces implemented by this class
	 */
	public String daoInterfaces(Properties prop) {

		List list = getCurrentClass().getInterfaces();
		Iterator it = list.iterator();

		boolean first = true;
		String interList = "";

		while (it.hasNext()) {
			XClass xclass = (XClass) it.next();
			String interName = xclass.getQualifiedName();
			if (!interName.equals(INTERFACE_NOT_TO_INCLUDE)) {
				interList = first ? interName : (interList + "," + interName);
				first = false;
			}
		}
		return interList;
	}

	/**
	 * private method to get a first letter uppercased string from given string
	 * 
	 * @param str
	 * @return string
	 */
	private String upperCaseFirstLetter(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	private String getStaticConstantName(String str) {
		return str.replaceAll("A", "_A").replaceAll("B", "_B").replaceAll("C", "_C").replaceAll("D", "_D").replaceAll("E", "_E")
				.replaceAll("F", "_F").replaceAll("G", "_G").replaceAll("H", "_H").replaceAll("I", "_I").replaceAll("J", "_J")
				.replaceAll("K", "_K").replaceAll("L", "_L").replaceAll("M", "_M").replaceAll("N", "_N").replaceAll("O", "_O")
				.replaceAll("P", "_P").replaceAll("Q", "_Q").replaceAll("R", "_R").replaceAll("S", "_S").replaceAll("T", "_T")
				.replaceAll("U", "_U").replaceAll("V", "_V").replaceAll("W", "_W").replaceAll("X", "_X").replaceAll("Y", "_Y")
				.replaceAll("Z", "_Z");
	}

	// constants for suffixes
	/** constant to add to the module name to get the modules constant class name */
	private static final String SUFFIX_MODULE_CONSTANT = "Constants";

	// xdoclet attribute names and tag nemes used
	/** tag neme of the module service interface */
	private static final String MODULE_SERVICE_INTERFACE = "isa.module.service-interface";

	/** tag neme of the module command */
	private static final String MODULE_COMMAND = "isa.module.command";

	/** tag neme of the module command */
	private static final String MODULE_MACRO_COMMAND = "isa.module.command.macro-command";

	/** attribute name of the module service interface tag, to specify the module name */
	private static final String MOSULE_NAME = "module-name";

	/** attribute name of the isa.module.command tag, to specify the command name */
	private static final String NAME = "name";

	/** tag neme of the module bussines deligates */
	private static final String MODULE_BD_INTF = "isa.module.bd-intf";

	/** tag neme of the module bussines deligates */
	private static final String MODULE_BD_IMPL = "isa.module.bd-impl";

	/** attribute name of the module bussines deligates tag, to specify the deligate key */
	private static final String MODULE_BD_ID = "id";

	private static final String INTERFACE_NOT_TO_INCLUDE = "org.springframework.beans.factory.InitializingBean";

	// rest
	/** fullstop */
	private static final char FST = '.';

	/** undescore */
	private static final char UNDCR = '_';
}
