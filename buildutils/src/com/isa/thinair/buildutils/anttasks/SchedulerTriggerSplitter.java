package com.isa.thinair.buildutils.anttasks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class SchedulerTriggerSplitter extends Task {
	private String excludes;
	private String includes;
	private String name = "xxscheduler";
	private String file = "";

	public void execute() {
		log("xxxx");
		log(file);
		log(excludes);
		log(includes);
		log(name);
		log("xxxx");
		if (notEmpty(file)) {
			List<String> excludedList = new ArrayList<>();
			List<String> includeList = new ArrayList<>();
			if (notEmpty(includes)) {
				includeList = split(includes);
			}

			if (notEmpty(excludes)) {
				excludedList = split(excludes);
			}
			if (excludedList.size() > 0 || includeList.size() > 0) {
				log("yyy");
				log(excludedList.toString());
				log(includeList.toString());
				log("yyy");
				try {
					List<String> list = read(file, includeList, excludedList);
					write(file, list);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new BuildException("Invalid file path:" + file + ". Scheduler trigger split failed");
				}

			} else {
				log("Nothing to do");
			}
		} else {
			log("File path not defined");
		}
	}

	private void write(String fileName, List<String> outLines) throws Exception {
		FileWriter fwr = new FileWriter(new File(fileName));
		BufferedWriter brw = new BufferedWriter(fwr);
		PrintWriter prw = new PrintWriter(brw);
		for (String key : outLines) {
			prw.println(key);

		}
		prw.close();
	}

	private List<String> read(String fileName, List<String> includes, List<String> excludes) throws Exception {
		File f = new File(fileName);

		FileReader fr = new FileReader(f);

		BufferedReader br = new BufferedReader(fr);

		String strLine = null;
		List<String> outLines = new ArrayList<>();
		boolean insideTriggers = false;
		while ((strLine = br.readLine()) != null) {
			if (notEmpty(strLine)) {
				// System.out.println(strLine);
				if (insideTriggers) {
					if (strLine.indexOf("ref") != -1 && strLine.indexOf("bean") != -1) {
						String[] arr = strLine.trim().split("\"");
						if (arr.length > 1) {
							if ((includes.size() > 0 && !includes.contains(arr[1].trim()))
									|| (excludes.size() > 0 && excludes.contains(arr[1].trim()))) {
								strLine = null;
							}
						}
					}
					if (strLine != null && strLine.indexOf("<!--") != -1) {
						strLine = null;
					}
				}
				if (strLine != null) {
					if (strLine.indexOf("xxscheduler") != -1) {
						strLine = strLine.replace("xxscheduler", name);
					} else if (strLine.indexOf("triggers") != -1) {
						insideTriggers = true;
					}

					outLines.add(strLine);
				}

			} else {
				outLines.add(strLine);
			}

		}
		br.close();

		return outLines;
	}

	private List<String> split(String text) {
		List<String> list = new ArrayList<>();
		for (String x : text.trim().split(",")) {
			list.add(x.trim());
		}
		return list;
	}

	private boolean notEmpty(String text) {
		return text != null && !"".equals(text.trim());
	}

	public String getExcludes() {
		return excludes;
	}

	public void setExcludes(String excludes) {
		this.excludes = excludes;
	}

	public String getIncludes() {
		return includes;
	}

	public void setIncludes(String includes) {
		this.includes = includes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

}
