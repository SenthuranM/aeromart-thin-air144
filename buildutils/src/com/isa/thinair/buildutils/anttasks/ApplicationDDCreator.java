/*
 * Created on Jun 26, 2005
 *
 */
package com.isa.thinair.buildutils.anttasks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
/**
 * @author nasly
 *
 */
public class ApplicationDDCreator extends Task {
	StringBuffer buffer = new StringBuffer();
    private Vector filesets = new Vector();
    private String description;
    private File baseDir;
    private String verbose;
    private String contextRootMappings;
    private String contextUrlMappings;

    public void addFileset(FileSet fileset) {
        filesets.add(fileset);
    }
		
	public void setDescription(String description) {
		this.description = description;
	}

	public void setVerbose(String verbose) {
		this.verbose = verbose;
	}

	public String getContextRootMappings() {
		return contextRootMappings;
	}

	public void setContextRootMappings(String contextRootMappings) {
		this.contextRootMappings = contextRootMappings;
	}
	
	public String getContextUrlMappings() {
		return contextUrlMappings;
	}

	public void setContextUrlMappings(String contextUrlMappings) {
		this.contextUrlMappings = contextUrlMappings;
	}

	public void execute() {
    	log("application.xml creation started...");
		validate();
		addHeader();
		addDescription();
		Iterator itFSets = filesets.iterator();
		FileSet fs = (FileSet) itFSets.next();
		DirectoryScanner ds = fs.getDirectoryScanner(getProject());
		String[] includedFiles = ds.getIncludedFiles();
		baseDir = ds.getBasedir();
		for (int i = 0; i < includedFiles.length; i++) {
			String filename = includedFiles[i].replace('\\', '/'); 
			filename = filename.substring(filename.lastIndexOf("/") + 1);
			System.out.println("filename =" + filename);
			if (filename.endsWith(".jar") && filename.startsWith("ejb-")) {
				if (getProject().getProperty("verbose") != null && getProject().getProperty("verbose").equals("true"))
					log("adding ejb archive " + filename
							+ " to the application.xml");
				addEJB(filename);
				//                    File base = ds.getBasedir(); // 5
				//                    File found = new File(base, includedFiles[i]);
				//                    foundLocation = found.getAbsolutePath();
			} else if (filename.endsWith(".war")) {
				if (getProject().getProperty("verbose") != null && getProject().getProperty("verbose").equals("true"))
					log("adding ejb archive " + filename + " to the application.xml");
				addWar(filename);
			}
		}

		addTail();
		try {
			BufferedWriter bwriter = new BufferedWriter(new FileWriter(
					new File(baseDir, "application.xml")));
			bwriter.write(buffer.toString());
			System.out.println(buffer.toString());
			bwriter.flush();
			bwriter.close();
		} catch (IOException e) {
			throw new BuildException("Application DD creation failed ", e);
		}
	}


	protected void validate() {
		if (filesets.size()<1) throw new BuildException("No element in fileset");
	}

	private void addWar(String warFileName){
		String contextRoot = "";
		String urlAppender = "";
		int dotPos = warFileName.indexOf(".");
		contextRoot = warFileName.substring(0, dotPos);
		
		if (getContextRootMappings() != null && !"".equals(getContextRootMappings())){
			String [] mappings = StringUtils.split(getContextRootMappings(), ";");
			for (int i = 0; i < mappings.length; i++){
				if (mappings[i].startsWith(contextRoot)){
					contextRoot = mappings[i].substring(mappings[i].indexOf(":")+1);
					break;
				}
			}
		}
		
		if(getContextUrlMappings() != null && !"".equals(getContextUrlMappings())){
			urlAppender = getContextUrlMappings();
		}
			
		buffer.append("\t<module><web>" +"\n" +
                       "\t\t"+ "<web-uri>"+ warFileName +"</web-uri>" +"\n"+
                       "\t\t"+ "<context-root>"+urlAppender+contextRoot+"</context-root>" +"\n" +
					   "\t</web></module>\n");
	}
	
	private void addEJB(String fileName){
		buffer.append("\t<module><ejb>"+fileName+"</ejb></module>\n");
	}
	
	private void addDescription(){
		String desc = getProject().getProperty("description");
		if (desc != null){
			buffer.append("\t<description>"+desc+"</description>\n");
		} else {
			buffer.append("\t<description>(C)Copyright ISA. All Rights Reserved.</description>\n");
		}
	}
	
	private void addHeader(){
		String desc = getProject().getProperty("description");
		buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
				"<!DOCTYPE application PUBLIC \"-//Sun Microsystems, Inc.//DTD J2EE Application 1.2//EN\" \"http://java.sun.com/j2ee/dtds/application_1_2.dtd\">\n"+
				"<application>\n"+
				"\t<display-name>"+desc+"</display-name>\n");
	}
	
	private void addTail(){
		buffer.append("\t</application>");
	}
}
