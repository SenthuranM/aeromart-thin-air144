package com.isa.thinair.buildutils.anttasks;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Expand;

/**
 * @author Mohamed Nasly Custom Ant Task for exploding an EAR
 */
public class Explode extends Task {

	private String src;

	private String dest;

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public void execute() throws BuildException {
		File srcFile = new File(getSrc());
		if (!srcFile.exists() || !srcFile.isFile()) {
			System.out.println("src file [" + getSrc() + "] does not exist");
			throw new BuildException("src file [" + getSrc() + "] does not exist");
		}
		File destDir = new File(getDest());
		if (!destDir.exists() || !destDir.isDirectory()) {
			System.out.println("dest dir [" + getSrc() + "] does not exist");
			throw new BuildException("dest dir [" + getSrc() + "] does not exist");
		}
		callUnzipTask(srcFile, destDir, this.getProject());

		File[] wars = destDir.listFiles(new WarFileFilter());
		if (wars != null) {
			for (int i = 0; i < wars.length; i++) {
				File war = wars[i];
				String absPath = war.getAbsolutePath();
				File toDir = new File(absPath.substring(0, absPath.length() - 4));
				callUnzipTask(war, toDir, this.getProject());
				war.delete();
				toDir.renameTo(new File(absPath));
			}
		}

	}

	private void callUnzipTask(File srcFile, File targetDir, Project project) {
		Expand expand = new Expand();
		expand.setProject(project);
		expand.setTaskName("unzip");
		expand.setTaskType("unzip");
		expand.setSrc(srcFile);
		expand.setDest(targetDir);

		System.out.println("expanding " + srcFile.getAbsolutePath());
		expand.execute();
	}

	private static class WarFileFilter implements FilenameFilter {

		private static final String ext = ".war";

		public WarFileFilter() {
		}

		public boolean accept(File dir, String name) {
			if (name != null && name.endsWith(ext)) {
				return true;
			}
			return false;
		}
	}

}
