package com.isa.thinair.buildutils.anttasks;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.Sequential;
import org.apache.tools.ant.taskdefs.condition.Condition;
import org.apache.tools.ant.taskdefs.condition.ConditionBase;

/**
 * @author Nilindra Fernando
 */
public class IfTask extends ConditionBase {
	private Sequential thenTasks = null;
	private Sequential elseTasks = null;

	/**
	 * A nested &lt;then&gt; element - a container of tasks that will be run if the condition holds true.
	 * 
	 * <p>
	 * Not required.
	 * </p>
	 */
	public void addThen(Sequential t) {
		if (thenTasks != null) {
			throw new BuildException("You must not nest more than one <then> into <if>");
		}
		thenTasks = t;
	}

	/**
	 * A nested &lt;else&gt; element - a container of tasks that will be run if the condition doesn't hold true.
	 * 
	 * <p>
	 * Not required.
	 * </p>
	 */
	public void addElse(Sequential e) {
		if (elseTasks != null) {
			throw new BuildException("You must not nest more than one <else> into <if>");
		}
		elseTasks = e;
	}

	public void execute() throws BuildException {
		if (countConditions() > 1) {
			throw new BuildException("You must not nest more than one condition into <if>");
		}
		if (countConditions() < 1) {
			throw new BuildException("You must nest a condition into <if>");
		}
		Condition c = (Condition) getConditions().nextElement();
		if (c.eval()) {
			if (thenTasks != null) {
				thenTasks.perform();
			}
		} else {
			if (elseTasks != null) {
				elseTasks.perform();
			}
		}
	}
}
