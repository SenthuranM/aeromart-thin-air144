/*
 * Created on Jun 26, 2005
 *
 */
package com.isa.thinair.buildutils.anttasks;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.Manifest;
import org.apache.tools.ant.taskdefs.ManifestException;
import org.apache.tools.ant.taskdefs.ManifestTask;

/**
 * @author nasly
 * 
 */

public class ManifestCreator extends ManifestTask {
	StringBuffer classpathBuffer = new StringBuffer();
	private File libDir;
	private File modulesDir;

	public File getLibDir() {
		return libDir;
	}

	public File getModulesDir() {
		return modulesDir;
	}

	public void setLibDir(File libDir) {
		this.libDir = libDir;
	}

	public void setModulesDir(File modulesDir) {
		this.modulesDir = modulesDir;
	}

	public void execute() throws BuildException {
		// build the classpath
		classpathBuffer.append(". ");

		File[] files = getLibDir().listFiles(new JarFileFilter());
		String prefix = " ../" + libDir.getName() + "/";
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			classpathBuffer.append(prefix + file.getName());
		}

		File[] moduleJars = getModulesDir().listFiles(new JarFileFilter());
		prefix = " ./";
		for (int i = 0; i < moduleJars.length; i++) {
			File file = moduleJars[i];
			classpathBuffer.append(prefix + file.getName());
		}

		try {
			addConfiguredAttribute(new Manifest.Attribute(Manifest.ATTRIBUTE_CLASSPATH, classpathBuffer.toString()));
		} catch (ManifestException ex) {
			log("exception in setting classpath in manifest");
		}
		super.execute();
	}

	private static class JarFileFilter implements FilenameFilter {

		private static final String FILE_EXT = ".jar";

		public JarFileFilter() {
		}

		public boolean accept(File dir, String name) {
			if (name != null && name.endsWith(FILE_EXT)) {
				return true;
			}
			return false;
		}
	}
}
