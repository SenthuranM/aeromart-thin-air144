package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.aircustomer.api.constants.CustomAliasConstants;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerAliasDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author rahul
 * @isa.module.dao-impl dao-name="customerAliasDAO"
 */
public class CustomerAliasDAOImpl extends PlatformHibernateDaoSupport implements CustomerAliasDAO {

	/** Default Constructor */
	public CustomerAliasDAOImpl() {
		super();
	}

	@Override
	public void saveOrUpdate(CustomerAlias customerAlias) {
		super.hibernateSaveOrUpdate(customerAlias);

	}

	@Override
	public void save(CustomerAlias customerAlias) {
		super.hibernateSave(customerAlias);

	}

	@Override
	public List<CustomerAlias> getCustomerAliasList(long customerId) {

		List<CustomerAlias> customerAliasList = null;

		String hql = "select customerAlias from CustomerAlias as customerAlias where customerAlias.status=? and customerAlias.customerId=? order by customerAlias.customerId";
		Object[] parmas = { CustomAliasConstants.ALIAS_ACTIVE, customerId };
		customerAliasList = find(hql, parmas, CustomerAlias.class);

		return customerAliasList;
	}

	@Override
	public CustomerAlias getCustomerAliasByCustomerAliasId(long customerAliasId, long customerId) {
		List<CustomerAlias> list = null;
		CustomerAlias customerAlias = null;

		String hql = "from CustomerAlias as customerAlias where customerAlias.customerAliasId=? and customerAlias.customerId=?";
		Object[] parmas = { customerAliasId, customerId };
		list = find(hql, parmas, CustomerAlias.class);

		if (list.size() != 0) {
			customerAlias = (CustomerAlias) list.get(0);
		}
		return customerAlias;
	}

	@Override
	public void removeCustomerAlias(String customerAliasId) {

	}
}
