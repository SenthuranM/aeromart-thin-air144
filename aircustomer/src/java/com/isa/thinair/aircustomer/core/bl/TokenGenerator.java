package com.isa.thinair.aircustomer.core.bl;

public interface TokenGenerator {
	String generateToken();
}
