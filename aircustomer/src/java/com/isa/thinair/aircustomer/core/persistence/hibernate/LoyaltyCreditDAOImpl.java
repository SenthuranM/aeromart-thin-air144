/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.LoyaltyCredit;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCreditDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author Chamindap
 * @isa.module.dao-impl dao-name="loyaltyCreditDAO"
 */
public class LoyaltyCreditDAOImpl extends PlatformHibernateDaoSupport implements LoyaltyCreditDAO {

	/**
	 * Default constructure
	 */
	public LoyaltyCreditDAOImpl() {
		super();
	}

	/**
	 * @see com.isa.thinair.customer.core.persistence.dao.CustomerDAO#
	 *      saveCustomer(com.isa.thinair.customer.api.model.Customer)
	 */
	public void saveOrUpdate(LoyaltyCredit credit) {
		super.hibernateSaveOrUpdate(credit);
	}

	public BigDecimal getTotalNonExpireCredit(String accountNo) {
		Object[] param = { accountNo, new Date() };

		List<BigDecimal> l = find(
				"SELECT SUM(c.creditBalance) from LoyaltyCredit c, LoyaltyCustomerProfile lcp " + " WHERE c.loyaltyAccountNo= ? "
						+ "AND lcp.loyaltyAccountNo =  c.loyaltyAccountNo " + "AND lcp.status = 'O' " + "AND c.dateExp > ?",
				param, BigDecimal.class);
		if (l != null && l.size() > 0)
			return l.get(0);
		return null;
	}

	public List<LoyaltyCredit> getNonExpireCredit(String accountNo) {
		Object[] param = { accountNo, new Date() };

		List<LoyaltyCredit> l = find(
				"Select c FROM LoyaltyCredit c, LoyaltyCustomerProfile lcp " + " WHERE c.creditBalance > 0 "
						+ " AND c.loyaltyAccountNo= ?" + " AND lcp.loyaltyAccountNo =  c.loyaltyAccountNo "
						+ " AND lcp.status = 'O' " + " AND c.dateExp> ? ORDER BY c.dateEarn ASC", param, LoyaltyCredit.class);
		return l;
	}

}
