/**
 * 
 */
package com.isa.thinair.aircustomer.core.bl;

import com.isa.thinair.aircustomer.api.dto.external.AgentFrequentCustomerDTO;
import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author suneth
 *
 */
public interface AgentFrequentCustomerBL {
	
	public void saveUpdateAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId, String operation)  throws ModuleException;

	public Page searchAgentFrequentCustomers(String firstName , String lastName , String email, String agentCode) throws ModuleException;
	
	public void deleteAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId) throws ModuleException;
	
	public Audit generateAudit(AgentFrequentCustomerDTO customerOldValues, AgentFrequentCustomerDTO customerNewValues, String userId);
	
}
