package com.isa.thinair.aircustomer.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;

/**
 * @author chethiya
 *
 */
@Remote
public interface LmsMemberServiceDelegateImpl extends LmsMemberServiceBD{

}
