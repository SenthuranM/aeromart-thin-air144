/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBDExternal;

/**
 * @author Chamindap
 */
@Remote
public interface AirCustomerServiceDelegateImpl extends AirCustomerServiceBD, AirCustomerServiceBDExternal {
}