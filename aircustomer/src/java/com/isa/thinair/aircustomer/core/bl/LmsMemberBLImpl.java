package com.isa.thinair.aircustomer.core.bl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlParameter;

import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.model.LmsMemberMerge;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.config.AirCustomerConfig;
import com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO;
import com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberMergeDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reporting.core.persistent.util.StoredProcedureCall;

/**
 * @author chethiya
 *
 */
public class LmsMemberBLImpl implements LmsMemberBL {

	private static Log log = LogFactory.getLog(LmsMemberBLImpl.class);

	private final static char YES = 'Y';

	private final static char NO = 'N';

	public static final String MERGE_TOPIC = "lms_member_merge";

	public static final String CONFIRM_TOPIC = "lms_member_confirmation";

	/** The CustomerDAO */
	private LmsMemberDAO lmsMemberDAO = null;

	private LmsMemberMergeDAO lmsMemberMergeDAO = null;

	public LmsMemberBLImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#getLmsMembers()
	 */
	@Override
	public List<LmsMember> getLmsMembers() throws ModuleException {
		List<LmsMember> lmsMembers = null;

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}

		lmsMembers = lmsMemberDAO.getLmsMembers();

		return lmsMembers;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#setPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public void setPassword(String password, String emailId) throws ModuleException {

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}

		lmsMemberDAO.setPassword(password, emailId);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#getLmsMember(java.lang.String)
	 */
	@Override
	public LmsMember getLmsMember(String emailId) throws ModuleException {

		LmsMember lmsMember = null;

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}

		lmsMember = lmsMemberDAO.getLmsMember(emailId);
		return lmsMember;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#getLmsMemberBySBId(int)
	 */
	@Override
	public LmsMember getLmsMemberBySBId(int sbInternalId) throws ModuleException {

		LmsMember lmsMember = null;

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}

		lmsMember = lmsMemberDAO.getLmsMemberBySBId(sbInternalId);
		return lmsMember;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#mergeLmsMember(com.isa.thinair.aircustomer.api.model.LmsMember,
	 *      java.lang.String)
	 */
	@Override
	public int mergeLmsMember(String verificationString, String emailId) throws ModuleException {

		if (lmsMemberMergeDAO == null) {
			lmsMemberMergeDAO = (LmsMemberMergeDAO) AirCustomerModuleUtils.getDAO("lmsMemberMergeDAO");
		}

		return lmsMemberMergeDAO.deleteRecord(verificationString, emailId);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#numberOfMerges(java.lang.String, java.lang.String)
	 */
	@Override
	public int numberOfMerges(String verificationString, String emailId) throws ModuleException {

		if (lmsMemberMergeDAO == null) {
			lmsMemberMergeDAO = (LmsMemberMergeDAO) AirCustomerModuleUtils.getDAO("lmsMemberMergeDAO");
		}

		return lmsMemberMergeDAO.numberOfMerges(verificationString, emailId);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#confirmMergeLms(java.lang.String, java.lang.String, String,
	 *      Locale)
	 */
	@Override
	public boolean confirmMergeLms(LmsMember lmsMember, String emailId, String carrierCode, Locale locale, boolean isServiceAppRQ)
			throws ModuleException {

		LmsMemberMerge lmsMemberMerge = new LmsMemberMerge();

		String verificationString = UUID.randomUUID().toString();
		verificationString.replace("-", "");

		lmsMemberMerge.setEmailId(emailId);
		lmsMemberMerge.setVerificationString(verificationString);

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}
		if (lmsMemberMergeDAO == null) {
			lmsMemberMergeDAO = (LmsMemberMergeDAO) AirCustomerModuleUtils.getDAO("lmsMemberMergeDAO");
		}

		boolean mergeConfSent = sendLMSConfAsEmail(verificationString, lmsMember, MERGE_TOPIC, carrierCode, locale,
				isServiceAppRQ);

		lmsMemberDAO.saveOrUpdate(lmsMember);
		lmsMemberMergeDAO.saveOrUpdate(lmsMemberMerge);

		return mergeConfSent;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#getLmsMemberById(int)
	 */
	@Override
	public LmsMember getLmsMemberById(int lmsMemberId) throws ModuleException {

		LmsMember lmsMember = null;

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}

		lmsMember = lmsMemberDAO.getLmsMemberById(lmsMemberId);
		return lmsMember;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.aircustomer.core.bl.LmsMemberBL#saveOrUpdate(com.isa.thinair.aircustomer.api.model.LmsMember)
	 */
	@Override
	public boolean saveOrUpdate(LmsMember lmsMember) throws ModuleException {

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}

		lmsMemberDAO.saveOrUpdate(lmsMember);
		return false;

	}

	@Override
	public boolean register(LmsMember lmsMember, String carrierCode, Locale locale, boolean isServiceAppRQ)
			throws ModuleException {

		if (lmsMemberDAO == null) {
			lmsMemberDAO = (LmsMemberDAO) AirCustomerModuleUtils.getDAO("lmsMemberDAO");
		}
		if (lmsMemberMergeDAO == null) {
			lmsMemberMergeDAO = (LmsMemberMergeDAO) AirCustomerModuleUtils.getDAO("lmsMemberMergeDAO");
		}

		LmsMemberMerge lmsMemberMerge = new LmsMemberMerge();

		String verificationString = UUID.randomUUID().toString().replace("-", "");

		lmsMemberMerge.setEmailId(lmsMember.getFfid());
		lmsMemberMerge.setVerificationString(verificationString);

		boolean mergeConfSent = sendLMSConfAsEmail(verificationString, lmsMember, CONFIRM_TOPIC, carrierCode, locale,
				isServiceAppRQ);

		if (mergeConfSent) {
			lmsMember.setEmailConfSent(YES);
		}

		lmsMemberDAO.saveOrUpdate(lmsMember);
		lmsMemberMergeDAO.saveOrUpdate(lmsMemberMerge);

		return mergeConfSent;
	}

	@Override
	public String getVerificationString(String ffid) throws ModuleException {
		if (lmsMemberMergeDAO == null) {
			lmsMemberMergeDAO = (LmsMemberMergeDAO) AirCustomerModuleUtils.getDAO("lmsMemberMergeDAO");
		}
		String verificationString = lmsMemberMergeDAO.getVerificationString(ffid);
		return verificationString;
	}

	@SuppressWarnings("unused")
	public static Boolean sendLMSConfAsEmail(String validationString, LmsMember lmsMember, String emailTopic, String carrierCode,
			Locale locale, boolean isServiceAppRQ) throws ModuleException {

		HashMap<String, Object> mergeConfDataMap = new HashMap<String, Object>();

		Topic topic = new Topic();
		UserMessaging usermsg = new UserMessaging();
		String firstName = lmsMember.getFirstName().substring(0, 1).toUpperCase() + lmsMember.getFirstName().substring(1);// make
																															// first
																															// character
																															// capital
		usermsg.setFirstName(firstName);
		usermsg.setLastName(lmsMember.getLastName());
		usermsg.setToAddres(lmsMember.getFfid());

		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		messageList.add(usermsg);

		MessageProfile msgProfile = new MessageProfile();
		msgProfile.setUserMessagings(messageList);

		AirCustomerConfig airCustomerConfig = AirCustomerModuleUtils.getAirCustomerConfig();
		String url = AppSysParamsUtil.getSecureServiceAppIBEUrl() + "/ibe/reservation.html#/lmsMemberConfirmation/EN?ffid="
					+ lmsMember.getFfid() + "&validationText=" + validationString;
		mergeConfDataMap.put("loginurl", url);
		mergeConfDataMap.put("user", usermsg);

		mergeConfDataMap.put("commonTemplatingDTO", AppSysParamsUtil.composeCommonTemplatingDTO(carrierCode));
		topic.setTopicParams(mergeConfDataMap);
		topic.setLocale(locale);
		topic.setTopicName(emailTopic);
		topic.setAttachMessageBody(false);

		msgProfile.setTopic(topic);

		if (msgProfile != null) {
			AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("unused")
	public static Boolean sendLMSMergeSuccessEmail(LmsMember lmsMember, String carrierCode) throws ModuleException {

		HashMap<String, Object> mergeConfDataMap = new HashMap<String, Object>();

		Topic topic = new Topic();
		UserMessaging usermsg = new UserMessaging();
		usermsg.setFirstName(lmsMember.getFirstName());
		usermsg.setLastName(lmsMember.getLastName());
		usermsg.setToAddres(lmsMember.getFfid());

		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		messageList.add(usermsg);

		MessageProfile msgProfile = new MessageProfile();
		msgProfile.setUserMessagings(messageList);

		AirCustomerConfig airCustomerConfig = AirCustomerModuleUtils.getAirCustomerConfig();
		mergeConfDataMap.put("user", usermsg);

		mergeConfDataMap.put("commonTemplatingDTO", AppSysParamsUtil.composeCommonTemplatingDTO(carrierCode));
		topic.setTopicParams(mergeConfDataMap);
		topic.setTopicName("lms_merge_success");
		topic.setAttachMessageBody(true);

		msgProfile.setTopic(topic);

		if (msgProfile != null) {
			AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);
			return true;
		} else {
			return false;
		}

	}

	public String getCrossPortalLoginUrl(String ffid, String menuIntRef) throws ModuleException {
		String url = null;

		LmsMember lmsMember = getLmsMember(ffid);
		if (lmsMember != null) {
			url = AirCustomerModuleUtils.getLoyaltyManagementBD().getCrossPortalLoginUrl(lmsMember,	menuIntRef);
		}
		return url;
	}

	public boolean syncLmsMember(String marketingAirLineCode, String memberFFID) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("email_id", memberFFID);
		map.put("carrier_code", marketingAirLineCode);

		try {

			String sql = "P_SYNC_LMS_MEMBER";
			SqlParameter spParam[] = new SqlParameter[2];

			spParam[0] = new SqlParameter("email_id", Types.VARCHAR);
			spParam[1] = new SqlParameter("carrier_code", Types.VARCHAR);

			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(), sql, spParam);
			spCall.callStoredProc(map);

		} catch (Exception e) {
			log.error("Exception in P_SYNC_LMS_MEMBER proc " + e.getMessage());
		}
		return true;
	}

	private DataSource getDataSource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

}
