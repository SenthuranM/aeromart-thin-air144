package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author chethiya
 *	@isa.module.dao-impl dao-name="lmsMemberDAO"
 */
public class LmsMemberDAOImpl extends PlatformHibernateDaoSupport implements LmsMemberDAO {

	
	public LmsMemberDAOImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO#getLmsMembers()
	 * 
	 */
	@Override
	public List<LmsMember> getLmsMembers() {
		
		List <LmsMember> members = find("from LmsMember ", LmsMember.class);
		return members;
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO#setPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public void setPassword(String password, String emailId) {
				
		LmsMember lmsMember = getLmsMember(emailId);
		lmsMember.setPassword(password);
		super.hibernateSaveOrUpdate(lmsMember);
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO#getLmsMember(java.lang.String)
	 */
	@Override
	public LmsMember getLmsMember(String emailId) {
		LmsMember lmsMember = null;
		List<LmsMember> list = null;

		list = find("Select lmsMember " + "from LmsMember as lmsMember where upper(lmsMember.ffid)=?",
				emailId.toUpperCase(),LmsMember.class);

		if (list.size() != 0) {
			lmsMember = list.get(0);
		}

		return lmsMember;
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO#getLmsMemberBySBId(int)
	 */
	@Override
	public LmsMember getLmsMemberBySBId(int sbInternalId) {
		LmsMember lmsMember = null;
		List<LmsMember> list = null;

		list = find("Select lmsMember " + "from LmsMember as lmsMember where lmsMember.sbInternalId=?",
				sbInternalId, LmsMember.class);

		if (list.size() != 0) {
			lmsMember = list.get(0);
		}

		return lmsMember;
	}


	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO#getLmsMemberById(int)
	 */
	@Override
	public LmsMember getLmsMemberById(int lmsMemberId) {
		Integer lmsId = new Integer(lmsMemberId);
		return get(LmsMember.class, lmsId);
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberDAO#saveOrUpdate(com.isa.thinair.aircustomer.api.model.LmsMember)
	 */
	@Override
	public void saveOrUpdate(LmsMember lmsMember) {
		lmsMember.setFfid(lmsMember.getFfid().toUpperCase());
		super.hibernateSaveOrUpdate(lmsMember);
	}
	
	
}
