/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.bl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.config.AirCustomerConfig;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCustomerDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author chamindap
 * 
 */
public class CustomerRegistrarImpl implements CustomerRegistrar {

	private static Log log = LogFactory.getLog(CustomerRegistrarImpl.class);

	private final static String CONFIRMED = "C";

	private final static String REGISTERED = "R";

	/** The CustomerDAO */
	private CustomerDAO customerDAO = null;

	/**
	 * The constructure.
	 */
	public CustomerRegistrarImpl() {
		super();
	}

	/**
	 * Method to register a customer.
	 * 
	 * @param customer
	 *            the customer.
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar
	 *      #register(com.isa.thinair.aircustomer.api.model.Customer)
	 * @AppParam AppSysParamsUtil.isCustomerRegiterConfirmation() check the whether the confirmation mail required or
	 *           not;
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public boolean register(Customer customer, String originCarrierCode, LoyaltyCustomerProfile loyaltyCustomerProfile,
			boolean isServiceAppRQ) throws ModuleException {
		boolean registered = false;
		String encryptedString = null;
		String encryptedPassword = null;

		customer.setRegistration_date(Calendar.getInstance());
		encryptedPassword = genKey(customer.getPassword());
		customer.setPassword(encryptedPassword);

		saveCustomer(customer);
		if (loyaltyCustomerProfile != null) {
			LoyaltyCustomerDAO loyaltyDao = (LoyaltyCustomerDAO) AirCustomerModuleUtils.getDAO("loyaltyCustomerDAO");
			loyaltyCustomerProfile.setCustomerId(customer.getCustomerId());
			loyaltyDao.saveOrUpdate(loyaltyCustomerProfile);
		}
		if (AppSysParamsUtil.isCustomerRegiterConfirmation()) {
			encryptedString = genKey(customer.getEmailId());
			sendConfirmationMail(customer, encryptedString, originCarrierCode, isServiceAppRQ);
		}
		
		if(customer.getIsXBECustomer() == 'Y'){
			
			encryptedString = genKey(customer.getEmailId());
			sendProfileDetailsMail(customer , encryptedString , originCarrierCode);
			
		}
		
		registered = true;

		return registered;
	}
	
	/**
	 * Method to confirm a customer.
	 * 
	 * @param customerLoginId
	 *            the customerLoginId.
	 * @param key
	 *            the key.
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar #confirm(java.lang.String, java.lang.String)
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public boolean confirm(String customerLoginId, String key) throws ModuleException {
		String encryptedString = null;
		boolean confirm = false;
		Customer customer = null;

		encryptedString = genKey(customerLoginId);
		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}
		if (encryptedString.equals(key)) {
			customer = customerDAO.getCustomer(customerLoginId);
			customer.setStatus(CONFIRMED);
			customer.setConfirmation_date(Calendar.getInstance());
			customerDAO.saveOrUpdate(customer);
			confirm = true;
		} else {
			return confirm;
		}

		return confirm;
	}

	/**
	 * Method to send the password to a customer.
	 * 
	 * @param customerLoginId
	 *            the customerLoginId.
	 * @param question
	 *            the question.
	 * @param answer
	 *            the answer.
	 * @return the boolean value.
	 * @throws ModuleException
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar #getForgotPassword(java.lang.String, java.lang.String,
	 *      java.lang.String)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean getForgotPassword(String loginId, String question, String answer, String originCarrierCode)
			throws ModuleException {
		boolean pssswordSend = false;
		String password = null;
		String decryptPassword = null;
		UserMessaging usermsg = null;
		List messageList = null;
		MessageProfile msgProfile = null;
		HashMap map = null;
		Topic topic = null;

		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}

		Customer customer = customerDAO.getForgotPassword(loginId, question, answer);

		if (customer != null) {
			password = customer.getPassword();

			if (password != null) {
				decryptPassword = decrypt(password);

				usermsg = new UserMessaging();
				messageList = new ArrayList();
				msgProfile = new MessageProfile();
				map = new HashMap();
				topic = new Topic();

				// alternativeEmail = customer.getAlternativeEmailId(); The mail should be sent to users email id not to
				// the alternate

				/*
				 * if (alternativeEmail != null) { usermsg.setToAddres(alternativeEmail); } else {
				 * usermsg.setToAddres(loginId); }
				 */

				usermsg.setToAddres(loginId);
				usermsg.setFirstName(customer.getFirstName());
				usermsg.setLastName(customer.getLastName());
				messageList.add(usermsg);
				msgProfile.setUserMessagings(messageList);

				map.put("user", usermsg);
				map.put("password", decryptPassword);
				map.put("commonTemplatingDTO", AppSysParamsUtil.composeCommonTemplatingDTO(originCarrierCode));

				topic.setTopicParams(map);
				topic.setTopicName("password_reiterate");
				msgProfile.setTopic(topic);

				AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);
				pssswordSend = true;
			}
		}
		return pssswordSend;
	}

	/**
	 * 
	 * @param loginId
	 * @param password
	 * @return
	 * @throws ModuleException
	 */
	public Customer authenticate(String loginId, String password) throws ModuleException {
		Customer customer = null;
		String encryptedPassword = null;
		String decryptedPassword = null;

		encryptedPassword = genKey(password);

		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}

		customer = customerDAO.authenticate(loginId, encryptedPassword);
		if (customer != null) {
			decryptedPassword = decrypt(customer.getPassword());
			customer.setPassword(decryptedPassword);
		}

		return customer;
	}

	/**
	 * @param customerId
	 * @return
	 * @throws ModuleException .
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar#getCustomer(int)
	 */
	public Customer getCustomer(int customerId) throws ModuleException {
		Customer customer = null;
		String decryptedPassword = null;

		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}

		customer = customerDAO.getCustomer(customerId);

		if (customer != null) {
			decryptedPassword = decrypt(customer.getPassword());
			customer.setPassword(decryptedPassword);
		}

		return customer;
	}

	/**
	 * @param loginId
	 * @return
	 * @throws ModuleException .
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar#getCustomer(java.lang.String)
	 */
	public Customer getCustomer(String loginId) throws ModuleException {
		Customer customer = null;
		String decryptedPassword = null;

		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}

		customer = customerDAO.getCustomer(loginId);

		if (customer != null) {
			decryptedPassword = decrypt(customer.getPassword());
			customer.setPassword(decryptedPassword);
		}

		return customer;
	}

	public SocialCustomer getSocialCustomer(String socialCustomerId, String socialSiteType) throws ModuleException {
		SocialCustomer socialCustomer = null;
		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}
		socialCustomer = customerDAO.getSocialCustomer(socialCustomerId, socialSiteType);
		return socialCustomer;
	}

	/**
	 * @param customer
	 * @throws ModuleException .
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar
	 *      #saveOrUpdate(com.isa.thinair.aircustomer.api.model.Customer)
	 */
	public void saveOrUpdate(Customer customer) throws ModuleException {
		String encryptedPassword = null;

		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}

		encryptedPassword = genKey(customer.getPassword());
		customer.setPassword(encryptedPassword);
		customerDAO.saveOrUpdate(customer);
	}

	/**
	 * Private method to save customer.
	 * 
	 * @param customer
	 *            the customer.
	 * @AppParam AppSysParamsUtil.isCustomerRegiterConfirmation() check the whether the confirmation required or not;
	 */
	private void saveCustomer(Customer customer) throws ModuleException {
		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}
		if (customer.getStatus() == null && AppSysParamsUtil.isCustomerRegiterConfirmation()) {
			customer.setStatus(REGISTERED);
		} else {
			customer.setStatus(CONFIRMED);
			customer.setConfirmation_date(Calendar.getInstance());
		}

		try {
			customerDAO.saveOrUpdate(customer);
		} catch (CommonsDataAccessException e) {
			if (e.getCause() instanceof DataIntegrityViolationException) {
				throw new ModuleException("aircustomer.logic.loginid.already.exist", "aircustomer.desc");
			} else {
				throw new ModuleException(e, e.getExceptionCode(), "aircustomer.desc");
			}
		}
	}

	/**
	 * Private method to generate a key.
	 * 
	 * @param toEncrypt
	 *            text to encrypt.
	 * @return the key.
	 * @throws ModuleException
	 */
	private String genKey(String toEncrypt) throws ModuleException {
		return AirCustomerModuleUtils.getCryptoServiceBD().encrypt(toEncrypt);
	}

	private String decrypt(String encryptedText) throws ModuleException {
		return AirCustomerModuleUtils.getCryptoServiceBD().decrypt(encryptedText);
	}

	/**
	 * Private method to send a mail to a customer.
	 * 
	 * @param customer
	 *            the customer.
	 * @param cypherText
	 *            the cypherText.
	 * @param isServiceAppRQ
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void sendConfirmationMail(Customer customer, String cypherText, String originCarrierCode, boolean isServiceAppRQ) {
		MessageProfile msgProfile = null;
		UserMessaging usermsg = null;
		String url = null;
		HashMap map = null;
		Topic topic = null;
		List messageList = null;
		String encrptText = null;
		String result = null;
		String email = null;
		String userId = null;
		String password = null;
		String decryptedPassword = null;

		try {
			result = URLEncoder.encode(cypherText, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("UTF-8 not supported", e);
		}
		email = customer.getEmailId();
		userId = customer.getEmailId();
		password = customer.getPassword();

		try {
			decryptedPassword = decrypt(password);
		} catch (ModuleException e) {
			log.error(e);
		}

		AirCustomerConfig airCustomerConfig = AirCustomerModuleUtils.getAirCustomerConfig();
		if (isServiceAppRQ && !AppSysParamsUtil.useOldIbeUrlInServiceAppConfirmRegister()) {
			url = AppSysParamsUtil.getSecureServiceAppIBEUrl() + "/ibe/reservation.html#/confirmCustomer/EN?emailId=" + email
					+ "&validationText=" + result;
		} else {
			url = PlatformUtiltiies.nullHandler(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL))
					+ airCustomerConfig.getConfirmProperties().getProperty("url").concat("crypt=" + result + "&emailId=" + email
							+ (originCarrierCode != null ? "&originCarrier=" + originCarrierCode : ""));
		}


		usermsg = new UserMessaging();
		messageList = new ArrayList();
		msgProfile = new MessageProfile();
		map = new HashMap();
		topic = new Topic();
		encrptText = cypherText;

		usermsg.setFirstName(customer.getFirstName());
		usermsg.setLastName(customer.getLastName());
		usermsg.setToAddres(customer.getEmailId());
		messageList.add(usermsg);
		msgProfile.setUserMessagings(messageList);

		map.put("user", usermsg);
		map.put("loginurl", url);
		map.put("userId", userId);
		map.put("password", decryptedPassword);
		map.put("strCarrierNames", getCarrierNames());
		map.put("commonTemplatingDTO", AppSysParamsUtil.composeCommonTemplatingDTO(originCarrierCode));
		topic.setTopicParams(map);
		topic.setTopicName("registration_confirm");
		msgProfile.setTopic(topic);

		AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);
	}
	
	/**
	 * Private method to send a mail to a customer with the generated password when customer is registered through
	 * call center
	 * 
	 * @param customer
	 *            the customer.
	 *            
	 * @param cypherText
	 *            the cypherText.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void sendProfileDetailsMail(Customer customer , String cypherText ,String originCarrierCode){

		String email = null;
		String password = null;
		String decryptedPassword = null;
		String result = null;
		
		email = customer.getEmailId();
		password = customer.getPassword();

		
		try {
			result = URLEncoder.encode(cypherText, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("UTF-8 not supported", e);
		}
		
		HashMap<String, Object> dataMap = new HashMap<String, Object>() ;
		
		try {
			decryptedPassword = decrypt(password);
		} catch (ModuleException e) {
			log.error(e);
		}
		
		Topic topic = new Topic();
		UserMessaging usermsg = new UserMessaging();
		String firstName = customer.getFirstName().substring(0, 1).toUpperCase() + customer.getFirstName().substring(1);//make first character capital
		usermsg.setFirstName(firstName);
		usermsg.setLastName(customer.getLastName());
		usermsg.setToAddres(customer.getEmailId());

		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		messageList.add(usermsg);

		MessageProfile msgProfile = new MessageProfile();
		msgProfile.setUserMessagings(messageList);
		
		StringBuilder urlBuild = new StringBuilder();
		urlBuild.append(AppSysParamsUtil.getDefaultAirlineUrl());
		urlBuild.append("/en/login-air-rewards");

		dataMap.put("user", usermsg);
		dataMap.put("loginurl", urlBuild.toString());
		dataMap.put("password", decryptedPassword);
		dataMap.put("commonTemplatingDTO", AppSysParamsUtil.composeCommonTemplatingDTO(originCarrierCode));
		topic.setTopicParams(dataMap);
		topic.setTopicName("xbe_user_registration_success");
		topic.setAttachMessageBody(false);

		msgProfile.setTopic(topic);

		AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);

		
	}

	private String getCarrierNames() {
		String strCarrierNames = "";
		Collection<String> colCarrierNames = CommonsServices.getGlobalConfig().getActiveSysCarriersMap().values();
		if (colCarrierNames != null && colCarrierNames.size() > 0) {
			int count = 0;
			for (Iterator<String> colCarrierNamesIt = colCarrierNames.iterator(); colCarrierNamesIt.hasNext(); ++count) {
				if (count == 0) {
					strCarrierNames += colCarrierNamesIt.next();
				} else if (count == colCarrierNames.size() - 1) {
					strCarrierNames += " and " + colCarrierNamesIt.next();
				} else {
					strCarrierNames += ", " + colCarrierNamesIt.next();
				}
			}
		}
		return strCarrierNames;
	}

	private String getUrlEncodedString(String str) {
		String urlEncoded = str;
		try {
			urlEncoded = URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.error(e); // in case error, return non urlEncoded!
		}
		return urlEncoded;
	}

	@Override
	public void confirmLMSMemberStatus(String customerLoginId) throws ModuleException {
		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}
		Customer customer = customerDAO.getCustomer(customerLoginId);
		customer.setIsLmsMember('Y');
		customerDAO.saveOrUpdate(customer);
	}

	@Override
	public Page<Customer> getCustomerByPartialEmail(String partialEmail, int start, int pageSize) throws ModuleException {
		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}
		Page<Customer> customerPage = customerDAO.getCustomerByPartialEmail(partialEmail, start, pageSize);
		return customerPage;
	}

	@Override
	public Page<Customer> getCustomerByEmail(String partialEmail) throws ModuleException {
		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}
		Page<Customer> customerPage = customerDAO.getCustomerByEmail(partialEmail);
		return customerPage;
	}

	@Override
	public void resendProfileDetailsMail(String emailId, String originCarrierCode) throws ModuleException {
		String encryptedString = genKey(emailId);
		Customer customer = getCustomer(emailId);
		sendProfileDetailsMail(customer, encryptedString, originCarrierCode);

	}

	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean sendPasswordResetLink(String loginId, String question,
			String answer, String originCarrierCode, String baseUrl) throws ModuleException {
		boolean pssswordSend = false;
		UserMessaging usermsg = null;
		List messageList = null;
		MessageProfile msgProfile = null;
		HashMap map = null;
		Topic topic = null;

		if (customerDAO == null) {
			customerDAO = AirCustomerModuleUtils.getCustomerDAO();
		}

		Customer customer = customerDAO.getForgotPassword(loginId, question, answer);

		if (customer != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			String token = "date=" + simpleDateFormat.format(new Date()) + "email=" + loginId;
			String encryptedToken = genKey(token);
			String url = baseUrl + "/ibe/reservation.html#/resetPassword/en/AED/modify/" + encryptedToken;
			
			usermsg = new UserMessaging();
			messageList = new ArrayList();
			msgProfile = new MessageProfile();
			map = new HashMap();
			topic = new Topic();

			usermsg.setToAddres(loginId);
			usermsg.setFirstName(customer.getFirstName());
			usermsg.setLastName(customer.getLastName());
			messageList.add(usermsg);
			msgProfile.setUserMessagings(messageList);

			map.put("firstName", customer.getFirstName());
			map.put("carrierName", AppSysParamsUtil
					.composeCommonTemplatingDTO(originCarrierCode).getCarrierName());
			map.put("passwordResetLink",url);
			map.put("validPeriod", AppSysParamsUtil.getIBEPasswordResetTimeOut());
			topic.setTopicParams(map);
			topic.setTopicName("password_reset");
			
			msgProfile.setTopic(topic);

			AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(
					msgProfile);
			pssswordSend = true;
		}
		return pssswordSend;
	}
	
}