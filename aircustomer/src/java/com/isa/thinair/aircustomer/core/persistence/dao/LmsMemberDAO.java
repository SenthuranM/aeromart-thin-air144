package com.isa.thinair.aircustomer.core.persistence.dao;

import java.util.List;

import com.isa.thinair.aircustomer.api.model.LmsMember;

/**
 * @author chethiya
 *
 */
public interface LmsMemberDAO {

	/**
	 * @return
	 */
	public List<LmsMember> getLmsMembers() ;

	/**
	 * @param password
	 * @param emailId
	 */
	public void setPassword(String password, String emailId) ;
	
	/**
	 * @param emailId
	 * @return
	 */
	public LmsMember getLmsMember(String emailId) ;
		
	/**
	 * @param sbInternalId
	 * @return
	 */
	public LmsMember getLmsMemberBySBId(int sbInternalId) ;

	/**
	 * @param lmsMemberId
	 * @return
	 */
	public LmsMember getLmsMemberById(int lmsMemberId) ;
	
	/**
	 * @param lmsMember
	 */
	public void saveOrUpdate(LmsMember lmsMember) ;

}
