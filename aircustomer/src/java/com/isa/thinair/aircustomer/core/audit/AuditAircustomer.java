package com.isa.thinair.aircustomer.core.audit;

import java.util.Date;
import java.util.LinkedHashMap;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;

public class AuditAircustomer {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditSaveUpdateCustomer(Customer oldCustomer, Customer newCustomer, String userID)
			throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap contents = new LinkedHashMap();

		if (userID != null) {
			audit.setUserId(userID);
		} else {
			audit.setUserId("");
		}

		audit.setTaskCode(String.valueOf(TasksUtil.EDIT_CUSTOMER));

		contents.put("old emailId", nullHandler(oldCustomer.getEmailId()));
		contents.put("old title", nullHandler(oldCustomer.getTitle()));
		contents.put("old firstName", nullHandler(oldCustomer.getFirstName()));
		contents.put("old lastName", nullHandler(oldCustomer.getLastName()));
		contents.put("old telephone", nullHandler(oldCustomer.getTelephone()));
		contents.put("old mobile", nullHandler(oldCustomer.getMobile()));
		contents.put("old country code", nullHandler(oldCustomer.getCountryCode()));
		contents.put("old nationality code", nullHandler(oldCustomer.getNationalityCode()));
		if (oldCustomer.getLMSMemberDetails() != null) {
			contents.put("old date of birth", nullHandler(oldCustomer.getLMSMemberDetails().getDateOfBirth()));
			contents.put("old passport", nullHandler(oldCustomer.getLMSMemberDetails().getPassportNum()));
			contents.put("old comm. language", nullHandler(oldCustomer.getLMSMemberDetails().getLanguage()));
			contents.put("old family head email", nullHandler(oldCustomer.getLMSMemberDetails().getHeadOFEmailId()));
		}

		contents.put("new emailId", nullHandler(newCustomer.getEmailId()));
		contents.put("new title", nullHandler(newCustomer.getTitle()));
		contents.put("new firstName", nullHandler(newCustomer.getFirstName()));
		contents.put("new lastName", nullHandler(newCustomer.getLastName()));
		contents.put("new telephone", nullHandler(newCustomer.getTelephone()));
		contents.put("new mobile", nullHandler(newCustomer.getMobile()));
		contents.put("new country code", nullHandler(newCustomer.getCountryCode()));
		contents.put("new nationality code", nullHandler(newCustomer.getNationalityCode()));
		if (newCustomer.getLMSMemberDetails() != null) {
			contents.put("new date of birth", nullHandler(newCustomer.getLMSMemberDetails().getDateOfBirth()));
			contents.put("new passport", nullHandler(newCustomer.getLMSMemberDetails().getPassportNum()));
			contents.put("new comm. language", nullHandler(newCustomer.getLMSMemberDetails().getLanguage()));
			contents.put("new family head email", nullHandler(newCustomer.getLMSMemberDetails().getHeadOFEmailId()));
		}


		AirCustomerModuleUtils.getAuditorBD().audit(audit, contents);

	}

	private static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

}
