/**
 * 
 */
package com.isa.thinair.aircustomer.core.bl;

import java.util.Date;

import com.isa.thinair.aircustomer.api.dto.external.AgentFrequentCustomerDTO;
import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.persistence.dao.AgentFrequentCustomerDAO;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;

/**
 * @author suneth
 *
 */
public class AgentFrequentCustomerBLImpl implements AgentFrequentCustomerBL {

	public AgentFrequentCustomerBLImpl() {
		super();
	}

	public void saveUpdateAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId, String operation)
			throws ModuleException {
		AgentFrequentCustomerDAO frequentCustomerDAO = (AgentFrequentCustomerDAO) AirCustomerModuleUtils
				.getDAO("agentFrequentCustomerDAO");
		if (operation.equals("save")) {
			frequentCustomerDAO.saveOrUpdate(frequentCustomer);
			Audit audit = generateAudit(null, frequentCustomer.toAgentFrequentCustomerDTO(), userId);
			AirCustomerModuleUtils.getAuditorBD().audit(audit, null);
		} else if (operation.equals("update")) {
			AgentFrequentCustomer customerOldValues = frequentCustomerDAO.getFrequentCustomer(frequentCustomer
					.getFrequentCustomerId());
			frequentCustomer.setVersion(customerOldValues.getVersion());
			frequentCustomerDAO.saveOrUpdate(frequentCustomer);
			Audit audit = generateAudit(customerOldValues.toAgentFrequentCustomerDTO(),
					frequentCustomer.toAgentFrequentCustomerDTO(), userId);
			AirCustomerModuleUtils.getAuditorBD().audit(audit, null);
		}
	}

	public Page searchAgentFrequentCustomers(String firstName, String lastName, String email, String agentCode)
			throws ModuleException {
		AgentFrequentCustomerDAO frequentCustomerDAO = (AgentFrequentCustomerDAO) AirCustomerModuleUtils
				.getDAO("agentFrequentCustomerDAO");
		return frequentCustomerDAO.searchFrequentCustomers(firstName, lastName, email, agentCode);
	}

	public void deleteAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId) throws ModuleException {
		AgentFrequentCustomerDAO frequentCustomerDAO = (AgentFrequentCustomerDAO) AirCustomerModuleUtils
				.getDAO("agentFrequentCustomerDAO");
		Audit audit = generateAudit(frequentCustomer.toAgentFrequentCustomerDTO(), null, userId);
		AgentFrequentCustomer tempAgentFrequentCustomer = frequentCustomerDAO.getFrequentCustomer(frequentCustomer
				.getFrequentCustomerId());
		frequentCustomer.setVersion(tempAgentFrequentCustomer.getVersion());
		frequentCustomerDAO.deleteFrequentCustomer(frequentCustomer);
		AirCustomerModuleUtils.getAuditorBD().audit(audit, null);
	}

	public Audit generateAudit(AgentFrequentCustomerDTO customerOldValues, AgentFrequentCustomerDTO customerNewValues,
			String userId) {
		String details = "";
		Date timeStamp = new Date();

		if (customerOldValues == null) {

			details += "values : " + customerNewValues.getTitle() + ",";
			details += customerNewValues.getFirstName() + ",";
			details += customerNewValues.getLastName() + ",";
			details += customerNewValues.getDateOfBirth() + ",";
			details += customerNewValues.getNationalityCode() + ",";
			details += customerNewValues.getEmail() + ",";
			details += customerNewValues.getPassportNumber() + ",";
			details += customerNewValues.getPassportExpiryDate() + ",";
			details += customerNewValues.getPaxType();

			return new Audit(TasksUtil.ADD_AGENT_FREQUENT_CUSTOMER, timeStamp, "xbe", userId, details);

		} else if (customerNewValues == null) {

			details += "values : " + customerOldValues.getTitle() + ",";
			details += customerOldValues.getFirstName() + ",";
			details += customerOldValues.getLastName() + ",";
			details += customerOldValues.getDateOfBirth() + ",";
			details += customerOldValues.getNationalityCode() + ",";
			details += customerOldValues.getEmail() + ",";
			details += customerOldValues.getPassportNumber() + ",";
			details += customerOldValues.getPassportExpiryDate() + ",";
			details += customerOldValues.getPaxType();

			return new Audit(TasksUtil.REMOVE_AGENT_FREQUENT_CUSTOMER, timeStamp, "xbe", userId, details);

		} else {

			details += "old values : " + customerOldValues.getTitle() + ",";
			details += customerOldValues.getFirstName() + ",";
			details += customerOldValues.getLastName() + ",";
			details += customerOldValues.getDateOfBirth() + ",";
			details += customerOldValues.getNationalityCode() + ",";
			details += customerOldValues.getEmail() + ",";
			details += customerOldValues.getPassportNumber() + ",";
			details += customerOldValues.getPassportExpiryDate() + ",";
			details += customerOldValues.getPaxType() + " || ";

			details += "new values : " + customerNewValues.getTitle() + ",";
			details += customerNewValues.getFirstName() + ",";
			details += customerNewValues.getLastName() + ",";
			details += customerNewValues.getDateOfBirth() + ",";
			details += customerNewValues.getNationalityCode() + ",";
			details += customerNewValues.getEmail() + ",";
			details += customerNewValues.getPassportNumber() + ",";
			details += customerNewValues.getPassportExpiryDate() + ",";
			details += customerNewValues.getPaxType();

			return new Audit(TasksUtil.EDIT_AGENT_FREQUENT_CUSTOMER, timeStamp, "xbe", userId, details);

		}

	}

}
