/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.remoting.ejb;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Hashtable;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.model.LoyaltyCredit;
import com.isa.thinair.aircustomer.core.bl.LoyaltyCreditBL;
import com.isa.thinair.aircustomer.core.bl.LoyaltyCreditBLImpl;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCreditDAO;
import com.isa.thinair.aircustomer.core.service.bd.LoyaltyCreditServiceDelegateImpl;
import com.isa.thinair.aircustomer.core.service.bd.LoyaltyCreditServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Haider
 */
@Stateless
@LocalBinding(jndiBinding = "LoyaltyCreditService.local")
@RemoteBinding(jndiBinding = "LoyaltyCreditService.remote")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LoyaltyCreditServiceBean extends PlatformBaseSessionBean implements LoyaltyCreditServiceDelegateImpl,
		LoyaltyCreditServiceLocalDelegateImpl {

	private final Log log = LogFactory.getLog(getClass());

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCredit> credit) throws ModuleException {
		try {
			LoyaltyCreditBL loyaltyCreditBL = new LoyaltyCreditBLImpl();
			return loyaltyCreditBL.saveAll(credit);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAll Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal getTotalNonExpireCredit(String accountNo) throws ModuleException {
		BigDecimal credit = null;
		try {
			credit = lookupLoyaltyCreditDAO().getTotalNonExpireCredit(accountNo);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getTotalNonExpireCredits(accountNo) Loyalty credit module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return credit;
	}

	/**
	 * Private method for looking dao interface.
	 * 
	 * @return
	 */
	private LoyaltyCreditDAO lookupLoyaltyCreditDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (LoyaltyCreditDAO) lookupService.getBean("isa:base://modules/" + "aircustomer?id=loyaltyCreditDAOProxy");
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void consumeLoyaltyCreditFromCustomer(BigDecimal amount, String accountNo) throws ModuleException {
		try {
			LoyaltyCreditBL loyaltyCreditBL = new LoyaltyCreditBLImpl();
			loyaltyCreditBL.consumeLoyaltyCreditFromCustomer(amount, accountNo);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAll Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
}