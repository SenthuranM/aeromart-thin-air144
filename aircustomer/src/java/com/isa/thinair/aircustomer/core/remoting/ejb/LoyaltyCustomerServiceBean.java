/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.remoting.ejb;

import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.core.bl.LoyaltyCustomerBL;
import com.isa.thinair.aircustomer.core.bl.LoyaltyCustomerBLImpl;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCustomerDAO;
import com.isa.thinair.aircustomer.core.service.bd.LoyaltyCustomerServiceDelegateImpl;
import com.isa.thinair.aircustomer.core.service.bd.LoyaltyCustomerServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Haider
 */
@Stateless
@LocalBinding(jndiBinding = "LoyaltyCustomerService.local")
@RemoteBinding(jndiBinding = "LoyaltyCustomerService.remote")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class LoyaltyCustomerServiceBean extends PlatformBaseSessionBean implements LoyaltyCustomerServiceDelegateImpl,
		LoyaltyCustomerServiceLocalDelegateImpl {

	private final Log log = LogFactory.getLog(getClass());

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveOrUpdate(LoyaltyCustomerProfile customer) throws ModuleException {
		try {
			lookupLoyaltyCustomerDAO().saveOrUpdate(customer);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveOrUpdate Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCustomerProfile> customer) throws ModuleException {
		try {
			LoyaltyCustomerBL loyaltyCustomerBL = new LoyaltyCustomerBLImpl();
			return loyaltyCustomerBL.saveAll(customer);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveAll Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<LoyaltyCustomerProfile> getLoyaltyCustomers() throws ModuleException {
		List<LoyaltyCustomerProfile> list = null;

		try {
			list = lookupLoyaltyCustomerDAO().getLoyaltyCustomers();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getLoyaltyCustomers Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return list;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getLoyaltyAccountNo(Integer customerId) throws ModuleException {
		try {
			return lookupLoyaltyCustomerDAO().getLoyaltyAccountNo(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getLoyaltyAccountNo Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyCustomerProfile getLoyaltyCustomerByCustomerId(int customerId) throws ModuleException {
		LoyaltyCustomerProfile customer = null;
		try {
			customer = lookupLoyaltyCustomerDAO().getLoyaltyCustomerByCustomerId(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getLoyaltyCustomerByCustumerId(int) Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customer;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyCustomerProfile getLoyaltyCustomer(String accountNo) throws ModuleException {
		LoyaltyCustomerProfile customer = null;
		try {
			customer = lookupLoyaltyCustomerDAO().getLoyaltyCustomer(accountNo);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getLoyaltyCustomer(accountNo) Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customer;
	}

	/**
	 * Private method for looking dao interface.
	 * 
	 * @return
	 */
	private LoyaltyCustomerDAO lookupLoyaltyCustomerDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (LoyaltyCustomerDAO) lookupService.getBean("isa:base://modules/" + "aircustomer?id=loyaltyCustomerDAOProxy");
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyCustomerProfile getLoyaltyCustomerByExample(LoyaltyCustomerProfile loyaltyCustomer) throws ModuleException {
		LoyaltyCustomerProfile customer = null;
		try {
			customer = lookupLoyaltyCustomerDAO().getLoyaltyCustomerByExample(loyaltyCustomer);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getLoyaltyCustomerByExample(loyaltyCustomer) Loyalty customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customer;
	}

}