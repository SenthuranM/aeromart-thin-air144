/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.dto.external.CustomerTO;
import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMeal;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredSeat;
import com.isa.thinair.aircustomer.api.model.CustomerSession;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.audit.AuditAircustomer;
import com.isa.thinair.aircustomer.core.bl.AgentFrequentCustomerBL;
import com.isa.thinair.aircustomer.core.bl.AgentFrequentCustomerBLImpl;
import com.isa.thinair.aircustomer.core.bl.CustomerAliasBL;
import com.isa.thinair.aircustomer.core.bl.CustomerAliasBLImpl;
import com.isa.thinair.aircustomer.core.bl.CustomerRegistrar;
import com.isa.thinair.aircustomer.core.bl.CustomerRegistrarImpl;
import com.isa.thinair.aircustomer.core.bl.TokenGenerator;
import com.isa.thinair.aircustomer.core.bl.TokenGeneratorImpl;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerAliasDAO;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO;
import com.isa.thinair.aircustomer.core.service.bd.AirCustomerServiceDelegateImpl;
import com.isa.thinair.aircustomer.core.service.bd.AirCustomerServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Chamindap
 */
@Stateless
@RemoteBinding(jndiBinding = "AirCustomerService.remote")
@LocalBinding(jndiBinding = "AirCustomerService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirCustomerServiceBean extends PlatformBaseSessionBean implements AirCustomerServiceDelegateImpl,
		AirCustomerServiceLocalDelegateImpl {

	public static String CUSTOMER_QUESTIONAIRA_DAO = "customerQuestionaireDAO";

	private final Log log = LogFactory.getLog(getClass());

	public void saveOrUpdate(Customer customer) throws ModuleException {
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customerRegistrar.saveOrUpdate(customer);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveOrUpdate customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public List<Customer> getCustomers() throws ModuleException {
		List<Customer> list = null;

		try {
			list = lookupCustomerDAO().getCustomers();
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomers customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return list;
	}

	public Customer getCustomer(int customerId) throws ModuleException {
		Customer customer = null;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customer = customerRegistrar.getCustomer(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomer(int) customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customer;
	}

	public Customer authenticate(String loginId, String password) throws ModuleException {
		Customer customer = null;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customer = customerRegistrar.authenticate(loginId, password);
		} catch (CommonsDataAccessException cdaex) {
			log.error("authenticate customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customer;
	}

	public Customer getCustomer(String loginId) throws ModuleException {
		Customer customer = null;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customer = customerRegistrar.getCustomer(loginId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomer(loginId) customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customer;
	}

	public SocialCustomer getSocialCustomer(String socialCustomerId, String socialSiteType) throws ModuleException {

		SocialCustomer socialCustomer = null;
		try {
			CustomerRegistrar customerRegistrar = new CustomerRegistrarImpl();
			socialCustomer = customerRegistrar.getSocialCustomer(socialCustomerId, socialSiteType);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getSocialCustomer(Integer socialCustomerId) customer module failed", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return socialCustomer;
	}

	public boolean confirmCustomer(String customerId, String customerKey) throws ModuleException {
		boolean confirm = false;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			confirm = customerRegistrar.confirm(customerId, customerKey);
		} catch (CommonsDataAccessException cdaex) {
			log.error("confirmCustomer customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return confirm;
	}

	public boolean registerCustomer(Customer customer, String originCarrierCode, LoyaltyCustomerProfile loyaltyCustomerProfile,
			boolean isServiceAppRQ) throws ModuleException {
		boolean registered = false;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			registered = customerRegistrar.register(customer, originCarrierCode, loyaltyCustomerProfile, isServiceAppRQ);
		} catch (CommonsDataAccessException cdaex) {
			log.error("registerCustomer customer module failed.", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return registered;
	}

	/**
	 * Returns the password for the user.
	 * 
	 * @param loginId
	 * @param question
	 * @param answer
	 * @return
	 */
	public boolean getForgotPassword(String loginId, String question, String answer, String originCarrierCode)
			throws ModuleException {
		boolean pssswordSend = false;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();
		try {
			pssswordSend = customerRegistrar.getForgotPassword(loginId, question, answer, originCarrierCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getForgotPassword customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return pssswordSend;
	}

	@Override
	public void confirmLMSMemberStatus(String customerId) throws ModuleException {
		CustomerRegistrar customerRegistrar = new CustomerRegistrarImpl();
		customerRegistrar.confirmLMSMemberStatus(customerId);
	}

	/**
	 * Private method for looking dao interface.
	 * 
	 * @return
	 */
	private CustomerDAO lookupCustomerDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (CustomerDAO) lookupService.getBean("isa:base://modules/" + "aircustomer?id=customerDAOProxy");
	}

	/**
	 * Return all the customers
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<CustomerTO> getCustomerTOs() throws ModuleException {
		try {
			Collection<Customer> colCustomers = lookupCustomerDAO().getCustomers();
			Collection<CustomerTO> colCustomerTOs = new ArrayList<CustomerTO>();
			Customer customer;
			for (Iterator<Customer> iter = colCustomers.iterator(); iter.hasNext();) {
				customer = (Customer) iter.next();
				colCustomerTOs.add(customer.toCustomerTO());
			}

			return colCustomerTOs;
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomerTOs customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}


	public String generateToken() {
		TokenGenerator tokenGenerator = new TokenGeneratorImpl();
		return tokenGenerator.generateToken();
	}

	public CustomerSession loadCustomerSession(String token) {
		return AirCustomerModuleUtils.getCustomerDAO().loadCustomerSession(token);
	}

	public void createCustomerSession(int customerId, String token) {
		AirCustomerModuleUtils.getCustomerDAO().createCustomerSession(customerId, token);
	}

	@Override
	public Page<Customer> getCustomerByPartialEmail(String partialEmail, int start, int pageSize) throws ModuleException {
		Page<Customer> customerPage = null;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customerPage = customerRegistrar.getCustomerByPartialEmail(partialEmail, start, pageSize);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomerByPartialEmail(String) customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return customerPage;
	}

	@Override
	public Page<Customer> getCustomerByEmail(String email, int start, int pageSize) throws ModuleException {
		Page<Customer> customerPage = null;
		CustomerRegistrar customerRegistrar = null;
		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customerPage = customerRegistrar.getCustomerByEmail(email);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomerByPartialEmail(String) customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		return customerPage;

	}

	@Override
	public void resendProfileDetailsMail(String emailId, String originCarrierCode) throws ModuleException {
		CustomerRegistrar customerRegistrar = null;
		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customerRegistrar.resendProfileDetailsMail(emailId, originCarrierCode);
		} catch (CommonsDataAccessException cdaex) {
			log.error("resendProfileDetailsMail(String) customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public List<SeatType> getPreferenceSeatTypes(){
		return AirCustomerModuleUtils.getCustomerDAO().getPreferenceSeatTypes();
	}
	
	public void setCustomerPreferredSeat(CustomerPreferredSeat customerPreferredSeat){
		AirCustomerModuleUtils.getCustomerDAO().setCustomerPreferredSeat(customerPreferredSeat);
	}
	
	public void setCustomerPreferredMeals(List<CustomerPreferredMeal> customerPreferredMealList){
		AirCustomerModuleUtils.getCustomerDAO().setCustomerPreferredMeals(customerPreferredMealList);
	}
	
	public String getCustomerPreferredSeatType(int customerId){
		return AirCustomerModuleUtils.getCustomerDAO().getCustomerPreferredSeatType(customerId);
	}
	
	public List<CustomerPreferredMealDTO> getCustomerPreferredMeals(int customerId){
		return AirCustomerModuleUtils.getCustomerDAO().getCustomerPreferredMeals(customerId);
	}
	
	public void saveOrUpdate(FamilyMember familyMember) throws ModuleException{
		try {
			AirCustomerModuleUtils.getCustomerDAO().saveOrUpdate(familyMember);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveOrUpdate familyMember module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	public FamilyMember getFamilyMember(String firstName , String lastName, int customerId) throws ModuleException{
		try {
			return AirCustomerModuleUtils.getCustomerDAO().getFamilyMember(firstName, lastName, customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getFamilyMember using firstName lastName failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	public FamilyMember getFamilyMember(int familyMemberId) throws ModuleException{
		try {
			return AirCustomerModuleUtils.getCustomerDAO().getFamilyMember(familyMemberId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getFamilyMember using familyMemberId failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	public List<FamilyMember> getFamilyMembers(int customerId) throws ModuleException{
		try {
			return AirCustomerModuleUtils.getCustomerDAO().getFamilyMembers(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getFamilyMembers failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	public void removeFamilyMember(FamilyMember familyMember) throws ModuleException{
		try {
			AirCustomerModuleUtils.getCustomerDAO().removeFamilyMember(familyMember);;
		} catch (CommonsDataAccessException cdaex) {
			log.error("removeFamilyMember failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void saveUpdateAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId, String operation)  throws ModuleException{
		AgentFrequentCustomerBL agentFrequentCustomerBL = new AgentFrequentCustomerBLImpl();
		agentFrequentCustomerBL.saveUpdateAgentFrequentCustomer(frequentCustomer, userId, operation);
	}
	
	public Page searchAgentFrequentCustomers(String firstName , String lastName , String email, String agentCode) throws ModuleException{
		AgentFrequentCustomerBL agentFrequentCustomerBL = new AgentFrequentCustomerBLImpl();
		return agentFrequentCustomerBL.searchAgentFrequentCustomers(firstName, lastName, email, agentCode);
	}
	
	public void deleteAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId) throws ModuleException{
		AgentFrequentCustomerBL agentFrequentCustomerBL = new AgentFrequentCustomerBLImpl();
		agentFrequentCustomerBL.deleteAgentFrequentCustomer(frequentCustomer, userId);
		
	}

	public void saveOrUpdateWithName(Customer oldCustomer, Customer newCustomer, String userID) throws ModuleException {
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();

		try {
			customerRegistrar.saveOrUpdate(newCustomer);
			if (oldCustomer != null) {
				AuditAircustomer.doAuditSaveUpdateCustomer(oldCustomer, newCustomer, userID);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveOrUpdate customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public boolean sendPasswordResetLink(String loginId, String question,
			String answer, String originCarrierCode, String baseUrl)
			throws ModuleException {
		
		boolean pssswordSend = false;
		CustomerRegistrar customerRegistrar = null;

		customerRegistrar = new CustomerRegistrarImpl();
		try {
			pssswordSend = customerRegistrar.sendPasswordResetLink(loginId, question, answer, originCarrierCode, baseUrl);
		} catch (CommonsDataAccessException cdaex) {
			log.error("sendPasswordResetLink customer module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return pssswordSend;
	}

	/**
	 * returns the all customerAlias object for given customerId.
	 */
	@Override
	public List<CustomerAlias> getCustomerAliasList(long customerId) throws ModuleException {

		List<CustomerAlias> list = null;
		try {
			list = lookupCustomerAliasDAO().getCustomerAliasList(customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomerAliasList(long) customerAlias module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return list;
	}

	/**
	 * returns all the customerAlias Object which matches with the given customerAliasId and customerId.
	 */
	@Override
	public CustomerAlias getCustomerAliasByCustomerAliasId(long customerAliasId, long customerId) throws ModuleException {

		CustomerAlias customerAlias = null;
		CustomerAliasBL customerAliasBL = null;
		customerAliasBL = new CustomerAliasBLImpl();
		try {
			customerAlias = customerAliasBL.getCustomerAliasByCustomerAliasId(customerAliasId, customerId);
		} catch (CommonsDataAccessException cdaex) {
			log.error("getCustomerAliasByCustomerAliasId(long,long) customerAlias module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

		return customerAlias;
	}

	/**
	 * Method for saveOrUpdate CustomerAlias object accrodingly
	 */
	@Override
	public void saveOrUpdate(CustomerAlias customerAlias) throws ModuleException {

		CustomerAliasBL customerAliasBL = null;
		customerAliasBL = new CustomerAliasBLImpl();

		try {
			customerAliasBL.saveOrUpdate(customerAlias);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveOrUpdate customerAlias module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Method for saving the CustomerAlias object
	 */
	@Override
	public void save(CustomerAlias customerAlias) throws ModuleException {

		CustomerAliasBL customerAliasBL = null;
		customerAliasBL = new CustomerAliasBLImpl();

		try {
			customerAliasBL.save(customerAlias);
		} catch (CommonsDataAccessException cdaex) {
			log.error("saveOrUpdate customerAlias module failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Private method for looking dao interface.
	 * 
	 * @return CustomerAliasDAO
	 */
	private CustomerAliasDAO lookupCustomerAliasDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (CustomerAliasDAO) lookupService.getBean("isa:base://modules/" + "aircustomer?id=customerAliasDAOProxy");
	}
}