package com.isa.thinair.aircustomer.core.persistence.dao;

import java.util.List;

import com.isa.thinair.aircustomer.api.model.LmsMemberMerge;

/**
 * @author chethiya
 *
 */
public interface LmsMemberMergeDAO {
	
	/**
	 * @return
	 */
	public List<LmsMemberMerge> getLmsMergeList();
	
	/**
	 * @param emailId
	 * @return
	 */
	public LmsMemberMerge getLmsMerge(String emailId);
	
	/**
	 * @param mergeId
	 * @return
	 */
	public LmsMemberMerge getLmsMergeById(String mergeId);
	
	/**
	 * @param emailId
	 * @return
	 */
	public String getVerificationString(String emailId);
	
	/**
	 * @param merge
	 */
	public void saveOrUpdate(LmsMemberMerge lmsMemberMerge);
	
	/**
	 * @param verificationString
	 * @param emailId
	 * @return TODO
	 */
	public int deleteRecord(String verificationString, String emailId);
	
	/**
	 * @param verificationString
	 * @param emailId
	 * @return
	 */
	public int numberOfMerges(String verificationString, String emailId);

}
