package com.isa.thinair.aircustomer.core.bl;

import java.util.List;

import com.isa.thinair.aircustomer.api.constants.CustomAliasConstants;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerAliasDAO;

public class CustomerAliasBLImpl implements CustomerAliasBL {

	/** The CustomerDAO */
	private CustomerAliasDAO customerAliasDAO = null;

	@Override
	public void saveOrUpdate(CustomerAlias customerAlias) {
		if (customerAliasDAO == null) {
			customerAliasDAO = (CustomerAliasDAO) AirCustomerModuleUtils.getDAO(CustomAliasConstants.CUSTOMER_ALIAS_DAO);
		}
		customerAliasDAO.saveOrUpdate(customerAlias);
	}

	@Override
	public void save(CustomerAlias customerAlias) {
		if (customerAliasDAO == null) {
			customerAliasDAO = (CustomerAliasDAO) AirCustomerModuleUtils.getDAO(CustomAliasConstants.CUSTOMER_ALIAS_DAO);
		}
		customerAliasDAO.save(customerAlias);
	}

	@Override
	public List<CustomerAlias> getCustomerAliasList(long customerId) {
		if (customerAliasDAO == null) {
			customerAliasDAO = (CustomerAliasDAO) AirCustomerModuleUtils.getDAO(CustomAliasConstants.CUSTOMER_ALIAS_DAO);
		}
		return customerAliasDAO.getCustomerAliasList(customerId);
	}

	@Override
	public CustomerAlias getCustomerAliasByCustomerAliasId(long customerAliasId, long customerId) {
		if (customerAliasDAO == null) {
			customerAliasDAO = (CustomerAliasDAO) AirCustomerModuleUtils.getDAO(CustomAliasConstants.CUSTOMER_ALIAS_DAO);
		}
		return customerAliasDAO.getCustomerAliasByCustomerAliasId(customerAliasId, customerId);
	}

	@Override
	public void removeCustomerAlias(String customerAliasId) {
		// TODO Auto-generated method stub

	}
}
