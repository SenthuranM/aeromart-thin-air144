package com.isa.thinair.aircustomer.core.persistence.dao;

import java.util.List;

import com.isa.thinair.aircustomer.api.model.CustomerAlias;

public interface CustomerAliasDAO {

	/**
	 * Save or update Customer Alias data.
	 * 
	 * @param customerAlias
	 *            the CustomerAlias
	 */
	public void saveOrUpdate(CustomerAlias customerAlias);

	/**
	 * Get all Customer Alias.
	 * 
	 * @return a collection
	 */
	public List<CustomerAlias> getCustomerAliasList(long customerId);

	/**
	 * Get a given Customer Alias.
	 * 
	 * @param customerAliasId
	 * @return the CustomerAlias
	 */
	public CustomerAlias getCustomerAliasByCustomerAliasId(long customerAliasId, long customerId);

	/**
	 * Get a given Customer Alias.
	 * 
	 * @param customerAliasId
	 * @return the CustomerAlias
	 */
	public void save(CustomerAlias customerAlias);

	/**
	 * Remove given CustomerAlias.
	 * 
	 * @param customerAliasId
	 */
	public void removeCustomerAlias(String customerAliasId);

}
