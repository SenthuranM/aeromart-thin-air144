/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.persistence.dao;

import java.util.List;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;

/**
 * LoyaltyCustomerDAO interface
 * 
 * @author Chamindap
 * 
 */
public interface LoyaltyCustomerDAO {

	/**
	 * Save or update loyalty customer data.
	 * 
	 * @param loyaltyCustomer
	 *            the customer
	 */
	public void saveOrUpdate(LoyaltyCustomerProfile loyaltyCustomer);

	/**
	 * Remove given customer.
	 * 
	 * @param accountNo
	 */
	public void removeLoyaltyCustomer(String accountNo);

	/**
	 * Get all customers.
	 * 
	 * @return a collection
	 */
	public List<LoyaltyCustomerProfile> getLoyaltyCustomers();

	/**
	 * Get a given customer.
	 * 
	 * @param customerId
	 * @return the customer
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomerByCustomerId(int customerId);

	/**
	 * 
	 * @param accountNo
	 * @return
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomer(String accountNo);

	/**
	 * 
	 * @param creteria
	 * @param startRec
	 * @param pageSize
	 * @return
	 */
	public Page getLoyaltyCustomers(List<ModuleCriterion> criteria, int startRec, int pageSize);

	/**
	 * 
	 * @param loyaltyCustomer
	 * @return
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomerByExample(LoyaltyCustomerProfile loyaltyCustomer);

	public String getLoyaltyAccountNo(Integer customerId);
}