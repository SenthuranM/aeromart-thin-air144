package com.isa.thinair.aircustomer.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;

/**
 * @author chethiya
 *
 */
@Local
public interface LmsMemberServiceLocalDelegateImpl extends LmsMemberServiceBD{

}
	