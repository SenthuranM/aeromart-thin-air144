/**
 * 
 */
package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.SQLQuery;

import com.isa.thinair.aircustomer.api.dto.external.AgentFrequentCustomerDTO;
import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.aircustomer.core.persistence.dao.AgentFrequentCustomerDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author suneth
 * @isa.module.dao-impl dao-name="agentFrequentCustomerDAO"
 */
public class AgentFrequentCustomerDAOImpl extends PlatformHibernateDaoSupport implements AgentFrequentCustomerDAO {

	public void saveOrUpdate(AgentFrequentCustomer frequentCustomer) {
		hibernateSaveOrUpdate(frequentCustomer);
	}

	public AgentFrequentCustomer getFrequentCustomer(Integer agentFrequentCustomerId) {
		AgentFrequentCustomer frequentCustomer = null;
		List<AgentFrequentCustomer> list = null;

		String hql = "select frequentCustomer from AgentFrequentCustomer as frequentCustomer where frequentCustomer.frequentCustomerId=?";
		Object[] parmas = { agentFrequentCustomerId };
		list = find(hql, parmas, AgentFrequentCustomer.class);

		if (list.size() != 0) {
			frequentCustomer = (AgentFrequentCustomer) list.get(0);
		}

		return frequentCustomer;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Page searchFrequentCustomers(String firstName, String lastName, String email, String agentCode) throws ModuleException {

		Collection<AgentFrequentCustomerDTO> agentFrequentCustomerDTOList = new ArrayList<AgentFrequentCustomerDTO>();

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT * FROM T_FREQUENT_CUSTOMER frequentCustomer WHERE ");

		sb.append("frequentCustomer.AGENT_CODE = :agentCode ");

		if (!firstName.equals("")) {
			sb.append("AND lower(frequentCustomer.FIRST_NAME) like :firstName ");
		}

		if (!lastName.equals("")) {
			sb.append("AND lower(frequentCustomer.LAST_NAME) like :lastName ");
		}

		if (!email.equals("")) {
			sb.append("AND frequentCustomer.EMAIL like :email ");
		}

		SQLQuery query = getSession().createSQLQuery(sb.toString());

		query.setParameter("agentCode", agentCode);

		if (!firstName.equals("")) {
			query.setParameter("firstName", firstName + "%");
		}

		if (!lastName.equals("")) {
			query.setParameter("lastName", lastName + "%");
		}

		if (!email.equals("")) {
			query.setParameter("email", email + "%");
		}

		List<AgentFrequentCustomer> result = query.addEntity(AgentFrequentCustomer.class).list();

		for (AgentFrequentCustomer agentFrequentCustomer : result) {

			agentFrequentCustomerDTOList.add(agentFrequentCustomer.toAgentFrequentCustomerDTO());

		}

		int recordCnt = (agentFrequentCustomerDTOList == null) ? 0 : agentFrequentCustomerDTOList.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, agentFrequentCustomerDTOList);

	}

	public void deleteFrequentCustomer(AgentFrequentCustomer frequentCustomer) {
		delete(frequentCustomer);
	}
}
