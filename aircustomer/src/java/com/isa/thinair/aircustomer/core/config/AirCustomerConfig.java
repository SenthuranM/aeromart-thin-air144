/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.config;

import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Chamindap
 * @isa.module.config-bean
 */
public class AirCustomerConfig extends DefaultModuleConfig {

	private Properties confirmProperties;

	/**
	 * 
	 */
	public AirCustomerConfig() {
		super();
	}

	/**
	 * @return Returns the confirmProperties.
	 */
	public Properties getConfirmProperties() {
		return confirmProperties;
	}

	/**
	 * @param confirmProperties
	 *            The confirmProperties to set.
	 */
	public void setConfirmProperties(Properties confirmProperties) {
		this.confirmProperties = confirmProperties;
	}
}