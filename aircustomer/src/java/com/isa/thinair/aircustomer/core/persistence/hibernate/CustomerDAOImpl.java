/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMeal;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredSeat;
import com.isa.thinair.aircustomer.api.model.CustomerSession;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author Chamindap
 * @isa.module.dao-impl dao-name="customerDAO"
 */
public class CustomerDAOImpl extends PlatformHibernateDaoSupport implements CustomerDAO {

	/**
	 * Default constructure
	 */
	public CustomerDAOImpl() {
		super();
	}

	/**
	 * @see com.isa.thinair.customer.core.persistence.dao.CustomerDAO#
	 *      saveCustomer(com.isa.thinair.customer.api.model.Customer)
	 */
	public void saveOrUpdate(Customer customer) {
		// try {
		customer.setEmailId(customer.getEmailId().toUpperCase());
		super.hibernateSaveOrUpdate(customer);
		// } catch (RuntimeException e) {

		// }
	}

	/**
	 * Get all customers.
	 * 
	 * @return a collection
	 */
	public List<Customer> getCustomers() {
		return find("from Customer", Customer.class);
	}

	/**
	 * Get a given customer.
	 * 
	 * @param customerId
	 * @return the customer
	 */
	public Customer getCustomer(int customerId) {
		Integer cusId = new Integer(customerId);
		return get(Customer.class, cusId);	
	}

	/**
	 * @see com.isa.thinair.aircustomer.core.persistence.dao .CustomerDAO#authenticate(java.lang.String,
	 *      java.lang.String)
	 */
	public Customer authenticate(String loginId, String password) {
		Customer customer = null;
		List<Customer> list = null;

		String hql = "select customer from Customer as customer where upper(customer.emailId)=? and customer.password=? and customer.status= ?";
		Object[] parmas = { loginId.toUpperCase(), password, "C" };
		list = find(hql, parmas, Customer.class);

		if (list.size() != 0) {
			customer = (Customer) list.get(0);
		}

		return customer;
	}

	/**
	 * @see com.isa.thinair.aircustomer.core.persistence.dao .CustomerDAO#getCustomer(java.lang.String)
	 */

	public Customer getCustomer(String loginId) {
		Customer customer = null;
		List<Customer> list = null;

		list = find("Select customer " + "from Customer as customer where upper(customer.emailId)=?",
				loginId.toUpperCase(), Customer.class);

		if (list.size() != 0) {
			customer = (Customer) list.get(0);
		}

		return customer;
	}

	public SocialCustomer getSocialCustomer(String socialCustomerId, String socialSiteType) {
		SocialCustomer socialCustomer = null;
		List<SocialCustomer> customerList = null;

		String hql = "select customer from SocialCustomer customer where customer.socialSiteCustId=? and customer.socialCustomerTypeId=? ";
		Object[] parmas = { socialCustomerId, socialSiteType };
		customerList = find(hql, parmas, SocialCustomer.class);

		if (customerList.size() != 0) {
			socialCustomer = customerList.get(0);
		}

		return socialCustomer;
	}

	/**
	 * Returns the password for the user.
	 * 
	 * @param loginId
	 * @return .
	 * @see com.isa.thinair.aircustomer.core.persistence.dao .CustomerDAO#getForgotPassword(java.lang.String)
	 */
	public Customer getForgotPassword(String loginId, String question, String answer) {
		Customer customer = null;

		List<Customer> list = null;

		if (!question.equals("") && !answer.equals("")) {
			Object ob[] = new Object[3];
			ob[0] = loginId.toUpperCase();
			ob[1] = question;
			ob[2] = answer.toUpperCase();

			list = find(
					"select customer" + " from Customer as customer where upper(customer.emailId)=? "
							+ "and customer.secretQuestion =? and upper(customer.secretAnswer)=?", ob, Customer.class);
		} else {
			Object ob[] = new Object[1];
			ob[0] = loginId.toUpperCase();
			list = find("select customer from Customer as customer where upper(customer.emailId)=?", ob, Customer.class);
		}

		if (list.size() != 0) {
			customer = (Customer) list.get(0);
		}

		return customer;
	}


	public boolean isTokenExists(String token) {

		String query = " select count(*) from CustomerSession cs where cs.tokenNumber = :tokenNumber ";
		Long count = (Long) getSession().createQuery(query).setString("tokenNumber", token).uniqueResult();

		return count > 0;
	}

	public CustomerSession loadCustomerSession(String token) {
		String query = " select cs from CustomerSession cs where cs.tokenNumber = :tokenNumber ";
		CustomerSession customerSession = (CustomerSession) getSession().createQuery(query).setString("tokenNumber", token)
				.uniqueResult();

		return customerSession;
	}

	public void createCustomerSession(int customerId, String token) {
		CustomerSession customerSession = new CustomerSession();
		customerSession.setCustomerId(customerId);
		customerSession.setTokenNumber(token);
		customerSession.setCreatedTime(new Date());

		hibernateSave(customerSession);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Customer> getCustomerByPartialEmail(String partialEmail, int start, int pageSize) {

		partialEmail = partialEmail.toUpperCase();
		String countHql = "select count(*) from Customer where upper(emailId) like :partialEmail ORDER BY customerId";
		Query countQuery = getSession().createQuery(countHql).setString("partialEmail", "%" + partialEmail + "%");
		Long count = (Long)countQuery.uniqueResult();
		String hql = "from Customer where upper(emailId) like :partialEmail ORDER BY customerId";
		Query query = getSession().createQuery(hql).setString("partialEmail", "%" + partialEmail + "%");		
		query.setFirstResult(start);
		query.setMaxResults(pageSize);
		List<Customer> records = query.list();
		return new Page<Customer>(count.intValue(), start, start + records.size(), records);
	}

	@Override
	public Page<Customer> getCustomerByEmail(String email) {

		List<Customer> records = find("from Customer where upper(emailId) =?",email.toUpperCase(),Customer.class);
		return new Page<Customer>(1, 0, records.size(), records);
	}

	public List<SeatType> getPreferenceSeatTypes(){
		
		List<SeatType> seatTypes = null;
		seatTypes = find("select seatType from SeatType seatType where seatType.status='ACT'", SeatType.class);
		return seatTypes;
		
	}
	
	public void setCustomerPreferredSeat(CustomerPreferredSeat customerPreferredSeat){
		
		String query = "delete CustomerPreferredSeat where customerId = " + customerPreferredSeat.getCustomerId();
		getSession().createQuery(query).executeUpdate();
		if(customerPreferredSeat.getSeatType()!=null) {
            hibernateSave(customerPreferredSeat);
        }
		
	}
	
	public void setCustomerPreferredMeals(List<CustomerPreferredMeal> customerPreferredMealList){
		String query = "delete CustomerPreferredMeal where id.customerId = " + customerPreferredMealList.get(0).getId().getCustomerId();
		getSession().createQuery(query).executeUpdate();
		hibernateSaveOrUpdateAll(customerPreferredMealList);
	}
	
	public String getCustomerPreferredSeatType(int customerId){
		CustomerPreferredSeat customerPreferredSeat = get(CustomerPreferredSeat.class, customerId);
		if(customerPreferredSeat != null){
			return customerPreferredSeat.getSeatType();
		}
		return null;		
	}
	
	public List<CustomerPreferredMealDTO> getCustomerPreferredMeals(int customerId){
		
		List<CustomerPreferredMeal> customerPreferredMealList;
		List<CustomerPreferredMealDTO> customerPreferredMealDTOList = new ArrayList<CustomerPreferredMealDTO>();
		//String mealList= "";
		
		String hql = "select customerPreferredMeal from CustomerPreferredMeal customerPreferredMeal where "
						+ "customerPreferredMeal.id.customerId=?";
		Object[] parmas = { customerId };
		customerPreferredMealList = find(hql, parmas, CustomerPreferredMeal.class);
		
		for(CustomerPreferredMeal customerPreferredMeal : customerPreferredMealList){
			//mealList += customerPreferredMeal.getId().getMealCode()  + "_" + customerPreferredMeal.getQuantity() + ",";
			CustomerPreferredMealDTO customerPreferredMealDTO = new CustomerPreferredMealDTO();
			customerPreferredMealDTO.setMealCode(customerPreferredMeal.getId().getMealCode());
			customerPreferredMealDTO.setQuantity(customerPreferredMeal.getQuantity());
			
			customerPreferredMealDTOList.add(customerPreferredMealDTO);
		}
//		
//		if(mealList != ""){
//			mealList = mealList.substring(0, mealList.length()-1);
//		}
		
		return customerPreferredMealDTOList;
	}
	
	public void saveOrUpdate(FamilyMember familyMember){
		hibernateSaveOrUpdate(familyMember);
	}

	public FamilyMember getFamilyMember(String firstName , String lastName, int customerId){
		FamilyMember familyMember = null;
		List<FamilyMember> list = null;

		String hql = "select familyMember from FamilyMember as familyMember where familyMember.customerId=? and familyMember.firstName=? "
						+ "and familyMember.lastName=?";
		Object[] parmas = { customerId , firstName , lastName };
		list = find(hql, parmas, FamilyMember.class);

		if (list.size() != 0) {
			familyMember = (FamilyMember) list.get(0);
		}

		return familyMember;
	}
	
	public FamilyMember getFamilyMember(int familyMemberId){
		Integer id = new Integer(familyMemberId);
		return get(FamilyMember.class, id);
	}
	
	public List<FamilyMember> getFamilyMembers(int customerId){
		
		Integer cusId = new Integer(customerId);
		List<FamilyMember> list = null;

		String hql = "select familyMember from FamilyMember as familyMember where familyMember.customerId=? order by familyMember.familyMemberId";
		Object[] parmas = { cusId };
		list = find(hql, parmas, FamilyMember.class);
		
		return list;
	}
	
	public void removeFamilyMember(FamilyMember familyMember){
		delete(familyMember);
	}
}
