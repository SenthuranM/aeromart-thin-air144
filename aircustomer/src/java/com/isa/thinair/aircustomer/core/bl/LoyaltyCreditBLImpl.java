/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aircustomer.api.model.LoyaltyCredit;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCreditDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author chamindap
 * 
 */
public class LoyaltyCreditBLImpl implements LoyaltyCreditBL {

	/** The CustomerDAO */
	private LoyaltyCreditDAO creditDAO = null;

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * The constructure.
	 */
	public LoyaltyCreditBLImpl() {
		super();
	}

	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCredit> c) throws ModuleException {
		Hashtable<String, Integer> failedField = new Hashtable<String, Integer>();
		Hashtable<String, Hashtable<String, Integer>> failedCredit = new Hashtable<String, Hashtable<String, Integer>>();

		Iterator<LoyaltyCredit> iter = c.iterator();
		if (creditDAO == null) {
			creditDAO = (LoyaltyCreditDAO) AirCustomerModuleUtils.getDAO("loyaltyCreditDAO");
		}
		while (iter.hasNext()) {
			Integer errorCode = -1;
			LoyaltyCredit loyaltyCredit = (LoyaltyCredit) iter.next();
			try {
				creditDAO.saveOrUpdate(loyaltyCredit);
			} catch (CommonsDataAccessException cdae) {
				log.error("Error in saving Loyalty Credit", cdae);
				if ("module.duplicate.key".equals(cdae.getExceptionCode()))
					errorCode = 1;
				else if ("module.insert.error".equals(cdae.getExceptionCode())
						|| "module.update.error".equals(cdae.getExceptionCode()))
					errorCode = 2;
				else if ("module.value.toolarge".equals(cdae.getExceptionCode()))
					errorCode = 3;
				else if ("module.integer.toolarge".equals(cdae.getExceptionCode()))
					errorCode = 4;
				else if ("module.saveconstraint.childerror".equals(cdae.getExceptionCode()))
					errorCode = 5;
			} catch (Exception e) {
				log.error("Error in saving Loyalty Credit", e);
				errorCode = 0;
			}

			if (errorCode > 0) {
				if (errorCode == 1)
					failedField.put("accountNumber", 1);
				else if (errorCode == 5)
					failedField.put("accountNumber", 5);
				else {
					if (loyaltyCredit.getLoyaltyAccountNo() == null || loyaltyCredit.getLoyaltyAccountNo().length() == 0)
						failedField.put("accountNumber", 2);
					else if (loyaltyCredit.getLoyaltyAccountNo().length() > 12)
						failedField.put("accountNumber", 3);
				}
				if (loyaltyCredit.getDateEarn() == null)
					failedField.put("dateEarn", 2);
				if (errorCode == 4)
					failedField.put("credit", 4);
				else if (loyaltyCredit.getCreditEarned() == null)
					failedField.put("credit", 2);
				failedCredit.put(loyaltyCredit.getLoyaltyAccountNo(), failedField);
			}

		}
		return failedCredit;
	}

	public void consumeLoyaltyCreditFromCustomer(BigDecimal amount, String accountNo) throws ModuleException {
		if (creditDAO == null) {
			creditDAO = (LoyaltyCreditDAO) AirCustomerModuleUtils.getDAO("loyaltyCreditDAO");
		}
		List<LoyaltyCredit> l = (List<LoyaltyCredit>) creditDAO.getNonExpireCredit(accountNo);
		ArrayList<LoyaltyCredit> updatedCredits = new ArrayList<LoyaltyCredit>();
		if (l != null) {
			BigDecimal totCredits = BigDecimal.ZERO;
			for (LoyaltyCredit c : l) {
				totCredits = AccelAeroCalculator.add(totCredits, c.getCreditBalance());
			}
			if (totCredits.doubleValue() < amount.doubleValue()) {
				throw new ModuleException("amount.exceeds.loyalty.credit");
			}

			for (LoyaltyCredit c : l) {
				if (amount.doubleValue() > 0) {
					BigDecimal credit = c.getCreditBalance();
					amount = AccelAeroCalculator.subtract(amount, credit);
					if (amount.doubleValue() < 0)
						c.setCreditBalance(amount.negate());
					else
						c.setCreditBalance(BigDecimal.ZERO);
					updatedCredits.add(c);
				} else
					break;

			}
		}
		if (updatedCredits.size() > 0)
			saveAll(updatedCredits);
	}
}