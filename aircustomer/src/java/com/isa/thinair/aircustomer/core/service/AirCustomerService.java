/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Chamindap
 * @isa.module.service-interface module-name="aircustomer" description="air customer module"
 */
public class AirCustomerService extends DefaultModule {

	/**
	 * 
	 */
	public AirCustomerService() {
		super();
	}

}
