/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.bl;

import java.util.Collection;
import java.util.Hashtable;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Haider
 * 
 */
public interface LoyaltyCustomerBL {

	/**
	 * Method to save a list of Loyalty customers .
	 * 
	 * @param customer
	 *            the customer.
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCustomerProfile> customers) throws ModuleException;
}