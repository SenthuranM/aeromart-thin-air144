/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.persistence.dao;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.LoyaltyCredit;

/**
 * LoyaltyCreditDAO interface
 * 
 * @author Haider
 * 
 */
public interface LoyaltyCreditDAO {

	/**
	 * Save or update loyalty customer data.
	 * 
	 * @param loyaltyCustomer
	 *            the customer
	 */
	public void saveOrUpdate(LoyaltyCredit loyaltyCredit);

	public BigDecimal getTotalNonExpireCredit(String accountNo);

	public List<LoyaltyCredit> getNonExpireCredit(String accountNo);
}