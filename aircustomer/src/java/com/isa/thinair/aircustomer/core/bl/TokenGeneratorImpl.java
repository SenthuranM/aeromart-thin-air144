package com.isa.thinair.aircustomer.core.bl;

import java.util.UUID;

import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO;

public class TokenGeneratorImpl implements TokenGenerator {


	public String generateToken() {
		UUID uuid;
		String strUuid;

		CustomerDAO customerDAO = AirCustomerModuleUtils.getCustomerDAO();

		while (true) {
			uuid = UUID.randomUUID();
			strUuid = uuid.toString();

			if (!customerDAO.isTokenExists(strUuid)) {
				break;
			}
		}

		return strUuid;
	}
}
