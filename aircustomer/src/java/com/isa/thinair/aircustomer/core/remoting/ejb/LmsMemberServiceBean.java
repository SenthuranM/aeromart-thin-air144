package com.isa.thinair.aircustomer.core.remoting.ejb;

import java.util.Collection;
import java.util.Locale;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.core.bl.LmsMemberBLImpl;
import com.isa.thinair.aircustomer.core.service.bd.LmsMemberServiceDelegateImpl;
import com.isa.thinair.aircustomer.core.service.bd.LmsMemberServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author chethiya
 *
 */
@Stateless
@LocalBinding(jndiBinding = "LmsMemberService.local")
@RemoteBinding(jndiBinding = "LmsMemberService.remote")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered

public class LmsMemberServiceBean extends PlatformBaseSessionBean implements LmsMemberServiceDelegateImpl,LmsMemberServiceLocalDelegateImpl{

	private final Log log = LogFactory.getLog(getClass());

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#getLmsMembers()
	 */
	@Override
	public Collection <LmsMember> getLmsMembers() throws ModuleException {
		
		Collection <LmsMember> lmsMemberList= null;
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		
		try {
			lmsMemberList = lmsRegistrar.getLmsMembers();
		} catch (CommonsDataAccessException lmsEx) {
			log.error("saveOrUpdate customer module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
		return lmsMemberList;
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#setPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public void setPassword(String password, String emailId)	throws ModuleException {
		
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		
		try {
			lmsRegistrar.setPassword(password, emailId);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("setPassword lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#getLmsMember(java.lang.String)
	 */
	@Override
	public LmsMember getLmsMember(String emailId) throws ModuleException {
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		LmsMember lmsMember = null;
		
		try {
			lmsMember = lmsRegistrar.getLmsMember(emailId);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("getLmsMember lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		return lmsMember;
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#getLmsMemberBySBId(int)
	 */
	@Override
	public LmsMember getLmsMemberBySBId(int sbInternalId) throws ModuleException {
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		LmsMember lmsMember = null;
		
		try {
			lmsMember = lmsRegistrar.getLmsMemberBySBId(sbInternalId);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("getLmsMemberBySBId lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		return lmsMember;
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#getLmsMemberById(int)
	 */
	@Override
	public LmsMember getLmsMemberById(int lmsMemberId) throws ModuleException {
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		LmsMember lmsMember = null;
		
		try {
			lmsMember = lmsRegistrar.getLmsMemberById(lmsMemberId);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("getLmsMemberById lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		return lmsMember;
	}

	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#saveOrUpdate(com.isa.thinair.aircustomer.api.model.LmsMember)
	 */
	@Override
	public void saveOrUpdate(LmsMember lmsMember) throws ModuleException {
		
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		
		try {
			lmsRegistrar.saveOrUpdate(lmsMember);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("saveOrUpdate lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
	}
	
	/** (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD#mergeLmsMember(com.isa.thinair.aircustomer.api.model.LmsMember, java.lang.String)
	 */
	@Override
	public void confirmMergeLmsMember(LmsMember lmsMember, String emailId, String carrierCode, Locale locale,
			boolean isServiceAppRQ) throws ModuleException {
		
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		
		try {
			lmsRegistrar.confirmMergeLms(lmsMember, emailId, carrierCode, locale, isServiceAppRQ);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("mergeLmsMember lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
	}

	@Override
	public int mergeLmsMember(String validationString, String emailId) throws ModuleException {
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		
		try {
			return lmsRegistrar.mergeLmsMember( validationString, emailId);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("mergeLmsMember lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
	}

	@Override
	public int numberOfMerges(String validationString, String emailId) throws ModuleException{
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		
		try {
			return lmsRegistrar.numberOfMerges( validationString, emailId);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("mergeLmsMember lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
	}

	@Override
	public boolean register(LmsMember lmsMember, String carrierCode, Locale locale, boolean isServiceAppRQ)
			throws ModuleException {
		LmsMemberBLImpl lmsRegistrar = new LmsMemberBLImpl();
		boolean confMailSent = false;
		try {
			confMailSent = lmsRegistrar.register(lmsMember, carrierCode, locale, isServiceAppRQ);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("mergeLmsMember lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
		return confMailSent;
	}

	@Override
	public String getCrossProtalLoginUrl(String ffid, String menuIntRef) throws ModuleException {
		LmsMemberBLImpl lmsMemberBLImpl = new LmsMemberBLImpl();
		return lmsMemberBLImpl.getCrossPortalLoginUrl(ffid, menuIntRef);
	}

	@Override
	public String getVerificationString(String ffid) throws ModuleException {
		LmsMemberBLImpl lmsMemberBLImpl = new LmsMemberBLImpl();
		return lmsMemberBLImpl.getVerificationString(ffid);
	}
	
	@Override
	public boolean resendLmsConfEmail(LmsMember lmsMember, String validationString, String carrierCode, boolean available,
			Locale locale, boolean isServiceAppRQ) throws ModuleException {
		
		boolean confMailSent = false;
		try {		
			if(available){
				confMailSent = LmsMemberBLImpl.sendLMSConfAsEmail(validationString, lmsMember, LmsMemberBLImpl.MERGE_TOPIC,
						carrierCode, locale, isServiceAppRQ);
			}else{
				confMailSent = LmsMemberBLImpl.sendLMSConfAsEmail(validationString, lmsMember, LmsMemberBLImpl.CONFIRM_TOPIC,
						carrierCode, locale, isServiceAppRQ);
			}
		} catch (CommonsDataAccessException lmsEx) {
			log.error("mergeLmsMember lmsMember module failed.", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
		return confMailSent;		
		
	}
	
	public boolean sendLMSMergeSuccessEmail(LmsMember lmsMember, String carrierCode) throws ModuleException {
		
		boolean confMailSent = false;
		try {
			confMailSent = LmsMemberBLImpl.sendLMSMergeSuccessEmail(lmsMember, carrierCode);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("Sending merge success email failed", lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}
		
		return confMailSent;		
		
	}

	public boolean syncLmsMember(String marketingAirLineCode, String memberFFID) throws ModuleException {
		boolean syncSuccess = false;
		try {
			LmsMemberBLImpl lmsMemberBLImpl = new LmsMemberBLImpl();
			syncSuccess = lmsMemberBLImpl.syncLmsMember(marketingAirLineCode, memberFFID);
		} catch (CommonsDataAccessException lmsEx) {
			log.error("Sync LMS member failed from marketingAirLineCode: " + marketingAirLineCode + " memberFFID: " + memberFFID,
					lmsEx);
			throw new ModuleException(lmsEx, lmsEx.getExceptionCode(), lmsEx.getModuleCode());
		}

		return syncSuccess;

	}
}
