package com.isa.thinair.aircustomer.api.dto.lms;

import java.io.Serializable;

/**
 * DTO to hold point details related to loyalty member
 * 
 * @author rumesh
 * 
 */
public class LoyaltyPointDetailsDTO extends LoyaltyBaseDTO implements Serializable {
	private static final long serialVersionUID = -9218922743768268298L;

	private double memberPointsEarned;

	private double memberPointsDeducted;

	private double memberPointsUsed;

	private double memberPointsExpired;

	private double memberPointsAvailable;

	private double memberPointsLocked;

	public double getMemberPointsEarned() {
		return memberPointsEarned;
	}

	public void setMemberPointsEarned(double memberPointsEarned) {
		this.memberPointsEarned = memberPointsEarned;
	}

	public double getMemberPointsDeducted() {
		return memberPointsDeducted;
	}

	public void setMemberPointsDeducted(double memberPointsDeducted) {
		this.memberPointsDeducted = memberPointsDeducted;
	}

	public double getMemberPointsUsed() {
		return memberPointsUsed;
	}

	public void setMemberPointsUsed(double memberPointsUsed) {
		this.memberPointsUsed = memberPointsUsed;
	}

	public double getMemberPointsExpired() {
		return memberPointsExpired;
	}

	public void setMemberPointsExpired(double memberPointsExpired) {
		this.memberPointsExpired = memberPointsExpired;
	}

	public double getMemberPointsAvailable() {
		return memberPointsAvailable;
	}

	public void setMemberPointsAvailable(double memberPointsAvailable) {
		this.memberPointsAvailable = memberPointsAvailable;
	}

	public double getMemberPointsLocked() {
		return memberPointsLocked;
	}

	public void setMemberPointsLocked(double memberPointsLocked) {
		this.memberPointsLocked = memberPointsLocked;
	}

}
