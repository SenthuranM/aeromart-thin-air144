/**
 * 
 */
package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_CUSTOMER_MEAL"
 * CustomerPreferredMeal is the entity class that is used to store customer meal preferences
 * @author suneth
 *
 */
public class CustomerPreferredMeal implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6705215003827512314L;
	
	private CustomerPreferredMealPK id; // this is the object for the composite key of T_CUSTOMER_MEAL table.
	
	private int quantity;

	/**
	 * @return the quantity
	 * @hibernate.id generator-class = "assigned" type="com.isa.thinair.aircustomer.api.model.CustomerPreferredMealPK" 
	 */
	public CustomerPreferredMealPK getId() {
		return id;
	}

	public void setId(CustomerPreferredMealPK id) {
		this.id = id;
	}

	/**
	 * @return the quantity
	 * @hibernate.property column = "QUANTITY"
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}	

}
