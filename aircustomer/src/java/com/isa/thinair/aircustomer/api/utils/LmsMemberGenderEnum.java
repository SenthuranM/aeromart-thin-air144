package com.isa.thinair.aircustomer.api.utils;

import java.io.Serializable;

import com.isa.thinair.airreservation.api.utils.AllocateEnum;

/**
 * @author chethiya
 *
 */
public class LmsMemberGenderEnum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8559031205690206506L;
	private int code;

	private LmsMemberGenderEnum(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public boolean equals(AllocateEnum allocateEnum) {
		return (allocateEnum.getCode() == this.getCode());
	}

	public int getCode() {
		return code;
	}

	public static final LmsMemberGenderEnum UNDEFINED = new LmsMemberGenderEnum(0);
	public static final LmsMemberGenderEnum MALE = new LmsMemberGenderEnum(1);
	public static final LmsMemberGenderEnum FEMALE = new LmsMemberGenderEnum(2);

}
