package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_SOCIAL_CUSTOMER_TYPE"
 * 
 */
public class SocialCustomerType implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer socialCustomerTypeId;

	private String socialCustomerTypeName;

	private String status; // TODO:

	/**
	 * @return Returns the socialCustomerTypeId.
	 * @hibernate.id column = "SOCIAL_CUSTOMER_TYPE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SOCIAL_CUSTOMER_TYPE"
	 */
	public Integer getSocialCustomerTypeId() {
		return socialCustomerTypeId;
	}

	/**
	 * 
	 * @param socialCustomerTypeId
	 */
	public void setSocialCustomerTypeId(Integer socialCustomerTypeId) {
		this.socialCustomerTypeId = socialCustomerTypeId;
	}

	/**
	 * @return Returns the socialCustomerTypeName
	 * @hibernate.property column = "SOCIAL_CUSTOMER_TYPE_NAME"
	 */
	public String getSocialCustomerTypeName() {
		return socialCustomerTypeName;
	}

	/**
	 * 
	 * @param socialCustomerTypeName
	 */
	public void setSocialCustomerTypeName(String socialCustomerTypeName) {
		this.socialCustomerTypeName = socialCustomerTypeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
