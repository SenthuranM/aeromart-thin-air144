package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_CUSTOMER_ALIAS" CustomerAlias is the entity class to repesent a CustomerAlias model
 * @author rahul
 * 
 */

public class CustomerAlias extends Persistent implements Serializable {

	private static final long serialVersionUID = -4797750734748570720L;

	/** Holds the customerAliasId */
	private long customerAliasId;

	/** Holds the customer reference */
	private long customerId;

	/** Holds the alias */
	private String alias;

	/** Holds the credit card number last digits */
	private String noLastDigits;

	/** Holds credit card holder name */
	private String holderName;

	/** Holds credit card expiry date (YYMM) format */
	private Date expiryDate;

	/** Holds the status */
	private String status;

	/** Holds the createdDate (YYMM) format */
	private Date createDate;

	/** Holds the credit card name */
	private String ccName;

	/**
	 * @return the customerAliasId
	 * 
	 * @hibernate.id column="CUSTOMER_ALIAS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CUSTOMER_ALIAS"
	 */
	public long getCustomerAliasId() {
		return customerAliasId;
	}

	/**
	 * @param customerAliasId
	 *            the customerAliasId to set
	 */
	public void setCustomerAliasId(long customerAliasId) {
		this.customerAliasId = customerAliasId;
	}

	/**
	 * @return the customerId
	 * 
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the alias
	 * 
	 * @hibernate.property column = "ALIAS"
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param alias
	 *            the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * @return the noLastDigits
	 * 
	 * @hibernate.property column = "CC_LAST_4_DIGITS"
	 */
	public String getNoLastDigits() {
		return noLastDigits;
	}

	/**
	 * @param noLastDigits
	 *            the noLastDigits to set
	 */
	public void setNoLastDigits(String noLastDigits) {
		this.noLastDigits = noLastDigits;
	}

	/**
	 * @return the holderName
	 * 
	 * @hibernate.property column = "CC_HOLDER_NAME"
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName
	 *            the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return the eDate
	 * 
	 * @hibernate.property column = "CC_EXPIRY"
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the status
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the createDate
	 * 
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the ccName
	 * 
	 * @hibernate.property column = "CC_NAME"
	 */
	public String getCcName() {
		return ccName;
	}

	/**
	 * @param ccName
	 *            the ccName to set
	 */
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
}