/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.api.service;

import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Haider
 */
public interface LoyaltyCustomerServiceBD {

	public static final String SERVICE_NAME = "LoyaltyCustomerService";

	/**
	 * Handles save and update of customer object.
	 * 
	 * @param loyaltyCustomer
	 *            the loyalty customer.
	 * @throws ModuleException
	 */
	public void saveOrUpdate(LoyaltyCustomerProfile loyaltyCustomer) throws ModuleException;

	/**
	 * Handles save and update a list of object.
	 * 
	 * @param loyaltyCustomer
	 *            the loyalty customer.
	 * @throws ModuleException
	 */
	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCustomerProfile> loyaltyCustomer)
			throws ModuleException;

	/**
	 * Get a loyalty customer.
	 * 
	 * @param customerId
	 *            the customer id link with loyalty customer.
	 * @return the LoyaltyCustomerProfile
	 * @throws ModuleException
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomerByCustomerId(int customerId) throws ModuleException;

	/**
	 * Get a customer by account number.
	 * 
	 * @param accountNo
	 * @return the LoyaltyCustomerProfile
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomer(String accountNo) throws ModuleException;

	/**
	 * Get a customer by account number.
	 * 
	 * @param accountNo
	 * @return the LoyaltyCustomerProfile
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomerByExample(LoyaltyCustomerProfile loyaltyCustomer) throws ModuleException;

	/**
	 * Get all Loyalty customers.
	 * 
	 * @return a collection
	 * @throws ModuleException
	 */
	public List<LoyaltyCustomerProfile> getLoyaltyCustomers() throws ModuleException;

	public String getLoyaltyAccountNo(Integer customerId) throws ModuleException;

}