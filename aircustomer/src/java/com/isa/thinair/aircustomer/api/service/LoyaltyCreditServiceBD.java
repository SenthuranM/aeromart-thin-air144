/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.api.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Hashtable;

import com.isa.thinair.aircustomer.api.model.LoyaltyCredit;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Haider
 */
public interface LoyaltyCreditServiceBD {

	public static final String SERVICE_NAME = "LoyaltyCreditService";

	/**
	 * Handles save and update a list of object.
	 * 
	 * @param loyaltyCustomer
	 *            the loyalty customer.
	 * @throws ModuleException
	 */
	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCredit> loyaltyCustomer) throws ModuleException;

	/**
	 * Get total Loyalty non expire credit for given account number .
	 * 
	 * @return a collection
	 * @throws ModuleException
	 */
	public BigDecimal getTotalNonExpireCredit(String accountNo) throws ModuleException;

	/**
	 * Get total Loyalty non expire credit for given account number .
	 * 
	 * @return a collection
	 * @throws ModuleException
	 */
	public void consumeLoyaltyCreditFromCustomer(BigDecimal amount, String accountNo) throws ModuleException;
}