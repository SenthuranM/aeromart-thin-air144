/**
 * 
 */
package com.isa.thinair.aircustomer.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;


/**
 * 
 * @hibernate.class table = "T_FAMILY_MEMBER" FamilyMember is the entity class to repesent a FamilyMember model
 * @author suneth
 *
 */
public class FamilyMember extends Persistent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2632665121448582630L;

	private Integer familyMemberId;
	
	private Integer customerId;
	
	private String title;
	
	private String firstName;
	
	private String lastName;
	
	private String relationshipId;
	
	private int nationalityCode;
	
	private Date dateOfBirth;

	/**
	 * @hibernate.id column = "FAMILY_MEMBER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FAMILY_MEMBER"
	 * @return the familyMemberId
	 */
	public Integer getFamilyMemberId() {
		return familyMemberId;
	}

	/**
	 * @param familyMemberId the familyMemberId to set
	 */
	public void setFamilyMemberId(Integer familyMemberId) {
		this.familyMemberId = familyMemberId;
	}

	/**
	 * @hibernate.property column = "CUSTOMER_ID"
	 * @return the customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	
	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @hibernate.property column = "FIRST_NAME"
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @hibernate.property column = "LAST_NAME"
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @hibernate.property column = "FAMILY_RELATIONSHIP_ID"
	 * @return the relationshipId
	 */
	public String getRelationshipId() {
		return relationshipId;
	}

	/**
	 * @param relationshipId the relationshipId to set
	 */
	public void setRelationshipId(String relationshipId) {
		this.relationshipId = relationshipId;
	}

	/**
	 * @hibernate.property column = "NATIONALITY_CODE"
	 * @return the nationalityCode
	 */
	public int getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode the nationalityCode to set
	 */
	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
		
}
