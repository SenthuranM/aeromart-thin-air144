package com.isa.thinair.aircustomer.api.dto.lms;

import java.io.Serializable;

/**
 * Holds loyalty member status attributes
 * 
 * @author rumesh
 * 
 */
public class LoyaltyMemberStateDTO extends LoyaltyBaseDTO implements Serializable {

	private static final long serialVersionUID = 3700383596121427863L;

	private boolean loyaltyMember;

	private boolean memberSuspended;

	public boolean isLoyaltyMember() {
		return loyaltyMember;
	}

	public void setLoyaltyMember(boolean loyaltyMember) {
		this.loyaltyMember = loyaltyMember;
	}

	public boolean isMemberSuspended() {
		return memberSuspended;
	}

	public void setMemberSuspended(boolean memberSuspended) {
		this.memberSuspended = memberSuspended;
	}

}
