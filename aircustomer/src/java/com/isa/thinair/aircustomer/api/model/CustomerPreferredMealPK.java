package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * This class is used as the composite key for the CustomerPreferredMeal model class for the T_CUSTOMER_MEAL table 
 * @author suneth
 *
 */
public class CustomerPreferredMealPK implements Serializable{

	private int customerId; 
		
	private String mealCode = null;

	public CustomerPreferredMealPK() {
	}

	public CustomerPreferredMealPK(int customerId, String mealCode) {
		this.customerId = customerId;
		this.mealCode = mealCode;
	}

	/**
	 * @return the customerId
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 * 
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the mealId
	 * @hibernate.property column = "MEAL_CODE" 
	 */
	public String getMealCode() {
		return mealCode;
	}
	
	/**
	 * @param mealId the mealId to set
	 */
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
		
	public boolean equals(Object key) {
		boolean result = true;
		if (!(key instanceof CustomerPreferredMealPK)) {
			return false;
		}
		int otherCustomerId = ((CustomerPreferredMealPK) key).getCustomerId();
		String otherMealCode = ((CustomerPreferredMealPK) key).getMealCode();
		if (customerId == 0 || otherCustomerId == 0) {
			result = false;
		} else {
			result = (customerId == otherCustomerId);
		}
		if (mealCode == null || otherMealCode == null || mealCode.equals("") || otherMealCode.equals("")) {
			result = false;
		} else {
			result = (mealCode.equals(otherMealCode));
		}
		return result;
	}
			 
	public int hashCode() {
		int code = 0;
		if (customerId != 0) {
			code += customerId;
		}
		return code;
	}

}