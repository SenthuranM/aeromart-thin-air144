package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * @author Chethiya Palliyaguruge
 * 
 * @hibernate.class table = "T_LMS_MERGE_MEMBER"
 */
public class LmsMemberMerge implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8352011559111418558L;

	private int mergeId;
	
	private String emailId;
	
	private String verificationString;

	/**
	 * @return the mergeId
	 * 
	 * @hibernate.id column = "LMS_MERGE_MEMBER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_LMS_MERGE_MEMBER"
	 */
	public int getMergeId() {
		return mergeId;
	}

	/**
	 * @param mergeId the mergeId to set
	 */
	public void setMergeId(int mergeId) {
		this.mergeId = mergeId;
	}

	/**
	 * @return the emailId
	 * 
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the verificationString
	 * 
	 * @hibernate.property column = "VERIFICATION_STRING"
	 */
	public String getVerificationString() {
		return verificationString;
	}

	/**
	 * @param verificationString the verificationString to set
	 */
	public void setVerificationString(String verificationString) {
		this.verificationString = verificationString;
	}
	
}
