/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.api.service;

import java.util.List;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerAlias;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMeal;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredSeat;
import com.isa.thinair.aircustomer.api.model.CustomerSession;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamindap
 */
public interface AirCustomerServiceBD {

	public static final String SERVICE_NAME = "AirCustomerService";

	/**
	 * Handles save and update of customer object.
	 * @param customer TODO
	 * 
	 * @throws ModuleException
	 */
	public void saveOrUpdate(Customer customer) throws ModuleException;

	/**
	 * Get a given customer.
	 * 
	 * @param customerId
	 *            the customer id.
	 * @return the customer
	 * @throws ModuleException
	 */
	public Customer getCustomer(int customerId) throws ModuleException;

	/**
	 * Get a customer by login.
	 * 
	 * @param loginId
	 * @return the customer
	 */
	public Customer getCustomer(String loginId) throws ModuleException;

	/**
	 * Get the social customer from soical site customer id
	 * 
	 * @param socialCustomerId
	 * @return
	 * @throws ModuleException
	 */
	public SocialCustomer getSocialCustomer(String socialCustomerId, String socialSiteType) throws ModuleException;

	/**
	 * Get all customers.
	 * 
	 * @return a collection
	 * @throws ModuleException
	 */
	public List<Customer> getCustomers() throws ModuleException;

	/**
	 * Get an authentication of a cuatomer.
	 * 
	 * @param loginId
	 *            the loginId.
	 * @param password
	 *            the password.
	 * @return an authentication of a cuatomer.
	 */
	public Customer authenticate(String loginId, String password) throws ModuleException;

	/**
	 * Confirms a customer for registration.
	 * 
	 * @param customerId
	 *            the customer id.
	 * @param customerKey
	 *            the customer key.
	 * @return the confirmation true or false.
	 */
	public boolean confirmCustomer(String customerId, String customerKey) throws ModuleException;

	/**
	 * Registers a customer.
	 * 
	 * @param customer
	 *            the cutomer
	 * @param isServiceAppRQ TODO
	 * @return the customer registration true or false.
	 * @throws ModuleException
	 */
	public boolean registerCustomer(Customer customer, String originCarrierCode, LoyaltyCustomerProfile loyaltyCustomerProfile, boolean isServiceAppRQ)
			throws ModuleException;

	/**
	 * Returns the password for the user.
	 * 
	 * @param loginId
	 * @param question
	 * @param answer
	 * @return
	 * @throws ModuleException
	 */
	public boolean getForgotPassword(String loginId, String question, String answer, String originCarrierCode)
			throws ModuleException;

	/**
	 * @param oldCustomer
	 * @param newCustomer
	 * @param userID
	 * @throws ModuleException
	 */
	public void saveOrUpdateWithName(Customer oldCustomer, Customer newCustomer, String userID) throws ModuleException;

	public void confirmLMSMemberStatus(String customerId) throws ModuleException;

	public String generateToken();

	public CustomerSession loadCustomerSession(String token);

	public void createCustomerSession(int customerId, String token);

	public Page<Customer> getCustomerByPartialEmail(String partialEmail, int start, int pageSize) throws ModuleException;

	public Page<Customer> getCustomerByEmail(String email, int start, int pageSize) throws ModuleException;

	public void resendProfileDetailsMail(String emailId, String originCarrierCode) throws ModuleException;
	
	/**
	 * Send a link to reset password
	 * 
	 * @param loginId
	 * @param question
	 * @param answer
	 * @return
	 * @throws ModuleException
	 */
	public boolean sendPasswordResetLink(String loginId, String question, String answer, String originCarrierCode, String baseUrl)
			throws ModuleException;

	public List<SeatType> getPreferenceSeatTypes();
	
	public void setCustomerPreferredSeat(CustomerPreferredSeat customerPreferredSeat);
	
	public void setCustomerPreferredMeals(List<CustomerPreferredMeal> customerPreferredMealList);
	
	public String getCustomerPreferredSeatType(int customerId);
	
	public List<CustomerPreferredMealDTO> getCustomerPreferredMeals(int customerId);
	
	public void saveOrUpdate(FamilyMember familyMember) throws ModuleException;
	
	public FamilyMember getFamilyMember(String firstName , String lastName, int customerId) throws ModuleException;
	
	public FamilyMember getFamilyMember(int familyMemberId) throws ModuleException;
	
	public List<FamilyMember> getFamilyMembers(int customerId) throws ModuleException;
	
	public void removeFamilyMember(FamilyMember familyMember) throws ModuleException;

	public void saveUpdateAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId, String operation)  throws ModuleException;

	public Page searchAgentFrequentCustomers(String firstName , String lastName , String email, String agentCode) throws ModuleException;
	
	public void deleteAgentFrequentCustomer(AgentFrequentCustomer frequentCustomer, String userId) throws ModuleException;

	/**
	 * Handles save and update of CustomerAlias object.
	 * 
	 * @param CustomerAlias
	 * @throws ModuleException
	 */
	public void saveOrUpdate(CustomerAlias customerAlias) throws ModuleException;

	/**
	 * Get all customer alias in Active State.
	 * 
	 * @return List<CustomerAlias>
	 * @param customerId
	 * @throws ModuleException
	 */
	public List<CustomerAlias> getCustomerAliasList(long customerId) throws ModuleException;

	/**
	 * Handles save and update of customerAlias object.
	 * 
	 * @param customerAlias
	 * @throws ModuleException
	 */
	public void save(CustomerAlias customerAlias) throws ModuleException;

	/**
	 * Get a given Customer Alias.
	 * 
	 * @param customerAliasId
	 * @param customerId
	 * @return the CustomerAlias
	 * @throws ModuleException
	 */
	public CustomerAlias getCustomerAliasByCustomerAliasId(long customerAliasId, long customerId) throws ModuleException;
}