/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 * @version $$Id$$
 */

package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.aircustomer.api.dto.external.CustomerTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.Util;

/**
 * @hibernate.class table = "T_CUSTOMER" Customer is the entity class to repesent a Customer model
 * @author Chamindap
 * 
 */

public class Customer extends Persistent implements Serializable {

	private static final long serialVersionUID = -6432993396709491065L;

	private int customerId;

	private String emailId;

	private String password;

	private String title;

	private String firstName;

	private String lastName;

	private String gender;

	private String alternativeEmailId;

	private String telephone;

	private String mobile;

	private String officeTelephone;

	private Date dateOfBirth;

	private String fax;

	private String addressStreet;

	private String addressLine;

	private String zipCode;

	private String countryCode;

	private Integer nationalityCode;

	private String status;

	private String secretQuestion;

	private String secretAnswer;

	private Calendar registration_date;

	private Calendar confirmation_date;

	private Set<CustomerQuestionaire> customerQuestionaire;

	private Set<SocialCustomer> socialCustomer;

	private String city;

	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnPhoneNo;

	private String emgnEmail;
	
	private char isLmsMember;
	
	private Set<LmsMember> customerLmsDetails;
	
	private char isXBECustomer;
	
	private Boolean sendPromoEmail;

	public Customer() {

	}

	/**
	 * Converts the Customer object to CustomerTO object
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public CustomerTO toCustomerTO() throws ModuleException {
		CustomerTO customerTO = new CustomerTO();
		Util.copyProperties(customerTO, this);

		Set<CustomerQuestionaire> setCustomerQuestionaire = this.getCustomerQuestionaire();

		if (setCustomerQuestionaire != null && setCustomerQuestionaire.size() > 0) {
			CustomerQuestionaire customerQuestionaire;
			for (Iterator<CustomerQuestionaire> iter = setCustomerQuestionaire.iterator(); iter.hasNext();) {
				customerQuestionaire = (CustomerQuestionaire) iter.next();
				customerTO.addCustomerQuestionaireTO(customerQuestionaire.toCustomerQuestionaireTO());
			}
		}

		return customerTO;
	}

	/**
	 * returns the customerId
	 * 
	 * @return Returns the customerId.
	 * @hibernate.id column = "CUSTOMER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CUSTOMER_ID"
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * sets the customerId
	 * 
	 * @param customerId
	 *            The customerId to set.
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * returns the alternativeEmailId
	 * 
	 * @return Returns the alternativeEmailId.
	 * @hibernate.property column = "ALTERNATIVE_EMAIL_ID"
	 */
	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	/**
	 * sets the alternativeEmailId
	 * 
	 * @param alternativeEmailId
	 *            The alternativeEmailId to set.
	 */
	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

	/**
	 * returns the dateOfBirth
	 * 
	 * @return Returns the dateOfBirth.
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * sets the dateOfBirth
	 * 
	 * @param dateOfBirth
	 *            The dateOfBirth to set.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * returns the emailId
	 * 
	 * @return Returns the emailId.
	 * @hibernate.property column = "EMAIL_ID"
	 * 
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * sets the emailId
	 * 
	 * @param emailId
	 *            The emailId to set.
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * returns the fax
	 * 
	 * @return Returns the fax.
	 * @hibernate.property column = "FAX"
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * sets the fax
	 * 
	 * @param fax
	 *            The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * returns the firstName
	 * 
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * sets the firstName
	 * 
	 * @param firstName
	 *            The firstName to set.
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * returns the gender
	 * 
	 * @return Returns the gender.
	 * @hibernate.property column = "GENDER"
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * sets the gender
	 * 
	 * @param gender
	 *            The gender to set.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * returns the lastName
	 * 
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * sets the lastName
	 * 
	 * @param lastName
	 *            The lastName to set.
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * returns the mobile
	 * 
	 * @return Returns the mobile.
	 * @hibernate.property column = "MOBILE"
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * sets the mobile
	 * 
	 * @param mobile
	 *            The mobile to set.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * returns the officeTelephone
	 * 
	 * @return Returns the officeTelephone.
	 * @hibernate.property column = "OFFICE_TELEPHONE"
	 */
	public String getOfficeTelephone() {
		return officeTelephone;
	}

	/**
	 * sets the officeTelephone
	 * 
	 * @param officeTelephone
	 *            The officeTelephone to set.
	 */
	public void setOfficeTelephone(String officeTelephone) {
		this.officeTelephone = officeTelephone;
	}

	/**
	 * returns the password
	 * 
	 * @return Returns the password.
	 * @hibernate.property column = "PASSWORD"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * sets the password
	 * 
	 * @param password
	 *            The password to set.
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * returns the telephone
	 * 
	 * @return Returns the telephone.
	 * @hibernate.property column = "TELEPHONE"
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * sets the telephone
	 * 
	 * @param telephone
	 *            The telephone to set.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * returns the title
	 * 
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * sets the title
	 * 
	 * @param title
	 *            The title to set.
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the addressStreet
	 * 
	 * @hibernate.property column = "STREET_ADDRESS_1"
	 */
	public String getAddressStreet() {
		return addressStreet;
	}

	/**
	 * @param addressStreet
	 *            the addressStreet to set
	 */
	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	/**
	 * @return the addressLine
	 * 
	 * @hibernate.property column = "STREET_ADDRESS_2"
	 */
	public String getAddressLine() {
		return addressLine;
	}

	/**
	 * @param addressLine
	 *            the addressLine to set
	 */
	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	/**
	 * @return the zipCode
	 * 
	 * @hibernate.property column = "ZIP_CODE"
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param country
	 *            The country to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @hibernate.property column = "NATIONALITY_CODE"
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationality
	 *            The nationality to set.
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the secretAnswer.
	 * @hibernate.property column = "SECRET_ANSWER"
	 */
	public String getSecretAnswer() {
		return secretAnswer;
	}

	/**
	 * @param secretAnswer
	 *            The secretAnswer to set.
	 */
	public void setSecretAnswer(String secretAnswer) {
		this.secretAnswer = secretAnswer;
	}

	/**
	 * @return Returns the secretQuestion.
	 * @hibernate.property column = "SECRET_QUESTION"
	 */
	public String getSecretQuestion() {
		return secretQuestion;
	}

	/**
	 * @param secretQuestion
	 *            The secretQuestion to set.
	 */
	public void setSecretQuestion(String secretQuestion) {
		this.secretQuestion = secretQuestion;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="QUES_KEY"
	 * @hibernate.collection-key column="CUSTOMER_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.aircustomer.api.model.CustomerQuestionaire"
	 */
	public Set<CustomerQuestionaire> getCustomerQuestionaire() {
		return customerQuestionaire;
	}

	/**
	 * @param customerQuestionaire
	 *            The customerQuestionaire to set.
	 */
	public void setCustomerQuestionaire(Set<CustomerQuestionaire> customerQuestionaire) {
		this.customerQuestionaire = customerQuestionaire;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="SOCIAL_CUSTOMER_TYPE_ID"
	 * @hibernate.collection-key column="CUSTOMER_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.aircustomer.api.model.SocialCustomer"
	 */
	public Set<SocialCustomer> getSocialCustomer() {
		return socialCustomer;
	}

	/**
	 * 
	 * @param socialCustomer
	 */
	public void setSocialCustomer(Set<SocialCustomer> socialCustomer) {
		this.socialCustomer = socialCustomer;
	}

	/**
	 * @return Returns the secretAnswer.
	 * @hibernate.property column = "CONFIRMED_DATE"
	 */
	public Calendar getConfirmation_date() {
		return confirmation_date;
	}

	public void setConfirmation_date(Calendar confirmation_date) {
		this.confirmation_date = confirmation_date;
	}

	/**
	 * @return Returns the secretAnswer.
	 * @hibernate.property column = "REGISTRATION_DATE"
	 */
	public Calendar getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Calendar registration_date) {
		this.registration_date = registration_date;
	}

	/**
	 * @hibernate.property column = "CITY"
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the emgnTitle
	 * 
	 * @hibernate.property column = "E_TITLE"
	 */
	public String getEmgnTitle() {
		return emgnTitle;
	}

	/**
	 * @param emgnTitle
	 *            the emgnTitle to set
	 */
	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	/**
	 * @return the emgnFirstName
	 * 
	 * @hibernate.property column = "E_FIRST_NAME"
	 */
	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	/**
	 * @param emgnFirstName
	 *            the emgnFirstName to set
	 */
	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	/**
	 * @return the emgnLastName
	 * 
	 * @hibernate.property column = "E_LAST_NAME"
	 */
	public String getEmgnLastName() {
		return emgnLastName;
	}

	/**
	 * @param emgnLastName
	 *            the emgnLastName to set
	 */
	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	/**
	 * @return the emgnPhoneNo
	 * 
	 * @hibernate.property column = "E_PHONE_NO"
	 */
	public String getEmgnPhoneNo() {
		return emgnPhoneNo;
	}

	/**
	 * @param emgnPhoneNo
	 *            the emgnPhoneNo to set
	 */
	public void setEmgnPhoneNo(String emgnPhoneNo) {
		this.emgnPhoneNo = emgnPhoneNo;
	}

	/**
	 * @return the emgnEmail
	 * 
	 * @hibernate.property column = "E_EMAIL"
	 */
	public String getEmgnEmail() {
		return emgnEmail;
	}

	/**
	 * @param emgnEmail
	 *            the emgnEmail to set
	 */
	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}

	/**
	 * @return the isLmsMember
	 * 
	 * @hibernate.property column = "IS_LMS_MEMBER"
	 */
	public char getIsLmsMember() {
		return isLmsMember;
	}

	/**
	 * @param isLmsMember the isLmsMember to set
	 */
	public void setIsLmsMember(char isLmsMember) {
		this.isLmsMember = isLmsMember;
	}

	/**
	 * @return the customerLmsDetails
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="CUSTOMER_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.aircustomer.api.model.LmsMember"
	 */
	public Set<LmsMember> getCustomerLmsDetails() {
		return customerLmsDetails;
	}

	/**
	 * @param customerLmsDetails the customerLmsDetails to set
	 */
	public void setCustomerLmsDetails(Set<LmsMember> customerLmsDetails) {
		this.customerLmsDetails = customerLmsDetails;
	}
	
	public LmsMember getLMSMemberDetails() {
		LmsMember lmsMember = null;
		if (this.customerLmsDetails != null && !this.customerLmsDetails.isEmpty()) {
			lmsMember = this.customerLmsDetails.iterator().next();
		}
		return lmsMember;
	}
	
	public char getIsXBECustomer() {
		return isXBECustomer;
	}

	public void setIsXBECustomer(char isXBECustomer) {
		this.isXBECustomer = isXBECustomer;
	}

	/**
	 * @return the sendPromoEmail
	 * 
	 * @hibernate.property column = "SEND_PROMO_EMAIL" type = "yes_no"
	 * 
	 */
	public Boolean getSendPromoEmail() {
		return sendPromoEmail;
	}

	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}	
}