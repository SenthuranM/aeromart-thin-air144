/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 * @version $$Id$$
 */

package com.isa.thinair.aircustomer.api.dto.external;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public class CustomerTO extends Persistent implements Serializable {

	private static final long serialVersionUID = -4683299087182656579L;

	private int customerId;

	private String emailId;

	private String password;

	private String title;

	private String firstName;

	private String lastName;

	private String gender;

	private String alternativeEmailId;

	private String telephone;

	private String mobile;

	private String officeTelephone;

	private Date dateOfBirth;

	private String fax;

	private String addressStreet;

	private String addressLine;

	private String zipCode;

	private String countryCode;

	private int nationalityCode;

	private String status;

	private String secretQuestion;

	private String secretAnswer;

	private Collection<CustomerQuestionaireTO> customerQuestionaireTO;

	private String emgnTitle;

	private String emgnFirstName;

	private String emgnLastName;

	private String emgnPhoneNo;

	private String emgnEmail;
	
	private Boolean sendPromoEmail;

	/**
	 * returns the customerId
	 * 
	 * @return Returns the customerId.
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * sets the customerId
	 * 
	 * @param customerId
	 *            The customerId to set.
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * returns the alternativeEmailId
	 * 
	 * @return Returns the alternativeEmailId.
	 */
	public String getAlternativeEmailId() {
		return alternativeEmailId;
	}

	/**
	 * sets the alternativeEmailId
	 * 
	 * @param alternativeEmailId
	 *            The alternativeEmailId to set.
	 */
	public void setAlternativeEmailId(String alternativeEmailId) {
		this.alternativeEmailId = alternativeEmailId;
	}

	/**
	 * returns the dateOfBirth
	 * 
	 * @return Returns the dateOfBirth.
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * sets the dateOfBirth
	 * 
	 * @param dateOfBirth
	 *            The dateOfBirth to set.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * returns the emailId
	 * 
	 * @return Returns the emailId.
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * sets the emailId
	 * 
	 * @param emailId
	 *            The emailId to set.
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * returns the fax
	 * 
	 * @return Returns the fax.
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * sets the fax
	 * 
	 * @param fax
	 *            The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * returns the firstName
	 * 
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * sets the firstName
	 * 
	 * @param firstName
	 *            The firstName to set.
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * returns the gender
	 * 
	 * @return Returns the gender.
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * sets the gender
	 * 
	 * @param gender
	 *            The gender to set.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * returns the lastName
	 * 
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * sets the lastName
	 * 
	 * @param lastName
	 *            The lastName to set.
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * returns the mobile
	 * 
	 * @return Returns the mobile.
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * sets the mobile
	 * 
	 * @param mobile
	 *            The mobile to set.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * returns the officeTelephone
	 * 
	 * @return Returns the officeTelephone.
	 */
	public String getOfficeTelephone() {
		return officeTelephone;
	}

	/**
	 * sets the officeTelephone
	 * 
	 * @param officeTelephone
	 *            The officeTelephone to set.
	 */
	public void setOfficeTelephone(String officeTelephone) {
		this.officeTelephone = officeTelephone;
	}

	/**
	 * returns the password
	 * 
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * sets the password
	 * 
	 * @param password
	 *            The password to set.
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * returns the telephone
	 * 
	 * @return Returns the telephone.
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * sets the telephone
	 * 
	 * @param telephone
	 *            The telephone to set.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * returns the title
	 * 
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * sets the title
	 * 
	 * @param title
	 *            The title to set.
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the addressStreet
	 */
	public String getAddressStreet() {
		return addressStreet;
	}

	/**
	 * @param addressStreet
	 *            the addressStreet to set
	 */
	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	/**
	 * @return the addressLine
	 */
	public String getAddressLine() {
		return addressLine;
	}

	/**
	 * @param addressLine
	 *            the addressLine to set
	 */
	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param country
	 *            The country to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 */
	public int getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationality
	 *            The nationality to set.
	 */
	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the secretAnswer.
	 */
	public String getSecretAnswer() {
		return secretAnswer;
	}

	/**
	 * @param secretAnswer
	 *            The secretAnswer to set.
	 */
	public void setSecretAnswer(String secretAnswer) {
		this.secretAnswer = secretAnswer;
	}

	/**
	 * @return Returns the secretQuestion.
	 */
	public String getSecretQuestion() {
		return secretQuestion;
	}

	/**
	 * @param secretQuestion
	 *            The secretQuestion to set.
	 */
	public void setSecretQuestion(String secretQuestion) {
		this.secretQuestion = secretQuestion;
	}

	/**
	 * @return Returns the customerQuestionaireTO.
	 */
	public Collection<CustomerQuestionaireTO> getCustomerQuestionaireTO() {
		return customerQuestionaireTO;
	}

	/**
	 * @param customerQuestionaireTO
	 *            The customerQuestionaireTO to set.
	 */
	private void setCustomerQuestionaireTO(Collection<CustomerQuestionaireTO> customerQuestionaireTO) {
		this.customerQuestionaireTO = customerQuestionaireTO;
	}

	/**
	 * Adds CustomerQuestionaireTO Object
	 * 
	 * @param customerQuestionaireTO
	 */
	public void addCustomerQuestionaireTO(CustomerQuestionaireTO customerQuestionaireTO) {
		if (this.getCustomerQuestionaireTO() == null) {
			this.setCustomerQuestionaireTO(new HashSet<CustomerQuestionaireTO>());
		}

		this.getCustomerQuestionaireTO().add(customerQuestionaireTO);
	}

	/**
	 * @return the emgnTitle
	 */
	public String getEmgnTitle() {
		return emgnTitle;
	}

	/**
	 * @param emgnTitle
	 *            the emgnTitle to set
	 */
	public void setEmgnTitle(String emgnTitle) {
		this.emgnTitle = emgnTitle;
	}

	/**
	 * @return the emgnFirstName
	 */
	public String getEmgnFirstName() {
		return emgnFirstName;
	}

	/**
	 * @param emgnFirstName
	 *            the emgnFirstName to set
	 */
	public void setEmgnFirstName(String emgnFirstName) {
		this.emgnFirstName = emgnFirstName;
	}

	/**
	 * @return the emgnLastName
	 */
	public String getEmgnLastName() {
		return emgnLastName;
	}

	/**
	 * @param emgnLastName
	 *            the emgnLastName to set
	 */
	public void setEmgnLastName(String emgnLastName) {
		this.emgnLastName = emgnLastName;
	}

	/**
	 * @return the emgnPhoneNo
	 */
	public String getEmgnPhoneNo() {
		return emgnPhoneNo;
	}

	/**
	 * @param emgnPhoneNo
	 *            the emgnPhoneNo to set
	 */
	public void setEmgnPhoneNo(String emgnPhoneNo) {
		this.emgnPhoneNo = emgnPhoneNo;
	}

	/**
	 * @return the emgnEmail
	 */
	public String getEmgnEmail() {
		return emgnEmail;
	}

	/**
	 * @param emgnEmail
	 *            the emgnEmail to set
	 */
	public void setEmgnEmail(String emgnEmail) {
		this.emgnEmail = emgnEmail;
	}
	
	/**
	 * @return the sendPromoEmail
	 */
	public Boolean getSendPromoEmail() {
		return sendPromoEmail;
	}

	/**
	 * @param sendPromoEmail
	 *            the sendPromoEmail to set
	 */
	public void setSendPromoEmail(Boolean sendPromoEmail) {
		this.sendPromoEmail = sendPromoEmail;
	}

}