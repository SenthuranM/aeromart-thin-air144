package com.isa.thinair.aircustomer.api.utils;

import java.io.Serializable;
import java.util.Calendar;

import com.isa.thinair.aircustomer.api.constants.CustomAliasConstants;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class CustomerAliasGenerator implements Serializable {

	private static final long serialVersionUID = -1482122553534966175L;

	public static String generateAlias(final String card) throws ModuleException {

		StringBuffer alias = new StringBuffer();
		alias.append(AppSysParamsUtil.getCarrierCode());
		alias.append(CustomAliasConstants.ALIAS).append(BeanUtils.getFirst6Digits(card));
		Calendar calendar = Calendar.getInstance();
		alias.append(calendar.getTimeInMillis());
		String randomString = StringUtil.generateRandomString(4);
		alias.append(randomString);
		alias.append(BeanUtils.getLast4Digits((card)));

		return alias.toString();
	}

}
