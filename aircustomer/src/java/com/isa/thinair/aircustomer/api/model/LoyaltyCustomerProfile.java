/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 * @version $$Id$$
 */

package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_LOYALTY_CUSTOMER_PROFILE" LoyaltyCustomerProfile is the entity class to repesent a
 *                  Loyalty Customer Profile model
 * @author Chamindap
 * 
 */
public class LoyaltyCustomerProfile extends Persistent implements Serializable {

	private static final long serialVersionUID = 1421619154132591600L;

	private String loyaltyAccountNo;

	private String email;

	private String mobile;

	private Date dateOfBirth;

	private String countryCode;

	private int nationalityCode;

	private String city;

	private Integer customerId;

	private String cardType;

	private String status;

	private Date createdDate;

	private Date confirmedDate;

	public LoyaltyCustomerProfile() {

	}

	/**
	 * @return Returns the loyaltyAccountNo.
	 * @hibernate.id column = "LOYALTY_ACCOUNT_NO" generator-class = "assigned"
	 */
	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	/**
	 * @param loyaltyAccountNo
	 *            The loyaltyAccountNo to set.
	 */
	public void setLoyaltyAccountNo(String loyaltyAccountNo) {
		this.loyaltyAccountNo = loyaltyAccountNo;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * returns the dateOfBirth
	 * 
	 * @return Returns the dateOfBirth.
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * sets the dateOfBirth
	 * 
	 * @param dateOfBirth
	 *            The dateOfBirth to set.
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * returns the email
	 * 
	 * @return Returns the email.
	 * @hibernate.property column = "EMAIL"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * sets the email
	 * 
	 * @param email
	 *            The email to set.
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * returns the mobile
	 * 
	 * @return Returns the mobile.
	 * @hibernate.property column = "MOBILE"
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * sets the mobile
	 * 
	 * @param mobile
	 *            The mobile to set.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param country
	 *            The country to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @hibernate.property column = "NATIONALITY_CODE"
	 */
	public int getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationality
	 *            The nationality to set.
	 */
	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @hibernate.property column = "CITY"
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @hibernate.property column = "CARD_TYPE"
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            The cardType to set.
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	// Jira 3504

	/**
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param status
	 *            The Created Time to set.
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @hibernate.property column = "CONFIRMED_DATE"
	 */
	public Date getConfirmedDate() {
		return confirmedDate;
	}

	/**
	 * @param status
	 *            The Created Time to set.
	 */
	public void setConfirmedDate(Date confirmedDate) {
		this.confirmedDate = confirmedDate;
	}

}