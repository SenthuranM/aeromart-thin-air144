package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Chethiya Palliyaguruge
 * 
 * @hibernate.class table = "T_LMS_MEMBER"
 */
public class LmsMember extends Persistent implements Serializable {
	
	private static final long serialVersionUID = -7697551475867640131L;

	private Integer lmsMemberId;
	
	private String ffid; 
    
	private String firstName;
     
	private String middleName;
	 
	private String lastName;
	
	private  Date dateOfBirth;
	
	private String mobileNumber;
	
	private String phoneNumber; 
     
	private Date regDate;
	
	private char emailConfSent;
	
	private char emailConfirmed;
	
	private String enrollmentLocExtRef;
	
	private String passportNum;
	
	private String language;
	
	private String nationalityCode;
	
	private String residencyCode;
	
	private String headOFEmailId;
	
	private Integer sbInternalId;
	
	private String memberExternalId;
	
	private Integer genderTypeId;
	
	private String password;
	
	private Integer customerId;
	
	private String refferedEmail;
	
	private String enrollingCarrier;
	
	private String enrollingChannelExtRef;

	private String enrollingChannelIntRef;
	/**
	 * @return Returns the lmsMemberId
	 * 
	 * @hibernate.id column = "LMS_MEMBER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_LMS_MEMBER"
	 */
	public Integer getLmsMemberId() {
		return lmsMemberId;
	}

	/**
	 * @param lmsMemberId
	 */
	public void setLmsMemberId(Integer lmsMemberId) {
		this.lmsMemberId = lmsMemberId;
	}

	/**
	 * @return Returns the ffid
	 * 
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getFfid() {
		return ffid;
	}

	/**
	 * @param ffid
	 */
	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	/**
	 * @return Returns the firstName
	 * 
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the middleName
	 * 
	 * @hibernate.property column = "MIDDLE_NAME"
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return Returns the lastName
	 * 
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the dateOfBirth
	 * 
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return Returns the mobileNumber
	 * 
	 * @hibernate.property column = "MOBILE_PHONE_NUMBER"
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return Returns the phoneNumber
	 * 
	 * @hibernate.property column = "PHONE_NUMBER"
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return Returns the regDate
	 * 
	 * @hibernate.property column = "REGISTRATION_DATE"
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * @param regDate
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * @return Returns the emailConfSent
	 * 
	 * @hibernate.property column = "EMAIL_CONF_SENT"
	 */
	public char getEmailConfSent() {
		return emailConfSent;
	}

	/**
	 * @param emailConfSent
	 */
	public void setEmailConfSent(char emailConfSent) {
		this.emailConfSent = emailConfSent;
	}

	/**
	 * @return Returns the emailConfirmed
	 * 
	 * @hibernate.property column = "EMAIL_CONFIRMED"
	 */
	public char getEmailConfirmed() {
		return emailConfirmed;
	}

	/**
	 * @param emailConfirmed
	 */
	public void setEmailConfirmed(char emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}

	/**
	 * @return Returns the enrollmentLocExtRef
	 * 
	 * @hibernate.property column = "ENROLLMENT_LOC_EXT_REF"
	 */
	public String getEnrollmentLocExtRef() {
		return enrollmentLocExtRef;
	}

	/**
	 * @param enrollmentLocExtRef
	 */
	public void setEnrollmentLocExtRef(String enrollmentLocExtRef) {
		this.enrollmentLocExtRef = enrollmentLocExtRef;
	}

	/**
	 * @return Returns the passportNum
	 * 
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 */
	public String getPassportNum() {
		return passportNum;
	}

	/**
	 * @param passportNum
	 */
	public void setPassportNum(String passportNum) {
		this.passportNum = passportNum;
	}

	/**
	 * @return Returns the language
	 * 
	 * @hibernate.property column = "COMMUNICATION_LANGUAGE"
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return Returns the nationalityCod
	 * 
	 * @hibernate.property column = "NATIONALITY_CODE"
	 */
	public String getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 */
	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return Returns the residencyCode
	 * 
	 * @hibernate.property column = "RESIDENCY_CODE"
	 */
	public String getResidencyCode() {
		return residencyCode;
	}

	/**
	 * @param residencyCode
	 */
	public void setResidencyCode(String residencyCode) {
		this.residencyCode = residencyCode;
	}

	/**
	 * @return Returns the headOFEmailId
	 * 
	 * @hibernate.property column = "HEAD_OF_FAMILY_EMAIL_ID"
	 */
	public String getHeadOFEmailId() {
		return headOFEmailId;
	}

	/**
	 * @param headOFEmailId
	 */
	public void setHeadOFEmailId(String headOFEmailId) {
		this.headOFEmailId = headOFEmailId;
	}

	/**
	 * @return Returns the sbInternalId
	 * 
	 * @hibernate.property column = "SB_INTERNAL_ID"
	 */
	public Integer getSbInternalId() {
		return sbInternalId;
	}

	/**
	 * @param sbInternalId
	 */
	public void setSbInternalId(Integer sbInternalId) {
		this.sbInternalId = sbInternalId;
	}

	/**
	 * @return Returns the genderTypeId
	 * 
	 * @hibernate.property column = "GENDER_TYPE_ID"
	 */
	public Integer getGenderTypeId() {
		return genderTypeId;
	}

	/**
	 * @param genderTypeId
	 */
	public void setGenderTypeId(Integer genderTypeId) {
		this.genderTypeId = genderTypeId;
	}

	/**
	 * @return password
	 * 
	 * @hibernate.property column = "PASSWORD_LMS"
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param Returns the password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the customerId
	 * 
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the refferedEmail
	 * 
	 * @hibernate.property column = "REFERRING_EMAIL"
	 */
	public String getRefferedEmail() {
		return refferedEmail;
	}

	/**
	 * @param refferedEmail
	 */
	public void setRefferedEmail(String refferedEmail) {
		this.refferedEmail = refferedEmail;
	}

	/**
	 * @return the memberExternalId
	 * 
	 * @hibernate.property column = "MEMBER_EXTERNAL_ID"
	 */
	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}

	/**
	 * @return the enrollingCarrier
	 * 
	 * @hibernate.property column = "ENROLLING_CARRIER"
	 */
	public String getEnrollingCarrier() {
		return enrollingCarrier;
	}

	public void setEnrollingCarrier(String enrollingCarrier) {
		this.enrollingCarrier = enrollingCarrier;
	}

	/**
	 * @return the enrollingChannelExtRef
	 * 
	 * @hibernate.property column = "ENROLLMENT_CHANNEL_EXT_REF"
	 */
	public String getEnrollingChannelExtRef() {
		return enrollingChannelExtRef;
	}

	public void setEnrollingChannelExtRef(String enrollingChannelExtRef) {
		this.enrollingChannelExtRef = enrollingChannelExtRef;
	}

	/**
	 * @return the enrollingChannelExtRef
	 * 
	 * @hibernate.property column = "ENROLLMENT_CHANNEL_INT_REF"
	 */
	public String getEnrollingChannelIntRef() {
		return enrollingChannelIntRef;
	}

	public void setEnrollingChannelIntRef(String enrollingChannelIntRef) {
		this.enrollingChannelIntRef = enrollingChannelIntRef;
	}

}
