package com.isa.thinair.aircustomer.api.constants;

public enum SocialCustomerType {

	FACEBOOK(1, "Facebook user"), GOOGLE(2, "Google user");

	private Integer id;
	private String code;

	private SocialCustomerType(Integer id, String code) {
		this.id = id;
		this.code = code;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static SocialCustomerType resolveSocialCustomerType(Integer id) {

		for (SocialCustomerType type : SocialCustomerType.values()) {
			if (type.getId().equals(id)) {
				return type;
			}

		}
		return null;

	}
}
