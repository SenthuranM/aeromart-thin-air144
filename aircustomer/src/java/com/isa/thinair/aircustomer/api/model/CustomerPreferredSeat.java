/**
 * 
 */
package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_CUSTOMER_SEAT" CustomerPreferredSeat is the entity class that is used to store customer seat preferences
 * @author suneth
 *
 */
public class CustomerPreferredSeat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2676186320594071899L;

	private int customerId;
	
	private String seatType;

	/**
	 * @return the customerId
	 * @hibernate.id column = "CUSTOMER_ID" generator-class = "assigned"
	 */
	public int getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the seatType
	 * @hibernate.property column = "SEAT_TYPE"
	 */
	public String getSeatType() {
		return seatType;
	}

	/**
	 * @param seatType the seatType to set
	 */
	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}
	
	

}
