/**
 * 
 */
package com.isa.thinair.aircustomer.api.dto;

import java.io.Serializable;

/**
 * @author suneth
 *
 */
public class CustomerPreferredMealDTO implements Serializable {
	
	private String mealCode;
	
	private Integer quantity;

	/**
	 * @return the mealCode
	 */
	public String getMealCode() {
		return mealCode;
	}

	/**
	 * @param mealCode the mealCode to set
	 */
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	
	
	

}
