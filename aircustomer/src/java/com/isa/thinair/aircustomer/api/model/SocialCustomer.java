package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_SOCIAL_CUSTOMER"
 * 
 */
public class SocialCustomer implements Serializable {


	private static final long serialVersionUID = -6123136572641259227L;

	private Integer socialCustomerId;

	private Integer customerId;

	private String socialSiteCustId;

	private Integer socialCustomerTypeId;

	/**
	 * @return Returns the socialCustomerId
	 * @hibernate.id column = "SOCIAL_CUSTOMER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SOCIAL_CUSTOMER"
	 */
	public Integer getSocialCustomerId() {
		return socialCustomerId;
	}

	/**
	 * 
	 * @param socialCustomerId
	 */
	public void setSocialCustomerId(Integer socialCustomerId) {
		this.socialCustomerId = socialCustomerId;
	}

	/**
	 * @return Returns the customerId
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the socialSiteCustId
	 * @hibernate.property column = "SOCIAL_SITE_CUST_ID"
	 */

	public String getSocialSiteCustId() {
		return socialSiteCustId;
	}

	/**
	 * 
	 * @param socialSiteCustId
	 */
	public void setSocialSiteCustId(String socialSiteCustId) {
		this.socialSiteCustId = socialSiteCustId;
	}

	/**
	 * @return
	 * @hibernate.property column = "SOCIAL_CUSTOMER_TYPE_ID"
	 */
	public Integer getSocialCustomerTypeId() {
		return socialCustomerTypeId;
	}

	public void setSocialCustomerTypeId(Integer socialCustomerTypeId) {
		this.socialCustomerTypeId = socialCustomerTypeId;
	}

}
