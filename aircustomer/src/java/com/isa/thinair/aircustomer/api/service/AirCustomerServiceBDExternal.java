/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.api.service;

import java.util.Collection;

import com.isa.thinair.aircustomer.api.dto.external.CustomerTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public interface AirCustomerServiceBDExternal {

	/**
	 * Return all the customers
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection<CustomerTO> getCustomerTOs() throws ModuleException;
}