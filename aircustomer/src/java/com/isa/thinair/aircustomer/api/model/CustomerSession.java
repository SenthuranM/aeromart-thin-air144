package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @hibernate.class table = "T_CUSTOMER_SESSION"
 *
 */
public class CustomerSession implements Serializable {
	private Integer customerSessionId;

	private Integer customerId;

	private String tokenNumber;

	private Date createdTime;

	/**
	 * @hibernate.id column = "CUSTOMER_SESSION_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CUSTOMER_SESSION"
	 */
	public Integer getCustomerSessionId() {
		return customerSessionId;
	}

	public void setCustomerSessionId(Integer customerSessionId) {
		this.customerSessionId = customerSessionId;
	}

	/**
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @hibernate.property column = "CUSTOMER_TOCKEN"
	 */
	public String getTokenNumber() {
		return tokenNumber;
	}

	public void setTokenNumber(String tokenNumber) {
		this.tokenNumber = tokenNumber;
	}

	/**
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
}
