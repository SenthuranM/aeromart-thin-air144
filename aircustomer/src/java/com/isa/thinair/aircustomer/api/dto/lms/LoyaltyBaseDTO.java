package com.isa.thinair.aircustomer.api.dto.lms;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public abstract class LoyaltyBaseDTO implements Serializable {

	private static final long serialVersionUID = -4161810355198267846L;

	private int returnCode;

	private String memberAccountId;

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	public String getMemberAccountId() {
		return memberAccountId;
	}

	public void setMemberAccountId(String memberAccountId) {
		this.memberAccountId = memberAccountId;
	}

}
