package com.isa.thinair.aircustomer.api.constants;

public interface CustomAliasConstants {

	String ALIAS_ACTIVE = "ACT";

	String ALIAS_INACTIVE = "INA";

	String ALIAS = "alias";

	String CUSTOMER_ALIAS_DAO = "customerAliasDAO";
	
	String NO_CARD_STORED = "msg_no_card_stored";

	String UNABLE_TO_DELETE_THE_STORED_CARD = "msg_unable_to_deleted_stored_card";

	String SUCCESSFULLY_DELETED_THE_STORED_CARD = "msg_successfully_deleted_stored_card";

	String TNX_TYPE_DR = "DR";

	String TNX_TYPE_CR = "CR";

	char PRODUCT_TYPE = 'C';

}
