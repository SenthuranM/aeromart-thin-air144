/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.customer.core.persistence.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestCustomerDAOImpl extends PlatformTestCase {
	
	Customer customer = null; 
	Country country = null; 
	Currency currency = null; 
	Nationality nationality = null;
	LookupService lookup = null;
	CustomerDAO dao = null;
		
	/**
	 *  Sets up the environment.
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		int nationalitycode = 100;
		
		lookup = LookupServiceFactory.getInstance();
		dao = (CustomerDAO)lookup.getBean("isa:base://modules/" 
				+ "aircustomer?id=customerDAOProxy");	
		
		customer = new Customer();
		customer.setCountryCode("12");
		customer.setNationalityCode(nationalitycode);		
		customer.setAlternativeEmailId("NEWW@cba");
		customer.setDateOfBirth(new GregorianCalendar().getTime());
		customer.setFax("Fax FAX");
		customer.setEmailId("ae@a2");
		customer.setFirstName("Symon test");
		customer.setGender("M");
		customer.setLastName("last");
		customer.setMobile("123332");
		customer.setOfficeTelephone("456");
		customer.setPassword("password");
		customer.setTelephone("1234");
		customer.setTitle("Mr");
		customer.setStatus("test");
	}

	/**
	 * Tear down method of TestCustomerDAOImpl class.
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for 'com.isa.thinair.customer.core.hibernate
	 * .CustomerDAOImpl.saveOrUpdate(Customer)'
	 */
	public void testSaveOrUpdate() {
		Customer cust = null;
		customer.setEmailId("awXX");
		
		assertNotNull(dao);	
		dao.saveOrUpdate(customer);
		cust = dao.getCustomer(customer.getCustomerId());			
		dao.removeCustomer(cust.getCustomerId());
	}

	/**
	 * Test method for 'com.isa.thinair.customer.core.hibernate
	 * .CustomerDAOImpl.delete(int)'
	 */
	public void testDelete() {
		Customer cust = null;
		
		customer.setEmailId("A@AB");		
		assertNotNull(dao);
		dao.saveOrUpdate(customer);		
		cust = dao.getCustomer(customer.getCustomerId());	
		dao.removeCustomer(cust.getCustomerId());		
	}	
	
	/**
	 * Test method for dao implementataion.
	 */
	public void testGetCustomer() {
		Customer cust = null;		

		assertNotNull(dao);	
		cust = dao.getCustomer(46);
		assertEquals(" incorrect first name ", "Symon test", cust.getFirstName());
		assertEquals(" incorrect id ", 46, cust.getCustomerId());
		assertEquals(" incorrect email ", "ad@a", cust.getEmailId());
		assertEquals(" incorrect gender ", "M", cust.getGender());		
	}
	
	/**
	 * Test method for dao implementataion.
	 */
	public void testGetCustomers() {
		List customerList = null;		

		assertNotNull(dao);		
		customerList = dao.getCustomers();	
		assertNotNull(customerList);		
	}
	
	/**
	 * Test method for dao implementataion.
	 */
	public void testAuthenticate() {
		Customer cust = null;
		
		String loginId = "ad@a";
		String password = "password";

		assertNotNull(dao);	
		cust = dao.authenticate(loginId, password);
	
		assertEquals(" incorrect first name ", "Symon test", cust.getFirstName());
		assertEquals(" incorrect id ", 46, cust.getCustomerId());
		assertEquals(" incorrect email ", "ad@a", cust.getEmailId());
		assertEquals(" incorrect gender ", "M", cust.getGender());		
	}
	
	/**
	 * Test method for dao implementataion.
	 */
	public void testGetCustomerLogin() {
		Customer cust = null;		
		String loginId = "ac@a";

		assertNotNull(dao);	
	
		cust = dao.getCustomer(loginId);	
		assertNotNull(cust);
		assertEquals(" incorrect first name ", "Symon", cust.getFirstName());
		assertEquals(" incorrect id ", 45, cust.getCustomerId());
		assertEquals(" incorrect email ", "ac@a", cust.getEmailId());
		assertEquals(" incorrect gender ", "M", cust.getGender());		
	}

	/**
	 * @see com.isa.thinair.aircustomer.core.persistence.dao
	 * .CustomerDAO#getAllReservations(java.lang.String, int, int)
	 */
	public void testGetAllReservations() {
		Collection currentReservations = null;
		String loginId = "ac@a";
		int startIndex = 0;
		int pageSize = 10;
		
		assertNotNull(dao);		
		currentReservations = dao.getAllReservations(loginId, startIndex, pageSize);		
		Iterator iter = currentReservations.iterator(); 
		
		while (iter.hasNext()) {
			Reservation res = (Reservation) iter.next();
			
			if (res.getPnr().equals("aa")) {
//				assertEquals(" incorrect first name ", "Sanath", res.getCFirstName());
//				assertEquals(" incorrect last name ", "Jayasuriya", res.getCLastName());
				assertEquals(" incorrect pnr ", "aa", res.getPnr());
//				assertEquals(" incorrect Phone ", "123456", res.getCPhoneNo());	
			}			
		}
	}

	/**
	 * @see com.isa.thinair.aircustomer.core.persistence.dao
	 * .CustomerDAO#getCurrentReservations(java.lang.String)
	 */
	public void testGetCurrentReservations() {
		Collection currentReservations = null;
		String loginId = "ac@a";
		
		assertNotNull(dao);		
		currentReservations = dao.getCurrentReservations(loginId);			
		Iterator iter = currentReservations.iterator(); 		
		while (iter.hasNext()) {
			Reservation res = (Reservation) iter.next();			
			if (res.getPnr().equals("aa")) {
//				assertEquals(" incorrect first name ", "Sanath", res.getCFirstName());
//				assertEquals(" incorrect last name ", "Jayasuriya", res.getCLastName());
				assertEquals(" incorrect pnr ", "aa", res.getPnr());
//				assertEquals(" incorrect Phone ", "123456", res.getCPhoneNo());		
			}			
		}		
	}
	
	/**
	 * @param creteria
	 * @param startRec
	 * @param pageSize
	 * @return .
	 * @see com.isa.thinair.aircustomer.core.persistence.dao
	 * .CustomerDAO#getCustomers(java.util.List, int, int)
	 */
	public void testGetCustomersCriteria() {
		Page searchCustomers = null;
		List criteria = new ArrayList();	
		int startRec = 0;
		int pageSize = 10;
		
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);		
		criterion.setFieldName("emailId");
		List listValues = new ArrayList();
		listValues.add("ac@a");
		criterion.setValue(listValues);	
		
		ModuleCriterion criterion1 = new ModuleCriterion();
		criterion1.setCondition(ModuleCriterion.CONDITION_EQUALS);		
		criterion1.setFieldName("password");
		List listValues1 = new ArrayList();	
		listValues1.add("password");
		criterion1.setValue(listValues1);
		
		criteria.add(criterion);
		criteria.add(criterion1);
		
		searchCustomers = dao.getCustomers(criteria, startRec, pageSize);
		assertEquals("Incorrect size", 1, searchCustomers.getTotalNoOfRecords());		
	}
//
//	/**
//	 * @param loginId
//	 * @return .
//	 * @see com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO#getCustomerCredits(java.lang.String)
//	 */
//	public Collection getCustomerCredits(String loginId) {
//		return dao.getCustomerCredits(loginId);
//	}	

}