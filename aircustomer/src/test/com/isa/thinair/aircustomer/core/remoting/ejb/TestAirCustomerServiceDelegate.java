/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.remoting.ejb;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestAirCustomerServiceDelegate extends PlatformTestCase {

	Customer customer = null; 

	/**
	 * 
	 */
	protected void setUp() throws Exception {
		super.setUp();	
		
		int nationalitycode = 100;
		
		customer = new Customer();
		customer.setCountryCode("12");
		customer.setNationalityCode(nationalitycode);		
		customer.setAlternativeEmailId("sshhhs@cba");
		customer.setDateOfBirth(new GregorianCalendar().getTime());
		customer.setFax("aaa");		
		customer.setFirstName("Symon test");
		customer.setGender("M");
		customer.setLastName("last");
		customer.setMobile("123332");
		customer.setOfficeTelephone("456");
		customer.setPassword("password");
		customer.setTelephone("1234");
		customer.setTitle("Mr");
		customer.setStatus("test");
	}

	/**
	 * 
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.saveOrUpdate(Customer)'
	 * @throws ModuleException 
	 */
	public void testSaveOrUpdate() throws ModuleException {
		Customer cus1 = null;
		
		customer.setEmailId("22e@a");		
		
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();		
		airCustomerServiceBD.saveOrUpdate(customer);
		
		cus1 = airCustomerServiceBD.getCustomer(customer.getEmailId());
		assertNotNull(cus1);
		airCustomerServiceBD.removeCustomer(cus1.getCustomerId());
	}

	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.saveOrUpdate(Customer)'
	 * @throws ModuleException 
	 */
	public void testUpdate() throws ModuleException {
		Customer cus1 = null;
		int id = 0;
		
		customer.setEmailId("UPDATE");		
		customer.setStatus("NEW");
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();
		
		if (airCustomerServiceBD.getCustomer(customer.getEmailId()) == null) {
			airCustomerServiceBD.saveOrUpdate(customer);			
		}		
		
		cus1 = airCustomerServiceBD.getCustomer(customer.getEmailId());
		assertNotNull(cus1);
		
		id = cus1.getCustomerId();
		String name = cus1.getFirstName();	
		
		name = "Changed Update";
		cus1.setFirstName(name);
		cus1.setFax("UpFAX");
		cus1.setPassword("PASSSUP");
		cus1.setTelephone("1234");		
		airCustomerServiceBD.saveOrUpdate(cus1);
		
		assertEquals(" incorrect first name ", "Changed Update", cus1.getFirstName());
		assertEquals(" incorrect id ", id, cus1.getCustomerId());
		assertEquals(" incorrect fax ", "UpFAX", cus1.getFax());
		assertEquals(" incorrect password ", "PASSSUP", cus1.getPassword());
		assertEquals(" incorrect email ", "UPDATE", cus1.getEmailId());
		assertEquals(" incorrect gender ", "M", cus1.getGender());
		
	}
	
	
	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.removeCustomer(int)'
	 * @throws ModuleException 
	 */
	public void testRemoveCustomer() throws ModuleException {
		Customer cus1 = null;		
		
		customer.setEmailId("A@Delete");
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();	
		
		airCustomerServiceBD.saveOrUpdate(customer);
		
		cus1 = airCustomerServiceBD.getCustomer(customer.getEmailId());
		assertNotNull(cus1);
		assertEquals(" incorrect email id ", "A@Delete", cus1.getEmailId());
		airCustomerServiceBD.removeCustomer(cus1.getCustomerId());
	}

	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.getCustomers()'
	 * @throws Exception 
	 */
	public void testGetCustomers() throws Exception {
		List customersList = null;
		
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();		
		customersList = airCustomerServiceBD.getCustomers();	
		assertNotNull(customersList);	
	}

	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.getCustomer(int)'
	 * @throws ModuleException 
	 */
	public void testGetCustomerInt() throws ModuleException {
		Customer cust = null;
		
		//customer.setEmailId("A@AeeA");

		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();
		//airCustomerServiceBD.saveOrUpdate(customer);		
		cust = airCustomerServiceBD.getCustomer(48);
		
		assertEquals(" incorrect first name ", "Symon test", cust.getFirstName());
		assertEquals(" incorrect id ", 48, cust.getCustomerId());
		assertEquals(" incorrect email ", "A@A", cust.getEmailId());
		assertEquals(" incorrect gender ", "M", cust.getGender());
	}

	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.getCustomer(String)'
	 * @throws ModuleException 
	 */
	public void testGetCustomerString() throws ModuleException {
		Customer cust = null;
		String loginId = "ad@a";			
	
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();		
		cust = airCustomerServiceBD.getCustomer(loginId);
	
		assertEquals(" incorrect first name ", "Symon test", cust.getFirstName());
		assertEquals(" incorrect id ", 46, cust.getCustomerId());
		assertEquals(" incorrect login id ", "ad@a", cust.getEmailId());
		assertEquals(" incorrect password ", "password", cust.getPassword());

	}
	
	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.authenticate(String, String)'
	 * @throws ModuleException 
	 */
	public void testAuthenticate() throws ModuleException {
		Customer cust = null;
		String loginId = "ad@a";
		String password = "password";		
	
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();		
		cust = airCustomerServiceBD.authenticate(loginId, password);
	
		assertEquals(" incorrect first name ", "Symon test", cust.getFirstName());
		assertEquals(" incorrect id ", 46, cust.getCustomerId());
		assertEquals(" incorrect login id ", "ad@a", cust.getEmailId());
		assertEquals(" incorrect password ", "password", cust.getPassword());
	}
	
	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.getCurrentReservations(String)'
	 * @throws ModuleException 
	 */
	public void testGetCurrentReservations() throws ModuleException {
		Collection currentReservations = null;
		String loginId = "ac@a";
		
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();
				
		currentReservations = airCustomerServiceBD.getCurrentReservations(loginId);
		assertNotNull(currentReservations);
		
		Iterator iter = currentReservations.iterator(); 
		
		while (iter.hasNext()) {
			Reservation res = (Reservation) iter.next();
			
			if (res.getPnr().equals("aa")) {
//				assertEquals(" incorrect first name ", "Sanath", res.getCFirstName());
//				assertEquals(" incorrect last name ", "Jayasuriya", res.getCLastName());
				assertEquals(" incorrect pnr ", "aa", res.getPnr());
//				assertEquals(" incorrect Phone ", "123456", res.getCPhoneNo());	
			}			
		}
	}

	/**
	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
	 * .AirCustomerServiceDelegateImpl.getAllReservations(String, int, int)'
	 * @throws ModuleException 
	 */
	public void testGetAllReservations() throws ModuleException {
		Collection currentReservations = null;
		String loginId = "ac@a";
		int startIndex = 0;
		int pageSize = 10;
		
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();
		
		currentReservations = airCustomerServiceBD.getAllReservations(loginId, startIndex, pageSize);
		assertNotNull(currentReservations);	
		Iterator iter = currentReservations.iterator(); 
		
		while (iter.hasNext()) {
			Reservation res = (Reservation) iter.next();
			
			if (res.getPnr().equals("aa")) {
//				assertEquals(" incorrect first name ", "Sanath", res.getCFirstName());
//				assertEquals(" incorrect last name ", "Jayasuriya", res.getCLastName());
				assertEquals(" incorrect pnr ", "aa", res.getPnr());
//				assertEquals(" incorrect Phone ", "123456", res.getCPhoneNo());					
			}			
		}
	}
	
	/**
	 * @throws ModuleException 
	 * @see com.isa.thinair.aircustomer.core.persistence.dao
	 * .CustomerDAO#registerCustomer(Customer)
	 */
	public void testRegisterCustomer() throws ModuleException {
		Customer cus1 = null;
		boolean registered = false;
		int nationalitycode = 100;
		
		customer = new Customer();		
		customer.setCountryCode("12");
		customer.setNationalityCode(nationalitycode);		
		customer.setAlternativeEmailId("NEWW@cba");
		customer.setDateOfBirth(new GregorianCalendar().getTime());
		customer.setFax("Fax FAX");		
		customer.setFirstName("Chaminda");
		customer.setGender("M");
		customer.setLastName("Perera");
		customer.setMobile("123332");
		customer.setOfficeTelephone("456");
		customer.setPassword("password");
		customer.setTelephone("1234");
		customer.setTitle("Mr");
		customer.setEmailId("chamindap@jkcsworld.com");		
		
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();
		assertNotNull(airCustomerServiceBD);		
		registered = airCustomerServiceBD.registerCustomer(customer);
		assertEquals("Incorrect registration", true, registered);
		
		cus1 = airCustomerServiceBD.getCustomer(customer.getEmailId());
		assertNotNull(cus1);
		assertEquals(" Incorrect status ", "Registered", cus1.getStatus());	
	
		airCustomerServiceBD.removeCustomer(cus1.getCustomerId());
	}
	
	/**
	 * @throws ModuleException 
	 * @see com.isa.thinair.aircustomer.core.persistence.dao
	 * .CustomerDAO#confirmCustomer()
	 */
	public void testConfirmCustomer() throws ModuleException {
		Customer cus1 = null;
		boolean confirm = false;
		boolean registered = false;
		String customerLoginId = "chamindap@jkcs.slt.lk"; 
		String customerKey = "/9KrVzWbOH92HixdvmXjWNC4OH8BR74C";		
		int nationalitycode = 100;
		
		customer = new Customer();		
		customer.setCountryCode("12");
		customer.setNationalityCode(nationalitycode);		
		customer.setAlternativeEmailId("Sun@sun.com");
		customer.setDateOfBirth(new GregorianCalendar().getTime());
		customer.setFax("Fax FAX");		
		customer.setFirstName("AWC");
		customer.setGender("M");
		customer.setLastName("Perera");
		customer.setMobile("123332");
		customer.setOfficeTelephone("456");
		customer.setPassword("password");
		customer.setTelephone("1234");
		customer.setTitle("Mr");	
		
		customer.setEmailId(customerLoginId);
		AirCustomerServiceBD airCustomerServiceBD = getAirCustomerServiceBD();
		assertNotNull(airCustomerServiceBD);		
		registered = airCustomerServiceBD.registerCustomer(customer);
		assertEquals("Incorrect registration", true, registered);
		
		cus1 = airCustomerServiceBD.getCustomer(customer.getEmailId());
		assertNotNull(cus1);
		assertEquals(" Incorrect status ", "Registered", cus1.getStatus());
		
		confirm = airCustomerServiceBD.confirmCustomer(customerLoginId, customerKey);
		assertEquals("Incorrect registration", true, confirm);
		cus1 = airCustomerServiceBD.getCustomer(customer.getEmailId());	
		assertNotNull(cus1);
		assertEquals(" Incorrect status ", "Confirmed", cus1.getStatus());		
		airCustomerServiceBD.removeCustomer(cus1.getCustomerId());		
	}
	

//	/**
//	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
//	 * .AirCustomerServiceDelegateImpl.getCustomerCredits(String)'
//	 */
//	public void testGetCustomerCredits() {
//		// 
//
//	}
//	/**
//	 * Test method for 'com.isa.thinair.aircustomer.core.service.bd
//	 * .AirCustomerServiceDelegateImpl.getCustomer(Set, int, int)'
//	 */
//	public void testGetCustomerSetIntInt() {
//		// 
//
//	}
	
	/**
	 * Private method to get AirCustomerServiceBD reference.
	 * @return AirCustomerServiceBD
	 */
	private AirCustomerServiceBD getAirCustomerServiceBD() { 
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule aircustomerModule = lookup.getModule("aircustomer");
		System.out.println("Air customer module looked up successful ...........................");
		AirCustomerServiceBD airCustomerServiceDelegate = null;
		try {
			airCustomerServiceDelegate = (AirCustomerServiceBD) aircustomerModule
				.getServiceBD("aircustomer.service.remote");
			System.out.println("Air customer module delegate creation successful...");
			return airCustomerServiceDelegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return airCustomerServiceDelegate;
	}
}
