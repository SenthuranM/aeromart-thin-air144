#!/bin/sh

LOGS_HOME=/usr/isa/isalogs
timestamp=`date '+%Y%m%d_%H%M'`
FOLDER_NAME=thinair
BACKUPS_DIR=/backups/logs
IP=69

if [ ! -e $BACKUPS_DIR ]; then
	mkdir $BACKUPS_DIR
fi

if [ -e "$LOGS_HOME/thinair" ]; then
        mv $LOGS_HOME/$FOLDER_NAME $BACKUPS_DIR/$FOLDER_NAME-$timestamp-$IP
        zip -r $BACKUPS_DIR/$FOLDER_NAME-$timestamp-$IP.zip $BACKUPS_DIR/$FOLDER_NAME-$timestamp-$IP
        rm -rf $BACKUPS_DIR/$FOLDER_NAME-$timestamp-$IP
	mkdir $LOGS_HOME/$FOLDER_NAME
fi
