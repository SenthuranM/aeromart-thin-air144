#!/bin/sh

echo "WARNING:This shold be only called in production deployment"
echo "This will repalce all static files and their reference to avoid caching"

originalString=$(echo $1 | sed -e 's/^ *//' -e 's/* $//')
projectRoot="$2"
#newString=$(date +%d%m%Y)
extdate="$3"

newString=${extdate:-$(date +%d%m%Y)};

echo "_no_cache replaced by: $newString"

filterString='jpg\|gif\|png\|css'
oldPwd=$(pwd)

cd $projectRoot

echo "renaming *.mod.xml file content"
find . -type f -iname "*.mod.xml" | xargs sed -i "s/$originalString/$newString/"

echo "renaming .*.jsp|.*.js|.*.html|.*.htm|.*.css|.*.properties file content"
for rFile in `find . -type  f -regextype posix-egrep -iregex '(.*.jsp|.*.js|.*.html|.*.htm|.*.css|.*.properties|.*.vm)'`; do
	if [ -f $rFile ]; then
		sed -i "s/$originalString\.\($filterString\)/$newString.\1/ig" $rFile
	fi
done

echo "renaming actual .*.jpg|.*.png|.*.gif|.*.css file"
find . -type f -regextype posix-egrep -iregex '(.*.jpg|.*.png|.*.gif|.*.css)' -exec /bin/bash resources/scripts/rename.sh {} $originalString $newString $filterString \; | more

cd $oldPwd




