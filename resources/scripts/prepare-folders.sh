#!/bin/sh

FOLDERS=(/usr/isa /usr/isa/isalogs /usr/isa/isalogs/thinair /usr/isa/scheduledservices /usr/isa/scheduledservices/tmppnl /usr/isa/scheduledservices/tmppfs /usr/isa/scheduledservices/genpnl /usr/isa/scheduledservices/genadl /usr/isa/scheduledservices/sentpnl /usr/isa/scheduledservices/sentadl /usr/isa/scheduledservices/xappnlprocesspath /usr/isa/scheduledservices/xapnlparsedpath /usr/isa/scheduledservices/xapnlerrorpath /usr/isa/scheduledservices/pfsprocesspath /usr/isa/scheduledservices/pfsparsedpath /usr/isa/scheduledservices/pfserrorpath)

abort_if_failed(){
	if [ "$1" -ne 0 ]; then
		echo "ERROR: Creating $2 failed"
	else
		echo "Created $2 Success" 
	fi
}

folders_count=${#FOLDERS[@]}
index=0
while [ "$index" -lt "$folders_count" ]
do
        if [ ! -e ${FOLDERS[$index]} ]; then
		mkdir "${FOLDERS[$index]}"
		abort_if_failed "$?" "${FOLDERS[$index]}"
		chown -R isa "${FOLDERS[$index]}"
		abort_if_failed "$?" "${FOLDERS[$index]}"
	else
		echo "Folder ${FOLDERS[$index]} already exists. Skipping creation."
	fi   
        let "index = $index + 1"
done
