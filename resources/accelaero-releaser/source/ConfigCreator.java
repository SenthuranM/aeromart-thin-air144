import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

/**
 * Configuration Creator for AccelAero
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ConfigCreator {

	private static Properties properties;

	private static String elementScan = null;

	/**
	 * Visit all files
	 * 
	 * @param dir
	 * @throws Exception
	 */
	public static void visitAllFiles(File dirLevel1) throws Exception {
		if (dirLevel1.isDirectory()) {
			String[] elmentsLevel1 = dirLevel1.list();
			for (int x = 0; x < elmentsLevel1.length; x++) {
				if (elmentsLevel1[x].equals("client")) {
					System.out.println(" [-] DELETED Client Directory");
					ConfigUtils.deleteDir(new File(dirLevel1 + "/" + elmentsLevel1[x]));
				} else if (elmentsLevel1[x].equals("repository")) {
					File dirLevel2 = new File(dirLevel1 + "/" + elmentsLevel1[x]);
					if (dirLevel2.isDirectory()) {
						String[] elmentsLevel2 = dirLevel2.list();
						for (int y = 0; y < elmentsLevel2.length; y++) {
							File dirLevel3 = new File(dirLevel2 + "/" + elmentsLevel2[y]);
							if (dirLevel3.isDirectory()) {
								String[] elmentsLevel3 = dirLevel3.list();
								for (int z = 0; z < elmentsLevel3.length; z++) {
									handleParsing(dirLevel3, elmentsLevel3[z]);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Handle parsing logic
	 * 
	 * @param dir
	 * @param module
	 * @throws Exception
	 */
	private static void handleParsing(File dir, String module) throws Exception {
		elementScan = null;

		if (module.equals("persistence.mod.xml")) {
			File file = ConfigUtils.getFileName(dir, module, "persistence");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handlePersistenceElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airadmin")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleAdminElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("aircustomer")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleCustomerElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airinventory")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleInventoryElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airmaster")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleAirMasterElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airschedules")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleAirSchedulesElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airpricing")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleAirPricingElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airsecurity")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleAirSecurityElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("auditor")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleAuditorElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("crypto")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleCryptoElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("paymentbroker")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handlePaymentBrokerElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("reporting")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleReportingElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("reportingframework")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleReportingFrameworkElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airreservation")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleReservationElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("airtravelagents")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleTravelAgentElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("commons")) {
			File file = ConfigUtils.getFileName(dir, module, "globalConfig");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleCommonsElement(element));
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("ibe")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleIBEElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("messaging")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleMessagingElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("scheduledservices")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleScheduledServicesElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("scheduler")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleSchedulerElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("xbe")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleXBEElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("webservices")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleWebServicesElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		} else if (module.equals("wsclient")) {
			File file = ConfigUtils.getFileName(dir, module, "config");
			Collection contents = ConfigUtils.getContents(file);
			Collection newContents = new ArrayList();
			for (Iterator iter = contents.iterator(); iter.hasNext();) {
				String element = (String) iter.next();
				newContents.add(handleWebServiceClientElement(element));
				checkElementFound(element, module);
			}
			ConfigUtils.process(file, newContents.toArray());
		}
	}

	/**
	 * Returns the naming provider extracted
	 * 
	 * @param element
	 * @return
	 */
	private static String getNamingProviderExtracted(String element) {
		return element.substring(0, element.indexOf(">") + ">".length()) + properties.get("namingProvider")
				+ element.substring(element.indexOf("</prop>"));
	}

	/**
	 * Handle Web Service Client Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleWebServiceClientElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Reporting Framework Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleReportingFrameworkElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Reporting Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleReportingElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Payment Broker Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handlePaymentBrokerElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Crypto
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleCryptoElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle the Auditor Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleAuditorElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle the Air Security Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleAirSecurityElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Air Schedules Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleAirSchedulesElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Air Pricing Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleAirPricingElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handle Air Master Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleAirMasterElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handles the inventory element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleInventoryElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Checks whether the element found
	 * 
	 * @param element
	 */
	private static void checkElementFound(String element, String module) {
		// Persistence Checking
		if (module.equals("persistence.mod.xml")) {
			if (element.indexOf("\"myXDBDataSourceUtil\"") != -1) {
				elementScan = "myXDBDataSourceUtil";
			} else if (element.indexOf("\"my1DataSource\"") != -1) {
				elementScan = "my1DataSource";
			}
			// Messaging Checking
		} else if (module.equals("messaging")) {
			if (element.indexOf("\"hostAddress\"") != -1) {
				elementScan = "hostAddress";
			}
			// Scheduled Services Checking
		} else if (module.equals("scheduledservices")) {
			if (element.indexOf("\"username\"") != -1) {
				elementScan = "username";
			} else if (element.indexOf("\"password\"") != -1) {
				elementScan = "password";
			}
			// Scheduler Checking
		} else if (module.equals("scheduler")) {
			if (element.indexOf("\"username\"") != -1) {
				elementScan = "username";
			} else if (element.indexOf("\"password\"") != -1) {
				elementScan = "password";
			}
			// IBE Checking
		} else if (module.equals("ibe")) {
			if (element.indexOf("\"enableTracking\"") != -1) {
				elementScan = "enableTracking";
			} else if (element.indexOf("\"defaultPaymentBroker\"") != -1) {
				elementScan = "defaultPaymentBroker";
			} else if (element.indexOf("\"supportCreditCardPayments\"") != -1) {
				elementScan = "supportCreditCardPayments";
			} else if (element.indexOf("\"supportOnHoldReservations\"") != -1) {
				elementScan = "supportOnHoldReservations";
			} else if (element.indexOf("\"supportCOSSelection\"") != -1) {
				elementScan = "supportCOSSelection";
			} else if (element.indexOf("\"secureIBEUrl\"") != -1) {
				elementScan = "secureIBEUrl";
			} else if (element.indexOf("\"nonsecureIBEUrl\"") != -1) {
				elementScan = "nonSecureIBEUrl";
			}
			// XBE Checking
		} else if (module.equals("xbe")) {
			if (element.indexOf("\"defaultPaymentBroker\"") != -1) {
				elementScan = "defaultPaymentBroker";
			}
		}
	}

	/**
	 * Handling Reservation Changes
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleReservationElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		} else if (element.indexOf("\"sitaMessageSender\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("sitaMessageSender") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"person_a\"") != -1) {
			element = element.substring(0, element.indexOf(">") + ">".length()) + properties.get("person_a")
					+ element.substring(element.lastIndexOf("<"));
		} else if (element.indexOf("\"person_b\"") != -1) {
			element = element.substring(0, element.indexOf(">") + ">".length()) + properties.get("person_b")
					+ element.substring(element.lastIndexOf("<"));
		} else if (element.indexOf("\"defaultMailServer\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("defaultMailServer") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"transferToSqlServer\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("transferToSqlServer") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling XBE Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleXBEElement(String element) {
		if (element.indexOf("\"port\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("port") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"sslPort\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("sslPort") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"nonSecureProtocol\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("nonSecureProtocol") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"secureProtocol\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("secureProtocol") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"loadExtPayDetailsInACC\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("loadExtPayDetailsInACC") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("defaultPaymentBroker") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("xbeDefaultPaymentBroker") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handle Web Services Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleWebServicesElement(String element) {
		if (elementScan != null && elementScan.indexOf("defaultPaymentBroker") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("wsDefaultPaymentBroker") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling Scheduler Changes
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleSchedulerElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		} else if (elementScan != null && elementScan.indexOf("username") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("aausername") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("password") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("aapassword") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling Scheduler Service Changes
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleScheduledServicesElement(String element) {
		if (elementScan != null && elementScan.indexOf("username") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("aausername") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("password") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("aapassword") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling Commons Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleMessagingElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		} else if (element.indexOf("@") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("sitaMessageSender") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("hostAddress") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("defaultMailServer") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Returns the port number after formating
	 * 
	 * @param portNumber
	 * @return
	 */
	private static String getPortNumber(Object portNumber) {
		if ("80".equals(portNumber.toString())) {
			return "";
		}
		return ":" + portNumber;
	}

	/**
	 * Handling Commons Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleIBEElement(String element) {
		if (elementScan != null && elementScan.indexOf("secureIBEUrl") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("secureProtocol") + "://" + properties.get("serverName")
					+ getPortNumber(properties.get("sslPort")) + "/ibe/public/"
					+ element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("nonSecureIBEUrl") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("nonSecureProtocol") + "://" + properties.get("serverName")
					+ getPortNumber(properties.get("port")) + "/ibe/public/"
					+ element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("enableTracking") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length()) + "false"
					+ element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("defaultPaymentBroker") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("ibeDefaultPaymentBroker") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("supportCreditCardPayments") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("supportCreditCardPayments") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("supportOnHoldReservations") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("supportOnHoldReservations") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("supportCOSSelection") != -1) {
			elementScan = null;
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("supportCOSSelection") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling Commons Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleCommonsElement(String element) {
		if (element.indexOf("0A98F21B") != -1) {
			element = element.substring(0, element.indexOf("\"") + "\"".length()) + properties.get("serverip")
					+ element.substring(element.lastIndexOf("\""));
		} else if (element.indexOf("ftpServerName") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("ftpServerName") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("ftpServerUserName") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("ftpServerUserName") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("ftpServerPassword") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("ftpServerPassword") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("lastBuildVersion") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ ConfigUtils.getBuildVersion() + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling Travel Agent Element
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleTravelAgentElement(String element) {
		if (element.indexOf("\"transferToSqlServer\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("transferToSqlServer") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handling Customer Changes
	 * 
	 * @param element
	 * @return
	 */
	private static Object handleCustomerElement(String element) {
		// URL Change
		if (element.indexOf("\"url\"") != -1) {
			element = element.substring(0, element.indexOf(">") + ">".length()) + properties.get("nonSecureProtocol") + "://"
					+ properties.get("serverName") + getPortNumber(properties.get("port"))
					+ "/ibe/public/confirmCustomer.action?" + element.substring(element.indexOf("</prop>"));
		} else if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		}

		return element;
	}

	/**
	 * Handling Persistence Changes
	 * 
	 * @param element
	 * @return
	 */
	private static Object handlePersistenceElement(String element) {
		if (element.indexOf("\"java.naming.provider.url\"") != -1) {
			element = getNamingProviderExtracted(element);
		} else if (elementScan != null && elementScan.indexOf("my1DataSource") != -1 && element.indexOf("\"url\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("driverUrl") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("my1DataSource") != -1 && element.indexOf("\"username\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("driverUsername") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("my1DataSource") != -1 && element.indexOf("\"password\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("driverPassword") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("myXDBDataSourceUtil") != -1
				&& element.indexOf("\"serverIp\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("xserverip") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("myXDBDataSourceUtil") != -1 && element.indexOf("port") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("xport") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("myXDBDataSourceUtil") != -1
				&& element.indexOf("\"databaseServer\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("xdatabaseServer") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("myXDBDataSourceUtil") != -1
				&& element.indexOf("\"dbUserName\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("xdbUserName") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (elementScan != null && elementScan.indexOf("myXDBDataSourceUtil") != -1
				&& element.indexOf("\"dbPassword\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("xdbPassword") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Handling Admin Changes
	 * 
	 * @param element
	 * @return
	 */
	private static String handleAdminElement(String element) {
		if (element.indexOf("\"port\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("port") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"sslPort\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("sslPort") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"nonSecureProtocol\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("nonSecureProtocol") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		} else if (element.indexOf("\"secureProtocol\"") != -1) {
			element = element.substring(0, element.indexOf(ConfigUtils.FIRST_INDEX) + ConfigUtils.FIRST_INDEX.length())
					+ properties.get("secureProtocol") + element.substring(element.indexOf(ConfigUtils.LAST_INDEX));
		}

		return element;
	}

	/**
	 * Invoke the process
	 */
	private static void invoke(Properties properties) throws Exception {
		System.out.println();
		System.out.println("###########################################################");
		System.out.println("### CONFIGURATION CREATION SEQUENCE INITIATED (" + new Date() + ") ");
		System.out.println("###########################################################");
		System.out.println();
		visitAllFiles(new File((String) properties.get("targetDir")));
		System.gc();
		System.out.println();
		System.out.println("###########################################################");
		System.out.println("### CONFIGURATION CREATION SEQUENCE ENDED (" + new Date() + ") ");
		System.out.println("###########################################################");
	}

	/**
	 * Load the configurations and perform nessary validations
	 */
	private static void loadConfigurations(String configFileName, long buildVersion) {
		try {
			System.out.println(" (((o))) CONFIGURATION FILE IS (" + configFileName + ") ");
			properties = new Properties();
			properties.load(new FileInputStream(configFileName));

			// Loading the configuration version
			String strConfigVersion = (String) properties.get("version");
			long configVersion = Long.parseLong((strConfigVersion == null) ? "0" : strConfigVersion);

			// Checking build versions
			if (buildVersion == configVersion) {
				invoke(properties);
			} else {
				System.out.println("");
				System.out.println("AccelAero Releaser and AccelAero Configuration file does not match. ");
				System.out.println("Please make sure you are running compatible versions");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		long buildVersion = 200710031510L;
		System.out.println("###########################################################");
		System.out.println("###    ACCELAERO RELEASER(TM) BUILD  [" + buildVersion + "]     ###");
		System.out.println("###########################################################");
		System.out.println();

		String configFileName;

		if (args.length > 0) {
			configFileName = args[0];
		} else {
			configFileName = System.getProperty("user.dir") + "/config.properties";
		}

		File file = new File(configFileName);

		if (file.isDirectory()) {
			System.out.println(" Parameter is a directory not a configuration file ");
		} else {
			loadConfigurations(configFileName, buildVersion);
		}
	}
}
