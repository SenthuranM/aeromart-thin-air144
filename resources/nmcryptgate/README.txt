1. install nmcryptgate library
rpm -i nmcryptgate-libs-1.10.0-1.rhel3.i386.rpm

2. set LD_LIBRARY_PATH pointing to the location where nmcryptgate lib is installed

add following to /etc/profile - it assumed libnncryptgate.so is located in /usr/local/lib

LD_LIBRARY_PATH=.:/usr/local/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

after saving the profile execute following command to environment variable to take effect
source /etc/profile

3. Now copy nmcryptgate binary to /usr/local/bin and copy test.txt to the place libnmcryptgate.so resides

4. Now execute following to verify payment gw is installed properly and accessible

nmcryptgate --user=example --pass=example --service=concheckbasictest test.txt --debug

If the result of above command end as similar to following, then nmcryptgate is installed properly and accessible

rc_str=OK [ No Details ]
rc=0
_time_offset=-14828
.msts=338315404
_session=3CK95C612J9TNS028NE6P
_transaction=3CK95C612J9TNS028NETH
_trace=(netserv104,6.358,3CK95C612J9TNS028NE6P,3CK95C612J9TNS028NETH)
_req_ms=6364
_cipher=AES256-SHA
_timestamp=20061017153004
_rc=1
<-- output end -->