#!/bin/sh

builddir=/usr/isadata/build
distdir=$builddir/dist
antlog=$builddir/logs/ant.log
cvslog=$builddir/logs/cvs.log
pastbuildsdir=$builddir/past-builds
log=$builddir/logs/log.log
LAST_CHECKOUTTIME_FILE=$builddir/logs/lastcheckouttime.txt
timestamp=`date '+%Y%m%d_%H%M'`
lastcheckouttimestamp=$timestamp

if [ $# -ne 1 ]; then
  echo "Usage: $0 [build option]
		co - CheckOut and build
		up - Update and build
		no - Build only" >&2; exit 1
fi

if [ -e "$antlog" ]; then
	rm -f $antlog
fi

if [ -e "$log" ]; then
    rm -f $log
fi

if [ -e "$distdir" ]; then
	rm -rf $distdir
	mkdir $distdir
fi

echo "Starting build $timestamp" >> $log

if [ ! -e "$LAST_CHECKOUTTIME_FILE" ]; then
	echo $timestamp > $LAST_CHECKOUTTIME_FILE
fi

abort_if_failed(){
	if [ "$1" -ne 0 ];then
		echo "$2  F A I L E D !!!" >> $log 2>&1
		echo "$2  F A I L E D !!!"	
		exit
	else
		echo "$2 SUCCESS." >> $log 2>&1
		echo "$2 SUCCESS." 
	fi
}

#prepare backend
#cvs -d :pserver:isa:password@10.200.2.11:/cvsroot/isa co ThinAir
#cd ThinAir

if [ "$1" = "co" ]; then
	if [ -e "ThinAir" ]; then
		echo "Going to clean the previous build..." >> $log 2>&1
		#clean checkout
		ant -f ThinAir/build.xml clean-build  >> $antlog 2>&1
		abort_if_failed "$?" "Cleaning Build"

		# backup last checkout
		read lastcheckouttimestamp<$LAST_CHECKOUTTIME_FILE
		newName="ThinAir-$lastcheckouttimestamp"
		echo "Moving ThinAir to $newName"	
		mv ThinAir $pastbuildsdir/$newName
	fi

	echo "Going to checkout source from cvs..." >> $log 2>&1
	#cvs checkout
	cvs -d :pserver:builduser:password@10.200.2.11:/cvsroot/isa co ThinAir >> $cvslog 2>&1 
	abort_if_failed "$?" "CVS Checkout"
	echo "$timestamp" > $LAST_CHECKOUTTIME_FILE
	echo "Going to build ThinAir with fresh CHECKOUT done at $timestamp"
	echo "Going to build ThinAir with fresh CHECKOUT done at $timestamp" >> $log
 
elif [ "$1" = "up" ]; then
	#build updating ThinAir from CVS

	#abort if ThinAir folder is not there
	if [ ! -e "ThinAir" ]; then
		abort_if_failed "-1" "ThinAir folder does not exists. Run build with checkout option"
	fi

	#cvs update	
	echo "Going update source from cvs..." >> $log 2>&1
	cvs -d :pserver:builduser:password@10.200.2.11:/cvsroot/isa update ThinAir >> $cvslog 2>&1
        abort_if_failed "$?" "CVS Update"
	echo "$timestamp" > $LAST_CHECKOUTTIME_FILE
	echo "Going to build ThinAir with fresh UPDATE done at $timestamp"
        echo "Going to build ThinAir with fresh UPDATE done at $timestamp" >> $log
elif [ "$1" = "no" ]; then
	#build on the existing ThinAir folder - no updating

	#abort if ThinAir folder is not there
        if [ ! -e "ThinAir" ]; then
                abort_if_failed "-1" "ThinAir folder does not exists. Run build 
with checkout option"
        fi	
	read lastcheckouttimestamp<$LAST_CHECKOUTTIME_FILE
	echo "Going to build ThinAir with existing checkout done at $lastcheckouttimestamp"
        echo "Going to build ThinAir with existing checkout done at $lastcheckouttimestamp" >> $log
fi

cd ThinAir

cp -f $builddir/toreplace/indexDummy.html $builddir/ThinAir/ibe/src/web/js

echo "Going to build backend module..."
ant prepare-dist-jars >> $antlog 2>&1
abort_if_failed "$?" "Preparing EJB modules"

ant dist-hybrid-ear >> $antlog 2>&1
abort_if_failed "$?" "Preparing Hybrid EAR"

ant -Dproject.config.dir=$distdir/isaconfig build-config-repository-for-hybrid-ear >> $antlog 2>&1
abort_if_failed "$?" "Creating configuration files"

cp target/dist/aa-hybrid.ear $distdir >> $log 2>&1
abort_if_failed "$?" "Building EAR"

echo "Done" >> $log 2>&1
echo "Done"
exit 0
