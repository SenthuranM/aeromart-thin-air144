#!/bin/sh
#	+-----------------------------------------------+
#	| author : Nasly				|
#	| date	 : 6 Nov 2005				|
#	+-----------------------------------------------+

#echo started
#set the environement variables
ANT_HOME=/usr/apache-ant-1.6.2
PATH=$PATH:/usr/apache-ant-1.6.2/bin:/usr/j2sdk1.4.2_08/bin
JAVA_HOME=/usr/java/jdk1.6.0
export PATH JAVA_HOME ANT_HOME

BUILD_FOLDER=/home/builduser/build

# system modules and business modules
nonejbmodules=(login auditor crypto reportingframework)
modules=(messaging paymentbroker reporting airmaster airsecurity aircustomer airschedules airpricing airinventory airreservation airtravelagents)
# web modules
webmodules=(airadmin xbe ibe webservices)
#webmodules=(airadmin xbe)
#modules=(auditor)

#RECIPIENTS="byorn.desilva@jkcsworld.com,cbandara@jkcsworld.com,ChamindaP@jkcsworld.com,chan@jkcsworld.com,chandanak@jkcsworld.com,isuru@jkcsworld.com,lashini@jkcsworld.com,menakaw@jkcsworld.com,milinda@jkcsworld.com, nasly@jkcsworld.com,nilindra@jkcsworld.com,shakir@jkcsworld.com,shanikac@jkcsworld.com,srikanth@jkcsworld.com,sudheeral@jkcsworld.com,sumudupw@jkcsworld.com,thejaka@jkcsworld.com,thushara@jkcsworld.com,vinothini@jkcsworld.com,rilwan@jkcsworld.com,janaki@jkcsworld.com,kasun@jkcsworld.com"

RECIPIENTS_SUCCESS="support@isaaviation.ae"
RECIPIENTS_FAILURE="support@isaaviation.ae,tfernando@isaaviation.ae,nilindra@jkcsworld.com,sudheeral@jkcsworld.com,vinothini@jkcsworld.com"
#RECIPIENTS_FAILURE="support@isaaviation.ae"
RECIPIENTS_ADMIN="support@isaaviation.ae,nilindra@jkcsworld.com"
#RECIPIENTS_ADMIN="support@isaaviation.ae"

#DAY_RECIPIENTS="byorn.desilva@jkcsworld.com,cbandara@jkcsworld.com,ChamindaP@jkcsworld.com,isuru@jkcsworld.com,menakaw@jkcsworld.com,nasly@jkcsworld.com,nilindra@jkcsworld.com,shakir@jkcsworld.com,srikanth@jkcsworld.com,sudheeral@jkcsworld.com,sumudupw@jkcsworld.com,thejaka@jkcsworld.com,thushara@jkcsworld.com,vinothini@jkcsworld.com,rilwan@jkcsworld.com,kasun@jkcsworld.com"

DAY_RECIPIENTS="support@isaaviation.ae,tfernando@isaaviation.ae"
#DAY_RECIPIENTS="support@isaaviation.ae"

#RECIPIENTS=mnasly@gmail.com
#DAY_RECIPIENTS=mnasly@gmail.com

MSG_PREFIX="AccelAero Nightly Build"
WEBROOT=/var/www/html
suffix_date=`date '+%Y%m%d_%H%M%S'`
log_filename=$suffix_date.log
antlog_filename=ant_$log_filename
cvslog_filename=cvs_$log_filename
ant_logfile=$BUILD_FOLDER/logs/$antlog_filename
logfile=$BUILD_FOLDER/logs/$log_filename
cvslogfile=$BUILD_FOLDER/logs/$cvslog_filename
statcvslogfile=$BUILD_FOLDER/cvsstats/statcvs_$log_filename
#STATCVS_DIR=$BUILD_FOLDER/cvsstats/statcvs$suffix_date
STATCVS_DIR=$BUILD_FOLDER/cvsstats/stats

TMP_LOG=$BUILD_FOLDER/logs/email_$log_filename

failed_wm_count=0
failed_web_modules=""
echo "cvslogfile=$cvslogfile"

read_build_status(){
        read build_status<$BUILD_FOLDER/previous.buildstatus
}
read_build_status
echo "previous_build_status=$build_status"


hour="`date '+%H'`"
if [ "$hour" -gt 5 ] && [ "$hour" -lt 22 ]; then
        read_build_status
        if [ "$build_status" -eq "0" ]; then
		echo "Exiting as midnight build success"
                exit
        fi
	echo "Start fixing midnight build"
fi

increment_build_version(){
        read num < $BUILD_FOLDER/build.versions
        newnum=`expr "$num" "+" "1"`
        echo $newnum > $BUILD_FOLDER/build.versions
}
increment_build_version
CVSROOT=:pserver:builduser:password@10.200.2.11:/cvsroot/isa
success="$MSG_PREFIX [$newnum] SUCCESSFUL"
failure="$MSG_PREFIX [$newnum] FAILED"
completed="$MSG_PREFIX [$newnum] COMPLETED"
fixed="$MSG_PREFIX [$newnum] FIXED"

revert_build_version(){
	read num < $BUILD_FOLDER/build.versions
        newnum=`expr "$num" "-" "1"`
        echo $newnum > $BUILD_FOLDER/build.versions
}

set_build_status(){
	echo $1 > $BUILD_FOLDER/previous.buildstatus
}

send_email(){
	
	if [ "$hour" -gt 5 ] && [ "$hour" -lt 18 ]; then
		SELECTED_USERS="$DAY_RECIPIENTS"
	else
		SELECTED_USERS="$3"
	fi
	if [ -n "$4" ]; then
		echo "subject=$1"
		echo "logfile=$2"
		echo "recipients=$SELECTED_USERS"
	#	echo "log.dir=$BUILD_FOLDER/logs"
	#	echo "attachements_fileter=$4"
		ant -f $BUILD_FOLDER/email.xml send.email.wa -Dsubject="$1" -Dlogfile="$2" -Drecipients="$SELECTED_USERS" -Dattachments="$4" -verbose >> $TMP_LOG 2>&1
	else
		ant -f $BUILD_FOLDER/email.xml send.email -Dsubject="$1" -Dlogfile="$2" -Drecipients="$SELECTED_USERS" -verbose >> $TMP_LOG 2>&1
	fi
}

abort_if_failed(){
	if [ "$1" -ne 0 ];then
		echo "***************FAILURE*****************"
		echo "*********** Exit code = $1 *************"
		echo "***************************************"
		revert_build_version	
		echo "recipients=$4"
		echo "cvslogfile=$5"

		echo "" >> $3 2>&1		
        	echo "$2 at `date '+%d/%b/%Y-%H:%M'`" >> $3 2>&1
		echo "See attached log file(s) for details"  >> $3 2>&1
		echo "" >> $3 2>&1
		set_build_status "1"
		if [ -n "$5" ]; then
			send_email "$2" "$3" "$4" "$5"
		else
			send_email "$2" "$3" "$4"
		fi
		exit
	else
		if [ -n "$6" ]; then
			echo "Successfully built : [$6] " >> $logfile 2>&1
		fi
	fi
}

web_mod_status(){
        if [ "$1" -ne 0 ];then
		echo "WEB MODULE BUILD FAILURE  : [$2] (See $antlog_filename for details)" >> $logfile 2>&1
		let "failed_wm_count=$failed_wm_count + 1"
		if [ "$failed_web_modules" = "" ];then
			failed_web_modules="$2"
		else
			failed_web_modules="$failed_web_modules,$2"
		fi
        else
                echo "Successfully built : [$2] " >> $logfile 2>&1
        fi
}


build_all(){
	cd $BUILD_FOLDER/ThinAir
	echo "" >> $logfile 2>&1
        ant -f buildutils/build.xml dist-core >> $ant_logfile 2>&1
        abort_if_failed "$?" "$failure, building  [buildutils] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file"
        ant -f build.xml empty-module-repository >> $ant_logfile 2>&1
	abort_if_failed "$?" "$failure, emptying modules repository failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file"
        ant -f platform/build.xml dist-core >> $ant_logfile 2>&1
	abort_if_failed "$?" "$failure, building [platform] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file" "platform"

	ant -f commons/build.xml dist-core >> $ant_logfile 2>&1        
	abort_if_failed "$?" "$failure, building [commons] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file" "commons"

        ant -f build.xml dist-apis >> $ant_logfile 2>&1
	abort_if_failed "$?" "$failure, building [api] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file" "api"
                                                                                                                                                             
	ant -f webplatform/build.xml dist-core >> $ant_logfile 2>&1
        abort_if_failed "$?" "$failure, building  [webplatform] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file" "webplatform"	

        # building system and business modules
	echo "" >> $logfile 2>&1
        echo "System/Business Modules Build Status" >> $logfile 2>&1
	echo "" >> $logfile 2>&1

	nonejb_modules_count=${#nonejbmodules[@]}
        index=0
        while [ "$index" -lt "$nonejb_modules_count" ]
        do
                echo "Prepare dist for  ${nonejbmodules[$index]}"
                ant -f ${nonejbmodules[$index]}/build.xml dist-core >> $ant_logfile 2>&1                
		abort_if_failed "$?" "$failure, building [${nonejbmodules[$index]}] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file" "${nonejbmodules[$index]}"
                let "index = $index + 1"
        done
	
        modules_count=${#modules[@]}
        index=0
        while [ "$index" -lt "$modules_count" ]
        do
                echo "Prepare dist for  ${modules[$index]}"
                ant -f ${modules[$index]}/build.xml dist-all >> $ant_logfile 2>&1
		abort_if_failed "$?" "$failure, building [${modules[$index]}] failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file" "${modules[$index]}"
                let "index = $index + 1"
        done
                                                                                                                                                             
        # building web modules
	echo "" >> $logfile 2>&1
	echo "Web Modules Build Status" >> $logfile 2>&1
	echo "" >> $logfile 2>&1

        webmodules_count=${#webmodules[@]}
        index=0
        while [ "$index" -lt "$webmodules_count" ]
        do
                echo "Prepare war for  ${webmodules[$index]}"
                ant -f ${webmodules[$index]}/build.xml create-war  >> $ant_logfile 2>&1
       		web_mod_status "$?" "${webmodules[$index]}"
                let "index = $index + 1"
        done
                                                                                                                                                             
        # removing module build target folders
        ant clean-build  >> $ant_logfile 2>&1
	abort_if_failed "$?" "$failure, cleaning build targets failed" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,$loc_per_auther_file"
}

generate_cvsstats(){
	echo "Generating CVS stats..."
	cd $BUILD_FOLDER/ThinAir
	cvs -d $CVSROOT log > $statcvslogfile
	abort_if_failed "$?" "$failure, CVS log creationg failed" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"
	cd $BUILD_FOLDER/cvsstats
	if [ -e $STATCVS_DIR ]; then
		rm -rf $STATCVS_DIR
		abort_if_failed "$?" "$failure, Old CVS stats removal failed" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"
	fi
	mkdir $STATCVS_DIR
	java -jar $BUILD_FOLDER/statcvs.jar $statcvslogfile $BUILD_FOLDER/ThinAir -output-dir $STATCVS_DIR -include "**/*.java;**/*.js" >> $cvslogfile 2>&1
	abort_if_failed "$?" "$failure, CVS stat gen failed" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"
#	cat $STATCVS_DIR/commit_log.html >> $logfile 2>&1
	if [ -e $WEBROOT/stats ]; then
		rm -rf $WEBROOT/stats
		abort_if_failed "$?" "$failure, Undeploying CVS stats failed" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"
	fi
	cp -rf $STATCVS_DIR $WEBROOT
	abort_if_failed "$?" "$failure, Deploying CVS stats failed" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"
	echo "Done CVS stats gen."
}
read_build_status
echo "This is an automated email generated by AccelAero build."  >> $logfile 2>&1
echo "" >> $logfile 2>&1

#checkout/update the project from cvs
cvs -d $CVSROOT login >> $cvslogfile 2>&1
abort_if_failed "$?" "$failure, Could not connect to CVS Server" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"

if [ -e "$BUILD_FOLDER/ThinAir" ]; then 
	#cd $BUILD_FOLDER/ThinAir
	#echo "Updating ThinAir from CVS" >> $logfile 2>&1
	#cvs -d $CVSROOT update -d >> $cvslogfile 2>&1
	#abort_if_failed "$?" "$failure, CVS Update failed" "$logfile" "$RECIPIENTS" "$cvslogfile"
	cd $BUILD_FOLDER
	rm -rf ThinAir
#else
#	cd $BUILD_FOLDER
#	#echo "Checking out ThinAir from CVS" >> $logfile 2>&1
#	cvs -d $CVSROOT co ThinAir >> $cvslogfile 2>&1
#	abort_if_failed "$?" "$failure, CVS Checkout failed" "$logfile" "$RECIPIENTS" "$cvslogfile"
fi
#checkout the project from cvs
echo "Checking out source..."
cvs -d $CVSROOT co ThinAir >> $cvslogfile 2>&1
abort_if_failed "$?" "$failure, CVS Checkout failed" "$logfile" "$RECIPIENTS_ADMIN" "$cvslogfile"
echo "Done checking out."
# generate cvs statistics
generate_cvsstats
echo "Access CVS stats from http://10.200.2.11/stats/ [local] or http://213.42.198.203/stats/" >> $logfile 2>&1
echo "" >> $logfile 2>&1

loc_per_auther_file="$STATCVS_DIR/loc_per_author.png"

cd $BUILD_FOLDER/ThinAir
echo "Started ThinAir build [$newnum]  at `date '+%d/%b/%Y-%H:%M:%S'`" >> $logfile 2>&1
# build modules
echo "Starting modules build..."
build_all
echo "Done modules build."

echo "" >> $logfile 2>&1

msg="$success"

if [ "$failed_wm_count" -ne 0 ]; then
   	msg="$failure, building [${failed_web_modules}] failed"
   	abort_if_failed "-1" "$msg" "$logfile" "$RECIPIENTS_FAILURE" "$cvslogfile,$ant_logfile,${loc_per_auther_file}"
fi

if [ "$build_status" -eq 0 ]; then
	msg="$success"
	echo "$msg  at `date '+%d/%b/%Y-%H:%M:%S'`" >> $logfile 2>&1
else
        msg="$fixed"
	echo "$msg  at `date '+%d/%b/%Y-%H:%M:%S'`" >> $logfile 2>&1
fi
echo "" >> $logfile 2>&1
set_build_status "0"

send_email "$msg" "$logfile" "$RECIPIENTS_SUCCESS" "$cvslogfile,$ant_logfile,$loc_per_auther_file"
echo "RECIPIENTS=$RECIPIENTS" >> $TMP_LOG 2>&1
echo "Done"
exit 0

