#!/bin/sh

# +-------------------------------------------------------------+
# | Purpose	: Script for clustered production build		|
# | Author	: Mohamed Nasly					|
# | Date	: 01 April 2006					|
# +-------------------------------------------------------------+

BUILD_DIR=/oradata/AAReservations/clustered
DIST_DIR=$BUILD_DIR/dist-production
PAST_BUILDS_DIR=$BUILD_DIR/past-builds
LOGS_DIR=$BUILD_DIR/logs-all/logs
MASTER_LOG_FILE=$BUILD_DIR/logs-all/master.log
PAST_LOGS_DIR=$BUILD_DIR/logs-all/past-logs

if [ $# -ne 4 ]; then
  echo "Usage: $0 <cvs operation> <cluster identifier> <which modules to build option>
	cvs operation
		co - checkout and build
		up - update and build
		no - build existing source
	client idenfier
		-1 - [AirArabia] 	- default
		00 - [Not Used] 	- app-00
		01 - [MihinLanka] 	- app-01
		02 - [TMA] 		- app-02
		04 - [Yeti] 		- app-04
		05 - [Jupiter] 		- app-05
	build options
       		1 - all
       		2 - be only
       		3 - webs only
       		4 - private web only
       		5 - public web only
       		6 - be, pri and pub web only
	past build identifier
		-1 - [AirArabia]        - default
                00 - [Not Used]         - app-00
                01 - [MihinLanka]       - app-01
                02 - [TMA]              - app-02
                04 - [Yeti]             - app-04
                05 - [Jupiter]          - app-05
	cvs branch name [optional]">&2; exit 1
fi

#inputs
CVS_OPTION=$1
CLIENT_OPTION=$2
BUILD_OPTION=$3
PAST_BUIID_IDENTIFIER=$4
CVS_BRANCH=" "

DIST_MODULES_EAR_TARGET=prepare-module-ejbs-ear
MODULE_EAR_CONFIGS_TARGET=build-config-repository-for-module-ejbs-ear
if [ "$2" = "05" ]; then
	DIST_MODULES_EAR_TARGET=dist-modules-ejb-ear-all-in-one
	MODULE_EAR_CONFIGS_TARGET=build-config-repository-modules-ejb-ear-all-in-one
fi

if [ $5 ]; then
	CVS_BRANCH=" -r $5 "
fi

if [ "$CLIENT_OPTION" -ne "-1" ]; then
	DIST_DIR=$BUILD_DIR/dist-production-$2
	LOGS_DIR=$LOGS_DIR-$2
	if [ ! -e $LOGS_DIR ]; then
		mkdir $LOGS_DIR
	fi
fi

if [ "$PAST_BUIID_IDENTIFIER" -ne "-1" ]; then
	PAST_BUILDS_DIR=$PAST_BUILDS_DIR-$5
	PAST_LOGS_DIR=$PAST_LOGS_DIR-$5
	if [ ! -e $PAST_BUILDS_DIR ]; then
		mkdir $PAST_BUILDS_DIR
	fi
	if [ ! -e $PAST_LOGS_DIR ]; then
		mkdir $PAST_LOGS_DIR
	fi
fi

#updated file/dir paths
ANT_LOG_FILE=$LOGS_DIR/ant.log
STATUS_LOG_FILE=$LOGS_DIR/status.log
CVS_LOG_FILE=$LOGS_DIR/cvs.log
LAST_CHECKOUTTIME_FILE=$LOGS_DIR/lastcheckouttime.txt
LAST_BUILD_TIME_FILE=$LOGS_DIR/lastbuildtime.txt

timestamp=`date '+%Y%m%d_%H%M'`
lastcheckouttimestamp=$timestamp
lastbuildtimestamp=$timestamp

abort_if_failed(){
        if [ "$1" -ne 0 ];then
                echo "$2  F A I L E D !!!"
                echo "$2  F A I L E D !!!" >> $STATUS_LOG_FILE 2>&1
		echo "STATUS [BUILD_TIME=$lastbuildtimestamp] $2 FAILED!" >> $MASTER_LOG_FILE 2>&1
                exit
        else
                echo "$2 SUCCESS."
                echo "$2 SUCCESS." >> $STATUS_LOG_FILE 2>&1
        fi
}

if [ ! -e "$LAST_BUILD_TIME_FILE" ]; then
        echo $timestamp > $LAST_BUILD_TIME_FILE
fi

read lastbuildtimestamp<$LAST_BUILD_TIME_FILE

if [ -e "$STATUS_LOG_FILE" ]; then
        mv  -f $STATUS_LOG_FILE $PAST_LOGS_DIR/status_$lastbuildtimestamp.log
        abort_if_failed "$?" "Moving $STATUS_LOG_FILE"
        #echo "Moved $STATUS_LOG_FILE to $PAST_LOGS_DIR/status_$lastcheckouttimestamp" >> $STATUS_LOG_FILE
fi

echo "Starting current build $timestamp args=[$1,$2,$3,$4,$5]" >> $STATUS_LOG_FILE

echo "Last build timestamp=$timestamp" >> $MASTER_LOG_FILE
echo "Last build options:
		cvs operation         = $1
		client identifier     = $2
		build option          = $3
                past build identifier = $4
		cvs branch [optional] = $5" >> $MASTER_LOG_FILE

echo "Last build was done at $lastbuildtimestamp" >> $STATUS_LOG_FILE

if [ ! -e "$LAST_CHECKOUTTIME_FILE" ]; then
        echo $timestamp > $LAST_CHECKOUTTIME_FILE
fi

read lastcheckouttimestamp<$LAST_CHECKOUTTIME_FILE
echo "Last CVS checkout/update was done at $lastcheckouttimestamp" >> $STATUS_LOG_FILE

if [ -e "$ANT_LOG_FILE" ]; then
	mv -f $ANT_LOG_FILE $PAST_LOGS_DIR/ant_$lastbuildtimestamp.log
	abort_if_failed "$?" "Moving $ANT_LOG_FILE"
	echo "Moved $ANT_LOG_FILE to $PAST_LOGS_DIR/ant_$lastbuildtimestamp.log" >> $STATUS_LOG_FILE
fi

if [ -e "$CVS_LOG_FILE" ]; then
        mv -f $CVS_LOG_FILE $PAST_LOGS_DIR/cvs_$lastbuildtimestamp.log
        abort_if_failed "$?" "Moving $CVS_LOG_FILE"
	echo "Moved $CVS_LOG_FILE to $PAST_LOGS_DIR/cvs_$lastbuildtimestamp.log" >> $STATUS_LOG_FILE
fi

if [ -e "$DIST_DIR" ]; then
	rm -rf $DIST_DIR
	echo "$?" "Removing $DIST_DIR"
	echo "Removed $DIST_DIR" >> $STATUS_LOG_FILE
	mkdir $DIST_DIR
	echo "$?" "Creating $DIST_DIR"
	echo "Created $DIST_DIR" >> $STATUS_LOG_FILE
fi


if [ "$CVS_OPTION" = "co" ]; then
	if [ -e "ThinAir" ]; then
		echo "Going to clean the previous build..." >> $STATUS_LOG_FILE 2>&1
                #clean previous checkout
                ant -f ThinAir/build.xml clean-build  >> $ANT_LOG_FILE 2>&1
                abort_if_failed "$?" "Cleaning Build"

		# backup last checkout
		newName="ThinAir-$lastcheckouttimestamp"
		echo "Moving ThinAir to $newName"	
		mv ThinAir $PAST_BUILDS_DIR/$newName
		echo "Moving ThinAir to $PAST_BUILDS_DIR/$newName" >> $STATUS_LOG_FILE 2>&1
	fi
	
	echo "Going to checkout source from CVS... branch option=[$CVS_BRANCH]" >> $STATUS_LOG_FILE 2>&1
	#cvs checkout
	cvs -d :pserver:builduser:password@10.200.2.11:/cvsroot/isa co $CVS_BRANCH  ThinAir >> $CVS_LOG_FILE 2>&1
	abort_if_failed "$?" "CVS Checkout"
	echo "$timestamp" > $LAST_CHECKOUTTIME_FILE
	echo "Going to build ThinAir with fresh CHECKOUT done at $timestamp"
	echo "Going to build ThinAir with fresh CHECKOUT done at $timestamp" >> $STATUS_LOG_FILE
 
elif [ "$CVS_OPTION" = "up" ]; then
	#build updating ThinAir from CVS
	
	echo "Going to update soruce from CVS... branch option=[$CVS_BRANCH]" >> $STATUS_LOG_FILE 2>&1
	#abort if ThinAir folder is not there
	if [ ! -e "ThinAir" ]; then
		abort_if_failed "-1" "ThinAir folder does not exists. Run build with checkout option"
	fi

	#cvs update	
	cvs -d :pserver:builduser:password@10.200.2.11:/cvsroot/isa update $CVS_BRANCH ThinAir >> $CVS_LOG_FILE 2>&1
        abort_if_failed "$?" "CVS Update"
	echo "$timestamp" > $LAST_CHECKOUTTIME_FILE
	echo "Going to build ThinAir with fresh UPDATE done at $timestamp"
        echo "Going to build ThinAir with fresh UPDATE done at $timestamp" >> $STATUS_LOG_FILE
elif [ "$CVS_OPTION" = "no" ]; then
	#build on the existing ThinAir folder - no updating

	#abort if ThinAir folder is not there
        if [ ! -e "ThinAir" ]; then
                abort_if_failed "-1" "ThinAir folder does not exists. Run build 
with checkout option"
        else
		echo "Going to clean the previous build..." >> $STATUS_LOG_FILE 2>&1
                #clean previous checkout
                #ant -f ThinAir/build.xml clean-build  >> $ANT_LOG_FILE 2>&1
                abort_if_failed "$?" "Cleaning Build"
	fi	
	read lastcheckouttimestamp<$LAST_CHECKOUTTIME_FILE
	echo "Going to build ThinAir with existing checkout done at $lastcheckouttimestamp"
        echo "Going to build ThinAir with existing checkout done at $lastcheckouttimestamp" >> $STATUS_LOG_FILE
else
	echo "Invalid build option"
	exit
fi


# replace xdoclet customized for clustered build
cp -f  $BUILD_DIR/toreplace/xdoclet-jboss-module-1.2.3.jar $BUILD_DIR/ThinAir/repository/lib/build

# replace AirAdmin Menu configuration customized for production
cp -f $BUILD_DIR/ThinAir/airadmin/src/web/WEB-INF/MenuData.xml $BUILD_DIR/ThinAir/airadmin/src/web/WEB-INF/MenuDataDefault.xml
cp -f $BUILD_DIR/ThinAir/airadmin/src/web/WEB-INF/MenuDataProduction.xml $BUILD_DIR/ThinAir/airadmin/src/web/WEB-INF/MenuData.xml

# replace payment broker configurations for production access
if [ "$CLIENT_OPTION" = -1 ]; then
	echo "Client -1 payment broker settings - using the one in CVS" >> $STATUS_LOG_FILE
	echo "Default cluster configurations are used for building" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/jboss-cluster-config.xml $BUILD_DIR/ThinAir/resources/xdoclet/mergedir/jboss-cluster-config.xml
	echo "Client -1 project properties are used" >> $STATUS_LOG_FILE
	cp -f $BUILD_DIR/toreplace/project.properties $BUILD_DIR/ThinAir/project.properties
elif [ "$CLIENT_OPTION" = "00" ]; then
	echo "Client 00 payment broker settings are used for building" >> $STATUS_LOG_FILE
	cp -f $BUILD_DIR/toreplace/PaymentBrokerPro_en_US.properties-00 ThinAir/paymentbroker/src/resources/resource-bundles/PaymentBrokerPro_en_US.properties
	echo "Cluster configurations for app-00 are used for building" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/jboss-cluster-config-00.xml $BUILD_DIR/ThinAir/resources/xdoclet/mergedir/jboss-cluster-config.xml
	echo "Client 00 project properties are used" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/project.properties-00 $BUILD_DIR/ThinAir/project.properties
elif [ "$CLIENT_OPTION" =  "01" ]; then
	echo "Client 01 payment broker settings - using the one in CVS" >> $STATUS_LOG_FILE
	echo "Cluster configurations for app-01 are used for building" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/jboss-cluster-config-01.xml $BUILD_DIR/ThinAir/resources/xdoclet/mergedir/jboss-cluster-config.xml
	echo "Client 01 project properties are used" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/project.properties-01 $BUILD_DIR/ThinAir/project.properties
elif [ "$CLIENT_OPTION" =  "02" ]; then
	echo "Client 02 payment broker settings - using the one in CVS" >> $STATUS_LOG_FILE
	echo "Cluster configurations for app-02 are used for building" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/jboss-cluster-config-02.xml $BUILD_DIR/ThinAir/resources/xdoclet/mergedir/jboss-cluster-config.xml
	echo "Client 02 project properties are used" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/project.properties-02 $BUILD_DIR/ThinAir/project.properties
elif [ "$CLIENT_OPTION" =  "04" ]; then
        echo "Client 04 payment broker settings - using the one in CVS" >> $STATUS_LOG_FILE
	echo "Cluster configurations for app-04 are used for building" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/jboss-cluster-config-04.xml $BUILD_DIR/ThinAir/resources/xdoclet/mergedir/jboss-cluster-config.xml
	echo "Client 04 project properties are used"  >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/project.properties-04 $BUILD_DIR/ThinAir/project.properties
elif [ "$CLIENT_OPTION" =  "05" ]; then
        echo "Client 05 payment broker settings - using the one in CVS" >> $STATUS_LOG_FILE
	echo "Non-cluster configurations for app-05 are used for building" >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/jboss-cluster-config-05.xml $BUILD_DIR/ThinAir/resources/xdoclet/mergedir/jboss-cluster-config.xml	
	echo "Client 05 project properties are used"  >> $STATUS_LOG_FILE
        cp -f $BUILD_DIR/toreplace/project.properties-05 $BUILD_DIR/ThinAir/project.properties
else
	echo "Invalid client option"
	exit
fi

cd ThinAir

if [ "$BUILD_OPTION" = "1" ] || [ "$BUILD_OPTION" = "2" ] || [ "$BUILD_OPTION" = "6" ] ; then
	# prepare backend modules
	echo "Going to build backend modules " >> $STATUS_LOG_FILE
	ant prepare-dist-jars >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Preparing module dists"
	ant $DIST_MODULES_EAR_TARGET >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "EJB modules build"
	ant -Dproject.config.dir=$DIST_DIR/isaconfig-be $MODULE_EAR_CONFIGS_TARGET >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "BackEnd configurations copying"
	cp target/dist/aa-ejb-modules.ear $DIST_DIR >> $STATUS_LOG_FILE 2>&1
fi

if [ "$BUILD_OPTION" = "1" ] || [ "$BUILD_OPTION" = "3" ] || [ "$BUILD_OPTION" = "5" ] || [ "$BUILD_OPTION" = "6" ] ; then
	#build ws-client in non-clustered mode
	echo "Building wsclient in non-clustered mode " >> $STATUS_LOG_FILE
	cp -f  $BUILD_DIR/toreplace/xdoclet-jboss-module-1.2.3.jar-original $BUILD_DIR/ThinAir/repository/lib/build/xdoclet-jboss-module-1.2.3.jar
	cd wsclient
	ant dist-core >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Preparing wsclient"
	cd ..
	#prepare public web
	echo "Going to build public web module " >> $STATUS_LOG_FILE
	ant dist-public-web-modules-ear >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Public web build"
	ant -Dproject.config.dir=$DIST_DIR/isaconfig-pub build-config-repository-for-dist-public-web-modules-ear >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Public web configurations copying"
	cp target/dist/aa-publicweb.ear $DIST_DIR >> $STATUS_LOG_FILE 2>&1
fi

if [ "$BUILD_OPTION" = "1" ] || [ "$BUILD_OPTION" = "3" ] || [ "$BUILD_OPTION" = "4" ] || [ "$BUILD_OPTION" = "6" ] ; then
	#build ws-client in non-clustered mode
        echo "Building wsclient in non-clustered mode " >> $STATUS_LOG_FILE
        cp -f  $BUILD_DIR/toreplace/xdoclet-jboss-module-1.2.3.jar-original $BUILD_DIR/ThinAir/repository/lib/build/xdoclet-jboss-module-1.2.3.jar
        cd wsclient
        ant dist-core >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Preparing wsclient"
        cd ..
	#prepare private web
	echo "Going to build private web module" >> $STATUS_LOG_FILE
	ant dist-private-web-modules-ear >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Private web modules build"
	ant -Dproject.config.dir=$DIST_DIR/isaconfig-pri build-config-repository-for-dist-private-web-modules-ear >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Private web configurations copying"
	cp target/dist/aa-privateweb.ear $DIST_DIR >> $STATUS_LOG_FILE 2>&1
fi

if [ "$BUILD_OPTION" = "1" ] || [ "$BUILD_OPTION" = "3" ] ; then
	#build ws-client in non-clustered mode
        echo "Building wsclient in non-clustered mode " >> $STATUS_LOG_FILE
        cp -f  $BUILD_DIR/toreplace/xdoclet-jboss-module-1.2.3.jar-original $BUILD_DIR/ThinAir/repository/lib/build/xdoclet-jboss-module-1.2.3.jar
        cd wsclient
        ant dist-core >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "Preparing wsclient"
        cd ..
	#prepare ear with all web modules
	echo "Going to build all in one web module" >> $STATUS_LOG_FILE
	ant dist-web-modules-ear >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "web modules ear  build"
	ant -Dproject.config.dir=$DIST_DIR/isaconfig-web build-config-repository-for-web-modules-ear >> $ANT_LOG_FILE 2>&1
	abort_if_failed "$?" "web configurations copying"
	cp target/dist/aa-web-modules.ear $DIST_DIR >> $STATUS_LOG_FILE 2>&1
fi

t=`date '+%Y%m%d_%H%M'`
echo "Done $t" >> $STATUS_LOG_FILE 2>&1
echo "STATUS [BUILD_TIME=$lastbuildtimestamp] SUCCESS at $t" >> $MASTER_LOG_FILE 2>&1
echo "Done $t"
exit 0
